import React, { useEffect, useState } from "react";
import { useNavigate, useParams } from "react-router-dom";
import user_service from "../service/user_service";
import Loader from "../../Pages/Loader/Loader.js";
import Toaster from "../../Pages/Toaster/Toaster";
import AllTab from "../../Pages/AllTab";
// import { renderHTML } from 'react-render-html';
// import StatusBar from "../../Pages/StatusBar.js";
import ReactPaginate from "react-paginate";

const NotesHistory = () => {
  const [loader, setLoader] = useState({ isActive: null });
  const { isActive } = loader;
  const [toaster, setToaster] = useState({
    types: null,
    isShow: null,
    toasterBody: "",
    message: "",
  });
  const { types, isShow, toasterBody, message } = toaster;

  const [summary, setSummary] = useState([]);
  const [notessummary, setNotesSummary] = useState([]);
  const [pageCount, setpageCount] = useState(0);

  const params = useParams();

  const TransactionGetById = async () => {
    await user_service.transactionGetById(params.id).then((response) => {
      setLoader({ isActive: false });
      if (response) {
        setSummary(response.data);
      }
    });
  };

  const TransactionNotes = async () => {
    await user_service.transactionNotesByTid(1, params.id).then((response) => {
      setLoader({ isActive: false });
      if (response) {
        setNotesSummary(response.data.data);
        setpageCount(Math.ceil(response.data.totalRecords / 10));
      }
    });
  };

  const handlePageClick = async (data) => {
    let currentPage = data.selected + 1;
    let skip = (currentPage - 1) * 10; // Assuming 10 items per page
    await user_service
      .transactionNotesByTid(currentPage, params.id)
      .then((response) => {
        if (response) {
          setNotesSummary(response.data.data);
          setpageCount(Math.ceil(response.data.totalRecords / 10));
        }
      });
  };

  useEffect(() => {
    window.scrollTo(0, 0);
    setLoader({ isActive: true });
    TransactionGetById(params.id);
    TransactionNotes(params.id);
  }, []);

  const navigate = useNavigate();

  const AddNote = () => {
    navigate(`/add-note/${params.id}`);
  };

  const formatDate = (dateString) => {
    const date = new Date(dateString);
    return new Intl.DateTimeFormat("en-US", {
      weekday: "short",
      year: "numeric",
      month: "2-digit",
      day: "2-digit",
      hour: "numeric",
      minute: "2-digit",
      hour12: true,
    }).format(date);
  };
  return (
    <div className="bg-secondary float-left w-100 pt-4 mb-4">
      <Loader isActive={isActive} />
      {isShow && (
        <Toaster
          types={types}
          isShow={isShow}
          toasterBody={toasterBody}
          message={message}
        />
      )}
      <main className="page-wrapper">
        <div className="transaction_tab_view">
          <div className="row">
            <AllTab />
            {/* <!-- Page container--> */}
            <div className="col-md-12">
              <div className="bg-light border rounded-3 mb-4 p-3">
                <h3 className="text-left mb-0">Transaction Notes & History</h3>
              </div>
              {/* <StatusBar/> */}
              <div className="bg-light p-3 border-0 rounded-3 shadow-sm float-start w-100">
                <div className="float-start w-100">
                  <div className="">
                    <h3 id="getContact" className="pull-left mb-2">
                      Notes
                    </h3>
                    <p className="float-start w-100 mb-0">
                      The following info is provided to keep track of every
                      change to the transaction. Having this information
                      available can greatly help your case in a dispute.
                    </p>
                  </div>
                </div>
                <div className="row">
                  <div className="col-md-12 mt-4">
                    {notessummary.map((note) => (
                      <>
                        <div
                          key={note._id}
                          className="notes-main d-flex align-items-center justify-content-start"
                        >
                          <img
                            className="rounded-circle image-note mt-3 mb-3"
                            src={note.contact_image}
                            alt="image"
                            width="75"
                            height="75"
                          />
                          <div className="ms-3">
                            <strong className="">{note.contact_name}</strong>
                            <p id="" className="mb-0">{formatDate(note.created)}</p>
                          </div>
                        </div>
                        <div className="float-left w-100">
                          {/* <h6 id="" className="float-left w-100 mb-2">{note.note_type.charAt(0).toUpperCase() + note.note_type.slice(1)}</h6> */}
                          <p
                            id=""
                            className="mb-0"
                            dangerouslySetInnerHTML={{ __html: note.message }}
                          />
                          <a
                            className="ms-1"
                            href={note.seenby}
                            target="_blank"
                            id="note-text-two"
                          >
                            {note.noteName}
                          </a>
                        </div>
                      </>
                    ))}
                  </div>
                </div>
              </div>

              <div className="float-start w-100 d-flex align-items-center justify-content-center mb-0 mt-4 ">
                <ReactPaginate
                  previousLabel={"Previous"}
                  nextLabel={"next"}
                  breakLabel={"..."}
                  pageCount={pageCount}
                  marginPagesDisplayed={1}
                  pageRangeDisplayed={2}
                  onPageChange={handlePageClick}
                  containerClassName={"pagination justify-content-center"}
                  pageClassName={"page-item"}
                  pageLinkClassName={"page-link"}
                  previousClassName={"page-item"}
                  previousLinkClassName={"page-link"}
                  nextClassName={"page-item"}
                  nextLinkClassName={"page-link"}
                  breakClassName={"page-item"}
                  breakLinkClassName={"page-link"}
                  activeClassName={"active"}
                />
              </div>

              <div className="float-left w-100 pull-right mt-4">
                <a
                  className="btn btn-primary btn-sm  pull-right"
                  onClick={AddNote}
                >
                  <i className="fi-plus me-2"></i>Add Note
                </a>
              </div>
              {/* <!-- Note History End */}
            </div>
          </div>
        </div>
      </main>
    </div>
  );
};

export default NotesHistory;

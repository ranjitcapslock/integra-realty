import React, { useRef, useState, useEffect } from "react";
import { Editor } from "react-draft-wysiwyg";
import { EditorState, convertToRaw } from "draft-js";
import draftToHtml from "draftjs-to-html";
import "react-draft-wysiwyg/dist/react-draft-wysiwyg.css";

import { useNavigate, useParams } from "react-router-dom";
import AllTab from "../../Pages/AllTab";
import user_service from "../service/user_service";
import Loader from "../../Pages/Loader/Loader.js";
import Toaster from "../../Pages/Toaster/Toaster";
import jwt from "jwt-decode";
const AddNote = () => {
  const [loader, setLoader] = useState({ isActive: null });
  const { isActive } = loader;
  const [toaster, setToaster] = useState({
    types: null,
    isShow: null,
    toasterBody: "",
    message: "",
  });
  const { types, isShow, toasterBody, message } = toaster;

  const navigate = useNavigate();
  const [formValues, setFormValues] = useState({ message: "" });

  const [editorState, setEditorState] = useState(EditorState.createEmpty());

  const handleChanges = (editorState) => {
    setEditorState(editorState);
    const htmlContent = draftToHtml(
      convertToRaw(editorState.getCurrentContent())
    );
    setFormValues({ ...formValues, message: htmlContent });
  };

  const [summary, setSummary] = useState([]);
  const params = useParams();

  // console.log(params);
  useEffect(() => {
    window.scrollTo(0, 0);
    setLoader({ isActive: true });
    const TransactionGetById = async () => {
      // console.log(summary.id);

      await user_service.transactionGetById(params.id).then((response) => {
        setLoader({ isActive: false });
        if (response) {
          setSummary(response.data);
        }
      });
    };
    TransactionGetById(params.id);
  }, []);

  const Cancel = () => {
    navigate(`/transact/detail/history/${params.id}`);
  };
  const [isSubmitClick, setISSubmitClick] = useState(false);
  const handleSubmit = async (e) => {
    e.preventDefault();
    setISSubmitClick(true);
    const userData = {
      addedBy: jwt(localStorage.getItem("auth")).id,
      agentId: jwt(localStorage.getItem("auth")).id,
      transactionId: params.id,
      message: formValues.message,
      transaction_property_address: "ADDRESS 2",
      note_type: "Agent Notes",
    };

    try {
      setLoader({ isActive: true });
      const response = await user_service.transactionNotes(userData);
      setLoader({ isActive: false });
      if (response) {
        console.log(response.data);
        setLoader({ isActive: false });
        setToaster({
          types: "Add NOtes",
          isShow: true,
          toasterBody: response.data.message,
          message: "Notes Add Successfully",
        });
        setTimeout(() => {
          navigate(`/transact/detail/history/${params.id}`);
        }, 0);
      } else {
        setLoader({ isActive: false });
        setToaster({
          types: "error",
          isShow: true,
          toasterBody: response.data.message,
          message: "Error",
        });
      }
    } catch (error) {
      setLoader({ isActive: false });
      setToaster({
        types: "error",
        isShow: true,
        toasterBody: error,
        message: "Error",
      });
    }
    setTimeout(() => {
      setToaster({
        types: "error",
        isShow: false,
        toasterBody: null,
        message: "Error",
      });
    }, 1000);
  };

  //   const handleChanges = (e) => {

  //     setFormValues({ ...formValues, eventDescription: e });
  //   };
  return (
    <div className="bg-secondary float-left w-100 pt-4 mb-4">
      <Loader isActive={isActive} />
      {isShow && (
        <Toaster
          types={types}
          isShow={isShow}
          toasterBody={toasterBody}
          message={message}
        />
      )}
      <main className="page-wrapper">
        <div className="content-overlay transaction_tab_view mt-0">
        <div className="row">
          <AllTab />
          {/* <!-- Page container--> */}
          {/* <!-- Add Note History Start */}
          <div className="col-md-12">
            <div className="d-flex align-items-center justify-content-between mb-4">
              <h3 className="text-white mb-0">Add a Note</h3>
            </div>
            <div className="bg-light border rounded-3 p-3">
              <div className="">
                <Editor
                  editorState={editorState}
                  onEditorStateChange={handleChanges}
                  value={formValues.message}
                  toolbar={{
                    options: [
                      "inline",
                      "blockType",
                      "fontSize",
                      "list",
                      "textAlign",
                      "history",
                      "link", // Add link option here
                    ],
                    inline: {
                      options: ["bold", "italic", "underline", "strikethrough"],
                    },
                    list: { options: ["unordered", "ordered"] },
                    textAlign: {
                      options: ["left", "center", "right"],
                    },
                    history: { options: ["undo", "redo"] },
                    link: {
                      // Configure link options
                      options: ["link", "unlink"],
                    },
                  }}
                  wrapperClassName="demo-wrapper"
                  editorClassName="demo-editor"
                />
              </div>
            </div>
            <div className="pull-right mt-3">
              <button
                onClick={handleSubmit}
                type="button"
                className="btn btn-primary btn-sm"
                id="save-button"
              >
                Add Note Now
              </button>
              <a className="btn btn-secondary btn-sm ms-3" onClick={Cancel}>
                Cancel & Go Back
              </a>
            </div>
          </div>
          </div>
        </div>
        {/* <!--Add Note History End */}
      </main>
    </div>
  );
};
export default AddNote;

import React, { useState, useEffect } from "react";
import Sidebar from "../Post/Sidebar.js";
import user_service from "../service/user_service.js";
import jwt from "jwt-decode";
import { useNavigate } from "react-router-dom";

const EmptyRoute = ({ Component }) => {
  const [formData, setFormData] = useState("");
  const PlatformdetailsGet = async () => {
    await user_service.platformdetailsGet().then((response) => {
      if (response) {
        setFormData(response.data.data[0]);
      }
    });
  };

  const [imageData, setImageData] = useState({});
  // const HomeImagePhotoGet = async () => {
  //   const agentId = jwt(localStorage.getItem("auth")).id;
  //   await user_service.homePagePhotoGet(agentId).then((response) => {
  //     if (response) {
  //       const groupedByImage = response.data.data.reduce((acc, item) => {
  //         const imageType = item.imageType; // Fixed variable name to be consistent
  //         if (!acc[imageType]) {
  //           acc[imageType] = [];
  //         }
  //         acc[imageType].push(item);
  //         return acc;
  //       }, {});
  //       setImageData(groupedByImage);
  //     }
  //   });
  // };

  const HomeImagePhotoGet = async () => {
    await user_service.homePagePhotoGet().then((response) => {
      if (response) {
        const groupedByImage = response.data.data.reduce((acc, item) => {
          const { imageType, is_active } = item; // Destructure to get is_active and imageType
          if (is_active === "1") { // Check if is_active is "1"
            if (!acc[imageType]) {
              acc[imageType] = [];
            }
            acc[imageType].push(item);
          }
          return acc;
        }, {});
        console.log(groupedByImage, "groupedByImage")
        setImageData(groupedByImage);
      }
    });
  };

  useEffect(() => {
    window.scrollTo(0, 0);
    PlatformdetailsGet();
    HomeImagePhotoGet();
  }, []);

  const navigate = useNavigate();
  const handleClickFeatures = () => {
    navigate("/feature-Listing");
};
  // return auth && auth ? Component : <Navigate to={"/signin"} />;
  return (
    <>
      <div
        className="featured_bg"
        style={{
          backgroundImage: `url(${
            imageData.HomeImage && imageData.HomeImage[0]?.homeImage
          })`,
        }}
      >
        <div className="container">
          <main className="page-wrapper homepage_layout">
            <div className="right_sidebar" id="auth">
              <div className="right_sidebar_innerlayout">{Component}</div>
            </div>
          </main>

          {/* <!-- Footer--> */}
        </div>
      </div>
      <footer className="footer pt-5">
        <div className="pb-2">
          <div className="row">
            <div className="col-md-4">
              <p>
                <b>Email</b>
              </p>
              <p>{formData?.email}</p>
            </div>
            <div className="col-md-4">
              <p>
                <b>Address</b>
              </p>
              <p>
                {formData?.addresline1}
                <br />
                {formData?.addresline2}
                <br />
                {formData?.addresline3}
                <br />
                {formData?.phone}
              </p>
            </div>
            <div className="col-md-4">
              <p>
                <b>Social Media</b>
              </p>
              <div className="text-nowrap">
                <a
                  className="btn btn-icon btn-translucent-light btn-xs rounded-circle me-2"
                  href={formData?.facebook}
                  target="_blank"
                  rel="noopener noreferrer"
                >
                  <i className="fi-facebook"></i>
                </a>
                <a
                  className="btn btn-icon btn-translucent-light btn-xs rounded-circle me-2"
                  href={formData?.twitter}
                  target="_blank"
                  rel="noopener noreferrer"
                >
                  <i className="fi-twitter"></i>
                </a>
                <a
                  className="btn btn-icon btn-translucent-light btn-xs rounded-circle me-2"
                  href={formData?.instagram}
                  target="_blank"
                  rel="noopener noreferrer"
                >
                  <i className="fi-messenger"></i>
                </a>
                <a
                  className="btn btn-icon btn-translucent-light btn-xs rounded-circle me-2"
                  href={formData?.telegram}
                  target="_blank"
                  rel="noopener noreferrer"
                >
                  <i className="fi-telegram"></i>
                </a>
                <a
                  className="btn btn-icon btn-translucent-light btn-xs rounded-circle"
                  href={formData?.whatsapp}
                  target="_blank"
                  rel="noopener noreferrer"
                >
                  <i className="fi-whatsapp"></i>
                </a>
              </div>
            </div>
          </div>
          <hr />
          <p className="fs-sm text-center mb-0 py-3">
            &copy; {new Date().getFullYear()} InDepth Realty Inc. All rights
            reserved.
          </p>

          <p className="fs-sm text-center mb-0" onClick={handleClickFeatures}>
            New Feature
          </p>
        </div>
      </footer>
    </>
  );
};

export default EmptyRoute;

import React, { useState, useEffect } from "react";
import { Navigate, useNavigate } from "react-router-dom";
import { authenticateUser } from "../service/storage_services";
import Sidebar from "../Post/Sidebar";
import user_service from "../service/user_service.js";
import jwt from "jwt-decode";
import Loader from "../../Pages/Loader/Loader.js";
import Toaster from "../../Pages/Toaster/Toaster.js";
import axios from "axios";
import _ from "lodash";

const ProtectRouteNew = ({ Component }) => {
  const [file, setFile] = useState(null);
  const [data, setData] = useState("");

  const auth = authenticateUser();
  const [formData, setFormData] = useState("");
  const PlatformdetailsGet = async () => {
    await user_service.platformdetailsGet().then((response) => {
      if (response) {
        setFormData(response.data.data[0]);
      }
    });
  };

  const [imageData, setImageData] = useState({});

  const [loader, setLoader] = useState({ isActive: null });
  const { isActive } = loader;
  const [toaster, setToaster] = useState({
    types: null,
    isShow: null,
    toasterBody: "",
    message: "",
  });
  const { types, isShow, toasterBody, message } = toaster;

  const initialValues = {
    email: "",
    title: "",
    agentId: "",
    description: "",
    uploadFile: "",
  };

  const [formValues, setFormValues] = useState(initialValues);
  const [formErrors, setFormErrors] = useState({});
  const [isSubmitClick, setISSubmitClick] = useState(false);

  const handleChange = (e) => {
    const { name, value } = e.target;
    setFormValues({ ...formValues, [name]: value });
  };

  useEffect(() => {
    window.scrollTo(0, 0);
    if (formValues && isSubmitClick) {
      validate();
    }
  }, [formValues]);

  const handleFileUpload = async (e) => {
    const selectedFile = e.target.files[0];

    if (selectedFile) {
      setFile(selectedFile);

      const formData = new FormData();
      formData.append("file", selectedFile);

      const config = {
        headers: {
          Accept: "application/json",
          "Content-Type": "multipart/form-data",
          Authorization: `Bearer ${localStorage.getItem("auth")}`,
        },
      };

      try {
        setLoader({ isActive: true });

        const uploadResponse = await axios.post(
          "https://api.brokeragentbase.com/upload",
          formData,
          config
        );
        const uploadedFileData = uploadResponse.data;

        setLoader({ isActive: false });
        setData(uploadedFileData);
      } catch (error) {
        console.error("Error occurred during file upload:", error);
      }
    }
  };

  const validate = () => {
    const values = formValues;
    const errors = {};

    if (!values.email) {
      errors.email = "email is required";
    } else if (
      !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.email)
    ) {
      errors.email = "Invalid email address";
    }

    if (!values.title) {
      errors.title = "Title is required";
    }

    if (!values.description) {
      errors.description = "Description is required";
    }
    setFormErrors(errors);
    return errors;
  };

  {
    /* <!--Api call Form onSubmit Start--> */
  }

  const handleReport = async (e) => {
    e.preventDefault();
    setISSubmitClick(true);
    let checkValue = validate();
    if (_.isEmpty(checkValue)) {
      const userData = {
        email: formValues.email,
        title: formValues.title,
        agentId: jwt(localStorage.getItem("auth")).id,
        description: formValues.description,
        uploadFile: data,
      };
      console.log("userData", userData);
      setLoader({ isActive: true });
      const response = await user_service.reportBugAgentPost(userData);
      if (response) {
        setLoader({ isActive: false });
        setToaster({
          types: "Bug Report Add",
          isShow: true,
          message: "Bug Report Added Successfully",
        });
        setTimeout(() => {
          setToaster((prevToaster) => ({ ...prevToaster, isShow: false }));
          document.getElementById("closeModal").click();
        }, 2000);
        setData("");
        setFormValues("");
      }
    }
  };

  const handleModalClick = () => {
    setFormValues("");
    setData("");
  };
  // const HomeImagePhotoGet = async () => {
  //   const agentId = jwt(localStorage.getItem("auth")).id;
  //   await user_service.homePagePhotoGet(agentId).then((response) => {
  //     if (response) {
  //       const groupedByImage = response.data.data.reduce((acc, item) => {
  //         const imageType = item.imageType; // Fixed variable name to be consistent
  //         if (!acc[imageType]) {
  //           acc[imageType] = [];
  //         }
  //         acc[imageType].push(item);
  //         return acc;
  //       }, {});
  //       setImageData(groupedByImage);
  //     }
  //   });
  // };

  const HomeImagePhotoGet = async () => {
    await user_service.homePagePhotoGet().then((response) => {
      if (response) {
        const groupedByImage = response.data.data.reduce((acc, item) => {
          const { imageType, is_active } = item; // Destructure to get is_active and imageType
          if (is_active === "1") { // Check if is_active is "1"
            if (!acc[imageType]) {
              acc[imageType] = [];
            }
            acc[imageType].push(item);
          }
          return acc;
        }, {});
        setImageData(groupedByImage);
      }
    });
  };

  useEffect(() => {
    window.scrollTo(0, 0);
    PlatformdetailsGet();
    HomeImagePhotoGet();
  }, []);
  // return auth && auth ? Component : <Navigate to={"/signin"} />;

  const navigate = useNavigate();
  const handleClickFeatures = () => {
    navigate("/feature-Listing");
  };
  return auth ? (
    <>
      <div
        className="featured_bg"
        style={{
          backgroundImage: `url(${
            imageData.HomeImage && imageData.HomeImage[0]?.homeImage
          })`,
        }}
      >
        <div className="container_layout">
          <main className="page-wrapper homepage_layout">
            <Sidebar />
            <div className="right_sidebar" id="auth">
              <div className="right_sidebar_innerlayout pt-0">{Component}</div>
            </div>
          </main>

          {/* <!-- Footer--> */}
        </div>

        <div className="modal in" role="dialog" id="reportbug">
          <Loader isActive={isActive} />

          {isShow && (
            <Toaster
              types={types}
              isShow={isShow}
              toasterBody={toasterBody}
              message={message}
            />
          )}
          <div
            className="modal-dialog modal-md modal-dialog-scrollable"
            role="document"
          >
            <div className="modal-content mt-5">
              <div className="modal-header">
                <h4 className="modal-title">Report a bug</h4>
                <button
                  className="btn-close"
                  id="closeModal"
                  type="button"
                  data-dismiss="modal"
                  // onClick={handleBack}
                  aria-label="closeModal"
                ></button>
              </div>

              <div className="modal-body fs-sm">
                <h6 className="modal-title text-center">
                  Tell us what's broken
                </h6>
                <div className="mb-3 mt-3">
                  <label className="form-label">Email</label>
                  <input
                    className="form-control"
                    id="email-input"
                    type="text"
                    name="email"
                    placeholder="Your email address"
                    onChange={handleChange}
                    autoComplete="on"
                    style={{
                      border: formErrors?.email
                        ? "1px solid red"
                        : "1px solid #00000026",
                    }}
                    value={formValues.email}
                  />
                  <div className="invalid-tooltip">{formErrors.email}</div>
                </div>
                <div className="mb-3">
                  <label className="form-label">Title</label>
                  <input
                    className="form-control"
                    id="email-input"
                    type="text"
                    name="title"
                    placeholder="Add a title"
                    onChange={handleChange}
                    autoComplete="on"
                    style={{
                      border: formErrors?.title
                        ? "1px solid red"
                        : "1px solid #00000026",
                    }}
                    value={formValues.title}
                  />
                  <div className="invalid-tooltip">{formErrors.title}</div>
                </div>

                <div className="mb-3">
                  <label className="form-label">Description</label>
                  <textarea
                    className="form-control mt-0"
                    id="textarea-input"
                    rows="5"
                    name="description"
                    placeholder="Describe the bug in detail"
                    onChange={handleChange}
                    autoComplete="on"
                    value={formValues.description}
                  ></textarea>
                  <div className="invalid-tooltip">
                    {formErrors.description}
                  </div>
                </div>

                <div className="mb-3">
                  {data ? (
                    <img src={data} />
                  ) : (
                    <label className="btn btn-primary documentlabel d-inline-block">
                      <input
                        id="REPC_real_estate_purchase_contract"
                        type="file"
                        onChange={handleFileUpload}
                        name="uploadFile"
                        value={formValues.uploadFile}
                      />
                      <i className="fi-plus me-2"></i> Upload
                    </label>
                  )}
                </div>
                <div className="mb-3 pull-right">
                  <button className="btn btn-primary" onClick={handleReport}>
                    Submit
                  </button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <footer className="footer pt-5">
        <div className="report_bug">
          <button
            className="btn btn-primary"
            data-toggle="modal"
            data-target="#reportbug"
            onClick={handleModalClick}
          >
            Report a bug
          </button>
        </div>
        <div className="pb-2">
          <div className="row">
            <div className="col-md-4">
              <p>
                <b>Email</b>
              </p>
              <p>{formData?.email}</p>
            </div>
            <div className="col-md-4">
              <p>
                <b>Address</b>
              </p>
              <p>
                {formData?.addresline1}
                <br />
                {formData?.addresline2}
                <br />
                {formData?.addresline3}
                <br />
                {formData?.phone}
              </p>
            </div>
            <div className="col-md-4">
              <p>
                <b>Social Media</b>
              </p>
              <div className="text-nowrap">
                <a
                  className="btn btn-icon btn-translucent-light btn-xs rounded-circle me-2"
                  href={formData?.facebook}
                  target="_blank"
                  rel="noopener noreferrer"
                >
                  <i className="fi-facebook"></i>
                </a>
                <a
                  className="btn btn-icon btn-translucent-light btn-xs rounded-circle me-2"
                  href={formData?.twitter}
                  target="_blank"
                  rel="noopener noreferrer"
                >
                  <i className="fi-twitter"></i>
                </a>
                <a
                  className="btn btn-icon btn-translucent-light btn-xs rounded-circle me-2"
                  href={formData?.instagram}
                  target="_blank"
                  rel="noopener noreferrer"
                >
                  <i className="fi-messenger"></i>
                </a>
                <a
                  className="btn btn-icon btn-translucent-light btn-xs rounded-circle me-2"
                  href={formData?.telegram}
                  target="_blank"
                  rel="noopener noreferrer"
                >
                  <i className="fi-telegram"></i>
                </a>
                <a
                  className="btn btn-icon btn-translucent-light btn-xs rounded-circle"
                  href={formData?.whatsapp}
                  target="_blank"
                  rel="noopener noreferrer"
                >
                  <i className="fi-whatsapp"></i>
                </a>
              </div>
            </div>
          </div>
          <hr />
          <p className="fs-sm text-center mb-0 py-3">
            &copy; {new Date().getFullYear()} InDepth Realty Inc. All rights
            reserved.
          </p>
          <p className="fs-sm text-center mb-0" onClick={handleClickFeatures}>
            New Feature
          </p>
        </div>
      </footer>
    </>
  ) : (
    // window.location.reload()
    <Navigate to="/signin" />
  );
};

export default ProtectRouteNew;

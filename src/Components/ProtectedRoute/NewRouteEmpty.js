import React, { useState, useEffect } from "react";
import Sidebar from "../Post/Sidebar.js";
import user_service from "../service/user_service.js";
import { useNavigate } from "react-router-dom";
import jwt from "jwt-decode";

// import HomeBG from "../img/back-agent-background-dark.jpg";

const NewRouteEmpty = ({ Component }) => {
  const [formData, setFormData] = useState("");
  const PlatformdetailsGet = async () => {
    await user_service.platformdetailsGet().then((response) => {
      if (response) {
        setFormData(response.data.data[0]);
      }
    });
  };

  useEffect(() => {
    window.scrollTo(0, 0);
    PlatformdetailsGet();
  }, []);
  // return auth && auth ? Component : <Navigate to={"/signin"} />;

  const navigate = useNavigate();
  const handleClickFeatures = () => {
    navigate("/feature-Listing");
};

  return (
    <>
      <div className="">
        <div className="">
          <main className="page-wrapper homepage_layout">
            <div className="right_sidebar" id="auth">
              <div className="right_sidebar_innerlayout p-0">{Component}</div>
            </div>
          </main>

          {/* <!-- Footer--> */}
        </div>
      </div>
      <footer className="footer pt-5">
        <div className="pb-2">
          <div className="row">
            <div className="col-md-4">
              <p>
                <b>Email</b>
              </p>
              <p>{formData?.email}</p>
            </div>
            <div className="col-md-4">
              <p>
                <b>Address</b>
              </p>
              <p>
                {formData?.addresline1}
                <br />
                {formData?.addresline2}
                <br />
                {formData?.addresline3}
                <br />
                {formData?.phone}
              </p>
            </div>
            <div className="col-md-4">
              <p>
                <b>Social Media</b>
              </p>
              <div className="text-nowrap">
                <a
                  className="btn btn-icon btn-translucent-light btn-xs rounded-circle me-2"
                  href={formData?.facebook}
                  target="_blank"
                  rel="noopener noreferrer"
                >
                  <i className="fi-facebook"></i>
                </a>
                <a
                  className="btn btn-icon btn-translucent-light btn-xs rounded-circle me-2"
                  href={formData?.twitter}
                  target="_blank"
                  rel="noopener noreferrer"
                >
                  <i className="fi-twitter"></i>
                </a>
                <a
                  className="btn btn-icon btn-translucent-light btn-xs rounded-circle me-2"
                  href={formData?.instagram}
                  target="_blank"
                  rel="noopener noreferrer"
                >
                  <i className="fi-messenger"></i>
                </a>
                <a
                  className="btn btn-icon btn-translucent-light btn-xs rounded-circle me-2"
                  href={formData?.telegram}
                  target="_blank"
                  rel="noopener noreferrer"
                >
                  <i className="fi-telegram"></i>
                </a>
                <a
                  className="btn btn-icon btn-translucent-light btn-xs rounded-circle"
                  href={formData?.whatsapp}
                  target="_blank"
                  rel="noopener noreferrer"
                >
                  <i className="fi-whatsapp"></i>
                </a>
              </div>
            </div>
          </div>
          <hr />
          <p className="fs-sm text-center mb-0 py-3">
            &copy; {new Date().getFullYear()} InDepth Realty Inc. All rights
            reserved.
          </p>

          <p className="fs-sm text-center mb-0" onClick={handleClickFeatures}>
            New Feature
          </p>
        </div>
      </footer>
    </>
  );
};

export default NewRouteEmpty;

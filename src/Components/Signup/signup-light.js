import React, { useState, useEffect } from 'react';
import images from '../img/signup.jpg';
import _ from 'lodash'
import user_service from '../service/user_service';
import Loader from "../../Pages/Loader/Loader.js";
import Toaster from '../../Pages/Toaster/Toaster.js';

function Signup() {

    const initialValues = { firstName: "", email: "", password: "", confirmPassword: "" };
    const [formValues, setFormValues] = useState(initialValues);
    const [formErrors, setFormErrors] = useState({});
    const [isSubmitClick, setISSubmitClick] = useState(false)
    const [passwordShown, setPasswordShown] = useState(false);
    const [confirmPasswordShown, setConfirmPasswordShown] = useState(false);
    const [loader, setLoader] = useState({ isActive: null })
    const { isActive } = loader;
    const [toaster, setToaster] = useState({ types: null, isShow: null, toasterBody: "", message: "" });
    const { types, isShow, toasterBody, message } = toaster;

    const togglePassword = () => {
        setPasswordShown(passwordShown ? false : true);
    };
    const toggleConfirmPassword = () => {
        setConfirmPasswordShown(confirmPasswordShown ? false : true);
    };

    {/* <!-- Input onChange Start--> */ }
    const handleChange = (e) => {
        const { name, value } = e.target;
        setFormValues({ ...formValues, [name]: value });
    };
    {/* <!-- Input onChange End--> */ }


    {/* <!-- Form Validation Start--> */ }
    useEffect(() => { 
        if (formValues && isSubmitClick) {
            validate()
        }
    }, [formValues])

    const validate = () => {
        const values = formValues
        const errors = {};
        if (!values.firstName) {
            errors.firstName = "Name is required!";
        }

        if (!values.email) {
            errors.email = "Email is required";
          } else if (
            !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i.test(values.email)
          ) {
            errors.email = "Invalid email address";
          }

          
        // if (!values.email) {
        //     errors.email = "Email is required!";
        // }
        // else if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.email)) {
        //     errors.email = 'Invalid email address'
        // }
        if (!values.password) {
            errors.password = "Password is required";
        }
        // if (values.password.length > 10) {
        //     errors.password = "Password length less then 10";
        // }
        if (values.confirmPassword === "" || values.confirmPassword !== values.password) {
            errors.confirmPassword = "Confirm password didn't match";
        }
        if (!values.confirmPassword) {
            errors.confirmPassword = "ConfirmPassword is required";
        }
        // if (values.confirmPassword.length > 10) {
        //     errors.confirmPassword = "ConfirmPassword length less then 10";
        // }

        setFormErrors(errors)
        return errors;
    }
    {/* <!-- Form Validation End--> */ }



    {/* <!-- Form onSubmit Start--> */ }
    const handleSubmit = async (e) => {
        e.preventDefault();
        setISSubmitClick(true)
        let checkValue = validate()
        if (_.isEmpty(checkValue)) {
            const userData = {
                firstName: formValues.firstName,
                company:"N/A",
                email: formValues.email,
                phone: "N/A",
                password: formValues.password,
                confirmPassword: formValues.confirmPassword,
                agentId: "N/A",
                contactType:"admin",
            };
            try {
                setLoader({ isActive: true })
                const response = await user_service.signup(userData)
                setLoader({ isActive: false })
                if (response) {
                    setLoader({ isActive: false })
                    setToaster({ type: "success", isShow: true, toasterBody: response.data.message, message: "Signup Successfully", });
                    setTimeout(() => {
                        setToaster((prevToaster) => ({ ...prevToaster, isShow: false }));
                      }, 500);
                    setTimeout(() => {
                      window.location.href = "/";
                    }, 1000);
                }
            }
            catch (error) {
                setLoader({ isActive: false })
                setToaster({
                    type: "API Error",
                    isShow: true,
                    toasterBody: error.response.data.message,
                    message: "API Error",
                });
                
                setTimeout(() => {
                    setToaster((prevToaster) => ({ ...prevToaster, isShow: false }))
                }, 2000);
            }

        }
    }

    {/* <!-- Form onSubmit End--> */ }
    return (
        <div className="bg-secondary float-left w-100 pt-4">
            <Loader isActive={isActive} />
            {isShow && <Toaster types={types}
                isShow={isShow}
                toasterBody={toasterBody}
                message={message} />}
            <main className="page-wrapper">
                {/* <!-- Page content--> */}
                <div className="container-fluid d-flex h-100 align-items-center justify-content-center py-4 py-sm-5">
                    <div className="card card-body"><a className="position-absolute top-0 end-0 nav-link fs-sm py-1 px-2 mt-3 me-3" href="#"><i className="fi-arrow-long-left fs-base me-2"></i>Go back</a>
                        <div className="row mx-0 align-items-center">
                            <div className="col-md-6 border-end-md p-2 p-sm-5">
                                <h2 className="h3 mb-4 mb-sm-5">Join Finder.<br />Get premium benefits:</h2>
                                <ul className="list-unstyled mb-4 mb-sm-5">
                                    <li className="d-flex mb-2"><i className="fi-check-circle text-primary mt-1 me-2"></i><span>Add and promote your listings</span></li>
                                    <li className="d-flex mb-2"><i className="fi-check-circle text-primary mt-1 me-2"></i><span>Easily manage your wishlist</span></li>
                                    <li className="d-flex mb-0"><i className="fi-check-circle text-primary mt-1 me-2"></i><span>Leave reviews</span></li>
                                </ul>

                                <img className="d-block mx-auto" src={images} width="344" alt="Illustartion" />
                                <div className="mt-sm-4 pt-md-3">Already have an account? <a href="/signin">Sign in</a></div>
                            </div>

                            <div className="col-md-6 border-end-md p-2 p-sm-5"><a className="btn btn-outline-info w-100 mb-3" href="#"><i className="fi-google fs-lg me-1"></i>Sign in with Google</a><a className="btn btn-outline-info w-100 mb-3" href="#"><i className="fi-facebook fs-lg me-1"></i>Sign in with Facebook</a>
                                <div className="d-flex align-items-center py-3 mb-3">
                                    <hr className="w-100" />
                                    <div className="px-3">Or</div>
                                    <hr className="w-100" />
                                </div>

                                {/* <!-- Form Values Start--> */}
                                <form className="needs-validation" onSubmit={handleSubmit}>
                                    <div className="mb-4">
                                        <label className="form-label">Name</label>
                                        <input className="form-control"
                                            type="text"
                                            name='firstName'
                                            placeholder="Enter your name"
                                            onChange={handleChange}
                                            autoComplete="on"
                                            style={{
                                                border: formErrors?.firstName ? '1px solid red' : '1px solid #00000026'
                                            }}
                                            value={formValues.firstName} />
                                        <div className="invalid-feedback">{formErrors.firstName}</div>
                                    </div>

                                    <div className="mb-4">
                                        <label className="form-label">Email address</label>
                                        <input className="form-control"
                                            type="text"
                                            name='email'
                                            placeholder="Enter your email"
                                            onChange={handleChange}
                                            autoComplete="on"
                                            style={{
                                                border: formErrors?.email ? '1px solid red' : '1px solid #00000026'
                                            }}
                                            value={formValues.email} />
                                        <div className="invalid-feedback">{formErrors.email}</div>
                                    </div>

                                    <div className="mb-4">
                                        <label className="form-label">Password</label>

                                      
                                        <div className="password-toggle">
                                            <input className="form-control"
                                                type={passwordShown ? "text" : "password"}
                                                name='password'
                                                placeholder="Enter password"
                                                onChange={handleChange}
                                                autoComplete="on"
                                                style={{
                                                    border: formErrors?.password ? '1px solid red' : '1px solid #00000026'
                                                }}
                                                value={formValues.password} />
                                                  <label className="password-toggle-btn m-0" aria-label="Show/hide password" onClick={togglePassword}>
                                                <span className="password-toggle-indicator"></span>
                                            </label>
                                            <div className="invalid-feedback">{formErrors.password}</div>

                                      
                                        </div>
                                    </div>

                                    <div className="mb-4">
                                        <label className="form-label">Confirm password</label>
                                        <div className="password-toggle">
                                            <input className="form-control"
                                                type={confirmPasswordShown ? "password" : "confirmPassword"}
                                                name='confirmPassword'
                                                placeholder="Enter confirm-password"
                                                onChange={handleChange}
                                                autoComplete="on"
                                                style={{
                                                    border: formErrors?.confirmPassword ? '1px solid red' : '1px solid #00000026'
                                                }}
                                                value={formValues.confirmPassword}
                                            />
                                            <div className="invalid-feedback">{formErrors.confirmPassword}</div>
                                            <label className="password-toggle-btn m-0" aria-label="Show/hide password" onClick={toggleConfirmPassword}>
                                                <span className="password-toggle-indicator"></span>
                                            </label>
                                        </div>
                                    </div>

                                    <div className="form-check mb-4">
                                        <input className="form-check-input"
                                            name="checkbox"
                                            type="checkbox"
                                            id="agree-to-terms"
                                            required />

                                        <label className="form-check-label">By joining, I agree to the <a href='#'>Terms of use</a> and <a href='#'>Privacy policy</a></label>
                                    </div>
                                    <button className="btn btn-primary btn-lg w-100" type="submit">Sign up</button>
                                </form>
                                {/* <!-- Form Values End--> */}
                            </div>
                        </div>
                    </div>
                </div>
            </main>
        </div>
    )
}
export default Signup;
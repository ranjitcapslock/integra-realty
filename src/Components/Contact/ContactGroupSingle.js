import React, { useState, useEffect } from "react";
import Loader from "../../Pages/Loader/Loader.js";
import Toaster from "../../Pages/Toaster/Toaster.js";
import { NavLink, useNavigate } from "react-router-dom";
import { useParams } from "react-router-dom";
import user_service from "../service/user_service";
import jwt from "jwt-decode";
import moment from "moment";
import Pic from "../img/pic.png";
import axios from "axios";

const ContactGroupSingle = () => {
  const navigate = useNavigate();
  const [loader, setLoader] = useState({ isActive: null });
  const { isActive } = loader;
  const [toaster, setToaster] = useState({
    types: null,
    isShow: null,
    toasterBody: "",
    message: "",
  });
  const { types, isShow, toasterBody, message } = toaster;
  const [summary, setSummary] = useState("");
  const params = useParams();

  useEffect(() => {
    window.scrollTo(0, 0);
    setLoader({ isActive: true });
    ContactGroup(params.id);
  }, []);

  const ContactGroup = async () => {
    await user_service.contactGroupGetById(params.id).then((response) => {
      setLoader({ isActive: false });
      if (response) {
        setSummary(response.data);
      }
    });
  };

  const [file, setFile] = useState(null);
  const [fileNew, setFileNew] = useState(null);
  const [data, setData] = useState("");
  const [fileExtension, setFileExtension] = useState("");
  const [imageData, setImageData] = useState("");

  const handleFileUpload = async (e) => {
    const selectedFile = e.target.files[0];

    if (selectedFile) {
      setFile(selectedFile);
      setFileExtension(selectedFile.name.split(".").pop());

      const formData = new FormData();
      formData.append("file", selectedFile);
      console.log(selectedFile);
      const config = {
        headers: {
          Accept: "application/json",
          "Content-Type": "multipart/form-data",
          Authorization: `Bearer ${localStorage.getItem("auth")}`,
        },
      };
      try {
        setLoader({ isActive: true });
        const uploadResponse = await axios.post(
          "https://api.brokeragentbase.com/upload",
          formData,
          config
        );
        const uploadedFileData = uploadResponse.data;
        setData(uploadedFileData);
        const updatedgetContact = {
          image: uploadedFileData,
        };

        setLoader({ isActive: true });
        await user_service
          .contactGroupUpdate(params.id, updatedgetContact)
          .then((response) => {
            if (response) {
              console.log(response.data);
              ContactGroup(params.id);
            } else {
              setLoader({ isActive: false });
              setToaster({
                type: "error",
                isShow: true,
                toasterBody: response.data.message,
                message: "Error",
              });

              setTimeout(() => {
                setToaster((prevToaster) => ({
                  ...prevToaster,
                  isShow: false,
                }));
              }, 3000);
            }
          });
      } catch (error) {
        setLoader({ isActive: false });
        setToaster({
          types: "error",
          isShow: true,
          toasterBody: error.response
            ? error.response.data.message
            : error.message,
          message: "Error",
        });

        setTimeout(() => {
          setToaster((prevToaster) => ({ ...prevToaster, isShow: false }));
        }, 1000);
      }
    }
  };

  const handleContactGroup = (id) => {
    navigate(`/contact-group-member-detail/${id}`);
  };

  return (
    <div className="bg-secondary float-left w-100 pt-4 mb-4">
      <Loader isActive={isActive} />
      {isShow && (
        <Toaster
          types={types}
          isShow={isShow}
          toasterBody={toasterBody}
          message={message}
        />
      )}
      <div className="content-overlay mt-0">
        <main className="page-wrapper getContact_page_wrap">
          {/* <!-- Page container--> */}
          <div className="">
            <div className="d-flex align-items-center justify-content-between float-left mb-3">
              <h3 className="text-white text-left mb-0">
                {summary.image ? (
                  <img
                    className="rounded-circle profile_picture p-1 me-2"
                    height="75"
                    width="75"
                    src={
                      summary.image && summary.image !== "image"
                        ? summary.image
                        : Pic
                    }
                    alt="Profile"
                  />
                ) : (
                  ""
                )}{" "}
                {summary.group_name}
              </h3>
              <button
                className="btn btn-primary"
                onClick={() => handleContactGroup(summary._id)}>
                Add Members
              </button>
            </div>
            <div className="bg-light border rounded-3 p-3">
              <div className="row">
                <div className="col-md-8">
                  <h4>Group Summary</h4>

                  <div className="card bg-secondary card-hover mb-2 mt-2">
                    <div className="card-body">
                      <div className="float-left w-100">
                        <div className="float-left w-100 d-flex align-items-center justify-content-between">
                          <div className="float-left w-100 d-flex align-items-center justify-content-start">
                            {summary.image ? (
                              <img
                                className="rounded-circle profile_picture"
                                height="50"
                                width="50"
                                src={
                                  summary.image && summary.image !== "image"
                                    ? summary.image
                                    : Pic
                                }
                                alt="Profile"
                              />
                            ) : (
                              ""
                            )}
                            {/* <div className="ms-3" onClick={()=>handleSubmit(post._id)}> */}
                            <div className="ms-3">
                              <strong>{summary.group_name}</strong>
                              <br />
                              <strong>
                                Integra Realty- {summary.groupOffice}
                              </strong>
                              <br />
                              <strong>
                                {summary.group_type === "standard"
                                  ? "Standard Group"
                                  : summary.group_type === "transaction_team"
                                  ? "Transaction Team"
                                  : ""}
                              </strong>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="col-md-4 mt-3">
                  <img
                    className="mt-3"
                    src={
                      summary.image && summary.image !== "image"
                        ? summary.image
                        : Pic
                    }
                    alt="Profile"
                  />

                  <label className="btn btn-primary ms-4 mt-2 documentlabel d-inline-block">
                    <input
                      id="REPC_real_estate_purchase_contract"
                      type="file"
                      name="file"
                      onChange={handleFileUpload}
                      // value={getContact.file}
                    />
                    Upload Photo
                  </label>
                </div>
              </div>
            </div>
          </div>
        </main>
      </div>
    </div>
  );
};

export default ContactGroupSingle;

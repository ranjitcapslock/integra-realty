import React, { useState, useEffect, useMemo, useRef } from "react";
import _ from "lodash";
import jwt from "jwt-decode";
import { Editor } from "react-draft-wysiwyg";
import { EditorState, ContentState, convertToRaw } from "draft-js";
import draftToHtml from "draftjs-to-html";
import htmlToDraft from "html-to-draftjs";
import "react-draft-wysiwyg/dist/react-draft-wysiwyg.css";
import SignaturePad from "signature_pad";

// import jsPDF from "jspdf";
import moment from "moment-timezone";
import $, { event } from "jquery";

// import { PDFDocument, } from 'pdf-lib';
import AgentAdded from "../../Components/img/Thankyou_page_Image.jpg";
import axios from "axios";

import user_service from "../service/user_service";
import Loader from "../../Pages/Loader/Loader.js";
import Toaster from "../../Pages/Toaster/Toaster.js";
import { useNavigate, useParams, NavLink } from "react-router-dom";

const VendorOnboarding = () => {
  const params = useParams();
  const [acceptedFileTypes, setAcceptedFileTypes] = useState([]);
  const navigate = useNavigate();
  const [formValuesNew, setFormValuesNew] = useState("");
  const [show, setIsShow] = useState(false);
  const [formErrors, setFormErrors] = useState({});

  const [formErrorsVendor, setFormErrorsVendor] = useState({});
  const [isSubmitClickVendor, setISSubmitClickVendor] = useState(false);

  const [isSubmitClick, setISSubmitClick] = useState(false);
  const [loader, setLoader] = useState({ isActive: null });
  const [isSubmitClickNew, setISSubmitClickNew] = useState(false);

  const { isActive } = loader;
  const [toaster, setToaster] = useState({
    type: null,
    isShow: null,
    toasterBody: "",
    message: "",
  });
  const { type, isShow, toasterBody, message } = toaster;
  const [category, setCategory] = useState("");
  const [step1, setStep1] = useState(true);
  const [step4, setStep4] = useState(false);
  const [step5, setStep5] = useState(false);
  const [phase, setPhase] = useState(""); // it was step 3, we removed it for now

  const initialValues = {
    firstName: "",
    nickName: "",
    middleName: "",
    middleInitials: "",
    prefix: "",
    lastName: "",
    maidenName: "",
    suffix: "",
    email: "",
    passwordHash: "",
    confirmPassword: "",
  };

  const [accountData, setAccountData] = useState(initialValues);

  const initialValuesVendor = {
    office: "",
    productname: "",
    productblurb: "",
    description: "",
    linkurl: "",
    email: "",
    contactName: "",
    phone: "",
    product_fullDetails: "",
    companyName: "",
    adType: "",
  };

  const initialImage = {
    image: [],
  };
  const [formValues, setFormValues] = useState(initialValuesVendor);
  const [imageData, setImageData] = useState(initialImage);

  useEffect(() => {
    window.scrollTo(0, 0);
    ContactGetByuniqueid(params.id);
    // OrganizationTreeGets();
  }, [params.id]);

  const ContactGetByuniqueid = async () => {
    await user_service.contactGetById(params.id).then((response) => {
      if (response && response.data) {
        const contactAgentId = response.data.agentId;
        if (response.data.passwordHash) {
          setStep1(false);
          setCategory(true);
        }
        setAccountData(response.data || {});
      }
    });
  };

  useEffect(() => {
    if (accountData && isSubmitClick) {
      validate();
    }
  }, [accountData, isSubmitClick]);

  const validate = () => {
    const values = accountData;
    const errors = {};

    if (values.passwordHash) {
      if (values.passwordHash.length < 8 || values.passwordHash.length > 30) {
        errors.passwordHash =
          "Password length must be between 8 and 30 characters.";
      }

      // Check for strong password criteria
      const strongPasswordRegex =
        /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,}$/;
      if (!strongPasswordRegex.test(values.passwordHash)) {
        errors.passwordHash =
          "Password must contain at least one lowercase letter, one uppercase letter, one digit, and one special character.";
      }
    } else {
      errors.passwordHash = "passwordHash is required";
    }

    if (values.confirmPassword !== values.passwordHash) {
      errors.confirmPassword = "Confirm password must match the password.";
    }

    if (values.confirmPassword) {
      if (
        values.confirmPassword.length < 8 ||
        values.confirmPassword.length > 30
      ) {
        errors.confirmPassword =
          "ConfirmPassword length must be between 8 and 30 characters.";
      }
    } else {
      errors.confirmPassword = "ConfirmPassword is required";
    }

    setFormErrors(errors);
    return errors;
  };

  const handleSubmit = async (e) => {
    if (params.id) {
      e.preventDefault();
      setISSubmitClick(true);
      let checkValue = validate();

      if (_.isEmpty(checkValue)) {
        const userData = {
          firstName: accountData.firstName,
          lastName: accountData.lastName,
          passwordHash: accountData.passwordHash,
        };

        setLoader({ isActive: true });
        const response = await user_service.contactUpdate(params.id, userData);

        if (response && response.data) {
          setCategory(response.data);
          setStep1("");
          setLoader({ isActive: false });

          setToaster({
            type: "success",
            isShow: true,
            // toasterBody: response.data.message,
            message: "Password Setup Successfully.",
          });
          setTimeout(() => {
            setToaster((prevToaster) => ({ ...prevToaster, isShow: false }));
          }, 2000);
        } else {
          setLoader({ isActive: false });
          setToaster({
            type: "error",
            isShow: true,
            toasterBody: "Error",
            message: "Something Went Wrong.",
          });
        }
      }
    }
  };

  const handleProgressbar = (stepno) => {
    if (stepno === "1") {
      setStep1((current) => !current);
      setPhase(false);
      setCategory(false);
    } else if (stepno === "2") {
      setCategory((current) => !current);
      setPhase(false);
      setStep1(false);
    } else if (stepno === "3") {
      setPhase((current) => !current);
      setCategory(false);
      setStep1(false);
      setStep5(false);
    } else if (stepno === "4") {
      setStep4((current) => !current);
      setCategory(false);
      setStep5(false);
      setStep1(false);
      setPhase(false);
    } else if (stepno === "5") {
      setStep5((current) => !current);
      setCategory(false);
      setStep4(false);
      setStep1(false);
      setPhase(false);
    }
  };

  const [passwordShownn, setPasswordShownn] = useState(false);
  const [passwordShown, setPasswordShown] = useState(false);
  const togglePasswordd = () => {
    setPasswordShownn(passwordShownn ? false : true);
  };
  const togglePassword = () => {
    setPasswordShown(passwordShown ? false : true);
  };

  const [products, setProducts] = useState([]); // To hold the filtered vendor data

  const PromotoProductsGet = async () => {
    try {
      const response = await user_service.PromotoProductsGet(
        localStorage.getItem("active_office")
      );

      if (response) {
        const vendorData = response.data.data.filter(
          (item) => item.category === "vendor"
        ); // Filter vendors
        setProducts(vendorData); // Set only vendor data
      }

      setLoader({ isActive: false });
    } catch (error) {
      console.error("Error fetching vendor data:", error);
      setLoader({ isActive: false });
    }
  };

  useEffect(() => {
    setLoader({ isActive: true });
    PromotoProductsGet();
  }, []);

  const [selectedVendor, setSelectedVendor] = useState({
    _id: "",
    description: "",
  });

  // Handle the dropdown change
  // const handleSelectChange = (e) => {
  //   const selectedOption = JSON.parse(e.target.value);
  //   setSelectedVendor(selectedOption);
  // };

  const handleSelectChange = (e) => {
    const selectedVendorId = e.target.value;
    const vendor = products.find((vendor) => vendor._id === selectedVendorId);

    // Store the selected vendor object
    setSelectedVendor(vendor || {});
  };

  const handleChange = (e) => {
    const { name, value } = e.target;
    setAccountData({ ...accountData, [name]: value });
  };

  const handleChangevendor = (e) => {
    const { name, value } = e.target;
    setFormValues({ ...formValues, [name]: value });

    $("input[name='phone']").keyup(function () {
      $(this).val(
        $(this)
          .val()
          .replace(/^(\d{3})(\d{3})(\d+)$/, "$1-$2-$3")
      );
      const formattedNumber = value.replace(
        /^(\d{3})(\d{3})(\d+)$/,
        "$1-$2-$3"
      );
      setFormValues({ ...formValues, ["phone"]: formattedNumber });
    });
  };

  const handleFileChange = async (e) => {
    const selectedFile = e.target.files[0];

    if (selectedFile) {
      const formData = new FormData();
      formData.append("file", selectedFile);

      const config = {
        headers: {
          Accept: "application/json",
          "Content-Type": "multipart/form-data",
          Authorization: `Bearer ${localStorage.getItem("auth")}`,
        },
      };

      try {
        const uploadResponse = await axios.post(
          "https://api.brokeragentbase.com/upload",
          // "http://localhost:4000/upload",
          formData,
          config
        );

        const uploadedFileURL = uploadResponse.data;
        setImageData((prevImageData) => ({
          ...prevImageData,
          image: [
            ...(prevImageData.image || []), // Ensure previous images (if any) are kept
            {
              images: uploadedFileURL, // Add the new image URL
            },
          ],
        }));
      } catch (error) {
        console.error("Error occurred during file upload:", error);
      }
    }
  };

  const [editorState, setEditorState] = useState(EditorState.createEmpty());

  const handleChangesVendor = (editorState) => {
    setEditorState(editorState);
    const htmlContent = draftToHtml(
      convertToRaw(editorState.getCurrentContent())
    );
    setFormValues({ ...formValues, product_fullDetails: htmlContent });
  };

  useEffect(() => {
    if (formValues && isSubmitClickVendor) {
      validatevendor();
    }
  }, [formValues]);

  const validatevendor = () => {
    const values = formValues;
    const errors = {};

    if (!selectedVendor.description) {
      errors.category = "Product is required."; // Add this validation for category
    }

    if (!values.productname) {
      errors.productname = "Product Name is required.";
    } else if (values.productname.length > 100) {
      errors.productname = "Product Name must be 100 characters or less.";
    }

    if (!values.adType) {
      errors.adType = "Ad Type is required.";
    }

    setFormErrorsVendor(errors);
    return errors;
  };

  const handlevendorData = async (e) => {
    e.preventDefault();
    setISSubmitClickVendor(true);
    let checkValue = validatevendor();
    if (_.isEmpty(checkValue)) {
      const userData = {
        agentId: jwt(localStorage.getItem("auth")).id,
        office_id: localStorage.getItem("active_office_id"),
        office_name: localStorage.getItem("active_office"),
        category: selectedVendor.description,
        category_id: selectedVendor._id,
        productname: formValues.productname,
        productblurb: formValues.productblurb,
        description: formValues.description,
        linkurl: formValues.linkurl,
        email: formValues.email,
        contactName: formValues.contactName,
        phone: formValues.phone,
        companyName: formValues.companyName,
        product_fullDetails: formValues.product_fullDetails,
        image: imageData.image,
        adType: formValues.adType,
      };
      try {
        setLoader({ isActive: true });
        const response = await user_service.addProductItems(userData);
        if (response) {
          setCategory("");
          // setPhase(response.data);
          setLoader({ isActive: false });
          const newProductId = response.data._id; 
          console.log(newProductId);
          const newProductData = await user_service.productItemsGetId(
            newProductId
          );

          setFormValues(newProductData.data);
          setCategory("")
          setPhase(true)
          setToaster({
            type: "Add ProductItems",
            isShow: true,
            message: "Add ProductItems Successfully",
          });

          setTimeout(() => {
            setToaster((prevToaster) => ({ ...prevToaster, isShow: false }));
          }, 1000);
        }
      } catch (error) {
        console.error("Error occurred while adding product items:", error);
        setLoader({ isActive: false });
        setToaster({
          type: "error",
          isShow: true,
          // toasterBody: error.response?.data?.message || "An error occurred while adding the product.",
          message: "Error",
        });

        setTimeout(() => {
          setToaster((prevToaster) => ({ ...prevToaster, isShow: false }));
        }, 3000);
      }
    }
  };

  // step3 function
  const [signaturePad, setSignaturePad] = useState(null);
  const [savedSignature, setSavedSignature] = useState("");
  let signatureRedoArray = [];

  const readyPad = () => {
    let wrapper = document.getElementById("signature-pad");
    if (!wrapper) {
      console.error("Signature pad wrapper not found!");
      return;
    }

    let canvas = wrapper.querySelector("canvas");
    if (!canvas) {
      console.error("Canvas element not found!");
      return;
    }

    let context = canvas.getContext("2d");
    if (!context) {
      console.error("Could not get the 2D context for canvas!");
      return;
    }

    // Set the scale and initialize the signature pad
    context.scale(1, 1);

    let tempSignaturePad = new SignaturePad(canvas, {
      backgroundColor: "rgb(255, 255, 255)",
    });

    setSignaturePad(tempSignaturePad);
  };

  useEffect(() => {
    readyPad();
  }, []);
  const handleSave = async (e) => {
    if (params.id) {
      if (signaturePad) {
        e.preventDefault();
        const dataURL = signaturePad.toDataURL();
        const signatureBlob = dataURLToBlob(dataURL);

        // Create a FormData object to send the Blob
        const formData = new FormData();
        formData.append("file", signatureBlob);

        const config = {
          headers: {
            Accept: "application/json",
            "Content-Type": "multipart/form-data",
            Authorization: `Bearer ${localStorage.getItem("auth")}`,
          },
        };

        try {
          const uploadResponse = await axios.post(
            "https://api.brokeragentbase.com/upload",
            // "http://localhost:4000/upload",
            formData,
            config
          );
          const uploadedFileData = uploadResponse.data;
          console.log(uploadedFileData);
          setSavedSignature(uploadedFileData);
          const userData = {
            staff_signature: uploadedFileData,
          };
          if (uploadedFileData) {
            setLoader({ isActive: true });
            await user_service
              .contactUpdate(params.id, userData)
              .then((response) => {
                if (response) {
                  console.log(response.data);
                  setLoader({ isActive: false });
                  setToaster({
                    type: "Update Signature",
                    isShow: true,
                    toasterBody: response.data.message,
                    message: "Update  Signature Successfully",
                  });
                  setTimeout(() => {
                    setToaster((prevToaster) => ({
                      ...prevToaster,
                      isShow: false,
                    }));
                    navigate(`/staff-signature/${params.id}/`);
                  }, 1000);
                  const ContactGet = async () => {
                    await user_service.contactGet().then((response) => {
                      if (response) {
                        console.log(response.data);
                      }
                    });
                  };
                  ContactGet();
                } else {
                  setLoader({ isActive: false });
                  setToaster({
                    types: "error",
                    isShow: true,
                    toasterBody: response.data.message,
                    message: "Error",
                  });
                }
              });
          }
        } catch (error) {
          setLoader({ isActive: false });
          setToaster({
            types: "error",
            isShow: true,
            toasterBody: error.response
              ? error.response.data.message
              : error.message,
            message: "Error",
          });
          setTimeout(() => {
            setToaster((prevToaster) => ({ ...prevToaster, isShow: false }));
          }, 2000);
        }
      }
    }
  };

  function dataURLToBlob(dataURL) {
    const parts = dataURL.split(";base64,");
    const contentType = parts[0].split(":")[1];
    const raw = window.atob(parts[1]);
    const rawLength = raw.length;
    const uInt8Array = new Uint8Array(rawLength);

    for (let i = 0; i < rawLength; ++i) {
      uInt8Array[i] = raw.charCodeAt(i);
    }

    return new Blob([uInt8Array], { type: contentType });
  }

  const handleUndo = () => {
    let signatureRemovedData = [];
    let signatureData = signaturePad.toData();
    let signatureRedoData = _.cloneDeep(signatureData); //original data

    if (signatureData.length > 0) {
      signatureData.pop(); // remove the last dot or line
      signaturePad.fromData(signatureData);
      signatureRemovedData = signatureRedoData[signatureRedoData.length - 1];
      signatureRedoArray.push(signatureRemovedData);
    }
  };

  //   const handleRedo = () => {
  //     if (signatureRedoArray.length !== 0) {
  //       let values = signaturePad.toData();
  //       let lastValue = signatureRedoArray[signatureRedoArray.length - 1];
  //       values.push(lastValue);
  //       signaturePad.fromData(values);
  //       signatureRedoArray.pop(lastValue); //remove the redo item from array
  //     }
  //   };

  const handleClear = () => {
    signaturePad.clear();
  };

  useEffect(() => {
    window.scrollTo(0, 0);
    readyPad();
  }, []);

  return (
    <div className="bg-secondary float-left w-100 pt-4 mb-4">
      <Loader isActive={isActive} />
      {isShow && (
        <Toaster
          type={type}
          isShow={isShow}
          toasterBody={toasterBody}
          message={message}
        />
      )}
      <div className="container onboarding">
        <main className="page-wrapper homepage_layout getContact_page_wrap">
          <div className="onboardingbutton">
            <div className="">
              {accountData.onboardfinal === "done" ? (
                <div className="thank_you row align-items-center mt-md-2">
                  <div className="col-lg-7 order-lg-2 mb-lg-0 mb-4 pb-2 pb-lg-0 mt-lg-0 mt-md-0 mt-sm-5 mt-5">
                    <img
                      className="d-block mx-auto"
                      src={AgentAdded}
                      alt="Hero image"
                    />
                  </div>
                  <div className="col-lg-5 order-lg-1 pe-lg-0">
                    <h1 className="display-5 mb-4 me-lg-n5 text-lg-start text-center mb-4">
                      {" "}
                      Thank you
                    </h1>
                    <p className="text-lg-start text-center mb-4 mb-lg-5 fs-lg">
                      We are pleased to inform you that your account setup is
                      already done. Our team at Indepth Realty will review your
                      application, and upon approval, you will be notified via
                      email.
                    </p>
                    <p className="text-lg-start text-center mb-4 mb-lg-5 fs-lg">
                      Thank you for choosing Indepth Realty. We look forward to
                      assisting you in your real estate endeavors.
                    </p>
                    <div className="pull-right mt-2">
                      <NavLink
                        type="button"
                        className="btn btn-primary btn-sm"
                        to="/"
                      >
                        Go To Homepage
                      </NavLink>
                    </div>
                  </div>
                </div>
              ) : (
                <>
                  <h3 className="">Welcome Onboard</h3>
                  <p>
                    Welcome to our vendor marketing platform! We’ve made it
                    simple for you to showcase your business and services
                    directly to our agents, who can help bring new opportunities
                    your way. Whether you're just starting to advertise or
                    already well-established, we’re here to make reaching your
                    goals easier. To get started, please provide the following
                    basic info about your business:
                  </p>
                  {/* <hr className="mb-3" /> */}
                  <div className="bg-light rounded-3 p-3 mb-3">
                    <div className="steps pt-4 pb-1">
                      <div
                        className={step1 ? "step active" : ""}
                        onClick={handleProgressbar}
                      >
                        <div className="step-progress">
                          <span className="step-progress-start"></span>
                          <span className="step-progress-end"></span>
                          <span className="step-number">1</span>
                        </div>
                        <div className="step-label">Password</div>
                      </div>
                      <div
                        className={category ? "step active" : ""}
                        onClick={handleProgressbar}
                      >
                        <div className="step-progress">
                          <span className="step-progress-start"></span>
                          <span className="step-progress-end"></span>
                          <span className="step-number">2</span>
                        </div>
                        <div className="step-label">Information</div>
                      </div>

                      <div
                        className={phase ? "step active" : ""}
                        onClick={handleProgressbar}
                      >
                        <div className="step-progress">
                          <span className="step-progress-start"></span>
                          <span className="step-progress-end"></span>
                          <span className="step-number">3</span>
                        </div>
                        <div className="step-label">About Me</div>
                      </div>

                      {/* <div
                          className={step4 ? "step active" : ""}
                          onClick={handleProgressbar}
                        >
                          <div className="step-progress">
                            <span className="step-progress-start"></span>
                            <span className="step-progress-end"></span>
                            <span className="step-number">3</span>
                          </div>
                          <div className="step-label">Tax info</div>
                        </div> */}

                      {/* <div
                          className={step5 ? "step active" : ""}
                          onClick={handleProgressbar}
                        >
                          <div className="step-progress">
                            <span className="step-progress-start"></span>
                            <span className="step-progress-end"></span>
                            <span className="step-number">4</span>
                          </div>
                          <div className="step-label">Document</div>
                        </div> */}
                    </div>
                  </div>

                  {step1 ? (
                    <>
                      <div className="bg-light rounded-3 p-3 mb-3">
                        {/* <h2 className="h4 mb-4">
                        <i className="fi-info-circle text-primary fs-5 mt-n1 me-2"></i>
                        Basic info
                      </h2> */}
                        <div className="row">
                          <div className="col-sm-6 mb-2">
                            <label className="form-label" for="pr-fn">
                              First name
                            </label>
                            <input
                              className="form-control form-control-lg"
                              id="inline-form-input"
                              type="text"
                              name="firstName"
                              value={accountData.firstName}
                              onChange={handleChange}
                            />
                            {formErrors.firstName && (
                              <div className="invalid-tooltip">
                                {formErrors.firstName}
                              </div>
                            )}
                          </div>

                          <div className="col-sm-6 mb-2">
                            <label className="form-label" for="pr-sn">
                              Last Name
                            </label>
                            <input
                              className="form-control form-control-lg"
                              id="inline-form-input"
                              type="text"
                              name="lastName"
                              value={accountData.lastName}
                              onChange={handleChange}
                            />
                            {formErrors.lastName && (
                              <div className="invalid-tooltip">
                                {formErrors.lastName}
                              </div>
                            )}
                          </div>

                          <div className="col-sm-6 mb-2">
                            <label className="form-label" for="pr-phone">
                              Password
                            </label>
                            <span className="text-danger">*</span>
                            <div className="password-toggle">
                              <label
                                className="password-toggle-btn"
                                aria-label="Show/hide password"
                                onClick={togglePasswordd}
                              >
                                <span className="password-toggle-indicator"></span>
                              </label>
                            </div>
                            <input
                              className="form-control form-control-lg"
                              id="inline-form-input"
                              type={passwordShownn ? "text" : "password"}
                              placeholder="create a password"
                              name="passwordHash"
                              value={accountData.passwordHash}
                              onChange={handleChange}
                            />
                            {formErrors.passwordHash && (
                              <div className="invalid-tooltip">
                                {formErrors.passwordHash}
                              </div>
                            )}
                          </div>

                          <div className="col-sm-6 mb-2">
                            <label className="form-label" for="pr-phone">
                              Confirm Password
                            </label>
                            <span className="text-danger">*</span>
                            <div className="password-toggle">
                              <label
                                className="password-toggle-btn"
                                aria-label="Show/hide password"
                                onClick={togglePassword}
                              >
                                <span className="password-toggle-indicator"></span>
                              </label>
                            </div>
                            <input
                              className="form-control form-control-lg"
                              id="inline-form-input"
                              type={passwordShown ? "text" : "password"}
                              name="confirmPassword"
                              placeholder="confirm password"
                              value={accountData.confirmPassword}
                              onChange={handleChange}
                            />
                            {formErrors.confirmPassword && (
                              <div className="invalid-tooltip">
                                {formErrors.confirmPassword}
                              </div>
                            )}
                          </div>
                        </div>
                      </div>
                      <div className="pull-right mt-3">
                        <button
                          className="btn btn-primary btn-sm"
                          onClick={handleSubmit}
                        >
                          Next step
                          <i className="fi-chevron-right fs-sm ms-2"></i>
                        </button>
                      </div>
                    </>
                  ) : (
                    ""
                  )}

                  {category ? (
                    <>
                      {/* <div className="bg-light rounded-3 p-3 mb-3">
                        <aside className="col-lg-12 col-md-5 pe-xl-4 mb-5">
                          <div className="multitab-action">
                            <div className="d-flex d-md-block d-lg-flex align-items-start pt-lg-2 mb-4">
                              <img
                                width="48"
                                height="48"
                                className="rounded-circle getContact_picture"
                                src={getContact?.image || Pic}
                                alt="getContact"
                              />
                              <div className="pt-md-2 pt-lg-0 ps-3 ps-md-0 ps-lg-3">
                                <h2 className="fs-lg mb-0">
                                  {getContact?.firstName} {getContact?.lastName}
                                </h2>
                                <ul className="list-unstyled fs-sm mt-2 mb-0 ps-0">
                                  {getContact?.phone ? (
                                    <>
                                      <li className="pb-0">
                                        <a
                                          className="nav-link fw-normal p-0"
                                          href={`tel:${
                                            getContact?.phone ?? ""
                                          }`}
                                        >
                                          <i className="fi-phone opacity-60 me-2"></i>
                                          {getContact?.phone}
                                        </a>
                                      </li>
                                    </>
                                  ) : (
                                    ""
                                  )}

                                  <li className="pb-2">
                                    <a
                                      className="nav-link fw-normal p-0"
                                      href={`mailto:${getContact?.email ?? ""}`}
                                    >
                                      <i className="fi-mail opacity-60 me-2"></i>
                                      {getContact?.email}
                                    </a>
                                  </li>
                                </ul>
                              </div>
                            </div>
                            {agentId ? (
                              <div className="float-left text-center">
                                <label className="btn btn-primary documentlabel">
                                  <input
                                    id="REPC_real_estate_purchase_contract"
                                    type="file"
                                    accept={acceptedFileTypes
                                      .map((type) => `.${type}`)
                                      .join(",")}
                                    name="file"
                                    onChange={handleFileUpload}
                                    value={getContact.file}
                                  />
                                  <i className="fi-plus me-2"></i> Add a Photo
                                </label>
                                {formErrors.image && (
                                  <div
                                    className="invalid-tooltip"
                                    style={{
                                      backgroundColor: "#f6f3f800",
                                      float: "left",
                                    }}
                                  >
                                    {formErrors.image}
                                  </div>
                                )}
                              </div>
                            ) : (
                              ""
                            )}
                          </div>
                        </aside>
                      </div> */}

                      <div className="col-md-12">
                        <div className="bg-light rounded-3 p-3  mb-3">
                          {/* <h2 className="h4 mb-4">
                            Contact
                          </h2> */}
                          <div className="row">
                            <div className="col-sm-6 mb-2">
                              <label className="form-label" for="pr-fn">
                                Name of Company
                                {/* <span className="text-danger">*</span> */}
                              </label>
                              <input
                                className="form-control form-control-lg"
                                id="inline-form-input"
                                type="text"
                                name="companyName"
                                value={formValues.companyName}
                                onChange={handleChangevendor}
                              />
                              {formErrors.companyName && (
                                <div className="invalid-tooltip">
                                  {formErrors.companyName}
                                </div>
                              )}
                            </div>
                            <div className="col-sm-6 mb-2">
                              <label className="form-label" htmlFor="pr-sn">
                                Describe product or service
                              </label>

                              <select
                                className="form-select"
                                id="pr-city"
                                name="category"
                                value={selectedVendor._id || ""} // Ensure the selected _id is bound
                                onChange={handleSelectChange}
                              >
                                <option value="">--Select--</option>
                                {products.length > 0 ? (
                                  products.map((vendor, index) => (
                                    <option
                                      key={index}
                                      value={vendor._id} // Store only _id for simplicity
                                    >
                                      {vendor.title}
                                    </option>
                                  ))
                                ) : (
                                  <option>No vendors available</option>
                                )}
                              </select>

                              {formErrorsVendor?.category && (
                                <div className="invalid-tooltip">
                                  {formErrorsVendor.category}
                                </div>
                              )}
                            </div>

                            <div className="col-sm-6 mb-2">
                              <label className="form-label">
                                Product Name *
                              </label>
                              <input
                                className="form-control form-control-lg"
                                id="inline-form-input"
                                type="text"
                                name="productname"
                                value={formValues.productname}
                                onChange={handleChangevendor}
                                style={{
                                  border: formErrorsVendor?.productname
                                    ? "1px solid red"
                                    : "1px solid #00000026",
                                }}
                              />
                              {formErrorsVendor.productname && (
                                <div className="invalid-tooltip">
                                  {formErrorsVendor.productname}
                                </div>
                              )}
                            </div>

                            <div className="col-sm-6 mb-2">
                              <label className="form-label" for="pr-fn">
                                Product Blurb (Max Length: 50 characters)
                              </label>
                              <input
                                className="form-control form-control-lg"
                                id="inline-form-input"
                                type="text"
                                name="productblurb"
                                value={formValues.productblurb}
                                onChange={handleChangevendor}
                              />
                            </div>

                            <div className="col-sm-6 mb-2">
                              <label className="form-label" for="pr-fn">
                                Product Description (Max Length: 500 characters)
                              </label>
                              <input
                                className="form-control form-control-lg"
                                id="inline-form-input"
                                type="text"
                                name="description"
                                value={formValues.description}
                                onChange={handleChangevendor}
                              />
                            </div>

                            <div className="col-sm-6 mb-2">
                              <label className="form-label" for="pr-fn">
                                Website Link URL
                              </label>
                              <input
                                className="form-control form-control-lg"
                                id="inline-form-input"
                                type="text"
                                name="linkurl"
                                value={formValues.linkurl}
                                onChange={handleChangevendor}
                              />
                            </div>

                            <div className="col-sm-6 mb-2">
                              <label className="form-label" for="pr-fn">
                                Owner Name
                              </label>
                              <input
                                className="form-control form-control-lg"
                                id="inline-form-input"
                                type="text"
                                name="contactName"
                                value={formValues.contactName}
                                onChange={handleChangevendor}
                              />
                            </div>

                            <div className="col-sm-6 mb-2">
                              <label className="form-label" for="pr-fn">
                                Email
                              </label>
                              <input
                                className="form-control form-control-lg"
                                id="inline-form-input"
                                type="text"
                                name="email"
                                value={formValues.email}
                                onChange={handleChangevendor}
                              />
                            </div>

                            <div className="col-sm-6 mb-2">
                              <label className="form-label" for="pr-fn">
                                Phone
                              </label>
                              <input
                                className="form-control form-control-lg"
                                id="inline-form-input"
                                type="text"
                                name="phone"
                                value={formValues.phone}
                                onChange={handleChangevendor}
                              />
                            </div>
                            <div className="col-sm-6 mb-2">
                              <div className="my-3 file-uploader pt-3">
                                <label className="form-label">
                                  Select a photo to upload:
                                </label>
                                <br />
                                <input
                                  id="fileUpload"
                                  type="file"
                                  name="image"
                                  onChange={handleFileChange}
                                  accept={acceptedFileTypes
                                    .map((type) => `.${type}`)
                                    .join(",")}
                                />
                              </div>

                              <div className="productItemImage">
                                {imageData.image.length > 0 && (
                                  <>
                                    {imageData.image.map(
                                      (uploadedImage, index) => (
                                        <>
                                          {uploadedImage ? (
                                            <div className="product_images">
                                              <img
                                                className="m-0 mb-3 shadow-sm"
                                                src={uploadedImage.images}
                                              />
                                              {/* <span className="delete_data">
                                    <img
                                      className="m-0"
                                      onClick={(e) =>
                                        handleRemove(uploadedImage.images)
                                      }
                                      src="https://cdn.pboffice.net/shared/transaction/page_select_drop.png"
                                    />
                                  </span> */}
                                            </div>
                                          ) : (
                                            ""
                                          )}
                                        </>
                                      )
                                    )}
                                  </>
                                )}
                              </div>
                            </div>

                            <div className="col-md-12 mt-3">
                              <label
                                htmlFor="ad-type-select"
                                className="form-label"
                              >
                                Choose your ad type:
                              </label>
                              <select
                                className="form-select"
                                id="pr-city"
                                name="adType"
                                value={formValues.adType}
                                onChange={handleChangevendor}
                                style={{
                                  border: formErrorsVendor?.adType
                                    ? "1px solid red"
                                    : "1px solid #00000026",
                                }}
                              >
                                <option>--Select Ad Type--</option>
                                <option value="Basic">
                                  Basic: $60 for six months – Small ad in the
                                  vendor list.
                                </option>
                                <option value="Silver">
                                  Silver: $120 for six months – Regular-sized ad
                                  in the vendor list (current size).
                                </option>
                                <option value="Platinum">
                                  Platinum: $350 for six months – Banner ad on
                                  the homepage.
                                </option>
                                <option value="Exclusive">
                                  Exclusive Founder: $1500 for six months –
                                  Regular-sized ad in the vendor list and a
                                  banner ad at the top of the
                                  homepage.(Exclusive to the first in each
                                  category—no competitors will be listed in the
                                  same category.)
                                </option>
                              </select>
                              {formErrorsVendor.adType && (
                                <div className="invalid-tooltip">
                                  {formErrorsVendor.adType}
                                </div>
                              )}
                            </div>

                            <div className="col-md-12 mt-3">
                              <label className="form-label">
                                Product Full Details
                              </label>
                              <Editor
                                editorState={editorState}
                                onEditorStateChange={handleChangesVendor}
                                value={formValues.product_fullDetails}
                                toolbar={{
                                  options: [
                                    "inline",
                                    "blockType",
                                    "fontSize",
                                    "list",
                                    "textAlign",
                                    "history",
                                    "link", // Add link option here
                                  ],
                                  inline: {
                                    options: [
                                      "bold",
                                      "italic",
                                      "underline",
                                      "strikethrough",
                                    ],
                                  },
                                  list: { options: ["unordered", "ordered"] },
                                  textAlign: {
                                    options: ["left", "center", "right"],
                                  },
                                  history: { options: ["undo", "redo"] },
                                  link: {
                                    // Configure link options
                                    options: ["link", "unlink"],
                                  },
                                }}
                                wrapperClassName="demo-wrapper"
                                editorClassName="demo-editor"
                              />
                            </div>
                          </div>
                        </div>
                        <div className="pull-right mt-3">
                          <button
                            className="btn btn-primary btn-sm"
                            onClick={handlevendorData}
                          >
                            Next step
                            <i className="fi-chevron-right fs-sm ms-2"></i>
                          </button>
                        </div>
                      </div>
                    </>
                  ) : (
                    ""
                  )}

                  {phase ? (
                    <div className="">
                      <div className="bg-light border rounded-3 p-3 mb-3">
                        <aside className="col-lg-12 col-md-5 pe-xl-4 mb-5">
                          <div className="">
                            <div className="col-md-8">
                              <div className="float-left w-100 d-flex align-items-center justify-content-between mb-4">
                                <h3 className="mb-0">Add Signature</h3>
                              </div>
                              <div className="">
                                <p className="">
                                  Use a touch screen device or your mouse to
                                  draw in the box below.
                                </p>
                                <div id="signature-pad">
                                  <canvas className="signature-canvas"></canvas>
                                </div>
                              </div>
                              <div className="pull-right mt-3">
                                <button
                                  className="btn btn-primary ms-2 px-3 px-sm-4"
                                  type="button"
                                  onClick={handleSave}
                                >
                                  Save Signature
                                </button>
                                <button
                                  className="btn btn-secondary ms-3 px-3 px-sm-4"
                                  onClick={handleUndo}
                                >
                                  <i
                                    className="fa fa-undo me-1"
                                    aria-hidden="true"
                                  ></i>{" "}
                                  Undo
                                </button>

                                <button
                                  className="btn btn-secondary ms-3 px-3 px-sm-4"
                                  onClick={handleClear}
                                >
                                  <i
                                    className="fa fa-eraser me-1"
                                    aria-hidden="true"
                                  ></i>{" "}
                                  Clear
                                </button>

                                {/* <NavLink to={`/staff-signature/${params.id}`}
                className="btn btn-secondary ms-3 px-3 px-sm-4"
                type="button"
              >
                Cancel & Go Back
              </NavLink> */}
                              </div>
                            </div>
                          </div>
                        </aside>
                      </div>
                    </div>
                  ) : (
                    ""
                  )}
                </>
              )}
            </div>
          </div>
        </main>
      </div>
    </div>
  );
};

export default VendorOnboarding;

import React, { useState, useEffect, useMemo } from "react";
import _ from "lodash";
import user_service from "../service/user_service";
import jwt from "jwt-decode";
import Loader from "../../Pages/Loader/Loader.js";
import Toaster from "../../Pages/Toaster/Toaster.js";
import countryList from "country-list";
import { useParams, useNavigate } from "react-router-dom";
import Pic from "../img/pic.png";
import { MultiSelect } from "react-multi-select-component";
import { MultiSelect as MultiSelect2 } from "react-multi-select-component";

function ActivateAccount() {
  const initialValues = {
    office: "Corporate",
    intranet_id: "",
    subscription_level: "Intranet Only",
    admin_note: "",
    sponsor_associate: "",
    staff_recruiter: "",
    staff_recruiter_id: "",
    nrds_id: "",
    associate_member: "Broker Plan",
    plan_date: "",
    billing_date: "",
    account_name: "Integra Reality",
    invoicing: "Credit Card Auto-Bill",
    licensed_since: "",
    associate_since: "",
    staff_since: "",
    iscorporation: "",
    agentTax_ein: "",
    agent_funding: "",
    ssn_itin: "",
    date_birth: "",
    private_idcard_type: "Driver License",
    private_idstate: "Utah",
    private_idnumber: "",
    license_state: "",
    license_type: "",
    license_number: "",
    date_issued: "",
    date_expire: "",
    mls_membership: [],
    board_membership: [],
    office_affiliation: "",
    agent_id: "",
    parkcityagent_id: "",
    washingtonagent_id: "",
    ironcountyagent_id: "",

    activatecontact: "",
  };
  const [board_membership, setBoard_membership] = useState([]);

  const [mls_membership, setMls_membership] = useState([]);

  const [formValues, setFormValues] = useState(initialValues);
  const [loader, setLoader] = useState({ isActive: null });
  const { isActive } = loader;
  const [toaster, setToaster] = useState({
    types: null,
    isShow: null,
    toasterBody: "",
    message: "",
  });
  const { types, isShow, toasterBody, message } = toaster;
  const [getContact, setGetContact] = useState("");
  const [checkAll, setCheckAll] = useState("");
  const [organizationGet, setOrganizationGet] = useState([]);

  const params = useParams();
  const navigate = useNavigate();

  const options = useMemo(() => {
    const countries = countryList.getData();
    const utahState = {
      code: "UT",
      name: "Utah",
      districts: [
        "Alabama",
        "Alaska",
        "Arizona",
        "Arkansas",
        "California",
        "Colorado",
        "Connecticut",
        "Delaware",
        "District of Columbia",
        "Florida",
        "Georgia",
        "Hawaii",
        "Idaho",
        "Illinois",
        "Indiana",
        "Iowa",
        "Kansas",
        "Kentucky",
        "Louisiana",
        "Maine",
        "Maryland",
        "Massachusetts",
        "Michigan",
        "Minnesota",
        "Mississippi",
        "Missouri",
        "Montana",
        "Nebraska",
        "Nevada",
        "New Hampshire",
        "New Jersey",
        "New Mexico",
        "New York",
        "North Carolina",
        "North Dakota",
        "Ohio",
        "Oklahoma",
        "Oregon",
        "Pennsylvania",
        "Rhode Island",
        "South Carolina",
        "South Dakota",
        "Tennessee",
        "Texas",
        "Utah",
        "Vermont",
        "Virginia",
        "Washington",
        "West Virginia",
        "Wisconsin",
        "Wyoming",
      ],
    };
    return [...countries, utahState];
  }, []);

  const optionss = [
    { label: "Grapes 🍇", value: "grapes" },
    { label: "Mango 🥭", value: "mango" },
    { label: "Strawberry 🍓", value: "strawberry", disabled: true },
  ];

  useEffect(() => {
    ContactGetById(params.id);
  }, []);

  const ContactGetById = async () => {
    await user_service.contactGetById(params.id).then((response) => {
      if (response) {
        setGetContact(response.data);

        if (response.data && response.data.additionalActivateFields) {
          setFormValues((prevFormValues) => ({
            ...prevFormValues,
            office:
              response.data.additionalActivateFields.office ||
              prevFormValues.office,
            //   intranet_id: response.data.additionalActivateFields.intranet_id || prevFormValues.intranet_id,
            intranet_id:
              response.data.additionalActivateFields.intranet_id ||
              response.data.email,
            subscription_level:
              response.data.additionalActivateFields.subscription_level ||
              prevFormValues.subscription_level,
            admin_note:
              response.data.additionalActivateFields.admin_note ||
              prevFormValues.admin_note,
            sponsor_associate:
              response.data.additionalActivateFields.sponsor_associate ||
              prevFormValues.sponsor_associate,
            staff_recruiter:
              response.data.additionalActivateFields.staff_recruiter ||
              prevFormValues.staff_recruiter,
            nrds_id:
              response.data.additionalActivateFields.nrds_id ||
              prevFormValues.nrds_id,
            associate_member:
              response.data.additionalActivateFields.associate_member ||
              prevFormValues.associate_member,
            plan_date:
              response.data.additionalActivateFields.plan_date ||
              prevFormValues.plan_date,
            billing_date:
              response.data.additionalActivateFields.billing_date ||
              prevFormValues.billing_date,
            account_name:
              response.data.additionalActivateFields.account_name ||
              prevFormValues.account_name,
            invoicing:
              response.data.additionalActivateFields.invoicing ||
              prevFormValues.invoicing,
            licensed_since:
              response.data.additionalActivateFields.licensed_since ||
              prevFormValues.licensed_since,
            associate_since:
              response.data.additionalActivateFields.associate_since ||
              prevFormValues.associate_since,
            staff_since:
              response.data.additionalActivateFields.staff_since ||
              prevFormValues.staff_since,
            iscorporation:
              response.data.additionalActivateFields.iscorporation ||
              prevFormValues.iscorporation,
            agentTax_ein:
              response.data.additionalActivateFields.agentTax_ein ||
              prevFormValues.agentTax_ein,
            agent_funding:
              response.data.additionalActivateFields.agent_funding ||
              prevFormValues.agent_funding,
            ssn_itin:
              response.data.additionalActivateFields.ssn_itin ||
              prevFormValues.ssn_itin,
            date_birth:
              response.data.additionalActivateFields.date_birth ||
              prevFormValues.date_birth,
            private_idcard_type:
              response.data.additionalActivateFields.private_idcard_type ||
              prevFormValues.private_idcard_type,
            private_idstate:
              response.data.additionalActivateFields.private_idstate ||
              prevFormValues.private_idstate,
            private_idnumber:
              response.data.additionalActivateFields.private_idnumber ||
              prevFormValues.private_idnumber,
            license_state:
              response.data.additionalActivateFields.license_state ||
              prevFormValues.license_state,
            license_type:
              response.data.additionalActivateFields.license_type ||
              prevFormValues.license_type,
            license_number:
              response.data.additionalActivateFields.license_number ||
              prevFormValues.license_number,
            date_issued:
              response.data.additionalActivateFields.date_issued ||
              prevFormValues.date_issued,
            date_expire:
              response.data.additionalActivateFields.date_expire ||
              prevFormValues.date_expire,
            //   mls_membership: response.data.additionalActivateFields.mls_membership || prevFormValues.mls_membership,
            office_affiliation:
              response.data.additionalActivateFields.office_affiliation ||
              prevFormValues.office_affiliation,

            agent_id:
              response.data.additionalActivateFields.agent_id ||
              prevFormValues.agent_id,

            parkcityagent_id:
              response.data.additionalActivateFields.parkcityagent_id ||
              prevFormValues.parkcityagent_id,

            washingtonagent_id:
              response.data.additionalActivateFields.washingtonagent_id ||
              prevFormValues.washingtonagent_id,

            ironcountyagent_id:
              response.data.additionalActivateFields.ironcountyagent_id ||
              prevFormValues.ironcountyagent_id,
          }));
          // setBoard_membership(response.data.additionalActivateFields.board_membership ? JSON.parse(response.data.additionalActivateFields.board_membership) : "");
          // setMls_membership(response.data.additionalActivateFields.mls_membership ? JSON.parse(response.data.additionalActivateFields.mls_membership) : "");

          const mls_membershipString =
            response.data.additionalActivateFields.mls_membership;

          // if(mls_membershipString){
          //     // Check if mls_membershipString is a string
          //     if (typeof mls_membershipString === 'string') {
          //         try {
          //         // Attempt to parse the string as JSON
          //         const mls_membershipParsed = JSON.parse(mls_membershipString);
          //         // Set the parsed value to your state or variable
          //         setMls_membership(mls_membershipParsed);
          //         } catch (error) {
          //         // Handle the error if parsing fails
          //         console.error('Error parsing mls_membership as JSON:', error);
          //         // You might want to set a default value or handle the error in another way
          //         }
          //     } else {
          //         // Handle the case where mls_membershipString is not a string (e.g., it's already an object)
          //         // You can directly set it to your state or variable
          //         setMls_membership(mls_membershipString);
          //     }
          // }
          if (mls_membershipString) {
            // Check if mls_membershipString is a string
            if (typeof mls_membershipString === "string") {
              try {
                // Attempt to parse the string as JSON
                const mls_membershipParsed = JSON.parse(mls_membershipString);
                // Set the parsed value to your state or variable
                if (mls_membershipParsed == "") {
                  setMls_membership(mls_membership);
                } else {
                  setMls_membership(mls_membershipParsed);
                }
              } catch (error) {
                // Handle the error if parsing fails
                console.error("Error parsing mls_membership as JSON:", error);
                // You might want to set a default value or handle the error in another way
                // For debugging, you can log the JSON parse error message:
                console.error("JSON Parse Error:", error.message);
                // Set a default value if parsing fails
                setMls_membership(mls_membership); // Replace "defaultValue" with your default value
              }
            } else {
              // Handle the case where mls_membershipString is not a string (e.g., it's already an object)
              // You can directly set it to your state or variable
              setMls_membership(mls_membership);
            }
          } else {
            // Handle the case where mls_membershipString is undefined or null
            // Set a default value or handle it as needed
            setMls_membership(mls_membership); // Replace "defaultValue" with your default value
          }

          const board_membershipString =
            response.data.additionalActivateFields.board_membership;
          if (board_membershipString) {
            // Check if board_membershipString is a string
            if (typeof board_membershipString === "string") {
              try {
                // Attempt to parse the string as JSON
                const board_membershipParsed = JSON.parse(
                  board_membershipString
                );
                // Set the parsed value to your state or variable
                if (board_membershipParsed == "") {
                  setBoard_membership(board_membership);
                } else {
                  setBoard_membership(board_membershipParsed);
                }
              } catch (error) {
                // Handle the error if parsing fails
                console.error("Error parsing board_membership as JSON:", error);
                // You might want to set a default value or handle the error in another way
                // For debugging, you can log the JSON parse error message:
                console.error("JSON Parse Error:", error.message);
                // Set a default value if parsing fails
                setBoard_membership(board_membership); // Replace "defaultValue" with your default value
              }
            } else {
              console.log("sdfdsfdsfsd");
              // Handle the case where board_membershipString is not a string (e.g., it's already an object)
              // You can directly set it to your state or variable
              setBoard_membership(board_membership);
            }
          } else {
            // Handle the case where board_membershipString is undefined or null
            // Set a default value or handle it as needed
            setBoard_membership(board_membership); // Replace "defaultValue" with your default value
          }
        }
      }
    });
  };

  const OrganizationTreeGets = async () => {
    // setLoader({ isActive: true });
    if (jwt(localStorage.getItem("auth")).id) {
      const agentId = jwt(localStorage.getItem("auth")).id;
      await user_service.organizationTreeGet(agentId).then((response) => {
        // setLoader({ isActive: false });
        if (response) {
          setOrganizationGet(response.data);
        }
      });
    }
  };

  useEffect(() => {
    OrganizationTreeGets();
  }, []);

  const handleBack = () => {
    navigate(`/contact-profile/${params.id}`);
  };

  const CheckBox = (e) => {
    // console.log(e.target.name);
    // console.log(e.target.value);

    setCheckAll(e.target.name);
    setCheckAll(e.target.value);
  };

  const handleChange = (e) => {
    const { name, value } = e.target;
    setFormValues({ ...formValues, [name]: value });
    if (e.target.name === "staff_recruiter") {
      SearchGetAll(e.target.value);
    }
  };

  const handleSubmit = async (e) => {
    e.preventDefault();

    const additionalActivateFields = {
      office: formValues.office,
      intranet_id: formValues.intranet_id,
      subscription_level: formValues.subscription_level,
      admin_note: formValues.admin_note,
      sponsor_associate: formValues.sponsor_associate,
      staff_recruiter: formValues.staff_recruiter,
      staff_recruiter_id: formValues.staff_recruiter_id,
      nrds_id: formValues.nrds_id,
      associate_member: formValues.associate_member,
      plan_date: formValues.plan_date,
      billing_date: formValues.billing_date,
      account_name: formValues.account_name,
      invoicing: formValues.invoicing,
      licensed_since: formValues.licensed_since,
      associate_since: formValues.associate_since,
      staff_since: formValues.staff_since,
      iscorporation: checkAll,
      agentTax_ein: formValues.agentTax_ein,
      agent_funding: formValues.agent_funding,
      ssn_itin: formValues.ssn_itin,
      date_birth: formValues.date_birth,
      private_idcard_type: formValues.private_idcard_type,
      private_idstate: formValues.private_idstate,
      private_idnumber: formValues.private_idnumber,
      license_state: formValues.license_state,
      license_type: formValues.license_type,
      license_number: formValues.license_number,
      date_issued: formValues.date_issued,
      date_expire: formValues.date_expire,
      // mls_membership: formValues.mls_membership,
      mls_membership: JSON.stringify(mls_membership),
      board_membership: JSON.stringify(board_membership),
      office_affiliation: formValues.office_affiliation,
      agent_id: formValues.agent_id,
      parkcityagent_id: formValues.parkcityagent_id,
      washingtonagent_id: formValues.washingtonagent_id,
      ironcountyagent_id: formValues.ironcountyagent_id,
      activatecontact: "yes",
    };

    const userData = {
      // agentId: jwt(localStorage.getItem("auth")).id,
      additionalActivateFields,
    };
    try {
      setLoader({ isActive: true });
      await user_service.contactUpdate(params.id, userData).then((response) => {
        if (response) {
          setLoader({ isActive: false });
          setToaster({
            type: "Profile Updated Successfully",
            isShow: true,
            toasterBody: response.data.message,
            message: "Profile Updated Successfully",
          });
          setTimeout(() => {
            setToaster((prevToaster) => ({ ...prevToaster, isShow: false }));
            // navigate(`/contact-profile/${params.id}`);
          }, 1000);

          ContactGetById(params.id);
        } else {
          setLoader({ isActive: false });
          setToaster({
            types: "error",
            isShow: true,
            toasterBody: response.data.message,
            message: "Error",
          });
        }
      });
    } catch (error) {
      setLoader({ isActive: false });
      setToaster({
        types: "error",
        isShow: true,
        toasterBody: error.response
          ? error.response.data.message
          : error.message,
        message: "Error",
      });

      setTimeout(() => {
        setToaster((prevToaster) => ({ ...prevToaster, isShow: false }));
      }, 1000);
    }
  };
  const [isLoading, setIsLoading] = useState(false);
  const [results, setResults] = useState([]);

  const SearchGetAll = async () => {
    setIsLoading(true);

    await user_service
      .SearchContactGet(0,3, formValues.staff_recruiter)

      .then((response) => {
        setIsLoading(false);
        if (response) {
          setResults(response.data.data);
        }
      });
  };

  const [clientId, setClientId] = useState("");
  const [conatctdata, setConatctdata] = useState("");

  const handleSearchTransaction = async (conatactdata) => {
    setClientId(conatactdata._id);
    setConatctdata(conatactdata);
    setFormValues({
      ...formValues,
      staff_recruiter: conatactdata.firstName + " " + conatactdata.lastName,
      staff_recruiter_id: conatactdata._id,
    });
    setResults("");
  };
  const profilepic = (e) => {
    e.target.src = Pic;
  }
  return (
    <div className="float-left w-100">
      <Loader isActive={isActive} />
      {isShow && (
        <Toaster
          types={types}
          isShow={isShow}
          toasterBody={toasterBody}
          message={message}
        />
      )}
      <main className="page-wrapper profile_page_wrap">
        <div className="content-overlay">
          <div className="">
            <div className="">
              <div className="">
                <div className="">
                  <div className="float-left w-100 d-flex align-items-center justify-content-between mb-3">
                    <h3 id="getContact" className="pull-left mb-0 text-white">
                      Edit Account
                    </h3>
                  </div>
                  <div className="add_listing border bg-light rounded-3 p-3">
                    <label className="col-form-label">Office</label>
                    <div className="col-lg-12 mb-3">
                      <label className="form-label" htmlFor="pr-country">
                        Assigned Home Office
                      </label>
                      <select
                        className="form-select"
                        id="pr-country"
                        name="office"
                        value={formValues.office}
                        onChange={handleChange}
                      >
                        {organizationGet.data &&
                          organizationGet.data.map((item) => (
                            <option key={item._id} value={item.name}>
                              {item.name}
                            </option>
                          ))}
                      </select>
                    </div>
                    <div className="col-lg-12 mb-3">
                      <label className="col-form-label">Account</label>
                      <div className="col-lg-12 mb-3">
                        <label className="form-label" for="pr-city">
                          Intranet ID
                        </label>
                        {getContact.additionalActivateFields &&
                        getContact.additionalActivateFields.intranet_id ? (
                          <input
                            className="form-control"
                            id="inline-form-input"
                            type="text"
                            name="intranet_id"
                            value={
                              getContact.additionalActivateFields &&
                              getContact.additionalActivateFields.intranet_id
                                ? getContact.additionalActivateFields
                                    .intranet_id
                                : formValues.intranet_id
                            }
                            onChange={handleChange}
                            readonly
                          />
                        ) : (
                          <input
                            className="form-control"
                            id="inline-form-input"
                            type="text"
                            name="intranet_id"
                            value={
                              getContact.additionalActivateFields &&
                              getContact.additionalActivateFields.intranet_id
                                ? getContact.additionalActivateFields
                                    .intranet_id
                                : formValues.intranet_id
                            }
                            onChange={handleChange}
                          />
                        )}
                      </div>
                    </div>
                    <div className="col-lg-12 mb-3">
                      <label className="form-label" for="pr-city">
                        BackAgent Subscription Level
                      </label>
                      <select
                        className="form-select"
                        id="pr-city"
                        name="subscription_level"
                        value={formValues.subscription_level}
                        onChange={handleChange}
                      >
                        <option>Intranet Only</option>
                        <option>Transaction Only</option>
                        <option>Full Account</option>
                      </select>
                    </div>
                    <div className="col-lg-12 mb-3">
                      <label className="form-label">
                        Transfer From / Admin Note
                      </label>
                      <input
                        className="form-control"
                        id="inline-form-input"
                        type="text"
                        name="admin_note"
                        value={formValues.admin_note}
                        onChange={handleChange}
                      />
                    </div>
                    <div className="col-lg-12 mb-3">
                      <div className="row">
                        <div className="col-md-6">
                          <label className="form-label">
                            Sponsoring Associate
                          </label>
                          <input
                            className="form-control"
                            id="inline-form-input"
                            type="text"
                            name="sponsor_associate"
                            value={formValues.sponsor_associate}
                            onChange={handleChange}
                          />
                        </div>

                        <div className="col-md-6">
                          <label className="form-label">Staff Recruiter</label>
                          <input
                            className="form-control staff_recruiterinput"
                            id="inline-form-input"
                            type="text"
                            name="staff_recruiter"
                            value={formValues.staff_recruiter}
                            onChange={handleChange}
                          />

                          {isLoading ? (
                            // <p>Loading...</p>
                            ""
                          ) : (
                            <div className="activateaccountstaff_recruiter ">
                              {results && results.length > 0
                                ? results.map((post) => {
                                    return (
                                      <div className="activateaccountstaff">
                                        <ul
                                          className="list-group"
                                          onClick={(e) =>
                                            handleSearchTransaction(post)
                                          }
                                        >
                                          <li className="list-group-item d-flex justify-content-start align-items-center">
                                            <div>
                                              <img
                                                className="rounded-circle profile_picture"
                                                src={post.image || Pic}
                                                alt="Profile"
                                                onError={e => profilepic(e)}
                                              />
                                              <br />
                                            </div>
                                            <div>
                                              <strong>
                                                {post.firstName}&nbsp;
                                                {post.lastName}{" "}
                                              </strong>
                                              <br />
                                              <strong>
                                                {post.contactType
                                                  ? post.contactType
                                                      .charAt(0)
                                                      .toUpperCase() +
                                                    post.contactType.slice(1)
                                                  : ""}
                                              </strong>
                                              <br />
                                            </div>
                                          </li>
                                        </ul>
                                      </div>
                                    );
                                  })
                                : ""}
                            </div>
                          )}
                        </div>
                      </div>
                    </div>
                    <div className="col-lg-12 mb-3">
                      <label className="col-form-label">
                        Account Additional
                      </label>
                      <div className="col-lg-12 mb-3">
                        <label className="form-label" for="pr-city">
                          NRDS ID ?
                          <small>
                            <a
                              target="_blank"
                              href="https://login.connect.realtor/#!/forgotmember"
                            >
                              Look up your NRDS number.
                            </a>
                          </small>{" "}
                        </label>
                        <input
                          className="form-control"
                          id="inline-form-input"
                          type="text"
                          name="nrds_id"
                          value={formValues.nrds_id}
                          onChange={handleChange}
                        />
                      </div>
                    </div>
                    <div className="col-lg-12 mb-3">
                      <label className="col-form-label">Finances</label>
                      <div className="col-lg-12 mb-3">
                        <label className="form-label" for="pr-city">
                          Associate Member/Commission Plan
                        </label>
                        <select
                          className="form-select"
                          id="pr-city"
                          name="associate_member"
                          value={formValues.associate_member}
                          onChange={handleChange}
                        >
                          <option>Broker Plan</option>
                          <option>Standard Plan</option>
                        </select>
                      </div>
                    </div>
                    <div className="col-lg-12 mb-3">
                      <label className="form-label" for="pr-birth-date">
                        Plan Effective Date
                      </label>
                      <input
                        className="form-control"
                        type="date"
                        id="inline-form-input"
                        placeholder="Choose date"
                        name="plan_date"
                        value={formValues.plan_date}
                        onChange={handleChange}
                        data-datepicker-options='{"altInput": true, "altFormat": "F j, Y", "dateFormat": "Y-m-d"}'
                      />
                    </div>
                    <div className="col-lg-12 mb-3">
                      <label className="form-label" for="pr-birth-date">
                        Billing Date
                      </label>
                      <input
                        className="form-control"
                        type="date"
                        id="inline-form-input"
                        placeholder="Choose date"
                        name="billing_date"
                        value={formValues.billing_date}
                        onChange={handleChange}
                        data-datepicker-options='{"altInput": true, "altFormat": "F j, Y", "dateFormat": "Y-m-d"}'
                      />
                    </div>
                    <div className="col-lg-12 mb-3">
                      <label className="form-label" for="pr-city">
                        Bank Account Name
                      </label>
                      <select
                        className="form-select"
                        id="pr-city"
                        name="account_name"
                        value={formValues.account_name}
                        onChange={handleChange}
                      >
                        <option>Integra Reality</option>
                      </select>
                    </div>
                    <div className="col-lg-12 mb-3">
                      <label className="form-label" for="pr-city">
                        Invoicing
                      </label>
                      <select
                        className="form-select"
                        id="pr-city"
                        name="invoicing"
                        value={formValues.invoicing}
                        onChange={handleChange}
                      >
                        <option>Credit Card Auto-Bill</option>
                        <option>Standard Plan</option>
                      </select>
                    </div>
                    <div className="col-lg-12 mb-3">
                      <label className="col-form-label">Timeline</label>
                      <div className="col-lg-12 mb-3">
                        <label className="form-label" for="pr-birth-date">
                          Licensed Since
                        </label>
                        <input
                          className="form-control"
                          type="date"
                          id="inline-form-input"
                          placeholder="Choose date"
                          name="licensed_since"
                          value={formValues.licensed_since}
                          onChange={handleChange}
                          data-datepicker-options='{"altInput": true, "altFormat": "F j, Y", "dateFormat": "Y-m-d"}'
                        />
                      </div>
                    </div>
                    <div className="col-lg-12 mb-3">
                      <label className="form-label" for="pr-birth-date">
                        Associate Since
                      </label>
                      <input
                        className="form-control"
                        type="date"
                        id="inline-form-input"
                        placeholder="Choose date"
                        name="associate_since"
                        value={formValues.associate_since}
                        onChange={handleChange}
                        data-datepicker-options='{"altInput": true, "altFormat": "F j, Y", "dateFormat": "Y-m-d"}'
                      />
                    </div>
                    <div className="col-lg-12 mb-3">
                      <label className="form-label" for="pr-birth-date">
                        Staff Since
                      </label>
                      <input
                        className="form-control"
                        type="date"
                        id="inline-form-input"
                        placeholder="Choose date"
                        name="staff_since"
                        value={formValues.staff_since}
                        onChange={handleChange}
                        data-datepicker-options='{"altInput": true, "altFormat": "F j, Y", "dateFormat": "Y-m-d"}'
                      />
                    </div>
                    <label className="col-form-label">Tax</label>
                    <div className="col-lg-12 mb-3">
                      <div className="d-flex">
                        <div
                          className="form-check"
                          onClick={(e) => CheckBox(e)}
                        >
                          <input
                            className="form-check-input"
                            id="form-radio-4"
                            type="radio"
                            name="checkAll"
                            value="No"
                          />
                          <label
                            className="form-check-label ms-2"
                            id="radio-level1"
                          >
                            No Corporation
                          </label>
                        </div>
                        <div className="form-check">
                          <input
                            className="form-check-input ms-5"
                            id="form-radio-4"
                            type="radio"
                            name="checkAll"
                            value="Yes"
                          />
                          <label
                            className="form-check-label ms-2"
                            id="radio-level1"
                          >
                            Yes, use:
                          </label>
                        </div>
                      </div>
                    </div>
                    {checkAll === "Yes" ? (
                      <div className="col-lg-12 mb-3 mt-3">
                        <div className="row">
                          <div className="col-md-6">
                            <label className="form-label">Agent Tax EIN</label>
                            <input
                              className="form-control"
                              id="inline-form-input"
                              type="text"
                              name="agentTax_ein"
                              value={formValues.agentTax_ein}
                              onChange={handleChange}
                            />
                          </div>
                          <div className="col-md-6">
                            <label className="form-label">
                              Agent Corp Name for Funding
                            </label>
                            <input
                              className="form-control"
                              id="inline-form-input"
                              type="text"
                              name="agent_funding"
                              value={formValues.agent_funding}
                              onChange={handleChange}
                            />
                          </div>
                        </div>
                      </div>
                    ) : (
                      ""
                    )}
                    <div className="col-lg-12 mb-3">
                      <div className="row">
                        <label className="col-form-label">Private Info</label>
                        <div className="col-md-6 mb-lg-0 mb-md-0 mb-sm-3 mb-3">
                          <label className="form-label">SSN/ITIN</label>
                          <input
                            className="form-control"
                            id="inline-form-input"
                            type="text"
                            name="ssn_itin"
                            value={formValues.ssn_itin}
                            onChange={handleChange}
                          />
                        </div>
                        <div className="col-md-6">
                          <label className="form-label">Date of Birth</label>
                          <input
                            className="form-control"
                            type="date"
                            id="inline-form-input"
                            placeholder="Choose date"
                            name="date_birth"
                            value={formValues.date_birth}
                            onChange={handleChange}
                            data-datepicker-options='{"altInput": true, "altFormat": "F j, Y", "dateFormat": "Y-m-d"}'
                          />
                        </div>
                      </div>
                    </div>
                    <div className="col-lg-12 mb-3">
                      <label className="form-label" for="pr-city">
                        Driver License or ID Card
                      </label>
                      <select
                        className="form-select"
                        id="pr-city"
                        name="private_idcard_type"
                        value={formValues.private_idcard_type}
                        onChange={handleChange}
                      >
                        <option>Driver License</option>
                        <option>Military ID</option>
                        <option>Passport</option>
                      </select>
                    </div>
                    <div className="col-lg-12 mb-3">
                      <select
                        className="form-select"
                        id="pr-city"
                        name="private_idstate"
                        onChange={handleChange}
                        value={formValues.private_idstate}
                      >
                        <option value="0">--Select--</option>
                        {options
                          .find((option) => option.code === "UT")
                          .districts.map((district, key) => (
                            <option value={district} key={key}>
                              {district}
                            </option>
                          ))}
                      </select>
                    </div>
                    <div className="col-lg-12 mb-3">
                      <input
                        className="form-control"
                        id="inline-form-input"
                        type="text"
                        name="private_idnumber"
                        value={formValues.private_idnumber}
                        onChange={handleChange}
                      />
                    </div>
                    <div className="col-lg-12 mb-3">
                      <div className="row">
                        <label className="col-form-label">License</label>
                        <div className="col-md-6">
                          <label className="form-label">State</label>
                          <select
                            className="form-select"
                            id="pr-city"
                            name="license_state"
                            onChange={handleChange}
                            value={formValues.license_state}
                          >
                            <option value="0">--Select--</option>
                            {options
                              .find((option) => option.code === "UT")
                              .districts.map((district, key) => (
                                <option value={district} key={key}>
                                  {district}
                                </option>
                              ))}
                          </select>
                        </div>

                        <div className="col-md-6">
                          <label className="form-label">License Type</label>
                          <select
                            className="form-select"
                            id="pr-city"
                            name="license_type"
                            onChange={handleChange}
                            value={formValues.license_type}
                          >
                            <option></option>
                            <option>Sales Agent</option>
                            <option>Associate Broker</option>
                            <option>Principal Broker</option>
                            <option>Company</option>
                          </select>
                        </div>
                      </div>
                    </div>
                    <div className="col-lg-12 mb-3">
                      <label className="form-label">License Number</label>
                      <input
                        className="form-control"
                        id="inline-form-input"
                        type="text"
                        name="license_number"
                        onChange={handleChange}
                        value={formValues.license_number}
                      />
                    </div>
                    <div className="col-lg-12 mb-3">
                      <div className="row">
                        <div className="col-md-6">
                          <label className="form-label">Date Issued</label>
                          <input
                            className="form-control"
                            type="date"
                            id="inline-form-input"
                            placeholder="Choose date"
                            name="date_issued"
                            onChange={handleChange}
                            value={formValues.date_issued}
                            data-datepicker-options='{"altInput": true, "altFormat": "F j, Y", "dateFormat": "Y-m-d"}'
                          />
                        </div>
                        <div className="col-md-6">
                          <label className="form-label">Date Expires</label>
                          <input
                            className="form-control"
                            type="date"
                            id="inline-form-input"
                            placeholder="Choose date"
                            name="date_expire"
                            onChange={handleChange}
                            value={formValues.date_expire}
                            data-datepicker-options='{"altInput": true, "altFormat": "F j, Y", "dateFormat": "Y-m-d"}'
                          />
                        </div>
                      </div>
                    </div>
                    <div className="col-lg-12 mb-3">
                      <div className="row">
                        <label className="col-form-label">MLS</label>
                        <div className="col-md-6">
                          <label className="form-label">MLS Membership</label>
                          {/* <select className="form-select" id="pr-city"
                                                    name="mls_membership"
                                                    onChange={handleChange}
                                                    value={formValues.mls_membership}>
                                                    <option>UtahRealEstate (Utah)</option>
                                                    <option>Local MLS (No IDX)</option>
                                                    <option>UtahRealEstate.com</option>
                                                    <option>Park City</option>
                                                    <option>Washington County</option>
                                                    <option>Iron County</option>
                                                </select> */}
                          <MultiSelect
                            options={[
                              {
                                label: `UtahRealEstate.com`,
                                value: `UtahRealEstate.com`,
                              },
                              { label: `Park City`,
                               value: `Park City` },
                              {
                                label: `Washington County`,
                                value: `Washington County`,
                              },
                              { label: `Iron County`, value: `Iron County` },
                              // {
                              //   label: `Local MLS (No IDX)`,
                              //   value: `Local MLS (No IDX)`,
                              // },
                              // { label: `Tooele County`, value: `Tooele County` },
                              // { label: `Cache Valley`, value: `Cache Valley` },
                              // { label: `Brigham-Tremonton Board`, value: `Brigham-Tremonton Board` }
                            ]}
                            value={mls_membership}
                            onChange={setMls_membership}
                            labelledBy="Select MLS"
                            name="mls_membership"
                            // hasSelectAll={false}
                            // selectionLimit={1}
                          />
                        </div>
                        
                        {mls_membership.some(
                          (member) => member.value === "UtahRealEstate.com"
                        ) && (
                          <div className="col-md-6 mb-3">
                            <label className="form-label">Utah Agent ID</label>
                            <input
                              className="form-control"
                              id="inline-form-input"
                              type="text"
                              name="agent_id"
                              value={formValues.agent_id}
                              onChange={handleChange}
                            />
                          </div>
                        )}

                        {mls_membership.some(
                          (member) => member.value === "Park City"
                        ) && (
                          <div className="col-md-6">
                            <label className="form-label">
                              Park City Agent ID
                            </label>
                            <input
                              className="form-control"
                              id="inline-form-input"
                              type="text"
                              name="parkcityagent_id"
                              value={formValues.parkcityagent_id}
                              onChange={handleChange}
                            />
                          </div>
                        )}

                        {mls_membership.some(
                          (member) => member.value === "Washington County"
                        ) && (
                          <div className="col-md-6">
                            <label className="form-label">
                              Washington County Agent ID
                            </label>
                            <input
                              className="form-control"
                              id="inline-form-input"
                              type="text"
                              name="washingtonagent_id"
                              value={formValues.washingtonagent_id}
                              onChange={handleChange}
                            />
                          </div>
                        )}

                        {mls_membership.some(
                          (member) => member.value === "Iron County"
                        ) && (
                          <div className="col-md-6">
                            <label className="form-label">
                              Iron County Agent ID
                            </label>
                            <input
                              className="form-control"
                              id="inline-form-input"
                              type="text"
                              name="ironcountyagent_id"
                              value={formValues.ironcountyagent_id}
                              onChange={handleChange}
                            />
                          </div>
                        )}
                      </div>
                    </div>
                    <div className="col-lg-12 mb-3">
                      <div className="row">
                        <div className="col-md-6">
                          <label className="col-form-label">Board</label>
                          <div className="col-lg-12 mb-3">
                            <label className="form-label">
                              Board and the Board Membership
                            </label>
                            <MultiSelect2
                              options={[
                                {
                                  label: `Salt Lake Board`,
                                  value: `Salt Lake Board`,
                                },
                                {
                                  label: `Utah Central Association UCAR`,
                                  value: `Utah Central Association UCAR`,
                                },
                                {
                                  label: `Northern Wasatch`,
                                  value: `Northern Wasatch`,
                                },
                                {
                                  label: `Washington County`,
                                  value: `Washington County`,
                                },
                                { label: `Park City`, value: `Park City` },
                                { label: `Iron County`, value: `Iron County` },
                                {
                                  label: `Tooele County`,
                                  value: `Tooele County`,
                                },
                                {
                                  label: `Cache Valley`,
                                  value: `Cache Valley`,
                                },
                                {
                                  label: `Brigham-Tremonton Board`,
                                  value: `Brigham-Tremonton Board`,
                                },
                              ]}
                              value={board_membership}
                              onChange={setBoard_membership}
                              labelledBy="Select Board Membership"
                              name="board_membership"
                            />
                          </div>
                        </div>
                        {/* <div className="col-md-6">
                                                <label className="form-label">Office Affiliation</label>
                                                 <select className="form-select" id="office_affiliation"
                                                    name="office_affiliation"
                                                    onChange={handleChange}
                                                    value={formValues.office_affiliation}>
                                                    <option>South Jordan/ Corporate</option>
                                                    <option>Saint George</option>
                                                    <option>Park City</option>
                                                    <option>Elite</option>
                                                    <option>Orem</option>
                                                    <option>Ogden</option>
                                                    <option>Referral Only</option>
                                                </select>
                                            </div> */}

                        <div className="col-md-6">
                          <label className="col-form-label">
                            Office Affiliation
                          </label>
                          <div className="col-lg-12 mb-3">
                            <label className="form-label">
                              Office Affiliation
                            </label>
                            <select
                              className="form-select"
                              id="office_affiliation"
                              name="office_affiliation"
                              onChange={handleChange}
                              value={formValues.office_affiliation}
                            >
                              {organizationGet.data &&
                                organizationGet.data.map((item) => (
                                  <option key={item._id} value={item.name}>
                                    {item.name}
                                  </option>
                                ))}
                            </select>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="pull-right mt-3">
                    <button
                      type="button"
                      className="m-2 btn btn-primary btn-sm"
                      id="save-button"
                      onClick={handleSubmit}
                    >
                      Update Account
                    </button>
                    <button
                      className="btn btn-secondary btn-sm ml-10"
                      onClick={handleBack}
                    >
                      Cancel & Go Back
                    </button>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </main>
    </div>
  );
}
export default ActivateAccount;

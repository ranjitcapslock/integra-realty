import React, { useState, useEffect } from "react";
import user_service from "../service/user_service";
import Pic from "../img/pic.png";
import { NavLink, useNavigate, useParams } from "react-router-dom";
import axios from "axios";
import Loader from "../../Pages/Loader/Loader.js";
import Toaster from "../../Pages/Toaster/Toaster.js";
import jwt from "jwt-decode";
import { Rating } from "react-simple-star-rating";
import StarIcon from "../img/like-preview.png"
import _ from "lodash";

const Testimonials = () => {
  const [loader, setLoader] = useState({ isActive: null });
  const [toaster, setToaster] = useState({
    types: null,
    isShow: null,
    toasterBody: "",
    message: "",
  });
  // const [dataNew, setDataNew] = useState("");
  // const [fileNew, setFileNew] = useState(null);
  // const [fileExtensionNew, setFileExtensionNew] = useState("");
  // const [acceptedFileTypes, setAcceptedFileTypes] = useState([]);
  // const [showSubmitButtonNew, setShowSubmitButtonNew] = useState(false);
  const [getContact, setGetContact] = useState("");
  const [contactTestimonials, setContactTestimonials] = useState("");
  const [formErrors, setFormErrors] = useState({});
  const [isSubmitClick, setISSubmitClick] = useState(false);
  const [rating, setRating] = useState(0);
  // const [ratingagent, setRatingagent] = useState("yes");
  const [agentExpection, setAgentExpection] = useState("above_expectations");
  const [testimonialsId, setTestimonialsId] = useState("");


  const { isActive } = loader;
  const { types, isShow, toasterBody, message } = toaster;
  const params = useParams();
  const navigate = useNavigate();

  const handleChangeTestimonials = (e) => {
    const { name, value } = e.target;
    setContactTestimonials({ ...contactTestimonials, [name]: value });
  };

  const TransactionGetById = async () => {
     if(params.id){
       await user_service.transactionGetById(params.id).then((response) => {
         if (response) {
           setLoader({ isActive: false });
           console.log(response);
           setGetContact(response.data);
         }
       });
     }
  };

  useEffect(() => {
    TransactionGetById(params.id);
  }, []);

  

  const handleRating = (newRating) => {
    setRating(newRating);
  };

  useEffect(() => {
    if (contactTestimonials && isSubmitClick) {
      validate();
    }
  }, [contactTestimonials]);

  const validate = () => {
    const values = contactTestimonials;
    const errors = {};
    if (!values.headlineReview) {
      errors.headlineReview = "Review is required";
    }

    if (!values.displayName) {
      errors.displayName = "Name is required";
    }
    setFormErrors(errors);
    return errors;
  };

  const handleSubmitTestimonials = async (e) => {
      e.preventDefault();
      setISSubmitClick(true);
      let checkValue = validate();
      if (_.isEmpty(checkValue)) {
      const userData = {
        star: rating,
        // rateAgent: ratingagent,
        description: contactTestimonials.description,
        headlineReview: contactTestimonials.headlineReview,
        displayName: contactTestimonials.displayName,
        image: getContact?.propertyDetail?.image,
        // expectations: agentExpection,
        clientId: getContact.contact3?.[0].data?._id,
        transactionId :params.id
      };
      console.log(userData);
      setLoader({ isActive: true });
      const response = await user_service.TestimonialsCreate(userData);
      if (response) {
        setTestimonialsId(response.data._id);
        const responseData = await user_service.TestmoinalsEmail(response.data._id);
        console.log(responseData);
        setLoader({ isActive: false });
        setToaster({
          type: "Testimonials Added",
          isShow: true,
          toasterBody: response.data.message,
          message: "Testimonials Added Successfully",
        });
        setTimeout(() => {
          setToaster((prevToaster) => ({ ...prevToaster, isShow: false }));
        }, 2000);
        setStep(2);
      } else {
        setLoader({ isActive: false });
        setToaster({
          types: "error",
          isShow: true,
          toasterBody: response.data.message,
          message: "Error",
        });
      }
    }
  };
  

  const onPointerEnter = () => {
    // Handle pointer enter event here
  };

  const onPointerLeave = () => {
    // Handle pointer leave event here
  };

  const onPointerMove = () => {
    // Handle pointer move event here
  };
  const [step, setStep] = useState(1);

  return (
    <div className="bg-secondary float-left w-100 pt-4 mb-4">
      <Loader isActive={isActive} />
      {isShow && (
        <Toaster
          types={types}
          isShow={isShow}
          toasterBody={toasterBody}
          message={message}
        />
      )}
      <main className="page-wrapper profile_page_wrap mb-3">
        <div className="container">
          <div className="bg-light steper rounded-3 py-4 px-md-4 mb-3">
            <div className="steps pt-4 pb-1">
              <div className={`step ${step === 1 ? "active" : ""}`}>
                <div className="step-progress">
                  <span className="step-progress-start"></span>
                  <span className="step-progress-end"></span>
                  <span className="step-number">1</span>
                </div>
                {/* <div className="step-label">1</div> */}
              </div>
              <div className={`step ${step === 2 ? "active" : ""}`}>
                <div className="step-progress">
                  <span className="step-progress-start"></span>
                  <span className="step-progress-end"></span>
                  <span className="step-number">2</span>
                </div>
                {/* <div className="step-label">2</div> */}
              </div>
            </div>
          </div>
          <div className="m-auto float-none w-75 bg-light rounded-3 p-4">
            
            {
              getContact ?
              <>
          {step === 1 && (
            <div className="content-overlay">
                <div className="float-start w-100 shadow card-hover rounded-3 p-4">
                  <div>
                    <p>Thank you for choosing In Depth Realty for your recent home {getContact?.represent==="buyer" ? "Purchase" : "Sale"}. We hope your experience with us and your real estate agent met your expectations and helped make this important milestone as smooth as possible.</p>
                     <p>To ensure we continue providing exceptional service, we’d greatly appreciate your feedback on your experience working with <b>{getContact.contact3?.[0].data?.firstName}</b>{" "}
                    <b>{getContact.contact3?.[0].data?.lastName}</b>. Your insights help us recognize outstanding performance and identify areas where we can improve.</p>
                     <p>Please take a moment to share your thoughts by completing this quick survey and leaving a review here. Your input is invaluable in helping us maintain the highest standards of service.</p>
                    <p>Thank you again for trusting In Depth Realty and <b>{getContact.contact3?.[0].data?.firstName}</b>{" "}
                    <b>{getContact.contact3?.[0].data?.lastName}</b></p>
                  </div>
                  {
                    getContact.contact3?.[0].data?.image ?
                      <div className="contactProfile businesscard position-static text-center mb-3">
                        <img class="rounded-circle" src={getContact.contact3?.[0].data?.image} alt="Contact Image 2"></img>
                      </div>
                      : ""
                  }
                  <h5 className="w-100 mb-0">
                    Write a review for{" "}
                    {getContact.contact3?.[0].data?.firstName}{" "}
                    {getContact.contact3?.[0].data?.lastName}
                  </h5>
                  <p className="pt-0 mb-0">
                    Share your experience as the {getContact?.represent==="buyer" ? "Purchase" : "Sale"} of{" "}
                    {getContact?.propertyDetail?.streetAddress}
                  </p>
                  <div className="pt-md-2 pt-lg-0 mt-2">
                    <label className="col-form-label pt-0 mb-0 pb-0">Select the number of stars to rate &nbsp;
                      {getContact.contact3?.[0].data?.firstName} &nbsp;
                      {getContact.contact3?.[0].data?.lastName}
                    </label>
                    <div className="float-start w-100 mb-0">
                      <Rating
                        onClick={handleRating}
                        onPointerEnter={onPointerEnter}
                        onPointerLeave={onPointerLeave}
                        onPointerMove={onPointerMove}
                      />
                    </div>
                  </div>
                  <label className="col-form-label pt-0 mb-0 pb-1">Describe of your experience with&nbsp;
                    {getContact.contact3?.[0].data?.firstName} &nbsp;
                    {getContact.contact3?.[0].data?.lastName}</label>
                  <p className="mb-0">How is their market knowledge, communication and professionalism</p>
               
              </div>
              <div className="float-start w-100 bg-light rounded-3 mt-3">
                <div className="float-start w-100">
                  {/* <label className="col-form-label pt-0 mb-0">
                  Surprise your experiences
                  </label> */}
                  <input
                    className="form-control"
                    id="inline-form-input"
                    type="text"
                    name="headlineReview"
                    placeholder="Review Headline*"
                    value={contactTestimonials.headlineReview}
                    onChange={handleChangeTestimonials}
                    style={{
                      border: formErrors?.headlineReview
                        ? "1px solid red"
                        : "1px solid #00000026",
                    }}
                  />
                  <div className="invalid-tooltip">
                    {formErrors.headlineReview}
                  </div>
                </div>
                
                <div className="float-start w-100 mt-3">
                  <label className="col-form-label pt-0 mb-0">
                    Share your experience in detail
                  </label>
                  <textarea
                    rows="4"
                    className="form-control"
                    id="inline-form-input"
                    type="text"
                    placeholder="Your Review*"
                    name="description"
                    value={contactTestimonials.description}
                    onChange={handleChangeTestimonials}
                  />
                </div>

                <div className="float-start w-100 mt-3">
                  <label className="col-form-label pt-0 mb-0">
                   Display Name 
                  </label>
                  <input
                    className="form-control"
                    id="inline-form-input"
                    type="text"
                    name="displayName"
                    placeholder="name"
                    value={contactTestimonials.displayName}
                    onChange={handleChangeTestimonials}
                    style={{
                      border: formErrors?.displayName
                        ? "1px solid red"
                        : "1px solid #00000026",
                    }}
                  />
                  <div className="invalid-tooltip">
                    {formErrors.displayName}
                  </div>
                </div>

                  {/* <div className="float-start w-100 mt-4 p-4 rate_my_agent">
                    <label className="col-form-label pt-0 mb-0">
                      Did you use RateMyAgent to help you select    {getContact.contact3?.[0].data?.firstName} &nbsp;
                      {getContact.contact3?.[0].data?.lastName}? <br />
                    </label><br />
               
                <ul
            className="nav nav-tabs d-flex pb-2 mb-0"
            role="tablist"
          >
            <li className="nav-item me-sm-3" role="presentation">
              <a
                className="nav-link text-center active"
                href="#tab"
                data-bs-toggle="tab"
                role="tab"
                aria-controls="reviews-about-you"
                aria-selected="true"
                onClick={() => RateAgent("yes")}
              >
            <i className="fa fa-thumbs-o-up me-2" aria-hidden="true"></i>
               Yes
              </a>
            </li>
            <li className="nav-item" role="presentation">
              <a
                className="nav-link text-center"
                href="#tab2"
                data-bs-toggle="tab"
                role="tab"
                aria-controls="reviews-by-you"
                aria-selected="false"
                tabindex="-1"
                onClick={() => RateAgent("no")}
              >
                          <i class="fa fa-thumbs-o-down me-2" aria-hidden="true"></i>
               No
              </a>
            </li>
          </ul>

          {
            ratingagent ==="yes" ?
            <>

                     <div
                      className="col-sm-12 mb-3"
                    >
                        
                      <label className="col-form-label pt-0 mb-0">
                 Did the final sale price of the property match your expectations?<br/>
                 (optional)
                  </label><br/>
                      <div className="form-check form-check-inline">
                        <input
                          className="form-check-input"
                          id="form-radio-5"
                          type="radio"
                          name="expectation"
                          value="above_expectations"
                          onChange={CheckExpected}
                        />
                        <label className="form-check-label">
                        Above my expectations
                        </label>
                      </div>
                      <br />
                      <div className="form-check form-check-inline">
                        <input
                          className="form-check-input"
                          id="form-radio-5"
                          type="radio"
                          name="expectation"
                          value="about_expected"
                          onChange={CheckExpected}
                        />
                        <label className="form-check-label">
                          About but i expected
                        </label>
                      </div>

                      <br />
                      <div className="form-check form-check-inline">
                        <input
                          className="form-check-input"
                          id="form-radio-5"
                          type="radio"
                          name="expectation"
                          value="below_expectations"
                          onChange={CheckExpected}
                        />
                        <label className="form-check-label">
                         Below my expectations
                        </label>
                      </div>
                    </div>

            </>
            :""
          }
                   </div>
                

                 */}

             

                <div className="float-start w-100 mt-4">
                  <button
                    className="btn btn-info"
                    type="button"
                    onClick={handleSubmitTestimonials}
                  >
                    Submit
                  </button>
                </div>
              </div>
            </div>
          )}
              </>
              :""
            }


          {
              getContact ?
              <>
          {step === 2 && (
            <section className="">
              <div className="container">
                <div className="">
                  <div className="text-center">
                      <img src={StarIcon} />
                    <h4 className="mb-2 pb-md-3 mt-4">Thanks for leaving  {getContact.contact3?.[0].data?.firstName}  {getContact.contact3?.[0].data?.lastName} a <br/> 
                    review!</h4>
                   {console.log(testimonialsId)}
                    <p className="lead mb-md-5 mb-4">
                    Your review is greatly appreciated
                    </p>
                    <NavLink to={`/testimonials-listing/${testimonialsId}`}
                    className="btn btn-info"
                    type="button"
                  >
                   View your review
                  </NavLink>
                  </div>
                </div>
              </div>
            </section>
          )}
              </>
              :""
         }
        </div>
         </div>
      </main>
    </div>
  );
};

export default Testimonials;

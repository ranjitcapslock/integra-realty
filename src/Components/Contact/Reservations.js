import React, { useState, useEffect } from "react";
import _ from "lodash";
import user_service from "../service/user_service.js";
import jwt from "jwt-decode";
import Loader from "../../Pages/Loader/Loader.js";
import Toaster from "../../Pages/Toaster/Toaster.js";
import { useParams, useNavigate } from "react-router-dom";
import axios from "axios";

function Reservations() {
  const initialValues = {
    calender_name: "",
    calender_blurb: "",
    publication_Level: "corporate",
    effective_Date: "",
    image: "",
  };
  const [formValues, setFormValues] = useState(initialValues);
  const [formErrors, setFormErrors] = useState({});
  const [isSubmitClick, setISSubmitClick] = useState(false);
  const [loader, setLoader] = useState({ isActive: null });
  const { isActive } = loader;
  const [toaster, setToaster] = useState({
    types: null,
    isShow: null,
    toasterBody: "",
    message: "",
  });
  const { types, isShow, toasterBody, message } = toaster;
  const [file, setFile] = useState(null);
  const [data, setData] = useState("");
  const [checkreservation, setCheckreservation] = useState("open_Registration");
  const params = useParams();
  const navigate = useNavigate();
  // onChange Function start
  const handleChange = (e) => {
    const { name, value } = e.target;
    setFormValues({ ...formValues, [name]: value });
  };

  const handleCheckreservation = (event) => {
    setCheckreservation(event.target.name);
    setCheckreservation(event.target.value);
  };
  // onChange Function end

  {
    /* <!-- Form Validation Start--> */
  }
  useEffect(() => {
    if (formValues && isSubmitClick) {
      validate();
    }
  }, [formValues]);

  const validate = () => {
    const values = formValues;
    const errors = {};
    if (!values.calender_name) {
      errors.calender_name = "Calender Name is required";
    }

    setFormErrors(errors);
    return errors;
  };
  {
    /* <!-- Form Validation End--> */
  }

  const handleFileUpload = async (e) => {
    const selectedFile = e.target.files[0];

    if (selectedFile) {
      setFile(selectedFile);

      const formData = new FormData();
      formData.append("file", selectedFile);
      console.log(selectedFile);
      const config = {
        headers: {
          Accept: "application/json",
          "Content-Type": "multipart/form-data",
          Authorization: `Bearer ${localStorage.getItem("auth")}`,
        },
      };
      try {
        setLoader({ isActive: true });
        const uploadResponse = await axios.post(
          "https://api.brokeragentbase.com/upload",
          formData,
          config
        );
        const uploadedFileData = uploadResponse.data;
        setLoader({ isActive: false });
        setToaster({
          type: "Upload File",
          isShow: true,
          message: "Image Uploaded Successfully",
        });
        setTimeout(() => {
          setToaster((prevToaster) => ({ ...prevToaster, isShow: false }));
        }, 2000);
        setData(uploadedFileData);
      } catch (error) {
        console.error("Error occurred during file upload:", error);
      }
    }
  };

  const handleSubmit = async (e) => {
    if (params.id) {
      e.preventDefault();

      const userData = {
        calender_name: formValues.calender_name,
        calender_blurb: formValues.calender_blurb,
        publication_Level: "corporate",
        image: data,
        registration: checkreservation,
      };

      try {
        setLoader({ isActive: true });
        const response = await user_service.reservationUpdate(
          params.id,
          userData
        );
        if (response) {
          setLoader({ isActive: false });
          setToaster({
            type: "Update Reservation",
            isShow: true,
            // toasterBody: response.data.message,
            message: "Update Reservation Successfully",
          });
          setTimeout(() => {
            setToaster((prevToaster) => ({
              ...prevToaster,
              isShow: false,
            }));
            navigate(`/reservation-listing`);
          }, 1000);
        } else {
          setLoader({ isActive: false });
          setToaster({
            types: "error",
            isShow: true,
            toasterBody: response.data.message,
            message: "Error",
          });
        }
      } catch (error) {
        setLoader({ isActive: false });
        setToaster({
          types: "error",
          isShow: true,
          toasterBody: error.response
            ? error.response.data.message
            : error.message,
          message: "Error",
        });
        setTimeout(() => {
          setToaster((prevToaster) => ({ ...prevToaster, isShow: false }));
        }, 2000);
      }
    } 
    else {
      e.preventDefault();
      setISSubmitClick(true);
      let checkValue = validate();
      if (_.isEmpty(checkValue)) {
        const userData = {
          agentId: jwt(localStorage.getItem("auth")).id,
          registration: checkreservation,
          image: data,
          calender_name: formValues.calender_name,
          calender_blurb: formValues.calender_blurb,
          publication_Level: "corporate",
          effective_Date: formValues.effective_Date,
        };
        try {
          setLoader({ isActive: true });
          const response = await user_service.reservationPost(userData);
          if (response) {
            const userDatan = {
              agentId: jwt(localStorage.getItem("auth")).id,
              startTime: "8:00 AM",
              endTime: "6:00 PM",
              shift: "1",
              reservationId: response.data._id,
              days: ["monday", "tuesday", "wednesday", "thursday", "friday"],
              status: "complete",
            };
            console.log(userDatan);
            const responsen = await user_service.configureSchedulePost(
              userDatan
            );
            console.log(responsen);
            setLoader({ isActive: false });
            setToaster({
              type: "Add Reservation",
              isShow: true,
              // toasterBody: response.data.message,
              message: "Add Reservation Successfully",
            });
            setTimeout(() => {
              setToaster((prevToaster) => ({
                ...prevToaster,
                isShow: false,
              }));
              navigate(`/reservation-listing`);
            }, 1000);
          } else {
            setLoader({ isActive: false });
            setToaster({
              types: "error",
              isShow: true,
              toasterBody: response.data.message,
              message: "Error",
            });
          }
        } catch (error) {
          setLoader({ isActive: false });
          setToaster({
            types: "error",
            isShow: true,
            toasterBody: error.response
              ? error.response.data.message
              : error.message,
            message: "Error",
          });
          setTimeout(() => {
            setToaster((prevToaster) => ({ ...prevToaster, isShow: false }));
          }, 2000);
        }
      }
    }
  };

  const handleCancel = () => {
    navigate(`/reservation-listing`);
  };

  const reservationGetIdData = async () => {
    await user_service.reservationGetId(params.id).then((response) => {
      if (response) {
        setLoader({ isActive: false });
        console.log(response);
        setFormValues(response.data);
      }
    });
  };

  useEffect(() => {
    if (params.id) {
      reservationGetIdData();
    }
  }, []);

  const handleDelete = async () => {
    setLoader({ isActive: true });
    await user_service.reservationDelete(params.id).then((response) => {
      if (response) {
        setLoader({ isActive: false });
        setToaster({
          types: "Delete",
          isShow: true,
          // toasterBody: response.data.message,
          message: "Reservation Deleted Successfully",
        });

        setTimeout(() => {
          setToaster((prevToaster) => ({
            ...prevToaster,
            isShow: false,
          }));
          navigate(`/reservation-listing`);
        }, 2000);
      }
    });
  };

  return (
    <>
      <div className="bg-secondary float-left w-100 pt-4 mb-4">
        <Loader isActive={isActive} />
        {isShow && (
          <Toaster
            types={types}
            isShow={isShow}
            toasterBody={toasterBody}
            message={message}
          />
        )}
        <main className="page-wrapper">
          <div className="d-flex align-items-center justify-content-start mb-4">
            <h3 className="text-white mb-0">
              {params.id ? "Update Calendar" : "Add a Calendar"}
            </h3>
          </div>
          <div className="bg-light border rounded-3 p-3">
            <div className="row">
              {formValues.registration === "open_Registration" ? (
                <h6>This is an Open Registration Calendar</h6>
              ) : (
                <h6>How should reservations be made available?</h6>
              )}
              {/* {params.id ? (
                <></>
              ) : ( */}
                  <div
                    className="d-flex mb-3"
                    onClick={(e) => handleCheckreservation(e)}
                  >
                    <div className="col-md-6 mb-3 mt-2">
                    <div className="form-check mb-0 ms-2">
                      <input
                        className="form-check-input"
                        id="form-check-1"
                        type="radio"
                        name="registration"
                        value="open_Registration"
                        />

                      <label className="form-label" id="radio-level1">
                        Open Registration
                      </label>
                      <br />
                      <span>
                        Calendar can be reserved freely at varying lengths.
                      </span>
                    </div>
                    </div>
                    <div className="col-md-6 mb-3 mt-2">
                    <div className="form-check mb-0 ms-2">
                      <input
                        className="form-check-input"
                        id="form-check-1"
                        type="radio"
                        name="registration"
                        value="shift_Based"
                      />

                      <label className="form-label" id="radio-level1">
                        Shift-Based
                      </label>
                      <br />
                      <span>
                        Calendar can be reserved in pre-defined time slots.
                      </span>
                    </div>
                    </div>
                  </div>
              {/* )} */}
              <div className="col-md-6 mb-3 mt-2">
                <label className="form-label">Calendar Name *</label>
                <input
                  className="form-control"
                  id="inline-form-input"
                  type="text"
                  name="calender_name"
                  value={formValues.calender_name}
                  onChange={handleChange}
                  style={{
                    border: formErrors?.calender_name ? "1px solid red" : "",
                  }}
                />
                {formErrors.calender_name && (
                  <div className="invalid-tooltip">
                    {formErrors.calender_name}
                  </div>
                )}
              </div>

              <div className="col-md-6 mb-3 mt-2">
                <label className="form-label">Calendar Blurb</label>
                <input
                  className="form-control"
                  id="inline-form-input"
                  type="text"
                  name="calender_blurb"
                  value={formValues.calender_blurb}
                  onChange={handleChange}
                />
              </div>

              <div className="col-md-6 mb-3">
                <label className="form-label">Publication Level</label>
                <select
                  className="form-select mb-2"
                  name="membership"
                  onChange={handleChange}
                  value={formValues.publication_Level}
                >
                  <option value="corporate">Corporate</option>
                </select>
              </div>
              {params.id ? (
                <></>
              ) : (
                <div className="col-md-6 mb-3">
                  <label className="form-label">Effective Date</label>
                  <input
                    className="form-control"
                    id="inline-form-input"
                    type="date"
                    name="effective_Date"
                    value={formValues.effective_Date}
                    onChange={handleChange}
                  />
                </div>
              )}

              <div className="col-md-3">
                {formValues.image ? (
                  <label className="documentlabel pull-left mt-4">
                    {" "}
                    Update Image
                    <input
                      className="form-control"
                      id="inline-form-input"
                      type="file"
                      name="image"
                      onChange={handleFileUpload}
                      // value={formValues.image}
                    />
                  </label>
                ) : (
                  <>
                    <label className="documentlabel pull-left mt-4">
                      {" "}
                      Upload Image
                      <input
                        className="form-control"
                        id="inline-form-input"
                        type="file"
                        name="image"
                        onChange={handleFileUpload}
                        // value={formValues.image}
                      />
                    </label>
                  </>
                )}

                {data ? (
                  <img className="mt-3" src={data} alt="Utility Logo" />
                ) : (
                  formValues.image && (
                    <img
                      className="mt-3"
                      src={formValues.image}
                      alt="Utility Logo"
                    />
                  )
                )}
              </div>

              <div className="pull-right mt-4">
                <button
                  className="btn btn-primary  ms-3  pull-right"
                  type="button"
                  onClick={handleSubmit}
                >
                  {params.id ? "Update" : " Submit"}
                </button>

                <button
                  className="btn btn-secondary pull-right ms-2"
                  type="button"
                  onClick={handleCancel}
                >
                  Cancel
                </button>

                {params.id ? (
                  <button
                    className="btn btn-primary  ms-3  pull-right"
                    type="button"
                    onClick={handleDelete}
                  >
                    Remove
                  </button>
                ) : (
                  ""
                )}
              </div>
            </div>
          </div>
        </main>
      </div>
    </>
  );
}
export default Reservations;

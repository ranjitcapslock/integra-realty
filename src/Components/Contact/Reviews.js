import React, { useState, useEffect } from 'react'
import Pic from "../img/pic.png";
import { NavLink, useNavigate, useParams } from 'react-router-dom';
import user_service from '../service/user_service';
import axios from "axios";
import Loader from "../../Pages/Loader/Loader.js";
import Toaster from '../../Pages/Toaster/Toaster.js';

const Reviews = () => {
    const [loader, setLoader] = useState({ isActive: null });
    const { isActive } = loader;
    const [toaster, setToaster] = useState({ types: null, isShow: null, toasterBody: "", message: "" });
    const { types, isShow, toasterBody, message } = toaster;

    const [getContact, setGetContact] = useState([]);
    const params = useParams();


    useEffect(() => { window.scrollTo(0, 0);
        const ContactGetAll = async () => {
            await user_service.contactGet(params.id).then((response) => {

                if (response) {
                    setGetContact(response.data.data);
                }
            });
        }
        ContactGetAll()
    }, []);




    return (
        <div className="bg-secondary float-left w-100 pt-4">
            <Loader isActive={isActive} />
            {isShow && <Toaster types={types}
                isShow={isShow}
                toasterBody={toasterBody}
                message={message} />}
            <main className="page-wrapper profile_page_wrap">
                <div className="container content-overlay mt-3">
                    <div className="col-lg-9 pull-right">
                        <div className="collapse d-md-block" id="account-nav">
                            <ul className="nav nav-pills flex-column flex-md-row pt-3 pt-md-0 pb-md-4 border-bottom-md">
                                <li className="nav-item mb-md-0 me-md-2 pe-md-1"><NavLink className="nav-link" to={`/contact-profile/${params.id}`} aria-current="page"><i className="fi-heart mt-n1 me-2 fs-base"></i>Profile</NavLink></li>
                                <li className="nav-item mb-md-0 me-md-2 pe-md-1"><NavLink className="nav-link" to={`/contact-reviews/${params.id}`}><i className="fi-bell mt-n1 me-2 fs-base"></i>Reviews</NavLink></li>

                            </ul>
                        </div>
                    </div>
                    <div className="">

                        <div className="row">

                            <div className="col-md-8">

                            </div>

                            <div className="col-md-4">
                             hello
                            </div>
                            {/* <h4 className="nav-item mb-md-0 me-md-2 pe-md-1 mb-5" id="profile"><a className="nav-link active" href="#" aria-current="page">Profile</a></h4>


                            <div className="col-lg-3">
                                <h2 className="h4" id="info">Contact Info</h2>
                            </div><br /> */}

                        </div>



                        <div className="row">

                        </div>
                    </div>
                </div>

            </main>
            

        </div >
    )
}

export default Reviews;
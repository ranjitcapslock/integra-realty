import React, { useState, useEffect, useRef } from "react";
import Loader from "../../Pages/Loader/Loader.js";
import Toaster from "../../Pages/Toaster/Toaster.js";
import { NavLink, useNavigate, useParams } from "react-router-dom";
import user_service from "../service/user_service";
import jwt from "jwt-decode";

const CommunityContact = () => {
  const [loader, setLoader] = useState({ isActive: null });
  const { isActive } = loader;
  const [toaster, setToaster] = useState({
    type: null,
    isShow: null,
    toasterBody: "",
    message: "",
  });
  const { type, isShow, toasterBody, message } = toaster;

  const initialValues = {
    email: "",
    image: "",
    phone: "",
    firstName: "",
    lastName: "",
    nickname: "",
    address: "",
    web: "",
    fax: "",
    inbox: "",
    work_phone: "",
    home_phone: "",
    video_email: "",
    printed_name: "",
    spouse_firstname: "",
    spouse_lastname: "",
    spouse_number: "",
    spouse_email: "",
    spouse_birthday: "",
    bussiness_name: "",
    bussiness_address: "",
    sellers: "",
    personal_contacttype: "",
    organization: "",
    homeOffice: "",
    title: "",
    agentId: "",
    profession: "",
    industry: "",
    assistant: "",
    designations: "",
    primary_areas: "",
    languages: "",
    specialties: "",
    marketing_message: "",
    birthday: "",
    gender: "",
    anniversary: "",
    private_note: "",
    recruiter: "",
    supporters: "",
    connection: "",
    notes: "",
  };

  const params = useParams();

  console.log(params.id);

  const [getContact, setGetContact] = useState(initialValues);

  const ContactGetById = async () => {
    await user_service.contactGetById(params.id).then((response) => {
      if (response) {
        setGetContact(response.data);
      }
    });
  };

  useEffect(() => {
    window.scrollTo(0, 0);
    ContactGetById(params.id);
  }, []);

  const handleChange = (e) => {
    const { name, value } = e.target;
    setGetContact({ ...getContact, [name]: value });
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
    try {
      const userData = {
        agentId: jwt(localStorage.getItem("auth")).id,
        firstName: getContact.firstName,
        lastName: getContact.lastName,
        nickname: getContact.nickname
          ? getContact.nickname
          : getContact.lastName + "" + getContact.firstName,
        email: getContact.email,
        phone: getContact.phone,
        contactType: getContact.contactType,
        address: getContact.address,
        web: getContact.web,
        fax: getContact.fax,
        work_phone: getContact.work_phone,
        home_phone: getContact.home_phone,
        video_email: getContact.video_email,
        printed_name: getContact.printed_name,
        spouse_firstname: getContact.spouse_firstname,
        spouse_lastname: getContact.spouse_lastname,
        spouse_number: getContact.spouse_number,
        spouse_email: getContact.spouse_email,
        spouse_birthday: getContact.spouse_birthday,
        bussiness_name: getContact.bussiness_name,
        bussiness_address: getContact.bussiness_address,
        sellers: getContact.sellers,
        personal_contacttype: getContact.personal_contacttype,
        inbox: getContact.inbox,
        organization: getContact.organization,
        homeOffice: getContact.homeOffice,
        title: getContact.title,
        profession: getContact.profession,
        private_note: getContact.private_note,
        industry: getContact.industry,
        assistant: getContact.assistant,
        designations: getContact.designations,
        primary_areas: getContact.primary_areas,
        languages: getContact.languages,
        specialties: getContact.specialties,
        marketing_message: getContact.marketing_message,
        birthday: getContact.birthday,
        gender: getContact.gender,
        anniversary: getContact.anniversary,
        recruiter: getContact.recruiter,
        Supporters: getContact.Supporters,
        connection: getContact.connection,
        note: getContact.note,
      };
      setLoader({ isActive: true });
      await user_service.contactUpdate(params.id, userData).then((response) => {
        if (response) {
          setGetContact(response.data);
          setLoader({ isActive: false });
          ContactGetById(params.id);
          setToaster({
            type: "Profile Updated",
            isShow: true,
            toasterBody: response.data.message,
            message: "Profile Updated Successfully",
          });
          setTimeout(() => {
            setToaster((prevToaster) => ({ ...prevToaster, isShow: false }));
          }, 3000);
        } else {
          setLoader({ isActive: false });
          setToaster({
            type: "error",
            isShow: true,
            toasterBody: response.data.message,
            message: "Error",
          });
        }
      });
    } catch (error) {
      setLoader({ isActive: false });
      setToaster({
        type: "error",
        isShow: true,
        toasterBody: error.response
          ? error.response.data.message
          : error.message,
        message: "Error",
      });
      setTimeout(() => {
        setToaster((prevToaster) => ({ ...prevToaster, isShow: false }));
      }, 3000);
    }
  };

  return (
    <div className="bg-secondary float-left w-100 pt-4 mb-4">
      <Loader isActive={isActive} />
      {isShow && (
        <Toaster
          type={type}
          isShow={isShow}
          toasterBody={toasterBody}
          message={message}
        />
      )}
      <main className="page-wrapper profile_page_wrap">
        <div className="content-overlay">
          {/* <div className="d-flex align-items-center justify-content-start mb-4">
            <h3 className="mb-0">{formValues.name}</h3>
          </div> */}
          <div className="">
            {/* <hr className="mt-2" /> */}
            {/* <hr className="mt-5" /> */}
            <div className="add_listing w-100">
              <div className="d-flex align-items-center justify-content-start mb-4">
                <h3 className="mb-0 text-white">Community Contact</h3>
              </div>
              <div className="bg-light rounded-3 border p-3 mb-3">
                <div className="row">
                  <div className="col-md-6">
                    <div className="row">
                      <div className="col-sm-12">
                        <label className="form-label">First Name</label>
                        <input
                          className="form-control"
                          id="inline-form-input"
                          type="text"
                          name="firstName"
                          placeholder="firstName"
                          value={getContact.firstName}
                          onChange={handleChange}
                        />
                      </div>

                      <div className="col-sm-12 mt-3">
                        <label className="form-label">Phone</label>
                        <input
                          className="form-control"
                          id="inline-form-input"
                          type="number"
                          name="phone"
                          placeholder="phone"
                          value={getContact.phone}
                          onChange={handleChange}
                        />
                      </div>

                      <div className="col-sm-12 mt-3">
                        <label className="form-label">Web</label>
                        <input
                          className="form-control"
                          id="inline-form-input"
                          type="text"
                          name="web"
                          value={getContact.web}
                          onChange={handleChange}
                        />
                      </div>

                      <div className="col-sm-12 mt-3">
                        <label className="form-label">Inbox</label>
                        <input
                          className="form-control"
                          id="inline-form-input"
                          type="text"
                          name="inbox"
                          value={getContact.inbox}
                          onChange={handleChange}
                        />
                      </div>

                      <div className="col-sm-12 mt-3">
                        <label className="form-label">Home Phone</label>
                        <input
                          className="form-control"
                          id="inline-form-input"
                          type="number"
                          name="home_phone"
                          value={getContact.home_phone}
                          onChange={handleChange}
                        />
                      </div>

                      <div className="col-sm-12 mt-3">
                        <label className="form-label">Work Phone</label>
                        <input
                          className="form-control"
                          id="inline-form-input"
                          type="number"
                          name="work_phone"
                          value={getContact.work_phone}
                          onChange={handleChange}
                        />
                      </div>

                      <div className="col-sm-12 mt-3">
                        <label className="form-label">Printed Name</label>
                        <input
                          className="form-control"
                          id="inline-form-input"
                          type="text"
                          name="printed_name"
                          value={getContact.printed_name}
                          onChange={handleChange}
                        />
                      </div>

                      <div className="col-sm-12 mt-3">
                        <label className="form-label">Spouse Last Name</label>
                        <input
                          className="form-control"
                          id="inline-form-input"
                          type="text"
                          name="spouse_lastname"
                          value={getContact.spouse_lastname}
                          onChange={handleChange}
                        />
                      </div>

                      <div className="col-sm-12 mt-3">
                        <label className="form-label">Spouse Number</label>
                        <input
                          className="form-control"
                          id="inline-form-input"
                          type="number"
                          name="spouse_number"
                          value={getContact.spouse_number}
                          onChange={handleChange}
                        />
                      </div>

                      <div className="col-sm-12 mt-3">
                        <label className="form-label">Business Name</label>
                        <input
                          className="form-control"
                          id="inline-form-input"
                          type="text"
                          name="bussiness_name"
                          value={getContact.bussiness_name}
                          onChange={handleChange}
                        />
                      </div>

                      <div className="col-sm-12 mt-3">
                        <label className="form-label">Sellers</label>
                        <input
                          className="form-control"
                          id="inline-form-input"
                          type="text"
                          name="sellers"
                          value={getContact.sellers}
                          onChange={handleChange}
                        />
                      </div>
                    </div>
                  </div>

                  <div className="col-md-6">
                    <div className="row">
                      <div className="col-sm-12">
                        <label className="form-label">Last Name</label>
                        <input
                          className="form-control"
                          id="inline-form-input"
                          type="text"
                          name="lastName"
                          placeholder="lastName"
                          value={getContact.lastName}
                          onChange={handleChange}
                        />
                      </div>

                      <div className="col-sm-12 mt-3">
                        <label className="form-label">Email</label>
                        <input
                          className="form-control"
                          id="inline-form-input"
                          type="text"
                          name="email"
                          placeholder="email"
                          value={getContact.email}
                          onChange={handleChange}
                        />
                      </div>

                      <div className="col-sm-12 mt-3">
                        <label className="form-label">Address</label>
                        <input
                          className="form-control"
                          id="inline-form-input"
                          type="text"
                          name="address"
                          value={getContact.address}
                          onChange={handleChange}
                          placeholder="Enter address"
                        />
                      </div>

                      <div className="col-sm-12 mt-3">
                        <label className="form-label">Fax</label>
                        <input
                          className="form-control"
                          id="inline-form-input"
                          type="text"
                          name="fax"
                          value={getContact.fax}
                          onChange={handleChange}
                        />
                      </div>

                      <div className="col-sm-12 mt-3">
                        <label className="form-label">Video Email</label>
                        <input
                          className="form-control"
                          id="inline-form-input"
                          type="text"
                          name="video_email"
                          value={getContact.video_email}
                          onChange={handleChange}
                        />
                      </div>

                      <div className="col-sm-12 mt-3">
                        <label className="form-label">Spouse First Name</label>
                        <input
                          className="form-control"
                          id="inline-form-input"
                          type="text"
                          name="spouse_firstname"
                          value={getContact.spouse_firstname}
                          onChange={handleChange}
                        />
                      </div>

                      <div className="col-sm-12 mt-3">
                        <label className="form-label">Spouse Email Name</label>
                        <input
                          className="form-control"
                          id="inline-form-input"
                          type="text"
                          name="spouse_email"
                          value={getContact.spouse_email}
                          onChange={handleChange}
                        />
                      </div>

                      <div className="col-sm-12 mt-3">
                        <label className="form-label">Spouse Birthday</label>
                        <input
                          className="form-control"
                          id="inline-form-input"
                          type="date"
                          name="spouse_birthday"
                          value={getContact.spouse_birthday}
                          onChange={handleChange}
                        />
                      </div>

                      <div className="col-sm-12 mt-3">
                        <label className="form-label">Business Address</label>
                        <input
                          className="form-control"
                          id="inline-form-input"
                          type="text"
                          name="bussiness_address"
                          value={getContact.bussiness_address}
                          onChange={handleChange}
                        />
                      </div>

                      <div className="col-sm-12 mt-3">
                        <label className="form-label">
                          Add to Contact type
                        </label>
                        <select
                          className="form-select"
                          id="pr-city"
                          name="personal_contacttype"
                          value={getContact.personal_contacttype}
                          data-bs-binded-element="#personal_contacttype"
                          onChange={handleChange}
                        >
                          <option>--Select--</option>
                          <option value="family_friends">Family&Friends</option>
                          <option value="past_clients">Past Clients</option>
                          <option value="b2b">B2B</option>
                          <option value="buyer">Buyer</option>
                        </select>
                      </div>

                      <div className="col-sm-12 mt-3">
                        <label className="form-label">Private Note</label>
                        <textarea
                          className="form-control mt-0"
                          id="textarea-input"
                          rows="5"
                          name="private_note"
                          onChange={handleChange}
                          autoComplete="on"
                          value={getContact.private_note}
                        ></textarea>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className="pull-right mt-3">
              <button
                className="btn btn-primary pull-right"
                type="button"
                onClick={handleSubmit}
              >
                Update
              </button>
            </div>
          </div>
        </div>
      </main>
    </div>
  );
};
export default CommunityContact;

import React, { useState, useEffect } from "react";
import _ from "lodash";
import user_service from "../service/user_service";
import jwt from "jwt-decode";
import Loader from "../../Pages/Loader/Loader.js";
import Toaster from "../../Pages/Toaster/Toaster.js";
import { useNavigate, NavLink, useParams } from "react-router-dom";
import Pic from "../img/pic.png";

function AddContactGroup() {
  const initialValues = {
    office_id: localStorage.getItem("active_office_id"),
    office_name: localStorage.getItem("active_office"),
    group_name: "",
    groupOffice: "",
    group_type: "",
    transactionSharing: "",
    admin_level: "",
  };
  const [formValues, setFormValues] = useState(initialValues);
  const [formErrors, setFormErrors] = useState({});
  const [isSubmitClick, setISSubmitClick] = useState(false);

  const [loader, setLoader] = useState({ isActive: null });
  const { isActive } = loader;
  const [toaster, setToaster] = useState({
    types: null,
    isShow: null,
    toasterBody: "",
    message: "",
  });
  const { types, isShow, toasterBody, message } = toaster;

  const [mainNavLinkText, setMainNavLinkText] = useState("Corporate");
  const [checkRadioGroup, setCheckRadioGroup] = useState("standard");
  const [checkRadioTransaction, setCheckRadioTransaction] = useState("");
  const [checkRadioAdmin, setCheckRadioAdmin] = useState("admin_label");

  const navigate = useNavigate();
  const params = useParams();
  // onChange Function start
  const handleChange = (e) => {
    const { name, value } = e.target;
    setFormValues({ ...formValues, [name]: value });
  };
  // onChange Function end

  {
    /* <!-- Form Validation Start--> */
  }
  useEffect(() => {
    if (formValues && isSubmitClick) {
      validate();
    }
  }, [formValues]);

  const validate = () => {
    const values = formValues;
    const errors = {};

    if (!values.group_name) {
      errors.group_name = "Group Name is required.";
    }
    setFormErrors(errors);
    return errors;
  };
  {
    /* <!-- Form Validation End--> */
  }
  const handleSubmit = async (e) => {
    // if (params.id) {
    //     e.preventDefault();
    //     setISSubmitClick(true)
    //     let checkValue = validate()
    //     if (_.isEmpty(checkValue)) {
    //         const userData = {
    //             agentId: jwt(localStorage.getItem('auth')).id,
    //             group_name: formValues.group_name,
    //             groupOffice: mainNavLinkText,
    //             office_id: localStorage.getItem("active_office_id"),
    //             office_name: localStorage.getItem("active_office"),
    //             group_type:checkRadioGroup,
    //             transactionSharing:checkRadioTransaction,
    //             admin_level: checkRadioAdmin
    //         };
    //         //console.log(userData);
    //         setLoader({ isActive: true })
    //         await user_service.ShareDocPostById(params.id, userData).then((response) => {
    //             if (response) {
    //                 console.log(response);
    //                 setFormValues(response.data);
    //                 setLoader({ isActive: false })
    //                 setToaster({ type: "Add Section", isShow: true, toasterBody: response.data.message, message: "Add Section Successfully", });
    //                 setTimeout(() => {
    //                     setToaster((prevToaster) => ({ ...prevToaster, isShow: false }))
    //                     navigate("/control-panel/documents/");
    //                 }, 1000);
    //             }
    //             else {
    //                 setLoader({ isActive: false })
    //                 setToaster({ types: "error", isShow: true, toasterBody: response.data.message, message: "Error", });
    //             }
    //         });
    //     }
    // }
    // else {
    e.preventDefault();
    setISSubmitClick(true);
    let checkValue = validate();
    if (_.isEmpty(checkValue)) {
      const userData = {
        agentId: jwt(localStorage.getItem("auth")).id,
        group_name: formValues.group_name,
        groupOffice: mainNavLinkText,
        office_id: localStorage.getItem("active_office_id"),
        office_name: localStorage.getItem("active_office"),
        group_type: checkRadioGroup,
        transactionSharing: checkRadioTransaction,
        admin_level: checkRadioAdmin,
      };
      console.log(userData);
      setLoader({ isActive: true });
      await user_service.contactGroupPost(userData).then((response) => {
        if (response) {
          console.log(response);
          setLoader({ isActive: false });
          setToaster({
            type: "Add Group",
            isShow: true,
            toasterBody: response.data.message,
            message: "Group Added Successfully",
          });
          contactGroupGetAll()
          setTimeout(() => {
            setToaster((prevToaster) => ({ ...prevToaster, isShow: false }));
            // navigate("/control-panel/documents/");
          }, 1000);
        } else {
          setLoader({ isActive: false });
          setToaster({
            types: "error",
            isShow: true,
            toasterBody: response.data.message,
            message: "Error",
          });
        }
      });
    }
    // }
  };

  {
    /* <!-- Api call Form onSubmit End--> */
  }

  const handleBack = () => {
    navigate("/contact/");
  };

  const checkGroup = (e) => {
    setCheckRadioGroup(e.target.name);
    setCheckRadioGroup(e.target.value);
  };

  const checkTransactionType = (e) => {
    setCheckRadioTransaction(e.target.name);
    setCheckRadioTransaction(e.target.value);
  };

  const checkAdminLevel = (e) => {
    setCheckRadioAdmin(e.target.name);
    setCheckRadioAdmin(e.target.value);
  };

  const handleMainNavLinkClick = (e, item) => {
    setMainNavLinkText(item);
    document.getElementById("closeLocation").click();
  };

  const [contactGroup, setContactGroup] = useState([]);

  useEffect(() => {
    window.scrollTo(0, 0);
    setLoader({ isActive: true });
    contactGroupGetAll();
  }, []);

  const contactGroupGetAll = async () => {
    await user_service.contactGroupGet().then((response) => {
      setLoader({ isActive: false });
      if (response) {
        setContactGroup(response.data.data);
      }
    });
  };

  const handleContactGroup = (id)=>{
    navigate(`/contact-group-detail/${id}`);
  }

  return (
    <div className="bg-secondary float-left w-100 pt-4 mb-4">
      <Loader isActive={isActive} />
      {isShow && (
        <Toaster
          types={types}
          isShow={isShow}
          toasterBody={toasterBody}
          message={message}
        />
      )}
      <main className="page-wrapper">
        <div className="content-overlay">
          <div className="d-flex align-items-center justify-content-start mb-4">
            <h3 className="mb-0 text-white">Create a New Group</h3>
          </div>
          <div className="">
            {/* <hr className="mt-2" /> */}
            <div className="">
              <div className="row">
                <div className="col-md-6">
                  <div className="float-left w-100 bg-light rounded-3 border p-3">
                    <div className="col-sm-12">
                      <label className="form-label">Group Name</label>
                      <input
                        className="form-control"
                        id="inline-form-input"
                        type="text"
                        name="group_name"
                        value={formValues.group_name}
                        onChange={handleChange}
                        style={{
                          border: formErrors?.group_name ? "1px solid red" : "",
                        }}
                      />
                      {formErrors.group_name && (
                        <div className="invalid-tooltip">
                          {formErrors.group_name}
                        </div>
                      )}
                    </div>
                    <div className="col-sm-12 mt-4">
                      <h6 className="mb-2">Publication Level</h6>
                      <label className="form-label">
                        At what location level is this section published?
                      </label>
                      <div className="d-flex">
                        <p className="mb-0">
                          {mainNavLinkText}&nbsp;&nbsp;
                          <NavLink>
                            <span
                              type="button"
                              data-toggle="modal"
                              data-target="#addLocation"
                            >
                              change
                            </span>
                          </NavLink>
                        </p>
                      </div>
                    </div>
                    <div
                      className="col-sm-12 mt-4"
                      onClick={(e) => checkGroup(e)}
                    >
                      <h6 className="mb-2">Group Type</h6>
                      <label className="form-label">
                        Group could be either normal group or transactional team
                      </label>
                      <div className="d-flex">
                        <div className="form-check">
                          <input
                            className="form-check-input"
                            id="form-check-1"
                            type="radio"
                            name="group"
                            value="standard"
                          />
                          <label className="form-check-label" id="radio-level1">
                            Standard
                          </label>
                        </div>
                        <div className="form-check ms-3">
                          <input
                            className="form-check-input"
                            id="form-check-2"
                            type="radio"
                            name="group"
                            value="transaction_team"
                          />
                          <label className="form-check-label" id="radio-level2">
                            Transaction Team
                          </label>
                        </div>
                      </div>
                    </div>

                    {checkRadioGroup === "transaction_team" ? (
                      <div
                        className="col-sm-12 mt-4"
                        onClick={(e) => checkTransactionType(e)}
                      >
                        <h6 className="mb-2">Transaction Sharing Type</h6>
                        <label className="form-label">
                          Based on the sharing type, members can view/edit the
                          transactions
                        </label>
                        <div className="">
                          <div className="form-check">
                            <input
                              className="form-check-input"
                              id="form-check-1"
                              type="radio"
                              name="transaction_type"
                              value="leader"
                            />
                            <label
                              className="form-check-label"
                              id="radio-level1"
                            >
                              <b>Leader Only </b> - Leaders see all
                              transactions, members see only their own
                            </label>
                          </div>
                          <div className="form-check">
                            <input
                              className="form-check-input"
                              id="form-check-2"
                              type="radio"
                              name="transaction_type"
                              value="members_view"
                            />
                            <label
                              className="form-check-label"
                              id="radio-level2"
                            >
                              <b>Members View </b> - All members see all
                              transactions, view only
                            </label>
                          </div>
                          <div className="form-check">
                            <input
                              className="form-check-input"
                              id="form-check-3"
                              type="radio"
                              name="transaction_type"
                              value="member_edit"
                            />
                            <label
                              className="form-check-label"
                              id="radio-level3"
                            >
                              <b>Members Edit </b> - All members are able to
                              edit all transactions
                            </label>
                          </div>
                        </div>
                      </div>
                    ) : (
                      ""
                    )}
                    <div></div>

                    <div
                      className="col-sm-12 mt-4"
                      onClick={(e) => checkAdminLevel(e)}
                    >
                      <h6 className="mb-2">Admin Level</h6>
                      <label className="form-label">
                        Level of staff required to change name or publication
                        level.
                      </label>
                      <div className="d-flex">
                        <div className="form-check">
                          <input
                            className="form-check-input"
                            id="form-check-1"
                            type="radio"
                            name="admin"
                            value="admin_label"
                            checked
                          />
                          <label className="form-check-label" id="radio-level1">
                            1-Office Staff
                          </label>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="col-md-12 mt-3">
                    <button className="btn btn-secondary pull-right ms-3 mt-2" type="button" onClick={handleBack}>Cancel & Go Back</button>
                    <button
                      className="btn btn-primary pull-right mt-2"
                      type="button"
                      onClick={handleSubmit}
                    >
                      Add Group
                    </button>
                  </div>
                </div>
                <div className="col-md-6 bg-light p-3 border-0 rounded-3">
                  {contactGroup && contactGroup.length > 0 ? (
                    contactGroup.map((post) => (
                      <div className="card bg-secondary card-hover mb-2 mt-4">
                        <div className="card-body">
                          <div className="float-left w-100">
                            <div className="float-left w-100 d-flex align-items-center justify-content-between">
                              <div
                                className="float-left w-100 d-flex align-items-center justify-content-start"
                                key={post._id}
                                onClick={()=>handleContactGroup(post._id)}
                              >
                                <img
                                  className="rounded-circlee profile_picture"
                                  height="70"
                                  width="70"
                                  src={
                                    post.image && post.image !== "image"
                                      ? post.image
                                      : Pic
                                  }
                                  alt="Profile"
                                />

                                {/* <div className="ms-3" onClick={()=>handleSubmit(post._id)}> */}
                                <div className="ms-3">
                                  <strong>{post.group_name}</strong>
                                  <br />
                                  <strong>
                                    Integra Realty- {post.groupOffice}
                                  </strong>
                                  <br />
                                  <strong>
                                    {post.group_type === "standard"
                                      ? "Standard Group"
                                      : post.group_type === "transaction_team"
                                      ? "Transaction Team"
                                      : ""}
                                  </strong>
                                    
                                </div>
                              </div>

                              {/* <span className="pull-right tags-badge d-flex align-items-center justify-content-end w-100">
                            <NavLink
                              to={`/contact-profile/${post._id}`}
                              className=""
                              title="Please click on the edit icon to open the contact page, then click to the account details tab and  update your license"
                            >
                              <i className="fi-edit"></i>
                            </NavLink>
                          </span> */}
                            </div>
                          </div>
                        </div>
                      </div>
                    ))
                  ) : (
                    <p className="text-center mt-5">
                      <span className="">No Data found</span>
                    </p>
                  )}
                </div>
              </div>
            </div>
          </div>
        </div>
      </main>

      <div className="modal in" role="dialog" id="addLocation">
        <div
          className="modal-dialog modal-dialog-centered modal-dialog-scrollable"
          role="document"
        >
          <div className="modal-content">
            <div className="modal-header border-0 pb-0">
              {/* <h4 className="modal-title"></h4> */}
              <button
                id="closeLocation"
                className="btn-close"
                type="button"
                data-dismiss="modal"
                aria-label="Close"
              ></button>
            </div>
            <div className="modal-body fs-sm pt-0">
              <div className="content-overlay">
                <div className="bg-light">
                  <p className="mb-2">Choose a location. </p>
                  {/* <button type="button" className="btn btn-default btn btn-primary btn-sm mt-0 pull-right" id="closeLocation" data-dismiss="modal">X</button> */}
                  <p>Choose the location from the tree.</p>
                  <div className="pull-right mt-4">
                    <NavLink className="btn btn-sm btn-primary">
                      <span
                        onClick={(e) => handleMainNavLinkClick(e, "Corporate")}
                      >
                        Corporate
                      </span>
                    </NavLink>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
export default AddContactGroup;

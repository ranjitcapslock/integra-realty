import React, { useState, useEffect } from "react";
import _ from "lodash";
import user_service from "../service/user_service";
import jwt from "jwt-decode";
import Loader from "../../Pages/Loader/Loader.js";
import Toaster from "../../Pages/Toaster/Toaster.js";
import { useNavigate, NavLink, useParams } from "react-router-dom";
import Pic from "../img/pic.png";
import defaultpropertyimage from "../../Components/img/defaultpropertyimage.jpeg";



function AddGroupMember() {
  const initialValues = {
    office_id: localStorage.getItem("active_office_id"),
    office_name: localStorage.getItem("active_office"),
    group_name: "",
    groupId: "",
    group_member: [],
    join_date: "",
  };
  const [formValues, setFormValues] = useState(initialValues);
  const [formErrors, setFormErrors] = useState({});
  const [isSubmitClick, setISSubmitClick] = useState(false);

  const [loader, setLoader] = useState({ isActive: null });
  const { isActive } = loader;
  const [toaster, setToaster] = useState({
    types: null,
    isShow: null,
    toasterBody: "",
    message: "",
  });
  const { types, isShow, toasterBody, message } = toaster;

  const [contactType, setContactType] = useState("associate");
  const [contactStatus, setContactStatus] = useState("active");

  const [contactName, setContactName] = useState("");
  const [getContact, setGetContact] = useState([]);
  const navigate = useNavigate();
  const params = useParams();

  const handleChange = (e) => {
    const { name, value } = e.target;
    setFormValues({ ...formValues, [name]: value });
  };
  
 
  const SearchGetAll = async () => {
    try {
      const response = await user_service.SearchContactfilterAssociate(
        1,
        contactType,
        contactStatus,
        contactName
      );
      if (response) {
        setGetContact(response.data.data);
        // setPageCount(Math.ceil(response.data.totalRecords / 10));
        // setTotalRecords(response.data.totalRecords);
      }
    } catch (error) {
      console.error("Error fetching data:", error);
    }
  };

  useEffect(() => {
    window.scrollTo(0, 0);
    if (contactName) {
      SearchGetAll(1);
    }
  }, [contactType, contactName]);

  const handleSubmit = async (e) => {
    e.preventDefault();
  
    try {
      const groupToUpdate = contactGroupMember.find(group => group._id);
      const groupToUpdateAssociate = contactsAssociate.find(group => group._id);
  
      if (groupToUpdate) {
        // Update existing group member
        const updatedGroupMember = {
          ...groupToUpdate,
          group_member: [
            ...groupToUpdate.group_member,
            { _id: groupToUpdateAssociate._id } // Ensure group_member is an array of objects
          ]
          // Include other fields as needed for update
        };
  
        console.log(updatedGroupMember);
  
        setLoader({ isActive: true });
  
        const response = await user_service.contactGroupMemberUpdate(groupToUpdate._id, updatedGroupMember);
  
        if (response) {
          console.log(response);
          setContactsAssociate([]);
          setLoader({ isActive: false });
          setToaster({
            type: "success",
            isShow: true,
            message: "Group Member Updated Successfully",
          });
          ContactGroupMemberGetAll(summary.group_name);
          setTimeout(() => {
            setToaster((prevToaster) => ({ ...prevToaster, isShow: false }));
          }, 1000);
        } else {
          setLoader({ isActive: false });
          setToaster({
            type: "error",
            isShow: true,
            toasterBody: response.data.message,
          });
        }
  
      } else {
        // Prepare data for post (create)
        let currentDate = formValues.join_date ? new Date(formValues.join_date) : new Date();
  
        // Ensure that the join_date is a valid Date object
        if (isNaN(currentDate)) {
          currentDate = new Date();
        }
  
        const formattedDate = currentDate.toISOString();
  
        const userData = {
          agentId: jwt(localStorage.getItem("auth")).id,
          group_name: summary.group_name,
          groupId: summary._id,
          group_member: contactsAssociate.map(member => ({ _id: member._id })), // Ensure group_member is an array of objects
          office_id: localStorage.getItem("active_office_id"),
          office_name: localStorage.getItem("active_office"),
          join_date: formattedDate,
        };
  
        console.log(userData);
  
        setLoader({ isActive: true });
  
        const response = await user_service.contactGroupMemberPost(userData);
  
        if (response) {
          console.log(response);
          setContactsAssociate([]);
          setLoader({ isActive: false });
          setToaster({
            type: "success",
            isShow: true,
            message: "Group Member Added Successfully",
          });
          ContactGroupMemberGetAll(summary.group_name);
          setTimeout(() => {
            setToaster((prevToaster) => ({ ...prevToaster, isShow: false }));
          }, 1000);
        } else {
          setLoader({ isActive: false });
          setToaster({
            type: "error",
            isShow: true,
            toasterBody: response.data.message,
          });
        }
      }
  
    } catch (err) {
      setLoader({ isActive: false });
      setToaster({
        type: "API Error",
        isShow: true,
        toasterBody: err.response?.data?.message || "An error occurred",
        message: "API Error",
      });
      setTimeout(() => {
        setToaster((prevToaster) => ({ ...prevToaster, isShow: false }));
      }, 3000);
    }
  };
  

    // const handleSubmit = async (e) => {
    //   e.preventDefault();
    //   let currentDate = formValues.join_date ? new Date(formValues.join_date) : new Date();
  
    // if (isNaN(currentDate)) {
    //     currentDate = new Date();
    // }
    
    // let formattedDate = currentDate.toISOString();
    //   const userData = {
    //     agentId: jwt(localStorage.getItem("auth")).id,
    //     group_name: summary.group_name,
    //     groupId: summary._id,
    //     group_member: contactsAssociate,
    //     office_id: localStorage.getItem("active_office_id"),
    //     office_name: localStorage.getItem("active_office"),
    //     join_date: formattedDate,
    //   };
    
    //   console.log(userData);
    //   try {
    //     setLoader({ isActive: true });
    //       await user_service.contactGroupMemberPost(userData)
    //         .then((response) => {
    //           if (response) {
    //             console.log(response);
    //             setContactsAssociate([]);
    //             setLoader({ isActive: false });
    //             setToaster({
    //               type: "Add Group Member",
    //               isShow: true,
    //               message: "Group Member Added Successfully",
    //             });
    //             ContactGroupMemberGetAll(summary.group_name);
    //             setTimeout(() => {
    //               setToaster((prevToaster) => ({ ...prevToaster, isShow: false }));
    //             }, 1000);
    //           } else {
    //             setLoader({ isActive: false });
    //             setToaster({
    //               type: "error",
    //               isShow: true,
    //               toasterBody: response.data.message,
    //             });
    //           }
    //         });
    //   } catch (err) {
    //     setLoader({ isActive: false });
    //     setToaster({
    //       type: "API Error",
    //       isShow: true,
    //       toasterBody: err.response?.data?.message || "An error occurred",
    //       message: "API Error",
    //     });
    //     setTimeout(() => {
    //       setToaster((prevToaster) => ({ ...prevToaster, isShow: false }));
    //     }, 3000);
    //   }
    // };
    


  {
    /* <!-- Api call Form onSubmit End--> */
  }

  const handleBack = () => {
    navigate(`/contact-group`);
  };

  const [summary, setSummary] = useState("");

  useEffect(() => {
    window.scrollTo(0, 0);
    setLoader({ isActive: true });
    ContactGroup(params.id);
  }, []);

  const ContactGroup = async () => {
    await user_service.contactGroupGetById(params.id).then((response) => {
      setLoader({ isActive: false });
      if (response) {
        setSummary(response.data);
        ContactGroupMemberGetAll(response.data.group_name)
      }
    });
  };
  const handleContactGroup = (id) => {
    navigate(`/contact-group-detail/${id}`);
  };

  const [contactsAssociate, setContactsAssociate] = useState([]);
  const handleContactAssociate = (contactAssociate) => {
    setContactsAssociate((prevContacts) => [...prevContacts, contactAssociate]);
    setGetContact([]);
    setContactName("");
  };

  useEffect(() => {
    window.scrollTo(0, 0);
    setLoader({ isActive: true });
    ContactGroupMemberGetAll();
  }, []);

  const [contactGroupMember, setContactGroupMember] = useState([]);

  const ContactGroupMemberGetAll = async (group_name) => {
    try {
      setLoader({ isActive: true });
      const response = await user_service.contactGroupMemberGet(group_name);
  
      setLoader({ isActive: false });
  
      if (response && response.data) {
        console.log(response);
        setContactGroupMember(response.data.data);
      }
    } catch (error) {
      console.error('Error fetching data:', error);
      setLoader({ isActive: false });
    }
  };
  

  const handleContactPeofile = (id)=>{
   navigate(`/contact-profile/${id}`);
  }

  const formatDate = (dateString) => {
    const date = new Date(dateString);
    const options = {
      year: 'numeric',
      month: '2-digit',
      day: '2-digit',
      // hour: '2-digit',
      // minute: '2-digit',
      // second: '2-digit',
    };
    return new Intl.DateTimeFormat('en-US', options).format(date);
  };

  const handleDelete = async (groupId, memberIndex) => {
    try {
      const updatedMembers = contactGroupMember.map(group => {
        if (group._id === groupId) {
          const updatedGroupMembers = group.group_member.filter((_, index) => index !== memberIndex);
          return { ...group, group_member: updatedGroupMembers };
        }
        return group;
      });
  
      setContactGroupMember(updatedMembers);
  
      // Prepare data for API update
      const groupToUpdate = updatedMembers.find(group => group._id);
      const userData = {
        group_member: groupToUpdate.group_member,
      };
  
      console.log(userData);
  
      // // Uncomment the following once integrated with your API
      setLoader({ isActive: true });
      const response = await user_service.contactGroupMemberUpdate(groupId, userData);
      
      if (response) {
        console.log(response);
        setContactsAssociate([]);
        setLoader({ isActive: false });
        setToaster({
          type: "success",
          isShow: true,
          message: "Group Member Deleted Successfully",
        });
        ContactGroupMemberGetAll(summary.group_name);
        setTimeout(() => {
          setToaster((prevToaster) => ({ ...prevToaster, isShow: false }));
        }, 1000);
      } else {
        setLoader({ isActive: false });
        setToaster({
          type: "error",
          isShow: true,
          toasterBody: response.data.message,
        });
      }
    } catch (err) {
      console.error('API Error:', err);
      // Handle error state in your React component
      // setLoader({ isActive: false });
      // setToaster({
      //   type: "error",
      //   isShow: true,
      //   toasterBody: err.response?.data?.message || "An error occurred",
      // });
      // setTimeout(() => {
      //   setToaster((prevToaster) => ({ ...prevToaster, isShow: false }));
      // }, 3000);
    }
  };
  
  return (
    <div className="bg-secondary float-left w-100 pt-4 mb-4">
      <Loader isActive={isActive} />
      {isShow && (
        <Toaster
          types={types}
          isShow={isShow}
          toasterBody={toasterBody}
          message={message}
        />
      )}
      <main className="page-wrapper">
        <div className="content-overlay">
          <div className="d-flex align-items-center justify-content-start mb-4">
            <h3 className="mb-0 text-white">Add Member</h3>
          </div>
          <div className="row">
            <div className="col-md-6">
              <div className="bg-light border rounded-3 p-3">
                <div className="row">
                  <div className="col-md-6">
                    <div className="col-sm-12">
                      <label className="col-form-label">
                        Group : {summary.group_name}
                      </label>
                    </div>
                    <div className="col-sm-12 mt-3">
                      <label className="col-form-label">
                        Find an associate by name:
                      </label>
                      <input
                        className="form-control"
                        id="inline-form-input"
                        type="text"
                        name="contactName"
                        onChange={(event) => setContactName(event.target.value)}
                      />
                          {
                            contactName ?
                            <>
                             {getContact.length > 0 ? (
                            getContact.map((contact, index) => (
                              <div
                                className="member_Associate mb-1 mt-1"
                                key={index}
                                onClick={() => handleContactAssociate(contact)}
                              >
                                <div
                                  className="float-left w-100 d-flex align-items-center justify-content-start"
                                  key={contact._id}
                                >
                                  <img
                                    className="rounded-circlee profile_picture"
                                    height="30"
                                    width="30"
                                    src={
                                      contact.image && contact.image !== "image"
                                        ? contact.image
                                        : Pic
                                    }
                                    alt="Profile"
                                  />

                                  {/* <div className="ms-3" onClick={()=>handleSubmit(post._id)}> */}
                                  <div className="ms-3">
                                    <strong>
                                      {contact.firstName} {contact.lastName}
                                    </strong>
                                    <br />
                                    <strong>{contact.active_office}</strong>
                                  </div>
                                </div>
                              </div>
                            ))
                          ) : (
                            <p></p>
                          )}
                            </>
                            :
                            <></>
                          }
                    </div>

                    <div className="col-sm-12 mt-3">
                      <label className="col-form-label">Join Date:</label>
                      <input
                        className="form-control"
                        id="inline-form-input"
                        type="date"
                        name="join_date"
                        value={formValues.join_date}
                        onChange={handleChange}
                        style={{
                          border: formErrors?.join_date ? "1px solid red" : "",
                        }}
                      />
                      {formErrors.join_date && (
                        <div className="invalid-tooltip">
                          {formErrors.join_date}
                        </div>
                      )}
                    </div>
                  </div>
                  <div className="col-md-6 mt-3">
                    <label className="col-form-label">Members to add:</label>
                    <div className="col-sm-12 member_group rounded-2">
                      {contactsAssociate.length > 0 ? (
                        contactsAssociate.map((contact, index) => (
                          <div className="member_Associate mb-1" key={index}>
                            <div
                              className="float-left w-100 d-flex align-items-center justify-content-start"
                              key={contact._id}
                            >
                              <img
                                className="rounded-circlee profile_picture"
                                height="30"
                                width="30"
                                src={
                                  contact.image && contact.image !== "image"
                                    ? contact.image
                                    : Pic
                                }
                                alt="Profile"
                              />

                              {/* <div className="ms-3" onClick={()=>handleSubmit(post._id)}> */}
                              <div className="ms-3">
                                <strong>
                                  {contact.firstName} {contact.lastName}
                                </strong>
                                <br />
                                <strong>{contact.active_office}</strong>
                              </div>
                            </div>
                          </div>
                        ))
                      ) : (
                        <p>No contacts found.</p>
                      )}
                    </div>
                  </div>
                  <div className="mt-4">
                    <button className="btn btn-primary" onClick={handleSubmit}>
                      Add New Members
                    </button>
                    <button className="btn btn-secondary ms-2" onClick={handleBack}>
                     Cancel
                    </button>
                  </div>
                </div>
              </div>
            </div>
            <div className="col-md-6 mt-lg-0 mt-md-0 mt-sm-3 mt-3">
              <div className="table-responsive bg-light border rounded-3 p-3">
                  <table
                    id="DepositLedger"
                    className="table table-striped align-middle mb-0 w-100"
                    width="100%"
                    border="0"
                    cellSpacing="0"
                    cellPadding="0">
                    <thead>
                      <tr>
                        <th>Member</th>
                        <th>Office</th>
                        <th>Joined</th>
                        <th></th>
                        {/* <th>DELETE</th>  */}
                        {/* {/* <th>EDIT</th> */}
                      </tr>
                    </thead>
                    <tbody>
                      {Array.isArray(contactGroupMember) &&
                      contactGroupMember.length > 0 ? (
                        contactGroupMember.map((item, index) => (
                          <React.Fragment key={index}>
                            {item.group_member.map(
                              (member, memberIndex) => (
                                <tr key={memberIndex}>
                                  <td onClick={()=>handleContactPeofile(member._id)}>
                                    <img className="rounded-circlee profile_picture" style={{ width: "45px", height: "45px", }}
                                   src={
                                    member.image &&
                                    member.image !== "image"
                                     ?  member.image
                                     : defaultpropertyimage
                                 }
                                  /> {member.firstName}  {member.lastName}
                                   </td>
                                   <td>{member.active_office}</td>
                                   <td>{formatDate(item.join_date)}</td>
                                   <td><button className="btn btn-secondary btn-sm" onClick={()=>handleDelete(item._id, memberIndex)}>Delete</button></td>
                                </tr>
                              )
                            )}
                          </React.Fragment>
                        ))
                      ) : (
                        <tr>
                          <td colSpan="2">No data to display.</td>
                        </tr>
                      )}
                    </tbody>
                  </table>
               
              </div>
            </div>
          </div>
        </div>
      </main>
    </div>
  );
}
export default AddGroupMember;

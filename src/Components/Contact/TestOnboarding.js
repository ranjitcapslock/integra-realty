import React, { useState, useRef, useEffect } from "react";
// import SignatureCanvas from "react-signature-canvas";
import {
  PDFDocument,
  PDFLib,
  PDFTextField,
  PDFCheckBox,
  PDFRadioGroup,
  rgb,
  StandardFonts,
} from "pdf-lib";
import axios from "axios";
import SignatureCanvas from "react-signature-canvas";
import user_service from "../service/user_service";
const PdfIframe = () => {
  const signatureCanvasRef = useRef(null);
  const [pageNumber, setPageNumber] = useState(1);

  const [signatures, setSignatures] = useState({});

  const [fieldValues, setFieldValues] = useState({});
  const [form, setForm] = useState([]);
  const [fieldNames, setFieldNames] = useState([]);

  const [pdfSrc, setPdfSrc] = useState(PDF_SRC);
  const [isdone, setIsdone] = useState(false);
  const [blobResponce, setBlobResponce] = useState("");

  const handleFieldChange = (fieldName, value) => {
    setFieldValues((prevFieldValues) => ({
      ...prevFieldValues,
      [fieldName]: value,
    }));
  };

  const handleClearSignature = () => {
    signatureCanvasRef.current.clear();
  };

  const handleSaveSignature = () => {
    const isCanvasEmpty = signatureCanvasRef.current.isEmpty();
    if (!isCanvasEmpty) {
      const signatureData = signatureCanvasRef.current.toDataURL();
      console.log(signatureData);
      setSignatures((prevSignatures) => ({
        ...prevSignatures,
        [pageNumber]: signatureData,
      }));
    } else {
      alert("Please sign before saving.");
    }
  };

  const fetchAndDisplayFields = async () => {
    try {
      const response = await fetch(pdfSrc);
      if (!response.ok) {
        throw new Error("Failed to fetch PDF");
      }

      const existingPdfBytes = await response.arrayBuffer();
      const existingPdfDoc = await PDFDocument.load(existingPdfBytes);

      const form = existingPdfDoc.getForm();
      const names = form.getFields().map((field) => field.getName());

      setForm(form);
      setFieldNames(names);
      // console.log(form);
      // console.log(names);
    } catch (error) {
      console.error("Error fetching PDF fields:", error.message);
    }
  };

  useEffect(() => {
    fetchAndDisplayFields();
  }, [pdfSrc]);

  const handleFillAndGeneratePdf = async () => {
    try {
      // setIsdone(true);
      const response = await fetch(pdfSrc);
      if (!response.ok) {
        throw new Error("Failed to fetch PDF");
      }

      const existingPdfBytes = await response.arrayBuffer();
      const existingPdfDoc = await PDFDocument.load(existingPdfBytes);
      const firstPage = existingPdfDoc.getPages()[0];

      const lastPage = existingPdfDoc.getPages()[existingPdfDoc.getPageCount() - 1];

      const signatureData = signatures[pageNumber];
      const signatureBytes = await fetch(signatureData).then((res) => res.arrayBuffer());
      const signatureImage = await existingPdfDoc.embedPng(signatureBytes);

      firstPage.drawImage(signatureImage, {
        x: 105,
        y: 205,
        width: 200,
        height: 100,
      });

      lastPage.drawImage(signatureImage, {
        x: 105,
        y: 205,
        width: 200,
        height: 100,
      });


      const form = existingPdfDoc.getForm();
      const fieldNames = form.getFields().map((field) => field.getName());

      console.log("Form field names:", fieldNames);

      for (const fieldName of fieldNames) {
        const field = form.getField(fieldName);

        if (field) {
          if (field instanceof PDFTextField) {
            const textField = form.getTextField(fieldName);
            const fieldValue = fieldValues[fieldName] || ""; // Get value from state

            // Attach the onInput event to capture real-time updates
            const inputElement = document.querySelector(
              `input[name="${fieldName}"]`
            );
            if (inputElement) {
              inputElement.addEventListener("input", (event) => {
                handleFieldChange(fieldName, event.target.value);
              });
            }

            textField.setText(fieldValue);
          } else if (field instanceof PDFCheckBox) {
            const checkbox = form.getCheckBox(fieldName);
            const isChecked = fieldValues[fieldName]; // Get value from state
            console.log(fieldName);
            console.log(isChecked);
            if (isChecked) {
              checkbox.check(isChecked);
            }
          } else {
            console.warn(`Unsupported field type for "${fieldName}"`);
          }
        } else {
          console.warn(`No field found with the name "${fieldName}"`);
        }
      }

      const modifiedPdfBytes = await existingPdfDoc.save();
      const modifiedPdfBlob = new Blob([modifiedPdfBytes], {
        type: "application/pdf",
      });

      const pdfFormData = new FormData();
      pdfFormData.append("file", modifiedPdfBlob, "modified.pdf");

      const apiUrl = "https://api.brokeragentbase.com/upload";
      const config = {
        headers: {
          "Content-Type": "multipart/form-data",
        },
      };

      const uploadResponse = await axios.post(apiUrl, pdfFormData, config);
      setBlobResponce(uploadResponse)
      console.log("Response from the server:", uploadResponse.data);
           
              // setLoader({ isActive: false });
              // setStep5(true);
              // setIsdone(false);
      // const userData = {
      //   onboard_taxcertification: uploadResponse.data,
      // };
      // console.log(userData);

      // //  w9papernameAdd
      // const userDatap = {
      //   agentId: formValuesNew?._id,
      //   paperwork_id: w9papernameAdd?._id ?? "qqq",
      //   documenturl: uploadResponse.data,
      // };

      // const responsepaperwork = await user_service.agentpaperworkDocument(userDatap);

      // try {
      //   setLoader({ isActive: true });
      //   await user_service
      //     .contactUpdate(formValuesNew?._id, userData)
      //     .then((response) => {
      //       if (response) {
      //         // setIsShow(true);
      //         setStep4(false);
      //         setStep1("");
      //         setCategory("");
      //         setPhase("");
      //         setLoader({ isActive: false });
      //         setStep5(true);
      //         setIsdone(false);
      //         if (onBoard[0]?.image) {
      //         } else {
      //           readyPad();
      //         }
      //         ContactGetById(formValuesNew?._id);
      //         setToaster({
      //           type: "Document",
      //           isShow: true,
      //           toasterBody: response.data.message,
      //           message: "Document sign successfully",
      //         });
      //         setTimeout(() => {
      //           setToaster((prevToaster) => ({
      //             ...prevToaster,
      //             isShow: false,
      //           }));
      //           setIsShow(false);
      //           //navigate("/signin");
      //         }, 2000);
      //       } else {
      //         setLoader({ isActive: false });
      //         setToaster({
      //           types: "error",
      //           isShow: true,
      //           toasterBody: response.data.message,
      //           message: "Error",
      //         });
      //       }
      //     });
      // } catch (error) {
      //   setLoader({ isActive: false });
      //   setToaster({
      //     types: "error",
      //     isShow: true,
      //     toasterBody: error.response
      //       ? error.response.data.message
      //       : error.message,
      //     message: "Error",
      //   });
      //   setTimeout(() => {
      //     setToaster((prevToaster) => ({ ...prevToaster, isShow: false }));
      //   }, 2000);
      // }
    } catch (error) {
      console.error("Error:", error.message);
    }
  };

  return (
    <div className="bg-secondary float-left w-100 pt-4 mb-4">
      <main className="container onboarding">
        <div className="page-wrapper homepage_layout getContact_page_wrap">
          <div className="bg-light rounded-3 p-3  mb-3">
            <div className="row">
              <div className="col-md-4">
                <label className="col-form-label float-left w-100" for="pr-fn">
                  Please complete the following form to complete signup.
                </label>

                <div className="row">
                  <div className="col-sm-12 mb-2">
                    <label className="form-label" for="pr-fn">
                      1. Name
                    </label>
                    <p className="mb-0">
                      (as shown on your income tax return). Name is required on
                      this line; do not leave this line blank.
                    </p>
                    <input
                      className="form-control form-control-lg"
                      id="inline-form-input"
                      type="text"
                      name="topmostSubform[0].Page1[0].f1_1[0]"
                      // value={formValuesAboout?.topmostSubform[0].Page1[0].f1_1[0]}
                      onChange={(e) =>
                        handleFieldChange(
                          "topmostSubform[0].Page1[0].f1_1[0]",
                          e.target.value
                        )
                      }
                    />
                  </div>
                  <div className="col-sm-12 mb-2">
                    <label className="form-label" for="pr-fn">
                      2. Business
                    </label>
                    <p className="mb-0">
                      name/disregarded entity name, if different from above
                    </p>
                    <input
                      className="form-control form-control-lg"
                      id="inline-form-input"
                      type="text"
                      name="topmostSubform[0].Page1[0].f1_2[0]"
                      // value={formValuesAboout?.topmostSubform[0].Page1[0].f1_2[0]}
                      onChange={(e) =>
                        handleFieldChange(
                          "topmostSubform[0].Page1[0].f1_2[0]",
                          e.target.value
                        )
                      }
                    />
                  </div>
                  <div className="col-sm-12 mb-2 federal_tax_classification">
                    <label
                      className="form-label float-left w-100 pt-0"
                      for="pr-fn"
                    >
                      3. Check appropriate box for federal tax classification of
                      the person whose name is entered on line 1. Check only one
                      of the following seven boxes.
                    </label>
                  </div>
                  <div className="row">
                    <div className="col-sm-12 mb-2 d-flex">
                      <input
                        className="form-check-input"
                        type="checkbox"
                        name="topmostSubform[0].Page1[0].FederalClassification[0].c1_1[0]"
                        onChange={(e) =>
                          handleFieldChange(
                            "topmostSubform[0].Page1[0].FederalClassification[0].c1_1[0]",
                            e.target.checked
                          )
                        }
                        checked={
                          fieldValues[
                            "topmostSubform[0].Page1[0].FederalClassification[0].c1_1[0]"
                          ]
                        }
                      />
                      &nbsp;
                      <p className="float-left mb-0 d-flex align-items-center">
                        Individual/sole proprietor or single-member LLC
                      </p>
                    </div>
                    <div className="col-sm-12 mb-2 d-flex">
                      <input
                        className="form-check-input"
                        type="checkbox"
                        name="topmostSubform[0].Page1[0].FederalClassification[0].c1_1[1]"
                        onChange={(e) =>
                          handleFieldChange(
                            "topmostSubform[0].Page1[0].FederalClassification[0].c1_1[1]",
                            e.target.checked
                          )
                        }
                        checked={
                          fieldValues[
                            "topmostSubform[0].Page1[0].FederalClassification[0].c1_1[1]"
                          ]
                        }
                      />
                      &nbsp;
                      <p className="float-left mb-0 d-flex align-items-center">
                        C Corporation{" "}
                      </p>
                    </div>
                    <div className="col-sm-12 mb-2 d-flex">
                      <input
                        className="form-check-input"
                        type="checkbox"
                        name="topmostSubform[0].Page1[0].FederalClassification[0].c1_1[2]"
                        onChange={(e) =>
                          handleFieldChange(
                            "topmostSubform[0].Page1[0].FederalClassification[0].c1_1[2]",
                            e.target.checked
                          )
                        }
                        checked={
                          fieldValues[
                            "topmostSubform[0].Page1[0].FederalClassification[0].c1_1[2]"
                          ]
                        }
                      />
                      &nbsp;
                      <p className="float-left mb-0 d-flex align-items-center">
                        S Corporation{" "}
                      </p>
                    </div>
                    <div className="col-sm-12 mb-2 d-flex">
                      <input
                        className="form-check-input"
                        type="checkbox"
                        name="topmostSubform[0].Page1[0].FederalClassification[0].c1_1[3]"
                        onChange={(e) =>
                          handleFieldChange(
                            "topmostSubform[0].Page1[0].FederalClassification[0].c1_1[3]",
                            e.target.checked
                          )
                        }
                        checked={
                          fieldValues[
                            "topmostSubform[0].Page1[0].FederalClassification[0].c1_1[3]"
                          ]
                        }
                      />
                      &nbsp;
                      <p className="float-left mb-0 d-flex align-items-center">
                        Partnership{" "}
                      </p>
                    </div>

                    <div className="col-sm-12 mb-2">
                      <div className="col-sm-12 d-flex">
                        <input
                          className="form-check-input"
                          type="checkbox"
                          name="topmostSubform[0].Page1[0].FederalClassification[0].c1_1[5]"
                          onChange={(e) =>
                            handleFieldChange(
                              "topmostSubform[0].Page1[0].FederalClassification[0].c1_1[5]",
                              e.target.checked
                            )
                          }
                          checked={
                            fieldValues[
                              "topmostSubform[0].Page1[0].FederalClassification[0].c1_1[5]"
                            ]
                          }
                        />
                        &nbsp;
                        <p className="float-left mb-0 d-flex align-items-center">
                          Limited liability company. Enter the tax
                          classification (C=C corporation, S=S corporation,
                          P=Partnership){" "}
                        </p>
                      </div>
                      <input
                        className="form-control form-control-lg ms-4"
                        id="inline-form-input"
                        type="text"
                        maxlength="1"
                        name="topmostSubform[0].Page1[0].FederalClassification[0].f1_3[0]"
                        // value={formValuesAboout?.topmostSubform[0].Page1[0].FederalClassification[0].f1_3[0]}
                        onChange={(e) =>
                          handleFieldChange(
                            "topmostSubform[0].Page1[0].FederalClassification[0].f1_3[0]",
                            e.target.value
                          )
                        }
                      />
                    </div>

                    <div className="col-sm-12 mb-2">
                      <div className="col-sm-12 d-flex">
                        <input
                          className="form-check-input"
                          type="checkbox"
                          name="topmostSubform[0].Page1[0].FederalClassification[0].c1_1[6]"
                          onChange={(e) =>
                            handleFieldChange(
                              "topmostSubform[0].Page1[0].FederalClassification[0].c1_1[6]",
                              e.target.checked
                            )
                          }
                          checked={
                            fieldValues[
                              "topmostSubform[0].Page1[0].FederalClassification[0].c1_1[6]"
                            ]
                          }
                        />
                        &nbsp;
                        <p className="mb-0 d-flex align-items-center">
                          Other (see instructions){" "}
                        </p>
                      </div>
                      <input
                        className="form-control form-control-lg ms-4"
                        id="inline-form-input"
                        type="text"
                        name="topmostSubform[0].Page1[0].FederalClassification[0].f1_4[0]"
                        // value={formValuesAboout?.topmostSubform[0].Page1[0].FederalClassification[0].f1_4[0]}
                        onChange={(e) =>
                          handleFieldChange(
                            "topmostSubform[0].Page1[0].FederalClassification[0].f1_4[0]",
                            e.target.value
                          )
                        }
                      />
                    </div>
                    <div className="col-sm-12 mb-2 d-flex">
                      <input
                        className="form-check-input"
                        type="checkbox"
                        name="topmostSubform[0].Page1[0].FederalClassification[0].c1_1[4]"
                        onChange={(e) =>
                          handleFieldChange(
                            "topmostSubform[0].Page1[0].FederalClassification[0].c1_1[4]",
                            e.target.checked
                          )
                        }
                        checked={
                          fieldValues[
                            "topmostSubform[0].Page1[0].FederalClassification[0].c1_1[4]"
                          ]
                        }
                      />
                      &nbsp;
                      <p className="mb-0">Trust/estate </p>
                    </div>
                  </div>

                  <div className="col-sm-12 mb-2">
                    <label className="form-label" for="pr-fn">
                      4. Exemptions (codes apply only to certain entities, not
                      individuals; see instructions on page 3):
                    </label>
                    <p className="mb-0">Exempt payee code (if any)</p>
                    <input
                      className="form-control form-control-lg"
                      id="inline-form-input"
                      type="text"
                      name="topmostSubform[0].Page1[0].Exemptions[0].f1_5[0]"
                      // value={formValuesAboout?.topmostSubform[0].Page1[0].Exemptions[0].f1_5[0]}
                      onChange={(e) =>
                        handleFieldChange(
                          "topmostSubform[0].Page1[0].Exemptions[0].f1_5[0]",
                          e.target.value
                        )
                      }
                    />
                    <p className="mb-0">
                      Exemption from FATCA reporting code (if any)
                    </p>
                    <input
                      className="form-control form-control-lg"
                      id="inline-form-input"
                      type="text"
                      name="topmostSubform[0].Page1[0].Exemptions[0].f1_6[0]"
                      // value={formValuesAboout?.topmostSubform[0].Page1[0].Exemptions[0].f1_6[0]}
                      onChange={(e) =>
                        handleFieldChange(
                          "topmostSubform[0].Page1[0].Exemptions[0].f1_6[0]",
                          e.target.value
                        )
                      }
                    />
                  </div>

                  <div className="col-sm-12 mb-2">
                    <label className="form-label" for="pr-fn">
                      5. Address
                    </label>
                    {/* <p className="mb-0">(number, street, and apt. or suite no.) See instructions.</p> */}
                    <input
                      className="form-control form-control-lg"
                      id="inline-form-input"
                      type="text"
                      name="topmostSubform[0].Page1[0].Address[0].f1_7[0]"
                      // value={formValuesAboout?.topmostSubform[0].Page1[0].Address[0].f1_7[0]}
                      onChange={(e) =>
                        handleFieldChange(
                          "topmostSubform[0].Page1[0].Address[0].f1_7[0]",
                          e.target.value
                        )
                      }
                    />
                  </div>

                  <div className="col-sm-12 mb-2">
                    <label className="form-label" for="pr-fn">
                      6. City, state, and ZIP code
                    </label>
                    <p className="mb-0"></p>
                    <input
                      className="form-control form-control-lg"
                      id="inline-form-input"
                      type="text"
                      name="topmostSubform[0].Page1[0].Address[0].f1_8[0]"
                      // value={formValuesAboout?.topmostSubform[0].Page1[0].Address[0].f1_8[0]}
                      onChange={(e) =>
                        handleFieldChange(
                          "topmostSubform[0].Page1[0].Address[0].f1_8[0]",
                          e.target.value
                        )
                      }
                    />
                  </div>

                  <div className="col-sm-12 mb-2">
                    <label className="form-label" for="pr-fn">
                      7. List account number(s) here (optional)
                    </label>
                    <input
                      className="form-control form-control-lg"
                      id="inline-form-input"
                      type="text"
                      name="topmostSubform[0].Page1[0].f1_9[0]"
                      // value={formValuesAboout?.topmostSubform[0].Page1[0].f1_9[0]}
                      onChange={(e) =>
                        handleFieldChange(
                          "topmostSubform[0].Page1[0].f1_9[0]",
                          e.target.value
                        )
                      }
                    />
                  </div>

                  <div className="col-sm-12 mb-2">
                    <label className="form-label" for="pr-fn">
                      *. Requester’s name and address (optional)
                    </label>
                    <input
                      className="form-control form-control-lg"
                      id="inline-form-input"
                      type="text"
                      name="topmostSubform[0].Page1[0].f1_10[0]"
                      // value={formValuesAboout?.topmostSubform[0].Page1[0].f1_10[0]}
                      onChange={(e) =>
                        handleFieldChange(
                          "topmostSubform[0].Page1[0].f1_10[0]",
                          e.target.value
                        )
                      }
                    />
                  </div>

                  <div className="col-sm-12 mb-2">
                    <label className="form-label" for="pr-fn">
                      *. Social Security Number
                    </label>
                    <div className="row">
                      <div className="col-md-4">
                        <input
                          className="form-control form-control-lg"
                          id="inline-form-input"
                          type="text"
                          maxlength="3"
                          name="topmostSubform[0].Page1[0].SSN[0].f1_11[0]"
                          // value={formValuesAboout?.topmostSubform[0].Page1[0].SSN[0].f1_11[0]}
                          onChange={(e) =>
                            handleFieldChange(
                              "topmostSubform[0].Page1[0].SSN[0].f1_11[0]",
                              e.target.value
                            )
                          }
                        />
                      </div>
                      <div className="col-md-4">
                        <input
                          className="form-control form-control-lg"
                          id="inline-form-input"
                          type="text"
                          maxlength="2"
                          name="topmostSubform[0].Page1[0].SSN[0].f1_12[0]"
                          // value={formValuesAboout?.topmostSubform[0].Page1[0].SSN[0].f1_12[0]}
                          onChange={(e) =>
                            handleFieldChange(
                              "topmostSubform[0].Page1[0].SSN[0].f1_12[0]",
                              e.target.value
                            )
                          }
                        />
                      </div>
                      <div className="col-md-4">
                        <input
                          className="form-control form-control-lg"
                          id="inline-form-input"
                          type="text"
                          maxlength="4"
                          name="topmostSubform[0].Page1[0].SSN[0].f1_13[0]"
                          // value={formValuesAboout?.topmostSubform[0].Page1[0].SSN[0].f1_13[0]}
                          onChange={(e) =>
                            handleFieldChange(
                              "topmostSubform[0].Page1[0].SSN[0].f1_13[0]",
                              e.target.value
                            )
                          }
                        />
                      </div>
                    </div>
                  </div>
                  <div className="col-sm-12 mb-2">
                    <label className="form-label" for="pr-fn">
                      *. Employer identification number
                    </label>
                    <div className="row">
                      <div className="col-md-6">
                        <input
                          className="form-control form-control-lg"
                          id="inline-form-input"
                          type="text"
                          maxlength="2"
                          name="topmostSubform[0].Page1[0].EmployerID[0].f1_14[0]"
                          // value={formValuesAboout?.topmostSubform[0].Page1[0].EmployerID[0].f1_14[0]}
                          onChange={(e) =>
                            handleFieldChange(
                              "topmostSubform[0].Page1[0].EmployerID[0].f1_14[0]",
                              e.target.value
                            )
                          }
                        />
                      </div>

                      <div className="col-md-6">
                        <input
                          className="form-control form-control-lg"
                          id="inline-form-input"
                          type="text"
                          maxlength="7"
                          name="topmostSubform[0].Page1[0].EmployerID[0].f1_15[0]"
                          // value={formValuesAboout?.topmostSubform[0].Page1[0].EmployerID[0].f1_15[0]}
                          onChange={(e) =>
                            handleFieldChange(
                              "topmostSubform[0].Page1[0].EmployerID[0].f1_15[0]",
                              e.target.value
                            )
                          }
                        />
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div className="col-md-8">
                <div className="onboard_iframe">
                  {
                    blobResponce?
                     <>
                    <iframe className="" src={blobResponce.data} />
                   {console.log(blobResponce)}
                     </>
                    :
                    <iframe className="" src={PDF_SRC} />

                  }
                </div>

                <>
                  {Object.keys(signatures).length > 0 ? (
                    ""
                  ) : (
                    <div className="canvas_signature w-100">
                      <p className="mt-3">
                        <b>Please Sign here </b>
                      </p>
                      <SignatureCanvas
                        ref={signatureCanvasRef}
                        canvasProps={{
                          className: "signature-canvas w-100",
                        }}
                      />
                      <div className="float-left w-100 mt-3">
                        <button
                          className="btn btn-primary btn-sm"
                          onClick={handleClearSignature}
                        >
                          Clear Signature
                        </button>
                        <button
                          className="btn btn-primary btn-sm ms-3"
                          onClick={handleSaveSignature}
                        >
                          Save Signature
                        </button>
                      </div>
                    </div>
                  )}
                </>
              </div>
            </div>
          </div>

          {Object.keys(signatures).length > 0 ? (
            <div className="pull-left mt-3">
              <button
                className="btn btn-primary btn-sm"
                // onClick={handleAddTexterm}
                onClick={handleFillAndGeneratePdf}
                // disabled={isdone}
              >
                {isdone ? "Please wait..." : "Submit"}

                <i className="fi-chevron-right fs-sm ms-2"></i>
              </button>
            </div>
          ) : (
            ""
          )}
        </div>
      </main>
    </div>
  );
};

export default PdfIframe;

export const PDF_SRC =
  "https://brokeragentbase.s3.amazonaws.com/assets/IRS+W9.pdf#view=FitH&zoom=100";

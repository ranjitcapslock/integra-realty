import React, { useState, useEffect } from "react";
import Pic from "../img/pic.png";
import { NavLink, useNavigate, useParams } from "react-router-dom";
import user_service from "../service/user_service";
import axios from "axios";
import Loader from "../../Pages/Loader/Loader.js";
import Toaster from "../../Pages/Toaster/Toaster.js";
import jwt from "jwt-decode";
import Modalresetpassword from "../../Modalresetpassword";
import { MultiSelect } from "react-multi-select-component";
import { MultiSelect as MultiSelect2 } from "react-multi-select-component";
// import BusinessCard from "../ControlPanel/BusinessCard.js";
import ActivateAccount from "../Contact/ActivateAccount";
import ProfileInfo from "../Contact/ProfileInfo.js";
import ContactInfo from "../Contact/ContactInfo.js";
import defaultpropertyimage from "../../Components/img/defaultpropertyimage.jpeg";

import OnboardDocuments from "./OnboardDocuments.js";

import _ from "lodash";
import Swal from "sweetalert2";
import "sweetalert2/dist/sweetalert2.css";
import { Rating } from "react-simple-star-rating";

const socialMediaTypes = [
  { type: "Twitter", linkurl: "" },
  { type: "Linkedin", linkurl: "" },
  { type: "Facebook", linkurl: "" },
  { type: "Instagram", linkurl: "" },
  { type: "Tumblr", linkurl: "" },
  { type: "YouTube Channel", linkurl: "" },
  { type: "Pinterest", linkurl: "" },
  { type: "TikTok", linkurl: "" },
  { type: "Vimeo", linkurl: "" },
];
const ContactProfile = () => {
  const [loader, setLoader] = useState({ isActive: null });
  const { isActive } = loader;
  const [toaster, setToaster] = useState({
    types: null,
    isShow: null,
    toasterBody: "",
    message: "",
  });
  const { types, isShow, toasterBody, message } = toaster;
  const initialValues = {
    email: "",
    image: "",
    phone: "",
    firstName: "",
    lastName: "",
    nickname: "",
    address: "",
    web: "",
    fax: "",
    inbox: "",
    work_phone: "",
    home_phone: "",
    video_email: "",
    printed_name: "",
    spouse_firstname: "",
    spouse_lastname: "",
    spouse_number: "",
    spouse_email: "",
    spouse_birthday: "",
    bussiness_name: "",
    bussiness_address: "",
    sellers: "",
    personal_contacttype: "",
    organization: "",
    homeOffice: "",
    title: "",
    agentId: "",
    profession: "",
    industry: "",
    assistant: "",
    designations: "",
    primary_areas: "",
    languages: "",
    specialties: "",
    marketing_message: "",
    birthday: "",
    gender: "",
    anniversary: "",
    recruiter: "",
    supporters: "",
    connection: "",
    notes: "",
  };

  const [step1, setStep1] = useState("1");
  const params = useParams();
  const [data, setData] = useState("");
  const [dataNew, setDataNew] = useState("");

  const [imageData, setImageData] = useState("");
  const [submitCount, setSubmitCount] = useState(0);
  const [getContact, setGetContact] = useState(initialValues);
  const [agentpapernameAdd, setAgentpapernameAdd] = useState(initialValues);
  const [file, setFile] = useState(null);
  const [fileNew, setFileNew] = useState(null);

  const [fileExtension, setFileExtension] = useState("");
  const [fileExtensionNew, setFileExtensionNew] = useState("");

  const [acceptedFileTypes, setAcceptedFileTypes] = useState([
    "jpg",
    "jpeg",
    "png",
    "svg",
  ]);
  const [showSubmitButton, setShowSubmitButton] = useState(false);
  const [showSubmitButtonNew, setShowSubmitButtonNew] = useState(false);

  const [activeButton, setActiveButton] = useState("1");
  const [board_membership, setBoard_membership] = useState([]);
  const [mls_membership, setMls_membership] = useState([]);
  const [organizationGet, setOrganizationGet] = useState([]);
  const [socialLinks, setSocialLinks] = useState(socialMediaTypes);
  const [languageInput, setLanguageInput] = useState([]);
  const [specialtiesInput, setSpecialtiesInput] = useState([]);

  const [contactDeleted, setContactDeleted] = useState("1");
  const [approveOnbaord, setApproveOnbaord] = useState(false);

  const [formValues, setFormValues] = useState({
    office: "Corporate",
    intranet_id: "",
    nickname: "",
    subscription_level: "Intranet Only",
    admin_note: "",
    sponsor_associate: "",
    staff_recruiter: "",
    nrds_id: "",
    associate_member: "",
    plan_date: "",
    billing_date: "",
    account_name: "Integra Reality",
    invoicing: "Credit Card Auto-Bill",
    licensed_since: "",
    associate_since: "",
    staff_since: "",
    iscorporation: "",
    agentTax_ein: "",
    agent_funding: "",
    ssn_itin: "",
    date_birth: "",
    private_idcard_type: "",
    private_idstate: "",
    private_idnumber: "",
    license_state: "",
    license_type: "Sales Agent",
    license_number: "",
    date_issued: "",
    date_expire: "",
    mls_membership: [],
    board_membership: [],
    agent_id: "",
    activatecontact: "",
  });
  const navigate = useNavigate();
  const [note, setNotes] = useState(false);
  const [activeGet, setActiveGet] = useState([]);

  const [notesdata, setNotesdata] = useState("");
  const [contactListings, setContactListings] = useState("");
  const [commaSeparatedValues, setCommaSeparatedValues] = useState("");
  const [
    commaSeparatedValuesmls_membership,
    setCommaSeparatedValuesmls_membership,
  ] = useState("");

  const ContactGetById = async () => {
    await user_service.contactGetById(params.id).then((response) => {
      if (response) {
        setGetContact(response.data);
        const contactData = response.data;
        if (response.data.passwordHash) {
          setApproveOnbaord(true);
        }
        setSocialLinks(contactData.social_links);
        // console.log(contactData.specialties[0]?.specialties);
        // console.log(contactData.languages[0]?.language);
        setSpecialtiesInput(contactData.specialties);
        setLanguageInput(contactData.languages);

        if (response.data && response.data.additionalActivateFields) {
          setFormValues((prevFormValues) => ({
            ...prevFormValues,
            office:
              response.data.additionalActivateFields.office ||
              prevFormValues.office,
            intranet_id:
              response.data.additionalActivateFields.intranet_id ||
              prevFormValues.intranet_id,
            subscription_level:
              response.data.additionalActivateFields.subscription_level ||
              prevFormValues.subscription_level,
            admin_note:
              response.data.additionalActivateFields.admin_note ||
              prevFormValues.admin_note,
            sponsor_associate:
              response.data.additionalActivateFields.sponsor_associate ||
              prevFormValues.sponsor_associate,
            staff_recruiter:
              response.data.additionalActivateFields.staff_recruiter ||
              prevFormValues.staff_recruiter,
            nrds_id:
              response.data.additionalActivateFields.nrds_id ||
              prevFormValues.nrds_id,
            associate_member:
              response.data.additionalActivateFields.associate_member ||
              prevFormValues.associate_member,
            plan_date:
              response.data.additionalActivateFields.plan_date ||
              prevFormValues.plan_date,
            billing_date:
              response.data.additionalActivateFields.billing_date ||
              prevFormValues.billing_date,
            account_name:
              response.data.additionalActivateFields.account_name ||
              prevFormValues.account_name,
            invoicing:
              response.data.additionalActivateFields.invoicing ||
              prevFormValues.invoicing,
            licensed_since:
              response.data.additionalActivateFields.licensed_since ||
              prevFormValues.licensed_since,
            associate_since:
              response.data.additionalActivateFields.associate_since ||
              prevFormValues.associate_since,
            staff_since:
              response.data.additionalActivateFields.staff_since ||
              prevFormValues.staff_since,
            iscorporation:
              response.data.additionalActivateFields.iscorporation ||
              prevFormValues.iscorporation,
            agentTax_ein:
              response.data.additionalActivateFields.agentTax_ein ||
              prevFormValues.agentTax_ein,
            agent_funding:
              response.data.additionalActivateFields.agent_funding ||
              prevFormValues.agent_funding,
            ssn_itin:
              response.data.additionalActivateFields.ssn_itin ||
              prevFormValues.ssn_itin,
            date_birth:
              response.data.additionalActivateFields.date_birth ||
              prevFormValues.date_birth,
            private_idcard_type:
              response.data.additionalActivateFields.private_idcard_type ||
              prevFormValues.private_idcard_type,
            private_idstate:
              response.data.additionalActivateFields.private_idstate ||
              prevFormValues.private_idstate,
            private_idnumber:
              response.data.additionalActivateFields.private_idnumber ||
              prevFormValues.private_idnumber,
            license_state:
              response.data.additionalActivateFields.license_state ||
              prevFormValues.license_state,
            license_type:
              response.data.additionalActivateFields.license_type ||
              prevFormValues.license_type,
            license_number:
              response.data.additionalActivateFields.license_number ||
              prevFormValues.license_number,
            date_issued:
              response.data.additionalActivateFields.date_issued ||
              prevFormValues.date_issued,
            date_expire:
              response.data.additionalActivateFields.date_expire ||
              prevFormValues.date_expire,
            mls_membership:
              response.data.additionalActivateFields.mls_membership ||
              prevFormValues.mls_membership,
          }));

          // const boardMembershipData = response.data.additionalActivateFields && response.data.additionalActivateFields?.board_membership
          //     ? JSON.parse(response.data.additionalActivateFields.board_membership)
          //     : [];
          //     const boardMembershipValues = boardMembershipData.map(item => item.value);
          //     setCommaSeparatedValues(boardMembershipValues.join(', '))

          // const mls_membershipData = response.data.additionalActivateFields && response.data.additionalActivateFields?.mls_membership
          // ? JSON.parse(response.data.additionalActivateFields.mls_membership)
          // : [];
          // const mls_membershipValues = mls_membershipData.map(item => item.value);
          // setCommaSeparatedValuesmls_membership(mls_membershipValues.join(', '))

          const board_membershipString =
            response.data.additionalActivateFields?.board_membership;
          if (board_membershipString) {
            // Check if board_membershipString is a string
            if (typeof board_membershipString === "string") {
              try {
                // Attempt to parse the string as JSON
                const board_membershipParsed = JSON.parse(
                  board_membershipString
                );

                if (board_membershipParsed == "") {
                  setBoard_membership(board_membership);
                } else {
                  setBoard_membership(board_membershipParsed);
                }
                // Set the parsed value to your state or variable
                if (board_membershipParsed == "") {
                  setCommaSeparatedValues("");
                } else {
                  const boardMembershipValues = board_membershipParsed.map(
                    (item) => item.value
                  );
                  setCommaSeparatedValues(boardMembershipValues.join(", "));
                }
              } catch (error) {
                console.error("Error parsing board_membership as JSON:", error);

                console.error("JSON Parse Error:", error.message);
                setCommaSeparatedValues("");
              }
            } else {
              setCommaSeparatedValues("");
            }
          } else {
            setCommaSeparatedValues("");
          }

          const mls_membershipString =
            response.data.additionalActivateFields?.mls_membership;
          if (mls_membershipString) {
            if (typeof mls_membershipString === "string") {
              try {
                const mls_membershipParsed = JSON.parse(mls_membershipString);

                if (mls_membershipParsed == "") {
                  setMls_membership(mls_membership);
                } else {
                  setMls_membership(mls_membershipParsed);
                }
                if (mls_membershipParsed == "") {
                  setCommaSeparatedValuesmls_membership("");
                } else {
                  const boardMembershipValues = mls_membershipParsed.map(
                    (item) => item.value
                  );
                  setCommaSeparatedValuesmls_membership(
                    boardMembershipValues.join(", ")
                  );
                }
              } catch (error) {
                console.error("Error parsing mls_membership as JSON:", error);
                console.error("JSON Parse Error:", error.message);
                setCommaSeparatedValuesmls_membership("");
              }
            } else {
              setCommaSeparatedValuesmls_membership("");
            }
          } else {
            setCommaSeparatedValuesmls_membership("");
          }
        }
      }
    });
  };

  const propertypic = (e) => {
    e.target.src = defaultpropertyimage;
  };

  const OrganizationTreeGets = async () => {
    // setLoader({ isActive: true });
    if (jwt(localStorage.getItem("auth")).id) {
      const agentId = jwt(localStorage.getItem("auth")).id;
      await user_service.organizationTreeGet(agentId).then((response) => {
        // setLoader({ isActive: false });
        if (response) {
          setOrganizationGet(response.data);
        }
      });
    }
  };

  const TransactionGetAll = async () => {
    try {
      setLoader({ isActive: true });

      let queryParams = `?page=1`;

      if (
        localStorage.getItem("auth") &&
        jwt(localStorage.getItem("auth")).contactType == "admin"
      ) {
        // queryParams += `&agentId=${jwt(localStorage.getItem("auth")).id}`;
      } else {
        queryParams += `&agentId=${jwt(localStorage.getItem("auth")).id}`;
      }

      const response = await user_service.TransactionGet(queryParams);

      setLoader({ isActive: false });

      if (response && response.data) {
        console.log(response);
        setActiveGet(response.data.data);
      }
    } catch (error) {
      console.error("Error in TransactionGetAll:", error);
      // Handle errors as needed
    }
  };

  //  console.log(activeGet[0]?.contact3[0]?.data?.firstName);
  useEffect(() => {
    window.scrollTo(0, 0);
    OrganizationTreeGets();
    ContactGetByIdMLS();
    TransactionGetAll();
  }, []);

  const [w9papernameAdd, setW9papernameAdd] = useState("");
  const [contractorAgreementpapernameAdd, setContractorAgreementpapernameAdd] =
    useState("");

  const [onBoard, setOnBoard] = useState("");
  const [onBoardparkcity, setOnBoardparkcity] = useState(false);

  const AgentpapernameAdd = async () => {
    await user_service.AgentpapernameAdd().then((response) => {
      if (response) {
        setAgentpapernameAdd(response.data);

        const filteredData = response.data.data.find(
          (item) => item.paperwork_title === "W9"
        );
        const filteredDatacontractorAgreement = response.data.data.find(
          (item) => item.paperwork_title === "Independent Contractor Agreement"
        );
        setW9papernameAdd(filteredData);
        setContractorAgreementpapernameAdd(filteredDatacontractorAgreement);
      }
    });
  };

  const ContactGetByIdMLS = async () => {
    await user_service.contactGetById(params.id).then((response) => {
      //if (response) {
      //  setGetContact(response.data);
      //  const contactData = response.data;

      // await user_service
      //   .ContactGetByuniqueid(params.uid, params.cid)
      //   .then((response) => {
      if (response && response.data) {
        // console.log(response.data);
        // setFormValuesNew(response.data.data);
        // const contactId = response.data.data._id;
        // ContactGetById(contactId);
        AgentpapernameAdd();
        const contactAgentId = response.data.agentId;

        //  setAgentId(contactAgentId);
        //  OrganizationTreeGets(contactAgentId);

        const mlsMembership =
          response.data.additionalActivateFields?.mls_membership;

        let MlsMembershipValues = []; // Declare the variable here

        if (mlsMembership) {
          // Check if mlsMembership is a string
          if (typeof mlsMembership === "string") {
            try {
              // Attempt to parse the string as JSON
              const mls_membershipParsed = JSON.parse(mlsMembership);

              if (mls_membershipParsed == "") {
                // console.log(mls_membership);
                // setAgentOnBoardMls(mls_membership);
              } else {
                // console.log(mls_membershipParsed);
                // setAgentOnBoardMls(mls_membershipParsed);

                MlsMembershipValues = mls_membershipParsed.map(
                  (item) => item.value
                );
                // console.log(MlsMembershipValues);
                // setCommaSeparatedValues(MlsMembershipValues.join(", "));
              }
            } catch (error) {
              console.error("Error parsing board_membershipString:", error);
            }
          } else {
            console.error("board_membershipString is not a string.");
          }
        }

        const boardMembership =
          response.data.additionalActivateFields?.board_membership;

        let boardMembershipValues = []; // Declare the variable here

        if (boardMembership) {
          // Check if boardMembership is a string
          if (typeof boardMembership === "string") {
            try {
              // Attempt to parse the string as JSON
              const board_membershipParsed = JSON.parse(boardMembership);

              if (board_membershipParsed == "") {
                // console.log(board_membership);
                // setAgentOnBoardboard(board_membership);
              } else {
                //console.log(board_membershipParsed);
                //  setAgentOnBoardboard(board_membershipParsed);

                boardMembershipValues = board_membershipParsed.map(
                  (item) => item.value
                );
                // console.log(boardMembershipValues);
                // setCommaSeparatedValues(boardMembershipValues.join(", "));
              }
              // Rest of your code...
            } catch (error) {
              console.error("Error parsing boardMembership:", error);
            }
          } else {
            console.error("boardMembership is not a string.");
          }
        }
        MlsBoardDocumentGetAll(MlsMembershipValues, boardMembershipValues);
      }
    });
  };

  const MlsBoardDocumentGetAll = async (
    MlsMembershipValues,
    boardMembershipValues
  ) => {
    // console.log(MlsMembershipValues)
    // console.log(boardMembershipValues)
    if (
      MlsMembershipValues[0] === "Park City" ||
      boardMembershipValues[0] === "Park City"
    ) {
      //setOnBoardparkcity(true);
    } else {
      await user_service
        .mlsBoardDocumentGetId(MlsMembershipValues, boardMembershipValues)
        .then((response) => {
          // setLoader({ isActive: false });
          if (response) {
            if (response.data.data[0]?.image) {
              //generateHtmlForm1("https://brokeragentbase.s3.amazonaws.com/1702881544800-1700758888303-Utah%20Real%20Estate%20Membership%20Change%20Form%20%282%29.pdf");
              //generateHtmlForm1("https://brokeragentbase.s3.amazonaws.com/assets/Salt+Lake+Board+Membership+Form.pdf");
            } else {
              //readyPad();
            }
            setOnBoard(response.data.data);
            // console.log("setOnBoard", response.data.data)
          }
        });
    }
  };

  const OfficeNoteGet = async () => {
    const agentId = params.id;
    await user_service.officeNoteGet(agentId).then((response) => {
      if (response) {
        setNotesdata(response.data);
      }
    });
  };

  const getContactListings = async () => {
    const agentId = params.id;
    await user_service.getContactListings(agentId, "count").then((response) => {
      if (response) {
        setContactListings(response.data);
      }
    });
  };

  const handleRemove = async (id) => {
    setLoader({ isActive: true });
    await user_service.officeDelete(id).then((response) => {
      if (response) {
        setLoader({ isActive: false });
        const OfficeNoteGet = async () => {
          await user_service.officeNoteGet().then((response) => {
            if (response) {
              setNotesdata(response.data);
            }
          });
        };
        OfficeNoteGet();
      }
    });
  };

  useEffect(() => {
    window.scrollTo(0, 0);
    ContactGetById(params.id);
    AgentpapernameAdd();
    OfficeNoteGet();
  }, []);

  const handleFileUpload = async (e) => {
    const selectedFile = e.target.files[0];

    if (selectedFile) {
      const fileExtension = selectedFile.name.split(".").pop().toLowerCase();

      if (!acceptedFileTypes.includes(fileExtension)) {
        alert(`Please upload a valid image file: ${acceptedFileTypes.join(", ")}`);
        return;
      }

      setFile(selectedFile);
      setFileExtension(fileExtension);
      setShowSubmitButton(true);

      const formData = new FormData();
      formData.append("file", selectedFile);
      const config = {
        headers: {
          Accept: "application/json",
          "Content-Type": "multipart/form-data",
          Authorization: `Bearer ${localStorage.getItem("auth")}`,
        },
      };

      try {
        const uploadResponse = await axios.post(
          "https://api.brokeragentbase.com/upload",
          formData,
          config
        );
        const uploadedFileData = uploadResponse.data;
        setData(uploadedFileData);
        const updatedgetContact = {
          ...getContact,
          image: uploadedFileData,
        };

        setLoader({ isActive: true });
        await user_service
          .contactUpdate(params.id, updatedgetContact)
          .then((response) => {
            
            if (response) {
              setLoader({ isActive: false });
              setImageData(response.data);
            
              ContactGetById(params.id);
            } else {
              setLoader({ isActive: false });
              setToaster({
                type: "error",
                isShow: true,
                // toasterBody: response.data.message,
                message: "Error",
              });

              setTimeout(() => {
                setToaster((prevToaster) => ({
                  ...prevToaster,
                  isShow: false,
                }));
              }, 3000);
            }
          });
      } catch (error) {
        setLoader({ isActive: false });
        setToaster({
          types: "error",
          isShow: true,
          message: "Error",
        });

        setTimeout(() => {
          setToaster((prevToaster) => ({ ...prevToaster, isShow: false }));
        }, 1000);
      }
    }
  };

  const handleChangeSocial = (event, index) => {
    const { value } = event.target;
    setSocialLinks((prevSocialLinks) => {
      const updatedSocialLinks = [...prevSocialLinks];
      updatedSocialLinks[index] = {
        ...updatedSocialLinks[index],
        linkurl: value,
      };
      return updatedSocialLinks;
    });
  };

  const handleChange = (e) => {
    const { name, value } = e.target;
    setGetContact({ ...getContact, [name]: value });
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
    try {
      const userData = {
        agentId: jwt(localStorage.getItem("auth")).id,
        firstName: getContact.firstName,
        lastName: getContact.lastName,
        nickname: getContact.nickname
          ? getContact.nickname
          : getContact.lastName + "" + getContact.firstName,
        email: getContact.email,
        phone: getContact.phone,
        contactType: getContact.contactType,
        address: getContact.address,
        city: getContact.city,
        state: getContact.state,
        zip: getContact.zip,

        web: getContact.web,
        fax: getContact.fax,
        work_phone: getContact.work_phone,
        home_phone: getContact.home_phone,
        video_email: getContact.video_email,
        printed_name: getContact.printed_name,
        spouse_firstname: getContact.spouse_firstname,
        spouse_lastname: getContact.spouse_lastname,
        spouse_number: getContact.spouse_number,
        spouse_email: getContact.spouse_email,
        spouse_birthday: getContact.spouse_birthday,
        bussiness_name: getContact.bussiness_name,
        bussiness_address: getContact.bussiness_address,
        sellers: getContact.sellers,
        personal_contacttype: getContact.personal_contacttype,
        inbox: getContact.inbox,
        organization: getContact.organization,
        homeOffice: getContact.homeOffice,
        title: getContact.title,
        department: getContact.department,
        nickname: getContact.nickname,
        profession: getContact.profession,
        industry: getContact.industry,
        assistant: getContact.assistant,
        designations: getContact.designations,
        primary_areas: getContact.primary_areas,
        languages: getContact.languages,
        specialties: getContact.specialties,
        social_links: socialLinks,
        marketing_message: getContact.marketing_message,
        birthday: getContact.birthday,
        gender: getContact.gender,
        anniversary: getContact.anniversary,
        recruiter: getContact.recruiter,
        Supporters: getContact.Supporters,
        connection: getContact.connection,
        note: getContact.note,
      };
      setLoader({ isActive: true });
      await user_service.contactUpdate(params.id, userData).then((response) => {
        if (response) {
          setGetContact(response.data);
          setLoader({ isActive: false });
          ContactGetById(params.id);
          setToaster({
            type: "Profile Updated",
            isShow: true,
            toasterBody: response.data.message,
            message: "Profile Updated Successfully",
          });
          setTimeout(() => {
            setToaster((prevToaster) => ({ ...prevToaster, isShow: false }));
          }, 3000);
        } else {
          setLoader({ isActive: false });
          setToaster({
            types: "error",
            isShow: true,
            toasterBody: response.data.message,
            message: "Error",
          });
        }
      });
    } catch (error) {
      setLoader({ isActive: false });
      setToaster({
        types: "error",
        isShow: true,
        toasterBody: error.response
          ? error.response.data.message
          : error.message,
        message: "Error",
      });
      setTimeout(() => {
        setToaster((prevToaster) => ({ ...prevToaster, isShow: false }));
      }, 3000);
    }
  };

  const handleChanges = (e) => {
    const { name, value } = e.target;
    setFormValues({ ...formValues, [name]: value });
  };

  const handleAccount = async (e) => {
    e.preventDefault();

    const additionalActivateFields = {
      office: formValues.office,
      intranet_id: formValues.intranet_id,
      subscription_level: formValues.subscription_level,
      admin_note: formValues.admin_note,
      sponsor_associate: formValues.sponsor_associate,
      staff_recruiter: formValues.staff_recruiter,
      nrds_id: formValues.nrds_id,
      associate_member: formValues.associate_member,
      plan_date: formValues.plan_date,
      billing_date: formValues.billing_date,
      account_name: formValues.account_name,
      invoicing: formValues.invoicing,
      licensed_since: formValues.licensed_since,
      associate_since: formValues.associate_since,
      staff_since: formValues.staff_since,
      iscorporation: "",
      agentTax_ein: formValues.agentTax_ein,
      agent_funding: formValues.agent_funding,
      ssn_itin: formValues.ssn_itin,
      date_birth: formValues.date_birth,
      private_idcard_type: formValues.private_idcard_type,
      private_idstate: formValues.private_idstate,
      private_idnumber: formValues.private_idnumber,
      license_state: formValues.license_state,
      license_type: formValues.license_type,
      license_number: formValues.license_number,
      date_issued: formValues.date_issued,
      date_expire: formValues.date_expire,
      mls_membership: JSON.stringify(mls_membership),
      board_membership: JSON.stringify(board_membership),
      agent_id: formValues.agent_id,
      activatecontact: "yes",
    };

    const userData = {
      agentId: jwt(localStorage.getItem("auth")).id,
      birthday: formValues.date_birth,
      additionalActivateFields,
    };

    try {
      setLoader({ isActive: true });
      await user_service.contactUpdate(params.id, userData).then((response) => {
        if (response) {
          setFormValues(response.data);
          setLoader({ isActive: false });
          ContactGetById(params.id);
          setToaster({
            type: "Profile Updated",
            isShow: true,
            toasterBody: response.data.message,
            message: "Profile Updated Successfully",
          });
          setTimeout(() => {
            setToaster((prevToaster) => ({ ...prevToaster, isShow: false }));
          }, 3000);
        } else {
          setLoader({ isActive: false });
          setToaster({
            types: "error",
            isShow: true,
            toasterBody: response.data.message,
            message: "Error",
          });
        }
      });
    } catch (error) {
      setLoader({ isActive: false });
      setToaster({
        types: "error",
        isShow: true,
        toasterBody: error.response
          ? error.response.data.message
          : error.message,
        message: "Error",
      });
      setTimeout(() => {
        setToaster((prevToaster) => ({ ...prevToaster, isShow: false }));
      }, 3000);
    }
  };

  const handleBack = () => {
    navigate(`/activate-account/${params.id}`);
  };

  const handleDeleteContact = async () => {
    if (params.id) {
      const userData = {
        contact_status:
          getContact.contact_status === "active" ? "deleted" : "active",
        reason: getContact.contact_status === "active" ? getContact.reason : "",
      };
      setLoader({ isActive: true });
      await user_service.contactUpdate(params.id, userData).then((response) => {
        if (response) {
          setLoader({ isActive: false });
          // setContactDeleted(response.data);
          ContactGetById();
        } else {
          setLoader({ isActive: false });
          setToaster({
            type: "error",
            isShow: true,
            toasterBody: response.data.message,
            message: "Error",
          });

          setTimeout(() => {
            setToaster((prevToaster) => ({
              ...prevToaster,
              isShow: false,
            }));
          }, 3000);
        }
      });
    }
  };

  const handlegetContact = (stepNo) => {

    
    if (stepNo === "201") {
      setStep1("201");
      setNotes(true);
      setActiveButton("201");
    }

    if (stepNo === "1") {
      setStep1(stepNo);
      setActiveButton(stepNo);
    }

    if (stepNo === "2") {
      setStep1(stepNo);
      setNotes(false);
      setActiveButton(stepNo);
    }
    if (stepNo === "3") {
      setStep1(stepNo);
      setActiveButton(stepNo);
    }
    if (stepNo === "4") {
      setStep1(stepNo);
      setActiveButton(stepNo);
      getContactListings();
    }
    if (stepNo === "5") {
      setStep1(stepNo);
      setActiveButton(stepNo);
    }

    // if (stepNo === "6") {
    //   setStep1(stepNo);
    // }

    if (stepNo === "7") {
      setStep1(stepNo);
      setActiveButton(stepNo);
    }

    if (stepNo === "8") {
      setStep1(stepNo);
      setActiveButton(stepNo);
    }

    if (stepNo === "9") {
      setStep1(stepNo);
      setActiveButton(stepNo);
    }
    if (stepNo === "10") {
      setStep1(stepNo);
      setActiveButton(stepNo);
    }

    if (stepNo === "11") {
      setStep1(stepNo);
      setActiveButton(stepNo);
    }
    if (stepNo === "12") {
      setStep1(stepNo);
      setActiveButton(stepNo);
    }
  };

  const [selectedPaperworkId, setSelectedPaperworkId] = useState("");
  const [selectedFilee, setSelectedFilee] = useState(null);

  const handlePaperworkIdChange = (event) => {
    const selectedValue = event.target.value;
    setSelectedPaperworkId(selectedValue);
  };

  const onFileChange = (event) => {
    const selectedFilee = event.target.files[0];
    setSelectedFilee(selectedFilee);
  };

  const handleFileUploadpaperbook = async (e) => {
    if (!selectedFilee) {
      // alert("Please select a file to upload.");
      setToaster({
        type: "error",
        isShow: true,
        toasterBody: "Please select a file to upload.",
        message: "Error",
      });
      setTimeout(() => {
        setToaster((prevToaster) => ({ ...prevToaster, isShow: false }));
      }, 500);
      return;
    }

    // Check if the selected file is a PDF
    const fileExtension = selectedFilee.name.split(".").pop();
    if (fileExtension !== "pdf") {
      // alert("Please upload only PDF files.");
      setToaster({
        type: "error",
        isShow: true,
        toasterBody: "Please upload only PDF files.",
        message: "Error",
      });
      setTimeout(() => {
        setToaster((prevToaster) => ({ ...prevToaster, isShow: false }));
      }, 500);
      return;
    }

    const formData = new FormData();
    formData.append("file", selectedFilee);
    const config = {
      headers: {
        Accept: "application/json",
        "Content-Type": "multipart/form-data",
        Authorization: `Bearer ${localStorage.getItem("auth")}`,
      },
    };

    try {
      setLoader({ isActive: true });
      const uploadResponse = await axios.post(
        "https://api.brokeragentbase.com/upload",
        formData,
        config
      );
      const uploadedFileDatapaperwork = uploadResponse.data;
      // setData(uploadedFileDatapaperwork);

      const userData = {
        agentId: params.id,
        paperwork_id: selectedPaperworkId,
        documenturl: uploadedFileDatapaperwork,
      };

      setLoader({ isActive: true });
      await user_service.agentpaperworkDocument(userData).then((response) => {
        if (response) {
          setImageData(response.data);
          ContactGetById(params.id);
          setLoader({ isActive: false });
          setSelectedFilee(null);
          setSelectedPaperworkId("");
          document.getElementById("closeModalpaperwork").click();
          setToaster({
            type: "Profile Updated Successfully",
            isShow: true,
            toasterBody: response.data.message,
            message: "Profile Updated Successfully",
          });
          setTimeout(() => {
            setToaster((prevToaster) => ({ ...prevToaster, isShow: false }));
          }, 2000);
        } else {
          setLoader({ isActive: false });
          setToaster({
            type: "error",
            isShow: true,
            toasterBody: response.data.message,
            message: "Error",
          });

          setTimeout(() => {
            setToaster((prevToaster) => ({ ...prevToaster, isShow: false }));
          }, 3000);
        }
      });
    } catch (error) {
      setLoader({ isActive: false });
      setToaster({
        types: "error",
        isShow: true,
        toasterBody: error.response
          ? error.response.data.message
          : error.message,
        message: "Error",
      });

      setTimeout(() => {
        setToaster((prevToaster) => ({ ...prevToaster, isShow: false }));
      }, 2000);
    }
  };

  const [paperworkData, setPaperworkData] = useState("");
  const [selectedFilePaper, setSelectedFilePaper] = useState(null);

  // const ContactPaperwork = async () => {
  //   await user_service
  //     .agentpaperworkDocumentgetId(params.id,"additional_documents")
  //     .then((response) => {
  //       setLoader({ isActive: false });
  //       if (response) {
  //         console.log(response.data);
  //         setPaperworkData(response.data.data);
  //       }
  //     });
  // };
  // useEffect(() => {
  //   if (agentpapernameAdd) {
  //     ContactPaperwork(params.id);
  //   }
  // }, []);

  const handleChangePaperwork = (e) => {
    const { name, value } = e.target;
    setPaperworkData({ ...paperworkData, [name]: value });
  };

  const onFileChangePaperwork = (event) => {
    const selectedFile = event.target.files[0];
    setSelectedFilePaper(selectedFile);
  };

  const handleFilePaperwork = async (e) => {
    if (!selectedFilePaper) {
      // alert("Please select a file to upload.");
      setToaster({
        type: "error",
        isShow: true,
        toasterBody: "Please select a file to upload.",
        message: "Error",
      });
      setTimeout(() => {
        setToaster((prevToaster) => ({ ...prevToaster, isShow: false }));
      }, 500);
      return;
    }

    // Check if the selected file is a PDF
    const fileExtension = selectedFilePaper.name.split(".").pop();
    if (fileExtension !== "pdf") {
      // alert("Please upload only PDF files.");
      setToaster({
        type: "error",
        isShow: true,
        toasterBody: "Please upload only PDF files.",
        message: "Error",
      });
      setTimeout(() => {
        setToaster((prevToaster) => ({ ...prevToaster, isShow: false }));
      }, 500);
      return;
    }

    const formData = new FormData();
    formData.append("file", selectedFilePaper);
    const config = {
      headers: {
        Accept: "application/json",
        "Content-Type": "multipart/form-data",
        Authorization: `Bearer ${localStorage.getItem("auth")}`,
      },
    };

    try {
      // setLoader({ isActive: true });
      const uploadResponse = await axios.post(
        "https://api.brokeragentbase.com/upload",
        formData,
        config
      );
      const uploadedFileDatapaperwork = uploadResponse.data;
      // setData(uploadedFileDatapaperwork);

      const userData = {
        agentId: params.id,
        // additional_agentId:  params.id,
        documentType: "additional_documents",
        documentName: paperworkData.documentName,
        documenturl: uploadedFileDatapaperwork,
      };

      console.log(userData);

      setLoader({ isActive: true });
      await user_service.agentpaperworkDocument(userData).then((response) => {
        if (response) {
          setImageData(response.data);
          // ContactPaperwork(params.id)
          ContactGetById(params.id);
          setLoader({ isActive: false });
          setSelectedFilePaper(null);
          setSelectedPaperworkId("");
          document.getElementById("closeAdditionalpaperwork").click();
          setToaster({
            type: "Profile Updated Successfully",
            isShow: true,
            toasterBody: response.data.message,
            message: "Profile Updated Successfully",
          });
          setTimeout(() => {
            setToaster((prevToaster) => ({ ...prevToaster, isShow: false }));
          }, 2000);
        } else {
          setLoader({ isActive: false });
          setToaster({
            type: "error",
            isShow: true,
            toasterBody: response.data.message,
            message: "Error",
          });

          setTimeout(() => {
            setToaster((prevToaster) => ({ ...prevToaster, isShow: false }));
          }, 3000);
        }
      });
    } catch (error) {
      setLoader({ isActive: false });
      setToaster({
        types: "error",
        isShow: true,
        toasterBody: error.response
          ? error.response.data.message
          : error.message,
        message: "Error",
      });

      setTimeout(() => {
        setToaster((prevToaster) => ({ ...prevToaster, isShow: false }));
      }, 2000);
    }
  };

  const getTimeDifference = (createdDate) => {
    const currentDate = new Date();
    const diffInMilliseconds = currentDate - new Date(createdDate);
    const diffInMinutes = Math.floor(diffInMilliseconds / 60000);
    const hours = Math.floor(diffInMinutes / 60);
    const days = Math.floor(hours / 24);
    const months = Math.floor(days / 30);
    const years = Math.floor(months / 12);

    if (years > 0) {
      return `${years} year${years > 1 ? "s" : ""} ago`;
    } else if (months > 0) {
      return `${months} month${months > 1 ? "s" : ""} ago`;
    } else if (days > 0) {
      return `${days} day${days > 1 ? "s" : ""} ago`;
    } else if (hours > 24) {
      const remainingHours = hours - Math.floor(hours / 24) * 24;
      return `1 day, ${remainingHours} hr${remainingHours > 1 ? "s" : ""} ago`;
    } else {
      return `${hours} hr${hours > 1 ? "s" : ""}, ${diffInMinutes % 60} min${
        diffInMinutes % 60 > 1 ? "s" : ""
      } ago`;
    }
  };

  const handleResetSuccess = () => {
    ContactGetById(params.id);
    AgentpapernameAdd();
  };

  const [viewmore, setViewmore] = useState(false);
  const Viewmorefunction = (e) => {
    setViewmore(!viewmore);
  };

  const initialValuesAbout = {
    full_name: "",
    birthday: "",
    spouse: "",
    spend_birthday: "",
    shirt_size: "",
    spend_day: "",
    professional_skill: "",
    hobbies: "",
    vacation: "",
    travel_bucket: "",
    food: "",
    restaurant: "",
    music: "",
    book: "",
    phone_app: "",
    dessert: "",
    anything_else: "",
  };
  // const [data, setData] = useState("")
  const [formValuesAboout, setFormValuesAbout] = useState(initialValuesAbout);
  const [formErrors, setFormErrors] = useState({});
  const [isSubmitClick, setISSubmitClick] = useState(false);

  const handleChangesAbout = (e) => {
    const { name, value } = e.target;
    setFormValuesAbout({ ...formValuesAboout, [name]: value });
  };

  const handleRadioChange = (event) => {
    const select = event.target.value;
    setFormValuesAbout({
      ...formValuesAboout,
      shirt_size: select,
    });
  };

  const rawDate = formValuesAboout?.birthday;
  const date = new Date(rawDate);
  const formattedDatesAbout = date.toLocaleDateString();
  if (formattedDatesAbout !== "Invalid Date") {
  }

  {
    /* <!-- Form Validation Start--> */
  }

  const handleSubmitAbout = async (e) => {
    if (formValuesAboout && formValuesAboout._id) {
      e.preventDefault();
      try {
        const userData = {
          agentId: params.id,
          full_name: formValuesAboout.full_name,
          birthday: formValuesAboout.birthday,
          spouse: formValuesAboout.spouse,
          spend_birthday: formValuesAboout.spend_birthday,
          shirt_size: formValuesAboout.shirt_size,
          spend_day: formValuesAboout.spend_day,
          professional_skill: formValuesAboout.professional_skill,

          hobbies: formValuesAboout.hobbies,
          vacation: formValuesAboout.vacation,
          travel_bucket: formValuesAboout.travel_bucket,
          food: formValuesAboout.food,
          restaurant: formValuesAboout.restaurant,
          music: formValuesAboout.music,
          book: formValuesAboout.book,
          phone_app: formValuesAboout.phone_app,
          dessert: formValuesAboout.dessert,
          anything_else: formValuesAboout.anything_else,
        };
        setLoader({ isActive: true });
        await user_service
          .aboutMeUpdate(formValuesAboout._id, userData)
          .then((response) => {
            if (response) {
              setFormValuesAbout(response.data);
              setLoader({ isActive: false });
              setToaster({
                type: "Profile Successfully",
                isShow: true,
                toasterBody: response.data.message,
                message: "Profile Updated Successfully",
              });
              setTimeout(() => {
                setToaster((prevToaster) => ({
                  ...prevToaster,
                  isShow: false,
                }));
              }, 2000);
            } else {
              setLoader({ isActive: false });
              setToaster({
                types: "error",
                isShow: true,
                toasterBody: response.data.message,
                message: "Error",
              });
              setTimeout(() => {
                setToaster((prevToaster) => ({
                  ...prevToaster,
                  isShow: false,
                }));
              }, 2000);
            }
          });
      } catch (error) {
        setLoader({ isActive: false });
        setToaster({
          types: "error",
          isShow: true,
          toasterBody: error.response
            ? error.response.data.message
            : error.message,
          message: "Error",
        });

        setTimeout(() => {
          setToaster((prevToaster) => ({ ...prevToaster, isShow: false }));
        }, 2000);
      }
    } else {
      e.preventDefault();
      try {
        const userData = {
          agentId: params.id,
          full_name: formValuesAboout.full_name,
          birthday: formValuesAboout.birthday,
          spouse: formValuesAboout.spouse,
          spend_birthday: formValuesAboout.spend_birthday,
          shirt_size: formValuesAboout.shirt_size,
          spend_day: formValuesAboout.spend_day,
          professional_skill: formValuesAboout.professional_skill,
          hobbies: formValuesAboout.hobbies,
          vacation: formValuesAboout.vacation,
          travel_bucket: formValuesAboout.travel_bucket,
          food: formValuesAboout.food,
          restaurant: formValuesAboout.restaurant,
          music: formValuesAboout.music,
          book: formValuesAboout.book,
          phone_app: formValuesAboout.phone_app,
          dessert: formValuesAboout.dessert,
          anything_else: formValuesAboout.anything_else,
        };
        setLoader({ isActive: true });
        await user_service.aboutMePost(userData).then((response) => {
          if (response) {
            // setFormValues(response.data);
            setLoader({ isActive: false });
            setToaster({
              type: "Profile Updated Successfully",
              isShow: true,
              toasterBody: response.data.message,
              message: "Profile Updated Successfully",
            });
            setTimeout(() => {
              setToaster((prevToaster) => ({ ...prevToaster, isShow: false }));
            }, 1000);
          } else {
            setLoader({ isActive: false });
            setToaster({
              types: "error",
              isShow: true,
              toasterBody: response.data.message,
              message: "Error",
            });
            setTimeout(() => {
              setToaster((prevToaster) => ({ ...prevToaster, isShow: false }));
            }, 1000);
          }
        });
      } catch (error) {
        setLoader({ isActive: false });
        setToaster({
          types: "error",
          isShow: true,
          toasterBody: error.response
            ? error.response.data.message
            : error.message,
          message: "Error",
        });

        setTimeout(() => {
          setToaster((prevToaster) => ({ ...prevToaster, isShow: false }));
        }, 1000);
      }
    }
  };
  const AboutMeget = async () => {
    const agentId = params.id;
    setLoader({ isActive: true });
    await user_service.aboutMeGet(agentId).then((response) => {
      if (response) {
        setLoader({ isActive: false });
        setFormValuesAbout(response.data.data[0]);
      }
    });
  };

  useEffect(() => {
    window.scrollTo(0, 0);
    ContactGetById();
    AboutMeget();
    TestimonialsGetAll();
  }, []);

  function getFormattedDate(dateString) {
    const options = { weekday: "long", month: "long", day: "numeric" };
    const date = new Date(dateString);
    return date.toLocaleDateString("en-US", options);
  }

  // function getFormattedDateFormat(dateString) {
  //   const options = { year: "numeric", month: "2-digit", day: "2-digit" };
  //   const date = new Date(dateString);
  //   return date.toLocaleDateString("en-US", options);
  // }

  function getFormattedDateFormat(dateString) {
    const date = new Date(dateString);

    // Check if the date is valid
    if (isNaN(date.getTime())) {
      return ""; // or any default value you prefer
    }

    // Extract day, month, and year components
    const day = date.getDate().toString().padStart(2, "0");
    const month = (date.getMonth() + 1).toString().padStart(2, "0");
    const year = date.getFullYear();

    // Format the date as "DD/MM/YYYY"
    return `${day}/${month}/${year}`;
  }

  const handleNavigate = (id = "") => {
    if (id) {
      setActiveButton(id);

      navigate(`/contact/${id}`);
    } else {
      navigate(`/contact/${params.id}`);
    }
  };

  const handleApprovePaperwork = async (pid, pstatus) => {
    try {
      const userData = {
        approvalStatus: pstatus,
      };

      setLoader({ isActive: true });
      await user_service
        .agentpaperworkDocumentUpdate(pid, userData)
        .then((response) => {
          if (response) {
            ContactGetById(params.id);

            setLoader({ isActive: false });

            setToaster({
              type: "Document Updated Successfully",
              isShow: true,
              toasterBody: response.data.message,
              message: "Document Updated Successfully",
            });
            setTimeout(() => {
              setToaster((prevToaster) => ({ ...prevToaster, isShow: false }));
            }, 3000);
          } else {
            setLoader({ isActive: false });
            setToaster({
              type: "error",
              isShow: true,
              toasterBody: response.data.message,
              message: "Error",
            });

            setTimeout(() => {
              setToaster((prevToaster) => ({ ...prevToaster, isShow: false }));
            }, 3000);
          }
        });
    } catch (error) {
      setLoader({ isActive: false });
      setToaster({
        types: "error",
        isShow: true,
        toasterBody: error.response
          ? error.response.data.message
          : error.message,
        message: "Error",
      });

      setTimeout(() => {
        setToaster((prevToaster) => ({ ...prevToaster, isShow: false }));
      }, 2000);
    }
  };

  const handlesetuppassword = async (e) => {
    e.preventDefault();

    const userData = {
      _id: params.id,
      agentId: jwt(localStorage.getItem("auth")).id,
    };
    //console.log("userData", userData)
    try {
      setLoader({ isActive: true });
      const response = await user_service.onboardlink(userData);
      if (response) {
        // setClientId(response.data._id);
        // setSummary(response.data);
        // setResults(response.data);
        setLoader({ isActive: false });
        //setFormValues(initialValues);
        setToaster({
          type: "Onboarding Link Sent Successfully",
          isShow: true,
          toasterBody: response.data.message,
          message: "Onboarding Link Sent Successfully",
        });
        setTimeout(() => {
          setToaster((prevToaster) => ({ ...prevToaster, isShow: false }));
          //  navigate(`/onboarding/${response.data.intranet_confirmationcode}/${response.data.intranet_uniqueid}`);
        }, 2000);
      } else {
        setLoader({ isActive: false });
        setToaster({
          types: "error",
          isShow: true,
          toasterBody: response.data.message,
          message: "Error",
        });
      }
    } catch (error) {
      setLoader({ isActive: false });
      setToaster({
        types: "error",
        isShow: true,
        toasterBody: error,
        message: "Error",
      });
    }
    setTimeout(() => {
      setToaster({
        types: "error",
        isShow: false,
        toasterBody: null,
        message: "Error",
      });
    }, 2000);
  };

  const handlesetlegacy = async () => {
    if (params.id) {
      const userData = {
        legacy_account: getContact.legacy_account === "yes" ? "no" : "yes",
      };
      setLoader({ isActive: true });
      await user_service.contactUpdate(params.id, userData).then((response) => {
        if (response) {
          setLoader({ isActive: false });
          // setContactDeleted(response.data);
          ContactGetById();
        } else {
          setLoader({ isActive: false });
          setToaster({
            type: "error",
            isShow: true,
            toasterBody: response.data.message,
            message: "Error",
          });

          setTimeout(() => {
            setToaster((prevToaster) => ({
              ...prevToaster,
              isShow: false,
            }));
          }, 3000);
        }
      });
    }
  };

  const handleApproveonboard = async () => {
    if (params.id) {
      Swal.fire({
        title: "Are you sure?",
        text: "You want to submit this user onboard, without filling documents.",
        icon: "warning",
        showCancelButton: true,
        confirmButtonColor: "#3085d6",
        cancelButtonColor: "#d33",
        confirmButtonText: "Yes, On-Baord it!",
      }).then(async (result) => {
        if (result.isConfirmed) {
          try {
            const userData = {
              contact_status: "active",
              onboard_approve: "yes",
              // onboard_signature: response.data,
              onboardfinal: "done",
              // onboard_taxcertification: uploadResponse.data,
            };
            const response = await user_service.contactUpdate(
              params.id,
              userData
            );
            if (response) {
              setLoader({ isActive: false });
              if (response) {
                Swal.fire("On-Board Done!", "Agent is On Boarded .", "success");
                //setFormValues(response.data);
              }
              ContactGetById();
            } else {
              Swal.fire("Error", response.data.message, "error");
            }
          } catch (error) {
            Swal.fire("Error", "An error occurred.", "error");
            console.error(error);
          }
        }
      });

      // contact_status : "active"
      // onboard_approve :  "yes"
      // onboard_signature: response.data,
      // onboardfinal :  "done"
      // onboard_taxcertification: uploadResponse.data

      // const userData = {
      //     contact_status : "active",
      //     onboard_approve :  "yes",
      //     // onboard_signature: response.data,
      //     onboardfinal :  "done",
      //     // onboard_taxcertification: uploadResponse.data,
      // };
      // setLoader({ isActive: true });
      // await user_service.contactUpdate(params.id, userData).then((response) => {
      //   if (response) {
      //     setLoader({ isActive: false });
      //     // setContactDeleted(response.data);
      //     ContactGetById();
      //   } else {
      //     setLoader({ isActive: false });
      //     setToaster({
      //       type: "error",
      //       isShow: true,
      //       toasterBody: response.data.message,
      //       message: "Error",
      //     });

      //     setTimeout(() => {
      //       setToaster((prevToaster) => ({
      //         ...prevToaster,
      //         isShow: false,
      //       }));
      //     }, 3000);
      //   }
      // });
    }
  };

  const [contactTestimonials, setContactTestimonials] = useState("");
  const [contactTestimonialsUser, setContactTestimonialsUser] = useState("");

  const handleTestimonialAgent = async (id) => {
    await user_service.TestimonialsGetId(id).then((response) => {
      if (response) {
        setLoader({ isActive: false });
        setContactTestimonialsUser(response.data);
      }
    });
  };

  const TestimonialsGetAll = async () => {
    const clientId = params.id;
    if (clientId) {
      setLoader({ isActive: true });
      await user_service.TestimonialsGet(clientId).then((response) => {
        if (response) {
          setLoader({ isActive: false });
          setContactTestimonials(response.data.data);
        }
      });
    }
  };

  const [checkboxState, setCheckboxState] = useState({});

  const handleCheckboxChange = (itemId) => {
    console.log(itemId);
    setCheckboxState((prevState) => {
      const newState = { ...prevState };
      newState[itemId] = !newState[itemId];
      if (!newState[itemId]) {
        delete newState[itemId];
      }
      return newState;
    });
  };

  const clearcheckboxState = () => {
    setCheckboxState({});
  };

  const [formData, setFormData] = useState([]);

  const StaffRoleGetData = async () => {
    await user_service.StaffRoleGet().then((response) => {
      if (response) {
        console.log(response);
        setFormData(response.data.data);
      }
    });
  };
  useEffect(() => {
    if (formData) {
      StaffRoleGetData();
    }
  }, []);

  const profileGetAll = async () => {
    const token = localStorage.getItem("auth");
    if (token) {
      try {
        const userId = jwt(token).id;
        const response = await user_service.profileGet(userId);
        if (response && response.data) {
          setGetContact(response.data);
        }
      } catch (error) {
        console.error("Error fetching profile:", error);
        // Handle the error, e.g., by showing a notification
      }
    }
  };

  const handleImpersonate = async (e) => {
    e.preventDefault();

    const userData = {
      email: getContact?.email,
      password: "vikashtest",
    };

    try {
      // setLoader({ isActive: true });
      const response = await user_service.signin(userData);
      // setLoader({ isActive: false });

      if (response && response.data.jwtToken) {
        // Store the current admin token in impersonationToken
        const currentAuthToken = localStorage.getItem("auth");
        localStorage.setItem("impersonationToken", currentAuthToken);
        // Set the new user token as the current auth token
        localStorage.setItem("auth", response.data.jwtToken);

        setToaster({
          type: "success",
          isShow: true,
          message: "Impersonate Login Successfully",
        });
        setTimeout(() => {
          setToaster((prevToaster) => ({ ...prevToaster, isShow: false }));
        }, 2000);

        // Fetch the profile of the impersonated user
        await profileGetAll();
        window.location.reload();
      } else {
        throw new Error("Impersonation failed");
      }
    } catch (error) {
      setLoader({ isActive: false });
      console.error("Impersonation error:", error);
      setToaster({
        type: "error",
        isShow: true,
        message: "Failed to Impersonate Login",
      });
      setTimeout(() => {
        setToaster((prevToaster) => ({ ...prevToaster, isShow: false }));
      }, 2000);
    }
  };

  return (
    <>
      <Loader isActive={isActive} />
      {isShow && (
        <Toaster
          types={types}
          isShow={isShow}
          toasterBody={toasterBody}
          message={message}
        />
      )}
      <Modalresetpassword
        contactId={params.id}
        onResetSuccess={handleResetSuccess}
      />
      <>
        <div className="bg-secondary float-left w-100 pt-4 mb-4">
          <div className="page-wrapper getContact_page_wrap">
            <div className="content-overlay mt-0">
              <div className="row">
                <aside className="col-lg-4 col-md-5 pe-xl-4">
                  <div className="multitab-action border rounded-3 bg-light p-3">
                    <div className="d-flex d-md-block d-lg-flex align-items-start pt-lg-2 mb-4">
                      <img
                        width="48"
                        height="48"
                        className="rounded-circle getContact_picture"
                        src={getContact?.image || Pic}
                        alt="getContact"
                      />
                      <div className="pt-md-2 pt-lg-0 ps-3 ps-md-0 ps-lg-3">
                        <h2 className="fs-lg mb-0">
                          {getContact?.firstName.charAt(0).toUpperCase() +
                            getContact?.firstName.slice(1)}{" "}
                          {getContact?.lastName}
                        </h2>
                        <ul className="list-unstyled fs-sm mt-2 mb-0 ps-0">
                          {getContact?.phone ? (
                            <li className="pb-0">
                              <a
                                className="nav-link fw-normal p-0"
                                href={`tel:${getContact?.phone ?? ""}`}
                              >
                                <i className="fi-phone opacity-60 me-2"></i>
                                {getContact?.phone}
                              </a>
                            </li>
                          ) : (
                            ""
                          )}

                          {getContact?.email ? (
                            <li className="pb-0">
                              <a
                                className="nav-link fw-normal p-0"
                                href={`tel:${getContact?.email ?? ""}`}
                              >
                                <i className="fi-mail opacity-60 me-2 mt-1"></i>
                                {getContact?.email}
                              </a>
                            </li>
                          ) : (
                            ""
                          )}

                          {getContact.additionalActivateFields?.office ? (
                            <li className="pb-0">
                              <i
                                className="fa fa-briefcase opacity-60 me-2"
                                aria-hidden="true"
                              ></i>
                              {getContact.additionalActivateFields?.office}
                            </li>
                          ) : (
                            ""
                          )}

                          {getContact &&
                            (getContact.contactType === "associate" ||
                              getContact.contactType === "staff" ||
                              getContact.contactType === "admin") &&
                            getContact.nickname &&
                            getContact.languages[0]?.language.length > 0 && (
                              <li className="pb-0 d-flex">
                                <i
                                className="fa fa-language opacity-60"
                                  aria-hidden="true"
                                ></i>
                                {Array.isArray(languageInput) && (
                                  <div className="ms-2">
                                    {languageInput.map((item, index) => (
                                      <React.Fragment key={index}>
                                        {index > 0 && ","}
                                        {item.language}
                                      </React.Fragment>
                                    ))}
                                  </div>
                                )}
                              </li>
                            )}
                          {getContact &&
                            (getContact.contactType === "associate" ||
                              getContact.contactType === "staff" ||
                              getContact.contactType === "admin") &&
                            getContact.nickname &&
                            getContact.specialties[0]?.specialties.length >
                              0 && (
                              <li className="pb-0 d-flex">
                                <i
                                className="fa fa-scribd opacity-60"
                                  aria-hidden="true"
                                ></i>
                                {Array.isArray(specialtiesInput) && (
                                  <div className="ms-2">
                                    {specialtiesInput.map((item, index) => (
                                      <React.Fragment key={index}>
                                        {index > 0 && ","}
                                        {item.specialties}
                                      </React.Fragment>
                                    ))}
                                  </div>
                                )}
                              </li>
                            )}
                        </ul>
                      </div>
                    </div>

                    {localStorage.getItem("auth") &&
                    (jwt(localStorage.getItem("auth")).contactType == "admin" ||
                      jwt(localStorage.getItem("auth")).contactType ==
                        "staff" ||
                      jwt(localStorage.getItem("auth")).id == params.id) ? (
                      <div className="float-left w-100 text-center">
                        <label className="btn btn-primary btn-lg w-100 mb-3 documentlabel d-inline-block">
                          <input
                            id="REPC_real_estate_purchase_contract"
                            type="file"
                            accept={acceptedFileTypes
                              .map((type) => `.${type}`)
                              .join(",")}
                            name="file"
                            onChange={handleFileUpload}
                            value={getContact.file}
                          />
                          <i className="fi-plus me-2"></i> Add a Photo
                        </label>
                      </div>
                    ) : (
                      ""
                    )}

                    {localStorage.getItem("auth") &&
                    jwt(localStorage.getItem("auth")).contactType == "admin" ? (
                      <button
                        className="btn btn-primary"
                        onClick={handleImpersonate}
                      >
                        Impersonate
                      </button>
                    ) : (
                      ""
                    )}

                    <a
                      className="btn btn-outline-secondary d-block d-md-none w-100 mb-3"
                      href="#account-nav"
                      data-bs-toggle="collapse"
                    >
                      <i className="fi-align-justify me-2"></i>Menu
                    </a>
                    <div className="collapse d-md-block mt-3" id="account-nav">
                      <div className="card-nav">
                        {/* <NavLink className={getContact ? "card-nav-link active" : "card-nav-link"} */}


                        {jwt(localStorage.getItem("auth")).contactType ==
                          "admin" ||
                        jwt(localStorage.getItem("auth")).contactType ==
                          "staff" ||
                        jwt(localStorage.getItem("auth")).id == params.id ? (
                          <>
                            {getContact &&
                            (getContact.contactType === "associate" ||
                              getContact.contactType === "staff" ||
                              getContact.contactType === "admin") ? (
                              getContact.nickname ? (
                                <>
                                  <button
                                    className={`card-nav-link ${
                                      activeButton === "201" ? "active" : ""
                                    }`}
                                    onClick={() => { handlegetContact("201") }}
                                  >
                                    {/* <i className="fi-lock opacity-60 me-2"></i> */}
                                    <i class="fa fa-sticky-note-o me-2" aria-hidden="true"></i>

                                    Notes
                                  </button>
                                </>
                              ) : (
                                ""
                              )
                            ) : (
                              ""
                            )}


                          </>
                        ) : (
                          ""
                        )}






                        {getContact &&
                        (getContact.contactType === "associate" ||
                          getContact.contactType === "staff" ||
                          getContact.contactType === "admin") ? (
                          getContact.nickname ? (
                            <>
                              <button
                                className={`card-nav-link ${
                                  activeButton === "1" ? "active" : ""
                                }`}
                                onClick={() => handlegetContact("1")}
                              >
                                <i className="fi-user opacity-60 me-2"></i>
                                Profile
                              </button>
                            </>
                          ) : (
                            ""
                          )
                        ) : (
                          <>
                            <button
                              className={`card-nav-link ${
                                activeButton === "1" ? "active" : ""
                              }`}
                              onClick={() => handlegetContact("1")}
                            >
                              <i className="fi-user opacity-60 me-2"></i>
                              Profile
                            </button>
                          </>
                        )}

                        {jwt(localStorage.getItem("auth")).contactType ==
                          "admin" ||
                        jwt(localStorage.getItem("auth")).contactType ==
                          "staff" ||
                        jwt(localStorage.getItem("auth")).id == params.id ? (
                          <>
                            {getContact &&
                            (getContact.contactType === "associate" ||
                              getContact.contactType === "staff" ||
                              getContact.contactType === "admin") ? (
                              getContact.nickname ? (
                                <>
                                  <button
                                    className={`card-nav-link ${
                                      activeButton === "2" ? "active" : ""
                                    }`}
                                    onClick={() => handlegetContact("2")}
                                  >
                                    <i className="fi-lock opacity-60 me-2"></i>
                                    Account
                                  </button>
                                </>
                              ) : (
                                ""
                              )
                            ) : (
                              ""
                            )}

                            {(localStorage.getItem("auth") &&
                              jwt(localStorage.getItem("auth")).contactType ==
                                "admin") ||
                            jwt(localStorage.getItem("auth")).contactType ==
                              "staff" ? (
                              getContact.nickname ? (
                                <>
                                  <button
                                    className={`card-nav-link ${
                                      activeButton === "11" ? "active" : ""
                                    }`}
                                    onClick={() => handlegetContact("11")}
                                  >
                                    <i className="fi-file opacity-60 me-2"></i>
                                    Documents
                                  </button>
                                </>
                              ) : (
                                ""
                              )
                            ) : (
                              ""
                            )}

                            {getContact &&
                            (getContact.contactType === "associate" ||
                              getContact.contactType === "staff" ||
                              getContact.contactType === "admin") ? (
                              getContact.nickname ? (
                                <button
                                  className={`card-nav-link ${
                                    activeButton === "3" ? "active" : ""
                                  }`}
                                  onClick={() => handlegetContact("3")}
                                >
                                  <i
                                    className="fa fa-sliders  opacity-60 me-2"
                                    aria-hidden="true"
                                  ></i>
                                  Settings
                                </button>
                              ) : (
                                ""
                              )
                            ) : (
                              ""
                            )}

                            {getContact &&
                            (getContact.contactType === "associate" ||
                              getContact.contactType === "staff" ||
                              getContact.contactType === "admin") ? (
                              getContact.nickname ? (
                                <button
                                  className={`card-nav-link ${
                                    activeButton === "4" ? "active" : ""
                                  }`}
                                  onClick={() => handlegetContact("4")}
                                >
                                  <i
                                    className="fa fa-line-chart opacity-60 me-2"
                                    aria-hidden="true"
                                  ></i>
                                  Activity
                                </button>
                              ) : (
                                ""
                              )
                            ) : (
                              ""
                            )}
                            {getContact &&
                            (getContact.contactType === "associate" ||
                              getContact.contactType === "staff" ||
                              getContact.contactType === "admin") ? (
                              getContact.nickname ? (
                                <button
                                  className={`card-nav-link ${
                                    activeButton === "5" ? "active" : ""
                                  }`}
                                  onClick={() => handlegetContact("5")}
                                >
                                  <i className="fi-user opacity-60 me-2"></i>
                                  About Me
                                </button>
                              ) : (
                                ""
                              )
                            ) : (
                              ""
                            )}

                            {getContact &&
                            (getContact.contactType === "associate" ||
                              getContact.contactType === "staff" ||
                              getContact.contactType === "admin") ? (
                              getContact.nickname ? (
                                <button
                                  className={`card-nav-link ${
                                    activeButton === getContact.nickname
                                      ? "active"
                                      : ""
                                  }`}
                                  onClick={() =>
                                    handleNavigate(getContact.nickname)
                                  }
                                >
                                  <i
                                    className="fa fa-credit-card-alt opacity-60 me-2"
                                    aria-hidden="true"
                                  ></i>
                                  Business Card
                                </button>
                              ) : (
                                ""
                              )
                            ) : (
                              ""
                            )}

                            {localStorage.getItem("auth") &&
                            (jwt(localStorage.getItem("auth")).contactType ==
                              "admin" ||
                              jwt(localStorage.getItem("auth")).contactType ==
                                "staff" ||
                              jwt(localStorage.getItem("auth")).id ==
                                params.id) ? (
                              <>
                                {getContact &&
                                (getContact.contactType === "associate" ||
                                  getContact.contactType === "staff" ||
                                  getContact.contactType === "admin") ? (
                                  getContact.nickname ? (
                                    <>
                                      <button
                                        className={`card-nav-link ${
                                          activeButton === "7" ? "active" : ""
                                        }`}
                                        onClick={() => handlegetContact("7")}
                                      >
                                        <i
                                          className="fa fa-address-card opacity-60 me-2"
                                          aria-hidden="true"
                                        ></i>
                                        Account Details
                                      </button>
                                    </>
                                  ) : (
                                    ""
                                  )
                                ) : (
                                  ""
                                )}

                                {getContact &&
                                (getContact.contactType === "associate" ||
                                  getContact.contactType === "staff" ||
                                  getContact.contactType === "admin") ? (
                                  getContact.nickname ? (
                                    <>
                                      <button
                                        className={`card-nav-link ${
                                          activeButton === "8" ? "active" : ""
                                        }`}
                                        onClick={() => handlegetContact("8")}
                                      >
                                        <i
                                          className="fa fa-pencil-square-o opacity-60 me-2"
                                          aria-hidden="true"
                                        ></i>
                                        Edit Profile
                                      </button>
                                    </>
                                  ) : (
                                    ""
                                  )
                                ) : (
                                  ""
                                )}

                                {getContact &&
                                (getContact.contactType === "associate" ||
                                  getContact.contactType === "staff" ||
                                  getContact.contactType === "admin") ? (
                                  getContact.nickname ? (
                                    <>
                                      <button
                                        className={`card-nav-link ${
                                          activeButton === "9" ? "active" : ""
                                        }`}
                                        onClick={() => handlegetContact("9")}
                                      >
                                        <i
                                          className="fa fa-phone-square opacity-60 me-2"
                                          aria-hidden="true"
                                        ></i>
                                        Contact Details
                                      </button>
                                    </>
                                  ) : (
                                    ""
                                  )
                                ) : (
                                  <>
                                    <button
                                      className={`card-nav-link ${
                                        activeButton === "9" ? "active" : ""
                                      }`}
                                      onClick={() => handlegetContact("9")}
                                    >
                                      <i
                                        className="fa fa-phone-square opacity-60 me-2"
                                        aria-hidden="true"
                                      ></i>
                                      Contact Details
                                    </button>
                                  </>
                                )}

                                {localStorage.getItem("auth") &&
                                (jwt(localStorage.getItem("auth"))
                                  .contactType == "admin" ||
                                  jwt(localStorage.getItem("auth"))
                                    .contactType == "staff" ||
                                  jwt(localStorage.getItem("auth")).id ==
                                    params.id) ? (
                                  <>
                                    {getContact &&
                                    (getContact.contactType === "associate" ||
                                      getContact.contactType === "admin") ? (
                                      getContact.nickname ? (
                                        <>
                                          <button
                                            className={`card-nav-link ${
                                              activeButton === "10"
                                                ? "active"
                                                : ""
                                            }`}
                                            onClick={() =>
                                              handlegetContact("10")
                                            }
                                          >
                                            <i
                                              className="fa fa-times-circle-o opacity-60 me-2"
                                              aria-hidden="true"
                                            ></i>
                                            Manage Account
                                          </button>
                                        </>
                                      ) : (
                                        ""
                                      )
                                    ) : (
                                      ""
                                    )}
                                  </>
                                ) : (
                                  ""
                                )}

                                {getContact &&
                                (getContact.contactType === "associate" ||
                                  getContact.contactType === "admin") ? (
                                  getContact.nickname ? (
                                    <>
                                      <button
                                        className={`card-nav-link ${
                                          activeButton === "12" ? "active" : ""
                                        }`}
                                        onClick={() => handlegetContact("12")}
                                      >
                                        <i
                                          className="fa fa-pencil-square-o opacity-60 me-2"
                                          aria-hidden="true"
                                        ></i>
                                        Testimonials
                                      </button>
                                    </>
                                  ) : (
                                    ""
                                  )
                                ) : (
                                  ""
                                )}
                              </>
                            ) : (
                              ""
                            )}
                          </>
                        ) : (
                          ""
                        )}
                      </div>
                    </div>
                  </div>
                </aside>
                <div className="col-md-8 mt-lg-0 mt-md-0 mt-sm-4 mt-5">
                  {step1 == "1" || step1 == "" ? (
                    <div className="float-left w-100">
                      <Loader isActive={isActive} />
                      {isShow && (
                        <Toaster
                          types={types}
                          isShow={isShow}
                          toasterBody={toasterBody}
                          message={message}
                        />
                      )}
                      <main className="page-wrapper getContact_page_wrap">
                        <div className="content-overlay mt-0">
                          <div className="">
                            <div className="float-left w-100 d-flex align-items-center justify-content-between mb-3">
                              <h3
                                className="pull-left mb-0 text-white"
                                id="getContact"
                              >
                                Profile
                              </h3>
                            </div>
                            {getContact.contactType === "community" ? (
                              <div className="mb-3">
                                <div className="d-flex align-items-center justify-content-between float-left mb-3">
                                  <h6
                                    className="text-white text-left mb-0"
                                    id="info"
                                  >
                                    Contact Info
                                  </h6>
                                </div>

                                <div className="">
                                  <div
                                    className="border rounded-3 bg-light p-3"
                                    id="auth-info"
                                  >
                                    <div className="border-bottom pb-3 mb-3">
                                      <div className="d-flex align-items-center justify-content-between">
                                        <div className="pe-2">
                                          <label className="col-form-label">
                                            <span>
                                              <i
                                                className="fa fa-user me-2"
                                                aria-hidden="true"
                                              ></i>
                                              Firstname
                                            </span>
                                          </label>
                                          <div id="firstName-value">
                                            {getContact.firstName}
                                          </div>
                                        </div>

                                        {localStorage.getItem("auth") &&
                                        (jwt(localStorage.getItem("auth"))
                                          .contactType == "admin" ||
                                          jwt(localStorage.getItem("auth"))
                                            .id == params.id) ? (
                                          <div
                                            data-bs-toggle="tooltip"
                                            title="Edit"
                                          >
                                            <a
                                              className="nav-link py-0"
                                              href="#firstName-collapse"
                                              data-bs-toggle="collapse"
                                            >
                                              <i className="fi-edit me-2"></i>
                                            </a>
                                          </div>
                                        ) : (
                                          <>
                                            {localStorage.getItem("auth") &&
                                              jwt(localStorage.getItem("auth"))
                                                .contactType == "staff" &&
                                              (Array.isArray(formData) &&
                                              formData.length > 0
                                                ? formData.map((item) =>
                                                    item.roleStaff ===
                                                    "add_contact" ? (
                                                      <div
                                                        data-bs-toggle="tooltip"
                                                        title="Edit"
                                                      >
                                                        <a
                                                          className="nav-link py-0"
                                                          href="#firstName-collapse"
                                                          data-bs-toggle="collapse"
                                                        >
                                                          <i className="fi-edit me-2"></i>
                                                        </a>
                                                      </div>
                                                    ) : (
                                                      ""
                                                    )
                                                  )
                                                : "")}
                                          </>
                                        )}
                                      </div>
                                      <div
                                        className="collapse"
                                        id="firstName-collapse"
                                        data-bs-parent="#auth-info"
                                      >
                                        <input
                                          className="form-control mt-3"
                                          type="firstName"
                                          name="firstName"
                                          placeholder="firstName"
                                          value={getContact.firstName}
                                          onChange={handleChange}
                                          data-bs-binded-element="#firstName-value"
                                          data-bs-unset-value="Not specified"
                                        />
                                      </div>
                                    </div>

                                    <div className="border-bottom pb-3 mb-3">
                                      <div className="d-flex align-items-center justify-content-between">
                                        <div className="pe-2">
                                          <label className="col-form-label">
                                            <span>
                                              <i
                                                className="fa fa-user me-2"
                                                aria-hidden="true"
                                              ></i>
                                              Lastname
                                            </span>
                                          </label>
                                          <div id="lastName-value">
                                            {getContact.lastName}
                                          </div>
                                        </div>
                                        {localStorage.getItem("auth") &&
                                        (jwt(localStorage.getItem("auth"))
                                          .contactType == "admin" ||
                                          jwt(localStorage.getItem("auth"))
                                            .id == params.id) ? (
                                          <div
                                            data-bs-toggle="tooltip"
                                            title="Edit"
                                          >
                                            <a
                                              className="nav-link py-0"
                                              href="#lastName-collapse"
                                              data-bs-toggle="collapse"
                                            >
                                              <i className="fi-edit me-2"></i>
                                            </a>
                                          </div>
                                        ) : (
                                          <>
                                            {localStorage.getItem("auth") &&
                                              jwt(localStorage.getItem("auth"))
                                                .contactType == "staff" &&
                                              (Array.isArray(formData) &&
                                              formData.length > 0
                                                ? formData.map((item) =>
                                                    item.roleStaff ===
                                                    "add_contact" ? (
                                                      <div
                                                        data-bs-toggle="tooltip"
                                                        title="Edit"
                                                      >
                                                        <a
                                                          className="nav-link py-0"
                                                          href="#lastName-collapse"
                                                          data-bs-toggle="collapse"
                                                        >
                                                          <i className="fi-edit me-2"></i>
                                                        </a>
                                                      </div>
                                                    ) : (
                                                      ""
                                                    )
                                                  )
                                                : "")}
                                          </>
                                        )}
                                      </div>
                                      <div
                                        className="collapse"
                                        id="lastName-collapse"
                                        data-bs-parent="#auth-info"
                                      >
                                        <input
                                          className="form-control mt-3"
                                          type="lastName"
                                          name="lastName"
                                          placeholder="lastName"
                                          value={getContact.lastName}
                                          onChange={handleChange}
                                          data-bs-binded-element="#lastName-value"
                                          data-bs-unset-value="Not specified"
                                        />
                                      </div>
                                    </div>

                                    <div className="border-bottom pb-3 mb-3">
                                      <div className="d-flex align-items-center justify-content-between">
                                        <div className="pe-2">
                                          <label className="col-form-label">
                                            <span>
                                              <i
                                                className="fa fa-user me-2"
                                                aria-hidden="true"
                                              ></i>
                                              Nickname
                                            </span>
                                          </label>
                                          <div id="nickname-value">
                                            {getContact.nickname}
                                          </div>
                                        </div>

                                        {localStorage.getItem("auth") &&
                                        (jwt(localStorage.getItem("auth"))
                                          .contactType == "admin" ||
                                          jwt(localStorage.getItem("auth"))
                                            .id == params.id) ? (
                                          <div
                                            data-bs-toggle="tooltip"
                                            title="Edit"
                                          >
                                            <a
                                              className="nav-link py-0"
                                              href="#nickname-collapse"
                                              data-bs-toggle="collapse"
                                            >
                                              <i className="fi-edit me-2"></i>
                                            </a>
                                          </div>
                                        ) : (
                                          <>
                                            {localStorage.getItem("auth") &&
                                              jwt(localStorage.getItem("auth"))
                                                .contactType == "staff" &&
                                              (Array.isArray(formData) &&
                                              formData.length > 0
                                                ? formData.map((item) =>
                                                    item.roleStaff ===
                                                    "add_contact" ? (
                                                      <div
                                                        data-bs-toggle="tooltip"
                                                        title="Edit"
                                                      >
                                                        <a
                                                          className="nav-link py-0"
                                                          href="#nickname-collapse"
                                                          data-bs-toggle="collapse"
                                                        >
                                                          <i className="fi-edit me-2"></i>
                                                        </a>
                                                      </div>
                                                    ) : (
                                                      ""
                                                    )
                                                  )
                                                : "")}
                                          </>
                                        )}
                                      </div>
                                      <div
                                        className="collapse"
                                        id="nickname-collapse"
                                        data-bs-parent="#auth-info"
                                      >
                                        <input
                                          className="form-control mt-3"
                                          type="nickname"
                                          name="nickname"
                                          placeholder="nickname"
                                          value={getContact.nickname}
                                          onChange={handleChange}
                                          data-bs-binded-element="#nickname-value"
                                          data-bs-unset-value="Not specified"
                                        />
                                      </div>
                                    </div>

                                    <div className="border-bottom pb-3 mb-3">
                                      <div className="d-flex align-items-center justify-content-between">
                                        <div className="pe-2">
                                          <label
                                            for="staticEmail"
                                            className="col-form-label"
                                          >
                                            <span>
                                              <i
                                                className="fa fa-at me-2"
                                                aria-hidden="true"
                                              ></i>
                                              Email
                                            </span>
                                          </label>
                                          <div id="email-value">
                                            {getContact.email}
                                          </div>
                                        </div>

                                        {localStorage.getItem("auth") &&
                                        (jwt(localStorage.getItem("auth"))
                                          .contactType == "admin" ||
                                          jwt(localStorage.getItem("auth"))
                                            .id == params.id) ? (
                                          <div
                                            data-bs-toggle="tooltip"
                                            title="Edit"
                                          >
                                            <a
                                              className="nav-link py-0"
                                              href="#email-collapse"
                                              data-bs-toggle="collapse"
                                            >
                                              <i className="fi-edit me-2"></i>
                                            </a>
                                          </div>
                                        ) : (
                                          <>
                                            {localStorage.getItem("auth") &&
                                              jwt(localStorage.getItem("auth"))
                                                .contactType == "staff" &&
                                              (Array.isArray(formData) &&
                                              formData.length > 0
                                                ? formData.map((item) =>
                                                    item.roleStaff ===
                                                    "add_contact" ? (
                                                      <div
                                                        data-bs-toggle="tooltip"
                                                        title="Edit"
                                                      >
                                                        <a
                                                          className="nav-link py-0"
                                                          href="#email-collapse"
                                                          data-bs-toggle="collapse"
                                                        >
                                                          <i className="fi-edit me-2"></i>
                                                        </a>
                                                      </div>
                                                    ) : (
                                                      ""
                                                    )
                                                  )
                                                : "")}
                                          </>
                                        )}
                                      </div>
                                      <div
                                        className="collapse"
                                        id="email-collapse"
                                        data-bs-parent="#auth-info"
                                      >
                                        <input
                                          className="form-control mt-3"
                                          type="email"
                                          name="email"
                                          placeholder="email"
                                          value={getContact.email}
                                          onChange={handleChange}
                                          data-bs-binded-element="#email-value"
                                          data-bs-unset-value="Not specified"
                                        />
                                      </div>
                                    </div>

                                    <div className="border-bottom pb-3 mb-3">
                                      <div className="d-flex align-items-center justify-content-between">
                                        <div className="pe-2">
                                          <label
                                            for="staticEmail"
                                            className="col-form-label"
                                          >
                                            <i
                                              className="fa fa-phone-square me-2"
                                              aria-hidden="true"
                                            ></i>
                                            <span>Phone</span>
                                          </label>
                                          <div id="phone-value">
                                            {getContact.phone}
                                          </div>
                                        </div>

                                        {localStorage.getItem("auth") &&
                                        (jwt(localStorage.getItem("auth"))
                                          .contactType == "admin" ||
                                          jwt(localStorage.getItem("auth"))
                                            .id == params.id) ? (
                                          <div
                                            data-bs-toggle="tooltip"
                                            title="Edit"
                                          >
                                            <a
                                              className="nav-link py-0"
                                              href="#phone-collapse"
                                              data-bs-toggle="collapse"
                                            >
                                              <i className="fi-edit me-2"></i>
                                            </a>
                                          </div>
                                        ) : (
                                          <>
                                            {localStorage.getItem("auth") &&
                                              jwt(localStorage.getItem("auth"))
                                                .contactType == "staff" &&
                                              (Array.isArray(formData) &&
                                              formData.length > 0
                                                ? formData.map((item) =>
                                                    item.roleStaff ===
                                                    "add_contact" ? (
                                                      <div
                                                        data-bs-toggle="tooltip"
                                                        title="Edit"
                                                      >
                                                        <a
                                                          className="nav-link py-0"
                                                          href="#phone-collapse"
                                                          data-bs-toggle="collapse"
                                                        >
                                                          <i className="fi-edit me-2"></i>
                                                        </a>
                                                      </div>
                                                    ) : (
                                                      ""
                                                    )
                                                  )
                                                : "")}
                                          </>
                                        )}
                                      </div>
                                      <div
                                        className="collapse"
                                        id="phone-collapse"
                                        data-bs-parent="#personal-details"
                                      >
                                        <input
                                          className="form-control mt-3"
                                          type="number"
                                          name="phone"
                                          placeholder="phone"
                                          value={getContact.phone}
                                          onChange={handleChange}
                                          data-bs-binded-element="#phone-value"
                                          data-bs-unset-value="Not specified"
                                        />
                                      </div>
                                    </div>

                                    {localStorage.getItem("auth") &&
                                    (jwt(localStorage.getItem("auth"))
                                      .contactType == "admin" ||
                                      jwt(localStorage.getItem("auth")).id ==
                                        params.id) ? (
                                      <div className="border-bottom pb-3 mb-3">
                                        <div className="d-flex align-items-center justify-content-between">
                                          <div className="pe-2">
                                            <label
                                              for="staticEmail"
                                              className="col-form-label"
                                            >
                                              <i
                                                className="fa fa-map-marker me-2"
                                                aria-hidden="true"
                                              ></i>
                                              <span>Address</span>
                                            </label>

                                            <div id="address-value">
                                              {getContact.address}
                                            </div>
                                          </div>
                                          {localStorage.getItem("auth") &&
                                          (jwt(localStorage.getItem("auth"))
                                            .contactType == "admin" ||
                                            jwt(localStorage.getItem("auth"))
                                              .id == params.id) ? (
                                            <div
                                              data-bs-toggle="tooltip"
                                              title="Edit"
                                            >
                                              <a
                                                className="nav-link py-0"
                                                href="#address-collapse"
                                                data-bs-toggle="collapse"
                                              >
                                                <i className="fi-edit me-2"></i>
                                              </a>
                                            </div>
                                          ) : (
                                            ""
                                          )}
                                        </div>
                                        <div
                                          className="collapse"
                                          id="address-collapse"
                                          data-bs-parent="#personal-details"
                                        >
                                          <input
                                            className="form-control mt-3"
                                            type="text"
                                            name="address"
                                            value={getContact.address}
                                            onChange={handleChange}
                                            data-bs-binded-element="#address-value"
                                            data-bs-unset-value="Not specified"
                                            placeholder="Enter address"
                                          />
                                        </div>
                                      </div>
                                    ) : (
                                      <>
                                        {localStorage.getItem("auth") &&
                                          jwt(localStorage.getItem("auth"))
                                            .contactType == "staff" &&
                                          (Array.isArray(formData) &&
                                          formData.length > 0
                                            ? formData.map((item) =>
                                                item.roleStaff ===
                                                "add_contact" ? (
                                                  <div className="border-bottom pb-3 mb-3">
                                                    <div className="d-flex align-items-center justify-content-between">
                                                      <div className="pe-2">
                                                        <label
                                                          for="staticEmail"
                                                          className="col-form-label"
                                                        >
                                                          <i
                                                            className="fa fa-map-marker me-2"
                                                            aria-hidden="true"
                                                          ></i>
                                                          <span>Address</span>
                                                        </label>

                                                        <div id="address-value">
                                                          {getContact.address}
                                                        </div>
                                                      </div>
                                                      <>
                                                        {localStorage.getItem(
                                                          "auth"
                                                        ) &&
                                                          jwt(
                                                            localStorage.getItem(
                                                              "auth"
                                                            )
                                                          ).contactType ==
                                                            "staff" &&
                                                          (Array.isArray(
                                                            formData
                                                          ) &&
                                                          formData.length > 0
                                                            ? formData.map(
                                                                (item) =>
                                                                  item.roleStaff ===
                                                                  "add_contact" ? (
                                                                    <div
                                                                      data-bs-toggle="tooltip"
                                                                      title="Edit"
                                                                    >
                                                                      <a
                                                                        className="nav-link py-0"
                                                                        href="#address-collapse"
                                                                        data-bs-toggle="collapse"
                                                                      >
                                                                        <i className="fi-edit me-2"></i>
                                                                      </a>
                                                                    </div>
                                                                  ) : (
                                                                    ""
                                                                  )
                                                              )
                                                            : "")}
                                                      </>
                                                    </div>
                                                    <div
                                                      className="collapse"
                                                      id="address-collapse"
                                                      data-bs-parent="#personal-details"
                                                    >
                                                      <input
                                                        className="form-control mt-3"
                                                        type="text"
                                                        name="address"
                                                        value={
                                                          getContact.address
                                                        }
                                                        onChange={handleChange}
                                                        data-bs-binded-element="#address-value"
                                                        data-bs-unset-value="Not specified"
                                                        placeholder="Enter address"
                                                      />
                                                    </div>
                                                  </div>
                                                ) : (
                                                  ""
                                                )
                                              )
                                            : "")}
                                      </>
                                    )}
                                    <div className="border-bottom pb-3 mb-3">
                                      <div className="d-flex align-items-center justify-content-between">
                                        <div className="pe-2">
                                          <label
                                            for="staticEmail"
                                            className="col-form-label"
                                          >
                                            <i
                                              className="fa fa-chrome me-2"
                                              aria-hidden="true"
                                            ></i>
                                            <span>Web</span>
                                          </label>

                                          <div id="web-value">
                                            {getContact.web}
                                          </div>
                                        </div>
                                        {localStorage.getItem("auth") &&
                                        (jwt(localStorage.getItem("auth"))
                                          .contactType == "admin" ||
                                          jwt(localStorage.getItem("auth"))
                                            .id == params.id) ? (
                                          <div
                                            data-bs-toggle="tooltip"
                                            title="Edit"
                                          >
                                            <a
                                              className="nav-link py-0"
                                              href="#web-collapse"
                                              data-bs-toggle="collapse"
                                            >
                                              <i className="fi-edit me-2"></i>
                                            </a>
                                          </div>
                                        ) : (
                                          <>
                                            {localStorage.getItem("auth") &&
                                              jwt(localStorage.getItem("auth"))
                                                .contactType == "staff" &&
                                              (Array.isArray(formData) &&
                                              formData.length > 0
                                                ? formData.map((item) =>
                                                    item.roleStaff ===
                                                    "add_contact" ? (
                                                      <div
                                                        data-bs-toggle="tooltip"
                                                        title="Edit"
                                                      >
                                                        <a
                                                          className="nav-link py-0"
                                                          href="#web-collapse"
                                                          data-bs-toggle="collapse"
                                                        >
                                                          <i className="fi-edit me-2"></i>
                                                        </a>
                                                      </div>
                                                    ) : (
                                                      ""
                                                    )
                                                  )
                                                : "")}
                                          </>
                                        )}
                                      </div>
                                      <div
                                        className="collapse"
                                        id="web-collapse"
                                        data-bs-parent="#personal-details"
                                      >
                                        <input
                                          className="form-control mt-3"
                                          type="text"
                                          name="web"
                                          value={getContact.web}
                                          onChange={handleChange}
                                          data-bs-binded-element="#web-value"
                                          data-bs-unset-value="Not specified"
                                          placeholder="Enter Website"
                                        />
                                      </div>
                                    </div>

                                    {localStorage.getItem("auth") &&
                                    (jwt(localStorage.getItem("auth"))
                                      .contactType == "admin" ||
                                      jwt(localStorage.getItem("auth")).id ==
                                        params.id) ? (
                                      <>
                                        <div className="border-bottom pb-3 mb-3">
                                          <div className="d-flex align-items-center justify-content-between">
                                            <div className="pe-2">
                                              <label
                                                for="staticEmail"
                                                className="col-form-label"
                                              >
                                                <i
                                                  className="fa fa-fax me-2"
                                                  aria-hidden="true"
                                                ></i>
                                                <span>Fax</span>
                                              </label>

                                              <div id="fax-value">
                                                {getContact.fax}
                                              </div>
                                            </div>
                                            {localStorage.getItem("auth") &&
                                            (jwt(localStorage.getItem("auth"))
                                              .contactType == "admin" ||
                                              jwt(localStorage.getItem("auth"))
                                                .id == params.id) ? (
                                              <div
                                                data-bs-toggle="tooltip"
                                                title="Edit"
                                              >
                                                <a
                                                  className="nav-link py-0"
                                                  href="#fax-collapse"
                                                  data-bs-toggle="collapse"
                                                >
                                                  <i className="fi-edit me-2"></i>
                                                </a>
                                              </div>
                                            ) : (
                                              ""
                                            )}
                                          </div>
                                          <div
                                            className="collapse"
                                            id="fax-collapse"
                                            data-bs-parent="#personal-details"
                                          >
                                            <input
                                              className="form-control mt-3"
                                              type="text"
                                              name="fax"
                                              value={getContact.fax}
                                              onChange={handleChange}
                                              data-bs-binded-element="#fax-value"
                                              data-bs-unset-value="Not specified"
                                              placeholder="Enter Fax NO"
                                            />
                                          </div>
                                        </div>

                                        <div className="border-bottom pb-3 mb-3">
                                          <div className="d-flex align-items-center justify-content-between">
                                            <div className="pe-2">
                                              <label
                                                for="staticEmail"
                                                className="col-form-label"
                                              >
                                                <i
                                                  className="fa fa-inbox me-2"
                                                  aria-hidden="true"
                                                ></i>
                                                <span>Inbox</span>
                                              </label>

                                              <div id="inbox-value">
                                                {getContact.inbox}
                                              </div>
                                            </div>
                                            {localStorage.getItem("auth") &&
                                            (jwt(localStorage.getItem("auth"))
                                              .contactType == "admin" ||
                                              jwt(localStorage.getItem("auth"))
                                                .id == params.id) ? (
                                              <div
                                                data-bs-toggle="tooltip"
                                                title="Edit"
                                              >
                                                <a
                                                  className="nav-link py-0"
                                                  href="#inbox-collapse"
                                                  data-bs-toggle="collapse"
                                                >
                                                  <i className="fi-edit me-2"></i>
                                                </a>
                                              </div>
                                            ) : (
                                              ""
                                            )}
                                          </div>
                                          <div
                                            className="collapse"
                                            id="inbox-collapse"
                                            data-bs-parent="#personal-details"
                                          >
                                            <input
                                              className="form-control mt-3"
                                              type="text"
                                              name="inbox"
                                              value={getContact.inbox}
                                              onChange={handleChange}
                                              data-bs-binded-element="#inbox-value"
                                              data-bs-unset-value="Not specified"
                                              placeholder="Enter message"
                                            />
                                          </div>
                                        </div>
                                      </>
                                    ) : (
                                      <>
                                        {localStorage.getItem("auth") &&
                                          jwt(localStorage.getItem("auth"))
                                            .contactType == "staff" &&
                                          (Array.isArray(formData) &&
                                          formData.length > 0
                                            ? formData.map((item) =>
                                                item.roleStaff ===
                                                "add_contact" ? (
                                                  <>
                                                    <div className="border-bottom pb-3 mb-3">
                                                      <div className="d-flex align-items-center justify-content-between">
                                                        <div className="pe-2">
                                                          <label
                                                            for="staticEmail"
                                                            className="col-form-label"
                                                          >
                                                            <i
                                                              className="fa fa-fax me-2"
                                                              aria-hidden="true"
                                                            ></i>
                                                            <span>Fax</span>
                                                          </label>

                                                          <div id="fax-value">
                                                            {getContact.fax}
                                                          </div>
                                                        </div>
                                                        {localStorage.getItem(
                                                          "auth"
                                                        ) &&
                                                          jwt(
                                                            localStorage.getItem(
                                                              "auth"
                                                            )
                                                          ).contactType ==
                                                            "staff" &&
                                                          (Array.isArray(
                                                            formData
                                                          ) &&
                                                          formData.length > 0
                                                            ? formData.map(
                                                                (item) =>
                                                                  item.roleStaff ===
                                                                  "add_contact" ? (
                                                                    <div
                                                                      data-bs-toggle="tooltip"
                                                                      title="Edit"
                                                                    >
                                                                      <a
                                                                        className="nav-link py-0"
                                                                        href="#fax-collapse"
                                                                        data-bs-toggle="collapse"
                                                                      >
                                                                        <i className="fi-edit me-2"></i>
                                                                      </a>
                                                                    </div>
                                                                  ) : (
                                                                    ""
                                                                  )
                                                              )
                                                            : "")}
                                                      </div>
                                                      <div
                                                        className="collapse"
                                                        id="fax-collapse"
                                                        data-bs-parent="#personal-details"
                                                      >
                                                        <input
                                                          className="form-control mt-3"
                                                          type="text"
                                                          name="fax"
                                                          value={getContact.fax}
                                                          onChange={
                                                            handleChange
                                                          }
                                                          data-bs-binded-element="#fax-value"
                                                          data-bs-unset-value="Not specified"
                                                          placeholder="Enter Fax NO"
                                                        />
                                                      </div>
                                                    </div>

                                                    <div className="border-bottom pb-3 mb-3">
                                                      <div className="d-flex align-items-center justify-content-between">
                                                        <div className="pe-2">
                                                          <label
                                                            for="staticEmail"
                                                            className="col-form-label"
                                                          >
                                                            <i
                                                              className="fa fa-inbox me-2"
                                                              aria-hidden="true"
                                                            ></i>
                                                            <span>Inbox</span>
                                                          </label>

                                                          <div id="inbox-value">
                                                            {getContact.inbox}
                                                          </div>
                                                        </div>
                                                        {localStorage.getItem(
                                                          "auth"
                                                        ) &&
                                                        (jwt(
                                                          localStorage.getItem(
                                                            "auth"
                                                          )
                                                        ).contactType ==
                                                          "admin" ||
                                                          jwt(
                                                            localStorage.getItem(
                                                              "auth"
                                                            )
                                                          ).id == params.id) ? (
                                                          <div
                                                            data-bs-toggle="tooltip"
                                                            title="Edit"
                                                          >
                                                            <a
                                                              className="nav-link py-0"
                                                              href="#inbox-collapse"
                                                              data-bs-toggle="collapse"
                                                            >
                                                              <i className="fi-edit me-2"></i>
                                                            </a>
                                                          </div>
                                                        ) : (
                                                          <>
                                                            {localStorage.getItem(
                                                              "auth"
                                                            ) &&
                                                              jwt(
                                                                localStorage.getItem(
                                                                  "auth"
                                                                )
                                                              ).contactType ==
                                                                "staff" &&
                                                              (Array.isArray(
                                                                formData
                                                              ) &&
                                                              formData.length >
                                                                0
                                                                ? formData.map(
                                                                    (item) =>
                                                                      item.roleStaff ===
                                                                      "add_contact" ? (
                                                                        <div
                                                                          data-bs-toggle="tooltip"
                                                                          title="Edit"
                                                                        >
                                                                          <a
                                                                            className="nav-link py-0"
                                                                            href="#inbox-collapse"
                                                                            data-bs-toggle="collapse"
                                                                          >
                                                                            <i className="fi-edit me-2"></i>
                                                                          </a>
                                                                        </div>
                                                                      ) : (
                                                                        ""
                                                                      )
                                                                  )
                                                                : "")}
                                                          </>
                                                        )}
                                                      </div>
                                                      <div
                                                        className="collapse"
                                                        id="inbox-collapse"
                                                        data-bs-parent="#personal-details"
                                                      >
                                                        <input
                                                          className="form-control mt-3"
                                                          type="text"
                                                          name="inbox"
                                                          value={
                                                            getContact.inbox
                                                          }
                                                          onChange={
                                                            handleChange
                                                          }
                                                          data-bs-binded-element="#inbox-value"
                                                          data-bs-unset-value="Not specified"
                                                          placeholder="Enter message"
                                                        />
                                                      </div>
                                                    </div>
                                                  </>
                                                ) : (
                                                  ""
                                                )
                                              )
                                            : "")}
                                      </>
                                    )}
                                  </div>
                                </div>

                                <div className="d-flex align-items-center justify-content-between float-left mb-3 mt-3">
                                  <h6 className="text-white text-left mb-0" id="info">Profile Info</h6>
                                </div>
                                <div className="">
                                  <div
                                    className="border rounded-3 bg-light p-3"
                                    id="auth-info">
                                    <div className="border-bottom pb-3 mb-3">
                                      <div className="d-flex align-items-center justify-content-between">
                                        <div className="pe-2">
                                          <label className="col-form-label">
                                            <span>
                                              <i
                                                className="fa fa-sitemap me-2"
                                                aria-hidden="true"
                                              ></i>
                                              Organization
                                            </span>
                                          </label>
                                          <div id="organization-value">
                                            {getContact.organization}
                                          </div>
                                        </div>
                                        {localStorage.getItem("auth") &&
                                        (jwt(localStorage.getItem("auth"))
                                          .contactType == "admin" ||
                                          jwt(localStorage.getItem("auth"))
                                            .id == params.id) ? (
                                          <div
                                            data-bs-toggle="tooltip"
                                            title="Edit"
                                          >
                                            <a
                                              className="nav-link py-0"
                                              href="#organization-collapse"
                                              data-bs-toggle="collapse"
                                            >
                                              <i className="fi-edit me-2"></i>
                                            </a>
                                          </div>
                                        ) : (
                                          <>
                                            {localStorage.getItem("auth") &&
                                              jwt(localStorage.getItem("auth"))
                                                .contactType == "staff" &&
                                              (Array.isArray(formData) &&
                                              formData.length > 0
                                                ? formData.map((item) =>
                                                    item.roleStaff ===
                                                    "add_contact" ? (
                                                      <div
                                                        data-bs-toggle="tooltip"
                                                        title="Edit"
                                                      >
                                                        <a
                                                          className="nav-link py-0"
                                                          href="#organization-collapse"
                                                          data-bs-toggle="collapse"
                                                        >
                                                          <i className="fi-edit me-2"></i>
                                                        </a>
                                                      </div>
                                                    ) : (
                                                      ""
                                                    )
                                                  )
                                                : "")}
                                          </>
                                        )}
                                      </div>
                                      <div
                                        className="collapse"
                                        id="organization-collapse"
                                        data-bs-parent="#auth-info"
                                      >
                                        <input
                                          className="form-control mt-3"
                                          type="organization"
                                          name="organization"
                                          placeholder="organization"
                                          value={getContact.organization}
                                          onChange={handleChange}
                                          data-bs-binded-element="#organization-value"
                                          data-bs-unset-value="Not specified"
                                        />
                                      </div>
                                    </div>

                                    <div className="border-bottom pb-3 mb-3">
                                      <div className="d-flex align-items-center justify-content-between">
                                        <div className="pe-2">
                                          <label className="col-form-label">
                                            <span>
                                              <i
                                                className="fa fa-building me-2"
                                                aria-hidden="true"
                                              ></i>
                                              Department
                                            </span>
                                          </label>
                                          <div id="department-value">
                                            {getContact.department}
                                          </div>
                                        </div>
                                        {localStorage.getItem("auth") &&
                                        (jwt(localStorage.getItem("auth"))
                                          .contactType == "admin" ||
                                          jwt(localStorage.getItem("auth"))
                                            .id == params.id) ? (
                                          <div
                                            data-bs-toggle="tooltip"
                                            title="Edit"
                                          >
                                            <a
                                              className="nav-link py-0"
                                              href="#department-collapse"
                                              data-bs-toggle="collapse"
                                            >
                                              <i className="fi-edit me-2"></i>
                                            </a>
                                          </div>
                                        ) : (
                                          <>
                                            {localStorage.getItem("auth") &&
                                              jwt(localStorage.getItem("auth"))
                                                .contactType == "staff" &&
                                              (Array.isArray(formData) &&
                                              formData.length > 0
                                                ? formData.map((item) =>
                                                    item.roleStaff ===
                                                    "add_contact" ? (
                                                      <div
                                                        data-bs-toggle="tooltip"
                                                        title="Edit"
                                                      >
                                                        <a
                                                          className="nav-link py-0"
                                                          href="#department-collapse"
                                                          data-bs-toggle="collapse"
                                                        >
                                                          <i className="fi-edit me-2"></i>
                                                        </a>
                                                      </div>
                                                    ) : (
                                                      ""
                                                    )
                                                  )
                                                : "")}
                                          </>
                                        )}
                                      </div>
                                      <div
                                        className="collapse"
                                        id="department-collapse"
                                        data-bs-parent="#auth-info"
                                      >
                                        <input
                                          className="form-control mt-3"
                                          type="department"
                                          name="department"
                                          placeholder="department"
                                          value={getContact.department}
                                          onChange={handleChange}
                                          data-bs-binded-element="#department-value"
                                          data-bs-unset-value="Not specified"
                                        />
                                      </div>
                                    </div>

                                    <div className="border-bottom pb-3 mb-3">
                                      <div className="d-flex align-items-center justify-content-between">
                                        <div className="pe-2">
                                          <label
                                            for="staticEmail"
                                            className="col-form-label"
                                          >
                                              <span className="ms-1">
                                              <i
                                                className="fa fa-text-width me-2"
                                                aria-hidden="true"
                                              ></i>
                                                Title
                                              </span>
                                           
                                          </label>
                                          <div id="title-value">
                                            {getContact.title}
                                          </div>
                                        </div>
                                        {localStorage.getItem("auth") &&
                                        (jwt(localStorage.getItem("auth"))
                                          .contactType == "admin" ||
                                          jwt(localStorage.getItem("auth"))
                                            .id == params.id) ? (
                                          <div
                                            data-bs-toggle="tooltip"
                                            title="Edit"
                                          >
                                            <a
                                              className="nav-link py-0"
                                              href="#title-collapse"
                                              data-bs-toggle="collapse"
                                            >
                                              <i className="fi-edit me-2"></i>
                                            </a>
                                          </div>
                                        ) : (
                                          <>
                                            {localStorage.getItem("auth") &&
                                              jwt(localStorage.getItem("auth"))
                                                .contactType == "staff" &&
                                              (Array.isArray(formData) &&
                                              formData.length > 0
                                                ? formData.map((item) =>
                                                    item.roleStaff ===
                                                    "add_contact" ? (
                                                      <div
                                                        data-bs-toggle="tooltip"
                                                        title="Edit"
                                                      >
                                                        <a
                                                          className="nav-link py-0"
                                                          href="#title-collapse"
                                                          data-bs-toggle="collapse"
                                                        >
                                                          <i className="fi-edit me-2"></i>
                                                        </a>
                                                      </div>
                                                    ) : (
                                                      ""
                                                    )
                                                  )
                                                : "")}
                                          </>
                                        )}
                                      </div>
                                      <div
                                        className="collapse"
                                        id="title-collapse"
                                        data-bs-parent="#auth-info"
                                      >
                                        <input
                                          className="form-control mt-3"
                                          type="title"
                                          name="title"
                                          placeholder="title"
                                          value={getContact.title}
                                          onChange={handleChange}
                                          data-bs-binded-element="#title-value"
                                          data-bs-unset-value="Not specified"
                                        />
                                      </div>
                                    </div>
                                    {socialLinks.length > 0 ? (
                                      <div className="border-bottom pb-3 mb-3">
                                        <div className="d-flex align-items-center justify-content-between">
                                          <div className="pe-2">
                                            <label
                                              htmlFor="staticEmail"
                                              className="col-form-label"
                                            >
                                              <i
                                                className="fa fa-share-square-o"
                                                aria-hidden="true"
                                              ></i>
                                              <span>Social Links</span>
                                            </label>
                                            {/* <div id="social_links-value">
                                            {getContact.social_links.map(
                                              (socialLink, index) => (
                                                <div key={index}><br/>
                                                  {socialLink.linkurl}
                                                </div>
                                              )
                                            )}
                                          </div> */}
                                          </div>
                                          {localStorage.getItem("auth") &&
                                          (jwt(localStorage.getItem("auth"))
                                            .contactType === "admin" ||
                                            jwt(localStorage.getItem("auth"))
                                              .id === params.id) ? (
                                            <div
                                              data-bs-toggle="tooltip"
                                              title="Edit"
                                            >
                                              <a
                                                className="nav-link py-0"
                                                href="#social_links-collapse"
                                                data-bs-toggle="collapse"
                                              >
                                                <i className="fi-edit me-2"></i>
                                              </a>
                                            </div>
                                          ) : (
                                            <>
                                              {localStorage.getItem("auth") &&
                                                jwt(
                                                  localStorage.getItem("auth")
                                                ).contactType == "staff" &&
                                                (Array.isArray(formData) &&
                                                formData.length > 0
                                                  ? formData.map((item) =>
                                                      item.roleStaff ===
                                                      "add_contact" ? (
                                                        <div
                                                          data-bs-toggle="tooltip"
                                                          title="Edit"
                                                        >
                                                          <a
                                                            className="nav-link py-0"
                                                            href="#social_links-collapse"
                                                            data-bs-toggle="collapse"
                                                          >
                                                            <i className="fi-edit me-2"></i>
                                                          </a>
                                                        </div>
                                                      ) : (
                                                        ""
                                                      )
                                                    )
                                                  : "")}
                                            </>
                                          )}
                                        </div>
                                        <div
                                          className="collapse"
                                          id="social_links-collapse"
                                          data-bs-parent="#personal-details"
                                        >
                                          {socialLinks.map(
                                            (socialLink, index) => (
                                              <div key={index}>
                                                <label className="form-label mt-3">
                                                  {socialLink.type} URL
                                                </label>
                                                <input
                                                  className="form-control"
                                                  type="text"
                                                  name="linkurl"
                                                  value={socialLink.linkurl}
                                                  onChange={(event) =>
                                                    handleChangeSocial(
                                                      event,
                                                      index
                                                    )
                                                  }
                                                />
                                              </div>
                                            )
                                          )}
                                        </div>
                                      </div>
                                    ) : (
                                      ""
                                    )}

                                    {localStorage.getItem("auth") &&
                                    (jwt(localStorage.getItem("auth"))
                                      .contactType == "admin" ||
                                      jwt(localStorage.getItem("auth")).id ==
                                        params.id) ? (
                                      <div className="d-flex alignhange-items-center justify-content-between">
                                        <button
                                          className="btn btn-primary  px-3 px-sm-4"
                                          type="button"
                                          onClick={handleSubmit}
                                        >
                                          Save Change
                                        </button>
                                      </div>
                                    ) : (
                                      <>
                                        {localStorage.getItem("auth") &&
                                          jwt(localStorage.getItem("auth"))
                                            .contactType == "staff" &&
                                          (Array.isArray(formData) &&
                                          formData.length > 0
                                            ? formData.map((item) =>
                                                item.roleStaff ===
                                                "add_contact" ? (
                                                  <div className="d-flex alignhange-items-center justify-content-between">
                                                    <button
                                                      className="btn btn-primary  px-3 px-sm-4"
                                                      type="button"
                                                      onClick={handleSubmit}
                                                    >
                                                      Save Change
                                                    </button>
                                                  </div>
                                                ) : (
                                                  ""
                                                )
                                              )
                                            : "")}
                                      </>
                                    )}
                                  </div>
                                </div>
                              </div>
                            ) : (
                              <div className="mb-3">
                                <div className="d-flex align-items-center justify-content-between float-left mb-3">
                                  <h6
                                    className="text-white text-left mb-0"
                                    id="info"
                                  >
                                    Contact Info
                                  </h6>
                                </div>

                                <div className="">
                                  <div
                                    className="border rounded-3 bg-light p-3"
                                    id="auth-info"
                                  >
                                    <div className="border-bottom pb-3 mb-3">
                                      <div className="d-flex align-items-center justify-content-between">
                                        <div className="pe-2">
                                          <label className="col-form-label">
                                            <span>
                                              <i
                                                className="fa fa-user me-2"
                                                aria-hidden="true"
                                              ></i>
                                              Firstname
                                            </span>
                                          </label>
                                          <div id="firstName-value">
                                            {getContact.firstName}
                                          </div>
                                        </div>

                                        {localStorage.getItem("auth") &&
                                        (jwt(localStorage.getItem("auth"))
                                          .contactType == "admin" ||
                                          jwt(localStorage.getItem("auth"))
                                            .id == params.id) ? (
                                          <div
                                            data-bs-toggle="tooltip"
                                            title="Edit"
                                          >
                                            <a
                                              className="nav-link py-0"
                                              href="#firstName-collapse"
                                              data-bs-toggle="collapse"
                                            >
                                              <i className="fi-edit me-2"></i>
                                            </a>
                                          </div>
                                        ) : (
                                          <>
                                            {localStorage.getItem("auth") &&
                                              jwt(localStorage.getItem("auth"))
                                                .contactType == "staff" &&
                                              (Array.isArray(formData) &&
                                              formData.length > 0
                                                ? formData.map((item) =>
                                                    item.roleStaff ===
                                                    "add_contact" ? (
                                                      <div
                                                        data-bs-toggle="tooltip"
                                                        title="Edit"
                                                      >
                                                        <a
                                                          className="nav-link py-0"
                                                          href="#firstName-collapse"
                                                          data-bs-toggle="collapse"
                                                        >
                                                          <i className="fi-edit me-2"></i>
                                                        </a>
                                                      </div>
                                                    ) : (
                                                      ""
                                                    )
                                                  )
                                                : "")}
                                          </>
                                        )}
                                      </div>
                                      <div
                                        className="collapse"
                                        id="firstName-collapse"
                                        data-bs-parent="#auth-info"
                                      >
                                        <input
                                          className="form-control mt-3"
                                          type="firstName"
                                          name="firstName"
                                          placeholder="firstName"
                                          value={getContact.firstName}
                                          onChange={handleChange}
                                          data-bs-binded-element="#firstName-value"
                                          data-bs-unset-value="Not specified"
                                        />
                                      </div>
                                    </div>

                                    <div className="border-bottom pb-3 mb-3">
                                      <div className="d-flex align-items-center justify-content-between">
                                        <div className="pe-2">
                                          <label className="col-form-label">
                                            <span>
                                              <i
                                                className="fa fa-user me-2"
                                                aria-hidden="true"
                                              ></i>
                                              Lastname
                                            </span>
                                          </label>
                                          <div id="lastName-value">
                                            {getContact.lastName}
                                          </div>
                                        </div>
                                        {localStorage.getItem("auth") &&
                                        (jwt(localStorage.getItem("auth"))
                                          .contactType == "admin" ||
                                          jwt(localStorage.getItem("auth"))
                                            .id == params.id) ? (
                                          <div
                                            data-bs-toggle="tooltip"
                                            title="Edit"
                                          >
                                            <a
                                              className="nav-link py-0"
                                              href="#lastName-collapse"
                                              data-bs-toggle="collapse"
                                            >
                                              <i className="fi-edit me-2"></i>
                                            </a>
                                          </div>
                                        ) : (
                                          <>
                                            {localStorage.getItem("auth") &&
                                              jwt(localStorage.getItem("auth"))
                                                .contactType == "staff" &&
                                              (Array.isArray(formData) &&
                                              formData.length > 0
                                                ? formData.map((item) =>
                                                    item.roleStaff ===
                                                    "add_contact" ? (
                                                      <div
                                                        data-bs-toggle="tooltip"
                                                        title="Edit"
                                                      >
                                                        <a
                                                          className="nav-link py-0"
                                                          href="#lastName-collapse"
                                                          data-bs-toggle="collapse"
                                                        >
                                                          <i className="fi-edit me-2"></i>
                                                        </a>
                                                      </div>
                                                    ) : (
                                                      ""
                                                    )
                                                  )
                                                : "")}
                                          </>
                                        )}
                                      </div>
                                      <div
                                        className="collapse"
                                        id="lastName-collapse"
                                        data-bs-parent="#auth-info"
                                      >
                                        <input
                                          className="form-control mt-3"
                                          type="lastName"
                                          name="lastName"
                                          placeholder="lastName"
                                          value={getContact.lastName}
                                          onChange={handleChange}
                                          data-bs-binded-element="#lastName-value"
                                          data-bs-unset-value="Not specified"
                                        />
                                      </div>
                                    </div>

                                    <div className="border-bottom pb-3 mb-3">
                                      <div className="d-flex align-items-center justify-content-between">
                                        <div className="pe-2">
                                          <label className="col-form-label">
                                            <span>
                                              <i
                                                className="fa fa-user me-2"
                                                aria-hidden="true"
                                              ></i>
                                              Nickname
                                            </span>
                                          </label>
                                          <div id="nickname-value">
                                            {getContact.nickname}
                                          </div>
                                        </div>

                                        {localStorage.getItem("auth") &&
                                        (jwt(localStorage.getItem("auth"))
                                          .contactType == "admin" ||
                                          jwt(localStorage.getItem("auth"))
                                            .id == params.id) ? (
                                          <div
                                            data-bs-toggle="tooltip"
                                            title="Edit"
                                          >
                                            <a
                                              className="nav-link py-0"
                                              href="#nickname-collapse"
                                              data-bs-toggle="collapse"
                                            >
                                              <i className="fi-edit me-2"></i>
                                            </a>
                                          </div>
                                        ) : (
                                          <>
                                            {localStorage.getItem("auth") &&
                                              jwt(localStorage.getItem("auth"))
                                                .contactType == "staff" &&
                                              (Array.isArray(formData) &&
                                              formData.length > 0
                                                ? formData.map((item) =>
                                                    item.roleStaff ===
                                                    "add_contact" ? (
                                                      <div
                                                        data-bs-toggle="tooltip"
                                                        title="Edit"
                                                      >
                                                        <a
                                                          className="nav-link py-0"
                                                          href="#nickname-collapse"
                                                          data-bs-toggle="collapse"
                                                        >
                                                          <i className="fi-edit me-2"></i>
                                                        </a>
                                                      </div>
                                                    ) : (
                                                      ""
                                                    )
                                                  )
                                                : "")}
                                          </>
                                        )}
                                      </div>
                                      <div
                                        className="collapse"
                                        id="nickname-collapse"
                                        data-bs-parent="#auth-info"
                                      >
                                        <input
                                          className="form-control mt-3"
                                          type="nickname"
                                          name="nickname"
                                          placeholder="nickname"
                                          value={getContact.nickname}
                                          onChange={handleChange}
                                          data-bs-binded-element="#nickname-value"
                                          data-bs-unset-value="Not specified"
                                        />
                                      </div>
                                    </div>

                                    <div className="border-bottom pb-3 mb-3">
                                      <div className="d-flex align-items-center justify-content-between">
                                        <div className="pe-2">
                                          <label
                                            for="staticEmail"
                                            className="col-form-label"
                                          >
                                            <span>
                                              <i
                                                className="fa fa-at me-2"
                                                aria-hidden="true"
                                              ></i>
                                              Email
                                            </span>
                                          </label>
                                          <div id="email-value">
                                            {getContact.email}
                                          </div>
                                        </div>

                                        {localStorage.getItem("auth") &&
                                        (jwt(localStorage.getItem("auth"))
                                          .contactType == "admin" ||
                                          jwt(localStorage.getItem("auth"))
                                            .id == params.id) ? (
                                          <div
                                            data-bs-toggle="tooltip"
                                            title="Edit"
                                          >
                                            <a
                                              className="nav-link py-0"
                                              href="#email-collapse"
                                              data-bs-toggle="collapse"
                                            >
                                              <i className="fi-edit me-2"></i>
                                            </a>
                                          </div>
                                        ) : (
                                          <>
                                            {localStorage.getItem("auth") &&
                                              jwt(localStorage.getItem("auth"))
                                                .contactType == "staff" &&
                                              (Array.isArray(formData) &&
                                              formData.length > 0
                                                ? formData.map((item) =>
                                                    item.roleStaff ===
                                                    "add_contact" ? (
                                                      <div
                                                        data-bs-toggle="tooltip"
                                                        title="Edit"
                                                      >
                                                        <a
                                                          className="nav-link py-0"
                                                          href="#email-collapse"
                                                          data-bs-toggle="collapse"
                                                        >
                                                          <i className="fi-edit me-2"></i>
                                                        </a>
                                                      </div>
                                                    ) : (
                                                      ""
                                                    )
                                                  )
                                                : "")}
                                          </>
                                        )}
                                      </div>
                                      <div
                                        className="collapse"
                                        id="email-collapse"
                                        data-bs-parent="#auth-info"
                                      >
                                        <input
                                          className="form-control mt-3"
                                          type="email"
                                          name="email"
                                          placeholder="email"
                                          value={getContact.email}
                                          onChange={handleChange}
                                          data-bs-binded-element="#email-value"
                                          data-bs-unset-value="Not specified"
                                        />
                                      </div>
                                    </div>

                                    <div className="border-bottom pb-3 mb-3">
                                      <div className="d-flex align-items-center justify-content-between">
                                        <div className="pe-2">
                                          <label
                                            for="staticEmail"
                                            className="col-form-label"
                                          >
                                            <i
                                              className="fa fa-phone-square me-2"
                                              aria-hidden="true"
                                            ></i>
                                            <span>Phone</span>
                                          </label>
                                          <div id="phone-value">
                                            {getContact.phone}
                                          </div>
                                        </div>

                                        {localStorage.getItem("auth") &&
                                        (jwt(localStorage.getItem("auth"))
                                          .contactType == "admin" ||
                                          jwt(localStorage.getItem("auth"))
                                            .id == params.id) ? (
                                          <div
                                            data-bs-toggle="tooltip"
                                            title="Edit"
                                          >
                                            <a
                                              className="nav-link py-0"
                                              href="#phone-collapse"
                                              data-bs-toggle="collapse"
                                            >
                                              <i className="fi-edit me-2"></i>
                                            </a>
                                          </div>
                                        ) : (
                                          <>
                                            {localStorage.getItem("auth") &&
                                              jwt(localStorage.getItem("auth"))
                                                .contactType == "staff" &&
                                              (Array.isArray(formData) &&
                                              formData.length > 0
                                                ? formData.map((item) =>
                                                    item.roleStaff ===
                                                    "add_contact" ? (
                                                      <div
                                                        data-bs-toggle="tooltip"
                                                        title="Edit"
                                                      >
                                                        <a
                                                          className="nav-link py-0"
                                                          href="#phone-collapse"
                                                          data-bs-toggle="collapse"
                                                        >
                                                          <i className="fi-edit me-2"></i>
                                                        </a>
                                                      </div>
                                                    ) : (
                                                      ""
                                                    )
                                                  )
                                                : "")}
                                          </>
                                        )}
                                      </div>
                                      <div
                                        className="collapse"
                                        id="phone-collapse"
                                        data-bs-parent="#personal-details"
                                      >
                                        <input
                                          className="form-control mt-3"
                                          type="number"
                                          name="phone"
                                          placeholder="phone"
                                          value={getContact.phone}
                                          onChange={handleChange}
                                          data-bs-binded-element="#phone-value"
                                          data-bs-unset-value="Not specified"
                                        />
                                      </div>
                                    </div>

                                    {localStorage.getItem("auth") &&
                                    (jwt(localStorage.getItem("auth"))
                                      .contactType == "admin" ||
                                      jwt(localStorage.getItem("auth")).id ==
                                        params.id) ? (
                                      <div className="border-bottom pb-3 mb-3">
                                        <div className="d-flex align-items-center justify-content-between">
                                          <div className="pe-2">
                                            <label
                                              for="staticEmail"
                                              className="col-form-label"
                                            >
                                              <i
                                                className="fa fa-map-marker me-2"
                                                aria-hidden="true"
                                              ></i>
                                              <span>Address</span>
                                            </label>

                                            <div id="address-value">
                                              {getContact.address}
                                            </div>
                                          </div>
                                          {localStorage.getItem("auth") &&
                                          (jwt(localStorage.getItem("auth"))
                                            .contactType == "admin" ||
                                            jwt(localStorage.getItem("auth"))
                                              .id == params.id) ? (
                                            <div
                                              data-bs-toggle="tooltip"
                                              title="Edit"
                                            >
                                              <a
                                                className="nav-link py-0"
                                                href="#address-collapse"
                                                data-bs-toggle="collapse"
                                              >
                                                <i className="fi-edit me-2"></i>
                                              </a>
                                            </div>
                                          ) : (
                                            ""
                                          )}
                                        </div>
                                        <div
                                          className="collapse"
                                          id="address-collapse"
                                          data-bs-parent="#personal-details"
                                        >
                                          <input
                                            className="form-control mt-3"
                                            type="text"
                                            name="address"
                                            value={getContact.address}
                                            onChange={handleChange}
                                            data-bs-binded-element="#address-value"
                                            data-bs-unset-value="Not specified"
                                            placeholder="Enter address"
                                          />
                                        </div>
                                      </div>
                                    ) : (
                                      <>
                                        {localStorage.getItem("auth") &&
                                          jwt(localStorage.getItem("auth"))
                                            .contactType == "staff" &&
                                          (Array.isArray(formData) &&
                                          formData.length > 0
                                            ? formData.map((item) =>
                                                item.roleStaff ===
                                                "add_contact" ? (
                                                  <div className="border-bottom pb-3 mb-3">
                                                    <div className="d-flex align-items-center justify-content-between">
                                                      <div className="pe-2">
                                                        <label
                                                          for="staticEmail"
                                                          className="col-form-label"
                                                        >
                                                          <i
                                                            className="fa fa-map-marker me-2"
                                                            aria-hidden="true"
                                                          ></i>
                                                          <span>Address</span>
                                                        </label>

                                                        <div id="address-value">
                                                          {getContact.address}
                                                        </div>
                                                      </div>
                                                      <>
                                                        {localStorage.getItem(
                                                          "auth"
                                                        ) &&
                                                          jwt(
                                                            localStorage.getItem(
                                                              "auth"
                                                            )
                                                          ).contactType ==
                                                            "staff" &&
                                                          (Array.isArray(
                                                            formData
                                                          ) &&
                                                          formData.length > 0
                                                            ? formData.map(
                                                                (item) =>
                                                                  item.roleStaff ===
                                                                  "add_contact" ? (
                                                                    <div
                                                                      data-bs-toggle="tooltip"
                                                                      title="Edit"
                                                                    >
                                                                      <a
                                                                        className="nav-link py-0"
                                                                        href="#address-collapse"
                                                                        data-bs-toggle="collapse"
                                                                      >
                                                                        <i className="fi-edit me-2"></i>
                                                                      </a>
                                                                    </div>
                                                                  ) : (
                                                                    ""
                                                                  )
                                                              )
                                                            : "")}
                                                      </>
                                                    </div>
                                                    <div
                                                      className="collapse"
                                                      id="address-collapse"
                                                      data-bs-parent="#personal-details"
                                                    >
                                                      <input
                                                        className="form-control mt-3"
                                                        type="text"
                                                        name="address"
                                                        value={
                                                          getContact.address
                                                        }
                                                        onChange={handleChange}
                                                        data-bs-binded-element="#address-value"
                                                        data-bs-unset-value="Not specified"
                                                        placeholder="Enter address"
                                                      />
                                                    </div>
                                                  </div>
                                                ) : (
                                                  ""
                                                )
                                              )
                                            : "")}
                                      </>
                                    )}


                                     {localStorage.getItem("auth") &&
                                    (jwt(localStorage.getItem("auth"))
                                      .contactType == "admin" ||
                                      jwt(localStorage.getItem("auth")).id ==
                                        params.id) ? (
                                      <div className="border-bottom pb-3 mb-3">
                                        <div className="d-flex align-items-center justify-content-between">
                                          <div className="pe-2">
                                            <label
                                              for="staticEmail"
                                              className="col-form-label"
                                            >
                                             <i class="fa fa-map me-2" aria-hidden="true"></i>
                                              <span>City</span>
                                            </label>

                                            <div id="city-value">
                                              {getContact.city}
                                            </div>
                                          </div>
                                          {localStorage.getItem("auth") &&
                                          (jwt(localStorage.getItem("auth"))
                                            .contactType == "admin" ||
                                            jwt(localStorage.getItem("auth"))
                                              .id == params.id) ? (
                                            <div
                                              data-bs-toggle="tooltip"
                                              title="Edit"
                                            >
                                              <a
                                                className="nav-link py-0"
                                                href="#city-collapse"
                                                data-bs-toggle="collapse"
                                              >
                                                <i className="fi-edit me-2"></i>
                                              </a>
                                            </div>
                                          ) : (
                                            ""
                                          )}
                                        </div>
                                        <div
                                          className="collapse"
                                          id="city-collapse"
                                          data-bs-parent="#personal-details"
                                        >
                                          <input
                                            className="form-control mt-3"
                                            type="text"
                                            name="city"
                                            value={getContact.city}
                                            onChange={handleChange}
                                            data-bs-binded-element="#city-value"
                                            data-bs-unset-value="Not specified"
                                            placeholder="Enter city"
                                          />
                                        </div>
                                      </div>
                                    ) : (
                                      <>
                                        {localStorage.getItem("auth") &&
                                          jwt(localStorage.getItem("auth"))
                                            .contactType == "staff" &&
                                          (Array.isArray(formData) &&
                                          formData.length > 0
                                            ? formData.map((item) =>
                                                item.roleStaff ===
                                                "add_contact" ? (
                                                  <div className="border-bottom pb-3 mb-3">
                                                    <div className="d-flex align-items-center justify-content-between">
                                                      <div className="pe-2">
                                                        <label
                                                          for="staticEmail"
                                                          className="col-form-label"
                                                        >
                                                     <i class="fa fa-map me-2" aria-hidden="true"></i>
                                                          <span>City</span>
                                                        </label>

                                                        <div id="city-value">
                                                          {getContact.city}
                                                        </div>
                                                      </div>
                                                      <>
                                                        {localStorage.getItem(
                                                          "auth"
                                                        ) &&
                                                          jwt(
                                                            localStorage.getItem(
                                                              "auth"
                                                            )
                                                          ).contactType ==
                                                            "staff" &&
                                                          (Array.isArray(
                                                            formData
                                                          ) &&
                                                          formData.length > 0
                                                            ? formData.map(
                                                                (item) =>
                                                                  item.roleStaff ===
                                                                  "add_contact" ? (
                                                                    <div
                                                                      data-bs-toggle="tooltip"
                                                                      title="Edit"
                                                                    >
                                                                      <a
                                                                        className="nav-link py-0"
                                                                        href="#city-collapse"
                                                                        data-bs-toggle="collapse"
                                                                      >
                                                                        <i className="fi-edit me-2"></i>
                                                                      </a>
                                                                    </div>
                                                                  ) : (
                                                                    ""
                                                                  )
                                                              )
                                                            : "")}
                                                      </>
                                                    </div>
                                                    <div
                                                      className="collapse"
                                                      id="city-collapse"
                                                      data-bs-parent="#personal-details"
                                                    >
                                                      <input
                                                        className="form-control mt-3"
                                                        type="text"
                                                        name="city"
                                                        value={
                                                          getContact.city
                                                        }
                                                        onChange={handleChange}
                                                        data-bs-binded-element="#city-value"
                                                        data-bs-unset-value="Not specified"
                                                        placeholder="Enter city"
                                                      />
                                                    </div>
                                                  </div>
                                                ) : (
                                                  ""
                                                )
                                              )
                                            : "")}
                                      </>
                                    )}

                                    {localStorage.getItem("auth") &&
                                    (jwt(localStorage.getItem("auth"))
                                      .contactType == "admin" ||
                                      jwt(localStorage.getItem("auth")).id ==
                                        params.id) ? (
                                      <div className="border-bottom pb-3 mb-3">
                                        <div className="d-flex align-items-center justify-content-between">
                                          <div className="pe-2">
                                            <label
                                              for="staticEmail"
                                              className="col-form-label"
                                            >
                                           <i class="fa fa-location-arrow me-2" aria-hidden="true"></i>
                                              <span>State</span>
                                            </label>

                                            <div id="state-value">
                                              {getContact.state}
                                            </div>
                                          </div>
                                          {localStorage.getItem("auth") &&
                                          (jwt(localStorage.getItem("auth"))
                                            .contactType == "admin" ||
                                            jwt(localStorage.getItem("auth"))
                                              .id == params.id) ? (
                                            <div
                                              data-bs-toggle="tooltip"
                                              title="Edit"
                                            >
                                              <a
                                                className="nav-link py-0"
                                                href="#state-collapse"
                                                data-bs-toggle="collapse"
                                              >
                                                <i className="fi-edit me-2"></i>
                                              </a>
                                            </div>
                                          ) : (
                                            ""
                                          )}
                                        </div>
                                        <div
                                          className="collapse"
                                          id="state-collapse"
                                          data-bs-parent="#personal-details"
                                        >
                                          <input
                                            className="form-control mt-3"
                                            type="text"
                                            name="state"
                                            value={getContact.state}
                                            onChange={handleChange}
                                            data-bs-binded-element="#state-value"
                                            data-bs-unset-value="Not specified"
                                            placeholder="Enter state"
                                          />
                                        </div>
                                      </div>
                                    ) : (
                                      <>
                                        {localStorage.getItem("auth") &&
                                          jwt(localStorage.getItem("auth"))
                                            .contactType == "staff" &&
                                          (Array.isArray(formData) &&
                                          formData.length > 0
                                            ? formData.map((item) =>
                                                item.roleStaff ===
                                                "add_contact" ? (
                                                  <div className="border-bottom pb-3 mb-3">
                                                    <div className="d-flex align-items-center justify-content-between">
                                                      <div className="pe-2">
                                                        <label
                                                          for="staticEmail"
                                                          className="col-form-label"
                                                        >
                                                     <i class="fa fa-location-arrow me-2" aria-hidden="true"></i>
                                                          <span>State</span>
                                                        </label>

                                                        <div id="state-value">
                                                          {getContact.state}
                                                        </div>
                                                      </div>
                                                      <>
                                                        {localStorage.getItem(
                                                          "auth"
                                                        ) &&
                                                          jwt(
                                                            localStorage.getItem(
                                                              "auth"
                                                            )
                                                          ).contactType ==
                                                            "staff" &&
                                                          (Array.isArray(
                                                            formData
                                                          ) &&
                                                          formData.length > 0
                                                            ? formData.map(
                                                                (item) =>
                                                                  item.roleStaff ===
                                                                  "add_contact" ? (
                                                                    <div
                                                                      data-bs-toggle="tooltip"
                                                                      title="Edit"
                                                                    >
                                                                      <a
                                                                        className="nav-link py-0"
                                                                        href="#state-collapse"
                                                                        data-bs-toggle="collapse"
                                                                      >
                                                                        <i className="fi-edit me-2"></i>
                                                                      </a>
                                                                    </div>
                                                                  ) : (
                                                                    ""
                                                                  )
                                                              )
                                                            : "")}
                                                      </>
                                                    </div>
                                                    <div
                                                      className="collapse"
                                                      id="state-collapse"
                                                      data-bs-parent="#personal-details"
                                                    >
                                                      <input
                                                        className="form-control mt-3"
                                                        type="text"
                                                        name="state"
                                                        value={
                                                          getContact.state
                                                        }
                                                        onChange={handleChange}
                                                        data-bs-binded-element="#state-value"
                                                        data-bs-unset-value="Not specified"
                                                        placeholder="Enter state"
                                                      />
                                                    </div>
                                                  </div>
                                                ) : (
                                                  ""
                                                )
                                              )
                                            : "")}
                                      </>
                                    )}


                                   {localStorage.getItem("auth") &&
                                    (jwt(localStorage.getItem("auth"))
                                      .contactType == "admin" ||
                                      jwt(localStorage.getItem("auth")).id ==
                                        params.id) ? (
                                      <div className="border-bottom pb-3 mb-3">
                                        <div className="d-flex align-items-center justify-content-between">
                                          <div className="pe-2">
                                            <label
                                              for="staticEmail"
                                              className="col-form-label"
                                            >
                                          <i class="fa fa-map-signs me-2" aria-hidden="true"></i>
                                              <span>Zipcode</span>
                                            </label>

                                            <div id="zip-value">
                                              {getContact.zip}
                                            </div>
                                          </div>
                                          {localStorage.getItem("auth") &&
                                          (jwt(localStorage.getItem("auth"))
                                            .contactType == "admin" ||
                                            jwt(localStorage.getItem("auth"))
                                              .id == params.id) ? (
                                            <div
                                              data-bs-toggle="tooltip"
                                              title="Edit"
                                            >
                                              <a
                                                className="nav-link py-0"
                                                href="#zip-collapse"
                                                data-bs-toggle="collapse"
                                              >
                                                <i className="fi-edit me-2"></i>
                                              </a>
                                            </div>
                                          ) : (
                                            ""
                                          )}
                                        </div>
                                        <div
                                          className="collapse"
                                          id="zip-collapse"
                                          data-bs-parent="#personal-details"
                                        >
                                          <input
                                            className="form-control mt-3"
                                            type="text"
                                            name="zip"
                                            value={getContact.zip}
                                            onChange={handleChange}
                                            data-bs-binded-element="#zip-value"
                                            data-bs-unset-value="Not specified"
                                            placeholder="Enter zip-code"
                                          />
                                        </div>
                                      </div>
                                    ) : (
                                      <>
                                        {localStorage.getItem("auth") &&
                                          jwt(localStorage.getItem("auth"))
                                            .contactType == "staff" &&
                                          (Array.isArray(formData) &&
                                          formData.length > 0
                                            ? formData.map((item) =>
                                                item.roleStaff ===
                                                "add_contact" ? (
                                                  <div className="border-bottom pb-3 mb-3">
                                                    <div className="d-flex align-items-center justify-content-between">
                                                      <div className="pe-2">
                                                        <label
                                                          for="staticEmail"
                                                          className="col-form-label"
                                                        >
                                                         <i class="fa fa-map-signs me-2" aria-hidden="true"></i>
                                                          <span>Zipcode</span>
                                                        </label>

                                                        <div id="zip-value">
                                                          {getContact.zip}
                                                        </div>
                                                      </div>
                                                      <>
                                                        {localStorage.getItem(
                                                          "auth"
                                                        ) &&
                                                          jwt(
                                                            localStorage.getItem(
                                                              "auth"
                                                            )
                                                          ).contactType ==
                                                            "staff" &&
                                                          (Array.isArray(
                                                            formData
                                                          ) &&
                                                          formData.length > 0
                                                            ? formData.map(
                                                                (item) =>
                                                                  item.roleStaff ===
                                                                  "add_contact" ? (
                                                                    <div
                                                                      data-bs-toggle="tooltip"
                                                                      title="Edit"
                                                                    >
                                                                      <a
                                                                        className="nav-link py-0"
                                                                        href="#zip-collapse"
                                                                        data-bs-toggle="collapse"
                                                                      >
                                                                        <i className="fi-edit me-2"></i>
                                                                      </a>
                                                                    </div>
                                                                  ) : (
                                                                    ""
                                                                  )
                                                              )
                                                            : "")}
                                                      </>
                                                    </div>
                                                    <div
                                                      className="collapse"
                                                      id="zip-collapse"
                                                      data-bs-parent="#personal-details"
                                                    >
                                                      <input
                                                        className="form-control mt-3"
                                                        type="text"
                                                        name="zip"
                                                        value={
                                                          getContact.zip
                                                        }
                                                        onChange={handleChange}
                                                        data-bs-binded-element="#zip-value"
                                                        data-bs-unset-value="Not specified"
                                                        placeholder="Enter zip-code"
                                                      />
                                                    </div>
                                                  </div>
                                                ) : (
                                                  ""
                                                )
                                              )
                                            : "")}
                                      </>
                                    )}


                                    <div className="border-bottom pb-3 mb-3">
                                      <div className="d-flex align-items-center justify-content-between">
                                        <div className="pe-2">
                                          <label
                                            for="staticEmail"
                                            className="col-form-label"
                                          >
                                            <i
                                              className="fa fa-chrome me-2"
                                              aria-hidden="true"
                                            ></i>
                                            <span>Web</span>
                                          </label>

                                          <div id="web-value">
                                            {getContact.web}
                                          </div>
                                        </div>
                                        {localStorage.getItem("auth") &&
                                        (jwt(localStorage.getItem("auth"))
                                          .contactType == "admin" ||
                                          jwt(localStorage.getItem("auth"))
                                            .id == params.id) ? (
                                          <div
                                            data-bs-toggle="tooltip"
                                            title="Edit"
                                          >
                                            <a
                                              className="nav-link py-0"
                                              href="#web-collapse"
                                              data-bs-toggle="collapse"
                                            >
                                              <i className="fi-edit me-2"></i>
                                            </a>
                                          </div>
                                        ) : (
                                          <>
                                            {localStorage.getItem("auth") &&
                                              jwt(localStorage.getItem("auth"))
                                                .contactType == "staff" &&
                                              (Array.isArray(formData) &&
                                              formData.length > 0
                                                ? formData.map((item) =>
                                                    item.roleStaff ===
                                                    "add_contact" ? (
                                                      <div
                                                        data-bs-toggle="tooltip"
                                                        title="Edit"
                                                      >
                                                        <a
                                                          className="nav-link py-0"
                                                          href="#web-collapse"
                                                          data-bs-toggle="collapse"
                                                        >
                                                          <i className="fi-edit me-2"></i>
                                                        </a>
                                                      </div>
                                                    ) : (
                                                      ""
                                                    )
                                                  )
                                                : "")}
                                          </>
                                        )}
                                      </div>
                                      <div
                                        className="collapse"
                                        id="web-collapse"
                                        data-bs-parent="#personal-details"
                                      >
                                        <input
                                          className="form-control mt-3"
                                          type="text"
                                          name="web"
                                          value={getContact.web}
                                          onChange={handleChange}
                                          data-bs-binded-element="#web-value"
                                          data-bs-unset-value="Not specified"
                                          placeholder="Enter Website"
                                        />
                                      </div>
                                    </div>

                                    {localStorage.getItem("auth") &&
                                    (jwt(localStorage.getItem("auth"))
                                      .contactType == "admin" ||
                                      jwt(localStorage.getItem("auth")).id ==
                                        params.id) ? (
                                      <>
                                        <div className="border-bottom pb-3 mb-3">
                                          <div className="d-flex align-items-center justify-content-between">
                                            <div className="pe-2">
                                              <label
                                                for="staticEmail"
                                                className="col-form-label"
                                              >
                                                <i
                                                  className="fa fa-fax me-2"
                                                  aria-hidden="true"
                                                ></i>
                                                <span>Fax</span>
                                              </label>

                                              <div id="fax-value">
                                                {getContact.fax}
                                              </div>
                                            </div>
                                            {localStorage.getItem("auth") &&
                                            (jwt(localStorage.getItem("auth"))
                                              .contactType == "admin" ||
                                              jwt(localStorage.getItem("auth"))
                                                .id == params.id) ? (
                                              <div
                                                data-bs-toggle="tooltip"
                                                title="Edit"
                                              >
                                                <a
                                                  className="nav-link py-0"
                                                  href="#fax-collapse"
                                                  data-bs-toggle="collapse"
                                                >
                                                  <i className="fi-edit me-2"></i>
                                                </a>
                                              </div>
                                            ) : (
                                              ""
                                            )}
                                          </div>
                                          <div
                                            className="collapse"
                                            id="fax-collapse"
                                            data-bs-parent="#personal-details"
                                          >
                                            <input
                                              className="form-control mt-3"
                                              type="text"
                                              name="fax"
                                              value={getContact.fax}
                                              onChange={handleChange}
                                              data-bs-binded-element="#fax-value"
                                              data-bs-unset-value="Not specified"
                                              placeholder="Enter Fax NO"
                                            />
                                          </div>
                                        </div>

                                        <div className="border-bottom pb-3 mb-3">
                                          <div className="d-flex align-items-center justify-content-between">
                                            <div className="pe-2">
                                              <label
                                                for="staticEmail"
                                                className="col-form-label"
                                              >
                                                <i
                                                  className="fa fa-inbox me-2"
                                                  aria-hidden="true"
                                                ></i>
                                                <span>Inbox</span>
                                              </label>

                                              <div id="inbox-value">
                                                {getContact.inbox}
                                              </div>
                                            </div>
                                            {localStorage.getItem("auth") &&
                                            (jwt(localStorage.getItem("auth"))
                                              .contactType == "admin" ||
                                              jwt(localStorage.getItem("auth"))
                                                .id == params.id) ? (
                                              <div
                                                data-bs-toggle="tooltip"
                                                title="Edit"
                                              >
                                                <a
                                                  className="nav-link py-0"
                                                  href="#inbox-collapse"
                                                  data-bs-toggle="collapse"
                                                >
                                                  <i className="fi-edit me-2"></i>
                                                </a>
                                              </div>
                                            ) : (
                                              ""
                                            )}
                                          </div>
                                          <div
                                            className="collapse"
                                            id="inbox-collapse"
                                            data-bs-parent="#personal-details"
                                          >
                                            <input
                                              className="form-control mt-3"
                                              type="text"
                                              name="inbox"
                                              value={getContact.inbox}
                                              onChange={handleChange}
                                              data-bs-binded-element="#inbox-value"
                                              data-bs-unset-value="Not specified"
                                              placeholder="Enter message"
                                            />
                                          </div>
                                        </div>
                                      </>
                                    ) : (
                                      <>
                                        {localStorage.getItem("auth") &&
                                          jwt(localStorage.getItem("auth"))
                                            .contactType == "staff" &&
                                          (Array.isArray(formData) &&
                                          formData.length > 0
                                            ? formData.map((item) =>
                                                item.roleStaff ===
                                                "add_contact" ? (
                                                  <>
                                                    <div className="border-bottom pb-3 mb-3">
                                                      <div className="d-flex align-items-center justify-content-between">
                                                        <div className="pe-2">
                                                          <label
                                                            for="staticEmail"
                                                            className="col-form-label"
                                                          >
                                                            <i
                                                              className="fa fa-fax me-2"
                                                              aria-hidden="true"
                                                            ></i>
                                                            <span>Fax</span>
                                                          </label>

                                                          <div id="fax-value">
                                                            {getContact.fax}
                                                          </div>
                                                        </div>
                                                        {localStorage.getItem(
                                                          "auth"
                                                        ) &&
                                                          jwt(
                                                            localStorage.getItem(
                                                              "auth"
                                                            )
                                                          ).contactType ==
                                                            "staff" &&
                                                          (Array.isArray(
                                                            formData
                                                          ) &&
                                                          formData.length > 0
                                                            ? formData.map(
                                                                (item) =>
                                                                  item.roleStaff ===
                                                                  "add_contact" ? (
                                                                    <div
                                                                      data-bs-toggle="tooltip"
                                                                      title="Edit"
                                                                    >
                                                                      <a
                                                                        className="nav-link py-0"
                                                                        href="#fax-collapse"
                                                                        data-bs-toggle="collapse"
                                                                      >
                                                                        <i className="fi-edit me-2"></i>
                                                                      </a>
                                                                    </div>
                                                                  ) : (
                                                                    ""
                                                                  )
                                                              )
                                                            : "")}
                                                      </div>
                                                      <div
                                                        className="collapse"
                                                        id="fax-collapse"
                                                        data-bs-parent="#personal-details"
                                                      >
                                                        <input
                                                          className="form-control mt-3"
                                                          type="text"
                                                          name="fax"
                                                          value={getContact.fax}
                                                          onChange={
                                                            handleChange
                                                          }
                                                          data-bs-binded-element="#fax-value"
                                                          data-bs-unset-value="Not specified"
                                                          placeholder="Enter Fax NO"
                                                        />
                                                      </div>
                                                    </div>

                                                    <div className="border-bottom pb-3 mb-3">
                                                      <div className="d-flex align-items-center justify-content-between">
                                                        <div className="pe-2">
                                                          <label
                                                            for="staticEmail"
                                                            className="col-form-label"
                                                          >
                                                            <i
                                                              className="fa fa-inbox me-2"
                                                              aria-hidden="true"
                                                            ></i>
                                                            <span>Inbox</span>
                                                          </label>

                                                          <div id="inbox-value">
                                                            {getContact.inbox}
                                                          </div>
                                                        </div>
                                                        {localStorage.getItem(
                                                          "auth"
                                                        ) &&
                                                        (jwt(
                                                          localStorage.getItem(
                                                            "auth"
                                                          )
                                                        ).contactType ==
                                                          "admin" ||
                                                          jwt(
                                                            localStorage.getItem(
                                                              "auth"
                                                            )
                                                          ).id == params.id) ? (
                                                          <div
                                                            data-bs-toggle="tooltip"
                                                            title="Edit"
                                                          >
                                                            <a
                                                              className="nav-link py-0"
                                                              href="#inbox-collapse"
                                                              data-bs-toggle="collapse"
                                                            >
                                                              <i className="fi-edit me-2"></i>
                                                            </a>
                                                          </div>
                                                        ) : (
                                                          <>
                                                            {localStorage.getItem(
                                                              "auth"
                                                            ) &&
                                                              jwt(
                                                                localStorage.getItem(
                                                                  "auth"
                                                                )
                                                              ).contactType ==
                                                                "staff" &&
                                                              (Array.isArray(
                                                                formData
                                                              ) &&
                                                              formData.length >
                                                                0
                                                                ? formData.map(
                                                                    (item) =>
                                                                      item.roleStaff ===
                                                                      "add_contact" ? (
                                                                        <div
                                                                          data-bs-toggle="tooltip"
                                                                          title="Edit"
                                                                        >
                                                                          <a
                                                                            className="nav-link py-0"
                                                                            href="#inbox-collapse"
                                                                            data-bs-toggle="collapse"
                                                                          >
                                                                            <i className="fi-edit me-2"></i>
                                                                          </a>
                                                                        </div>
                                                                      ) : (
                                                                        ""
                                                                      )
                                                                  )
                                                                : "")}
                                                          </>
                                                        )}
                                                      </div>
                                                      <div
                                                        className="collapse"
                                                        id="inbox-collapse"
                                                        data-bs-parent="#personal-details"
                                                      >
                                                        <input
                                                          className="form-control mt-3"
                                                          type="text"
                                                          name="inbox"
                                                          value={
                                                            getContact.inbox
                                                          }
                                                          onChange={
                                                            handleChange
                                                          }
                                                          data-bs-binded-element="#inbox-value"
                                                          data-bs-unset-value="Not specified"
                                                          placeholder="Enter message"
                                                        />
                                                      </div>
                                                    </div>
                                                  </>
                                                ) : (
                                                  ""
                                                )
                                              )
                                            : "")}
                                      </>
                                    )}
                                  </div>
                                </div>

                                <div className="d-flex align-items-center justify-content-between float-left mb-3 mt-3">
                                  <h6
                                    className="text-white text-left mb-0"
                                    id="info"
                                  >
                                    Profile Info
                                  </h6>
                                </div>
                                <div className="">
                                  <div
                                    className="border rounded-3 bg-light p-3"
                                    id="auth-info"
                                  >
                                    <div className="border-bottom pb-3 mb-3">
                                      <div className="d-flex align-items-center justify-content-between">
                                        <div className="pe-2">
                                          <label className="col-form-label">
                                            <span>
                                              <i
                                                className="fa fa-sitemap me-2"
                                                aria-hidden="true"
                                              ></i>
                                              Organization
                                            </span>
                                          </label>
                                          <div id="organization-value">
                                            {getContact.organization}
                                          </div>
                                        </div>
                                        {localStorage.getItem("auth") &&
                                        (jwt(localStorage.getItem("auth"))
                                          .contactType == "admin" ||
                                          jwt(localStorage.getItem("auth"))
                                            .id == params.id) ? (
                                          <div
                                            data-bs-toggle="tooltip"
                                            title="Edit"
                                          >
                                            <a
                                              className="nav-link py-0"
                                              href="#organization-collapse"
                                              data-bs-toggle="collapse"
                                            >
                                              <i className="fi-edit me-2"></i>
                                            </a>
                                          </div>
                                        ) : (
                                          <>
                                            {localStorage.getItem("auth") &&
                                              jwt(localStorage.getItem("auth"))
                                                .contactType == "staff" &&
                                              (Array.isArray(formData) &&
                                              formData.length > 0
                                                ? formData.map((item) =>
                                                    item.roleStaff ===
                                                    "add_contact" ? (
                                                      <div
                                                        data-bs-toggle="tooltip"
                                                        title="Edit"
                                                      >
                                                        <a
                                                          className="nav-link py-0"
                                                          href="#organization-collapse"
                                                          data-bs-toggle="collapse"
                                                        >
                                                          <i className="fi-edit me-2"></i>
                                                        </a>
                                                      </div>
                                                    ) : (
                                                      ""
                                                    )
                                                  )
                                                : "")}
                                          </>
                                        )}
                                      </div>
                                      <div
                                        className="collapse"
                                        id="organization-collapse"
                                        data-bs-parent="#auth-info"
                                      >
                                        <input
                                          className="form-control mt-3"
                                          type="organization"
                                          name="organization"
                                          placeholder="organization"
                                          value={getContact.organization}
                                          onChange={handleChange}
                                          data-bs-binded-element="#organization-value"
                                          data-bs-unset-value="Not specified"
                                        />
                                      </div>
                                    </div>

                                    <div className="border-bottom pb-3 mb-3">
                                      <div className="d-flex align-items-center justify-content-between">
                                        <div className="pe-2">
                                          <label className="col-form-label">
                                            <span>
                                              <i
                                                className="fa fa-building me-2"
                                                aria-hidden="true"
                                              ></i>
                                              Department
                                            </span>
                                          </label>
                                          <div id="department-value">
                                            {getContact.department}
                                          </div>
                                        </div>
                                        {localStorage.getItem("auth") &&
                                        (jwt(localStorage.getItem("auth"))
                                          .contactType == "admin" ||
                                          jwt(localStorage.getItem("auth"))
                                            .id == params.id) ? (
                                          <div
                                            data-bs-toggle="tooltip"
                                            title="Edit"
                                          >
                                            <a
                                              className="nav-link py-0"
                                              href="#department-collapse"
                                              data-bs-toggle="collapse"
                                            >
                                              <i className="fi-edit me-2"></i>
                                            </a>
                                          </div>
                                        ) : (
                                          <>
                                            {localStorage.getItem("auth") &&
                                              jwt(localStorage.getItem("auth"))
                                                .contactType == "staff" &&
                                              (Array.isArray(formData) &&
                                              formData.length > 0
                                                ? formData.map((item) =>
                                                    item.roleStaff ===
                                                    "add_contact" ? (
                                                      <div
                                                        data-bs-toggle="tooltip"
                                                        title="Edit"
                                                      >
                                                        <a
                                                          className="nav-link py-0"
                                                          href="#department-collapse"
                                                          data-bs-toggle="collapse"
                                                        >
                                                          <i className="fi-edit me-2"></i>
                                                        </a>
                                                      </div>
                                                    ) : (
                                                      ""
                                                    )
                                                  )
                                                : "")}
                                          </>
                                        )}
                                      </div>
                                      <div
                                        className="collapse"
                                        id="department-collapse"
                                        data-bs-parent="#auth-info"
                                      >
                                        <input
                                          className="form-control mt-3"
                                          type="department"
                                          name="department"
                                          placeholder="department"
                                          value={getContact.department}
                                          onChange={handleChange}
                                          data-bs-binded-element="#department-value"
                                          data-bs-unset-value="Not specified"
                                        />
                                      </div>
                                    </div>

                                    <div className="border-bottom pb-3 mb-3">
                                      <div className="d-flex align-items-center justify-content-between">
                                        <div className="pe-2">
                                          <label
                                            for="staticEmail"
                                            className="col-form-label"
                                          >
                                              <span className="ms-1">
                                              <i
                                                className="fa fa-text-width me-2"
                                                aria-hidden="true"
                                              ></i>
                                                Title
                                              </span>
                                           
                                          </label>
                                          <div id="title-value">
                                            {getContact.title}
                                          </div>
                                        </div>
                                        {localStorage.getItem("auth") &&
                                        (jwt(localStorage.getItem("auth"))
                                          .contactType == "admin" ||
                                          jwt(localStorage.getItem("auth"))
                                            .id == params.id) ? (
                                          <div
                                            data-bs-toggle="tooltip"
                                            title="Edit"
                                          >
                                            <a
                                              className="nav-link py-0"
                                              href="#title-collapse"
                                              data-bs-toggle="collapse"
                                            >
                                              <i className="fi-edit me-2"></i>
                                            </a>
                                          </div>
                                        ) : (
                                          <>
                                            {localStorage.getItem("auth") &&
                                              jwt(localStorage.getItem("auth"))
                                                .contactType == "staff" &&
                                              (Array.isArray(formData) &&
                                              formData.length > 0
                                                ? formData.map((item) =>
                                                    item.roleStaff ===
                                                    "add_contact" ? (
                                                      <div
                                                        data-bs-toggle="tooltip"
                                                        title="Edit"
                                                      >
                                                        <a
                                                          className="nav-link py-0"
                                                          href="#title-collapse"
                                                          data-bs-toggle="collapse"
                                                        >
                                                          <i className="fi-edit me-2"></i>
                                                        </a>
                                                      </div>
                                                    ) : (
                                                      ""
                                                    )
                                                  )
                                                : "")}
                                          </>
                                        )}
                                      </div>
                                      <div
                                        className="collapse"
                                        id="title-collapse"
                                        data-bs-parent="#auth-info"
                                      >
                                        <input
                                          className="form-control mt-3"
                                          type="title"
                                          name="title"
                                          placeholder="title"
                                          value={getContact.title}
                                          onChange={handleChange}
                                          data-bs-binded-element="#title-value"
                                          data-bs-unset-value="Not specified"
                                        />
                                      </div>
                                    </div>
                                    {socialLinks.length > 0 ? (
                                      <div className="border-bottom pb-3 mb-3">
                                        <div className="d-flex align-items-center justify-content-between">
                                          <div className="pe-2">
                                            <label
                                              htmlFor="staticEmail"
                                              className="col-form-label"
                                            >
                                              <i
                                                className="fa fa-share-square-o me-2"
                                                aria-hidden="true"
                                              ></i>
                                              <span>Social Links</span>
                                            </label>
                                            {/* <div id="social_links-value">
                                            {getContact.social_links.map(
                                              (socialLink, index) => (
                                                <div key={index}><br/>
                                                  {socialLink.linkurl}
                                                </div>
                                              )
                                            )}
                                          </div> */}
                                          </div>
                                          {localStorage.getItem("auth") &&
                                          (jwt(localStorage.getItem("auth"))
                                            .contactType === "admin" ||
                                            jwt(localStorage.getItem("auth"))
                                              .id === params.id) ? (
                                            <div
                                              data-bs-toggle="tooltip"
                                              title="Edit"
                                            >
                                              <a
                                                className="nav-link py-0"
                                                href="#social_links-collapse"
                                                data-bs-toggle="collapse"
                                              >
                                                <i className="fi-edit me-2"></i>
                                              </a>
                                            </div>
                                          ) : (
                                            <>
                                              {localStorage.getItem("auth") &&
                                                jwt(
                                                  localStorage.getItem("auth")
                                                ).contactType == "staff" &&
                                                (Array.isArray(formData) &&
                                                formData.length > 0
                                                  ? formData.map((item) =>
                                                      item.roleStaff ===
                                                      "add_contact" ? (
                                                        <div
                                                          data-bs-toggle="tooltip"
                                                          title="Edit"
                                                        >
                                                          <a
                                                            className="nav-link py-0"
                                                            href="#social_links-collapse"
                                                            data-bs-toggle="collapse"
                                                          >
                                                            <i className="fi-edit me-2"></i>
                                                          </a>
                                                        </div>
                                                      ) : (
                                                        ""
                                                      )
                                                    )
                                                  : "")}
                                            </>
                                          )}
                                        </div>
                                        <div
                                          className="collapse"
                                          id="social_links-collapse"
                                          data-bs-parent="#personal-details"
                                        >
                                          {socialLinks.map(
                                            (socialLink, index) => (
                                              <div key={index}>
                                                <label className="form-label mt-3">
                                                  {socialLink.type} URL
                                                </label>
                                                <input
                                                  className="form-control"
                                                  type="text"
                                                  name="linkurl"
                                                  value={socialLink.linkurl}
                                                  onChange={(event) =>
                                                    handleChangeSocial(
                                                      event,
                                                      index
                                                    )
                                                  }
                                                />
                                              </div>
                                            )
                                          )}
                                        </div>
                                      </div>
                                    ) : (
                                      ""
                                    )}

                                    {localStorage.getItem("auth") &&
                                    (jwt(localStorage.getItem("auth"))
                                      .contactType == "admin" ||
                                      jwt(localStorage.getItem("auth")).id ==
                                        params.id) ? (
                                      <div className="d-flex alignhange-items-center justify-content-between">
                                        <button
                                          className="btn btn-primary  px-3 px-sm-4"
                                          type="button"
                                          onClick={handleSubmit}
                                        >
                                          Save Change
                                        </button>
                                      </div>
                                    ) : (
                                      <>
                                        {localStorage.getItem("auth") &&
                                          jwt(localStorage.getItem("auth"))
                                            .contactType == "staff" &&
                                          (Array.isArray(formData) &&
                                          formData.length > 0
                                            ? formData.map((item) =>
                                                item.roleStaff ===
                                                "add_contact" ? (
                                                  <div className="d-flex alignhange-items-center justify-content-between">
                                                    <button
                                                      className="btn btn-primary  px-3 px-sm-4"
                                                      type="button"
                                                      onClick={handleSubmit}
                                                    >
                                                      Save Change
                                                    </button>
                                                  </div>
                                                ) : (
                                                  ""
                                                )
                                              )
                                            : "")}
                                      </>
                                    )}
                                  </div>
                                </div>
                              </div>
                            )}
                          </div>
                        </div>
                      </main>
                    </div>
                  ) : step1 == "2" || step1 == "201" ? (
                    <div className="float-left w-100">
                      <Loader isActive={isActive} />
                      {isShow && (
                        <Toaster
                          types={types}
                          isShow={isShow}
                          toasterBody={toasterBody}
                          message={message}
                        />
                      )}
                      {note ? (
                        <>
                          <main className="page-wrapper getContact_page_wrap">
                            <div className="container content-overlay mt-0">
                              {localStorage.getItem("auth") &&
                              (jwt(localStorage.getItem("auth")).contactType ==
                                "admin" ||
                                jwt(localStorage.getItem("auth")).id ==
                                  params.id) ? (
                                <NavLink
                                  to={`/officeNote/${params.id}`}
                                  className="btn btn-primary pull-right m-3"
                                  type="button"
                                >
                                  Add A New Account Note
                                </NavLink>
                              ) : (
                                ""
                              )}
                              <div className="bg-light shadow-sm rounded-3 p-4 p-md-5 mb-2">
                                <h4
                                  className="nav-item mb-md-0 me-md-2 pe-md-1 mb-5"
                                  id="getContact"
                                >
                                  <a
                                    className="nav-link active"
                                    href="#"
                                    aria-current="page"
                                  >
                                    Notes
                                  </a>
                                </h4>

                                <p>
                                  The following notes have been recorded by
                                  office management. Notes are only visible by
                                  associated agent and authorized office staff.
                                </p>

                                <div className="row">
                                  <div className="col-md-12">
                                    <table
                                      id="DepositLedger"
                                      className="DepositTable"
                                      width="100%"
                                      border="0"
                                      cellSpacing="0"
                                      cellPadding="0"
                                    >
                                      <thead>
                                        <tr>
                                          <th>Author</th>
                                          <th>Category</th>
                                          <th>Notes</th>
                                          <th className="hright">
                                            Date Created
                                          </th>
                                          <th className="">Remove/Edit</th>
                                        </tr>
                                      </thead>

                                      <tbody>
                                        {notesdata.data
                                          ? notesdata.data.length > 0
                                            ? notesdata.data.map((items) => {
                                                const formattedDate = new Date(
                                                  items.created
                                                ).toLocaleString("en-US", {
                                                  year: "numeric",
                                                  month: "long",
                                                  day: "numeric",
                                                  hour: "2-digit",
                                                  minute: "2-digit",
                                                  second: "2-digit",
                                                  // timeZoneName: 'short'
                                                });
                                                return (
                                                  <tr>
                                                    <td>
                                                      <span className="pull-left SuppressText">
                                                        {items.contact_name}
                                                      </span>
                                                    </td>

                                                    <td>
                                                      <span className="pull-left SuppressText">
                                                        {items.category}
                                                      </span>
                                                    </td>
                                                    <td>
                                                      <span className="pull-left SuppressText">
                                                      <b>{items.message}</b>
                                                      </span>
                                                    </td>
                                                    
                                                    <td>
                                                      <span className="pull-left SuppressText">
                                                        {formattedDate}
                                                      </span>
                                                    </td>
                                                    <td>
                                                      <span className="pull-left SuppressText m-3">
                                                        <img
                                                          onClick={(e) =>
                                                            handleRemove(
                                                              items._id
                                                            )
                                                          }
                                                          className="pull-right"
                                                          src="https://cdn.pboffice.net/shared/transaction/page_select_drop.png"
                                                        />
                                                      </span>
                                                      <span className="pull-left SuppressText m-3">
                                                      <NavLink
                                                        className="pull-left"
                                                        to={`/officeNote/${params.id}/${items._id}`}
                                                      >
                                                        <i
                                                          className="fa fa-pencil"
                                                          aria-hidden="true"
                                                        ></i>
                                                      </NavLink>
                                                      </span>
                                                    </td>

                                                    {/* <span>{items.message}</span> */}
                                                  </tr>
                                                );
                                              })
                                            : ""
                                          : ""}
                                      </tbody>
                                    </table>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </main>
                        </>
                      ) : (
                        <main className="page-wrapper getContact_page_wrap">
                          <div className="content-overlay mt-0">
                            <div className="">
                              <div className="float-left w-100 d-flex align-items-center justify-content-between mb-3">
                                <h3
                                  className="pull-left mb-0 text-white"
                                  id="getContact"
                                >
                                  Account
                                </h3>
                              </div>
                              <div className="d-flex align-items-center justify-content-between float-left mb-3">
                                <h6
                                  className="text-white text-left mb-0"
                                  id="info"
                                >
                                  Account Details
                                </h6>
                              </div>
                              <div className="">
                                <div className="">
                                  <div className="">
                                    <div
                                      className="border bg-light rounded-3 p-3"
                                      id="auth-info"
                                    >
                                      <div className="border-bottom pb-3 mb-3">
                                        <div className="d-flex align-items-center justify-content-between">
                                          <div className="pe-2">
                                            <label
                                              for="staticEmail"
                                              className="col-form-label"
                                            >
                                              <span>Membership Plan</span>
                                            </label>
                                            <div id="associate_member">
                                              {getContact.additionalActivateFields
                                                ? getContact
                                                    .additionalActivateFields
                                                    .associate_member
                                                : formValues.associate_member ||
                                                  ""}
                                            </div>
                                          </div>
                                          {localStorage.getItem("auth") &&
                                          jwt(localStorage.getItem("auth"))
                                            .contactType == "admin" ? (
                                            <div
                                              data-bs-toggle="tooltip"
                                              associate_member="Edit"
                                            >
                                              <a
                                                className="nav-link py-0"
                                                href="#associate_member-collapse"
                                                data-bs-toggle="collapse"
                                              >
                                                <i className="fi-edit"></i>
                                              </a>
                                            </div>
                                          ) : (
                                            <>
                                              {localStorage.getItem("auth") &&
                                                jwt(
                                                  localStorage.getItem("auth")
                                                ).contactType == "staff" &&
                                                (Array.isArray(formData) &&
                                                formData.length > 0
                                                  ? formData.map((item) =>
                                                      item.roleStaff ===
                                                      "add_contact" ? (
                                                        <div
                                                          data-bs-toggle="tooltip"
                                                          associate_member="Edit"
                                                        >
                                                          <a
                                                            className="nav-link py-0"
                                                            href="#associate_member-collapse"
                                                            data-bs-toggle="collapse"
                                                          >
                                                            <i className="fi-edit"></i>
                                                          </a>
                                                        </div>
                                                      ) : (
                                                        ""
                                                      )
                                                    )
                                                  : "")}
                                            </>
                                          )}
                                        </div>
                                        <div
                                          className="collapse"
                                          id="associate_member-collapse"
                                          data-bs-parent="#auth-info"
                                        >
                                          <select
                                            className="form-select mt-3"
                                            id="pr-city"
                                            name="associate_member"
                                            value={
                                              formValues.associate_member || ""
                                            }
                                            onChange={handleChanges}
                                            data-bs-binded-element="#associate_member-value"
                                            data-bs-unset-value="Not specified"
                                          >
                                            <option>Broker Plan</option>
                                            <option>Standard Plan</option>
                                          </select>
                                        </div>
                                      </div>

                                      {/* <div className="border-bottom pb-3 mb-3">
                                        <div className="d-flex align-items-center justify-content-between">
                                          <div className="pe-2">
                                            <label
                                              htmlFor="staticEmail"
                                              className="col-form-label"
                                            >
                                              <span>Membership Plan Date</span>
                                            </label>
                                            <div
                                              id="plan_date-value"
                                              className=""
                                            >
                                              {getContact.additionalActivateFields
                                                ? getFormattedDateFormat(
                                                    getContact
                                                      .additionalActivateFields
                                                      .plan_date
                                                  )
                                                : getFormattedDateFormat(
                                                    formValues.plan_date
                                                  ) || ""}
                                            </div>
                                          </div>
                                          {localStorage.getItem("auth") &&
                                          jwt(localStorage.getItem("auth"))
                                            .contactType == "admin" ? (
                                            <div
                                              data-bs-toggle="tooltip"
                                              title="Edit"
                                            >
                                              <a
                                                className="nav-link py-0"
                                                href="#plan_date-collapse"
                                                data-bs-toggle="collapse"
                                              >
                                                <i className="fi-edit"></i>
                                              </a>
                                            </div>
                                          ) : (
                                            ""
                                          )}
                                        </div>
                                        <div
                                          className="collapse"
                                          id="plan_date-collapse"
                                          data-bs-parent="#auth-info"
                                        >
                                          <input
                                            className="form-control mt-3"
                                            type="date"
                                            placeholder="Choose date"
                                            name="plan_date"
                                            value={formValues.plan_date || ""}
                                            onChange={handleChanges}
                                            data-datepicker-options='{ "altInput": true, "altFormat": "F j, Y", "dateFormat": "Y-m-d" }'
                                            data-bs-binded-element="#plan_date-value"
                                            data-bs-unset-value="Not specified"
                                          />
                                        </div>
                                      </div> */}

                                      {/* <div className="border-bottom pb-3 mb-3">
                                        <div className="d-flex align-items-center justify-content-between">
                                          <div className="pe-2 mt-2">
                                            <label
                                              for="staticEmail"
                                              className="col-form-label"
                                            >
                                              <span>Billing Date</span>
                                            </label>
                                            <div id="billing_date">
                                              {getContact.additionalActivateFields
                                                ? getFormattedDateFormat(
                                                    getContact
                                                      .additionalActivateFields
                                                      .billing_date
                                                  )
                                                : getFormattedDateFormat(
                                                    formValues.billing_date
                                                  ) || ""}
                                            </div>
                                          </div>
                                          {localStorage.getItem("auth") &&
                                          jwt(localStorage.getItem("auth"))
                                            .contactType == "admin" ? (
                                            <div
                                              data-bs-toggle="tooltip"
                                              billing_date="Edit"
                                            >
                                              <a
                                                className="nav-link py-0"
                                                href="#billing_date-collapse"
                                                data-bs-toggle="collapse"
                                              >
                                                <i className="fi-edit"></i>
                                              </a>
                                            </div>
                                          ) : (
                                            ""
                                          )}
                                        </div>
                                        <div
                                          className="collapse"
                                          id="billing_date-collapse"
                                          data-bs-parent="#auth-info"
                                        >
                                          <input
                                            className="form-control mt-3"
                                            type="date"
                                            placeholder="Choose date"
                                            name="billing_date"
                                            value={
                                              formValues.billing_date || ""
                                            }
                                            onChange={handleChanges}
                                            data-datepicker-options='{"altInput": true, "altFormat": "F j, Y", "dateFormat": "Y-m-d"}'
                                            data-bs-binded-element="#billing_date-value"
                                            data-bs-unset-value="Not specified"
                                          />
                                        </div>
                                      </div> */}

                                      <div className="border-bottom pb-3 mb-3">
                                        <div className="d-flex align-items-center justify-content-between">
                                          <div className="pe-2">
                                            <label
                                              for="staticEmail"
                                              className="col-form-label"
                                            >
                                              <span>Subscription Level</span>
                                            </label>

                                            <div id="subscription_level-value">
                                              {getContact.additionalActivateFields
                                                ? getContact
                                                    .additionalActivateFields
                                                    .subscription_level
                                                : formValues.subscription_level ||
                                                  ""}
                                            </div>
                                          </div>
                                          {localStorage.getItem("auth") &&
                                          jwt(localStorage.getItem("auth"))
                                            .contactType == "admin" ? (
                                            <div
                                              data-bs-toggle="tooltip"
                                              title="Edit"
                                            >
                                              <a
                                                className="nav-link py-0"
                                                href="#subscription_level-collapse"
                                                data-bs-toggle="collapse"
                                              >
                                                <i className="fi-edit"></i>
                                              </a>
                                            </div>
                                          ) : (
                                            <>
                                              {localStorage.getItem("auth") &&
                                                jwt(
                                                  localStorage.getItem("auth")
                                                ).contactType == "staff" &&
                                                (Array.isArray(formData) &&
                                                formData.length > 0
                                                  ? formData.map((item) =>
                                                      item.roleStaff ===
                                                      "add_contact" ? (
                                                        <div
                                                          data-bs-toggle="tooltip"
                                                          title="Edit"
                                                        >
                                                          <a
                                                            className="nav-link py-0"
                                                            href="#subscription_level-collapse"
                                                            data-bs-toggle="collapse"
                                                          >
                                                            <i className="fi-edit"></i>
                                                          </a>
                                                        </div>
                                                      ) : (
                                                        ""
                                                      )
                                                    )
                                                  : "")}
                                            </>
                                          )}
                                        </div>
                                        <div
                                          className="collapse"
                                          id="subscription_level-collapse"
                                          data-bs-parent="#personal-details"
                                        >
                                          <select
                                            className="form-select mt-3"
                                            id="pr-city"
                                            name="subscription_level"
                                            value={
                                              formValues.subscription_level ||
                                              ""
                                            }
                                            onChange={handleChanges}
                                            data-bs-binded-element="#subscription_level-value"
                                            data-bs-unset-value="Not specified"
                                            placeholder="Select"
                                          >
                                            <option>Intranet Only</option>
                                            <option>Transaction Only</option>
                                            <option>Full Account</option>
                                          </select>
                                          {/* {formErrors.subscription_level && (
                                                                    <div className="invalid-tooltip">{formErrors.subscription_level}</div>
                                                                )} */}
                                        </div>
                                      </div>

                                      {/* <div className="border-bottom pb-3 mb-3">
                                                            <div className="d-flex align-items-center justify-content-between">
                                                                <div className="pe-2">
                                                                    <label for="staticEmail" className="col-form-label">
                                                                        <span>Integrations</span>
                                                                    </label>

                                                                    <div id="Integrations-value">Not Set</div>
                                                                </div>
                                                                <div data-bs-toggle="tooltip" title="Edit"><a className="nav-link py-0" href="#Integrations-collapse" data-bs-toggle="collapse"></a></div>
                                                            </div>

                                                        </div> */}

                                      <div className="border-bottom pb-3 mb-3">
                                        <div className="d-flex align-items-center justify-content-between">
                                          <div className="pe-2">
                                            <label
                                              for="staticEmail"
                                              className="col-form-label"
                                            >
                                              <span>Intranet ID</span>
                                            </label>
                                            <div id="intranet_id-value">
                                              {getContact.additionalActivateFields
                                                ? getContact
                                                    .additionalActivateFields
                                                    .intranet_id
                                                : formValues.intranet_id || ""}
                                            </div>
                                          </div>
                                          {localStorage.getItem("auth") &&
                                          jwt(localStorage.getItem("auth"))
                                            .contactType == "admin" ? (
                                            <div
                                              data-bs-toggle="tooltip"
                                              title="Edit"
                                            >
                                              <a
                                                className="nav-link py-0"
                                                href="#intranet_id-collapse"
                                                data-bs-toggle="collapse"
                                              >
                                                <i className="fi-edit"></i>
                                              </a>
                                            </div>
                                          ) : (
                                            <>
                                              {localStorage.getItem("auth") &&
                                                jwt(
                                                  localStorage.getItem("auth")
                                                ).contactType == "staff" &&
                                                (Array.isArray(formData) &&
                                                formData.length > 0
                                                  ? formData.map((item) =>
                                                      item.roleStaff ===
                                                      "add_contact" ? (
                                                        <div
                                                          data-bs-toggle="tooltip"
                                                          title="Edit"
                                                        >
                                                          <a
                                                            className="nav-link py-0"
                                                            href="#intranet_id-collapse"
                                                            data-bs-toggle="collapse"
                                                          >
                                                            <i className="fi-edit"></i>
                                                          </a>
                                                        </div>
                                                      ) : (
                                                        ""
                                                      )
                                                    )
                                                  : "")}
                                            </>
                                          )}
                                        </div>
                                        <div
                                          className="collapse"
                                          id="intranet_id-collapse"
                                          data-bs-parent="#personal-details"
                                        >
                                          <input
                                            className="form-control mt-3"
                                            type="text"
                                            name="intranet_id"
                                            value={formValues.intranet_id || ""}
                                            onChange={handleChanges}
                                            data-bs-binded-element="#intranet_id-value"
                                            data-bs-unset-value="Not specified"
                                          />
                                        </div>
                                      </div>

                                      {localStorage.getItem("auth") &&
                                      jwt(localStorage.getItem("auth"))
                                        .contactType == "admin" ? (
                                        <div className="border-bottom pb-3 mb-3">
                                          <div className="d-flex align-items-center justify-content-between">
                                            <div className="pe-2">
                                              <label
                                                for="staticEmail"
                                                className="col-form-label"
                                              >
                                                <span>Account Password</span>
                                              </label>
                                              <div id="intranet_id-value">
                                                {getContact.passwordHash &&
                                                getContact.passwordHash !==
                                                  "" ? (
                                                  // <button className="btn btn-primary  px-3 px-sm-4" type="button" onClick={""}>Change Password - Send Reset Link</button>
                                                  <button
                                                    type="button"
                                                    className="btn btn-info btn-lg my-3 pull-right"
                                                    data-toggle="modal"
                                                    data-target="#Modalresetpassword"
                                                  >
                                                    Change Password - Send Reset
                                                    Link
                                                  </button>
                                                ) : (
                                                  // <button className="btn btn-primary  px-3 px-sm-4" type="button" onClick={""}>Send Reset Link</button>
                                                  <button
                                                    type="button"
                                                    className="btn btn-info btn-lg mb-5 pull-right"
                                                    data-toggle="modal"
                                                    data-target="#Modalresetpassword"
                                                  >
                                                    Send Reset Link
                                                  </button>
                                                )}
                                              </div>
                                            </div>
                                            {getContact.additionalActivateFields ? (
                                              getContact
                                                .additionalActivateFields
                                                .intranet_uniqueid &&
                                              getContact
                                                .additionalActivateFields
                                                .intranet_confirmationcode ? (
                                                <>
                                                  <p>
                                                    You have genera te reset
                                                    link already , Link is :
                                                    <br />{" "}
                                                    {window.location.origin}
                                                    /account-setup/
                                                    {
                                                      getContact
                                                        .additionalActivateFields
                                                        .intranet_uniqueid
                                                    }
                                                    /
                                                    {
                                                      getContact
                                                        .additionalActivateFields
                                                        .intranet_confirmationcode
                                                    }{" "}
                                                  </p>
                                                </>
                                              ) : (
                                                ""
                                              )
                                            ) : (
                                              ""
                                            )}
                                          </div>
                                        </div>
                                      ) : (
                                        <>
                                          {localStorage.getItem("auth") &&
                                            jwt(localStorage.getItem("auth"))
                                              .contactType == "staff" &&
                                            (Array.isArray(formData) &&
                                            formData.length > 0
                                              ? formData.map((item) =>
                                                  item.roleStaff ===
                                                  "add_contact" ? (
                                                    <div className="border-bottom pb-3 mb-3">
                                                      <div className="d-flex align-items-center justify-content-between">
                                                        <div className="pe-2">
                                                          <label
                                                            for="staticEmail"
                                                            className="col-form-label"
                                                          >
                                                            <span>
                                                              Account Password
                                                            </span>
                                                          </label>
                                                          <div id="intranet_id-value">
                                                            {getContact.passwordHash &&
                                                            getContact.passwordHash !==
                                                              "" ? (
                                                              // <button className="btn btn-primary  px-3 px-sm-4" type="button" onClick={""}>Change Password - Send Reset Link</button>
                                                              <button
                                                                type="button"
                                                                className="btn btn-info btn-lg my-3 pull-right"
                                                                data-toggle="modal"
                                                                data-target="#Modalresetpassword"
                                                              >
                                                                Change Password
                                                                - Send Reset
                                                                Link
                                                              </button>
                                                            ) : (
                                                              // <button className="btn btn-primary  px-3 px-sm-4" type="button" onClick={""}>Send Reset Link</button>
                                                              <button
                                                                type="button"
                                                                className="btn btn-info btn-lg mb-5 pull-right"
                                                                data-toggle="modal"
                                                                data-target="#Modalresetpassword"
                                                              >
                                                                Send Reset Link
                                                              </button>
                                                            )}
                                                          </div>
                                                        </div>
                                                        {getContact.additionalActivateFields ? (
                                                          getContact
                                                            .additionalActivateFields
                                                            .intranet_uniqueid &&
                                                          getContact
                                                            .additionalActivateFields
                                                            .intranet_confirmationcode ? (
                                                            <>
                                                              <p>
                                                                You have genera
                                                                te reset link
                                                                already , Link
                                                                is :
                                                                <br />{" "}
                                                                {
                                                                  window
                                                                    .location
                                                                    .origin
                                                                }
                                                                /account-setup/
                                                                {
                                                                  getContact
                                                                    .additionalActivateFields
                                                                    .intranet_uniqueid
                                                                }
                                                                /
                                                                {
                                                                  getContact
                                                                    .additionalActivateFields
                                                                    .intranet_confirmationcode
                                                                }{" "}
                                                              </p>
                                                            </>
                                                          ) : (
                                                            ""
                                                          )
                                                        ) : (
                                                          ""
                                                        )}
                                                      </div>
                                                    </div>
                                                  ) : (
                                                    ""
                                                  )
                                                )
                                              : "")}
                                        </>
                                      )}

                                      <div className="border-bottom pb-3 mb-3">
                                        <h6>Finacial</h6>
                                        <div className="">
                                          <div className="">
                                            <label
                                              for="staticEmail"
                                              className="col-form-label"
                                            >
                                              <span>Account Balance</span>
                                            </label>
                                            {localStorage.getItem("auth") &&
                                            jwt(localStorage.getItem("auth"))
                                              .contactType == "admin" ? (
                                              <span className="pull-right">
                                                {" "}
                                                <NavLink
                                                  to={`/account-balance/${params.id}`}
                                                >
                                                  update
                                                </NavLink>
                                              </span>
                                            ) : (
                                              <>
                                                {localStorage.getItem("auth") &&
                                                  jwt(
                                                    localStorage.getItem("auth")
                                                  ).contactType == "staff" &&
                                                  (Array.isArray(formData) &&
                                                  formData.length > 0
                                                    ? formData.map((item) =>
                                                        item.roleStaff ===
                                                        "add_contact" ? (
                                                          <span className="pull-right">
                                                            {" "}
                                                            <NavLink
                                                              to={`/account-balance/${params.id}`}
                                                            >
                                                              update
                                                            </NavLink>
                                                          </span>
                                                        ) : (
                                                          ""
                                                        )
                                                      )
                                                    : "")}
                                              </>
                                            )}
                                          </div>
                                        </div>
                                      </div>

                                      <div className="border-bottom pb-3 mb-3">
                                        <h6>MLS Membership</h6>
                                        <div className="d-flex align-items-center justify-content-between">
                                          <div className="pe-2">
                                            <label
                                              for="staticEmail"
                                              className="col-form-label"
                                            >
                                              <span>MLS Membership</span>
                                            </label>
                                            <div id="mls_membership-value">
                                              {getContact.additionalActivateFields
                                                ? commaSeparatedValuesmls_membership
                                                : formValues.mls_membership ||
                                                  ""}
                                            </div>
                                          </div>
                                          {localStorage.getItem("auth") &&
                                          jwt(localStorage.getItem("auth"))
                                            .contactType == "admin" ? (
                                            <div
                                              data-bs-toggle="tooltip"
                                              title="Edit"
                                            >
                                              <a
                                                className="nav-link py-0"
                                                href="#mls_membership-collapse"
                                                data-bs-toggle="collapse"
                                              >
                                                <i className="fi-edit"></i>
                                              </a>
                                            </div>
                                          ) : (
                                            <>
                                              {localStorage.getItem("auth") &&
                                                jwt(
                                                  localStorage.getItem("auth")
                                                ).contactType == "staff" &&
                                                (Array.isArray(formData) &&
                                                formData.length > 0
                                                  ? formData.map((item) =>
                                                      item.roleStaff ===
                                                      "add_contact" ? (
                                                        <div
                                                          data-bs-toggle="tooltip"
                                                          title="Edit"
                                                        >
                                                          <a
                                                            className="nav-link py-0"
                                                            href="#mls_membership-collapse"
                                                            data-bs-toggle="collapse"
                                                          >
                                                            <i className="fi-edit"></i>
                                                          </a>
                                                        </div>
                                                      ) : (
                                                        ""
                                                      )
                                                    )
                                                  : "")}
                                            </>
                                          )}
                                        </div>
                                        <div
                                          className="collapse mt-3"
                                          id="mls_membership-collapse"
                                          data-bs-parent="#personal-details"
                                        >
                                          <MultiSelect
                                            options={[
                                              {
                                                label: `UtahRealEstate.com`,
                                                value: `UtahRealEstate.com`,
                                              },
                                              {
                                                label: `Park City`,
                                                value: `Park City`,
                                              },
                                              {
                                                label: `Washington County`,
                                                value: `Washington County`,
                                              },
                                              {
                                                label: `Iron County`,
                                                value: `Iron County`,
                                              },
                                              // {
                                              //   label: `Local MLS (No IDX)`,
                                              //   value: `Local MLS (No IDX)`,
                                              // },
                                              // { label: `Tooele County`, value: `Tooele County` },
                                              // { label: `Cache Valley`, value: `Cache Valley` },
                                              // { label: `Brigham-Tremonton Board`, value: `Brigham-Tremonton Board` }
                                            ]}
                                            value={mls_membership}
                                            onChange={setMls_membership}
                                            labelledBy="Select MLS"
                                            name="mls_membership"
                                            // hasSelectAll={false}
                                            // selectionLimit={1}
                                          />
                                        </div>
                                      </div>

                                      <div className="border-bottom pb-3 mb-3">
                                        <h6>Board</h6>
                                        <div className="d-flex align-items-center justify-content-between">
                                          <div className="pe-2">
                                            <label className="form-label">
                                              Board and the Board Membership
                                            </label>

                                            <div id="board-value">
                                              {/* {getContact.additionalActivateFields
                                    ? commaSeparatedValues.board_membership
                                    : formValues.board_membership || ""} */}
                                              {getContact.additionalActivateFields
                                                ? commaSeparatedValues
                                                : formValues.board_membership ||
                                                  ""}
                                            </div>
                                          </div>
                                          {localStorage.getItem("auth") &&
                                          jwt(localStorage.getItem("auth"))
                                            .contactType == "admin" ? (
                                            <div
                                              data-bs-toggle="tooltip"
                                              title="Edit"
                                            >
                                              <a
                                                className="nav-link py-0"
                                                href="#board-collapse"
                                                data-bs-toggle="collapse"
                                              >
                                                <i className="fi-edit"></i>
                                              </a>
                                            </div>
                                          ) : (
                                            <>
                                              {localStorage.getItem("auth") &&
                                                jwt(
                                                  localStorage.getItem("auth")
                                                ).contactType == "staff" &&
                                                (Array.isArray(formData) &&
                                                formData.length > 0
                                                  ? formData.map((item) =>
                                                      item.roleStaff ===
                                                      "add_contact" ? (
                                                        <div
                                                          data-bs-toggle="tooltip"
                                                          title="Edit"
                                                        >
                                                          <a
                                                            className="nav-link py-0"
                                                            href="#board-collapse"
                                                            data-bs-toggle="collapse"
                                                          >
                                                            <i className="fi-edit"></i>
                                                          </a>
                                                        </div>
                                                      ) : (
                                                        ""
                                                      )
                                                    )
                                                  : "")}
                                            </>
                                          )}
                                        </div>
                                        <div
                                          className="collapse mt-3"
                                          id="board-collapse"
                                          data-bs-parent="#auth-info"
                                        >
                                          <MultiSelect2
                                            options={[
                                              {
                                                label: `Salt Lake Board`,
                                                value: `Salt Lake Board`,
                                              },
                                              {
                                                label: `Utah Central Association UCAR`,
                                                value: `Utah Central Association UCAR`,
                                              },
                                              {
                                                label: `Northern Wasatch`,
                                                value: `Northern Wasatch`,
                                              },
                                              {
                                                label: `Washington County`,
                                                value: `Washington County`,
                                              },
                                              {
                                                label: `Park City`,
                                                value: `Park City`,
                                              },
                                              {
                                                label: `Iron County`,
                                                value: `Iron County`,
                                              },
                                              {
                                                label: `Tooele County`,
                                                value: `Tooele County`,
                                              },
                                              {
                                                label: `Cache Valley`,
                                                value: `Cache Valley`,
                                              },
                                              {
                                                label: `Brigham-Tremonton Board`,
                                                value: `Brigham-Tremonton Board`,
                                              },
                                            ]}
                                            value={board_membership}
                                            onChange={setBoard_membership}
                                            labelledBy="Select Board Membership"
                                            name="board_membership"
                                          />
                                        </div>
                                      </div>

                                      <div className="border-bottom pb-3 mb-3">
                                        <h6>Office</h6>
                                        <div className="d-flex align-items-center justify-content-between">
                                          <div className="pe-2">
                                            <label
                                              for="staticEmail"
                                              className="col-form-label"
                                            >
                                              <span>Office Affiliation</span>
                                            </label>
                                            <div id="office-value">
                                              {getContact.additionalActivateFields
                                                ? getContact
                                                    .additionalActivateFields
                                                    .office
                                                : formValues.office || ""}
                                            </div>
                                            {/* <div id="office-value">{getContact.additionalActivateFields
                                                        ? getContact.additionalActivateFields.office_affiliation
                                                        : formValues.office_affiliation || ''}
                                                    </div> */}
                                          </div>
                                          {localStorage.getItem("auth") &&
                                          jwt(localStorage.getItem("auth"))
                                            .contactType == "admin" ? (
                                            <div
                                              data-bs-toggle="tooltip"
                                              title="Edit"
                                            >
                                              <a
                                                className="nav-link py-0"
                                                href="#office-collapse"
                                                data-bs-toggle="collapse"
                                              >
                                                <i className="fi-edit"></i>
                                              </a>
                                            </div>
                                          ) : (
                                            <>
                                              {localStorage.getItem("auth") &&
                                                jwt(
                                                  localStorage.getItem("auth")
                                                ).contactType == "staff" &&
                                                (Array.isArray(formData) &&
                                                formData.length > 0
                                                  ? formData.map((item) =>
                                                      item.roleStaff ===
                                                      "add_contact" ? (
                                                        <div
                                                          data-bs-toggle="tooltip"
                                                          title="Edit"
                                                        >
                                                          <a
                                                            className="nav-link py-0"
                                                            href="#office-collapse"
                                                            data-bs-toggle="collapse"
                                                          >
                                                            <i className="fi-edit"></i>
                                                          </a>
                                                        </div>
                                                      ) : (
                                                        ""
                                                      )
                                                    )
                                                  : "")}
                                            </>
                                          )}
                                        </div>
                                        <div
                                          className="collapse mt-3"
                                          id="office-collapse"
                                          data-bs-parent="#auth-info"
                                        >
                                          <select
                                            className="form-select"
                                            id="pr-country"
                                            name="office"
                                            value={formValues.office}
                                            onChange={handleChanges}
                                          >
                                            {organizationGet.data &&
                                              organizationGet.data.map(
                                                (item) => (
                                                  <option
                                                    key={item._id}
                                                    value={item.name}
                                                  >
                                                    {item.name}
                                                  </option>
                                                )
                                              )}
                                          </select>
                                        </div>
                                      </div>

                                      <div className="border-bottom pb-3 mb-3">
                                        <div className="d-flex align-items-center justify-content-between">
                                          <div className="pe-2">
                                            <label
                                              for="staticEmail"
                                              className="col-form-label"
                                            >
                                              <span>Office Notes</span>
                                            </label>
                                            <div id="account_name">
                                              <b>
                                                {notesdata.data
                                                  ? notesdata.data.length
                                                  : 0}{" "}
                                                Office Notes
                                              </b>
                                            </div>
                                          </div>
                                          {localStorage.getItem("auth") &&
                                          (jwt(localStorage.getItem("auth"))
                                            .contactType == "admin" ||
                                            jwt(localStorage.getItem("auth"))
                                              .id == params.id) ? (
                                            <NavLink>
                                              <p
                                                onClick={() => {
                                                  setNotes(true);
                                                }}
                                              >
                                                Office Note
                                              </p>
                                            </NavLink>
                                          ) : (
                                            <>
                                              {localStorage.getItem("auth") &&
                                                jwt(
                                                  localStorage.getItem("auth")
                                                ).contactType == "staff" &&
                                                (Array.isArray(formData) &&
                                                formData.length > 0
                                                  ? formData.map((item) =>
                                                      item.roleStaff ===
                                                      "add_contact" ? (
                                                        <NavLink>
                                                          <p
                                                            onClick={() => {
                                                              setNotes(true);
                                                            }}
                                                          >
                                                            Office Note
                                                          </p>
                                                        </NavLink>
                                                      ) : (
                                                        ""
                                                      )
                                                    )
                                                  : "")}
                                            </>
                                          )}
                                        </div>
                                      </div>

                                      <div className="border-bottom pb-3 mb-3">
                                        <div className="d-flex align-items-center justify-content-between">
                                          <div className="pe-2 mt-2">
                                            <label
                                              for="staticEmail"
                                              className="col-form-label"
                                            >
                                              <span>Accounting Group</span>
                                            </label>
                                            <div id="account_name">
                                              {getContact.additionalActivateFields
                                                ? getContact
                                                    .additionalActivateFields
                                                    .account_name
                                                : formValues.account_name || ""}
                                            </div>
                                          </div>
                                          {localStorage.getItem("auth") &&
                                          jwt(localStorage.getItem("auth"))
                                            .contactType == "admin" ? (
                                            <div
                                              data-bs-toggle="tooltip"
                                              account_name="Edit"
                                            >
                                              <a
                                                className="nav-link py-0"
                                                href="#account_name-collapse"
                                                data-bs-toggle="collapse"
                                              >
                                                <i className="fi-edit"></i>
                                              </a>
                                            </div>
                                          ) : (
                                            <>
                                              {localStorage.getItem("auth") &&
                                                jwt(
                                                  localStorage.getItem("auth")
                                                ).contactType == "staff" &&
                                                (Array.isArray(formData) &&
                                                formData.length > 0
                                                  ? formData.map((item) =>
                                                      item.roleStaff ===
                                                      "add_contact" ? (
                                                        <div
                                                          data-bs-toggle="tooltip"
                                                          account_name="Edit"
                                                        >
                                                          <a
                                                            className="nav-link py-0"
                                                            href="#account_name-collapse"
                                                            data-bs-toggle="collapse"
                                                          >
                                                            <i className="fi-edit"></i>
                                                          </a>
                                                        </div>
                                                      ) : (
                                                        ""
                                                      )
                                                    )
                                                  : "")}
                                            </>
                                          )}
                                        </div>
                                        <div
                                          className="collapse mt-3"
                                          id="account_name-collapse"
                                          data-bs-parent="#auth-info"
                                        >
                                          <select
                                            className="form-select"
                                            id="pr-city"
                                            name="account_name"
                                            value={
                                              formValues.account_name || ""
                                            }
                                            onChange={handleChanges}
                                            data-bs-binded-element="#account_name-value"
                                            data-bs-unset-value="Not specified"
                                          >
                                            <option>Integra Reality</option>
                                          </select>
                                        </div>
                                      </div>

                                      <div className="border-bottom pb-3 mb-3">
                                        <h6 className="pull-left">
                                          Paperwork{" "}
                                        </h6>
                                        {localStorage.getItem("auth") &&
                                        jwt(localStorage.getItem("auth"))
                                          .contactType == "admin" ? (
                                          <span className="pull-right">
                                            {" "}
                                            <NavLink to="/paperwork">
                                              Manage Paperwork
                                            </NavLink>
                                          </span>
                                        ) : (
                                          <>
                                            {localStorage.getItem("auth") &&
                                              jwt(localStorage.getItem("auth"))
                                                .contactType == "staff" &&
                                              (Array.isArray(formData) &&
                                              formData.length > 0
                                                ? formData.map((item) =>
                                                    item.roleStaff ===
                                                    "add_contact" ? (
                                                      <span className="pull-right">
                                                        {" "}
                                                        <NavLink to="/paperwork">
                                                          Manage Paperwork
                                                        </NavLink>
                                                      </span>
                                                    ) : (
                                                      ""
                                                    )
                                                  )
                                                : "")}
                                          </>
                                        )}

                                        <div className="float-left w-100 d-flex align-items-center justify-content-between">
                                          <div className="pe-2 mt-2">
                                            <label
                                              for="staticEmail"
                                              className="col-form-label"
                                            >
                                              <span>
                                                {getContact.contact_paperwork
                                                  ? getContact.contact_paperwork
                                                      .length
                                                  : 0}{" "}
                                                paperwork documents on file
                                              </span>
                                            </label>
                                          </div>
                                          {localStorage.getItem("auth") &&
                                          jwt(localStorage.getItem("auth"))
                                            .contactType == "admin" ? (
                                            <div
                                              data-bs-toggle="tooltip"
                                              license_type="Edit"
                                            >
                                              <a
                                                className="nav-link py-0"
                                                href="#license_type-collapse"
                                                data-bs-toggle="collapse"
                                              >
                                                <i className="fi-edit"></i>
                                              </a>
                                            </div>
                                          ) : (
                                            <>
                                              {localStorage.getItem("auth") &&
                                                jwt(
                                                  localStorage.getItem("auth")
                                                ).contactType == "staff" &&
                                                (Array.isArray(formData) &&
                                                formData.length > 0
                                                  ? formData.map((item) =>
                                                      item.roleStaff ===
                                                      "add_contact" ? (
                                                        <div
                                                          data-bs-toggle="tooltip"
                                                          license_type="Edit"
                                                        >
                                                          <a
                                                            className="nav-link py-0"
                                                            href="#license_type-collapse"
                                                            data-bs-toggle="collapse"
                                                          >
                                                            <i className="fi-edit"></i>
                                                          </a>
                                                        </div>
                                                      ) : (
                                                        ""
                                                      )
                                                    )
                                                  : "")}
                                            </>
                                          )}
                                        </div>
                                        <div
                                          className="collapse"
                                          id="license_type-collapse"
                                          data-bs-parent="#auth-info"
                                        >
                                          <p className="mb-0">
                                            Paperwork Summary{" "}
                                            {localStorage.getItem("auth") &&
                                            (jwt(localStorage.getItem("auth"))
                                              .contactType == "admin" ||
                                              jwt(localStorage.getItem("auth"))
                                                .id == params.id) ? (
                                              <button
                                                type="button"
                                                className="btn btn-info btn-lg mb-5 pull-right"
                                                data-toggle="modal"
                                                data-target="#ModalAddpaperworkdocument"
                                              >
                                                Add Paperwork
                                              </button>
                                            ) : (
                                              <>
                                                {localStorage.getItem("auth") &&
                                                  jwt(
                                                    localStorage.getItem("auth")
                                                  ).contactType == "staff" &&
                                                  (Array.isArray(formData) &&
                                                  formData.length > 0
                                                    ? formData.map((item) =>
                                                        item.roleStaff ===
                                                        "add_contact" ? (
                                                          <button
                                                            type="button"
                                                            className="btn btn-info btn-lg mb-5 pull-right"
                                                            data-toggle="modal"
                                                            data-target="#ModalAddpaperworkdocument"
                                                          >
                                                            Add Paperwork
                                                          </button>
                                                        ) : (
                                                          ""
                                                        )
                                                      )
                                                    : "")}
                                              </>
                                            )}
                                          </p>
                                          {getContact.contact_paperwork &&
                                          getContact.contact_paperwork.length >
                                            0
                                            ? getContact.contact_paperwork.map(
                                                (item) => (
                                                  <p key={item._id}>
                                                    {item.contact_paperworkdocument &&
                                                    item
                                                      .contact_paperworkdocument
                                                      .length > 0 &&
                                                    item
                                                      .contact_paperworkdocument[0]
                                                      .approvalStatus ===
                                                      "yes" ? (
                                                      <i
                                                        className="fa fa-check"
                                                        aria-hidden="true"
                                                      ></i>
                                                    ) : (
                                                      <i
                                                        className="fa fa-times"
                                                        aria-hidden="true"
                                                      ></i>
                                                    )}
                                                    <b>
                                                      {item.paperwork_title}
                                                    </b>{" "}
                                                    -{" "}
                                                    {item.contact_paperworkdocument &&
                                                      item
                                                        .contact_paperworkdocument
                                                        .length > 0 && (
                                                        <a
                                                          href={
                                                            item
                                                              .contact_paperworkdocument[0]
                                                              .documenturl
                                                          }
                                                          target="_blank"
                                                          rel="noopener noreferrer"
                                                        >
                                                          View File
                                                        </a>
                                                      )}
                                                    <br />
                                                    <span>
                                                      {item.contact_paperworkdocument &&
                                                        item
                                                          .contact_paperworkdocument
                                                          .length > 0 &&
                                                        new Date(
                                                          item.contact_paperworkdocument[0].created
                                                        ).toLocaleString(
                                                          "en-US",
                                                          {
                                                            month: "short",
                                                            day: "numeric",
                                                            year: "numeric",
                                                            hour: "numeric",
                                                            minute: "numeric",
                                                          }
                                                        )}
                                                      <small>
                                                        {item.contact_paperworkdocument &&
                                                          item
                                                            .contact_paperworkdocument
                                                            .length > 0 &&
                                                          getTimeDifference(
                                                            item
                                                              .contact_paperworkdocument[0]
                                                              .created
                                                          )}
                                                      </small>
                                                      {localStorage.getItem(
                                                        "auth"
                                                      ) &&
                                                      jwt(
                                                        localStorage.getItem(
                                                          "auth"
                                                        )
                                                      ).contactType ===
                                                        "admin" ? (
                                                        <span className="ms-4">
                                                          {item.contact_paperworkdocument &&
                                                          item
                                                            .contact_paperworkdocument
                                                            .length > 0 &&
                                                          item
                                                            .contact_paperworkdocument[0]
                                                            .approvalStatus ===
                                                            "yes" ? (
                                                            <button
                                                              type="button"
                                                              onClick={() =>
                                                                handleApprovePaperwork(
                                                                  item
                                                                    .contact_paperworkdocument[0]
                                                                    ._id,
                                                                  "no"
                                                                )
                                                              }
                                                              className="btn btn-translucent-primary"
                                                            >
                                                              Reject
                                                            </button>
                                                          ) : (
                                                            <button
                                                              type="button"
                                                              onClick={() =>
                                                                handleApprovePaperwork(
                                                                  item
                                                                    .contact_paperworkdocument[0]
                                                                    ._id,
                                                                  "yes"
                                                                )
                                                              }
                                                              className="btn btn-translucent-primary"
                                                            >
                                                              Approve
                                                            </button>
                                                          )}
                                                        </span>
                                                      ) : (
                                                        <>
                                                          {localStorage.getItem(
                                                            "auth"
                                                          ) &&
                                                          jwt(
                                                            localStorage.getItem(
                                                              "auth"
                                                            )
                                                          ).contactType ===
                                                            "staff" &&
                                                          Array.isArray(
                                                            formData
                                                          ) &&
                                                          formData.length > 0
                                                            ? formData.map(
                                                                (innerItem) =>
                                                                  innerItem.roleStaff ===
                                                                  "add_contact" ? (
                                                                    <span
                                                                      className="ms-4"
                                                                      key={
                                                                        innerItem._id
                                                                      }
                                                                    >
                                                                      {innerItem.contact_paperworkdocument &&
                                                                      innerItem
                                                                        .contact_paperworkdocument
                                                                        .length >
                                                                        0 &&
                                                                      innerItem
                                                                        .contact_paperworkdocument[0]
                                                                        .approvalStatus ===
                                                                        "yes" ? (
                                                                        <button
                                                                          type="button"
                                                                          onClick={() =>
                                                                            handleApprovePaperwork(
                                                                              innerItem
                                                                                .contact_paperworkdocument[0]
                                                                                ._id,
                                                                              "no"
                                                                            )
                                                                          }
                                                                          className="btn btn-translucent-primary"
                                                                        >
                                                                          Reject
                                                                        </button>
                                                                      ) : (
                                                                        <button
                                                                          type="button"
                                                                          onClick={() =>
                                                                            handleApprovePaperwork(
                                                                              innerItem
                                                                                .contact_paperworkdocument[0]
                                                                                ._id,
                                                                              "yes"
                                                                            )
                                                                          }
                                                                          className="btn btn-translucent-primary"
                                                                        >
                                                                          Approve
                                                                        </button>
                                                                      )}
                                                                    </span>
                                                                  ) : (
                                                                    ""
                                                                  )
                                                              )
                                                            : ""}
                                                        </>
                                                      )}
                                                    </span>
                                                  </p>
                                                )
                                              )
                                            : ""}
                                        </div>
                                      </div>

                                      <div className="border-bottom pb-3 mb-3">
                                        <h6>License</h6>
                                        <div className="d-flex align-items-center justify-content-between">
                                          <div className="pe-2 mt-2">
                                            <label
                                              for="staticEmail"
                                              className="col-form-label"
                                            >
                                              <span>Licensed Type</span>
                                            </label>
                                            <div id="license_type">
                                              {getContact.additionalActivateFields
                                                ? getContact
                                                    .additionalActivateFields
                                                    .license_type
                                                : formValues.license_type || ""}
                                            </div>
                                          </div>
                                          {localStorage.getItem("auth") &&
                                          jwt(localStorage.getItem("auth"))
                                            .contactType == "admin" ? (
                                            <div
                                              data-bs-toggle="tooltip"
                                              license_type="Edit"
                                            >
                                              <a
                                                className="nav-link py-0"
                                                href="#license_type-collapse"
                                                data-bs-toggle="collapse"
                                              >
                                                <i className="fi-edit"></i>
                                              </a>
                                            </div>
                                          ) : (
                                            <>
                                              {localStorage.getItem("auth") &&
                                                jwt(
                                                  localStorage.getItem("auth")
                                                ).contactType == "staff" &&
                                                (Array.isArray(formData) &&
                                                formData.length > 0
                                                  ? formData.map((item) =>
                                                      item.roleStaff ===
                                                      "add_contact" ? (
                                                        <div
                                                          data-bs-toggle="tooltip"
                                                          license_type="Edit"
                                                        >
                                                          <a
                                                            className="nav-link py-0"
                                                            href="#license_type-collapse"
                                                            data-bs-toggle="collapse"
                                                          >
                                                            <i className="fi-edit"></i>
                                                          </a>
                                                        </div>
                                                      ) : (
                                                        ""
                                                      )
                                                    )
                                                  : "")}
                                            </>
                                          )}
                                        </div>
                                        <div
                                          className="collapse mt-3"
                                          id="license_type-collapse"
                                          data-bs-parent="#auth-info"
                                        >
                                          <select
                                            className="form-select"
                                            id="pr-city"
                                            name="license_type"
                                            onChange={handleChanges}
                                            value={
                                              formValues.license_type || ""
                                            }
                                            data-bs-binded-element="#license_type-value"
                                            data-bs-unset-value="Not specified"
                                            data-datepicker-options='{"altInput": true, "altFormat": "F j, Y", "dateFormat": "Y-m-d"}'
                                          >
                                            <option>Sales Agent</option>
                                            <option>Associate Broker</option>
                                            <option>Principal Broker</option>
                                            <option>Company</option>
                                          </select>
                                        </div>
                                      </div>

                                      <div className="border-bottom pb-3 mb-3">
                                        <div className="d-flex align-items-center justify-content-between">
                                          <div className="pe-2 mt-2">
                                            <label
                                              for="staticEmail"
                                              className="col-form-label"
                                            >
                                              <span>Licensed Number</span>
                                            </label>
                                            <div id="date_expire">
                                              {getContact.additionalActivateFields
                                                ? getContact
                                                    .additionalActivateFields
                                                    .license_number
                                                : formValues.license_number ||
                                                  ""}
                                            </div>
                                          </div>
                                          {localStorage.getItem("auth") &&
                                          jwt(localStorage.getItem("auth"))
                                            .contactType == "admin" ? (
                                            <div
                                              data-bs-toggle="tooltip"
                                              license_number="Edit"
                                            >
                                              <a
                                                className="nav-link py-0"
                                                href="#license_number-collapse"
                                                data-bs-toggle="collapse"
                                              >
                                                <i className="fi-edit"></i>
                                              </a>
                                            </div>
                                          ) : (
                                            <>
                                              {localStorage.getItem("auth") &&
                                                jwt(
                                                  localStorage.getItem("auth")
                                                ).contactType == "staff" &&
                                                (Array.isArray(formData) &&
                                                formData.length > 0
                                                  ? formData.map((item) =>
                                                      item.roleStaff ===
                                                      "add_contact" ? (
                                                        <div
                                                          data-bs-toggle="tooltip"
                                                          license_number="Edit"
                                                        >
                                                          <a
                                                            className="nav-link py-0"
                                                            href="#license_number-collapse"
                                                            data-bs-toggle="collapse"
                                                          >
                                                            <i className="fi-edit"></i>
                                                          </a>
                                                        </div>
                                                      ) : (
                                                        ""
                                                      )
                                                    )
                                                  : "")}
                                            </>
                                          )}
                                        </div>
                                        <div
                                          className="collapse mt-3"
                                          id="license_number-collapse"
                                          data-bs-parent="#auth-info"
                                        >
                                          <input
                                            className="form-control"
                                            id="inline-form-input"
                                            type="text"
                                            placeholder="status"
                                            name="license_number"
                                            onChange={handleChanges}
                                            value={
                                              formValues.license_number || ""
                                            }
                                            data-bs-binded-element="#license_number-value"
                                            data-bs-unset-value="Not specified"
                                            data-datepicker-options='{"altInput": true, "altFormat": "F j, Y", "dateFormat": "Y-m-d"}'
                                          />
                                        </div>
                                      </div>

                                      <div className="border-bottom pb-3 mb-3">
                                        <div className="d-flex align-items-center justify-content-between">
                                          <div className="pe-2 mt-2">
                                            <label
                                              for="staticEmail"
                                              className="col-form-label"
                                            >
                                              <span>Licensed Expires</span>
                                            </label>
                                            <div id="date_expire">
                                              {getContact.additionalActivateFields
                                                ? getFormattedDateFormat(
                                                    getContact
                                                      .additionalActivateFields
                                                      .date_expire
                                                  )
                                                : getFormattedDateFormat(
                                                    formValues.date_expire
                                                  ) || ""}
                                            </div>
                                          </div>
                                          {localStorage.getItem("auth") &&
                                          jwt(localStorage.getItem("auth"))
                                            .contactType == "admin" ? (
                                            <div
                                              data-bs-toggle="tooltip"
                                              date_expire="Edit"
                                            >
                                              <a
                                                className="nav-link py-0"
                                                href="#date_expire-collapse"
                                                data-bs-toggle="collapse"
                                              >
                                                <i className="fi-edit"></i>
                                              </a>
                                            </div>
                                          ) : (
                                            <>
                                              {localStorage.getItem("auth") &&
                                                jwt(
                                                  localStorage.getItem("auth")
                                                ).contactType == "staff" &&
                                                (Array.isArray(formData) &&
                                                formData.length > 0
                                                  ? formData.map((item) =>
                                                      item.roleStaff ===
                                                      "add_contact" ? (
                                                        <div
                                                          data-bs-toggle="tooltip"
                                                          date_expire="Edit"
                                                        >
                                                          <a
                                                            className="nav-link py-0"
                                                            href="#date_expire-collapse"
                                                            data-bs-toggle="collapse"
                                                          >
                                                            <i className="fi-edit"></i>
                                                          </a>
                                                        </div>
                                                      ) : (
                                                        ""
                                                      )
                                                    )
                                                  : "")}
                                            </>
                                          )}
                                        </div>
                                        <div
                                          className="collapse mt-3"
                                          id="date_expire-collapse"
                                          data-bs-parent="#auth-info"
                                        >
                                          <input
                                            className="form-control"
                                            type="date"
                                            id="inline-form-input"
                                            placeholder="Choose date"
                                            name="date_expire"
                                            onChange={handleChanges}
                                            value={formValues.date_expire || ""}
                                            data-bs-binded-element="#date_expire-value"
                                            data-bs-unset-value="Not specified"
                                            data-datepicker-options='{"altInput": true, "altFormat": "F j, Y", "dateFormat": "Y-m-d"}'
                                          />
                                        </div>
                                      </div>

                                      <div className="border-bottom pb-3 mb-3">
                                        <div className="d-flex align-items-center justify-content-between">
                                          <div className="pe-2 mt-2">
                                            <label
                                              for="staticEmail"
                                              className="col-form-label"
                                            >
                                              <span>NRDS ID</span>
                                            </label>
                                            <div id="nrds_id-value">
                                              {getContact.additionalActivateFields
                                                ? getContact
                                                    .additionalActivateFields
                                                    .nrds_id
                                                : formValues.nrds_id || ""}
                                            </div>
                                          </div>
                                          {localStorage.getItem("auth") &&
                                          jwt(localStorage.getItem("auth"))
                                            .contactType == "admin" ? (
                                            <div
                                              data-bs-toggle="tooltip"
                                              title="Edit"
                                            >
                                              <a
                                                className="nav-link py-0"
                                                href="#nrds_id-collapse"
                                                data-bs-toggle="collapse"
                                              >
                                                <i className="fi-edit"></i>
                                              </a>
                                            </div>
                                          ) : (
                                            <>
                                              {localStorage.getItem("auth") &&
                                                jwt(
                                                  localStorage.getItem("auth")
                                                ).contactType == "staff" &&
                                                (Array.isArray(formData) &&
                                                formData.length > 0
                                                  ? formData.map((item) =>
                                                      item.roleStaff ===
                                                      "add_contact" ? (
                                                        <div
                                                          data-bs-toggle="tooltip"
                                                          title="Edit"
                                                        >
                                                          <a
                                                            className="nav-link py-0"
                                                            href="#nrds_id-collapse"
                                                            data-bs-toggle="collapse"
                                                          >
                                                            <i className="fi-edit"></i>
                                                          </a>
                                                        </div>
                                                      ) : (
                                                        ""
                                                      )
                                                    )
                                                  : "")}
                                            </>
                                          )}
                                        </div>
                                        <div
                                          className="collapse"
                                          id="nrds_id-collapse"
                                          data-bs-parent="#auth-info"
                                        >
                                          <input
                                            className="form-control mt-3"
                                            type="text"
                                            name="nrds_id"
                                            value={formValues.nrds_id || ""}
                                            onChange={handleChanges}
                                            data-bs-binded-element="#nrds_id-value"
                                            data-bs-unset-value="Not specified"
                                          />
                                        </div>
                                      </div>

                                      <div className="border-bottom pb-3 mb-3">
                                        <h6>Legal</h6>

                                        <div className="d-flex align-items-center justify-content-between">
                                          <div className="pe-2 mt-2">
                                            <label
                                              for="staticEmail"
                                              className="col-form-label"
                                            >
                                              <span>Date Of Birth</span>
                                            </label>
                                            <div id="date_birth-value">
                                              {getContact.additionalActivateFields
                                                ? getFormattedDateFormat(
                                                    getContact
                                                      .additionalActivateFields
                                                      .date_birth
                                                  )
                                                : getFormattedDateFormat(
                                                    formValues.date_birth
                                                  ) || ""}
                                            </div>
                                          </div>
                                          {localStorage.getItem("auth") &&
                                          jwt(localStorage.getItem("auth"))
                                            .contactType == "admin" ? (
                                            <div
                                              data-bs-toggle="tooltip"
                                              title="Edit"
                                            >
                                              <a
                                                className="nav-link py-0"
                                                href="#date_birth-collapse"
                                                data-bs-toggle="collapse"
                                              >
                                                <i className="fi-edit"></i>
                                              </a>
                                            </div>
                                          ) : (
                                            <>
                                              {localStorage.getItem("auth") &&
                                                jwt(
                                                  localStorage.getItem("auth")
                                                ).contactType == "staff" &&
                                                (Array.isArray(formData) &&
                                                formData.length > 0
                                                  ? formData.map((item) =>
                                                      item.roleStaff ===
                                                      "add_contact" ? (
                                                        <div
                                                          data-bs-toggle="tooltip"
                                                          title="Edit"
                                                        >
                                                          <a
                                                            className="nav-link py-0"
                                                            href="#date_birth-collapse"
                                                            data-bs-toggle="collapse"
                                                          >
                                                            <i className="fi-edit"></i>
                                                          </a>
                                                        </div>
                                                      ) : (
                                                        ""
                                                      )
                                                    )
                                                  : "")}
                                            </>
                                          )}
                                        </div>
                                        <div
                                          className="collapse"
                                          id="date_birth-collapse"
                                          data-bs-parent="#auth-info"
                                        >
                                          <input
                                            className="form-control mt-3"
                                            type="date"
                                            placeholder="Choose date"
                                            name="date_birth"
                                            value={formValues.date_birth || ""}
                                            onChange={handleChanges}
                                            data-datepicker-options='{"altInput": true, "altFormat": "F j, Y", "dateFormat": "Y-m-d"}'
                                            data-bs-binded-element="#date_birth-value"
                                            data-bs-unset-value="Not specified"
                                          />
                                        </div>
                                      </div>

                                      <div className="border-bottom pb-3 mb-3">
                                        <div className="d-flex align-items-center justify-content-between">
                                          <div className="pe-2 mt-2">
                                            <label
                                              for="staticEmail"
                                              className="col-form-label"
                                            >
                                              <span>SSN / ITIN</span>
                                            </label>
                                            <div id="ssn_itin-value">
                                              {getContact.additionalActivateFields
                                                ? getContact
                                                    .additionalActivateFields
                                                    .ssn_itin
                                                : formValues.ssn_itin || ""}
                                            </div>
                                          </div>
                                          {localStorage.getItem("auth") &&
                                          jwt(localStorage.getItem("auth"))
                                            .contactType == "admin" ? (
                                            <div
                                              data-bs-toggle="tooltip"
                                              title="Edit"
                                            >
                                              <a
                                                className="nav-link py-0"
                                                href="#ssn_itin-collapse"
                                                data-bs-toggle="collapse"
                                              >
                                                <i className="fi-edit"></i>
                                              </a>
                                            </div>
                                          ) : (
                                            <>
                                              {localStorage.getItem("auth") &&
                                                jwt(
                                                  localStorage.getItem("auth")
                                                ).contactType == "staff" &&
                                                (Array.isArray(formData) &&
                                                formData.length > 0
                                                  ? formData.map((item) =>
                                                      item.roleStaff ===
                                                      "add_contact" ? (
                                                        <div
                                                          data-bs-toggle="tooltip"
                                                          title="Edit"
                                                        >
                                                          <a
                                                            className="nav-link py-0"
                                                            href="#ssn_itin-collapse"
                                                            data-bs-toggle="collapse"
                                                          >
                                                            <i className="fi-edit"></i>
                                                          </a>
                                                        </div>
                                                      ) : (
                                                        ""
                                                      )
                                                    )
                                                  : "")}
                                            </>
                                          )}
                                        </div>
                                        <div
                                          className="collapse"
                                          id="ssn_itin-collapse"
                                          data-bs-parent="#auth-info"
                                        >
                                          <input
                                            className="form-control mt-3"
                                            type="text"
                                            name="ssn_itin"
                                            value={formValues.ssn_itin || ""}
                                            onChange={handleChanges}
                                            data-bs-binded-element="#ssn_itin-value"
                                            data-bs-unset-value="Not specified"
                                          />
                                        </div>
                                      </div>

                                      <div className="border-bottom pb-3 mb-3">
                                        <div className="d-flex align-items-center justify-content-between">
                                          <div className="pe-2 mt-2">
                                            <label
                                              for="staticEmail"
                                              className="col-form-label"
                                            >
                                              <span>EIN / DBA</span>
                                            </label>
                                            <div id="agentTax_ein-value">
                                              {getContact.additionalActivateFields
                                                ? getContact
                                                    .additionalActivateFields
                                                    .agentTax_ein
                                                : formValues.agentTax_ein || ""}
                                            </div>
                                          </div>
                                          {localStorage.getItem("auth") &&
                                          jwt(localStorage.getItem("auth"))
                                            .contactType == "admin" ? (
                                            <div
                                              data-bs-toggle="tooltip"
                                              title="Edit"
                                            >
                                              <a
                                                className="nav-link py-0"
                                                href="#agentTax_ein-collapse"
                                                data-bs-toggle="collapse"
                                              >
                                                <i className="fi-edit"></i>
                                              </a>
                                            </div>
                                          ) : (
                                            <>
                                              {localStorage.getItem("auth") &&
                                                jwt(
                                                  localStorage.getItem("auth")
                                                ).contactType == "staff" &&
                                                (Array.isArray(formData) &&
                                                formData.length > 0
                                                  ? formData.map((item) =>
                                                      item.roleStaff ===
                                                      "add_contact" ? (
                                                        <div
                                                          data-bs-toggle="tooltip"
                                                          title="Edit"
                                                        >
                                                          <a
                                                            className="nav-link py-0"
                                                            href="#agentTax_ein-collapse"
                                                            data-bs-toggle="collapse"
                                                          >
                                                            <i className="fi-edit"></i>
                                                          </a>
                                                        </div>
                                                      ) : (
                                                        ""
                                                      )
                                                    )
                                                  : "")}
                                            </>
                                          )}
                                        </div>
                                        <div
                                          className="collapse"
                                          id="agentTax_ein-collapse"
                                          data-bs-parent="#auth-info"
                                        >
                                          <input
                                            className="form-control mt-3"
                                            type="text"
                                            name="agentTax_ein"
                                            value={
                                              formValues.agentTax_ein || ""
                                            }
                                            onChange={handleChanges}
                                            data-bs-binded-element="#agentTax_ein-value"
                                            data-bs-unset-value="Not specified"
                                          />
                                        </div>
                                      </div>

                                      <div className="border-bottom pb-3 mb-3">
                                        <div className="d-flex align-items-center justify-content-between">
                                          <div className="pe-2 mt-2">
                                            <label
                                              for="staticEmail"
                                              className="col-form-label"
                                            >
                                              <span>Transfer From</span>
                                            </label>
                                            <div id="admin_note">
                                              {getContact.additionalActivateFields
                                                ? getContact
                                                    .additionalActivateFields
                                                    .admin_note
                                                : formValues.admin_note || ""}
                                            </div>
                                          </div>
                                          {localStorage.getItem("auth") &&
                                          jwt(localStorage.getItem("auth"))
                                            .contactType == "admin" ? (
                                            <div
                                              data-bs-toggle="tooltip"
                                              admin_note="Edit"
                                            >
                                              <a
                                                className="nav-link py-0"
                                                href="#admin_note-collapse"
                                                data-bs-toggle="collapse"
                                              >
                                                <i className="fi-edit"></i>
                                              </a>
                                            </div>
                                          ) : (
                                            <>
                                              {localStorage.getItem("auth") &&
                                                jwt(
                                                  localStorage.getItem("auth")
                                                ).contactType == "staff" &&
                                                (Array.isArray(formData) &&
                                                formData.length > 0
                                                  ? formData.map((item) =>
                                                      item.roleStaff ===
                                                      "add_contact" ? (
                                                        <div
                                                          data-bs-toggle="tooltip"
                                                          admin_note="Edit"
                                                        >
                                                          <a
                                                            className="nav-link py-0"
                                                            href="#admin_note-collapse"
                                                            data-bs-toggle="collapse"
                                                          >
                                                            <i className="fi-edit"></i>
                                                          </a>
                                                        </div>
                                                      ) : (
                                                        ""
                                                      )
                                                    )
                                                  : "")}
                                            </>
                                          )}
                                        </div>
                                        <div
                                          className="collapse"
                                          id="admin_note-collapse"
                                          data-bs-parent="#auth-info"
                                        >
                                          <input
                                            className="form-control mt-3"
                                            type="text"
                                            name="admin_note"
                                            value={formValues.admin_note || ""}
                                            onChange={handleChanges}
                                            data-bs-binded-element="#admin_note-value"
                                            data-bs-unset-value="Not specified"
                                          />
                                        </div>
                                      </div>

                                      <div className="border-bottom pb-3 mb-3">
                                        <div className="d-flex align-items-center justify-content-between">
                                          <div className="pe-2 mt-2">
                                            <label
                                              for="staticEmail"
                                              className="col-form-label"
                                            >
                                              <span>Sponsored By</span>
                                            </label>
                                            <div id="sponsor_associate">
                                              {getContact.additionalActivateFields
                                                ? getContact
                                                    .additionalActivateFields
                                                    .sponsor_associate
                                                : formValues.sponsor_associate ||
                                                  ""}
                                            </div>
                                          </div>
                                          {localStorage.getItem("auth") &&
                                          jwt(localStorage.getItem("auth"))
                                            .contactType == "admin" ? (
                                            <div
                                              data-bs-toggle="tooltip"
                                              sponsor_associate="Edit"
                                            >
                                              <a
                                                className="nav-link py-0"
                                                href="#sponsor_associate-collapse"
                                                data-bs-toggle="collapse"
                                              >
                                                <i className="fi-edit"></i>
                                              </a>
                                            </div>
                                          ) : (
                                            <>
                                              {localStorage.getItem("auth") &&
                                                jwt(
                                                  localStorage.getItem("auth")
                                                ).contactType == "staff" &&
                                                (Array.isArray(formData) &&
                                                formData.length > 0
                                                  ? formData.map((item) =>
                                                      item.roleStaff ===
                                                      "add_contact" ? (
                                                        <div
                                                          data-bs-toggle="tooltip"
                                                          sponsor_associate="Edit"
                                                        >
                                                          <a
                                                            className="nav-link py-0"
                                                            href="#sponsor_associate-collapse"
                                                            data-bs-toggle="collapse"
                                                          >
                                                            <i className="fi-edit"></i>
                                                          </a>
                                                        </div>
                                                      ) : (
                                                        ""
                                                      )
                                                    )
                                                  : "")}
                                            </>
                                          )}
                                        </div>
                                        <div
                                          className="collapse"
                                          id="sponsor_associate-collapse"
                                          data-bs-parent="#auth-info"
                                        >
                                          <input
                                            className="form-control mt-3"
                                            type="text"
                                            name="sponsor_associate"
                                            value={
                                              formValues.sponsor_associate || ""
                                            }
                                            onChange={handleChanges}
                                            data-bs-binded-element="#sponsor_associate-value"
                                            data-bs-unset-value="Not specified"
                                          />
                                        </div>
                                      </div>

                                      <div className="border-bottom pb-3 mb-3">
                                        <div className="d-flex align-items-center justify-content-between">
                                          <div className="pe-2 mt-2">
                                            <label
                                              for="staticEmail"
                                              className="col-form-label"
                                            >
                                              <span>Recruited By</span>
                                            </label>
                                            <div id="staff_recruiter">
                                              {getContact.additionalActivateFields ? (
                                                <a
                                                  href={`/contact-profile/${getContact.additionalActivateFields.staff_recruiter_id}`}
                                                  target="_blank"
                                                >
                                                  {" "}
                                                  {
                                                    getContact
                                                      .additionalActivateFields
                                                      .staff_recruiter
                                                  }{" "}
                                                </a>
                                              ) : (
                                                formValues.staff_recruiter || ""
                                              )}
                                            </div>
                                          </div>

                                          {/* <div data-bs-toggle="tooltip" staff_recruiter="Edit"><a className="nav-link py-0" href="#staff_recruiter-collapse" data-bs-toggle="collapse"><i className="fi-edit"></i></a></div> */}
                                        </div>
                                        {/* <div className="collapse" id="staff_recruiter-collapse" data-bs-parent="#auth-info">
                                                                <input className="form-control mt-3" type="text"
                                                                    name="staff_recruiter"
                                                                    value={formValues.staff_recruiter || ''}
                                                                    onChange={handleChanges}
                                                                    data-bs-binded-element="#staff_recruiter-value" data-bs-unset-value="Not specified" />
                                                            </div> */}
                                      </div>
                                    </div>
                                    {localStorage.getItem("auth") &&
                                    jwt(localStorage.getItem("auth"))
                                      .contactType == "admin" ? (
                                      <div className="d-flex align-items-center justify-content-between mt-3">
                                        <button
                                          className="btn btn-primary"
                                          type="button"
                                          onClick={handleAccount}
                                        >
                                          Save Change
                                        </button>
                                      </div>
                                    ) : (
                                      <>
                                        {localStorage.getItem("auth") &&
                                          jwt(localStorage.getItem("auth"))
                                            .contactType == "staff" &&
                                          (Array.isArray(formData) &&
                                          formData.length > 0
                                            ? formData.map((item) =>
                                                item.roleStaff ===
                                                "add_contact" ? (
                                                  <div className="d-flex align-items-center justify-content-between mt-3">
                                                    <button
                                                      className="btn btn-primary"
                                                      type="button"
                                                      onClick={handleAccount}
                                                    >
                                                      Save Change
                                                    </button>
                                                  </div>
                                                ) : (
                                                  ""
                                                )
                                              )
                                            : "")}
                                      </>
                                    )}
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </main>
                      )}
                    </div>
                  ) : step1 == "11" ? (
                    <div className="float-left w-100">
                      <Loader isActive={isActive} />
                      {isShow && (
                        <Toaster
                          types={types}
                          isShow={isShow}
                          toasterBody={toasterBody}
                          message={message}
                        />
                      )}
                      <main className="page-wrapper getContact_page_wrap">
                        <div className="content-overlay mt-0">
                          <div className="">
                            <div className="float-left w-100 d-flex align-items-center justify-content-between mb-3">
                              <h3
                                className="pull-left mb-0 text-white"
                                id="getContact"
                              >
                                Documents
                              </h3>
                            </div>
                            <div className="d-flex align-items-center justify-content-between float-left mb-3">
                              <h6
                                className="text-white text-left mb-0"
                                id="info"
                              >
                                Signed Documents
                              </h6>
                            </div>
                            <div className="">
                              <div className="">
                                <div className="">
                                  <div
                                    className="border bg-light rounded-3 p-3"
                                    id="auth-info"
                                  >
                                    <div className="border-bottom pb-3 mb-5">
                                      <h6 className="pull-left">Paperwork </h6>
                                      <div className="float-left w-100 d-flex align-items-center justify-content-between">
                                        <div className="pe-2 mt-2">
                                          {Object.keys(checkboxState).length >
                                          0 ? (
                                            <span className="pull-right w-100">
                                              <button
                                                type="button"
                                                className="btn btn-primary mt-1 float-left"
                                                data-toggle="modal"
                                                data-target="#SendSelectedDocumentsOnBoard"
                                              >
                                                Send Selected Documents
                                              </button>
                                            </span>
                                          ) : (
                                            ""
                                          )}
                                          <label
                                            for="staticEmail"
                                            className="col-form-label"
                                          >
                                            <span>
                                              {getContact.contact_paperwork
                                                ? getContact.contact_paperwork
                                                    .length
                                                : 0}{" "}
                                              paperwork documents on file
                                            </span>
                                          </label>
                                        </div>
                                        {localStorage.getItem("auth") &&
                                        jwt(localStorage.getItem("auth"))
                                          .contactType == "admin" ? (
                                          <div
                                            data-bs-toggle="tooltip"
                                            license_type="Edit"
                                          ></div>
                                        ) : (
                                          <>
                                            {localStorage.getItem("auth") &&
                                              jwt(localStorage.getItem("auth"))
                                                .contactType == "staff" &&
                                              (Array.isArray(formData) &&
                                              formData.length > 0
                                                ? formData.map((item) =>
                                                    item.roleStaff ===
                                                    "add_contact" ? (
                                                      <div
                                                        data-bs-toggle="tooltip"
                                                        license_type="Edit"
                                                      ></div>
                                                    ) : (
                                                      ""
                                                    )
                                                  )
                                                : "")}
                                          </>
                                        )}
                                      </div>
                                      <div
                                        className=""
                                        id="license_type-collapse"
                                        data-bs-parent=""
                                      >
                                        <p className="mb-0">
                                          Paperwork Summary{" "}
                                          {localStorage.getItem("auth") &&
                                          (jwt(localStorage.getItem("auth"))
                                            .contactType == "admin" ||
                                            jwt(localStorage.getItem("auth"))
                                              .id == params.id) ? (
                                            <button
                                              type="button"
                                              className="btn btn-info pull-right"
                                              data-toggle="modal"
                                              data-target="#ModalAddpaperworkdocument"
                                            >
                                              Add Paperwork
                                            </button>
                                          ) : (
                                            <>
                                              {localStorage.getItem("auth") &&
                                                jwt(
                                                  localStorage.getItem("auth")
                                                ).contactType == "staff" &&
                                                (Array.isArray(formData) &&
                                                formData.length > 0
                                                  ? formData.map((item) =>
                                                      item.roleStaff ===
                                                      "add_contact" ? (
                                                        <button
                                                          type="button"
                                                          className="btn btn-info btn-lg mb-5 pull-right"
                                                          data-toggle="modal"
                                                          data-target="#ModalAddpaperworkdocument"
                                                        >
                                                          Add Paperwork
                                                        </button>
                                                      ) : (
                                                        ""
                                                      )
                                                    )
                                                  : "")}
                                            </>
                                          )}
                                        </p>
                                        {getContact.contact_paperwork &&
                                        getContact.contact_paperwork.length > 0
                                          ? getContact.contact_paperwork.map(
                                              (item) => {
                                                const isChecked =
                                                  checkboxState[item._id] ||
                                                  false;
                                                return (
                                                  <p
                                                    key={item._id}
                                                    className="mb-5"
                                                  >
                                                    {localStorage.getItem(
                                                      "auth"
                                                    ) &&
                                                    jwt(
                                                      localStorage.getItem(
                                                        "auth"
                                                      )
                                                    ).contactType ===
                                                      "admin" ? (
                                                      <input
                                                        type="checkbox"
                                                        id={`checkbox-${item._id}`}
                                                        name={`checkbox-${item._id}`}
                                                        checked={isChecked}
                                                        onChange={() =>
                                                          handleCheckboxChange(
                                                            item._id
                                                          )
                                                        }
                                                      />
                                                    ) : (
                                                      <>
                                                        {localStorage.getItem(
                                                          "auth"
                                                        ) &&
                                                          jwt(
                                                            localStorage.getItem(
                                                              "auth"
                                                            )
                                                          ).contactType ==
                                                            "staff" &&
                                                          (Array.isArray(
                                                            formData
                                                          ) &&
                                                          formData.length > 0
                                                            ? formData.map(
                                                                (item) =>
                                                                  item.roleStaff ===
                                                                  "add_contact" ? (
                                                                    <input
                                                                      type="checkbox"
                                                                      id={`checkbox-${item._id}`}
                                                                      name={`checkbox-${item._id}`}
                                                                      checked={
                                                                        isChecked
                                                                      }
                                                                      onChange={() =>
                                                                        handleCheckboxChange(
                                                                          item._id
                                                                        )
                                                                      }
                                                                    />
                                                                  ) : (
                                                                    ""
                                                                  )
                                                              )
                                                            : "")}
                                                      </>
                                                    )}
                                                    {item
                                                      .contact_paperworkdocument[0]
                                                      .approvalStatus ===
                                                    "yes" ? (
                                                      <i
                                                        className="fa fa-check ms-3"
                                                        aria-hidden="true"
                                                      ></i>
                                                    ) : (
                                                      <i
                                                        className="fa fa-times ms-3"
                                                        aria-hidden="true"
                                                      ></i>
                                                    )}
                                                    &nbsp;
                                                    <b>
                                                      {item.paperwork_title}
                                                    </b>{" "}
                                                    -{" "}
                                                    <a
                                                      href={
                                                        item
                                                          .contact_paperworkdocument[0]
                                                          .documenturl
                                                      }
                                                      target="_blank"
                                                      rel="noopener noreferrer"
                                                    >
                                                      View File
                                                    </a>
                                                    <br />
                                                    <span>
                                                      {new Date(
                                                        item.contact_paperworkdocument[0].created
                                                      ).toLocaleString(
                                                        "en-US",
                                                        {
                                                          month: "short",
                                                          day: "numeric",
                                                          year: "numeric",
                                                          hour: "numeric",
                                                          minute: "numeric",
                                                        }
                                                      )}
                                                      <small>
                                                        {" "}
                                                        {getTimeDifference(
                                                          item
                                                            .contact_paperworkdocument[0]
                                                            .created
                                                        )}
                                                      </small>
                                                      {localStorage.getItem(
                                                        "auth"
                                                      ) &&
                                                      jwt(
                                                        localStorage.getItem(
                                                          "auth"
                                                        )
                                                      ).contactType ===
                                                        "admin" ? (
                                                        <span className="ms-4">
                                                          {item.contact_paperworkdocument &&
                                                          item
                                                            .contact_paperworkdocument
                                                            .length > 0 &&
                                                          item
                                                            .contact_paperworkdocument[0]
                                                            .approvalStatus ===
                                                            "yes" ? (
                                                            <button
                                                              type="button"
                                                              onClick={() =>
                                                                handleApprovePaperwork(
                                                                  item
                                                                    .contact_paperworkdocument[0]
                                                                    ._id,
                                                                  "no"
                                                                )
                                                              }
                                                              className="btn btn-translucent-primary"
                                                            >
                                                              Reject
                                                            </button>
                                                          ) : (
                                                            <button
                                                              type="button"
                                                              onClick={() =>
                                                                handleApprovePaperwork(
                                                                  item
                                                                    .contact_paperworkdocument[0]
                                                                    ._id,
                                                                  "yes"
                                                                )
                                                              }
                                                              className="btn btn-translucent-primary"
                                                            >
                                                              Approve
                                                            </button>
                                                          )}
                                                        </span>
                                                      ) : (
                                                        <>
                                                          {localStorage.getItem(
                                                            "auth"
                                                          ) &&
                                                            jwt(
                                                              localStorage.getItem(
                                                                "auth"
                                                              )
                                                            ).contactType ==
                                                              "staff" &&
                                                            (Array.isArray(
                                                              formData
                                                            ) &&
                                                            formData.length > 0
                                                              ? formData.map(
                                                                  (item) =>
                                                                    item.roleStaff ===
                                                                    "add_contact" ? (
                                                                      <span className="ms-4">
                                                                        {item.contact_paperworkdocument &&
                                                                        item
                                                                          .contact_paperworkdocument
                                                                          .length >
                                                                          0 &&
                                                                        item
                                                                          .contact_paperworkdocument[0]
                                                                          .approvalStatus ===
                                                                          "yes" ? (
                                                                          <button
                                                                            type="button"
                                                                            onClick={() =>
                                                                              handleApprovePaperwork(
                                                                                item
                                                                                  .contact_paperworkdocument[0]
                                                                                  ._id,
                                                                                "no"
                                                                              )
                                                                            }
                                                                            className="btn btn-translucent-primary"
                                                                          >
                                                                            Reject
                                                                          </button>
                                                                        ) : (
                                                                          <button
                                                                            type="button"
                                                                            onClick={() =>
                                                                              handleApprovePaperwork(
                                                                                item
                                                                                  .contact_paperworkdocument[0]
                                                                                  ._id,
                                                                                "yes"
                                                                              )
                                                                            }
                                                                            className="btn btn-translucent-primary"
                                                                          >
                                                                            Approve
                                                                          </button>
                                                                        )}
                                                                      </span>
                                                                    ) : (
                                                                      ""
                                                                    )
                                                                )
                                                              : "")}
                                                        </>
                                                      )}
                                                    </span>
                                                  </p>
                                                );
                                              }
                                            )
                                          : ""}
                                      
                                        {getContact.document_type &&
                                        getContact.document_type.length > 0 ? (
                                          getContact.document_type.map(
                                            (doc, index) => (
                                              <div
                                                key={index}
                                                className="d-flex mb-3"
                                              >
                                                <b>{doc.documentName}</b> -{" "}
                                                <a
                                                  href={doc.documenturl}
                                                  target="_blank"
                                                  rel="noopener noreferrer"
                                                >
                                                  View File
                                                </a>
                                              </div>
                                            )
                                          )
                                        ) : (
                                         ""
                                        )}

                                        {localStorage.getItem("auth") &&
                                        jwt(localStorage.getItem("auth"))
                                          .contactType == "admin" ? (
                                          <button
                                            type="button"
                                            className="btn btn-info pull-right mt-4"
                                            data-toggle="modal"
                                            data-target="#ModalAdditionalpaperworkdocument"
                                          >
                                            Add Additional Documents
                                          </button>
                                        ) : (
                                          ""
                                        )}
                                      </div>
                                    </div>
                                  </div>
                                  {getContact.contact_status == "active" &&
                                  getContact.onboard_approve == "yes" &&
                                  getContact.onboardfinal == "done" ? (
                                    <></>
                                  ) : (
                                    <>
                                      {localStorage.getItem("auth") &&
                                      jwt(localStorage.getItem("auth"))
                                        .contactType == "admin" ? (
                                        <>
                                          <div
                                            className="border bg-light rounded-3 p-3 mt-5"
                                            id="auth-info"
                                          >
                                            <div className="border-bottom pb-3 mb-3">
                                              <h6 className="pull-left">
                                                Admin Can approve onboarding for
                                                this agent at own risk.{" "}
                                              </h6>

                                              <div className="float-left w-100 d-flex align-items-center justify-content-between">
                                                <div className="pe-2 mt-2">
                                                  <label
                                                    for="staticEmail"
                                                    className="col-form-label"
                                                  >
                                                    <span>
                                                      Required documents for
                                                      onboarding agent
                                                    </span>
                                                  </label>
                                                </div>

                                                <div className="pe-2 mt-2">
                                                  <div className="mb-0">
                                                    {/* Paperwork Summary{" "} */}
                                                    {localStorage.getItem(
                                                      "auth"
                                                    ) &&
                                                    (jwt(
                                                      localStorage.getItem(
                                                        "auth"
                                                      )
                                                    ).contactType == "admin" ||
                                                      jwt(
                                                        localStorage.getItem(
                                                          "auth"
                                                        )
                                                      ).id == params.id) ? (
                                                      <button
                                                        type="button"
                                                        className="btn btn-info btn-lg mb-0 pull-right"
                                                        data-toggle="modal"
                                                        data-target="#ModalAddpaperworkdocument"
                                                      >
                                                        Add Paperwork
                                                      </button>
                                                    ) : (
                                                      ""
                                                    )}
                                                  </div>
                                                </div>
                                              </div>
                                              <div className="float-left w-100 ">
                                                {/* <p>Agent required On-Board Documents</p><br /> */}
                                                <p>W9</p>
                                                <p>
                                                  Independent Contractor
                                                  Agreement{" "}
                                                </p>
                                                {onBoard && onBoard.length > 0
                                                  ? onBoard.map((item) => (
                                                      <p key={item._id}>
                                                        {
                                                          item
                                                            .agentPaperNameData
                                                            .paperwork_title
                                                        }
                                                      </p>
                                                    ))
                                                  : ""}

                                                <div className="mb-0">
                                                  {/* contact_status : "active"
                                              onboard_approve :  "yes"
                                              onboard_signature: response.data,
                                              onboardfinal :  "done"

                                              onboard_taxcertification: uploadResponse.data, */}
                                                  {approveOnbaord ? (
                                                    <button
                                                      type="button"
                                                      className="btn btn-info btn-lg mb-5 pull-right"
                                                      onClick={() =>
                                                        handleApproveonboard()
                                                      }
                                                    >
                                                      Approve Onboard
                                                    </button>
                                                  ) : (
                                                    <p style={{ color: "red" }}>
                                                      Once the agent completes
                                                      the password step on
                                                      onbaord, then admin can
                                                      submit the agent for
                                                      On-Board .
                                                    </p>
                                                  )}
                                                </div>
                                              </div>
                                            </div>
                                          </div>
                                        </>
                                      ) : (
                                        <>
                                          {localStorage.getItem("auth") &&
                                            jwt(localStorage.getItem("auth"))
                                              .contactType == "staff" &&
                                            (Array.isArray(formData) &&
                                            formData.length > 0
                                              ? formData.map((item) =>
                                                  item.roleStaff ===
                                                  "add_contact" ? (
                                                    <>
                                                      <div
                                                        className="border bg-light rounded-3 p-3 mt-5"
                                                        id="auth-info"
                                                      >
                                                        <div className="border-bottom pb-3 mb-3">
                                                          <h6 className="pull-left">
                                                            Admin Can approve
                                                            onboarding for this
                                                            agent at own risk.{" "}
                                                          </h6>

                                                          <div className="float-left w-100 d-flex align-items-center justify-content-between">
                                                            <div className="pe-2 mt-2">
                                                              <label
                                                                for="staticEmail"
                                                                className="col-form-label"
                                                              >
                                                                <span>
                                                                  Required
                                                                  documents for
                                                                  onboarding
                                                                  agent
                                                                </span>
                                                              </label>
                                                            </div>

                                                            <div className="pe-2 mt-2">
                                                              <div className="mb-0">
                                                                <>
                                                                  {localStorage.getItem(
                                                                    "auth"
                                                                  ) &&
                                                                    jwt(
                                                                      localStorage.getItem(
                                                                        "auth"
                                                                      )
                                                                    )
                                                                      .contactType ==
                                                                      "staff" &&
                                                                    (Array.isArray(
                                                                      formData
                                                                    ) &&
                                                                    formData.length >
                                                                      0
                                                                      ? formData.map(
                                                                          (
                                                                            item
                                                                          ) =>
                                                                            item.roleStaff ===
                                                                            "add_contact" ? (
                                                                              <button
                                                                                type="button"
                                                                                className="btn btn-info btn-lg mb-0 pull-right"
                                                                                data-toggle="modal"
                                                                                data-target="#ModalAddpaperworkdocument"
                                                                              >
                                                                                Add
                                                                                Paperwork
                                                                              </button>
                                                                            ) : (
                                                                              ""
                                                                            )
                                                                        )
                                                                      : "")}
                                                                </>
                                                              </div>
                                                            </div>
                                                          </div>
                                                          <div className="float-left w-100 ">
                                                            {/* <p>Agent required On-Board Documents</p><br /> */}
                                                            <p>W9</p>
                                                            <p>
                                                              Independent
                                                              Contractor
                                                              Agreement{" "}
                                                            </p>
                                                            {onBoard &&
                                                            onBoard.length > 0
                                                              ? onBoard.map(
                                                                  (item) => (
                                                                    <p
                                                                      key={
                                                                        item._id
                                                                      }
                                                                    >
                                                                      {
                                                                        item
                                                                          .agentPaperNameData
                                                                          .paperwork_title
                                                                      }
                                                                    </p>
                                                                  )
                                                                )
                                                              : ""}

                                                            <div className="mb-0">
                                                              {/* contact_status : "active"
                                                      onboard_approve :  "yes"
                                                      onboard_signature: response.data,
                                                      onboardfinal :  "done"
        
                                                      onboard_taxcertification: uploadResponse.data, */}
                                                              {approveOnbaord ? (
                                                                <button
                                                                  type="button"
                                                                  className="btn btn-info btn-lg mb-5 pull-right"
                                                                  onClick={() =>
                                                                    handleApproveonboard()
                                                                  }
                                                                >
                                                                  Approve
                                                                  Onboard
                                                                </button>
                                                              ) : (
                                                                <p
                                                                  style={{
                                                                    color:
                                                                      "red",
                                                                  }}
                                                                >
                                                                  Once the agent
                                                                  completes the
                                                                  password step
                                                                  on onbaord,
                                                                  then admin can
                                                                  submit the
                                                                  agent for
                                                                  On-Board .
                                                                </p>
                                                              )}
                                                            </div>
                                                          </div>
                                                        </div>
                                                      </div>
                                                    </>
                                                  ) : (
                                                    ""
                                                  )
                                                )
                                              : "")}
                                        </>
                                      )}
                                    </>
                                  )}
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                        <OnboardDocuments
                          checkboxState={checkboxState}
                          clearcheckboxState={clearcheckboxState}
                        />
                      </main>
                    </div>
                  ) : step1 == "3" ? (
                    <div className="float-left w-100">
                      <Loader isActive={isActive} />
                      {isShow && (
                        <Toaster
                          types={types}
                          isShow={isShow}
                          toasterBody={toasterBody}
                          message={message}
                        />
                      )}
                      <main className="page-wrapper getContact_page_wrap">
                        <div className="content-overlay mt-0">
                          <div className="">
                            <div className="float-left w-100 d-flex align-items-center justify-content-between mb-3">
                              <h3
                                className="pull-left mb-0 text-white"
                                id="getContact"
                              >
                                Settings
                              </h3>
                            </div>

                            <div className="float-left w-100 d-flex align-items-center justify-content-between mb-3">
                              <h6 className="pull-left mb-0 text-white">
                                Account Settings
                              </h6>
                            </div>
                            <div className="border bg-light rounded-3 p-3">
                              <div className="row">
                                <div className="col-lg-12 mt-2">
                                  <div className="col-lg-12 d-flex align-items-center justify-content-start mb-2">
                                    <i
                                      className="fa fa-calendar-check-o"
                                      aria-hidden="true"
                                    ></i>
                                    <label className="col-form-label ms-2">
                                      Calendar Subscriptions
                                    </label>
                                    {localStorage.getItem("auth") &&
                                    (jwt(localStorage.getItem("auth"))
                                      .contactType == "admin" ||
                                      jwt(localStorage.getItem("auth")).id ==
                                        params.id) ? (
                                      <NavLink
                                        to={`/profile-setting/calendar-subscription/${
                                          jwt(localStorage.getItem("auth")).id
                                        }`}
                                        className="ms-4"
                                      >
                                        View Subscriptions
                                      </NavLink>
                                    ) : (
                                      <>
                                        {localStorage.getItem("auth") &&
                                          jwt(localStorage.getItem("auth"))
                                            .contactType == "staff" &&
                                          (Array.isArray(formData) &&
                                          formData.length > 0
                                            ? formData.map((item) =>
                                                item.roleStaff ===
                                                "add_contact" ? (
                                                  <NavLink
                                                    to={`/profile-setting/calendar-subscription/${
                                                      jwt(
                                                        localStorage.getItem(
                                                          "auth"
                                                        )
                                                      ).id
                                                    }`}
                                                    className="ms-4"
                                                  >
                                                    View Subscriptions
                                                  </NavLink>
                                                ) : (
                                                  ""
                                                )
                                              )
                                            : "")}
                                      </>
                                    )}
                                  </div>

                                  <div className="col-lg-12 d-flex align-items-center justify-content-start mb-2">
                                    <i
                                      className="fa fa-envelope-square"
                                      aria-hidden="true"
                                    ></i>
                                    <label className="col-form-label ms-2">
                                      Digest Emails
                                    </label>
                                    {localStorage.getItem("auth") &&
                                    (jwt(localStorage.getItem("auth"))
                                      .contactType == "admin" ||
                                      jwt(localStorage.getItem("auth")).id ==
                                        params.id) ? (
                                      <NavLink
                                        className="ms-4"
                                        to={`/profile/settings/digest/${params.id}`}
                                      >
                                        View Digest Settings
                                      </NavLink>
                                    ) : (
                                      <>
                                        {localStorage.getItem("auth") &&
                                          jwt(localStorage.getItem("auth"))
                                            .contactType == "staff" &&
                                          (Array.isArray(formData) &&
                                          formData.length > 0
                                            ? formData.map((item) =>
                                                item.roleStaff ===
                                                "add_contact" ? (
                                                  <NavLink
                                                    className="ms-4"
                                                    to={`/profile/settings/digest/${params.id}`}
                                                  >
                                                    View Digest Settings
                                                  </NavLink>
                                                ) : (
                                                  ""
                                                )
                                              )
                                            : "")}
                                      </>
                                    )}
                                  </div>

                                  <div className="col-lg-12 d-flex align-items-center justify-content-start mb-2">
                                    <i
                                      className="fa fa-building"
                                      aria-hidden="true"
                                    ></i>
                                    <label className="col-form-label ms-2">
                                      Departments
                                    </label>
                                    {localStorage.getItem("auth") &&
                                    (jwt(localStorage.getItem("auth"))
                                      .contactType == "admin" ||
                                      jwt(localStorage.getItem("auth")).id ==
                                        params.id) ? (
                                      <a className="ms-4" href="#">
                                        View Departments
                                      </a>
                                    ) : (
                                      <>
                                        {localStorage.getItem("auth") &&
                                          jwt(localStorage.getItem("auth"))
                                            .contactType == "staff" &&
                                          (Array.isArray(formData) &&
                                          formData.length > 0
                                            ? formData.map((item) =>
                                                item.roleStaff ===
                                                "add_contact" ? (
                                                  <a className="ms-4" href="#">
                                                    View Departments
                                                  </a>
                                                ) : (
                                                  ""
                                                )
                                              )
                                            : "")}
                                      </>
                                    )}
                                  </div>

                                  {/* <div className="col-lg-12 d-flex align-items-center justify-content-start mb-2">
                                    <i
                                      className="fa fa-envelope-square"
                                      aria-hidden="true"
                                    ></i>
                                    <label className="col-form-label ms-2">
                                      Digest Emails
                                    </label>
                                    {localStorage.getItem("auth") &&
                                    (jwt(localStorage.getItem("auth"))
                                      .contactType == "admin" ||
                                      jwt(localStorage.getItem("auth")).id ==
                                        params.id) ? (
                                      <a className="ms-4" href="#">
                                        View Digest Settings
                                      </a>
                                    ) : (
                                      ""
                                    )}
                                  </div> */}
                                  {/* <div className="col-lg-12 d-flex align-items-center justify-content-start mb-2">
                                    <i
                                      className="fa fa-user"
                                      aria-hidden="true"
                                    ></i>
                                    <label className="col-form-label ms-2">
                                      Roles
                                    </label>
                                    {localStorage.getItem("auth") &&
                                    (jwt(localStorage.getItem("auth"))
                                      .contactType == "admin" ||
                                      jwt(localStorage.getItem("auth")).id ==
                                        params.id) ? (
                                      <a className="ms-4" href="#">
                                        View Roles
                                      </a>
                                    ) : (
                                      ""
                                    )}
                                  </div> */}

                                  {/* <div className="col-lg-12 d-flex align-items-center justify-content-start mb-2">
                                    <i
                                      className="fa fa-bell"
                                      aria-hidden="true"
                                    ></i>
                                    <label className="col-form-label ms-2">
                                      Notifications
                                    </label>
                                    {localStorage.getItem("auth") &&
                                    (jwt(localStorage.getItem("auth"))
                                      .contactType == "admin" ||
                                      jwt(localStorage.getItem("auth")).id ==
                                        params.id) ? (
                                      <a className="ms-4" href="#">
                                        View Notifications
                                      </a>
                                    ) : (
                                      ""
                                    )}
                                  </div> */}
                                  {/* <div className="col-lg-12 d-flex align-items-center justify-content-start mb-2">
                                    <i
                                      className="fa fa-bell-o"
                                      aria-hidden="true"
                                    ></i>
                                    <label className="col-form-label ms-2">
                                      {" "}
                                      Transaction Notifications
                                    </label>
                                    {localStorage.getItem("auth") &&
                                    (jwt(localStorage.getItem("auth"))
                                      .contactType == "admin" ||
                                      jwt(localStorage.getItem("auth")).id ==
                                        params.id) ? (
                                      <a className="ms-4" href="#">
                                        View Notifications
                                      </a>
                                    ) : (
                                      ""
                                    )}
                                  </div> */}

                                  <div className="col-lg-12 d-flex align-items-center justify-content-start mb-2">
                                    <i
                                      className="fa fa-envelope-square"
                                      aria-hidden="true"
                                    ></i>
                                    <label className="col-form-label ms-2">
                                      Email Signature
                                    </label>
                                    {localStorage.getItem("auth") &&
                                    (jwt(localStorage.getItem("auth"))
                                      .contactType == "admin" ||
                                      jwt(localStorage.getItem("auth")).id ==
                                        params.id) ? (
                                      <NavLink
                                        className="ms-4"
                                        to={`/email-signature/${params.id}`}
                                      >
                                        View Email Signature
                                      </NavLink>
                                    ) : (
                                      <>
                                        {localStorage.getItem("auth") &&
                                          jwt(localStorage.getItem("auth"))
                                            .contactType == "staff" &&
                                          (Array.isArray(formData) &&
                                          formData.length > 0
                                            ? formData.map((item) =>
                                                item.roleStaff ===
                                                "add_contact" ? (
                                                  <NavLink
                                                    className="ms-4"
                                                    to={`/email-signature/${params.id}`}
                                                  >
                                                    View Email Signature
                                                  </NavLink>
                                                ) : (
                                                  ""
                                                )
                                              )
                                            : "")}
                                      </>
                                    )}
                                  </div>
                                  <div className="col-lg-12 d-flex align-items-center justify-content-start mb-2">
                                    <i
                                      className="fa fa-envelope-square"
                                      aria-hidden="true"
                                    ></i>
                                    <label className="col-form-label ms-2">
                                      Staff Signature
                                    </label>
                                    {localStorage.getItem("auth") &&
                                    (jwt(localStorage.getItem("auth"))
                                      .contactType == "admin" ||
                                      jwt(localStorage.getItem("auth")).id ==
                                        params.id) ? (
                                      <NavLink
                                        className="ms-4"
                                        to={`/staff-signature/${params.id}`}
                                      >
                                        View Signature
                                      </NavLink>
                                    ) : (
                                      <>
                                        {localStorage.getItem("auth") &&
                                          jwt(localStorage.getItem("auth"))
                                            .contactType == "staff" &&
                                          (Array.isArray(formData) &&
                                          formData.length > 0
                                            ? formData.map((item) =>
                                                item.roleStaff ===
                                                "add_contact" ? (
                                                  <NavLink
                                                    className="ms-4"
                                                    to={`/staff-signature/${params.id}`}
                                                  >
                                                    View Signature
                                                  </NavLink>
                                                ) : (
                                                  ""
                                                )
                                              )
                                            : "")}
                                      </>
                                    )}
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </main>
                    </div>
                  ) : step1 == "4" ? (
                    <div className="float-left w-100">
                      <Loader isActive={isActive} />
                      {isShow && (
                        <Toaster
                          types={types}
                          isShow={isShow}
                          toasterBody={toasterBody}
                          message={message}
                        />
                      )}
                      <main className="page-wrapper getContact_page_wrap">
                        <div className="content-overlay mt-0">
                          <div className="">
                            <div className="float-left w-100 d-flex align-items-center justify-content-between mb-3">
                              <h3
                                className="pull-left mb-0 text-white"
                                id="getContact"
                              >
                                Activity
                              </h3>
                            </div>

                            <div className="float-left w-100 d-flex align-items-center justify-content-between mb-3">
                              <h6 className="text-left mb-0 text-white">
                                Account Access
                              </h6>
                            </div>

                            {/* <div className="row">
                                                <div className="col-lg-3">
                                                    <h2 className="h4" id="info">Activity Overview</h2>
                                                </div><br />
                                                <div className="col-lg-9">
                                                    <h6 className="ms-5">Account Access</h6>
                                                    <div className="col-md-12">
                                                        <div className="col-md-12">
                                                            <p className="ms-5">Login Status: Active</p>
                                                        </div>
                                                        <div className="col-md-12">
                                                            <p className="ms-5">Last Login:

                                                                {
                                                                    getContact.lastlogin ?
                                                                        new Date(getContact.lastlogin).toLocaleString('en-US', {
                                                                            month: 'short',
                                                                            day: 'numeric',
                                                                            year: 'numeric',
                                                                            hour: 'numeric',
                                                                            minute: 'numeric',
                                                                        })
                                                                        : ""

                                                                }</p>
                                                        </div>
                                                    </div>
                                                    <div className="col-lg-3">
                                                        <h6 className="ms-5">Account Access</h6><br />
                                                    </div>
                                                </div>


                                            </div> */}
                            <div className="border bg-light rounded-3 p-3">
                              <div className="col-md-12">
                                <div>
                                  {/* <div className="Clear">&nbsp;</div> */}

                                  <div className=" d-flex align-items-center justify-content-between">
                                    <div className="float-right w-50">
                                      <label className="col-form-label  mb-0">
                                        Login Status
                                      </label>
                                    </div>
                                    <div className="float-left w-100">
                                      Offline
                                    </div>
                                  </div>
                                  <div className=" d-flex align-items-center justify-content-between">
                                    <div className="float-right w-50">
                                      <label className="col-form-label  mb-0">
                                        {" "}
                                        Last Login
                                      </label>
                                    </div>
                                    <div className="float-left w-100">
                                      {getContact.lastlogin
                                        ? new Date(
                                            getContact.lastlogin
                                          ).toLocaleString("en-US", {
                                            month: "short",
                                            day: "numeric",
                                            year: "numeric",
                                            hour: "numeric",
                                            minute: "numeric",
                                          })
                                        : ""}
                                    </div>
                                  </div>
                                  <div className=" d-flex align-items-center justify-content-between">
                                    <div className="float-right w-50">
                                      <label className="col-form-label  mb-0">
                                        Login Frequency
                                      </label>
                                    </div>
                                    <div
                                      className="float-left w-100"
                                      title="26 Sessions – 253 Days"
                                    >
                                      Monthly
                                    </div>
                                    {/* <div className="InfoLabel column medium-4 small-12">Login History</div>
                                                                        <div className="InfoText column medium-8 small-12"><a href="/profile/activity/sessions/?05c4d7b2-f471-4b7d-b87b-b5a45f48cf71">View Report</a></div> */}
                                    <div className="InfoSpacer">&nbsp;</div>
                                  </div>
                                </div>
                                <div id="PeopleTransSummary" className="mt-4">
                                  <div id="ctl00_PageContent_TransSummaryPanel">
                                    <div className="SectionTitle">
                                      <h6 className="">Transaction Overview</h6>
                                    </div>
                                    <div className="ClearLeft"></div>
                                    {contactListings &&
                                    contactListings.totalRecords > 0 ? (
                                      <div className="IndentStandard mb-3">
                                        <div
                                          className="InfoSpacer"
                                          style={{ paddingtop: "6px" }}
                                        >
                                          <strong>
                                            <NavLink
                                              to={`/contactlisting/listing/${params.id}`}
                                            >
                                              {contactListings.totalRecords}{" "}
                                              Listing
                                            </NavLink>
                                          </strong>
                                        </div>
                                        <div className="ClearLeft"></div>
                                      </div>
                                    ) : (
                                      ""
                                    )}
                                    <div
                                      className="IndentStandard"
                                      style={{ paddingtop: "6px" }}
                                    >
                                      <div
                                        className="InfoSpacer"
                                        style={{ paddingtop: "6px" }}
                                      >
                                        <strong>
                                          <NavLink
                                            to={`/agentcontact/transaction/${params.id}`}
                                          >
                                            {contactListings &&
                                            contactListings.totalRecords_transactionagent
                                              ? contactListings.totalRecords_transactionagent +
                                                " "
                                              : "0 "}
                                            Active Transactions
                                          </NavLink>
                                        </strong>
                                      </div>
                                      {/* <div className="ClearLeft"></div>
                                                                <div className="InfoListItem">2 Commercial Lease</div><div className="InfoListItem">1 Commercial Sale</div><div className="InfoListItem">14 Residential Sale</div><div className="ClearLeft"></div>
                                                                <div className="InfoListItem">14 Unsubmitted</div><div className="InfoListItem">1 In Review</div><div className="InfoListItem">2 Approved</div><div className="ClearLeft"></div>
                                                                <div className="ClearLeft" style={{ height: "5px" }}></div> */}
                                    </div>
                                    {/* <div className="IndentStandard" style={{ paddingtop: "0px" }}>
                                                                <div className="InfoSpacer" style={{ paddingtop: "6px" }}><strong><a href="/profile/activity/manage/filed/?05c4d7b2-f471-4b7d-b87b-b5a45f48cf71">40 Filed Transactions</a></strong></div>
                                                                <div className="ClearLeft"></div>
                                                                <div className="InfoListItem">4 Commercial Lease</div><div className="InfoListItem">36 Residential Sale</div><div className="ClearLeft"></div>
                                                                <div className="InfoListItem">40 Standard Funding</div><div className="ClearLeft"></div>
                                                            </div> */}
                                    {/* <div className="InfoSpacer">&nbsp;</div>
                                                            <div className="ClearLeft"></div>
                                                            <div className="SectionTitle">Participation Summary</div>
                                                            <div className="InfoSpacer">Quick summary of your transactions with this contact.</div>
                                                            <div className="IndentStandard">
                                                                <div className="InfoSpacer" style={{ paddingtop: "6px" }}><strong><a href="#PeopleTransDetail" onclick="return openParticipationPanel();">0 Active Transactions</a></strong></div>
                                                                <div className="ClearLeft"></div>
                                                                <div className="ClearLeft"></div>
                                                                <div className="ClearLeft"></div>
                                                                <div className="ClearLeft"></div>
                                                            </div>
                                                            <div className="IndentStandard">
                                                                <div className="InfoSpacer" style={{ paddingtop: "6px" }}><strong><a href="/profile/activity/history/?05c4d7b2-f471-4b7d-b87b-b5a45f48cf71">0 Filed Transactions</a></strong></div>
                                                                <div className="ClearLeft"></div>
                                                                <div className="ClearLeft"></div>
                                                                <div className="ClearLeft"></div>
                                                                <div className="ClearLeft"></div>
                                                            </div>
                                                            <div className="IndentStandard">
                                                                <div className="InfoSpacer" style={{ paddingtop: "6px" }}><strong><a href="/profile/activity/favorites/?05c4d7b2-f471-4b7d-b87b-b5a45f48cf71">0 Favorites</a></strong> — No AgentSite Account</div>
                                                                <div className="ClearLeft"></div>
                                                            </div>
                                                             */}
                                  </div>
                                </div>

                                <div
                                  id="PeopleTransDetail"
                                  style={{
                                    display: "none",
                                    position: "relative",
                                  }}
                                >
                                  <div className="SectionTitle">
                                    PARTICIPATION
                                  </div>
                                  <div className="InfoSpacer">
                                    Details of your active transactions with
                                    this contact.
                                  </div>
                                  <div className="Clear"></div>
                                  <div
                                    className="IndentStandard"
                                    style={{
                                      paddingtop: "6px",
                                      paddingbottom: "6px",
                                    }}
                                  >
                                    <strong>0 Active Transactions</strong>
                                  </div>

                                  <div className="ClearLeft"></div>
                                  <div className="">&nbsp;</div>
                                </div>
                              </div>
                              <div className="col-md-12">
                                <div className="mt-5">
                                  <strong>
                                    <NavLink
                                      to={`/contact/notice/${params.id}`}
                                      className="mt-3"
                                    >
                                      Past Notice
                                    </NavLink>
                                  </strong>
                                </div>
                                <div className="">
                                  <h6 className="mt-3">Associate Reports</h6>
                                  <ul className="TransReportList mb-2"></ul>
                                </div>
                                <div className="Clear"></div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </main>
                    </div>
                  ) : step1 == "5" ? (
                    <div className="float-left w-100">
                      <Loader isActive={isActive} />
                      {isShow && (
                        <Toaster
                          types={types}
                          isShow={isShow}
                          toasterBody={toasterBody}
                          message={message}
                        />
                      )}
                      <main className="page-wrapper getContact_page_wrap">
                        <div className="content-overlay mt-0">
                          <div className="">
                            <div className="float-left w-100 d-flex align-items-center justify-content-between mb-3">
                              <h3
                                className="pull-left mb-0 text-white"
                                id="getContact"
                              >
                                About Me
                              </h3>
                            </div>

                            <div className="">
                              <div className="">
                                <div className="">
                                  <div
                                    className="border bg-light rounded-3 p-3"
                                    id="auth-info"
                                  >
                                    <div className="border-bottom pb-3 mb-3">
                                      <div className="d-flex align-items-center justify-content-between">
                                        <div className="pe-2">
                                          <label className="col-form-label">
                                            <span>
                                              <i
                                                className="fa fa-user m-2"
                                                aria-hidden="true"
                                              ></i>
                                              Your Full Name
                                            </span>
                                          </label>
                                          <div
                                            id="full_name-value"
                                            className="ms-3 mt-2"
                                          >
                                            {formValuesAboout?.full_name}
                                          </div>
                                        </div>
                                        {localStorage.getItem("auth") &&
                                        (jwt(localStorage.getItem("auth"))
                                          .contactType == "admin" ||
                                          jwt(localStorage.getItem("auth"))
                                            .id == params.id) ? (
                                          <div
                                            data-bs-toggle="tooltip"
                                            title="Edit"
                                          >
                                            <a
                                              className="nav-link py-0"
                                              href="#full_name-collapse"
                                              data-bs-toggle="collapse"
                                            >
                                              <i className="fi-edit"></i>
                                            </a>
                                          </div>
                                        ) : (
                                          <>
                                            {localStorage.getItem("auth") &&
                                              jwt(localStorage.getItem("auth"))
                                                .contactType == "staff" &&
                                              (Array.isArray(formData) &&
                                              formData.length > 0
                                                ? formData.map((item) =>
                                                    item.roleStaff ===
                                                    "add_contact" ? (
                                                      <div
                                                        data-bs-toggle="tooltip"
                                                        title="Edit"
                                                      >
                                                        <a
                                                          className="nav-link py-0"
                                                          href="#full_name-collapse"
                                                          data-bs-toggle="collapse"
                                                        >
                                                          <i className="fi-edit"></i>
                                                        </a>
                                                      </div>
                                                    ) : (
                                                      ""
                                                    )
                                                  )
                                                : "")}
                                          </>
                                        )}
                                      </div>
                                      <div
                                        className="collapse"
                                        id="full_name-collapse"
                                        data-bs-parent="#full_name"
                                      >
                                        <input
                                          placeholder="Your answer"
                                          className="form-control mt-3"
                                          type="text"
                                          name="full_name"
                                          value={formValuesAboout?.full_name}
                                          onChange={handleChangesAbout}
                                        />
                                        {/* {formErrors.full_name && (
                                        <div className="invalid-tooltip">{formErrors.full_name}</div>
                                    )} */}
                                      </div>
                                    </div>

                                    <div className="border-bottom pb-3 mb-3">
                                      <div className="d-flex align-items-center justify-content-between">
                                        <div className="pe-2">
                                          <label className="col-form-label">
                                            <span>Birthday</span>
                                          </label>
                                          {formattedDatesAbout !==
                                          "Invalid Date" ? (
                                            <div id="birthday-value">
                                              {formattedDatesAbout}
                                            </div>
                                          ) : null}
                                        </div>
                                        {localStorage.getItem("auth") &&
                                        (jwt(localStorage.getItem("auth"))
                                          .contactType == "admin" ||
                                          jwt(localStorage.getItem("auth"))
                                            .id == params.id) ? (
                                          <div
                                            data-bs-toggle="tooltip"
                                            title="Edit"
                                          >
                                            <a
                                              className="nav-link py-0"
                                              href="#birthday-collapse"
                                              data-bs-toggle="collapse"
                                            >
                                              <i className="fi-edit"></i>
                                            </a>
                                          </div>
                                        ) : (
                                          <>
                                            {localStorage.getItem("auth") &&
                                              jwt(localStorage.getItem("auth"))
                                                .contactType == "staff" &&
                                              (Array.isArray(formData) &&
                                              formData.length > 0
                                                ? formData.map((item) =>
                                                    item.roleStaff ===
                                                    "add_contact" ? (
                                                      <div
                                                        data-bs-toggle="tooltip"
                                                        title="Edit"
                                                      >
                                                        <a
                                                          className="nav-link py-0"
                                                          href="#birthday-collapse"
                                                          data-bs-toggle="collapse"
                                                        >
                                                          <i className="fi-edit"></i>
                                                        </a>
                                                      </div>
                                                    ) : (
                                                      ""
                                                    )
                                                  )
                                                : "")}
                                          </>
                                        )}
                                      </div>
                                      <div
                                        className="collapse"
                                        id="birthday-collapse"
                                        data-bs-parent="#birthday"
                                      >
                                        <input
                                          placeholder="Your answer"
                                          className="form-control mt-3"
                                          type="date"
                                          name="birthday"
                                          value={formValuesAboout?.birthday}
                                          onChange={handleChangesAbout}
                                          style={{
                                            border: formErrors?.birthday
                                              ? "1px solid red"
                                              : "1px solid #00000026",
                                          }}
                                          data-bs-binded-element="#birthday-value"
                                          data-bs-unset-value="Not specified"
                                        />
                                        {formErrors.birthday && (
                                          <div className="invalid-tooltip">
                                            {formErrors.birthday}
                                          </div>
                                        )}
                                      </div>
                                    </div>

                                    <div className="border-bottom pb-3 mb-3">
                                      <div className="d-flex align-items-center justify-content-between">
                                        <div className="pe-2">
                                          <label className="col-form-label">
                                            <span>
                                              Ideally, how would you spend your
                                              birthday?
                                            </span>
                                          </label>
                                          <div id="spouse-value">
                                            {formValuesAboout?.spend_birthday}
                                          </div>
                                        </div>
                                        {localStorage.getItem("auth") &&
                                        (jwt(localStorage.getItem("auth"))
                                          .contactType == "admin" ||
                                          jwt(localStorage.getItem("auth"))
                                            .id == params.id) ? (
                                          <div
                                            data-bs-toggle="tooltip"
                                            title="Edit"
                                          >
                                            <a
                                              className="nav-link py-0"
                                              href="#spouse-collapse"
                                              data-bs-toggle="collapse"
                                            >
                                              <i className="fi-edit"></i>
                                            </a>
                                          </div>
                                        ) : (
                                          <>
                                            {localStorage.getItem("auth") &&
                                              jwt(localStorage.getItem("auth"))
                                                .contactType == "staff" &&
                                              (Array.isArray(formData) &&
                                              formData.length > 0
                                                ? formData.map((item) =>
                                                    item.roleStaff ===
                                                    "add_contact" ? (
                                                      <div
                                                        data-bs-toggle="tooltip"
                                                        title="Edit"
                                                      >
                                                        <a
                                                          className="nav-link py-0"
                                                          href="#spouse-collapse"
                                                          data-bs-toggle="collapse"
                                                        >
                                                          <i className="fi-edit"></i>
                                                        </a>
                                                      </div>
                                                    ) : (
                                                      ""
                                                    )
                                                  )
                                                : "")}
                                          </>
                                        )}
                                      </div>
                                      <div
                                        className="collapse"
                                        id="spouse-collapse"
                                        data-bs-parent="#personal-details"
                                      >
                                        <input
                                          placeholder="Your answer"
                                          className="form-control mt-3"
                                          type="text"
                                          name="spend_birthday"
                                          value={
                                            formValuesAboout?.spend_birthday
                                          }
                                          onChange={handleChangesAbout}
                                          style={{
                                            border: formErrors?.spend_birthday
                                              ? "1px solid red"
                                              : "1px solid #00000026",
                                          }}
                                          data-bs-binded-element="#spouse-value"
                                          data-bs-unset-value="Not specified"
                                        />
                                        {formErrors.spend_birthday && (
                                          <div className="invalid-tooltip">
                                            {formErrors.spend_birthday}
                                          </div>
                                        )}
                                      </div>
                                    </div>

                                    <div className="border-bottom pb-3 mb-3">
                                      <div className="d-flex align-items-center justify-content-between">
                                        <div className="pe-2">
                                          <label className="col-form-label">
                                            <span>
                                              Spouse/Partner Name and Birthday?
                                            </span>
                                          </label>
                                          <div id="spend_birthday-value">
                                            {formValuesAboout?.spouse}
                                          </div>
                                        </div>
                                        {localStorage.getItem("auth") &&
                                        (jwt(localStorage.getItem("auth"))
                                          .contactType == "admin" ||
                                          jwt(localStorage.getItem("auth"))
                                            .id == params.id) ? (
                                          <div
                                            data-bs-toggle="tooltip"
                                            title="Edit"
                                          >
                                            <a
                                              className="nav-link py-0"
                                              href="#spend_birthday-collapse"
                                              data-bs-toggle="collapse"
                                            >
                                              <i className="fi-edit"></i>
                                            </a>
                                          </div>
                                        ) : (
                                          <>
                                            {localStorage.getItem("auth") &&
                                              jwt(localStorage.getItem("auth"))
                                                .contactType == "staff" &&
                                              (Array.isArray(formData) &&
                                              formData.length > 0
                                                ? formData.map((item) =>
                                                    item.roleStaff ===
                                                    "add_contact" ? (
                                                      <div
                                                        data-bs-toggle="tooltip"
                                                        title="Edit"
                                                      >
                                                        <a
                                                          className="nav-link py-0"
                                                          href="#spend_birthday-collapse"
                                                          data-bs-toggle="collapse"
                                                        >
                                                          <i className="fi-edit"></i>
                                                        </a>
                                                      </div>
                                                    ) : (
                                                      ""
                                                    )
                                                  )
                                                : "")}
                                          </>
                                        )}
                                      </div>
                                      <div
                                        className="collapse"
                                        id="spend_birthday-collapse"
                                        data-bs-parent="#personal-details"
                                      >
                                        <input
                                          placeholder="Your answer"
                                          className="form-control mt-3"
                                          type="text"
                                          name="spouse"
                                          value={formValuesAboout?.spouse}
                                          onChange={handleChangesAbout}
                                          style={{
                                            border: formErrors?.spouse
                                              ? "1px solid red"
                                              : "1px solid #00000026",
                                          }}
                                          data-bs-binded-element="#spend_birthday-value"
                                          data-bs-unset-value="Not specified"
                                        />
                                        {formErrors.spouse && (
                                          <div className="invalid-tooltip">
                                            {formErrors.spouse}
                                          </div>
                                        )}
                                      </div>
                                    </div>

                                    <div className="border-bottom pb-3 mb-3 mt-4">
                                      <div className="d-flex align-items-center justify-content-between">
                                        <div className="pe-2">
                                          <label className="col-form-label">
                                            <span>Your t-shirt size</span>
                                          </label>
                                          <div id="shirt_size-value">
                                            {formValuesAboout?.shirt_size}
                                          </div>
                                        </div>
                                        {localStorage.getItem("auth") &&
                                        (jwt(localStorage.getItem("auth"))
                                          .contactType == "admin" ||
                                          jwt(localStorage.getItem("auth"))
                                            .id == params.id) ? (
                                          <div
                                            data-bs-toggle="tooltip"
                                            title="Edit"
                                          >
                                            <a
                                              className="nav-link py-0"
                                              href="#shirt_size-collapse"
                                              data-bs-toggle="collapse"
                                            >
                                              <i className="fi-edit"></i>
                                            </a>
                                          </div>
                                        ) : (
                                          <>
                                            {localStorage.getItem("auth") &&
                                              jwt(localStorage.getItem("auth"))
                                                .contactType == "staff" &&
                                              (Array.isArray(formData) &&
                                              formData.length > 0
                                                ? formData.map((item) =>
                                                    item.roleStaff ===
                                                    "add_contact" ? (
                                                      <div
                                                        data-bs-toggle="tooltip"
                                                        title="Edit"
                                                      >
                                                        <a
                                                          className="nav-link py-0"
                                                          href="#shirt_size-collapse"
                                                          data-bs-toggle="collapse"
                                                        >
                                                          <i className="fi-edit"></i>
                                                        </a>
                                                      </div>
                                                    ) : (
                                                      ""
                                                    )
                                                  )
                                                : "")}
                                          </>
                                        )}
                                      </div>

                                      <div
                                        className="collapse"
                                        id="shirt_size-collapse"
                                        data-bs-parent="#personal-details"
                                      >
                                        <div className="form-check">
                                          <input
                                            className="form-check-input "
                                            type="radio"
                                            name="shirt_size"
                                            value="XS"
                                            onChange={handleRadioChange}
                                            checked={
                                              formValuesAboout?.shirt_size ===
                                              "XS"
                                            }
                                          />
                                          <label className="form-check-label">
                                            XS
                                          </label>
                                        </div>

                                        <div className="form-check">
                                          <input
                                            className="form-check-input"
                                            type="radio"
                                            name="shirt_size"
                                            value="S"
                                            checked={
                                              formValuesAboout?.shirt_size ===
                                              "S"
                                            }
                                            onChange={handleRadioChange}
                                          />
                                          <label className="form-check-label">
                                            S
                                          </label>
                                        </div>

                                        <div className="form-check">
                                          <input
                                            className="form-check-input"
                                            type="radio"
                                            name="shirt_size"
                                            value="M"
                                            checked={
                                              formValuesAboout?.shirt_size ===
                                              "M"
                                            }
                                            onChange={handleRadioChange}
                                          />
                                          <label className="form-check-label">
                                            M
                                          </label>
                                        </div>

                                        <div className="form-check">
                                          <input
                                            className="form-check-input "
                                            type="radio"
                                            name="shirt_size"
                                            value="L"
                                            checked={
                                              formValuesAboout?.shirt_size ===
                                              "L"
                                            }
                                            onChange={handleRadioChange}
                                          />
                                          <label className="form-check-label">
                                            L
                                          </label>
                                        </div>

                                        <div className="form-check">
                                          <input
                                            className="form-check-input"
                                            type="radio"
                                            name="shirt_size"
                                            value="XL"
                                            checked={
                                              formValuesAboout?.shirt_size ===
                                              "XL"
                                            }
                                            onChange={handleRadioChange}
                                          />
                                          <label className="form-check-label">
                                            XL
                                          </label>
                                        </div>

                                        <div className="form-check">
                                          <input
                                            className="form-check-input"
                                            type="radio"
                                            name="shirt_size"
                                            value="XXL"
                                            checked={
                                              formValuesAboout?.shirt_size ===
                                              "XXL"
                                            }
                                            onChange={handleRadioChange}
                                          />
                                          <label className="form-check-label">
                                            XXL
                                          </label>
                                        </div>
                                      </div>
                                    </div>

                                    <div className="border-bottom pb-3 mb-3 mt-4">
                                      <div className="d-flex align-items-center justify-content-between">
                                        <div className="pe-2">
                                          <label className="col-form-label">
                                            <span>
                                              What’s your favorite way to spend
                                              a day off?
                                            </span>
                                          </label>
                                          <div id="spend_day-value">
                                            {formValuesAboout?.spend_day}
                                          </div>
                                        </div>
                                        {localStorage.getItem("auth") &&
                                        (jwt(localStorage.getItem("auth"))
                                          .contactType == "admin" ||
                                          jwt(localStorage.getItem("auth"))
                                            .id == params.id) ? (
                                          <div
                                            data-bs-toggle="tooltip"
                                            title="Edit"
                                          >
                                            <a
                                              className="nav-link py-0"
                                              href="#spend_day-collapse"
                                              data-bs-toggle="collapse"
                                            >
                                              <i className="fi-edit"></i>
                                            </a>
                                          </div>
                                        ) : (
                                          <>
                                            {localStorage.getItem("auth") &&
                                              jwt(localStorage.getItem("auth"))
                                                .contactType == "staff" &&
                                              (Array.isArray(formData) &&
                                              formData.length > 0
                                                ? formData.map((item) =>
                                                    item.roleStaff ===
                                                    "add_contact" ? (
                                                      <div
                                                        data-bs-toggle="tooltip"
                                                        title="Edit"
                                                      >
                                                        <a
                                                          className="nav-link py-0"
                                                          href="#spend_day-collapse"
                                                          data-bs-toggle="collapse"
                                                        >
                                                          <i className="fi-edit"></i>
                                                        </a>
                                                      </div>
                                                    ) : (
                                                      ""
                                                    )
                                                  )
                                                : "")}
                                          </>
                                        )}
                                      </div>
                                      <div
                                        className="collapse"
                                        id="spend_day-collapse"
                                        data-bs-parent="#personal-details"
                                      >
                                        <input
                                          placeholder="Your answer"
                                          className="form-control mt-3"
                                          type="text"
                                          name="spend_day"
                                          value={formValuesAboout?.spend_day}
                                          onChange={handleChangesAbout}
                                          style={{
                                            border: formErrors?.spend_day
                                              ? "1px solid red"
                                              : "1px solid #00000026",
                                          }}
                                          data-bs-binded-element="#spend_day-value"
                                          data-bs-unset-value="Not specified"
                                        />
                                        {formErrors.spend_day && (
                                          <div className="invalid-tooltip">
                                            {formErrors.spend_day}
                                          </div>
                                        )}
                                      </div>
                                    </div>

                                    <div className="border-bottom pb-3 mb-3">
                                      <div className="d-flex align-items-center justify-content-between">
                                        <div className="pe-2">
                                          <label className="col-form-label">
                                            <span>
                                              What’s one professional skill
                                              you’re currently working on?
                                            </span>
                                          </label>
                                          <div id="professional_skill-value">
                                            {
                                              formValuesAboout?.professional_skill
                                            }
                                          </div>
                                        </div>
                                        {localStorage.getItem("auth") &&
                                        (jwt(localStorage.getItem("auth"))
                                          .contactType == "admin" ||
                                          jwt(localStorage.getItem("auth"))
                                            .id == params.id) ? (
                                          <div
                                            data-bs-toggle="tooltip"
                                            title="Edit"
                                          >
                                            <a
                                              className="nav-link py-0"
                                              href="#professional_skill-collapse"
                                              data-bs-toggle="collapse"
                                            >
                                              <i className="fi-edit"></i>
                                            </a>
                                          </div>
                                        ) : (
                                          <>
                                            {localStorage.getItem("auth") &&
                                              jwt(localStorage.getItem("auth"))
                                                .contactType == "staff" &&
                                              (Array.isArray(formData) &&
                                              formData.length > 0
                                                ? formData.map((item) =>
                                                    item.roleStaff ===
                                                    "add_contact" ? (
                                                      <div
                                                        data-bs-toggle="tooltip"
                                                        title="Edit"
                                                      >
                                                        <a
                                                          className="nav-link py-0"
                                                          href="#professional_skill-collapse"
                                                          data-bs-toggle="collapse"
                                                        >
                                                          <i className="fi-edit"></i>
                                                        </a>
                                                      </div>
                                                    ) : (
                                                      ""
                                                    )
                                                  )
                                                : "")}
                                          </>
                                        )}
                                      </div>
                                      <div
                                        className="collapse"
                                        id="professional_skill-collapse"
                                        data-bs-parent="#personal-details"
                                      >
                                        <input
                                          placeholder="Your answer"
                                          className="form-control mt-3"
                                          type="text"
                                          name="professional_skill"
                                          value={
                                            formValuesAboout?.professional_skill
                                          }
                                          onChange={handleChangesAbout}
                                          style={{
                                            border:
                                              formErrors?.professional_skill
                                                ? "1px solid red"
                                                : "1px solid #00000026",
                                          }}
                                          data-bs-binded-element="#professional_skill-value"
                                          data-bs-unset-value="Not specified"
                                        />
                                        {formErrors.professional_skill && (
                                          <div className="invalid-tooltip">
                                            {formErrors.professional_skill}
                                          </div>
                                        )}
                                      </div>
                                    </div>

                                    <div className="border-bottom pb-3 mb-3">
                                      <div className="d-flex align-items-center justify-content-between">
                                        <div className="pe-2">
                                          <label className="col-form-label">
                                            <span>
                                              What are your hobbies, and how did
                                              you get into them?
                                            </span>
                                          </label>
                                          <div id="hobbies-value">
                                            {formValuesAboout?.hobbies}
                                          </div>
                                        </div>
                                        {localStorage.getItem("auth") &&
                                        (jwt(localStorage.getItem("auth"))
                                          .contactType == "admin" ||
                                          jwt(localStorage.getItem("auth"))
                                            .id == params.id) ? (
                                          <div
                                            data-bs-toggle="tooltip"
                                            title="Edit"
                                          >
                                            <a
                                              className="nav-link py-0"
                                              href="#hobbies-collapse"
                                              data-bs-toggle="collapse"
                                            >
                                              <i className="fi-edit"></i>
                                            </a>
                                          </div>
                                        ) : (
                                          <>
                                            {localStorage.getItem("auth") &&
                                              jwt(localStorage.getItem("auth"))
                                                .contactType == "staff" &&
                                              (Array.isArray(formData) &&
                                              formData.length > 0
                                                ? formData.map((item) =>
                                                    item.roleStaff ===
                                                    "add_contact" ? (
                                                      <div
                                                        data-bs-toggle="tooltip"
                                                        title="Edit"
                                                      >
                                                        <a
                                                          className="nav-link py-0"
                                                          href="#hobbies-collapse"
                                                          data-bs-toggle="collapse"
                                                        >
                                                          <i className="fi-edit"></i>
                                                        </a>
                                                      </div>
                                                    ) : (
                                                      ""
                                                    )
                                                  )
                                                : "")}
                                          </>
                                        )}
                                      </div>
                                      <div
                                        className="collapse"
                                        id="hobbies-collapse"
                                        data-bs-parent="#personal-details"
                                      >
                                        <input
                                          placeholder="Your answer"
                                          className="form-control mt-3"
                                          type="text"
                                          name="hobbies"
                                          value={formValuesAboout?.hobbies}
                                          onChange={handleChangesAbout}
                                          style={{
                                            border: formErrors?.hobbies
                                              ? "1px solid red"
                                              : "1px solid #00000026",
                                          }}
                                          data-bs-binded-element="#hobbies-value"
                                          data-bs-unset-value="Not specified"
                                        />
                                        {formErrors.hobbies && (
                                          <div className="invalid-tooltip">
                                            {formErrors.hobbies}
                                          </div>
                                        )}
                                      </div>
                                    </div>

                                    <div className="border-bottom pb-3 mb-3">
                                      <div className="d-flex align-items-center justify-content-between">
                                        <div className="pe-2">
                                          <label className="col-form-label">
                                            <span>
                                              What was the best vacation you
                                              ever took and why?
                                            </span>
                                          </label>
                                          <div id="vacation-value">
                                            {formValuesAboout?.vacation}
                                          </div>
                                        </div>
                                        {localStorage.getItem("auth") &&
                                        (jwt(localStorage.getItem("auth"))
                                          .contactType == "admin" ||
                                          jwt(localStorage.getItem("auth"))
                                            .id == params.id) ? (
                                          <div
                                            data-bs-toggle="tooltip"
                                            title="Edit"
                                          >
                                            <a
                                              className="nav-link py-0"
                                              href="#vacation-collapse"
                                              data-bs-toggle="collapse"
                                            >
                                              <i className="fi-edit"></i>
                                            </a>
                                          </div>
                                        ) : (
                                          <>
                                            {localStorage.getItem("auth") &&
                                              jwt(localStorage.getItem("auth"))
                                                .contactType == "staff" &&
                                              (Array.isArray(formData) &&
                                              formData.length > 0
                                                ? formData.map((item) =>
                                                    item.roleStaff ===
                                                    "add_contact" ? (
                                                      <div
                                                        data-bs-toggle="tooltip"
                                                        title="Edit"
                                                      >
                                                        <a
                                                          className="nav-link py-0"
                                                          href="#vacation-collapse"
                                                          data-bs-toggle="collapse"
                                                        >
                                                          <i className="fi-edit"></i>
                                                        </a>
                                                      </div>
                                                    ) : (
                                                      ""
                                                    )
                                                  )
                                                : "")}
                                          </>
                                        )}
                                      </div>
                                      <div
                                        className="collapse"
                                        id="vacation-collapse"
                                        data-bs-parent="#personal-details"
                                      >
                                        <input
                                          placeholder="Your answer"
                                          className="form-control mt-3"
                                          type="text"
                                          name="vacation"
                                          value={formValuesAboout?.vacation}
                                          onChange={handleChangesAbout}
                                          style={{
                                            border: formErrors?.vacation
                                              ? "1px solid red"
                                              : "1px solid #00000026",
                                          }}
                                          data-bs-binded-element="#vacation-value"
                                          data-bs-unset-value="Not specified"
                                        />
                                        {formErrors.vacation && (
                                          <div className="invalid-tooltip">
                                            {formErrors.vacation}
                                          </div>
                                        )}
                                      </div>
                                    </div>

                                    <div className="border-bottom pb-3 mb-3">
                                      <div className="d-flex align-items-center justify-content-between">
                                        <div className="pe-2">
                                          <label className="col-form-label">
                                            <span>
                                              Where’s the next place on your
                                              travel bucket list and why?
                                            </span>
                                          </label>
                                          <div id="travel_bucket-value">
                                            {formValuesAboout?.travel_bucket}
                                          </div>
                                        </div>
                                        {localStorage.getItem("auth") &&
                                        (jwt(localStorage.getItem("auth"))
                                          .contactType == "admin" ||
                                          jwt(localStorage.getItem("auth"))
                                            .id == params.id) ? (
                                          <div
                                            data-bs-toggle="tooltip"
                                            title="Edit"
                                          >
                                            <a
                                              className="nav-link py-0"
                                              href="#travel_bucket-collapse"
                                              data-bs-toggle="collapse"
                                            >
                                              <i className="fi-edit"></i>
                                            </a>
                                          </div>
                                        ) : (
                                          <>
                                            {localStorage.getItem("auth") &&
                                              jwt(localStorage.getItem("auth"))
                                                .contactType == "staff" &&
                                              (Array.isArray(formData) &&
                                              formData.length > 0
                                                ? formData.map((item) =>
                                                    item.roleStaff ===
                                                    "add_contact" ? (
                                                      <div
                                                        data-bs-toggle="tooltip"
                                                        title="Edit"
                                                      >
                                                        <a
                                                          className="nav-link py-0"
                                                          href="#travel_bucket-collapse"
                                                          data-bs-toggle="collapse"
                                                        >
                                                          <i className="fi-edit"></i>
                                                        </a>
                                                      </div>
                                                    ) : (
                                                      ""
                                                    )
                                                  )
                                                : "")}
                                          </>
                                        )}
                                      </div>
                                      <div
                                        className="collapse"
                                        id="travel_bucket-collapse"
                                        data-bs-parent="#personal-details"
                                      >
                                        <input
                                          placeholder="Your answer"
                                          className="form-control mt-3"
                                          type="text"
                                          name="travel_bucket"
                                          value={
                                            formValuesAboout?.travel_bucket
                                          }
                                          onChange={handleChangesAbout}
                                          style={{
                                            border: formErrors?.travel_bucket
                                              ? "1px solid red"
                                              : "1px solid #00000026",
                                          }}
                                          data-bs-binded-element="#travel_bucket-value"
                                          data-bs-unset-value="Not specified"
                                        />
                                        {formErrors.travel_bucket && (
                                          <div className="invalid-tooltip">
                                            {formErrors.travel_bucket}
                                          </div>
                                        )}
                                      </div>
                                    </div>

                                    <div className="border-bottom pb-3 mb-3">
                                      <div className="d-flex align-items-center justify-content-between">
                                        <div className="pe-2">
                                          <label className="col-form-label">
                                            <span>
                                              What's the phone app or program
                                              you use most?
                                            </span>
                                          </label>
                                          <div id="phone_app-value">
                                            {formValuesAboout?.phone_app}
                                          </div>
                                        </div>
                                        {localStorage.getItem("auth") &&
                                        (jwt(localStorage.getItem("auth"))
                                          .contactType == "admin" ||
                                          jwt(localStorage.getItem("auth"))
                                            .id == params.id) ? (
                                          <div
                                            data-bs-toggle="tooltip"
                                            title="Edit"
                                          >
                                            <a
                                              className="nav-link py-0"
                                              href="#phone_app-collapse"
                                              data-bs-toggle="collapse"
                                            >
                                              <i className="fi-edit"></i>
                                            </a>
                                          </div>
                                        ) : (
                                          <>
                                            {localStorage.getItem("auth") &&
                                              jwt(localStorage.getItem("auth"))
                                                .contactType == "staff" &&
                                              (Array.isArray(formData) &&
                                              formData.length > 0
                                                ? formData.map((item) =>
                                                    item.roleStaff ===
                                                    "add_contact" ? (
                                                      <div
                                                        data-bs-toggle="tooltip"
                                                        title="Edit"
                                                      >
                                                        <a
                                                          className="nav-link py-0"
                                                          href="#phone_app-collapse"
                                                          data-bs-toggle="collapse"
                                                        >
                                                          <i className="fi-edit"></i>
                                                        </a>
                                                      </div>
                                                    ) : (
                                                      ""
                                                    )
                                                  )
                                                : "")}
                                          </>
                                        )}
                                      </div>
                                      <div
                                        className="collapse"
                                        id="phone_app-collapse"
                                        data-bs-parent="#personal-details"
                                      >
                                        <input
                                          placeholder="Your answer"
                                          className="form-control mt-3"
                                          type="text"
                                          name="phone_app"
                                          value={formValuesAboout?.phone_app}
                                          onChange={handleChangesAbout}
                                          style={{
                                            border: formErrors?.phone_app
                                              ? "1px solid red"
                                              : "1px solid #00000026",
                                          }}
                                          data-bs-binded-element="#phone_app-value"
                                          data-bs-unset-value="Not specified"
                                        />
                                        {formErrors.phone_app && (
                                          <div className="invalid-tooltip">
                                            {formErrors.phone_app}
                                          </div>
                                        )}
                                      </div>
                                    </div>

                                    <div className="border-bottom pb-3 mb-3">
                                      <div className="d-flex align-items-center justify-content-between">
                                        <div className="pe-2">
                                          <label className="col-form-label">
                                            <span>
                                              Favorite book and or podcast?
                                            </span>
                                          </label>
                                          <div id="book-value">
                                            {formValuesAboout?.book}
                                          </div>
                                        </div>
                                        {localStorage.getItem("auth") &&
                                        (jwt(localStorage.getItem("auth"))
                                          .contactType == "admin" ||
                                          jwt(localStorage.getItem("auth"))
                                            .id == params.id) ? (
                                          <div
                                            data-bs-toggle="tooltip"
                                            title="Edit"
                                          >
                                            <a
                                              className="nav-link py-0"
                                              href="#book-collapse"
                                              data-bs-toggle="collapse"
                                            >
                                              <i className="fi-edit"></i>
                                            </a>
                                          </div>
                                        ) : (
                                          <>
                                            {localStorage.getItem("auth") &&
                                              jwt(localStorage.getItem("auth"))
                                                .contactType == "staff" &&
                                              (Array.isArray(formData) &&
                                              formData.length > 0
                                                ? formData.map((item) =>
                                                    item.roleStaff ===
                                                    "add_contact" ? (
                                                      <div
                                                        data-bs-toggle="tooltip"
                                                        title="Edit"
                                                      >
                                                        <a
                                                          className="nav-link py-0"
                                                          href="#book-collapse"
                                                          data-bs-toggle="collapse"
                                                        >
                                                          <i className="fi-edit"></i>
                                                        </a>
                                                      </div>
                                                    ) : (
                                                      ""
                                                    )
                                                  )
                                                : "")}
                                          </>
                                        )}
                                      </div>
                                      <div
                                        className="collapse"
                                        id="book-collapse"
                                        data-bs-parent="#personal-details"
                                      >
                                        <input
                                          placeholder="Your answer"
                                          className="form-control mt-3"
                                          type="text"
                                          name="book"
                                          value={formValuesAboout?.book}
                                          onChange={handleChangesAbout}
                                          style={{
                                            border: formErrors?.book
                                              ? "1px solid red"
                                              : "1px solid #00000026",
                                          }}
                                          data-bs-binded-element="#book-value"
                                          data-bs-unset-value="Not specified"
                                        />
                                        {formErrors.book && (
                                          <div className="invalid-tooltip">
                                            {formErrors.book}
                                          </div>
                                        )}
                                      </div>
                                    </div>

                                    <div className="border-bottom pb-3 mb-3">
                                      <div className="d-flex align-items-center justify-content-between">
                                        <div className="pe-2">
                                          <label className="col-form-label">
                                            <span>
                                              What type of music are you into?
                                            </span>
                                          </label>
                                          <div id="music-value">
                                            {formValuesAboout?.music}
                                          </div>
                                        </div>

                                        {localStorage.getItem("auth") &&
                                        (jwt(localStorage.getItem("auth"))
                                          .contactType == "admin" ||
                                          jwt(localStorage.getItem("auth"))
                                            .id == params.id) ? (
                                          <div
                                            data-bs-toggle="tooltip"
                                            title="Edit"
                                          >
                                            <a
                                              className="nav-link py-0"
                                              href="#music-collapse"
                                              data-bs-toggle="collapse"
                                            >
                                              <i className="fi-edit"></i>
                                            </a>
                                          </div>
                                        ) : (
                                          <>
                                            {localStorage.getItem("auth") &&
                                              jwt(localStorage.getItem("auth"))
                                                .contactType == "staff" &&
                                              (Array.isArray(formData) &&
                                              formData.length > 0
                                                ? formData.map((item) =>
                                                    item.roleStaff ===
                                                    "add_contact" ? (
                                                      <div
                                                        data-bs-toggle="tooltip"
                                                        title="Edit"
                                                      >
                                                        <a
                                                          className="nav-link py-0"
                                                          href="#music-collapse"
                                                          data-bs-toggle="collapse"
                                                        >
                                                          <i className="fi-edit"></i>
                                                        </a>
                                                      </div>
                                                    ) : (
                                                      ""
                                                    )
                                                  )
                                                : "")}
                                          </>
                                        )}
                                      </div>
                                      <div
                                        className="collapse"
                                        id="music-collapse"
                                        data-bs-parent="#personal-details"
                                      >
                                        <input
                                          placeholder="Your answer"
                                          className="form-control mt-3"
                                          type="text"
                                          name="music"
                                          value={formValuesAboout?.music}
                                          onChange={handleChangesAbout}
                                          style={{
                                            border: formErrors?.music
                                              ? "1px solid red"
                                              : "1px solid #00000026",
                                          }}
                                          data-bs-binded-element="#music-value"
                                          data-bs-unset-value="Not specified"
                                        />
                                        {formErrors.music && (
                                          <div className="invalid-tooltip">
                                            {formErrors.music}
                                          </div>
                                        )}
                                      </div>
                                    </div>

                                    <div className="border-bottom pb-3 mb-3">
                                      <div className="d-flex align-items-center justify-content-between">
                                        <div className="pe-2">
                                          <label className="col-form-label">
                                            <span>Favorite Restaurant?</span>
                                          </label>
                                          <div id="restaurant-value">
                                            {formValuesAboout?.restaurant}
                                          </div>
                                        </div>
                                        {localStorage.getItem("auth") &&
                                        (jwt(localStorage.getItem("auth"))
                                          .contactType == "admin" ||
                                          jwt(localStorage.getItem("auth"))
                                            .id == params.id) ? (
                                          <div
                                            data-bs-toggle="tooltip"
                                            title="Edit"
                                          >
                                            <a
                                              className="nav-link py-0"
                                              href="#restaurant-collapse"
                                              data-bs-toggle="collapse"
                                            >
                                              <i className="fi-edit"></i>
                                            </a>
                                          </div>
                                        ) : (
                                          <>
                                            {localStorage.getItem("auth") &&
                                              jwt(localStorage.getItem("auth"))
                                                .contactType == "staff" &&
                                              (Array.isArray(formData) &&
                                              formData.length > 0
                                                ? formData.map((item) =>
                                                    item.roleStaff ===
                                                    "add_contact" ? (
                                                      <div
                                                        data-bs-toggle="tooltip"
                                                        title="Edit"
                                                      >
                                                        <a
                                                          className="nav-link py-0"
                                                          href="#restaurant-collapse"
                                                          data-bs-toggle="collapse"
                                                        >
                                                          <i className="fi-edit"></i>
                                                        </a>
                                                      </div>
                                                    ) : (
                                                      ""
                                                    )
                                                  )
                                                : "")}
                                          </>
                                        )}
                                      </div>
                                      <div
                                        className="collapse"
                                        id="restaurant-collapse"
                                        data-bs-parent="#personal-details"
                                      >
                                        <input
                                          placeholder="Your answer"
                                          className="form-control mt-3"
                                          type="text"
                                          name="restaurant"
                                          value={formValuesAboout?.restaurant}
                                          onChange={handleChangesAbout}
                                          style={{
                                            border: formErrors?.restaurant
                                              ? "1px solid red"
                                              : "1px solid #00000026",
                                          }}
                                          data-bs-binded-element="#restaurant-value"
                                          data-bs-unset-value="Not specified"
                                        />
                                        {formErrors.restaurant && (
                                          <div className="invalid-tooltip">
                                            {formErrors.restaurant}
                                          </div>
                                        )}
                                      </div>
                                    </div>

                                    <div className="border-bottom pb-3 mb-3">
                                      <div className="d-flex align-items-center justify-content-between">
                                        <div className="pe-2">
                                          <label className="col-form-label">
                                            <span>Favorite Food?</span>
                                          </label>
                                          <div id="food-value">
                                            {formValuesAboout?.food}
                                          </div>
                                        </div>
                                        {localStorage.getItem("auth") &&
                                        (jwt(localStorage.getItem("auth"))
                                          .contactType == "admin" ||
                                          jwt(localStorage.getItem("auth"))
                                            .id == params.id) ? (
                                          <div
                                            data-bs-toggle="tooltip"
                                            title="Edit"
                                          >
                                            <a
                                              className="nav-link py-0"
                                              href="#food-collapse"
                                              data-bs-toggle="collapse"
                                            >
                                              <i className="fi-edit"></i>
                                            </a>
                                          </div>
                                        ) : (
                                          <>
                                            {localStorage.getItem("auth") &&
                                              jwt(localStorage.getItem("auth"))
                                                .contactType == "staff" &&
                                              (Array.isArray(formData) &&
                                              formData.length > 0
                                                ? formData.map((item) =>
                                                    item.roleStaff ===
                                                    "add_contact" ? (
                                                      <div
                                                        data-bs-toggle="tooltip"
                                                        title="Edit"
                                                      >
                                                        <a
                                                          className="nav-link py-0"
                                                          href="#food-collapse"
                                                          data-bs-toggle="collapse"
                                                        >
                                                          <i className="fi-edit"></i>
                                                        </a>
                                                      </div>
                                                    ) : (
                                                      ""
                                                    )
                                                  )
                                                : "")}
                                          </>
                                        )}
                                      </div>
                                      <div
                                        className="collapse"
                                        id="food-collapse"
                                        data-bs-parent="#personal-details"
                                      >
                                        <input
                                          placeholder="Your answer"
                                          className="form-control mt-3"
                                          type="text"
                                          name="food"
                                          value={formValuesAboout?.food}
                                          onChange={handleChangesAbout}
                                          style={{
                                            border: formErrors?.food
                                              ? "1px solid red"
                                              : "1px solid #00000026",
                                          }}
                                          data-bs-binded-element="#food-value"
                                          data-bs-unset-value="Not specified"
                                        />
                                        {formErrors.food && (
                                          <div className="invalid-tooltip">
                                            {formErrors.food}
                                          </div>
                                        )}
                                      </div>
                                    </div>

                                    <div className="border-bottom pb-3 mb-3">
                                      <div className="d-flex align-items-center justify-content-between">
                                        <div className="pe-2">
                                          <label className="col-form-label">
                                            <span>
                                              Favorite Dessert/Candy/Drink?
                                            </span>
                                          </label>
                                          <div id="dessert-value">
                                            {formValuesAboout?.dessert}
                                          </div>
                                        </div>
                                        {localStorage.getItem("auth") &&
                                        (jwt(localStorage.getItem("auth"))
                                          .contactType == "admin" ||
                                          jwt(localStorage.getItem("auth"))
                                            .id == params.id) ? (
                                          <div
                                            data-bs-toggle="tooltip"
                                            title="Edit"
                                          >
                                            <a
                                              className="nav-link py-0"
                                              href="#dessert-collapse"
                                              data-bs-toggle="collapse"
                                            >
                                              <i className="fi-edit"></i>
                                            </a>
                                          </div>
                                        ) : (
                                          <>
                                            {localStorage.getItem("auth") &&
                                              jwt(localStorage.getItem("auth"))
                                                .contactType == "staff" &&
                                              (Array.isArray(formData) &&
                                              formData.length > 0
                                                ? formData.map((item) =>
                                                    item.roleStaff ===
                                                    "add_contact" ? (
                                                      <div
                                                        data-bs-toggle="tooltip"
                                                        title="Edit"
                                                      >
                                                        <a
                                                          className="nav-link py-0"
                                                          href="#dessert-collapse"
                                                          data-bs-toggle="collapse"
                                                        >
                                                          <i className="fi-edit"></i>
                                                        </a>
                                                      </div>
                                                    ) : (
                                                      ""
                                                    )
                                                  )
                                                : "")}
                                          </>
                                        )}
                                      </div>
                                      <div
                                        className="collapse"
                                        id="dessert-collapse"
                                        data-bs-parent="#personal-details"
                                      >
                                        <input
                                          placeholder="Your answer"
                                          className="form-control mt-3"
                                          type="text"
                                          name="dessert"
                                          value={formValuesAboout?.dessert}
                                          onChange={handleChangesAbout}
                                          style={{
                                            border: formErrors?.dessert
                                              ? "1px solid red"
                                              : "1px solid #00000026",
                                          }}
                                          data-bs-binded-element="#dessert-value"
                                          data-bs-unset-value="Not specified"
                                        />
                                        {formErrors.dessert && (
                                          <div className="invalid-tooltip">
                                            {formErrors.dessert}
                                          </div>
                                        )}
                                      </div>
                                    </div>

                                    <div className="border-bottom pb-3 mb-3">
                                      <div className="d-flex align-items-center justify-content-between">
                                        <div className="pe-2">
                                          <label className="col-form-label">
                                            <span>
                                              Anything else you'd like to share?
                                            </span>
                                          </label>
                                          <div id="anything_else-value">
                                            {formValuesAboout?.anything_else}
                                          </div>
                                        </div>
                                        {localStorage.getItem("auth") &&
                                        (jwt(localStorage.getItem("auth"))
                                          .contactType == "admin" ||
                                          jwt(localStorage.getItem("auth"))
                                            .id == params.id) ? (
                                          <div
                                            data-bs-toggle="tooltip"
                                            title="Edit"
                                          >
                                            <a
                                              className="nav-link py-0"
                                              href="#anything_else-collapse"
                                              data-bs-toggle="collapse"
                                            >
                                              <i className="fi-edit"></i>
                                            </a>
                                          </div>
                                        ) : (
                                          <>
                                            {localStorage.getItem("auth") &&
                                              jwt(localStorage.getItem("auth"))
                                                .contactType == "staff" &&
                                              (Array.isArray(formData) &&
                                              formData.length > 0
                                                ? formData.map((item) =>
                                                    item.roleStaff ===
                                                    "add_contact" ? (
                                                      <div
                                                        data-bs-toggle="tooltip"
                                                        title="Edit"
                                                      >
                                                        <a
                                                          className="nav-link py-0"
                                                          href="#anything_else-collapse"
                                                          data-bs-toggle="collapse"
                                                        >
                                                          <i className="fi-edit"></i>
                                                        </a>
                                                      </div>
                                                    ) : (
                                                      ""
                                                    )
                                                  )
                                                : "")}
                                          </>
                                        )}
                                      </div>
                                      <div
                                        className="collapse"
                                        id="anything_else-collapse"
                                        data-bs-parent="#personal-details"
                                      >
                                        <input
                                          placeholder="Your answer"
                                          className="form-control mt-3"
                                          type="text"
                                          name="anything_else"
                                          value={
                                            formValuesAboout?.anything_else
                                          }
                                          onChange={handleChangesAbout}
                                          style={{
                                            border: formErrors?.anything_else
                                              ? "1px solid red"
                                              : "1px solid #00000026",
                                          }}
                                          data-bs-binded-element="#anything_else-value"
                                          data-bs-unset-value="Not specified"
                                        />
                                        {formErrors.anything_else && (
                                          <div className="invalid-tooltip">
                                            {formErrors.anything_else}
                                          </div>
                                        )}
                                      </div>
                                    </div>

                                    {localStorage.getItem("auth") &&
                                    (jwt(localStorage.getItem("auth"))
                                      .contactType == "admin" ||
                                      jwt(localStorage.getItem("auth")).id ===
                                        params.id) ? (
                                      <div className="d-flex align-items-center justify-content-between mt-5 pull-right">
                                        <button
                                          className="btn btn-primary  px-3 px-sm-4"
                                          type="button"
                                          onClick={handleSubmitAbout}
                                        >
                                          Submit
                                        </button>
                                      </div>
                                    ) : (
                                      <>
                                        {localStorage.getItem("auth") &&
                                          jwt(localStorage.getItem("auth"))
                                            .contactType == "staff" &&
                                          (Array.isArray(formData) &&
                                          formData.length > 0
                                            ? formData.map((item) =>
                                                item.roleStaff ===
                                                "add_contact" ? (
                                                  <div className="d-flex align-items-center justify-content-between mt-5 pull-right">
                                                    <button
                                                      className="btn btn-primary  px-3 px-sm-4"
                                                      type="button"
                                                      onClick={
                                                        handleSubmitAbout
                                                      }
                                                    >
                                                      Submit
                                                    </button>
                                                  </div>
                                                ) : (
                                                  ""
                                                )
                                              )
                                            : "")}
                                      </>
                                    )}
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </main>
                    </div>
                  ) : // ) : step1 == "6"?(
                  //   <div className="float-left w-100">
                  //   <BusinessCard/>
                  //   </div>
                  step1 == "7" ? (
                    <div className="float-left w-100">
                      <ActivateAccount />
                    </div>
                  ) : step1 == "8" ? (
                    <div className="float-left w-100">
                      <ContactInfo />
                    </div>
                  ) : step1 == "9" ? (
                    <div className="float-left w-100">
                      <ProfileInfo />
                    </div>
                  ) : step1 == "10" ? (
                    <div className="float-left w-100">
                      <Loader isActive={isActive} />
                      {isShow && (
                        <Toaster
                          types={types}
                          isShow={isShow}
                          toasterBody={toasterBody}
                          message={message}
                        />
                      )}
                      <main className="page-wrapper profile_page_wrap mb-3">
                        <div className="content-overlay">
                          <div className="border bg-light rounded-3 p-3">
                            <div className="float-left mb-3">
                              <p className="w-100">
                                This feature empowers administrators to manage
                                user access efficiently. You can click on the
                                buttom below to disable a agent's platform
                                access when necessary and can reverse it too.
                              </p>
                              <p className="w-100">
                                <b>Current Status:</b>{" "}
                                {getContact.contact_status
                                  ? getContact.contact_status === "active"
                                    ? "Active"
                                    : "Terminate Agent"
                                  : "Active"}
                              </p>
                              {getContact.contact_status === "deleted" ? (
                                <p className="">
                                  <b>Reason:</b> {getContact.reason}
                                </p>
                              ) : (
                                ""
                              )}

                              {localStorage.getItem("auth") &&
                              jwt(localStorage.getItem("auth")).contactType ==
                                "admin" ? (
                                <>
                                  {getContact.contact_status === "active" ? (
                                    <>
                                      <span>
                                        <b>Name:</b> {getContact.firstName}{" "}
                                        {getContact.lastName}
                                      </span>
                                      <br />
                                      {getContact.active_office ? (
                                        <span>
                                          <b>Office:</b>{" "}
                                          {getContact.active_office}
                                        </span>
                                      ) : (
                                        ""
                                      )}

                                      <div className="mt-3">
                                        <label className="form-label">
                                          Reason
                                        </label>
                                        <textarea
                                          className="form-control float-left w-100 mb-3"
                                          id="textarea-input"
                                          rows="5"
                                          name="reason"
                                          onChange={handleChange}
                                          autoComplete="on"
                                          value={getContact.reason}
                                        ></textarea>
                                      </div>
                                    </>
                                  ) : (
                                    ""
                                  )}

                                  <button
                                    className="btn btn-primary px-3 px-sm-4"
                                    onClick={handleDeleteContact}
                                  >
                                    {getContact.contact_status === "deleted"
                                      ? "Activate"
                                      : "Terminate Agent"}
                                  </button>
                                </>
                              ) : (
                                <>
                                  {localStorage.getItem("auth") &&
                                    jwt(localStorage.getItem("auth"))
                                      .contactType == "staff" &&
                                    (Array.isArray(formData) &&
                                    formData.length > 0
                                      ? formData.map((item) =>
                                          item.roleStaff === "add_contact" ? (
                                            <>
                                              {getContact.contact_status ===
                                              "active" ? (
                                                <>
                                                  <span>
                                                    <b>Name:</b>{" "}
                                                    {getContact.firstName}{" "}
                                                    {getContact.lastName}
                                                  </span>
                                                  <br />
                                                  {getContact.active_office ? (
                                                    <span>
                                                      <b>Office:</b>{" "}
                                                      {getContact.active_office}
                                                    </span>
                                                  ) : (
                                                    ""
                                                  )}

                                                  <div className="mt-3">
                                                    <label className="form-label">
                                                      Reason
                                                    </label>
                                                    <textarea
                                                      className="form-control float-left w-100 mb-3"
                                                      id="textarea-input"
                                                      rows="5"
                                                      name="reason"
                                                      onChange={handleChange}
                                                      autoComplete="on"
                                                      value={getContact.reason}
                                                    ></textarea>
                                                  </div>
                                                </>
                                              ) : (
                                                ""
                                              )}

                                              <button
                                                className="btn btn-primary px-3 px-sm-4"
                                                onClick={handleDeleteContact}
                                              >
                                                {getContact.contact_status ===
                                                "deleted"
                                                  ? "Activate"
                                                  : "Terminate Agent"}
                                              </button>
                                            </>
                                          ) : (
                                            ""
                                          )
                                        )
                                      : "")}
                                </>
                              )}
                            </div>

                            <div className="float-left mb-3">
                              <p className="w-100">
                                This feature empowers administrators to manage
                                user onboarding process. You can click on the
                                buttom below to resend the onboarding link to
                                client's Email.
                              </p>
                              <p className="w-100">
                                <b>Current Status:</b>{" "}
                                {getContact.onboardfinal
                                  ? getContact.onboardfinal === "done" &&
                                    getContact.onboard_approve === "yes"
                                    ? "On Boarded"
                                    : getContact.onboardfinal === "done" &&
                                      getContact.onboard_approve === "no"
                                    ? "Pending for approval"
                                    : "Onboarding Pending"
                                  : "Onboarding Pending"}
                              </p>

                              {localStorage.getItem("auth") &&
                              jwt(localStorage.getItem("auth")).contactType ==
                                "admin" ? (
                                <>
                                  {(getContact.onboardfinal != "done" ||
                                    !getContact.onboard_approve) && (
                                    <button
                                      className="btn btn-primary px-3 px-sm-4"
                                      onClick={handlesetuppassword}
                                    >
                                      Send Onboarding Request
                                    </button>
                                  )}
                                </>
                              ) : (
                                <>
                                  {localStorage.getItem("auth") &&
                                    jwt(localStorage.getItem("auth"))
                                      .contactType == "staff" &&
                                    (Array.isArray(formData) &&
                                    formData.length > 0
                                      ? formData.map((item) =>
                                          item.roleStaff === "add_contact" ? (
                                            <>
                                              {(getContact.onboardfinal !=
                                                "done" ||
                                                !getContact.onboard_approve) && (
                                                <button
                                                  className="btn btn-primary px-3 px-sm-4"
                                                  onClick={handlesetuppassword}
                                                >
                                                  Send Onboarding Request
                                                </button>
                                              )}
                                            </>
                                          ) : (
                                            ""
                                          )
                                        )
                                      : "")}
                                </>
                              )}
                            </div>

                            <div className="float-left mb-3">
                              <p className="w-100">
                                This feature empowers administrators to manage
                                user as a legacy account.
                              </p>
                              <p className="w-100">
                                <b>Legacy Status:</b>{" "}
                                {getContact.legacy_account &&
                                getContact.legacy_account === "yes"
                                  ? "Legacy Account"
                                  : "Non Legacy Account"}
                              </p>

                              {localStorage.getItem("auth") &&
                              jwt(localStorage.getItem("auth")).contactType ==
                                "admin" ? (
                                <>
                                  {getContact.contactType === "associate" && (
                                    <button
                                      className="btn btn-primary px-3 px-sm-4"
                                      onClick={handlesetlegacy}
                                    >
                                      Change Legacy Status
                                    </button>
                                  )}
                                </>
                              ) : (
                                <>
                                  {localStorage.getItem("auth") &&
                                    jwt(localStorage.getItem("auth"))
                                      .contactType == "staff" &&
                                    (Array.isArray(formData) &&
                                    formData.length > 0
                                      ? formData.map((item) =>
                                          item.roleStaff === "add_contact" ? (
                                            <>
                                              {getContact.contactType ===
                                                "associate" && (
                                                <button
                                                  className="btn btn-primary px-3 px-sm-4"
                                                  onClick={handlesetlegacy}
                                                >
                                                  Change Legacy Status
                                                </button>
                                              )}
                                            </>
                                          ) : (
                                            ""
                                          )
                                        )
                                      : "")}
                                </>
                              )}
                            </div>
                          </div>
                        </div>
                      </main>
                    </div>
                  ) : step1 == "12" ? (
                    <div className="float-left w-100">
                      <Loader isActive={isActive} />
                      {isShow && (
                        <Toaster
                          types={types}
                          isShow={isShow}
                          toasterBody={toasterBody}
                          message={message}
                        />
                      )}
                      <main className="page-wrapper profile_page_wrap">
                        <div className="">
                          <div className="border bg-light rounded-3 p-3">
                            <div className="promoted_product float-left w-100">
                              <div className="float-left mb-3 w-100">
                                <h6>Testimonials</h6>
                              </div>
                              <div className="row">
                                {localStorage.getItem("auth") &&
                                (jwt(localStorage.getItem("auth"))
                                  .contactType === "admin" ||
                                  jwt(localStorage.getItem("auth")).id) ? (
                                  <>
                                    {contactTestimonials &&
                                    contactTestimonials.length > 0 ? (
                                      contactTestimonials.map((post, index) => (
                                        <div
                                          key={post.id}
                                          className="col-lg-4  col-md-4 col-sm-6 col-12 mb-4"
                                        >
                                          <div className="card shadow-sm cursor-pointer card-hover border-0 h-100">
                                            <div className="card-img-top card-img-hover">
                                              <img
                                                className="img-fluid rounded-0 w-100"
                                                src={
                                                  post.image &&
                                                  post.image !== "image"
                                                    ? post.image
                                                    : defaultpropertyimage
                                                }
                                                alt="Testimonial"
                                              />
                                            </div>
                                            <div className="card-body position-relative p-2 pb-0">
                                              <p className="mb-1">
                                                {Array(parseInt(post.star))
                                                  .fill()
                                                  .map((_, index) => (
                                                    <i
                                                      key={index}
                                                      style={{
                                                        color: "#FFBC0B",
                                                        fontSize: "20px",
                                                        margin: "2px",
                                                      }}
                                                      className="fa fa-star"
                                                      aria-hidden="true"
                                                    ></i>
                                                  ))}
                                              </p>

                                              {post.displayName ? (
                                                <p className="mb-2">
                                                  <b>Name:</b>{" "}
                                                  {post.displayName}
                                                </p>
                                              ) : (
                                                ""
                                              )}
                                              {post.headlineReview ? (
                                                <p className="mb-2">
                                                  {/* <b>Headline Review:</b>{" "} */}
                                                  <b>{post.headlineReview}</b>
                                                </p>
                                              ) : (
                                                ""
                                              )}
                                              {post.description ? (
                                                <p className="review_description mb-2">
                                                  <b>Description:</b>{" "}
                                                  {post.description}
                                                </p>
                                              ) : (
                                                ""
                                              )}

                                              <button
                                                className="btn btn-primary"
                                                data-toggle="modal"
                                                data-target="#testimonialUser"
                                                onClick={() =>
                                                  handleTestimonialAgent(
                                                    post._id
                                                  )
                                                }
                                              >
                                                Read View
                                              </button>
                                            </div>
                                            <hr className="w-100 mt-2 mb-0" />
                                          </div>
                                        </div>
                                      ))
                                    ) : (
                                      <p>No testimonials available</p>
                                    )}
                                  </>
                                ) : (
                                  ""
                                )}
                              </div>
                            </div>
                          </div>
                        </div>
                      </main>
                    </div>
                  ) : (
                    ""
                  )}
                </div>
              </div>
            </div>
          </div>

          <div
            className="modal in"
            role="dialog"
            id="ModalAddpaperworkdocument"
          >
            <div
              className="modal-dialog modal-md modal-dialog-scrollable"
              role="document"
            >
              <div className="modal-content">
                <div className="modal-header border">
                  <h4 className="modal-title" id="myModalLabel">
                    Upload Paperwork Document
                  </h4>
                  <button
                    className="btn-close"
                    id="closeModalpaperwork"
                    type="button"
                    data-dismiss="modal"
                    aria-label="Close"
                  ></button>
                </div>

                <div className="modal-body fs-sm">
                  <div className="col-md-12 mt-12">
                    <form>
                      <h6>Upload Paperwork</h6>
                      <div className="mb-3">
                        <label className="form-label">Select Document</label>

                        <select
                          name="paperwork_id"
                          className="form-select"
                          value={selectedPaperworkId} // Set the value of the select to the current state value
                          onChange={handlePaperworkIdChange} // Handle the change event
                        >
                          <option>Select Document </option>
                          {agentpapernameAdd?.data &&
                          agentpapernameAdd.data.length > 0
                            ? agentpapernameAdd.data.map((item) => (
                                <option key={item._id} value={item._id}>
                                  {item.paperwork_title}
                                </option>
                              ))
                            : null}
                        </select>
                        {/* <div className="invalid-tooltip">{formErrors.paperwork_id}</div> */}
                      </div>

                      <div>
                        <div className="form-check ps-0">
                          <label className="form-label">
                            Select File (Must be PDF format.)
                          </label>

                          <input
                            className="form-control"
                            id="inline-form-input"
                            type="file"
                            accept={acceptedFileTypes
                              .map((type) => `.${type}`)
                              .join(",")}
                            name="file"
                            onChange={onFileChange}
                          />

                          {/* <input
                            id="paperwork_document"
                            type="file"
                            accept="pdf"
                            name="file"
                            onChange={onFileChange}
                            value=""
                          /> */}
                        </div>
                      </div>
                    </form>
                  </div>
                  <div className="modal-footer px-0 mt-3">
                    <button
                      type="button"
                      className="btn btn-primary btn-sm m-0"
                      id="save-button"
                      disabled={isActive}
                      onClick={handleFileUploadpaperbook}
                    >
                      {isActive ? "Please Wait..." : "Upload File Now"}
                    </button>
                    <button
                      type="button"
                      className="btn btn btn-secondary btn-sm m-0 ms-3"
                      id="closeModalpaperwork"
                      data-dismiss="modal"
                    >
                      Cancel & Close
                    </button>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div
            className="modal in"
            role="dialog"
            id="ModalAdditionalpaperworkdocument"
          >
            <div
              className="modal-dialog modal-md modal-dialog-scrollable"
              role="document"
            >
              <div className="modal-content">
                <div className="modal-header border">
                  <h4 className="modal-title" id="myModalLabel">
                    Upload Paperwork Document
                  </h4>
                  <button
                    className="btn-close"
                    id="closeAdditionalpaperwork"
                    type="button"
                    data-dismiss="modal"
                    aria-label="Close"
                  ></button>
                </div>

                <div className="modal-body fs-sm">
                  <div className="col-md-12 mt-12">
                    <form>
                      <h6>Upload Paperwork</h6>
                      <div className="mb-3">
                        <label className="form-label">
                          Additional Documents
                        </label>
                        <input
                          className="form-control"
                          id="inline-form-input"
                          type="text"
                          name="documentName"
                          value={paperworkData.documentName}
                          onChange={handleChangePaperwork}
                        />
                        {/* <select
                          name="paperwork_id"
                          className="form-select"
                          value={selectedPaperworkId} // Set the value of the select to the current state value
                          onChange={handlePaperworkIdChange} // Handle the change event
                        >
                          <option>Select Document </option>
                          {agentpapernameAdd?.data &&
                          agentpapernameAdd.data.length > 0
                            ? agentpapernameAdd.data.map((item) => (
                                <option key={item._id} value={item._id}>
                                  {item.paperwork_title}
                                </option>
                              ))
                            : null}
                        </select> */}
                        {/* <div className="invalid-tooltip">{formErrors.paperwork_id}</div> */}
                      </div>

                      <div>
                        <div className="form-check ps-0">
                          <label className="form-label">
                            Select File (Must be PDF format.)
                          </label>

                          <input
                            className="form-control"
                            id="inline-form-input"
                            type="file"
                            accept={acceptedFileTypes
                              .map((type) => `.${type}`)
                              .join(",")}
                            name="file"
                            onChange={onFileChangePaperwork}
                          />

                          {/* <input
                            id="paperwork_document"
                            type="file"
                            accept="pdf"
                            name="file"
                            onChange={onFileChangePaperwork}
                            value=""
                          /> */}
                        </div>
                      </div>
                    </form>
                  </div>
                  <div className="modal-footer px-0 mt-3">
                    <button
                      type="button"
                      className="btn btn-primary btn-sm m-0"
                      id="save-button"
                      disabled={isActive}
                      onClick={handleFilePaperwork}
                    >
                      {isActive ? "Please Wait..." : "Upload File Now"}
                    </button>
                    <button
                      type="button"
                      className="btn btn btn-secondary btn-sm m-0 ms-3"
                      id="closeAdditionalpaperwork"
                      data-dismiss="modal"
                    >
                      Cancel & Close
                    </button>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div className="modal IN" role="dialog" id="testimonialUser">
            <div
              className="modal-dialog modal-md modal-dialog-scrollable"
              role="document"
            >
              <div className="modal-content">
                <div className="modal-header">
                  <h4 className="modal-title" id="myModalLabel">
                    Testimonials
                  </h4>
                  <button
                    className="btn-close"
                    id="closeModalAttach"
                    type="button"
                    data-dismiss="modal"
                    aria-label="Close"
                  ></button>
                </div>

                <div className="modal-body fs-sm">
                  <div className="col-md-12">
                    <img
                      src={
                        contactTestimonialsUser.image &&
                        contactTestimonialsUser.image !== "image"
                          ? contactTestimonialsUser.image
                          : defaultpropertyimage
                      }
                      alt="Property"
                      onError={(e) => propertypic(e)}
                    />
                    {contactTestimonialsUser.star &&
                    !isNaN(contactTestimonialsUser.star) &&
                    parseInt(contactTestimonialsUser.star) > 0 ? (
                      <p className="mt-2">
                        {[...Array(parseInt(contactTestimonialsUser.star))].map(
                          (_, index) => (
                            <i
                              key={index}
                              style={{
                                color: "#FFBC0B",
                                fontSize: "30px",
                              }}
                              className="fa fa-star"
                              aria-hidden="true"
                            ></i>
                          )
                        )}
                      </p>
                    ) : (
                      <p>No star rating available</p>
                    )}

                    {contactTestimonialsUser.displayName ? (
                      <p>
                        <b>Display Name:</b>{" "}
                        {contactTestimonialsUser.displayName}
                      </p>
                    ) : (
                      ""
                    )}

                    {contactTestimonialsUser.headlineReview ? (
                      <p>
                        <b>Headline Review:</b>{" "}
                        {contactTestimonialsUser.headlineReview}
                      </p>
                    ) : (
                      ""
                    )}

                    {contactTestimonialsUser.description ? (
                      <p>
                        <b>Description:</b>{" "}
                        {contactTestimonialsUser.description}
                      </p>
                    ) : (
                      ""
                    )}
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </>
    </>
  );
};

export default ContactProfile;

import React, { useEffect, useState } from "react";
import avtar from "../img/avtar.jpg";
import { NavLink, useNavigate, useParams } from "react-router-dom";
import user_service from "../service/user_service";
import ReactPaginate from "react-paginate";
import Loader from "../../Pages/Loader/Loader.js";
import Toaster from "../../Pages/Toaster/Toaster";
import defaultpropertyimage from "../../Components/img/defaultpropertyimage.jpeg";
import { logDOM } from "@testing-library/react";
import jwt from "jwt-decode";

function ContactTransaction() {
  const [activeGet, setActiveGet] = useState([]);
  const params = useParams();
  const [results, setResults] = useState([]);
  const [pageCount, setPageCount] = useState(0);
  const [loader, setLoader] = useState({ isActive: null });
  const { isActive } = loader;
  const [toaster, setToaster] = useState({
    types: null,
    isShow: null,
    toasterBody: "",
    message: "",
  });
  const { types, isShow, toasterBody, message } = toaster;

  const navigate = useNavigate();

  const AddTransaction = () => {
    navigate("/add-transaction");
  };

  const Catalog = (id) => {
    navigate(`/transaction-summary/${id}`);
  };

  const TransactionGetAll = async () => {
    try {
      setLoader({ isActive: true });

      let queryParams = `?page=1`;

      queryParams += `&filing=`;

      if (
        localStorage.getItem("auth") &&
        jwt(localStorage.getItem("auth")).contactType === "admin"
      ) {
        // queryParams += `&agentId=${jwt(localStorage.getItem("auth")).id}`;
      } else {
        queryParams += `&agentId=${jwt(localStorage.getItem("auth")).id}`;
      }

      const response = await user_service.TransactionGet(queryParams);

      setLoader({ isActive: false });

      if (response && response.data) {
        setActiveGet(response.data.data);
        setPageCount(Math.ceil(response.data.totalRecords / 10));
      }
    } catch (error) {
      console.error("Error in TransactionGetAll:", error);
      // Handle errors as needed
    }
  };

  useEffect(() => {
    window.scrollTo(0, 0);
    setLoader({ isActive: true });
    TransactionGetAll();
  }, []);

  const handleResetClick = () => {
    setLoader({ isActive: true });
    setSearchdata("");
    TransactionGetAll();
  };

  const handlePageClick = async (data) => {
    let currentPage = data.selected + 1;
    let skip = currentPage - 1 + currentPage;
    setLoader({ isActive: true });
    if (
      localStorage.getItem("auth") &&
      (jwt(localStorage.getItem("auth")).contactType == "associate" ||
        jwt(localStorage.getItem("auth")).contactType == "agent")
    ) {
      var query = `?page=${currentPage}&agentId=${
        jwt(localStorage.getItem("auth")).id
      }`;
    } else {
      var query = `?page=${currentPage}`;
    }
    await user_service.TransactionGet(query).then((response) => {
      setLoader({ isActive: false });
      if (response) {
        setActiveGet(response.data.data);
        setPageCount(Math.ceil(response.data.totalRecords / 10));
      }
    });
  };

  // const SearchGetAll = async () => {

  //     await user_service.SearchTransaction(1, searchdata).then((response) => {
  //         if (response) {
  //             // console.log(response);
  //             setActiveGet(response.data.data);
  //             setPageCount(Math.ceil(response.data.totalRecords / 10));
  //         }
  //     });
  // }

  const SearchGetAll = async () => {
    let queryParams = `?page=1`;
    if (
      localStorage.getItem("auth") &&
      jwt(localStorage.getItem("auth")).contactType == "admin"
    ) {
      // queryParams += `&agentId=${jwt(localStorage.getItem("auth")).id}`;
    } else {
      queryParams += `&agentId=${jwt(localStorage.getItem("auth")).id}`;
    }
    await user_service
      .TransactionGet(queryParams, searchdata)
      .then((response) => {
        if (response) {
          console.log(response);
          setActiveGet(response.data.data);
          setPageCount(Math.ceil(response.data.totalRecords / 10));
        }
      });
  };

  const propertypic = (e) => {
    e.target.src = defaultpropertyimage;
  };
  const [searchdata, setSearchdata] = useState("");
  const handleChange = (e) => {
    setSearchdata(e.target.value);
  };


  const handleSubmit = (id, contactType) => {
    if(id){
      if (contactType === "private") {
        navigate(`/private-contact/${id}`);
      } else {
        navigate(`/contact-profile/${id}`);
      }
    }
  };

  return (
    <div className="bg-secondary float-left w-100 pt-4 mb-4">
      <Loader isActive={isActive} />
      {isShow && (
        <Toaster
          types={types}
          isShow={isShow}
          toasterBody={toasterBody}
          message={message}
        />
      )}
      <main className="page-wrapper">
        {/* <!-- Page container--> */}
        <div className="content-overlay">
          {/* <!-- Page card like wrapper--> */}

          {/* <nav className="mb-3 mb-md-4 pt-md-3" aria-label="Breadcrumb">
                        <ol className="breadcrumb breadcrumb-dark">
                            <li className="breadcrumb-item"><a href="#">Home</a></li>
                            <li className="breadcrumb-item"><a href="#">Account</a></li>
                            <li className="breadcrumb-item active" aria-current="page">Listing Transaction</li>
                        </ol>
                    </nav> */}
          <div className="">
            <div className="d-flex align-items-center justify-contact-start">
              <h3 className="mb-2 text-white">Active Transactions</h3>
            </div>

            {/* <!-- Content--> */}
            <div className="row">
              <div className="col-lg-12">
                <div className="d-flex justify-content-start align-items-center">
                  <div className="form-outline w-50">
                    <input
                      id="search-input-1"
                      type="search"
                      className="form-control"
                      placeholder="Street address, contact name, or reference number."
                      name="searchdata"
                      onChange={handleChange}
                      onKeyDown={(e) => {
                        if (e.key === "Enter") {
                          SearchGetAll();
                        }
                      }}
                    />
                  </div>
                  <span className="ms-4">
                    <button
                      type="button"
                      className="btn btn-primary"
                      onClick={() => SearchGetAll()}
                    >
                      Find
                    </button>
                    <a
                      href="#"
                      className="text-white"
                      id="button-item"
                      onClick={handleResetClick}
                      type="button"
                    >
                      Reset
                    </a>
                  </span>
                </div>

                {/* <div className="mt-2" id="from-check-inline">
                                    <span className="ms-0" id="match-only">Show Only</span>
                                    <span className="ms-2">
                                        <input className="form-check-input" id="form-radio-1" type="radio" name="radios-inline" checked />
                                        <label className="form-check-label">My Transactions</label>
                                    </span>
                                    <span className="ms-2">
                                        <input className="form-check-input" id="form-radio-2" type="radio" name="radios-inline" checked />
                                        <label className="form-check-label">Managed Transactions</label>
                                    </span>
                                </div> */}
              

                {/* <!-- Item--> */}
                <div class="table-responsive bg-light border rounded-3 p-3 mt-3">
                  <table
                    class="table table-striped align-middle mb-0 w-100"
                    border="0"
                    cellspacing="0"
                    cellpadding="0">
                    <thead>
                      <tr>
                        <th>ADDRESS</th>
                        <th>SIDE</th>
                        <th>CLIENT</th>
                        <th>AGENT</th>
                        <th>STATUS</th>
                        <th>TRANSACTION REVIEW</th>
                        {/* {localStorage.getItem("auth") &&
                    jwt(localStorage.getItem("auth")).contactType == "admin" ? (
                      <th>ACTION</th>
                    ) : (
                      ""
                    )} */}
                      </tr>
                    </thead>
                    <tbody>
                      {activeGet && activeGet.length > 0 ? (
                        activeGet.map((post) => (
                          <tr key={post._id}>
                            {post.propertyDetail ? (
                              <>
                                <td>
                                  <div className="d-flex align-items-center justify-content-start" style={{ width: "250px" }}>
                                    <img
                                      style={{ width: "65px", height: "65px" }}
                                      onClick={(e) =>
                                        Catalog(post._id, post.agentId)
                                      }
                                      src={
                                        post.propertyDetail?.image &&
                                        post.propertyDetail.image !== "image"
                                          ? post.propertyDetail.image
                                          : defaultpropertyimage
                                      }
                                      alt="Property"
                                      onError={(e) => propertypic(e)}
                                    />
                                    <span className="ms-2">
                                      {post.propertyDetail.streetAddress}
                                      {/* {post.propertyDetail.city},
                                  {post.propertyDetail.state} */}
                                    </span>
                                  </div>
                                </td>
                                <td>
                                  {post.represent === "seller"
                                    ? "Seller"
                                    : post.represent === "buyer"
                                    ? "Buyer"
                                    : post.represent === "both"
                                    ? "Buyer & Seller"
                                    : post.represent === "referral"
                                    ? "Referral"
                                    : "N/A"}
                                </td>

                                <td>
                                  {post.contact1 &&
                                  post.contact1.length > 0 &&
                                  post.contact1[0] ? (
                                    <div
                                      onClick={() =>
                                        handleSubmit(
                                          post.contact1[0].data._id,
                                          post.contact1[0].data.contactType
                                        )
                                      }
                                    >
                                      {post.contact1[0].data.organization
                                        ? post.contact1[0].data.organization
                                        : post.contact1[0].data.firstName}
                                      &nbsp;{post.contact1[0].data.lastName}
                                    </div>
                                  ) : (
                                    "N/A"
                                  )}
                                </td>
                                <td>
                                  {post.contact3 &&
                                  post.contact3.length > 0 &&
                                  post.contact3[0] ? (
                                    <>
                                      {post.contact3[0].data.organization
                                        ? post.contact3[0].data.organization
                                        : post.contact3[0].data.firstName}
                                      &nbsp;{post.contact3[0].data.lastName}
                                    </>
                                  ) : (
                                    "N/A"
                                  )}
                                </td>

                                <td>
                                  {post.phase ? (
                                    <span className="badge bg-success">
                                      {post.phase === "pre-listed"
                                        ? "Pre-Listed"
                                        : post.phase === "active-listing"
                                        ? "Active Listing"
                                        : post.phase === "showing-homes"
                                        ? "Showing Homes"
                                        : post.phase === "under-contract"
                                        ? "Under Contract"
                                        : post.phase === "closed"
                                        ? "Closed"
                                        : post.phase === "canceled"
                                        ? "Canceled"
                                        : ""}
                                    </span>
                                  ) : (
                                    "N/A"
                                  )}
                                </td>
                                <td>
                                  <div className="btn btn-secondary btn-sm">
                                    {post.trans_status === "submitted"
                                      ? "Submitted"
                                      : post.trans_status === "approve"
                                      ? "In Review Approved"
                                      : post.trans_status === "confirmed"
                                      ? "In Review Confirmed"
                                      : post.trans_status === "funding_complete"
                                      ? "Funding Complete"
                                      : post.trans_status === "filed"
                                      ? "Filed"
                                      : "Active"}
                                  </div>
                                </td>
                              </>
                            ) : (
                              <>
                                <td>
                                  <img
                                    style={{ width: "65px", height: "65px" }}
                                    onClick={(e) =>
                                      Catalog(post._id, post.agentId)
                                    }
                                    src="https://integra.workspace.lwolf.com/common/handler/mls/photo_custom.ashx?s=1&t=5eada3e3-491e-46e2-938c-7c785a1f9c68&r=Seller"
                                    alt="Property"
                                    onError={(e) => propertypic(e)}
                                  />
                                  Property Not Set
                                </td>

                                <td>
                                  {post.represent === "seller"
                                    ? "Seller"
                                    : post.represent === "buyer"
                                    ? "Buyer"
                                    : post.represent === "both"
                                    ? "Buyer & Seller"
                                    : post.represent === "referral"
                                    ? "Referral"
                                    : "N/A"}
                                </td>

                                <td>
                                  <div
                                    onClick={() =>
                                      handleSubmit(post._id, post.contactType)
                                    }
                                  >
                                    {post.contact1 &&
                                    post.contact1.length > 0 &&
                                    post.contact1[0] ? (
                                      <>
                                        {post.contact1[0].data.organization
                                          ? post.contact1[0].data.organization
                                          : post.contact1[0].data.firstName}
                                        &nbsp;{post.contact1[0].data.lastName}
                                      </>
                                    ) : (
                                      "N/A"
                                    )}
                                  </div>
                                </td>

                                <td>
                                  {post.contact3 &&
                                  post.contact3.length > 0 &&
                                  post.contact3[0] ? (
                                    <>
                                      {post.contact3[0].data.organization
                                        ? post.contact3[0].data.organization
                                        : post.contact3[0].data.firstName}
                                      &nbsp;{post.contact3[0].data.lastName}
                                    </>
                                  ) : (
                                    "N/A"
                                  )}
                                </td>

                                <td>
                                  {post.phase ? (
                                    <span className="badge bg-success">
                                      {post.phase === "pre-listed"
                                        ? "Pre-Listed"
                                        : post.phase === "active-listing"
                                        ? "Active Listing"
                                        : post.phase === "showing-homes"
                                        ? "Showing Homes"
                                        : post.phase === "under-contract"
                                        ? "Under Contract"
                                        : post.phase === "closed"
                                        ? "Closed"
                                        : post.phase === "canceled"
                                        ? "Canceled"
                                        : ""}
                                    </span>
                                  ) : (
                                    "N/A"
                                  )}
                                </td>

                                <td>
                                  <div className="btn btn-secondary btn-sm">
                                    {post.trans_status === "submitted"
                                      ? "Submitted"
                                      : post.trans_status === "approve"
                                      ? "In Review Approved"
                                      : post.trans_status === "confirmed"
                                      ? "In Review Confirmed"
                                      : post.trans_status === "funding_complete"
                                      ? "Funding Complete"
                                      : post.trans_status === "filed"
                                      ? "Filed"
                                      : "Active"}
                                  </div>
                                </td>
                              </>
                            )}
                          </tr>
                        ))
                      ) : (
                        <tr>
                          <td colSpan="7" className="text-center">
                            <div className="mt-5 float-start w-100">
                              <i
                                className="fa fa-calculator mb-5"
                                aria-hidden="true"
                                style={{ fontSize: "135px" }}
                              ></i>
                              <br />
                              <span className="text-dark">
                                No Active Transactions
                              </span>
                            </div>
                          </td>
                        </tr>
                      )}
                    </tbody>
                  </table>
                </div>
                {pageCount > 1 ? (
                  <div className="justify-content-end mb-1 mt-4">
                    <ReactPaginate
                      previousLabel={"Previous"}
                      nextLabel={"next"}
                      breakLabel={"..."}
                      pageCount={pageCount}
                      marginPagesDisplayed={1}
                      pageRangeDisplayed={2}
                      onPageChange={handlePageClick}
                      containerClassName={"pagination justify-content-center"}
                      pageClassName={"page-item"}
                      pageLinkClassName={"page-link"}
                      previousClassName={"page-item"}
                      previousLinkClassName={"page-link"}
                      nextClassName={"page-item"}
                      nextLinkClassName={"page-link"}
                      breakClassName={"page-item"}
                      breakLinkClassName={"page-link"}
                      activeClassName={"active"}
                    />
                  </div>
                ) : (
                  ""
                )}
              </div>
            </div>
          </div>
        </div>
      </main>
    </div>
  );
}

export default ContactTransaction;

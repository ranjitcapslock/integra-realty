import React, { useState, useEffect, useRef } from "react";
import Pic from "../img/pic.png";

import user_service from "../service/user_service";
import ReactPaginate from "react-paginate";
import Loader from "../../Pages/Loader/Loader.js";
import Toaster from "../../Pages/Toaster/Toaster.js";
import { NavLink, useNavigate, useParams } from "react-router-dom";
import jwt from "jwt-decode";
// import $, { each } from "jquery";
// import { Editor } from "@tinymce/tinymce-react";
// import Select2 from "react-select2-wrapper";
// import "react-select2-wrapper/css/select2.css";
// import de from "date-fns/locale/de/index";

function OnboardDocuments({ checkboxState, clearcheckboxState }) {
  // console.log("clearcheckboxState")
  // console.log(checkboxState)
  const [getContact, setGetContact] = useState([]);
  const [pageCount, setPageCount] = useState(0);
  const [totalRecords, setTotalRecords] = useState("");
  const [loader, setLoader] = useState({ isActive: null });
  const { isActive } = loader;
  const [toaster, setToaster] = useState({
    types: null,
    isShow: null,
    toasterBody: "",
    message: "",
  });
  const { types, isShow, toasterBody, message } = toaster;

  const [query, setQuery] = useState("");
  const [contactType, setContactType] = useState("");
  const [currentPage, setCurrentPage] = useState(1);

  const [isSubmitClick, setISSubmitClick] = useState(false);
  const [results, setResults] = useState([]);
  const [formValuesNew, setFormValuesNew] = useState({
    staff_recruiter: [],
    staff_recruiter_id: [],
  });
  const [selectedContacts, setSelectedContacts] = useState([]);
  const [isLoading, setIsLoading] = useState(false);

  const isValidEmail = (email) => {
    // Regular expression for a simple email validation
    const emailRegex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
    return emailRegex.test(email);
  };
  const handleChangeNew = (e) => {
    const { name, value } = e.target;
    setFormValuesNew({ ...formValuesNew, [name]: value });
    if (e.target.name === "staff_recruiter") {
      SearchGetAll(e.target.value);
    }
  };
  const handleInputKeyUp = (e) => {
    if (e.key === "Enter") {
      const enteredEmail = e.target.value.trim();

      if (isValidEmail(enteredEmail)) {
        setSelectedContacts([...selectedContacts, enteredEmail]);
        setFormValuesNew({ ...formValuesNew, staff_recruiter: "" });
      } else {
        console.log("Invalid email format!");
      }
    }
  };
  const handleSearchTransaction = (contactData) => {
    if (isValidEmail(contactData.email)) {
      setSelectedContacts([...selectedContacts, contactData.email]);
      setResults([]);
      setFormValuesNew("");
      setFormValuesNew({ ...formValuesNew, ["staff_recruiter"]: "" });
    } else {
      console.log("Invalid email format!");
    }
  };

  const SearchGetAll = async () => {
    setIsLoading(true);

    await user_service
      .SearchContactGet(0, 3, formValuesNew.staff_recruiter)
      .then((response) => {
        setIsLoading(false);
        if (response) {
          setResults(response.data.data);
        }
      });
  };

  const profilepic = (e) => {
    e.target.src = Pic;
  };

  const [notesmessage, setNotesmessage] = useState("");
  // const editorRef = useRef(null);
  // const log = () => {
  //   if (editorRef.current) {
  //     setNotesmessage(editorRef.current.getContent());
  //   }
  // };

  const params = useParams();

  const initialValues = {
    subject: "",
    message: "",
  };

  const [formValues, setFormValues] = useState(initialValues);
  const [formData, setFormData] = useState({});
  const [detail, setDetail] = useState("");
  const [formErrors, setFormErrors] = useState({});
  const [useridd, setUseridd] = useState();

  const ContactGetAll = async () => {
    //if (params.type == "post") {
    setLoader({ isActive: true });
    const ppp = `1&limit=10`;
    await user_service.contactGet(ppp).then((response) => {
      setLoader({ isActive: false });
      if (response) {
        //console.log(response.data.data);
        setGetContact(response.data.data);
        setPageCount(Math.ceil(response.data.totalRecords / 10));
        setTotalRecords(response.data.totalRecords);
      }
    });
    //}
  };

  const profileGetAll = async () => {
    setLoader({ isActive: true });
    await user_service
      .profileGet(jwt(localStorage.getItem("auth")).id)
      .then((response) => {
        if (response) {
          setLoader({ isActive: false });
          //   console.log(response);
          setFormData(response.data);
        }
      });
  };

  const PostGetById = async () => {
    await user_service.postDetail(params.id).then((response) => {
      setLoader({ isActive: false });
      if (response) {
        setDetail(response.data);
      }
    });
  };
  useEffect(() => {
    window.scrollTo(0, 0);
    ContactGetAll();
    profileGetAll();
    if (params.type == "post") {
      PostGetById(params.id);
    }
  }, []);

  useEffect(() => {
    window.scrollTo(0, 0);
    if (params.id) {
      setFormValues({
        subject: "OnBoard Documents ",
        message: `<b>${detail?.postTitle}</b> <p><a href="${
          window.location.origin
        }/post-detail/${detail?._id}">View Full Post</a></p><p>${
          formData?.email_signature || ""
        }</p>`,
      });
    } else {
      setFormValues({
        //subject: "Announcement for " + (detail?.postTitle || ""),
        message: `<p>${formData?.email_signature || ""}</p>`,
      });
    }
  }, [detail, formData]);

  const handleChanges = (e) => {
    setFormValues({ ...formValues, message: e });
  };
  const handleChange = (e) => {
    const { name, value } = e.target;
    setFormValues({ ...formValues, [name]: value });
  };

  const validate = () => {
    const values = formValues;
    const errors = {};

    if (!values.productname) {
      errors.productname = "Message is required.";
    } else if (values.productname.length > 100) {
      errors.productname = "Message must be 1000 characters or less.";
    }

    if (!values.message) {
      errors.message = "Product Full Details is required.";
    }

    setFormErrors(errors);
    return errors;
  };

  useEffect(() => {
    window.scrollTo(0, 0);
    if (query || contactType) {
      SearchGetAll(1);
    }
  }, [query, contactType]);

  const navigate = useNavigate();

  const handleSubmit = async (e, id) => {
    e.preventDefault();
    //const  selectedIds = getSelectedContactIds();

    console.log(selectedContacts);
    // console.log(checkboxState.checkboxState); return;
    if (selectedContacts?.length > 0) {
      setISSubmitClick(true);
      const userData = {
        agentId: jwt(localStorage.getItem("auth")).id,
        subject: formValues.subject,
        // message: formValues.message,
        ids: selectedContacts,
        doc_ids: checkboxState,
      };
      // setLoader({ isActive: true })
      await user_service.sendOnBoardMail(userData).then((response) => {
        if (response) {
          setLoader({ isActive: false });
          setISSubmitClick(false);
          setToaster({
            type: "Email",
            isShow: true,
            toasterBody: response.data.message,
            message: "Email Send Successfully",
          });
          setTimeout(() => {
            setToaster((prevToaster) => ({ ...prevToaster, isShow: false }));
            // navigate("/control-panel/documents/");
            clearcheckboxState();
            setSelectedContacts([]);
          }, 2000);
          document.getElementById("closeModalSendSelectedDocument").click();
        } else {
          setLoader({ isActive: false });
          setToaster({
            types: "error",
            isShow: true,
            toasterBody: response.data.message,
            message: "Error",
          });
          setTimeout(() => {
            setToaster((prevToaster) => ({ ...prevToaster, isShow: false }));
            // navigate("/control-panel/documents/");
          }, 2000);
        }
      });
    } else {
      setToaster({
        types: "error",
        isShow: true,
        toasterBody: "Please select any contact.",
        message: "Error",
      });
      setTimeout(() => {
        setToaster((prevToaster) => ({ ...prevToaster, isShow: false }));
      }, 2000);
    }
  };

  return (
    <div className="modal" role="dialog" id="SendSelectedDocumentsOnBoard">
      <Loader isActive={isActive} />
      {isShow && (
        <Toaster
          types={types}
          isShow={isShow}
          toasterBody={toasterBody}
          message={message}
        />
      )}
      <div
        className="modal-dialog modal-md modal-dialog-scrollable"
        role="document"
      >
        <div className="modal-content">
          <div className="modal-header">
            <h4 className="modal-title">Send Email</h4>
            <button
              className="btn-close"
              id="closeModalSendSelectedDocument"
              type="button"
              data-dismiss="modal"
              aria-label="Close"
            ></button>
          </div>
          <div className="modal-body fs-sm">
            <div className="">
              <>
                <div className="">
                  <div className="">
                    {/* <!-- List of resumes--> */}
                    <div className="d-flex align-items-center justify-content-start w-100 mb-4"></div>
                    <div className="bg-light border rounded-3 p-3">
                      <p className="w-100">
                        Compose your email message here. Use this space to craft
                        your message, add links, images, and personalize it
                        before sending it to your recipients. Remember to
                        proofread and test before hitting send!
                      </p>
                      <div className="">
                        <div className="row">
                          <div className="col-md-6">
                            <label className="form-label">
                              Select Email From Contacts / External{" "}
                            </label>
                            <input
                              className="form-control staff_recruiterinput"
                              id="inline-form-input"
                              type="text"
                              name="staff_recruiter"
                              value={formValuesNew.staff_recruiter ?? ""}
                              onChange={handleChangeNew}
                              onKeyUp={handleInputKeyUp}
                            />

                            {isLoading ? (
                              ""
                            ) : (
                              <div className="activateaccountstaff_recruiter">
                                {results && results.length > 0
                                  ? results.map((post) => (
                                      <div
                                        className="activateaccountstaff"
                                        key={post._id}
                                      >
                                        <ul
                                          className="list-group"
                                          onClick={() =>
                                            handleSearchTransaction(post)
                                          }
                                        >
                                          <li className="list-group-item d-flex justify-content-start align-items-center">
                                            <div>
                                              <img
                                                className="rounded-circle profile_picture"
                                                src={post.image || Pic}
                                                alt="Profile"
                                                onError={(e) => profilepic(e)}
                                              />
                                              <br />
                                            </div>
                                            <div>
                                              <strong>
                                                {post.firstName}&nbsp;
                                                {post.lastName}{" "}
                                              </strong>
                                              <br />
                                              <strong>
                                                {post.contactType
                                                  ? post.contactType
                                                      .charAt(0)
                                                      .toUpperCase() +
                                                    post.contactType.slice(1)
                                                  : ""}
                                              </strong>
                                              <br />
                                            </div>
                                          </li>
                                        </ul>
                                      </div>
                                    ))
                                  : ""}
                              </div>
                            )}
                          </div>

                          <div className="col-md-6">
                            {selectedContacts.map((contact) => (
                              <div>{`${contact}`}</div>
                            ))}
                          </div>
                        </div>
                        <div className="row">
                          <div className="col-md-12">
                            <label className="col-form-label">
                              Subject — Please enter a unique, descriptive
                              subject.
                            </label>

                            {/* <span className="pull-right">
                                                                      Uniquely identifies product. (Max length: 100 chars)
                                                                  </span> */}
                            <input
                              className="form-control"
                              id="inline-form-input"
                              type="text"
                              name="subject"
                              value={formValues.subject}
                              onChange={handleChange}
                            />
                            {formErrors.subject && (
                              <div className="invalid-tooltip">
                                {formErrors.subject}
                              </div>
                            )}
                          </div>
                        </div>
                        {/* <div className="row">
                                                <div className="col-md-12">
                                                  <label className="col-form-label">Message</label>
                                                  <Editor
                                                    onEditorChange={(e) => handleChanges(e)}
                                                    value={formValues.message}
                                                    onInit={(evt, editor) => (editorRef.current = editor)}
                                                    initialValue=""
                                                    init={{
                                                      height: 300,
                                                      menubar: false,
                                                      plugins: [
                                                        "advlist autolink lists link image charmap print preview anchor",
                                                        "searchreplace visualblocks code fullscreen",
                                                        "insertdatetime media table paste code help wordcount",
                                                      ],
                                                      toolbar:
                                                        "undo redo | formatselect | " +
                                                        "bold italic backcolor | alignleft aligncenter " +
                                                        "alignright alignjustify | bullist numlist outdent indent | " +
                                                        "removeformat | help",
                                                      content_style:
                                                        "body { font-family:Helvetica,Arial,sans-serif; font-size:14px }",
                                                    }}
                                                  />
                                                  {formErrors.message && (
                                                    <div className="invalid-tooltip">
                                                      {formErrors.message}
                                                    </div>
                                                  )}
                                                </div>
                                              </div> */}
                      </div>
                    </div>
                    <div className="pull-right mt-3">
                      {/* <NavLink
                                              className="btn btn-secondary pull-right ms-3"
                                              type="button"
                                              to="/"
                                            >
                                              Cancel
                                            </NavLink> */}
                      <button
                        className="btn btn-primary pull-right"
                        type="button"
                        onClick={handleSubmit}
                        disabled={isSubmitClick}
                      >
                        {isSubmitClick ? "Please wait..." : "Send Mail"}{" "}
                      </button>
                    </div>
                  </div>
                </div>
              </>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
export default OnboardDocuments;

import React, { useState, useEffect } from "react";
import _ from "lodash";
import user_service from "../service/user_service";
import jwt from "jwt-decode";
import Loader from "../../Pages/Loader/Loader.js";
import Toaster from "../../Pages/Toaster/Toaster.js";
import { NavLink, useNavigate } from "react-router-dom";
import defaultpropertyimage from "../../Components/img/defaultpropertyimage.jpeg";

function ReservationListing() {
  const [loader, setLoader] = useState({ isActive: null });
  const { isActive } = loader;
  const [toaster, setToaster] = useState({
    types: null,
    isShow: null,
    toasterBody: "",
    message: "",
  });
  const { types, isShow, toasterBody, message } = toaster;
  const navigate = useNavigate();

  const [reservationData, setReservationData] = useState([]);

  useEffect(() => {
    window.scrollTo(0, 0);
    setLoader({ isActive: true });
    reservationGetAllData();
  }, []);

  const reservationGetAllData = async () => {
    await user_service.reservationGetAll().then((response) => {
      setLoader({ isActive: false });
      if (response) {
        console.log(response);
        setReservationData(response.data.data);
      }
    });
  };

  const handleCalender = () => {
    navigate(`/add-reservations`);
  };

  const handleReservationData = (id)=>{
    navigate(`/reservation-calender/${id}`);
  }

 

  return (
    <div className="bg-secondary float-left w-100 pt-4 mb-4">
      <Loader isActive={isActive} />
      {isShow && (
        <Toaster
          types={types}
          isShow={isShow}
          toasterBody={toasterBody}
          message={message}
        />
      )}
      <main className="page-wrapper">
        <div className="content-overlay">
          <button
            className="btn btn-primary pull-right"
            onClick={handleCalender}
          >
            Add a Calender
          </button>
          <div className="d-flex align-items-center justify-content-start mb-4">
            <h3 className="mb-0 text-white">Reservations</h3>
          </div>

          <div className="bg-light p-3 border-0 rounded-3">
            <div className="promoted_product float-left w-100">
              <div className="row">
                {reservationData && reservationData.length > 0 ? (
                  reservationData.map((post) => (
                    <div className="col-lg-4  col-md-4 col-sm-6 col-12 mb-4" onClick={()=>handleReservationData(post._id)}>
                      <div className="card shadow-sm cursor-pointer card-hover border-0 h-100">
                        <div className="card-body position-relative p-2 pb-0">
                          <i
                            class="fa fa-briefcase icon"
                            aria-hidden="True"
                          ></i>
                          <NavLink className="ms-2">
                            {post.calender_name}
                          </NavLink>
                          <h6 className="mt-2 ms-3">
                            {post.publication_Level === "corporate"
                              ? "Corporate"
                              : ""}
                          </h6>
                        </div>
                        <div className="card-img-top card-img-hover">
                          <img
                            className="img-fluid rounded-0 w-100"
                            src={
                              post.image && post.image !== "image"
                                ? post.image
                                : defaultpropertyimage
                            }
                            alt="Reservation"
                          />
                        </div>
                        <p className="reservation_description mt-2 ms-3">{post.calender_blurb}</p>
                      </div>
                    </div>
                  ))
                ) : (
                  <p className="text-center mt-5">
                    <span className="">No Data found</span>
                  </p>
                )}
              </div>
            </div>
          </div>
        </div>
      </main>
    </div>
  );
}
export default ReservationListing;

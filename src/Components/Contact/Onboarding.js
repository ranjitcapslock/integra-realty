import React, { useState, useEffect, useMemo, useRef } from "react";
import _ from "lodash";
import { Document, Page } from "react-pdf";
import SignatureCanvas from "react-signature-canvas";
import Draggable from "react-draggable";
import html2canvas from "html2canvas";
import jsPDF from "jspdf";
import moment from "moment-timezone";
import { PDFDocument, PDFLib, PDFTextField, PDFCheckBox, PDFRadioGroup, rgb, StandardFonts } from "pdf-lib";
// import { PDFDocument, } from 'pdf-lib';
import AgentAdded from "../../Components/img/Thankyou_page_Image.jpg";
import axios from "axios";

import { MultiSelect } from "react-multi-select-component";
import { MultiSelect as MultiSelect2 } from "react-multi-select-component";
import user_service from "../service/user_service";
import Loader from "../../Pages/Loader/Loader.js";
import Toaster from "../../Pages/Toaster/Toaster.js";
import { useNavigate, useParams, NavLink } from "react-router-dom";
import jwt from "jwt-decode";
import Pic from "../img/pic.png";
import countryList from "country-list";

import SignaturePad from "signature_pad";
import { Nav } from "react-bootstrap";

const Onboarding = () => {
  const params = useParams();
  // console.log(params.cid);
  // console.log(params.uid);

  const options = useMemo(() => {
    const countries = countryList.getData();
    const utahState = {
      code: "UT",
      name: "Utah",
      districts: [
        "Alabama",
        "Alaska",
        "Arizona",
        "Arkansas",
        "California",
        "Colorado",
        "Connecticut",
        "Delaware",
        "District of Columbia",
        "Florida",
        "Georgia",
        "Hawaii",
        "Idaho",
        "Illinois",
        "Indiana",
        "Iowa",
        "Kansas",
        "Kentucky",
        "Louisiana",
        "Maine",
        "Maryland",
        "Massachusetts",
        "Michigan",
        "Minnesota",
        "Mississippi",
        "Missouri",
        "Montana",
        "Nebraska",
        "Nevada",
        "New Hampshire",
        "New Jersey",
        "New Mexico",
        "New York",
        "North Carolina",
        "North Dakota",
        "Ohio",
        "Oklahoma",
        "Oregon",
        "Pennsylvania",
        "Rhode Island",
        "South Carolina",
        "South Dakota",
        "Tennessee",
        "Texas",
        "Utah",
        "Vermont",
        "Virginia",
        "Washington",
        "West Virginia",
        "Wisconsin",
        "Wyoming",
      ],
    };
    return [...countries, utahState];
  }, []);

  const initialValues = {
    firstName: "",
    nickName: "",
    middleName: "",
    middleInitials: "",
    prefix: "",
    lastName: "",
    maidenName: "",
    suffix: "",
    email: "",
    passwordHash: "",
    confirmPassword: "",
  };

  const initialValuess = {
    email: "",
    image: "",
    phone: "",
    firstName: "",
    lastName: "",
    nickname: "",
    address: "",
    city: "",
    state: "",
    zip: "",
    contactpermission: "",
    web: "",
    fax: "",
    inbox: "",
    organization: "",
    homeOffice: "",
    title: "",
    agentId: "",
    profession: "",
    industry: "",
    assistant: "",
    designations: "",
    primary_areas: "",
    languages: "",
    specialties: "",
    marketing_message: "",
    birthday: "",
    gender: "",
    anniversary: "",
    recruiter: "",
    supporters: "",
    connection: "",
    notes: "",
  };

  const initialValuesAbout = {
    full_name: "",
    birthday: "",
    spouse: "",
    spend_birthday: "",
    shirt_size: "",
    spend_day: "",
    professional_skill: "",
    hobbies: "",
    vacation: "",
    travel_bucket: "",
    food: "",
    restaurant: "",
    music: "",
    book: "",
    phone_app: "",
    dessert: "",
    anything_else: "",
  };
  // const [data, setData] = useState("")
  const [formValuesAboout, setFormValuesAbout] = useState(initialValuesAbout);
  const [getContact, setGetContact] = useState(initialValuess);
  const [agentpapernameAdd, setAgentpapernameAdd] = useState("");
  const [w9papernameAdd, setW9papernameAdd] = useState("");
  const [contractorAgreementpapernameAdd, setContractorAgreementpapernameAdd] = useState("");
  const [file, setFile] = useState(null);
  const [fileExtension, setFileExtension] = useState("");
  const [acceptedFileTypes, setAcceptedFileTypes] = useState([]);
  const [showSubmitButton, setShowSubmitButton] = useState(false);

  const [isdone, setIsdone] = useState(false);
  const [pdfSrcNew, setPdfSrcNew] = useState(false);
  const [pdfSrcNewsign, setPdfSrcNewsign] = useState(PDF_SRCNew);


  const [submitNewPdf, setSubmitNewPdf] = useState(false);
  const [activeButton, setActiveButton] = useState("1");
  const [data, setData] = useState("");
  const [imageData, setImageData] = useState("");
  const [commaSeparatedValues, setCommaSeparatedValues] = useState("");
  const [
    commaSeparatedValuesmls_membership,
    setCommaSeparatedValuesmls_membership,
  ] = useState("");
  const [board_membership, setBoard_membership] = useState([]);
  const [mls_membership, setMls_membership] = useState([]);

  if (mls_membership.length > 1) {
    setMls_membership(mls_membership.slice(1));
  }
  if (board_membership.length > 1) {
    setBoard_membership(board_membership.slice(1));
  }
  const [clientId, setClientId] = useState("");
  const [conatctdata, setConatctdata] = useState("");

  const [note, setNotes] = useState(false);
  const [notesdata, setNotesdata] = useState("");
  const [organizationGet, setOrganizationGet] = useState([]);

  const navigate = useNavigate();
  const [formValuesNew, setFormValuesNew] = useState("");
  const [accountData, setAccountData] = useState(initialValues);
  const [formValues, setFormValues] = useState({
    office: "Corporate",
    intranet_id: "",
    nickname: "",
    subscription_level: "Intranet Only",
    admin_note: "",
    sponsor_associate: "",
    staff_recruiter: "",
    nrds_id: "",
    associate_member: "",
    plan_date: "",
    billing_date: "",
    account_name: "Integra Reality",
    invoicing: "Credit Card Auto-Bill",
    licensed_since: "",
    associate_since: "",
    staff_since: "",
    iscorporation: "",
    agentTax_ein: "",
    agent_funding: "",
    ssn_itin: "",
    date_birth: "",
    private_idcard_type: "",
    private_idstate: "",
    private_idnumber: "",
    license_state: "",
    license_type: "Sales Agent",
    license_number: "",
    date_issued: "",
    date_expire: "",
    mls_membership: [],
    board_membership: [],
    agent_id: "",
    activatecontact: "",
  });

  const today = "2023/11/23";
  const [onBoard, setOnBoard] = useState("");
  const [onBoardparkcity, setOnBoardparkcity] = useState(false);
  const [parkcityfinal, setParkcityfinal] = useState("");

  const [show, setIsShow] = useState(false);

  const [additionalActivateFields, setAdditionalActivateFields] = useState("");
  const [formErrors, setFormErrors] = useState({});
  const [isSubmitClick, setISSubmitClick] = useState(false);
  const [loader, setLoader] = useState({ isActive: null });
  const [isSubmitClickNew, setISSubmitClickNew] = useState(false);

  const { isActive } = loader;
  const [toaster, setToaster] = useState({
    type: null,
    isShow: null,
    toasterBody: "",
    message: "",
  });
  const { type, isShow, toasterBody, message } = toaster;
  const [checkAll, setCheckAll] = useState("");
  const [category, setCategory] = useState("");
  const [step1, setStep1] = useState(true);
  const [step4, setStep4] = useState(false);
  const [step5, setStep5] = useState(false);
  const [phase, setPhase] = useState("");   // it was step 3, we removed it for now 

  const [agentId, setAgentId] = useState("");
  const [agentOnBoardMls, setAgentOnBoardMls] = useState("");
  const [agentOnBoardboard, setAgentOnBoardboard] = useState("");

  const [pageNumber, setPageNumber] = useState(1);
  const [numPages, setNumPages] = useState(null);
  const signatureCanvasRef = useRef(null);
  const signatureCanvasRefNew = useRef(null);
  const [signatures, setSignatures] = useState({});
  const [isDragging, setIsDragging] = useState(false);
  const [position, setPosition] = useState({ x: 50, y: 50 });
  const [confirmedSignatures, setConfirmedSignatures] = useState([]);
  const [pdfPath, setPdfPath] = useState("");
  const [isLoading, setIsLoading] = useState(false);
  const [results, setResults] = useState([]);
  const [pdfSrc, setPdfSrc] = useState(PDF_SRC);
  const [fieldValues, setFieldValues] = useState({});
  const [fieldNames, setFieldNames] = useState([]);
  const [form, setForm] = useState([]);



  const [agreementdone, setAgreementdone] = useState(false);
  const [referralAgreementdone, setReferralAgreementdone] = useState(false);

  const profilepic = (e) => {
    e.target.src = Pic;
  }


  const checkurlstring = (urlString, content) => {
    // const urlString = "https://brokeragentbase.s3.amazonaws.com/1700758888303-Utah%20Real%20Estate%20Membership%20Change%20Form.pdf";

    // Replace %20 with spaces in the URL
    const decodedUrl = decodeURIComponent(urlString);

    // Extracting the file name from the URL (assuming the last part after the last '/' is the filename)
    const fileName = decodedUrl.substring(decodedUrl.lastIndexOf('/') + 1);

    // Checking if the filename contains the specified text
    if (fileName.includes(content)) {
      return true;
    } else {
      return false;
      console.log("not found in the URL.");
    }

  }


  useEffect(() => {
    window.scrollTo(0, 0);
    ContactGetByuniqueid(params.uid, params.cid);
    // OrganizationTreeGets();
  }, [params.uid, params.cid]);

  const AgentpapernameAdd = async () => {
    await user_service.AgentpapernameAdd().then((response) => {
      if (response) {
        // setAgentpapernameAdd(response.data);
        // console.log(response.data.data)
        const filteredData = response.data.data.find(item => item.paperwork_title === "W9");
        const filteredDatacontractorAgreement = response.data.data.find(item => item.paperwork_title === "Independent Contractor Agreement");
        //console.log(filteredData)
        setW9papernameAdd(filteredData);
        setContractorAgreementpapernameAdd(filteredDatacontractorAgreement);
      }
    });
  };
  const ContactGetByuniqueid = async () => {
    await user_service
      .ContactGetByuniqueid(params.uid, params.cid)
      .then((response) => {
        if (response && response.data) {
          // console.log(response.data.data._id);
          setFormValuesNew(response.data.data);
          const contactId = response.data.data._id;
          ContactGetById(contactId);
          AgentpapernameAdd();
          const contactAgentId = response.data.data.agentId;
          if (response.data.data.passwordHash) {
            setStep1(false);
            setCategory(true);
          }
          setAgentId(contactAgentId);
          OrganizationTreeGets(contactAgentId);

          // const mlsMembership =
          //   response.data.data.additionalActivateFields.mls_membership;
          // console.log(mlsMembership);
          // setAgentOnBoardMls(mlsMembership);

          const mlsMembership =
            response.data.data.additionalActivateFields?.mls_membership;

          let MlsMembershipValues = []; // Declare the variable here

          if (mlsMembership) {
            // Check if mlsMembership is a string
            if (typeof mlsMembership === "string") {
              try {
                // Attempt to parse the string as JSON
                const mls_membershipParsed = JSON.parse(mlsMembership);

                if (mls_membershipParsed == "") {
                  // console.log(mls_membership);
                  setAgentOnBoardMls(mls_membership);
                } else {
                  // console.log(mls_membershipParsed);
                  setAgentOnBoardMls(mls_membershipParsed);

                  MlsMembershipValues = mls_membershipParsed.map(
                    (item) => item.value
                  );
                  // console.log(MlsMembershipValues);
                  setCommaSeparatedValues(MlsMembershipValues.join(", "));
                }
              } catch (error) {
                console.error("Error parsing board_membershipString:", error);
              }
            } else {
              console.error("board_membershipString is not a string.");
            }
          }

          const boardMembership =
            response.data.data.additionalActivateFields?.board_membership;

          let boardMembershipValues = []; // Declare the variable here

          if (boardMembership) {
            // Check if boardMembership is a string
            if (typeof boardMembership === "string") {
              try {
                // Attempt to parse the string as JSON
                const board_membershipParsed = JSON.parse(boardMembership);

                if (board_membershipParsed == "") {
                  console.log(board_membership);
                  setAgentOnBoardboard(board_membership);
                } else {
                  console.log(board_membershipParsed);
                  setAgentOnBoardboard(board_membershipParsed);

                  boardMembershipValues = board_membershipParsed.map(
                    (item) => item.value
                  );
                  console.log(boardMembershipValues);
                  setCommaSeparatedValues(boardMembershipValues.join(", "));
                }
                // Rest of your code...
              } catch (error) {
                console.error("Error parsing boardMembership:", error);
              }
            } else {
              console.error("boardMembership is not a string.");
            }
          }
          MlsBoardDocumentGetAll(MlsMembershipValues, boardMembershipValues);

          setAdditionalActivateFields(
            response.data.data.additionalActivateFields || {}
          );
          setAccountData(response.data.data || {});
        }
      });
  };


  const ContactGetByuniqueid2ndstep = async () => {
    await user_service
      .ContactGetByuniqueid(params.uid, params.cid)
      .then((response) => {
        if (response && response.data) {
          // console.log(response.data.data._id);
          setFormValuesNew(response.data.data);
          const contactId = response.data.data._id;
          ContactGetById(contactId);
          AgentpapernameAdd();
          const contactAgentId = response.data.data.agentId;

          setAgentId(contactAgentId);
          OrganizationTreeGets(contactAgentId);

          // const mlsMembership =
          //   response.data.data.additionalActivateFields.mls_membership;
          // console.log(mlsMembership);
          // setAgentOnBoardMls(mlsMembership);

          const mlsMembership =
            response.data.data.additionalActivateFields?.mls_membership;

          let MlsMembershipValues = []; // Declare the variable here

          if (mlsMembership) {
            // Check if mlsMembership is a string
            if (typeof mlsMembership === "string") {
              try {
                // Attempt to parse the string as JSON
                const mls_membershipParsed = JSON.parse(mlsMembership);

                if (mls_membershipParsed == "") {
                  // console.log(mls_membership);
                  setAgentOnBoardMls(mls_membership);
                } else {
                  // console.log(mls_membershipParsed);
                  setAgentOnBoardMls(mls_membershipParsed);

                  MlsMembershipValues = mls_membershipParsed.map(
                    (item) => item.value
                  );
                  // console.log(MlsMembershipValues);
                  setCommaSeparatedValues(MlsMembershipValues.join(", "));
                }
              } catch (error) {
                console.error("Error parsing board_membershipString:", error);
              }
            } else {
              console.error("board_membershipString is not a string.");
            }
          }

          const boardMembership =
            response.data.data.additionalActivateFields?.board_membership;

          let boardMembershipValues = []; // Declare the variable here

          if (boardMembership) {
            // Check if boardMembership is a string
            if (typeof boardMembership === "string") {
              try {
                // Attempt to parse the string as JSON
                const board_membershipParsed = JSON.parse(boardMembership);

                if (board_membershipParsed == "") {
                  console.log(board_membership);
                  setAgentOnBoardboard(board_membership);
                } else {
                  console.log(board_membershipParsed);
                  setAgentOnBoardboard(board_membershipParsed);

                  boardMembershipValues = board_membershipParsed.map(
                    (item) => item.value
                  );
                  console.log(boardMembershipValues);
                  setCommaSeparatedValues(boardMembershipValues.join(", "));
                }
                // Rest of your code...
              } catch (error) {
                console.error("Error parsing boardMembership:", error);
              }
            } else {
              console.error("boardMembership is not a string.");
            }
          }
          MlsBoardDocumentGetAll(MlsMembershipValues, boardMembershipValues);

          setAdditionalActivateFields(
            response.data.data.additionalActivateFields || {}
          );
          setAccountData(response.data.data || {});
        }
      });
  };

  const OrganizationTreeGets = async (contactAgentId) => {
    // setLoader({ isActive: true });
    await user_service.organizationTreeGet(contactAgentId).then((response) => {
      // setLoader({ isActive: false });
      if (response) {
        setOrganizationGet(response.data);
      }
    });
  };

  const ContactGetById = async (contactId) => {
    await user_service.contactGetById(contactId).then((response) => {
      if (response) {
        // console.log(response);
        setGetContact(response.data);

        if (response.data && response.data.additionalActivateFields) {
          setFormValues((prevFormValues) => ({
            ...prevFormValues,
            address: response.data.address ||
              prevFormValues.address,
            city: response.data.city ||
              prevFormValues.city,
            state: response.data.state ||
              prevFormValues.state,
            zip: response.data.zip ||
              prevFormValues.zip,
            contactpermission: response.data.contactpermission ||
              prevFormValues.contactpermission,
            office:
              response.data.additionalActivateFields.office ||
              prevFormValues.office,
            intranet_id:
              response.data.additionalActivateFields.intranet_id ||
              prevFormValues.intranet_id,
            subscription_level:
              response.data.additionalActivateFields.subscription_level ||
              prevFormValues.subscription_level,
            admin_note:
              response.data.additionalActivateFields.admin_note ||
              prevFormValues.admin_note,
            sponsor_associate:
              response.data.additionalActivateFields.sponsor_associate ||
              prevFormValues.sponsor_associate,
            staff_recruiter:
              response.data.additionalActivateFields.staff_recruiter ||
              prevFormValues.staff_recruiter,
            nrds_id:
              response.data.additionalActivateFields.nrds_id ||
              prevFormValues.nrds_id,
            associate_member:
              response.data.additionalActivateFields.associate_member ||
              prevFormValues.associate_member,
            plan_date:
              response.data.additionalActivateFields.plan_date ||
              prevFormValues.plan_date,
            billing_date:
              response.data.additionalActivateFields.billing_date ||
              prevFormValues.billing_date,
            account_name:
              response.data.additionalActivateFields.account_name ||
              prevFormValues.account_name,
            invoicing:
              response.data.additionalActivateFields.invoicing ||
              prevFormValues.invoicing,
            licensed_since:
              response.data.additionalActivateFields.licensed_since ||
              prevFormValues.licensed_since,
            associate_since:
              response.data.additionalActivateFields.associate_since ||
              prevFormValues.associate_since,
            staff_since:
              response.data.additionalActivateFields.staff_since ||
              prevFormValues.staff_since,
            iscorporation:
              response.data.additionalActivateFields.iscorporation ||
              prevFormValues.iscorporation,
            agentTax_ein:
              response.data.additionalActivateFields.agentTax_ein ||
              prevFormValues.agentTax_ein,
            agent_funding:
              response.data.additionalActivateFields.agent_funding ||
              prevFormValues.agent_funding,
            ssn_itin:
              response.data.additionalActivateFields.ssn_itin ||
              prevFormValues.ssn_itin,
            date_birth:
              response.data.additionalActivateFields.date_birth ||
              prevFormValues.date_birth,
            private_idcard_type:
              response.data.additionalActivateFields.private_idcard_type ||
              prevFormValues.private_idcard_type,
            private_idstate:
              response.data.additionalActivateFields.private_idstate ||
              prevFormValues.private_idstate,
            private_idnumber:
              response.data.additionalActivateFields.private_idnumber ||
              prevFormValues.private_idnumber,
            license_state:
              response.data.additionalActivateFields.license_state ||
              prevFormValues.license_state,
            license_type:
              response.data.additionalActivateFields.license_type ||
              prevFormValues.license_type,
            license_number:
              response.data.additionalActivateFields.license_number ||
              prevFormValues.license_number,
            date_issued:
              response.data.additionalActivateFields.date_issued ||
              prevFormValues.date_issued,
            date_expire:
              response.data.additionalActivateFields.date_expire ||
              prevFormValues.date_expire,
            mls_membership:
              response.data.additionalActivateFields.mls_membership ||
              prevFormValues.mls_membership,
          }));

          // const boardMembershipData = response.data.additionalActivateFields && response.data.additionalActivateFields?.board_membership
          //     ? JSON.parse(response.data.additionalActivateFields.board_membership)
          //     : [];
          //     const boardMembershipValues = boardMembershipData.map(item => item.value);
          //     setCommaSeparatedValues(boardMembershipValues.join(', '))

          // const mls_membershipData = response.data.additionalActivateFields && response.data.additionalActivateFields?.mls_membership
          // ? JSON.parse(response.data.additionalActivateFields.mls_membership)
          // : [];
          // const mls_membershipValues = mls_membershipData.map(item => item.value);
          // setCommaSeparatedValuesmls_membership(mls_membershipValues.join(', '))

          const board_membershipString =
            response.data.additionalActivateFields?.board_membership;
          if (board_membershipString) {
            // Check if board_membershipString is a string
            if (typeof board_membershipString === "string") {
              try {
                // Attempt to parse the string as JSON
                const board_membershipParsed = JSON.parse(
                  board_membershipString
                );

                if (board_membershipParsed == "") {
                  // console.log(board_membership);
                  setBoard_membership(board_membership);
                } else {
                  // console.log(board_membershipParsed);
                  setBoard_membership(board_membershipParsed);
                }
                // Set the parsed value to your state or variable
                if (board_membershipParsed == "") {
                  setCommaSeparatedValues("");
                } else {
                  const boardMembershipValues = board_membershipParsed.map(
                    (item) => item.value
                  );
                  setCommaSeparatedValues(boardMembershipValues.join(", "));
                }
              } catch (error) {
                // Handle the error if parsing fails
                console.error("Error parsing board_membership as JSON:", error);
                // You might want to set a default value or handle the error in another way
                // For debugging, you can log the JSON parse error message:
                console.error("JSON Parse Error:", error.message);
                // Set a default value if parsing fails
                setCommaSeparatedValues(""); // Replace "defaultValue" with your default value
              }
            } else {
              //console.log("sdfdsfdsfsd");
              // Handle the case where board_membershipString is not a string (e.g., it's already an object)
              // You can directly set it to your state or variable
              setCommaSeparatedValues("");
            }
          } else {
            // Handle the case where board_membershipString is undefined or null
            // Set a default value or handle it as needed
            setCommaSeparatedValues(""); // Replace "defaultValue" with your default value
          }

          const mls_membershipString =
            response.data.additionalActivateFields?.mls_membership;
          if (mls_membershipString) {
            // Check if mls_membershipString is a string
            if (typeof mls_membershipString === "string") {
              try {
                // Attempt to parse the string as JSON
                const mls_membershipParsed = JSON.parse(mls_membershipString);
                // Set the parsed value to your state or variable
                if (mls_membershipParsed == "") {
                  setMls_membership(mls_membership);
                } else {
                  setMls_membership(mls_membershipParsed);
                }
                if (mls_membershipParsed == "") {
                  setCommaSeparatedValuesmls_membership("");
                } else {
                  const boardMembershipValues = mls_membershipParsed.map(
                    (item) => item.value
                  );
                  console.log(boardMembershipValues);
                  setCommaSeparatedValuesmls_membership(
                    boardMembershipValues.join(", ")
                  );
                }
              } catch (error) {
                // Handle the error if parsing fails
                console.error("Error parsing mls_membership as JSON:", error);
                // You might want to set a default value or handle the error in another way
                // For debugging, you can log the JSON parse error message:
                console.error("JSON Parse Error:", error.message);
                // Set a default value if parsing fails
                setCommaSeparatedValuesmls_membership(""); // Replace "defaultValue" with your default value
              }
            } else {
              //console.log("sdfdsfdsfsd");
              // Handle the case where mls_membershipString is not a string (e.g., it's already an object)
              // You can directly set it to your state or variable
              setCommaSeparatedValuesmls_membership("");
            }
          } else {
            // Handle the case where mls_membershipString is undefined or null
            // Set a default value or handle it as needed
            setCommaSeparatedValuesmls_membership(""); // Replace "defaultValue" with your default value
          }
        }
      }
    });
  };

  const getTimeDifference = (createdDate) => {
    const currentDate = new Date();
    const diffInMilliseconds = currentDate - new Date(createdDate);
    const diffInMinutes = Math.floor(diffInMilliseconds / 60000);
    const hours = Math.floor(diffInMinutes / 60);
    const days = Math.floor(hours / 24);
    const months = Math.floor(days / 30);
    const years = Math.floor(months / 12);

    if (years > 0) {
      return `${years} year${years > 1 ? "s" : ""} ago`;
    } else if (months > 0) {
      return `${months} month${months > 1 ? "s" : ""} ago`;
    } else if (days > 0) {
      return `${days} day${days > 1 ? "s" : ""} ago`;
    } else if (hours > 24) {
      const remainingHours = hours - Math.floor(hours / 24) * 24;
      return `1 day, ${remainingHours} hr${remainingHours > 1 ? "s" : ""} ago`;
    } else {
      return `${hours} hr${hours > 1 ? "s" : ""}, ${diffInMinutes % 60} min${diffInMinutes % 60 > 1 ? "s" : ""
        } ago`;
    }
  };

  function getFormattedDateFormat(dateString) {
    const options = { year: "numeric", month: "2-digit", day: "2-digit" };
    const date = new Date(dateString);
    return date.toLocaleDateString("en-US", options);
  }

  const handleChange = (e) => {
    const { name, value } = e.target;
    setAccountData({ ...accountData, [name]: value });
  };

  const handleNewContact = (e) => {
    const { name, value } = e.target;
    setFormValues({ ...formValues, [name]: value });
    if (e.target.name === "staff_recruiter") {
      SearchGetAll(e.target.value);
    }
  };

  const handleChangesAbout = (e) => {
    const { name, value } = e.target;
    setFormValuesAbout({ ...formValuesAboout, [name]: value });
  };
  // const handleChanges = (e) => {
  //   const { name, value } = e.target;
  //   setAdditionalActivateFields({ ...additionalActivateFields, [name]: value });
  // };

  // const handleChangesAccount = (e) => {
  //   const { name, value } = e.target;
  //   setFormValues({ ...formValues, [name]: value });
  // };

  // const CheckBox = (e) => {
  //   setCheckAll(e.target.value);
  // };

  useEffect(() => {
    if (accountData && isSubmitClick) {
      validate();
    }
  }, [accountData, isSubmitClick]);

  const validate = () => {
    const values = accountData;
    const errors = {};

    if (values.passwordHash) {
      if (values.passwordHash.length < 8 || values.passwordHash.length > 30) {
        errors.passwordHash =
          "Password length must be between 8 and 30 characters.";
      }

      // Check for strong password criteria
      const strongPasswordRegex = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,}$/;
      if (!strongPasswordRegex.test(values.passwordHash)) {
        errors.passwordHash =
          "Password must contain at least one lowercase letter, one uppercase letter, one digit, and one special character.";
      }
    } else {
      errors.passwordHash = "passwordHash is required";
    }

    if (values.confirmPassword !== values.passwordHash) {
      errors.confirmPassword = "Confirm password must match the password.";
    }

    if (values.confirmPassword) {
      if (
        values.confirmPassword.length < 8 ||
        values.confirmPassword.length > 30
      ) {
        errors.confirmPassword =
          "ConfirmPassword length must be between 8 and 30 characters.";
      }
    } else {
      errors.confirmPassword = "ConfirmPassword is required";
    }

    setFormErrors(errors);
    return errors;
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
    setISSubmitClick(true);
    let checkValue = validate();

    if (_.isEmpty(checkValue)) {
      const userData = {
        prefix: accountData.prefix,
        _id: accountData._id,
        firstName: accountData.firstName,
        lastName: accountData.lastName,
        intranet_id: additionalActivateFields.intranet_id,
        password: accountData.passwordHash,
      };

      setLoader({ isActive: true });
      const response = await user_service.resetpassword(userData);

      if (response && response.data) {
        setCategory(response.data);
        setStep1("");
        setLoader({ isActive: false });

        setToaster({
          type: "success",
          isShow: true,
          // toasterBody: response.data.message,
          message: "Password Setup Successfully.",
        });
        setTimeout(() => {
          setToaster((prevToaster) => ({ ...prevToaster, isShow: false }));
        }, 2000);
      } else {
        setLoader({ isActive: false });
        setToaster({
          type: "error",
          isShow: true,
          toasterBody: "Error",
          message: "Something Went Wrong.",
        });
      }
    }
  };

  const handleFileUpload = async (e) => {
    const selectedFile = e.target.files[0];

    if (selectedFile) {
      setFile(selectedFile);
      setFileExtension(selectedFile.name.split(".").pop());
      setShowSubmitButton(true);

      const formData = new FormData();
      formData.append("file", selectedFile);
      const config = {
        headers: {
          Accept: "application/json",
          "Content-Type": "multipart/form-data",
          Authorization: `Bearer ${localStorage.getItem("auth")}`,
        },
      };

      try {
        setLoader({ isActive: true });
        const uploadResponse = await axios.post(
          "https://api.brokeragentbase.com/upload",
          formData,
          config
        );
        const uploadedFileData = uploadResponse.data;
        setData(uploadedFileData);
        // console.log(uploadedFileData);
        const updatedgetContact = {
          //  ...getContact,
          image: uploadedFileData,
        };

        setLoader({ isActive: true });
        await user_service
          .contactUpdate(formValuesNew?._id, updatedgetContact)
          .then((response) => {
            if (response) {
              setImageData(response.data);
              setLoader({ isActive: false });

              // const ContactGetById = async (contactId) => {
              //   await user_service
              //     .contactGetById(contactId)
              //     .then((response) => {
              //       if (response) {
              //         setGetContact(response.data);
              //         setLoader({ isActive: false });
              //         setSubmitCount(submitCount + 1);
              //         setToaster({
              //           type: "Profile Updated Successfully",
              //           isShow: true,
              //           toasterBody: response.data.message,
              //           message: "Profile Updated Successfully",
              //         });

              //         setTimeout(() => {
              //           setToaster((prevToaster) => ({
              //             ...prevToaster,
              //             isShow: false,
              //           }));
              //         }, 3000);
              //       }
              //     });
              // };
              ContactGetById(formValuesNew?._id);
              ContactGetByuniqueid2ndstep(params.uid, params.cid);
            } else {
              setLoader({ isActive: false });
              setToaster({
                type: "error",
                isShow: true,
                toasterBody: response.data.message,
                message: "Something Went Wrong.",
              });

              setTimeout(() => {
                setToaster((prevToaster) => ({
                  ...prevToaster,
                  isShow: false,
                }));
              }, 3000);
            }
          });
      } catch (error) {
        setLoader({ isActive: false });
        setToaster({
          types: "error",
          isShow: true,
          toasterBody: error.response
            ? error.response.data.message
            : error.message,
          message: "Something Went Wrong.",
        });

        setTimeout(() => {
          setToaster((prevToaster) => ({ ...prevToaster, isShow: false }));
        }, 1000);
      }
    }
  };
  const handleProgressbar = (stepno) => {
    if (stepno === "1") {
      setStep1((current) => !current);
      setPhase(false);
      setCategory(false);
    } else if (stepno === "2") {
      setCategory((current) => !current);
      setPhase(false);
      setStep1(false);
    } else if (stepno === "3") {
      setPhase((current) => !current);
      setCategory(false);
      setStep1(false);
      setStep5(false);
    } else if (stepno === "4") {
      setStep4((current) => !current);
      setCategory(false);
      setStep5(false);
      setStep1(false);
      setPhase(false);
    } else if (stepno === "5") {
      setStep5((current) => !current);
      setCategory(false);
      setStep4(false);
      setStep1(false);
      setPhase(false);
    }
  };

  const handleSkip = async (e) => {
    //  setPhase(true);
    setStep1("");
    setCategory("");
  };
  const handleSkipNew = async (e) => {
    setStep4(true);
    setPhase("");
    setStep1("");
    setCategory("");
  };

  useEffect(() => {
    if (formValues && isSubmitClickNew) {
      validateNew();
    }
  }, []);

  const validateNew = () => {
    const values = formValues;
    const errors = {};
    //console.log(formValues)
    if (!values.date_birth) {
      errors.date_birth = "Date is required.";
    }

    if (!formValuesNew.image) {
      errors.image = "Profile picture is required.";
    }

    if (!values.license_type) {
      errors.license_type = "License type is required.";
    }

    if (!values.license_state) {
      errors.license_state = "License State is required.";
    }
    if (!values.license_number) {
      errors.license_number = "License Number is required.";
    }

    if (!values.date_expire) {
      errors.date_expire = "license Expire is required.";
    }
    if (!values.admin_note) {
      errors.admin_note = "  Transfer From is required.";
    }


    if (formValuesNew.contactpermission === "referralonly") {

    } else {
      if (!board_membership || board_membership.length === 0) {
        errors.board_membership = "Board Membership is required.";
      } else if (typeof board_membership === 'string') {
        try {
          const mlsMembershipArray = JSON.parse(board_membership);

          if (!Array.isArray(mlsMembershipArray) || mlsMembershipArray.length === 0) {
            errors.board_membership = "Board Membership is required.";
          }
        } catch (error) {
          errors.board_membership = "Invalid Board Membership data.";
          console.error("Error parsing board_membership:", error);
        }
      }
      // if (!board_membership || board_membership.length === 0 || JSON.parse(mls_membership).length === 0) {
      //   errors.board_membership = "Board Membership is required.";
      // }

      // if (!mls_membership || mls_membership.length === 0 || JSON.parse(mls_membership).length === 0) {
      //   errors.mls_membership = "MLS Membership is required.";
      // }


      if (!mls_membership || mls_membership.length === 0) {
        errors.mls_membership = "MLS Membership is required.";
      } else if (typeof mls_membership === 'string') {
        console.log("sczxzc")
        try {
          const mlsMembershipArray = JSON.parse(mls_membership);

          if (!Array.isArray(mlsMembershipArray) || mlsMembershipArray.length === 0) {
            errors.mls_membership = "MLS Membership is required.";
          }
        } catch (error) {
          errors.mls_membership = "Invalid MLS Membership data.";
          console.error("Error parsing mls_membership:", error);
        }
      }

    }




    if (!values.private_idcard_type) {
      errors.private_idcard_type = "ID Type is required.";
    }

    if (!values.private_idstate && values.private_idcard_type != "Passport") {
      errors.private_idstate = "Id State is required.";
    }

    if (!values.private_idnumber) {
      errors.private_idnumber = "ID Number is required.";
    }

    if (!values.address) {
      errors.address = "Address is required.";
    }

    if (!values.city) {
      errors.city = "City is required.";
    }

    if (!values.state) {
      errors.state = "State is required.";
    }

    if (!values.zip) {
      errors.zip = "zipcode is required.";
    }

    setFormErrors(errors);


    // Scroll to the first error element
    const firstErrorKey = Object.keys(errors)[0];
    if (firstErrorKey) {
      const firstErrorElement = document.querySelector(`[name="${firstErrorKey}"]`);
      if (firstErrorElement) {
        // Scroll to the top of the error element
        window.scrollTo({
          top: firstErrorElement.offsetTop,
          behavior: "smooth" // You can use "auto" instead of "smooth" for an instant scroll
        });
      }
    }
    
    return errors;
  };

  const handleAccount = async (e) => {
    setISSubmitClickNew(true);
    let checkValue = validateNew();

    if (_.isEmpty(checkValue)) {
      e.preventDefault();
      const additionalActivateFields = {
        active_office: formValuesNew?.active_office,
        active_office_id: formValuesNew?.active_office_id,
        intranet_confirmationcode:
          formValuesNew?.additionalActivateFields?.intranet_confirmationcode,
        intranet_uniqueid:
          formValuesNew?.additionalActivateFields?.intranet_uniqueid,
        office: formValues.office ?? "",
        intranet_id: formValues?.additionalActivateFields?.intranet_id ?? "",
        subscription_level: formValues.subscription_level ?? "",
        admin_note: formValues.admin_note ?? "",
        sponsor_associate: formValues.sponsor_associate ?? "",
        staff_recruiter: formValues.staff_recruiter ?? "",
        nrds_id: formValues.nrds_id ?? "",
        associate_member: formValues.associate_member ?? "",
        plan_date: formValues.plan_date ?? "",
        billing_date: formValues.billing_date ?? "",
        account_name: formValues.account_name ?? "",
        invoicing: formValues.invoicing ?? "",
        licensed_since: formValues.licensed_since ?? "",
        associate_since: formValues.associate_since ?? "",
        staff_since: formValues.staff_since ?? "",
        iscorporation: "",
        agentTax_ein: formValues.agentTax_ein ?? "",
        agent_funding: formValues.agent_funding ?? "",
        ssn_itin: formValues.ssn_itin ?? "",
        date_birth: formValues.date_birth ?? "",
        private_idcard_type: formValues.private_idcard_type ?? "",
        private_idstate: formValues.private_idstate ?? "",
        private_idnumber: formValues.private_idnumber ?? "",
        license_state: formValues.license_state ?? "",
        license_type: formValues.license_type ?? "",
        license_number: formValues.license_number ?? "",
        date_issued: formValues.date_issued ?? "",
        date_expire: formValues.date_expire ?? "",
        mls_membership: JSON.stringify(mls_membership),
        board_membership: JSON.stringify(board_membership),
        agent_id: formValues.agent_id ?? "",
        parkcityagent_id: formValues.parkcityagent_id ?? "",
        washingtonagent_id: formValues.washingtonagent_id ?? "",
        ironcountyagent_id: formValues.ironcountyagent_id ?? "",
        activatecontact: "yes",
      };

      const userData = {
        agentId: agentId,
        birthday: formValues.date_birth ?? "",
        address: formValues.address ?? "",
        city: formValues.city ?? "",
        state: formValues.state ?? "",
        zip: formValues.zip ?? "",
        birthday: formValues.date_birth ?? "",
        additionalActivateFields,
      };

      try {
        setLoader({ isActive: true });
        await user_service
          .contactUpdate(formValuesNew?._id, userData)
          .then((response) => {
            if (response) {
              // setPhase(response.data); 
              setStep1("");
              setStep4(response.data);
              setCategory("");
              setLoader({ isActive: false });
              ContactGetById(formValuesNew?._id);
              ContactGetByuniqueid2ndstep(params.uid, params.cid);
              setToaster({
                type: "Profile Updated",
                isShow: true,
                toasterBody: response.data.message,
                message: "Profile Updated Successfully",
              });
              setTimeout(() => {
                setToaster((prevToaster) => ({
                  ...prevToaster,
                  isShow: false,
                }));
              }, 3000);
            } else {
              setLoader({ isActive: false });
              setToaster({
                types: "error",
                isShow: true,
                toasterBody: response.data.message,
                message: "Something Went Wrong.",
              });
            }
          });
      } catch (error) {
        setLoader({ isActive: false });
        setToaster({
          types: "error",
          isShow: true,
          toasterBody: error.response
            ? error.response.data.message
            : error.message,
          message: "Something Went Wrong.",
        });
        setTimeout(() => {
          setToaster((prevToaster) => ({ ...prevToaster, isShow: false }));
        }, 3000);
      }
    }
  };

  const handleSubmitAbout = async (e) => {
    if (formValuesAboout && formValuesAboout._id) {
      e.preventDefault();
      try {
        const userData = {
          agentId: formValuesNew?._id,
          full_name: formValuesAboout.full_name,
          birthday: formValuesAboout.birthday ?? "",
          spouse: formValuesAboout.spouse,
          spend_birthday: formValuesAboout.spend_birthday,
          shirt_size: formValuesAboout.shirt_size,
          spend_day: formValuesAboout.spend_day,
          professional_skill: formValuesAboout.professional_skill,
          hobbies: formValuesAboout.hobbies,
          vacation: formValuesAboout.vacation,
          travel_bucket: formValuesAboout.travel_bucket,
          food: formValuesAboout.food,
          restaurant: formValuesAboout.restaurant,
          music: formValuesAboout.music,
          book: formValuesAboout.book,
          phone_app: formValuesAboout.phone_app,
          dessert: formValuesAboout.dessert,
          anything_else: formValuesAboout.anything_else,
        };
        setLoader({ isActive: true });
        await user_service
          .aboutMeUpdate(formValuesAboout._id, userData)
          .then((response) => {
            if (response) {
              //console.log(response);
              setStep4(response.data);
              setStep1("");
              setCategory("");
              setPhase("");
              setLoader({ isActive: false });
              setToaster({
                type: "Profile Successfully",
                isShow: true,
                toasterBody: response.data.message,
                message: "Profile Updated Successfully",
              });
              setTimeout(() => {
                setToaster((prevToaster) => ({
                  ...prevToaster,
                  isShow: false,
                }));
              }, 2000);
            } else {
              setLoader({ isActive: false });
              setToaster({
                types: "error",
                isShow: true,
                toasterBody: response.data.message,
                message: "Something Went Wrong.",
              });
              setTimeout(() => {
                setToaster((prevToaster) => ({
                  ...prevToaster,
                  isShow: false,
                }));
              }, 2000);
            }
          });
      } catch (error) {
        setLoader({ isActive: false });
        setToaster({
          types: "error",
          isShow: true,
          toasterBody: error.response
            ? error.response.data.message
            : error.message,
          message: "Something Went Wrong.",
        });

        setTimeout(() => {
          setToaster((prevToaster) => ({ ...prevToaster, isShow: false }));
        }, 2000);
      }
    } else {
      e.preventDefault();
      try {
        const userData = {
          agentId: formValuesNew?._id,
          full_name: formValuesAboout.full_name,
          birthday: formValuesAboout.birthday,
          spouse: formValuesAboout.spouse,
          spend_birthday: formValuesAboout.spend_birthday,
          shirt_size: formValuesAboout.shirt_size,
          spend_day: formValuesAboout.spend_day,
          professional_skill: formValuesAboout.professional_skill,
          hobbies: formValuesAboout.hobbies,
          vacation: formValuesAboout.vacation,
          travel_bucket: formValuesAboout.travel_bucket,
          food: formValuesAboout.food,
          restaurant: formValuesAboout.restaurant,
          music: formValuesAboout.music,
          book: formValuesAboout.book,
          phone_app: formValuesAboout.phone_app,
          dessert: formValuesAboout.dessert,
          anything_else: formValuesAboout.anything_else,
        };
        //  console.log(userData);
        setLoader({ isActive: true });
        await user_service.aboutMePost(userData).then((response) => {
          if (response) {
            setStep4(response.data);
            setStep1("");
            setCategory("");
            setPhase("");
            setLoader({ isActive: false });
            setToaster({
              type: "Profile Updated Successfully",
              isShow: true,
              toasterBody: response.data.message,
              message: "Profile Updated Successfully",
            });
            setTimeout(() => {
              setToaster((prevToaster) => ({
                ...prevToaster,
                isShow: false,
              }));
            }, 2000);
          } else {
            setLoader({ isActive: false });
            setToaster({
              types: "error",
              isShow: true,
              toasterBody: response.data.message,
              message: "Something Went Wrong.",
            });
            setTimeout(() => {
              setToaster((prevToaster) => ({ ...prevToaster, isShow: false }));
            }, 1000);
          }
        });
      } catch (error) {
        setLoader({ isActive: false });
        setToaster({
          types: "error",
          isShow: true,
          toasterBody: error.response
            ? error.response.data.message
            : error.message,
          message: "Something Went Wrong.",
        });

        setTimeout(() => {
          setToaster((prevToaster) => ({ ...prevToaster, isShow: false }));
        }, 1000);
      }
    }
  };



  const SearchGetAll = async () => {
    setIsLoading(true);

    await user_service
      .SearchContactGet(0, 3, formValues.staff_recruiter)
      .then((response) => {
        setIsLoading(false);
        if (response) {
          console.log(response);
          setResults(response.data.data);
        }
      });
  };

  const handleSearchTransaction = async (conatactdata) => {
    setClientId(conatactdata._id);
    setConatctdata(conatactdata);
    setFormValues({
      ...formValues,
      staff_recruiter: conatactdata.firstName + " " + conatactdata.lastName,
      staff_recruiter_id: conatactdata._id,
    });
    setResults("");
  };


  const MlsBoardDocumentGetAll = async (
    MlsMembershipValues,
    boardMembershipValues
  ) => {
    console.log(MlsMembershipValues)
    console.log(boardMembershipValues)
    if (MlsMembershipValues[0] === "Park City" || boardMembershipValues[0] === "Park City") {
      setOnBoardparkcity(true);
    } else {
      await user_service
        .mlsBoardDocumentGetId(MlsMembershipValues, boardMembershipValues)
        .then((response) => {
          setLoader({ isActive: false });
          if (response) {
            if (response.data.data[0]?.image) {
              generateHtmlForm1("https://brokeragentbase.s3.amazonaws.com/1702881544800-1700758888303-Utah%20Real%20Estate%20Membership%20Change%20Form%20%282%29.pdf");
              //generateHtmlForm1("https://brokeragentbase.s3.amazonaws.com/assets/Salt+Lake+Board+Membership+Form.pdf");
            } else {
              readyPad();
            }
            setOnBoard(response.data.data);
            console.log(response.data.data)
          }
        });
    }
  };



  const handleLoadSuccess = ({ numPages }) => {
    setNumPages(numPages);
  };

  const handleClearSignature = () => {
    signatureCanvasRef.current.clear();
  };



  const handleDrag = (e, data) => {
    if (!isDragging) {
      setIsDragging(true);
    }
    setPosition({ x: data.x, y: data.y });
  };

  const handleStopDrag = () => {
    setIsDragging(false);
  };

  const handleAddTexterm = async () => {
    // setIsShow(true);
    setStep4(false);
    setStep1("");
    setCategory("");
    setPhase("");
    setLoader({ isActive: false });
    setStep5(true);
  };

  const handleNewSignPdf = async () => {
    setSubmitNewPdf(true)
    setPdfSrcNew(false);
  }

  const handleAddSignatureToPdf = async () => {
    setIsShow(true);
    if (onBoard[0]?.image) {
      const signatureData = signatures[pageNumber];
      if (signatureData) {
        try {
          // Load the existing PDF
          const existingPdfUrl = onBoard[0]?.image;
          const existingPdfBytes = await fetch(existingPdfUrl).then((res) => res.arrayBuffer());
          const existingPdfDoc = await PDFDocument.load(existingPdfBytes);

          const signatureBytes = await fetch(signatureData).then((res) => res.arrayBuffer());
          const signatureImage = await existingPdfDoc.embedPng(signatureBytes);

          // Loop through all pages and add the signature
          for (let i = 0; i < existingPdfDoc.getPageCount(); i++) {
            const existingPdfPage = existingPdfDoc.getPage(i);

            const { width: existingPageWidth, height: existingPageHeight } = existingPdfPage.getSize();
            const imageWidth = signatureImage.width;
            const imageHeight = signatureImage.height;

            // Calculate position for the bottom right corner
            const xPosition = existingPageWidth - imageWidth - 2;
            const yPosition = 2;

            existingPdfPage.drawImage(signatureImage, {
              x: xPosition,
              y: yPosition,
              width: imageWidth,
              height: imageHeight,
            });

            // Add text to the bottom right corner
            const fontSize = 1; // Adjust the font size as needed
            existingPdfPage.drawText("", {
              x: xPosition + imageWidth - 10,
              y: yPosition + imageHeight - 5,
              size: fontSize,
              color: rgb(0, 0, 0),
            });
          }

          // Save the modified PDF
          const modifiedPdfBytes = await existingPdfDoc.save();

          // Convert the modified PDF to a Blob
          const modifiedPdfBlob = new Blob([modifiedPdfBytes], {
            type: "application/pdf",
          });

          // Create FormData for file upload
          const pdfFormData = new FormData();
          pdfFormData.append("file", modifiedPdfBlob, "generated.pdf");

          const apiUrl = "https://api.brokeragentbase.com/upload";
          const config = {
            headers: {
              "Content-Type": "multipart/form-data",
            },
          };

          // Use axios to post the FormData
          const response = await axios.post(apiUrl, pdfFormData, config);
          setPdfPath(response.data);

          const userData = {
            onboard_signature: response.data,
            onboardfinal: "done",
          };
          console.log(userData);

          try {
            setLoader({ isActive: true });
            await user_service
              .contactUpdate(formValuesNew?._id, userData)
              .then((response) => {
                if (response.data) {
                  setPdfSrcNew(true);
                  // setStep5(false);
                  // setStep4(false);
                  // setStep1("");
                  // setCategory("");
                  // setPhase("");
                  setLoader({ isActive: false });
                  ContactGetById(formValuesNew?._id);
                  setToaster({
                    type: "Document",
                    isShow: true,
                    toasterBody: response.data.message,
                    message: "Document signed successfully",
                  });
                  setTimeout(() => {
                    setToaster((prevToaster) => ({
                      ...prevToaster,
                      isShow: false,
                    }));
                    setIsShow(false);
                    //  navigate("/onboard-thankyou");
                  }, 2000);
                } else {
                  setLoader({ isActive: false });
                  setToaster({
                    types: "error",
                    isShow: true,
                    toasterBody: response.data.message,
                    message: "Something Went Wrong.",
                  });
                }
              });
          } catch (error) {
            setLoader({ isActive: false });
            setToaster({
              types: "error",
              isShow: true,
              toasterBody: error.response
                ? error.response.data.message
                : error.message,
              message: "Something Went Wrong.",
            });
            setTimeout(() => {
              setToaster((prevToaster) => ({ ...prevToaster, isShow: false }));
            }, 2000);
          }
          // Set the modified PDF URL to state or use it as needed
          // setConfirmedSignatures((prevConfirmedSignatures) => [
          //   ...prevConfirmedSignatures,
          //   { pageNumber, signatureData, pdfBase64: modifiedPdfUrl },
          // ]);
        } catch (error) {
          console.error("Error adding signature to PDF:", error);
        }
      } else {
        setToaster({
          types: "error",
          isShow: true,
          toasterBody: "Invalid Signature",
          message: "Please add a signature before confirming.",
        });
      }
    } else {
      if (signaturePad) {
        //e.preventDefault();
        const dataURL = signaturePad.toDataURL();
        const signatureBlob = dataURLToBlob(dataURL);

        // Create a FormData object to send the Blob
        const formData = new FormData();
        formData.append("file", signatureBlob);

        const config = {
          headers: {
            Accept: "application/json",
            "Content-Type": "multipart/form-data",
            Authorization: `Bearer ${localStorage.getItem("auth")}`,
          },
        };

        try {
          const uploadResponse = await axios.post(
            "https://api.brokeragentbase.com/upload",
            formData,
            config
          );
          const uploadedFileData = uploadResponse.data;
          console.log(uploadedFileData);
          //  setSavedSignature(uploadedFileData);
          const userData = {
            onboard_signature: uploadedFileData,
            onboardfinal: "done",
          };
          console.log(userData);

          try {
            setLoader({ isActive: true });
            await user_service
              .contactUpdate(formValuesNew?._id, userData)
              .then((response) => {
                if (response) {
                  //  setStep5(false);
                  //  setStep4(false);
                  // setStep1("");
                  // setCategory("");
                  // setPhase("");
                  setLoader({ isActive: false });
                  ContactGetById(formValuesNew?._id);
                  setToaster({
                    type: "Document",
                    isShow: true,
                    toasterBody: response.data.message,
                    message: "Document signed successfully",
                  });
                  setTimeout(() => {
                    setToaster((prevToaster) => ({
                      ...prevToaster,
                      isShow: false,
                    }));
                    setIsShow(false);
                    navigate("/onboard-thankyou");
                  }, 2000);
                } else {
                  setLoader({ isActive: false });
                  setToaster({
                    types: "error",
                    isShow: true,
                    toasterBody: response.data.message,
                    message: "Something Went Wrong.",
                  });
                }
              });
          } catch (error) {
            setLoader({ isActive: false });
            setToaster({
              types: "error",
              isShow: true,
              toasterBody: error.response
                ? error.response.data.message
                : error.message,
              message: "Something Went Wrong.",
            });
            setTimeout(() => {
              setToaster((prevToaster) => ({ ...prevToaster, isShow: false }));
            }, 2000);
          }
        } catch (error) {
          setLoader({ isActive: false });
          setToaster({
            types: "error",
            isShow: true,
            toasterBody: error.response
              ? error.response.data.message
              : error.message,
            message: "Something Went Wrong.",
          });
          setTimeout(() => {
            setToaster((prevToaster) => ({ ...prevToaster, isShow: false }));
          }, 2000);
        }
      } else {
        setToaster({
          types: "error",
          isShow: true,
          toasterBody: "Invalid Signature",
          message: "Please add a signature before confirming.",
        });
      }
    }
  };

  const handleNewSign = async () => {
    setIsShow(true);
    setPdfSrcNewsign(true);
    if (pdfSrcNewsign) {
      const signatureData = signatures[pageNumber];
      if (signatureData) {
        try {
          // Load the existing PDF
          const existingPdfUrl = pdfSrcNewsign;
          const existingPdfBytes = await fetch(existingPdfUrl).then((res) => res.arrayBuffer());
          const existingPdfDoc = await PDFDocument.load(existingPdfBytes);

          const signatureBytes = await fetch(signatureData).then((res) => res.arrayBuffer());
          const signatureImage = await existingPdfDoc.embedPng(signatureBytes);

          // Loop through all pages and add the signature
          for (let i = 0; i < existingPdfDoc.getPageCount(); i++) {
            const existingPdfPage = existingPdfDoc.getPage(i);

            const { width: existingPageWidth, height: existingPageHeight } = existingPdfPage.getSize();
            const imageWidth = signatureImage.width;
            const imageHeight = signatureImage.height;

            // Calculate position for the bottom right corner
            const xPosition = existingPageWidth - imageWidth - 10;
            const yPosition = 10;

            existingPdfPage.drawImage(signatureImage, {
              x: xPosition,
              y: yPosition,
              width: imageWidth,
              height: imageHeight,
            });

            // Add text with font size 5px to the bottom right corner
            existingPdfPage.drawText("", {
              x: xPosition + imageWidth - 20,
              y: yPosition + imageHeight - 5,
              fontSize: 5,
              color: rgb(0, 0, 0), // Black color
            });

          }

          // Save the modified PDF
          const modifiedPdfBytes = await existingPdfDoc.save();

          // Convert the modified PDF to a Blob
          const modifiedPdfBlob = new Blob([modifiedPdfBytes], {
            type: "application/pdf",
          });

          // Create FormData for file upload
          const pdfFormData = new FormData();
          pdfFormData.append("file", modifiedPdfBlob, "generated.pdf");

          const apiUrl = "https://api.brokeragentbase.com/upload";
          const config = {
            headers: {
              "Content-Type": "multipart/form-data",
            },
          };


          // Use axios to post the FormData
          const response = await axios.post(apiUrl, pdfFormData, config);
          setPdfPath(response.data);

          const userData = {
            onboard_signature: response.data,
            onboardfinal: "done",
          };
          console.log(userData);

          try {
            setLoader({ isActive: true });
            await user_service
              .contactUpdate(formValuesNew?._id, userData)
              .then((response) => {
                if (response.data) {
                  setStep5(false);
                  setStep4(false);
                  setStep1("");
                  setCategory("");
                  setPhase("");
                  setLoader({ isActive: false });
                  ContactGetById(formValuesNew?._id);
                  setToaster({
                    type: "Document",
                    isShow: true,
                    toasterBody: response.data.message,
                    message: "Document signed successfully",
                  });
                  setTimeout(() => {
                    setToaster((prevToaster) => ({
                      ...prevToaster,
                      isShow: false,
                    }));
                    setIsShow(false);
                    navigate("/onboard-thankyou");
                  }, 2000);
                } else {
                  setLoader({ isActive: false });
                  setToaster({
                    types: "error",
                    isShow: true,
                    toasterBody: response.data.message,
                    message: "Something Went Wrong.",
                  });
                }
              });
          } catch (error) {
            setLoader({ isActive: false });
            setToaster({
              types: "error",
              isShow: true,
              toasterBody: error.response
                ? error.response.data.message
                : error.message,
              message: "Something Went Wrong.",
            });
            setTimeout(() => {
              setToaster((prevToaster) => ({ ...prevToaster, isShow: false }));
            }, 2000);
          }
          // Set the modified PDF URL to state or use it as needed
          // setConfirmedSignatures((prevConfirmedSignatures) => [
          //   ...prevConfirmedSignatures,
          //   { pageNumber, signatureData, pdfBase64: modifiedPdfUrl },
          // ]);
        } catch (error) {
          console.error("Error adding signature to PDF:", error);
        }
      } else {
        setToaster({
          types: "error",
          isShow: true,
          toasterBody: "Invalid Signature",
          message: "Please add a signature before confirming.",
        });
      }
    } else {
      if (signaturePad) {
        //e.preventDefault();
        const dataURL = signaturePad.toDataURL();
        const signatureBlob = dataURLToBlob(dataURL);

        // Create a FormData object to send the Blob
        const formData = new FormData();
        formData.append("file", signatureBlob);

        const config = {
          headers: {
            Accept: "application/json",
            "Content-Type": "multipart/form-data",
            Authorization: `Bearer ${localStorage.getItem("auth")}`,
          },
        };

        try {
          const uploadResponse = await axios.post(
            "https://api.brokeragentbase.com/upload",
            formData,
            config
          );
          const uploadedFileData = uploadResponse.data;
          console.log(uploadedFileData);
          //  setSavedSignature(uploadedFileData);
          const userData = {
            onboard_signature: uploadedFileData,
            onboardfinal: "done",
          };
          console.log(userData);

          try {
            setLoader({ isActive: true });
            await user_service
              .contactUpdate(formValuesNew?._id, userData)
              .then((response) => {
                if (response) {
                  setStep5(false);
                  setStep4(false);
                  setStep1("");
                  setCategory("");
                  setPhase("");
                  setLoader({ isActive: false });
                  ContactGetById(formValuesNew?._id);
                  setToaster({
                    type: "Document",
                    isShow: true,
                    toasterBody: response.data.message,
                    message: "Document signed successfully",
                  });
                  setTimeout(() => {
                    setToaster((prevToaster) => ({
                      ...prevToaster,
                      isShow: false,
                    }));
                    setIsShow(false);
                    navigate("/onboard-thankyou");
                  }, 2000);
                } else {
                  setLoader({ isActive: false });
                  setToaster({
                    types: "error",
                    isShow: true,
                    toasterBody: response.data.message,
                    message: "Something Went Wrong.",
                  });
                }
              });
          } catch (error) {
            setLoader({ isActive: false });
            setToaster({
              types: "error",
              isShow: true,
              toasterBody: error.response
                ? error.response.data.message
                : error.message,
              message: "Something Went Wrong.",
            });
            setTimeout(() => {
              setToaster((prevToaster) => ({ ...prevToaster, isShow: false }));
            }, 2000);
          }
        } catch (error) {
          setLoader({ isActive: false });
          setToaster({
            types: "error",
            isShow: true,
            toasterBody: error.response
              ? error.response.data.message
              : error.message,
            message: "Something Went Wrong.",
          });
          setTimeout(() => {
            setToaster((prevToaster) => ({ ...prevToaster, isShow: false }));
          }, 2000);
        }
      } else {
        setToaster({
          types: "error",
          isShow: true,
          toasterBody: "Invalid Signature",
          message: "Please add a signature before confirming.",
        });
      }
    }
  }


  // const handleSaveSignature = () => {
  //   const signatureData = signatureCanvasRef.current.toDataURL();
  //   console.log(signatureData);
  //   setSignatures((prevSignatures) => ({
  //     ...prevSignatures,
  //     [pageNumber]: signatureData,
  //   }));
  // };

  const handleSaveSignature = () => {
    const isCanvasEmpty = signatureCanvasRef.current.isEmpty();
    if (!isCanvasEmpty) {
      const signatureData = signatureCanvasRef.current.toDataURL();
      console.log(signatureData);
      setSignatures((prevSignatures) => ({
        ...prevSignatures,
        [pageNumber]: signatureData,
      }));
    } else {
      alert("Please sign before saving.");
    }
  };

  const handleSaveSignatureNew = () => {
    const signatureData = signatureCanvasRefNew.current.toDataURL();
    console.log(signatureData);
    setSignatures((prevSignatures) => ({
      ...prevSignatures,
      [pageNumber]: signatureData,
    }));
  };


  const getFieldDataFromSomeSource = (fieldName) => {
    return fieldValues[fieldName] || "";
  };

  const handleFieldChange = (fieldName, value) => {
    setFieldValues((prevFieldValues) => ({
      ...prevFieldValues,
      [fieldName]: value,
    }));


    // if(fieldName == "topmostSubform[0].Page1[0].f1_1[0]" || fieldName == "topmostSubform[0].Page1[0].f1_2[0]"){
    //     handleFieldpreview();
    // }
  };


  const [w9modifiedPdfBlob, setW9modifiedPdfBlob] = useState("");
  const handleFieldpreview = async () => {

    const response = await fetch("https://brokeragentbase.s3.amazonaws.com/assets/IRS+W9.pdf");
    if (!response.ok) {
      throw new Error("Failed to fetch PDF");
    }

    const existingPdfBytes = await response.arrayBuffer();
    const existingPdfDoc = await PDFDocument.load(existingPdfBytes);


    const form = existingPdfDoc.getForm();
    const fieldNames = form.getFields().map((field) => field.getName());

    console.log("Form field names:", fieldNames);

    for (const fieldName of fieldNames) {
      const field = form.getField(fieldName);

      if (field) {
        if (field instanceof PDFTextField) {
          const textField = form.getTextField(fieldName);
          const fieldValue = fieldValues[fieldName] || ""; // Get value from state

          // Attach the onInput event to capture real-time updates
          const inputElement = document.querySelector(
            `input[name="${fieldName}"]`
          );
          if (inputElement) {
            inputElement.addEventListener("input", (event) => {
              handleFieldChange(fieldName, event.target.value);
            });
          }

          textField.setText(fieldValue);
        } else if (field instanceof PDFCheckBox) {
          const checkbox = form.getCheckBox(fieldName);
          const isChecked = fieldValues[fieldName]; // Get value from state
          console.log(fieldName);
          console.log(isChecked);
          if (isChecked) {
            checkbox.check(isChecked);
          }
        } else {
          console.warn(`Unsupported field type for "${fieldName}"`);
        }
      } else {
        console.warn(`No field found with the name "${fieldName}"`);
      }
    }

    const modifiedPdfBytes = await existingPdfDoc.save();
    const modifiedPdfBlob = new Blob([modifiedPdfBytes], {
      type: "application/pdf",
    });

    // const pdfFormData = new FormData();
    // pdfFormData.append("file", modifiedPdfBlob, "modified.pdf");
    console.log("modifiedPdfBlob")
    console.log(modifiedPdfBlob)
    setW9modifiedPdfBlob(modifiedPdfBlob)

    const modifiedPdfDataUrl = URL.createObjectURL(modifiedPdfBlob);

    // Set the data URL as the source for the iframe
    const iframe = document.getElementById("pdfPreviewIframe");
    iframe.src = modifiedPdfDataUrl;
  }






  const fetchAndDisplayFields = async () => {
    try {
      const response = await fetch("https://brokeragentbase.s3.amazonaws.com/assets/IRS+W9.pdf");
      if (!response.ok) {
        throw new Error("Failed to fetch PDF");
      }

      const existingPdfBytes = await response.arrayBuffer();
      const existingPdfDoc = await PDFDocument.load(existingPdfBytes);

      const form = existingPdfDoc.getForm();
      const names = form.getFields().map((field) => field.getName());

      setForm(form);
      setFieldNames(names);
      // console.log(form);
      // console.log(names);
    } catch (error) {
      console.error("Error fetching PDF fields:", error.message);
    }
  };

  useEffect(() => {
    fetchAndDisplayFields();
  }, [pdfSrc]);

  const renderFieldNames = () => {
    return fieldNames.map((fieldName) => {
      const fieldValue = getFieldDataFromSomeSource(fieldName);
      const field = form.getField(fieldName);

      if (field instanceof PDFCheckBox) {
        // If the field is a checkbox, render options as checkboxes
        const checkBox = field;

        return (
          <div key={fieldName}>
            {fieldName}:
            <div>
              <input
                type="checkbox"
                name={fieldName}
                checked=""
              // onChange={(e) => {
              //   const isChecked = e.target.checked;
              //   const updatedValue = isChecked ? [checkBox.getOnValue()] : [checkBox.getOffValue()];
              //   handleFieldChange(fieldName, updatedValue);
              // }}
              />
              {/* <label>{fieldName}</label> */}
            </div>
          </div>
        );
      } else {
        // For other field types, render as text input
        return (
          <div key={fieldName}>
            {fieldName}:
            <input
              type="text"
              name={fieldName}
              value={fieldValue}
              onChange={(e) => handleFieldChange(fieldName, e.target.value)}
            />
          </div>
        );
      }
    });
  };

  const validatew9 = () => {
    const values = fieldValues;
    const errors = {};
    console.log(values['topmostSubform[0].Page1[0].f1_1[0]'])

    if (!values['topmostSubform[0].Page1[0].f1_1[0]']) {
      errors['topmostSubform[0].Page1[0].f1_1[0]'] = "Name is required.";
    }

    if (!values['topmostSubform[0].Page1[0].f1_2[0]']) {
      errors['topmostSubform[0].Page1[0].f1_2[0]'] = "Business is required.";
    }

    if (!values['topmostSubform[0].Page1[0].Address[0].f1_7[0]']) {
      errors['topmostSubform[0].Page1[0].Address[0].f1_7[0]'] = "Address is required.";
    }

    if (!values['topmostSubform[0].Page1[0].Address[0].f1_8[0]']) {
      errors['topmostSubform[0].Page1[0].Address[0].f1_8[0]'] = "City And State is required.";
    }

    if ((values['topmostSubform[0].Page1[0].SSN[0].f1_11[0]'] && values['topmostSubform[0].Page1[0].SSN[0].f1_12[0]'] && values['topmostSubform[0].Page1[0].SSN[0].f1_13[0]']) || (values['topmostSubform[0].Page1[0].EmployerID[0].f1_14[0]'] && values['topmostSubform[0].Page1[0].EmployerID[0].f1_15[0]'])) {

    } else {
      errors['ssn'] = "Social Security Number or Employer identification number is required.";
    }


    // if (!values['topmostSubform[0].Page1[0].SSN[0].f1_12[0]']) {
    //   errors['topmostSubform[0].Page1[0].SSN[0].f1_12[0]'] = "Address is required.";
    // }
    // if (values['topmostSubform[0].Page1[0].SSN[0].f1_13[0]']) {
    //   errors['topmostSubform[0].Page1[0].SSN[0].f1_13[0]'] = "Social Security Number or Employer identification number is required.";
    // }

    // if (!values['topmostSubform[0].Page1[0].SSN[0].f1_14[0]']) {
    //   errors['topmostSubform[0].Page1[0].SSN[0].f1_14[0]'] = "Social Security Number is required.";
    // }

    // if (!values['topmostSubform[0].Page1[0].SSN[0].f1_15[0]']) {
    //   errors['topmostSubform[0].Page1[0].SSN[0].f1_15[0]'] = "Social Security Number is required.";
    // }


    setFormErrors(errors);

    // Scroll to the first error element
    const firstErrorKey = Object.keys(errors)[0];
    if (firstErrorKey) {
      const firstErrorElement = document.querySelector(`[name="${firstErrorKey}"]`);
      if (firstErrorElement) {
        // Scroll to the top of the error element
        window.scrollTo({
          top: firstErrorElement.offsetTop,
          behavior: "smooth" // You can use "auto" instead of "smooth" for an instant scroll
        });
      }
    }

    return errors;
  };


  const validateCONTRACTORAGREEMENT = () => {
    const values = fieldValues;
    const errors = {};

    if (!values['Text-2']) {
      errors['Text-2'] = "Name is required.";
    }


    setFormErrors(errors);

    // Scroll to the first error element
    const firstErrorKey = Object.keys(errors)[0];
    if (firstErrorKey) {
      const firstErrorElement = document.querySelector(`[name="${firstErrorKey}"]`);
      if (firstErrorElement) {
        // Scroll to the top of the error element
        window.scrollTo({
          top: firstErrorElement.offsetTop,
          behavior: "smooth" // You can use "auto" instead of "smooth" for an instant scroll
        });
      }
    }
    return errors;
  };

  const handleFillAndGeneratePdf = async () => {
    try {

      let checkValue = validatew9();
      console.log(checkValue)
      if (_.isEmpty(checkValue)) {
        // if (1 != 1) {

        setIsdone(true);
        const response = await fetch("https://brokeragentbase.s3.amazonaws.com/assets/IRS+W9.pdf");
        if (!response.ok) {
          throw new Error("Failed to fetch PDF");
        }

        const existingPdfBytes = await response.arrayBuffer();
        const existingPdfDoc = await PDFDocument.load(existingPdfBytes);
        const firstPage = existingPdfDoc.getPages()[0];

        const lastPage = existingPdfDoc.getPages()[existingPdfDoc.getPageCount() - 1];

        const signatureData = signatures[pageNumber];
        const signatureBytes = await fetch(signatureData).then((res) => res.arrayBuffer());
        const signatureImage = await existingPdfDoc.embedPng(signatureBytes);

        firstPage.drawImage(signatureImage, {
          x: 105,
          y: 205,
          width: 200,
          height: 100,
        });

        lastPage.drawImage(signatureImage, {
          x: 105,
          y: 205,
          width: 200,
          height: 100,
        });


        const form = existingPdfDoc.getForm();
        const fieldNames = form.getFields().map((field) => field.getName());

        console.log("Form field names:", fieldNames);

        for (const fieldName of fieldNames) {
          const field = form.getField(fieldName);

          if (field) {
            if (field instanceof PDFTextField) {
              const textField = form.getTextField(fieldName);
              const fieldValue = fieldValues[fieldName] || ""; // Get value from state

              // Attach the onInput event to capture real-time updates
              const inputElement = document.querySelector(
                `input[name="${fieldName}"]`
              );
              if (inputElement) {
                inputElement.addEventListener("input", (event) => {
                  handleFieldChange(fieldName, event.target.value);
                });
              }

              textField.setText(fieldValue);
            } else if (field instanceof PDFCheckBox) {
              const checkbox = form.getCheckBox(fieldName);
              const isChecked = fieldValues[fieldName]; // Get value from state
              console.log(fieldName);
              console.log(isChecked);
              if (isChecked) {
                checkbox.check(isChecked);
              }
            } else {
              console.warn(`Unsupported field type for "${fieldName}"`);
            }
          } else {
            console.warn(`No field found with the name "${fieldName}"`);
          }
        }

        const modifiedPdfBytes = await existingPdfDoc.save();
        const modifiedPdfBlob = new Blob([modifiedPdfBytes], {
          type: "application/pdf",
        });

        const pdfFormData = new FormData();
        pdfFormData.append("file", modifiedPdfBlob, "modified.pdf");

        const apiUrl = "https://api.brokeragentbase.com/upload";
        const config = {
          headers: {
            "Content-Type": "multipart/form-data",
          },
        };

        const uploadResponse = await axios.post(apiUrl, pdfFormData, config);

        console.log("Response from the server:", uploadResponse.data);

        const userData = {
          onboard_taxcertification: uploadResponse.data,
        };
        console.log(userData);

        //  w9papernameAdd
        const userDatap = {
          agentId: formValuesNew?._id,
          paperwork_id: w9papernameAdd?._id ?? "qqq",
          documenturl: uploadResponse.data,
        };

        const responsepaperwork = await user_service.agentpaperworkDocument(userDatap);

        try {
          setLoader({ isActive: true });
          await user_service
            .contactUpdate(formValuesNew?._id, userData)
            .then((response) => {
              if (response) {
                // setIsShow(true);
                setStep4(false);
                setStep1("");
                setCategory("");
                setPhase("");
                setLoader({ isActive: false });
                setStep5(true);
                setIsdone(false);
                if (onBoard[0]?.image) {
                } else {
                  readyPad();
                }
                ContactGetById(formValuesNew?._id);
                setToaster({
                  type: "Document",
                  isShow: true,
                  toasterBody: response.data.message,
                  message: "Document signed successfully",
                });
                setTimeout(() => {
                  setToaster((prevToaster) => ({
                    ...prevToaster,
                    isShow: false,
                  }));
                  setIsShow(false);
                  //navigate("/signin");
                }, 2000);
              } else {
                setLoader({ isActive: false });
                setToaster({
                  types: "error",
                  isShow: true,
                  toasterBody: response.data.message,
                  message: "Something Went Wrong.",
                });
              }
            });
        } catch (error) {
          setLoader({ isActive: false });
          setToaster({
            types: "error",
            isShow: true,
            toasterBody: error.response
              ? error.response.data.message
              : error.message,
            message: "Something Went Wrong.",
          });
          setTimeout(() => {
            setToaster((prevToaster) => ({ ...prevToaster, isShow: false }));
          }, 2000);
        }
      }
    } catch (error) {
      console.error("Error:", error.message);
    }

  };

  const [signaturePad, setSignaturePad] = useState(null);
  const [savedSignature, setSavedSignature] = useState("");
  let signatureRedoArray = [];

  const readyPad = () => {
    let wrapper = document.getElementById("signature-pad");
    if (wrapper) {
      let canvas = wrapper?.querySelector("canvas");
      canvas.getContext("2d").scale(1, 1);
      let tempSignaturePad = new SignaturePad(canvas, {
        backgroundColor: "rgb(255, 255, 255)",
      });
      setSignaturePad(tempSignaturePad);
    }
  };

  function dataURLToBlob(dataURL) {
    const parts = dataURL.split(";base64,");
    const contentType = parts[0].split(":")[1];
    const raw = window.atob(parts[1]);
    const rawLength = raw.length;
    const uInt8Array = new Uint8Array(rawLength);

    for (let i = 0; i < rawLength; ++i) {
      uInt8Array[i] = raw.charCodeAt(i);
    }

    return new Blob([uInt8Array], { type: contentType });
  }
  const handleUndo = () => {
    let signatureRemovedData = [];
    let signatureData = signaturePad.toData();
    let signatureRedoData = _.cloneDeep(signatureData); //original data

    if (signatureData.length > 0) {
      signatureData.pop(); // remove the last dot or line
      signaturePad.fromData(signatureData);
      signatureRemovedData = signatureRedoData[signatureRedoData.length - 1];
      signatureRedoArray.push(signatureRemovedData);
    }
  };

  const handleClear = () => {
    signaturePad.clear();
  };

  useEffect(() => {
    window.scrollTo(0, 0);
    // readyPad();
  }, []);


  const [formHTML1, setFormHTML1] = useState("");
  const generateHtmlForm = (url) => {
    setFormHTML1(generateHtmlForm1(url));
  }

  async function generateHtmlForm1(pdfBytes) {
    const response = await fetch(pdfBytes);
    if (!response.ok) {
      throw new Error("Failed to fetch PDF");
    }

    const existingPdfBytes = await response.arrayBuffer();
    const existingPdfDoc = await PDFDocument.load(existingPdfBytes);

    const form = existingPdfDoc.getForm();
    const names = form.getFields().map((field) => field.getName());

    // console.log(form)

    const fields = existingPdfDoc.getForm().getFields();
    console.log(names)

    let htmlForm = '<form>';

    fields.forEach((field) => {
      const fieldName = field.getName();
      const fieldType = field.constructor.name;
      // console.log(fieldName)
      // console.log(fieldType)
      if (fieldType === 'PDFFormTextField' || fieldType === 'PDFTextField') {
        // Text field
        // const fieldValue = field.getDefaultValue();
        const fieldValue = field.getDefaultValue ? field.getDefaultValue() : '';
        htmlForm += `<label for="${fieldName}">${fieldName}:</label>`;
        htmlForm += `<input type="text" id="${fieldName}" name="${fieldName}" value="${fieldValue}"><br>`;
      } else if (fieldType === 'PDFFormCheckBox' || fieldType === 'PDFCheckBox') {
        // Checkbox
        const isChecked = field.isChecked();
        htmlForm += `<input type="checkbox" id="${fieldName}" name="${fieldName}" ${isChecked ? 'checked' : ''}>`;
        htmlForm += `<label for="${fieldName}">${fieldName}</label><br>`;
      }
      // Add other field types as needed (radio buttons, etc.)
    });

    htmlForm += '<input type="submit" value="Submit"></form>';
    // return htmlForm;


    setFormHTML1(htmlForm);
  }


  const handleFillAndGeneratePdfCONTRACTORAGREEMENT = async () => {
    try {


      let checkValue = validateCONTRACTORAGREEMENT();
      console.log(checkValue)
      if (_.isEmpty(checkValue)) {
        setIsdone(true);
        if (accountData.legacy_account && accountData.legacy_account === "yes") {
          var pdfSrcutah = "https://brokeragentbase.s3.amazonaws.com/assets/In-Depth+Realty+Legacy+Independent+Contractor+Agreement.pdf";
        } else {
          var pdfSrcutah = "https://brokeragentbase.s3.amazonaws.com/1702879744145-In%20Depth%20Realty%20Independent%20Contractor%20Agreement.pdf";
        }
        const response = await fetch(pdfSrcutah);
        if (!response.ok) {
          throw new Error("Failed to fetch PDF");
        }

        const existingPdfBytes = await response.arrayBuffer();
        const existingPdfDoc = await PDFDocument.load(existingPdfBytes);


        const secondPage = existingPdfDoc.getPages()[1];

        const lastPage = existingPdfDoc.getPages()[existingPdfDoc.getPageCount() - 1];

        const signatureData = signatures[pageNumber];
        const signatureBytes = await fetch(signatureData).then((res) => res.arrayBuffer());
        const signatureImage = await existingPdfDoc.embedPng(signatureBytes);

        secondPage.drawImage(signatureImage, {
          x: 100,
          y: 550,
          width: 100,
          height: 50,
        });

        lastPage.drawImage(signatureImage, {
          x: 100,
          y: 550,
          width: 100,
          height: 50,
        });
        
        // Loop through all pages and add the signature
        for (let i = 0; i < existingPdfDoc.getPageCount(); i++) {
          const existingPdfPage = existingPdfDoc.getPage(i);

          existingPdfPage.drawImage(signatureImage, {
            x: 400,
            y: 115,
            width: 200,
            height: 100,
          });
        }

        const form = existingPdfDoc.getForm();
        const fieldNames = form.getFields().map((field) => field.getName());

        console.log("Form field names:", fieldNames);



        for (const fieldName of fieldNames) {
          const field = form.getField(fieldName);

          if (field) {
            if (field instanceof PDFTextField) {
              const textField = form.getTextField(fieldName);
              const fieldValue = fieldValues[fieldName] || ""; // Get value from state
              console.log(fieldName);
              console.log(fieldValue);
              // Attach the onInput event to capture real-time updates
              const inputElement = document.querySelector(
                `input[name="${fieldName}"]`
              );
              if (inputElement) {
                inputElement.addEventListener("input", (event) => {
                  handleFieldChange(fieldName, event.target.value);
                });
              }

              textField.setText(fieldValue);

              // console.log(moment().format("D/M/YYYY"))
              if (fieldName === "Datefor" || fieldName === "Date-UmhjyFVm7Q" || fieldName === "Date-lp3s08IqyR" || fieldName === "Date-5dlZMJ-k_d" || fieldName === "Date-Q6eWflRnYZ" || fieldName === "Date-tYyCyajztN" || fieldName === "Date-01d1MUEWmf" || fieldName === "Date-kjDfYznWpK" || fieldName === "Date-0zMlYqw7RC" || fieldName === "Date-hZnp26AmC_" || fieldName === "Date-UXrRVROjjE" || fieldName === "Date-mIUd5vU4wQ" || fieldName === "Date-flNoUatAs_" || fieldName === "Date-Q1KWIkEd3j") {
                textField.setText(moment().format("D/M/YYYY"));
              }

              const agename =  accountData?.firstName + " " + accountData?.lastName;

              
              if (fieldName === "Text-FtWzrAoEX5") {
                textField.setText(agename);
              }

              if (fieldName === "Text-UhBRgC5wDw") {
                textField.setText("Shawn Kenney");
              }

              if (fieldName === "Text-yGXRq14uJW") {
                textField.setText("Shawn Kenney");
              }

              if (fieldName === "Text-1") {
                textField.setText(moment().format("D/M/YYYY"));
              }

            } else if (field instanceof PDFCheckBox) {
              const checkbox = form.getCheckBox(fieldName);
              const isChecked = fieldValues[fieldName]; // Get value from state
              console.log(fieldName);
              console.log(isChecked);
              if (isChecked) {
                checkbox.check(isChecked);
              }
            } else {
              console.warn(`Unsupported field type for "${fieldName}"`);
            }
          } else {
            console.warn(`No field found with the name "${fieldName}"`);
          }
        }

        const modifiedPdfBytes = await existingPdfDoc.save();
        const modifiedPdfBlob = new Blob([modifiedPdfBytes], {
          type: "application/pdf",
        });

        const pdfFormData = new FormData();
        pdfFormData.append("file", modifiedPdfBlob, "modified.pdf");

        const apiUrl = "https://api.brokeragentbase.com/upload";
        const config = {
          headers: {
            "Content-Type": "multipart/form-data",
          },
        };

        const uploadResponse = await axios.post(apiUrl, pdfFormData, config);

        console.log("Response from the server:", uploadResponse.data);
        // setIsdone(false);
        // setAgreementdone(true);

        const userDatap = {
          agentId: formValuesNew?._id,
          paperwork_id: contractorAgreementpapernameAdd?._id ?? "qqq",
          documenturl: uploadResponse.data,
        };

        const userData = {
          onboard_signature: uploadResponse.data,
        };

        const signn = await user_service.contactUpdate(formValuesNew?._id, userData);

        try {
          setLoader({ isActive: true });
          await user_service
            .agentpaperworkDocument(userDatap)
            .then((response) => {
              if (response) {
                // setIsShow(true);
                setStep4(false);
                setStep1("");
                setCategory("");
                setPhase("");
                setLoader({ isActive: false });
                setStep5(true);
                setIsdone(false);
                setAgreementdone(true);
                if (onBoard[0]?.image) {
                } else {
                  readyPad();
                }
                ContactGetById(formValuesNew?._id);
                setToaster({
                  type: "Document",
                  isShow: true,
                  toasterBody: response.data.message,
                  message: "Document signed successfully",
                });
                setTimeout(() => {
                  setToaster((prevToaster) => ({
                    ...prevToaster,
                    isShow: false,
                  }));
                  setIsShow(false);
                  //navigate("/signin");
                }, 2000);
              } else {
                setLoader({ isActive: false });
                setToaster({
                  types: "error",
                  isShow: true,
                  toasterBody: response.data.message,
                  message: "Something Went Wrong.",
                });
              }
            });
        } catch (error) {
          setLoader({ isActive: false });
          setToaster({
            types: "error",
            isShow: true,
            toasterBody: error.response
              ? error.response.data.message
              : error.message,
            message: "Something Went Wrong.",
          });
          setTimeout(() => {
            setToaster((prevToaster) => ({ ...prevToaster, isShow: false }));
          }, 2000);
        }

      }

    } catch (error) {
      console.error("Error:", error.message);
    }
  };


  const handleFillAndGeneratePdfreferralCONTRACTORAGREEMENT = async () => {
    try {
      setIsdone(true);
      const pdfSrcutah = "https://brokeragentbase.s3.amazonaws.com/assets/Referral+Agent+Independent+Contractor+Agreement.pdf";
      const response = await fetch(pdfSrcutah);
      if (!response.ok) {
        throw new Error("Failed to fetch PDF");
      }

      const existingPdfBytes = await response.arrayBuffer();
      const existingPdfDoc = await PDFDocument.load(existingPdfBytes);


      const sevenPage = existingPdfDoc.getPages()[6];
      const thirteenPage = existingPdfDoc.getPages()[12];

      // const lastPage = existingPdfDoc.getPages()[existingPdfDoc.getPageCount() - 1];

      const signatureData = signatures[pageNumber];
      const signatureBytes = await fetch(signatureData).then((res) => res.arrayBuffer());
      const signatureImage = await existingPdfDoc.embedPng(signatureBytes);

      sevenPage.drawImage(signatureImage, {
        x: 200,
        y: 240,
        width: 200,
        height: 100,
      });

      thirteenPage.drawImage(signatureImage, {
        x: 200,
        y: 400,
        width: 200,
        height: 100,
      });

      // Loop through all pages and add the signature
      for (let i = 0; i < existingPdfDoc.getPageCount(); i++) {
        const existingPdfPage = existingPdfDoc.getPage(i);

        existingPdfPage.drawImage(signatureImage, {
          x: 100,
          y: 40,
          width: 200,
          height: 100,
        });
      }

      const form = existingPdfDoc.getForm();
      const fieldNames = form.getFields().map((field) => field.getName());

      console.log("Form field names:", fieldNames);



      for (const fieldName of fieldNames) {
        const field = form.getField(fieldName);

        if (field) {
          if (field instanceof PDFTextField) {
            const textField = form.getTextField(fieldName);
            const fieldValue = fieldValues[fieldName] || ""; // Get value from state
            console.log(fieldName);
            console.log(fieldValue);
            // Attach the onInput event to capture real-time updates
            const inputElement = document.querySelector(
              `input[name="${fieldName}"]`
            );
            if (inputElement) {
              inputElement.addEventListener("input", (event) => {
                handleFieldChange(fieldName, event.target.value);
              });
            }

            textField.setText(fieldValue);

            // console.log(moment().format("D/M/YYYY"))
            if (fieldName === "referraldate" || fieldName === "contractordated") {
              textField.setText(moment().format("D/M/YYYY"));
            }

            if (fieldName === "printname" || fieldName === "referralname") {
              textField.setText(fieldValues['firstname']);
            }

            // madethis    25

            // dayof    month

            // between    year

            if (fieldName === "madethis") {
              textField.setText(moment().format("DD"));
            }

            if (fieldName === "dayof") {
              textField.setText(moment().format("MM"));
            }

            if (fieldName === "between") {
              textField.setText(moment().format("YY"));
            }


          } else if (field instanceof PDFCheckBox) {
            const checkbox = form.getCheckBox(fieldName);
            const isChecked = fieldValues[fieldName]; // Get value from state
            console.log(fieldName);
            console.log(isChecked);
            if (isChecked) {
              checkbox.check(isChecked);
            }
          } else {
            console.warn(`Unsupported field type for "${fieldName}"`);
          }
        } else {
          console.warn(`No field found with the name "${fieldName}"`);
        }
      }

      const modifiedPdfBytes = await existingPdfDoc.save();
      const modifiedPdfBlob = new Blob([modifiedPdfBytes], {
        type: "application/pdf",
      });

      const pdfFormData = new FormData();
      pdfFormData.append("file", modifiedPdfBlob, "modified.pdf");

      const apiUrl = "https://api.brokeragentbase.com/upload";
      const config = {
        headers: {
          "Content-Type": "multipart/form-data",
        },
      };

      const uploadResponse = await axios.post(apiUrl, pdfFormData, config);

      console.log("Response from the server:", uploadResponse.data);
      // setIsdone(false);
      // setAgreementdone(true);

      const userDatap = {
        agentId: formValuesNew?._id,
        paperwork_id: contractorAgreementpapernameAdd?._id ?? "qqq",
        documenturl: uploadResponse.data,
      };

      const userData = {
        onboard_signature: uploadResponse.data,
      };

      const signn = await user_service.contactUpdate(formValuesNew?._id, userData);

      try {
        setLoader({ isActive: true });
        await user_service
          .agentpaperworkDocument(userDatap)
          .then((response) => {
            if (response) {
              // setIsShow(true);
              setStep4(false);
              setStep1("");
              setCategory("");
              setPhase("");
              setLoader({ isActive: false });
              setStep5(true);
              setIsdone(false);
              setReferralAgreementdone(true);
              if (onBoard[0]?.image) {
              } else {
                readyPad();
              }
              ContactGetById(formValuesNew?._id);
              setToaster({
                type: "Document",
                isShow: true,
                toasterBody: response.data.message,
                message: "Document signed successfully",
              });
              setTimeout(() => {
                setToaster((prevToaster) => ({
                  ...prevToaster,
                  isShow: false,
                }));
                setIsShow(false);
                //navigate("/signin");
              }, 2000);
            } else {
              setLoader({ isActive: false });
              setToaster({
                types: "error",
                isShow: true,
                toasterBody: response.data.message,
                message: "Something Went Wrong.",
              });
            }
          });
      } catch (error) {
        setLoader({ isActive: false });
        setToaster({
          types: "error",
          isShow: true,
          toasterBody: error.response
            ? error.response.data.message
            : error.message,
          message: "Something Went Wrong.",
        });
        setTimeout(() => {
          setToaster((prevToaster) => ({ ...prevToaster, isShow: false }));
        }, 2000);
      }

    } catch (error) {
      console.error("Error:", error.message);
    }
  };



  const handleFillAndGeneratePdfutah = async (boarddata = "") => {
    try {
      setIsdone(true);
      const pdfSrcutah = "https://brokeragentbase.s3.amazonaws.com/1702881544800-1700758888303-Utah%20Real%20Estate%20Membership%20Change%20Form%20%282%29.pdf";
      const response = await fetch(pdfSrcutah);
      if (!response.ok) {
        throw new Error("Failed to fetch PDF");
      }

      const existingPdfBytes = await response.arrayBuffer();
      const existingPdfDoc = await PDFDocument.load(existingPdfBytes);


      const firstPage = existingPdfDoc.getPages()[0];

      // const lastPage = existingPdfDoc.getPages()[existingPdfDoc.getPageCount() - 1];

      const signatureData = signatures[pageNumber];
      const signatureBytes = await fetch(signatureData).then((res) => res.arrayBuffer());
      const signatureImage = await existingPdfDoc.embedPng(signatureBytes);

      firstPage.drawImage(signatureImage, {
        x: 155,
        y: 140,
        width: 200,
        height: 100,
      });

      // lastPage.drawImage(signatureImage, {
      //   x: 105,
      //   y: 205,
      //   width: 200,
      //   height: 100,
      // });


      const form = existingPdfDoc.getForm();
      const fieldNames = form.getFields().map((field) => field.getName());

      console.log("Form field names:", fieldNames);



      for (const fieldName of fieldNames) {
        const field = form.getField(fieldName);

        if (field) {
          if (field instanceof PDFTextField) {
            const textField = form.getTextField(fieldName);
            const fieldValue = fieldValues[fieldName] || ""; // Get value from state
            console.log(fieldName);
            console.log(fieldValue);
            // Attach the onInput event to capture real-time updates
            const inputElement = document.querySelector(
              `input[name="${fieldName}"]`
            );
            if (inputElement) {
              inputElement.addEventListener("input", (event) => {
                handleFieldChange(fieldName, event.target.value);
              });
            }

            textField.setText(fieldValue);
          } else if (field instanceof PDFCheckBox) {
            const checkbox = form.getCheckBox(fieldName);
            const isChecked = fieldValues[fieldName]; // Get value from state
            console.log(fieldName);
            console.log(isChecked);
            if (isChecked) {
              checkbox.check(isChecked);
            }
          } else {
            console.warn(`Unsupported field type for "${fieldName}"`);
          }
        } else {
          console.warn(`No field found with the name "${fieldName}"`);
        }
      }

      const modifiedPdfBytes = await existingPdfDoc.save();
      const modifiedPdfBlob = new Blob([modifiedPdfBytes], {
        type: "application/pdf",
      });

      const pdfFormData = new FormData();
      pdfFormData.append("file", modifiedPdfBlob, "modified.pdf");

      const apiUrl = "https://api.brokeragentbase.com/upload";
      const config = {
        headers: {
          "Content-Type": "multipart/form-data",
        },
      };

      const uploadResponse = await axios.post(apiUrl, pdfFormData, config);

      console.log("Response from the server:", uploadResponse.data);
      // handleNextStep();
      // setIsdone(false);

      const userDatap = {
        agentId: formValuesNew?._id,
        paperwork_id: boarddata.agentPaperNameData._id ?? boarddata?.document,
        documenturl: uploadResponse.data,
      };

      try {
        setLoader({ isActive: true });
        await user_service
          .agentpaperworkDocument(userDatap)
          .then((response) => {
            if (response) {
              // setIsShow(true);
              setStep4(false);
              setStep1("");
              setCategory("");
              setPhase("");
              setLoader({ isActive: false });
              setStep5(true);
              handleNextStep();
              setIsdone(false);

              if (boarddata?.image) {
              } else {
                readyPad();
              }
              ContactGetById(formValuesNew?._id);
              setToaster({
                type: "Document",
                isShow: true,
                toasterBody: response.data.message,
                message: "Document signed successfully",
              });
              setTimeout(() => {
                setToaster((prevToaster) => ({
                  ...prevToaster,
                  isShow: false,
                }));
                setIsShow(false);
                //navigate("/signin");
              }, 2000);
            } else {
              setLoader({ isActive: false });
              setToaster({
                types: "error",
                isShow: true,
                toasterBody: response.data.message,
                message: "Something Went Wrong.",
              });
            }
          });
      } catch (error) {
        setLoader({ isActive: false });
        setToaster({
          types: "error",
          isShow: true,
          toasterBody: error.response
            ? error.response.data.message
            : error.message,
          message: "Something Went Wrong.",
        });
        setTimeout(() => {
          setToaster((prevToaster) => ({ ...prevToaster, isShow: false }));
        }, 2000);
      }
    } catch (error) {
      console.error("Error:", error.message);
    }
  };



  const handleFillAndGeneratePdfSaltLake = async (boarddata = "") => {
    try {
      setIsdone(true);
      const pdfSrcutah = "https://brokeragentbase.s3.amazonaws.com/assets/Salt+Lake+Board+Membership+Form.pdf";
      const response = await fetch(pdfSrcutah);
      if (!response.ok) {
        throw new Error("Failed to fetch PDF");
      }

      const existingPdfBytes = await response.arrayBuffer();
      const existingPdfDoc = await PDFDocument.load(existingPdfBytes);


      const secondPage = existingPdfDoc.getPages()[1];

      // const lastPage = existingPdfDoc.getPages()[existingPdfDoc.getPageCount() - 1];

      const signatureData = signatures[pageNumber];
      const signatureBytes = await fetch(signatureData).then((res) => res.arrayBuffer());
      const signatureImage = await existingPdfDoc.embedPng(signatureBytes);

      secondPage.drawImage(signatureImage, {
        x: 155,
        y: 40,
        width: 200,
        height: 100,
      });

      // lastPage.drawImage(signatureImage, {
      //   x: 105,
      //   y: 205,
      //   width: 200,
      //   height: 100,
      // });


      const form = existingPdfDoc.getForm();
      const fieldNames = form.getFields().map((field) => field.getName());

      console.log("Form field names saltlake:", fieldNames);
      console.log("Form :", form);



      for (const fieldName of fieldNames) {
        const field = form.getField(fieldName);

        if (fieldName != "COMPANY NAME") {
          if (field) {
            if (field instanceof PDFTextField) {
              const textField = form.getTextField(fieldName);

              const fieldValue = fieldValues[fieldName] || ""; // Get value from state
              console.log(fieldName);
              console.log(fieldValue);
              // Attach the onInput event to capture real-time updates
              const inputElement = document.querySelector(
                `input[name="${fieldName}"]`
              );
              if (inputElement) {
                inputElement.addEventListener("input", (event) => {
                  handleFieldChange(fieldName, event.target.value);
                });
              }

              textField.setText(fieldValue);

              // console.log(moment().format("D/M/YYYY"))
              if (fieldName === "DATE") {
                textField.setText(moment().format("D/M/YYYY"));
              }

            } else if (field instanceof PDFCheckBox) {
              const checkbox = form.getCheckBox(fieldName);
              const isChecked = fieldValues[fieldName]; // Get value from state
              console.log(fieldName);
              console.log(isChecked);
              if (isChecked) {
                checkbox.check(isChecked);
              }
            } else if (field instanceof PDFRadioGroup) {
              // Handle radio button groups
              //  if(fieldName === "VETERAN"){
              //      // Handle radio button groups
              //     const radioGroup = form.getRadioGroup(fieldName);
              //     const selectedOption = fieldValues[fieldName]; // Get the name of the selected option
              //     console.log(fieldName);
              //     console.log(selectedOption);

              //     // Iterate through options and set the selected option
              //     for (const option of radioGroup.getOptions()) {
              //       const optionName = option.name;
              //       if (optionName === selectedOption) {
              //         option.setValue(true);
              //       } else {
              //         option.setValue(false);
              //       }
              //     }
              //   }
            } else {
              console.warn(`Unsupported field type for "${fieldName}"`);
            }
          } else {
            console.warn(`No field found with the name "${fieldName}"`);
          }

        }
      }

      const modifiedPdfBytes = await existingPdfDoc.save();
      const modifiedPdfBlob = new Blob([modifiedPdfBytes], {
        type: "application/pdf",
      });

      const pdfFormData = new FormData();
      pdfFormData.append("file", modifiedPdfBlob, "modified.pdf");

      const apiUrl = "https://api.brokeragentbase.com/upload";
      const config = {
        headers: {
          "Content-Type": "multipart/form-data",
        },
      };

      const uploadResponse = await axios.post(apiUrl, pdfFormData, config);

      console.log("Response from the server:", uploadResponse.data);
      // handleNextStep();
      // setIsdone(false);

      const userDatap = {
        agentId: formValuesNew?._id,
        paperwork_id: boarddata.agentPaperNameData._id ?? boarddata?.document,
        documenturl: uploadResponse.data,
      };

      try {
        setLoader({ isActive: true });
        await user_service
          .agentpaperworkDocument(userDatap)
          .then((response) => {
            if (response) {
              // setIsShow(true);
              setStep4(false);
              setStep1("");
              setCategory("");
              setPhase("");
              setLoader({ isActive: false });
              setStep5(true);
              handleNextStep();
              setIsdone(false);
              if (boarddata.image) {
              } else {
                readyPad();
              }
              ContactGetById(formValuesNew?._id);
              setToaster({
                type: "Document",
                isShow: true,
                toasterBody: response.data.message,
                message: "Document signed successfully",
              });
              setTimeout(() => {
                setToaster((prevToaster) => ({
                  ...prevToaster,
                  isShow: false,
                }));
                setIsShow(false);
                //navigate("/signin");
              }, 2000);
            } else {
              setLoader({ isActive: false });
              setToaster({
                types: "error",
                isShow: true,
                toasterBody: response.data.message,
                message: "Something Went Wrong.",
              });
            }
          });
      } catch (error) {
        setLoader({ isActive: false });
        setToaster({
          types: "error",
          isShow: true,
          toasterBody: error.response
            ? error.response.data.message
            : error.message,
          message: "Something Went Wrong.",
        });
        setTimeout(() => {
          setToaster((prevToaster) => ({ ...prevToaster, isShow: false }));
        }, 2000);
      }
    } catch (error) {
      console.error("Error:", error.message);
    }
  };




  const handleFillAndGeneratePdfUCAR = async (boarddata = "") => {
    try {
      setIsdone(true);
      const pdfSrcutah = "https://brokeragentbase.s3.amazonaws.com/1702980048348-UCAR.pdf";
      const response = await fetch(pdfSrcutah);
      if (!response.ok) {
        throw new Error("Failed to fetch PDF");
      }

      const existingPdfBytes = await response.arrayBuffer();
      const existingPdfDoc = await PDFDocument.load(existingPdfBytes);


      const secondPage = existingPdfDoc.getPages()[0];

      // const lastPage = existingPdfDoc.getPages()[existingPdfDoc.getPageCount() - 1];

      const signatureData = signatures[pageNumber];
      const signatureBytes = await fetch(signatureData).then((res) => res.arrayBuffer());
      const signatureImage = await existingPdfDoc.embedPng(signatureBytes);

      secondPage.drawImage(signatureImage, {
        x: 155,
        y: 130,
        width: 200,
        height: 100,
      });

      // lastPage.drawImage(signatureImage, {
      //   x: 105,
      //   y: 205,
      //   width: 200,
      //   height: 100,
      // });


      const form = existingPdfDoc.getForm();
      const fieldNames = form.getFields().map((field) => field.getName());

      console.log("Form field names:", fieldNames);



      for (const fieldName of fieldNames) {
        const field = form.getField(fieldName);

        if (field) {
          if (field instanceof PDFTextField) {
            const textField = form.getTextField(fieldName);
            const fieldValue = fieldValues[fieldName] || ""; // Get value from state
            console.log(fieldName);
            console.log(fieldValue);
            // Attach the onInput event to capture real-time updates
            const inputElement = document.querySelector(
              `input[name="${fieldName}"]`
            );
            if (inputElement) {
              inputElement.addEventListener("input", (event) => {
                handleFieldChange(fieldName, event.target.value);
              });
            }

            textField.setText(fieldValue);

            // console.log(moment().format("D/M/YYYY"))
            if (fieldName === "Date" || fieldName === "Date_2" || fieldName === "Date_3" || fieldName === "Date_4") {
              textField.setText(moment().format("D/M/YYYY"));
            }

          } else if (field instanceof PDFCheckBox) {
            const checkbox = form.getCheckBox(fieldName);
            const isChecked = fieldValues[fieldName]; // Get value from state
            console.log(fieldName);
            console.log(isChecked);
            if (isChecked) {
              checkbox.check(isChecked);
            }
          } else {
            console.warn(`Unsupported field type for "${fieldName}"`);
          }
        } else {
          console.warn(`No field found with the name "${fieldName}"`);
        }
      }

      const modifiedPdfBytes = await existingPdfDoc.save();
      const modifiedPdfBlob = new Blob([modifiedPdfBytes], {
        type: "application/pdf",
      });

      const pdfFormData = new FormData();
      pdfFormData.append("file", modifiedPdfBlob, "modified.pdf");

      const apiUrl = "https://api.brokeragentbase.com/upload";
      const config = {
        headers: {
          "Content-Type": "multipart/form-data",
        },
      };

      const uploadResponse = await axios.post(apiUrl, pdfFormData, config);

      console.log("Response from the server:", uploadResponse.data);
      // handleNextStep();
      // setIsdone(false);

      const userDatap = {
        agentId: formValuesNew?._id,
        paperwork_id: boarddata.agentPaperNameData._id ?? boarddata?.document,
        documenturl: uploadResponse.data,
      };

      try {
        setLoader({ isActive: true });

        await user_service
          .agentpaperworkDocument(userDatap)
          .then((response) => {
            if (response) {
              // setIsShow(true);
              setStep4(false);
              setStep1("");
              setCategory("");
              setPhase("");
              setLoader({ isActive: false });
              setStep5(true);
              handleNextStep();
              setIsdone(false);
              if (boarddata?.image) {
              } else {
                readyPad();
              }
              ContactGetById(formValuesNew?._id);
              setToaster({
                type: "Document",
                isShow: true,
                toasterBody: response.data.message,
                message: "Document signed successfully",
              });
              setTimeout(() => {
                setToaster((prevToaster) => ({
                  ...prevToaster,
                  isShow: false,
                }));
                setIsShow(false);
                //navigate("/signin");
              }, 2000);
            } else {
              setLoader({ isActive: false });
              setToaster({
                types: "error",
                isShow: true,
                toasterBody: response.data.message,
                message: "Something Went Wrong.",
              });
            }
          });
      } catch (error) {
        setLoader({ isActive: false });
        setToaster({
          types: "error",
          isShow: true,
          toasterBody: error.response
            ? error.response.data.message
            : error.message,
          message: "Something Went Wrong.",
        });
        setTimeout(() => {
          setToaster((prevToaster) => ({ ...prevToaster, isShow: false }));
        }, 2000);
      }
    } catch (error) {
      console.error("Error:", error.message);
    }
  };

  const handleFillAndGeneratePdfNorthernWasatch = async (boarddata = "") => {
    try {
      setIsdone(true);
      const pdfSrcutah = "https://brokeragentbase.s3.amazonaws.com/1703053628646-Northern%20Wasatch%20Association.pdf";
      const response = await fetch(pdfSrcutah);
      if (!response.ok) {
        throw new Error("Failed to fetch PDF");
      }

      const existingPdfBytes = await response.arrayBuffer();
      const existingPdfDoc = await PDFDocument.load(existingPdfBytes);


      const secondPage = existingPdfDoc.getPages()[0];

      // const lastPage = existingPdfDoc.getPages()[existingPdfDoc.getPageCount() - 1];

      const signatureData = signatures[pageNumber];
      const signatureBytes = await fetch(signatureData).then((res) => res.arrayBuffer());
      const signatureImage = await existingPdfDoc.embedPng(signatureBytes);

      secondPage.drawImage(signatureImage, {
        x: 155,
        y: 500,
        width: 200,
        height: 100,
      });

      // lastPage.drawImage(signatureImage, {
      //   x: 105,
      //   y: 205,
      //   width: 200,
      //   height: 100,
      // });




      const form = existingPdfDoc.getForm();
      const fieldNames = form.getFields().map((field) => field.getName());

      console.log("Form field names:", fieldNames);



      for (const fieldName of fieldNames) {
        const field = form.getField(fieldName);

        if (field) {
          if (field instanceof PDFTextField) {
            const textField = form.getTextField(fieldName);
            const fieldValue = fieldValues[fieldName] || ""; // Get value from state
            console.log(fieldName);
            console.log(fieldValue);
            // Attach the onInput event to capture real-time updates
            const inputElement = document.querySelector(
              `input[name="${fieldName}"]`
            );
            if (inputElement) {
              inputElement.addEventListener("input", (event) => {
                handleFieldChange(fieldName, event.target.value);
              });
            }

            textField.setText(fieldValue);

            // console.log(moment().format("D/M/YYYY"))
            if (fieldName === "date") {
              textField.setText(moment().format("D/M/YYYY"));
            }

            // if (fieldName === "sognature") {
            //   const { x, y, width, height } = textField.getRectangle();

            //   // Draw the image into the specified field
            //   secondPage.drawImage(signatureImage, {
            //     x,
            //     y,
            //     width,
            //     height,
            //   });


            // }

          } else if (field instanceof PDFCheckBox) {
            const checkbox = form.getCheckBox(fieldName);
            const isChecked = fieldValues[fieldName]; // Get value from state
            console.log(fieldName);
            console.log(isChecked);
            if (isChecked) {
              checkbox.check(isChecked);
            }
          } else {
            console.warn(`Unsupported field type for "${fieldName}"`);
          }
        } else {
          console.warn(`No field found with the name "${fieldName}"`);
        }
      }

      const modifiedPdfBytes = await existingPdfDoc.save();
      const modifiedPdfBlob = new Blob([modifiedPdfBytes], {
        type: "application/pdf",
      });

      const pdfFormData = new FormData();
      pdfFormData.append("file", modifiedPdfBlob, "modified.pdf");

      const apiUrl = "https://api.brokeragentbase.com/upload";
      const config = {
        headers: {
          "Content-Type": "multipart/form-data",
        },
      };

      const uploadResponse = await axios.post(apiUrl, pdfFormData, config);

      console.log("Response from the server:", uploadResponse.data);
      // handleNextStep();
      // setIsdone(false);

      const userDatap = {
        agentId: formValuesNew?._id,
        paperwork_id: boarddata.agentPaperNameData._id ?? boarddata?.document,
        documenturl: uploadResponse.data,
      };

      try {
        setLoader({ isActive: true });

        await user_service
          .agentpaperworkDocument(userDatap)
          .then((response) => {
            if (response) {
              // setIsShow(true);
              setStep4(false);
              setStep1("");
              setCategory("");
              setPhase("");
              setLoader({ isActive: false });
              setStep5(true);
              handleNextStep();
              setIsdone(false);
              if (boarddata?.image) {
              } else {
                readyPad();
              }
              ContactGetById(formValuesNew?._id);
              setToaster({
                type: "Document",
                isShow: true,
                toasterBody: response.data.message,
                message: "Document signed successfully",
              });
              setTimeout(() => {
                setToaster((prevToaster) => ({
                  ...prevToaster,
                  isShow: false,
                }));
                setIsShow(false);
                //navigate("/signin");
              }, 2000);
            } else {
              setLoader({ isActive: false });
              setToaster({
                types: "error",
                isShow: true,
                toasterBody: response.data.message,
                message: "Something Went Wrong.",
              });
            }
          });
      } catch (error) {
        setLoader({ isActive: false });
        setToaster({
          types: "error",
          isShow: true,
          toasterBody: error.response
            ? error.response.data.message
            : error.message,
          message: "Something Went Wrong.",
        });
        setTimeout(() => {
          setToaster((prevToaster) => ({ ...prevToaster, isShow: false }));
        }, 2000);
      }
    } catch (error) {
      console.error("Error:", error.message);
    }
  };

  // Washington County Board of Realtors
  const handleFillAndGeneratePdfWashingtonCountyBoard = async (boarddata = "") => {
    try {
      setIsdone(true);
      const pdfSrcutah = "https://brokeragentbase.s3.amazonaws.com/1703057663392-Washington%20County%20Board%20of%20Realtors.pdf";
      const response = await fetch(pdfSrcutah);
      if (!response.ok) {
        throw new Error("Failed to fetch PDF");
      }

      const existingPdfBytes = await response.arrayBuffer();
      const existingPdfDoc = await PDFDocument.load(existingPdfBytes);


      const secondPage = existingPdfDoc.getPages()[0];

      const lastPage = existingPdfDoc.getPages()[existingPdfDoc.getPageCount() - 1];

      const signatureData = signatures[pageNumber];
      const signatureBytes = await fetch(signatureData).then((res) => res.arrayBuffer());
      const signatureImage = await existingPdfDoc.embedPng(signatureBytes);

      secondPage.drawImage(signatureImage, {
        x: 155,
        y: 50,
        width: 200,
        height: 100,
      });

      lastPage.drawImage(signatureImage, {
        x: 105,
        y: 205,
        width: 200,
        height: 100,
      });




      const form = existingPdfDoc.getForm();
      const fieldNames = form.getFields().map((field) => field.getName());

      console.log("Form field names:", fieldNames);



      for (const fieldName of fieldNames) {
        const field = form.getField(fieldName);

        if (field) {
          if (field instanceof PDFTextField) {
            const textField = form.getTextField(fieldName);
            const fieldValue = fieldValues[fieldName] || ""; // Get value from state
            console.log(fieldName);
            console.log(fieldValue);
            // Attach the onInput event to capture real-time updates
            const inputElement = document.querySelector(
              `input[name="${fieldName}"]`
            );
            if (inputElement) {
              inputElement.addEventListener("input", (event) => {
                handleFieldChange(fieldName, event.target.value);
              });
            }

            textField.setText(fieldValue);

            // console.log(moment().format("D/M/YYYY"))
            if (fieldName === "applicantdate") {
              textField.setText(moment().format("D/M/YYYY"));
            }
            if (fieldName === "primename") {
              textField.setText(fieldValues['agentname']);
            }

            // if (fieldName === "sognature") {
            //   const { x, y, width, height } = textField.getRectangle();

            //   // Draw the image into the specified field
            //   secondPage.drawImage(signatureImage, {
            //     x,
            //     y,
            //     width,
            //     height,
            //   });


            // }

          } else if (field instanceof PDFCheckBox) {
            const checkbox = form.getCheckBox(fieldName);
            const isChecked = fieldValues[fieldName]; // Get value from state
            console.log(fieldName);
            console.log(isChecked);
            if (isChecked) {
              checkbox.check(isChecked);
            }
          } else {
            console.warn(`Unsupported field type for "${fieldName}"`);
          }
        } else {
          console.warn(`No field found with the name "${fieldName}"`);
        }
      }

      const modifiedPdfBytes = await existingPdfDoc.save();
      const modifiedPdfBlob = new Blob([modifiedPdfBytes], {
        type: "application/pdf",
      });

      const pdfFormData = new FormData();
      pdfFormData.append("file", modifiedPdfBlob, "modified.pdf");

      const apiUrl = "https://api.brokeragentbase.com/upload";
      const config = {
        headers: {
          "Content-Type": "multipart/form-data",
        },
      };

      const uploadResponse = await axios.post(apiUrl, pdfFormData, config);

      console.log("Response from the server:", uploadResponse.data);
      // handleNextStep();
      // setIsdone(false);

      const userDatap = {
        agentId: formValuesNew?._id,
        paperwork_id: boarddata.agentPaperNameData._id ?? boarddata?.document,
        documenturl: uploadResponse.data,
      };

      try {
        setLoader({ isActive: true });

        await user_service
          .agentpaperworkDocument(userDatap)
          .then((response) => {
            if (response) {
              // setIsShow(true);
              setStep4(false);
              setStep1("");
              setCategory("");
              setPhase("");
              setLoader({ isActive: false });
              setStep5(true);
              handleNextStep();
              setIsdone(false);
              if (boarddata?.image) {
              } else {
                readyPad();
              }
              ContactGetById(formValuesNew?._id);
              setToaster({
                type: "Document",
                isShow: true,
                toasterBody: response.data.message,
                message: "Document signed successfully",
              });
              setTimeout(() => {
                setToaster((prevToaster) => ({
                  ...prevToaster,
                  isShow: false,
                }));
                setIsShow(false);
                //navigate("/signin");
              }, 2000);
            } else {
              setLoader({ isActive: false });
              setToaster({
                types: "error",
                isShow: true,
                toasterBody: response.data.message,
                message: "Something Went Wrong.",
              });
            }
          });
      } catch (error) {
        setLoader({ isActive: false });
        setToaster({
          types: "error",
          isShow: true,
          toasterBody: error.response
            ? error.response.data.message
            : error.message,
          message: "Something Went Wrong.",
        });
        setTimeout(() => {
          setToaster((prevToaster) => ({ ...prevToaster, isShow: false }));
        }, 2000);
      }
    } catch (error) {
      console.error("Error:", error.message);
    }
  };


  const [currentStep, setCurrentStep] = useState(0);

  const handleNextStep = () => {
    setCurrentStep((prevStep) => prevStep + 1);
  };

  const handleReset = () => {
    setCurrentStep(0);
  };

  const handleFinishstep = async () => {
    setIsShow(true);
    try {

      const userData = {
        // onboard_signature: response.data,
        onboardfinal: "done",
      };
      console.log(userData);

      try {
        setLoader({ isActive: true });
        await user_service
          .contactUpdate(formValuesNew?._id, userData)
          .then((response) => {
            if (response.data) {
              setPdfSrcNew(true);
              // setStep5(false);
              // setStep4(false);
              // setStep1("");
              // setCategory("");
              // setPhase("");
              setLoader({ isActive: false });
              ContactGetById(formValuesNew?._id);
              setToaster({
                type: "Document",
                isShow: true,
                toasterBody: response.data.message,
                message: "Document signed successfully",
              });
              setTimeout(() => {
                setToaster((prevToaster) => ({
                  ...prevToaster,
                  isShow: false,
                }));
                setIsShow(false);
                navigate("/onboard-thankyou");
              }, 2000);
            } else {
              setLoader({ isActive: false });
              setToaster({
                types: "error",
                isShow: true,
                toasterBody: response.data.message,
                message: "Something Went Wrong.",
              });
            }
          });
      } catch (error) {
        setLoader({ isActive: false });
        setToaster({
          types: "error",
          isShow: true,
          toasterBody: error.response
            ? error.response.data.message
            : error.message,
          message: "Something Went Wrong.",
        });
        setTimeout(() => {
          setToaster((prevToaster) => ({ ...prevToaster, isShow: false }));
        }, 2000);
      }
      // Set the modified PDF URL to state or use it as needed
      // setConfirmedSignatures((prevConfirmedSignatures) => [
      //   ...prevConfirmedSignatures,
      //   { pageNumber, signatureData, pdfBase64: modifiedPdfUrl },
      // ]);
    } catch (error) {
      console.error("Error adding signature to PDF:", error);
    }
  };


  const [passwordShownn, setPasswordShownn] = useState(false);
  const [passwordShown, setPasswordShown] = useState(false);
  const togglePasswordd = () => {
    setPasswordShownn(passwordShownn ? false : true);
  };
  const togglePassword = () => {
    setPasswordShown(passwordShown ? false : true);
  };
  return (
    <div className="bg-secondary float-left w-100 pt-4 mb-4">
      <Loader isActive={isActive} />
      {isShow && (
        <Toaster
          type={type}
          isShow={isShow}
          toasterBody={toasterBody}
          message={message}
        />
      )}
      <div className="container onboarding">
        <main className="page-wrapper homepage_layout getContact_page_wrap">
          <div className="onboardingbutton">
            <div className="">


              {
                accountData.onboardfinal === "done" ?

                  <div className="thank_you row align-items-center mt-md-2">
                    <div className="col-lg-7 order-lg-2 mb-lg-0 mb-4 pb-2 pb-lg-0 mt-lg-0 mt-md-0 mt-sm-5 mt-5"><img className="d-block mx-auto" src={AgentAdded} alt="Hero image" /></div>
                    <div className="col-lg-5 order-lg-1 pe-lg-0">
                      <h1 className="display-5 mb-4 me-lg-n5 text-lg-start text-center mb-4"> Thank you
                      </h1>
                      <p className="text-lg-start text-center mb-4 mb-lg-5 fs-lg">We are pleased to inform you that your account setup is already done. Our team at Indepth Realty will review your application, and upon approval, you will be notified via email.</p>
                      <p className="text-lg-start text-center mb-4 mb-lg-5 fs-lg">Thank you for choosing Indepth Realty. We look forward to assisting you in your real estate endeavors.</p>
                      <div className="pull-right mt-2">
                        <NavLink type="button"
                          className="btn btn-primary btn-sm" to="/">Go To Homepage</NavLink>
                      </div>
                    </div>
                  </div>
                  :
                  <>
                    <h3 className="">Welcome Onboard</h3>
                    <p>
                      Let's get started with just a few easy steps: Complete your
                      profile, explore tools, and connect. Your journey with us begins
                      now!
                    </p>
                    {/* <hr className="mb-3" /> */}
                    <div className="bg-light rounded-3 p-3 mb-3">
                      <div className="steps pt-4 pb-1">
                        <div
                          className={step1 ? "step active" : ""}
                          onClick={handleProgressbar}
                        >
                          <div className="step-progress">
                            <span className="step-progress-start"></span>
                            <span className="step-progress-end"></span>
                            <span className="step-number">1</span>
                          </div>
                          <div className="step-label">Password</div>
                        </div>
                        <div
                          className={category ? "step active" : ""}
                          onClick={handleProgressbar}
                        >
                          <div className="step-progress">
                            <span className="step-progress-start"></span>
                            <span className="step-progress-end"></span>
                            <span className="step-number">2</span>
                          </div>
                          <div className="step-label">Information</div>
                        </div>

                        {/* <div
                    className={phase ? "step active" : ""}
                    onClick={handleProgressbar}
                  >
                    <div className="step-progress">
                      <span className="step-progress-start"></span>
                      <span className="step-progress-end"></span>
                      <span className="step-number">3</span>
                    </div>
                    <div className="step-label">About Me</div>
                  </div> */}


                        <div
                          className={step4 ? "step active" : ""}
                          onClick={handleProgressbar}
                        >
                          <div className="step-progress">
                            <span className="step-progress-start"></span>
                            <span className="step-progress-end"></span>
                            <span className="step-number">3</span>
                          </div>
                          <div className="step-label">Tax info</div>
                        </div>

                        <div
                          className={step5 ? "step active" : ""}
                          onClick={handleProgressbar}
                        >
                          <div className="step-progress">
                            <span className="step-progress-start"></span>
                            <span className="step-progress-end"></span>
                            <span className="step-number">4</span>
                          </div>
                          <div className="step-label">Document</div>
                        </div>
                      </div>
                    </div>

                    {step1 ? (
                      <>
                        <div className="bg-light rounded-3 p-3 mb-3">
                          {/* <h2 className="h4 mb-4">
                        <i className="fi-info-circle text-primary fs-5 mt-n1 me-2"></i>
                        Basic info
                      </h2> */}
                          <div className="row">
                            <div className="col-sm-6 mb-2">
                              <label className="form-label" for="pr-fn">
                                First name
                              </label>
                              <input
                                className="form-control form-control-lg"
                                id="inline-form-input"
                                type="text"
                                name="firstName"
                                value={accountData.firstName}
                                onChange={handleChange}
                              />
                              {formErrors.firstName && (
                                <div className="invalid-tooltip">
                                  {formErrors.firstName}
                                </div>
                              )}
                            </div>

                            <div className="col-sm-6 mb-2">
                              <label className="form-label" for="pr-sn">
                                Last Name
                              </label>
                              <input
                                className="form-control form-control-lg"
                                id="inline-form-input"
                                type="text"
                                name="lastName"
                                value={accountData.lastName}
                                onChange={handleChange}
                              />
                              {formErrors.lastName && (
                                <div className="invalid-tooltip">
                                  {formErrors.lastName}
                                </div>
                              )}
                            </div>
                            {/* <div className="col-sm-6 mb-2">
                          <label className="form-label" for="pr-email">
                            Confirm Intranet ID{" "}
                            <span className="text-danger">*</span>
                          </label>
                          <input
                            className="form-control form-control-lg"
                            id="inline-form-input"
                            type="text"
                            name="intranet_id"
                            value={additionalActivateFields.intranet_id}
                            onChange={handleChanges}
                          />
                          {formErrors.intranet_id && (
                            <div className="invalid-tooltip">
                              {formErrors.intranet_id}
                            </div>
                          )}
                        </div> */}

                            <div className="col-sm-6 mb-2">
                              <label className="form-label" for="pr-phone">
                                Password
                              </label>
                              <span className="text-danger">*</span>
                              <div className="password-toggle">
                                <label className="password-toggle-btn" aria-label="Show/hide password" onClick={togglePasswordd}>
                                  <span className="password-toggle-indicator"></span>
                                </label>
                              </div>
                              <input
                                className="form-control form-control-lg"
                                id="inline-form-input"
                                type={passwordShownn ? "text" : "password"}
                                placeholder="create a password"
                                name="passwordHash"
                                value={accountData.passwordHash}
                                onChange={handleChange}
                              />
                              {formErrors.passwordHash && (
                                <div className="invalid-tooltip">
                                  {formErrors.passwordHash}
                                </div>
                              )}
                            </div>

                            <div className="col-sm-6 mb-2">
                              <label className="form-label" for="pr-phone">
                                Confirm Password
                              </label>
                              <span className="text-danger">*</span>
                              <div className="password-toggle">
                                <label className="password-toggle-btn" aria-label="Show/hide password" onClick={togglePassword}>
                                  <span className="password-toggle-indicator"></span>
                                </label>
                              </div>
                              <input
                                className="form-control form-control-lg"
                                id="inline-form-input"
                                type={passwordShown ? "text" : "password"}
                                name="confirmPassword"
                                placeholder="confirm password"
                                value={accountData.confirmPassword}
                                onChange={handleChange}
                              />
                              {formErrors.confirmPassword && (
                                <div className="invalid-tooltip">
                                  {formErrors.confirmPassword}
                                </div>
                              )}
                            </div>
                          </div>
                        </div>
                        <div className="pull-right mt-3">
                          <button
                            className="btn btn-primary btn-sm"
                            onClick={handleSubmit}
                          >
                            Next step<i className="fi-chevron-right fs-sm ms-2"></i>
                          </button>
                        </div>
                      </>
                    ) : (
                      ""
                    )}

                    {category ? (
                      <>
                        <div className="bg-light rounded-3 p-3 mb-3">
                          <aside className="col-lg-12 col-md-5 pe-xl-4 mb-5">
                            <div className="multitab-action">
                              <div className="d-flex d-md-block d-lg-flex align-items-start pt-lg-2 mb-4">
                                <img
                                  width="48"
                                  height="48"
                                  className="rounded-circle getContact_picture"
                                  src={getContact?.image || Pic}
                                  alt="getContact"
                                />
                                <div className="pt-md-2 pt-lg-0 ps-3 ps-md-0 ps-lg-3">
                                  <h2 className="fs-lg mb-0">
                                    {getContact?.firstName} {getContact?.lastName}
                                  </h2>
                                  <ul className="list-unstyled fs-sm mt-2 mb-0 ps-0">
                                    {getContact?.phone ? (
                                      <>
                                        <li className="pb-0">
                                          <a
                                            className="nav-link fw-normal p-0"
                                            href={`tel:${getContact?.phone ?? ""}`}
                                          >
                                            <i className="fi-phone opacity-60 me-2"></i>
                                            {getContact?.phone}
                                          </a>
                                        </li>
                                      </>
                                    ) : (
                                      ""
                                    )}

                                    <li className="pb-2">
                                      <a
                                        className="nav-link fw-normal p-0"
                                        href={`mailto:${getContact?.email ?? ""}`}
                                      >
                                        <i className="fi-mail opacity-60 me-2"></i>
                                        {getContact?.email}
                                      </a>
                                    </li>
                                  </ul>
                                </div>
                              </div>
                              {agentId ? (
                                <div className="float-left text-center">
                                  <label className="btn btn-primary documentlabel">
                                    <input
                                      id="REPC_real_estate_purchase_contract"
                                      type="file"
                                      accept={acceptedFileTypes
                                        .map((type) => `.${type}`)
                                        .join(",")}
                                      name="file"
                                      onChange={handleFileUpload}
                                      value={getContact.file}
                                    />
                                    <i className="fi-plus me-2"></i> Add a Photo
                                  </label>
                                  {formErrors.image && (
                                    <div className="invalid-tooltip" style={{ backgroundColor: "#f6f3f800", float: "left" }}>
                                      {formErrors.image}
                                    </div>
                                  )}
                                </div>
                              ) : (
                                ""
                              )}
                            </div>
                          </aside>
                        </div>

                        <div className="col-md-12">
                          <div className="bg-light rounded-3 p-3  mb-3">
                            <h2 className="h4 mb-4">
                              {/* <i className="fi-info-circle text-primary fs-5 mt-n1 me-2"></i> */}
                              Contact
                            </h2>
                            <div className="row">
                              <div className="col-sm-6 mb-2">
                                <label className="form-label" for="pr-fn">
                                  Date of Birth (Required)
                                  {/* <span className="text-danger">*</span> */}
                                </label>
                                <input
                                  className="form-control form-control-lg"
                                  id="inline-form-input"
                                  type="date"
                                  name="date_birth"
                                  value={formValues.date_birth}
                                  onChange={handleNewContact}
                                />
                                {formErrors.date_birth && (
                                  <div className="invalid-tooltip">
                                    {formErrors.date_birth}
                                  </div>
                                )}
                              </div>

                              <div className="col-sm-6 mb-2">
                                <label className="form-label" for="pr-sn">
                                  Recruited By{" "}
                                  {/* <span className="text-danger">*</span> */}
                                </label>
                                <input
                                  className="form-control form-control-lg"
                                  id="inline-form-input"
                                  type="text"
                                  name="staff_recruiter"
                                  value={formValues.staff_recruiter}
                                  onChange={handleNewContact}
                                />

                                {isLoading ? (
                                  ""
                                ) : (
                                  <div className="activateaccountstaff_recruiter ">
                                    {results && results.length > 0
                                      ? results.map((post) => {
                                        return (
                                          <div className="activateaccountstaff">
                                            <ul
                                              className="list-group"
                                              onClick={(e) =>
                                                handleSearchTransaction(post)
                                              }
                                            >
                                              <li className="list-group-item d-flex justify-content-start align-items-center">
                                                <div>
                                                  <img
                                                    className="rounded-circle profile_picture"
                                                    src={post.image || Pic}
                                                    alt="Profile"
                                                    onError={e => profilepic(e)}
                                                  />
                                                  <br />
                                                </div>
                                                <div>
                                                  <strong>
                                                    {post.firstName}&nbsp;
                                                    {post.lastName}{" "}
                                                  </strong>
                                                  <br />
                                                  <strong>
                                                    {post.contactType
                                                      ? post.contactType
                                                        .charAt(0)
                                                        .toUpperCase() +
                                                      post.contactType.slice(1)
                                                      : ""}
                                                  </strong>
                                                  <br />
                                                </div>
                                              </li>
                                            </ul>
                                          </div>
                                        );
                                      })
                                      : ""}
                                  </div>
                                )}
                              </div>


                              <div className="col-sm-6 mb-2">
                                <label className="form-label" for="pr-fn">
                                  Address (Required)
                                  {/* <span className="text-danger">*</span> */}
                                </label>
                                <input
                                  className="form-control form-control-lg"
                                  id="inline-form-input"
                                  type="text"
                                  name="address"
                                  placeholder="Enter number, street, and apt. or suite no.)"
                                  value={formValues.address}
                                  onChange={handleNewContact}
                                />
                                {formErrors.address && (
                                  <div className="invalid-tooltip">
                                    {formErrors.address}
                                  </div>
                                )}
                              </div>

                              <div className="col-sm-6 mb-2">
                                <label className="form-label" for="pr-fn">
                                  City (Required)
                                  {/* <span className="text-danger">*</span> */}
                                </label>
                                <input
                                  className="form-control form-control-lg"
                                  id="inline-form-input"
                                  type="text"
                                  name="city"
                                  placeholder="Enter City "
                                  value={formValues.city}
                                  onChange={handleNewContact}
                                />
                                {formErrors.city && (
                                  <div className="invalid-tooltip">
                                    {formErrors.city}
                                  </div>
                                )}
                              </div>


                              <div className="col-sm-6 mb-2">
                                <label className="form-label" for="pr-fn">
                                  State (Required)
                                  {/* <span className="text-danger">*</span> */}
                                </label>
                                <input
                                  className="form-control form-control-lg"
                                  id="inline-form-input"
                                  type="text"
                                  name="state"
                                  placeholder="Enter State "
                                  value={formValues.state}
                                  onChange={handleNewContact}
                                />
                                {formErrors.state && (
                                  <div className="invalid-tooltip">
                                    {formErrors.state}
                                  </div>
                                )}
                              </div>

                              <div className="col-sm-6 mb-2">
                                <label className="form-label" for="pr-fn">
                                  Zipcode (Required)
                                  {/* <span className="text-danger">*</span> */}
                                </label>
                                <input
                                  className="form-control form-control-lg"
                                  id="inline-form-input"
                                  type="text"
                                  name="zip"
                                  placeholder="Enter Zipcode "
                                  value={formValues.zip}
                                  onChange={handleNewContact}
                                />
                                {formErrors.zip && (
                                  <div className="invalid-tooltip">
                                    {formErrors.zip}
                                  </div>
                                )}
                              </div>
                            </div>
                          </div>
                          {
                            formValuesNew.contactpermission === "referralonly" ?
                              ""
                              :
                              <div className="bg-light rounded-3 p-3  mb-3">
                                <h2 className="h4 mb-4">
                                  MLS & Board
                                </h2>
                                <div className="row">
                                  <div className="col-sm-6 mb-3">
                                    <label className="form-label" for="pr-sn">
                                      MLS Membership
                                    </label>
                                    <br />
                                    <small>Choose one only.  This should be your Primary MLS Membership</small>
                                    <MultiSelect
                                      options={[
                                        {
                                          label: `UtahRealEstate.com`,
                                          value: `UtahRealEstate.com`,
                                        },
                                        {
                                          label: `Park City`,
                                          value: `Park City`,
                                        },
                                        {
                                          label: `Washington County`,
                                          value: `Washington County`,
                                        },
                                        {
                                          label: `Iron County`,
                                          value: `Iron County`,
                                        },
                                        // {
                                        //   label: `Local MLS (No IDX)`,
                                        //   value: `Local MLS (No IDX)`,
                                        // },
                                        // { label: `Tooele County`, value: `Tooele County` },
                                        // { label: `Cache Valley`, value: `Cache Valley` },
                                        // { label: `Brigham-Tremonton Board`, value: `Brigham-Tremonton Board` }
                                      ]}

                                      value={mls_membership}
                                      onChange={setMls_membership}
                                      labelledBy="Select MLS"
                                      name="mls_membership"
                                      hasSelectAll={false}
                                      closeOnChangedValue={true}
                                    />
                                    {formErrors.mls_membership && (
                                      <div className="invalid-tooltip">
                                        {formErrors.mls_membership}
                                      </div>
                                    )}
                                  </div>

                                  <div className="col-sm-6 mb-3">
                                    <label className="form-label" for="pr-sn">
                                      Board and the Board Membership
                                    </label>
                                    <br />
                                    <small>Select one only.  This should be your Primary Board Membership</small>
                                    <MultiSelect2
                                      options={[
                                        {
                                          label: `Salt Lake Board`,
                                          value: `Salt Lake Board`,
                                        },
                                        {
                                          label: `Utah Central Association UCAR`,
                                          value: `Utah Central Association UCAR`,
                                        },
                                        {
                                          label: `Northern Wasatch`,
                                          value: `Northern Wasatch`,
                                        },
                                        {
                                          label: `Washington County`,
                                          value: `Washington County`,
                                        },
                                        {
                                          label: `Park City`,
                                          value: `Park City`,
                                        },
                                        {
                                          label: `Iron County`,
                                          value: `Iron County`,
                                        },
                                        {
                                          label: `Tooele County`,
                                          value: `Tooele County`,
                                        },
                                        {
                                          label: `Cache Valley`,
                                          value: `Cache Valley`,
                                        },
                                        {
                                          label: `Brigham-Tremonton Board`,
                                          value: `Brigham-Tremonton Board`,
                                        },
                                      ]}
                                      value={board_membership}
                                      onChange={setBoard_membership}
                                      labelledBy="Select Board Membership"
                                      name="board_membership"
                                      hasSelectAll={false}
                                      closeOnChangedValue={true}
                                    />
                                    {formErrors.board_membership && (
                                      <div className="invalid-tooltip">
                                        {formErrors.board_membership}
                                      </div>
                                    )}
                                  </div>

                                  {/* <h2 className="h4 mb-4">MLS Agent ID</h2> */}

                                  {mls_membership.some(
                                    (member) => member.value === "UtahRealEstate.com"
                                  ) && (
                                      <>
                                        <div className="col-sm-6 mb-3">
                                          <label className="form-label" for="pr-sn">
                                            UtahRealEstate.com Agent ID (Recommended)
                                          </label>
                                          <br />
                                          <small>Entering this number will feed your current listings over to BrokerAgentBase.com</small>
                                          <input
                                            className="form-control form-control-lg"
                                            id="inline-form-input"
                                            type="text"
                                            name="agent_id"
                                            placeholder="Enter Agent ID"
                                            value={formValues.agent_id}
                                            onChange={handleNewContact}
                                          />
                                        </div>
                                        <div className="col-md-12 mb-3">
                                          <p>
                                            <b>How can I find my agent ID</b>
                                          </p>
                                          <p>
                                            Following are the instructions to get your Agent
                                            ID...
                                            <br />
                                            To find your MLS ID login to Utahrealestate.com
                                            and click on “Search” then “Roster Search” and
                                            look up your name. Pull a “Brief Report” and you
                                            will see your agent ID. This will be a 5 or 6
                                            digit number.
                                          </p>
                                        </div>
                                      </>
                                    )}

                                  {mls_membership.some(
                                    (member) => member.value === "Park City"
                                  ) && (
                                      <>
                                        <div className="col-sm-6 mb-3">
                                          <label className="form-label">
                                            Park City Agent ID (Recommended)
                                          </label>
                                          <br />
                                          <small>Entering this number will feed your current listings over to BrokerAgentBase.com</small>
                                          <input
                                            className="form-control form-control-lg"
                                            id="inline-form-input"
                                            type="text"
                                            name="parkcityagent_id"
                                            placeholder="Enter Agent ID"
                                            value={formValues.parkcityagent_id}
                                            onChange={handleNewContact}
                                          />
                                        </div>
                                        <div className="col-md-12  mb-3">
                                          <p>
                                            <b>How can I find my agent ID</b>
                                          </p>
                                          <p>
                                            Following are the instructions to get your Agent
                                            ID...
                                            <br />
                                          </p>
                                        </div>
                                      </>
                                    )}

                                  {mls_membership.some(
                                    (member) => member.value === "Washington County"
                                  ) && (
                                      <>
                                        <div className="col-sm-6  mb-3">
                                          <label className="form-label">
                                            Washington County Agent ID (Recommended)
                                          </label>
                                          <br />
                                          <small>Entering this number will feed your current listings over to BrokerAgentBase.com</small>
                                          <input
                                            className="form-control form-control-lg"
                                            id="inline-form-input"
                                            type="text"
                                            name="washingtonagent_id"
                                            placeholder="Enter Agent ID"
                                            value={formValues.washingtonagent_id}
                                            onChange={handleNewContact}
                                          />
                                        </div>
                                        <div className="col-md-6 mb-3">
                                          <p>
                                            <b>How can I find my agent ID</b>
                                          </p>
                                          <p>
                                            Following are the instructions to get your Agent
                                            ID...
                                            <br />
                                          </p>
                                        </div>
                                      </>
                                    )}

                                  {mls_membership.some(
                                    (member) => member.value === "Iron County"
                                  ) && (
                                      <>
                                        <div className="col-sm-6  mb-3">
                                          <label className="form-label">
                                            Iron County Agent ID (Recommended)
                                          </label>
                                          <br />
                                          <small>Entering this number will feed your current listings over to BrokerAgentBase.com</small>
                                          <input
                                            className="form-control form-control-lg"
                                            id="inline-form-input"
                                            type="text"
                                            name="ironcountyagent_id"
                                            placeholder="Enter Agent ID"
                                            value={formValues.ironcountyagent_id}
                                            onChange={handleNewContact}
                                          />
                                        </div>
                                        <div className="col-md-12 mb-3">
                                          <p>
                                            <b>How can I find my agent ID</b>
                                          </p>
                                          <p>
                                            Following are the instructions to get your Agent
                                            ID...
                                            <br />
                                          </p>
                                        </div>
                                      </>
                                    )}
                                </div>
                              </div>
                          }

                          <div className="bg-light rounded-3 p-3  mb-3">
                            <h2 className="h4 mb-4">
                              {/* <i className="fi-info-circle text-primary fs-5 mt-n1 me-2"></i> */}
                              License
                            </h2>
                            <div className="row">
                              <div className="col-sm-6 mb-2">
                                <label className="form-label" for="pr-fn">
                                  Licensed Type (Required)
                                  {/* <span className="text-danger">*</span> */}
                                </label>
                                <select
                                  className="form-select"
                                  id="pr-city"
                                  name="license_type"
                                  value={formValues.license_type}
                                  onChange={handleNewContact}
                                >
                                  <option>--Select--</option>
                                  <option>Sales Agent</option>
                                  <option>Associate Broker</option>
                                  <option>Principal Broker</option>
                                  <option>Company</option>
                                </select>
                                {formErrors.license_type && (
                                  <div className="invalid-tooltip">
                                    {formErrors.license_type}
                                  </div>
                                )}
                              </div>

                              <div className="col-sm-6 mb-2">
                                <label className="form-label" for="pr-sn">
                                  Licensed Number (Required)
                                  {/* <span className="text-danger">*</span> */}
                                </label>
                                <input
                                  className="form-control form-control-lg"
                                  id="inline-form-input"
                                  type="text"
                                  name="license_number"
                                  value={formValues.license_number}
                                  onChange={handleNewContact}
                                />
                                {formErrors.license_number && (
                                  <div className="invalid-tooltip">
                                    {formErrors.license_number}
                                  </div>
                                )}
                              </div>

                              <div className="col-sm-6 mb-2">
                                <label className="form-label" for="pr-sn">
                                  Licensed Expires (Required)
                                  {/* <span className="text-danger">*</span> */}
                                </label>
                                <input
                                  className="form-control form-control-lg"
                                  id="inline-form-input"
                                  type="date"
                                  name="date_expire"
                                  value={formValues.date_expire}
                                  onChange={handleNewContact}
                                />
                                {formErrors.date_expire && (
                                  <div className="invalid-tooltip">
                                    {formErrors.date_expire}
                                  </div>
                                )}
                              </div>

                              <div className="col-sm-6 mb-2">
                                <label className="form-label" for="pr-sn">
                                  Licensed Issue State (Required)
                                  {/* <span className="text-danger">*</span> */}
                                </label>
                                <select
                                  className="form-select"
                                  id="pr-city"
                                  name="license_state"
                                  onChange={handleNewContact}
                                  value={formValues.license_state}
                                >
                                  <option value="0">--Select--</option>
                                  {options
                                    .find((option) => option.code === "UT")
                                    .districts.map((district, key) => (
                                      <option value={district} key={key}>
                                        {district}
                                      </option>
                                    ))}
                                </select>
                                {formErrors.license_state && (
                                  <div className="invalid-tooltip">
                                    {formErrors.license_state}
                                  </div>
                                )}
                              </div>

                              <div className="col-sm-6 mb-2">
                                <label className="form-label" for="pr-sn">
                                  NRDS ID (Recommended)
                                  <a href="https://login.connect.realtor/#!/forgotmember" target="_blank"><span className="text-danger"> Look up your NRDS number</span></a>
                                </label>
                                <input
                                  className="form-control form-control-lg"
                                  id="inline-form-input"
                                  type="text"
                                  name="nrds_id"
                                  value={formValues.nrds_id}
                                  onChange={handleNewContact}
                                />
                              </div>
                            </div>
                          </div>

                          <div className="bg-light rounded-3 p-3  mb-3">
                            <h2 className="h4 mb-4">
                              {/* <i className="fi-info-circle text-primary fs-5 mt-n1 me-2"></i> */}
                              Legal
                            </h2>
                            <div className="row">
                              {/* <div className="col-sm-6 mb-2">
                          <label className="form-label" for="pr-fn">
                            SSN / ITIN (Recommended)
                          </label>
                          <input
                            className="form-control form-control-lg"
                            id="inline-form-input"
                            type="text"
                            name="ssn_itin"
                            value={formValues.ssn_itin}
                            onChange={handleNewContact}
                          />
                        </div>

                        <div className="col-sm-6 mb-2">
                          <label className="form-label" for="pr-sn">
                            EIN / DBA (Recommended)
                          </label>
                          <input
                            className="form-control form-control-lg"
                            id="inline-form-input"
                            type="text"
                            name="agentTax_ein"
                            value={formValues.agentTax_ein}
                            onChange={handleNewContact}
                          />
                        </div> */}


                              <div className="col-lg-6 mb-3">
                                <label className="form-label" for="pr-city">
                                  Driver License / State ID / Passport
                                </label>
                                <br />
                                <small>&nbsp;</small>
                                <div className="row">
                                  <div className="col-lg-6 mb-3 ">

                                    <select
                                      className="form-select"
                                      id="pr-city"
                                      name="private_idcard_type"
                                      value={formValues.private_idcard_type}
                                      onChange={handleNewContact}
                                    >
                                      <option value="">--Select Any ID Option --</option>
                                      <option>Driver License</option>
                                      <option>Military ID</option>
                                      <option>Passport</option>
                                    </select>
                                    {formErrors.private_idcard_type && (
                                      <div className="invalid-tooltip">
                                        {formErrors.private_idcard_type}
                                      </div>
                                    )}
                                  </div>

                                  {
                                    formValues.private_idcard_type === "Passport" ?
                                      ""
                                      :
                                      <div className="col-lg-6 mb-3">
                                        <select
                                          className="form-select"
                                          id="pr-city"
                                          name="private_idstate"
                                          onChange={handleNewContact}
                                          value={formValues.private_idstate}
                                        >
                                          <option value="">--Select State --</option>
                                          {options
                                            .find((option) => option.code === "UT")
                                            .districts.map((district, key) => (
                                              <option value={district} key={key}>
                                                {district}
                                              </option>
                                            ))}
                                        </select>
                                        {formErrors.private_idstate && (
                                          <div className="invalid-tooltip">
                                            {formErrors.private_idstate}
                                          </div>
                                        )}
                                      </div>
                                  }
                                  <div className="col-lg-12 mb-3">
                                    <input
                                      className="form-control"
                                      id="inline-form-input"
                                      type="text"
                                      name="private_idnumber"
                                      placeholder="Enter ID Number"
                                      value={formValues.private_idnumber}
                                      onChange={handleNewContact}
                                    />
                                    {formErrors.private_idnumber && (
                                      <div className="invalid-tooltip">
                                        {formErrors.private_idnumber}
                                      </div>
                                    )}
                                  </div>
                                </div>
                              </div>

                              <div className="col-sm-6 mb-2">
                                <label className="form-label" for="pr-sn">
                                  Transfer From (Required)
                                </label>
                                <br />
                                <small> Please enter the name of the last brokerage you were licensed with.  If your a new agent then put “None”
                                </small>
                                <input
                                  className="form-control form-control-lg"
                                  id="inline-form-input"
                                  type="text"
                                  name="admin_note"
                                  value={formValues.admin_note}
                                  onChange={handleNewContact}
                                />
                                {formErrors.admin_note && (
                                  <div className="invalid-tooltip">
                                    {formErrors.admin_note}
                                  </div>
                                )}
                              </div>
                            </div>
                          </div>

                          <div className="pull-right mt-3">
                            <button
                              className="btn btn-primary btn-sm"
                              onClick={handleAccount}
                            >
                              Next step
                              <i className="fi-chevron-right fs-sm ms-2"></i>
                            </button>
                            {/* <button
                          className="btn btn-primary btn-sm ms-3"
                          onClick={handleSkip}
                        >
                          Skip
                        </button> */}
                          </div>
                        </div>
                      </>
                    ) : (
                      ""
                    )}

                    {phase ? (
                      <div className="">
                        <div className="bg-light border rounded-3 p-3 mb-3">
                          <aside className="col-lg-12 col-md-5 pe-xl-4 mb-5">
                            <div className="multitab-action">
                              <div className="d-flex d-md-block d-lg-flex align-items-start pt-lg-2 mb-4">
                                <img
                                  width="48"
                                  height="48"
                                  className="rounded-circle getContact_picture"
                                  src={getContact?.image || Pic}
                                  alt="getContact"
                                />
                                <div className="pt-md-2 pt-lg-0 ps-3 ps-md-0 ps-lg-3">
                                  <h2 className="fs-lg mb-0">
                                    {getContact?.firstName} {getContact?.lastName}
                                  </h2>
                                  <ul className="list-unstyled fs-sm mt-2 mb-0 ps-0">
                                    <li className="pb-0">
                                      <a
                                        className="nav-link fw-normal p-0"
                                        href={`tel:${getContact?.phone ?? ""}`}
                                      >
                                        <i className="fi-phone opacity-60 me-2"></i>
                                        {getContact?.phone}
                                      </a>
                                    </li>
                                    <li className="pb-2">
                                      <a
                                        className="nav-link fw-normal p-0"
                                        href={`mailto:${getContact?.email ?? ""}`}
                                      >
                                        <i className="fi-mail opacity-60 me-2"></i>
                                        {getContact?.email}
                                      </a>
                                    </li>
                                  </ul>
                                </div>
                              </div>
                              {agentId ? (
                                <div className="float-left text-center">
                                  <label className="btn btn-primary documentlabel">
                                    <input
                                      id="REPC_real_estate_purchase_contract"
                                      type="file"
                                      accept={acceptedFileTypes
                                        .map((type) => `.${type}`)
                                        .join(",")}
                                      name="file"
                                      onChange={handleFileUpload}
                                      value={getContact.file}
                                    />
                                    <i className="fi-plus me-2"></i> Add a Photo
                                  </label>
                                </div>
                              ) : (
                                ""
                              )}
                            </div>
                          </aside>
                        </div>

                        <div className="col-md-12">
                          <div className="bg-light border rounded-3 p-3">
                            <h2 className="h4 mb-4">
                              {/* <i className="fi-info-circle text-primary fs-5 mt-n1 me-2"></i> */}
                              About Me
                            </h2>
                            <div className="row">
                              <div className="col-sm-6 mb-2">
                                <label className="form-label" for="pr-fn">
                                  Full Name
                                </label>
                                <input
                                  className="form-control form-control-lg"
                                  id="inline-form-input"
                                  type="text"
                                  name="full_name"
                                  value={formValuesAboout?.full_name}
                                  onChange={handleChangesAbout}
                                />
                              </div>

                              <div className="col-sm-6 mb-2">
                                <label className="form-label" for="pr-sn">
                                  Birthday
                                  {/* <span className="text-danger">*</span> */}
                                </label>
                                <input
                                  className="form-control form-control-lg"
                                  id="inline-form-input"
                                  type="date"
                                  name="birthday"
                                  value={formValuesAboout?.birthday}
                                  onChange={handleChangesAbout}
                                />
                              </div>

                              <div className="col-sm-6 mb-2">
                                <label className="form-label" for="pr-sn">
                                  Ideally, how would you spend your birthday?
                                </label>
                                <input
                                  className="form-control form-control-lg"
                                  id="inline-form-input"
                                  type="text"
                                  name="spend_birthday"
                                  value={formValues.spend_birthday}
                                  onChange={handleChangesAbout}
                                />
                              </div>

                              <div className="col-sm-6 mb-2">
                                <label className="form-label" for="pr-sn">
                                  Spouse/Partner Name and Birthday?
                                </label>
                                <input
                                  className="form-control form-control-lg"
                                  id="inline-form-input"
                                  type="text"
                                  name="spouse"
                                  value={formValues.spouse}
                                  onChange={handleChangesAbout}
                                />
                              </div>

                              <div className="col-sm-6 mb-2">
                                <label className="form-label" for="pr-sn">
                                  Your t-shirt size
                                  {/* <span className="text-danger">*</span> */}
                                </label>
                                <select
                                  className="form-select"
                                  id="pr-city"
                                  name="shirt_size"
                                  value={formValues.shirt_size}
                                  onChange={handleChangesAbout}
                                >
                                  <option>--Select--</option>
                                  <option value="xs">XS</option>
                                  <option value="s">S</option>
                                  <option value="m">M</option>
                                  <option value="l">L</option>
                                  <option value="xl">XL</option>
                                  <option value="xxl">XXL</option>
                                </select>
                              </div>

                              <div className="col-sm-6 mb-2">
                                <label className="form-label" for="pr-sn">
                                  What’s your favorite way to spend a day off?
                                </label>
                                <input
                                  className="form-control form-control-lg"
                                  id="inline-form-input"
                                  type="text"
                                  name="spend_day"
                                  value={formValues.spend_day}
                                  onChange={handleChangesAbout}
                                />
                              </div>

                              <div className="col-sm-6 mb-2">
                                <label className="form-label" for="pr-sn">
                                  What’s one professional skill you’re currently
                                  working on?
                                </label>
                                <input
                                  className="form-control form-control-lg"
                                  id="inline-form-input"
                                  type="text"
                                  name="professional_skill"
                                  value={formValues.professional_skill}
                                  onChange={handleChangesAbout}
                                />
                              </div>

                              <div className="col-sm-6 mb-2">
                                <label className="form-label" for="pr-sn">
                                  What are your hobbies, and how did you get into
                                  them?
                                </label>
                                <input
                                  className="form-control form-control-lg"
                                  id="inline-form-input"
                                  type="text"
                                  name="hobbies"
                                  value={formValues.hobbies}
                                  onChange={handleChangesAbout}
                                />
                              </div>

                              <div className="col-sm-6 mb-2">
                                <label className="form-label" for="pr-sn">
                                  What was the best vacation you ever took and why?
                                </label>
                                <input
                                  className="form-control form-control-lg"
                                  id="inline-form-input"
                                  type="text"
                                  name="vacation"
                                  value={formValues.vacation}
                                  onChange={handleChangesAbout}
                                />
                              </div>

                              <div className="col-sm-6 mb-2">
                                <label className="form-label" for="pr-sn">
                                  Where’s the next place on your travel bucket list
                                  and why?
                                </label>
                                <input
                                  className="form-control form-control-lg"
                                  id="inline-form-input"
                                  type="text"
                                  name="travel_bucket"
                                  value={formValues.travel_bucket}
                                  onChange={handleChangesAbout}
                                />
                              </div>

                              <div className="col-sm-6 mb-2">
                                <label className="form-label" for="pr-sn">
                                  What's the phone app or program you use most?
                                </label>
                                <input
                                  className="form-control form-control-lg"
                                  id="inline-form-input"
                                  type="text"
                                  name="phone_app"
                                  value={formValues.phone_app}
                                  onChange={handleChangesAbout}
                                />
                              </div>

                              <div className="col-sm-6 mb-2">
                                <label className="form-label" for="pr-sn">
                                  Favorite book and or podcast?
                                </label>
                                <input
                                  className="form-control form-control-lg"
                                  id="inline-form-input"
                                  type="text"
                                  name="book"
                                  value={formValues.book}
                                  onChange={handleChangesAbout}
                                />
                              </div>

                              <div className="col-sm-6 mb-2">
                                <label className="form-label" for="pr-sn">
                                  What type of music are you into?
                                </label>
                                <input
                                  className="form-control form-control-lg"
                                  id="inline-form-input"
                                  type="text"
                                  name="music"
                                  value={formValues.music}
                                  onChange={handleChangesAbout}
                                />
                              </div>

                              <div className="col-sm-6 mb-2">
                                <label className="form-label" for="pr-sn">
                                  Favorite Restaurant?
                                </label>
                                <input
                                  className="form-control form-control-lg"
                                  id="inline-form-input"
                                  type="text"
                                  name="food"
                                  value={formValues.food}
                                  onChange={handleChangesAbout}
                                />
                              </div>

                              <div className="col-sm-6 mb-2">
                                <label className="form-label" for="pr-sn">
                                  Favorite Dessert/Candy/Drink?
                                </label>
                                <input
                                  className="form-control form-control-lg"
                                  id="inline-form-input"
                                  type="text"
                                  name="dessert"
                                  value={formValues.dessert}
                                  onChange={handleChangesAbout}
                                />
                              </div>

                              <div className="col-sm-6 mb-2">
                                <label className="form-label" for="pr-sn">
                                  Anything else you'd like to share?
                                </label>
                                <input
                                  className="form-control form-control-lg"
                                  id="inline-form-input"
                                  type="text"
                                  name="anything_else"
                                  value={formValues.anything_else}
                                  onChange={handleChangesAbout}
                                />
                              </div>
                            </div>
                          </div>
                          <div className="pull-right mt-3">
                            <button
                              className="btn btn-primary btn-sm"
                              onClick={handleSubmitAbout}
                            >
                              Next step
                              <i className="fi-chevron-right fs-sm ms-2"></i>
                            </button>
                            <button
                              className="btn btn-primary btn-sm ms-3"
                              onClick={handleSkipNew}
                            >
                              Skip
                            </button>
                          </div>
                        </div>
                      </div>
                    ) : (
                      ""
                    )}

                    {step4 ? (
                      <>
                        {/* <h2 className="h4 mb-4">
                        <i className="fi-info-circle text-primary fs-5 mt-n1 me-2"></i>
                        Basic info
                      </h2> */}
                        <div className="">
                          <div className="bg-light rounded-3 p-3  mb-3">
                            <div className="row">
                              {/* <iframe id="pdfPreviewIframe" title="PDF Preview" width="100%" height="500px" /> */}
                              {/* <div className="onboard_document mb-4">
                              
                          <Document file="https://brokeragentbase.s3.amazonaws.com/assets/IRS+W9.pdf" onLoadSuccess={handleLoadSuccess}>
                            {Array.from(new Array(5), (el, index) => (
                              <div key={index} style={{ position: 'relative' }}>
                                <Page pageNumber={index + 1} />
                              
                              </div>
                            ))}
                          </Document>
                            </div> */}

                              <div className="col-md-4">
                                <label className="col-form-label float-left w-100" for="pr-fn">Please complete the following form to complete signup.</label>

                                <div className="row">
                                  <div className="col-sm-12 mb-2">
                                    <label className="form-label" for="pr-fn">
                                      1. Name
                                    </label>
                                    <p className="mb-0">
                                      (as shown on your income tax return). Name is
                                      required on this line; do not leave this line
                                      blank.
                                    </p>
                                    <input
                                      className="form-control form-control-lg"
                                      id="inline-form-input"
                                      type="text"
                                      name="topmostSubform[0].Page1[0].f1_1[0]"
                                      // value={formValuesAboout?.topmostSubform[0].Page1[0].f1_1[0]}
                                      onChange={(e) =>
                                        handleFieldChange(
                                          "topmostSubform[0].Page1[0].f1_1[0]",
                                          e.target.value
                                        )
                                      }
                                    />
                                    {formErrors['topmostSubform[0].Page1[0].f1_1[0]'] && (
                                      <div className="invalid-tooltip">
                                        {formErrors['topmostSubform[0].Page1[0].f1_1[0]']}
                                      </div>
                                    )}
                                  </div>
                                  <div className="col-sm-12 mb-2">
                                    <label className="form-label" for="pr-fn">
                                      2. Business
                                    </label>
                                    <p className="mb-0">
                                      name/disregarded entity name, if different from
                                      above
                                    </p>
                                    <input
                                      className="form-control form-control-lg"
                                      id="inline-form-input"
                                      type="text"
                                      name="topmostSubform[0].Page1[0].f1_2[0]"
                                      // value={formValuesAboout?.topmostSubform[0].Page1[0].f1_2[0]}
                                      onChange={(e) =>
                                        handleFieldChange(
                                          "topmostSubform[0].Page1[0].f1_2[0]",
                                          e.target.value
                                        )
                                      }
                                    />
                                    {formErrors['topmostSubform[0].Page1[0].f1_2[0]'] && (
                                      <div className="invalid-tooltip">
                                        {formErrors['topmostSubform[0].Page1[0].f1_2[0]']}
                                      </div>
                                    )}
                                  </div>
                                  <div className="col-sm-12 mb-2 federal_tax_classification">
                                    <label
                                      className="form-label float-left w-100 pt-0"
                                      for="pr-fn"
                                    >
                                      3. Check appropriate box for federal tax
                                      classification of the person whose name is
                                      entered on line 1. Check only one of the
                                      following seven boxes.
                                    </label>
                                  </div>
                                  <div className="row">
                                    <div className="col-sm-12 mb-2 d-flex">
                                      <input
                                        className="form-check-input"
                                        type="checkbox"
                                        name="topmostSubform[0].Page1[0].FederalClassification[0].c1_1[0]"
                                        onChange={(e) =>
                                          handleFieldChange(
                                            "topmostSubform[0].Page1[0].FederalClassification[0].c1_1[0]",
                                            e.target.checked
                                          )
                                        }
                                        checked={
                                          fieldValues[
                                          "topmostSubform[0].Page1[0].FederalClassification[0].c1_1[0]"
                                          ]
                                        }
                                      />
                                      &nbsp;
                                      <p className="float-left mb-0 d-flex align-items-center">
                                        Individual/sole proprietor or single-member
                                        LLC
                                      </p>
                                    </div>
                                    <div className="col-sm-12 mb-2 d-flex">
                                      <input
                                        className="form-check-input"
                                        type="checkbox"
                                        name="topmostSubform[0].Page1[0].FederalClassification[0].c1_1[1]"
                                        onChange={(e) =>
                                          handleFieldChange(
                                            "topmostSubform[0].Page1[0].FederalClassification[0].c1_1[1]",
                                            e.target.checked
                                          )
                                        }
                                        checked={
                                          fieldValues[
                                          "topmostSubform[0].Page1[0].FederalClassification[0].c1_1[1]"
                                          ]
                                        }
                                      />
                                      &nbsp;
                                      <p className="float-left mb-0 d-flex align-items-center">
                                        C Corporation{" "}
                                      </p>
                                    </div>
                                    <div className="col-sm-12 mb-2 d-flex">
                                      <input
                                        className="form-check-input"
                                        type="checkbox"
                                        name="topmostSubform[0].Page1[0].FederalClassification[0].c1_1[2]"
                                        onChange={(e) =>
                                          handleFieldChange(
                                            "topmostSubform[0].Page1[0].FederalClassification[0].c1_1[2]",
                                            e.target.checked
                                          )
                                        }
                                        checked={
                                          fieldValues[
                                          "topmostSubform[0].Page1[0].FederalClassification[0].c1_1[2]"
                                          ]
                                        }
                                      />
                                      &nbsp;
                                      <p className="float-left mb-0 d-flex align-items-center">
                                        S Corporation{" "}
                                      </p>
                                    </div>
                                    <div className="col-sm-12 mb-2 d-flex">
                                      <input
                                        className="form-check-input"
                                        type="checkbox"
                                        name="topmostSubform[0].Page1[0].FederalClassification[0].c1_1[3]"
                                        onChange={(e) =>
                                          handleFieldChange(
                                            "topmostSubform[0].Page1[0].FederalClassification[0].c1_1[3]",
                                            e.target.checked
                                          )
                                        }
                                        checked={
                                          fieldValues[
                                          "topmostSubform[0].Page1[0].FederalClassification[0].c1_1[3]"
                                          ]
                                        }
                                      />
                                      &nbsp;
                                      <p className="float-left mb-0 d-flex align-items-center">
                                        Partnership{" "}
                                      </p>
                                    </div>

                                    <div className="col-sm-12 mb-2">
                                      <div className="col-sm-12 d-flex">
                                        <input
                                          className="form-check-input"
                                          type="checkbox"
                                          name="topmostSubform[0].Page1[0].FederalClassification[0].c1_1[5]"
                                          onChange={(e) =>
                                            handleFieldChange(
                                              "topmostSubform[0].Page1[0].FederalClassification[0].c1_1[5]",
                                              e.target.checked
                                            )
                                          }
                                          checked={
                                            fieldValues[
                                            "topmostSubform[0].Page1[0].FederalClassification[0].c1_1[5]"
                                            ]
                                          }
                                        />
                                        &nbsp;
                                        <p className="float-left mb-0 d-flex align-items-center">
                                          Limited liability company. Enter the tax
                                          classification (C=C corporation, S=S
                                          corporation, P=Partnership){" "}
                                        </p>
                                      </div>
                                      <input
                                        className="form-control form-control-lg ms-4"
                                        id="inline-form-input"
                                        type="text"
                                        maxlength="1"
                                        name="topmostSubform[0].Page1[0].FederalClassification[0].f1_3[0]"
                                        // value={formValuesAboout?.topmostSubform[0].Page1[0].FederalClassification[0].f1_3[0]}
                                        onChange={(e) =>
                                          handleFieldChange(
                                            "topmostSubform[0].Page1[0].FederalClassification[0].f1_3[0]",
                                            e.target.value
                                          )
                                        }
                                      />
                                    </div>

                                    <div className="col-sm-12 mb-2">
                                      <div className="col-sm-12 d-flex">
                                        <input
                                          className="form-check-input"
                                          type="checkbox"
                                          name="topmostSubform[0].Page1[0].FederalClassification[0].c1_1[6]"
                                          onChange={(e) =>
                                            handleFieldChange(
                                              "topmostSubform[0].Page1[0].FederalClassification[0].c1_1[6]",
                                              e.target.checked
                                            )
                                          }
                                          checked={
                                            fieldValues[
                                            "topmostSubform[0].Page1[0].FederalClassification[0].c1_1[6]"
                                            ]
                                          }
                                        />
                                        &nbsp;
                                        <p className="mb-0 d-flex align-items-center">
                                          Other (see instructions){" "}
                                        </p>
                                      </div>
                                      <input
                                        className="form-control form-control-lg ms-4"
                                        id="inline-form-input"
                                        type="text"
                                        name="topmostSubform[0].Page1[0].FederalClassification[0].f1_4[0]"
                                        // value={formValuesAboout?.topmostSubform[0].Page1[0].FederalClassification[0].f1_4[0]}
                                        onChange={(e) =>
                                          handleFieldChange(
                                            "topmostSubform[0].Page1[0].FederalClassification[0].f1_4[0]",
                                            e.target.value
                                          )
                                        }
                                      />
                                    </div>
                                    <div className="col-sm-12 mb-2 d-flex">
                                      <input
                                        className="form-check-input"
                                        type="checkbox"
                                        name="topmostSubform[0].Page1[0].FederalClassification[0].c1_1[4]"
                                        onChange={(e) =>
                                          handleFieldChange(
                                            "topmostSubform[0].Page1[0].FederalClassification[0].c1_1[4]",
                                            e.target.checked
                                          )
                                        }
                                        checked={
                                          fieldValues[
                                          "topmostSubform[0].Page1[0].FederalClassification[0].c1_1[4]"
                                          ]
                                        }
                                      />
                                      &nbsp;
                                      <p className="mb-0">Trust/estate </p>
                                    </div>
                                  </div>


                                  <div className="col-sm-12 mb-2">
                                    <label className="form-label" for="pr-fn">
                                      4. Exemptions (codes apply only to certain entities, not individuals; see instructions on page 3):
                                    </label>
                                    <p className="mb-0">Exempt payee code (if any)</p>
                                    <input
                                      className="form-control form-control-lg"
                                      id="inline-form-input"
                                      type="text"
                                      name="topmostSubform[0].Page1[0].Exemptions[0].f1_5[0]"
                                      // value={formValuesAboout?.topmostSubform[0].Page1[0].Exemptions[0].f1_5[0]}
                                      onChange={(e) =>
                                        handleFieldChange(
                                          "topmostSubform[0].Page1[0].Exemptions[0].f1_5[0]",
                                          e.target.value
                                        )
                                      }
                                    />
                                    <p className="mb-0">Exemption from FATCA reporting code (if any)</p>
                                    <input
                                      className="form-control form-control-lg"
                                      id="inline-form-input"
                                      type="text"
                                      name="topmostSubform[0].Page1[0].Exemptions[0].f1_6[0]"
                                      // value={formValuesAboout?.topmostSubform[0].Page1[0].Exemptions[0].f1_6[0]}
                                      onChange={(e) =>
                                        handleFieldChange(
                                          "topmostSubform[0].Page1[0].Exemptions[0].f1_6[0]",
                                          e.target.value
                                        )
                                      }
                                    />
                                  </div>

                                  <div className="col-sm-12 mb-2">
                                    <label className="form-label" for="pr-fn">
                                      5. Address
                                    </label>
                                    {/* <p className="mb-0">(number, street, and apt. or suite no.) See instructions.</p> */}
                                    <input
                                      className="form-control form-control-lg"
                                      id="inline-form-input"
                                      type="text"
                                      name="topmostSubform[0].Page1[0].Address[0].f1_7[0]"
                                      // value={formValuesAboout?.topmostSubform[0].Page1[0].Address[0].f1_7[0]}
                                      onChange={(e) =>
                                        handleFieldChange(
                                          "topmostSubform[0].Page1[0].Address[0].f1_7[0]",
                                          e.target.value
                                        )
                                      }
                                    />
                                    {formErrors['topmostSubform[0].Page1[0].Address[0].f1_7[0]'] && (
                                      <div className="invalid-tooltip">
                                        {formErrors['topmostSubform[0].Page1[0].Address[0].f1_7[0]']}
                                      </div>
                                    )}
                                  </div>

                                  <div className="col-sm-12 mb-2">
                                    <label className="form-label" for="pr-fn">
                                      6. City, state, and ZIP code
                                    </label>
                                    <p className="mb-0"></p>
                                    <input
                                      className="form-control form-control-lg"
                                      id="inline-form-input"
                                      type="text"
                                      name="topmostSubform[0].Page1[0].Address[0].f1_8[0]"
                                      // value={formValuesAboout?.topmostSubform[0].Page1[0].Address[0].f1_8[0]}
                                      onChange={(e) =>
                                        handleFieldChange(
                                          "topmostSubform[0].Page1[0].Address[0].f1_8[0]",
                                          e.target.value
                                        )
                                      }
                                    />
                                    {formErrors['topmostSubform[0].Page1[0].Address[0].f1_8[0]'] && (
                                      <div className="invalid-tooltip">
                                        {formErrors['topmostSubform[0].Page1[0].Address[0].f1_8[0]']}
                                      </div>
                                    )}
                                  </div>

                                  <div className="col-sm-12 mb-2">
                                    <label className="form-label" for="pr-fn">
                                      7. List account number(s) here (optional)
                                    </label>
                                    <input
                                      className="form-control form-control-lg"
                                      id="inline-form-input"
                                      type="text"
                                      name="topmostSubform[0].Page1[0].f1_9[0]"
                                      // value={formValuesAboout?.topmostSubform[0].Page1[0].f1_9[0]}
                                      onChange={(e) =>
                                        handleFieldChange(
                                          "topmostSubform[0].Page1[0].f1_9[0]",
                                          e.target.value
                                        )
                                      }
                                    />
                                  </div>

                                  <div className="col-sm-12 mb-2">
                                    <label className="form-label" for="pr-fn">
                                      *. Requester’s name and address (optional)
                                    </label>
                                    <input
                                      className="form-control form-control-lg"
                                      id="inline-form-input"
                                      type="text"
                                      name="topmostSubform[0].Page1[0].f1_10[0]"
                                      // value={formValuesAboout?.topmostSubform[0].Page1[0].f1_10[0]}
                                      onChange={(e) =>
                                        handleFieldChange(
                                          "topmostSubform[0].Page1[0].f1_10[0]",
                                          e.target.value
                                        )
                                      }
                                    />
                                  </div>

                                  <div className="col-sm-12 mb-2">
                                    <label className="form-label" for="pr-fn">
                                      *. Social Security Number
                                    </label>
                                    <div className="row">
                                      <div className="col-md-4">
                                        <input
                                          className="form-control form-control-lg"
                                          id="inline-form-input"
                                          type="text"
                                          maxlength="3"
                                          name="topmostSubform[0].Page1[0].SSN[0].f1_11[0]"
                                          // value={formValuesAboout?.topmostSubform[0].Page1[0].SSN[0].f1_11[0]}
                                          onChange={(e) =>
                                            handleFieldChange(
                                              "topmostSubform[0].Page1[0].SSN[0].f1_11[0]",
                                              e.target.value
                                            )
                                          }
                                        />
                                      </div>
                                      <div className="col-md-4">
                                        <input
                                          className="form-control form-control-lg"
                                          id="inline-form-input"
                                          type="text"
                                          maxlength="2"
                                          name="topmostSubform[0].Page1[0].SSN[0].f1_12[0]"
                                          // value={formValuesAboout?.topmostSubform[0].Page1[0].SSN[0].f1_12[0]}
                                          onChange={(e) =>
                                            handleFieldChange(
                                              "topmostSubform[0].Page1[0].SSN[0].f1_12[0]",
                                              e.target.value
                                            )
                                          }
                                        />
                                      </div>
                                      <div className="col-md-4">
                                        <input
                                          className="form-control form-control-lg"
                                          id="inline-form-input"
                                          type="text"
                                          maxlength="4"
                                          name="topmostSubform[0].Page1[0].SSN[0].f1_13[0]"
                                          // value={formValuesAboout?.topmostSubform[0].Page1[0].SSN[0].f1_13[0]}
                                          onChange={(e) =>
                                            handleFieldChange(
                                              "topmostSubform[0].Page1[0].SSN[0].f1_13[0]",
                                              e.target.value
                                            )
                                          }
                                        />
                                      </div>
                                      {/* {formErrors['topmostSubform[0].Page1[0].SSN[0].f1_13[0]'] && (
                                <div className="invalid-tooltip">
                                  {formErrors['topmostSubform[0].Page1[0].SSN[0].f1_13[0]']}
                                </div>
                              )} */}
                                    </div>
                                  </div>
                                  <p>or</p>
                                  <div className="col-sm-12 mb-2">
                                    <label className="form-label" for="pr-fn">
                                      *. Employer identification number
                                    </label>
                                    <div className="row">
                                      <div className="col-md-6">
                                        <input
                                          className="form-control form-control-lg"
                                          id="inline-form-input"
                                          type="text"
                                          maxlength="2"
                                          name="topmostSubform[0].Page1[0].EmployerID[0].f1_14[0]"
                                          // value={formValuesAboout?.topmostSubform[0].Page1[0].EmployerID[0].f1_14[0]}
                                          onChange={(e) =>
                                            handleFieldChange(
                                              "topmostSubform[0].Page1[0].EmployerID[0].f1_14[0]",
                                              e.target.value
                                            )
                                          }
                                        />
                                      </div>

                                      <div className="col-md-6">
                                        <input
                                          className="form-control form-control-lg"
                                          id="inline-form-input"
                                          type="text"
                                          maxlength="7"
                                          name="topmostSubform[0].Page1[0].EmployerID[0].f1_15[0]"
                                          // value={formValuesAboout?.topmostSubform[0].Page1[0].EmployerID[0].f1_15[0]}
                                          onChange={(e) =>
                                            handleFieldChange(
                                              "topmostSubform[0].Page1[0].EmployerID[0].f1_15[0]",
                                              e.target.value
                                            )
                                          }
                                        />
                                      </div>
                                    </div>
                                  </div>
                                  {formErrors['ssn'] && (
                                    <div className="invalid-tooltip">
                                      {formErrors['ssn']}
                                    </div>
                                  )}
                                </div>
                              </div>
                              <div className="col-md-8">
                                <div className="onboard_iframe">
                                  <iframe className="" src={PDF_SRC} />
                                </div>

                                <>
                                  {Object.keys(signatures).length > 0 ? (
                                    ""
                                  ) : (
                                    <div className="canvas_signature w-100">
                                      <p className="mt-3"><b>Please Sign here </b></p>
                                      <SignatureCanvas
                                        ref={signatureCanvasRef}
                                        canvasProps={{
                                          className: "signature-canvas w-100",
                                        }}
                                      />
                                      <div className="float-left w-100 mt-3">
                                        <button
                                          className="btn btn-primary btn-sm"
                                          onClick={handleClearSignature}
                                        >
                                          Clear Signature
                                        </button>
                                        <button
                                          className="btn btn-primary btn-sm ms-3"
                                          onClick={handleSaveSignature}
                                        >
                                          Save Signature
                                        </button>
                                      </div>
                                    </div>
                                  )}
                                </>

                              </div>
                            </div>
                          </div>
                          {
                            Object.keys(signatures).length > 0 ? (
                              <div className="pull-left mt-3">
                                <button
                                  className="btn btn-primary btn-sm"
                                  // onClick={handleAddTexterm}
                                  onClick={handleFillAndGeneratePdf}
                                  disabled={isdone}
                                >
                                  {isdone ? "Please wait..." : "Next"}

                                  <i className="fi-chevron-right fs-sm ms-2"></i>
                                </button>

                                {/* <button className="btn btn-primary btn-sm" onClick={handleFillAndGeneratePdf}>
                                Fill and Generate PDF
                              </button> */}
                              </div>
                            )
                              : (
                                ""
                              )
                          }

                        </div>
                      </>
                    ) : (
                      ""
                    )}



                    {step5 ? (
                      <>
                        <div className="bg-light border rounded-3 p-3 mb-3">
                          {
                            formValuesNew.contactpermission === "referralonly" ?
                              <>

                                {referralAgreementdone === true ?
                                  <div className="row">
                                    <>
                                      <div className="thank_you row align-items-center mt-md-2 text-center">
                                        {/* <div className="col-lg-7 order-lg-2 mb-lg-0 mb-4 pb-2 pb-lg-0 mt-lg-0 mt-md-0 mt-sm-5 mt-5">
                                      <img className="d-block mx-auto" src={AgentAdded} alt="Hero image" />
                                      </div> */}
                                        <div className="col-lg-12 order-lg-1 pe-lg-0 text-center">
                                          <h1 className="display-5 mb-4 me-lg-n5 text-center mb-4"> Thank you</h1>
                                          <ul className="status_bar">
                                            <li className="d-flex align-items-center justify-content-start"><i className="fi-check text-success bg-white icon-box-media me-2 rounded-4"></i>Password</li>
                                            <li className="d-flex align-items-center justify-content-start"><i className="fi-check text-success bg-white icon-box-media me-2 rounded-4"></i>Primary Information</li>
                                            <li className="d-flex align-items-center justify-content-start"><i className="fi-check text-success bg-white icon-box-media me-2 rounded-4"></i>W9</li>
                                            <li className="d-flex align-items-center justify-content-start"><i className="fi-check text-success bg-white icon-box-media me-2 rounded-4"></i>Agreement</li>
                                          </ul>

                                          {/* <p className="text-lg-start text-center mb-4 mb-lg-5 fs-lg">Fantastic news! Onboarding is complete! 🎉 Now, let's wrap things up with the final setup.
                                      </p>
                                      <p className="text-lg-start text-center mb-4 mb-lg-5 fs-lg"> If you have any remaining steps or need assistance, please don't hesitate to ask. We're here to ensure everything is in place for a seamless experience. Welcome to the team, and let's get started on this exciting journey together! 👋🚀</p> */}
                                          <div className="float-left w-100 my-4 text-center">
                                            {/* <NavLink  type="button"
                                        className="btn btn-primary btn-sm" to="/">Go To Homepage</NavLink> */}
                                            <button
                                              className="btn btn-primary btn-sm"
                                              type="button"
                                              onClick={handleFinishstep}
                                            >
                                              Submit and Complete Onboarding
                                            </button>
                                          </div>
                                        </div>
                                      </div>

                                      {/* <div className="text-center mt-4">
                                  <button
                                    className="btn btn-primary ms-2 px-3 px-sm-4"
                                    type="button"
                                    onClick={handleFinishstep}
                                  >
                                    Finish Setup
                                  </button>
                                </div> */}
                                    </>
                                  </div>
                                  :
                                  <div className="row">
                                    <div className="col-md-4">
                                      <label className="col-form-label float-left w-100" for="pr-fn">Please complete the following form to complete signup.</label>
                                      <div className="row">
                                        <div className="col-sm-12 mb-2">
                                          <label className="form-label" for="pr-fn">
                                            *. Member Information
                                          </label>
                                          <label className="form-label float-left w-100" for="pr-fn">First Name
                                          </label>
                                          <input
                                            className="form-control form-control-lg"
                                            id="inline-form-input"
                                            type="text"
                                            name="firstname"
                                            // value={formValuesAboout?.firstname}
                                            onChange={(e) =>
                                              handleFieldChange(
                                                "firstname",
                                                e.target.value
                                              )
                                            }
                                          />
                                        </div>

                                        <div className="col-sm-12 mb-2">

                                          <label className="form-label float-left w-100" for="pr-fn">Mid Int:
                                          </label>
                                          <input
                                            className="form-control form-control-lg"
                                            id="inline-form-input"
                                            type="text"
                                            name="midint"
                                            // value={formValuesAboout?.midint}
                                            onChange={(e) =>
                                              handleFieldChange(
                                                "midint",
                                                e.target.value
                                              )
                                            }
                                          />
                                        </div>

                                        <div className="col-sm-12 mb-2">

                                          <label className="form-label float-left w-100" for="pr-fn">Last:
                                          </label>
                                          <input
                                            className="form-control form-control-lg"
                                            id="inline-form-input"
                                            type="text"
                                            name="lastname"
                                            // value={formValuesAboout?.lastname}
                                            onChange={(e) =>
                                              handleFieldChange(
                                                "lastname",
                                                e.target.value
                                              )
                                            }
                                          />
                                        </div>



                                        <div className="col-sm-12 mb-2">
                                          <p className="mb-2"><small>This Agreement, made this day</small></p>
                                          <p className="mb-2">
                                            <input
                                              className="form-control form-control-lg"
                                              id="inline-form-input"
                                              type="text"
                                              name="dayof"
                                              // value={fieldValues?.dayof}
                                              value={moment().format("M/D/YYYY")}
                                              onChange={(e) =>
                                                handleFieldChange(
                                                  "dayof",
                                                  e.target.value
                                                )
                                              }
                                              readOnly
                                            />
                                            <p className="my-2"><small>between In Depth Realty Referral, Hereinafter called the BROKERAGE and </small></p>
                                            <input
                                              className="form-control form-control-lg"
                                              id="inline-form-input"
                                              type="text"
                                              name="referral"
                                              // value={formValuesAboout?.referral}
                                              onChange={(e) =>
                                                handleFieldChange(
                                                  "referral",
                                                  e.target.value
                                                )
                                              }
                                            />
                                            Hereinafter called CONTRACTOR.
                                            WHEREAS, the BROKERAGE is duly registered as a Real Estate Brokerage company in the
                                            State of Utah, and WHEREAS, the CONTRACTOR is duly licensed as a Real Estate Sales
                                            Agent and properly qualified to solicit real estate for sale or exchange, WHEREAS, it is
                                            deemed to be to the mutual advantage of the BROKERAGE and CONTRACTOR to form the
                                            association for good and valuable consideration.
                                          </p>

                                        </div>

                                        {Object.keys(signatures).length > 0 ? (
                                          <div className="pull-right mt-3">
                                            <button
                                              className="btn btn-primary btn-sm"
                                              // onClick={handleAddTexterm}
                                              onClick={() => handleFillAndGeneratePdfreferralCONTRACTORAGREEMENT()}
                                              disabled={isdone}
                                            >
                                              {isdone ? "Please wait..." : "Next"}

                                              <i className="fi-chevron-right fs-sm ms-2"></i>
                                            </button>
                                          </div>
                                        ) : (
                                          <></>
                                        )
                                        }
                                      </div>
                                    </div>
                                    <div className="col-md-8">
                                      <div className="">
                                        <div className="">
                                          <div className="">
                                            <div className="onboard_iframe">
                                              <iframe className="" src={"https://brokeragentbase.s3.amazonaws.com/assets/Referral+Agent+Independent+Contractor+Agreement.pdf#view=FitH&zoom=100"} />
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                }

                              </>
                              :
                              <>
                                {
                                  onBoardparkcity ?
                                    <div className="row">
                                      {
                                        agreementdone ?
                                          <>
                                            {
                                              parkcityfinal === true ?
                                                <>
                                                  <div className="thank_you row align-items-center mt-md-2 text-center">
                                                    {/* <div className="col-lg-7 order-lg-2 mb-lg-0 mb-4 pb-2 pb-lg-0 mt-lg-0 mt-md-0 mt-sm-5 mt-5">
                                            <img className="d-block mx-auto" src={AgentAdded} alt="Hero image" />
                                            </div> */}
                                                    <div className="col-lg-12 order-lg-1 pe-lg-0 text-center">
                                                      <h1 className="display-5 mb-4 me-lg-n5 text-center mb-4"> Thank you</h1>
                                                      <ul className="status_bar">
                                                        <li className="d-flex align-items-center justify-content-start"><i className="fi-check text-success bg-white icon-box-media me-2 rounded-4"></i>Password</li>
                                                        <li className="d-flex align-items-center justify-content-start"><i className="fi-check text-success bg-white icon-box-media me-2 rounded-4"></i>Primary Information</li>
                                                        <li className="d-flex align-items-center justify-content-start"><i className="fi-check text-success bg-white icon-box-media me-2 rounded-4"></i>W9</li>
                                                        <li className="d-flex align-items-center justify-content-start"><i className="fi-check text-success bg-white icon-box-media me-2 rounded-4"></i>Agreement</li>
                                                      </ul>

                                                      {/* <p className="text-lg-start text-center mb-4 mb-lg-5 fs-lg">Fantastic news! Onboarding is complete! 🎉 Now, let's wrap things up with the final setup.
                                            </p>
                                            <p className="text-lg-start text-center mb-4 mb-lg-5 fs-lg"> If you have any remaining steps or need assistance, please don't hesitate to ask. We're here to ensure everything is in place for a seamless experience. Welcome to the team, and let's get started on this exciting journey together! 👋🚀</p> */}
                                                      <div className="float-left w-100 my-4 text-center">
                                                        {/* <NavLink  type="button"
                                              className="btn btn-primary btn-sm" to="/">Go To Homepage</NavLink> */}
                                                        <button
                                                          className="btn btn-primary btn-sm"
                                                          type="button"
                                                          onClick={handleFinishstep}
                                                        >
                                                          Submit and Complete Onboarding
                                                        </button>
                                                      </div>
                                                    </div>
                                                  </div>

                                                  {/* <div className="text-center mt-4">
                                        <button
                                          className="btn btn-primary ms-2 px-3 px-sm-4"
                                          type="button"
                                          onClick={handleFinishstep}
                                        >
                                          Finish Setup
                                        </button>
                                      </div> */}
                                                </>
                                                :
                                                <>
                                                  <div className="col-md-12">
                                                    <p>Please go this link for Parkcity Membership.</p>
                                                  </div>
                                                  <div className="text-center mt-4">
                                                    <a onClick={() => setParkcityfinal(true)} className="btn btn-primary ms-2 px-3 px-sm-4" target="_blank" href="https://fetch.tangilla.com/forms/v1/f/a55a3b41-4e02-11ee-a990-0a0e0f013ec5">Park City</a>
                                                  </div>
                                                </>
                                            }
                                          </>
                                          :
                                          <>
                                            <div className="col-md-4">
                                              <label className="col-form-label float-left w-100" for="pr-fn">Please complete the following form to complete signup.</label>
                                              <div className="row">
                                                <div className="col-sm-12 mb-2">
                                                  <label className="form-label" for="pr-fn">
                                                    In Depth Realty BROKER - AGENT INDEPENDENT CONTRACTOR AGREEMENT
                                                  </label>
                                                  <p className="mb-2"><small>This Agreement, made this day</small></p>
                                                  <p className="mb-2">
                                                    <input
                                                      className="form-control form-control-lg"
                                                      id="inline-form-input"
                                                      type="text"
                                                      name="Text-1"
                                                      // value={fieldValues?.Text-1}
                                                      value={moment().format("M/D/YYYY")}
                                                      onChange={(e) =>
                                                        handleFieldChange(
                                                          "Text-1",
                                                          e.target.value
                                                        )
                                                      }
                                                      readOnly
                                                    />
                                                    <p className="my-2"><small>by and between In Depth Realty hereinafter referred to as “Broker”, and</small></p>
                                                    <input
                                                      className="form-control form-control-lg"
                                                      id="inline-form-input"
                                                      type="text"
                                                      name="Text-2"
                                                      // value={formValuesAboout?.Text-2}
                                                      onChange={(e) =>
                                                        handleFieldChange(
                                                          "Text-2",
                                                          e.target.value
                                                        )
                                                      }
                                                    />
                                                    {
                                                      formErrors['Text-2'] && (
                                                        <div className="invalid-tooltip">
                                                          {formErrors['Text-2']}
                                                        </div>
                                                      )}
                                                    hereinafter referred to as
                                                    “Salesperson” .WITNESSETH: WHEREAS, Broker is duly licensed as a real estate broker by the State of Utah, and
                                                    WHEREAS, Broker maintains the tools, systems and other items necessary and incidental to the proper operation of a real estate
                                                    brokerage business, and WHEREAS, Salesperson is engaged in business as a real estate licensee, duly licensed by the State of Utah.
                                                  </p>

                                                </div>

                                                {Object.keys(signatures).length > 0 ? (
                                                  <div className="pull-right mt-3">
                                                    <button
                                                      className="btn btn-primary btn-sm"
                                                      // onClick={handleAddTexterm}
                                                      onClick={() => handleFillAndGeneratePdfCONTRACTORAGREEMENT()}
                                                      disabled={isdone}
                                                    >
                                                      {isdone ? "Please wait..." : "Next"}

                                                      <i className="fi-chevron-right fs-sm ms-2"></i>
                                                    </button>
                                                  </div>
                                                ) : (
                                                  <></>
                                                )
                                                }
                                              </div>
                                            </div>
                                            <div className="col-md-8">
                                              <div className="">
                                                <div className="">
                                                  <div className="">
                                                    <div className="onboard_iframe">
                                                      {
                                                        accountData.legacy_account && accountData.legacy_account === "yes" ?
                                                          <iframe className="" src={"https://brokeragentbase.s3.amazonaws.com/assets/In-Depth+Realty+Legacy+Independent+Contractor+Agreement.pdf#view=FitH&zoom=100"} />
                                                          :
                                                          <iframe className="" src={"https://brokeragentbase.s3.amazonaws.com/1702879744145-In%20Depth%20Realty%20Independent%20Contractor%20Agreement.pdf#view=FitH&zoom=100"} />
                                                      }

                                                    </div>
                                                  </div>
                                                </div>
                                              </div>
                                            </div>
                                          </>
                                      }
                                    </div>
                                    :
                                    <div className="row">
                                      {
                                        agreementdone ?
                                          <>
                                            {currentStep < onBoard.length && (
                                              <>
                                                <div className="col-md-4">

                                                  <div className="onboarding_common_form_section">
                                                    {/* <div dangerouslySetInnerHTML={{ __html: formHTML1 }} /> */}

                                                    {/* contract Agreement */}
                                                    {
                                                      agreementdone ?
                                                        checkurlstring(onBoard[currentStep].image, "Utah Real Estate") ?
                                                          <>
                                                            {/* utah realstate.com form */}
                                                            <>
                                                              <div className="row" >
                                                                <label className="col-form-label float-left w-100" for="pr-fn">Please complete the following form to complete signup.</label>

                                                                <div className="col-sm-12 mb-2">
                                                                  <label className="form-label" for="pr-fn">
                                                                    *. Type of Request.
                                                                  </label>
                                                                  <p className="mb-1">
                                                                    Please select one of the following options:.
                                                                  </p>
                                                                  <div className="row">
                                                                    <div className="col-sm-12 mb-2 d-flex">
                                                                      <input
                                                                        className="form-check-input"
                                                                        type="checkbox"
                                                                        name="membertransfer"
                                                                        onChange={(e) =>
                                                                          handleFieldChange(
                                                                            "membertransfer",
                                                                            e.target.checked
                                                                          )
                                                                        }
                                                                        checked={
                                                                          fieldValues[
                                                                          "membertransfer"
                                                                          ]
                                                                        }
                                                                      />
                                                                      &nbsp;
                                                                      <p className="float-left mb-0 d-flex align-items-center">
                                                                        Member Transfer
                                                                      </p>
                                                                    </div>
                                                                    <div className="col-sm-12 mb-2 d-flex">
                                                                      <input
                                                                        className="form-check-input"
                                                                        type="checkbox"
                                                                        name="inactivatemembership"
                                                                        onChange={(e) =>
                                                                          handleFieldChange(
                                                                            "inactivatemembership",
                                                                            e.target.checked
                                                                          )
                                                                        }
                                                                        checked={
                                                                          fieldValues[
                                                                          "inactivatemembership"
                                                                          ]
                                                                        }
                                                                      />
                                                                      &nbsp;
                                                                      <p className="float-left mb-0 d-flex align-items-center">
                                                                        Inactivate Membership
                                                                      </p>
                                                                    </div>
                                                                    <div className="col-sm-12 mb-2 d-flex">
                                                                      <input
                                                                        className="form-check-input"
                                                                        type="checkbox"
                                                                        name="reactivatemembership"
                                                                        onChange={(e) =>
                                                                          handleFieldChange(
                                                                            "reactivatemembership",
                                                                            e.target.checked
                                                                          )
                                                                        }
                                                                        checked={
                                                                          fieldValues[
                                                                          "reactivatemembership"
                                                                          ]
                                                                        }
                                                                      />
                                                                      &nbsp;
                                                                      <p className="float-left mb-0 d-flex align-items-center">
                                                                        Reactivate Membership
                                                                      </p>
                                                                    </div>
                                                                    <div className="col-sm-12 mb-2 d-flex">
                                                                      <input
                                                                        className="form-check-input"
                                                                        type="checkbox"
                                                                        name="membernamechange"
                                                                        onChange={(e) =>
                                                                          handleFieldChange(
                                                                            "membernamechange",
                                                                            e.target.checked
                                                                          )
                                                                        }
                                                                        checked={
                                                                          fieldValues[
                                                                          "membernamechange"
                                                                          ]
                                                                        }
                                                                      />
                                                                      &nbsp;
                                                                      <p className="float-left mb-0 d-flex align-items-center">
                                                                        Member Name Change
                                                                      </p>
                                                                    </div>
                                                                  </div>
                                                                </div>
                                                                <div className="col-sm-12 mb-2">
                                                                  <label className="form-label" for="pr-fn">
                                                                    *. Type of Membership. .
                                                                  </label>
                                                                  <p className="mb-1">
                                                                    Please select one of the following options:
                                                                  </p>
                                                                  <div className="row">
                                                                    <div className="col-sm-12 mb-2 d-flex">
                                                                      <input
                                                                        className="form-check-input"
                                                                        type="checkbox"
                                                                        name="principalbroker"
                                                                        onChange={(e) =>
                                                                          handleFieldChange(
                                                                            "principalbroker",
                                                                            e.target.checked
                                                                          )
                                                                        }
                                                                        checked={
                                                                          fieldValues[
                                                                          "principalbroker"
                                                                          ]
                                                                        }
                                                                      />
                                                                      &nbsp;
                                                                      <p className="float-left mb-0 d-flex align-items-center">
                                                                        Principal Broker
                                                                      </p>
                                                                    </div>
                                                                    <div className="col-sm-12 mb-2 d-flex">
                                                                      <input
                                                                        className="form-check-input"
                                                                        type="checkbox"
                                                                        name="officeassistant"
                                                                        onChange={(e) =>
                                                                          handleFieldChange(
                                                                            "officeassistant",
                                                                            e.target.checked
                                                                          )
                                                                        }
                                                                        checked={
                                                                          fieldValues[
                                                                          "officeassistant"
                                                                          ]
                                                                        }
                                                                      />
                                                                      &nbsp;
                                                                      <p className="float-left mb-0 d-flex align-items-center">
                                                                        Office Assistant
                                                                      </p>
                                                                    </div>
                                                                    <div className="col-sm-12 mb-2 d-flex">
                                                                      <input
                                                                        className="form-check-input"
                                                                        type="checkbox"
                                                                        name="branchbroker"
                                                                        onChange={(e) =>
                                                                          handleFieldChange(
                                                                            "branchbroker",
                                                                            e.target.checked
                                                                          )
                                                                        }
                                                                        checked={
                                                                          fieldValues[
                                                                          "branchbroker"
                                                                          ]
                                                                        }
                                                                      />
                                                                      &nbsp;
                                                                      <p className="float-left mb-0 d-flex align-items-center">
                                                                        Branch Broker
                                                                      </p>
                                                                    </div>
                                                                    <div className="col-sm-12 mb-2 d-flex">
                                                                      <input
                                                                        className="form-check-input"
                                                                        type="checkbox"
                                                                        name="appraiserparticipant"
                                                                        onChange={(e) =>
                                                                          handleFieldChange(
                                                                            "appraiserparticipant",
                                                                            e.target.checked
                                                                          )
                                                                        }
                                                                        checked={
                                                                          fieldValues[
                                                                          "appraiserparticipant"
                                                                          ]
                                                                        }
                                                                      />
                                                                      &nbsp;
                                                                      <p className="float-left mb-0 d-flex align-items-center">
                                                                        Appraiser Participant
                                                                      </p>
                                                                    </div>


                                                                    <div className="col-sm-12 mb-2 d-flex">
                                                                      <input
                                                                        className="form-check-input"
                                                                        type="checkbox"
                                                                        name="appraisersubscriber"
                                                                        onChange={(e) =>
                                                                          handleFieldChange(
                                                                            "appraisersubscriber",
                                                                            e.target.checked
                                                                          )
                                                                        }
                                                                        checked={
                                                                          fieldValues[
                                                                          "appraisersubscriber"
                                                                          ]
                                                                        }
                                                                      />
                                                                      &nbsp;
                                                                      <p className="float-left mb-0 d-flex align-items-center">
                                                                        Appraiser Subscriber
                                                                      </p>
                                                                    </div>



                                                                    <div className="col-sm-12 mb-2 d-flex">
                                                                      <input
                                                                        className="form-check-input"
                                                                        type="checkbox"
                                                                        name="associatebroker"
                                                                        onChange={(e) =>
                                                                          handleFieldChange(
                                                                            "associatebroker",
                                                                            e.target.checked
                                                                          )
                                                                        }
                                                                        checked={
                                                                          fieldValues[
                                                                          "associatebroker"
                                                                          ]
                                                                        }
                                                                      />
                                                                      &nbsp;
                                                                      <p className="float-left mb-0 d-flex align-items-center">
                                                                        Associate Broker
                                                                      </p>
                                                                    </div>



                                                                    <div className="col-sm-12 mb-2 d-flex">
                                                                      <input
                                                                        className="form-check-input"
                                                                        type="checkbox"
                                                                        name="salesagent"
                                                                        onChange={(e) =>
                                                                          handleFieldChange(
                                                                            "salesagent",
                                                                            e.target.checked
                                                                          )
                                                                        }
                                                                        checked={
                                                                          fieldValues[
                                                                          "salesagent"
                                                                          ]
                                                                        }
                                                                      />
                                                                      &nbsp;
                                                                      <p className="float-left mb-0 d-flex align-items-center">
                                                                        Sales Agent
                                                                      </p>
                                                                    </div>


                                                                    <div className="col-sm-12 mb-2 d-flex">
                                                                      <input
                                                                        className="form-check-input"
                                                                        type="checkbox"
                                                                        name="appraisertrainee"
                                                                        onChange={(e) =>
                                                                          handleFieldChange(
                                                                            "appraisertrainee",
                                                                            e.target.checked
                                                                          )
                                                                        }
                                                                        checked={
                                                                          fieldValues[
                                                                          "appraisertrainee"
                                                                          ]
                                                                        }
                                                                      />
                                                                      &nbsp;
                                                                      <p className="float-left mb-0 d-flex align-items-center">
                                                                        Appraiser Trainee
                                                                      </p>
                                                                    </div>
                                                                  </div>
                                                                </div>
                                                                <div className="col-sm-12 mb-2">
                                                                  <label className="form-label" for="pr-fn">
                                                                    *. Member Information
                                                                  </label>
                                                                  <label className="form-label float-left w-100" for="pr-fn">Name
                                                                  </label>
                                                                  <input
                                                                    className="form-control form-control-lg"
                                                                    id="inline-form-input"
                                                                    type="text"
                                                                    name="name"
                                                                    // value={formValuesAboout?.name}
                                                                    onChange={(e) =>
                                                                      handleFieldChange(
                                                                        "name",
                                                                        e.target.value
                                                                      )
                                                                    }
                                                                  />
                                                                </div>
                                                                <div className="col-sm-12 mb-2 federal_tax_classification">
                                                                </div>
                                                                <div className="col-sm-12 mb-2">
                                                                  <label className="form-label" for="pr-fn">
                                                                    *. License
                                                                  </label>
                                                                  <input
                                                                    className="form-control form-control-lg"
                                                                    id="inline-form-input"
                                                                    type="text"
                                                                    name="license"
                                                                    onChange={(e) =>
                                                                      handleFieldChange(
                                                                        "license",
                                                                        e.target.value
                                                                      )
                                                                    }
                                                                  />
                                                                </div>

                                                                <div className="col-sm-12 mb-2">
                                                                  <label className="form-label" for="pr-fn">
                                                                    *. Cell Phone #
                                                                  </label>
                                                                  <p className="mb-0"></p>
                                                                  <input
                                                                    className="form-control form-control-lg"
                                                                    id="inline-form-input"
                                                                    type="text"
                                                                    name="phone"
                                                                    // value={formValuesAboout?.phone}
                                                                    onChange={(e) =>
                                                                      handleFieldChange(
                                                                        "phone",
                                                                        e.target.value
                                                                      )
                                                                    }
                                                                  />
                                                                </div>

                                                                <div className="col-sm-12 mb-2">
                                                                  <label className="form-label" for="pr-fn">
                                                                    *. Email
                                                                  </label>
                                                                  <input
                                                                    className="form-control form-control-lg"
                                                                    id="inline-form-input"
                                                                    type="text"
                                                                    name="email"
                                                                    // value={formValuesAboout?.email}
                                                                    onChange={(e) =>
                                                                      handleFieldChange(
                                                                        "email",
                                                                        e.target.value
                                                                      )
                                                                    }
                                                                  />
                                                                </div>

                                                                <div className="col-sm-12 mb-2">
                                                                  <label className="form-label" for="pr-fn">
                                                                    *. Businessaddress
                                                                  </label>
                                                                  <input
                                                                    className="form-control form-control-lg"
                                                                    id="inline-form-input"
                                                                    type="text"
                                                                    name="businessaddress"
                                                                    // value={formValuesAboout?.businessaddress}
                                                                    onChange={(e) =>
                                                                      handleFieldChange(
                                                                        "businessaddress",
                                                                        e.target.value
                                                                      )
                                                                    }
                                                                  />
                                                                </div>

                                                                <div className="col-sm-12 mb-2">
                                                                  <label className="form-label" for="pr-fn">
                                                                    *. Primary Association Membership:
                                                                  </label>
                                                                  <input
                                                                    className="form-control form-control-lg"
                                                                    id="inline-form-input"
                                                                    type="text"
                                                                    name="primaryassociation"
                                                                    // value={formValuesAboout?.primaryassociation}
                                                                    onChange={(e) =>
                                                                      handleFieldChange(
                                                                        "primaryassociation",
                                                                        e.target.value
                                                                      )
                                                                    }
                                                                  />
                                                                </div>


                                                                <div className="col-sm-12 mb-2">
                                                                  <label className="form-label" for="pr-fn">
                                                                    *. If Member Transfer, please check the listing input permissions you wish to assign to the agent:
                                                                  </label>
                                                                  {/* <p className="mb-0">
                                                                    Please select one of the following options:
                                                                  </p> */}
                                                                  <div className="row">
                                                                    <div className="col-sm-12 mb-2 d-flex">
                                                                      <input
                                                                        className="form-check-input"
                                                                        type="checkbox"
                                                                        name="readonly"
                                                                        onChange={(e) =>
                                                                          handleFieldChange(
                                                                            "readonly",
                                                                            e.target.checked
                                                                          )
                                                                        }
                                                                        checked={
                                                                          fieldValues[
                                                                          "readonly"
                                                                          ]
                                                                        }
                                                                      />
                                                                      &nbsp;
                                                                      <p className="float-left mb-0 d-flex align-items-center">
                                                                        Read Only Access
                                                                      </p>
                                                                    </div>
                                                                    <div className="col-sm-12 mb-2 d-flex">
                                                                      <input
                                                                        className="form-check-input"
                                                                        type="checkbox"
                                                                        name="addeditlisting"
                                                                        onChange={(e) =>
                                                                          handleFieldChange(
                                                                            "addeditlisting",
                                                                            e.target.checked
                                                                          )
                                                                        }
                                                                        checked={
                                                                          fieldValues[
                                                                          "addeditlisting"
                                                                          ]
                                                                        }
                                                                      />
                                                                      &nbsp;
                                                                      <p className="float-left mb-0 d-flex align-items-center">
                                                                        Add/Edit Own Listings
                                                                      </p>
                                                                    </div>
                                                                    <div className="col-sm-12 mb-2 d-flex">
                                                                      <input
                                                                        className="form-check-input"
                                                                        type="checkbox"
                                                                        name="onlyeditopenhouse"
                                                                        onChange={(e) =>
                                                                          handleFieldChange(
                                                                            "onlyeditopenhouse",
                                                                            e.target.checked
                                                                          )
                                                                        }
                                                                        checked={
                                                                          fieldValues[
                                                                          "onlyeditopenhouse"
                                                                          ]
                                                                        }
                                                                      />
                                                                      &nbsp;
                                                                      <p className="float-left mb-0 d-flex align-items-center">
                                                                        Only Edit Open House Information
                                                                      </p>
                                                                    </div>
                                                                    <div className="col-sm-12 mb-2 d-flex">
                                                                      <input
                                                                        className="form-check-input"
                                                                        type="checkbox"
                                                                        name="addeditofficelisting"
                                                                        onChange={(e) =>
                                                                          handleFieldChange(
                                                                            "addeditofficelisting",
                                                                            e.target.checked
                                                                          )
                                                                        }
                                                                        checked={
                                                                          fieldValues[
                                                                          "addeditofficelisting"
                                                                          ]
                                                                        }
                                                                      />
                                                                      &nbsp;
                                                                      <p className="float-left mb-0 d-flex align-items-center">
                                                                        Add/Edit Office Listings
                                                                      </p>
                                                                    </div>


                                                                    <div className="col-sm-12 mb-2 d-flex">
                                                                      <input
                                                                        className="form-check-input"
                                                                        type="checkbox"
                                                                        name="onlyeditphotos"
                                                                        onChange={(e) =>
                                                                          handleFieldChange(
                                                                            "onlyeditphotos",
                                                                            e.target.checked
                                                                          )
                                                                        }
                                                                        checked={
                                                                          fieldValues[
                                                                          "onlyeditphotos"
                                                                          ]
                                                                        }
                                                                      />
                                                                      &nbsp;
                                                                      <p className="float-left mb-0 d-flex align-items-center">
                                                                        Only Edit Photos/Tours
                                                                      </p>
                                                                    </div>



                                                                    <div className="col-sm-12 mb-2 d-flex">
                                                                      <input
                                                                        className="form-check-input"
                                                                        type="checkbox"
                                                                        name="addeditbranchlisting"
                                                                        onChange={(e) =>
                                                                          handleFieldChange(
                                                                            "addeditbranchlisting",
                                                                            e.target.checked
                                                                          )
                                                                        }
                                                                        checked={
                                                                          fieldValues[
                                                                          "addeditbranchlisting"
                                                                          ]
                                                                        }
                                                                      />
                                                                      &nbsp;
                                                                      <p className="float-left mb-0 d-flex align-items-center">
                                                                        Add/Edit All Branch/Brokerage Listings
                                                                      </p>
                                                                    </div>
                                                                  </div>
                                                                </div>


                                                                <div className="col-sm-12 mb-2">
                                                                  <label className="form-label" for="pr-fn">
                                                                    *. Former Office Information. Please complete for Membership transfers and inactivations:
                                                                  </label>
                                                                  <label className="form-label float-left w-100" for="pr-fn">
                                                                    Office Name:
                                                                  </label>
                                                                  <input
                                                                    className="form-control form-control-lg"
                                                                    id="inline-form-input"
                                                                    type="text"
                                                                    name="formerofficename"
                                                                    // value={formValuesAboout?.formerofficename}
                                                                    onChange={(e) =>
                                                                      handleFieldChange(
                                                                        "formerofficename",
                                                                        e.target.value
                                                                      )
                                                                    }
                                                                  />
                                                                </div>

                                                                <div className="col-sm-12 mb-2">
                                                                  <label className="form-label" for="pr-fn">
                                                                    *. Office ID:
                                                                  </label>
                                                                  {/* <p className="mb-0">(number, street, and apt. or suite no.) See instructions.</p> */}
                                                                  <input
                                                                    className="form-control form-control-lg"
                                                                    id="inline-form-input"
                                                                    type="text"
                                                                    name="formerofficeid"
                                                                    // value={formValuesAboout?.formerofficeid}
                                                                    onChange={(e) =>
                                                                      handleFieldChange(
                                                                        "formerofficeid",
                                                                        e.target.value
                                                                      )
                                                                    }
                                                                  />
                                                                </div>


                                                                <div className="col-sm-12 mb-2">
                                                                  <label className="form-label" for="pr-fn">
                                                                    *. Broker/Appraiser Name:
                                                                  </label>
                                                                  {/* <p className="mb-0">(number, street, and apt. or suite no.) See instructions.</p> */}
                                                                  <input
                                                                    className="form-control form-control-lg"
                                                                    id="inline-form-input"
                                                                    type="text"
                                                                    name="formerbrokername"
                                                                    // value={formValuesAboout?.formerbrokername}
                                                                    onChange={(e) =>
                                                                      handleFieldChange(
                                                                        "formerbrokername",
                                                                        e.target.value
                                                                      )
                                                                    }
                                                                  />
                                                                </div>


                                                                {
                                                                  Object.keys(signatures).length > 0 ? (
                                                                    <div className="pull-right mt-3">
                                                                      <button
                                                                        className="btn btn-primary btn-sm"
                                                                        // onClick={handleAddTexterm}
                                                                        onClick={() => handleFillAndGeneratePdfutah(onBoard[currentStep])}
                                                                        disabled={isdone}
                                                                      >
                                                                        {isdone ? "Please wait..." : "Next"}

                                                                        <i className="fi-chevron-right fs-sm ms-2"></i>
                                                                      </button>
                                                                    </div>
                                                                  ) : (
                                                                    <></>
                                                                  )
                                                                }

                                                              </div>
                                                            </>

                                                          </>
                                                          :
                                                          checkurlstring(onBoard[currentStep].image, "UCAR") ?
                                                            <>
                                                              {/* UCAR */}
                                                              <div className="row" >
                                                                <label className="col-form-label float-left w-100" for="pr-fn">Please complete the following form to complete signup.</label>

                                                                <div className="col-sm-12 mb-2">
                                                                  <label className="form-label" for="pr-fn">
                                                                    *. UCAR Membership Form
                                                                  </label>
                                                                  <p className="mb-1">
                                                                    Please select one of the following options:.
                                                                  </p>
                                                                  <div className="row">
                                                                    <div className="col-sm-12 mb-2 d-flex">
                                                                      <input
                                                                        className="form-check-input"
                                                                        type="radio"
                                                                        name="59"
                                                                        onChange={(e) =>
                                                                          handleFieldChange(
                                                                            "59",
                                                                            e.target.checked
                                                                          )
                                                                        }
                                                                        checked={
                                                                          fieldValues[
                                                                          "59"
                                                                          ]
                                                                        }
                                                                      />
                                                                      &nbsp;
                                                                      <p className="float-left mb-0 d-flex align-items-center">
                                                                        New Member
                                                                      </p>
                                                                    </div>
                                                                    <div className="col-sm-12 mb-2 d-flex">
                                                                      <input
                                                                        className="form-check-input"
                                                                        type="radio"
                                                                        name="59"
                                                                        onChange={(e) =>
                                                                          handleFieldChange(
                                                                            "59",
                                                                            e.target.checked
                                                                          )
                                                                        }
                                                                        checked={
                                                                          fieldValues[
                                                                          "59"
                                                                          ]
                                                                        }
                                                                      />
                                                                      &nbsp;
                                                                      <p className="float-left mb-0 d-flex align-items-center">
                                                                        Secondary Membership
                                                                      </p>
                                                                    </div>
                                                                    <div className="col-sm-12 mb-2 d-flex">
                                                                      <input
                                                                        className="form-check-input"
                                                                        type="radio"
                                                                        name="59"
                                                                        onChange={(e) =>
                                                                          handleFieldChange(
                                                                            "59",
                                                                            e.target.checked
                                                                          )
                                                                        }
                                                                        checked={
                                                                          fieldValues[
                                                                          "59"
                                                                          ]
                                                                        }
                                                                      />
                                                                      &nbsp;
                                                                      <p className="float-left mb-0 d-flex align-items-center">
                                                                        Transfer Offices
                                                                      </p>
                                                                    </div>
                                                                    <div className="col-sm-12 mb-2 d-flex">
                                                                      <input
                                                                        className="form-check-input"
                                                                        type="radio"
                                                                        name="59"
                                                                        onChange={(e) =>
                                                                          handleFieldChange(
                                                                            "59",
                                                                            e.target.checked
                                                                          )
                                                                        }
                                                                        checked={
                                                                          fieldValues[
                                                                          "59"
                                                                          ]
                                                                        }
                                                                      />
                                                                      &nbsp;
                                                                      <p className="float-left mb-0 d-flex align-items-center">
                                                                        Inactivate
                                                                      </p>
                                                                    </div>

                                                                    <div className="col-sm-12 mb-2 d-flex">
                                                                      <input
                                                                        className="form-check-input"
                                                                        type="radio"
                                                                        name="59"
                                                                        onChange={(e) =>
                                                                          handleFieldChange(
                                                                            "59",
                                                                            e.target.checked
                                                                          )
                                                                        }
                                                                        checked={
                                                                          fieldValues[
                                                                          "59"
                                                                          ]
                                                                        }
                                                                      />
                                                                      &nbsp;
                                                                      <p className="float-left mb-0 d-flex align-items-center">
                                                                        Reactivate
                                                                      </p>
                                                                    </div>


                                                                    <div className="col-sm-12 mb-2 d-flex">
                                                                      <input
                                                                        className="form-check-input"
                                                                        type="radio"
                                                                        name="59"
                                                                        onChange={(e) =>
                                                                          handleFieldChange(
                                                                            "59",
                                                                            e.target.checked
                                                                          )
                                                                        }
                                                                        checked={
                                                                          fieldValues[
                                                                          "59"
                                                                          ]
                                                                        }
                                                                      />
                                                                      &nbsp;
                                                                      <p className="float-left mb-0 d-flex align-items-center">
                                                                        New Appraiser
                                                                      </p>
                                                                    </div>

                                                                    <div className="col-sm-12 mb-2 d-flex">
                                                                      <input
                                                                        className="form-check-input"
                                                                        type="radio"
                                                                        name="59"
                                                                        onChange={(e) =>
                                                                          handleFieldChange(
                                                                            "59",
                                                                            e.target.checked
                                                                          )
                                                                        }
                                                                        checked={
                                                                          fieldValues[
                                                                          "59"
                                                                          ]
                                                                        }
                                                                      />
                                                                      &nbsp;
                                                                      <p className="float-left mb-0 d-flex align-items-center">
                                                                        Change Contact Information
                                                                      </p>
                                                                    </div>
                                                                  </div>
                                                                </div>


                                                                <div className="col-sm-12 mb-2">
                                                                  <label className="form-label" for="pr-fn">
                                                                    *. Member Information
                                                                  </label>
                                                                  <p className="mb-0">
                                                                    Name
                                                                  </p>
                                                                  <input
                                                                    className="form-control form-control-lg"
                                                                    id="inline-form-input"
                                                                    type="text"
                                                                    name="Name"
                                                                    // value={formValuesAboout?.Name}
                                                                    onChange={(e) =>
                                                                      handleFieldChange(
                                                                        "Name",
                                                                        e.target.value
                                                                      )
                                                                    }
                                                                  />
                                                                </div>
                                                                <div className="col-sm-12 mb-2 federal_tax_classification">
                                                                </div>
                                                                <div className="col-sm-12 mb-2">
                                                                  <label className="form-label" for="pr-fn">
                                                                    *. Birth Date:
                                                                  </label>
                                                                  <input
                                                                    className="form-control form-control-lg"
                                                                    id="inline-form-input"
                                                                    type="text"
                                                                    name="Birth Date"
                                                                    onChange={(e) =>
                                                                      handleFieldChange(
                                                                        "Birth Date",
                                                                        e.target.value
                                                                      )
                                                                    }
                                                                  />/
                                                                  <input
                                                                    className="form-control form-control-lg"
                                                                    id="inline-form-input"
                                                                    type="text"
                                                                    name="Birth date 2"
                                                                    onChange={(e) =>
                                                                      handleFieldChange(
                                                                        "Birth date 2",
                                                                        e.target.value
                                                                      )
                                                                    }
                                                                  />
                                                                  /<input
                                                                    className="form-control form-control-lg"
                                                                    id="inline-form-input"
                                                                    type="text"
                                                                    name="Birth date 3"
                                                                    onChange={(e) =>
                                                                      handleFieldChange(
                                                                        "Birth date 3",
                                                                        e.target.value
                                                                      )
                                                                    }
                                                                  />
                                                                </div>

                                                                <div className="col-sm-12 mb-2">
                                                                  <label className="form-label" for="pr-fn">
                                                                    *. Gender #
                                                                  </label>
                                                                  <p className="mb-0"></p>
                                                                  <div className="col-sm-12 mb-2 d-flex">
                                                                    <input
                                                                      className="form-check-input"
                                                                      type="radio"
                                                                      name="36"
                                                                      onChange={(e) =>
                                                                        handleFieldChange(
                                                                          "59",
                                                                          e.target.checked
                                                                        )
                                                                      }
                                                                      checked={
                                                                        fieldValues[
                                                                        "36"
                                                                        ]
                                                                      }
                                                                    />
                                                                    &nbsp;
                                                                    <p className="float-left mb-0 d-flex align-items-center">
                                                                      Male
                                                                    </p>
                                                                  </div>

                                                                  <div className="col-sm-12 mb-2 d-flex">
                                                                    <input
                                                                      className="form-check-input"
                                                                      type="radio"
                                                                      name="36"
                                                                      onChange={(e) =>
                                                                        handleFieldChange(
                                                                          "36",
                                                                          e.target.checked
                                                                        )
                                                                      }
                                                                      checked={
                                                                        fieldValues[
                                                                        "36"
                                                                        ]
                                                                      }
                                                                    />
                                                                    &nbsp;
                                                                    <p className="float-left mb-0 d-flex align-items-center">
                                                                      Female
                                                                    </p>
                                                                  </div>
                                                                </div>

                                                                <div className="col-sm-12 mb-2">
                                                                  <label className="form-label" for="pr-fn">
                                                                    *. Home Address
                                                                  </label>
                                                                  <input
                                                                    className="form-control form-control-lg"
                                                                    id="inline-form-input"
                                                                    type="text"
                                                                    name="Home Address"
                                                                    // value={formValuesAboout?.Home Address}
                                                                    onChange={(e) =>
                                                                      handleFieldChange(
                                                                        "Home Address",
                                                                        e.target.value
                                                                      )
                                                                    }
                                                                  />
                                                                </div>

                                                                <div className="col-sm-12 mb-2">
                                                                  <label className="form-label" for="pr-fn">
                                                                    *. City
                                                                  </label>
                                                                  <input
                                                                    className="form-control form-control-lg"
                                                                    id="inline-form-input"
                                                                    type="text"
                                                                    name="City"
                                                                    // value={formValuesAboout?.City}
                                                                    onChange={(e) =>
                                                                      handleFieldChange(
                                                                        "City",
                                                                        e.target.value
                                                                      )
                                                                    }
                                                                  />
                                                                </div>

                                                                <div className="col-sm-12 mb-2">
                                                                  <label className="form-label" for="pr-fn">
                                                                    *. State:
                                                                  </label>
                                                                  <input
                                                                    className="form-control form-control-lg"
                                                                    id="inline-form-input"
                                                                    type="text"
                                                                    name="State"
                                                                    // value={formValuesAboout?.State}
                                                                    onChange={(e) =>
                                                                      handleFieldChange(
                                                                        "State",
                                                                        e.target.value
                                                                      )
                                                                    }
                                                                  />
                                                                </div>

                                                                <div className="col-sm-12 mb-2">
                                                                  <label className="form-label" for="pr-fn">
                                                                    *. Zip Code:
                                                                  </label>
                                                                  <input
                                                                    className="form-control form-control-lg"
                                                                    id="inline-form-input"
                                                                    type="text"
                                                                    name="Zip Code"
                                                                    // value={formValuesAboout?.Zip Code}
                                                                    onChange={(e) =>
                                                                      handleFieldChange(
                                                                        "Zip Code",
                                                                        e.target.value
                                                                      )
                                                                    }
                                                                  />
                                                                </div>

                                                                <div className="col-sm-12 mb-2">
                                                                  <label className="form-label" for="pr-fn">
                                                                    *. Phone:
                                                                  </label>
                                                                  <input
                                                                    className="form-control form-control-lg"
                                                                    id="inline-form-input"
                                                                    type="text"
                                                                    name="Phone"
                                                                    // value={formValuesAboout?.Phone}
                                                                    onChange={(e) =>
                                                                      handleFieldChange(
                                                                        "Phone",
                                                                        e.target.value
                                                                      )
                                                                    }
                                                                  />
                                                                </div>
                                                                <div className="col-sm-12 mb-2">
                                                                  <label className="form-label" for="pr-fn">
                                                                    *. Email Address:
                                                                  </label>
                                                                  <input
                                                                    className="form-control form-control-lg"
                                                                    id="inline-form-input"
                                                                    type="text"
                                                                    name="Email Address"
                                                                    // value={formValuesAboout?.Email Address}
                                                                    onChange={(e) =>
                                                                      handleFieldChange(
                                                                        "Email Address",
                                                                        e.target.value
                                                                      )
                                                                    }
                                                                  />
                                                                </div>
                                                                <div className="col-sm-12 mb-2">
                                                                  <label className="form-label" for="pr-fn">
                                                                    *. Real Estate License:
                                                                  </label>
                                                                  <input
                                                                    className="form-control form-control-lg"
                                                                    id="inline-form-input"
                                                                    type="text"
                                                                    name="Real Estate License"
                                                                    // value={formValuesAboout?.Real Estate License}
                                                                    onChange={(e) =>
                                                                      handleFieldChange(
                                                                        "Real Estate License",
                                                                        e.target.value
                                                                      )
                                                                    }
                                                                  />
                                                                </div>

                                                                <div className="col-sm-12 mb-2">
                                                                  <label className="form-label" for="pr-fn">
                                                                    *. Expiration Date of License:
                                                                  </label>
                                                                  <input
                                                                    className="form-control form-control-lg"
                                                                    id="inline-form-input"
                                                                    type="text"
                                                                    name="Expiration Date of License"
                                                                    // value={formValuesAboout?.Expiration Date of License}
                                                                    onChange={(e) =>
                                                                      handleFieldChange(
                                                                        "Expiration Date of License",
                                                                        e.target.value
                                                                      )
                                                                    }
                                                                  />/
                                                                  <input
                                                                    className="form-control form-control-lg"
                                                                    id="inline-form-input"
                                                                    type="text"
                                                                    name="undefined_4"
                                                                    // value={formValuesAboout?.undefined_4}
                                                                    onChange={(e) =>
                                                                      handleFieldChange(
                                                                        "undefined_4",
                                                                        e.target.value
                                                                      )
                                                                    }
                                                                  />/

                                                                  <input
                                                                    className="form-control form-control-lg"
                                                                    id="inline-form-input"
                                                                    type="text"
                                                                    name="undefined_5"
                                                                    // value={formValuesAboout?.undefined_5}
                                                                    onChange={(e) =>
                                                                      handleFieldChange(
                                                                        "undefined_5",
                                                                        e.target.value
                                                                      )
                                                                    }
                                                                  />
                                                                </div>

                                                                <div className="col-sm-12 mb-2">
                                                                  <label className="form-label" for="pr-fn">
                                                                    *. Company Name:
                                                                  </label>
                                                                  <input
                                                                    className="form-control form-control-lg"
                                                                    id="inline-form-input"
                                                                    type="text"
                                                                    name="Company Name"
                                                                    // value={formValuesAboout?.Company Name}
                                                                    onChange={(e) =>
                                                                      handleFieldChange(
                                                                        "Company Name",
                                                                        e.target.value
                                                                      )
                                                                    }
                                                                  />
                                                                </div>

                                                                <div className="col-sm-12 mb-2">
                                                                  <label className="form-label" for="pr-fn">
                                                                    *. Broker Name:
                                                                  </label>
                                                                  <input
                                                                    className="form-control form-control-lg"
                                                                    id="inline-form-input"
                                                                    type="text"
                                                                    name="Broker Name"
                                                                    // value={formValuesAboout?.Broker Name}
                                                                    onChange={(e) =>
                                                                      handleFieldChange(
                                                                        "Broker Name",
                                                                        e.target.value
                                                                      )
                                                                    }
                                                                  />
                                                                </div>

                                                                <div className="col-sm-12 mb-2">
                                                                  <label className="form-label" for="pr-fn">
                                                                    *. Have you ever been a member of a REALTOR association? If yes which board:
                                                                  </label>
                                                                  <input
                                                                    className="form-control form-control-lg"
                                                                    id="inline-form-input"
                                                                    type="text"
                                                                    name="Have you ever been a member of a REALTOR association? If yes which board"
                                                                    // value={formValuesAboout?.Have you ever been a member of a REALTOR association? If yes which board}
                                                                    onChange={(e) =>
                                                                      handleFieldChange(
                                                                        "Have you ever been a member of a REALTOR association? If yes which board",
                                                                        e.target.value
                                                                      )
                                                                    }
                                                                  />
                                                                </div>
                                                                <div className="col-sm-12 mb-2">
                                                                  <label className="form-label" for="pr-fn">
                                                                    *. Has your real estate license ever been suspended? If yes explain:
                                                                  </label>
                                                                  <input
                                                                    className="form-control form-control-lg"
                                                                    id="inline-form-input"
                                                                    type="text"
                                                                    name="Has your real estate license ever been suspended? If yes explain"
                                                                    // value={formValuesAboout?.Has your real estate license ever been suspended? If yes explain}
                                                                    onChange={(e) =>
                                                                      handleFieldChange(
                                                                        "Has your real estate license ever been suspended? If yes explain",
                                                                        e.target.value
                                                                      )
                                                                    }
                                                                  />
                                                                </div>

                                                                <div className="col-sm-12 mb-2">
                                                                  <label className="form-label" for="pr-fn">
                                                                    *. Has your real estate license ever been suspended? If yes explain:
                                                                  </label>
                                                                  <input
                                                                    className="form-control form-control-lg"
                                                                    id="inline-form-input"
                                                                    type="text"
                                                                    name="Has your real estate license ever been suspended? If yes explain"
                                                                    // value={formValuesAboout?.Has your real estate license ever been suspended? If yes explain}
                                                                    onChange={(e) =>
                                                                      handleFieldChange(
                                                                        "Has your real estate license ever been suspended? If yes explain",
                                                                        e.target.value
                                                                      )
                                                                    }
                                                                  />
                                                                </div>


                                                                <div className="col-sm-12 mb-2">
                                                                  <label className="form-label" for="pr-fn">
                                                                    *. New & Reactivating Members:
                                                                  </label>
                                                                  <p className="mb-0">
                                                                    Office:
                                                                  </p>
                                                                  <input
                                                                    className="form-control form-control-lg"
                                                                    id="inline-form-input"
                                                                    type="text"
                                                                    name="Office"
                                                                    // value={formValuesAboout?.Office}
                                                                    onChange={(e) =>
                                                                      handleFieldChange(
                                                                        "Office",
                                                                        e.target.value
                                                                      )
                                                                    }
                                                                  />
                                                                </div>

                                                                <div className="col-sm-12 mb-2">
                                                                  <label className="form-label" for="pr-fn">
                                                                    *. Broker Signature:
                                                                  </label>
                                                                  {/* <p className="mb-0">(number, street, and apt. or suite no.) See instructions.</p> */}
                                                                  <input
                                                                    className="form-control form-control-lg"
                                                                    id="inline-form-input"
                                                                    type="text"
                                                                    name="Signature9_es_:signer:signature"
                                                                    // value={formValuesAboout?.Signature9_es_:signer:signature}
                                                                    onChange={(e) =>
                                                                      handleFieldChange(
                                                                        "Signature9_es_:signer:signature",
                                                                        e.target.value
                                                                      )
                                                                    }
                                                                  />
                                                                </div>


                                                                <div className="col-sm-12 mb-2">
                                                                  <label className="form-label" for="pr-fn">
                                                                    *. Transferring Offices:
                                                                  </label>
                                                                  <label className="form-label" for="pr-fn">
                                                                    *. Old Office:
                                                                  </label>
                                                                  <input
                                                                    className="form-control form-control-lg"
                                                                    id="inline-form-input"
                                                                    type="text"
                                                                    name="Old Office"
                                                                    // value={formValuesAboout?.Old Office}
                                                                    onChange={(e) =>
                                                                      handleFieldChange(
                                                                        "Old Office",
                                                                        e.target.value
                                                                      )
                                                                    }
                                                                  />
                                                                </div>

                                                                <div className="col-sm-12 mb-2">
                                                                  <label className="form-label" for="pr-fn">
                                                                    *. New Office:
                                                                  </label>
                                                                  <input
                                                                    className="form-control form-control-lg"
                                                                    id="inline-form-input"
                                                                    type="text"
                                                                    name="New Office"
                                                                    // value={formValuesAboout?.New Office}
                                                                    onChange={(e) =>
                                                                      handleFieldChange(
                                                                        "New Office",
                                                                        e.target.value
                                                                      )
                                                                    }
                                                                  />
                                                                </div>

                                                                <div className="col-sm-12 mb-2">
                                                                  <label className="form-label" for="pr-fn">
                                                                    *. New Broker Signature:
                                                                  </label>
                                                                  <input
                                                                    className="form-control form-control-lg"
                                                                    id="inline-form-input"
                                                                    type="text"
                                                                    name="Signature2_es_:signer:signature"
                                                                    // value={formValuesAboout?.Signature2_es_:signer:signature}
                                                                    onChange={(e) =>
                                                                      handleFieldChange(
                                                                        "Signature2_es_:signer:signature",
                                                                        e.target.value
                                                                      )
                                                                    }
                                                                  />
                                                                </div>

                                                                <div className="col-sm-12 mb-2">
                                                                  <label className="form-label" for="pr-fn">
                                                                    *. Terminations
                                                                  </label>
                                                                  <label className="form-label" for="pr-fn">
                                                                    *. Terminating Office
                                                                  </label>
                                                                  <input
                                                                    className="form-control form-control-lg"
                                                                    id="inline-form-input"
                                                                    type="text"
                                                                    name="Terminating Office"
                                                                    // value={formValuesAboout?.Terminating Office}
                                                                    onChange={(e) =>
                                                                      handleFieldChange(
                                                                        "Terminating Office",
                                                                        e.target.value
                                                                      )
                                                                    }
                                                                  />
                                                                </div>

                                                                <div className="col-sm-12 mb-2">
                                                                  <label className="form-label" for="pr-fn">
                                                                    *. Broker Signature:
                                                                  </label>
                                                                  <input
                                                                    className="form-control form-control-lg"
                                                                    id="inline-form-input"
                                                                    type="text"
                                                                    name="Signature3_es_:signer:signature"
                                                                    // value={formValuesAboout?.Signature3_es_:signer:signature}
                                                                    onChange={(e) =>
                                                                      handleFieldChange(
                                                                        "Signature3_es_:signer:signature",
                                                                        e.target.value
                                                                      )
                                                                    }
                                                                  />
                                                                </div>



                                                                <div className="col-sm-12 mb-2">
                                                                  <label className="form-label" for="pr-fn">
                                                                    NEW MEMBERS ONLY
                                                                  </label>
                                                                  <label className="form-label" for="pr-fn">
                                                                    *. Code of Ethics
                                                                  </label>
                                                                  <input
                                                                    className="form-control form-control-lg"
                                                                    id="inline-form-input"
                                                                    type="text"
                                                                    name="Date attending course"
                                                                    // value={formValuesAboout?.Date attending course}
                                                                    onChange={(e) =>
                                                                      handleFieldChange(
                                                                        "Date attending course",
                                                                        e.target.value
                                                                      )
                                                                    }
                                                                  />
                                                                </div>

                                                                <div className="col-sm-12 mb-2">
                                                                  <label className="form-label" for="pr-fn">
                                                                    *. Date attending course
                                                                  </label>
                                                                  <input
                                                                    className="form-control form-control-lg"
                                                                    id="inline-form-input"
                                                                    type="text"
                                                                    name="Date attending course"
                                                                    // value={formValuesAboout?.Date attending course}
                                                                    onChange={(e) =>
                                                                      handleFieldChange(
                                                                        "Date attending course",
                                                                        e.target.value
                                                                      )
                                                                    }
                                                                  />
                                                                </div>
                                                                <div className="col-sm-12 mb-2">
                                                                  <label className="form-label" for="pr-fn">
                                                                    *. Date attending course
                                                                  </label>
                                                                  <input
                                                                    className="form-control form-control-lg"
                                                                    id="inline-form-input"
                                                                    type="text"
                                                                    name="Date attending course_2"
                                                                    // value={formValuesAboout?.Date attending course_2}
                                                                    onChange={(e) =>
                                                                      handleFieldChange(
                                                                        "Date attending course_2",
                                                                        e.target.value
                                                                      )
                                                                    }
                                                                  />
                                                                </div>


                                                                <div className="col-sm-12 mb-2">
                                                                  <label className="form-label" for="pr-fn">
                                                                    *.Select committees to attend:
                                                                  </label>
                                                                  <label className="form-label" for="pr-fn">
                                                                    *.Government Affairs Committee:
                                                                  </label>

                                                                  <p className="mb-0"></p>
                                                                  <div className="col-sm-12 mb-2 d-flex">
                                                                    <input
                                                                      className="form-check-input"
                                                                      type="checkbox"
                                                                      name="Government Affairs Committee"
                                                                      onChange={(e) =>
                                                                        handleFieldChange(
                                                                          "Government Affairs Committee",
                                                                          e.target.checked
                                                                        )
                                                                      }
                                                                      checked={
                                                                        fieldValues[
                                                                        "Government Affairs Committee"
                                                                        ]
                                                                      }
                                                                    />
                                                                    &nbsp;
                                                                    <p className="float-left mb-0 d-flex align-items-center">
                                                                      Government Affairs Committee
                                                                    </p>
                                                                  </div>

                                                                  <div className="col-sm-12 mb-2 d-flex">
                                                                    <input
                                                                      className="form-check-input"
                                                                      type="checkbox"
                                                                      name="Community Involvement Committee"
                                                                      onChange={(e) =>
                                                                        handleFieldChange(
                                                                          "Community Involvement Committee",
                                                                          e.target.checked
                                                                        )
                                                                      }
                                                                      checked={
                                                                        fieldValues[
                                                                        "Community Involvement Committee"
                                                                        ]
                                                                      }
                                                                    />
                                                                    &nbsp;
                                                                    <p className="float-left mb-0 d-flex align-items-center">
                                                                      Community Involvement Committee
                                                                    </p>
                                                                  </div>

                                                                  <div className="col-sm-12 mb-2 d-flex">
                                                                    <input
                                                                      className="form-check-input"
                                                                      type="checkbox"
                                                                      name="Social Committee"
                                                                      onChange={(e) =>
                                                                        handleFieldChange(
                                                                          "Social Committee",
                                                                          e.target.checked
                                                                        )
                                                                      }
                                                                      checked={
                                                                        fieldValues[
                                                                        "Social Committee"
                                                                        ]
                                                                      }
                                                                    />
                                                                    &nbsp;
                                                                    <p className="float-left mb-0 d-flex align-items-center">
                                                                      Social Committee
                                                                    </p>
                                                                  </div>


                                                                  <div className="col-sm-12 mb-2 d-flex">
                                                                    <input
                                                                      className="form-check-input"
                                                                      type="checkbox"
                                                                      name="Affiliate Committee"
                                                                      onChange={(e) =>
                                                                        handleFieldChange(
                                                                          "Affiliate Committee",
                                                                          e.target.checked
                                                                        )
                                                                      }
                                                                      checked={
                                                                        fieldValues[
                                                                        "Affiliate Committee"
                                                                        ]
                                                                      }
                                                                    />
                                                                    &nbsp;
                                                                    <p className="float-left mb-0 d-flex align-items-center">
                                                                      Affiliate Committee
                                                                    </p>
                                                                  </div>
                                                                </div>


                                                                {Object.keys(signatures).length > 0 ? (
                                                                  <div className="pull-right mt-3">
                                                                    <button
                                                                      className="btn btn-primary btn-sm"
                                                                      // onClick={handleAddTexterm}
                                                                      onClick={() => handleFillAndGeneratePdfUCAR(onBoard[currentStep])}
                                                                      disabled={isdone}
                                                                    >
                                                                      {isdone ? "Please wait..." : "Next"}

                                                                      <i className="fi-chevron-right fs-sm ms-2"></i>
                                                                    </button>
                                                                  </div>
                                                                ) : (
                                                                  <></>
                                                                )
                                                                }
                                                              </div>

                                                            </>
                                                            :
                                                            checkurlstring(onBoard[currentStep].image, "Salt Lake Board Membership") ?
                                                              <>
                                                                <div className="row">
                                                                  <label className="col-form-label float-left w-100" for="pr-fn">Please complete the following form to complete signup.</label>

                                                                  <div className="col-sm-12 mb-2">
                                                                    <label className="form-label" for="pr-fn">
                                                                      *. PLEASE SELECT YOUR APPLICATION TYPE.
                                                                    </label>
                                                                    <div className="row">
                                                                      <div className="col-sm-12 mb-2 d-flex">
                                                                        <input
                                                                          className="form-check-input"
                                                                          type="checkbox"
                                                                          name="JOIN AS A NEW MEMBER"
                                                                          onChange={(e) =>
                                                                            handleFieldChange(
                                                                              "JOIN AS A NEW MEMBER",
                                                                              e.target.checked
                                                                            )
                                                                          }
                                                                          checked={
                                                                            fieldValues[
                                                                            "JOIN AS A NEW MEMBER"
                                                                            ]
                                                                          }
                                                                        />
                                                                        &nbsp;
                                                                        <p className="float-left mb-0 d-flex align-items-center">
                                                                          NEW MEMBER
                                                                        </p>
                                                                      </div>
                                                                      <div className="col-sm-12 mb-2 d-flex">
                                                                        <input
                                                                          className="form-check-input"
                                                                          type="checkbox"
                                                                          name="REACTIVATE MY INACTIVE MEMBERSHIP"
                                                                          onChange={(e) =>
                                                                            handleFieldChange(
                                                                              "REACTIVATE MY INACTIVE MEMBERSHIP",
                                                                              e.target.checked
                                                                            )
                                                                          }
                                                                          checked={
                                                                            fieldValues[
                                                                            "REACTIVATE MY INACTIVE MEMBERSHIP"
                                                                            ]
                                                                          }
                                                                        />
                                                                        &nbsp;
                                                                        <p className="float-left mb-0 d-flex align-items-center">
                                                                          NON-MEMBER
                                                                        </p>
                                                                      </div>
                                                                      <div className="col-sm-12 mb-2 d-flex">
                                                                        <input
                                                                          className="form-check-input"
                                                                          type="checkbox"
                                                                          name="TRANSFER TO A NEW BROKERAGE"
                                                                          onChange={(e) =>
                                                                            handleFieldChange(
                                                                              "TRANSFER TO A NEW BROKERAGE",
                                                                              e.target.checked
                                                                            )
                                                                          }
                                                                          checked={
                                                                            fieldValues[
                                                                            "TRANSFER TO A NEW BROKERAGE"
                                                                            ]
                                                                          }
                                                                        />
                                                                        &nbsp;
                                                                        <p className="float-left mb-0 d-flex align-items-center">
                                                                          TRANSFER TO A NEW BROKERAGE
                                                                        </p>
                                                                      </div>
                                                                      <div className="col-sm-12 mb-2 d-flex">
                                                                        <input
                                                                          className="form-check-input"
                                                                          type="checkbox"
                                                                          name="JOIN AS A SECONDARY MEMBER"
                                                                          onChange={(e) =>
                                                                            handleFieldChange(
                                                                              "JOIN AS A SECONDARY MEMBER",
                                                                              e.target.checked
                                                                            )
                                                                          }
                                                                          checked={
                                                                            fieldValues[
                                                                            "JOIN AS A SECONDARY MEMBER"
                                                                            ]
                                                                          }
                                                                        />
                                                                        &nbsp;
                                                                        <p className="float-left mb-0 d-flex align-items-center">
                                                                          SECONDARY (NO KEY)
                                                                        </p>
                                                                      </div>

                                                                      <div className="col-sm-12 mb-2 d-flex">
                                                                        <input
                                                                          className="form-check-input"
                                                                          type="checkbox"
                                                                          name="TERMINATE MY MEMBERSHIP"
                                                                          onChange={(e) =>
                                                                            handleFieldChange(
                                                                              "TERMINATE MY MEMBERSHIP",
                                                                              e.target.checked
                                                                            )
                                                                          }
                                                                          checked={
                                                                            fieldValues[
                                                                            "TERMINATE MY MEMBERSHIP"
                                                                            ]
                                                                          }
                                                                        />
                                                                        &nbsp;
                                                                        <p className="float-left mb-0 d-flex align-items-center">
                                                                          REACTIVATE MY INACTIVE MEMBERSHIP
                                                                        </p>
                                                                      </div>

                                                                      <div className="col-sm-12 mb-2 d-flex">
                                                                        <input
                                                                          className="form-check-input"
                                                                          type="checkbox"
                                                                          name="TRANSFER FROM ANOTHER BOARD"
                                                                          onChange={(e) =>
                                                                            handleFieldChange(
                                                                              "TRANSFER FROM ANOTHER BOARD",
                                                                              e.target.checked
                                                                            )
                                                                          }
                                                                          checked={
                                                                            fieldValues[
                                                                            "TRANSFER FROM ANOTHER BOARD"
                                                                            ]
                                                                          }
                                                                        />
                                                                        &nbsp;
                                                                        <p className="float-left mb-0 d-flex align-items-center">
                                                                          TRANSFER FROM ANOTHER BOARD
                                                                        </p>
                                                                      </div>

                                                                      <div className="col-sm-12 mb-2 d-flex">
                                                                        <input
                                                                          className="form-check-input"
                                                                          type="checkbox"
                                                                          name="Check Box1"
                                                                          onChange={(e) =>
                                                                            handleFieldChange(
                                                                              "Check Box1",
                                                                              e.target.checked
                                                                            )
                                                                          }
                                                                          checked={
                                                                            fieldValues[
                                                                            "Check Box1"
                                                                            ]
                                                                          }
                                                                        />
                                                                        &nbsp;
                                                                        <p className="float-left mb-0 d-flex align-items-center">
                                                                          SECONDARY W/KEY
                                                                        </p>
                                                                      </div>


                                                                      <div className="col-sm-12 mb-2 d-flex">
                                                                        <input
                                                                          className="form-check-input"
                                                                          type="checkbox"
                                                                          name="JOIN AS A NONMEMBER"
                                                                          onChange={(e) =>
                                                                            handleFieldChange(
                                                                              "JOIN AS A NONMEMBER",
                                                                              e.target.checked
                                                                            )
                                                                          }
                                                                          checked={
                                                                            fieldValues[
                                                                            "JOIN AS A NONMEMBER"
                                                                            ]
                                                                          }
                                                                        />
                                                                        &nbsp;
                                                                        <p className="float-left mb-0 d-flex align-items-center">
                                                                          INACTIVATE MY MEMBERSHIP ($75 annual fee)
                                                                        </p>
                                                                      </div>


                                                                      <div className="col-sm-12 mb-2 d-flex">
                                                                        <input
                                                                          className="form-check-input"
                                                                          type="checkbox"
                                                                          name="INACTIVE MY MEMBERSHIP"
                                                                          onChange={(e) =>
                                                                            handleFieldChange(
                                                                              "INACTIVE MY MEMBERSHIP",
                                                                              e.target.checked
                                                                            )
                                                                          }
                                                                          checked={
                                                                            fieldValues[
                                                                            "INACTIVE MY MEMBERSHIP"
                                                                            ]
                                                                          }
                                                                        />
                                                                        &nbsp;
                                                                        <p className="float-left mb-0 d-flex align-items-center">
                                                                          TERMINATE MY MEMBERSHIP
                                                                        </p>
                                                                      </div>

                                                                    </div>
                                                                  </div>



                                                                  <div className="col-sm-12 mb-2">
                                                                    <label className="form-label" for="pr-fn">
                                                                      *. SECTION 1 REALTOR® INFORMATION
                                                                    </label>
                                                                    <p className="mb-0">
                                                                      HAVE YOU EVER BEEN A MEMBER OF A REALTOR® ORGANIZATION BEFORE?
                                                                    </p>
                                                                    <div className="row">
                                                                      <div className="col-sm-12 mb-2 d-flex">
                                                                        <input
                                                                          className="form-check-input"
                                                                          type="checkbox"
                                                                          name="Check Box2"
                                                                          onChange={(e) =>
                                                                            handleFieldChange(
                                                                              "Check Box2",
                                                                              e.target.checked
                                                                            )
                                                                          }
                                                                          checked={
                                                                            fieldValues[
                                                                            "Check Box2"
                                                                            ]
                                                                          }
                                                                        />
                                                                        &nbsp;
                                                                        <p className="float-left mb-0 d-flex align-items-center">
                                                                          Yes
                                                                        </p>
                                                                      </div>
                                                                      <div className="col-sm-12 mb-2 d-flex">
                                                                        <input
                                                                          className="form-check-input"
                                                                          type="checkbox"
                                                                          name="officeassistant"
                                                                          onChange={(e) =>
                                                                            handleFieldChange(
                                                                              "officeassistant",
                                                                              e.target.checked
                                                                            )
                                                                          }
                                                                          checked={
                                                                            fieldValues[
                                                                            "officeassistant"
                                                                            ]
                                                                          }
                                                                        />
                                                                        &nbsp;
                                                                        <p className="float-left mb-0 d-flex align-items-center">
                                                                          No
                                                                        </p>
                                                                      </div>

                                                                    </div>
                                                                  </div>

                                                                  <div className="col-sm-12 mb-2">

                                                                    <p className="mb-0">
                                                                      (IF YES) PREVIOUS REALTOR® ASSOCIATION NAME
                                                                    </p>
                                                                    <input
                                                                      className="form-control form-control-lg"
                                                                      id="inline-form-input"
                                                                      type="text"
                                                                      name="IF YES PREVIOUS REALTOR ASSOCIATION NAME"
                                                                      // value={formValuesAboout?.IF YES PREVIOUS REALTOR ASSOCIATION NAME}
                                                                      onChange={(e) =>
                                                                        handleFieldChange(
                                                                          "IF YES PREVIOUS REALTOR ASSOCIATION NAME",
                                                                          e.target.value
                                                                        )
                                                                      }
                                                                    />
                                                                  </div>
                                                                  <div className="col-sm-12 mb-2">
                                                                    <label className="form-label" for="pr-fn">
                                                                      *. NATIONAL REALTOR® (NRDS) #:
                                                                    </label>
                                                                    {/* <p className="mb-0">(number, street, and apt. or suite no.) See instructions.</p> */}
                                                                    <input
                                                                      className="form-control form-control-lg"
                                                                      id="inline-form-input"
                                                                      type="text"
                                                                      name="NATIONAL REALTOR NRDS"
                                                                      // value={formValuesAboout?.NATIONAL REALTOR NRDS}
                                                                      onChange={(e) =>
                                                                        handleFieldChange(
                                                                          "NATIONAL REALTOR NRDS",
                                                                          e.target.value
                                                                        )
                                                                      }
                                                                    />
                                                                  </div>

                                                                  <div className="col-sm-12 mb-2">
                                                                    <label className="form-label" for="pr-fn">
                                                                      *. UT DIVISION OF REAL ESTATE LICENSE #:
                                                                    </label>
                                                                    {/* <p className="mb-0">(number, street, and apt. or suite no.) See instructions.</p> */}
                                                                    <input
                                                                      className="form-control form-control-lg"
                                                                      id="inline-form-input"
                                                                      type="text"
                                                                      name="UT DIVISION OF REAL ESTATE LICENSE"
                                                                      // value={formValuesAboout?.UT DIVISION OF REAL ESTATE LICENSE}
                                                                      onChange={(e) =>
                                                                        handleFieldChange(
                                                                          "UT DIVISION OF REAL ESTATE LICENSE",
                                                                          e.target.value
                                                                        )
                                                                      }
                                                                    />
                                                                  </div>

                                                                  <div className="col-sm-12 mb-2">
                                                                    <label className="form-label" for="pr-fn">
                                                                      *. SECTION 2 PERSONAL INFORMATION
                                                                    </label>
                                                                  </div>

                                                                  <div className="col-sm-12 mb-2">

                                                                    <p className="mb-0">
                                                                      FIRST NAME:
                                                                    </p>
                                                                    <input
                                                                      className="form-control form-control-lg"
                                                                      id="inline-form-input"
                                                                      type="text"
                                                                      name="FIRST NAME"
                                                                      // value={formValuesAboout?.FIRST NAME}
                                                                      onChange={(e) =>
                                                                        handleFieldChange(
                                                                          "FIRST NAME",
                                                                          e.target.value
                                                                        )
                                                                      }
                                                                    />
                                                                  </div>
                                                                  <div className="col-sm-12 mb-2">
                                                                    <label className="form-label" for="pr-fn">
                                                                      *. MIDDLE NAME:
                                                                    </label>
                                                                    {/* <p className="mb-0">(number, street, and apt. or suite no.) See instructions.</p> */}
                                                                    <input
                                                                      className="form-control form-control-lg"
                                                                      id="inline-form-input"
                                                                      type="text"
                                                                      name="MIDDLE NAME"
                                                                      // value={formValuesAboout?.MIDDLE NAME}
                                                                      onChange={(e) =>
                                                                        handleFieldChange(
                                                                          "MIDDLE NAME",
                                                                          e.target.value
                                                                        )
                                                                      }
                                                                    />
                                                                  </div>

                                                                  <div className="col-sm-12 mb-2">
                                                                    <label className="form-label" for="pr-fn">
                                                                      *. LAST NAME:
                                                                    </label>
                                                                    <p className="mb-0"></p>
                                                                    <input
                                                                      className="form-control form-control-lg"
                                                                      id="inline-form-input"
                                                                      type="text"
                                                                      name="LAST NAME"
                                                                      // value={formValuesAboout?.LAST NAME}
                                                                      onChange={(e) =>
                                                                        handleFieldChange(
                                                                          "LAST NAME",
                                                                          e.target.value
                                                                        )
                                                                      }
                                                                    />
                                                                  </div>

                                                                  <div className="col-sm-12 mb-2">
                                                                    <label className="form-label" for="pr-fn">
                                                                      *. PHONE:
                                                                    </label>
                                                                    <p className="mb-0"></p>
                                                                    <input
                                                                      className="form-control form-control-lg"
                                                                      id="inline-form-input"
                                                                      type="text"
                                                                      name="PHONE"
                                                                      // value={formValuesAboout?.PHONE}
                                                                      onChange={(e) =>
                                                                        handleFieldChange(
                                                                          "PHONE",
                                                                          e.target.value
                                                                        )
                                                                      }
                                                                    />
                                                                  </div>

                                                                  <div className="col-sm-12 mb-2">
                                                                    <label className="form-label" for="pr-fn">
                                                                      *. EMAIL ADDRESS::
                                                                    </label>
                                                                    <p className="mb-0"></p>
                                                                    <input
                                                                      className="form-control form-control-lg"
                                                                      id="inline-form-input"
                                                                      type="text"
                                                                      name="EMAIL ADDRESS"
                                                                      // value={formValuesAboout?.EMAIL ADDRESS}
                                                                      onChange={(e) =>
                                                                        handleFieldChange(
                                                                          "EMAIL ADDRESS",
                                                                          e.target.value
                                                                        )
                                                                      }
                                                                    />
                                                                  </div>
                                                                  <div className="col-sm-12 mb-2">
                                                                    <label className="form-label" for="pr-fn">
                                                                      *. HOME ADDRESS:
                                                                    </label>
                                                                    <p className="mb-0"></p>
                                                                    <input
                                                                      className="form-control form-control-lg"
                                                                      id="inline-form-input"
                                                                      type="text"
                                                                      name="HOME ADDRESS"
                                                                      // value={formValuesAboout?.HOME ADDRESS}
                                                                      onChange={(e) =>
                                                                        handleFieldChange(
                                                                          "HOME ADDRESS",
                                                                          e.target.value
                                                                        )
                                                                      }
                                                                    />
                                                                  </div>


                                                                  <div className="col-sm-12 mb-2">
                                                                    <label className="form-label" for="pr-fn">
                                                                      *. CITY :
                                                                    </label>
                                                                    <p className="mb-0"></p>
                                                                    <input
                                                                      className="form-control form-control-lg"
                                                                      id="inline-form-input"
                                                                      type="text"
                                                                      name="CITY"
                                                                      // value={formValuesAboout?.CITY}
                                                                      onChange={(e) =>
                                                                        handleFieldChange(
                                                                          "CITY",
                                                                          e.target.value
                                                                        )
                                                                      }
                                                                    />
                                                                  </div>

                                                                  <div className="col-sm-12 mb-2">
                                                                    <label className="form-label" for="pr-fn">
                                                                      *. STATE :
                                                                    </label>
                                                                    <p className="mb-0"></p>
                                                                    <input
                                                                      className="form-control form-control-lg"
                                                                      id="inline-form-input"
                                                                      type="text"
                                                                      name="STATE"
                                                                      // value={formValuesAboout?.STATE}
                                                                      onChange={(e) =>
                                                                        handleFieldChange(
                                                                          "STATE",
                                                                          e.target.value
                                                                        )
                                                                      }
                                                                    />
                                                                  </div>

                                                                  <div className="col-sm-12 mb-2">
                                                                    <label className="form-label" for="pr-fn">
                                                                      *. ZIP :
                                                                    </label>
                                                                    <p className="mb-0"></p>
                                                                    <input
                                                                      className="form-control form-control-lg"
                                                                      id="inline-form-input"
                                                                      type="text"
                                                                      name="ZIP"
                                                                      // value={formValuesAboout?.ZIP}
                                                                      onChange={(e) =>
                                                                        handleFieldChange(
                                                                          "ZIP",
                                                                          e.target.value
                                                                        )
                                                                      }
                                                                    />
                                                                  </div>

                                                                  <div className="col-sm-12 mb-2">
                                                                    <label className="form-label" for="pr-fn">
                                                                      *. LANGUAGES SPOKEN:
                                                                    </label>
                                                                    {/* <p className="mb-0">
                                                                    Please select one of the following options:
                                                                  </p> */}
                                                                    <div className="row">
                                                                      <div className="col-sm-12 mb-2 d-flex">
                                                                        <input
                                                                          className="form-check-input"
                                                                          type="checkbox"
                                                                          name="ENGLISH"
                                                                          onChange={(e) =>
                                                                            handleFieldChange(
                                                                              "ENGLISH",
                                                                              e.target.checked
                                                                            )
                                                                          }
                                                                          checked={
                                                                            fieldValues[
                                                                            "ENGLISH"
                                                                            ]
                                                                          }
                                                                        />
                                                                        &nbsp;
                                                                        <p className="float-left mb-0 d-flex align-items-center">
                                                                          ENGLISH
                                                                        </p>
                                                                      </div>
                                                                      <div className="col-sm-12 mb-2 d-flex">
                                                                        <input
                                                                          className="form-check-input"
                                                                          type="checkbox"
                                                                          name="SPANISH"
                                                                          onChange={(e) =>
                                                                            handleFieldChange(
                                                                              "SPANISH",
                                                                              e.target.checked
                                                                            )
                                                                          }
                                                                          checked={
                                                                            fieldValues[
                                                                            "SPANISH"
                                                                            ]
                                                                          }
                                                                        />
                                                                        &nbsp;
                                                                        <p className="float-left mb-0 d-flex align-items-center">
                                                                          SPANISH
                                                                        </p>
                                                                      </div>
                                                                      <div className="col-sm-12 mb-2 d-flex">
                                                                        <input
                                                                          className="form-check-input"
                                                                          type="checkbox"
                                                                          name="MANDARIN"
                                                                          onChange={(e) =>
                                                                            handleFieldChange(
                                                                              "MANDARIN",
                                                                              e.target.checked
                                                                            )
                                                                          }
                                                                          checked={
                                                                            fieldValues[
                                                                            "MANDARIN"
                                                                            ]
                                                                          }
                                                                        />
                                                                        &nbsp;
                                                                        <p className="float-left mb-0 d-flex align-items-center">
                                                                          MANDARIN
                                                                        </p>
                                                                      </div>
                                                                      <div className="col-sm-12 mb-2 d-flex">
                                                                        <input
                                                                          className="form-check-input"
                                                                          type="checkbox"
                                                                          name="undefined"
                                                                          onChange={(e) =>
                                                                            handleFieldChange(
                                                                              "undefined",
                                                                              e.target.checked
                                                                            )
                                                                          }
                                                                          checked={
                                                                            fieldValues[
                                                                            "undefined"
                                                                            ]
                                                                          }
                                                                        />
                                                                        &nbsp;
                                                                        <p className="float-left mb-0 d-flex align-items-center">
                                                                          OTHER
                                                                        </p>
                                                                      </div>
                                                                      
                                                                      <div className="col-sm-12 mb-2 d-flex">
                                                                        <input
                                                                          className="form-control form-control-lg"
                                                                          id="inline-form-input"
                                                                          type="text"
                                                                          name="OTHER_2"
                                                                          // value={formValuesAboout?.COMPANY NAME}
                                                                          onChange={(e) =>
                                                                            handleFieldChange(
                                                                              "OTHER_2",
                                                                              e.target.value
                                                                            )
                                                                          }
                                                                        /> 
                                                                      </div>



                                                                    </div>
                                                                  </div>

                                                                  <div className="col-sm-12 mb-2">
                                                                    <label className="form-label" htmlFor="veteran-checkbox">
                                                                      *. VETERAN :
                                                                    </label>
                                                                    <div className="row">
                                                                      <div className="col-sm-12 mb-2 d-flex">
                                                                        <input
                                                                          className="form-check-input"
                                                                          type="checkbox"
                                                                          name="veteranyes"
                                                                          onChange={(e) =>
                                                                            handleFieldChange(
                                                                              "veteranyes",
                                                                              e.target.checked
                                                                            )
                                                                          }
                                                                          checked={
                                                                            fieldValues[
                                                                            "veteranyes"
                                                                            ]
                                                                          }
                                                                        />
                                                                        &nbsp;
                                                                        <label htmlFor="veteran-yes" className="float-left mb-0 d-flex align-items-center">
                                                                          YES
                                                                        </label>
                                                                      </div>
                                                                      <div className="col-sm-12 mb-2 d-flex">
                                                                        <input
                                                                          className="form-check-input"
                                                                          type="checkbox"
                                                                          name="veteranno"
                                                                          onChange={(e) =>
                                                                            handleFieldChange(
                                                                              "veteranno",
                                                                              e.target.checked
                                                                            )
                                                                          }
                                                                          checked={
                                                                            fieldValues[
                                                                            "veteranno"
                                                                            ]
                                                                          }
                                                                        />
                                                                        &nbsp;
                                                                        <label htmlFor="veteran-no" className="float-left mb-0 d-flex align-items-center">
                                                                          NO
                                                                        </label>
                                                                      </div>
                                                                    </div>
                                                                  </div>



                                                                  <div className="col-sm-12 mb-2">
                                                                    <label className="form-label" for="pr-fn">
                                                                      *. GENDER :
                                                                    </label>
                                                                    <div className="row">
                                                                      <div className="col-sm-12 mb-2 d-flex">
                                                                        <input
                                                                          className="form-check-input"
                                                                          type="checkbox"
                                                                          name="male"
                                                                          onChange={(e) =>
                                                                            handleFieldChange(
                                                                              "male",
                                                                              e.target.checked
                                                                            )
                                                                          }
                                                                          checked={
                                                                            fieldValues[
                                                                            "male"
                                                                            ]
                                                                          }
                                                                        />
                                                                        &nbsp;
                                                                        <p className="float-left mb-0 d-flex align-items-center">
                                                                          MALE
                                                                        </p>
                                                                      </div>
                                                                      <div className="col-sm-12 mb-2 d-flex">
                                                                        <input
                                                                          className="form-check-input"
                                                                          type="checkbox"
                                                                          name="female"
                                                                          onChange={(e) =>
                                                                            handleFieldChange(
                                                                              "female",
                                                                              e.target.checked
                                                                            )
                                                                          }
                                                                          checked={
                                                                            fieldValues[
                                                                            "female"
                                                                            ]
                                                                          }
                                                                        />
                                                                        &nbsp;
                                                                        <p className="float-left mb-0 d-flex align-items-center">
                                                                          FEMALE
                                                                        </p>
                                                                      </div>

                                                                      <div className="col-sm-12 mb-2 d-flex">
                                                                        <input
                                                                          className="form-check-input"
                                                                          type="checkbox"
                                                                          name="nonbinary"
                                                                          onChange={(e) =>
                                                                            handleFieldChange(
                                                                              "nonbinary",
                                                                              e.target.checked
                                                                            )
                                                                          }
                                                                          checked={
                                                                            fieldValues[
                                                                            "nonbinary"
                                                                            ]
                                                                          }
                                                                        />
                                                                        &nbsp;
                                                                        <p className="float-left mb-0 d-flex align-items-center">
                                                                          NON-BINARY
                                                                        </p>
                                                                      </div>
                                                                    </div>
                                                                  </div>


                                                                  <div className="col-sm-12 mb-2">
                                                                    <label className="form-label" for="pr-fn">
                                                                      *. SECTION 3 BROKERAGE INFORMATION
                                                                    </label>

                                                                    {/* <label className="form-label" for="pr-fn">
                                                                      *. COMPANY NAME:
                                                                    </label>
                                                                    <input
                                                                      className="form-control form-control-lg"
                                                                      id="inline-form-input"
                                                                      type="text"
                                                                      name="COMPANY NAME"
                                                                      // value={formValuesAboout?.COMPANY NAME}
                                                                      onChange={(e) =>
                                                                        handleFieldChange(
                                                                          "COMPANY NAME",
                                                                          e.target.value
                                                                        )
                                                                      }
                                                                    /> */}
                                                                  </div>

                                                                  {/* <div className="col-sm-12 mb-2">
                                                                      <label className="form-label" for="pr-fn">
                                                                        *. BROKER NAME:
                                                                      </label>
                                                                      <input
                                                                        className="form-control form-control-lg"
                                                                        id="inline-form-input"
                                                                        type="text"
                                                                        name="BROKER NAME"
                                                                        // value={formValuesAboout?.BROKER NAME}
                                                                        onChange={(e) =>
                                                                          handleFieldChange(
                                                                            "BROKER NAME",
                                                                            e.target.value
                                                                          )
                                                                        }
                                                                      />
                                                                    </div> */}

                                                                  <div className="col-sm-12 mb-2">
                                                                    <label className="form-label" for="pr-fn">
                                                                      *. PRIOR COMPANY NAME:
                                                                    </label>
                                                                    <input
                                                                      className="form-control form-control-lg"
                                                                      id="inline-form-input"
                                                                      type="text"
                                                                      name="PRIOR COMPANY NAME"
                                                                      // value={formValuesAboout?.PRIOR COMPANY NAME}
                                                                      onChange={(e) =>
                                                                        handleFieldChange(
                                                                          "PRIOR COMPANY NAME",
                                                                          e.target.value
                                                                        )
                                                                      }
                                                                    />
                                                                  </div>


                                                                  <div className="col-sm-12 mb-2">
                                                                    <label className="form-label" for="pr-fn">
                                                                      *. PRIOR BROKER NAME:
                                                                    </label>
                                                                    <input
                                                                      className="form-control form-control-lg"
                                                                      id="inline-form-input"
                                                                      type="text"
                                                                      name="PRIOR BROKER NAME"
                                                                      // value={formValuesAboout?.PRIOR BROKER NAME}
                                                                      onChange={(e) =>
                                                                        handleFieldChange(
                                                                          "PRIOR BROKER NAME",
                                                                          e.target.value
                                                                        )
                                                                      }
                                                                    />
                                                                  </div>

                                                                  <div className="col-sm-12 mb-2">
                                                                    <label className="form-label" for="pr-fn">
                                                                      *. MEMBERSHIP AGREEMENT  :
                                                                    </label>
                                                                    <div className="row">
                                                                      {/* <div className="col-sm-12 mb-2">

                                                                      <input
                                                                        className="form-control form-control-lg"
                                                                        id="inline-form-input"
                                                                        type="text"
                                                                        name="AGREEMENT1"
                                                                        // value={formValuesAboout?.1}
                                                                        onChange={(e) =>
                                                                          handleFieldChange(
                                                                            "AGREEMENT1",
                                                                            e.target.value
                                                                          )
                                                                        }
                                                                      />
                                                                      <label className="form-label" for="pr-fn">
                                                                        I agree to complete the required Code of Ethics and Realtor Essentials courses (if applicable), as well as any other course the Board may require, within 60 days of joining the Board, or as otherwise specified by the Membership Department. I acknowledge that failure to complete all required courses will result in the termination of my membership and forfeiture of my dues and application fee.
                                                                      </label>
                                                                    </div> */}


                                                                      <div className="col-sm-12 mb-2">

                                                                        <input
                                                                          className="form-control form-control-lg"
                                                                          id="inline-form-input"
                                                                          type="text"
                                                                          name="2"
                                                                          // value={formValuesAboout?.2}
                                                                          onChange={(e) =>
                                                                            handleFieldChange(
                                                                              "2",
                                                                              e.target.value
                                                                            )
                                                                          }
                                                                        />
                                                                        <label className="form-label" for="pr-fn">
                                                                          I agree to become familiar with the Code of Ethics, as well as the bylaws of the Salt Lake Board of Realtors®, which can be found on the Board
                                                                          website. I will also familiarize myself with the rules and regulations of the Utah Association of Realtors® and the National Association
                                                                          of Realtors®. The annual renewal of my membership shall indicate my continued commitment to abide by the standards as they are amended
                                                                          from time to time.
                                                                        </label>
                                                                      </div>


                                                                      <div className="col-sm-12 mb-2">

                                                                        <input
                                                                          className="form-control form-control-lg"
                                                                          id="inline-form-input"
                                                                          type="text"
                                                                          name="3"
                                                                          // value={formValuesAboout?.3}
                                                                          onChange={(e) =>
                                                                            handleFieldChange(
                                                                              "3",
                                                                              e.target.value
                                                                            )
                                                                          }
                                                                        />
                                                                        <label className="form-label" for="pr-fn">
                                                                          I acknowledge that if I become a Board member and then subsequently resign, or cause my membership to be terminated, with an ethics
                                                                          complaint pending, future reactivation may be conditioned upon my compliance with any ethics hearing determinations.
                                                                        </label>
                                                                      </div>


                                                                      <div className="col-sm-12 mb-2">

                                                                        <input
                                                                          className="form-control form-control-lg"
                                                                          id="inline-form-input"
                                                                          type="text"
                                                                          name="4"
                                                                          // value={formValuesAboout?.4}
                                                                          onChange={(e) =>
                                                                            handleFieldChange(
                                                                              "4",
                                                                              e.target.value
                                                                            )
                                                                          }
                                                                        />
                                                                        <label className="form-label" for="pr-fn">
                                                                          I acknowledge that if I become a Board member and then subsequently resign, or otherwise cause my membership to be terminated, the duty to
                                                                          submit to arbitration continues in effect even after my membership resignation/termination, provided the dispute arose while my membership
                                                                          was active.
                                                                        </label>
                                                                      </div>


                                                                      <div className="col-sm-12 mb-2">

                                                                        <input
                                                                          className="form-control form-control-lg"
                                                                          id="inline-form-input"
                                                                          type="text"
                                                                          name="5"
                                                                          // value={formValuesAboout?.5}
                                                                          onChange={(e) =>
                                                                            handleFieldChange(
                                                                              "5",
                                                                              e.target.value
                                                                            )
                                                                          }
                                                                        />
                                                                        <label className="form-label" for="pr-fn">
                                                                          I agree to pay my annual renewal dues each June, in the amount specified to me by the Board. I understand that dues are subject to review and
                                                                          possible change annually. I recognize that dues are non-refundable, and that once paid, they become the property of the Board. If I do
                                                                          not renew, or my membership is terminated, I will immediately cease to represent myself as a Realtor®.
                                                                        </label>
                                                                      </div>


                                                                      <div className="col-sm-12 mb-2">

                                                                        <input
                                                                          className="form-control form-control-lg"
                                                                          id="inline-form-input"
                                                                          type="text"
                                                                          name="6"
                                                                          // value={formValuesAboout?.6}
                                                                          onChange={(e) =>
                                                                            handleFieldChange(
                                                                              "6",
                                                                              e.target.value
                                                                            )
                                                                          }
                                                                        />
                                                                        <label className="form-label" for="pr-fn">
                                                                          I understand that the termination of my membership for non-payment of annual dues (or for any other reason) will result in a $350 re-entry
                                                                          fee to be paid upon possible future reactivation with the Board.
                                                                        </label>
                                                                      </div>


                                                                      <div className="col-sm-12 mb-2">

                                                                        <input
                                                                          className="form-control form-control-lg"
                                                                          id="inline-form-input"
                                                                          type="text"
                                                                          name="7"
                                                                          // value={formValuesAboout?.7}
                                                                          onChange={(e) =>
                                                                            handleFieldChange(
                                                                              "7",
                                                                              e.target.value
                                                                            )
                                                                          }
                                                                        />
                                                                        <label className="form-label" for="pr-fn">
                                                                          I consent that the Salt Lake, Utah, and National Association of Realtors® and their subsidiaries, may contact me using the information provided
                                                                          on this form, or by other available means of communication. This consent extends to possible future changes in contact information. I waive
                                                                          any limits placed on communication by certain state/federal laws in order to receive all membership communications.
                                                                        </label>
                                                                      </div>

                                                                    </div>
                                                                  </div>


                                                                  {Object.keys(signatures).length > 0 ? (
                                                                    <div className="pull-right mt-3">
                                                                      <button
                                                                        className="btn btn-primary btn-sm"
                                                                        // onClick={handleAddTexterm}
                                                                        onClick={() => handleFillAndGeneratePdfSaltLake(onBoard[currentStep])}
                                                                     //   disabled={isdone}
                                                                      >
                                                                        {isdone ? "Please wait..." : "Next"}

                                                                        <i className="fi-chevron-right fs-sm ms-2"></i>
                                                                      </button>
                                                                    </div>
                                                                  ) : (
                                                                    <></>
                                                                  )
                                                                  }
                                                                </div>
                                                              </>
                                                              :
                                                              checkurlstring(onBoard[currentStep].image, "Northern Wasatch") ?
                                                                <>
                                                                  {/* Salt Lake Board Membership Form */}
                                                                  <div className="row">
                                                                    <label className="col-form-label float-left w-100" for="pr-fn">Please complete the following form to complete signup.</label>

                                                                    <div className="col-sm-12 mb-2">
                                                                      <label className="form-label" for="pr-fn">
                                                                        *. PLEASE SELECT YOUR APPLICATION TYPE.
                                                                      </label>
                                                                      <div className="row">
                                                                        <div className="col-sm-12 mb-2 d-flex">
                                                                          <input
                                                                            className="form-check-input"
                                                                            type="checkbox"
                                                                            name="broker"
                                                                            onChange={(e) =>
                                                                              handleFieldChange(
                                                                                "broker",
                                                                                e.target.checked
                                                                              )
                                                                            }
                                                                            checked={
                                                                              fieldValues[
                                                                              "broker"
                                                                              ]
                                                                            }
                                                                          />
                                                                          &nbsp;
                                                                          <p className="float-left mb-0 d-flex align-items-center">
                                                                            Broker
                                                                          </p>
                                                                        </div>
                                                                        <div className="col-sm-12 mb-2 d-flex">
                                                                          <input
                                                                            className="form-check-input"
                                                                            type="checkbox"
                                                                            name="agent"
                                                                            onChange={(e) =>
                                                                              handleFieldChange(
                                                                                "agent",
                                                                                e.target.checked
                                                                              )
                                                                            }
                                                                            checked={
                                                                              fieldValues[
                                                                              "agent"
                                                                              ]
                                                                            }
                                                                          />
                                                                          &nbsp;
                                                                          <p className="float-left mb-0 d-flex align-items-center">
                                                                            Agent
                                                                          </p>
                                                                        </div>
                                                                        <div className="col-sm-12 mb-2 d-flex">
                                                                          <input
                                                                            className="form-check-input"
                                                                            type="checkbox"
                                                                            name="appraiser"
                                                                            onChange={(e) =>
                                                                              handleFieldChange(
                                                                                "appraiser",
                                                                                e.target.checked
                                                                              )
                                                                            }
                                                                            checked={
                                                                              fieldValues[
                                                                              "appraiser"
                                                                              ]
                                                                            }
                                                                          />
                                                                          &nbsp;
                                                                          <p className="float-left mb-0 d-flex align-items-center">
                                                                            Appraiser
                                                                          </p>
                                                                        </div>
                                                                        <div className="col-sm-12 mb-2 d-flex">
                                                                          <input
                                                                            className="form-check-input"
                                                                            type="checkbox"
                                                                            name="New"
                                                                            onChange={(e) =>
                                                                              handleFieldChange(
                                                                                "New",
                                                                                e.target.checked
                                                                              )
                                                                            }
                                                                            checked={
                                                                              fieldValues[
                                                                              "New"
                                                                              ]
                                                                            }
                                                                          />
                                                                          &nbsp;
                                                                          <p className="float-left mb-0 d-flex align-items-center">
                                                                            New
                                                                          </p>
                                                                        </div>

                                                                        <div className="col-sm-12 mb-2 d-flex">
                                                                          <input
                                                                            className="form-check-input"
                                                                            type="checkbox"
                                                                            name="reactivate"
                                                                            onChange={(e) =>
                                                                              handleFieldChange(
                                                                                "reactivate",
                                                                                e.target.checked
                                                                              )
                                                                            }
                                                                            checked={
                                                                              fieldValues[
                                                                              "reactivate"
                                                                              ]
                                                                            }
                                                                          />
                                                                          &nbsp;
                                                                          <p className="float-left mb-0 d-flex align-items-center">
                                                                            Reactivate
                                                                          </p>
                                                                        </div>

                                                                        <div className="col-sm-12 mb-2 d-flex">
                                                                          <input
                                                                            className="form-check-input"
                                                                            type="checkbox"
                                                                            name="transfer"
                                                                            onChange={(e) =>
                                                                              handleFieldChange(
                                                                                "transfer",
                                                                                e.target.checked
                                                                              )
                                                                            }
                                                                            checked={
                                                                              fieldValues[
                                                                              "transfer"
                                                                              ]
                                                                            }
                                                                          />
                                                                          &nbsp;
                                                                          <p className="float-left mb-0 d-flex align-items-center">
                                                                            Transfer
                                                                          </p>
                                                                        </div>

                                                                        <div className="col-sm-12 mb-2 d-flex">
                                                                          <input
                                                                            className="form-check-input"
                                                                            type="checkbox"
                                                                            name="changeaddress"
                                                                            onChange={(e) =>
                                                                              handleFieldChange(
                                                                                "changeaddress",
                                                                                e.target.checked
                                                                              )
                                                                            }
                                                                            checked={
                                                                              fieldValues[
                                                                              "changeaddress"
                                                                              ]
                                                                            }
                                                                          />
                                                                          &nbsp;
                                                                          <p className="float-left mb-0 d-flex align-items-center">
                                                                            Change Address/Name /Phone
                                                                          </p>
                                                                        </div>


                                                                        <div className="col-sm-12 mb-2 d-flex">
                                                                          <input
                                                                            className="form-check-input"
                                                                            type="checkbox"
                                                                            name="dropinactive"
                                                                            onChange={(e) =>
                                                                              handleFieldChange(
                                                                                "dropinactive",
                                                                                e.target.checked
                                                                              )
                                                                            }
                                                                            checked={
                                                                              fieldValues[
                                                                              "dropinactive"
                                                                              ]
                                                                            }
                                                                          />
                                                                          &nbsp;
                                                                          <p className="float-left mb-0 d-flex align-items-center">
                                                                            Drop / Inactive
                                                                          </p>
                                                                        </div>
                                                                      </div>
                                                                    </div>


                                                                    <div className="col-sm-12 mb-2">
                                                                      <label className="form-label" for="pr-fn">
                                                                        *. Agent (NRDS) #:
                                                                      </label>
                                                                      {/* <p className="mb-0">(number, street, and apt. or suite no.) See instructions.</p> */}
                                                                      <input
                                                                        className="form-control form-control-lg"
                                                                        id="inline-form-input"
                                                                        type="text"
                                                                        name="nrds"
                                                                        // value={formValuesAboout?.nrds}
                                                                        onChange={(e) =>
                                                                          handleFieldChange(
                                                                            "nrds",
                                                                            e.target.value
                                                                          )
                                                                        }
                                                                      />
                                                                    </div>


                                                                    <div className="col-sm-12 mb-2">
                                                                      <label className="form-label" for="pr-fn">
                                                                        *. PERSONAL INFORMATION
                                                                      </label>
                                                                    </div>

                                                                    <div className="col-sm-12 mb-2">

                                                                      <label className="form-label" for="pr-fn">
                                                                        NAME:
                                                                      </label>
                                                                      <input
                                                                        className="form-control form-control-lg"
                                                                        id="inline-form-input"
                                                                        type="text"
                                                                        name="name"
                                                                        // value={formValuesAboout?.name}
                                                                        onChange={(e) =>
                                                                          handleFieldChange(
                                                                            "name",
                                                                            e.target.value
                                                                          )
                                                                        }
                                                                      />
                                                                    </div>
                                                                    <div className="col-sm-12 mb-2">
                                                                      <label className="form-label" for="pr-fn">
                                                                        *. Address:
                                                                      </label>
                                                                      {/* <p className="mb-0">(number, street, and apt. or suite no.) See instructions.</p> */}
                                                                      <input
                                                                        className="form-control form-control-lg"
                                                                        id="inline-form-input"
                                                                        type="text"
                                                                        name="address"
                                                                        // value={formValuesAboout?.address}
                                                                        onChange={(e) =>
                                                                          handleFieldChange(
                                                                            "address",
                                                                            e.target.value
                                                                          )
                                                                        }
                                                                      />
                                                                    </div>

                                                                    <div className="col-sm-12 mb-2">
                                                                      <label className="form-label" for="pr-fn">
                                                                        *. City:
                                                                      </label>
                                                                      <p className="mb-0"></p>
                                                                      <input
                                                                        className="form-control form-control-lg"
                                                                        id="inline-form-input"
                                                                        type="text"
                                                                        name="city"
                                                                        // value={formValuesAboout?.city}
                                                                        onChange={(e) =>
                                                                          handleFieldChange(
                                                                            "city",
                                                                            e.target.value
                                                                          )
                                                                        }
                                                                      />
                                                                    </div>

                                                                    <div className="col-sm-12 mb-2">
                                                                      <label className="form-label" for="pr-fn">
                                                                        *. State:
                                                                      </label>
                                                                      <p className="mb-0"></p>
                                                                      <input
                                                                        className="form-control form-control-lg"
                                                                        id="inline-form-input"
                                                                        type="text"
                                                                        name="state"
                                                                        // value={formValuesAboout?.state}
                                                                        onChange={(e) =>
                                                                          handleFieldChange(
                                                                            "state",
                                                                            e.target.value
                                                                          )
                                                                        }
                                                                      />
                                                                    </div>

                                                                    <div className="col-sm-12 mb-2">
                                                                      <label className="form-label" for="pr-fn">
                                                                        *. Zip:
                                                                      </label>
                                                                      <p className="mb-0"></p>
                                                                      <input
                                                                        className="form-control form-control-lg"
                                                                        id="inline-form-input"
                                                                        type="text"
                                                                        name="zip"
                                                                        // value={formValuesAboout?.zip}
                                                                        onChange={(e) =>
                                                                          handleFieldChange(
                                                                            "zip",
                                                                            e.target.value
                                                                          )
                                                                        }
                                                                      />
                                                                    </div>

                                                                    <div className="col-sm-12 mb-2">
                                                                      <label className="form-label" for="pr-fn">
                                                                        *. Phone:
                                                                      </label>
                                                                      <p className="mb-0"></p>
                                                                      <input
                                                                        className="form-control form-control-lg"
                                                                        id="inline-form-input"
                                                                        type="text"
                                                                        name="phone"
                                                                        // value={formValuesAboout?.phone}
                                                                        onChange={(e) =>
                                                                          handleFieldChange(
                                                                            "phone",
                                                                            e.target.value
                                                                          )
                                                                        }
                                                                      />
                                                                    </div>


                                                                    <div className="col-sm-12 mb-2">
                                                                      <label className="form-label" for="pr-fn">
                                                                        *. Email :
                                                                      </label>
                                                                      <p className="mb-0"></p>
                                                                      <input
                                                                        className="form-control form-control-lg"
                                                                        id="inline-form-input"
                                                                        type="text"
                                                                        name="email"
                                                                        // value={formValuesAboout?.email}
                                                                        onChange={(e) =>
                                                                          handleFieldChange(
                                                                            "email",
                                                                            e.target.value
                                                                          )
                                                                        }
                                                                      />
                                                                    </div>

                                                                    <div className="col-sm-12 mb-2">
                                                                      <label className="form-label" for="pr-fn">
                                                                        *. New Members & Transfers :
                                                                      </label>
                                                                      <label className="form-label" for="pr-fn">
                                                                        *. I will be associated with  :
                                                                      </label>
                                                                      <p className="mb-0"></p>
                                                                      <input
                                                                        className="form-control form-control-lg"
                                                                        id="inline-form-input"
                                                                        type="text"
                                                                        name="willbeassociatedwith"
                                                                        // value={formValuesAboout?.willbeassociatedwith}
                                                                        onChange={(e) =>
                                                                          handleFieldChange(
                                                                            "willbeassociatedwith",
                                                                            e.target.value
                                                                          )
                                                                        }
                                                                      />
                                                                    </div>

                                                                    <div className="col-sm-12 mb-2">
                                                                      <label className="form-label" for="pr-fn">
                                                                        *. Office Id :
                                                                      </label>
                                                                      <p className="mb-0"></p>
                                                                      <input
                                                                        className="form-control form-control-lg"
                                                                        id="inline-form-input"
                                                                        type="text"
                                                                        name="associatedofficeid"
                                                                        // value={formValuesAboout?.associatedofficeid}
                                                                        onChange={(e) =>
                                                                          handleFieldChange(
                                                                            "associatedofficeid",
                                                                            e.target.value
                                                                          )
                                                                        }
                                                                      />
                                                                    </div>


                                                                    <div className="col-sm-12 mb-2">
                                                                      <label className="form-label" for="pr-fn">
                                                                        *. Transfers & Drops: :
                                                                      </label>
                                                                      <label className="form-label" for="pr-fn">
                                                                        *. I will no longer with :
                                                                      </label>
                                                                      <input
                                                                        className="form-control form-control-lg"
                                                                        id="inline-form-input"
                                                                        type="text"
                                                                        name="nolongerwith"
                                                                        // value={formValuesAboout?.nolongerwith}
                                                                        onChange={(e) =>
                                                                          handleFieldChange(
                                                                            "nolongerwith",
                                                                            e.target.value
                                                                          )
                                                                        }
                                                                      />
                                                                    </div>
                                                                    <div className="col-sm-12 mb-2">

                                                                      <label className="form-label" for="pr-fn">
                                                                        *. Office ID :
                                                                      </label>
                                                                      <input
                                                                        className="form-control form-control-lg"
                                                                        id="inline-form-input"
                                                                        type="text"
                                                                        name="dropofficeid"
                                                                        // value={formValuesAboout?.dropofficeid}
                                                                        onChange={(e) =>
                                                                          handleFieldChange(
                                                                            "dropofficeid",
                                                                            e.target.value
                                                                          )
                                                                        }
                                                                      />
                                                                    </div>


                                                                    {Object.keys(signatures).length > 0 ? (
                                                                      <div className="pull-right mt-3">
                                                                        <button
                                                                          className="btn btn-primary btn-sm"
                                                                          // onClick={handleAddTexterm}
                                                                          onClick={() => handleFillAndGeneratePdfNorthernWasatch(onBoard[currentStep])}
                                                                          disabled={isdone}
                                                                        >
                                                                          {isdone ? "Please wait..." : "Next"}

                                                                          <i className="fi-chevron-right fs-sm ms-2"></i>
                                                                        </button>
                                                                      </div>
                                                                    ) : (
                                                                      <></>
                                                                    )
                                                                    }
                                                                  </div>
                                                                </>
                                                                :
                                                                checkurlstring(onBoard[currentStep].image, "Washington County") ?
                                                                  <>
                                                                    {/*WASHINGTON COUNTY BOARD OF REALTORS */}
                                                                    <div className="row">
                                                                      <label className="col-form-label float-left w-100" for="pr-fn">Please complete the following form to complete signup.</label>

                                                                      <div className="col-sm-12 mb-2">
                                                                        <label className="form-label" for="pr-fn">
                                                                          *. LICENSEE INFORMATION
                                                                        </label>
                                                                      </div>

                                                                      <div className="col-sm-12 mb-2">

                                                                        <label className="form-label" for="pr-fn">
                                                                          AGENT NAME (as it appears on your license):
                                                                        </label>
                                                                        <input
                                                                          className="form-control form-control-lg"
                                                                          id="inline-form-input"
                                                                          type="text"
                                                                          name="agentname"
                                                                          // value={formValuesAboout?.agentname}
                                                                          onChange={(e) =>
                                                                            handleFieldChange(
                                                                              "agentname",
                                                                              e.target.value
                                                                            )
                                                                          }
                                                                        />
                                                                      </div>
                                                                      <div className="col-sm-12 mb-2">
                                                                        <label className="form-label" for="pr-fn">
                                                                          *. UT RE LICENSE #
                                                                        </label>
                                                                        {/* <p className="mb-0">(number, street, and apt. or suite no.) See instructions.</p> */}
                                                                        <input
                                                                          className="form-control form-control-lg"
                                                                          id="inline-form-input"
                                                                          type="text"
                                                                          name="license"
                                                                          // value={formValuesAboout?.license}
                                                                          onChange={(e) =>
                                                                            handleFieldChange(
                                                                              "license",
                                                                              e.target.value
                                                                            )
                                                                          }
                                                                        />
                                                                      </div>

                                                                      <div className="col-sm-12 mb-2">
                                                                        <label className="form-label" for="pr-fn">
                                                                          *. EXPIRATION DATE:
                                                                        </label>
                                                                        <p className="mb-0"></p>
                                                                        <input
                                                                          className="form-control form-control-lg"
                                                                          id="inline-form-input"
                                                                          type="text"
                                                                          name="expirationdate"
                                                                          // value={formValuesAboout?.expirationdate}
                                                                          onChange={(e) =>
                                                                            handleFieldChange(
                                                                              "expirationdate",
                                                                              e.target.value
                                                                            )
                                                                          }
                                                                        />
                                                                      </div>

                                                                      <div className="col-sm-12 mb-2">
                                                                        <label className="form-label" for="pr-fn">
                                                                          *. EMAIL ADDRESS::
                                                                        </label>
                                                                        <p className="mb-0"></p>
                                                                        <input
                                                                          className="form-control form-control-lg"
                                                                          id="inline-form-input"
                                                                          type="text"
                                                                          name="emailaddress"
                                                                          // value={formValuesAboout?.emailaddress}
                                                                          onChange={(e) =>
                                                                            handleFieldChange(
                                                                              "emailaddress",
                                                                              e.target.value
                                                                            )
                                                                          }
                                                                        />
                                                                      </div>

                                                                      <div className="col-sm-12 mb-2">
                                                                        <label className="form-label" for="pr-fn">
                                                                          *. PHONE:
                                                                        </label>
                                                                        <p className="mb-0"></p>
                                                                        <input
                                                                          className="form-control form-control-lg"
                                                                          id="inline-form-input"
                                                                          type="text"
                                                                          name="phone"
                                                                          // value={formValuesAboout?.phone}
                                                                          onChange={(e) =>
                                                                            handleFieldChange(
                                                                              "phone",
                                                                              e.target.value
                                                                            )
                                                                          }
                                                                        />
                                                                      </div>

                                                                      <div className="col-sm-12 mb-2">
                                                                        <label className="form-label" for="pr-fn">
                                                                          *. Cell Phone:
                                                                        </label>
                                                                        <p className="mb-0"></p>
                                                                        <input
                                                                          className="form-control form-control-lg"
                                                                          id="inline-form-input"
                                                                          type="text"
                                                                          name="cellphone"
                                                                          // value={formValuesAboout?.cellphone}
                                                                          onChange={(e) =>
                                                                            handleFieldChange(
                                                                              "cellphone",
                                                                              e.target.value
                                                                            )
                                                                          }
                                                                        />
                                                                      </div>


                                                                      <div className="col-sm-12 mb-2">
                                                                        <label className="form-label" for="pr-fn">
                                                                          *. Home Address :
                                                                        </label>
                                                                        <p className="mb-0"></p>
                                                                        <input
                                                                          className="form-control form-control-lg"
                                                                          id="inline-form-input"
                                                                          type="text"
                                                                          name="homeaddress"
                                                                          // value={formValuesAboout?.homeaddress}
                                                                          onChange={(e) =>
                                                                            handleFieldChange(
                                                                              "homeaddress",
                                                                              e.target.value
                                                                            )
                                                                          }
                                                                        />
                                                                      </div>

                                                                      <div className="col-sm-12 mb-2">

                                                                        <label className="form-label" for="pr-fn">
                                                                          *. City State Zip   :
                                                                        </label>
                                                                        <p className="mb-0"></p>
                                                                        <input
                                                                          className="form-control form-control-lg"
                                                                          id="inline-form-input"
                                                                          type="text"
                                                                          name="citystatezip"
                                                                          // value={formValuesAboout?.citystatezip}
                                                                          onChange={(e) =>
                                                                            handleFieldChange(
                                                                              "citystatezip",
                                                                              e.target.value
                                                                            )
                                                                          }
                                                                        />
                                                                      </div>

                                                                      <div className="col-sm-12 mb-2">
                                                                        <label className="form-label" for="pr-fn">
                                                                          *. Primary Board :
                                                                        </label>
                                                                        <p className="mb-0"></p>
                                                                        <input
                                                                          className="form-control form-control-lg"
                                                                          id="inline-form-input"
                                                                          type="text"
                                                                          name="primaryboard"
                                                                          // value={formValuesAboout?.primaryboard}
                                                                          onChange={(e) =>
                                                                            handleFieldChange(
                                                                              "primaryboard",
                                                                              e.target.value
                                                                            )
                                                                          }
                                                                        />
                                                                      </div>


                                                                      <div className="col-sm-12 mb-2">

                                                                        <label className="form-label" for="pr-fn">
                                                                          *. Birthdate :
                                                                        </label>
                                                                        <input
                                                                          className="form-control form-control-lg"
                                                                          id="inline-form-input"
                                                                          type="text"
                                                                          name="birthdate"
                                                                          // value={formValuesAboout?.birthdate}
                                                                          onChange={(e) =>
                                                                            handleFieldChange(
                                                                              "birthdate",
                                                                              e.target.value
                                                                            )
                                                                          }
                                                                        />
                                                                      </div>
                                                                      <div className="col-sm-12 mb-2">

                                                                        <label className="form-label" for="pr-fn">
                                                                          *. Brokerage Name :
                                                                        </label>
                                                                        <input
                                                                          className="form-control form-control-lg"
                                                                          id="inline-form-input"
                                                                          type="text"
                                                                          name="brokeragename"
                                                                          // value={formValuesAboout?.brokeragename}
                                                                          onChange={(e) =>
                                                                            handleFieldChange(
                                                                              "brokeragename",
                                                                              e.target.value
                                                                            )
                                                                          }
                                                                        />
                                                                      </div>

                                                                      <div className="col-sm-12 mb-2">
                                                                        <label className="form-label" for="pr-fn">
                                                                          *. MEMBERSHIP TYPE
                                                                        </label>
                                                                      </div>

                                                                      <div className="col-sm-12 mb-2">
                                                                        <label className="form-label" for="pr-fn">
                                                                          *. I am applying for (select one):
                                                                        </label>
                                                                        <div className="row">
                                                                          <div className="col-sm-12 mb-2 d-flex">
                                                                            <input
                                                                              className="form-check-input"
                                                                              type="checkbox"
                                                                              name="primarymembership"
                                                                              onChange={(e) =>
                                                                                handleFieldChange(
                                                                                  "primarymembership",
                                                                                  e.target.checked
                                                                                )
                                                                              }
                                                                              checked={
                                                                                fieldValues[
                                                                                "primarymembership"
                                                                                ]
                                                                              }
                                                                            />
                                                                            &nbsp;
                                                                            <p className="float-left mb-0 d-flex align-items-center">
                                                                              PRIMARY MEMBERSHIP WITH MLS ACCESS
                                                                            </p>
                                                                          </div>
                                                                          <div className="col-sm-12 mb-2 d-flex">
                                                                            <input
                                                                              className="form-check-input"
                                                                              type="checkbox"
                                                                              name="secondarymembership"
                                                                              onChange={(e) =>
                                                                                handleFieldChange(
                                                                                  "secondarymembership",
                                                                                  e.target.checked
                                                                                )
                                                                              }
                                                                              checked={
                                                                                fieldValues[
                                                                                "secondarymembership"
                                                                                ]
                                                                              }
                                                                            />
                                                                            &nbsp;
                                                                            <p className="float-left mb-0 d-flex align-items-center">
                                                                              SECONDARY MEMBERSHIP WITH MLS ACCESS (a dues waiver from your Primary will be required to
                                                                              complete application)
                                                                            </p>
                                                                          </div>

                                                                        </div>
                                                                      </div>

                                                                      <div className="col-sm-12 mb-2">
                                                                        <label className="form-label" for="pr-fn">
                                                                          *. QUALIFYING QUESTIONS
                                                                        </label>
                                                                      </div>

                                                                      <p>
                                                                        Have you previously been a member of the WCBR?
                                                                        <input
                                                                          className="form-control form-control-lg"
                                                                          id="inline-form-input"
                                                                          type="text"
                                                                          name="wcbr"
                                                                          // value={formValuesAboout?.wcbr}
                                                                          onChange={(e) =>
                                                                            handleFieldChange(
                                                                              "wcbr",
                                                                              e.target.value
                                                                            )
                                                                          }
                                                                        />
                                                                        If yes, when?
                                                                        <input
                                                                          className="form-control form-control-lg"
                                                                          id="inline-form-input"
                                                                          type="text"
                                                                          name="wcbrwhen"
                                                                          // value={formValuesAboout?.wcbrwhen}
                                                                          onChange={(e) =>
                                                                            handleFieldChange(
                                                                              "wcbrwhen",
                                                                              e.target.value
                                                                            )
                                                                          }
                                                                        />
                                                                        Are you transferring from another Board/Association?
                                                                        <input
                                                                          className="form-control form-control-lg"
                                                                          id="inline-form-input"
                                                                          type="text"
                                                                          name="boardassociation"
                                                                          // value={formValuesAboout?.boardassociation}
                                                                          onChange={(e) =>
                                                                            handleFieldChange(
                                                                              "boardassociation",
                                                                              e.target.value
                                                                            )
                                                                          }
                                                                        />
                                                                        If yes, what Board?
                                                                        <input
                                                                          className="form-control form-control-lg"
                                                                          id="inline-form-input"
                                                                          type="text"
                                                                          name="associationboard"
                                                                          // value={formValuesAboout?.associationboard}
                                                                          onChange={(e) =>
                                                                            handleFieldChange(
                                                                              "associationboard",
                                                                              e.target.value
                                                                            )
                                                                          }
                                                                        />
                                                                        Do you, or have you ever held a real estate license in another state?
                                                                        <input
                                                                          className="form-control form-control-lg"
                                                                          id="inline-form-input"
                                                                          type="text"
                                                                          name="anotherstatelicense"
                                                                          // value={formValuesAboout?.anotherstatelicense}
                                                                          onChange={(e) =>
                                                                            handleFieldChange(
                                                                              "anotherstatelicense",
                                                                              e.target.value
                                                                            )
                                                                          }
                                                                        />
                                                                        If yes, where?
                                                                        <input
                                                                          className="form-control form-control-lg"
                                                                          id="inline-form-input"
                                                                          type="text"
                                                                          name="anotherstatewhere"
                                                                          // value={formValuesAboout?.anotherstatewhere}
                                                                          onChange={(e) =>
                                                                            handleFieldChange(
                                                                              "anotherstatewhere",
                                                                              e.target.value
                                                                            )
                                                                          }
                                                                        />
                                                                        Has your real estate license in this or any other state been suspended or revoked?
                                                                        <input
                                                                          className="form-control form-control-lg"
                                                                          id="inline-form-input"
                                                                          type="text"
                                                                          name="otherstatesuspended"
                                                                          // value={formValuesAboout?.otherstatesuspended}
                                                                          onChange={(e) =>
                                                                            handleFieldChange(
                                                                              "otherstatesuspended",
                                                                              e.target.value
                                                                            )
                                                                          }
                                                                        />
                                                                        If yes, please explain:
                                                                        <input
                                                                          className="form-control form-control-lg"
                                                                          id="inline-form-input"
                                                                          type="text"
                                                                          name="suspendedexplain"
                                                                          // value={formValuesAboout?.suspendedexplain}
                                                                          onChange={(e) =>
                                                                            handleFieldChange(
                                                                              "suspendedexplain",
                                                                              e.target.value
                                                                            )
                                                                          }
                                                                        />
                                                                        Are there now, or have there been within the past five years, any complaints against you before any local, state or
                                                                        national organization, any state real estate regulatory agency, or any other agency of government?
                                                                        <input
                                                                          className="form-control form-control-lg"
                                                                          id="inline-form-input"
                                                                          type="text"
                                                                          name="complain"
                                                                          // value={formValuesAboout?.complain}
                                                                          onChange={(e) =>
                                                                            handleFieldChange(
                                                                              "complain",
                                                                              e.target.value
                                                                            )
                                                                          }
                                                                        />
                                                                        If yes, please explain:
                                                                        <input
                                                                          className="form-control form-control-lg"
                                                                          id="inline-form-input"
                                                                          type="text"
                                                                          name="complainexplain"
                                                                          // value={formValuesAboout?.complainexplain}
                                                                          onChange={(e) =>
                                                                            handleFieldChange(
                                                                              "complainexplain",
                                                                              e.target.value
                                                                            )
                                                                          }
                                                                        />
                                                                        Do you have an unresolved bankruptcy?
                                                                        <input
                                                                          className="form-control form-control-lg"
                                                                          id="inline-form-input"
                                                                          type="text"
                                                                          name="bankruptcy"
                                                                          // value={formValuesAboout?.bankruptcy}
                                                                          onChange={(e) =>
                                                                            handleFieldChange(
                                                                              "bankruptcy",
                                                                              e.target.value
                                                                            )
                                                                          }
                                                                        />
                                                                        If yes, please explain?
                                                                        <input
                                                                          className="form-control form-control-lg"
                                                                          id="inline-form-input"
                                                                          type="text"
                                                                          name="bankruptcyexplain"
                                                                          // value={formValuesAboout?.bankruptcyexplain}
                                                                          onChange={(e) =>
                                                                            handleFieldChange(
                                                                              "bankruptcyexplain",
                                                                              e.target.value
                                                                            )
                                                                          }
                                                                        />
                                                                        Have you ever been convicted of a felony?
                                                                        <input
                                                                          className="form-control form-control-lg"
                                                                          id="inline-form-input"
                                                                          type="text"
                                                                          name="felony"
                                                                          // value={formValuesAboout?.felony}
                                                                          onChange={(e) =>
                                                                            handleFieldChange(
                                                                              "felony",
                                                                              e.target.value
                                                                            )
                                                                          }
                                                                        />
                                                                        If yes, please explain?
                                                                        <input
                                                                          className="form-control form-control-lg"
                                                                          id="inline-form-input"
                                                                          type="text"
                                                                          name="felonyexplain"
                                                                          // value={formValuesAboout?.felonyexplain}
                                                                          onChange={(e) =>
                                                                            handleFieldChange(
                                                                              "felonyexplain",
                                                                              e.target.value
                                                                            )
                                                                          }
                                                                        />
                                                                      </p>

                                                                      <div className="col-sm-12 mb-2">
                                                                        <label className="form-label" for="pr-fn">
                                                                          *. FEE BREAKDOWN
                                                                        </label>
                                                                      </div>

                                                                      <div className="col-sm-12 mb-2">

                                                                        <label className="form-label" for="pr-fn">
                                                                          *. AGENT APPLICATION FEE :
                                                                        </label>
                                                                        <input
                                                                          className="form-control form-control-lg"
                                                                          id="inline-form-input"
                                                                          type="text"
                                                                          name="agentfee"
                                                                          // value={formValuesAboout?.agentfee}
                                                                          onChange={(e) =>
                                                                            handleFieldChange(
                                                                              "agentfee",
                                                                              e.target.value
                                                                            )
                                                                          }
                                                                        />
                                                                      </div>
                                                                      <div className="col-sm-12 mb-2">

                                                                        <label className="form-label" for="pr-fn">
                                                                          *. BOARD DUES:
                                                                        </label>
                                                                        <input
                                                                          className="form-control form-control-lg"
                                                                          id="inline-form-input"
                                                                          type="text"
                                                                          name="boarddues"
                                                                          // value={formValuesAboout?.boarddues}
                                                                          onChange={(e) =>
                                                                            handleFieldChange(
                                                                              "boarddues",
                                                                              e.target.value
                                                                            )
                                                                          }
                                                                        />
                                                                      </div>
                                                                      <div className="col-sm-12 mb-2">

                                                                        <label className="form-label" for="pr-fn">
                                                                          *. PRORATED FOR THE PERIOD STARTING BOARD:
                                                                        </label>
                                                                        <input
                                                                          className="form-control form-control-lg"
                                                                          id="inline-form-input"
                                                                          type="text"
                                                                          name="boardperiod"
                                                                          // value={formValuesAboout?.boardperiod}
                                                                          onChange={(e) =>
                                                                            handleFieldChange(
                                                                              "boardperiod",
                                                                              e.target.value
                                                                            )
                                                                          }
                                                                        />
                                                                      </div>

                                                                      <div className="col-sm-12 mb-2">

                                                                        <label className="form-label" for="pr-fn">
                                                                          *. AND ENDING ON:
                                                                        </label>
                                                                        <input
                                                                          className="form-control form-control-lg"
                                                                          id="inline-form-input"
                                                                          type="text"
                                                                          name="boardending"
                                                                          // value={formValuesAboout?.boardending}
                                                                          onChange={(e) =>
                                                                            handleFieldChange(
                                                                              "boardending",
                                                                              e.target.value
                                                                            )
                                                                          }
                                                                        />
                                                                      </div>



                                                                      <div className="col-sm-12 mb-2">

                                                                        <label className="form-label" for="pr-fn">
                                                                          *. MLS DUES :
                                                                        </label>
                                                                        <input
                                                                          className="form-control form-control-lg"
                                                                          id="inline-form-input"
                                                                          type="text"
                                                                          name="mlsdues"
                                                                          // value={formValuesAboout?.mlsdues}
                                                                          onChange={(e) =>
                                                                            handleFieldChange(
                                                                              "mlsdues",
                                                                              e.target.value
                                                                            )
                                                                          }
                                                                        />
                                                                      </div>
                                                                      <div className="col-sm-12 mb-2">

                                                                        <label className="form-label" for="pr-fn">
                                                                          *. PRORATED FOR THE PERIOD STARTING:
                                                                        </label>
                                                                        <input
                                                                          className="form-control form-control-lg"
                                                                          id="inline-form-input"
                                                                          type="text"
                                                                          name="mlsperiod"
                                                                          // value={formValuesAboout?.mlsperiod}
                                                                          onChange={(e) =>
                                                                            handleFieldChange(
                                                                              "mlsperiod",
                                                                              e.target.value
                                                                            )
                                                                          }
                                                                        />
                                                                      </div>
                                                                      <div className="col-sm-12 mb-2">

                                                                        <label className="form-label" for="pr-fn">
                                                                          *. AND ENDING ON:
                                                                        </label>
                                                                        <input
                                                                          className="form-control form-control-lg"
                                                                          id="inline-form-input"
                                                                          type="text"
                                                                          name="mlsending"
                                                                          // value={formValuesAboout?.mlsending}
                                                                          onChange={(e) =>
                                                                            handleFieldChange(
                                                                              "mlsending",
                                                                              e.target.value
                                                                            )
                                                                          }
                                                                        />
                                                                      </div>

                                                                      <div className="col-sm-12 mb-2">

                                                                        <label className="form-label" for="pr-fn">
                                                                          *. TOTAL DUES & FEES:
                                                                        </label>
                                                                        <input
                                                                          className="form-control form-control-lg"
                                                                          id="inline-form-input"
                                                                          type="text"
                                                                          name="totalfees"
                                                                          // value={formValuesAboout?.totalfees}
                                                                          onChange={(e) =>
                                                                            handleFieldChange(
                                                                              "totalfees",
                                                                              e.target.value
                                                                            )
                                                                          }
                                                                        />
                                                                      </div>



                                                                      {/* <div className="col-sm-12 mb-2">
                                                          <label className="form-label" for="pr-fn">
                                                            *. SIGNATURES
                                                          </label>
                                                          </div> */}




                                                                      {Object.keys(signatures).length > 0 ? (
                                                                        <div className="pull-right mt-3">
                                                                          <button
                                                                            className="btn btn-primary btn-sm"
                                                                            // onClick={handleAddTexterm}
                                                                            onClick={() => handleFillAndGeneratePdfWashingtonCountyBoard(onBoard[currentStep])}
                                                                            disabled={isdone}
                                                                          >
                                                                            {isdone ? "Please wait..." : "Next"}

                                                                            <i className="fi-chevron-right fs-sm ms-2"></i>
                                                                          </button>
                                                                        </div>
                                                                      ) : (
                                                                        <></>
                                                                      )
                                                                      }
                                                                    </div>
                                                                  </>

                                                                  :
                                                                  ""
                                                        :
                                                        <div className="row ">
                                                          <label className="col-form-label float-left w-100" for="pr-fn">Please complete the following form to complete signup.</label>
                                                          <div className="col-sm-12 mb-2">
                                                            <label className="form-label" for="pr-fn">
                                                              In Depth Realty BROKER - AGENT INDEPENDENT CONTRACTOR AGREEMENT
                                                            </label>

                                                            <p className="mb-2"><small>This Agreement, made this day</small></p>
                                                            <p className="mb-2">
                                                              <input
                                                                className="form-control form-control-lg"
                                                                id="inline-form-input"
                                                                type="text"
                                                                name="Text-1"
                                                                value={moment().format("M/D/YYYY")}
                                                                onChange={(e) =>
                                                                  handleFieldChange(
                                                                    "Text-1",
                                                                    e.target.value
                                                                  )
                                                                }
                                                                readOnly
                                                              />
                                                              <p className="my-2"><small>by and between In Depth Realty hereinafter referred to as “Broker”, and</small></p>
                                                              <input
                                                                className="form-control form-control-lg"
                                                                id="inline-form-input"
                                                                type="text"
                                                                name="Text-2"
                                                                // value={formValuesAboout?.Text-2}
                                                                onChange={(e) =>
                                                                  handleFieldChange(
                                                                    "Text-2",
                                                                    e.target.value
                                                                  )
                                                                }
                                                              />
                                                              {
                                                                formErrors['Text-2'] && (
                                                                  <div className="invalid-tooltip">
                                                                    {formErrors['Text-2']}
                                                                  </div>
                                                                )}
                                                              hereinafter referred to as
                                                              “Salesperson” .WITNESSETH: WHEREAS, Broker is duly licensed as a real estate broker by the State of Utah, and
                                                              WHEREAS, Broker maintains the tools, systems and other items necessary and incidental to the proper operation of a real estate
                                                              brokerage business, and WHEREAS, Salesperson is engaged in business as a real estate licensee, duly licensed by the State of Utah.
                                                            </p>

                                                          </div>

                                                          {Object.keys(signatures).length > 0 ? (
                                                            <div className="pull-right mt-3">
                                                              <button
                                                                className="btn btn-primary btn-sm"
                                                                // onClick={handleAddTexterm}
                                                                onClick={() => handleFillAndGeneratePdfCONTRACTORAGREEMENT()}
                                                                disabled={isdone}
                                                              >
                                                                {isdone ? "Please wait..." : "Next"}

                                                                <i className="fi-chevron-right fs-sm ms-2"></i>
                                                              </button>
                                                            </div>
                                                          ) : (
                                                            <></>
                                                          )
                                                          }
                                                        </div>
                                                    }

                                                  </div>
                                                </div>


                                                <div className="col-md-8">
                                                  <div className="">
                                                    <div className="">
                                                      <div className="">
                                                        <div className="onboard_iframe">
                                                          {
                                                            agreementdone ?
                                                              checkurlstring(onBoard[currentStep].image, "Utah Real Estate") ?
                                                                <iframe className="" src={"https://brokeragentbase.s3.amazonaws.com/1702881544800-1700758888303-Utah%20Real%20Estate%20Membership%20Change%20Form%20%282%29.pdf#view=FitH&zoom=100"} />
                                                                :
                                                                checkurlstring(onBoard[currentStep].image, "UCAR") ?
                                                                  <iframe className="" src={"https://brokeragentbase.s3.amazonaws.com/1702980048348-UCAR.pdf#view=FitH&zoom=100"} />
                                                                  :
                                                                  checkurlstring(onBoard[currentStep].image, "Salt Lake Board Membership") ?
                                                                    <iframe className="" src={"https://brokeragentbase.s3.amazonaws.com/assets/Salt+Lake+Board+Membership+Form.pdf#view=FitH&zoom=100"} />
                                                                    :
                                                                    checkurlstring(onBoard[currentStep].image, "Northern Wasatch") ?
                                                                      <iframe className="" src={"https://brokeragentbase.s3.amazonaws.com/1703053628646-Northern%20Wasatch%20Association.pdf#view=FitH&zoom=100"} />
                                                                      :
                                                                      checkurlstring(onBoard[currentStep].image, "Washington County") ?
                                                                        <iframe className="" src={"https://brokeragentbase.s3.amazonaws.com/1703057663392-Washington%20County%20Board%20of%20Realtors.pdf#view=FitH&zoom=100"} />

                                                                        :
                                                                        ""
                                                              :
                                                              <>
                                                                {console.log(accountData.legacy_account)}
                                                                {
                                                                  accountData.legacy_account && accountData.legacy_account === "yes" ?
                                                                    <iframe className="" src={"https://brokeragentbase.s3.amazonaws.com/assets/In-Depth+Realty+Legacy+Independent+Contractor+Agreement.pdf#view=FitH&zoom=100"} />
                                                                    :
                                                                    <iframe className="" src={"https://brokeragentbase.s3.amazonaws.com/1702879744145-In%20Depth%20Realty%20Independent%20Contractor%20Agreement.pdf#view=FitH&zoom=100"} />

                                                                }
                                                              </>


                                                          }

                                                        </div>

                                                        {onBoard[currentStep].image ? (
                                                          <>
                                                            {Object.keys(signatures).length > 0 ? (
                                                              ""
                                                            ) : (
                                                              <div className="canvas_signature w-100">
                                                                <SignatureCanvas
                                                                  ref={signatureCanvasRef}
                                                                  canvasProps={{
                                                                    className: "signature-canvas w-100",
                                                                  }}
                                                                />
                                                                <div className="float-left w-100 mt-3">
                                                                  <button
                                                                    className="btn btn-primary btn-sm"
                                                                    onClick={handleClearSignature}
                                                                  >
                                                                    Clear Signature
                                                                  </button>
                                                                  <button
                                                                    className="btn btn-primary btn-sm ms-3"
                                                                    onClick={handleSaveSignature}
                                                                  >
                                                                    Save Signature
                                                                  </button>
                                                                </div>
                                                              </div>
                                                            )}
                                                          </>
                                                        ) : (
                                                          <div className="row">
                                                            <div className="col-md-8">
                                                              <div className="float-left w-100 d-flex align-items-center justify-content-between mb-4">
                                                                <h3 className="mb-0">Create Signature</h3>
                                                              </div>
                                                              <div className="bg-light border rounded-3 p-3">
                                                                <div className="">
                                                                  <p>
                                                                    Use a touch screen device or your
                                                                    mouse to draw in the box below.
                                                                  </p>
                                                                  <div id="signature-pad">
                                                                    <canvas className="signature-canvas"></canvas>
                                                                  </div>
                                                                </div>
                                                              </div>
                                                              <div className="pull-right mt-3">
                                                                <button
                                                                  className="btn btn-primary ms-2 px-3 px-sm-4"
                                                                  type="button"
                                                                  onClick={handleAddSignatureToPdf}
                                                                >
                                                                  Save Signature
                                                                </button>
                                                                <button
                                                                  className="btn btn-secondary ms-3 px-3 px-sm-4"
                                                                  onClick={handleUndo}
                                                                >
                                                                  <i
                                                                    className="fa fa-undo"
                                                                    aria-hidden="true"
                                                                  ></i>{" "}
                                                                  Undo
                                                                </button>

                                                                <button
                                                                  className="btn btn-secondary ms-3 px-3 px-sm-4"
                                                                  onClick={handleClear}
                                                                >
                                                                  <i
                                                                    className="fa fa-eraser"
                                                                    aria-hidden="true"
                                                                  ></i>{" "}
                                                                  Clear
                                                                </button>
                                                              </div>
                                                            </div>
                                                            <div className="col-md-4"></div>
                                                          </div>
                                                        )}
                                                      </div>
                                                    </div>
                                                  </div>
                                                  {Object.keys(signatures).length > 0 ? (
                                                    <div className="pull-left">


                                                      {pdfSrcNew ? (
                                                        <>
                                                          <button
                                                            className="btn btn-primary ms-2 px-3 px-sm-4"
                                                            type="button"
                                                            onClick={handleNewSignPdf}
                                                          >
                                                            Next
                                                          </button>
                                                        </>
                                                      ) : (
                                                        //   <NavLink
                                                        //   className=""
                                                        //   onClick={handleAddSignatureToPdf}
                                                        //   disabled={show}
                                                        // >
                                                        //   {show
                                                        //     ? "Please wait..."
                                                        //     : "I Agree to the Terms and Conditions"}

                                                        //   <i className="fi-chevron-right fs-sm ms-2"></i>
                                                        // </NavLink>
                                                        <></>
                                                      )}
                                                    </div>
                                                  ) : (
                                                    ""
                                                  )}
                                                </div>

                                                {/* <button onClick={handleNextStep}>Next Step</button>
                                          <hr /> */}
                                              </>
                                            )}
                                          </>
                                          :
                                          <>
                                            <div className="col-md-4">
                                              <label className="col-form-label float-left w-100" for="pr-fn">Please complete the following form to complete signup.</label>
                                              <div className="row">
                                                <div className="col-sm-12 mb-2">
                                                  <label className="form-label" for="pr-fn">
                                                    In Depth Realty BROKER - AGENT INDEPENDENT CONTRACTOR AGREEMENT
                                                  </label>
                                                  <p className="mb-2"><small>This Agreement, made this day</small></p>
                                                  <p className="mb-2">
                                                    <input
                                                      className="form-control form-control-lg"
                                                      id="inline-form-input"
                                                      type="text"
                                                      name="Text-1"
                                                      // value={fieldValues?.Text-1}
                                                      value={moment().format("M/D/YYYY")}
                                                      onChange={(e) =>
                                                        handleFieldChange(
                                                          "Text-1",
                                                          e.target.value
                                                        )
                                                      }
                                                      readOnly
                                                    />
                                                    <p className="my-2"><small>by and between In Depth Realty hereinafter referred to as “Broker”, and</small></p>
                                                    <input
                                                      className="form-control form-control-lg"
                                                      id="inline-form-input"
                                                      type="text"
                                                      name="Text-2"
                                                      // value={formValuesAboout?.Text-2}
                                                      onChange={(e) =>
                                                        handleFieldChange(
                                                          "Text-2",
                                                          e.target.value
                                                        )
                                                      }
                                                    />
                                                    {
                                                      formErrors['Text-2'] && (
                                                        <div className="invalid-tooltip">
                                                          {formErrors['Text-2']}
                                                        </div>
                                                      )}
                                                    hereinafter referred to as
                                                    “Salesperson” .WITNESSETH: WHEREAS, Broker is duly licensed as a real estate broker by the State of Utah, and
                                                    WHEREAS, Broker maintains the tools, systems and other items necessary and incidental to the proper operation of a real estate
                                                    brokerage business, and WHEREAS, Salesperson is engaged in business as a real estate licensee, duly licensed by the State of Utah.
                                                  </p>

                                                </div>

                                                {Object.keys(signatures).length > 0 ? (
                                                  <div className="pull-right mt-3">
                                                    <button
                                                      className="btn btn-primary btn-sm"
                                                      // onClick={handleAddTexterm}
                                                      onClick={() => handleFillAndGeneratePdfCONTRACTORAGREEMENT()}
                                                      disabled={isdone}
                                                    >
                                                      {isdone ? "Please wait..." : "Next"}

                                                      <i className="fi-chevron-right fs-sm ms-2"></i>
                                                    </button>
                                                  </div>
                                                ) : (
                                                  <></>
                                                )
                                                }
                                              </div>
                                            </div>
                                            <div className="col-md-8">
                                              <div className="">
                                                <div className="">
                                                  <div className="">
                                                    <div className="onboard_iframe">
                                                      {
                                                        accountData.legacy_account && accountData.legacy_account === "yes" ?
                                                          <iframe className="" src={"https://brokeragentbase.s3.amazonaws.com/assets/In-Depth+Realty+Legacy+Independent+Contractor+Agreement.pdf#view=FitH&zoom=100"} />
                                                          :
                                                          <iframe className="" src={"https://brokeragentbase.s3.amazonaws.com/1702879744145-In%20Depth%20Realty%20Independent%20Contractor%20Agreement.pdf#view=FitH&zoom=100"} />
                                                      }

                                                    </div>
                                                  </div>
                                                </div>
                                              </div>
                                            </div>
                                          </>
                                      }

                                      {agreementdone && currentStep === onBoard.length && (
                                        <>
                                          <div className="thank_you row align-items-center mt-md-2  text-center">
                                            {/* <div className="col-lg-7 order-lg-2 mb-lg-0 mb-4 pb-2 pb-lg-0 mt-lg-0 mt-md-0 mt-sm-5 mt-5">
                                          <img className="d-block mx-auto" src={AgentAdded} alt="Hero image" />
                                          </div> */}
                                            <div className="col-lg-12 order-lg-1 pe-lg-0 text-center">
                                              <h1 className="display-5 mb-4 me-lg-n5 text-center mb-4"> Thank you
                                              </h1>
                                              <ul className="status_bar">
                                                <li className="d-flex align-items-center justify-content-start"><i className="fi-check text-success bg-white icon-box-media me-2 rounded-4"></i>Password</li>
                                                <li className="d-flex align-items-center justify-content-start"><i className="fi-check text-success bg-white icon-box-media me-2 rounded-4"></i>Primary Information</li>
                                                <li className="d-flex align-items-center justify-content-start"><i className="fi-check text-success bg-white icon-box-media me-2 rounded-4"></i>W9</li>
                                                <li className="d-flex align-items-center justify-content-start"><i className="fi-check text-success bg-white icon-box-media me-2 rounded-4"></i>Agreement</li>
                                              </ul>

                                              {/*<p className="text-lg-start text-center mb-4 mb-lg-5 fs-lg">Fantastic news! Onboarding is complete! 🎉 Now, let's wrap things up with the final setup.
                                                  </p>
                                          <p className="text-lg-start text-center mb-4 mb-lg-5 fs-lg"> If you have any remaining steps or need assistance, please don't hesitate to ask. We're here to ensure everything is in place for a seamless experience. Welcome to the team, and let's get started on this exciting journey together! 👋🚀</p> */}
                                              {/*<div className="pull-right mt-2">
                                          <NavLink  type="button"
                                            className="btn btn-primary btn-sm" to="/">Go To Homepage</NavLink> 
                                            </div>*/}
                                            </div>
                                          </div>
                                          {/* <button onClick={handleReset}>Restart</button> */}
                                          {/* <button onClick={handleFinishstep}>Finish</button> */}

                                          <div className="text-center my-4">
                                            <button
                                              className="btn btn-primary"
                                              type="button"
                                              onClick={handleFinishstep}
                                            >
                                              Submit and Complete Onboarding

                                            </button>
                                          </div>
                                        </>
                                      )}


                                    </div>
                                }
                              </>

                          }

                        </div>
                      </>
                    ) : (
                      ""
                    )
                    }
                  </>
              }


            </div>
          </div>
        </main>
      </div>
    </div>
  );
};

export default Onboarding;

export const PDF_SRC =
  "https://brokeragentbase.s3.amazonaws.com/assets/IRS+W9.pdf#view=FitH&zoom=100";

export const PDF_SRCNew =
  "https://brokeragentbase.s3.amazonaws.com/1700834789094-modified.pdf#view=FitH&zoom=100";

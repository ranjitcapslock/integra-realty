import React, { useState, useEffect } from "react";
import _ from "lodash";
import user_service from "../service/user_service";
import jwt from "jwt-decode";
import Loader from "../../Pages/Loader/Loader.js";
import Toaster from "../../Pages/Toaster/Toaster.js";
import { NavLink, useNavigate, useParams } from "react-router-dom";
import defaultpropertyimage from "../../Components/img/defaultpropertyimage.jpeg";
import { Calendar as BigCalendar, momentLocalizer } from "react-big-calendar";
import moment from "moment-timezone";
import "react-big-calendar/lib/css/react-big-calendar.css";
import Pic from "../img/pic.png";

import Swal from "sweetalert2";
import "sweetalert2/dist/sweetalert2.css";

const localizer = momentLocalizer(moment);

const eventColors = {
  meeting: "#FF5733",
  duty: "#3366FF",
};

function ReservationSinglePage() {
  const CustomToolbar = () => {
    return (
      <div className="rbc-toolbar mt-3">
        <span className="rbc-toolbar-label"></span>
        <span className="rbc-btn-group mt-3"></span>
      </div>
    );
  };

  const [loader, setLoader] = useState({ isActive: null });
  const { isActive } = loader;
  const [toaster, setToaster] = useState({
    types: null,
    isShow: null,
    toasterBody: "",
    message: "",
  });
  const { types, isShow, toasterBody, message } = toaster;
  const navigate = useNavigate();
  const params = useParams();
  const [reservationData, setReservationData] = useState("");
  const [events, setEvents] = useState([]);
  const [isModalVisible, setIsModalVisible] = useState(false); // State to control modal visibility
  const [currentDate, setCurrentDate] = useState("");

  const reservationGetIdData = async () => {
    await user_service.reservationGetId(params.id).then((response) => {
      if (response) {
        setLoader({ isActive: false });
        console.log(response);
        setReservationData(response.data);
      }
    });
  };

  useEffect(() => {
    const interval = setInterval(() => {
      setCurrentDate(
        // moment().tz("America/Denver").format("dddd, D MMMM YYYY, h:mm:ss a")
        moment().tz("America/Denver").format("dddd, D MMMM YYYY")
      );
    }, 1000);

    return () => {
      clearInterval(interval);
    };
  }, []);

  const [shifts, setShifts] = useState([]);
  const [selectedTime, setSelectedTime] = useState("");
  const [selectedTimeEnd, setSelectedTimeEnd] = useState("");

  const [selectedDate, setSelectedDate] = useState(
    moment().format("YYYY-MM-DD")
  );
  const [selectedDayShifts, setSelectedDayShifts] = useState(null);

  const [calenderReservation, setCalenderReservation] = useState([]);

  useEffect(() => {
    // Assuming this logic runs when the component mounts or when the user changes
    const auth = localStorage.getItem("auth");
    if (auth) {
      const user = jwt(auth);
      // Additional logic can be placed here if needed based on user role
    }
  }, []);

  // const eventStyleGetter = (event, start, end, isSelected) => {
  //   const isDateConfirmed = calenderReservation.some(
  //     (item) => item.reservation_date
  //   );

  //   const confirmed = isDateConfirmed === moment(start).isSame(moment(), "day") ||moment(selectedDate).format("YYYY-MM-DD");
  //   console.log(confirmed);

  //   if(confirmed){
  //     const isToday = moment(start).isSame(moment(), "day");
  //     const backgroundColor = confirmed ? "green" : (isToday ? "#151e43" : eventColors[event.type]);
  //     const borderColor = isToday ? "black" : "transparent";

  //     return {
  //       className: isToday ? "rbc-day-bg" : "",
  //       style: {
  //         backgroundColor,
  //         borderColor,
  //       },
  //     };
  //   }
  // };

  const dayPropGetter = (date) => {
    const isDateConfirmed = calenderReservation.some(
      (item) =>
        moment(item.reservation_date).format("YYYY-MM-DD") ===
        moment(date).format("YYYY-MM-DD")
    );

    const isToday = moment(date).isSame(moment(), "day");
    const backgroundColor = isDateConfirmed
      ? "#72C772"
      : isToday
      ? "#eaf6ff"
      : "";

    return {
      style: {
        backgroundColor,
      },
    };
  };

  const handleSelectSlot = (slotInfo) => {
    const today = moment().startOf("day");
    const selectedDate = moment(slotInfo.start).startOf("day");

    const auth = localStorage.getItem("auth");
    if (auth) {
      const user = jwt(auth);
      if (
        (user.contactType === "admin" ||
          jwt(localStorage.getItem("auth")).id ||
          user.contactType === "staff") &&
        selectedDate.isSameOrAfter(today)
      ) {
        setSelectedDate(slotInfo.start);
        filterShiftsByDay(slotInfo.start);
      }
    }
  };

  useEffect(() => {
    filterShiftsByDay(moment());
  }, [shifts]);

  const handleNavigate = (date, view, action) => {
    switch (action) {
      case "TODAY":
        const today = moment();
        setSelectedDate(today);
        filterShiftsByDay(today);
        break;
      case "PREV":
        const previousMonth = moment(selectedDate).subtract(1, "month");
        setSelectedDate(previousMonth);
        filterShiftsByDay(previousMonth);
        break;
      case "NEXT":
        const nextMonth = moment(selectedDate).add(1, "month");
        setSelectedDate(nextMonth);
        filterShiftsByDay(nextMonth);
        break;
      default:
        break;
    }
  };

  const filterShiftsByDay = (date) => {
    const selectedDay = moment(date).format("dddd").toLowerCase();
    const dayShifts = shifts.filter((shift) =>
      shift.days.includes(selectedDay)
    );
    setSelectedDayShifts(dayShifts.length > 0 ? dayShifts[0] : null);
  };

  const renderTimeSlots = () => {
    if (!selectedDate || !selectedDayShifts) {
      return <p>Reservations not available on this day.</p>;
    }

    const { startTime, endTime } = selectedDayShifts;
    const start = moment(startTime, "h:mmA");
    const end = moment(endTime, "h:mmA");

    if (!start.isValid() || !end.isValid()) {
      console.error("Invalid start or end time");
      return <p>Invalid time format</p>;
    }

    const timeSlots = [];

    if (reservationData.registration === "shift_Based") {
      const ReservationskeyStart = start.format("h:mmA");
      const ReservationskeyEnd = end.format("h:mmA");

      // Check if there are reservations matching the shift duration
      const matchingReservations = calenderReservation.filter(
        (item) =>
          item.reservation_startTime === ReservationskeyStart &&
          item.reservation_endTime === ReservationskeyEnd &&
          moment(selectedDate).format("YYYY-MM-DD") === item.reservation_date
      );

      let reservationContent = null;

      if (matchingReservations.length > 0) {
        // Display each matching reservation
        reservationContent = matchingReservations.map((item, index) => {
          let contactData;
          try {
            contactData = JSON.parse(item.contactData);
          } catch (e) {
            console.error("Invalid JSON in contactData", e);
            return null;
          }

          return (
            <div
              className="float-left w-100 d-flex align-items-center justify-content-start"
              key={contactData._id}
            >
              <img
                className="rounded-circlee profile_picture"
                height="30"
                width="30"
                src={
                  contactData.image && contactData.image !== "image"
                    ? contactData.image
                    : Pic
                }
                alt="Profile"
              />
              <div className="">
                <strong>{contactData.firstName}</strong>
                <br />
                <strong>{contactData.active_office}</strong>
              </div>
              <a className="ms-5" onClick={() => handleDelete(item._id)}>
                Remove
              </a>
            </div>
          );
        });
      } else {
        // Display a "Reserve" button if no reservation exists for this shift
        reservationContent = (
          <a
            className="pull-right ms-5"
            onClick={() =>
              handleDateTime(
                selectedDate,
                ReservationskeyStart,
                ReservationskeyEnd
              )
            }
          >
            Reserve
          </a>
        );
      }

      // Push the time slot row into the timeSlots array
      timeSlots.push(
        <div
          key={`${ReservationskeyStart}-${ReservationskeyEnd}`}
          className="row my-2"
        >
          <div className="col-md-4">
            <p className="pull-left mb-0">
              {ReservationskeyStart} - {ReservationskeyEnd}
            </p>
          </div>
          <div className="col-md-8">{reservationContent}</div>
          <hr className="mt-2" />
        </div>
      );
    } else if (reservationData.registration === "open_Registration") {
      // The existing code for open_Registration as previously provided
      for (
        let current = start.clone();
        current.isBefore(end);
        current.add(30, "minutes")
      ) {
        const ReservationskeyStart = current.format("h:mmA");
        const ReservationskeyEnd = current
          .clone()
          .add(1, "hour")
          .format("h:mmA");

        let reservationContent = null;

        const matchingReservations = calenderReservation.filter(
          (item) =>
            item.reservation_startTime === ReservationskeyStart &&
            moment(selectedDate).format("YYYY-MM-DD") === item.reservation_date
        );

        if (matchingReservations.length > 0) {
          reservationContent = matchingReservations.map((item, index) => {
            let contactData;
            try {
              contactData = JSON.parse(item.contactData);
            } catch (e) {
              console.error("Invalid JSON in contactData", e);
              return null;
            }

            return (
              <div
                className="float-left w-100 d-flex align-items-center justify-content-start"
                key={contactData._id}
              >
                <img
                  className="rounded-circlee profile_picture"
                  height="30"
                  width="30"
                  src={
                    contactData.image && contactData.image !== "image"
                      ? contactData.image
                      : Pic
                  }
                  alt="Profile"
                />
                <div className="">
                  <strong>{contactData.firstName}</strong>
                  <br />
                  <strong>{contactData.active_office}</strong>
                </div>
                <a className="ms-5" onClick={() => handleDelete(item._id)}>
                  Remove
                </a>
              </div>
            );
          });
        } else {
          reservationContent = (
            <a
              className="pull-right ms-5"
              onClick={() =>
                handleDateTime(
                  selectedDate,
                  ReservationskeyStart,
                  ReservationskeyEnd
                )
              }
            >
              Reserve
            </a>
          );
        }

        timeSlots.push(
          <div
            className="row my-2"
            key={`${ReservationskeyStart}-${ReservationskeyEnd}`}
          >
            <div className="col-md-4">
              <p className="pull-left mb-0">{ReservationskeyStart}</p>
            </div>
            <div className="col-md-8">{reservationContent}</div>
            <hr className="mt-2" />
          </div>
        );
      }
    }

    return timeSlots;
  };

  // const renderTimeSlots = () => {

  //   if (!selectedDate || !selectedDayShifts) {
  //     return <p>Reservations not available on this day.</p>;
  //   }

  //   const { startTime, endTime } = selectedDayShifts;
  //   const start = moment(startTime, "h:mmA");
  //   const end = moment(endTime, "h:mmA");

  //   if (!start.isValid() || !end.isValid()) {
  //     console.error("Invalid start or end time");
  //     return <p>Invalid time format</p>;
  //   }

  //   const timeSlots = [];

  //   // Generating time slots for each hour from startTime to endTime
  //   for (
  //     let current = start.clone();
  //     current.isBefore(end);
  //     current.add(1, "hour")
  //   ) {
  //     const nextTimeSlot = current.clone().add(1, "hour");
  //     const ReservationskeyStart = `${current.format("h:mmA")}`;
  //     const ReservationskeyEnd = `${nextTimeSlot.format("h:mmA")}`;

  //     let isReserved = false;
  //     let reservationContent = null;

  //     // Check if there are reservations matching the current time slot
  //     const matchingReservations = calenderReservation.filter(
  //       (item) =>
  //         item.reservation_startTime === ReservationskeyStart &&
  //         moment(selectedDate).format("YYYY-MM-DD") === item.reservation_date
  //     );

  //     if (matchingReservations.length > 0) {
  //       // Display each matching reservation
  //       reservationContent = matchingReservations.map((item, index) => {
  //         let contactData;
  //         try {
  //           contactData = JSON.parse(item.contactData);
  //         } catch (e) {
  //           console.error("Invalid JSON in contactData", e);
  //           return null;
  //         }

  //         isReserved = true;
  //         return (
  //           <div
  //             className="float-left w-100 d-flex align-items-center justify-content-start"
  //             key={contactData._id}
  //           >
  //             <img
  //               className="rounded-circlee profile_picture"
  //               height="30"
  //               width="30"
  //               src={
  //                 contactData.image && contactData.image !== "image"
  //                   ? contactData.image
  //                   : Pic
  //               }
  //               alt="Profile"
  //             />
  //             <div className="">
  //               <strong>{contactData.firstName}</strong>
  //               <br />
  //               <strong>{contactData.active_office}</strong>
  //             </div>
  //             <a className="ms-5" onClick={() => handleDelete(item._id)}>
  //               Remove
  //             </a>
  //           </div>
  //         );
  //       });
  //     } else {
  //       // Display a "Reserve" button if no reservation exists for this time slot
  //       reservationContent = (
  //         <a
  //           className="pull-right ms-5"
  //           onClick={() =>
  //             handleDateTime(
  //               selectedDate,
  //               ReservationskeyStart,
  //               ReservationskeyEnd
  //             )
  //           }
  //         >
  //           Reserve
  //         </a>
  //       );
  //     }

  //     // Push the time slot row into the timeSlots array
  //     timeSlots.push(
  //       <>
  //         <div
  //           className="row my-2"
  //           key={`${ReservationskeyStart}-${ReservationskeyEnd}`}
  //         >
  //           <div className="col-md-4">
  //             <p className="pull-left mb-0">
  //               {ReservationskeyStart} - {ReservationskeyEnd}
  //             </p>
  //           </div>
  //           <div className="col-md-8">{reservationContent}</div>
  //         </div>
  //         <hr className="mt-2" />
  //       </>
  //     );
  //   }

  //   return timeSlots;
  // };

  const handleDelete = async (id) => {
    Swal.fire({
      title: "Are you sure?",
      text: "It will be permanently deleted all private /associate contacts !",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Yes, delete it!",
    }).then(async (result) => {
      if (result.isConfirmed) {
        try {
          const response = await user_service.calenderReservationDelete(id);
          if (response) {
            setLoader({ isActive: false });
            if (response) {
              Swal.fire(
                "Deleted!",
                "Contact is deleted permanently.",
                "success"
              );
              //setFormValues(response.data);
            }
            configureScheduleGetAllData();
            window.location.reload();
          }
        } catch (error) {
          Swal.fire(
            "Error",
            "An error occurred while deleting the file.",
            "error"
          );
          console.error(error);
        }
      }
    });

    // setLoader({ isActive: true });
    // await user_service.calenderReservationDelete(id).then((response) => {
    //   if (response) {
    //     setLoader({ isActive: false });
    //     setToaster({
    //       types: "Delete",
    //       isShow: true,
    //       toasterBody: response.data.message,
    //     });
    //     configureScheduleGetAllData();
    //     window.location.reload();
    //     setTimeout(() => {
    //       setToaster((prevToaster) => ({
    //         ...prevToaster,
    //         isShow: false,
    //       }));
    //     }, 2000);
    //   }
    // });
  };

  const handleDateTime = (date, start, end) => {
    setSelectedDate(date);
    setSelectedTime(start);
    setSelectedTimeEnd(end);
    setIsModalVisible(true);
    setContactsAssociate("");
  };

  useEffect(() => {
    setLoader({ isActive: true });
    reservationGetIdData();
    configureScheduleGetAllData();
  }, []);

  const configureScheduleGetAllData = async () => {
    await user_service.configureScheduleGetAll(params.id).then((response) => {
      setLoader({ isActive: false });
      if (response) {
        setShifts(response.data.data);
      }
    });
  };

  useEffect(() => {
    window.scrollTo(0, 0);
    setLoader({ isActive: true });
    calenderReservationgetAllData();
  }, []);

  const calenderReservationgetAllData = async () => {
    await user_service.calenderReservationgetAll(params.id).then((response) => {
      setLoader({ isActive: false });
      if (response) {
        setCalenderReservation(response.data.data);
      }
    });
  };

  const formattedDate = moment(reservationData.created).format(
    "dddd, MMM D, YYYY"
  );

  const closeModal = () => {
    setIsModalVisible(false);
  };

  const handleProfile = (id) => {
    navigate(`/contact-profile/${id}`);
  };

  const [reservationChange, setReservationChange] = useState(false);
  const [contactType, setContactType] = useState("associate");
  const [contactStatus, setContactStatus] = useState("active");

  const [contactName, setContactName] = useState("");
  const [getContact, setGetContact] = useState([]);

  const handleChange = () => {
    setReservationChange(true);
  };

  const handleAssociateCancel = () => {
    setReservationChange(false);
  };

  const SearchGetAll = async () => {
    try {
      const response = await user_service.SearchContactfilterAssociate(
        1,
        contactType,
        contactStatus,
        contactName
      );
      if (response) {
        setGetContact(response.data.data);
        // setPageCount(Math.ceil(response.data.totalRecords / 10));
        // setTotalRecords(response.data.totalRecords);
      }
    } catch (error) {
      console.error("Error fetching data:", error);
    }
  };

  useEffect(() => {
    window.scrollTo(0, 0);
    if (contactName) {
      SearchGetAll(1);
    }
  }, [contactType, contactName]);

  const [profile, setProfile] = useState("");

  useEffect(() => {
    window.scrollTo(0, 0);
    const profileGetAll = async () => {
      setLoader({ isActive: true });
      await user_service
        .profileGet(jwt(localStorage.getItem("auth")).id)
        .then((response) => {
          if (response) {
            setLoader({ isActive: false });
            console.log(response);
            setProfile(response.data);
          }
        });
    };
    profileGetAll();
  }, []);

  const [contactsAssociate, setContactsAssociate] = useState("");
  const handleContactAssociate = (contactAssociate) => {
    setContactsAssociate(contactAssociate);
    setGetContact([]);
    setContactName({});
  };

  const handleChangeAssociate = () => {
    setContactsAssociate("");
    setReservationChange(false);
  };

  const [changeDate, setChangeDate] = useState(false);
  const [changeTime, setChangeTime] = useState(false);

  const handleChangeDate = () => {
    setChangeDate(true);
  };
  const handleDateCancel = () => {
    setChangeDate(false);
  };

  const handleTimeCancel = () => {
    setChangeTime(false);
  };

  const handleChangeTime = () => {
    setChangeTime(true);
  };

  const initialValues = {
    reservation_date: "",
    reservation_endTime: "",
    reservation_startTime: "",
    policy: "",
    contactData: "",
    status: "",
  };

  const [formValues, setFormValues] = useState(initialValues);
  const [checkBox, setSetCheckbox] = useState(false);

  const handleCheck = () => {
    setSetCheckbox(!checkBox);
  };

  console.log(checkBox);

  const handleChangeInput = (e) => {
    const { name, value } = e.target;
    setFormValues({ ...formValues, [name]: value });
  };

  useEffect(() => {
    if (reservationData?.policy) {
      setFormValues({ policy: reservationData.policy });
    }
  }, [reservationData]);

  const handleSubmit = async (e) => {
    e.preventDefault();
  
    if (!checkBox) {
      alert("To proceed, please check to accept the Policies and Procedures.");
      return;
    }
  
    setLoader({ isActive: true });
  
    const commonData = {
      agentId: jwt(localStorage.getItem("auth")).id,
      reservation_date: moment(selectedDate).format("YYYY-MM-DD"),
      reservation_startTime: selectedTime,
      reservation_endTime: selectedTimeEnd,
      reservationId: params.id,
      calender_name: reservationData.calender_name,
      office: reservationData.publication_Level,
      status: "confirmed",
      contactData: contactsAssociate
        ? JSON.stringify(contactsAssociate)
        : JSON.stringify(profile),
    };
  
    try {
      if (formValues.policy) {
        const userDataUpdate = { policy: formValues.policy };
        await user_service.reservationUpdate(params.id, userDataUpdate);
      }
  
      const reservationResponse = await user_service.calenderReservationPost(commonData);
  
      if (reservationResponse) {
        const calendarEventData = {
          office_id: localStorage.getItem("active_office_id"),
          office_name: localStorage.getItem("active_office"),
          allDayEvent: "yes",
          title: reservationData.calender_name,
          startDate: moment(selectedDate).format("YYYY-MM-DD"),
          endDate: moment(selectedDate).format("YYYY-MM-DD"),
          startTime: selectedTime,
          endTime: selectedTimeEnd,
          reservationId: params.id,
          information: "information",
          registration: "registration",
          draft: "draft",
          guestSeats: "guestSeats",
          publicEvent: "publicEvent",
        };
  
        const calendarResponse = await user_service.CalenderPost(calendarEventData);
        console.log(calendarResponse);
  
        setToaster({
          types: "Add Calendar Reservation",
          isShow: true,
          toasterBody: reservationResponse.data.message,
          message: "Calendar Reservation Added Successfully",
        });
  
        calenderReservationgetAllData();
        setIsModalVisible(false);
        setTimeout(() => {
          setToaster((prevToaster) => ({ ...prevToaster, isShow: false }));
        }, 2000);
  
        navigate(`/reservation-calender/${params.id}`);
      } else {
        setLoader({ isActive: false });
        // setToaster({
        //   types: "error",
        //   isShow: true,
        //   message: "Error",
        // });
      }
    } catch (error) {
      setLoader({ isActive: false });
      // setToaster({
      //   types: "error",
      //   isShow: true,
      //   message: "Error",
      // });
      console.log(error);
    }
  };
  

  const handleCancel = () => {
    setIsModalVisible(false);
  };

  const handleBack = () => {
    navigate(`/reservation-listing`);
  };

  


  return (
    <div className="bg-secondary float-left w-100 pt-4 mb-4">
      <Loader isActive={isActive} />
      {isShow && (
        <Toaster
          types={types}
          isShow={isShow}
          toasterBody={toasterBody}
          message={message}
        />
      )}
      <main className="page-wrapper">
        {isModalVisible ? (
          <div className="bg-light border rounded-3 p-3">
            <button
              className="btn btn-secondary pull-right"
              type="button"
              onClick={handleCancel}>
              Cancel
            </button>
            <h4 className="">Calendar Reservation</h4>
            <div className="row mt-5 p-3">
              <div className="col-md-6 border rounded-3 p-3">
                <div className="mb-4">
                  <label className="form-label mt-3">Who</label>
                  {Object.keys(contactsAssociate).length > 0 ? (
                    <div className="mb-1">
                      <div
                        className="float-left w-100 d-flex align-items-center justify-content-start"
                        key={contactsAssociate._id}
                      >
                        <img
                          className="rounded-circlee profile_picture"
                          height="30"
                          width="30"
                          src={
                            contactsAssociate.image &&
                            contactsAssociate.image !== "image"
                              ? contactsAssociate.image
                              : Pic
                          }
                          alt="Profile"
                        />
                        <div className="ms-3">
                          <strong>
                            {contactsAssociate.firstName}{" "}
                            {contactsAssociate.lastName}
                          </strong>
                          <br />
                          <strong>{contactsAssociate.active_office}</strong>
                        </div>
                        <NavLink
                          className="pull-right ms-5"
                          onClick={handleChangeAssociate}
                        >
                          Change
                        </NavLink>
                      </div>
                    </div>
                  ) : (
                    <>
                      {reservationChange ? (
                        <div className="col-sm-12 mt-3">
                          <label className="form-label">
                            Find an associate by name:
                          </label>
                          <input
                            className="form-control"
                            id="inline-form-input"
                            type="text"
                            name="contactName"
                            onChange={(event) =>
                              setContactName(event.target.value)
                            }
                          />
                          {getContact.length > 0 ? (
                            getContact.map((contact, index) => (
                              <div
                                className="member_Associate mb-1 mt-1"
                                key={index}
                                onClick={() => handleContactAssociate(contact)}
                              >
                                <div
                                  className="float-left w-100 d-flex align-items-center justify-content-start"
                                  key={contact._id}
                                >
                                  <img
                                    className="rounded-circlee profile_picture"
                                    height="30"
                                    width="30"
                                    src={
                                      contact.image && contact.image !== "image"
                                        ? contact.image
                                        : Pic
                                    }
                                    alt="Profile"
                                  />
                                  <div className="ms-3">
                                    <strong>
                                      {contact.firstName} {contact.lastName}
                                    </strong>
                                    <br />
                                    <strong>{contact.active_office}</strong>
                                  </div>
                                </div>
                              </div>
                            ))
                          ) : (
                            <p></p>
                          )}
                          <a
                            className="pull-right"
                            onClick={handleAssociateCancel}
                          >
                            Cancel
                          </a>
                        </div>
                      ) : (
                        <>
                          <div
                            className="float-left w-100 d-flex align-items-center justify-content-start"
                            onClick={() => handleProfile(profile._id)}
                          >
                            <img
                              className="rounded-circlee profile_picture"
                              height="30"
                              width="30"
                              src={
                                profile.image && profile.image !== "image"
                                  ? profile.image
                                  : ""
                              }
                              alt="Profile"
                            />
                            <div className="ms-3">
                              <a className="">
                                {profile.firstName} {profile.lastName}
                              </a>
                              <br />
                              <strong>{profile.active_office}</strong>
                            </div>
                          </div>
                          <div>
                            <NavLink
                              className="pull-right ms-5"
                              onClick={handleChange}
                            >
                              Change
                            </NavLink>
                          </div>
                        </>
                      )}
                    </>
                  )}
                  <label className="form-label mt-5">What</label>
                  <h6 className="mb-0">{reservationData?.calender_name}</h6>

                  <label className="form-label mt-3">Where</label>
                  <h6 className="mb-0">
                    {reservationData.publication_Level === "corporate"
                      ? "Corporate"
                      : ""}
                  </h6>

                  <label className="form-label mt-3">Date</label>
                  {/* {changeDate ? (
                      <div className="col-sm-12 d-flex">
                        <input
                          className="form-control"
                          id="inline-form-input"
                          type="date"
                          name="reservation_date"
                          value={formValues.reservation_date}
                          onChange={handleChangeInput}
                        />

                        <div>
                          <NavLink
                            className="pull-right ms-5"
                            onClick={handleDateCancel}
                          >
                            Change
                          </NavLink>
                        </div>
                      </div>
                    ) : (
                      <div className="d-flex">
                        <h6>
                          {moment(selectedDate).format("dddd, MMM D, YYYY") ??
                            formattedDate}
                        </h6>
                        <NavLink
                          className="pull-right ms-5"
                          onClick={handleChangeDate}
                        >
                          Change
                        </NavLink>
                      </div>
                    )} */}
                  <div className="">
                    <h6>
                      {moment(selectedDate).format("dddd, MMM D, YYYY") ??
                        formattedDate}
                    </h6>
                  </div>

                  <label className="form-label mt-3">Time</label>
                  {/* {changeTime ? (
                      <div className="col-sm-12 d-flex">
                        <div className="">
                          <select
                            className="form-select "
                            id="pr-city"
                            name="reservation_startTime"
                            value={formValues.reservation_startTime}
                            onChange={handleChangeInput}
                          >
                            <option>Select Time</option>
                            <option>12:00 AM</option>
                            <option>12:30 AM</option>
                            <option>1:00 AM</option>
                            <option>1:30 AM</option>
                            <option>2:00 AM</option>
                            <option>2:30 AM</option>
                            <option>3:00 AM</option>
                            <option>3:30 AM</option>
                            <option>4:00 AM</option>
                            <option>4:30 AM</option>
                            <option>5:00 AM</option>
                            <option>5:30 AM</option>
                            <option>6:00 AM</option>
                            <option>6:30 AM</option>
                            <option>7:00 AM</option>
                            <option>7:30 AM</option>
                            <option>8:00 AM</option>
                            <option>8:30 AM</option>
                            <option>9:00 AM</option>
                            <option>9:30 AM</option>
                            <option>10:00 AM</option>
                            <option>10:30 AM</option>
                            <option>11:00 AM</option>
                            <option>11:30 AM</option>
                            <option>12:00 PM</option>
                            <option>12:30 PM</option>
                            <option>1:00 PM</option>
                            <option>1:30 PM</option>
                            <option>2:00 PM</option>
                            <option>2:30 PM</option>
                            <option>3:00 PM</option>
                            <option>3:30 PM</option>
                            <option>4:00 PM</option>
                            <option>4:30 PM</option>
                            <option>5:00 PM</option>
                            <option>5:30 PM</option>
                            <option>6:00 PM</option>
                            <option>6:30 PM</option>
                            <option>7:00 PM</option>
                            <option>7:30 PM</option>
                            <option>8:00 PM</option>
                            <option>8:30 PM</option>
                            <option>9:00 PM</option>
                            <option>9:30 PM</option>
                            <option>10:00 PM</option>
                            <option>10:30 PM</option>
                            <option>11:00 PM</option>
                            <option>11:30 PM</option>
                          </select>
                        </div>
                        <p className="ms-3">To</p>
                        <div className="">
                          <select
                            className="form-select "
                            id="pr-city"
                            name="reservation_endTime"
                            value={formValues.reservation_endTime}
                            onChange={handleChangeInput}
                          >
                            <option>Select Time</option>
                            <option>12:00 AM</option>
                            <option>12:30 AM</option>
                            <option>1:00 AM</option>
                            <option>1:30 AM</option>
                            <option>2:00 AM</option>
                            <option>2:30 AM</option>
                            <option>3:00 AM</option>
                            <option>3:30 AM</option>
                            <option>4:00 AM</option>
                            <option>4:30 AM</option>
                            <option>5:00 AM</option>
                            <option>5:30 AM</option>
                            <option>6:00 AM</option>
                            <option>6:30 AM</option>
                            <option>7:00 AM</option>
                            <option>7:30 AM</option>
                            <option>8:00 AM</option>
                            <option>8:30 AM</option>
                            <option>9:00 AM</option>
                            <option>9:30 AM</option>
                            <option>10:00 AM</option>
                            <option>10:30 AM</option>
                            <option>11:00 AM</option>
                            <option>11:30 AM</option>
                            <option>12:00 PM</option>
                            <option>12:30 PM</option>
                            <option>1:00 PM</option>
                            <option>1:30 PM</option>
                            <option>2:00 PM</option>
                            <option>2:30 PM</option>
                            <option>3:00 PM</option>
                            <option>3:30 PM</option>
                            <option>4:00 PM</option>
                            <option>4:30 PM</option>
                            <option>5:00 PM</option>
                            <option>5:30 PM</option>
                            <option>6:00 PM</option>
                            <option>6:30 PM</option>
                            <option>7:00 PM</option>
                            <option>7:30 PM</option>
                            <option>8:00 PM</option>
                            <option>8:30 PM</option>
                            <option>9:00 PM</option>
                            <option>9:30 PM</option>
                            <option>10:00 PM</option>
                            <option>10:30 PM</option>
                            <option>11:00 PM</option>
                            <option>11:30 PM</option>
                          </select>
                        </div>
                        <NavLink
                          className="pull-right ms-5"
                          onClick={handleTimeCancel}
                        >
                          Change
                        </NavLink>
                      </div>
                    ) : (
                      <div className="d-flex">
                        <h6>
                          {selectedTime} - {selectedTimeEnd}
                        </h6>
                        <NavLink
                          className="pull-right ms-5"
                          onClick={handleChangeTime}
                        >
                          Change
                        </NavLink>
                      </div>
                    )} */}

                  <div className="d-flex">
                    <h6>
                      {selectedTime} - {selectedTimeEnd}
                    </h6>
                    {/* <NavLink
                        className="pull-right ms-5"
                        onClick={handleChangeTime}
                      >
                        Change
                      </NavLink> */}
                  </div>

                  <div className="col-sm-12">
                    <h6>Reservation Status: OPEN</h6> <br />
                    <p>This reservation is valid and available.</p>
                  </div>

                  {reservationData?.policy ? (
                    <div className="col-sm-12">
                      <label className="form-label mt-3">Usage Policy</label>
                      <textarea
                        className="form-control mt-0"
                        id="textarea-input"
                        rows="5"
                        name="policy"
                        onChange={handleChangeInput}
                        autoComplete="on"
                        value={formValues.policy}
                      ></textarea>
                    </div>
                  ) : (
                    ""
                  )}

                  <div className="col-sm-12 form-check mt-4">
                    <input
                      className="form-check-input"
                      name="checkbox"
                      type="checkbox"
                      id="agree-to-terms"
                      onClick={handleCheck}
                      checked={checkBox}
                    />
                    <label className="form-check-label">
                      I have read and accept the terms for reserving this shift.
                    </label>
                  </div>

                  <button
                    className="btn btn-primary mb-5 mt-3"
                    onClick={handleSubmit}
                  >
                    Confirm Reservation
                  </button>
                </div>
              </div>
              <div className="col-md-6">
                <div style={{ height: "500px" }}>
                  <BigCalendar
                    localizer={localizer}
                    events={events}
                    startAccessor="start"
                    endAccessor="end"
                    // eventPropGetter={eventStyleGetter}
                    // dayPropGetter={dayPropGetter}
                    style={{ flex: 1 }}
                    // onSelectSlot={handleSelectSlot}
                    selectable={true}
                    // onNavigate={handleNavigate}
                    views={{
                      month: true,
                      week: false,
                      day: true,
                      agenda: false,
                    }}
                    component={{
                      toolbar: CustomToolbar,
                    }}
                  />
                </div>
              </div>
            </div>
          </div>
        ) : (
          <div className="content-overlay">
            <div class="btn-group dropdown pull-right">
              <button
                className="btn btn-secondary pull-right"
                type="button"
                onClick={handleBack}
              >
                Cancel
              </button>
              <button
                type="button"
                className="btn btn-primary dropdown-toggle ms-2"
                data-bs-toggle="dropdown"
                aria-haspopup="true"
                aria-expanded="false"
              >
                Action
              </button>
              <div className="dropdown-menu my-1">
                <NavLink
                  to={`/reservation-Schedule/${params.id}`}
                  className="dropdown-item"
                >
                  Configure Schedule
                </NavLink>

                <NavLink
                  to={`/reservation-policy/${params.id}`}
                  className="dropdown-item"
                >
                  {reservationData.policy
                    ? "Update Usage Policy "
                    : "Add Usage Policy "}
                </NavLink>

                <NavLink
                  to={`/add-reservations/${params.id}`}
                  className="dropdown-item"
                >
                  {params.id ? "Edit Resource Info" : ""}
                </NavLink>
              </div>
            </div>

            <div className="d-flex align-items-center justify-content-start mb-4">
              <h3 className="mb-0 text-white">
                {reservationData.calender_name}
              </h3>
            </div>

            <div className="bg-light p-3 border-0 rounded-3">
              <div className="promoted_product float-left w-100">
                <div className="row mb-4">
                  <div className="col-md-8">
                        <div className="card-body position-relative p-2 pb-0 d-lg-flex d-md-flex d-sm-flex d-block">
                          <div className="d-flex align-items-center justify-content-between">
                            <i class="fa fa-briefcase icon" aria-hidden="True"></i>
                            <h6 className="ms-2">
                              {reservationData.calender_name}
                            </h6>
                          </div>
                          <h6 className="ms-3">
                              {reservationData.publication_Level === "corporate"
                              ? "- Corporate"
                              : ""}
                          </h6>
                      </div>
                    
                    <div className="reservation_details">
                      <div className="blogpost_image d-flex">
                        <img
                          className="img-fluid"
                          src={
                            reservationData.image &&
                            reservationData.image !== "image"
                              ? reservationData.image
                              : defaultpropertyimage
                          }
                          alt="Reservation"
                        />

                        <span className="ms-4">
                          {reservationData.calender_blurb ? (
                            <p className="">{reservationData.calender_blurb}</p>
                          ) : (
                            ""
                          )}
                        </span>
                      </div>
                    </div>
                  </div>
                </div>

                <div className="row">
                  <div className="col-md-4">
                    <div className="reservation_calender">
                      <strong>
                        {moment(selectedDate).format("dddd, MMM D, YYYY") ??
                          formattedDate}
                      </strong>
                      <hr />
                      {renderTimeSlots()}
                    </div>
                  </div>
                  <div className="col-md-8">
                    <div style={{ height: "500px" }}>
                      <BigCalendar
                        localizer={localizer}
                        events={events}
                        startAccessor="start"
                        endAccessor="end"
                        // eventPropGetter={eventStyleGetter}
                        dayPropGetter={dayPropGetter}
                        style={{ flex: 1 }}
                        onSelectSlot={handleSelectSlot}
                        selectable={true}
                        onNavigate={handleNavigate}
                        views={{
                          month: true,
                          week: false,
                          day: true,
                          agenda: false,
                        }}
                        component={{
                          toolbar: CustomToolbar,
                        }}
                      />
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        )}
      </main>
    </div>
  );
}
export default ReservationSinglePage;

import React, { useState, useEffect } from "react";
import user_service from "../service/user_service";
import { NavLink, useNavigate, useParams } from "react-router-dom";
import Loader from "../../Pages/Loader/Loader.js";
import Toaster from "../../Pages/Toaster/Toaster";
import ReactPaginate from "react-paginate";
import defaultpropertyimage from "../../Components/img/defaultpropertyimage.jpeg";

const ContactListing = () => {
  const [loader, setLoader] = useState({ isActive: null });
  const params = useParams();
  const { isActive } = loader;
  const [toaster, setToaster] = useState({
    types: null,
    isShow: null,
    toasterBody: "",
    message: "",
  });
  const { types, isShow, toasterBody, message } = toaster;

  const [getListing, setGetListing] = useState([]);
  const [pageCount, setpageCount] = useState(0);
  const initialValues = {
    listingsAgent: "",
    category: "",
    mlsId: "",
    propertyType: "",
    mlsNumber: "",
    agentId: "",
    associateName: "",
    price: "",
    garage: "",
    bed: "",
    subDivision: "",
    streetAddress: "",
    city: "",
    state: "",
    zipCode: "",
    schoolDistrtict: "",
    country: "",
    areaLocation: "",
    status: "",
    listDate: "",
    image: "",
  };
  const [formValues, setFormValues] = useState(initialValues);
  const [formErrors, setFormErrors] = useState({});

  const navigate = useNavigate();

  const SingleListing = (id) => {
    navigate(`/single-listing/${id}`);
  };

  const propertypic = (e) => {
    e.target.src = defaultpropertyimage;
  };

  const ListingGetAll = async () => {
    const agentId = params.id;
    await user_service
      .getContactListings(agentId, "listing")
      .then((response) => {
        setLoader({ isActive: false });
        if (response) {
          setGetListing(response.data.data);
          setpageCount(Math.ceil(response.data.totalRecords / 10));
        }
      });
  };
  useEffect(() => {
    window.scrollTo(0, 0);
    setLoader({ isActive: true });
    ListingGetAll();
  }, []);

  return (
    <div className="bg-secondary float-left w-100 pt-4">
      <Loader isActive={isActive} />
      {isShow && (
        <Toaster
          types={types}
          isShow={isShow}
          toasterBody={toasterBody}
          message={message}
        />
      )}
      <main className="page-wrapper">
        {/* <!-- Page content--> */}
        {/* <!-- Page container--> */}
        <div className="container mb-md-4 py-5 pt-0">
          {/* <!-- Breadcrumb--> */}
          {/* <nav className="mb-3 mb-md-3 pt-md-3" aria-label="Breadcrumb">
            <ol className="breadcrumb breadcrumb-dark">
              <li className="breadcrumb-item">
                <a href="/">Home</a>
              </li>
              <li className="breadcrumb-item">
                <a href="/">Account</a>
              </li>
              <li className="breadcrumb-item active" aria-current="page">
                Property
              </li>
            </ol>
          </nav> */}

          {/* <ul
            className="nav nav-tabs d-flex align-items-center align-items-sm-center justify-content-center border-bottom mb-4 pb-2"
            role="tablist"
          >
            <li className="nav-item me-sm-3 mb-3" role="presentation">
              <a
                className="nav-link text-center active"
                href="#tab"
                data-bs-toggle="tab"
                role="tab"
                aria-controls="reviews-about-you"
                aria-selected="true"
              >
                Active Listings
              </a>
            </li>
            <li className="nav-item mb-3" role="presentation">
              <a
                className="nav-link text-center"
                href="#tab2"
                data-bs-toggle="tab"
                role="tab"
                aria-controls="reviews-by-you"
                aria-selected="false"
                tabindex="-1"
              >
                Recently Solid listings
              </a>
            </li>
          </ul> */}
          <div className="float-left w-100 d-flex align-items-center justify-content-between mb-4">
            <h3 className="mb-0 text-white" id="">
              My Listings
            </h3>
          </div>
          
          <div className="bg-light border rounded-3 p-3">
          <div className="tns-carousel-wrapper tns-controls-outside-xxl tns-nav-outside tns-nav-outside-flush mx-n2">
            <div className="tns-outer" id="tns2-ow">
              <div
                className="tns-liveregion tns-visually-hidden"
                aria-live="polite"
                aria-atomic="true"
              >
                slide <span className="current">9 to 12</span> of 5
              </div>
              <div id="tns2-mw" className="tns-ovh">
                <div className="tns-inner" id="tns2-iw">
                  <div
                    className="tns-carousel-inner row gx-4 mx-0 pt-3 pb-4  tns-slider tns-carousel tns-subpixel tns-calc tns-horizontal"
                    data-carousel-options='{"items": 4, "responsive": {"0":{"items":1},"500":{"items":2},"768":{"items":3},"992":{"items":4}}}'
                    id="tns2"
                  >
                    {
                      getListing && getListing.length > 0 ?
                    getListing.map((post) => (
                      <>
                        <div
                          className="col-md-3 tns-item tns-slide-cloned tns-slide-active"
                          onClick={() => SingleListing(post._id)}
                        >
                          <div className="card shadow-sm card-hover border-0 h-100">
                            <div className="card-img-top card-img-hover">
                              {/* <div className="position-absolute start-0 top-0 pt-3 ps-3"><span className="d-table badge bg-success mb-1">Verified</span><span className="d-table badge bg-info">New</span></div> */}
                              <div className="content-overlay end-0 top-0 pt-3 pe-3">
                                {/* <button className="btn btn-icon btn-light btn-xs text-primary rounded-circle"
                                   type="button" data-bs-toggle="tooltip" data-bs-placement="left" 
                                   aria-label="Add to Wishlist" data-bs-original-title="Add to Wishlist"><i className="fi-heart"></i></button> */}
                              </div>
                              <img
                                className="img-fluid w-100 rounded-0"
                                src={
                                  post.image && post.image !== "image"
                                    ? post.image
                                    : defaultpropertyimage
                                }
                                alt="Property"
                                onError={(e) => propertypic(e)}
                              />
                            </div>
                            <div className="card-body position-relative pb-3">
                              <h4 className="mb-2 fs-xs fw-normal text-uppercase text-primary">
                                {post.status}
                              </h4>
                              <h3 className="h6 mb-0 fs-base">
                                <i className="fi-cash mt-n1 me-2 lead align-middle opacity-70"></i>
                                $
                                {parseFloat(post.price).toLocaleString("en-US")}
                              </h3>
                              <p className="mb-1">
                                <small>
                                  <span>
                                    {
                                      JSON.parse(post.additionaldataMLS)
                                        .value[0].BedroomsTotal
                                    }
                                    &nbsp;beds
                                  </span>
                                  &nbsp;*&nbsp;
                                  <span>
                                    {JSON.parse(post.additionaldataMLS).value[0]
                                      .BathroomsFull ?? 0}{" "}
                                    &nbsp;bath
                                  </span>
                                  &nbsp;*&nbsp;
                                  <span>
                                    {
                                      JSON.parse(post.additionaldataMLS)
                                        .value[0].BuildingAreaTotal
                                    }
                                    &nbsp;Sqft
                                  </span>
                                </small>
                              </p>
                              <p className="mb-1 fs-sm text-muted">
                                {post.streetAddress} , {post.state}
                              </p>

                              <p className="mb-0 fs-sm text-muted">
                                MLS#: {post.mlsNumber}
                              </p>
                            </div>
                          </div>
                        </div>
                      </>
                    ))
                    :<p className="text-center">Not any listing available.</p>
                    }
                  </div>
                </div>
              </div>
            </div>
          </div>
          </div>
        </div>
      </main>
    </div>
  );
};

export default ContactListing;

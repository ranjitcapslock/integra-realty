import React, { useState, useEffect } from "react";
import avtar from "../../Components/img/avtar.jpg";
import _ from "lodash";
import user_service from "../../Components/service/user_service";
import jwt from "jwt-decode";
import Loader from "../../Pages/Loader/Loader.js";
import Toaster from "../../Pages/Toaster/Toaster";
import { useNavigate, NavLink, useParams } from "react-router-dom";
import { Calendar as BigCalendar, momentLocalizer } from "react-big-calendar";
import { MultiSelect } from "react-multi-select-component";
import { MultiSelect as MultiSelect2 } from "react-multi-select-component";
import moment from "moment-timezone";
const localizer = momentLocalizer(moment);

function AddContact() {
  const initialValues = {
    firstName: "",
    contactType: "private",
    lastName: "",
    company: "",
    email: "",
    phone: "",
    agentId: "",
    contactpermission: "",
    intranet_id: "",
    mls_membership: "",
    board_membership: "",
    
  };
  const [formValues, setFormValues] = useState(initialValues);
  const [formValuesss, setFormValuesss] = useState("");
  const [getContact, setGetContact] = useState("");

  const params = useParams();
  const [board_membership, setBoard_membership] = useState([]);
  const [mls_membership, setMls_membership] = useState([]);

  const [formErrors, setFormErrors] = useState({});
  const [isSubmitClick, setISSubmitClick] = useState(false);
  const [loader, setLoader] = useState({ isActive: null });
  const { isActive } = loader;
  const [toaster, setToaster] = useState({
    types: null,
    isShow: null,
    toasterBody: "",
    message: "",
  });
  const { types, isShow, toasterBody, message } = toaster;

  const [clientId, setClientId] = useState("");
  const [summary, setSummary] = useState([]);
  const [arrayData, setArrayData] = useState([]);
  const [select, setSelect] = useState("");

  const [results, setResults] = useState([]);
  const [pageCount, setPageCount] = useState(0);
  const [currentPage, setCurrentPage] = useState(1);
  const [isLoading, setIsLoading] = useState(false);
  const [contactType, setContactType] = useState(false);

  const navigate = useNavigate();
  const [checkBox, setSetCheckbox] = useState(false);

  const [commaSeparatedValues, setCommaSeparatedValues] = useState("");
  const [
    commaSeparatedValuesmls_membership,
    setCommaSeparatedValuesmls_membership,
  ] = useState("");

  const handleCheck = () => {
    setSetCheckbox(checkBox ? true : false);
    console.log(checkBox);
  };

  const handleChanges = (e) => {
    const select = e.target.value;
    setFormValuesss({
      ...formValuesss,
      intranet_id: select,
    });
    // setFormValues("")
  };

  const handleChangesMerber = (e) => {
    const select = e.target.value;
    setFormValuesss({
      ...formValuesss,
      mls_membership: select,
    });
    // setFormValues("")
  };

  const handleChangesBoard = (e) => {
    const select = e.target.value;
    setFormValuesss({
      ...formValuesss,
      board_membership: select,
    });
    // setFormValues("")
  };
  {
    /* <!-- Input onChange Start--> */
  }
  const handleChange = (e) => {
    const { name, value } = e.target;
    setFormValues({ ...formValues, [name]: value });
  };
  {
    /* <!-- Input onChange End--> */
  }

  {
    /* <!-- Form Validation Start--> */
  }
  useEffect(() => {
    window.scrollTo(0, 0);
    if (formValues && isSubmitClick) {
      validate();
    }
  }, [formValues]);

  const validate = () => {
    const values = formValues;
    const errors = {};

    // if (!values.contactType) {
    //   errors.contactType = "Contact Type is required";
    // }
    if (!values.firstName) {
      errors.firstName = "Name is required";
    }

    if (!values.lastName) {
      errors.lastName = "Last Name is required";
    }

    if (!values.email) {
      errors.email = "email is required";
    } else if (
      !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i.test(values.email)
    ) {
      errors.email = "Invalid email address";
    }
    

    // if (!values.email) {
    //   errors.email = "email is required";
    // } else if (
    //   !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.email)
    // ) {
    //   errors.email = "Invalid email address";
    // }

    setFormErrors(errors);
    return errors;
  };
  {
    /* <!-- Form Validation End--> */
  }

  {
    /* <!--Api call Form onSubmit Start--> */
  }
  const handleNewContact = async (e) => {
    if (formValues.contactType === "associate") {
      e.preventDefault();
      setISSubmitClick(true);
      let checkValue = validate();
      if (_.isEmpty(checkValue)) {
        const additionalActivateFields = {
          intranet_id: formValuesss.intranet_id ?? formValues.email,
          mls_membership: JSON.stringify(mls_membership),
          board_membership: JSON.stringify(board_membership),
        };
        const userData = {
          active_office: localStorage.getItem("active_office"),
          active_office_id: localStorage.getItem("active_office_id"),
          firstName: formValues.firstName,
          lastName: formValues.lastName,
          nickname: formValues.lastName + "" + formValues.firstName,
          company: formValues.company,
          email: formValues.email,
          phone: formValues.phone,
          agentId: jwt(localStorage.getItem("auth")).id,
          contactType: formValues.contactType,
          contactpermission: formValues.contactpermission,
          contact_status: "inapproval",
          additionalActivateFields,
        };
        console.log("userData", userData);
        try {
          setLoader({ isActive: true });
          const response = await user_service.contact(userData);
  
          if (response) {
            setLoader({ isActive: false });
            //const response = await user_service.contact(userData, formValues.firstName);
            const contactData = response.data;
            setSummary((prevSummary) => [...prevSummary, contactData]);
            setArrayData((prevArrayData) => [...prevArrayData, contactData._id]);
            setSelect("");
            setLoader({ isActive: false });
            if (formValues.contactType === "associate") {
              setToaster({
                type: "Contact Added Successfully",
                isShow: true,
                toasterBody: response.data.message,
                message: "Contact Added Successfully",
              });
            } else {
              setToaster({
                type: "Contact Added Successfully",
                isShow: true,
                toasterBody: response.data.message,
                message: "Contact Added Successfully",
              });
            }
            setTimeout(() => {
              setToaster((prevToaster) => ({ ...prevToaster, isShow: false }));
              navigate(`/thank-you/${response.data._id}`);
            }, 2000);
          }
        } catch (error) {
          setLoader({ isActive: false });
          setToaster({
            types: "error",
            isShow: true,
            toasterBody: error.response
              ? error.response.data.message
              : error.message,
            message: "Error",
          });
  
          setTimeout(() => {
            setToaster((prevToaster) => ({ ...prevToaster, isShow: false }));
          }, 2000);
        }
      }
    }
    else{
      e.preventDefault();
      setISSubmitClick(true);
      let checkValue = validate();
      if (_.isEmpty(checkValue)) {
        const additionalActivateFields = {
          intranet_id: formValuesss.intranet_id ?? formValues.email,
          mls_membership: JSON.stringify(mls_membership),
          board_membership: JSON.stringify(board_membership),
        };
        const userData = {
          active_office: localStorage.getItem("active_office"),
          active_office_id: localStorage.getItem("active_office_id"),
          firstName: formValues.firstName,
          lastName: formValues.lastName,
          nickname: formValues.lastName + "" + formValues.firstName,
          company: formValues.company,
          email: formValues.email,
          phone: formValues.phone,
          agentId: jwt(localStorage.getItem("auth")).id,
          contactType: formValues.contactType,
          contactpermission: formValues.contactpermission,
          contact_status: "active",
          additionalActivateFields,
        };
      
        try {
          setLoader({ isActive: true });
          const response = await user_service.contact(userData);
  
          if (response) {
            setLoader({ isActive: false });
            const contactData = response.data;
            setSummary((prevSummary) => [...prevSummary, contactData]);
            setArrayData((prevArrayData) => [...prevArrayData, contactData._id]);
            setSelect("");
            setLoader({ isActive: false });
            setToaster({
              type: "Contact Added Successfully",
              isShow: true,
              toasterBody: response.data.message,
              message: "Contact Added Successfully",
            });
            
            setTimeout(() => {
              setToaster((prevToaster) => ({ ...prevToaster, isShow: false }));
              navigate(`/thanks/${response.data._id}`);
            }, 2000);
          }
        } catch (error) {
          setLoader({ isActive: false });
          setToaster({
            types: "error",
            isShow: true,
            toasterBody: error.response
              ? error.response.data.message
              : error.message,
            message: "Error",
          });
  
          setTimeout(() => {
            setToaster((prevToaster) => ({ ...prevToaster, isShow: false }));
          }, 2000);
        }
      }
    }
  };
  {
    /* <!-- Api call Form onSubmit End--> */
  }

  const handleSearchTransaction = async (id) => {
    await user_service.contactGetById(id).then((response) => {
      setClientId(id);
      setSelect(response.data);
    });
  };

  const cancel = () => {
    setSelect("");
  };
  const selectContact = async (id) => {
    if (contactType === "associate") {
      navigate(`/activate-account/${id}`);
    } else {
      navigate(`/contact-profile/${id}`);
    }
  };

  {
    /* <!-- paginate Function Api call Start--> */
  }
  const [searchTerm, setSearchTerm] = useState("");

  // useEffect(() => { window.scrollTo(0, 0);
  //     if (formValues) {
  //         SearchGetAll(0);
  //     }
  // }, [formValues]);
  useEffect(() => {
    const delayDebounceFn = setTimeout(() => {
      if (formValues) {
        SearchGetAll(0);
      }
    }, 1000);

    return () => clearTimeout(delayDebounceFn);
  }, [formValues]);

  const SearchGetAll = async () => {
    setIsLoading(true);

    await user_service
      .SearchContactGet(0,3, formValues.firstName)
      .then((response) => {
        setIsLoading(false);
        if (response) {
          setResults(response.data.data);
          setPageCount(Math.ceil(response.data.totalRecords / 10));
        }
      });
  };

  const handlePageClick = async (data) => {
    let currentPage = data.selected + 1;
    let skip = currentPage - 1 + currentPage;
    setLoader({ isActive: true });
    console.log(currentPage);
    await user_service
    .SearchContactGet(currentPage, 3, formValues.firstName)
      .then((response) => {
        setLoader({ isActive: false });
        if (response) {
          setSummary(response.data);
          setResults(response.data.data);
          setPageCount(Math.ceil(response.data.totalRecords / 10));
        }
      });
  };

  const handleSearch = (event) => {
    event.preventDefault();
    setCurrentPage(1);
    SearchGetAll(0);
  };
  {
    /* <!-- paginate Function Api call End--> */
  }

  const [calenderGet, setCalenderGet] = useState([]);
  const [eventFilter, setEventFilter] = useState("Office");

  const fetchCalendar = async () => {
    try {
      setLoader({ isActive: true });
      let queryParams;
      if (eventFilter) {
        queryParams += `&category=${eventFilter}`;
      }
      const response = await user_service.CalenderGet(queryParams);

      setLoader({ isActive: false });

      if (response && response.data) {
        setCalenderGet(response.data);
      }
    } catch (error) {
      console.error("Error in fetchCalendar:", error);
      // Handle errors as needed, e.g., show a user-friendly error message
    }
  };

  useEffect(() => {
    window.scrollTo(0, 0);
    fetchCalendar();
  }, []);

  const [selectedDate, setSelectedDate] = useState(null);
  const handleSelectSlot = (slotInfo) => {
    if (
      localStorage.getItem("auth") &&
      jwt(localStorage.getItem("auth")).contactType == "admin"
    ) {
      setSelectedDate(slotInfo);
      window.$("#modal-show-calender").modal("show");
    }
  };
  const handleEventClick = (event) => {
    event.preventDefault();
    const eventId = event.currentTarget.getAttribute("data-eventid");
    navigate(`/event-detail/${eventId}`);
  };

  const [currentDate, setCurrentDate] = useState("");

  useEffect(() => {
    window.scrollTo(0, 0);
    const interval = setInterval(() => {
      const options = { dateStyle: "full" };
      setCurrentDate(new Date().toLocaleDateString("en-US", options));
    }, 1000);

    return () => {
      clearInterval(interval);
    };
  }, []);

  const [events, setEvents] = useState([]);

  useEffect(() => {
    window.scrollTo(0, 0);
    const generatedEvents = generateEvents();
    // console.log(generatedEvents);
    setEvents(generatedEvents);
  }, [calenderGet]);

  const eventColors = {
    meeting: "#FF5733",
    duty: "#3366FF",
  };
  const eventStyleGetter = (event) => {
    const backgroundColor = eventColors[event.type];
    return {
      style: {
        backgroundColor,
      },
    };
  };

 
  const generateEvents = () => {
    const generatedEvents = [];

    if (calenderGet && calenderGet.data) {
      calenderGet.data.forEach((date) => {
        let startDate;
        let endDate;

        if (date.startDate.includes("/")) {
          startDate = moment.tz(date.startDate, "M/D/YYYY", "America/Denver").toDate();
          endDate = moment.tz(date.endDate, "M/D/YYYY", "America/Denver").toDate();
        } else {
          startDate = moment.tz(date.startDate, "America/Denver").toDate();
          endDate = moment.tz(date.endDate, "America/Denver").toDate();
        }

        const eventId = date._id;

        generatedEvents.push({
          title: date.title,
          start: startDate,
          end: endDate,
          type: "meeting",
          id: eventId,
        });
      });
    }
    return generatedEvents;
  };


  const CalenderPage = () => {
    navigate("/calendar");
  };


  const [formData, setFormData] = useState([]);

  const StaffRoleGetData = async () => {
    await user_service.StaffRoleGet().then((response) => {
      if (response) {
        console.log(response);
        setFormData(response.data.data);
      }
    });
  };
  useEffect(() => {
    if (formData) {
      StaffRoleGetData();
    }
  }, []);

  return (
    <div className="bg-secondary float-left w-100 mb-4 pt-4">
      <Loader isActive={isActive} />
      {isShow && (
        <Toaster
          types={types}
          isShow={isShow}
          toasterBody={toasterBody}
          message={message}
        />
      )}
      <div className="">
        <main className="page-wrapper homepage_layout">
          <div className="d-flex align-items-center justify-content-start mb-4">
            <h3 className="mb-0 text-white">Add a new contact</h3>
          </div>
          {/* <hr className="mb-3" /> */}

          <div className="row event-calender">
            <>
              <div className="col-md-8">
                <div className="bg-light border rounded-3 p-3">
                  {select === "" ? (
                    <>
                      <form onSubmit={handleSearch}>
                        <div className="mb-3">
                          <label className="form-label">Contact Type</label>
                          <br />
                          <select
                            className="form-select"
                            name="contactType"
                            value={formValues.contactType}
                            onChange={handleChange}
                            style={{
                              border: formErrors?.contactType
                                ? "1px solid red"
                                : "1px solid #00000026",
                            }}
                          >
                            {/* <option></option> */}
                            <option value="private">Private</option>
                            <option value="community">Community</option>
                            {localStorage.getItem("auth") &&
                            jwt(localStorage.getItem("auth")).contactType ==
                              "admin"  ? (
                              <option value="associate">Associate</option>
                            ) : (
                              <>
                              {localStorage.getItem("auth") &&
                                jwt(localStorage.getItem("auth")).contactType ==
                                  "staff" &&
                                (Array.isArray(formData) && formData.length > 0
                                  ? formData.map((item) =>
                                      item.roleStaff === "add_contact" ? (
                                     <option value="associate">Associate</option>
                                      ) : (
                                        ""
                                      )
                                    )
                                  : "")}
                            </>
                            )}
                               {localStorage.getItem("auth") &&
                            jwt(localStorage.getItem("auth")).contactType ==
                              "admin" ? (
                              <option value="staff">Staff</option>
                            ) : (
                              ""
                            )}
                          </select>
                          <div className="invalid-tooltip">
                            {formErrors.contactType}
                          </div>
                        </div>

                        {/* <!-- First Name input --> */}
                        <div className="mb-3">
                          <label className="form-label">First Name</label>
                          <br />
                          <input
                            className="form-control"
                            type="text"
                            id="text-input"
                            name="firstName"
                            placeholder="Enter first name"
                            onChange={(event) => handleChange(event)}
                            autoComplete="on"
                            style={{
                              border: formErrors?.firstName
                                ? "1px solid red"
                                : "1px solid #00000026",
                            }}
                            value={formValues.firstName}
                          />
                          <div className="invalid-tooltip">
                            {formErrors.firstName}
                          </div>
                        </div>

                        {/* <!-- Last Name input --> */}
                        <div className="mb-3">
                          <label className="form-label">Last Name</label>
                          <input
                            className="form-control"
                            type="text"
                            id="text-input"
                            name="lastName"
                            placeholder="Enter last name"
                            onChange={(event) => handleChange(event)}
                            autoComplete="on"
                            style={{
                              border: formErrors?.lastName
                                ? "1px solid red"
                                : "1px solid #00000026",
                            }}
                            value={formValues.lastName}
                          />
                          <div className="invalid-tooltip">
                            {formErrors.lastName}
                          </div>
                        </div>

                        {/* <!-- Company Input --> */}
                        <div className="mb-3">
                          <label className="form-label">Company</label>
                          <input
                            className="form-control"
                            type="text"
                            id="text-input"
                            name="company"
                            placeholder="Enter company"
                            onChange={(event) => handleChange(event)}
                            autoComplete="on"
                            value={formValues.company}
                          />
                        </div>

                        {/* <!-- Email input --> */}
                        <div className="mb-3">
                          <label className="form-label">Email</label>
                          <input
                            className="form-control"
                            id="email-input"
                            type="text"
                            name="email"
                            placeholder="Enter email"
                            onChange={(event) => handleChange(event)}
                            autoComplete="on"
                            style={{
                              border: formErrors?.email
                                ? "1px solid red"
                                : "1px solid #00000026",
                            }}
                            value={formValues.email}
                          />
                          <div className="invalid-tooltip">
                            {formErrors.email}
                          </div>
                        </div>

                        {/* <!-- Phone Input --> */}
                        <div className="mb-3">
                          <label className="form-label">Phone</label>
                          <input
                            className="form-control"
                            type="tel"
                            id="tel-input"
                            name="phone"
                            placeholder="Enter Phone"
                            onChange={(event) => handleChange(event)}
                            autoComplete="on"
                            pattern="[0-9]+"
                            required
                            title="Please enter a valid phone number"
                            value={formValues.phone}
                          />
                        </div>

                        {formValues.contactType === "associate" ? (
                          <div className="mb-2">
                            <label className="form-label">Permision As</label>
                            <select
                              className="form-select form-select-dark"
                              name="contactpermission"
                              value={formValues.contactpermission}
                              onChange={(event) => handleChange(event)}
                            >
                              <option value="full">Full Agent</option>
                              <option value="referralonly">
                                Referral Only Agent
                              </option>
                            </select>
                          </div>
                        ) : (
                          ""
                        )}

                        {formValues.contactType === "associate" &&
                        localStorage.getItem("auth") &&
                        jwt(localStorage.getItem("auth")).contactType ===
                          "admin" ||   localStorage.getItem("auth") &&
                          jwt(localStorage.getItem("auth")).contactType ===
                            "staff"  ? (
                          <>
                            {/* <div className="mb-3">
                              <label className="form-label">Intranet ID</label>
                              <input
                                className="form-control"
                                type="text"
                                name="intranet_id"
                                value={
                                  formValuesss.intranet_id ?? formValues.email
                                }
                                onChange={(event) => handleChanges(event)}

                              />
                            
                            </div> */}

                            <div className="mb-0">
                              <div className="row">
                                <label className="col-form-label">MLS</label>
                                <div className="col-md-12">
                                  <label className="form-label">
                                    MLS Membership
                                  </label>
                               
                                  <MultiSelect
                                    options={[
                                      {
                                        label: `UtahRealEstate.com`,
                                        value: `UtahRealEstate.com`,
                                      },
                                      {
                                        label: `Park City`,
                                        value: `Park City`,
                                      },
                                      {
                                        label: `Washington County`,
                                        value: `Washington County`,
                                      },
                                      {
                                        label: `Iron County`,
                                        value: `Iron County`,
                                      },
                                      // {
                                      //   label: `Local MLS (No IDX)`,
                                      //   value: `Local MLS (No IDX)`,
                                      // },
                                      // { label: `Tooele County`, value: `Tooele County` },
                                      // { label: `Cache Valley`, value: `Cache Valley` },
                                      // { label: `Brigham-Tremonton Board`, value: `Brigham-Tremonton Board` }
                                    ]}
                                    value={mls_membership}
                                    onChange={setMls_membership}
                                    labelledBy="Select MLS"
                                    name="mls_membership"
                                    // hasSelectAll={false}
                                    // selectionLimit={1}
                                    closeOnChangedValue={true}
                                  />
                                </div>
                              
                              </div>
                            </div>
                            <div className="mb-3">
                              <div className="col-md-12">
                                <label className="col-form-label">Board</label>
                                <div className="col-lg-12 mb-3">
                                  <label className="form-label">
                                    Board and the Board Membership
                                  </label>
                                  <MultiSelect2
                                    options={[
                                      {
                                        label: `Salt Lake Board`,
                                        value: `Salt Lake Board`,
                                      },
                                      {
                                        label: `Utah Central Association UCAR`,
                                        value: `Utah Central Association UCAR`,
                                      },
                                      {
                                        label: `Northern Wasatch`,
                                        value: `Northern Wasatch`,
                                      },
                                      {
                                        label: `Washington County`,
                                        value: `Washington County`,
                                      },
                                      {
                                        label: `Park City`,
                                        value: `Park City`,
                                      },
                                      {
                                        label: `Iron County`,
                                        value: `Iron County`,
                                      },
                                      {
                                        label: `Tooele County`,
                                        value: `Tooele County`,
                                      },
                                      {
                                        label: `Cache Valley`,
                                        value: `Cache Valley`,
                                      },
                                      {
                                        label: `Brigham-Tremonton Board`,
                                        value: `Brigham-Tremonton Board`,
                                      },
                                    ]}
                                    value={board_membership}
                                    onChange={setBoard_membership}
                                    labelledBy="Select Board Membership"
                                    name="board_membership"
                                    closeOnChangedValue={true}
                                  />
                                </div>
                              </div>
                            </div>
                          </>
                        ) : (
                          ""
                        )}

                        {/* <!-- Checkbox Input --> */}
                        <div>
                          <div className="form-check">
                            <input
                              className="form-check-input"
                              name="checkbox"
                              type="checkbox"
                              id="agree-to-terms"
                              onClick={handleCheck}
                              value={checkBox.checkBox}
                            />
                            <div className="invalid-tooltip">
                              {checkBox.checkBox}
                            </div>
                            <label className="form-check-label">
                              No email address available.
                            </label>
                          </div>
                        </div>
                      </form>
                    </>
                  ) : (
                    <div className="col-md-6 mt-5 w-100">
                      <div className="card bg-secondary">
                        <h6>{select.represent}</h6>
                        <div className="card-body">
                          <img
                            className="pull-right"
                            onClick={cancel}
                            src="https://cdn.pboffice.net/shared/transaction/page_select_drop.png"
                          />
                          <div className="view_profile">
                            <img
                              className="pull-left"
                              src={
                                select.image ||
                                "https://integra.backagent.net/common/handler/pub/photo.ashx?V=1703c0890a0462fd68520b884a04f86f.0&C=people&S=2&P=%2f!Default%2fThumb%2fphoto_pending.jpg"
                              }
                              alt="Profile"
                            />

                            <span>
                              <a href="#" className="card-title mt-0">
                                {select.firstName}&nbsp;{select.lastName}
                              </a>
                              <p>
                                {select.company}
                                <br />
                                {select.phone}
                                <br />
                                <a href="#">Send Message</a>
                              </p>
                            </span>
                          </div>
                        </div>
                      </div>
                      <NavLink
                        to="/contact"
                        type="button"
                        className="btn btn-default btn btn-secondary btn-sm ms-3"
                      >
                        Cancel
                      </NavLink>
                      <button
                        type="button"
                        className="btn btn-primary btn-sm  pull-left mt-2"
                        onClick={(e) => selectContact(select._id)}
                      >
                        Select Contact
                      </button>
                    </div>
                  )}
                </div>
                <div className="pull-right mt-3">
                  <button
                    type="button"
                    className="btn btn-primary btn-sm"
                    id="save-button"
                    onClick={handleNewContact}
                  >
                    Add New Contact
                  </button>
                  <NavLink
                    to="/contact"
                    type="button"
                    className="btn btn-secondary btn-sm ms-3"
                  >
                    Cancel
                  </NavLink>
                </div>
              </div>

              <div className="col-md-4 mt-lg-0 mt-md-0 mt-sm-4 mt-4">
              <div className="border bg-light rounded-3 p-3">
              <div className="d-flex align-items-center justify-content-between pb-2">
                <h6 className="mb-0">{currentDate}</h6>
                <span onClick={CalenderPage}>
                  <i className="fa fa-plus" aria-hidden="true"></i>
                </span>
              </div>
              <hr />
              <div className="caledar_view mt-2" style={{ height: "285px" }}>
                <BigCalendar
                  localizer={localizer}
                  events={events}
                  startAccessor="start"
                  endAccessor="end"
                  onSelectSlot={handleSelectSlot}
                  selectable={true}
                  components={{
                    event: ({ event }) => (
                      <div
                        className="rbc-event-content"
                        title={event.title}
                        onClick={handleEventClick}
                        data-eventid={event.id}
                      >
                        {event.title}
                      </div>
                    ),
                  }}
                  views={{
                    month: true,
                    week: false,
                    day: false,
                    agenda: false,
                  }}
                  // components={{
                  //   toolbar: CustomToolbar,
                  // }}
                  eventPropGetter={eventStyleGetter}
                  style={{ flex: 1 }}
                />
              </div>
              </div>
              </div>
            </>
          </div>
        </main>
      </div>
    </div>
  );
}
export default AddContact;

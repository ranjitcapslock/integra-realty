import React, { useState, useEffect } from "react";
import _ from "lodash";
import user_service from "../service/user_service.js";
import jwt from "jwt-decode";
import Loader from "../../Pages/Loader/Loader.js";
import Toaster from "../../Pages/Toaster/Toaster.js";
import { NavLink, useNavigate, useParams } from "react-router-dom";
import axios from "axios";

function ConfigureSchedule() {
  const params = useParams();
  const [loader, setLoader] = useState({ isActive: null });
  const { isActive } = loader;
  const [toaster, setToaster] = useState({
    types: null,
    isShow: null,
    toasterBody: "",
    message: "",
  });
  const { types, isShow, toasterBody, message } = toaster;

  const navigate = useNavigate();

  const [shifts, setShifts] = useState([
    { startTime: "", endTime: "", days: [] },
  ]);

  const handleAddShift = () => {
    setShifts([...shifts, { startTime: "", endTime: "", days: [] }]);
  };

  const handleDayChange = (event, shiftIndex) => {
    const { value, checked } = event.target;
    setShifts((prevShifts) => {
      const updatedShifts = [...prevShifts];
      const updatedDaysSet = new Set(updatedShifts[shiftIndex].days);
      if (checked) {
        updatedDaysSet.add(value);
      } else {
        updatedDaysSet.delete(value);
      }
      updatedShifts[shiftIndex].days = Array.from(updatedDaysSet);
      return updatedShifts;
    });
  };

  const handleChange = (event, index) => {
    const { name, value } = event.target;
    setShifts((prevShifts) => {
      const updatedShifts = [...prevShifts];
      updatedShifts[index] = {
        ...updatedShifts[index],
        [name]: value,
      };
      return updatedShifts;
    });
  };
  const [initialShifts, setInitialShifts] = useState([]);
  // const handleSubmit = async (e) => {
  //   e.preventDefault();

  //   const newShifts = shifts.filter(
  //     (shift, index) =>
  //       !initialShifts.some(
  //         (initialShift) =>
  //           initialShift.startTime === shift.startTime &&
  //           initialShift.endTime === shift.endTime &&
  //           initialShift.days.length === shift.days.length &&
  //           initialShift.days.every((day) => shift.days.includes(day))
  //       )
  //   );

  //   newShifts.forEach(async (shift, index) => {
  //     const userData = {
  //       agentId: jwt(localStorage.getItem("auth")).id,
  //       startTime: shift.startTime,
  //       endTime: shift.endTime,
  //       shift: (index + 1).toString(),
  //       reservationId: params.id,
  //       days: shift.days,
  //       status: "complete",
  //     };
  //     try {
  //       setLoader({ isActive: true });
  //       await user_service.configureSchedulePost(userData).then((response) => {
  //         if (response) {
  //           setLoader({ isActive: false });
  //           setToaster({
  //             type: "Add Schedule",
  //             isShow: true,
  //             toasterBody: response.data.message,
  //             message: "Schedule Add Successfully",
  //           });
  //           configureScheduleGetAllData();
  //           setTimeout(() => {
  //             setToaster((prevToaster) => ({
  //               ...prevToaster,
  //               isShow: false,
  //             }));
  //             // navigate(`/reservation-listing`);
  //           }, 1000);
  //         } else {
  //           setLoader({ isActive: false });
  //           setToaster({
  //             types: "error",
  //             isShow: true,
  //             toasterBody: response.data.message,
  //             message: "Error",
  //           });
  //         }
  //       });
  //     } catch (error) {
  //       setLoader({ isActive: false });
  //       setToaster({
  //         types: "error",
  //         isShow: true,
  //         toasterBody: error.response
  //           ? error.response.data.message
  //           : error.message,
  //         message: "Error",
  //       });
  //       setTimeout(() => {
  //         setToaster((prevToaster) => ({ ...prevToaster, isShow: false }));
  //       }, 2000);
  //     }
  //   });
  // };

 
  const handleSubmit = async (e) => {
    e.preventDefault();
  
    const newShifts = shifts.filter(
      (shift) =>
        !initialShifts.some(
          (initialShift) =>
            initialShift.startTime === shift.startTime &&
            initialShift.endTime === shift.endTime &&
            initialShift.days.length === shift.days.length &&
            initialShift.days.every((day) => shift.days.includes(day))
        )
    );
  
    for (const [index, shift] of newShifts.entries()) {
      const userData = {
        agentId: jwt(localStorage.getItem("auth")).id,
        startTime: shift.startTime,
        endTime: shift.endTime,
        shift: (index + 1).toString(),
        reservationId: params.id,
        days: shift.days,
        status: "complete",
      };
      try {
        setLoader({ isActive: true });
        const response = await user_service.configureSchedulePost(userData);
        if (response) {
          setLoader({ isActive: false });
          setToaster({
            type: "Add Schedule",
            isShow: true,
            toasterBody: response.data.message,
            message: "Schedule Added Successfully",
          });
          configureScheduleGetAllData();
          setTimeout(() => {
            setToaster((prevToaster) => ({
              ...prevToaster,
              isShow: false,
            }));
          }, 1000);
        } else {
          setLoader({ isActive: false });
          setToaster({
            types: "error",
            isShow: true,
            toasterBody: response.data.message,
            message: "Error",
          });
        }
      } catch (error) {
        setLoader({ isActive: false });
        setToaster({
          types: "error",
          isShow: true,
          toasterBody: error.response
            ? error.response.data.message
            : error.message,
          message: "Error",
        });
        setTimeout(() => {
          setToaster((prevToaster) => ({ ...prevToaster, isShow: false }));
        }, 2000);
      }
    }
  };
  
  const handleUpdate = async (id) => {
    console.log(id);
    const shiftToUpdate = shifts.find((shift) => shift._id === id);
    if (!shiftToUpdate) return;
  
    const userData = {
      agentId: jwt(localStorage.getItem("auth")).id,
      startTime: shiftToUpdate.startTime,
      endTime: shiftToUpdate.endTime,
      shift: shiftToUpdate.shift,
      reservationId: params.id,
      days: shiftToUpdate.days,
      status: "complete",
    };
     console.log(userData);
    try {
      setLoader({ isActive: true });
      const response = await user_service.configureScheduleUpdate(id, userData);
      if (response) {
        setLoader({ isActive: false });
        setToaster({
          type: "Update Schedule",
          isShow: true,
          toasterBody: response.data.message,
          message: "Schedule Updated Successfully",
        });
        configureScheduleGetAllData();
        setTimeout(() => {
          setToaster((prevToaster) => ({
            ...prevToaster,
            isShow: false,
          }));
          navigate(`/reservation-calender/${params.id}`)
        }, 1000);
      } else {
        setLoader({ isActive: false });
        setToaster({
          types: "error",
          isShow: true,
          toasterBody: response.data.message,
          message: "Error",
        });
      }
    } catch (error) {
      setLoader({ isActive: false });
      setToaster({
        types: "error",
        isShow: true,
        toasterBody: error.response
          ? error.response.data.message
          : error.message,
        message: "Error",
      });
      setTimeout(() => {
        setToaster((prevToaster) => ({ ...prevToaster, isShow: false }));
      }, 2000);
    }
  };
  
  useEffect(() => {
    window.scrollTo(0, 0);
    setLoader({ isActive: true });
    configureScheduleGetAllData();
  }, []);

  const configureScheduleGetAllData = async () => {
    await user_service.configureScheduleGetAll(params.id).then((response) => {
      setLoader({ isActive: false });
      if (response) {
        console.log(response);
        setShifts(response.data.data);
        if (response) {
          setInitialShifts(response.data.data);
        }
      }
    });
  };

  const handleCancel = () => {
    navigate(`/reservation-calender/${params.id}`);
  };

  return (
    <>
      <div className="bg-secondary float-left w-100 pt-4 mb-4">
        <Loader isActive={isActive} />
        {isShow && (
          <Toaster
            types={types}
            isShow={isShow}
            toasterBody={toasterBody}
            message={message}
          />
        )}
        <main className="page-wrapper">
          <div className="d-flex align-items-center justify-content-start mb-4">
            <h3 className="text-white mb-0">Configure Schedule</h3>
          </div>

          <div className="bg-light border rounded-3 p-3 mb-2">
            <h6>Manage the times available for this calendar.</h6>
            <div>
              {shifts.map((shift, index) => (
                <div key={index} className="row configure_schedule rounded-3 m-0 mb-4 p-3">
                  <h6>Shift {index + 1}</h6>
                  <div className="col-md-4">
                    <div className="row">
                      <div className="col-md-6">
                        <label
                          className="col-form-label"
                          htmlFor={`start-time-${index}`}
                        >
                          Begin
                        </label>
                        <select
                          className="form-select"
                          id={`start-time-${index}`}
                          name="startTime"
                          value={shift.startTime}
                          onChange={(e) => handleChange(e, index)}
                        >
                          <option value="">Select time</option>
                          {/* Generate time options */}
                          {Array.from({ length: 96 }).map((_, i) => {
                            const hours = Math.floor(i / 4);
                            const minutes = (i % 4) * 15;
                            const time = `${hours % 12 || 12}:${minutes
                              .toString()
                              .padStart(2, "0")} ${hours < 12 ? "AM" : "PM"}`;
                            return (
                              <option key={i} value={time}>
                                {time}
                              </option>
                            );
                          })}
                        </select>
                      </div>
                      <div className="col-md-6">
                        <label
                          className="col-form-label"
                          htmlFor={`end-time-${index}`}>
                          End
                        </label>
                        <select
                          className="form-select"
                          id={`end-time-${index}`}
                          name="endTime"
                          value={shift.endTime}
                          onChange={(e) => handleChange(e, index)}
                        >
                          <option value="">Select time</option>
                          {/* Generate time options */}
                          {Array.from({ length: 96 }).map((_, i) => {
                            const hours = Math.floor(i / 4);
                            const minutes = (i % 4) * 15;
                            const time = `${hours % 12 || 12}:${minutes
                              .toString()
                              .padStart(2, "0")} ${hours < 12 ? "AM" : "PM"}`;
                            return (
                              <option key={i} value={time}>
                                {time}
                              </option>
                            );
                          })}
                        </select>
                      </div>
                    </div>
                  </div>
                  <div className="col-md-8">
                    <label className="col-form-label" htmlFor="days">
                      Every
                    </label>

                    <div className="d-lg-flex d-md-flex d-lg-block d-block">
                      {[
                        "sunday",
                        "monday",
                        "tuesday",
                        "wednesday",
                        "thursday",
                        "friday",
                        "saturday",
                      ].map((day, idx) => (
                        <div className="form-check ms-lg-3 ms-md-3 ms-sm-0 ms-0" key={idx}>
                          <input
                            className="form-check-input"
                            type="checkbox"
                            name={`days-${index}`}
                            value={day}
                            checked={shift.days.includes(day)}
                            onChange={(e) => handleDayChange(e, index)}
                          />
                          <label
                            className="form-check-label"
                            htmlFor={`days-${index}`}
                          >
                            {day.charAt(0).toUpperCase() + day.slice(1, 3)}
                          </label>
                        </div>
                      ))}
                    </div>
                    <div className="mb-3">
                      {shift._id ? (
                        <button
                          className="btn btn-primary pull-right ms-5"
                          onClick={() => handleUpdate(shift._id)}
                        >
                          Update
                        </button>
                      ) : (
                        <button
                          className="btn btn-primary  ms-3  pull-right"
                          type="button"
                          onClick={handleSubmit}
                        >
                          Submit Shift
                        </button>
                      )}
                    </div>
                  </div>
                </div>
              ))}
            </div>

            <div className="pull-right">
              <button
                className="btn btn-secondary pull-right"
                type="button"
                onClick={handleCancel}
              >
                Cancel
              </button>
            </div>
            {shifts.length < 24 ? (
              <button className="btn btn-primary" onClick={handleAddShift}>
                Add Shift
              </button>
            ) : (
              ""
            )}
          </div>
        </main>
      </div>
    </>
  );
}
export default ConfigureSchedule;

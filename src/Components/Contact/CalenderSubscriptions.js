import React, { useState, useEffect } from "react";
import { NavLink, useNavigate, useParams } from "react-router-dom";
import user_service from "../service/user_service";
import Loader from "../../Pages/Loader/Loader.js";
import Toaster from "../../Pages/Toaster/Toaster.js";
import jwt from "jwt-decode";
import Pic from "../img/pic.png";
import axios from "axios";
import ICAL from "ical.js";

const CalenderSubscriptions = () => {
  const params = useParams();

  const initialValues = {
    email: "",
    image: "",
    phone: "",
    firstName: "",
    lastName: "",
    nickname: "",
    address: "",
    web: "",
    fax: "",
    inbox: "",
    organization: "",
    homeOffice: "",
    title: "",
    agentId: "",
    profession: "",
    industry: "",
    assistant: "",
    designations: "",
    primary_areas: "",
    languages: "",
    specialties: "",
    marketing_message: "",
    birthday: "",
    gender: "",
    anniversary: "",
    recruiter: "",
    supporters: "",
    connection: "",
    notes: "",
  };

  const [getContact, setGetContact] = useState(initialValues);
  const [activeButton, setActiveButton] = useState("1");
  const [step1, setStep1] = useState("1");
  const [submitCount, setSubmitCount] = useState(0);

  const [file, setFile] = useState(null);
  const [fileExtension, setFileExtension] = useState("");
  const [acceptedFileTypes, setAcceptedFileTypes] = useState([]);
  const [showSubmitButton, setShowSubmitButton] = useState(false);

  const [board_membership, setBoard_membership] = useState([]);
  const [mls_membership, setMls_membership] = useState([]);

  const [note, setNotes] = useState(false);
  const navigate = useNavigate();

  const [notesdata, setNotesdata] = useState("");
  const [contactListings, setContactListings] = useState("");
  const [commaSeparatedValues, setCommaSeparatedValues] = useState("");
  const [
    commaSeparatedValuesmls_membership,
    setCommaSeparatedValuesmls_membership,
  ] = useState("");

  const currentDate = new Date();
  const formattedDate = `${
    currentDate.getMonth() + 1
  }/${currentDate.getDate()}/${currentDate.getFullYear()}`;

  const initialUserPersonal = {
    agentId: "",
    personal: [
      {
        // subscriptionURL: `http://localhost:3000/calendersubscriptions/${
        //   jwt(localStorage.getItem("auth")).id
        // }/personal`,
        // subscriptionURL: `https://brokeragentbase.com/calendersubscriptions/${jwt(localStorage.getItem("auth")).id}/personal`,
        subscriptionURL: `https://api.brokeragentbase.com/calender/subscriptions?agentId=${jwt(localStorage.getItem("auth")).id}&category=Personal`,
        calenderURL: ``,
        subscriptiondate: formattedDate,
        synceddate: "",
      },
    ],
    is_active: "",
    created: "",
  };

  const initialUserOffice = {
    agentId: "",

    office: [
      {
        // subscriptionURL: `http://localhost:3000/calendersubscriptions/${
        //   jwt(localStorage.getItem("auth")).id
        // }/office`,
        // subscriptionURL: `https://brokeragentbase.com//calendersubscriptions/${jwt(localStorage.getItem("auth")).id}/office`,
        subscriptionURL: `https://api.brokeragentbase.com/calender/subscriptions?agentId=${jwt(localStorage.getItem("auth")).id}&category=Office`,
        calenderURL: ``,
        subscriptiondate: formattedDate,
        synceddate: "",
      },
    ],

    is_active: "",
    created: "",
  };

  const initialUserStaff = {
    agentId: "",
    staff: [
      {
        // subscriptionURL: `http://localhost:3000/calendersubscriptions/${
        //   jwt(localStorage.getItem("auth")).id
        // }/staff`,
        // subscriptionURL: `https://brokeragentbase.com/calendersubscriptions/${jwt(localStorage.getItem("auth")).id}/staff`,
        subscriptionURL: `https://api.brokeragentbase.com/calender/subscriptions?agentId=${jwt(localStorage.getItem("auth")).id}&category=Staff`,
        calenderURL: ``,
        subscriptiondate: formattedDate,
        synceddate: "",
      },
    ],

    is_active: "",
    created: "",
  };

  const initialUserPublic = {
    agentId: "",
    public: [
      {
        // subscriptionURL: `http://localhost:3000/calendersubscriptions/${
        //   jwt(localStorage.getItem("auth")).id
        // }/public`,
        // subscriptionURL: `https://brokeragentbase.com/calendersubscriptions/${jwt(localStorage.getItem("auth")).id}/public`,
        subscriptionURL: `https://api.brokeragentbase.com/calender/subscriptions?agentId=${jwt(localStorage.getItem("auth")).id}&category=Public`,
        calenderURL: ``,
        subscriptiondate: formattedDate,
        synceddate: "",
      },
    ],
    is_active: "",
    created: "",
  };
  const [loader, setLoader] = useState({ isActive: null });
  const { isActive } = loader;
  const [toaster, setToaster] = useState({
    types: null,
    isShow: null,
    toasterBody: "",
    message: "",
  });
  const { types, isShow, toasterBody, message } = toaster;
  const [respoceData, setResonceData] = useState("");

  const [calenderData, setCalenderData] = useState("");
  const [data, setData] = useState("");
  const [imageData, setImageData] = useState("");

  const [formValues, setFormValues] = useState(initialUserPersonal);
  const [officeData, setOfficeData] = useState(initialUserOffice);

  const [staffData, setStaffData] = useState(initialUserStaff);
  const [publicData, setPublicData] = useState(initialUserPublic);

  const handlePersonal = async (e, type) => {
    try {
      setLoader({ isActive: true });
      let queryParams = "";
      // Fetch calendar events based on the query parameters
      if (jwt(localStorage.getItem("auth")).id) {
        queryParams += `&category=Personal&agentId=${jwt(localStorage.getItem("auth")).id}`;
      }
      const response = await user_service.CalenderSubscriptionURL(queryParams);
      setLoader({ isActive: false });

      if (response) {
        console.log("response", response.data.url)

        formValues.personal = formValues.personal.map((item, index) =>
          index === 0 ? { ...item, calenderURL: response.data.url } : item
        );

        console.log(formValues, "formValues")

        if (calenderData && calenderData._id) {
          if (type) {
            console.log(type);
            console.log("hhhhhh");
      
            e.preventDefault();
            const userData = {
              agentId: jwt(localStorage.getItem("auth")).id,
              personal: formValues.personal,
              // office: formValues.office,
              // staff: formValues.staff,
              // public: formValues.public,
              is_active: "yes",
              created: "8-9-2023",
            };
            console.log("Personal Data:", userData);
            setLoader({ isActive: true });
            try {
              await user_service
                .calenderSubscriptionUpdate(calenderData._id, userData)
                .then((response) => {
                  if (response) {
                    setFormValues(response.data);
                    setLoader({ isActive: false });
                    calenderSubscriptionGetApi();
                    setToaster({
                      type: "Personal Update Successfully",
                      isShow: true,
                      toasterBody: response.data.message,
                      message: "Personal Update Successfully",
                    });
                    setTimeout(() => {
                      setToaster((prevToaster) => ({
                        ...prevToaster,
                        isShow: false,
                      }));
                    }, 500);
                  } else {
                    setLoader({ isActive: false });
                    setToaster({
                      types: "error",
                      isShow: true,
                      toasterBody: response.data.message,
                      message: "Error",
                    });
                  }
                });
            } catch (error) {
              setLoader({ isActive: false });
              setToaster({
                types: "error",
                isShow: true,
                toasterBody: error.response
                  ? error.response.data.message
                  : error.message,
                message: "Error",
              });
              setTimeout(() => {
                setToaster((prevToaster) => ({ ...prevToaster, isShow: false }));
              }, 2000);
            }
          }
        } else {
          if (type) {
            e.preventDefault();
            const userData = {
              agentId: jwt(localStorage.getItem("auth")).id,
              personal: formValues.personal,
              // office: formValues.office,
              // staff: formValues.staff,
              // public: formValues.public,
              is_active: "yes",
              created: "8-9-2023",
            };
            console.log("Personal Data:", userData);
            setLoader({ isActive: true });
            try {
              await user_service
                .calenderSubscriptionPost(userData)
                .then((response) => {
                  if (response) {
                    setFormValues(response.data);
                    setLoader({ isActive: false });
                    calenderSubscriptionGetApi();
                    setToaster({
                      type: "Personal Post Successfully",
                      isShow: true,
                      toasterBody: response.data.message,
                      message: "Personal Post Successfully",
                    });
                    setTimeout(() => {
                      setToaster((prevToaster) => ({
                        ...prevToaster,
                        isShow: false,
                      }));
                    }, 500);
                  } else {
                    setLoader({ isActive: false });
                    setToaster({
                      types: "error",
                      isShow: true,
                      toasterBody: response.data.message,
                      message: "Error",
                    });
                  }
                });
            } catch (error) {
              setLoader({ isActive: false });
              setToaster({
                types: "error",
                isShow: true,
                toasterBody: error.response
                  ? error.response.data.message
                  : error.message,
                message: "Error",
              });
              setTimeout(() => {
                setToaster((prevToaster) => ({ ...prevToaster, isShow: false }));
              }, 2000);
            }
          }
        }
      }
    } catch (error) {
      console.error("Error in fetchCalendar:", error);
    }
    // http://localhost:4000/calender/subscriptions?agentId=657ab2ff749efe2ad688274b&category=Personal

  };

  const handleOffice = async (e, type) => {
    
    // upload a calender ics file on AWS and return the path and save that path into this .
    try {
      setLoader({ isActive: true });
      let queryParams = "";
      // Fetch calendar events based on the query parameters
      if (jwt(localStorage.getItem("auth")).id) {
        queryParams += `&category=Office&agentId=${jwt(localStorage.getItem("auth")).id}`;
      }
      const response = await user_service.CalenderSubscriptionURL(queryParams);
      setLoader({ isActive: false });

      if (response) {
        console.log("response", response.data.url)

        officeData.office = officeData.office.map((item, index) =>
          index === 0 ? { ...item, calenderURL: response.data.url } : item
        );

        console.log(officeData, "officeData")

          if (calenderData && calenderData._id) {
            if (type) {
              e.preventDefault();

              const userData = {
                agentId: jwt(localStorage.getItem("auth")).id,
                //personal: officeData.personal,
                office: officeData.office,
                // staff: officeData.staff,
                // public: officeData.public,
                is_active: "yes",
                created: "8/9/2023",
              };

              console.log("office Data:", userData);
              setLoader({ isActive: true });
              try {
                await user_service
                  .calenderSubscriptionUpdate(calenderData._id, userData)
                  .then((response) => {
                    if (response) {
                      setOfficeData(response.data);
                      setLoader({ isActive: false });
                      calenderSubscriptionGetApi();
                      setToaster({
                        type: "Office Post Successfully",
                        isShow: true,
                        toasterBody: response.data.message,
                        message: "Office Post Successfully",
                      });
                      setTimeout(() => {
                        setToaster((prevToaster) => ({
                          ...prevToaster,
                          isShow: false,
                        }));
                      }, 500);
                    } else {
                      setLoader({ isActive: false });
                      setToaster({
                        types: "error",
                        isShow: true,
                        toasterBody: response.data.message,
                        message: "Error",
                      });
                    }
                  });
              } catch (error) {
                setLoader({ isActive: false });
                setToaster({
                  types: "error",
                  isShow: true,
                  toasterBody: error.response
                    ? error.response.data.message
                    : error.message,
                  message: "Error",
                });
                setTimeout(() => {
                  setToaster((prevToaster) => ({ ...prevToaster, isShow: false }));
                }, 2000);
              }
            }
          } else {
            if (type) {
              e.preventDefault();

              const userData = {
                agentId: jwt(localStorage.getItem("auth")).id,
                personal: officeData.personal,
                office: officeData.office,
                staff: officeData.staff,
                public: officeData.public,
                is_active: "yes",
                created: "8/9/2023",
              };

              console.log("office Data:", userData);
              setLoader({ isActive: true });
              try {
                await user_service
                  .calenderSubscriptionPost(userData)
                  .then((response) => {
                    if (response) {
                      setOfficeData(response.data);
                      setLoader({ isActive: false });
                      calenderSubscriptionGetApi();
                      setToaster({
                        type: "Office Post Successfully",
                        isShow: true,
                        toasterBody: response.data.message,
                        message: "Office Post Successfully",
                      });
                      setTimeout(() => {
                        setToaster((prevToaster) => ({
                          ...prevToaster,
                          isShow: false,
                        }));
                      }, 500);
                    } else {
                      setLoader({ isActive: false });
                      setToaster({
                        types: "error",
                        isShow: true,
                        toasterBody: response.data.message,
                        message: "Error",
                      });
                    }
                  });
              } catch (error) {
                setLoader({ isActive: false });
                setToaster({
                  types: "error",
                  isShow: true,
                  toasterBody: error.response
                    ? error.response.data.message
                    : error.message,
                  message: "Error",
                });
                setTimeout(() => {
                  setToaster((prevToaster) => ({ ...prevToaster, isShow: false }));
                }, 2000);
              }
            }
          }
        }
      } catch (error) {
        console.error("Error in fetchCalendar:", error);
      }
  };

  const handleStaff = async (e, type) => {
    // upload a calender ics file on AWS and return the path and save that path into this .
    try {
      setLoader({ isActive: true });
      let queryParams = "";
      // Fetch calendar events based on the query parameters
      if (jwt(localStorage.getItem("auth")).id) {
        queryParams += `&category=Staff&agentId=${jwt(localStorage.getItem("auth")).id}`;
      }
      const response = await user_service.CalenderSubscriptionURL(queryParams);
      setLoader({ isActive: false });

      if (response) {
        console.log("response", response.data.url)

        staffData.staff = staffData.staff.map((item, index) =>
          index === 0 ? { ...item, calenderURL: response.data.url } : item
        );

        console.log(staffData, "staffData")


          if (calenderData && calenderData._id) {
            if (type) {
              e.preventDefault();
              const userData = {
                agentId: jwt(localStorage.getItem("auth")).id,

                // personal: staffData.personal,
                // office: staffData.office,
                staff: staffData.staff,
                // public: staffData.public,
                is_active: "yes",
                created: "8/9/2023",
              };
              console.log("staff Data:", userData);

              setLoader({ isActive: true });
              try {
                await user_service
                  .calenderSubscriptionUpdate(calenderData._id, userData)
                  .then((response) => {
                    if (response) {
                      setStaffData(response.data);
                      setLoader({ isActive: false });
                      calenderSubscriptionGetApi();
                      setToaster({
                        type: "Staff Post Successfully",
                        isShow: true,
                        toasterBody: response.data.message,
                        message: "Staff Post Successfully",
                      });
                      setTimeout(() => {
                        setToaster((prevToaster) => ({
                          ...prevToaster,
                          isShow: false,
                        }));
                      }, 500);
                    } else {
                      setLoader({ isActive: false });
                      setToaster({
                        types: "error",
                        isShow: true,
                        toasterBody: response.data.message,
                        message: "Error",
                      });
                    }
                  });
              } catch (error) {
                setLoader({ isActive: false });
                setToaster({
                  types: "error",
                  isShow: true,
                  toasterBody: error.response
                    ? error.response.data.message
                    : error.message,
                  message: "Error",
                });
                setTimeout(() => {
                  setToaster((prevToaster) => ({ ...prevToaster, isShow: false }));
                }, 2000);
              }
            }
          } else {
            if (type) {
              e.preventDefault();
              const userData = {
                agentId: jwt(localStorage.getItem("auth")).id,

                // personal: staffData.personal,
                // office: staffData.office,
                staff: staffData.staff,
                // public: staffData.public,
                is_active: "yes",
                created: "8/9/2023",
              };
              console.log("staff Data:", userData);

              setLoader({ isActive: true });
              try {
                await user_service
                  .calenderSubscriptionPost(userData)
                  .then((response) => {
                    if (response) {
                      setStaffData(response.data);
                      setLoader({ isActive: false });
                      calenderSubscriptionGetApi();
                      setToaster({
                        type: "Staff Post Successfully",
                        isShow: true,
                        toasterBody: response.data.message,
                        message: "Staff Post Successfully",
                      });
                      setTimeout(() => {
                        setToaster((prevToaster) => ({
                          ...prevToaster,
                          isShow: false,
                        }));
                      }, 500);
                    } else {
                      setLoader({ isActive: false });
                      setToaster({
                        types: "error",
                        isShow: true,
                        toasterBody: response.data.message,
                        message: "Error",
                      });
                    }
                  });
              } catch (error) {
                setLoader({ isActive: false });
                setToaster({
                  types: "error",
                  isShow: true,
                  toasterBody: error.response
                    ? error.response.data.message
                    : error.message,
                  message: "Error",
                });
                setTimeout(() => {
                  setToaster((prevToaster) => ({ ...prevToaster, isShow: false }));
                }, 2000);
              }
            }
          }
        }
      } catch (error) {
        console.error("Error in fetchCalendar:", error);
      }
  };

  const handlePublic = async (e, type) => {
    // upload a calender ics file on AWS and return the path and save that path into this .
    try {
      setLoader({ isActive: true });
      let queryParams = "";
      // Fetch calendar events based on the query parameters
      if (jwt(localStorage.getItem("auth")).id) {
        queryParams += `&category=Public&agentId=${jwt(localStorage.getItem("auth")).id}`;
      }
      const response = await user_service.CalenderSubscriptionURL(queryParams);
      setLoader({ isActive: false });

      if (response) {
        console.log("response", response.data.url)

        publicData.public = publicData.public.map((item, index) =>
          index === 0 ? { ...item, calenderURL: response.data.url } : item
        );

        console.log(publicData, "publicData")


          if (calenderData && calenderData._id) {
          if (type) {
            e.preventDefault();

            const userData = {
              agentId: jwt(localStorage.getItem("auth")).id,

              // personal: publicData.personal,
              // office: publicData.office,
              // staff: publicData.staff,
              public: publicData.public,
              is_active: "yes",
              created: "8/9/2023",
            };

            console.log("public Data:", userData);
            setLoader({ isActive: true });
            try {
              await user_service
                .calenderSubscriptionUpdate(calenderData._id, userData)
                .then((response) => {
                  if (response) {
                    setPublicData(response.data);
                    setLoader({ isActive: false });

                    calenderSubscriptionGetApi();
                    setToaster({
                      type: "Public Post Successfully",
                      isShow: true,
                      toasterBody: response.data.message,
                      message: "Public Post Successfully",
                    });
                    setTimeout(() => {
                      setToaster((prevToaster) => ({
                        ...prevToaster,
                        isShow: false,
                      }));
                    }, 500);
                  } else {
                    setLoader({ isActive: false });
                    setToaster({
                      types: "error",
                      isShow: true,
                      toasterBody: response.data.message,
                      message: "Error",
                    });
                  }
                });
            } catch (error) {
              setLoader({ isActive: false });
              setToaster({
                types: "error",
                isShow: true,
                toasterBody: error.response
                  ? error.response.data.message
                  : error.message,
                message: "Error",
              });
              setTimeout(() => {
                setToaster((prevToaster) => ({ ...prevToaster, isShow: false }));
              }, 2000);
            }
          }
          } else {
            if (type) {
              e.preventDefault();

              const userData = {
                agentId: jwt(localStorage.getItem("auth")).id,

                // personal: publicData.personal,
                // office: publicData.office,
                // staff: publicData.staff,
                public: publicData.public,
                is_active: "yes",
                created: "8/9/2023",
              };

              console.log("public Data:", userData);
              setLoader({ isActive: true });
              try {
                await user_service
                  .calenderSubscriptionPost(userData)
                  .then((response) => {
                    if (response) {
                      setPublicData(response.data);
                      setLoader({ isActive: false });

                      calenderSubscriptionGetApi();
                      setToaster({
                        type: "Public Post Successfully",
                        isShow: true,
                        toasterBody: response.data.message,
                        message: "Public Post Successfully",
                      });
                      setTimeout(() => {
                        setToaster((prevToaster) => ({
                          ...prevToaster,
                          isShow: false,
                        }));
                      }, 500);
                    } else {
                      setLoader({ isActive: false });
                      setToaster({
                        types: "error",
                        isShow: true,
                        toasterBody: response.data.message,
                        message: "Error",
                      });
                    }
                  });
              } catch (error) {
                setLoader({ isActive: false });
                setToaster({
                  types: "error",
                  isShow: true,
                  toasterBody: error.response
                    ? error.response.data.message
                    : error.message,
                  message: "Error",
                });
                setTimeout(() => {
                  setToaster((prevToaster) => ({ ...prevToaster, isShow: false }));
                }, 2000);
              }
            }
          }
        }
      } catch (error) {
        console.error("Error in fetchCalendar:", error);
      }
  };

  const profileGetAll = async () => {
    const agentId = jwt(localStorage.getItem("auth")).id;
    setLoader({ isActive: true });
    await user_service.profileGet(agentId).then((response) => {
      if (response) {
        setLoader({ isActive: false });
        setResonceData(response.data);
      }
    });
  };

  const calenderSubscriptionGetApi = async () => {
    const agentId = jwt(localStorage.getItem("auth")).id;
    setLoader({ isActive: true });
    await user_service.calenderSubscriptionGet(agentId).then((response) => {
      if (response) {
        setLoader({ isActive: false });
        const ResponceData = response.data;
        setCalenderData(ResponceData.data[0]);
        // setCalenderDataOffice(ResponceData.data[1].office);
        // setCalenderDataStaff(ResponceData.data[2].staff);
        // setCalenderDataPublic(ResponceData.data[3].public);
      }
    });
  };
  useEffect(() => {
    window.scrollTo(0, 0);
    profileGetAll();
    calenderSubscriptionGetApi();
    ContactGetById(params.id);
  }, []);

  const handleBack = () => {
    navigate(`/contact-profile/${jwt(localStorage.getItem("auth")).id}`);
  };

  const handlegetContact = (stepNo) => {
    if (stepNo === "1") {
      setStep1(stepNo);
      setActiveButton(stepNo);
    }

    if (stepNo === "2") {
      setStep1(stepNo);
      setNotes(false);
      setActiveButton(stepNo);
    }
    if (stepNo === "3") {
      setStep1(stepNo);
      setActiveButton(stepNo);
    }
    if (stepNo === "4") {
      setStep1(stepNo);
      setActiveButton(stepNo);
      getContactListings();
    }
    if (stepNo === "5") {
      setStep1(stepNo);
      setActiveButton(stepNo);
    }

    // if (stepNo === "6") {
    //   setStep1(stepNo);
    // }

    if (stepNo === "7") {
      setStep1(stepNo);
      setActiveButton(stepNo);
    }

    if (stepNo === "8") {
      setStep1(stepNo);
      setActiveButton(stepNo);
    }

    if (stepNo === "9") {
      setStep1(stepNo);
      setActiveButton(stepNo);
    }
  };

  const ContactGetById = async () => {
    await user_service.contactGetById(params.id).then((response) => {
      if (response) {
        setGetContact(response.data);

        if (response.data && response.data.additionalActivateFields) {
          setFormValues((prevFormValues) => ({
            ...prevFormValues,
            office:
              response.data.additionalActivateFields.office ||
              prevFormValues.office,
            intranet_id:
              response.data.additionalActivateFields.intranet_id ||
              prevFormValues.intranet_id,
            subscription_level:
              response.data.additionalActivateFields.subscription_level ||
              prevFormValues.subscription_level,
            admin_note:
              response.data.additionalActivateFields.admin_note ||
              prevFormValues.admin_note,
            sponsor_associate:
              response.data.additionalActivateFields.sponsor_associate ||
              prevFormValues.sponsor_associate,
            staff_recruiter:
              response.data.additionalActivateFields.staff_recruiter ||
              prevFormValues.staff_recruiter,
            nrds_id:
              response.data.additionalActivateFields.nrds_id ||
              prevFormValues.nrds_id,
            associate_member:
              response.data.additionalActivateFields.associate_member ||
              prevFormValues.associate_member,
            plan_date:
              response.data.additionalActivateFields.plan_date ||
              prevFormValues.plan_date,
            billing_date:
              response.data.additionalActivateFields.billing_date ||
              prevFormValues.billing_date,
            account_name:
              response.data.additionalActivateFields.account_name ||
              prevFormValues.account_name,
            invoicing:
              response.data.additionalActivateFields.invoicing ||
              prevFormValues.invoicing,
            licensed_since:
              response.data.additionalActivateFields.licensed_since ||
              prevFormValues.licensed_since,
            associate_since:
              response.data.additionalActivateFields.associate_since ||
              prevFormValues.associate_since,
            staff_since:
              response.data.additionalActivateFields.staff_since ||
              prevFormValues.staff_since,
            iscorporation:
              response.data.additionalActivateFields.iscorporation ||
              prevFormValues.iscorporation,
            agentTax_ein:
              response.data.additionalActivateFields.agentTax_ein ||
              prevFormValues.agentTax_ein,
            agent_funding:
              response.data.additionalActivateFields.agent_funding ||
              prevFormValues.agent_funding,
            ssn_itin:
              response.data.additionalActivateFields.ssn_itin ||
              prevFormValues.ssn_itin,
            date_birth:
              response.data.additionalActivateFields.date_birth ||
              prevFormValues.date_birth,
            private_idcard_type:
              response.data.additionalActivateFields.private_idcard_type ||
              prevFormValues.private_idcard_type,
            private_idstate:
              response.data.additionalActivateFields.private_idstate ||
              prevFormValues.private_idstate,
            private_idnumber:
              response.data.additionalActivateFields.private_idnumber ||
              prevFormValues.private_idnumber,
            license_state:
              response.data.additionalActivateFields.license_state ||
              prevFormValues.license_state,
            license_type:
              response.data.additionalActivateFields.license_type ||
              prevFormValues.license_type,
            license_number:
              response.data.additionalActivateFields.license_number ||
              prevFormValues.license_number,
            date_issued:
              response.data.additionalActivateFields.date_issued ||
              prevFormValues.date_issued,
            date_expire:
              response.data.additionalActivateFields.date_expire ||
              prevFormValues.date_expire,
            mls_membership:
              response.data.additionalActivateFields.mls_membership ||
              prevFormValues.mls_membership,
          }));

          // const boardMembershipData = response.data.additionalActivateFields && response.data.additionalActivateFields?.board_membership
          //     ? JSON.parse(response.data.additionalActivateFields.board_membership)
          //     : [];
          //     const boardMembershipValues = boardMembershipData.map(item => item.value);
          //     setCommaSeparatedValues(boardMembershipValues.join(', '))

          // const mls_membershipData = response.data.additionalActivateFields && response.data.additionalActivateFields?.mls_membership
          // ? JSON.parse(response.data.additionalActivateFields.mls_membership)
          // : [];
          // const mls_membershipValues = mls_membershipData.map(item => item.value);
          // setCommaSeparatedValuesmls_membership(mls_membershipValues.join(', '))

          const board_membershipString =
            response.data.additionalActivateFields?.board_membership;
          if (board_membershipString) {
            // Check if board_membershipString is a string
            if (typeof board_membershipString === "string") {
              try {
                // Attempt to parse the string as JSON
                const board_membershipParsed = JSON.parse(
                  board_membershipString
                );

                if (board_membershipParsed == "") {
                  setBoard_membership(board_membership);
                } else {
                  setBoard_membership(board_membershipParsed);
                }
                // Set the parsed value to your state or variable
                if (board_membershipParsed == "") {
                  setCommaSeparatedValues("");
                } else {
                  const boardMembershipValues = board_membershipParsed.map(
                    (item) => item.value
                  );
                  setCommaSeparatedValues(boardMembershipValues.join(", "));
                }
              } catch (error) {
                // Handle the error if parsing fails
                console.error("Error parsing board_membership as JSON:", error);
                // You might want to set a default value or handle the error in another way
                // For debugging, you can log the JSON parse error message:
                console.error("JSON Parse Error:", error.message);
                // Set a default value if parsing fails
                setCommaSeparatedValues(""); // Replace "defaultValue" with your default value
              }
            } else {
              //console.log("sdfdsfdsfsd");
              // Handle the case where board_membershipString is not a string (e.g., it's already an object)
              // You can directly set it to your state or variable
              setCommaSeparatedValues("");
            }
          } else {
            // Handle the case where board_membershipString is undefined or null
            // Set a default value or handle it as needed
            setCommaSeparatedValues(""); // Replace "defaultValue" with your default value
          }

          const mls_membershipString =
            response.data.additionalActivateFields?.mls_membership;
          if (mls_membershipString) {
            // Check if mls_membershipString is a string
            if (typeof mls_membershipString === "string") {
              try {
                // Attempt to parse the string as JSON
                const mls_membershipParsed = JSON.parse(mls_membershipString);
                // Set the parsed value to your state or variable
                if (mls_membershipParsed == "") {
                  setMls_membership(mls_membership);
                } else {
                  setMls_membership(mls_membershipParsed);
                }
                if (mls_membershipParsed == "") {
                  setCommaSeparatedValuesmls_membership("");
                } else {
                  const boardMembershipValues = mls_membershipParsed.map(
                    (item) => item.value
                  );
                  setCommaSeparatedValuesmls_membership(
                    boardMembershipValues.join(", ")
                  );
                }
              } catch (error) {
                // Handle the error if parsing fails
                console.error("Error parsing mls_membership as JSON:", error);
                // You might want to set a default value or handle the error in another way
                // For debugging, you can log the JSON parse error message:
                console.error("JSON Parse Error:", error.message);
                // Set a default value if parsing fails
                setCommaSeparatedValuesmls_membership(""); // Replace "defaultValue" with your default value
              }
            } else {
              //console.log("sdfdsfdsfsd");
              // Handle the case where mls_membershipString is not a string (e.g., it's already an object)
              // You can directly set it to your state or variable
              setCommaSeparatedValuesmls_membership("");
            }
          } else {
            // Handle the case where mls_membershipString is undefined or null
            // Set a default value or handle it as needed
            setCommaSeparatedValuesmls_membership(""); // Replace "defaultValue" with your default value
          }
        }
      }
    });
  };

  const getContactListings = async () => {
    const agentId = params.id;
    // console.log(agentId);
    await user_service.getContactListings(agentId, "count").then((response) => {
      if (response) {
        setContactListings(response.data);
      }
    });
  };

  const handleNavigate = (id = "") => {
    if (id) {
      setActiveButton(id);

      navigate(`/contact/${id}`);
    } else {
      navigate(`/contact/${params.id}`);
    }
  };

  const handleFileUpload = async (e) => {
    const selectedFile = e.target.files[0];

    if (selectedFile) {
      setFile(selectedFile);
      setFileExtension(selectedFile.name.split(".").pop());
      setShowSubmitButton(true);

      const formData = new FormData();
      formData.append("file", selectedFile);
      const config = {
        headers: {
          Accept: "application/json",
          "Content-Type": "multipart/form-data",
          Authorization: `Bearer ${localStorage.getItem("auth")}`,
        },
      };

      try {
        setLoader({ isActive: true });
        const uploadResponse = await axios.post(
          "https://api.brokeragentbase.com/upload",
          formData,
          config
        );
        const uploadedFileData = uploadResponse.data;
        setData(uploadedFileData);
        // console.log(uploadedFileData);
        const updatedgetContact = {
          ...getContact,
          image: uploadedFileData,
        };

        setLoader({ isActive: true });
        await user_service
          .contactUpdate(params.id, updatedgetContact)
          .then((response) => {
            if (response) {
              setImageData(response.data);
              const ContactGetById = async () => {
                await user_service
                  .contactGetById(params.id)
                  .then((response) => {
                    if (response) {
                      setGetContact(response.data);
                      setLoader({ isActive: false });
                      setSubmitCount(submitCount + 1);
                      setToaster({
                        type: "getContact Update",
                        isShow: true,
                        toasterBody: response.data.message,
                        message: "getContact Update Successfully",
                      });

                      setTimeout(() => {
                        setToaster((prevToaster) => ({
                          ...prevToaster,
                          isShow: false,
                        }));
                      }, 500);
                    }
                  });
              };
              ContactGetById(params.id);
            } else {
              setLoader({ isActive: false });
              setToaster({
                type: "error",
                isShow: true,
                toasterBody: response.data.message,
                message: "Error",
              });

              setTimeout(() => {
                setToaster((prevToaster) => ({
                  ...prevToaster,
                  isShow: false,
                }));
              }, 500);
            }
          });
      } catch (error) {
        console.error("Error occurred during file upload:", error);
        setLoader({ isActive: false });
      }
    }
  };

  const handleChange = (e) => {
    const { name, value } = e.target;
    setGetContact({ ...getContact, [name]: value });
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
    const userData = {
      agentId: jwt(localStorage.getItem("auth")).id,
      firstName: getContact.firstName,
      lastName: getContact.lastName,
      nickname: getContact.nickname
        ? getContact.nickname
        : getContact.lastName + "" + getContact.firstName,
      email: getContact.email,
      phone: getContact.phone,
      contactType: getContact.contactType,
      address: getContact.address,
      web: getContact.web,
      fax: getContact.fax,
      inbox: getContact.inbox,
      organization: getContact.organization,
      homeOffice: getContact.homeOffice,
      title: getContact.title,
      profession: getContact.profession,
      industry: getContact.industry,
      assistant: getContact.assistant,
      designations: getContact.designations,
      primary_areas: getContact.primary_areas,
      languages: getContact.languages,
      specialties: getContact.specialties,
      marketing_message: getContact.marketing_message,
      birthday: getContact.birthday,
      gender: getContact.gender,
      anniversary: getContact.anniversary,
      recruiter: getContact.recruiter,
      Supporters: getContact.Supporters,
      connection: getContact.connection,
      note: getContact.note,
    };

    await user_service.contactUpdate(params.id, userData).then((response) => {
      // setLoader({ isActive: true });
      if (response) {
        setGetContact(response.data);
        setLoader({ isActive: false });
        ContactGetById(params.id);
        setToaster({
          type: "Profile_Update",
          isShow: true,
          toasterBody: response.data.message,
          message: "Profile_Update Successfully",
        });
        setTimeout(() => {
          setToaster((prevToaster) => ({ ...prevToaster, isShow: false }));
        }, 1000);
      } else {
        setLoader({ isActive: false });
        setToaster({
          types: "error",
          isShow: true,
          toasterBody: response.data.message,
          message: "Error",
        });
      }
    });
  };

  const type =
    params.type === "personal"
      ? "Personal"
      : params.type === "office"
      ? "Office"
      : params.type === "staff"
      ? "Staff"
      : "";

  const publicType = params.type === "public" ? "Public" : "";
  const [copySuccess, setCopySuccess] = useState(''); // To show copy success message
  const [loading, setLoading] = useState(false);

  const [calenderGet, setCalenderGet] = useState([]);
  const [calenderMarketGet, setCalenderMarketGet] = useState([]);
  const [icalUrl, setIcalUrl] = useState("");

  const fetchCalendar = async (type) => {
    try {
      setLoading(true); // Start loader
      let queryParams;

      const authData = jwt(localStorage.getItem("auth"));
      if (authData.contactType === "admin" || authData.contactType === "staff") {
        queryParams = `?page=1`;
      } else {
        queryParams = `?page=1&agentId=${authData.id}`;
      }

      if (type) {
        queryParams += `&category=${type}`;
      }

      const response = await user_service.CalenderGet(queryParams);
      setLoading(false); // Stop loader

      if (response && response.data) {
        setCalenderGet(response.data.data);
        // After fetching, create the vCalendar (iCal format)
        createVCalendar(response.data.data);
      }
    } catch (error) {
      console.error("Error in fetchCalendar:", error);
      setLoading(false); // Stop loader on error
    }
  };


  function formatDateToICalendar(dateString) {
    const date = new Date(dateString);

    if (isNaN(date)) {
      return "00000000T000000Z"; // Return a default value for invalid dates
    }

    const year = date.getFullYear();
    const month = String(date.getMonth() + 1).padStart(2, "0"); // Add leading zero
    const day = String(date.getDate()).padStart(2, "0"); // Add leading zero
    const hours = String(date.getHours()).padStart(2, "0"); // Add leading zero
    const minutes = String(date.getMinutes()).padStart(2, "0"); // Add leading zero
    const seconds = String(date.getSeconds()).padStart(2, "0"); // Add leading zero

    return `${year}${month}${day}T${hours}${minutes}${seconds}Z`;
  }
  const createVCalendar = (events) => {
    const vcalendar = new ICAL.Component(['vcalendar', [], []]);

    vcalendar.addPropertyWithValue('VERSION', '2.0');
    vcalendar.addPropertyWithValue('CALSCALE', 'GREGORIAN');
    vcalendar.addPropertyWithValue('X-WR-CALNAME', `${type}`);

    events.forEach((eventData) => {
      const vevent = new ICAL.Component(['vevent', [], []]);

      const dtStartFormatted = formatDateToICalendar(eventData.startDate);
      const dtEndFormatted = formatDateToICalendar(eventData.endDate);
      const dtStampFormatted = formatDateToICalendar(new Date().toISOString()); // Current time

      vevent.addPropertyWithValue('UID', `ID_${eventData._id}`);
      vevent.addPropertyWithValue('SUMMARY', eventData.title);
      vevent.addPropertyWithValue('ORGANIZER', eventData.organizer || '');
      vevent.addPropertyWithValue('LOCATION', eventData.venue || '');
      vevent.addPropertyWithValue('DTSTART', dtStartFormatted);
      vevent.addPropertyWithValue('DTEND', dtEndFormatted);
      vevent.addPropertyWithValue('DTSTAMP', dtStampFormatted);
      vevent.addPropertyWithValue('DESCRIPTION', eventData.eventDescription || '');

      vcalendar.addSubcomponent(vevent);
    });

    const blob = new Blob([vcalendar.toString()], { type: 'text/calendar' });
    const url = URL.createObjectURL(blob);
    setIcalUrl(url); // Set the iCal URL to state so it can be copied
  };
  const copyToClipboard = async (url) => {
    if (!navigator.clipboard) {
      console.error("Clipboard API not supported in this browser.");
      setCopySuccess("Clipboard API not supported. Please copy manually.");
      return;
    }
  
    try {
      // Copy the URL to the clipboard
      await navigator.clipboard.writeText(url);
      setCopySuccess("URL copied to clipboard! Paste it in Google Calendar.");
      console.log("Copied URL:", url); // Optional: For debugging
      setToaster({
        type: "success",
        isShow: true,
        toasterBody: "URL copied to clipboard! Paste it in Google Calendar.",
        message: "Success",
      });
      setTimeout(() => {
        setToaster((prevToaster) => ({
          ...prevToaster,
          isShow: false,
        }));
      }, 2000);

    } catch (error) {
      console.error("Failed to copy URL:", error);
      setCopySuccess("Failed to copy the URL. Please try again.");
      setToaster({
        type: "error",
        isShow: true,
        toasterBody: "Failed to copy the URL. Please try again.",
        message: "error",
      });
      setTimeout(() => {
        setToaster((prevToaster) => ({
          ...prevToaster,
          isShow: false,
        }));
      }, 2000);
    }
  };


  return (
    <div className="bg-secondary float-left w-100 pt-4 mb-4">
      <Loader isActive={isActive} />
      {isShow && (
        <Toaster
          types={types}
          isShow={isShow}
          toasterBody={toasterBody}
          message={message}
        />
      )}
      <main className="page-wrapper profile_page_wrap">
        <div className="content-overlay">
          <div className="d-flex align-items-center justify-content-between mb-4">
            <h3 className="nav-item text-white" id="profile">
              {respoceData.firstName} {respoceData.lastName}
            </h3>
            <span className="pull-right">
              <button
                className="btn btn-secondary pull-right mt-1"
                type="button"
                onClick={handleBack}
              >
                Cancel & Go Back
              </button>
            </span>
          </div>

          <div className="row">
            <aside className="col-lg-4 col-md-5 pe-xl-4 mb-5">
              <div className="multitab-action bg-light border rounded-3 p-3">
                <div className="d-flex d-md-block d-lg-flex align-items-start pt-lg-2 mb-4">
                  <img
                    width="48"
                    height="48"
                    className="rounded-circle getContact_picture"
                    src={getContact?.image || Pic}
                    alt="getContact"
                  />
                  <div className="pt-md-2 pt-lg-0 ps-3 ps-md-0 ps-lg-3">
                    <h2 className="fs-lg mb-0">
                      {getContact?.firstName} {getContact?.lastName}
                    </h2>
                    <ul className="list-unstyled fs-sm mt-2 mb-0 ps-0">
                      <li className="pb-0">
                        <a
                          className="nav-link fw-normal p-0"
                          href={`tel:${getContact?.phone ?? ""}`}
                        >
                          <i className="fi-phone opacity-60 me-2"></i>
                          {getContact?.phone}
                        </a>
                      </li>
                      <li className="pb-2">
                        <a
                          className="nav-link fw-normal p-0"
                          href={`mailto:${getContact?.email ?? ""}`}
                        >
                          <i className="fi-mail opacity-60 me-2"></i>
                          {getContact?.email}
                        </a>
                      </li>
                    </ul>
                  </div>
                </div>
                {localStorage.getItem("auth") &&
                (jwt(localStorage.getItem("auth")).contactType == "admin" ||
                  jwt(localStorage.getItem("auth")).id == params.id) ? (
                  <div className="float-left w-100 text-center">
                    <label className="btn btn-primary btn-lg w-100 mb-3 documentlabel d-inline-block">
                      <input
                        id="REPC_real_estate_purchase_contract"
                        type="file"
                        accept={acceptedFileTypes
                          .map((type) => `.${type}`)
                          .join(",")}
                        name="file"
                        onChange={handleFileUpload}
                        value={getContact.file}
                      />
                      <i className="fi-plus me-2"></i> Add a Photo
                    </label>
                  </div>
                ) : (
                  ""
                )}

                {/* <a className="btn btn-outline-secondary d-block d-md-none w-100 mb-3" href="#account-nav" data-bs-toggle="collapse"><i className="fi-align-justify me-2"></i>Menu</a> */}
                <div className="collapse d-md-block mt-3" id="account-nav">
                  <div className="card-nav">
                    {/* <NavLink className={getContact ? "card-nav-link active" : "card-nav-link"} */}
                    <button
                      className={`card-nav-link ${
                        activeButton === "1" ? "active" : ""
                      }`}
                      onClick={() => handlegetContact("1")}
                    >
                      <i className="fi-user opacity-60 me-2"></i>Profile
                    </button>
                    {jwt(localStorage.getItem("auth")).contactType == "admin" ||
                    jwt(localStorage.getItem("auth")).id == params.id ? (
                      <>
                        <button
                          className={`card-nav-link ${
                            activeButton === "2" ? "active" : ""
                          }`}
                          onClick={() => handlegetContact("2")}
                        >
                          <i className="fi-lock opacity-60 me-2"></i>Account
                        </button>

                        <button
                          className={`card-nav-link ${
                            activeButton === "3" ? "active" : ""
                          }`}
                          onClick={() => handlegetContact("3")}
                        >
                          <i
                            className="fa fa-sliders  opacity-60 me-2"
                            aria-hidden="true"
                          ></i>
                          Settings
                        </button>

                        <button
                          className={`card-nav-link ${
                            activeButton === "4" ? "active" : ""
                          }`}
                          onClick={() => handlegetContact("4")}
                        >
                          <i
                            className="fa fa-line-chart opacity-60 me-2"
                            aria-hidden="true"
                          ></i>
                          Activity
                        </button>

                        <button
                          className={`card-nav-link ${
                            activeButton === "5" ? "active" : ""
                          }`}
                          onClick={() => handlegetContact("5")}
                        >
                          <i className="fi-user opacity-60 me-2"></i>
                          About Me
                        </button>

                        {getContact && getContact?.nickname ? (
                          <button
                            className={`card-nav-link ${
                              activeButton === getContact?.nickname
                                ? "active"
                                : ""
                            }`}
                            onClick={() => handleNavigate(getContact?.nickname)}
                          >
                            <i
                              className="fa fa-credit-card-alt opacity-60 me-2"
                              aria-hidden="true"
                            ></i>
                            Business Card
                          </button>
                        ) : (
                          ""
                        )}

                        {localStorage.getItem("auth") &&
                        (jwt(localStorage.getItem("auth")).contactType ==
                          "admin" ||
                          jwt(localStorage.getItem("auth")).id == params.id) ? (
                          <>
                            <button
                              className={`card-nav-link ${
                                activeButton === "7" ? "active" : ""
                              }`}
                              onClick={() => handlegetContact("7")}
                            >
                              <i
                                className="fa fa-address-card opacity-60 me-2"
                                aria-hidden="true"
                              ></i>
                              Account Details
                            </button>

                            <button
                              className={`card-nav-link ${
                                activeButton === "8" ? "active" : ""
                              }`}
                              onClick={() => handlegetContact("8")}
                            >
                              <i
                                className="fa fa-pencil-square-o opacity-60 me-2"
                                aria-hidden="true"
                              ></i>
                              Edit Profile
                            </button>

                            <button
                              className={`card-nav-link ${
                                activeButton === "9" ? "active" : ""
                              }`}
                              onClick={() => handlegetContact("9")}
                            >
                              <i
                                className="fa fa-phone-square opacity-60 me-2"
                                aria-hidden="true"
                              ></i>
                              Contact Details
                            </button>
                          </>
                        ) : (
                          ""
                        )}
                      </>
                    ) : (
                      ""
                    )}
                  </div>
                </div>
              </div>
            </aside>
            <div className="col-md-8">
              <div className="bg-light border rounded-3 p-3">
                <h6 className="">
                  Calendar Subscriptions for {respoceData.firstName}{" "}
                  {respoceData.lastName}
                </h6>
                <p className="">
                  All of the calendar events can be synchronized with external
                  devices and calendars.
                </p>
                <h6 className="mt-3">Personal Calendar</h6>
                <p className="">
                  Combined calendar of personal events, transaction dates,
                  office events you have registered for, and office resource
                  calendar reservations.
                </p>
                {calenderData && calenderData.personal.length > 0 ? (
                  calenderData.personal.map((item) => (
                    <>
                      <div className="col-md-12 d-lg-flex" key={item._id}>
                        <div className="col-md-4">
                          <NavLink>Subscription Created</NavLink>
                        </div>
                        <div className="col-md-8 d-flex">
                          <p>Subscription URL</p>
                          <button
                            className="btn btn-primary mb-3 ms-3"
                             onClick={() => copyToClipboard(item.calenderURL)}
                          >
                            Copy
                          </button>
                        </div>
                      </div>
                      <div className="col-md-12 d-lg-flex" key={item._id}>
                        <div className="col-md-4">
                          <NavLink>Synced</NavLink>
                        </div>
                        <div className="col-md-8">
                          <p className="text-truncate">
                            {item.calenderURL}
                          </p>
                        </div>
                      </div>
                    </>
                  ))
                ) : (
                  <div className="">
                    <NavLink>
                      <p onClick={(e) => handlePersonal(e, "personal")}>
                        Setup Now
                      </p>
                    </NavLink>
                    <span>- Create a new subscription link.</span>
                  </div>
                )}

                <h6 className="mt-3">Office Calendar</h6>
                <p className="">All office, training, and community events.</p>
                {calenderData && calenderData.office.length > 0 ? (
                  calenderData.office.map((item) => (
                    <>
                      <div className="col-md-12 d-lg-flex" key={item._id}>
                        <div className="col-md-4">
                          <NavLink>Subscription Created</NavLink>
                        </div>
                        <div className="col-md-8 d-flex">
                          <p>Subscription URL</p>
                          <button
                            className="btn btn-primary mb-3 ms-3"
                             onClick={() => copyToClipboard(item.calenderURL)}
                          >
                            Copy
                          </button>
                        </div>
                      </div>
                      <div className="col-md-12 d-lg-flex" key={item._id}>
                        <div className="col-md-4">
                          <NavLink>Synced</NavLink>
                        </div>
                        <div className="col-md-8">
                          <p className="text-truncate">
                            {item.calenderURL}
                          </p>
                        </div>
                      </div>
                    </>
                  ))
                ) : (
                  <div className="d-flex">
                    <NavLink>
                      <p onClick={(e) => handleOffice(e, "office")}>
                        Setup Now
                      </p>
                    </NavLink>
                    <span>- Create a new subscription link.</span>
                  </div>
                )}

                <h6 className="mt-3">Staff Calendar</h6>
                <p className="">Only available to other staff members.</p>
                {calenderData && calenderData.staff.length > 0 ? (
                  calenderData.staff.map((item) => (
                    <>
                      <div className="col-md-12 d-lg-flex" key={item._id}>
                        <div className="col-md-4">
                          <NavLink>Subscription Created</NavLink>
                        </div>
                        <div className="col-md-8 d-flex">
                          <p>Subscription URL</p>
                          <button
                            className="btn btn-primary mb-3 ms-3"
                             onClick={() => copyToClipboard(item.calenderURL)}
                          >
                            Copy
                          </button>
                        </div>
                      </div>
                      <div className="col-md-12 d-lg-flex" key={item._id}>
                        <div className="col-md-4">
                          <NavLink>Synced</NavLink>
                        </div>
                        <div className="col-md-8">
                          <p className="text-truncate">
                            {item.calenderURL}
                          </p>
                        </div>
                      </div>
                    </>
                  ))
                ) : (
                  <div className="d-flex">
                    <NavLink>
                      <p onClick={(e) => handleStaff(e, "staff")}>Setup Now</p>
                    </NavLink>
                    <span>- Create a new subscription link.</span>
                  </div>
                )}

                <h6 className="mt-3">Public Calendar</h6>
                <p className="">
                  Combined calendar of office events identified as public for
                  use on a marketing calendar.
                </p>

                {calenderData && calenderData.public.length > 0 ? (
                  calenderData.public.map((item) => (
                    <>
                      <div className="col-md-12 d-lg-flex" key={item._id}>
                        <div className="col-md-4">
                          <NavLink>Subscription Created</NavLink>
                        </div>
                        <div className="col-md-8 d-flex">
                          <p>Subscription URL</p>
                          <button
                            className="btn btn-primary mb-3 ms-3"
                             onClick={() => copyToClipboard(item.calenderURL)}
                          >
                            Copy
                          </button>
                        </div>
                      </div>
                      <div className="col-md-12 d-lg-flex" key={item._id}>
                        <div className="col-md-4">
                          <NavLink>Synced</NavLink>
                        </div>
                        <div className="col-md-8">
                          <p className="text-truncate">
                            {item.calenderURL}
                          </p>
                        </div>
                      </div>
                    </>
                  ))
                ) : (
                  <div className="d-flex">
                    <NavLink>
                      <p onClick={(e) => handlePublic(e, "public")}>
                        Setup Now
                      </p>
                    </NavLink>
                    <span>- Create a new subscription link.</span>
                  </div>
                )}

                <div className="col-md-12 d-flex mt-3">
                  <h6>Need help?</h6>&nbsp;&nbsp;
                  <p>
                    View our Help Center page, here:{" "}
                    <NavLink>
                      Subscribe to Propertybase Back Office Calendars
                    </NavLink>
                  </p>
                </div>
                <p className="">
                  Events from 30 days ago through 120 days after the current day
                  will be included in the subscribed feed. Our system caches the
                  calendar feeds for 20 minutes, it may take 20-30 minutes for
                  an event to appear on a subscribed calendar feed in Outlook or
                  an iOS device. Google Calendar feeds may take 10-12 hours to
                  reflect calendar changes, this is due to Google's policy of
                  only checking for updates this often - this setting cannot be
                  adjusted. As a best practice, you can expect to find events
                  added today to be on all subscribed devices tomorrow.
                </p>
              </div>
            </div>
          </div>
        </div>
      </main>
    </div>
  );
};

export default CalenderSubscriptions;

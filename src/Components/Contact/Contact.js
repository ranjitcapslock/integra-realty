import React, { useState, useEffect } from "react";
import Pic from "../img/pic.png";
// import Modal from "../../Modal";
import user_service from "../service/user_service";
import ReactPaginate from "react-paginate";
import Loader from "../../Pages/Loader/Loader.js";
import PagesNavigationUI from "../../Pages/PagesNavigationUI.js";
import Toaster from "../../Pages/Toaster/Toaster.js";
import { NavLink, useNavigate } from "react-router-dom";
import jwt from "jwt-decode";
import Swal from "sweetalert2";
import "sweetalert2/dist/sweetalert2.css";

function Contact() {
  const [getContact, setGetContact] = useState([]);
  const [pageCount, setPageCount] = useState(0);
  const [pageCountNew, setPageCountNew] = useState(0);

  const [totalRecords, setTotalRecords] = useState("");
  const [loader, setLoader] = useState({ isActive: null });
  const { isActive } = loader;
  const [toaster, setToaster] = useState({
    types: null,
    isShow: null,
    toasterBody: "",
    message: "",
  });
  const { types, isShow, toasterBody, message } = toaster;
  const [query, setQuery] = useState("");
  const [contactType, setContactType] = useState("private");
  const [contactpermission, setContactpermission] = useState("");
  const [contact_status, setContact_status] = useState("active");

  const [contactDeleted, setContactDeleted] = useState("");
  const [results, setResults] = useState([]);
  const [currentPage, setCurrentPage] = useState(1);
  const [isLoading, setIsLoading] = useState(false);
  const [getBanner, setGetBanner] = useState("");

  useEffect(() => {
    window.scrollTo(0, 0);
    setLoader({ isActive: true });
    const bannerGetAll = async () => {
      await user_service.bannerGetAll().then((response) => {
        setLoader({ isActive: false });
        if (response) {
          setGetBanner(response.data.data);
        }
      });
    };
    bannerGetAll();
  }, []);

  const SearchGetAll = async () => {
    setIsLoading(true);

    let contactTypee = contactType;

    if (contactpermission && contactpermission !== "") {
      contactTypee += `&contactpermission=${contactpermission}`;
    }

    if (contact_status && contact_status !== "") {
      if (contact_status === "inapproval") {
        contactTypee += `&contact_status=${contact_status}`;
      } else {
        contactTypee += `&contact_status=${contact_status}`;
      }
    }

    if (contactType === "private") {
      if (
        (localStorage.getItem("auth") &&
          jwt(localStorage.getItem("auth")).contactType == "admin") ||
        jwt(localStorage.getItem("auth")).contactType == "staff"
      ) {
      } else {
        contactTypee += `&agentId=${jwt(localStorage.getItem("auth")).id}`;
      }
    }

    try {
      const response = await user_service.SearchContactfilter(
        1,
        contactTypee,
        query
      );
      setIsLoading(false);
      if (response) {
        setGetContact(response.data.data);
        setPageCount(Math.ceil(response.data.totalRecords / 10));
        setTotalRecords(response.data.totalRecords);
      }
    } catch (error) {
      console.error("Error fetching data:", error);
      setIsLoading(false);
    }
  };

  const handlePageClick = async (data) => {
    let currentPage = data.selected + 1;
    setLoader({ isActive: true });

    let contactTypee = contactType;

    if (contactpermission && contactpermission !== "") {
      contactTypee += `&contactpermission=${contactpermission}`;
    }

    if (contact_status && contact_status !== "") {
      if (contact_status === "inapproval") {
        // Add =done only once when contact_status is inapproval
        contactTypee += `&contact_status=${contact_status}`;
      } else {
        contactTypee += `&contact_status=${contact_status}`;
      }
    }

    if (contactType === "private") {
      if (
        (localStorage.getItem("auth") &&
          jwt(localStorage.getItem("auth")).contactType == "admin") ||
        jwt(localStorage.getItem("auth")).contactType == "staff"
      ) {
      } else {
        contactTypee += `&agentId=${jwt(localStorage.getItem("auth")).id}`;
      }
    }

    try {
      const response = await user_service.SearchContactfilter(
        currentPage,
        contactTypee,
        query
      );
      setLoader({ isActive: false });

      if (response) {
        setGetContact(response.data.data);
        setPageCount(Math.ceil(response.data.totalRecords / 10));
        setTotalRecords(response.data.totalRecords);
      }
    } catch (error) {
      console.error("Error fetching data:", error);
      setLoader({ isActive: false });
    }
  };

  useEffect(() => {
    window.scrollTo(0, 0);
    if (contactType || contactpermission || contact_status || query) {
      SearchGetAll(1);
      // if (query) {
      //   SearchGetAllNew(1);
      // }
    }
  }, [contactType, contactpermission, contact_status, query]);

  // const SearchGetAllNew = async () => {
  //   setIsLoading(true);

  //   await user_service.SearchContactGet(1, 10, query).then((response) => {
  //     setIsLoading(false);
  //     if (response) {
  //       setGetContact(response.data.data);
  //       setPageCountNew(Math.ceil(response.data.totalRecords / 10));
  //     }
  //   });
  // };

  // const handlePageClickNew = async (data) => {
  //   let currentPage = data.selected + 1;
  //   setLoader({ isActive: true });

  //   try {
  //     const response = await user_service.SearchContactGet(
  //       currentPage,
  //       10,
  //       query
  //     );
  //     setLoader({ isActive: false });

  //     if (response) {
  //       setGetContact(response.data.data);
  //       setPageCountNew(Math.ceil(response.data.totalRecords / 10));
  //       // setTotalRecords(response.data.totalRecords);
  //     }
  //   } catch (error) {
  //     console.error("Error fetching data:", error);
  //     setLoader({ isActive: false });
  //   }
  // };
  const navigate = useNavigate();

  const handleSubmit = (id, contactType) => {
    if (contactType === "private") {
      navigate(`/private-contact/${id}`);
    } else {
      navigate(`/contact-profile/${id}`);
    }
  };

  {
    /* <!-- paginate Function Search Api call End--> */
  }

  const handleSearch = (event) => {
    event.preventDefault();
    setCurrentPage(1);
    SearchGetAll();
  };

  const handleDeleteContactpermanent = async (id) => {
    console.log(id);
    if (id) {
      const userData = {
        contact_status: "deleted",
      };
      console.log(userData);
      setLoader({ isActive: true });
      await user_service.contactUpdate(id, userData).then((response) => {
        if (response) {
          setLoader({ isActive: false });
          setContactDeleted(response.data);
          SearchGetAll();
        } else {
          setLoader({ isActive: false });
          setToaster({
            type: "error",
            isShow: true,
            toasterBody: response.data.message,
            message: "Error",
          });

          setTimeout(() => {
            setToaster((prevToaster) => ({
              ...prevToaster,
              isShow: false,
            }));
          }, 3000);
        }
      });
    }
  };

  const handleDeleteAllContactspermanent = async (id) => {
    Swal.fire({
      title: "Are you sure?",
      text: "It will be permanently deleted all private /associate contacts !",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Yes, delete it!",
    }).then(async (result) => {
      if (result.isConfirmed) {
        try {
          const response = await user_service.contactDelete(id);
          if (response) {
            setLoader({ isActive: false });
            if (response) {
              Swal.fire(
                "Deleted!",
                "Contact is deleted permanently.",
                "success"
              );
              //setFormValues(response.data);
            }
            SearchGetAll();
          }
        } catch (error) {
          Swal.fire(
            "Error",
            "An error occurred while deleting the file.",
            "error"
          );
          console.error(error);
        }
      }
    });
  };

  const profilepic = (e) => {
    e.target.src = Pic;
  };

  const handlesetuppassword = async (id) => {
    const userData = {
      _id: id,
      agentId: jwt(localStorage.getItem("auth")).id,
    };
    console.log("userData", userData);
    try {
      setLoader({ isActive: true });
      const response = await user_service.onboardlink(userData);
      if (response) {
        setLoader({ isActive: false });
        setToaster({
          type: "Onboarding Link Sent Successfully",
          isShow: true,
          toasterBody: response.data.message,
          message: "Onboarding Link Sent Successfully",
        });
        setTimeout(() => {
          setToaster((prevToaster) => ({ ...prevToaster, isShow: false }));
          //  navigate(`/onboarding/${response.data.intranet_confirmationcode}/${response.data.intranet_uniqueid}`);
        }, 2000);
      } else {
        setLoader({ isActive: false });
        setToaster({
          types: "error",
          isShow: true,
          toasterBody: response.data.message,
          message: "Error",
        });
      }
    } catch (error) {
      setLoader({ isActive: false });
      setToaster({
        types: "error",
        isShow: true,
        toasterBody: error,
        message: "Error",
      });
    }
    setTimeout(() => {
      setToaster({
        types: "error",
        isShow: false,
        toasterBody: null,
        message: "Error",
      });
    }, 2000);
  };

  const [formData, setFormData] = useState([]);

  const StaffRoleGetData = async () => {
    await user_service.StaffRoleGet().then((response) => {
      if (response) {
        console.log(response);
        setFormData(response.data.data);
      }
    });
  };
  useEffect(() => {
    if (formData) {
      StaffRoleGetData();
    }
  }, []);

  return (
    <div className="bg-secondary float-left w-100 pt-4 mb-4">
      <Loader isActive={isActive} />
      {isShow && (
        <Toaster
          types={types}
          isShow={isShow}
          toasterBody={toasterBody}
          message={message}
        />
      )}
      <div className="">
        <main className="page-wrapper homepage_layout">
          <div className="row mt-2">
            <div className="col-md-12">
              <div className="float-left w-100 mb-4">
                <h3 className="pull-left mb-0 text-white">My Contacts</h3>
                <NavLink
                  to="/add-contact"
                  type="button"
                  className="btn btn-info mb-2 pull-right ms-3"
                >
                  Add a Contact
                </NavLink>
                {localStorage.getItem("auth") &&
                jwt(localStorage.getItem("auth")).contactType == "admin" ? (
                  <NavLink
                    to="/contact-group"
                    type="button"
                    className="btn btn-info mb-2 pull-right ms-3"
                  >
                    Add Contact Group
                  </NavLink>
                ) : (
                  ""
                )}
                
                {/* {localStorage.getItem("auth") &&
                jwt(localStorage.getItem("auth")).contactType == "admin" ? ( */}
                  <NavLink
                    to="/reservation-listing"
                    type="button"
                    className="btn btn-info mb-2 pull-right ms-3"
                  >
                    Add Reservations
                  </NavLink>
                {/* ) : (
                  ""
                )} */}
                {localStorage.getItem("auth") &&
                jwt(localStorage.getItem("auth")).contactType == "admin" ? (
                  <NavLink
                    onClick={(e) => handleDeleteAllContactspermanent("all")}
                    to="#"
                    className="btn btn-info mb-2 pull-right"
                    type="button"
                  >
                    <i className="fa fa-trash-o me-2" aria-hidden="true"></i>
                    Delete
                  </NavLink>
                ) : (
                  ""
                )}
                &nbsp;
                <hr className="mb-3 w-100" />
              </div>

              {/* <!-- Normal search form group --> */}
              <form onSubmit={handleSearch}>
                {/* <p>
                  Quick Search: <a href="">Show All</a>
                </p> */}
                <div className="row">
                  <div className="col-lg-3 col-md-3 col-sm-6 col-6 form-outline mb-lg-0 mb-md-0 mb-sm-3 mb-3">
                    <input
                      className="form-control"
                      type="text"
                      id="search-input-1"
                      placeholder="Search All"
                      valvalue={query}
                      onChange={(event) => setQuery(event.target.value)}
                    />
                  </div>
                  <div className="col-lg-3 col-md-3 col-sm-6 col-6 form-outline mb-lg-0 mb-md-0 mb-sm-3 mb-3">
                    <select
                      className="form-select"
                      id="pr-city"
                      name="contactType"
                      onChange={(event) => setContactType(event.target.value)}
                      value={contactType}
                    >
                      <option value="all">Search All Available</option>
                      <option value="associate">Associates</option>
                      <option value="private">Personal</option>
                      <option value="community">Community</option>
                      {localStorage.getItem("auth") &&
                      jwt(localStorage.getItem("auth")).contactType ==
                        "admin" ? (
                        <option value="staff"> Staff</option>
                      ) : (
                        ""
                      )}
                    </select>
                  </div>
                  {localStorage.getItem("auth") &&
                  jwt(localStorage.getItem("auth")).contactType == "admin" ? (
                    <div className="col-lg-3 col-md-3 col-sm-6 col-6 form-outline">
                      <select
                        className="form-select"
                        id="pr-city"
                        name="contactpermission"
                        onChange={(event) =>
                          setContactpermission(event.target.value)
                        }
                        value={contactpermission}
                      >
                        <option value="">Permission As</option>
                        <option value="full">Full Agent</option>
                        <option value="referralonly">
                          Referral Only Agent
                        </option>
                      </select>
                    </div>
                  ) : (
                    ""
                  )}
                  <div className="col-lg-3 col-md-3 col-sm-6 col-6 form-outline">
                    <select
                      className="form-select"
                      id="pr-city"
                      name="contact_status"
                      onChange={(event) =>
                        setContact_status(event.target.value)
                      }
                      value={contact_status}
                    >
                      <option value="" disabled="">
                        Contact Status
                      </option>
                      <option value="active">Active Contacts</option>
                      {/* {localStorage.getItem("auth") &&
                              jwt(localStorage.getItem("auth")).contactType ===
                                "admin" && (
                                <>
                                 {console.log(getContact)}
                                  {getContact.contactType === "associate" &&
                                    (getContact.onboardfinal != "done" || !getContact.onboard_approve) && (
                                      <option value="inapproval">OnBoarding Contacts</option>
                                    )}
                                </>
                              )} */}

                      {contactType === "associate" && (
                        <option value="inapproval">OnBoarding Contacts</option>
                      )}

                      <option value="deleted">Past Contacts</option>
                    </select>
                  </div>

                  {/* <span className="ms-4">
                                            <button type="button" className="btn btn-translucent-primary">Find</button>
                                            <a href="#" id="button-item" type="button">Reset</a>
                                        </span> */}
                </div>

                {pageCount > 1 ? (
                  <div className="d-flex align-items-center justify-content-end mb-0 mt-4">
                    <span className="float-left w-100 text-white">
                      <b>Total Matches : {totalRecords}</b>
                    </span>
                    <ReactPaginate
                      className=""
                      previousLabel={"Previous"}
                      nextLabel={"next"}
                      breakLabel={"..."}
                      pageCount={pageCount}
                      marginPagesDisplayed={1}
                      pageRangeDisplayed={2}
                      onPageChange={handlePageClick}
                      containerClassName={
                        "pagination justify-content-center mb-0"
                      }
                      pageClassName={"page-item"}
                      pageLinkClassName={"page-link"}
                      previousClassName={"page-item"}
                      previousLinkClassName={"page-link"}
                      nextClassName={"page-item"}
                      nextLinkClassName={"page-link"}
                      breakClassName={"page-item"}
                      breakLinkClassName={"page-link"}
                      activeClassName={"active"}
                    />
                  </div>
                ) : (
                  ""
                )}
              </form>

              {/* <!-- Item--> */}
              {getContact && getContact.length > 0 ? (
                getContact.map((post) => (
                  <div className="card card-hover mb-2 mt-4">
                    <div className="card-body p-3">
                      <div className="float-left w-100">
                        <div className="float-left w-100 d-lg-flex align-items-center justify-content-between">
                          <div
                            className="float-left w-100 d-flex align-items-center justify-content-start"
                            key={post._id}
                            onClick={() =>
                              handleSubmit(post._id, post.contactType)
                            }
                          >
                            <img
                              className="rounded-circle profile_picture"
                              src={post.image || Pic}
                              alt="Profile"
                              onError={(e) => profilepic(e)}
                            />

                            <div
                              className="ms-3"
                             
                            >
                              <strong>{post.firstName}</strong> &nbsp;
                              <strong>{post.lastName}</strong>
                              <p className="mb-1">{post.company}</p>
                              <p className="mb-1">{post.phone}</p>
                              <p className="mb-0">
                                <NavLink>
                                  <i
                                    className="fa fa-envelope"
                                    aria-hidden="true"
                                  >
                                    &nbsp; {post.email}
                                  </i>
                                </NavLink>
                              </p>
                            </div>
                          </div>

                          <div className="pull-right my_contacts tags-badge d-flex align-items-center justify-content-end w-100">
                            {localStorage.getItem("auth") &&
                            jwt(localStorage.getItem("auth")).contactType ==
                              "admin" ? (
                              <>
                                {post.contactType === "associate" &&
                                  post.contact_status === "inapproval" &&
                                  (post.onboardfinal === "done" ||
                                    !post.onboard_approve) && (
                                    <button
                                      className="btn btn-primary"
                                      onClick={() =>
                                        handlesetuppassword(post._id)
                                      }
                                    >
                                      Send Onboarding Request
                                    </button>
                                  )}
                              </>
                            ) : (
                              <>
                                {localStorage.getItem("auth") &&
                                  jwt(localStorage.getItem("auth"))
                                    .contactType == "staff" &&
                                  (Array.isArray(formData) &&
                                  formData.length > 0
                                    ? formData.map((item) =>
                                        item.roleStaff === "add_contact" ? (
                                          <>
                                            {post.contactType === "associate" &&
                                              post.contact_status ===
                                                "inapproval" &&
                                              (post.onboardfinal === "done" ||
                                                !post.onboard_approve) && (
                                                <button
                                                  className="btn btn-primary"
                                                  onClick={() =>
                                                    handlesetuppassword(
                                                      post._id
                                                    )
                                                  }
                                                >
                                                  Send Onboarding Request
                                                </button>
                                              )}
                                          </>
                                        ) : (
                                          ""
                                        )
                                      )
                                    : "")}
                              </>
                            )}

                            <badge
                              className="mx-4"
                              style={{ fontSize: "14px" }}
                            >
                              {post.contactType
                                ? post.contactType.charAt(0).toUpperCase() +
                                  post.contactType.slice(1)
                                : "Not Assigned"}
                            </badge>

                            {localStorage.getItem("auth") &&
                            jwt(localStorage.getItem("auth")).contactType ==
                              "admin" ? (
                              <>
                                {post.contact_status === "deleted" ? (
                                  <>
                                    {post.contact_status === "deleted" && (
                                      <p className="mb-0 ms-3">
                                        Past
                                        {post.contactType === "associate"
                                          ? " Associate"
                                          : " Contact"}
                                      </p>
                                    )}
                                  </>
                                ) : (
                                  ""
                                )}

                                {post.contact_status === "active" ? (
                                  <>
                                    {post.contact_status === "active" && (
                                      <badge
                                        style={{ fontSize: "14px" }}
                                        className="mx-4"
                                      >
                                        Active
                                      </badge>
                                    )}
                                  </>
                                ) : (
                                  ""
                                )}
                              </>
                            ) : (
                              <>
                                {localStorage.getItem("auth") &&
                                  jwt(localStorage.getItem("auth"))
                                    .contactType == "staff" &&
                                  (Array.isArray(formData) &&
                                  formData.length > 0
                                    ? formData.map((item) =>
                                        item.roleStaff === "add_contact" ? (
                                          <>
                                            {post.contact_status ===
                                            "deleted" ? (
                                              <>
                                                {post.contact_status ===
                                                  "deleted" && (
                                                  <p className="mb-0 ms-3">
                                                    Past
                                                    {post.contactType ===
                                                    "associate"
                                                      ? " Associate"
                                                      : " Contact"}
                                                  </p>
                                                )}
                                              </>
                                            ) : (
                                              ""
                                            )}

                                            {post.contact_status ===
                                            "active" ? (
                                              <>
                                                {post.contact_status ===
                                                  "active" && (
                                                  <badge
                                                    style={{ fontSize: "14px" }}
                                                    className="mx-4"
                                                  >
                                                    Active
                                                  </badge>
                                                )}
                                              </>
                                            ) : (
                                              ""
                                            )}
                                          </>
                                        ) : (
                                          ""
                                        )
                                      )
                                    : "")}
                              </>
                            )}

                            {post.contactType &&
                            post.contactType == "associate" ? (
                              <>
                                {post.contactpermission &&
                                post.contactpermission != "" ? (
                                  <>
                                    {post.contact_status === "active" && (
                                      <badge
                                        style={{ fontSize: "14px" }}
                                        className="mx-4"
                                      >
                                        {post.contactpermission ===
                                        "referralonly"
                                          ? "Referral"
                                          : post.contactpermission
                                              .charAt(0)
                                              .toUpperCase() +
                                            post.contactpermission.slice(1)}
                                      </badge>
                                    )}
                                  </>
                                ) : (
                                  ""
                                )}
                              </>
                            ) : (
                              ""
                            )}

                            {localStorage.getItem("auth") &&
                            jwt(localStorage.getItem("auth")).contactType ==
                              "admin" ? (
                              <badge
                                className="mx-4"
                                style={{ fontSize: "14px" }}
                              >
                                <NavLink
                                  onClick={(e) =>
                                    handleDeleteContactpermanent(post._id)
                                  }
                                  to="#"
                                  className="text-center "
                                  type="button"
                                >
                                  <i
                                    className="fa fa-trash-o me-2"
                                    aria-hidden="true"
                                  ></i>
                                </NavLink>
                              </badge>
                            ) : (
                              <>
                                {localStorage.getItem("auth") &&
                                  jwt(localStorage.getItem("auth"))
                                    .contactType == "staff" &&
                                  (Array.isArray(formData) &&
                                  formData.length > 0
                                    ? formData.map((item) =>
                                        item.roleStaff === "add_contact" ? (
                                          <badge
                                            className="mx-4"
                                            style={{ fontSize: "14px" }}
                                          >
                                            <NavLink
                                              onClick={(e) =>
                                                handleDeleteContactpermanent(
                                                  post._id
                                                )
                                              }
                                              to="#"
                                              className="text-center "
                                              type="button"
                                            >
                                              <i
                                                className="fa fa-trash-o me-2"
                                                aria-hidden="true"
                                              ></i>
                                            </NavLink>
                                          </badge>
                                        ) : (
                                          ""
                                        )
                                      )
                                    : "")}
                              </>
                            )}
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                ))
              ) : (
                <p className="text-center mt-5">
                  <i
                    className="fa fa-search text-white mt-5 mb-3"
                    aria-hidden="true"
                    style={{ fontSize: "100px" }}
                  ></i>
                  <br />
                  <span className="text-white">
                    No contacts found, please refine your search.
                  </span>
                </p>
              )}
              {/* <!-- Item--> */}
            </div>

            {pageCount > 1 ? (
                  <div className="d-flex align-items-center justify-content-end mb-0 mt-4">
                    <ReactPaginate
                      className=""
                      previousLabel={"Previous"}
                      nextLabel={"next"}
                      breakLabel={"..."}
                      pageCount={pageCount}
                      marginPagesDisplayed={1}
                      pageRangeDisplayed={2}
                      onPageChange={handlePageClick}
                      containerClassName={
                        "pagination justify-content-center mb-0"
                      }
                      pageClassName={"page-item"}
                      pageLinkClassName={"page-link"}
                      previousClassName={"page-item"}
                      previousLinkClassName={"page-link"}
                      nextClassName={"page-item"}
                      nextLinkClassName={"page-link"}
                      breakClassName={"page-item"}
                      breakLinkClassName={"page-link"}
                      activeClassName={"active"}
                    />
                  </div>
                ) : (
                  ""
                )}
          </div>
        </main>
      </div>
    </div>
  );
}
export default Contact;

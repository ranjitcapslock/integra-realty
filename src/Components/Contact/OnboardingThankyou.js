import React, { useState, useEffect } from "react";
import user_service from "../service/user_service";
import jwt from "jwt-decode";
import { useNavigate, useParams, NavLink } from "react-router-dom";
import Loader from "../../Pages/Loader/Loader.js";
import Toaster from "../../Pages/Toaster/Toaster.js";
import AgentAdded from "../../Components/img/Thankyou_page_Image.jpg";
const OnboardingThankyou = () => {
  const [clientId, setClientId] = useState("");
  const params = useParams();
  const navigate = useNavigate();


  const [loader, setLoader] = useState({ isActive: null });
  const { isActive } = loader;
  const [toaster, setToaster] = useState({ type: null, isShow: null, toasterBody: "", message: "" });
  const { type, isShow, toasterBody, message } = toaster;

  return (
    <div className="bg-secondary float-left w-100">
      <Loader isActive={isActive} />
      {isShow && <Toaster type={type} isShow={isShow} toasterBody={toasterBody} message={message} />}
      <div className="">
        <main className="page-wrapper homepage_layout">
          <section className="d-flex align-items-center position-relative min-vh-100 pb-5">
            <div className="container">
              {/* <div className="row gy-4">
                <div className="col-md-4 align-self-sm-end ">
                  <div className="ratio ratio-1x1 mx-auto transaction_tips">
                    <img className="d-block mx-md-0 mx-auto mb-md-0 mb-4" src={AgentAdded}/>
                  </div>
                </div>
                <div className="col-lg-6 col-md-6 offset-lg-2 offset-md-1 text-md-start text-center">
                  <h3 className="display-3 mb-4 pb-md-3">
                      Thank you
                  </h3>
                  <p className="lead mb-md-5 mb-4">
                      We are pleased to inform you that your account setup is now complete. Our team at Indepth Realty will review your application, and upon approval, you will be notified via email.
                  </p>
                  <p className="lead mb-md-5 mb-4">
                  Thank you for choosing Indepth Realty. We look forward to assisting you in your real estate endeavors.
                  <NavLink to="/">Homepage</NavLink>.
                  </p>
                </div>
              </div> */}

              <div className="thank_you row align-items-center mt-md-2">
                <div className="col-lg-7 order-lg-2 mb-lg-0 mb-4 pb-2 pb-lg-0 mt-lg-0 mt-md-0 mt-sm-5 mt-5"><img className="d-block mx-auto" src={AgentAdded} alt="Hero image" /></div>
                <div className="col-lg-5 order-lg-1 pe-lg-0">
                  <h1 className="display-5 mb-4 me-lg-n5 text-lg-start text-center mb-4"> Thank you for joining In Depth Realty,
                    </h1>
                  <p className="text-lg-start text-center mb-4 mb-lg-5 fs-lg">We are pleased to inform you that your account setup is complete.  Our team at In Depth Realty will review your application, and upon approval you will be notified via email.</p>
                  <p className="text-lg-start text-center mb-4 mb-lg-5 fs-lg">You will also receive an email within the next 24-48 hours from our payroll  company Bill.com.  You will need to complete this final step to insure you get paid quickly.  </p>
                  <div className="pull-right mt-2">
                  <NavLink  type="button"
                    className="btn btn-primary btn-lg" to="/">Go To Homepage</NavLink>
                    </div>
                </div>
              </div>
            </div>
          </section>
        </main>
      </div>
    </div>
  );
};

export default OnboardingThankyou;

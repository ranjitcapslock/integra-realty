import React, { useState, useRef, useEffect } from "react";
// import avtar from "./Components/img/avtar.jpg";
import _ from "lodash";
import user_service from "../service/user_service";
import jwt from "jwt-decode";
import Loader from "../../Pages/Loader/Loader.js";
import Toaster from "../../Pages/Toaster/Toaster.js";
import { useNavigate, useParams, NavLink } from "react-router-dom";
import axios from "axios";
// import AllTab from "../../Pages/AllTab";

function StaffSignatureListing() {
  const [formValues, setFormValues] = useState("");
  const [loader, setLoader] = useState({ isActive: null });
  const { isActive } = loader;

  const [toaster, setToaster] = useState({
    types: null,
    isShow: null,
    toasterBody: "",
    message: "",
  });
  const { types, isShow, toasterBody, message } = toaster;
  const [show, setIsShow] = useState(false);

  const params = useParams();
  const navigate = useNavigate();
  const [data, setData] = useState("");
  // const [profile, setProfile] = useState(initialValues);
  const [file, setFile] = useState(null);
  const [acceptedFileTypes, setAcceptedFileTypes] = useState([]);

  useEffect(() => { window.scrollTo(0, 0);
    const ContactGetById = async () => {
      try {
        const response = await user_service.contactGetById(params.id);
        setFormValues(response.data);
      } catch (error) {
        console.error("Error fetching contact data:", error);
      }
    };

    ContactGetById(params.id);
  }, [params.id]);

  const handleFileUpload = async (e) => {
    const selectedFile = e.target.files[0];
    setIsShow(true);
    if (selectedFile) {
      setFile(selectedFile);
      const formData = new FormData();
      formData.append("file", selectedFile);

      const config = {
        headers: {
          Accept: "application/json",
          "Content-Type": "multipart/form-data",
          Authorization: `Bearer ${localStorage.getItem("auth")}`,
        },
      };

      try {
        const uploadResponse = await axios.post(
          "https://api.brokeragentbase.com/upload",
          formData,
          config
        );
        const uploadedFileData = uploadResponse.data;
        console.log(uploadedFileData);
        setData(uploadedFileData);
  
        const updatedProfile = {
          staff_signature: uploadedFileData,
        };
        console.log(updatedProfile);
        setLoader({ isActive: true });
        await user_service
          .contactUpdate(params.id, updatedProfile)
          .then((response) => {
            if (response) {
              console.log(response.data);
              setLoader({ isActive: false });

              setToaster({
                type: "Update Signature",
                isShow: true,
                toasterBody: response.data.message,
                message: "Update  Signature Successfully",
              });
              setTimeout(() => {
                setToaster((prevToaster) => ({
                  ...prevToaster,
                  isShow: false,
                }));
              }, 1000);

              const ContactGetById = async () => {
                try {
                  const response = await user_service.contactGetById(params.id);
                  setFormValues(response.data);
                } catch (error) {
                  console.error("Error fetching contact data:", error);
                }
              };
              ContactGetById(params.id);
              setIsShow(false);

            } else {
              setLoader({ isActive: false });
              setToaster({
                types: "error",
                isShow: true,
                toasterBody: response.data.message,
                message: "Error",
              });
            }
          });
      } catch (error) {
        console.error("Error occurred during file upload:", error);
        setLoader({ isActive: false });
      }
    }
  };

  return (
    <div className="bg-secondary float-left w-100 pt-4 mb-4">
      <Loader isActive={isActive} />
      {isShow && (
        <Toaster
          types={types}
          isShow={isShow}
          toasterBody={toasterBody}
          message={message}
        />
      )}
      <main className="page-wrapper">
        <div className="row">
          <div className="col-md-8">
            <div className="float-left w-100 d-flex align-items-center justify-content-between mb-4">
              <h3 className="pull-left mb-0 text-white">{formValues?.firstName} {formValues?.lastName}</h3>
            </div>
            <div className="bg-light border rounded-3 p-3 mb-3">
              <div className="row">
                <div className="d-flex">
                  <p className="mt-3">Staff Signature for</p>&nbsp;
                  <h6 className="mt-3">{formValues?.firstName} {formValues?.lastName}</h6>
                </div>
                <div className="">
                  <h6>On File</h6>
                  {formValues?.staff_signature ? (
                    <>
                      <img src={formValues?.staff_signature} />
                    </>
                  ) : (
                    ""
                  )}
                  <h6>{formValues?.firstName} {formValues?.lastName}</h6>
            
                 
                </div>
              </div>
            </div>
            <div className="pull-right d-flex mt-3">
              <label className="documentlabel btn btn-primary" id="uploadlabelstaff">
                <input
                  className=""
                  id="REPC_real_estate_purchase_contract"
                  type="file"
                  accept={acceptedFileTypes
                    .map((type) => `.${type}`)
                    .join(",")}
                  name="file"
                  onChange={handleFileUpload}
                  disabled={show}
                />
                {show ? "Please wait" : "Upload Signature"}
              </label>

              <NavLink
                to={`/add-signature/${params.id}`}
                className="btn btn-primary ms-3"
                type="button"
              // onClick={handleSubmit}
              >
                Create Signature
              </NavLink>
            </div>
          </div>
          <div className="col-md-4">
         </div>
        </div>
      </main>
    </div>
  );
}
export default StaffSignatureListing;

import React, { useState, useEffect } from 'react'
import Pic from "../img/pic.png";
import { NavLink, useNavigate } from 'react-router-dom';
import user_service from '../service/user_service';
import axios from "axios";
import Loader from "../../Pages/Loader/Loader.js";
import Toaster from '../../Pages/Toaster/Toaster.js';
import jwt from "jwt-decode";
const Profile = () => {
    const [loader, setLoader] = useState({ isActive: null });
    const { isActive } = loader;
    const [toaster, setToaster] = useState({ types: null, isShow: null, toasterBody: "", message: "" });
    const { types, isShow, toasterBody, message } = toaster;
    const initialValues = {
        contactType: "", company: "", agentId: "",firstName:"", lastName:"",
        email: "", phone:""
    };

    const [data, setData] = useState("")
    const [profile, setProfile] = useState(initialValues);
    const [file, setFile] = useState(null);
    const [fileExtension, setFileExtension] = useState('');
    const [acceptedFileTypes, setAcceptedFileTypes] = useState([]);
    const [showSubmitButton, setShowSubmitButton] = useState(false);
    const navigate = useNavigate();

    useEffect(() => { window.scrollTo(0, 0);
        const profileGetAll = async () => {
            setLoader({ isActive: true });
            await user_service.profileGet(jwt(localStorage.getItem("auth")).id).then((response) => {
                if (response) {
                    setLoader({ isActive: false });
                    setProfile(response.data);
                }
            });
        };
        profileGetAll();
    }, []);
     
    const handleFileUpload = async (e) => {
        const selectedFile = e.target.files[0];

        if (selectedFile) {
            setFile(selectedFile);
            setFileExtension(selectedFile.name.split('.').pop());
            setShowSubmitButton(true);

            const formData = new FormData();
            formData.append('file', selectedFile);

            const config = {
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'multipart/form-data',
                    Authorization: `Bearer ${localStorage.getItem('auth')}`,
                },
            };

            try {
              
                const uploadResponse = await axios.post('https://api.brokeragentbase.com/upload', formData, config);
                const uploadedFileData = uploadResponse.data;
                console.log(uploadedFileData);
                setData(uploadedFileData);

                const updatedProfile = {
                    ...profile,
                    image: uploadedFileData
                };
                console.log(updatedProfile);


                setLoader({ isActive: true });
                await user_service.profilePost(updatedProfile, jwt(localStorage.getItem("auth")).id).then((response) => {
                    if (response) {
                        const profileGetAll = async () => {
                            setLoader({ isActive: true });
                            await user_service.profileGet(jwt(localStorage.getItem("auth")).id).then((response) => {
                                if (response) {
                                    setLoader({ isActive: false });
                                    setProfile(response.data);
                                }
                            });
                        };
                        profileGetAll();  
                  
                      setLoader({ isActive: false });
                      setToaster({
                        type: "Profile Update",
                        isShow: true,
                        toasterBody: response.data.message,
                        message: "Profile Update Successfully",
                      });
                  
                      setTimeout(() => {
                        setToaster((prevToaster) => ({ ...prevToaster, isShow: false }));
                      }, 500);

                     
                    } else {
                      setLoader({ isActive: false });
                      setToaster({
                        type: "error",
                        isShow: true,
                        toasterBody: response.data.message,
                        message: "Error",
                      });
                  
                      setTimeout(() => {
                        setToaster((prevToaster) => ({ ...prevToaster, isShow: false }));
                      }, 500);
                    }
                  });
                  
                
            } catch (error) {
                console.error('Error occurred during file upload:', error);
                setLoader({ isActive: false });
            }
        }
    };


    {/* <!-- Input onChange Start--> */ }
    const handleChange =  (e) => {
        const { name, value } = e.target;
        setProfile({ ...profile, [name]: value });
    };


    const handleSubmit = async(e) => {
        e.preventDefault();
      
        const userData = {
           agentId: jwt(localStorage.getItem('auth')).id,
           firstName:profile.firstName,
            // contactType: "admin",
            phone: profile.phone,
            company: profile.company,
            lastName: profile.lastName,
            email: profile.email,
        }
        setLoader({ isActive: true });
        const response = await user_service.profilePost(userData, jwt(localStorage.getItem("auth")).id);
        if (response) {
            setProfile(response.data);
            setLoader({ isActive: false })
            setToaster({ type: "Profile_Update", isShow: true, toasterBody: response.data.message, message: "Profile_Update Successfully", });
            setTimeout(() => {
                setToaster((prevToaster) => ({ ...prevToaster, isShow: false }));
              }, 500);
        }
        else {
            setLoader({ isActive: false })
            setToaster({ types: "error", isShow: true, toasterBody: response.data.message, message: "Error", });
        }
    }




return (
    <div className="bg-secondary float-left w-100 pt-4 mb-0">
        <Loader isActive={isActive} />
        {isShow && <Toaster types={types}
            isShow={isShow}
            toasterBody={toasterBody}
            message={message} />}
        <main className="page-wrapper profile_page_wrap">
            <div className="container content-overlay mt-3">
                <div className="">
                    <h4 className="nav-item mb-md-0 me-md-2 pe-md-1 mb-5" id="profile"><a className="nav-link active" href="#" aria-current="page">Profile</a></h4>
                    <div className="row">
                        <div className="col-lg-3">
                            <img className="rounded-circle profile_picture" src={profile.image || Pic} alt="Profile" />
                            <label className="documentlabel" id="uploadlabel">
                                <input
                                    className=""
                                    id="REPC_real_estate_purchase_contract"
                                    type="file"
                                    accept={acceptedFileTypes.map((type) => `.${type}`).join(',')}
                                    name="file"
                                    onChange={handleFileUpload}
                                    value={profile.file}
                                />
                                <i className="fa fa-pencil-square-o" aria-hidden="true"></i>
                            </label>
                            <p className="mt-0 p-2"><strong>{profile.name}</strong></p>
                        </div>
                        <div className="col-lg-9 mt-5">
                                <div className="collapse d-md-block" id="account-nav">
                                    <ul className="nav nav-pills flex-column flex-md-row pt-3 pt-md-0 pb-md-4 border-bottom-md">
                                        <li className="nav-item mb-md-0 me-md-2 pe-md-1"><NavLink className="nav-link active" to="/profile" aria-current="page"><i className="fi-heart mt-n1 me-2 fs-base"></i>Profile</NavLink></li>
                                        <li className="nav-item mb-md-0 me-md-2 pe-md-1"><NavLink className="nav-link" to="/profile-account"><i className="fi-bell mt-n1 me-2 fs-base"></i>Account</NavLink></li>
                                        <li className="nav-item mb-md-0 me-md-2 pe-md-1"><NavLink className="nav-link" to="/profile-activity"><i className="fi-file mt-n1 me-2 fs-base"></i>Activity</NavLink></li>
                                        <li className="nav-item mb-md-0"><NavLink className="nav-link" to="/profile-setting"><i className="fi-settings mt-n1 me-2 fs-base"></i>Settings</NavLink></li>
                                    </ul>
                                </div>
                            </div>
                        <div className="col-lg-3">
                            <h2 className="h4" id="info">Contact Info</h2>
                        </div><br />
                        <div className="col-lg-9">
                            <div className="border rounded-3 p-3" id="auth-info">
                                <div className="border-bottom pb-3 mb-3">
                                    <div className="d-flex align-items-center justify-content-between">
                                        <div className="pe-2">
                                            <label className="form-label fw-bold">Firstname</label>
                                            <div id="firstName-value">{profile.firstName}</div>
                                        </div>
                                        <div data-bs-toggle="tooltip" title="Edit"><a className="nav-link py-0" href="#name-collapse" data-bs-toggle="collapse"><i className="fi-edit"></i></a></div>
                                    </div>
                                    <div className="collapse" id="name-collapse" data-bs-parent="#auth-info">
                                        <input className="form-control mt-3" type="name"
                                            name="firstName" value={profile.firstName}
                                            onChange={handleChange}
                                            data-bs-binded-element="#name-value" data-bs-unset-value="Not specified" />
                                    </div>
                                </div>

                                <div className="border-bottom pb-3 mb-3">
                                    <div className="d-flex align-items-center justify-content-between">
                                        <div className="pe-2">
                                            <label className="form-label fw-bold">Lastname</label>
                                            <div id="lastName-value">{profile.lastName}</div>
                                        </div>
                                        <div data-bs-toggle="tooltip" title="Edit"><a className="nav-link py-0" href="#lastName-collapse" data-bs-toggle="collapse"><i className="fi-edit"></i></a></div>
                                    </div>
                                    <div className="collapse" id="lastName-collapse" data-bs-parent="#auth-info">
                                        <input className="form-control mt-3" type="name"
                                            name="lastName" value={profile.lastName}
                                            onChange={handleChange}
                                            data-bs-binded-element="#lastName-value" data-bs-unset-value="Not specified" />
                                    </div>
                                </div>

                                <div className="border-bottom pb-3 mb-3">
                                    <div className="d-flex align-items-center justify-content-between">
                                        <div className="pe-2">
                                            <label className="form-label fw-bold">Email</label>
                                            <div id="email-value">{profile.email}</div>
                                        </div>
                                        <div data-bs-toggle="tooltip" title="Edit"><a className="nav-link py-0" href="#email-collapse" data-bs-toggle="collapse"><i className="fi-edit"></i></a></div>
                                    </div>
                                    <div className="collapse" id="email-collapse" data-bs-parent="#auth-info">
                                        <input className="form-control mt-3" type="email"
                                            name="email" value={profile.email}
                                            onChange={handleChange}
                                            data-bs-binded-element="#email-value" data-bs-unset-value="Not specified" />
                                    </div>
                                </div>


                                <div className="border-bottom pb-3 mb-3">
                                    <div className="d-flex align-items-center justify-content-between">
                                        <div className="pe-2">
                                            <label className="form-label fw-bold">Phone</label>
                                            <div id="phone-value">{profile.phone}</div>
                                        </div>
                                        <div data-bs-toggle="tooltip" title="Edit"><a className="nav-link py-0" href="#phone-collapse" data-bs-toggle="collapse"><i className="fi-edit"></i></a></div>
                                    </div>
                                    <div className="collapse" id="phone-collapse" data-bs-parent="#personal-details">
                                        <input className="form-control mt-3" type="number"
                                            name="phone" value={profile.phone}
                                            onChange={handleChange}
                                            data-bs-binded-element="#phone-value" data-bs-unset-value="Not specified" />
                                    </div>
                                </div>

                                <div className="border-bottom pb-3 mb-3">
                                    <div className="d-flex align-items-center justify-content-between">
                                        <div className="pe-2">
                                            <label className="form-label fw-bold">Address</label>
                                            <div id="company-value">{profile.company}</div>
                                        </div>
                                        <div data-bs-toggle="tooltip" title="Edit"><a className="nav-link py-0" href="#company-collapse" data-bs-toggle="collapse"><i className="fi-edit"></i></a></div>
                                    </div>
                                    <div className="collapse" id="company-collapse" data-bs-parent="#personal-details">
                                        <input className="form-control mt-3" type="text"
                                            name="company" value={profile.company}
                                            onChange={handleChange}
                                            data-bs-binded-element="#company-value" data-bs-unset-value="Not specified" placeholder="Enter address" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>



                    <div className="row">
                        <div className="col-lg-3">
                            <h2 className="h4" id="info">Profile Details</h2>
                        </div><br />
                        <div className="col-lg-9 mt-5">
                            <div className="border rounded-3 p-3" id="auth-info">
                                <div className="border-bottom pb-3 mb-3">
                                    <div className="d-flex align-items-center justify-content-between">
                                        <div className="pe-2 mt-2">
                                            <label className="form-label fw-bold">Organization</label>
                                            <div id="organization">{profile.organization}</div>
                                        </div>
                                        <div data-bs-toggle="tooltip" title="Edit"><a className="nav-link py-0" href="#organization-collapse" data-bs-toggle="collapse"><i className="fi-edit"></i></a></div>
                                    </div>
                                    <div className="collapse" id="organization-collapse" data-bs-parent="#auth-info">
                                        <input className="form-control mt-3" type="text"
                                            name="organization" value={profile.organization}
                                            onChange={handleChange}
                                            data-bs-binded-element="#organization-value" data-bs-unset-value="Not specified" />
                                    </div>
                                </div>


                                <div className="border-bottom pb-3 mb-3">
                                    <div className="d-flex align-items-center justify-content-between">
                                        <div className="pe-2 mt-2">
                                            <label className="form-label fw-bold">Home Office</label>
                                            <div id="homeOffice-value">{profile.homeOffice}</div>
                                        </div>
                                        <div data-bs-toggle="tooltip" title="Edit"><a className="nav-link py-0" href="#homeOffice-collapse" data-bs-toggle="collapse"><i className="fi-edit"></i></a></div>
                                    </div>
                                    <div className="collapse" id="homeOffice-collapse" data-bs-parent="#auth-info">
                                        <input className="form-control mt-3" type="text"
                                            name="homeOffice" value={profile.homeOffice}
                                            onChange={handleChange}
                                            data-bs-binded-element="#homeOffice-value" data-bs-unset-value="Not specified" />
                                    </div>
                                </div>


                                <div className="border-bottom pb-3 mb-3">
                                    <div className="d-flex align-items-center justify-content-between">
                                        <div className="pe-2 mt-2">
                                            <label className="form-label fw-bold">Title</label>
                                            <div id="title-value">{profile.title}</div>
                                        </div>

                                        <div data-bs-toggle="tooltip" title="Edit"><a className="nav-link py-0" href="#title-collapse" data-bs-toggle="collapse"><i className="fi-edit"></i></a></div>
                                    </div>
                                    <div className="collapse" id="title-collapse" data-bs-parent="#auth-info">
                                        <input className="form-control mt-3" type="text"
                                            name="title" value={profile.title}
                                            onChange={handleChange}
                                            data-bs-binded-element="#title-value" data-bs-unset-value="Not specified" />
                                    </div>
                                </div>


                                <div className="border-bottom pb-3 mb-3">
                                    <div className="d-flex align-items-center justify-content-between">
                                        <div className="pe-2 mt-2">
                                            <label className="form-label fw-bold">Designations</label>
                                            <div id="designations-value">{profile.designations}</div>
                                        </div>
                                        <div data-bs-toggle="tooltip" title="Edit"><a className="nav-link py-0" href="#designations-collapse" data-bs-toggle="collapse"><i className="fi-edit"></i></a></div>
                                    </div>
                                    <div className="collapse" id="designations-collapse" data-bs-parent="#auth-info">
                                        <input className="form-control mt-3" type="text"
                                            name="designations" value={profile.designations}
                                            onChange={handleChange}
                                            data-bs-binded-element="#designations-value" data-bs-unset-value="Not specified" />
                                    </div>
                                </div>


                                <div className="border-bottom pb-3 mb-3 mt-lg-0 mt-md-0 mt-sm-4 mt-4">
                                    <div className="d-flex align-items-center justify-content-between">
                                        <div className="pe-2 mt-2">
                                            <label className="form-label fw-bold">Social Links</label>
                                            <div>Not set</div>
                                        </div>
                                    </div>

                                </div><br />

                                <div className="pe-2 mt-2">
                                    <label className="form-label fw-bold">Supporters</label>
                                    <div>Not Supporters</div>
                                </div>
                            </div>
                            <div className="d-flex align-items-center justify-content-between">
                                <button className="btn btn-primary  px-3 px-sm-4" type="button" onClick={handleSubmit}>Save Change</button>
                            </div>


                        </div>
                    </div>
                </div>
            </div>

        </main>
        
    </div >
)
}

export default Profile
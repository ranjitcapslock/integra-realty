import React, { useState, useEffect } from "react";
import _ from "lodash";
import user_service from "../service/user_service";
import jwt from "jwt-decode";
import Loader from "../../Pages/Loader/Loader.js";
import Toaster from "../../Pages/Toaster/Toaster.js";
import { useNavigate, NavLink, useParams } from "react-router-dom";

function ContactInfo() {
  const initialValues = {
    firstName: "",
    lastName: "",
    email: "",
    phone: "",
    fax: "",
    address: "",
    prefix: "",
    nickname: "",
    middle: "",
    initials: "",
    maiden: "",
    suffix: "",
  };
  const [formValues, setFormValues] = useState(initialValues);
  const [loader, setLoader] = useState({ isActive: null });
  const { isActive } = loader;
  const [toaster, setToaster] = useState({
    types: null,
    isShow: null,
    toasterBody: "",
    message: "",
  });
  const { types, isShow, toasterBody, message } = toaster;

  const navigate = useNavigate();
  const params = useParams();

  // onChange Function start
  const handleChange = (e) => {
    const { name, value } = e.target;
    setFormValues({ ...formValues, [name]: value });
  };
  // onChange Function end

  const handleSubmit = async (e) => {
    e.preventDefault();
    const userData = {
      agentId: jwt(localStorage.getItem("auth")).id,
      firstName: formValues.firstName,
      lastName: formValues.lastName,
      email: formValues.email,
      phone: formValues.phone,
      address: formValues.address,
      fax: formValues.fax,
      prefix: formValues.prefix,
      nickname: formValues.nickname,
      middle: formValues.middle,
      initials: formValues.initials,
      maiden: formValues.maiden,
      suffix: formValues.suffix,
    };

    try {
      setLoader({ isActive: true });
      const response = await user_service.contactUpdate(params.id, userData);
      setFormValues(response.data);
      setToaster({
        type: "Profile Updated Successfully",
        isShow: true,
        toasterBody: response.data.message,
        message: "Profile Updated Successfully",
      });
     
      setTimeout(() => {
        setToaster((prevToaster) => ({ ...prevToaster, isShow: false }));
      }, 5000);
      ContactGetById(params.id);
      setLoader({ isActive: false });
    } catch (error) {
      setLoader({ isActive: false });
      setToaster({
        types: "error",
        isShow: true,
        toasterBody: error.response
          ? error.response.data.message
          : error.message,
        message: "Error",
      });

      setTimeout(() => {
        setToaster((prevToaster) => ({ ...prevToaster, isShow: false }));
      }, 1000);
    }
  };

  {
    /* <!-- Api call Form onSubmit End--> */
  }

  const handleBack = () => {
    navigate(`/contact-profile/${params.id}`);
  };

  useEffect(() => {
    ContactGetById(params.id);
  }, [params.id]);

  const ContactGetById = async () => {
    try {
      const response = await user_service.contactGetById(params.id);
      setFormValues(response.data);
    } catch (error) {
      console.error("Error fetching contact data:", error);
    }
  };

  return (
    <div className="float-left w-100">
      <Loader isActive={isActive} />
      {isShow && (
        <Toaster
          types={types}
          isShow={isShow}
          toasterBody={toasterBody}
          message={message}
        />
      )}

      <main className="page-wrapper profile_page_wrap">
        <div className="content-overlay">
          <div className="justify-content-center pb-sm-2">
            <div className="">
              <div className="">
                <div className="float-left w-100 d-flex align-items-center justify-content-between mb-3">
                  <h3 className="pull-left mb-0 text-white" id="getContact">
                    Legal Name:{" "}
                    <strong>
                      {formValues.firstName} {formValues.lastName}
                    </strong>
                  </h3>
                </div>
                <div className="float-left w-100 d-flex align-items-center justify-content-between mb-3">
                  <h6 className="pull-left mb-0 text-white" id="getContact">
                    Display Name:
                    <strong>
                      {formValues.firstName} {formValues.lastName}
                    </strong>
                  </h6>
                </div>

                <div className="add_listing rounded-3 bg-light border w-100 p-3">
                  <div className="row">
                    <div className="col-sm-4">
                      <label className="form-label">Prefix</label>
                      <select
                        className="form-select"
                        name="prefix"
                        value={formValues.prefix}
                        onChange={handleChange}
                      >
                        <option value=""></option>
                        <option selected="selected" value="Mr.">
                          Mr.
                        </option>
                        <option value="Ms.">Ms.</option>
                        <option value="Mrs.">Mrs.</option>
                        <option value="Dr.">Dr.</option>
                        <option value="Prof.">Prof.</option>
                        <option value="Sir">Sir</option>
                        <option value="Judge">Judge</option>
                        <option value="Sen.">Sen.</option>
                        <option value="Rev.">Rev.</option>
                        <option value="Ven.">Ven.</option>
                        <option value="Brother">Brother</option>
                        <option value="Deacon">Deacon</option>
                        <option value="Father">Father</option>
                        <option value="Minister">Minister</option>
                        <option value="Pastor">Pastor</option>
                        <option value="Priest">Priest</option>
                        <option value="Rabbi">Rabbi</option>
                        <option value="Rabbin">Rabbin</option>
                        <option value="Sister">Sister</option>
                      </select>
                    </div>
                    <div className="col-sm-4">
                      <label className="form-label">Given/First Name</label>
                      <input
                        className="form-control"
                        id="inline-form-input"
                        type="text"
                        name="firstName"
                        value={formValues.firstName}
                        onChange={handleChange}
                      />
                    </div>
                    <div className="col-sm-4">
                      <label className="form-label">Nick Name</label>
                      <input
                        className="form-control"
                        id="inline-form-input"
                        type="text"
                        name="nickname"
                        value={formValues.nickname}
                        onChange={handleChange}
                      />
                    </div>
                  </div>

                  <div className="row mt-4">
                    <div className="col-sm-4">
                      <label className="form-label">
                        Middle/Additional Names
                      </label>
                      <input
                        className="form-control"
                        id="inline-form-input"
                        type="text"
                        name="middle"
                        value={formValues.middle}
                        onChange={handleChange}
                      />
                    </div>
                    <div className="col-sm-4">
                      <label className="form-label">Middle Initials</label>
                      <input
                        className="form-control"
                        id="inline-form-input"
                        type="text"
                        name="initials"
                        value={formValues.initials}
                        onChange={handleChange}
                      />
                    </div>
                    <div className="col-sm-4">
                      <label className="form-label">Family/Last Name</label>
                      <input
                        className="form-control"
                        id="inline-form-input"
                        type="text"
                        name="lastName"
                        value={formValues.lastName}
                        onChange={handleChange}
                      />
                    </div>
                  </div>
                  <div className="row mt-4">
                    <div className="col-sm-6">
                      <label className="form-label">Former/Maiden Name</label>
                      <input
                        className="form-control"
                        id="inline-form-input"
                        type="text"
                        name="maiden"
                        value={formValues.maiden}
                        onChange={handleChange}
                      />
                    </div>
                    <div className="col-sm-6">
                      <label className="form-label">Suffix</label>
                      <input
                        className="form-control"
                        id="inline-form-input"
                        type="text"
                        name="suffix"
                        value={formValues.suffix}
                        onChange={handleChange}
                      />
                    </div>
                    <div className="col-sm-12 mt-3">
                      <p>
                        All name fields are searchable. First, Middle, Last, and
                        Suffix are used for legal forms. Selecting 'Display'
                        affects your default name display on your profile and
                        search results.
                      </p>
                      <span>
                        First Name is not shown when Nick name is set unless it
                        is a direct search match or used on legal forms. Middle
                        name is not shown when Middle Initial is set. For
                        hyphenates or multi-name, use the Middle and/or Last
                        Name fields instead of Maiden.
                      </span>
                    </div>
                  </div>
                  <div className="row mt-4">
                    <div className="col-sm-12">
                      <label className="form-label">Email</label>
                      <input
                        className="form-control"
                        id="inline-form-input"
                        type="text"
                        name="email"
                        value={formValues.email}
                        onChange={handleChange}
                      />
                    </div>

                    <div className="col-sm-12 mt-4">
                      <label className="form-label">Phone</label>
                      <input
                        className="form-control"
                        id="inline-form-input"
                        type="text"
                        name="phone"
                        value={formValues.phone}
                        onChange={handleChange}
                      />
                    </div>

                    <div className="col-sm-12 mt-4">
                      <label className="form-label">Fax</label>
                      <input
                        className="form-control"
                        id="inline-form-input"
                        type="text"
                        name="fax"
                        value={formValues.fax}
                        onChange={handleChange}
                      />
                    </div>

                    <div className="col-sm-12 mt-4">
                      <label className="form-label">Address</label>
                      <input
                        className="form-control"
                        id="inline-form-input"
                        type="text"
                        name="address"
                        value={formValues.address}
                        onChange={handleChange}
                      />
                    </div>
                  </div>
                </div>
                <div className="pull-right mt-3">
                  {/* <button
                    className="btn btn-secondary pull-right ms-3"
                    type="button"
                    onClick={handleBack}
                  >
                    Cancel & Go Back
                  </button> */}
                  <button
                    className="btn btn-primary"
                    type="button"
                    onClick={handleSubmit}
                  >
                    Save Details
                  </button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </main>
    </div>
  );
}
export default ContactInfo;

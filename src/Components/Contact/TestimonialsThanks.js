import React, { useState } from "react";
import Loader from "../../Pages/Loader/Loader.js";
import Toaster from "../../Pages/Toaster/Toaster.js";

const TestimonialsThanks = () => {
  
  const [loader, setLoader] = useState({ isActive: null });
  const { isActive } = loader;
  const [toaster, setToaster] = useState({
    type: null,
    isShow: null,
    toasterBody: "",
    message: "",
  });
  const { type, isShow, toasterBody, message } = toaster;

  return (
    <div className="bg-secondary float-left w-100">
          <Loader isActive={isActive} />
            {isShow && <Toaster type={type} isShow={isShow} toasterBody={toasterBody} message={message} />}
      <div className="container">
        <main className="page-wrapper homepage_layout">
          <section className="d-flex align-items-center position-relative pb-5">
            <div className="container">
              <div className="row">
                <div className="col-lg-6 col-md-6 offset-lg-2 offset-md-1 text-md-start text-center">
                  <h3 className="display-3 mb-4 pb-md-3 mt-4">
                  Thanks
                  </h3>
                  <p className="text-white lead mb-md-5 mb-4">
                  Thank you for taking the time to rate your experience. Your feedback is valuable to us and helps us improve our services. 
                  </p>
                </div>
              </div>
            </div>
          </section>
        </main>
      </div>
    </div>
  );
};

export default TestimonialsThanks;

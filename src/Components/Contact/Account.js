import React, { useState, useEffect } from 'react';
import Pic from "../img/pic.png";
import { NavLink } from 'react-router-dom';
import user_service from '../service/user_service';
import axios from "axios";
import Loader from "../../Pages/Loader/Loader.js";
import Toaster from '../../Pages/Toaster/Toaster.js';
import jwt from "jwt-decode";

const Account = () => {
    const [loader, setLoader] = useState({ isActive: null });
    const { isActive } = loader;
    const [toaster, setToaster] = useState({ types: null, isShow: null, toasterBody: "", message: "" });
    const { types, isShow, toasterBody, message } = toaster;
    const initialValues = {
        address: "", fax: "", inbox: "",
        web: "", name: "", image: "", phone: ""
    };
    const [data, setData] = useState("")
    const [formValues, setFormValues] = useState(initialValues);
    const [profile, setProfile] = useState(initialValues);
    const [file, setFile] = useState(null);
    const [fileExtension, setFileExtension] = useState('');
    const [acceptedFileTypes, setAcceptedFileTypes] = useState([]);
    const [showSubmitButton, setShowSubmitButton] = useState(false);

    useEffect(() => { window.scrollTo(0, 0);
        const profileGetAll = async () => {
            setLoader({ isActive: true });
            await user_service.profileGet(jwt(localStorage.getItem("auth")).id).then((response) => {
                if (response) {
                    setLoader({ isActive: false });
                    setProfile(response.data);
                }
            });
        };
        profileGetAll();
    }, []);

    const Upload = async (e) => {
        const selectedFile = e.target.files[0];

        if (selectedFile) {
            setFile(selectedFile);
            setFileExtension(selectedFile.name.split('.').pop());
            setShowSubmitButton(true);

            const formData = new FormData();
            formData.append('file', selectedFile);

            const config = {
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'multipart/form-data',
                    Authorization: `Bearer ${localStorage.getItem('auth')}`,
                },
            };

            try {

                const uploadResponse = await axios.post('https://api.brokeragentbase.com/upload', formData, config);
                const uploadedFileData = uploadResponse.data;
                console.log(uploadedFileData);
                setData(uploadedFileData);

                const updatedProfile = {
                    ...profile,
                    image: uploadedFileData
                };
                console.log(updatedProfile);


                setLoader({ isActive: true });
                await user_service.profilePost(updatedProfile, jwt(localStorage.getItem("auth")).id).then((response) => {
                    if (response) {
                        const profileGetAll = async () => {
                            setLoader({ isActive: true });
                            await user_service.profileGet(jwt(localStorage.getItem("auth")).id).then((response) => {
                                if (response) {
                                    setLoader({ isActive: false });
                                    setProfile(response.data);
                                }
                            });
                        };
                        profileGetAll();

                        setLoader({ isActive: false });
                        setToaster({
                            type: "Profile Update",
                            isShow: true,
                            toasterBody: response.data.message,
                            message: "Profile Update Successfully",
                        });

                        setTimeout(() => {
                            setToaster((prevToaster) => ({ ...prevToaster, isShow: false }));
                        }, 500);


                    } else {
                        setLoader({ isActive: false });
                        setToaster({
                            type: "error",
                            isShow: true,
                            toasterBody: response.data.message,
                            message: "Error",
                        });

                        setTimeout(() => {
                            setToaster((prevToaster) => ({ ...prevToaster, isShow: false }));
                        }, 500);
                    }
                });


            } catch (error) {
                console.error('Error occurred during file upload:', error);
                setLoader({ isActive: false });
            }
        }
    };

    const profilepic = (e) => {
        e.target.src = Pic;
    }
    return (
        <div className="bg-secondary float-left w-100 pt-4">
            <Loader isActive={isActive} />
            {isShow && <Toaster types={types}
                isShow={isShow}
                toasterBody={toasterBody}
                message={message} />}
            <main className="page-wrapper profile_page_wrap">

                <div className="container content-overlay mt-3">
                    <div className="">
                        <h4 className="nav-item mb-md-0 me-md-2 pe-md-1 mb-5" id="profile"><a className="nav-link active" href="#" aria-current="page">Account</a></h4>


                        <div className="row">
                            <div className="col-lg-3">
                                <img className="rounded-circle profile_picture" src={profile.image || Pic} alt="Profile"  onError={e => profilepic(e)} />
                                <label className="documentlabel" id="uploadlabel">
                                    <input
                                        className=""
                                        id="REPC_real_estate_purchase_contract"
                                        type="file"
                                        accept={acceptedFileTypes.map((type) => `.${type}`).join(',')}
                                        name="file"
                                        onChange={Upload}
                                        value={formValues.file}
                                    />
                                    <i className="fa fa-pencil-square-o" aria-hidden="true"></i>
                                </label>

                                <p className="mt-0 p-2"><strong>{profile.name}</strong></p>

                            </div>
                            <div className="col-lg-9 mt-5">
                                <div className="collapse d-md-block" id="account-nav">
                                    <ul className="nav nav-pills flex-column flex-md-row pt-3 pt-md-0 pb-md-4 border-bottom-md">
                                        <li className="nav-item mb-md-0 me-md-2 pe-md-1"><NavLink className="nav-link" to="/profile" aria-current="page"><i className="fi-heart mt-n1 me-2 fs-base"></i>Profile</NavLink></li>
                                        <li className="nav-item mb-md-0 me-md-2 pe-md-1"><NavLink className="nav-link" to="/profile-account"><i className="fi-bell mt-n1 me-2 fs-base"></i>Account</NavLink></li>
                                        <li className="nav-item mb-md-0 me-md-2 pe-md-1"><NavLink className="nav-link" to="/profile-activity"><i className="fi-file mt-n1 me-2 fs-base"></i>Activity</NavLink></li>
                                        <li className="nav-item mb-md-0"><NavLink className="nav-link" to="/profile-setting"><i className="fi-settings mt-n1 me-2 fs-base"></i>Settings</NavLink></li>
                                    </ul>
                                </div>
                            </div>

                            <div className="col-lg-3">
                                <h2 className="h4" id="info">Membership Info</h2>
                            </div><br />
                            <div className="col-lg-9 mt-5">
                                <div className="border rounded-3 p-3" id="auth-info">
                                    <div className="border-bottom pb-3 mb-3">
                                        <div className="d-flex align-items-center justify-content-between">
                                            <div className="pe-2 mt-2">
                                                <label className="form-label fw-bold">Membership Plan</label>
                                                <div id="emailValue">Standard Plan -  View Details </div>
                                            </div>
                                        </div>
                                        <div className="pe-2 mt-2">
                                            <label className="form-label fw-bold">Membership Plan Date</label>
                                            <div id="emailValue">Not set</div>
                                        </div>
                                        <div className="pe-2 mt-2">
                                            <label className="form-label fw-bold">Billing Date</label>
                                            <div id="emailValue">Not set</div>
                                        </div>
                                        <div className="pe-2 mt-2">
                                            <label className="form-label fw-bold">Subscription Level</label>
                                            <div id="emailValue">Full Account </div>
                                        </div>
                                        <div className="pe-2 mt-2">
                                            <label className="form-label fw-bold">Integrations</label>
                                            <div id="emailValue">Not set</div>
                                        </div><br />

                                        <div className="pe-2 mt-2">
                                            <label className="form-label fw-bold">Account Status</label>
                                            <div id="emailValue">Active</div>
                                        </div>

                                        <div className="pe-2 mt-2">
                                            <label className="form-label fw-bold">Intranet ID</label>
                                            <div id="emailValue">bhatt.crimson@gmail.com </div>
                                        </div>
                                        {/* <div className="pe-2 mt-2">
                                            <label className="form-label fw-bold">Account Password</label>
                                            <div id="emailValue">
                                                <p> <a href="#"> Request Change<br />Change Password - Send Reset Link</a></p>
                                            </div>
                                        </div> */}
                                    </div>

                                </div>

                            </div>
                        </div>

                        <div className="row">
                            <div className="col-lg-3">
                                <h2 className="h4" id="info">Financial</h2>
                            </div><br />

                            <div className="col-lg-9 mt-5">
                                <div className="border rounded-3 p-3" id="auth-info">
                                    <div className="border-bottom pb-3 mb-3">
                                        <div className="pe-2 mt-2">
                                            <h4>Office</h4>
                                        </div><br />
                                        <div className="pe-2 mt-2">
                                            <label className="form-label fw-bold">Assigned Office</label>
                                            <div id="emailValue">Corporate </div>
                                        </div>
                                        <div className="pe-2 mt-2">
                                            <label className="form-label fw-bold">Accounting Group</label>
                                            <div id="emailValue">InDepth Realty</div>
                                        </div>
                                        <div className="pe-2 mt-2">
                                            <label className="form-label fw-bold">Management Level</label>
                                            <div id="emailValue">1 - Office Support </div>
                                        </div>

                                        <div className="pe-2 mt-2">
                                            <label className="form-label fw-bold">Building Cards</label>
                                            <div id="emailValue">Not set</div>
                                        </div>
                                        <div className="pe-2 mt-2">
                                            <label className="form-label fw-bold">Office Keys</label>
                                            <div id="emailValue">Not set</div>
                                        </div>
                                        <div className="pe-2 mt-2">
                                            <label className="form-label fw-bold">Office Notes</label>
                                            <div id="emailValue">Not set</div>
                                        </div><br />

                                        <div className="pe-2 mt-2">
                                            <h4>Paperwork</h4>
                                            <div id="emailValue">0 paperwork documents on file</div>
                                        </div>
                                    </div>

                                </div>

                            </div>
                        </div>

                        <div className="row">
                            <div className="col-lg-3">
                                <h2 className="h4" id="info">License</h2>
                            </div><br />

                            <div className="col-lg-9 mt-5">
                                <div className="border rounded-3 p-3" id="auth-info">
                                    <div className="border-bottom pb-3 mb-3">
                                        <div className="pe-2 mt-2">
                                            <label className="form-label fw-bold">License Type</label>
                                            <div id="emailValue">Not set</div>
                                        </div>
                                        <div className="pe-2 mt-2">
                                            <label className="form-label fw-bold">License Number</label>
                                            <div id="emailValue">Not set</div>
                                        </div>
                                        <div className="pe-2 mt-2">
                                            <label className="form-label fw-bold">License Expires</label>
                                            <div id="emailValue">Not set</div>
                                        </div>

                                        <div className="pe-2 mt-2">
                                            <label className="form-label fw-bold">NRDS ID</label>
                                            <div id="emailValue">Not set</div>
                                        </div>
                                        <div className="pe-2 mt-2">
                                            <label className="form-label fw-bold">MLS Membership</label>
                                            <div id="emailValue">Not set</div>
                                        </div>
                                    </div>

                                </div>

                            </div>
                        </div>

                        <div className="row">
                            <div className="col-lg-3">
                                <h2 className="h4" id="info">Legal</h2>
                            </div><br />

                            <div className="col-lg-9 mt-5">
                                <div className="border rounded-3 p-3" id="auth-info">
                                    <div className="border-bottom pb-3 mb-3">
                                        <div className="pe-2 mt-2">
                                            <label className="form-label fw-bold">Government ID Number</label>
                                            <div id="emailValue">Not set</div>
                                        </div>
                                        <div className="pe-2 mt-2">
                                            <label className="form-label fw-bold">Date Of Birth</label>
                                            <div id="emailValue">Not set</div>
                                        </div>
                                        <div className="pe-2 mt-2">
                                            <label className="form-label fw-bold">SSN / ITIN</label>
                                            <div id="emailValue">Zeroed Out </div>
                                        </div>

                                        <div className="pe-2 mt-2">
                                            <label className="form-label fw-bold">EIN / DBA</label>
                                            <div id="emailValue">Not set</div>
                                        </div>
                                        <div className="pe-2 mt-2">
                                            <label className="form-label fw-bold">Transfer From</label>
                                            <div id="emailValue">Not set</div>
                                        </div>
                                        <div className="pe-2 mt-2">
                                            <label className="form-label fw-bold">Sponsored By</label>
                                            <div id="emailValue">Not set</div>
                                        </div>
                                    </div>

                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </main>
            

        </div >
    )
}

export default Account
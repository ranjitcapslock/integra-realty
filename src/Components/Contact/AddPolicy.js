import React, { useState, useEffect, useRef } from "react";
import Loader from "../../Pages/Loader/Loader.js";
import Toaster from "../../Pages/Toaster/Toaster.js";
import { NavLink, useParams } from "react-router-dom";
import { useNavigate } from "react-router-dom";
import user_service from "../service/user_service";

// const apiKey = "gtzihoqhxnyb7apgj00x766eel9cqv0herq11druhd2j6hki";

// console.log(apiKey, "tiny api key");
const AddPolicy = () => {
  const navigate = useNavigate();

  const [loader, setLoader] = useState({ isActive: null });
  const { isActive } = loader;
  const [toaster, setToaster] = useState({
    types: null,
    isShow: null,
    toasterBody: "",
    message: "",
  });
  const { types, isShow, toasterBody, message } = toaster;
  const params = useParams();
  const initialValues = {
    policy: "",
  };
  const [formValues, setFormValues] = useState(initialValues);
  const [reservationData, setReservationData] = useState("");

  {
    /* <!-- Input onChange Start--> */
  }
  const handleChange = (e) => {
    const { name, value } = e.target;
    setFormValues({ ...formValues, [name]: value });
  };

  const reservationGetIdData = async () => {
    await user_service.reservationGetId(params.id).then((response) => {
      if (response) {
        setLoader({ isActive: false });
        console.log(response);
        setFormValues(response.data);
      }
    });
  };

  useEffect(() => {
    reservationGetIdData();
  }, []);

  // {/* <!-- Form onSubmit Start--> */ }

  const handleSubmit = async (e) => {
    if(params.id){
        const userData = {
          policy: formValues.policy,
        };
        console.log("userData", userData);
    
        try {
          setLoader({ isActive: true });
          const response = await user_service.reservationUpdate(params.id, userData);
          setLoader({ isActive: false });
          if (response) {
            console.log(response.data);
            setLoader({ isActive: false });
            setToaster({
              types: "Add Policy",
              isShow: true,
              toasterBody: response.data.message,
              message: "Policy Added Successfully",
            });
              setTimeout(() => {
                navigate(`/reservation-calender/${params.id}`)
              }, 1000);
          } else {
            setLoader({ isActive: false });
            setToaster({
              types: "error",
              isShow: true,
              toasterBody: response.data.message,
              message: "Error",
            });
          }
        } catch (error) {
          setLoader({ isActive: false });
          setToaster({
            types: "error",
            isShow: true,
            toasterBody: error,
            message: "Error",
          });
        }
    }
  };

  {
    /* <!-- Form onSubmit End--> */
  }

  const handleCancel  = ()=>{
    navigate(`/reservation-calender/${params.id}`)
  }

  return (
    <div className="bg-secondary float-left w-100 pt-4 mb-4">
      <Loader isActive={isActive} />
      {isShow && (
        <Toaster
          types={types}
          isShow={isShow}
          toasterBody={toasterBody}
          message={message}
        />
      )}
      <main className="page-wrapper">
        {/* <!-- Page container--> */}
        <div className="">
           
          <div className="float-left w-100 d-flex align-items-center justify-content-between mb-4">
            <h3 className="text-white mb-0">Usage Policy</h3>
            <div className="pull-right">
            <button className="btn btn-secondary" onClick={handleCancel}>Cancel</button>

            <button className="btn btn-primary ms-2" onClick={handleSubmit}>{formValues.policy? "Update Policy" :"Add Policy"}</button>
            </div>
          </div>
       
          <div className="row">
            <div className="col-md-12">
              <div className="border bg-light rounded-3 p-3">
                <div className="bg-light rounded-3 mb-3 mt-4">
                  <h6>
                    Resource Usage Policy for {reservationData.calender_name}
                  </h6>
                  <p>
                    Provide the rules for reserving and requesting this
                    resource.
                  </p>
                  <div className="float-left w-100 mt-2">
                    <label className="form-label">Resource Usage Policy</label>
                    <textarea
                          className="form-control mt-0"
                          id="textarea-input"
                          rows="5"
                          name="policy"
                          onChange={handleChange}
                          autoComplete="on"
                          value={formValues.policy}
                        ></textarea>
                  </div>
                </div>
              </div>

            </div>
          </div>
        </div>
      </main>
    </div>
  );
};

export default AddPolicy;

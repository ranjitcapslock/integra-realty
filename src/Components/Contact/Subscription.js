import React, { useState, useEffect } from 'react';
import { NavLink, useNavigate, useParams } from 'react-router-dom';
import user_service from '../service/user_service';
import ICAL from 'ical.js';
import jwt from "jwt-decode";

const Subscription = () => {
  const [toaster, setToaster] = useState({
    types: null,
    isShow: null,
    toasterBody: "",
    message: "",
  });

  const [loader, setLoader] = useState({ isActive: null });
  
  const { isActive } = loader;
  
  const params = useParams();
  const type =
  params.type === "personal" ? "Personal" :
  params.type === "office" ? "Office" :
  params.type === "staff" ? "Staff" :
  ""; 


  const publicType =  params.type === "public" ? "Public" : "";
  
  const [calenderGet, setCalenderGet] = useState([]);
  const [calenderMarketGet, setCalenderMarketGet] = useState([]);
  const [icalUrl, setIcalUrl] = useState('');
  
  const fetchCalendar = async () => {
    try {
      setLoader({ isActive: true });
      let queryParams;
      if (
        localStorage.getItem("auth") &&
        jwt(localStorage.getItem("auth")).contactType == "admin" ||jwt(localStorage.getItem("auth")).contactType == "staff"
      ) {
        queryParams = `?page=1`;
      } else {
        queryParams = `?page=1&agentId=` + jwt(localStorage.getItem("auth")).id;
      }

      if (type) {
        queryParams += `&category=${type}`;
      }

      const response = await user_service.CalenderGet(queryParams);

      setLoader({ isActive: false });

      if (response && response.data) {
        setCalenderGet(response.data.data);
      }
    } catch (error) {
      console.error("Error in fetchCalendar:", error);
      // Handle errors as needed, e.g., show a user-friendly error message
    }
  };

  function formatDateToICalendar(dateString) {
    const date = new Date(dateString);
  
    if (isNaN(date)) {
      console.error("Invalid date:", dateString);
      return '00000000T000000Z'; // Return a default value for invalid dates
    }
  
    const year = date.getFullYear();
    const month = String(date.getMonth() + 1).padStart(2, '0'); // Add leading zero
    const day = String(date.getDate()).padStart(2, '0'); // Add leading zero
    const hours = String(date.getHours()).padStart(2, '0'); // Add leading zero
    const minutes = String(date.getMinutes()).padStart(2, '0'); // Add leading zero
    const seconds = String(date.getSeconds()).padStart(2, '0'); // Add leading zero
  
    return `${year}${month}${day}T${hours}${minutes}${seconds}Z`;
  }
 
  const createVCalendar = () => {
    const vcalendar = new ICAL.Component(['vcalendar', [], []]);
  
    vcalendar.addPropertyWithValue('VERSION', '2.0');
    vcalendar.addPropertyWithValue('CALSCALE', 'GREGORIAN');
    vcalendar.addPropertyWithValue('X-WR-CALNAME', `${type}`);
  
    calenderGet.forEach((eventData) => {
      const vevent = new ICAL.Component(['vevent', [], []]);
  
      const dtStartFormatted = formatDateToICalendar(eventData.startDate);
      const dtEndFormatted = formatDateToICalendar(eventData.endDate);
      const dtStampFormatted = formatDateToICalendar(new Date().toISOString()); // Current time
  
      vevent.addPropertyWithValue('UID', `ID_${eventData._id}`);
      vevent.addPropertyWithValue('SUMMARY', eventData.title);
      vevent.addPropertyWithValue('ORGANIZER', eventData.organizer || '');
      vevent.addPropertyWithValue('LOCATION', eventData.venue || '');
      vevent.addPropertyWithValue('DTSTART', dtStartFormatted);
      vevent.addPropertyWithValue('DTEND', dtEndFormatted);
      vevent.addPropertyWithValue('DTSTAMP', dtStampFormatted);
      vevent.addPropertyWithValue('DESCRIPTION', eventData.eventDescription || '');
  
      vcalendar.addSubcomponent(vevent);
    });

    const blob = new Blob([vcalendar.toString()], { type: 'text/calendar' });
    const url = URL.createObjectURL(blob);
    setIcalUrl(url);
  
    return vcalendar;
  };
  
  const saveICalFile = () => {
    const icsData = createVCalendar();
    const blob = new Blob([icsData], { type: 'text/calendar' });
    const url = URL.createObjectURL(blob);
    setIcalUrl(url); // Set the Blob URL for further use

    const link = document.createElement('a');
    link.href = url;
    link.download = 'calendar.ics';

    // Trigger download
    document.body.appendChild(link);
    link.click();
    document.body.removeChild(link);
  };
console.log(icalUrl, "icalUrl")

  useEffect(() => {
    window.scrollTo(0, 0);
    fetchCalendar();
    fetchMarketCalendar()
  }, []);

  useEffect(() => {
    // This useEffect runs when calenderGet changes
    if(type){
      if (calenderGet.length > 0) {
        // Call saveICalFile when calenderGet is not empty
        saveICalFile();
      }
    }
  }, [calenderGet]);

  
  const fetchMarketCalendar = async () => {
    try {
      setLoader({ isActive: true });
      let queryParams;
      if (
        localStorage.getItem("auth") &&
        jwt(localStorage.getItem("auth")).contactType == "admin"
      ) {
        queryParams = `?page=1`;
      } else {
        queryParams = `?page=1&agentId=` + jwt(localStorage.getItem("auth")).id;
      }
      const response = await user_service.MarketCalenderGet(queryParams);

      setLoader({ isActive: false });

      if (response && response.data) {
        setCalenderMarketGet(response.data.data);
      }
    } catch (error) {
      console.error("Error in fetchCalendar:", error);
      // Handle errors as needed, e.g., show a user-friendly error message
    }
  };

  
  // Example Usage in createVCalendarMarket
  const createVCalendarMarket = () => {
    const vcalendar = new ICAL.Component(['vcalendar', [], []]);
  
    vcalendar.addPropertyWithValue('VERSION', '2.0');
    vcalendar.addPropertyWithValue('CALSCALE', 'GREGORIAN');
    vcalendar.addPropertyWithValue('X-WR-CALNAME', `${publicType}`);
  
    calenderMarketGet.forEach((eventData) => {
      const vevent = new ICAL.Component(['vevent', [], []]);
  
      const dtStartFormatted = formatDateToICalendar(eventData.marketDate);
      const dtEndFormatted = formatDateToICalendar(eventData.marketEnd);
      const dtStampFormatted = formatDateToICalendar(new Date().toISOString()); // Current time
  
      vevent.addPropertyWithValue('UID', `ID_${eventData._id}`);
      
      // Use ATTACH for images or links to resources
      if (eventData.image) {
        vevent.addPropertyWithValue('ATTACH', eventData.image);
      }
      
      vevent.addPropertyWithValue('SUMMARY', eventData.marketText);
      vevent.addPropertyWithValue('ORGANIZER', eventData.office_name || '');
      vevent.addPropertyWithValue('DTSTART', dtStartFormatted);
      vevent.addPropertyWithValue('DTEND', dtEndFormatted);
      vevent.addPropertyWithValue('DTSTAMP', dtStampFormatted);
  
      vcalendar.addSubcomponent(vevent);
    });
  
    return vcalendar;
  };
  
  useEffect(() => {
    if(publicType){
    if (calenderMarketGet.length > 0) {
      saveICalFileMarket();
    }
  }
  }, [calenderMarketGet]);


  
  const saveICalFileMarket = () => {
    const vcalendar = createVCalendarMarket();
    const icsData = vcalendar.toString();

    const blob = new Blob([icsData], { publicType: 'text/calendar' });
    const link = document.createElement('a');
    link.href = window.URL.createObjectURL(blob);
    link.download = 'my_calendar.ics';

    document.body.appendChild(link);
    link.click();
    document.body.removeChild(link);
  };

  return (
    <div className="bg-secondary float-left w-100 pt-4 mb-4">
      <div>
      {icalUrl && (
        <>
        {console.log(icalUrl)}
          <a href={icalUrl} target="_blank" rel="noopener noreferrer">
            Download iCalendar
          </a>
        </>
        )}
      </div>
    </div>
  );
};

export default Subscription;
    
    
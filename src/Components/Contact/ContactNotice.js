import React, { useState, useEffect, useRef } from "react";
import Pic from "../img/pic.png";
import user_service from "../service/user_service";
import ReactPaginate from "react-paginate";
import Loader from "../../Pages/Loader/Loader.js";
import Toaster from "../../Pages/Toaster/Toaster.js";
import { NavLink, useNavigate, useParams } from "react-router-dom";
import jwt from "jwt-decode";
// import $, { each } from "jquery";
// import Select2 from "react-select2-wrapper";
// import "react-select2-wrapper/css/select2.css";
// import de from "date-fns/locale/de/index";

function ContactNotice() {
  const [getContact, setGetContact] = useState([]);
    const [pageCount, setPageCount] = useState(0);
    const [pageCountreceived, setPageCountreceived] = useState(0);
    const [pageCountsend, setPageCountsend] = useState(0);
    const [totalRecords, setTotalRecords] = useState("");
    const [loader, setLoader] = useState({ isActive: null })
    const { isActive } = loader;
    const [toaster, setToaster] = useState({ types: null, isShow: null, toasterBody: "", message: "" });
    const { types, isShow, toasterBody, message } = toaster;


    const [query, setQuery] = useState('');
    const [select, setSelect] = useState("");
    const [results, setResults] = useState([]);
    const [currentPage, setCurrentPage] = useState(1);
    const [isLoading, setIsLoading] = useState(false);


   
    const [noticecategory, setNoticecategory] = useState("");
    const [noticetitle, setNoticetitle] = useState("");
    const [noticemessage, setNoticemessage] = useState("");
    const [noticeid, setNoticeid] = useState("");

    const [noticereceived, setNoticereceived] = useState("");
    const noticeGetAllreceived = async () => {
      if (localStorage.getItem("auth")) {
        setLoader({ isActive: true })
        const userid = jwt(localStorage.getItem("auth")).id;
        // var query = `?agentId=${userid}&pin=yes`;
        var query = `?receiverId=${userid}`;
          await user_service.noticegetallconditional(query).then((response) => {
              if (response) {
                setLoader({ isActive: false })
                  setNoticereceived(response.data.data);
                  setPageCountreceived(Math.ceil(response.data.totalRecords / 10));
              }
          });
      }
    };

    const [noticesend, setNoticesend] = useState("");
    const noticeGetAllsend = async () => {
      if (localStorage.getItem("auth")) {
        setLoader({ isActive: true })
        const userid = jwt(localStorage.getItem("auth")).id;
        // var query = `?agentId=${userid}&pin=yes`;
        var query = `?agentId=${userid}`;
          await user_service.noticegetallconditional(query).then((response) => {
              if (response) {
                  setLoader({ isActive: false })
                  setNoticesend(response.data.data);
                  setPageCountsend(Math.ceil(response.data.totalRecords / 10));
              }
          });
      }
    };

    useEffect(() => { window.scrollTo(0, 0);
      noticeGetAllreceived();
      noticeGetAllsend();
    }, []);




    const handlePageClick = async (data) => {
        let currentPage = data.selected + 1;
        let skip = (currentPage - 1) + currentPage;
        setLoader({ isActive: true })
        console.log(currentPage);
        await user_service.contactGetassoc(currentPage,"associate").then((response) => {
            setLoader({ isActive: false })
            if (response) {
               // setResults(response.data.data);
                setGetContact(response.data.data);
                setPageCount(Math.ceil(response.data.totalRecords / 10));
            }
        });
    };



    {/* <!-- paginate Function Search Api call Start--> */ }
    useEffect(() => { window.scrollTo(0, 0);
        if (query) {
            SearchGetAll(1);
        }
    }, [query]);

    const SearchGetAll = async () => {
        setIsLoading(true);
        await user_service.SearchContactGet(1, query, query, query).then((response) => {
            setIsLoading(false);
            if (response) {
                // setResults(response.data.data);
                setGetContact(response.data.data);
                setPageCount(Math.ceil(response.data.totalRecords / 10));
            }
        });
    };
   
    const navigate = useNavigate();
    
    const handleSubmit = (id)=>{
       console.log(id);
       navigate(`/contact-profile/${id}`)
    }

    
    const handleview = (data)=>{
      setNoticecategory(data.category);
      setNoticetitle(data.title);
      setNoticemessage(data.message);
      setNoticeid(data._id);
   }

    {/* <!-- paginate Function Search Api call End--> */ }

    const handleSearch = (event) => {
        event.preventDefault();
        setCurrentPage(1);
        SearchGetAll(1);
    };

    function getFormattedDateTime(dateString) {
      const options = { year: "numeric", month: "2-digit", day: "2-digit", hour: "2-digit", minute: "2-digit", second: "2-digit" };
      const date = new Date(dateString);
      return date.toLocaleString("en-US", options);
    }

    const confirmnotice = async (id) => {
      if(id){
          
          const userData = {
              isview: "yes",
              viewDate: new Date(),
          };
          setLoader({ isActive: true })
          await user_service.noticeUpdate(id, userData).then((response) => {
              if (response) {
                  setLoader({ isActive: false })
                  document.getElementById("noticemodalclose").click();
              } else {
                  setLoader({ isActive: false })
                  setToaster({ types: "error", isShow: true, toasterBody: response.data.message, message: "Error", });
              }
          })
      }
}
    return (
        <div className="bg-secondary float-left w-100 pt-4 mb-4">
            <Loader isActive={isActive} />
            {isShow && <Toaster types={types}
                isShow={isShow}
                toasterBody={toasterBody}
                message={message} />}
            <main className="page-wrapper">
                {/* <!-- Page container--> */}
                <div className="content-overlay">
                    {/* <!-- Breadcrumb--> */}
                    {/* <nav className="mb-3 mb-md-4 pt-md-3" aria-label="Breadcrumb">
                        <ol className="breadcrumb breadcrumb-dark">
                            <li className="breadcrumb-item"><a href="/">Home</a></li>
                            <li className="breadcrumb-item"><a href="/">Reports</a></li>
                            <li className="breadcrumb-item active" aria-current="page">Notice</li>
                        </ol>
                    </nav> */}

                    {/* <!-- Page card like wrapper--> */}
                    <div className="">
                        <div className="row">

                            {/* <!-- List of resumes--> */}
                            <div className="col-md-12">
                                <h3 className="mb-4 text-white">Office Notices</h3>
                                {/* <!-- Normal search form group --> */}
                                {/* <form onSubmit={handleSearch}>
                                    <p className="mt-5">Quick Search: <a href="">Show All</a></p>
                                    <div className="d-flex justify-content-start align-items-center">
                                        <div className="form-outline">
                                            <input className="form-control" type="search" id="search-input-1" placeholder="Search All"
                                                valvalue={query} onChange={(event) => setQuery(event.target.value)} />
                                        </div>
                                        <span className="ms-4">
                                            <button type="button" className="btn btn-translucent-primary">Find</button>
                                            <a href="#" id="button-item" type="button">Reset</a>
                                        </span>

                                    </div>
                                 
                                </form> */}
                                
                                 <div className="stories_blog_section bg-light border rounded-3 p-3">
                                    <div className="tabbable" id="tabs-459476">
                                      <ul className="nav nav-tabs">
                                        <li
                                            className="nav-item"
                                           // onClick={(e) => CatagoryBlend("OfficePost")}
                                          >
                                          <a className="nav-link" href="#tab1" data-toggle="tab">
                                            Received
                                          </a>
                                        </li>
                                        
                                        <li className="nav-item">
                                          <a className="nav-link" href="#tab2" data-toggle="tab">
                                          Sent
                                          </a>
                                        </li>
                                        
                                      </ul>
                                      <div className="tab-content">
                                        <div className="tab-pane active" id="tab1">
                                            {
                                            noticesend && noticesend.length > 0 ?
                                            noticesend.map((post) => {
                                              return (
                                                      <div className="card card-hover mb-2 mt-4">
                                                      <div className="card-body">
                                                        <a href="" data-toggle="modal"  onClick={()=>handleview(post)} data-target="#noticemodalview">
                                                          <div className="float-left w-100">
                                                              <div className="float-left w-100 d-flex align-items-center justify-content-between">
                                                                  <div className="float-left w-100 d-flex align-items-center justify-content-start" key={post._id}>
                                                                      <div className="">
                                                                          <strong>{post.category}</strong> 
                                                                          <p className="mb-1">{post.title}</p>
                                                                      </div>
                                                                  </div>
                                                                  <div className="float-left w-100 d-flex align-items-center justify-content-start"> 
                                                                    <div>
                                                                        <badge className="" style={{fontSize: "14px" }}><i className="fa fa-calendar-check-o me-2" aria-hidden="true"></i>Assigned:{ post.assignDate ? getFormattedDateTime(post.assignDate):"" }</badge> <br />
                                                              <badge className="" style={{ fontSize: "14px" }}><i className="fa fa-eye me-2" aria-hidden="true"></i>Viewed: { post.viewDate ? getFormattedDateTime(post.viewDate) :""}</badge>
                                                                    </div>
                                                                </div>
                                                              </div>
                                                          </div>
                                                          </a>
                                                      </div>
                                                  </div>
                                              );
                                            }
                                            )
                                            :""
                                          }
                                            {/* <div className="pagination_blog w-100 d-block mt-3">
                                              {totalRecords > 5 ? (
                                                <div className="justify-content-end mb-1">
                                                  <ReactPaginate
                                                    previousLabel={"Previous"}
                                                    nextLabel={"Next"}
                                                    breakLabel={"..."}
                                                    pageCount={pageCount}
                                                    marginPagesDisplayed={1}
                                                    pageRangeDisplayed={2}
                                                    onPageChange={handlePageClick}
                                                    containerClassName={
                                                      "pagination justify-content-center"
                                                    }
                                                    pageClassName={"page-item"}
                                                    pageLinkClassName={"page-link"}
                                                    previousClassName={"page-item"}
                                                    previousLinkClassName={"page-link"}
                                                    nextClassName={"page-item"}
                                                    nextLinkClassName={"page-link"}
                                                    breakClassName={"page-item"}
                                                    breakLinkClassName={"page-link"}
                                                    activeClassName={"active"}
                                                  />
                                                </div>
                                              ) : (
                                                ""
                                              )}
                                            </div> */}
                                        </div>
                                       
                                        <div className="tab-pane" id="tab2">
                                          {
                                              noticereceived && noticereceived.length > 0 ?
                                              noticereceived.map((post) => {
                                                return (
                                                        <div className="card card-hover mb-2 mt-4">
                                                        <div className="card-body">
                                                          <a href="" data-toggle="modal"  onClick={()=>handleview(post)} data-target="#noticemodalview">
                                                            <div className="float-left w-100">
                                                                <div className="float-left w-100 d-flex align-items-center justify-content-between">
                                                                    <div className="float-left w-100 d-flex align-items-center justify-content-start" key={post._id}>
                                                                        <div className="">
                                                                            <strong>{post.category}</strong> 
                                                                            <p className="mb-1">{post.title}</p>
                                                                        </div>
                                                                    </div>
                                                                    <div className="float-left w-100 d-flex align-items-center justify-content-start"> 
                                                                      <div>
                                                                          <badge className="" style={{fontSize: "14px" }}><i className="fa fa-calendar-check-o me-2" aria-hidden="true"></i>Assigned:{ post.assignDate ? getFormattedDateTime(post.assignDate):"" }</badge> <br />
                                                                          <badge className="" style={{ fontSize: "14px" }}><i className="fa fa-eye me-2" aria-hidden="true"></i>Viewed: { post.viewDate ? getFormattedDateTime(post.viewDate) :""}</badge>
                                                                      </div>
                                                                  </div>
                                                                </div>
                                                            </div>
                                                            </a>
                                                        </div>
                                                    </div>
                                                );
                                              }
                                              )
                                              :""
                                            }
                                        </div>

                                      </div>
                                    </div>
                                  </div>
                            </div>

                        </div>
                    </div>
                </div>

                <div className="modal" role="dialog" id="noticemodalview">
                <div className="modal-dialog" role="document">
                    <div className="modal-content">
                                <div className="modal-header">
                                    <h5>{noticecategory}</h5>
                                    <button type="button" className="btn btn-default btn btn-secondary btn-sm" id="noticemodalcloseview" data-dismiss="modal">CLOSE</button>
                                </div>
                                <div className="modal-body row">
                                    <div className=" col-md-12" >
                                        <div className="row">
                                            <div className="col-md-6 d-flex">
                                            <h6> <b> {noticetitle}</b></h6>
                                            </div>
                                        </div>
                                        <div className="row">
                                            <div className="col-md-6 d-flex" dangerouslySetInnerHTML={{ __html: noticemessage }} />
                                        </div>
                                        <div className="row">
                                            <div className="col-md-6 d-flex"><button type="button" className="btn btn-default btn btn-secondary btn-sm" onClick={(e) => confirmnotice(noticeid)} >Confirm & Close</button></div>
                                        </div>
                                    </div>
                                </div>  
                    </div>
                </div>
            </div>

            </main >
            

        </div >
    )
}
export default ContactNotice;

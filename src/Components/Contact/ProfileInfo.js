import React, { useState, useEffect } from "react";
import _ from "lodash";
import user_service from "../service/user_service";
import jwt from "jwt-decode";
import Loader from "../../Pages/Loader/Loader.js";
import Toaster from "../../Pages/Toaster/Toaster.js";
import { useNavigate, NavLink, useParams } from "react-router-dom";
import { MultiSelect } from "react-multi-select-component";

const options = [
  { value: "ABR", label: "ABR - Accredited Buyer Representative" },
  { value: "ABRM", label: "ABRM - Accredited Buyer Representative Manager" },
  { value: "ALC", label: "ALC - Accredited Land Consultant" },
  { value: "ALHS", label: "ALHS - Accredited Luxury Homes Specialist" },
  { value: "AREP", label: "AREP - Accredited Real Estate Professional" },
  { value: "ASR", label: "ASR - Accredited Seller Representative" },
  { value: "ASP", label: "ASP - Accredited Staging Professional" },
  { value: "AHS", label: "AHS - Affordable Housing Specialist" },
  { value: "APREP", label: "APREP - Appraisal Real Estate Professional" },
  { value: "AHWD", label: "AHWD - At Home with Diversity® Certification" },
  {
    value: "CMREP",
    label: "CMREP - Career Management Real Estate Professional",
  },
  { value: "CCIM", label: "CCIM - Certified Commercial Investment Member" },
  { value: "CDPE", label: "CDPE - Certified Distressed Property Expert" },
  { value: "CFRM", label: "CFRM - Certified Florida Residential Manager" },
  { value: "CFA", label: "CFA - Certified Foreclosure Agent" },
  { value: "CHMS", label: "CHMS - Certified Home Marketing Specialist" },
  {
    value: "CIPS",
    label: "CIPS - Certified International Property Specialist",
  },
  { value: "CMRS", label: "CMRS - Certified Military Residential Specialist" },
  { value: "CNE", label: "CNE - Certified Negotiation Expert" },
  { value: "CNS", label: "CNS - Certified Neighborhood Specialist" },
  { value: "CNHS", label: "CNHS - Certified New Home Specialist" },
  { value: "CRB", label: "CRB - Certified Real Estate Brokerage Manager" },
  { value: "CREI", label: "CREI - Certified Real Estate Instructor" },
  { value: "CRS", label: "CRS - Certified Residential Specialist" },
  { value: "CSSA", label: "CSSA - Certified Short Sale Agent" },
  { value: "CSS", label: "CSS - Certified Staging Specialist" },
  { value: "CREP", label: "CREP - Commercial Real Estate Professional" },
  { value: "CRE", label: "CRE - Counselor of Real Estate" },
  { value: "EREP", label: "EREP - e-Business Real Estate Professional" },
  { value: "GAA", label: "GAA - General Accredited Appraiser" },
  { value: "GHS", label: "GHS - Generational Housing Specialist" },
  { value: "GRI", label: "GRI - Graduate REALTOR® Institute" },
  { value: "GREEN", label: "GREEN - Green Designation" },
  { value: "HIREP", label: "HIREP - Home Inspection Real Estate Professional" },
  { value: "MCNE", label: "MCNE - Master Certified Negotiation Expert" },
  { value: "MRP", label: "MRP - Military Relocation Professional" },
  { value: "MREP", label: "MREP - Mortgage Real Estate Professional" },
  { value: "e-PRO", label: "e-PRO - NAR e-PRO Certification" },
  { value: "PMN", label: "PMN - Performance Management Network " },
  { value: "PSA", label: "PSA - Pricing Strategy Advisor" },
  { value: "QSC", label: "QSC - Quality Service Certified" },
  { value: "REPA", label: "REPA - Real Estate Professional Assistant" },
  { value: "RCE", label: "RCE - REALTOR® association Certified Executive" },
  { value: "RREP", label: "RREP - Referral Real Estate Professional" },
  { value: "RAA", label: "RAA - Residential Accredited Appraiser®" },
  {
    value: "RSPS",
    label: "RSPS - Resort &amp; Second-Home Markets Certification",
  },
  { value: "SRS", label: "SRS - Sellers Representative Specialist" },
  { value: "SRES", label: "SRES - Senior Real Estate Specialist" },
  { value: "SFR", label: "SFR - Short Sales &amp; Foreclosure Resource" },
  { value: "SIOR", label: "SIOR - Society of Industrial and Office REALTORS®" },
  { value: "TAHS", label: "TAHS - Texas Affordable Housing Specialist" },
  { value: "TRLP", label: "TRLP - Texas REALTORS Leadership Program" },
  { value: "TRC", label: "TRC - Transnational Referral Certification" },
  { value: "VAC", label: "VAC - VA Certified" },
];

const primaryAreaFields = [
  { type: "zip1", zipcode: "" },
  { type: "zip2", zipcode: "" },
  { type: "zip3", zipcode: "" },
];

const languagesField = [
  { type: "language1", language: "" },
  { type: "language2", language: "" },
  { type: "language3", language: "" },
];

const specialtiesField = [
  { type: "specialties1", specialties: "" },
  { type: "specialties2", specialties: "" },
  { type: "specialties3", specialties: "" },
];

const socialMediaTypes = [
  { type: "Twitter", linkurl: "" },
  { type: "Linkedin", linkurl: "" },
  { type: "Facebook", linkurl: "" },
  { type: "Instagram", linkurl: "" },
  { type: "Tumblr", linkurl: "" },
  { type: "YouTube Channel", linkurl: "" },
  { type: "Pinterest", linkurl: "" },
  { type: "TikTok", linkurl: "" },
  { type: "Vimeo", linkurl: "" },
];

function ProfileInfo() {
  const initialValues = {
    company: "",
    department: "",
    title: "",
    profession: "",
    industry: "",
    assistant: "",
    notes: "",
    social_links: "",
    birthday: "",
    gender: "",
    anniversary: "",
    spouse_name: "",
    web: "",
    tag_line: "",
    marketing_message: "",
    marketing_videourl: "",
    designation: "",
    languages: "",
    primary_areas: "",
    specialties: "",
  };
  const [show, setIsShow] = useState(false);

  const [formValues, setFormValues] = useState(initialValues);
  const [loader, setLoader] = useState({ isActive: null });
  const { isActive } = loader;
  const [toaster, setToaster] = useState({
    types: null,
    isShow: null,
    toasterBody: "",
    message: "",
  });
  const { types, isShow, toasterBody, message } = toaster;
  const [checkRadio, setCheckRadio] = useState("");

  const [socialLinks, setSocialLinks] = useState(socialMediaTypes);

  const [primaryAreas, setPrimaryAreas] = useState(primaryAreaFields);

  const [specialtiesInput, setSpecialtiesInput] = useState(specialtiesField);

  const [languageInput, setLanguageInput] = useState(languagesField);
  const [languageData, setLanguageData] = useState([]);

  const navigate = useNavigate();
  const params = useParams();

  const [selectedOptions, setSelectedOptions] = useState([]);

  const handleDesignationChange = (value) => {
    if (selectedOptions.includes(value)) {
      setSelectedOptions(selectedOptions.filter((option) => option !== value));
    } else {
      setSelectedOptions([...selectedOptions, value]);
    }
  };

  const handleCheck = (e) => {
    setCheckRadio(e.target.name);
    setCheckRadio(e.target.value);
  };

  const handleChangePrimary = (event, index) => {
    const { value } = event.target;
    const updatedPrimaryAreas = [...primaryAreas];
    updatedPrimaryAreas[index].zipcode = value;
    setPrimaryAreas(updatedPrimaryAreas);
  };

  const handleChangeSocial = (event, index) => {
    const { value } = event.target;
    setSocialLinks((prevSocialLinks) => {
      const updatedSocialLinks = [...prevSocialLinks];
      updatedSocialLinks[index] = {
        ...updatedSocialLinks[index],
        linkurl: value,
      };
      return updatedSocialLinks;
    });
  };

  const handleLanguageChange = (event, index) => {
    const { value } = event.target;
    setLanguageInput((prevSocialLinks) => {
      const updatedSocialLinks = [...prevSocialLinks];
      updatedSocialLinks[index] = {
        ...updatedSocialLinks[index],
        language: value,
      };
      return updatedSocialLinks;
    });
  };

  const handleSpecialtyChange = (event, index) => {
    const { value } = event.target;
    setSpecialtiesInput((prevSpecialtiesInput) => {
      const updatedSpecialtiesInput = [...prevSpecialtiesInput];
      updatedSpecialtiesInput[index] = {
        ...updatedSpecialtiesInput[index],
        specialties: value,
      };
      return updatedSpecialtiesInput;
    });
  };


  useEffect(() => {
    ContactGetById(params.id);
  }, [params.id]);


  const ContactGetById = async () => {
    try {
      const response = await user_service.contactGetById(params.id);
      setFormValues(response.data);
    } catch (error) {
      console.error("Error fetching contact data:", error);
    }
  };
  
  // onChange Function start
  const handleChange = (e) => {
    const { name, value } = e.target;
    setFormValues({ ...formValues, [name]: value });
  };
  // onChange Function end

  const handleSubmit = async (e) => {
    e.preventDefault();
    setIsShow(true);
    const userData = {
      agentId: jwt(localStorage.getItem("auth")).id,
      company: formValues.company,
      department: formValues.department,
      title: formValues.title,
      profession: formValues.profession,
      industry: formValues.industry,
      assistant: formValues.assistant,
      notes: formValues.notes,
      social_links: socialLinks,
      birthday: formValues.birthday,
      anniversary: formValues.anniversary,
      spouse_name: formValues.spouse_name,
      web: formValues.web,
      tag_line: formValues.tag_line,
      marketing_message: formValues.marketing_message,
      marketing_videourl: formValues.marketing_videourl,
      designations: JSON.stringify(selectedOptions),
      primary_areas: primaryAreas,
      languages: languageInput,
      specialties: specialtiesInput,
    };
    try {
      setLoader({ isActive: true });
      const response = await user_service.contactUpdate(params.id, userData);
      setFormValues(response.data);
      setLoader({ isActive: false });
      setToaster({
        type: "Profile Updated Successfully",
        isShow: true,
        toasterBody: response.data.message,
        message: "Profile Updated Successfully",
      });
      setTimeout(() => {
        setToaster((prevToaster) => ({ ...prevToaster, isShow: false }));
        // navigate(`/contact-profile/${params.id}`);
      }, 3000);
      ContactGetById(params.id);
    } catch (error) {
      setLoader({ isActive: false });
      setIsShow(false);
      setToaster({
        types: "error",
        isShow: true,
        toasterBody: error.response
          ? error.response.data.message
          : error.message,
        message: "Error",
      });
      setTimeout(() => {
        setToaster((prevToaster) => ({ ...prevToaster, isShow: false }));
      }, 3000);
    }
  };

  {
    /* <!-- Api call Form onSubmit End--> */
  }

  const handleBack = () => {
    navigate(`/contact-profile/${params.id}`);
  };

  useEffect(() => { 
    if (params.id) {
      const ContactGetById = async () => {
        try {
          const response = await user_service.contactGetById(params.id);
          const contactData = response.data;
          console.log(contactData)  ;
          setFormValues(contactData);

          if (contactData.primary_areas.length === 0) {
            setFormValues([]);
          } else {
            setPrimaryAreas(contactData.primary_areas);
          }
          if (contactData.social_links.length === 0) {
            setFormValues([]);
          } else {
            setSocialLinks(contactData.social_links);
          }

          if (contactData.specialties.length === 0) {
            setFormValues([]);
          } else {
            setSpecialtiesInput(contactData.specialties);
          }

          if (contactData.languages.length === 0) {
            setFormValues([]);
          } else {
            setLanguageInput(contactData.languages);
          }
        } catch (error) {
          console.error("Error fetching contact data:", error);
        }
      };
      ContactGetById(params.id);
    }
  }, [params.id]);

  return (
    <div className="float-left w-100">
      <Loader isActive={isActive} />
      {isShow && (
        <Toaster
          types={types}
          isShow={isShow}
          toasterBody={toasterBody}
          message={message}
        />
      )}
      <main className="page-wrapper profile_page_wrap mb-3">
        <div className="content-overlay">
          <div className="justify-content-center pb-sm-2">
              <div className="add_listing w-100">
                <div className="">
                  <div className="">
                  <div className="float-left w-100 d-flex align-items-center justify-content-between mb-3">
                    <h3 id="getContact" className="text-white pull-left mb-0">
                      Company
                    </h3>
                  </div>
                  <div className="float-left w-100 d-flex align-items-center justify-content-between mb-3">
                    <h6 className="text-white mb-0">
                      Publicly linked professional information
                    </h6>
                  </div>
                  <div className="border rounded-3 bg-light mb-3 rounded-3 p-3">
                    <div className="float-left w-100">
                     
                      <div className="col-sm-12">
                        <label className="form-label">Company Name</label>
                        <input
                          className="form-control"
                          id="inline-form-input"
                          type="text"
                          name="company"
                          value={formValues.company}
                          onChange={handleChange}
                        />
                      </div>

                      <div className="col-sm-12 mt-3">
                        <label className="form-label">Department</label>
                        <input
                          className="form-control"
                          id="inline-form-input"
                          type="text"
                          name="department"
                          value={formValues.department}
                          onChange={handleChange}
                        />
                      </div>

                      <div className="col-sm-12 mt-3">
                        <label className="form-label">Title</label>
                        <input
                          className="form-control"
                          id="inline-form-input"
                          type="text"
                          name="title"
                          value={formValues.title}
                          onChange={handleChange}
                        />
                      </div>

                      <div className="col-sm-12 mt-3">
                        <label className="form-label">Profession</label>
                        <input
                          className="form-control"
                          id="inline-form-input"
                          type="text"
                          name="profession"
                          value={formValues.profession}
                          onChange={handleChange}
                        />
                      </div>

                      <div className="col-sm-12 mt-3">
                        <label className="form-label">Industry</label>
                        <input
                          className="form-control"
                          id="inline-form-input"
                          type="text"
                          name="industry"
                          value={formValues.industry}
                          onChange={handleChange}
                        />
                      </div>

                      <div className="col-sm-12 mt-3">
                        <label className="form-label">Assistant Name</label>
                        <input
                          className="form-control"
                          id="inline-form-input"
                          type="text"
                          name="assistant"
                          value={formValues.assistant}
                          onChange={handleChange}
                        />
                      </div>

                      <div className="col-sm-12 mt-3">
                        <label className="form-label">Shared Note</label>
                        <textarea
                          className="form-control mt-0"
                          id="textarea-input"
                          rows="5"
                          name="notes"
                          onChange={handleChange}
                          autoComplete="on"
                          value={formValues.notes}
                        ></textarea>
                      </div>
                    </div>
                    <div className="float-left w-100 mt-3">
                      <h6 style={{ fontSize: 20 }}>Social</h6>
                      <h6 style={{ fontSize: 18 }}>Social media links.</h6>
                      <div className="col-sm-12 mt-4">
                        {socialLinks.map((socialLink, index) => (
                          <div key={index}>
                            <label className="form-label mt-3">
                              {socialLink.type} URL
                            </label>
                            <input
                              className="form-control"
                              type="text"
                              name="linkurl"
                              value={socialLink.linkurl}
                              onChange={(event) =>
                                handleChangeSocial(event, index)
                              }
                            />
                          </div>
                        ))}
                      </div>
                    </div>
                    <div className="float-left w-100 mt-3">
                      <h6 style={{ fontSize: 20 }}>Personal</h6>
                      <h6 style={{ fontSize: 18 }}>
                        Internally shared information.
                      </h6>

                      <div className="col-sm-12">
                        <label className="form-label" for="pr-birth-date">
                          Birthday
                        </label>
                        <input
                          className="form-control"
                          type="date"
                          id="inline-form-input"
                          name="birthday"
                          value={formValues.birthday}
                          onChange={handleChange}
                          data-datepicker-options='{"altInput": true, "altFormat": "F j, Y", "dateFormat": "Y-m-d"}'
                        />
                      </div>

                      <div className="col-sm-12 mt-3">
                        <label className="form-label" for="pr-birth-date">
                          Gender
                        </label>
                        <div
                          className="row  d-flex"
                          onClick={(e) => handleCheck(e)}
                        >
                          <div className="col-md-2">
                            <input
                              className="form-check-input "
                              id="form-radio-four"
                              type="radio"
                              name="gender"
                              value="male"
                            />
                            <label className="form-check-label ms-2">
                              Male
                            </label>
                          </div>

                          <div className="col-md-2">
                            <input
                              className="form-check-input "
                              id="form-radio-four"
                              type="radio"
                              name="gender"
                              value="female"
                            />
                            <label className="form-check-label ms-2">
                              Female
                            </label>
                          </div>
                        </div>
                      </div>

                      <div className="col-sm-12 mt-3">
                        <label className="form-label">Anniversary</label>
                        <input
                          className="form-control"
                          id="inline-form-input"
                          type="date"
                          name="anniversary"
                          value={formValues.anniversary}
                          onChange={handleChange}
                        />
                      </div>

                      <div className="col-sm-12 mt-3">
                        <label className="form-label">
                          Spouse/Partner Name
                        </label>
                        <input
                          className="form-control"
                          id="inline-form-input"
                          type="text"
                          name="spouse_name"
                          value={formValues.spouse_name}
                          onChange={handleChange}
                        />
                      </div>
                    </div>
                    {formValues &&
                                (formValues.contactType === "associate" ||
                                  formValues.contactType === "admin") ? (
                                  formValues.nickname ? (
                    <div className="float-left w-100 mt-3">
                      <h6 style={{ fontSize: 20 }}>Web Profile</h6>
                      <h6 style={{ fontSize: 18 }}>
                        Publicly shared information.
                      </h6>

                      <div className="col-sm-12 mt-4">
                        <label className="form-label">
                          Website URL <NavLink>Test Site Link</NavLink>
                        </label>
                        <input
                          className="form-control"
                          id="inline-form-input"
                          type="text"
                          name="web"
                          value={formValues.web}
                          onChange={handleChange}
                        />
                      </div>

                      <div className="col-sm-12 mt-4">
                        <label className="form-label">
                          Promotional Tag Line
                        </label>
                        <input
                          className="form-control"
                          id="inline-form-input"
                          type="text"
                          name="tag_line"
                          value={formValues.tag_line}
                          onChange={handleChange}
                        />
                      </div>

                      <div className="col-sm-12 mt-4">
                        <label className="form-label">
                          Promotional Message
                        </label>
                        <textarea
                          className="form-control mt-0"
                          id="textarea-input"
                          rows="5"
                          name="marketing_message"
                          onChange={handleChange}
                          autoComplete="on"
                          value={formValues.marketing_message}
                        >
                          A quick summary about your specialties and real estate
                          interests.
                        </textarea>
                      </div>

                      <div className="col-sm-12 mt-4">
                        <label className="form-label">
                          Promotional Video URL
                        </label>
                        <input
                          className="form-control"
                          id="inline-form-input"
                          type="text"
                          name="marketing_videourl"
                          value={formValues.marketing_videourl}
                          onChange={handleChange}
                        />
                      </div>

                      <div className="col-sm-12 mt-4">
                        <label className="form-label">Primary Areas</label>

                        <div className="row">
                          {primaryAreas.map((area, index) => (
                          <div className="col-md-4">
                         
                              <input
                                className="form-control "
                                type="text"
                                placeholder="Zip Code"
                                value={area.zipcode}
                                onChange={(event) =>
                                  handleChangePrimary(event, index)
                                }
                              />
                          </div>
                          ))}

                        </div>
                      </div>

                      <div className="col-sm-12 mt-4">
                        <label className="form-label">Languages</label>
                        <div className="row">
                          {languageInput.map((languageField, index) => (
                          <div className="col-md-4">
                              <select
                                className="form-select"
                                name="language"
                                value={
                                  languageField.language
                                    ? languageField.language
                                    : languageData
                                }
                                onChange={(event) =>
                                  handleLanguageChange(event, index)
                                }
                              >
                                <option></option>
                                <option>Abkhazian</option>
                                <option>Afar</option>
                                <option>Afrikaans</option>
                                <option>Akan</option>
                                <option>Albanian</option>
                                <option>Amharic</option>
                                <option>Arabic</option>
                                <option>Aragonese</option>
                                <option>Armenian</option>
                                <option>Assamese</option>
                                <option>Avaric</option>
                                <option>Avestan</option>
                                <option>Aymara</option>
                                <option>Azerbaijani</option>
                                <option>Bambara</option>
                                <option>Bashkir</option>
                                <option>Basque</option>
                                <option>Belarusian</option>
                                <option>Bengali</option>
                                <option>Bihari</option>
                                <option>Bislama</option>
                                <option>Bosnian</option>
                                <option>Breton</option>
                                <option>Bulgarian</option>
                                <option>Burmese</option>
                                <option>Catalan; Valencian</option>
                                <option>Central Khmer</option>
                                <option>Chamorro</option>
                                <option>Chechen</option>
                                <option>Chichewa; Chewa; Nyanja</option>
                                <option>Chinese</option>
                                <option>Church Slavic; Old Slavonic</option>
                                <option>Chuvash</option>
                                <option>Cornish</option>
                                <option>Corsican</option>
                                <option>Cree</option>
                                <option>Croatian</option>
                                <option>Czech</option>
                                <option>Danish</option>
                                <option>Divehi; Maldivian</option>
                                <option>Dutch; Flemish</option>
                                <option>Dzongkha</option>
                                <option>Esperanto</option>
                                <option>Estonian</option>
                                <option>Ewe</option>
                                <option>Faroese</option>
                                <option>Fijian</option>
                                <option>Finnish</option>
                                <option>French</option>
                                <option>Fulah</option>
                                <option>Gaelic; Scottish Gaelic</option>
                                <option>Galician</option>
                                <option>Ganda</option>
                                <option>Georgian</option>
                                <option>German</option>
                                <option>Greek, Modern</option>
                                <option>Guarani</option>
                                <option>Gujarati</option>
                                <option>Haitian; Haitian Creole</option>
                                <option>Hausa</option>
                                <option>Hebrew</option>
                                <option>Herero</option>
                                <option>Hindi</option>
                                <option>Hiri Motu</option>
                                <option>Hungarian</option>
                                <option>Icelandic</option>
                                <option>Ido</option>
                                <option>Igbo</option>
                                <option>Indonesian</option>
                                <option>Interlingua</option>
                                <option>Inuktitut</option>
                                <option>Inupiaq</option>
                                <option>Irish</option>
                                <option>Italian</option>
                                <option>Japanese</option>
                                <option>Javanese</option>
                                <option>Kalaallisut; Greenlandic</option>
                                <option>Kannada</option>
                                <option>Kanuri</option>
                                <option>Kashmiri</option>
                                <option>Kazakh</option>
                                <option>Kikuyu; Gikuyu</option>
                                <option>Kinyarwanda</option>
                                <option>Kirghiz</option>
                                <option>Komi</option>
                                <option>Kongo</option>
                                <option>Korean</option>
                                <option>Kuanyama</option>
                                <option>Kurdish</option>
                                <option>Lao</option>
                                <option>Latin</option>
                                <option>Latvian</option>
                                <option>Limburgan</option>
                                <option>Lingala</option>
                                <option>Lithuanian</option>
                                <option>Luba-Katanga</option>
                                <option>Luxembourgish</option>
                                <option>Macedonian</option>
                                <option>Malagasy</option>
                                <option>Malay</option>
                                <option>Malayalam</option>
                                <option>Maltese</option>
                                <option>Manx</option>
                                <option>Maori</option>
                                <option>Marathi</option>
                                <option>Marshallese</option>
                                <option>Moldavian</option>
                                <option>Mongolian</option>
                                <option>Nauru</option>
                                <option>Navajo</option>
                                <option>Ndebele, North</option>
                                <option>Ndebele, South</option>
                                <option>Ndonga</option>
                                <option>Nepali</option>
                                <option>Northern Sami</option>
                                <option>Norwegian</option>
                                <option>Norwegian Bokmål</option>
                                <option>Norwegian Nynorsk</option>
                                <option>Occidental</option>
                                <option>Occitan; Provençal</option>
                                <option>Ojibwa</option>
                                <option>Oriya</option>
                                <option>Oromo</option>
                                <option>Ossetian</option>
                                <option>Pali</option>
                                <option>Panjabi; Punjabi</option>
                                <option>Persian</option>
                                <option>Polish</option>
                                <option>Portuguese</option>
                                <option>Pushto; Pashto</option>
                                <option>Quechua</option>
                                <option>Romanian</option>
                                <option>Romansh</option>
                                <option>Rundi</option>
                                <option>Russian</option>
                                <option>Samoan</option>
                                <option>Sango</option>
                                <option>Sanskrit</option>
                                <option>Sardinian</option>
                                <option>Serbian</option>
                                <option>Shona</option>
                                <option>Sichuan Yi; Nuosu</option>
                                <option>Sign Language</option>
                                <option>Sindhi</option>
                                <option>Sinhala; Sinhalese</option>
                                <option>Slovak</option>
                                <option>Slovenian</option>
                                <option>Somali</option>
                                <option>Sotho, Southern</option>
                                <option>Spanish; Castilian</option>
                                <option>Sundanese</option>
                                <option>Swahili</option>
                                <option>Swati</option>
                                <option>Swedish</option>
                                <option>Tagalog</option>
                                <option>Tahitian</option>
                                <option>Tajik</option>
                                <option>Tamil</option>
                                <option>Tatar</option>
                                <option>Telugu</option>
                                <option>Thai</option>
                                <option>Tibetan</option>
                                <option>Tigrinya</option>
                                <option>Tonga (Islands)</option>
                                <option>Tsonga</option>
                                <option>Tswana</option>
                                <option>Turkish</option>
                                <option>Turkmen</option>
                                <option>Twi</option>
                                <option>Uighur</option>
                                <option>Ukrainian</option>
                                <option>Urdu</option>
                                <option>Uzbek</option>
                                <option>Venda</option>
                                <option>Vietnamese</option>
                                <option>Volapük</option>
                                <option>Walloon</option>
                                <option>Welsh</option>
                                <option>Western Frisian</option>
                                <option>Wolof</option>
                                <option>Xhosa</option>
                                <option>Yiddish</option>
                                <option>Yoruba</option>
                                <option>Zhuang; Chuang</option>
                                <option>Zulu</option>
                              </select>
                            
                          </div>
                          ))}
                        </div>
                      </div>

                      <div className="col-sm-12 mt-4 primary_areas">
                        <label className="form-label">Specialties</label>
                        <div className="row">
                          {specialtiesInput.map((specialtyField, index) => (
                          <div className="col-md-4">
                              <select
                                className="form-select"
                                name="specialties"
                                value={specialtyField.specialties}
                                onChange={(event) =>
                                  handleSpecialtyChange(event, index)
                                }
                              >
                                <option value=""></option>
                                <option value="buyers">Buyers</option>
                                <option value="commercial">Commercial</option>
                                <option value="condominiums">Condominiums</option>
                                <option value="divorcees">Divorcees</option>
                                <option value="downtown">Downtown</option>
                                <option value="first-time_buyers">First-time Buyers</option>
                                <option value="foreclosures">Foreclosures</option>
                                <option value="investments">Investments</option>
                                <option value="new_builds">New Builds</option>
                                <option value="platinum">Platinum</option>
                                <option value="relocations">Relocations</option>
                                <option value="rentals">Rentals</option>
                                <option value="rural">Rural</option>
                                <option value="sellers">Sellers</option>
                                <option value="short_sales">Short Sales</option>
                                <option value="suburban">Suburban</option>
                                <option value="urban">Urban</option>
                              </select>
                          </div>
                          ))}
                        </div>
                      </div>
                    </div>
                      ) : (
                        ""
                      )
                    ) : (
                      ""
                    )}

                  </div>
                  <div className="pull-right mt-3">
                    <button
                      className="btn btn-primary pull-right"
                      type="button"
                      onClick={handleSubmit}
                      // disabled={show}
                      >
                      {/* {show ? "Please wait" : "Save Details"} */}
                      Save Details
                    </button>

                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </main>
    </div>
  );
}
export default ProfileInfo;

import React, { useState, useEffect, useRef } from "react";
import Loader from "../../Pages/Loader/Loader.js";
import Toaster from "../../Pages/Toaster/Toaster.js";
import { NavLink, useNavigate, useParams } from "react-router-dom";
import user_service from "../service/user_service";
import jwt from "jwt-decode";
import { Editor } from "react-draft-wysiwyg";
import { EditorState, ContentState, convertToRaw } from "draft-js";
import draftToHtml from "draftjs-to-html";
import Pic from "../img/pic.png";
import "react-draft-wysiwyg/dist/react-draft-wysiwyg.css";
import _ from "lodash";
import axios from "axios";
import Swal from "sweetalert2";
import "sweetalert2/dist/sweetalert2.css";
import DateTimePicker from "react-datetime-picker";
import "react-datetime-picker/dist/DateTimePicker.css";
import { Document, Page } from "react-pdf";

const PrivateContactEdit = () => {
  const [loader, setLoader] = useState({ isActive: null });
  const { isActive } = loader;
  const [toaster, setToaster] = useState({
    type: null,
    isShow: null,
    toasterBody: "",
    message: "",
  });
  const { type, isShow, toasterBody, message } = toaster;

  const initialValues = {
    email: "",
    image: "",
    phone: "",
    firstName: "",
    lastName: "",
    nickname: "",
    address: "",
    address2: "",
    city: "",
    state: "",
    zip: "",
    web: "",
    fax: "",
    inbox: "",
    work_phone: "",
    home_phone: "",
    video_email: "",
    printed_name: "",
    spouse_firstname: "",
    spouse_lastname: "",
    spouse_number: "",
    spouse_email: "",
    spouse_birthday: "",
    bussiness_name: "",
    bussiness_address: "",
    sellers: "",
    personal_contacttype: "",
    organization: "",
    homeOffice: "",
    title: "",
    agentId: "",
    profession: "",
    industry: "",
    assistant: "",
    designations: "",
    primary_areas: "",
    languages: "",
    specialties: "",
    marketing_message: "",
    birthday: "",
    gender: "",
    anniversary: "",
    private_note: "",
    recruiter: "",
    supporters: "",
    connection: "",
    notes: "",
    checkBussiness: "no",
     brokerage_Fee:"doller-percentage",
    buyer_broker_fee:"",
    buyer_dateTime:"",
    buyer_document:"",
    buyers_name:"",
    protecion_dateTime:""

  };

  const initialValuesNew = {
    message: "",
  };

  const initialNew = {
    from_name: "",
    send_email: "",
    subject: "",
    template: "",
    private_video: "",
    include_video: "",
    description: "",
  };

  const socialMediaTypes = [
    { type: "Twitter", linkurl: "" },
    { type: "Linkedin", linkurl: "" },
    { type: "Facebook", linkurl: "" },
    { type: "Instagram", linkurl: "" },
    { type: "Tumblr", linkurl: "" },
    { type: "YouTube Channel", linkurl: "" },
    { type: "Pinterest", linkurl: "" },
    { type: "TikTok", linkurl: "" },
    { type: "Vimeo", linkurl: "" },
  ];

  const [formValues, setFormValues] = useState(initialValuesNew);
  const [socialLinks, setSocialLinks] = useState(socialMediaTypes);

  const [formValuesNew, setFormValuesNew] = useState(initialNew);

  const [formErrors, setFormErrors] = useState({});
  const [isSubmitClick, setISSubmitClick] = useState(false);
  const [isSubmitClickNew, setISSubmitClickNew] = useState(false);

  const [data, setData] = useState("");
  const [file, setFile] = useState(null);
  const [fileExtension, setFileExtension] = useState("");
  const [acceptedFileTypes, setAcceptedFileTypes] = useState([
    "jpg",
    "jpeg",
    "png",
    "svg",
  ]);
  const [showSubmitButton, setShowSubmitButton] = useState(false);

  const [exclusive_video_url, setExclusive_video_url] = useState("");
  const [uploading, setUploading] = useState(false);
  const [stepon, setStepon] = useState(1);

  const navigate = useNavigate();

  const params = useParams();

  const [getContact, setGetContact] = useState(initialValues);
  const [step1, setStep1] = useState("1");

  const [notesData, setNotesData] = useState("");
  const [notesMessage, setNotesMessage] = useState("");
  const [notesId, setNotesId] = useState("");

  const [notesCheck, setNotesCheck] = useState(false);
  const [showInputs, setShowInputs] = useState([]);
  const [showContactType, setShowContactType] = useState([]);

  const [notesmessageEditor, setNotesmessageEditor] = useState("");
  const editorRef = useRef(null);
  const log = () => {
    if (editorRef.current) {
      setNotesmessageEditor(editorRef.current.getContent());
    }
  };

  const ContactGetById = async () => {
    try {
      const response = await user_service.contactGetById(params.id);

      if (response && response.data) {
        const contactData = response.data;
        setGetContact(contactData);

        if (Array.isArray(contactData.new_source)) {
          setShowInputs(
            contactData.new_source.map((source, index) => ({
              id: index,
              value: source,
            }))
          );
        }

        // if (Array.isArray(contactData.new_personal_contacttype)) {
        //   setShowContactType(
        //     contactData.new_personal_contacttype.map((source, index) => ({
        //       id: index,
        //       value: source,
        //     }))
        //   );
        // }

        if (
          Array.isArray(contactData.social_links) &&
          contactData.social_links.length > 0
        ) {
          setSocialLinks(contactData.social_links);
        }
      }
    } catch (error) {
      console.error("Error fetching contact:", error);
    }
  };

  const ContactNotes = async () => {
    setLoader({ isActive: true }); // Show loader
    try {
      const response = await user_service.contactNotesGetId(params.id);
      if (response) {
        const sortedNotes = response.data.data.sort(
          (a, b) => new Date(b.created) - new Date(a.created)
        );
        setLoader({ isActive: false }); // Show loader

        setNotesData(sortedNotes);
      }
    } catch (error) {
      setLoader({ isActive: false }); // Show loader

      console.error("Error fetching notes:", error);
    }
  };

  useEffect(() => {
    if (formValues && isSubmitClick) {
      validate();
    }
  }, [formValues]);

  const validate = () => {
    const values = formValues;
    const errors = {};

    if (!values.message) {
      errors.message = "Notes is required.";
    }

    setFormErrors(errors);
    return errors;
  };

  useEffect(() => {
    if (formValuesNew && isSubmitClickNew) {
      validateNew();
    }
  }, [formValuesNew]);

  const validateNew = () => {
    const values = formValuesNew;
    const errors = {};

    if (!values.from_name) {
      errors.from_name = "From name is required.";
    }

    if (!values.subject) {
      errors.subject = "Subject is required.";
    }

    if (!values.description) {
      errors.description = "Description is required.";
    }

    setFormErrors(errors);
    return errors;
  };

  const [editorState, setEditorState] = useState(EditorState.createEmpty());

  const handleChanges = (editorState) => {
    setEditorState(editorState);
    const htmlContent = draftToHtml(
      convertToRaw(editorState.getCurrentContent())
    );
    setFormValuesNew({ ...formValuesNew, description: htmlContent });
  };

  const handleFileUploadPersonal = async (e) => {
    const selectedFile = e.target.files[0];

    if (selectedFile) {
      const fileExtension = selectedFile.name.split(".").pop().toLowerCase();

      if (!acceptedFileTypes.includes(fileExtension)) {
        alert(
          `Please upload a valid image file: ${acceptedFileTypes.join(", ")}`
        );
        return;
      }

      setFile(selectedFile);
      setFileExtension(fileExtension);
      setShowSubmitButton(true);

      const formData = new FormData();
      formData.append("file", selectedFile);
      const config = {
        headers: {
          Accept: "application/json",
          "Content-Type": "multipart/form-data",
          Authorization: `Bearer ${localStorage.getItem("auth")}`,
        },
      };

      try {
        const uploadResponse = await axios.post(
          "https://api.brokeragentbase.com/upload",
          // "http://localhost:4000/upload",

          formData,
          config
        );
        const uploadedFileData = uploadResponse.data;
        setData(uploadedFileData);
        const updatedgetContact = {
          ...getContact,
          image: uploadedFileData,
        };

        setLoader({ isActive: true });
        await user_service
          .contactUpdate(params.id, updatedgetContact)
          .then((response) => {
            setLoader({ isActive: false });
            if (response) {
              ContactGetById();
            } else {
              setLoader({ isActive: false });
              setToaster({
                type: "error",
                isShow: true,
                message: "Error",
              });

              setTimeout(() => {
                setToaster((prevToaster) => ({
                  ...prevToaster,
                  isShow: false,
                }));
              }, 3000);
            }
          });
      } catch (error) {
        setLoader({ isActive: false });
        setToaster({
          types: "error",
          isShow: true,
          message: "Error",
        });

        setTimeout(() => {
          setToaster((prevToaster) => ({ ...prevToaster, isShow: false }));
        }, 1000);
      }
    }
  };

  // const handleChange = (e) => {
  //   const { name, value } = e.target;
  //   setGetContact({ ...getContact, [name]: value });
  // };

  // const [customSources, setCustomSources] = useState([]);

  const handleChange = (e) => {
    const { name, value } = e.target;
    setGetContact({ ...getContact, [name]: value });

    if (value === "new_source") {
      const newInput = { id: Date.now(), value: "" };
      setShowInputs([...showInputs, newInput]);
      setGetContact({ ...getContact, source: "" });
    }
    // if (value === "new_contact_type") {
    //   const newInput = { id: Date.now(), value: "" };
    //   setShowContactType([...showContactType, newInput]);
    //   setGetContact({ ...getContact, personal_contacttype: "" });
    // }
  };

  const handleChangeCheck = (event) => {
    const { checked } = event.target;
    console.log(checked ? "yes" : "no");
    setGetContact((prev) => ({
      ...prev,
      checkBussiness: checked ? "yes" : "no", // Update value based on checkbox state
    }));
  };
  // const handleInputContactType = (id, newValue) => {
  //   const updatedInputs = showContactType.map((input) =>
  //     input.id === id ? { ...input, value: newValue } : input
  //   );
  //   setShowContactType(updatedInputs);
  // };

  const handleInputChange = (id, newValue) => {
    const updatedInputs = showInputs.map((input) =>
      input.id === id ? { ...input, value: newValue } : input
    );
    setShowInputs(updatedInputs);
  };

  // const handleDeleteContactType = async (id) => {
  //   Swal.fire({
  //     title: "Are you sure?",
  //     text: "It will be permanently deleted!",
  //     icon: "warning",
  //     showCancelButton: true,
  //     confirmButtonColor: "#3085d6",
  //     cancelButtonColor: "#d33",
  //     confirmButtonText: "Yes, delete it!",
  //   }).then(async (result) => {
  //     if (result.isConfirmed) {
  //       // Remove the item from the local state
  //       const updatedInputs = showContactType.filter((input) => input.id !== id);
  //       setShowContactType(updatedInputs);

  //       // Prepare the updated data to be posted to the API
  //       const updatedSources = updatedInputs.map((input) => input.value);

  //       const userData = {
  //         new_personal_contacttype: updatedSources, // Send the updated new_source list to the API
  //       };

  //       // Post the updated data to the API
  //       setLoader({ isActive: true });
  //       try {
  //         const response = await user_service.contactUpdate(params.id, userData);
  //         if (response) {
  //           setGetContact(response.data);
  //           setLoader({ isActive: false });
  //           ContactGetById(params.id); // Reload the contact data
  //         } else {
  //           setLoader({ isActive: false });
  //           setToaster({
  //             type: "error",
  //             isShow: true,
  //             message: "Error updating contact",
  //           });
  //         }
  //       } catch (error) {
  //         setLoader({ isActive: false });
  //         console.error("Error updating contact:", error);
  //         setToaster({
  //           type: "error",
  //           isShow: true,
  //           message: "Error updating contact",
  //         });
  //       }
  //     }
  //   });
  // };

  const handleDeleteInput = async (id) => {
    Swal.fire({
      title: "Are you sure?",
      text: "It will be permanently deleted!",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Yes, delete it!",
    }).then(async (result) => {
      if (result.isConfirmed) {
        // Remove the item from the local state
        const updatedInputs = showInputs.filter((input) => input.id !== id);
        setShowInputs(updatedInputs);

        // Prepare the updated data to be posted to the API
        const updatedSources = updatedInputs.map((input) => input.value);

        const userData = {
          new_source: updatedSources, // Send the updated new_source list to the API
        };

        // Post the updated data to the API
        setLoader({ isActive: true });
        try {
          const response = await user_service.contactUpdate(
            params.id,
            userData
          );
          if (response) {
            setGetContact(response.data);
            setLoader({ isActive: false });
            ContactGetById(params.id); // Reload the contact data
          } else {
            setLoader({ isActive: false });
            setToaster({
              type: "error",
              isShow: true,
              message: "Error updating contact",
            });
          }
        } catch (error) {
          setLoader({ isActive: false });
          console.error("Error updating contact:", error);
          setToaster({
            type: "error",
            isShow: true,
            message: "Error updating contact",
          });
        }
      }
    });
  };

  const handleChangeSocial = (event, index) => {
    const { value } = event.target;
    setSocialLinks((prevSocialLinks) => {
      const updatedSocialLinks = [...prevSocialLinks];
      updatedSocialLinks[index] = {
        ...updatedSocialLinks[index],
        linkurl: value,
      };
      return updatedSocialLinks;
    });
  };

  const handleChangeVideo = (e) => {
    const { name, value } = e.target;
    setFormValuesNew({ ...formValuesNew, [name]: value });
  };

  const handleChangeNote = (e) => {
    const { name, value } = e.target;
    setFormValues((prevValues) => ({
      ...prevValues,
      [name]: value,
    }));

    // setNotesMessage(value);
  };

  const handleCheckGender = (e) => {
    const select = e.target.value;
    setGetContact({
      ...getContact,
      gender: select,
    });
  };

  const handleCheckSpuseGender = (e) => {
    const select = e.target.value;
    setGetContact({
      ...getContact,
      spouse_Gender: select,
    });
  };

  const handleCheck = (e) => {
    setNotesCheck(e.target.checked);
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
    try {
      const additionalSources = showInputs
        .map((input) => input.value)
        .filter(Boolean);
      // const additionalContactType = showContactType.map((input) => input.value).filter(Boolean);

      const userData = {
        agentId: jwt(localStorage.getItem("auth")).id,
        firstName: getContact.firstName,
        lastName: getContact.lastName,
        nickname: getContact.nickname
          ? getContact.nickname
          : getContact.lastName + "" + getContact.firstName,
        email: getContact.email,
        phone: getContact.phone,
        company: getContact.company,
        checkBussiness: getContact.checkBussiness,
        contactType: getContact.contactType,
        personal_contacttype: getContact.personal_contacttype,
        // new_personal_contacttype: additionalContactType,
        address: getContact.address,
        address2: getContact.address2,
        city: getContact.city,
        state: getContact.state,
        zip: getContact.zip,
        web: getContact.web,
        source: getContact.source,
        new_source: additionalSources,
        birthday: getContact.birthday,
        birth_Notification: getContact.birth_Notification,
        gender: getContact.gender,
        communication_Date: getContact.communication_Date,
        communication_Note: getContact.communication_Note,
        tags: getContact.tags,
        selling: getContact.selling,
        buying: getContact.buying,
        social_links: socialLinks,
      };
      setLoader({ isActive: true });
      await user_service.contactUpdate(params.id, userData).then((response) => {
        if (response) {
          setGetContact(response.data);
          setLoader({ isActive: false });
          ContactGetById(params.id);
          setToaster({
            type: "Contact Added Successfully",
            isShow: true,
            message: "Contact Added Successfully",
          });
          setTimeout(() => {
            setToaster((prevToaster) => ({ ...prevToaster, isShow: false }));
            // navigate(`/contact`);
          }, 3000);
        } else {
          setLoader({ isActive: false });
          setToaster({
            type: "error",
            isShow: true,
            message: "Error",
          });
        }
      });
    } catch (error) {
      setLoader({ isActive: false });
      setToaster({
        type: "error",
        isShow: true,
        message: "Error",
      });
      setTimeout(() => {
        setToaster((prevToaster) => ({ ...prevToaster, isShow: false }));
      }, 3000);
    }
  };

  const handleSubmitSpouse = async (e) => {
    e.preventDefault();
    try {
      const userData = {
        spouse_firstname: getContact.spouse_firstname,
        spouse_lastname: getContact.spouse_lastname,
        spouse_email: getContact.spouse_email,
        spouse_number: getContact.spouse_number,
        spouse_birthday: getContact.spouse_birthday,
        relationship_spouse: getContact.relationship_spouse,
        spouse_Anniversary: getContact.spouse_Anniversary,
        spouse_Gender: getContact.spouse_Gender,
      };
      console.log(userData);
      setLoader({ isActive: true });
      await user_service.contactUpdate(params.id, userData).then((response) => {
        if (response) {
          setGetContact(response.data);
          setLoader({ isActive: false });
          ContactGetById(params.id);
          setToaster({
            type: "Contact Added Successfully",
            isShow: true,
            message: "Contact Added Successfully",
          });
          setTimeout(() => {
            setToaster((prevToaster) => ({ ...prevToaster, isShow: false }));
            navigate(`/contact`);
          }, 3000);
        } else {
          setLoader({ isActive: false });
          setToaster({
            type: "error",
            isShow: true,
            message: "Error",
          });
        }
      });
    } catch (error) {
      setLoader({ isActive: false });
      setToaster({
        type: "error",
        isShow: true,
        message: "Error",
      });
      setTimeout(() => {
        setToaster((prevToaster) => ({ ...prevToaster, isShow: false }));
      }, 3000);
    }
  };

  const handleSubmitBussiness = async (e) => {
    e.preventDefault();
    try {
      const userData = {
        bussiness_name: getContact.bussiness_name,
        bussiness_address: getContact.bussiness_address,
        position_Title: getContact.position_Title,
        business_Phone: getContact.business_Phone,
        business_Website: getContact.business_Website,
        business_Social: getContact.business_Social,
      };
      console.log(userData);
      setLoader({ isActive: true });
      await user_service.contactUpdate(params.id, userData).then((response) => {
        if (response) {
          setGetContact(response.data);
          setLoader({ isActive: false });
          ContactGetById(params.id);
          setToaster({
            type: "Contact Added Successfully",
            isShow: true,
            message: "Contact Added Successfully",
          });
          setTimeout(() => {
            setToaster((prevToaster) => ({ ...prevToaster, isShow: false }));
            navigate(`/contact`);
          }, 3000);
        } else {
          setLoader({ isActive: false });
          setToaster({
            type: "error",
            isShow: true,
            message: "Error",
          });
        }
      });
    } catch (error) {
      setLoader({ isActive: false });
      setToaster({
        type: "error",
        isShow: true,
        message: "Error",
      });
      setTimeout(() => {
        setToaster((prevToaster) => ({ ...prevToaster, isShow: false }));
      }, 3000);
    }
  };
  const handleSubmitNote = async (e) => {
    e.preventDefault();
    setISSubmitClick(true);
    let checkValue = validate();
    if (_.isEmpty(checkValue)) {
      try {
        const userData = {
          addedBy: jwt(localStorage.getItem("auth")).id,
          agentId: jwt(localStorage.getItem("auth")).id,
          contactId: params.id,
          message: formValues.message,
          note_type: "Private Notes",
        };
        console.log(userData);
        setLoader({ isActive: true });
        await user_service.contactNotesPost(userData).then((response) => {
          if (response) {
            setFormValues(response.data);
            setNotesId("");
            setNotesMessage("");
            setLoader({ isActive: false });
            setToaster({
              type: "Note Added Successfully",
              isShow: true,
              // toasterBody: response.data.message,
              message: "Note Added Successfully",
            });
            setTimeout(() => {
              setToaster((prevToaster) => ({ ...prevToaster, isShow: false }));
              // navigate(`/contact`);
            }, 3000);
            ContactNotes(params.id);
            document.getElementById("close").click();
          } else {
            setLoader({ isActive: false });
            setToaster({
              type: "error",
              isShow: true,
              toasterBody: response.data.message,
              message: "Error",
            });
          }
        });
      } catch (error) {
        setLoader({ isActive: false });
        setToaster({
          type: "error",
          isShow: true,
          toasterBody: error.response
            ? error.response.data.message
            : error.message,
          message: "Error",
        });
        setTimeout(() => {
          setToaster((prevToaster) => ({ ...prevToaster, isShow: false }));
        }, 3000);
      }
    }
  };

  const handleUpdate = (id, message) => {
    setFormValues((prevValues) => ({
      ...prevValues,
      id: id,
      message: message,
      // Other form values...
    }));
    setNotesId(id);
    setNotesMessage(message);
  };

  const handleUpdateData = async () => {
    if (notesId) {
      console.log(notesId);

      try {
        const userData = {
          message: formValues.message,
        };
        console.log(userData);
        setLoader({ isActive: true });
        await user_service
          .contactNotesUpdate(notesId, userData)
          .then((response) => {
            if (response) {
              setFormValues(response.data);
              setLoader({ isActive: false });
              setToaster({
                type: "Note Updated Successfully",
                isShow: true,
                // toasterBody: response.data.message,
                message: "Note Updated Successfully",
              });
              setTimeout(() => {
                setToaster((prevToaster) => ({
                  ...prevToaster,
                  isShow: false,
                }));
                // navigate(`/contact`);
              }, 1000);
              ContactNotes(params.id);
              document.getElementById("closeModal").click();
            } else {
              setLoader({ isActive: false });
              setToaster({
                type: "error",
                isShow: true,
                toasterBody: response.data.message,
                message: "Error",
              });
            }
          });
      } catch (error) {
        setLoader({ isActive: false });
        setToaster({
          type: "error",
          isShow: true,
          toasterBody: error.response
            ? error.response.data.message
            : error.message,
          message: "Error",
        });
        setTimeout(() => {
          setToaster((prevToaster) => ({ ...prevToaster, isShow: false }));
        }, 3000);
      }
    }
  };

  const handleDelete = async (id) => {
    setLoader({ isActive: true });
    await user_service.contactNotesDelete(id).then((response) => {
      if (response) {
        setLoader({ isActive: false });
        setToaster({
          type: "Note Deleted Successfully",
          isShow: true,
          // toasterBody: response.data.message,
          message: "Note Deleted Successfully",
        });
        setTimeout(() => {
          setToaster((prevToaster) => ({ ...prevToaster, isShow: false }));
        }, 3000);
        ContactNotes(params.id);
      }
    });
  };

  const handleStep = (stepno) => {
    setStep1(stepno);
  };

  const [spouseClick, setSpouseClick] = useState(false);
  const handleSpouse = () => {
    setSpouseClick(true);
  };

  const handleFileUploadVideo = async (e, id) => {
    const selectedFile = e.target.files[0];
    if (selectedFile) {
      var fileExtension = selectedFile.name.split(".").pop();
      if (fileExtension != "mp4") {
        alert("Video File should be in mp4 format.", "error");
        return;
      }
      setUploading(true);
      const formData = new FormData();
      formData.append("file", selectedFile);
      const config = {
        headers: {
          Accept: "application/json",
          "Content-Type": "multipart/form-data",
          Authorization: `Bearer ${localStorage.getItem("auth")}`,
        },
      };
      try {
        // setLoader({ isActive: true });
        const uploadResponse = await axios.post(
          "https://api.brokeragentbase.com/upload",
          formData,
          config
        );
        const uploadedFileData = uploadResponse.data;

        console.log(uploadedFileData);

        setExclusive_video_url(uploadedFileData);
        // setLoader({ isActive: false });
        setToaster({
          type: "Video Uploaded Successfully",
          isShow: true,
          // toasterBody: response.data.message,
          message: "Video Uploaded Successfully",
        });
        setTimeout(() => {
          setToaster((prevToaster) => ({ ...prevToaster, isShow: false }));
        }, 3000);
      } catch (error) {
        console.error("Error occurred during file upload:", error);
      }
    }
  };

  const handleVideo = async (e) => {
    e.preventDefault();
    setISSubmitClickNew(true);
    let checkValue = validateNew();
    if (_.isEmpty(checkValue)) {
      try {
        const userData = {
          addedBy: jwt(localStorage.getItem("auth")).id,
          agentId: jwt(localStorage.getItem("auth")).id,
          contactId: params.id,
          from_name: formValuesNew.from_name,
          send_email: formValuesNew.send_email,
          subject: formValuesNew.subject,
          template: formValuesNew.template,
          description: formValuesNew.description,
          private_video: exclusive_video_url,
          include_video: notesCheck.toString(),
          document_url: data,
        };
        console.log(userData);
        setLoader({ isActive: true });
        await user_service.contactSendEmailPost(userData).then((response) => {
          if (response) {
            setLoader({ isActive: false });
            setToaster({
              type: "Email Sent Successfully",
              isShow: true,
              // toasterBody: response.data.message,
              message: "Email Sent Successfully",
            });
            setTimeout(() => {
              setToaster((prevToaster) => ({ ...prevToaster, isShow: false }));
              // navigate(`/contact`);
            }, 3000);
          } else {
            setLoader({ isActive: false });
            setToaster({
              type: "error",
              isShow: true,
              toasterBody: response.data.message,
              message: "Error",
            });
          }
        });
      } catch (error) {
        setLoader({ isActive: false });
        setToaster({
          type: "error",
          isShow: true,
          toasterBody: error.response
            ? error.response.data.message
            : error.message,
          message: "Error",
        });
        setTimeout(() => {
          setToaster((prevToaster) => ({ ...prevToaster, isShow: false }));
        }, 3000);
      }
    }
  };

  const [birthdayNotification, setBirthdayNotification] = useState("");
  const handleChangeBirthday = (e) => {
    const { name, value } = e.target;
    setBirthdayNotification({ ...birthdayNotification, [name]: value });
  };

  const handleSubmitBirthday = async (e) => {
    e.preventDefault();

    if (!getContact.birthday) {
      console.error("Birthday is not available.");
      return;
    }

    const contactData = [
      {
        email: getContact.email,
        firstName: getContact.firstName,
        is_view: "1", // Adding the static is_view field
        _id: getContact._id,
      },
    ];

    const userData = {
      agentId: jwt(localStorage.getItem("auth")).id,
      office_id: localStorage.getItem("active_office_id"),
      office_name: localStorage.getItem("active_office"),
      category: "Birthday Wishes",
      birthday_noticeTime: birthdayNotification.birthday_noticeTime,
      message: birthdayNotification.message,
      birthday_date: getContact.birthday,
      contactType: contactData,
    };

    console.log(userData);
    await user_service.birthdayNotification(userData).then((response) => {
      if (response) {
        setLoader({ isActive: false });
        setToaster({
          type: "Birthday",
          isShow: true,
          message: "Birthday send Successfully",
        });
        // noticeGetAllData();
        setTimeout(() => {
          setToaster((prevToaster) => ({ ...prevToaster, isShow: false }));
          // navigate("/send/notice");
        }, 2000);
      } else {
        setLoader({ isActive: false });
        setToaster({
          types: "error",
          isShow: true,
          // toasterBody: response.data.message,
          message: "Error",
        });
        setTimeout(() => {
          setToaster((prevToaster) => ({ ...prevToaster, isShow: false }));
          // navigate("/control-panel/documents/");
        }, 2000);
      }
    });
  };

  const initialValuestask = {
    taskType: "personal",
    recipients: "",
    // task_recipients:"",
    selectGroup: "marketing",
    selectLevel: "corporate",
    selectDepartment: "",
    taskTitle: "",
    question: "",
    responseFormat: "",
    answer: [],
    taskDate: "",
    taskTime: "",
  };

  const [formValuesTask, setFormValuesTask] = useState(initialValuestask);
  const [formErrorsTask, setFormErrorsTask] = useState({});
  const [isSubmitClickTask, setISSubmitClickTask] = useState(false);

  const handleChangeTask = (e) => {
    const { name, value } = e.target;
    setFormValuesTask({ ...formValuesTask, [name]: value });
  };

  {
    /* <!-- Form Validation Start--> */
  }
  useEffect(() => {
    if (formValuesTask && isSubmitClickTask) {
      validateTask();
    }
  }, [formValuesTask]);

  const validateTask = () => {
    const values = formValuesTask;
    const errors = {};
    if (!values.taskTitle) {
      errors.taskTitle = "TaskTitle is required";
    }

    if (!values.question) {
      errors.question = "Question is required";
    }

    setFormErrorsTask(errors);
    return errors;
  };
  {
    /* <!-- Form Validation End--> */
  }

  const handleSubmitTask = async (e) => {
    e.preventDefault();
    setISSubmitClickTask(true);
    let checkValue = validateTask();
    if (_.isEmpty(checkValue)) {
      const userData = {
        agentId: jwt(localStorage.getItem("auth")).id,
        office_id: localStorage.getItem("active_office_id"),
        office_name: localStorage.getItem("active_office"),
        taskType: "personal",
        recipients: getContact._id,
        taskTitle: formValuesTask.taskTitle,
        question: formValuesTask.question,
        taskDate: formValuesTask.taskDate,
        taskTime: formValuesTask.taskTime,
      };
      console.log(userData);
      try {
        setLoader({ isActive: true });
        const response = await user_service.assocTaskPost(userData);
        if (response) {
          setFormValuesTask(response);
          setLoader({ isActive: false });
          setToaster({
            type: "Task Successfully",
            isShow: true,
            message: "Task Successfully",
          });
          setTimeout(() => {
            setToaster((prevToaster) => ({ ...prevToaster, isShow: false }));
            // navigate(`/transaction-summary/${response.data._id}`);
          }, 500);

          assocTaskGetAll();
          document.getElementById("closeModalTask").click();
        } else {
          setLoader({ isActive: false });
          setToaster({
            types: "error",
            isShow: true,
            message: "Error",
          });
        }
      } catch (error) {
        setLoader({ isActive: false });
        setToaster({
          types: "error",
          isShow: true,
          message: "Error",
        });
        setTimeout(() => {
          setToaster({
            types: "error",
            isShow: false,
            message: "Error",
          });
        }, 2000);
      }
    }
  };

  const [noticereceived, setNoticereceived] = useState("");
  const assocTaskGetAll = async () => {
    if (localStorage.getItem("auth")) {
      setLoader({ isActive: true });
      const userid = jwt(localStorage.getItem("auth")).id;
      var query = `?recipients=${params.id}`;
      await user_service.assocTaskGet(query).then((response) => {
        if (response) {
          setLoader({ isActive: false });
          setNoticereceived(response.data.data);
        }
      });
    }
  };

  useEffect(() => {
    window.scrollTo(0, 0);
    ContactGetById(params.id);
    ContactNotes(params.id);
    assocTaskGetAll();
  }, []);

  const handleTask = () => {
    setFormValuesTask("");
  };

  const handleComplted = async (id) => {
    if (id) {
      const userData = {
        task_completed: "yes",
      };
      try {
        setLoader({ isActive: true });
        const response = await user_service.assocTaskGetUpdate(id, userData);
        if (response) {
          setLoader({ isActive: false });
          setToaster({
            type: "Task Completed Successfully",
            isShow: true,
            message: "Task Completed Successfully",
          });
          setTimeout(() => {
            setToaster((prevToaster) => ({ ...prevToaster, isShow: false }));
            // navigate(`/transaction-summary/${response.data._id}`);
          }, 500);
          assocTaskGetAll();
        } else {
          setLoader({ isActive: false });
          setToaster({
            types: "error",
            isShow: true,
            message: "Error",
          });
        }
      } catch (error) {
        setLoader({ isActive: false });
        setToaster({
          types: "error",
          isShow: true,
          message: "Error",
        });
        setTimeout(() => {
          setToaster({
            types: "error",
            isShow: false,
            message: "Error",
          });
        }, 2000);
      }
    }
  };

  const scrollRef = useRef(null);

  const handleStepAndScroll = (stepno) => {
    setStep1(stepno); // Update step
    // Wait for the DOM to re-render and then scroll
    setTimeout(() => {
      if (scrollRef.current) {
        scrollRef.current.scrollIntoView({
          behavior: "smooth",
          block: "start",
        });
      }
    }, 0);
  };

  const CheckPercentage = (e) => {
    // setIsPercentageReferral(e.target.value === "referral-percentage");
    setGetContact((prevState) => ({
      ...prevState,
      brokerage_Fee: e.target.value,
      buyer_broker_fee: getContact.buyer_broker_fee,
    }));
  };

  const handleDateChange = (value) => {
    setGetContact({
      ...getContact,
      buyer_dateTime: value,
    });
  };
  const handleDateProtection = (value) => {
    setGetContact({
      ...getContact,
      protecion_dateTime: value,
    });
  };

  const handleBuyerBrokerFee = (e) => {
    const { name, value } = e.target;
    const newValue = value.replace(/[^\d,.]/g, "");
    setGetContact({
      ...getContact,
      [name]: newValue,
    });
    // setFormErrorsReferral((prevFormErrors) => ({
    //   ...prevFormErrors,
    //   referral_commission: newValue
    //     ? ""
    //     : "Referral Company Commissions is required",
    // }));
  };

  const handleFileUpload = async (e) => {
    const selectedFile = e.target.files[0];

    if (selectedFile) {
      // Check if the file is a PDF
      const fileExtension = selectedFile.name.split(".").pop().toLowerCase();
      // if (fileExtension !== "pdf") {
      //   setToaster({
      //     type: "error",
      //     isShow: true,
      //     message: "Please upload a valid PDF file.",
      //   });
      //   setTimeout(() => {
      //     setToaster((prevToaster) => ({ ...prevToaster, isShow: false }));
      //   }, 1000);
      //   return;
      // }

      setFile(selectedFile);
      setFileExtension(fileExtension);
      setShowSubmitButton(true);

      const formData = new FormData();
      formData.append("file", selectedFile);
      const config = {
        headers: {
          Accept: "application/json",
          "Content-Type": "multipart/form-data",
          Authorization: `Bearer ${localStorage.getItem("auth")}`,
        },
      };

      try {
        const uploadResponse = await axios.post(
          "https://api.brokeragentbase.com/upload",
          // "http://localhost:4000/upload",
          formData,
          config
        );

        const uploadedFileData = uploadResponse.data;
        setData(uploadedFileData);

        // setToaster({
        //   type: "success",
        //   isShow: true,
        //   message: "Document Uploaded Successfully",
        // });

        // setTimeout(() => {
        //   setToaster((prevToaster) => ({ ...prevToaster, isShow: false }));
        // }, 1000);
      } catch (error) {
        // Handle error during upload
        setLoader({ isActive: false });
        setToaster({
          type: "error",
          isShow: true,
          message: "Error uploading the document.",
        });

        setTimeout(() => {
          setToaster((prevToaster) => ({ ...prevToaster, isShow: false }));
        }, 3000);
      }
    }
  };

  const handleSubmitDocument = async (e) => {
    e.preventDefault();
    try {
      const userData = {
        buyers_name: getContact.buyers_name,
        brokerage_Fee: getContact.brokerage_Fee,
        buyer_broker_fee: getContact.buyer_broker_fee,
        protecion_dateTime: getContact.protecion_dateTime,
        buyer_dateTime: getContact.buyer_dateTime,
        buyer_document: data,
      };
      setLoader({ isActive: true });
      await user_service.contactUpdate(params.id, userData).then((response) => {
        if (response) {
          setGetContact(response.data);
          setLoader({ isActive: false });
          ContactGetById(params.id);
          setToaster({
            type: "Contact Added Successfully",
            isShow: true,
            message: "Contact Added Successfully",
          });
          setTimeout(() => {
            setToaster((prevToaster) => ({ ...prevToaster, isShow: false }));
            navigate(`/contact`);
          }, 3000);
        } else {
          setLoader({ isActive: false });
          setToaster({
            type: "error",
            isShow: true,
            message: "Error",
          });
        }
      });
    } catch (error) {
      setLoader({ isActive: false });
      setToaster({
        type: "error",
        isShow: true,
        message: "Error",
      });
      setTimeout(() => {
        setToaster((prevToaster) => ({ ...prevToaster, isShow: false }));
      }, 3000);
    }
  };


  const handleUpdateDocument = async (e) => {
    const selectedFile = e.target.files[0];

    if (selectedFile) {
      // Check if the file is a PDF
      const fileExtension = selectedFile.name.split(".").pop().toLowerCase();
      // if (fileExtension !== "pdf") {
      //   setToaster({
      //     type: "error",
      //     isShow: true,
      //     message: "Please upload a valid PDF file.",
      //   });
      //   setTimeout(() => {
      //     setToaster((prevToaster) => ({ ...prevToaster, isShow: false }));
      //   }, 1000);
      //   return;
      // }

      setFile(selectedFile);
      setFileExtension(fileExtension);
      setShowSubmitButton(true);

      const formData = new FormData();
      formData.append("file", selectedFile);
      const config = {
        headers: {
          Accept: "application/json",
          "Content-Type": "multipart/form-data",
          Authorization: `Bearer ${localStorage.getItem("auth")}`,
        },
      };

      try {
        // Optional: Set a loading state
        // setLoader({ isActive: true });

        const uploadResponse = await axios.post(
          "https://api.brokeragentbase.com/upload",
          // "http://localhost:4000/upload",
          formData,
          config
        );

        const uploadedFileData = uploadResponse.data;
        // setData(uploadedFileData);

        const userData = {
          buyer_document: uploadedFileData,
        };
        setLoader({ isActive: true });
        await user_service.contactUpdate(params.id, userData).then((response) => {
          if (response) {
            setGetContact(response.data);
            setLoader({ isActive: false });
            ContactGetById(params.id);
            setToaster({
              type: "success",
              isShow: true,
              message: "Document Uploaded Successfully",
            });
            setTimeout(() => {
              setToaster((prevToaster) => ({ ...prevToaster, isShow: false }));
            }, 3000);
          } else {
            setLoader({ isActive: false });
            setToaster({
              type: "error",
              isShow: true,
              message: "Error",
            });
          }
        });

       
        setTimeout(() => {
          setToaster((prevToaster) => ({ ...prevToaster, isShow: false }));
        }, 1000);
      } catch (error) {
        // Handle error during upload
        setLoader({ isActive: false });
        setToaster({
          type: "error",
          isShow: true,
          message: "Error uploading the document.",
        });

        setTimeout(() => {
          setToaster((prevToaster) => ({ ...prevToaster, isShow: false }));
        }, 1000);
      }
    }
  };
  
  return (
    <div className="bg-secondary float-left w-100 pt-4 mb-4">
      <Loader isActive={isActive} />
      {isShow && (
        <Toaster
          type={type}
          isShow={isShow}
          toasterBody={toasterBody}
          message={message}
        />
      )}
      <main className="page-wrapper getContact_page_wrap col-lg-10 col-md-10 col-sm-10 col-12">
        <div className="content-overlay">
          <div className="private_contacts">
            <div className="bg-light rounded-3 w-100 p-3 align-items-center mb-4">
              <div className="d-flex d-md-block d-lg-flex align-items-center pt-lg-2 mb-4">
                <div className="edit_getContact_photo">
                  <img
                    width="140"
                    height="140"
                    className="rounded-circle getContact_picture"
                    src={getContact?.image || Pic}
                    // onClick={() => handleStep("1")}
                    alt="getContact"
                  />
                  <label className="documentlabel d-inline-block p-0 border-0 bg-light mb-0">
                    <input
                      id="REPC_real_estate_purchase_contract"
                      type="file"
                      accept={acceptedFileTypes
                        .map((type) => `.${type}`)
                        .join(",")}
                      name="file"
                      onChange={handleFileUploadPersonal}
                      value={getContact.file}
                    />
                    <i class="h2 fi-edit opacity-80 mb-0"></i>
                    {/* <i className="fi-plus me-2"></i> Add a Photo */}
                  </label>
                </div>

                <div className="pt-md-2 pt-lg-0 ps-3 ps-md-0 ps-lg-3 d-flex align-items-center justify-content-between float-start w-100">
                  <div className="">
                    <h2 className="fs-lg mb-0">
                      {getContact?.firstName.charAt(0).toUpperCase() +
                        getContact?.firstName.slice(1)}{" "}
                      {getContact?.lastName}
                    </h2>
                    <ul className="list-unstyled fs-sm mt-2 mb-0 ps-0">
                      {getContact?.phone ? (
                        <li className="pb-0">
                          <a
                            className="nav-link fw-normal p-0"
                            href={`tel:${getContact?.phone ?? ""}`}
                          >
                            <i className="fi-phone opacity-60 me-2"></i>
                            {getContact?.phone}
                          </a>
                        </li>
                      ) : (
                        ""
                      )}

                      {getContact?.email ? (
                        <li className="pb-0">
                          <a
                            className="nav-link fw-normal p-0"
                            href={`tel:${getContact?.email ?? ""}`}
                          >
                            <i className="fi-mail opacity-60 me-2 mt-1"></i>
                            {getContact?.email}
                          </a>
                        </li>
                      ) : (
                        ""
                      )}
                    </ul>
                  </div>
                </div>
              </div>

              <div className="collapse d-block" id="account-nav">
                <ul className="nav nav-tabs d-flex align-items-center justify-content-center my-4">
                  <li className="nav-item mb-md-0 me-md-2">
                    <button
                      className={`nav-link mb-lg-0 mb-md-0 mb-sm-4 mb-3 ${
                        step1 === "1" ? "active" : ""
                      }`}
                      onClick={() => handleStep("1")}
                    >
                      <i class="fa fa-phone" aria-hidden="true"></i>
                    </button>
                    Contact
                  </li>

                  <li className="nav-item mb-md-0 me-md-2">
                    <button
                      className={`nav-link mb-lg-0 mb-md-0 mb-sm-4 mb-3 ${
                        step1 === "2" ? "active" : ""
                      }`}
                      onClick={() => handleStep("2")}
                    >
                      <i className="fa fa-file-text-o me-2"></i>
                    </button>
                    Notes
                  </li>

                  <li className="nav-item mb-md-0 me-md-2">
                    <button
                      className={`nav-link mb-lg-0 mb-md-0 mb-sm-4 mb-3 ${
                        step1 === "5" ? "active" : ""
                      }`}
                      onClick={() => handleStep("5")}
                    >
                      <i class="fa fa-envelope" aria-hidden="true"></i>
                    </button>
                    Email
                  </li>

                  <li className="nav-item mb-md-0 me-md-2">
                    <button
                      className={`nav-link mb-lg-0 mb-md-0 mb-sm-4 mb-3 ${
                        step1 === "6" ? "active" : ""
                      }`}
                      onClick={() => handleStep("6")}
                    >
                      <i class="fa fa-tasks" aria-hidden="true"></i>
                    </button>
                    Task
                  </li>

                  <li className="nav-item mb-md-0 me-md-2">
                    <button
                      className={`nav-link mb-lg-0 mb-md-0 mb-sm-4 mb-3 ${
                        step1 === "7" ? "active" : ""
                      }`}
                      onClick={() => handleStep("7")}
                    >
                      <i class="fa fa-handshake-o" aria-hidden="true"></i>
                    </button>
                    Meeting
                  </li>

                  <li className="nav-item mb-md-0 me-md-2">
                    <button
                      className={`nav-link mb-lg-0 mb-md-0 mb-sm-4 mb-3 ${
                        step1 === "8" ? "active" : ""
                      }`}
                      onClick={() => handleStep("8")}
                    >
                      <i class="fa fa-commenting" aria-hidden="true"></i>
                    </button>
                    Log SMS
                  </li>

                  <li className="nav-item mb-md-0 me-md-2">
                    <button
                      className="nav-link mb-lg-0 mb-md-0 mb-sm-4 mb-3 ms-5"
                      onClick={() => handleStepAndScroll("1")}
                    >
                      <i className="fa fa-file-text-o" aria-hidden="true"></i>
                    </button>
                    Buyer-Broker Agreement
                  </li>
                </ul>
              </div>
            </div>
            {step1 === "1" && (
              <>
                <div className="add_listing w-100">
                  <div className="bg-light rounded-3 border p-3">
                    <div className="row">
                      <h3 className="float-left">Private Contact</h3>
                      <div className="col-sm-12">
                        <input
                          className="me-2"
                          type="checkbox"
                          name="checktask"
                          checked={getContact.checkBussiness === "yes"}
                          onChange={handleChangeCheck}
                        />
                        <label className="col-form-label">Bussiness</label>
                      </div>
                      <div className="col-md-6">
                        <div className="row">
                          {getContact.checkBussiness === "yes" ? (
                            <div className="col-sm-12">
                              <label className="col-form-label">
                                Company Name
                              </label>
                              <input
                                className="form-control"
                                id="inline-form-input"
                                type="text"
                                name="company"
                                placeholder="company"
                                value={getContact.company}
                                onChange={handleChange}
                              />
                            </div>
                          ) : (
                            <div className="col-sm-12">
                              <label className="col-form-label">
                                First Name
                              </label>
                              <input
                                className="form-control"
                                id="inline-form-input"
                                type="text"
                                name="firstName"
                                placeholder="firstName"
                                value={getContact.firstName}
                                onChange={handleChange}
                              />
                            </div>
                          )}

                          <div className="col-sm-12">
                            <label className="col-form-label">Phone</label>
                            <input
                              className="form-control"
                              id="inline-form-input"
                              type="number"
                              name="phone"
                              placeholder="phone"
                              value={getContact.phone}
                              onChange={handleChange}
                            />
                          </div>

                          <div className="col-sm-12">
                            <label className="col-form-label">
                              Contact Type
                            </label>
                            <select
                              className="form-select"
                              id="pr-city"
                              name="personal_contacttype"
                              value={getContact.personal_contacttype}
                              data-bs-binded-element="#personal_contacttype"
                              onChange={handleChange}
                            >
                              <option>--Select--</option>
                              <option value="buyer">Buyer</option>
                              <option value="seller">Seller</option>
                              <option value="both">Buyer & Seller</option>
                              <option value="renter">Renter</option>
                              <option value="investor">Investor</option>
                              <option value="agent">Agent</option>
                              <option value="homeowner">Homeowner</option>
                              <option value="landlord">Landlord</option>
                              {/* <option value="new_contact_type">+ Create New Contact Type</option> */}
                            </select>
                          </div>

                          {/* {showContactType.map((input) => (
                            <div
                              key={input.id}
                              className="col-sm-12"
                            >
                            <label className="col-form-label">New Contact Type</label>
                            <div className="d-flex">
                              <input
                                type="text"
                                className="form-control me-2"
                                placeholder="Enter new contact type"
                                value={input.value}
                                onChange={(e) =>
                                  handleInputContactType(input.id, e.target.value)
                                }
                              />
                              <button
                                type="button"
                                className="btn btn-danger btn-sm"
                                onClick={() => handleDeleteContactType(input.id)}
                              >
                                Delete
                              </button>
                              </div>
                            </div>
                          ))} */}

                          <div className="col-sm-12">
                            <label className="col-form-label">Birthday</label>
                            <input
                              className="form-control"
                              id="inline-form-input"
                              type="date"
                              name="birthday"
                              placeholder="birthday"
                              value={getContact.birthday}
                              onChange={handleChange}
                            />
                          </div>

                          <div className="col-sm-12">
                            <label className="col-form-label">Gender</label>
                            <div className="d-flex">
                              <div className="col-md-3">
                                <input
                                  className="form-check-input me-2"
                                  id="form-radio-four"
                                  type="radio"
                                  name="gender"
                                  value="male"
                                  onChange={handleCheckGender}
                                  checked={getContact?.gender === "male"}
                                />
                                <label className="form-check-label">Male</label>
                              </div>

                              <div className="col-md-3">
                                <input
                                  className="form-check-input me-2"
                                  id="form-radio-four"
                                  type="radio"
                                  name="gender"
                                  value="female"
                                  onChange={handleCheckGender}
                                  checked={getContact?.gender === "female"}
                                />
                                <label className="form-check-label">
                                  Female
                                </label>
                              </div>
                            </div>
                          </div>

                          <div className="col-sm-12 mt-4">
                            <label className="col-form-label">
                              {" "}
                              Street Address{" "}
                            </label>
                            <input
                              className="form-control"
                              id="inline-form-input"
                              type="text"
                              name="address"
                              value={getContact.address}
                              onChange={handleChange}
                              placeholder="Enter Street Address"
                            />
                          </div>

                          <div className="col-sm-12">
                            <label className="col-form-label">City</label>
                            <input
                              className="form-control"
                              id="inline-form-input"
                              type="text"
                              name="city"
                              value={getContact?.city}
                              onChange={handleChange}
                              placeholder="Enter city"
                            />
                          </div>

                          <div className="col-sm-12">
                            <label className="col-form-label">Zipcode</label>
                            <input
                              className="form-control"
                              id="inline-form-input"
                              type="text"
                              name="zip"
                              value={getContact?.zip}
                              onChange={handleChange}
                              placeholder="Enter zipcode"
                            />
                          </div>

                          <div className="col-sm-12">
                            <label className="col-form-label">
                              Selling Time Frame
                            </label>
                            <select
                              className="form-select"
                              id="pr-city"
                              name="selling"
                              value={getContact.selling}
                              onChange={handleChange}
                            >
                              <option value="N/A">N/A</option>
                              <option value="one">0-1 Months</option>
                              <option value="three">1-3 Months</option>
                              <option value="six">3-6 Months</option>
                              <option value="twelve">6-12 Months</option>
                              <option value="twelve_Plus">12+</option>
                            </select>
                          </div>

                          <div className="col-sm-12">
                            <label className="col-form-label">
                              {" "}
                              Last Communication Date
                            </label>
                            <input
                              className="form-control"
                              id="inline-form-input"
                              type="date"
                              name="communication_Date"
                              placeholder="communication_Date"
                              value={getContact.communication_Date}
                              onChange={handleChange}
                            />
                          </div>
                        </div>
                      </div>

                      <div className="col-md-6">
                        <div className="row">
                          {getContact.checkBussiness === "yes" ? (
                            <></>
                          ) : (
                            <div className="col-sm-12">
                              <label className="col-form-label">
                                Last Name
                              </label>
                              <input
                                className="form-control"
                                id="inline-form-input"
                                type="text"
                                name="lastName"
                                placeholder="lastName"
                                value={getContact.lastName}
                                onChange={handleChange}
                              />
                            </div>
                          )}

                          <div className="col-sm-12">
                            <label className="col-form-label">Email</label>
                            <input
                              className="form-control"
                              id="inline-form-input"
                              type="text"
                              name="email"
                              placeholder="email"
                              value={getContact.email}
                              onChange={handleChange}
                            />
                          </div>

                          <div className="col-sm-12">
                            <label className="col-form-label">Source</label>
                            <select
                              className="form-select"
                              id="pr-city"
                              name="source"
                              value={getContact.source}
                              onChange={handleChange}
                            >
                              <option value="">--Select--</option>
                              <option value="COI">COI</option>
                              <option value="family">Family</option>
                              <option value="friend">Friend</option>
                              <option value="past_clients">Past Clients</option>
                              <option value="google">Google</option>
                              <option value="bing">Bing</option>
                              <option value="coi_Referral">COI Referral</option>
                              <option value="facebook">Facebook</option>
                              <option value="instagram">Instagram</option>
                              <option value="sign_Call">Sign Call</option>
                              <option value="open_House">Open House</option>
                              <option value="website">Website</option>
                              <option value="door_Knock">Door Knock</option>
                              <option value="lead_Service">Lead Service</option>
                              <option value="new_source">
                                + Create New Source Type
                              </option>
                            </select>
                          </div>
                          {showInputs.map((input) => (
                            <div key={input.id} className="col-sm-12">
                              <label className="col-form-label">
                                New Source
                              </label>
                              <div className="d-flex">
                                <input
                                  type="text"
                                  className="form-control me-2"
                                  placeholder="Enter new source"
                                  value={input.value}
                                  onChange={(e) =>
                                    handleInputChange(input.id, e.target.value)
                                  }
                                />
                                <button
                                  type="button"
                                  className="btn btn-danger btn-sm"
                                  onClick={() => handleDeleteInput(input.id)}
                                >
                                  Delete
                                </button>
                              </div>
                            </div>
                          ))}

                          {/* <div className="col-sm-12">
                            <label className="col-form-label">
                              Birthday Notification
                            </label>
                            <input
                              className="form-control"
                              id="inline-form-input"
                              type="text"
                              name="birth_Notification"
                              value={getContact.birth_Notification}
                              onChange={handleChange}
                            />
                          </div> */}

                          <div className="col-sm-12">
                            <label className="col-form-label">Website</label>
                            <input
                              className="form-control"
                              id="inline-form-input"
                              type="text"
                              name="web"
                              value={getContact.web}
                              onChange={handleChange}
                            />
                          </div>

                          <div className="col-sm-12">
                            <label className="col-form-label">
                              Street Address 2
                            </label>
                            <input
                              className="form-control"
                              id="inline-form-input"
                              type="text"
                              name="address2"
                              value={getContact?.address2}
                              onChange={handleChange}
                              placeholder="Enter Street Address 2"
                            />
                          </div>

                          <div className="col-sm-12">
                            <label className="col-form-label">State</label>
                            <input
                              className="form-control"
                              id="inline-form-input"
                              type="text"
                              name="state"
                              value={getContact?.state}
                              onChange={handleChange}
                              placeholder="Enter State"
                            />
                          </div>

                          <div className="col-sm-12">
                            <label className="col-form-label">Tags</label>
                            <select
                              className="form-select"
                              id="pr-city"
                              name="tags"
                              value={getContact.tags}
                              onChange={handleChange}
                            >
                              <option value="">-select-</option>
                              <option value="buyer">Buyer</option>
                              <option value="seller">Seller</option>
                              <option value="friend">Friend</option>
                            </select>
                          </div>

                          <div className="col-sm-12">
                            <label className="col-form-label">
                              Buying Time Frame
                            </label>
                            <select
                              className="form-select"
                              id="pr-city"
                              name="buying"
                              value={getContact.buying}
                              onChange={handleChange}
                            >
                              <option value="N/A">N/A</option>
                              <option value="one">0-1 Months</option>
                              <option value="three">1-3 Months</option>
                              <option value="six">3-6 Months</option>
                              <option value="twelve">6-12 Months</option>
                              <option value="twelve_Plus">12+</option>
                            </select>
                          </div>

                          <div className="col-sm-12">
                            <label className="col-form-label">
                              Communication Notes
                            </label>
                            <textarea
                              className="form-control"
                              id="textarea-input"
                              name="communication_Note"
                              onChange={handleChange}
                              autoComplete="on"
                              value={getContact.communication_Note}
                            ></textarea>
                          </div>
                        </div>
                      </div>
                    </div>

                    <div className="row mt-5">
                      <label className="col-form-label">
                        Social Media Channels
                      </label>
                      {socialLinks.map((socialLink, index) => (
                        <div className="col-md-6 d-flex mb-3" key={index}>
                          {/* Render icons based on socialLink.type */}
                          {socialLink.type === "Twitter" && (
                            <i
                              className="h2 fi-twitter opacity-80 mt-3 me-2"
                              aria-hidden="true"
                            ></i>
                          )}
                          {socialLink.type === "Linkedin" && (
                            <i
                              className="h2 fi-linkedin opacity-80 h2 mt-3 me-2"
                              aria-hidden="true"
                            ></i>
                          )}
                          {socialLink.type === "Facebook" && (
                            <i
                              className="h2 fi-facebook opacity-80 mt-3 me-2"
                              aria-hidden="true"
                            ></i>
                          )}
                          {socialLink.type === "Instagram" && (
                            <i
                              className="fi-instagram h2 opacity-80 mt-3 me-2"
                              aria-hidden="true"
                            ></i>
                          )}

                          {socialLink.type === "Tumblr" && (
                            <i
                              className="fi-tumblr h2 opacity-80 mt-3 me-2"
                              aria-hidden="true"
                            ></i>
                          )}
                          {socialLink.type === "YouTube Channel" && (
                            <i
                              className="fi-youtube h2 opacity-80 mt-3 me-2"
                              aria-hidden="true"
                            ></i>
                          )}
                          {socialLink.type === "Pinterest" && (
                            <i
                              className="fi-pinterest h2 opacity-80 mt-3 me-2"
                              aria-hidden="true"
                            ></i>
                          )}
                          {socialLink.type === "TikTok" && (
                            <i
                              className="fi-tiktok h2 opacity-80 mt-3 me-2"
                              aria-hidden="true"
                            ></i>
                          )}
                          {socialLink.type === "Vimeo" && (
                            <i
                              className="fa fa-vimeo h2 opacity-80 mt-3 me-2"
                              aria-hidden="true"
                            ></i>
                          )}
                          {/* Input for URL */}
                          <input
                            className="form-control"
                            type="text"
                            name="linkurl"
                            value={socialLink.linkurl}
                            onChange={(event) =>
                              handleChangeSocial(event, index)
                            }
                          />
                        </div>
                      ))}

                      <div className="col-md-12">
                        <div className="">
                          <button
                            className="btn btn-primary pull-right mt-5"
                            type="button"
                            onClick={handleSubmit}
                          >
                            Submit
                          </button>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>

                <div className="add_listing w-100 mt-3">
                  <div className="bg-light rounded-3 border p-3 mb-3">
                    <div className="row">
                      <h4 className="float-left">Spouse/ Partner Info</h4>
                      {spouseClick ? (
                        <div className="row">
                          <div className="col-md-6">
                            <div className="col-sm-12">
                              <label className="col-form-label">
                                Fisrt Name
                              </label>
                              <input
                                className="form-control"
                                id="inline-form-input"
                                type="text"
                                name="spouse_firstname"
                                value={getContact.spouse_firstname}
                                onChange={handleChange}
                              />
                            </div>

                            <div className="col-sm-12">
                              <label className="col-form-label">Phone</label>
                              <input
                                className="form-control"
                                id="inline-form-input"
                                type="number"
                                name="spouse_number"
                                value={getContact.spouse_number}
                                onChange={handleChange}
                              />
                            </div>

                            <div className="col-sm-12">
                              <label className="col-form-label">
                                Partner Birthday
                              </label>
                              <input
                                className="form-control"
                                id="inline-form-input"
                                type="date"
                                name="spouse_birthday"
                                value={getContact.spouse_birthday}
                                onChange={handleChange}
                              />
                            </div>

                            <div className="col-sm-12">
                              <label className="col-form-label">
                                Relationship? Relationship to the primary
                                contact
                              </label>
                              <input
                                className="form-control"
                                id="inline-form-input"
                                type="text"
                                name="relationship_spouse"
                                value={getContact.relationship_spouse}
                                onChange={handleChange}
                              />
                            </div>
                          </div>
                          <div className="col-md-6">
                            <div className="col-sm-12">
                              <label className="col-form-label">
                                Last Name
                              </label>
                              <input
                                className="form-control"
                                id="inline-form-input"
                                type="text"
                                name="spouse_lastname"
                                value={getContact.spouse_lastname}
                                onChange={handleChange}
                              />
                            </div>

                            <div className="col-sm-12">
                              <label className="col-form-label">Email</label>
                              <input
                                className="form-control"
                                id="inline-form-input"
                                type="text"
                                name="spouse_email"
                                value={getContact.spouse_email}
                                onChange={handleChange}
                              />
                            </div>

                            <div className="col-sm-12">
                              <label className="col-form-label">
                                Anniversary
                              </label>
                              <input
                                className="form-control"
                                id="inline-form-input"
                                type="date"
                                name="spouse_Anniversary"
                                value={getContact.spouse_Anniversary}
                                onChange={handleChange}
                              />
                            </div>

                            <div className="col-sm-12">
                              <label className="col-form-label">
                                Spouse Gender
                              </label>
                              <div className="d-flex">
                                <div className="col-md-3">
                                  <input
                                    className="form-check-input me-2"
                                    id="form-radio-four"
                                    type="radio"
                                    name="spouse_Gender"
                                    value="male"
                                    onChange={handleCheckSpuseGender}
                                    checked={
                                      getContact?.spouse_Gender === "male"
                                    }
                                  />
                                  <label className="form-check-label">
                                    Male
                                  </label>
                                </div>

                                <div className="col-md-3">
                                  <input
                                    className="form-check-input me-2"
                                    id="form-radio-four"
                                    type="radio"
                                    name="spouse_Gender"
                                    value="female"
                                    onChange={handleCheckSpuseGender}
                                    checked={
                                      getContact?.spouse_Gender === "female"
                                    }
                                  />
                                  <label className="form-check-label">
                                    Female
                                  </label>
                                </div>
                              </div>
                            </div>
                          </div>

                          <div className="">
                            <button
                              className="btn btn-primary pull-right mt-5"
                              type="button"
                              onClick={handleSubmitSpouse}
                            >
                              Submit
                            </button>
                          </div>
                        </div>
                      ) : (
                        <div className="pull-left">
                          <button
                            className="btn btn-primary"
                            onClick={handleSpouse}
                          >
                            + Add Spouse
                          </button>
                        </div>
                      )}
                    </div>
                  </div>
                </div>

                <div className="add_listing w-100 mt-3">
                  <div className="bg-light rounded-3 border p-3 mb-3">
                    <div className="row">
                      <h4 className="float-left">
                        Contacts Work/Business Information
                      </h4>
                      <div className="col-md-6">
                        <div className="col-sm-12">
                          <label className="col-form-label">
                            Business Name
                          </label>
                          <input
                            className="form-control"
                            id="inline-form-input"
                            type="text"
                            name="bussiness_name"
                            value={getContact.bussiness_name}
                            onChange={handleChange}
                          />
                        </div>

                        <div className="col-sm-12">
                          <label className="col-form-label">
                            Position Title
                          </label>
                          <input
                            className="form-control"
                            id="inline-form-input"
                            type="text"
                            name="position_Title"
                            value={getContact.position_Title}
                            onChange={handleChange}
                          />
                        </div>

                        <div className="col-sm-12">
                          <label className="col-form-label">
                            Business Social Channels
                          </label>
                          <input
                            className="form-control"
                            id="inline-form-input"
                            type="text"
                            name="business_Social"
                            value={getContact.business_Social}
                            onChange={handleChange}
                          />
                        </div>
                      </div>
                      <div className="col-md-6">
                        <div className="col-sm-12">
                          <label className="col-form-label">
                            Business Address
                          </label>
                          <input
                            className="form-control"
                            id="inline-form-input"
                            type="text"
                            name="bussiness_address"
                            value={getContact.bussiness_address}
                            onChange={handleChange}
                          />
                        </div>

                        <div className="col-sm-12">
                          <label className="col-form-label">
                            Business Phone
                          </label>
                          <input
                            className="form-control"
                            id="inline-form-input"
                            type="number"
                            name="business_Phone"
                            value={getContact.business_Phone}
                            onChange={handleChange}
                          />
                        </div>

                        <div className="col-sm-12">
                          <label className="col-form-label">
                            Business Website
                          </label>
                          <input
                            className="form-control"
                            id="inline-form-input"
                            type="text"
                            name="business_Website"
                            value={getContact.business_Website}
                            onChange={handleChange}
                          />
                        </div>
                      </div>
                      <div className="">
                        <button
                          className="btn btn-primary pull-right mt-5"
                          type="button"
                          onClick={handleSubmitBussiness}
                        >
                          Submit
                        </button>
                      </div>
                    </div>
                  </div>
                </div>

                {getContact.birthday ? (
                  <div className="add_listing w-100 mt-3">
                    <div className="bg-light rounded-3 border p-3 mb-3">
                      <div className="row">
                        <h4 className="float-left">Birthday Notification</h4>
                        <div className="col-md-6">
                          <label className="col-form-label">
                            Birthday Notification
                          </label>
                          <select
                            className="form-select"
                            id="pr-city"
                            name="birthday_noticeTime"
                            value={birthdayNotification.birthday_noticeTime}
                            onChange={handleChangeBirthday}
                          >
                            <option>--Select--</option>
                            <option value="one_week">One week</option>
                            <option value="two_week">Two weeks</option>
                            <option value="one_month">One Month</option>
                          </select>
                        </div>

                        <div className="col-md-6">
                          <label className="col-form-label">
                            Birthday🎂🎉 Message
                          </label>
                          <textarea
                            className="form-control mt-0"
                            id="textarea-input"
                            rows="1"
                            name="message"
                            value={birthdayNotification.message}
                            onChange={handleChangeBirthday}
                            autoComplete="on"
                          ></textarea>
                        </div>
                        <div className="">
                          <button
                            className="btn btn-primary pull-right mt-5"
                            type="button"
                            onClick={handleSubmitBirthday}
                          >
                            Submit
                          </button>
                        </div>
                      </div>
                    </div>
                  </div>
                ) : (
                  ""
                )}

                <div className="add_listing w-100 mt-3" ref={scrollRef}>
                  <div className="bg-light rounded-3 border p-3 mb-3">
                    <div className="row">
                      <h4>Exclusive Buyer-Broker Agreement & Agency Disclosure Agreement</h4>
                      <p className="float-left mb-0">
                        The National Association of Realtors (NAR) now requires
                        buyers nationwide, including Utah, to sign a buyer
                        agreement before viewing any properties. This policy
                        ensures transparency and formalizes the commitment
                        between buyers and brokers. The Buyer-Broker Agreement
                        <b>
                          {" "}
                          must be signed prior to the first property showing.
                        </b>
                      </p>
                      <p>Please use this section to upload a copy of your Buyer-Broker Agreement, along with its terms, to the client’s profile. The Buyer-Broker can then later be reassigned to a contract. We are now required to maintain a copy on file at all times.</p>
                      <div className="col-md-6">
                        <div className="col-sm-12">
                          <label className="col-form-label d-flex">
                            Buyers Name’s 
                            {/* <div> */}
                          <i className="fi-help opacity-80 ms-2 mt-1" style={{ cursor: "pointer" }}></i>
                            <span className="tooltiptext">
                          <div className="col-md-12">
                            <p className="gray-text p-2">
                            Please make sure your Buyer-Broker includes the names of all parties you're showing homes to.
                            </p>
                          </div>
                           </span>
                            {/* </div> */}
                          </label>
                          <input
                            className="form-control"
                            id="inline-form-input"
                            type="text"
                            name="buyers_name"
                            value={getContact.buyers_name}
                            onChange={handleChange}
                          />
                        </div>
                      </div>
                      <div className="col-md-6">
                        <div className="col-sm-12">
                          <label className="col-form-label">
                            Buyer-Broker Agreement Expires Date and time
                          </label>

                          <DateTimePicker
                            onChange={handleDateChange}
                            value={getContact.buyer_dateTime}
                            className="form-control"
                            id="inline-form-input"
                          />
                        </div>
                      </div>
                      <div className="col-md-6">
                        <div className="col-sm-12">
                          <label className="col-form-label d-flex">
                            Buyer Brokerage Fee
                            <div className="form-check mb-0 ms-3">
                              <input
                                className="form-check-input"
                                type="radio"
                                value="fee-percentage"
                                onChange={CheckPercentage}
                                checked={
                                  getContact?.brokerage_Fee === "fee-percentage"
                                }
                              />
                              <label
                                className="form-check-label"
                                id="radio-level1"
                              >
                                %
                              </label>
                            </div>
                            &nbsp;
                            <div className="form-check mb-0">
                              <input
                                className="form-check-input"
                                type="radio"
                                value="doller-percentage"
                                onChange={CheckPercentage}
                                checked={
                                  getContact?.brokerage_Fee ===
                                    "doller-percentage" ||
                                  !getContact.brokerage_Fee
                                }
                              />
                              <label
                                className="form-check-label"
                                id="radio-level1"
                              >
                                $
                              </label>
                            </div>
                          </label>

                          <div className="d-flex">
                            {getContact?.brokerage_Fee === "fee-percentage" ? (
                              <></>
                            ) : (
                              <span className="mt-2">
                                {
                                  getContact?.brokerage_Fee ===
                                  "doller-percentage"
                                    ? "$"
                                    : ""
                                  //  isPercentageReferral ?? ""/
                                }
                              </span>
                            )}
                            <input
                              className="form-control"
                              type="number"
                              placeholder="00"
                              name="buyer_broker_fee"
                              value={getContact.buyer_broker_fee}
                              onChange={handleBuyerBrokerFee}
                            />
                            <span className="mt-2">
                              {getContact.brokerage_Fee === "fee-percentage"
                                ? "%"
                                : ""}
                            </span>
                          </div>
                        </div>
                      </div>

                      <div className="col-md-6">
                        <div className="col-sm-12">
                          <label className="col-form-label">
                            Protection Period Date and time
                          </label>

                          <DateTimePicker
                            onChange={handleDateProtection}
                            value={getContact.protecion_dateTime}
                            className="form-control"
                            id="inline-form-input"
                          />
                        </div>
                      </div>

                      <div className="col-md-6">
                        <div className="col-sm-12">
                          <label className="col-form-label">
                            Exclusive Buyer-Broker Agreement & Agency Disclosure
                            Agreement
                          </label>

                          {getContact.buyer_document ? (
                            <>
                                {/\.(png|jpg|jpeg)$/i.test(getContact.buyer_document) ? (
                                    <a
                                      className="pull-left mr-3"
                                      style={{ width: "100px" }}
                                      href={getContact.buyer_document}
                                      target="_blank"
                                      rel="noopener noreferrer"
                                      download=""
                                    >
                                      {/* Display image preview */}
                                      <img
                                        src={getContact.buyer_document}
                                        alt="Uploaded File"
                                        style={{ width: "100%", height: "auto", objectFit: "cover" }}
                                      />
                                    </a>
                                  ) : (
                                    <a
                                    className="pull-left mr-3"
                                    style={{ width: "100px" }}
                                      href={getContact.buyer_document}
                                      target="_blank"
                                      rel="noopener noreferrer"
                                      download=""
                                    >
                                      {/* Render PDF file */}
                                      <Document
                                        className="document_filePersonal"
                                        file={getContact.buyer_document}
                                        renderMode="canvas"
                                      >
                                        <Page pageNumber={1} />
                                      </Document>
                                    </a>
                                  )}

                                  {
                                   getContact.buyer_document_status === "yes" ?
                                   <span className="ms-5" style={{fontSize:"20px", color:"green"}}>Document Approved<i class="fi-check text-success ms-2"></i></span>
                                   :
                            <label className="btn btn-primary btn-lg w-100 mb-3 documentlabel d-inline-block mt-3">
                              <input
                                id="REPC_real_estate_purchase_contract"
                                type="file"
                                accept={acceptedFileTypes
                                  .map((type) => `.${type}`)
                                  .join(",")}
                                name="file"
                                onChange={handleUpdateDocument}
                              />
                             Update Document
                            </label>
                                  }
                            </>
                          ) : (
                              <>
                              {data ? (
                                <>
                                  {/\.(png|jpg|jpeg)$/i.test(data) ? (
                                    <a
                                      className="pull-left mr-3"
                                      style={{ width: "100px" }}
                                      href={data}
                                      target="_blank"
                                      rel="noopener noreferrer"
                                      download=""
                                    >
                                      {/* Display image preview */}
                                      <img
                                        src={data}
                                        alt="Uploaded File"
                                        style={{ width: "100%", height: "auto", objectFit: "cover" }}
                                      />
                                    </a>
                                  ) : (
                                    <a
                                      className="pull-left mr-3"
                                      style={{ width: "100px" }}
                                      href={data}
                                      target="_blank"
                                      rel="noopener noreferrer"
                                      download=""
                                    >
                                      {/* Render PDF file */}
                                      <Document
                                        className="document_filePersonal"
                                        file={data}
                                        renderMode="canvas"
                                      >
                                        <Page pageNumber={1} />
                                      </Document>
                                    </a>
                                  )}
                                </>
                              ) : (
                                <input
                                  className="file-uploader file-uploader-grid"
                                  type="file"
                                  accept=".png, .jpg, .jpeg, .pdf"
                                  name="file"
                                  onChange={handleFileUpload}
                                />
                              )}
                              </>
                            )}
                        </div>
                      </div>

                      <div className="">
                        <button
                          className="btn btn-primary pull-right mt-5"
                          type="button"
                          onClick={handleSubmitDocument}
                        >
                          Submit
                        </button>
                      </div>
                    </div>
                  </div>
                </div>
              </>
            )}

            {/* Add Note */}
            {step1 === "2" && (
              <div className="bg-light rounded-3 border p-3 mb-3">
                <div className="row">
                  <div className="d-flex align-items-center justify-content-between mb-4">
                    <h3 className="float-left">Add Notes</h3>
                    <div className="pull-right">
                      <button
                        type="button"
                        className="btn btn-primary pull-right btn-sm"
                        data-toggle="modal"
                        data-target="#addNotes"
                      >
                        <i className="fi-plus me-2"></i>Add Notes
                      </button>
                    </div>
                  </div>
                  {notesData && notesData.length > 0 ? (
                    notesData.map((item, index) => (
                      <div key={item._id || index} className="mt-3">
                        <div className="notes-main d-flex align-items-center justify-content-between">
                          <div className="pull-left">
                            <img
                              className="rounded-circle image-note mt-3 mb-3"
                              src={item.contact_image}
                              alt="image"
                              width="75"
                              height="75"
                            />
                            <strong className="ms-3">
                              {item.contact_name}
                            </strong>
                          </div>
                          <div className="pull-right">
                            <button
                              className="btn btn-danger pull-right btn-sm ms-3"
                              onClick={() => handleDelete(item._id)}
                            >
                              <i className="fa fa-trash" aria-hidden="true"></i>
                            </button>

                            <button
                              type="button"
                              className="btn btn-secondary pull-right btn-sm"
                              data-toggle="modal"
                              data-target="#addNotesUpdate"
                              onClick={() =>
                                handleUpdate(item._id, item.message)
                              }
                            >
                              <i
                                className="fa fa-pencil-square-o"
                                aria-hidden="true"
                              ></i>
                            </button>
                          </div>
                        </div>
                        <div className="float-left w-100">
                          <p className="float-left w-100 mb-1">
                            {item.message}
                          </p>
                          <i className="float-left w-100 mb-0 fw-normal fs-sm text-muted">
                            <small>
                              {new Date(item.created).toLocaleString("en-US", {
                                month: "short",
                                day: "2-digit",
                                year: "numeric",
                                hour: "2-digit",
                                minute: "2-digit",
                                hour12: true,
                              })}
                            </small>
                          </i>
                        </div>
                      </div>
                    ))
                  ) : (
                    <div className="bg-light border rounded-3 p-3">
                      <p className="text-center my-5">No notes found.</p>
                    </div>
                  )}
                </div>
              </div>
            )}

            {/* Add Personal task */}
            {step1 === "6" && (
              <div className="bg-light rounded-3 border p-3 mb-3">
                <div className="row">
                  <div className="d-flex align-items-center justify-content-between mb-4">
                    <h3 className="float-left">Add Task</h3>
                    <div className="pull-right">
                      <button
                        type="button"
                        className="btn btn-primary pull-right btn-sm"
                        data-toggle="modal"
                        data-target="#addTasks"
                        onClick={handleTask}
                      >
                        <i className="fi-plus me-2"></i>Add Task
                      </button>
                    </div>
                  </div>

                  <div className="col-md-12">
                    {noticereceived && noticereceived.length > 0 ? (
                      noticereceived.map((dataItem, index) => (
                        <div
                          className="notes-main bg-light border-top border-0 p-3 rounded-3"
                          key={index}
                          // onClick={() => handleClick(dataItem._id)}
                        >
                          <div className="row">
                            <div className="d-flex align-items-center justify-content-start col-md-6">
                              <img
                                className="rounded-circle image-note mt-3 mb-3"
                                src={getContact.image || Pic}
                                alt="image"
                                width="75"
                                height="75"
                              />
                              <div className="ms-3">
                                <strong>{dataItem.taskTitle}</strong>
                                <p id="" className="mb-0">
                                  <strong>From:</strong> {getContact.firstName}{" "}
                                  {getContact.lastName}
                                </p>
                              </div>
                            </div>
                            <div className="col-md-6 mt-4">
                              <button
                                className="btn btn-primary pull-right"
                                onClick={() => handleComplted(dataItem._id)}
                              >
                                Mark As Completed
                              </button>
                            </div>
                          </div>
                        </div>
                      ))
                    ) : (
                      <div className="bg-light border rounded-3 p-3">
                        <p className="text-center my-5">No tasks found.</p>
                      </div>
                    )}
                  </div>
                </div>
              </div>
            )}

            {step1 === "5" && (
              <div className="bg-light rounded-3 border p-3 mb-3">
                <div className="row">
                  <h6>Comming Soon</h6>
                </div>
              </div>
            )}

            {step1 === "7" && (
              <div className="bg-light rounded-3 border p-3 mb-3">
                <div className="row">
                  <h6>Comming Soon</h6>
                </div>
              </div>
            )}

            {step1 === "8" && (
              <div className="bg-light rounded-3 border p-3 mb-3">
                <div className="row">
                  <h6>Comming Soon</h6>
                </div>
              </div>
            )}
          </div>
        </div>

        <div className="modal" role="dialog" id="addNotes">
          <div
            className="modal-dialog modal-lg modal-dialog-scrollable"
            role="document"
          >
            <div className="modal-content">
              <div className="modal-header">
                <h4 className="modal-title">Add Notes</h4>
                <button
                  className="btn-close"
                  type="button"
                  data-dismiss="modal"
                  id="close"
                  aria-label="Close"
                ></button>
              </div>
              <div className="modal-body fs-sm">
                <textarea
                  className="form-control"
                  rows="5"
                  name="message"
                  onChange={handleChangeNote}
                  autoComplete="on"
                  value={formValues.message}
                  style={{
                    border: formErrors?.message ? "1px solid red" : "",
                  }}
                />
                {formErrors.message && (
                  <div className="invalid-tooltip">{formErrors.message}</div>
                )}

                <div className="pull-right mt-3">
                  <button
                    className="btn btn-primary pull-right"
                    type="button"
                    onClick={handleSubmitNote}
                  >
                    Add Note
                  </button>
                </div>
              </div>
            </div>
          </div>
        </div>

        <div className="modal" role="dialog" id="addTasks">
          <div
            className="modal-dialog modal-lg modal-dialog-scrollable"
            role="document"
          >
            <div className="modal-content">
              <div className="modal-header">
                <h4 className="modal-title">Add Task</h4>
                <button
                  className="btn-close"
                  type="button"
                  data-dismiss="modal"
                  id="closeModalTask"
                  onClick={handleTask}
                  aria-label="Close"
                ></button>
              </div>
              <div className="modal-body fs-sm">
                <h5>Task Recipient</h5>
                <div className="float-left w-100 p-3">
                  <p className="pull-left mb-0">
                    <img
                      className="rounded-circle profile_task mb-2"
                      src={getContact?.image || Pic}
                      alt="profile picture"
                    />
                  </p>
                  <span className="ms-2 pull-left">
                    {getContact.firstName}&nbsp;{getContact.lastName}
                    <p className="mb-0">
                      {getContact?.active_office
                        ? getContact?.active_office
                        : "Corporate"}
                    </p>
                  </span>
                </div>

                <div className="float-start w-100 mt-2">
                  <h6 lassName="float-left">Task Title</h6>
                  <input
                    className="form-control"
                    id="inline-form-input"
                    type="text"
                    name="taskTitle"
                    value={formValuesTask.taskTitle}
                    onChange={handleChangeTask}
                    style={{
                      border: formErrorsTask?.taskTitle
                        ? "1px solid red"
                        : "1px solid #00000026",
                    }}
                  />
                  {formErrorsTask.taskTitle && (
                    <div className="invalid-tooltip">
                      {formErrorsTask.taskTitle}
                    </div>
                  )}
                </div>

                <div className="float-start w-100 mt-3 mb-4">
                  <h6>Enter Your Request or Question</h6>
                  <textarea
                    className="form-control mt-0"
                    id="textarea-input"
                    rows="5"
                    name="question"
                    onChange={handleChangeTask}
                    autoComplete="on"
                    value={formValuesTask.question}
                    style={{
                      border: formErrorsTask?.question
                        ? "1px solid red"
                        : "1px solid #00000026",
                    }}
                  />
                  {formErrorsTask.question && (
                    <div className="invalid-tooltip">
                      {formErrorsTask.question}
                    </div>
                  )}
                </div>

                <div className="row">
                  <div className="col-md-6">
                    <h6>Select Due Date</h6>
                    <input
                      className="form-control"
                      id="inline-form-input"
                      type="date"
                      name="taskDate"
                      value={formValuesTask.taskDate}
                      onChange={handleChangeTask}
                    />
                  </div>
                  <div className="col-md-6">
                    <h6>Time</h6>
                    <input
                      className="form-control"
                      id="inline-form-input"
                      type="text"
                      name="taskTime"
                      value={formValuesTask.taskTime}
                      onChange={handleChangeTask}
                    />
                  </div>
                </div>

                <div className="pull-right mt-3">
                  <button
                    className="btn btn-primary pull-right"
                    type="button"
                    onClick={handleSubmitTask}
                  >
                    Send Task
                  </button>
                </div>
              </div>
            </div>
          </div>
        </div>

        <div className="modal" role="dialog" id="addNotesUpdate">
          <div
            className="modal-dialog modal-lg modal-dialog-scrollable"
            role="document"
          >
            <div className="modal-content">
              <div className="modal-header">
                <h4 className="modal-title">Update Notes</h4>
                <button
                  className="btn-close"
                  type="button"
                  data-dismiss="modal"
                  id="closeModal"
                  aria-label="Close"
                ></button>
              </div>
              <div className="modal-body fs-sm">
                <textarea
                  className="form-control"
                  rows="5"
                  name="message"
                  onChange={handleChangeNote}
                  autoComplete="on"
                  value={formValues.message}
                />

                <div className="pull-right mt-3">
                  <button
                    className="btn btn-primary pull-right"
                    type="button"
                    onClick={handleUpdateData}
                  >
                    Update Note
                  </button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </main>
    </div>
  );
};
export default PrivateContactEdit;

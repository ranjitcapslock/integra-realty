import React, { useEffect, useState } from "react";
import { NavLink, useParams, useNavigate } from "react-router-dom";
import user_service from "../service/user_service";
import Loader from "../../Pages/Loader/Loader.js";
import Toaster from "../../Pages/Toaster/Toaster";
import jwt from "jwt-decode";

const DeailyDigest = () => {
  const [loader, setLoader] = useState({ isActive: null });
  const { isActive } = loader;
  const [toaster, setToaster] = useState({
    types: null,
    isShow: null,
    toasterBody: "",
    message: "",
  });
  const { types, isShow, toasterBody, message } = toaster;

  const params = useParams();
  const navigate = useNavigate();

  const [contactData, setContactData] = useState("");

  useEffect(() => {
    window.scrollTo(0, 0);
    const ContactGetById = async () => {
      try {
        const response = await user_service.contactGetById(params.id);
        setContactData(response.data);
      } catch (error) {
        console.error("Error fetching contact data:", error);
      }
    };

    ContactGetById(params.id);
  }, [params.id]);

  const handleBack = () => {
    navigate(`/contact-profile/${params.id}`);
  };
  const [checkAll, setCheckAll] = useState("never");
  const [notificationId, setNotificationId] = useState("");

  const CheckBox = (e) => {
    setCheckAll(e.target.value);
  };
  const handleSubmit = async () => {
    const userData = {
      agentId: jwt(localStorage.getItem("auth")).id,
      emailDigests: checkAll,
    };
    console.log(userData);
    try {
      setLoader({ isActive: true });
      let response;

      if (notificationId) {
        response = await user_service.notificationUpdate(
          notificationId,
          userData
        );
        console.log(response);
      } else {
        // First time, post new data
        response = await user_service.notificationPost(userData);
        if (response && response.data._id) {
          setNotificationId(response.data._id); // Store the ID for future updates
        }
      }

      if (response) {
        setLoader({ isActive: false });
        setToaster({
          type: "Notification",
          isShow: true,
          message: notificationId
            ? "Notification Updated Successfully"
            : "Notification Added Successfully",
        });

        setTimeout(() => {
          setToaster((prevToaster) => ({ ...prevToaster, isShow: false }));
        }, 2000);
        notificationGetAllData()
      }
    } catch (err) {
      setLoader({ isActive: false });
      setToaster({
        type: "API Error",
        isShow: true,
        message: "API Error",
      });
    }
  };

  useEffect(() => {
    window.scrollTo(0, 0);
    setLoader({ isActive: true });
    notificationGetAllData();
  }, []);


  const notificationGetAllData = async () => {
    const agentId = jwt(localStorage.getItem("auth")).id;
    if (agentId) {
      try {
        const response = await user_service.notificationGetById(agentId);
        setLoader({ isActive: false });
        if (response) {
          const data = response.data.data;
          if (data && data[0]?.emailDigests) {
            setCheckAll(data[0]?.emailDigests);
          }
          if (data && data[0]?._id) {
            setNotificationId(data[0]?._id); 
          }
        }
      } catch (err) {
        setLoader({ isActive: false });
      }
    }
  };

  return (
    <div className="bg-secondary float-left w-100 pt-4 mb-4">
      <Loader isActive={isActive} />
      {isShow && (
        <Toaster
          types={types}
          isShow={isShow}
          toasterBody={toasterBody}
          message={message}
        />
      )}
      <main className="page-wrapper">
        <div className="d-flex align-items-center justify-content-start">
          <h3 className="mb-0 ms-3">
            {contactData?.firstName} {contactData?.lastName}
          </h3>
        </div>
        <hr className="m-3" />
        <div className="row">
          <div className="mb-5">
            <button
              className="btn btn-primary pull-right ms-3"
              onClick={handleSubmit}
            >
              Update Frequency
            </button>
            <button
              className="btn btn-secondary pull-right"
              onClick={handleBack}
            >
              Cancel
            </button>
          </div>

          <div className="col-md-8 ms-5">
            <div className="bg-light border rounded-3 p-3 mt-4">
              <h6>Email Digests</h6>
              <p>
                summary digest email of new posts, events, and other content is
                available to help you stay on top of what's happening in and
                around your office. A great way to stay connected.
              </p>

              <div className="mt-4">
                <label className="form-check-label ms-2 mb-3" id="radio-level1">
                  How often would you like to receive the digest?
                </label>
                 
                 {console.log(checkAll)}
                <div className="col-md-6 form-check">
                  <input
                    className="form-check-input"
                    id="form-radio-4"
                    type="radio"
                    name="digest"
                    value="never"
                    checked={checkAll === "never"}
                    onChange={CheckBox}
                  />
                  <label className="form-check-label ms-2" id="radio-level1">
                    Never
                  </label>
                </div>
                <div className="col-md-6 form-check">
                  <input
                    className="form-check-input"
                    id="form-radio-4"
                    type="radio"
                    name="digest"
                    value="daily"
                    checked={checkAll === "daily"}
                    onChange={CheckBox}
                  />
                  <label className="form-check-label ms-2" id="radio-level1">
                    Daily
                  </label>
                </div>
                <div className="col-md-6 form-check">
                  <input
                    className="form-check-input"
                    id="form-radio-4"
                    type="radio"
                    name="digest"
                    value="weekly"
                    checked={checkAll === "weekly"}
                    onChange={CheckBox}
                  />
                  <label className="form-check-label ms-2" id="radio-level1">
                    Weekly
                  </label>
                </div>
                <div className="col-md-6 form-check">
                  <input
                    className="form-check-input"
                    id="form-radio-4"
                    type="radio"
                    name="digest"
                    value="monthly"
                    checked={checkAll === "monthly"}
                    onChange={CheckBox}
                  />
                  <label className="form-check-label ms-2" id="radio-level1">
                    Monthly
                  </label>
                </div>
              </div>
            </div>
          </div>
        </div>
      </main>
    </div>
  );
};

export default DeailyDigest;

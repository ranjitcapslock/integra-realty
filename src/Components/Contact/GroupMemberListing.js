import React, { useState, useEffect } from "react";
import Loader from "../../Pages/Loader/Loader.js";
import Toaster from "../../Pages/Toaster/Toaster.js";
import { NavLink, useNavigate } from "react-router-dom";
import { useParams } from "react-router-dom";
import user_service from "../service/user_service";
import defaultpropertyimage from "../../Components/img/defaultpropertyimage.jpeg";

const GroupMemberListing = () => {
  const navigate = useNavigate();
  const [loader, setLoader] = useState({ isActive: null });
  const { isActive } = loader;
  const [toaster, setToaster] = useState({
    types: null,
    isShow: null,
    toasterBody: "",
    message: "",
  });
  const { types, isShow, toasterBody, message } = toaster;
  const params = useParams();



  const [contactGroupMember, setContactGroupMember] = useState("");

  const ContactGroupMemberGetAll = async () => {
    try {
      setLoader({ isActive: true });
      const response = await user_service.contactGroupMemberGet(params.id);
      setLoader({ isActive: false });
  
      if (response && response.data) {
        console.log(response);
        // setContactGroupMember(response.data.data);
      }
    } catch (error) {
      console.error('Error fetching data:', error);
      setLoader({ isActive: false });
    }
  };
  
  useEffect(() => {
    window.scrollTo(0, 0);
    setLoader({ isActive: true });
    ContactGroupMemberGetAll();
  }, []);
  const handleContactPeofile = (id)=>{
    navigate(`/contact-profile/${id}`);
   }

   const formatDate = (dateString) => {
    const date = new Date(dateString);
    const options = {
      year: 'numeric',
      month: '2-digit',
      day: '2-digit',
      // hour: '2-digit',
      // minute: '2-digit',
      // second: '2-digit',
    };
    return new Intl.DateTimeFormat('en-US', options).format(date);
  };

 
  return (
    <div className="bg-secondary float-left w-100 pt-4 mb-4">
      <Loader isActive={isActive} />
      {isShow && (
        <Toaster
          types={types}
          isShow={isShow}
          toasterBody={toasterBody}
          message={message}
        />
      )}
      <div className="content-overlay mt-0">
        <main className="page-wrapper getContact_page_wrap">
          {/* <!-- Page container--> */}
          <div className=" ">
            <div className="d-flex align-items-center justify-content-between float-left mb-3">
              <h3 className="text-white text-left mb-0">
                {/* {summary.group_name} */}
              </h3>
            </div>
            <div className="bg-light border rounded-3 p-3">
              <div className="row">
                <div className="col-md-8">
                <table
                id="DepositLedger"
                className="table table-striped mb-0"
                width="100%"
                border="0"
                cellSpacing="0"
                cellPadding="0"
              >
                <tbody>
                  <table
                    id="DepositLedger"
                    className="table table-striped mb-0"
                    width="100%"
                    border="0"
                    cellSpacing="0"
                    cellPadding="0"
                  >
                    <thead>
                      <tr>
                        <th>Member</th>
                        <th>Office</th>
                        <th>Joined</th>
                        {/* <th>DELETE</th>  */}
                        {/* {/* <th>EDIT</th> */}
                      </tr>
                    </thead>
                    <tbody>
                      {Array.isArray(contactGroupMember) &&
                      contactGroupMember.length > 0 ? (
                        contactGroupMember.map((item, index) => (
                          <React.Fragment key={index}>
                            {item.group_member_details.map(
                              (member, memberIndex) => (
                                <tr key={memberIndex}>
                                  <td onClick={()=>handleContactPeofile(member._id)}>
                                    <img
                                   className="rounded-circlee profile_picture"
                                   height="30"
                                   width="30"
                                   src={
                                    member.image &&
                                    member.image !== "image"
                                     ?  member.image
                                     : defaultpropertyimage
                                 }
                                  /> {member.firstName}  {member.lastName}
                                   </td>
                                   <td>{member.active_office}</td>
                                   <td>{formatDate(item.join_date)}</td>
                                   {/* <td><button className="btn btn-secondary btn-sm" onClick={()=>handleDelete()}>Delete</button></td> */}
                                </tr>
                              )
                            )}
                          </React.Fragment>
                        ))
                      ) : (
                        <tr>
                          <td colSpan="2">No data to display.</td>
                        </tr>
                      )}
                    </tbody>
                  </table>
                </tbody>
              </table>
                 
                </div>
            
              </div>
            </div>
          </div>
        </main>
      </div>
    </div>
  );
};

export default GroupMemberListing;

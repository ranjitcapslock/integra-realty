import React, { useState, useEffect } from "react";
import user_service from "../service/user_service";
import { NavLink, useNavigate, useParams } from "react-router-dom";
import Loader from "../../Pages/Loader/Loader.js";
import Toaster from "../../Pages/Toaster/Toaster.js";

import { formatDistanceToNow } from "date-fns";

const SinglePageTestimonials = () => {
  const [loader, setLoader] = useState({ isActive: null });
  const [toaster, setToaster] = useState({
    types: null,
    isShow: null,
    toasterBody: "",
    message: "",
  });

  const { isActive } = loader;
  const { types, isShow, toasterBody, message } = toaster;
  const params = useParams();
  const navigate = useNavigate();

  const [contactTestimonials, setContactTestimonials] = useState("");
  const [transactionData, setTransactionData] = useState("");

  const TestimonialsGetIds = async () => {
    const clientId = params.id;
    if (clientId) {
      setLoader({ isActive: true });
      await user_service.TestimonialsGetId(clientId).then((response) => {
        if (response) {
          setLoader({ isActive: false });
          console.log(response);
          setContactTestimonials(response.data);
          TransactionGetById(response.data.transactionId);
        }
      });
    }
  };

  const TransactionGetById = async (id) => {
    await user_service.transactionGetById(id).then((response) => {
      if (response) {
        setLoader({ isActive: false });
        console.log(response);
        setTransactionData(response.data);
      }
    });
  };

  useEffect(() => {
    TestimonialsGetIds();
  }, []);

  return (
    <div className="bg-secondary float-left w-100 pt-4 mb-4">
      <Loader isActive={isActive} />
      {isShow && (
        <Toaster
          types={types}
          isShow={isShow}
          toasterBody={toasterBody}
          message={message}
        />
      )}
      <div className="m-auto float-none w-75 bg-light rounded-3 p-4">
        <main className="page-wrapper profile_page_wrap mb-3">
          <div className="content-overlay">
            {transactionData ? (
              <>
                <div className="bg-light border rounded-3 p-3">
                  <div className="mt-2 row">
                    {transactionData?.propertyDetail?.image ? (
                      <div className="col-md-4">
                        <img
                          className="review_image mb-2"
                          src={transactionData?.propertyDetail?.image}
                        />
                      </div>
                    ) : (
                      ""
                    )}

                    <div className="col-md-8">
                      <div className="float-left mb-3 mt-3">
                        {contactTestimonials.displayName ? (
                          <h6>
                            {contactTestimonials.displayName} helped the
                            reviewer <br />
                            {transactionData?.represent === "buyer"
                              ? "purchase"
                              : "sell"}{" "}
                            the property
                          </h6>
                        ) : (
                          ""
                        )}
                      </div>

                      <div className="float-left mb-3">
                        <h6>
                          ${transactionData?.propertyDetail?.price ?? "0"}
                        </h6>
                      </div>

                      <div className="float-left mb-3 mt-3">
                        {transactionData?.propertyDetail?.streetAddress
                          ? transactionData?.propertyDetail?.streetAddress + ","
                          : ""}
                        <br />
                        {transactionData?.propertyDetail?.city
                          ? transactionData?.propertyDetail?.city + ", "
                          : ""}{" "}
                        {transactionData?.propertyDetail?.state + ", "
                          ? transactionData?.propertyDetail?.state
                          : ""}{" "}
                        {transactionData?.propertyDetail?.zipCode
                          ? transactionData?.propertyDetail?.zipCode + ", "
                          : ""}
                      </div>
                      <div className="testimanal_listing">
                        {transactionData?.propertyDetail?.bed ? (
                          <div className="testimanal_icon">
                            <i class="h2 fi-bed opacity-80"></i>
                            <h6>
                              {transactionData?.propertyDetail?.bed ?? "0"}
                            </h6>
                          </div>
                        ) : (
                          ""
                        )}

                        {transactionData?.propertyDetail?.baths ? (
                          <div className="testimanal_icon">
                            <i class="h2 fi-bath opacity-80"></i>
                            <h6>
                              {transactionData?.propertyDetail?.baths ?? "0"}
                            </h6>
                          </div>
                        ) : (
                          ""
                        )}

                        {transactionData?.propertyDetail?.garage ? (
                          <div className="testimanal_icon">
                            <i class="h2 fi-car opacity-80"></i>
                            <h6>
                              {transactionData?.propertyDetail?.garage ?? "0"}
                            </h6>
                          </div>
                        ) : (
                          ""
                        )}
                      </div>
                    </div>
                  </div>
                  <hr className="mt-4"></hr>
                    <div className="row p-3">
                    <div className="col-md-6">
                      <div className="float-left d-flex w-100">
                        <p>
                          {Array(
                            parseInt(contactTestimonials.star) > 0
                              ? parseInt(contactTestimonials.star)
                              : 1 // Default to 1 star if the rating is 0 or undefined
                          )
                            .fill()
                            .map((_, index) => (
                              <i
                                key={index}
                                style={{
                                  color: "#FFBC0B",
                                  fontSize: "20px",
                                  margin: "2px",
                                }}
                                className="fa fa-star"
                                aria-hidden="true"
                              ></i>
                            ))}
                        </p>
                       <p className="ms-2">{formatDistanceToNow(new Date(contactTestimonials.timeStamp), { addSuffix: true })}</p>
                      </div>
                      
                    </div>

                      <div className="col-md-6">
                    <div className="d-flex justify-content-end align-items-center">
                    <i class="h2 fi-checkbox-checked opacity-80"></i>
                    <h6 className="ms-2">Verified Review</h6>
                      </div>
                      </div>
                      </div>
                     
                  

                    <div className="row">
                    {contactTestimonials.headlineReview ? (
                        <p>
                          <b>Headline Review:</b>{" "}
                          {contactTestimonials.headlineReview}
                        </p>
                      ) : (
                        ""
                      )}

                       {contactTestimonials.description ? (
                        <p className="testimonial_description">
                          <b>Description:</b> {contactTestimonials.description}
                        </p>
                      ) : (
                        ""
                      )}
                     <div className="d-flex">
                      <img className="rounded-circle profile_thumbnail_testimonal mb-0" width="50px" height="50px" src={transactionData?.contact3?.[0]?.data?.image}/>
                        {/* <p>
                       {transactionData?.contact3?.[0]?.data?.firstName}
                        </p> */}
                        <div class="ms-3"><strong>{transactionData?.contact3?.[0]?.data?.firstName}</strong> &nbsp;<strong>{transactionData?.contact3?.[0]?.data?.lastName}</strong>
                        <p class="mb-1">{transactionData?.represent==="buyer" ? "Buyer" : "Seller"}</p>
                      </div>
                     </div>
                    </div>
               
                </div>
              </>
            ) : (
              ""
            )}
          </div>
        </main>
      </div>
    </div>
  );
};

export default SinglePageTestimonials;

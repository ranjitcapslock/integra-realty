import React, { useState, useEffect } from "react";
import {NavLink , useParams} from "react-router-dom";
import Loader from "../../Pages/Loader/Loader.js";
import Toaster from "../../Pages/Toaster/Toaster.js";
import AgentAdded from "../../Components/img/Agentadded.png";
import user_service from "../service/user_service";

const PrivateThanks = () => {
  const params = useParams();
  const [loader, setLoader] = useState({ isActive: null });
  const { isActive } = loader;
  const [toaster, setToaster] = useState({
    type: null,
    isShow: null,
    toasterBody: "",
    message: "",
  });
  const { type, isShow, toasterBody, message } = toaster;
  const [getContact, setGetContact] = useState("");


  const ContactGetById = async () => {
    await user_service.contactGetById(params.id).then((response) => {
      if (response) {
        setGetContact(response.data);
      }
    });
  };

  useEffect(() => {
    window.scrollTo(0, 0);
    ContactGetById(params.id);
  }, []);

  return (
    <div className="bg-secondary float-left w-100">
          <Loader isActive={isActive} />
            {isShow && <Toaster type={type} isShow={isShow} toasterBody={toasterBody} message={message} />}
      <div className="container">
        <main className="page-wrapper homepage_layout">
          <section className="d-flex align-items-center position-relative min-vh-100  pb-5">
            <div className="container">
              <div className="row gy-4">
                <div className="col-md-4 align-self-sm-end ">
                  <div className="ratio ratio-1x1 mx-auto transaction_tips">
                    <img className="d-block mx-md-0 mx-auto mb-md-0 mb-4" src={AgentAdded}/>
                  </div>
                </div>
                <div className="col-lg-6 col-md-6 offset-lg-2 offset-md-1 text-md-start text-center">
                  <h3 className="display-3 mb-4 pb-md-3">
                    Contact Added
                  </h3>
                  {/* {console.log(getContact)} */}
                  <p className="text-white lead mb-md-5 mb-4">
                    Your {getContact?.contactType} Contact is added now!{" "}
                   You may add another contact by <NavLink to="/add-contact">clicking here</NavLink>.
                  
                  <br/>
                  <br/>
                  or you may..
                  
                  </p>
                 
                 {
                   getContact?.contactType ==="community" ?
                   <>
                    <NavLink to={`/contact-profile/${params.id}`}
                    type="button"
                    className="btn btn-primary btn-sm  pull-right mt-2 ms-4"
                 
                  >
                     Edit Contact Details
                  </NavLink>
                   </>
                  :
                  <NavLink to={`/private-contact/${params.id}`}
                    type="button"
                    className="btn btn-primary btn-sm  pull-right mt-2 ms-4"
                 
                  >
                    Edit Contact Details
                  </NavLink>
                 }

                  <NavLink to="/contact"
                    type="button"
                    className="btn btn-primary btn-sm  pull-right mt-2 ms-4"
                 
                  >
                    Go back to your Contacts
                  </NavLink>
                </div>
              </div>
            </div>
          </section>
        </main>
      </div>
    </div>
  );
};

export default PrivateThanks;

import React, { useState, useRef, useEffect } from "react";
// import avtar from "./Components/img/avtar.jpg";
import _ from "lodash";
import user_service from "../service/user_service";
import jwt from "jwt-decode";
import Loader from "../../Pages/Loader/Loader.js";
import Toaster from "../../Pages/Toaster/Toaster.js";
import { useNavigate, useParams, NavLink } from "react-router-dom";
import SignaturePad from "signature_pad";
import axios from "axios";

function StaffSignature() {
  const initialValues = {
    staff_signature: "",
  };
  const [formValues, setFormValues] = useState("");
  const [data, setData] = useState("");
  const [loader, setLoader] = useState({ isActive: null });
  const { isActive } = loader;

  const [toaster, setToaster] = useState({
    types: null,
    isShow: null,
    toasterBody: "",
    message: "",
  });
  const { types, isShow, toasterBody, message } = toaster;

  const params = useParams();
  const navigate = useNavigate();

  useEffect(() => { window.scrollTo(0, 0);
    const ContactGetById = async () => {
      try {
        const response = await user_service.contactGetById(params.id);
        setFormValues(response.data);
        // Set initial signature data if needed
        // setSavedSignature(response.data.staff_signature);
      } catch (error) {
        console.error("Error fetching contact data:", error);
      }
    };

    ContactGetById(params.id);
  }, [params.id]);

  const [signaturePad, setSignaturePad] = useState(null);
  const [savedSignature, setSavedSignature] = useState("");
  let signatureRedoArray = [];

  const readyPad = () => {
    let wrapper = document.getElementById("signature-pad");
    let canvas = wrapper?.querySelector("canvas");
    canvas.getContext("2d").scale(1, 1);
    let tempSignaturePad = new SignaturePad(canvas, {
      backgroundColor: "rgb(255, 255, 255)",
    });
    setSignaturePad(tempSignaturePad);
  };

  const handleSave = async (e) => {
    if (params.id) {
      if (signaturePad) {
        e.preventDefault();
        const dataURL = signaturePad.toDataURL();
        const signatureBlob = dataURLToBlob(dataURL);

        // Create a FormData object to send the Blob
        const formData = new FormData();
        formData.append("file", signatureBlob);

        const config = {
          headers: {
            Accept: "application/json",
            "Content-Type": "multipart/form-data",
            Authorization: `Bearer ${localStorage.getItem("auth")}`,
          },
        };

        try {
          const uploadResponse = await axios.post(
            "https://api.brokeragentbase.com/upload",
            formData,
            config
          );
          const uploadedFileData = uploadResponse.data;
          console.log(uploadedFileData);
          setSavedSignature(uploadedFileData);
          const userData = {
            staff_signature: uploadedFileData,
          };
          if (uploadedFileData) {
            setLoader({ isActive: true });
            await user_service
              .contactUpdate(params.id, userData)
              .then((response) => {
                if (response) {
                  console.log(response.data);
                  setLoader({ isActive: false });
                  setToaster({
                    type: "Update Signature",
                    isShow: true,
                    toasterBody: response.data.message,
                    message: "Update  Signature Successfully",
                  });
                  setTimeout(() => {
                    setToaster((prevToaster) => ({
                      ...prevToaster,
                      isShow: false,
                    }));
                    navigate(`/staff-signature/${params.id}/`);
                  }, 1000);
                  const ContactGet = async () => {
                    await user_service.contactGet().then((response) => {
                      if (response) {
                        console.log(response.data);
                      }
                    });
                  };
                  ContactGet();
                } else {
                  setLoader({ isActive: false });
                  setToaster({
                    types: "error",
                    isShow: true,
                    toasterBody: response.data.message,
                    message: "Error",
                  });
                }
              });
          }
        } catch (error) {
          setLoader({ isActive: false });
          setToaster({
            types: "error",
            isShow: true,
            toasterBody: error.response
              ? error.response.data.message
              : error.message,
            message: "Error",
          });
          setTimeout(() => {
            setToaster((prevToaster) => ({ ...prevToaster, isShow: false }));
          }, 2000);
        }
      }
    }
  };

  function dataURLToBlob(dataURL) {
    const parts = dataURL.split(";base64,");
    const contentType = parts[0].split(":")[1];
    const raw = window.atob(parts[1]);
    const rawLength = raw.length;
    const uInt8Array = new Uint8Array(rawLength);

    for (let i = 0; i < rawLength; ++i) {
      uInt8Array[i] = raw.charCodeAt(i);
    }

    return new Blob([uInt8Array], { type: contentType });
  }

  const handleUndo = () => {
    let signatureRemovedData = [];
    let signatureData = signaturePad.toData();
    let signatureRedoData = _.cloneDeep(signatureData); //original data

    if (signatureData.length > 0) {
      signatureData.pop(); // remove the last dot or line
      signaturePad.fromData(signatureData);
      signatureRemovedData = signatureRedoData[signatureRedoData.length - 1];
      signatureRedoArray.push(signatureRemovedData);
    }
  };

  //   const handleRedo = () => {
  //     if (signatureRedoArray.length !== 0) {
  //       let values = signaturePad.toData();
  //       let lastValue = signatureRedoArray[signatureRedoArray.length - 1];
  //       values.push(lastValue);
  //       signaturePad.fromData(values);
  //       signatureRedoArray.pop(lastValue); //remove the redo item from array
  //     }
  //   };

  const handleClear = () => {
    signaturePad.clear();
  };

  useEffect(() => { window.scrollTo(0, 0);
    readyPad();
  }, []);

  return (
    <div className="bg-secondary float-left w-100 pt-4 mb-4">
      <Loader isActive={isActive} />
      {isShow && (
        <Toaster
          types={types}
          isShow={isShow}
          toasterBody={toasterBody}
          message={message}
        />
      )}
      <main className="page-wrapper">
        <div className="row">
          <div className="col-md-8">
            <div className="float-left w-100 d-flex align-items-center justify-content-between mb-4">
              <h3 className="mb-0 text-white">Create Signature</h3>
            </div>
            <div className="bg-light border rounded-3 p-3">
              <div className="">
                <p className="">
                  Use a touch screen device or your mouse to draw in the box
                  below.
                </p>
                <div id="signature-pad">
                  <canvas className="signature-canvas"></canvas>
                </div>
              </div>
            </div>
            <div className="pull-right mt-3">
              <button
                className="btn btn-primary ms-2 px-3 px-sm-4"
                type="button"
                onClick={handleSave}
              >
                Save Signature
              </button>
              <button
                className="btn btn-secondary ms-3 px-3 px-sm-4"
                onClick={handleUndo}
              >
                <i className="fa fa-undo me-1" aria-hidden="true"></i> Undo
              </button>

              <button
                className="btn btn-secondary ms-3 px-3 px-sm-4"
                onClick={handleClear}
              >
                <i className="fa fa-eraser me-1" aria-hidden="true"></i> Clear
              </button>

              <NavLink to={`/staff-signature/${params.id}`}
                className="btn btn-secondary ms-3 px-3 px-sm-4"
                type="button"
              >
                Cancel & Go Back
              </NavLink>
            </div>
          </div>
          <div className="col-md-4">
           </div>
        </div>
      </main>
    </div>
  );
}
export default StaffSignature;

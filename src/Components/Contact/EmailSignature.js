import React, { useState, useRef, useEffect } from "react";
// import avtar from "./Components/img/avtar.jpg";
import _ from "lodash";
import user_service from "../service/user_service";
import jwt from "jwt-decode";
import Loader from "../../Pages/Loader/Loader.js";
import Toaster from "../../Pages/Toaster/Toaster.js";
import { useNavigate, useParams } from "react-router-dom";
import { Editor } from "react-draft-wysiwyg";
import { EditorState, ContentState, convertToRaw } from "draft-js";
import draftToHtml from "draftjs-to-html";
import htmlToDraft from "html-to-draftjs";
import "react-draft-wysiwyg/dist/react-draft-wysiwyg.css";

function EmailSignature() {
  const [notesmessage, setNotesmessage] = useState("");
  const editorRef = useRef(null);
  const log = () => {
    if (editorRef.current) {
      setNotesmessage(editorRef.current.getContent());
    }
  };

  const initialValues = {
    email_signature: "",
  };
  const [show, setIsShow] = useState(false);

  const [formValues, setFormValues] = useState(initialValues);
  const [detail, setDetail] = useState("");
  const [loader, setLoader] = useState({ isActive: null });
  const { isActive } = loader;
  const [toaster, setToaster] = useState({
    types: null,
    isShow: null,
    toasterBody: "",
    message: "",
  });
  const { types, isShow, toasterBody, message } = toaster;

  const params = useParams();
  const navigate = useNavigate();

  useEffect(() => {
    window.scrollTo(0, 0);
    const ContactGetById = async () => {
      try {
        const response = await user_service.contactGetById(params.id);
        console.log(response);
        setFormValues(response.data);
        const contentBlock = htmlToDraft(response.data.email_signature);
        if (contentBlock) {
          const contentState = ContentState.createFromBlockArray(
            contentBlock.contentBlocks
          );
          setEditorState(EditorState.createWithContent(contentState));
        }
      } catch (error) {
        console.error("Error fetching contact data:", error);
      }
    };

    ContactGetById(params.id);
  }, [params.id]);

  const [editorState, setEditorState] = useState(EditorState.createEmpty());

  const handleChanges = (editorState) => {
    setEditorState(editorState);
    const htmlContent = draftToHtml(
      convertToRaw(editorState.getCurrentContent())
    );
    setFormValues({ ...formValues, email_signature: htmlContent });
  };

  // const handleChanges = (e) => {
  //   setFormValues({ ...formValues, email_signature: e });
  // };
  // onChange Function end

  {
    /* <!--Api call Form onSubmit Start--> */
  }

  const handleSubmit = async (e) => {
    if (params.id) {
      e.preventDefault();
      setIsShow(true);
      const userData = {
        email_signature: formValues.email_signature,
      };
      console.log(userData);
      try {
        setLoader({ isActive: true });
        await user_service
          .contactUpdate(params.id, userData)
          .then((response) => {
            if (response) {
              console.log(response.data);
              setLoader({ isActive: false });
              setToaster({
                type: "Update Signature",
                isShow: true,
                toasterBody: response.data.message,
                message: "Update Email Signature Successfully",
              });
              setTimeout(() => {
                setToaster((prevToaster) => ({
                  ...prevToaster,
                  isShow: false,
                }));
                navigate(`/contact-profile/${params.id}/`);
              }, 1000);
              setIsShow(false);
            } else {
              setLoader({ isActive: false });
              setToaster({
                types: "error",
                isShow: true,
                toasterBody: response.data.message,
                message: "Error",
              });
            }
          });
      } catch (error) {
        setLoader({ isActive: false });
        setToaster({
          types: "error",
          isShow: true,
          toasterBody: error.response
            ? error.response.data.message
            : error.message,
          message: "Error",
        });
        setTimeout(() => {
          setToaster((prevToaster) => ({ ...prevToaster, isShow: false }));
        }, 2000);
      }
    }
  };

  const handleRemove = async (e) => {
    if (params.id) {
      e.preventDefault();
      const userData = {
        email_signature: "",
      };
      console.log(userData);
      try {
        setLoader({ isActive: true });
        await user_service
          .contactUpdate(params.id, userData)
          .then((response) => {
            if (response) {
              setLoader({ isActive: false });
              setToaster({
                type: "Update Signature",
                isShow: true,
                toasterBody: response.data.message,
                message: "Update Email Signature Successfully",
              });
              setTimeout(() => {
                setToaster((prevToaster) => ({
                  ...prevToaster,
                  isShow: false,
                }));
              }, 1000);
              const ContactGet = async () => {
                await user_service.contactGet().then((response) => {
                  if (response) {
                    console.log(response.data);
                  }
                });
              };
              ContactGet();
            } else {
              setLoader({ isActive: false });
              setToaster({
                types: "error",
                isShow: true,
                toasterBody: response.data.message,
                message: "Error",
              });
            }
          });
      } catch (error) {
        setLoader({ isActive: false });
        setToaster({
          types: "error",
          isShow: true,
          toasterBody: error.response
            ? error.response.data.message
            : error.message,
          message: "Error",
        });
        setTimeout(() => {
          setToaster((prevToaster) => ({ ...prevToaster, isShow: false }));
        }, 2000);
      }
    }
  };

  return (
    <div className="bg-secondary float-left w-100 pt-4 mb-4">
      <Loader isActive={isActive} />
      {isShow && (
        <Toaster
          types={types}
          isShow={isShow}
          toasterBody={toasterBody}
          message={message}
        />
      )}
      <main className="page-wrapper">
        <div className="">
          <div className="">
            <div className="float-left w-100 d-flex align-items-center justify-content-between mb-4">
              <h3 className="pull-left mb-0 text-white">
                Customized Email Signature
              </h3>
            </div>
            <div className="row">
              <div className="col-md-8">
                <div className="bg-light border rounded-3 p-3 mb-3">
                  <div className="col-sm-12">
                    <p>
                      A signature is automatically added to all emails sent from
                      this system by your account. You can adjust the appearance
                      of this signature at anytime.
                    </p>
                    <div className="d-flex">
                      <p className="form-label">Email Signature for</p>&nbsp;
                      <h6>
                        {formValues?.firstName} {formValues?.lastName}
                      </h6>
                    </div>

                    <Editor
                      editorState={editorState}
                      onEditorStateChange={handleChanges}
                      value={formValues.email_signature}
                      toolbar={{
                        options: [
                          "inline",
                          "blockType",
                          "fontSize",
                          "list",
                          "textAlign",
                          "history",
                          "link", // Add link option here
                        ],
                        inline: {
                          options: [
                            "bold",
                            "italic",
                            "underline",
                            "strikethrough",
                          ],
                        },
                        list: { options: ["unordered", "ordered"] },
                        textAlign: {
                          options: ["left", "center", "right"],
                        },
                        history: { options: ["undo", "redo"] },
                        link: {
                          // Configure link options
                          options: ["link", "unlink"],
                        },
                      }}
                      wrapperClassName="demo-wrapper"
                      editorClassName="demo-editor"
                    />
                  </div>
                </div>
                <div className="pull-right mt-3">
                  <button
                    className="btn btn-primary"
                    type="button"
                    onClick={handleSubmit}
                  >
                    {show ? "Please wait" : "Update Signature"}
                  </button>
                </div>
              </div>
              <div className="col-md-4 mt-lg-0 mt-md-0 mt-sm-4 mt-4">
                <div className="bg-light border rounded-3 p-3 mb-3">
                  <h6>System-generated Signature Assigned</h6>
                  <p>
                    A custom signature is not currently assigned, display name
                    and contact info will dynamically update if they are
                    changed.
                  </p>
                  <h6>Need to start over?</h6>
                  <p>
                    If you would like to start over, you can select from the
                    following system-created signatures to help you get started.
                  </p>
                  <button
                    className="btn btn-primary px-3 px-sm-4"
                    type="button"
                    onClick={handleRemove}
                  >
                    Reset to Basic Signature
                  </button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </main>
    </div>
  );
}
export default EmailSignature;

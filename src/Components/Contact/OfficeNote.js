import React, { useState, useEffect } from "react";
import _ from 'lodash';
import user_service from "../service/user_service";
import jwt from "jwt-decode";
import Loader from "../../Pages/Loader/Loader.js";
import Toaster from "../../Pages/Toaster/Toaster.js";
import { NavLink, useParams, useNavigate } from 'react-router-dom';


function OfficeNote() {
    const navigate = useNavigate();

    const params = useParams();

    const [loader, setLoader] = useState({ isActive: null })
    const { isActive } = loader;
    const [toaster, setToaster] = useState({ types: null, isShow: null, toasterBody: "", message: "" });
    const { types, isShow, toasterBody, message } = toaster;

    const initialValues = {
        category: "", private_notes: "", message: "", addedBy:"", agentId:""
    }
    const [formValues, setFormValues] = useState(initialValues);
    const [formErrors, setFormErrors] = useState({});
    const [isSubmitClick, setISSubmitClick] = useState(false)
    const [requiredCheck, setRequiredCheck] = useState("Yes");

    const checkRequired = (e) => {
        setRequiredCheck(e.target.name)
        setRequiredCheck(e.target.value)
    }


    const handleChange = (e) => {
        const { name, value } = e.target;
        setFormValues({ ...formValues, [name]: value })
    };

    {/* <!-- Form Validation Start--> */ }
    useEffect(() => { 
        if (formValues && isSubmitClick) {
            validate()
        }
    }, [formValues])

    const validate = () => {
        const values = formValues
        const errors = {};
        if (!values.message) {
            errors.message = "message is required";
        }
        if (!values.category) {
            errors.category = "category is required";
        }

        setFormErrors(errors)
        return errors;
    }
    {/* <!-- Form Validation End--> */ }

    const handleSubmit = async (e) => {

        if(params.officeId){
            console.log(params.officeId);
            e.preventDefault();
            setISSubmitClick(true)
            let checkValue = validate()
            if (_.isEmpty(checkValue)) {
                const userData = {
                    private_notes: requiredCheck,
                    category: formValues.category ? formValues.category : "Accounting" ,
                    message: formValues.message,
                    addedBy: jwt(localStorage.getItem("auth")).id,
                    agentId:params.id
                };
                console.log(userData)
                setLoader({ isActive: true })
                const response = await user_service.officeNoteUpdate(params.officeId, userData);
                if (response) {
                    console.log(response)
                    setLoader({ isActive: false })
                    setToaster({
                        type: "OfficeNote Successfully",
                        isShow: true,
                        toasterBody: response.data.message,
                        message: "OfficeNote Successfully",
                    });
                    setTimeout(() => {
                        setToaster((prevToaster) => ({ ...prevToaster, isShow: false }))
                        navigate(`/contact-profile/${params.id}`);
                    }, 500);
                }
            } 
        }

       else{

            e.preventDefault();
            setISSubmitClick(true)
            let checkValue = validate()
            if (_.isEmpty(checkValue)) {
                const userData = {
                    private_notes: requiredCheck,
                    category: formValues.category ?formValues.category : "Accounting" ,
                    message: formValues.message,
                    addedBy: jwt(localStorage.getItem("auth")).id,
                    agentId:params.id
                };
                console.log(userData)
                setLoader({ isActive: true })
                const response = await user_service.officeNote(userData);
                if (response) {
                    console.log(response)
                    setLoader({ isActive: false })
                    setToaster({
                        type: "OfficeNote Successfully",
                        isShow: true,
                        toasterBody: response.data.message,
                        message: "OfficeNote Successfully",
                    });
                    setTimeout(() => {
                        setToaster((prevToaster) => ({ ...prevToaster, isShow: false }))
                        navigate(`/contact-profile/${params.id}`);
                    }, 500);
                }
            }
        }

    


    }


    useEffect(() => { window.scrollTo(0, 0);
        if (params.officeId) {
            OfficeNoteId()
        }
    }, [])


  

    const OfficeNoteId = async () => {
        await user_service.officeNoteGetId(params.officeId).then((response) => {
            if (response) {
                setFormValues(response.data)

            }
        });
    }

    return (
        <div className="bg-secondary float-left w-100 pt-4">
            <Loader isActive={isActive} />
            {isShow && <Toaster types={types} isShow={isShow} toasterBody={toasterBody} message={message} />}
            <div className="container">

                <div className="justify-content-center pb-sm-2">
                   
                    <div className="bg-light rounded-3 p-4 mb-3">
                        <div className="add_listing w-100 m-auto p-4">
                            <h4>Add Account Note</h4>

                            <div className="row mt-3">

                                <div className="col-md-8 pull-right  mt-3">
                                    <h6>Category</h6>
                                    <select className="form-select" id="pr-city"
                                        name="category"
                                        onChange={handleChange}
                                        value={formValues.category}
                                    >
                                        <option>Accounting</option>
                                        <option>Broker of Record</option>
                                        <option>Executive Management</option>
                                        <option>Franchising</option>
                                        <option>Human Resources</option>
                                        <option>IT Support</option>
                                        <option>Office Management</option>
                                        <option>REALTOR</option>
                                        <option>Training</option>
                                    </select>
                                    {formErrors.category && (
                                        <div className="invalid-tooltip">{formErrors.category}</div>
                                    )}
                                </div><br />

                                <div className="col-md-8 mt-3" onClick={(e) => checkRequired(e)}>
                                    <h6>Private Note</h6>
                                    <div className="form-check">
                                        <input className="form-check-input" id="form-check-1" type="radio"
                                            name="required"
                                            value="Yes"
                                            checked
                                        />
                                        <label className="form-check-label" id="radio-level1">Yes, staff view only.</label>
                                    </div>

                                    <div className="form-check">
                                        <input className="form-check-input" id="form-check-2" type="radio"
                                            name="required"
                                            value="No" />
                                        <label className="form-check-label" id="radio-level1">No, show to agent.</label>
                                    </div>
                                </div>


                                <div className="mb-3 mt-3">
                                    <h6>Message</h6>
                                    <textarea className="form-control mt-0"
                                        id="textarea-input"
                                        rows="5"
                                        name="message"
                                        onChange={handleChange}
                                        autoComplete="on"
                                        value={formValues.message}
                                        style={{
                                            border: formErrors?.message ? '1px solid red' : '1px solid #00000026'
                                        }}
                                    >Hello World!</textarea>
                                     {formErrors.message && (
                                        <div className="invalid-tooltip">{formErrors.message}</div>
                                    )}
                                </div>
                            </div>
                            <button to="/add-paperwork" className="btn btn-primary pull-right ms-5" type="button" onClick={handleSubmit}>{params.officeId ? "Update" : "Save New Note"}</button>
                            <NavLink to={`/contact-profile/${params.id}`} className="btn btn-secondary pull-right ms-5" type="button">Cancel & Go Back </NavLink>

                        </div>


                    </div>
                </div>
            </div>
        </div>
    )
}
export default OfficeNote;
import React, { useState, useEffect } from "react";
import user_service from "../../Components/service/user_service";
import jwt from "jwt-decode";
import { useNavigate, useParams,NavLink } from "react-router-dom";
import Loader from "../../Pages/Loader/Loader.js";
import Toaster from "../../Pages/Toaster/Toaster.js";
import AgentAdded from "../../Components/img/Agentadded.png";

const VendorThanks = () => {
  const [clientId, setClientId] = useState("");
  const params = useParams();
  const navigate = useNavigate();


  const initialValues = {
    firstName: "",
    lastName: "",
    company: "",
    email: "",
    phone: "",
    agentId: "",
  };
  const [formValues, setFormValues] = useState(initialValues);

  const [loader, setLoader] = useState({ isActive: null });
  const { isActive } = loader;
  const [toaster, setToaster] = useState({ type: null, isShow: null, toasterBody: "", message: "" });
  const { type, isShow, toasterBody, message } = toaster;

  const [summary, setSummary] = useState("");

  const handlesetuppassword = async (e) => {
    e.preventDefault();
  
    const userData = {
      _id: params.id,
      agentId: jwt(localStorage.getItem("auth")).id,
    };
    
    console.log("userData", userData);
    
    try {
      setLoader({ isActive: true });
      const response = await user_service.onboardlinkVendor(userData);
      
      if (response && response.data) {
        setClientId(response.data._id);
        setSummary(response.data);
        setLoader({ isActive: false });
        setFormValues(initialValues);
        
        setToaster({
          type: "Onboarding Link Sent Successfully",
          isShow: true,
        //   toasterBody: response.data.message || "Onboarding Link Sent Successfully", // Ensure it's a string
          message: "Onboarding Link Sent Successfully",
        });
  
        setTimeout(() => {
          setToaster((prevToaster) => ({ ...prevToaster, isShow: false }));
          navigate(`/contact`);
        }, 2000);
      } else {
        setLoader({ isActive: false });
        setToaster({
          types: "error",
          isShow: true,
          message: "Error",
        });
      }
    } catch (error) {
      setLoader({ isActive: false });
      setToaster({
        types: "error",
        isShow: true,
        message: "Error",
      });
    }
  
    setTimeout(() => {
      setToaster({
        types: "error",
        isShow: false,
        toasterBody: null,
        message: "Error",
      });
    }, 2000);
  };
  
  return (
    <div className="bg-secondary float-left w-100">
          <Loader isActive={isActive} />
            {isShow && <Toaster type={type} isShow={isShow} toasterBody={toasterBody} message={message} />}
      <div className="container">
        <main className="page-wrapper homepage_layout">
          <section className="d-flex align-items-center position-relative min-vh-100  pb-5">
            <div className="container">
              <div className="row gy-4">
                <div className="col-md-4 align-self-sm-end ">
                  <div className="ratio ratio-1x1 mx-auto transaction_tips">
                    <img className="d-block mx-md-0 mx-auto mb-md-0 mb-4" src={AgentAdded}/>
                  </div>
                </div>
                <div className="col-lg-6 col-md-6 offset-lg-2 offset-md-1 text-md-start text-center">
                  <h3 className="display-3 mb-4 pb-md-3 text-white">
                    Vendor Added
                  </h3>
                  <p className="lead mb-md-5 mb-4 text-white">
                    Vendor has been added, you can send them an onboarding email now!{" "}
                    <NavLink className="text-white" to="/">homepage</NavLink>.
                  </p>

                  <NavLink to={`/contact-profile/${params.id}`}
                    type="button"
                    className="btn btn-primary btn-sm  pull-right mt-2 ms-4"
                 
                  >
                    Edit Contact Details
                  </NavLink>
                  <button
                    type="button"
                    className="btn btn-primary btn-sm  pull-right mt-2"
                    onClick={handlesetuppassword}
                  >
                    Send Onboarding Request
                  </button>
                </div>
              </div>
            </div>
          </section>
        </main>
      </div>
    </div>
  );
};

export default VendorThanks;

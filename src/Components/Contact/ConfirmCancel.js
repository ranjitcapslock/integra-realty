import React from "react";

const ConfirmCancel = ({
  onCancel,
  onConfirm,
  confirmTitle = "Confirm",
  leftBlock,
  hideCancel,
  disabled,
}) => {
  return (
    <div>
      <div>{leftBlock}</div>
      <div>
        {!hideCancel ? (
          <button
            title={"Cancel"}
            // style={styles.cancel}
            onClick={onCancel}
          />
        ) : null}
        <button
          title={confirmTitle}
          inverted={true}
          onClick={onConfirm}
          disabled={disabled}
        />
      </div>
    </div>
  );
};

export default ConfirmCancel;

import React, { useState, useEffect } from 'react';
import { NavLink, useNavigate } from 'react-router-dom';
import { useParams } from 'react-router-dom';
import Loader from '../../Pages/Loader/Loader.js';
import Toaster from '../../Pages/Toaster/Toaster.js';
import AllTab from '../../Pages/AllTab';
import user_service from '../service/user_service';
import jwt from 'jwt-decode';
import axios from "axios";

const MlsPropertySet = () => {
  const navigate = useNavigate();

  const [loader, setLoader] = useState({ isActive: null });
  const { isActive } = loader;
  const [toaster, setToaster] = useState({
    types: null,
    isShow: null,
    toasterBody: '',
    message: '',
  });
  const { types, isShow, toasterBody, message } = toaster;
  const [transactionId, setTransactionId] = useState('');
  const [array, setArray] = useState('');
  const params = useParams();



  


  useEffect(() => { window.scrollTo(0, 0);
    setLoader({ isActive: true });

    const TransactionGetById = async () => {
      try {
        const response = await user_service.transactionGetById(params.id);
        setLoader({ isActive: false });

        if (response) {
          //console.log(response);
          setTransactionId(response.data);
        }
      } catch (error) {
        console.error(error);
        setLoader({ isActive: false });
      }
    };

    TransactionGetById(params.id);
  }, []);

  useEffect(() => { window.scrollTo(0, 0);
    setLoader({ isActive: true });

    const ListingById = async () => {
      try {
        const response = await user_service.listingsGetById(params.pid);
        setLoader({ isActive: false });

        if (response) {
          setArray(response.data);
        }
      } catch (error) {
        console.error(error);
        setLoader({ isActive: false });
      }
    };

    ListingById(params.pid);
  }, []);
 


  

  const handleSubmit = async () => {
  //   const contacts = transactionId?.contact1.map((post) => ({
  //     type: post.data.type,
  //     contactId: post.data._id
  // }));

    try {
      const userData = {
        // contact1: contacts,
        propertyDetail: params.pid,
        // _id: transactionId._id,
      }
       setLoader({ isActive: true })
       await user_service.TransactionUpdate(params.id,userData).then((response) => {

      if (response.status === 200) {
        setArray(response.data);
        setLoader({ isActive: false });
        setToaster({
          types: "Transaction_Updated",
          isShow: true,
          toasterBody: response.data.message,
          message: "Transaction_Updated Successfully",
        });
        navigate(`/t-property/${params.id}`);
      } else {
        setLoader({ isActive: false });
        setToaster({
          types: "error",
          isShow: true,
          toasterBody: response.data.message,
          message: "Error",
        });
      }
    })
    } catch (error) {
      setLoader({ isActive: false });
      setToaster({
        types: "error",
        isShow: true,
        toasterBody: error.response ? error.response.data.message : error.message,
        message: "Error",
      });
    }
  };


 

  return (
    <div className="bg-secondary float-left w-100 pt-4">
      <Loader isActive={isActive} />
      {isShow && (
        <Toaster
          types={types}
          isShow={isShow}
          toasterBody={toasterBody}
          message={message}
        />
      )}
      <main className="page-wrapper">
        <AllTab transaction_data = {transactionId}/>
        <div className="container mb-md-4 ">
          <div className="bg-light shadow-sm rounded-3 p-2 p-md-5 mb-2">
            <div className="row">
              <div className="bg-light rounded-3 p-4 p-md-5 mb-0">
                <a
                  className="btn btn-primary btn-sm  ms-5 mt-3 order-lg-3"
                  href="#"
                  onClick={handleSubmit}
                >
                  Set Property Now
                </a>
              </div>
            </div>
          </div>
        </div>
      </main>
    </div>
  );
};

export default MlsPropertySet;

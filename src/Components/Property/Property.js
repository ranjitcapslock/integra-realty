import React, { useState, useEffect } from "react";
import { NavLink, useNavigate, useParams } from "react-router-dom";
import user_service from "../service/user_service";
import Loader from "../../Pages/Loader/Loader.js";
import Toaster from "../../Pages/Toaster/Toaster";
import AllTab from "../../Pages/AllTab";
import jwt from "jwt-decode";
import axios from "axios";
import Pic from "../img/pic.png";
import moment from "moment";

import defaultpropertyimage from "../../Components/img/defaultpropertyimage.jpeg";
// import StatusBar from "../../Pages/StatusBar.js";

const Property = () => {
  const initialValues = {
    details_questionid: "",
    answer: "",
    agentId: "",
    transactionId: "",
  };

  const [propertyUpdate, setProprtyUpdate] = useState(initialValues);
  const [isEditing, setIsEditing] = useState(false);
  const [loader, setLoader] = useState({ isActive: null });
  const { isActive } = loader;
  const [toaster, setToaster] = useState({
    types: null,
    isShow: null,
    toasterBody: "",
    message: "",
  });
  const { types, isShow, toasterBody, message } = toaster;

  const [transactionId, setTransactionId] = useState("");

  const params = useParams();
  const navigate = useNavigate();
  const [data, setData] = useState("");
  const TransactionGetById = async () => {
    setLoader({ isActive: true });
    await user_service.transactionGetById(params.id).then((response) => {
      setLoader({ isActive: false });
      if (response) {
        console.log(response.data);
        setTransactionId(response.data);
        // handleSubmit(response.data)
      }
    });
  };

  const [detailsaddeddata, setDetailsaddeddata] = useState([]);
  const [requireddetailsaddeddata, setRequiredDetailsaddeddata] = useState([]);
  const [detailsaData, setDetailsaData] = useState("");
  const [organizationGet, setOrganizationGet] = useState([]);


  const Detailsaddeddata = async () => {
    const agentId = jwt(localStorage.getItem("auth")).id;
    await user_service.Detailsaddeddata(agentId, params.id).then((response) => {
      setLoader({ isActive: false });
      if (response) {
        const requiredYesArray = response.data.data.filter(
          (item) => item.required === "yes"
        );
        const groupedBycategory = response.data.data.reduce((acc, item) => {
          const category = item.category;
          if (!acc[category]) {
            acc[category] = [];
          }
          acc[category].push(item);
          return acc;
        }, {});

        setRequiredDetailsaddeddata(requiredYesArray);
        setDetailsaddeddata(groupedBycategory);
      }
    });
  };
  const [submitCount, setSubmitCount] = useState(0);

  useEffect(() => {
    setLoader({ isActive: true });
    TransactionGetById(params.id);
    Detailsaddeddata();
    fetchCalendar();
    Getrequireddoucmentno();
  }, [submitCount]);

  const setProperty = () => {
    navigate(`/select-property/${params.id}`);
  };

  const editInput = (e, id) => {
    const element = document.getElementById("propdetails" + id);
    if (element) {
      if (isEditing === true) {
        setIsEditing(false);
        element.classList.add("d-none");
        document.getElementById("editanswer" + id).innerHTML = "Edit";
      } else if (isEditing === false) {
        element.classList.remove("d-none");
        document.getElementById("editanswer" + id).innerHTML = "Cancel";
        setIsEditing(true);
      }
    }
  };

  const editInputProp = (e, id) => {
    if (isEditing === true) {
      setIsEditing(false);
      document.getElementById("propertydetails" + id).classList.add("d-none");
      document.getElementById("editanswerprop" + id).innerHTML = "Edit";
    } else if (isEditing === false) {
      document
        .getElementById("propertydetails" + id)
        .classList.remove("d-none");
      document.getElementById("editanswerprop" + id).innerHTML = "Cancel";
      setIsEditing(true);
    }
  };

  const handleChange = (e) => {
    const { name, value } = e.target;
    setProprtyUpdate({ ...propertyUpdate, [name]: value });
  };

  const save = async (id, answer, option) => {
    if (answer != "") {
      const updatedProprty = {
        agentId: jwt(localStorage.getItem("auth")).id,
        transactionId: transactionId._id,
        details_questionid: id,
        answer: option,
      };
      setLoader({ isActive: true });
      await user_service
        .transactionPropertyUpdate(answer._id, updatedProprty)
        .then((response) => {
          if (response) {
            setLoader({ isActive: false });
            setData(response.data);
            editInput("", id);
            editInputProp("", id);
            setLoader({ isActive: false });
            setSubmitCount(submitCount + 1);
          }
        });
    } else {
      const updatedProprty = {
        agentId: jwt(localStorage.getItem("auth")).id,
        transactionId: transactionId._id,
        details_questionid: id,
        answer: option,
      };

      setLoader({ isActive: true });
      const response = await user_service.transactionPropertyDetails(
        updatedProprty
      );
      if (response) {
        setLoader({ isActive: false });

        setData(response.data);
        editInput("", id);
        editInputProp("", id);
        setLoader({ isActive: false });
        setSubmitCount(submitCount + 1);
      }
    }
  };

  const saveDate = async (id, answer) => {
    if (answer != "") {
      const updatedProprty = {
        agentId: jwt(localStorage.getItem("auth")).id,
        transactionId: transactionId._id,
        details_questionid: id,
        answer: propertyUpdate.answer,
      };
      setLoader({ isActive: true });
      await user_service
        .transactionPropertyUpdate(answer._id, updatedProprty)
        .then((response) => {
          if (response) {
            setLoader({ isActive: false });
            setData(response.data);
            editInput("", id);
            editInputProp("", id);
            setLoader({ isActive: false });
            setSubmitCount(submitCount + 1);
          }
        });
    } else {
      const updatedProprty = {
        agentId: jwt(localStorage.getItem("auth")).id,
        transactionId: transactionId._id,
        details_questionid: id,
        answer: propertyUpdate.answer,
      };
      setLoader({ isActive: true });
      const response = await user_service.transactionPropertyDetails(
        updatedProprty
      );
      if (response) {
        setLoader({ isActive: false });
        setData(response.data);
        editInput("", id);
        editInputProp("", id);
        setLoader({ isActive: false });
        setSubmitCount(submitCount + 1);
      }
    }
  };

  const handleClear = async (id, answer) => {
    if (answer && answer._id) {
      setLoader({ isActive: true });
      await user_service
        .transactionPropertyDelete(answer._id)
        .then((response) => {
          if (response) {
            setLoader({ isActive: false });
            setSubmitCount(submitCount + 1);
          }
        });
    }
  };

  const fetchCalendar = async () => {
    setLoader({ isActive: true });
    await user_service.CalenderGet().then((response) => {
      setLoader({ isActive: false });
      if (response) {
      }
    });
  };

  
  
  const handleaddevent = async (transaction, id) => {
    console.log(id);
    if (id) {
      try {
        const response = await user_service.DetailsdatagetId(id);
        setLoader({ isActive: false });
  
        if (response) {
          const detailsData = response.data;
            if (detailsData.answer_type === "Date" && transaction.contact1) {
              let title = "";
  
              if (detailsData.question === "When is the financing and appraisal deadline?") {
                title = "financing deadline";
              } else if (detailsData.question === "When did your listing agreement take effect?") {
                title = "listing agreement effect";
              } else if (detailsData.question === "By what date does your client generally expect to be finished?") {
                title = "expect finished";
              } else if (detailsData.question === "When does your listing agreement expire?") {
                title = "listing agreement expire";
              } else if (detailsData.question === "What is the settlement deadline?") {
                title = "settlement deadline";
              } else if (detailsData.question === "When was the contract accepted?") {
                title = "contract accepted";
              }
  
              const userData = {
                office_id: localStorage.getItem("active_office_id"),
                office_name: localStorage.getItem("active_office"),
                category: "Transaction Event",
                publicationLevel: "N/A",
                allDayEvent: "yes",
                title: title,
                information: "N/A",
                registration: "N/A",
                venue:
                  transaction.contact1[0].data.firstName +
                  " " +
                  transaction.contact1[0].data.lastName,
                organizer: "N/A",
                draft: "N/A",
                agentId: jwt(localStorage.getItem("auth")).id,
                startDate: propertyUpdate.answer,
                endDate: propertyUpdate.answer,
                guestSeats: "N/A",
                publicEvent: "N/A",
                repeatingEvent: "N/A",
                anotherEvent: "N/A",
                eventDescription: "N/A",
                startTime: "9:00 AM",
                endTime: "10:00 AM",
                maxSeats: "N/A",
                seatCount: "N/A",
                question: "N/A",
                transactionId: transaction._id,
              };
              console.log(userData);
              setLoader({ isActive: true });
              const calendarResponse = await user_service.CalenderPost(userData);
              setLoader({ isActive: false });
  
              if (calendarResponse) {
                console.log(calendarResponse);
                setLoader({ isActive: false });
              }
            }
          }
      } catch (error) {
        console.error("Error fetching details:", error);
      }
    }
  };
  
  

  const propertypic = (e) => {
    e.target.src = defaultpropertyimage;
  };

  const [viewmore, setViewmore] = useState(false);

  const Viewmorefunction = (e) => {
    setViewmore(!viewmore);
  };

  const [getrequireddoucmentno, setGetrequireddoucmentno] = useState([]);

  const Getrequireddoucmentno = async () => {
    const agentId = jwt(localStorage.getItem("auth")).id;
    await user_service
      .Getrequireddoucmentno(agentId, params.id)
      .then((response) => {
        // setLoader({ isActive: false })
        if (response) {
          //  console.log(response.data)
          setGetrequireddoucmentno(response.data);
        }
      });
  };


  const handleChangeInput = (e) => {
    const { name, value } = e.target;
    const numericValue = value.replace(/[^\d.]/g, "");

    if (numericValue !== "") {
      const floatValue = parseFloat(numericValue);

      if (!isNaN(floatValue)) {
        setProprtyUpdate((prevValues) => ({
          ...prevValues,
          [name]: floatValue.toLocaleString("en-US", {
            style: "currency",
            currency: "USD",
            minimumFractionDigits: 0,
          }),
        }));
      }
    } else {
      setProprtyUpdate((prevValues) => ({
        ...prevValues,
        [name]: "",
      }));
    }
  };

  return (
    <div className="bg-secondary float-left w-100 pt-4">
      <Loader isActive={isActive} />
      {isShow && (
        <Toaster
          types={types}
          isShow={isShow}
          toasterBody={toasterBody}
          message={message}
        />
      )}
      <main className="page-wrapper">
        {/* <!-- Page container--> */}
        <div className="content-overlay mt-0">
          <div className="row">
            <AllTab />

            <div className="col-md-10">
              <div className="bg-light border rounded-3  mb-4 p-3">
                <div className="row">
                  <div className="col-md-9">
                    <h3 id="getContact" className="pull-left mb-2">
                      Transaction Status
                    </h3>
                    <p className="float-start w-100 mb-0">
                      Here you can see your transaction details and can edit them
                      easily.
                    </p>
                   </div>
                  <div className="col-md-3 justify-content-end d-flex align-items-center">
                    <span className="pull-right">
                      <a
                        aria-current="page"
                        className="btn btn-secondary btn-sm"
                        onClick={setProperty}
                      >
                        <i className="fi-check text-success me-2"></i>
                        <span className="text-success">
                          {transactionId.propertyDetail
                            ? "Edit Property"
                            : "Set Property"}
                        </span>
                      </a>
                    </span>
                  </div>
                </div>
              </div>
              {/* <StatusBar/> */}

              <div className="bg-light p-3 border-0 rounded-3 shadow-sm float-start w-100">
                <div className="">
                  <div className="transaction_property">
                    <div className="">
                      <div className="accordion mt-2" id="accordionExample1">
                        <div className="accordion-item mb-3">
                          <h2 className="accordion-header" id="headingOne">
                            <button
                              className="accordion-button"
                              type="button"
                              data-bs-toggle="collapse"
                              data-bs-target="#collapseOne"
                              aria-expanded="true"
                              aria-controls="collapseOne"
                            >
                              Property Listing Information
                            </button>
                          </h2>
                          <div
                            className="accordion-collapse collapse show"
                            aria-labelledby="headingOne"
                            data-bs-parent="#accordionExample"
                            id="collapseOne"
                          >
                            <div className="">
                              {/* <!-- Item--> */}
                              <div className="card card-hover card-horizontal border-0 shadow-sm p-0">
                                {transactionId.propertyDetail ? (
                                  <>
                                    {/* <a className="card-img-top">
                                      <img
                                        src={
                                          transactionId.propertyDetail.image ??
                                          defaultpropertyimage
                                        }
                                        onError={(e) => propertypic(e)}
                                      />
                                    </a> */}
                                    <div className="card-body position-relative pt-0">
                                      {transactionId.propertyDetail
                                        .propertyFrom &&
                                      transactionId.propertyDetail
                                        .propertyFrom === "mls" ? (
                                        <>
                                          {viewmore ? (
                                            <>
                                              <p>
                                                <strong>
                                                  {
                                                    transactionId.propertyDetail
                                                      .streetAddress
                                                  }
                                                  ,{" "}
                                                  {
                                                    transactionId.propertyDetail
                                                      .city
                                                  }
                                                  ,
                                                  {
                                                    transactionId.propertyDetail
                                                      .state
                                                  }
                                                </strong>
                                              </p>
                                              <p className="mb-2 fs-sm text-muted">
                                                {" "}
                                                MLS Source:{" "}
                                                {
                                                  JSON.parse(
                                                    transactionId.propertyDetail
                                                      .additionaldataMLS
                                                  ).value[0].SourceSystemName
                                                }
                                              </p>
                                              <p className="mb-2 fs-sm text-muted">
                                                {" "}
                                                MLS Number:{" "}
                                                {
                                                  transactionId.propertyDetail
                                                    .mlsNumber
                                                }
                                              </p>
                                              <p className="mb-2 fs-sm text-muted">
                                                {" "}
                                                Listed By:{" "}
                                                <strong>
                                                  {
                                                    transactionId.propertyDetail
                                                      .listingsAgent
                                                  }
                                                  <br />
                                                  {
                                                    JSON.parse(
                                                      transactionId
                                                        .propertyDetail
                                                        .additionaldataMLS
                                                    ).value[0].ListOfficeName
                                                  }
                                                </strong>
                                              </p>
                                              <p className="mb-2 fs-sm text-muted">
                                              List Price: 
                                            {
                                              transactionId?.propertyDetail.price?? "$0"
                                             }
                                              </p>
                                              <p className="mb-2 fs-sm text-muted">
                                                Category:{" "}
                                                {
                                                  transactionId.propertyDetail
                                                    .propertyType
                                                }
                                              </p>
                                              <p className="mb-2 fs-sm text-muted">
                                                Subdivision: $
                                                {
                                                  transactionId.propertyDetail
                                                    .subDivision
                                                }
                                              </p>
                                              <p className="mb-2 fs-sm text-muted">
                                                Beds:{" "}
                                                {
                                                  transactionId.propertyDetail
                                                    .bed
                                                }
                                              </p>
                                              <p className="mb-2 fs-sm text-muted">
                                                Baths:{" "}
                                                {JSON.parse(
                                                  transactionId.propertyDetail
                                                    .additionaldataMLS
                                                ).value[0].BathroomsFull ??
                                                  0}{" "}
                                                /{" "}
                                                {JSON.parse(
                                                  transactionId.propertyDetail
                                                    .additionaldataMLS
                                                ).value[0].BathroomsHalf ??
                                                  0}{" "}
                                              </p>
                                              <p className="mb-2 fs-sm text-muted">
                                              Building Area(Square Feet):{" "}
                                                {JSON.parse(
                                                  transactionId.propertyDetail
                                                    .additionaldataMLS
                                                ).value[0].BuildingAreaTotal ??
                                                  0}
                                              </p>
                                              <p className="mb-2 fs-sm text-muted">
                                                Year Built:{" "}
                                                {JSON.parse(
                                                  transactionId.propertyDetail
                                                    .additionaldataMLS
                                                ).value[0].YearBuilt ?? 0}
                                              </p>
                                              <p className="mb-2 fs-sm text-muted">
                                                <strong>
                                                  <NavLink
                                                    onClick={Viewmorefunction}
                                                  >
                                                    View Less
                                                  </NavLink>
                                                </strong>
                                              </p>
                                              <NavLink
                                                to={`/single-listing/${transactionId.propertyDetail._id}`}
                                                className="btn btn-primary btn-sm  pull-right"
                                              >
                                                View Listing Detail
                                              </NavLink>
                                            </>
                                          ) : (
                                            <>
                                              {/* <p className="mb-2 fs-sm text-muted"> MLS Number: #{transactionId.propertyDetail.mlsNumber}</p>
                                                                                <p className="mb-2 fs-sm text-muted">Street-Address: {transactionId.propertyDetail.streetAddress}</p>
                                                                                <p className="mb-2 fs-sm text-muted">Type: {transactionId.propertyDetail.propertyType}</p>
                                                                                <p className="mb-2 fs-sm text-muted">State: {transactionId.propertyDetail.areaLocation}</p>
                                                                                <p className="mb-2 fs-sm text-muted">List Price: ${transactionId.propertyDetail.price}</p>
                                                                                <p className="mb-2 fs-sm text-muted"><strong>ListingsAgent:</strong> {transactionId.propertyDetail.listingsAgent}</p>
                                                                               */}
                                              <p className="mb-2 fs-sm text-muted">
                                                {" "}
                                                MLS Source:{" "}
                                                {
                                                  JSON.parse(
                                                    transactionId.propertyDetail
                                                      .additionaldataMLS
                                                  ).value[0].SourceSystemName
                                                }
                                              </p>
                                              <p className="mb-2 fs-sm text-muted">
                                                {" "}
                                                MLS Number:{" "}
                                                {
                                                  transactionId.propertyDetail
                                                    .mlsNumber
                                                }
                                              </p>
                                              <p className="mb-2 fs-sm text-muted">
                                                {" "}
                                                Listed By:{" "}
                                                <strong>
                                                  {
                                                    transactionId.propertyDetail
                                                      .listingsAgent
                                                  }
                                                  <br />
                                                  {
                                                    JSON.parse(
                                                      transactionId
                                                        .propertyDetail
                                                        .additionaldataMLS
                                                    ).value[0].ListOfficeName
                                                  }
                                                </strong>
                                              </p>
                                              <p className="mb-2 fs-sm text-muted">
                                              List Price: 
                                            {
                                              transactionId?.propertyDetail.price?? "$0"
                                             }
                                              </p>
                                              <p className="mb-2 fs-sm text-muted">
                                                Category:{" "}
                                                {
                                                  transactionId.propertyDetail
                                                    .propertyType
                                                }
                                              </p>
                                              <p className="mb-2 fs-sm text-muted">
                                                Subdivision: $
                                                {
                                                  transactionId.propertyDetail
                                                    .subDivision
                                                }
                                              </p>
                                              <p className="mb-2 fs-sm text-muted">
                                                Beds:{" "}
                                                {
                                                  transactionId.propertyDetail
                                                    .bed
                                                }
                                              </p>
                                              <p className="mb-2 fs-sm text-muted">
                                                Baths:{" "}
                                                {JSON.parse(
                                                  transactionId.propertyDetail
                                                    .additionaldataMLS
                                                ).value[0].BathroomsFull ??
                                                  0}{" "}
                                                /{" "}
                                                {JSON.parse(
                                                  transactionId.propertyDetail
                                                    .additionaldataMLS
                                                ).value[0].BathroomsHalf ??
                                                  0}{" "}
                                              </p>
                                              <p className="mb-2 fs-sm text-muted">
                                              Building Area(Square Feet):{" "}
                                                {JSON.parse(
                                                  transactionId.propertyDetail
                                                    .additionaldataMLS
                                                ).value[0].BuildingAreaTotal ??
                                                  0}
                                              </p>
                                              <p className="mb-2 fs-sm text-muted">
                                                Year Built:{" "}
                                                {JSON.parse(
                                                  transactionId.propertyDetail
                                                    .additionaldataMLS
                                                ).value[0].YearBuilt ?? 0}
                                              </p>
                                              {/* <p className="mb-2 fs-sm text-muted"><strong><NavLink onClick={Viewmorefunction} >View Less</NavLink></strong></p> */}
                                              <p className="mb-2 fs-sm text-muted">
                                                <strong>
                                                  <NavLink
                                                    onClick={Viewmorefunction}
                                                  >
                                                    View More
                                                  </NavLink>
                                                </strong>
                                              </p>
                                            </>
                                          )}
                                        </>
                                      ) : (
                                        <>
                                          <p className="mb-2 fs-sm text-muted">
                                            MLS Source: Non-MLS Property
                                          </p>
                                          <p className="mb-2 fs-sm text-muted">
                                            List Price: 
                                            {
                                              transactionId?.propertyDetail.price?? "$0"
                                             }
                                          </p>
                                        </>
                                      )}
                                    </div>
                                  </>
                                ) : (
                                  <p className="px-3">Property Not Set</p>
                                )}
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>

                      <div className="accordion mt-3" id="accordionExample4">
                        {requireddetailsaddeddata.length > 0 ? (
                          <div className="accordion-item">
                            <h2
                              className="accordion-header"
                              id="headingRequired"
                            >
                              <button
                                className="accordion-button"
                                type="button"
                                data-bs-toggle="collapse"
                                data-bs-target="#collapseRequired"
                                aria-expanded="true"
                                aria-controls="collapseRequired"
                              >
                                {requireddetailsaddeddata.length} Items Required
                                Now
                              </button>
                            </h2>

                            {requireddetailsaddeddata.map((item) => {
                              var answerresp = "";
                              if (
                                item.transactionDetails &&
                                item.transactionDetails.answer
                              ) {
                                answerresp = item.transactionDetails.answer;
                              }
                              return (
                                <div
                                  className="accordion-collapse collapse show"
                                  aria-labelledby="headingRequired"
                                  data-bs-parent="#accordionExample"
                                  id="collapseRequired"
                                  key={item._id}
                                >
                                  <div className="accordion-body">
                                   
                                    {item.question} &nbsp; &nbsp;
                                    {item.transactionDetails &&
                                    item.transactionDetails.answer ? (
                                      <span
                                        className="pull-right ms-2"
                                        style={{ color: "green" }}
                                      >
                                        {item.answer_type ===
                                        "Date"
                                          ? moment(
                                              item.transactionDetails.answer
                                            ).format("M/D/YYYY")
                                          : item.transactionDetails.answer}
                                      </span>
                                    ) : (
                                      <span> Not Set</span>
                                    )}
                                    &nbsp;
                                    <NavLink
                                      className="pull-right"
                                      onClick={(e) => editInput(e, item._id)}
                                      id={`editanswer${item._id}`}
                                    >
                                      Edit
                                    </NavLink>
                                    {/* {isEditing ? "cancel" : "edit"} */}
                                    <div
                                      className="col-sm-12 mb-4 d-none"
                                      id={`propdetails${item._id}`}
                                    >
                                      {item.answer_type === "input" && (
                                        <>
                                          <hr className="mt-3 w-100" />
                                          <label className="col-form-label mb-0">
                                            {item.input_title}
                                          </label>
                                          <input
                                            className="form-control w-50"
                                            type="text"
                                            id={`propdetails${item._id}`}
                                            name="answer"
                                            value={propertyUpdate.answer}
                                            // value={propertyUpdate.answer ? propertyUpdate.answer : answerresp}
                                            onChange={handleChangeInput}
                                            data-datepicker-options='{"altInput": true, "altFormat": "F j, Y", "dateFormat": "Y-m-d"}'
                                          />
                                          {item?.transactionDetails?._id ? (
                                            <div className="float-left w-50 mb-3">
                                              <NavLink
                                                className="btn btn-primary btn-sm pull-right mt-2"
                                                onClick={() =>
                                                  handleClear(
                                                    item._id,
                                                    item.transactionDetails
                                                  )
                                                }
                                              >
                                                clear answer
                                              </NavLink>
                                            </div>
                                          ) : (
                                            ""
                                          )}
                                          {item.answer_type === "input" ? (
                                            <div className="float-left w-50 mb-3">
                                              <button
                                                className="btn btn-primary btn-sm pull-right mt-3"
                                                onClick={() => {
                                                  saveDate(
                                                    item._id,
                                                    item.transactionDetails
                                                  );
                                                  handleaddevent(
                                                    transactionId,
                                                    item._id,
                                                  );
                                                }}
                                              >
                                                Save
                                              </button>
                                            </div>
                                          ) : (
                                            ""
                                          )}
                                        </>
                                      )}

                                      {item.answer_type === "Select" &&
                                        JSON.parse(item.answer_options) && (
                                          <>
                                            <hr className="mt-3 w-100" />
                                            <label className="col-form-label mb-0">
                                              {item.input_title}
                                            </label>

                                            {JSON.parse(
                                              item.answer_options
                                            ).map((option, index) => (
                                              <div
                                                key={index}
                                                onClick={() =>
                                                  save(
                                                    item._id,
                                                    item.transactionDetails,
                                                    option
                                                  )
                                                }
                                                className={
                                                  item.transactionDetails
                                                    .answer == option
                                                    ? "Selected border rounded-3 w-50 m-0 mb-2"
                                                    : "answer_options border rounded-3 w-50 m-0 mb-2"
                                                }
                                              >
                                                {option}
                                              </div>
                                            ))}

                                            {item?.transactionDetails?._id ? (
                                              <div className="float-left w-50 mb-3">
                                                <NavLink
                                                  className="btn btn-primary btn-sm pull-right mt-2"
                                                  onClick={() =>
                                                    handleClear(
                                                      item._id,
                                                      item.transactionDetails
                                                    )
                                                  }
                                                >
                                                  clear answer
                                                </NavLink>
                                              </div>
                                            ) : (
                                              ""
                                            )}
                                          </>
                                        )}

                                      {item.answer_type === "Yes/No" &&
                                        JSON.parse(item.answer_options) && (
                                          <>
                                            <hr className="mt-3 w-100" />
                                            <label className="col-form-label mb-0">
                                              {item.input_title}
                                            </label>
                                            {item?.answer_options === "[]" ||
                                            item?.answer_options.length ===
                                              0 ? (
                                              <>
                                                <div
                                                  id={`propdetails${item._id}`}
                                                  onClick={() =>
                                                    save(
                                                      item._id,
                                                      item.transactionDetails,
                                                      "Yes"
                                                    )
                                                  }
                                                  className={
                                                    item.transactionDetails
                                                      .answer === "Yes"
                                                      ? "Selected border rounded-3 w-50 m-0 mb-2"
                                                      : "answer_options border rounded-3 w-50 m-0 mb-2"
                                                  }
                                                >
                                                  Yes
                                                </div>
                                                <div
                                                  onClick={() =>
                                                    save(
                                                      item._id,
                                                      item.transactionDetails,
                                                      "No"
                                                    )
                                                  }
                                                  className={
                                                    item.transactionDetails
                                                      .answer === "No"
                                                      ? "Selected border rounded-3 w-50 m-0 mb-2"
                                                      : "answer_options border rounded-3 w-50 m-0"
                                                  }
                                                >
                                                  No
                                                </div>
                                              </>
                                            ) : (
                                              <>
                                                {JSON.parse(
                                                  item.answer_options
                                                ).map((option, index) => (
                                                  <div
                                                    key={index}
                                                    onClick={() =>
                                                      save(
                                                        item._id,
                                                        item.transactionDetails,
                                                        option
                                                      )
                                                    }
                                                    className={
                                                      item.transactionDetails
                                                        .answer === option
                                                        ? "Selected border rounded-3 w-50 m-0 mb-2"
                                                        : "answer_options border rounded-3 w-50 m-0 mb-2"
                                                    }
                                                  >
                                                    {option}
                                                  </div>
                                                ))}
                                              </>
                                            )}

                                            {item?.transactionDetails?._id ? (
                                              <div className="float-left w-50 mb-3">
                                                <NavLink
                                                  className="btn btn-primary btn-sm pull-right mt-3"
                                                  onClick={() =>
                                                    handleClear(
                                                      item._id,
                                                      item.transactionDetails
                                                    )
                                                  }
                                                >
                                                  clear answer
                                                </NavLink>
                                              </div>
                                            ) : (
                                              ""
                                            )}
                                          </>
                                        )}

                                      {item.answer_type === "Date" && (
                                        <>
                                          <hr className="mt-3 w-100" />
                                          <label className="col-form-label mb-0">
                                            {item.input_title}
                                          </label>
                                          <label
                                            className="form-label"
                                            htmlFor="pr-birth-date"
                                          >
                                            Start Date
                                          </label>
                                          <input
                                            className="form-control w-50"
                                            type="date"
                                            id="inline-form-input"
                                            name="answer"
                                            value={
                                              propertyUpdate.answer
                                                ? propertyUpdate.answer
                                                : answerresp
                                            }
                                            onChange={handleChange}
                                            data-datepicker-options='{"altInput": true, "altFormat": "F j, Y", "dateFormat": "Y-m-d"}'
                                          />

                                          {item?.transactionDetails?._id ? (
                                            <div className="float-left w-50 mb-3">
                                              <NavLink
                                                className="btn btn-primary btn-sm pull-right mt-2"
                                                onClick={() =>
                                                  handleClear(
                                                    item._id,
                                                    item.transactionDetails
                                                  )
                                                }
                                              >
                                                clear answer
                                              </NavLink>
                                            </div>
                                          ) : (
                                            ""
                                          )}
                                          <div className="float-left w-50 mb-3">
                                            <button
                                              className="btn btn-primary btn-sm pull-right mt-3"
                                              onClick={() => {
                                                saveDate(
                                                  item._id,
                                                  item.transactionDetails
                                                );
                                                handleaddevent(
                                                  transactionId,
                                                  item._id,
                                                );
                                              }}
                                            >
                                              Save
                                            </button>
                                          </div>
                                        </>
                                      )}
                                    </div>
                                  </div>
                                </div>
                              );
                            })}
                          </div>
                        ) : (
                          ""
                        )}

                        {Object.entries(detailsaddeddata).map(
                          ([category, items]) => (
                            <div className="accordion-item" key={category}>
                              <h2
                                className="accordion-header"
                                id={`heading${category}`}
                              >
                                <button
                                  className="accordion-button"
                                  type="button"
                                  data-bs-toggle="collapse"
                                  data-bs-target={`#collapse${category}`}
                                  aria-expanded="true"
                                  aria-controls={`collapse${category}`}
                                >
                                  {category.charAt(0).toUpperCase() +
                                    category.slice(1)}
                                </button>
                              </h2>
                              <div
                                className="accordion-collapse collapse show"
                                aria-labelledby={`heading${category}`}
                                data-bs-parent="#accordionExample"
                                id={`collapse${category}`}
                              >
                                {items.map((item) => (
                                  <>
                                    <div
                                      className="accordion-body"
                                      key={item._id}
                                    >
                                      {item.question} &nbsp; &nbsp;
                                      {item.transactionDetails &&
                                      item.transactionDetails.answer ? (
                                        <span
                                          className="pull-right ms-2"
                                          style={{ color: "green" }}
                                        >
                                           {item.answer_type ===
                                        "Date"
                                          ? moment(
                                              item.transactionDetails.answer
                                              ).format("M/D/YYYY")
                                          : item.transactionDetails.answer}
                                        </span>
                                      ) : (
                                        <span className="">Not Set</span>
                                      )}
                                      &nbsp;
                                      <NavLink
                                        className="pull-right"
                                        onClick={(e) =>
                                          editInputProp(e, item._id)
                                        }
                                        id={`editanswerprop${item._id}`}
                                      >
                                        Edit
                                      </NavLink>
                                      <div
                                        className="col-sm-12 mb-4 d-none"
                                        id={`propertydetails${item._id}`}
                                      >
                                        {item.answer_type === "input" && (
                                          <>
                                            <hr className="mt-3 w-100" />
                                            <label className="col-form-label mb-0">
                                              {item.input_title}
                                            </label>
                                            <input
                                              className="form-control w-50"
                                              type="text"
                                              name="answer"
                                              value={propertyUpdate.answer}
                                              onChange={handleChangeInput}
                                              data-datepicker-options='{"altInput": true, "altFormat": "F j, Y", "dateFormat": "Y-m-d"}'
                                            />

                                            {item?.transactionDetails?._id ? (
                                              <div className="float-left w-50 mb-3">
                                                <NavLink
                                                  className="btn btn-primary btn-sm pull-right mt-2"
                                                  onClick={() =>
                                                    handleClear(
                                                      item._id,
                                                      item.transactionDetails
                                                    )
                                                  }
                                                >
                                                  clear answer
                                                </NavLink>
                                              </div>
                                            ) : (
                                              ""
                                            )}
                                            {item.answer_type === "input" ? (
                                              <div className="float-left w-50 mb-3">
                                                <button
                                                  className="btn btn-primary btn-sm pull-right mt-3"
                                                  onClick={() => {
                                                    saveDate(
                                                      item._id,
                                                      item.transactionDetails
                                                    );
                                                    handleaddevent(
                                                      transactionId,
                                                      item._id,
                                                    );
                                                  }}
                                                >
                                                  Save
                                                </button>
                                              </div>
                                            ) : (
                                              ""
                                            )}
                                          </>
                                        )}

                                        {item.answer_type === "Select" &&
                                          JSON.parse(item.answer_options) && (
                                            <>
                                              <hr className="mt-3 w-100" />
                                              <p>{item.input_title}</p>

                                              {JSON.parse(
                                                item.answer_options
                                              ).map((option, index) => (
                                                <div
                                                  key={index}
                                                  onClick={() =>
                                                    save(
                                                      item._id,
                                                      item.transactionDetails,
                                                      option
                                                    )
                                                  }
                                                  className={
                                                    item.transactionDetails
                                                      .answer == option
                                                      ? "Selected border rounded-3 w-50 m-0 mb-2"
                                                      : "answer_options border rounded-3 w-50 m-0 mb-2"
                                                  }
                                                >
                                                  {option}
                                                </div>
                                              ))}
                                              {item?.transactionDetails?._id ? (
                                                <div className="float-left w-50 mb-3">
                                                  <NavLink
                                                    className="btn btn-primary btn-sm pull-right mt-2"
                                                    onClick={() =>
                                                      handleClear(
                                                        item._id,
                                                        item.transactionDetails
                                                      )
                                                    }
                                                  >
                                                    clear answer
                                                  </NavLink>
                                                </div>
                                              ) : (
                                                ""
                                              )}
                                            </>
                                          )}

                                        {item.answer_type === "Yes/No" &&
                                          JSON.parse(item.answer_options) && (
                                            <>
                                              <hr className="mt-3 w-100" />
                                              <label className="col-form-label mb-0">
                                                {item.input_title}
                                              </label>
                                              {item?.answer_options === "[]" ||
                                              item?.answer_options.length ===
                                                0 ? (
                                                <>
                                                  <div
                                                    onClick={() =>
                                                      save(
                                                        item._id,
                                                        item.transactionDetails,
                                                        "Yes"
                                                      )
                                                    }
                                                    className={
                                                      item.transactionDetails
                                                        .answer === "Yes"
                                                        ? "Selected border rounded-3 w-50 m-0 mb-2"
                                                        : "answer_options border rounded-3 w-50 m-0 mb-2"
                                                    }
                                                  >
                                                    Yes
                                                  </div>
                                                  <div
                                                    onClick={() =>
                                                      save(
                                                        item._id,
                                                        item.transactionDetails,
                                                        "No"
                                                      )
                                                    }
                                                    className={
                                                      item.transactionDetails
                                                        .answer === "No"
                                                        ? "Selected border rounded-3 w-50 m-0 mb-2"
                                                        : "answer_options border rounded-3 w-50 m-0"
                                                    }
                                                  >
                                                    No
                                                  </div>
                                                </>
                                              ) : (
                                                <>
                                                  {JSON.parse(
                                                    item.answer_options
                                                  ).map((option, index) => (
                                                    <div
                                                      key={index}
                                                      onClick={() =>
                                                        save(
                                                          item._id,
                                                          item.transactionDetails,
                                                          option
                                                        )
                                                      }
                                                      className={
                                                        item.transactionDetails
                                                          .answer === option
                                                          ? "Selected border rounded-3 w-50 m-0 mb-2"
                                                          : "answer_options border rounded-3 w-50 m-0 mb-2"
                                                      }
                                                    >
                                                      {option}
                                                    </div>
                                                  ))}
                                                </>
                                              )}
                                              {item?.transactionDetails?._id ? (
                                                <div className="float-left w-50 mb-3">
                                                  <NavLink
                                                    className="btn btn-primary btn-sm pull-right mt-3"
                                                    onClick={() =>
                                                      handleClear(
                                                        item._id,
                                                        item.transactionDetails
                                                      )
                                                    }
                                                  >
                                                    clear answer
                                                  </NavLink>
                                                </div>
                                              ) : (
                                                ""
                                              )}
                                            </>
                                          )}

                                        {item.answer_type === "Date" && (
                                          <>
                                            <hr className="mt-3 w-100" />
                                            <label className="col-form-label ms-2 mb-0">
                                              {item.input_title}
                                            </label>
                                            <label
                                              className="form-label"
                                              htmlFor="pr-birth-date"
                                            >
                                              Start Date
                                            </label>
                                            <input
                                              className="form-control w-50"
                                              type="date"
                                              name="answer"
                                              value={propertyUpdate.answer}
                                              onChange={handleChange}
                                              data-datepicker-options='{"altInput": true, "altFormat": "F j, Y", "dateFormat": "Y-m-d"}'
                                            />
                                            {item?.transactionDetails?._id ? (
                                              <div className="float-left w-50 mb-3">
                                             {/* {console.log(item.question)} */}
                                                <NavLink
                                                  className="btn btn-primary btn-sm pull-right mt-2"
                                                  onClick={() =>
                                                    handleClear(
                                                      item._id,
                                                      item.transactionDetails,
                                                      
                                                    )
                                                  }
                                                >
                                                  clear answer
                                                </NavLink>
                                              </div>
                                            ) : (
                                              ""
                                            )}
                                            <div className="float-left w-50 mb-3">
                                              <button
                                                className="btn btn-primary btn-sm pull-right mt-3"
                                                onClick={() => {
                                                  saveDate(
                                                    item._id,
                                                    item.transactionDetails
                                                   
                                                  );
                                                  handleaddevent(
                                                    transactionId,
                                                      item._id,
                                                  );
                                                }}
                                              >
                                                Save
                                              </button>
                                            </div>
                                          </>
                                        )}
                                      </div>
                                    </div>
                                  </>
                                ))}
                              </div>
                            </div>
                          )
                        )}
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </main>
    </div>
  );
};

export default Property;

import React, { useState, useEffect, useRef } from 'react';
import { useParams, useNavigate } from "react-router-dom";
import user_service from '../service/user_service';
import Loader from '../../Pages/Loader/Loader.js';
import Toaster from '../../Pages/Toaster/Toaster';
import AllTab from '../../Pages/AllTab';
import axios from "axios";
import jwt from "jwt-decode";
import Cropper from 'react-cropper';
import 'cropperjs/dist/cropper.css';

const defaultSrc =
    '';


const UploadProperty = () => {
    // const [getProperty, setGetProperty] = useState("");
    const [loader, setLoader] = useState({ isActive: null })
    const { isActive } = loader;
    const [toaster, setToaster] = useState({ types: null, isShow: null, toasterBody: "", message: "" });
    const { types, isShow, toasterBody, message } = toaster;
    const [transactionId, setTransactionId] = useState("")
    const params = useParams();

    const [data, setData] = useState("")
    const [step1, setStep1] = useState("");
    const [step2, setStep2] = useState("");
    const [step3, setStep3] = useState("");

    const [listingId, setListingId] = useState("");
    const [file, setFile] = useState(null);
    const [fileExtension, setFileExtension] = useState('');
    const [acceptedFileTypes, setAcceptedFileTypes] = useState([]);
    const [showSubmitButton, setShowSubmitButton] = useState(false);

    const navigate = useNavigate();

    useEffect(() => { window.scrollTo(0, 0);

        const TransactionPropertyGetById = async () => {
            await user_service.listingsGetById(params.pid).then((response) => {
                setLoader({ isActive: false })
                if (response) {
                    ////console.log(response)
                    setListingId(response.data);
                }
            });
        }
        TransactionPropertyGetById(params.pid)
    }, []);


    useEffect(() => { window.scrollTo(0, 0);
        const TransactionGetById = async () => {
            await user_service.transactionGetById(params.id).then((response) => {
                if (response) {
                    setTransactionId(response.data);
                }
            });
        };
        TransactionGetById(params.id);
    }, []);


    const [image, setImage] = useState("");
    const [cropData, setCropData] = useState('');
    const cropperRef = useRef(null);



    const onChange = (e) => {
        e.preventDefault();
        const reader = new FileReader();
        reader.onload = () => {
            setImage(reader.result);
        };
        reader.readAsDataURL(e.target.files[0]);
    };
    
    const getCropData = async () => {
        if (!cropperRef.current || typeof cropperRef.current.getCroppedCanvas !== 'function') {
            return;
        }

        try {
            const croppedCanvas = cropperRef.current.getCroppedCanvas();
            if (croppedCanvas) {



                const imageData = croppedCanvas.toDataURL('image/jpeg');
                //console.log(imageData);

                const formData = new FormData();
                formData.append('file', imageData);

                const response = await axios.post('https://api.brokeragentbase.com/upload', formData, {
                    headers: {
                        'Content-Type': 'multipart/form-data',
                    },
                });
        
            }

           
        } catch (error) {
            console.error('Error uploading image:', error);
        }
    }




    const selectContact = async () => {

        try {
            const userData = {
                propertyDetail: listingId._id,
            };


            setLoader({ isActive: true });

            setLoader({ isActive: true })
            await user_service.TransactionUpdate(params.id, userData).then((response) => {

                if (response.status === 200) {
                    //console.log(response.data);
                    // setArray(response.data);
                    setLoader({ isActive: false });
                    setToaster({
                        types: "Transaction_Updated",
                        isShow: true,
                        toasterBody: response.data.message,
                        message: "Transaction_Updated Successfully",
                    });
                    navigate(`/t-property/${params.id}`);
                } else {
                    setLoader({ isActive: false });
                    setToaster({
                        types: "error",
                        isShow: true,
                        toasterBody: response.data.message,
                        message: "Error",
                    });
                }
            })
        } catch (error) {
            setLoader({ isActive: false });
            setToaster({
                types: "error",
                isShow: true,
                toasterBody: error.response ? error.response.data.message : error.message,
                message: "Error",
            });
        }
    };


    const PhotoUpload = (stepno, value) => {
        if (stepno === "1") {
            setStep1(stepno);
            setStep2("")
            // setStep1("");

            //console.log(stepno)
        }
        if (stepno === "2") {
            setStep2(stepno);
            //console.log(stepno)
        }
        if (stepno === "3") {
            setStep2(stepno);
            //console.log(stepno)
        }


    }

    const getCropChange = () => {
        setStep1("")
        setStep2("")
    }

    return (
        <div className="bg-secondary float-left w-100 pt-4">
            <Loader isActive={isActive} />
            {isShow && <Toaster types={types}
                isShow={isShow}
                toasterBody={toasterBody}
                message={message} />}
            <main className="page-wrapper">
                <AllTab transaction_data = {transactionId}/>
                {/* <!-- Page container--> */}
                <div className="container mb-md-4 ">
                    <div className="bg-light shadow-sm rounded-3 p-2 p-md-5 mb-2">
                        <div className="row">
                            <div className="col-lg-8 col-md-7 mb-5">
                                {/* <!-- Item--> */}
                                <div className="card card-hover card-horizontal border-0 shadow-sm mb-4 p-3">
                                    {step1 === '' && step2 === '' ? (
                                        <div className="col-lg-6">
                                            <div>
                                                {
                                                    cropData ?
                                                        <a className="card-img-top">
                                                            <img src={cropData} alt="cropped" />
                                                            <button type="button" className="btn btn-primary btn-sm  mt-2" onClick={() => PhotoUpload("1")}>Upload Photo</button>
                                                        </a>

                                                        :
                                                        <a className="card-img-top">
                                                            <img src="https://integra.workspace.lwolf.com/common/handler/mls/photo_custom.ashx?s=1&t=5eada3e3-491e-46e2-938c-7c785a1f9c68&r=Seller" alt="Property" /><br />

                                                            <button type="button" className="btn btn-primary btn-sm  mt-2" onClick={() => PhotoUpload("1")}>Upload Photo</button>
                                                        </a>

                                                }

                                                <div className="card-body position-relative pull-right mt-0">
                                                    <p className="mb-2 fs-sm text-muted"><strong>State</strong>:  {listingId.state}</p>
                                                    <p className="mb-2 fs-sm text-muted"><strong>City</strong>:  {listingId.city} </p>
                                                    <p className="mb-2 fs-sm text-muted"><strong>Street Name</strong>:  {listingId.streetName}</p>
                                                    <p className="mb-2 fs-sm text-muted"><strong>Street Number</strong>:  {listingId.mlsNumber}</p>
                                                    <p className="mb-2 fs-sm text-muted">
                                                        <strong>ListPriceLease</strong>: ${listingId.priceLease}
                                                    </p>
                                                    <button type="button" className="btn btn-primary btn-sm " onClick={selectContact}>Edit Property</button>

                                                </div>
                                            </div>

                                        </div>
                                    ) : ""
                                    }

                                    {step1 && step2 === '' ? (
                                        image ? (

                                            <div className="">
                                                <button type="button" className="btn btn-primary btn-sm  mb-5" onClick={getCropChange}>Change Image</button>
                                                <button type="button" className="btn btn-primary btn-sm  pull-right" onClick={getCropData}>Save Image</button>
                                                {/* <img  className="d-none" src={cropData} alt="cropped" /> */}

                                                <div style={{ width: '100%' }}>
                                                    <Cropper
                                                        ref={cropperRef}
                                                        style={{ height: 400, width: '100%' }}
                                                        zoomTo={0.5}
                                                        initialAspectRatio={1}
                                                        preview=".img-preview"
                                                        src={image}
                                                        viewMode={1}
                                                        minCropBoxHeight={10}
                                                        minCropBoxWidth={10}
                                                        background={false}
                                                        responsive={true}
                                                        autoCropArea={1}
                                                        checkOrientation={false}
                                                        guides={true}
                                                    />
                                                </div>
                                                <div>

                                                </div>
                                            </div>
                                        ) : (
                                            <div className="container content-overlay">
                                                <div className="bg-light shadow-sm rounded-3">
                                                    <div className="row">
                                                        <div className="col-md-9 mt-5 mb-6">
                                                            <span>
                                                                <h6 className="text-center ms-5 pull-left">
                                                                    Please select or place a file to upload.
                                                                </h6>
                                                            </span>
                                                            <input
                                                                className="file-uploader file-uploader-grid"
                                                                type="file"
                                                                accept="image/*"
                                                                onChange={onChange}
                                                            />
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        )
                                    ) : null}


                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </main >
            

        </div >
    )
}

export default UploadProperty
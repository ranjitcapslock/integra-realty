import React, { useState, useMemo, useEffect } from "react";
import countryList from "country-list";
import { NavLink, useNavigate, useParams } from "react-router-dom";
import user_service from "../service/user_service";
import jwt from "jwt-decode";
import _ from "lodash";
import Loader from "../../Pages/Loader/Loader.js";
import Toaster from "../../Pages/Toaster/Toaster.js";
import AllTab from "../../Pages/AllTab";
import ReactPaginate from "react-paginate";
import axios from "axios";

const SelectProperty = () => {
  const navigate = useNavigate();
  const initialValues = {
    listingsAgent: "",
    category: "",
    unitNumber: "",
    propertyType: "",
    mlsNumber: "",
    agentId: "",
    streetDirection: "North",
    associateName: "",
    price: "",
    garage: "",
    bed: "",
    subDivision: "",
    streetAddress: "",
    city: "",
    state: "Utah",
    zipCode: "",
    schoolDistrict: "",
    country: "",
    areaLocation: "",
    status: "",
    listDate: "",
    image: "",
  };

  // const initialValues = {agentId:"", mlsNumber: "", areaLocation: "", streetAddress: "", state: "Utah", mlsId: "", city: "", zipCode: "", price: "", price: "", };
  const [formValues, setFormValues] = useState(initialValues);

  const initialValuesNew = {
    listingsAgent: "",
    category: "",
    unitNumber: "",
    propertyType: "",
    mlsNumber: "",
    agentId: "",
    streetDirection: "North",
    associateName: "",
    price: "",
    garage: "",
    bed: "",
    subDivision: "",
    streetAddress: "",
    city: "",
    state: "Utah",
    zipCode: "",
    schoolDistrict: "",
    country: "",
    areaLocation: "",
    status: "",
    listDate: "",
    image: "",
  };
  const [formValuesNew, setFormValuesNew] = useState(initialValuesNew);

  const [formErrors, setFormErrors] = useState({});
  const [isSubmitClick, setISSubmitClick] = useState(false);

  const [formErrorsNew, setFormErrorsNew] = useState({});
  const [isSubmitClickNew, setISSubmitClickNew] = useState(false);
  const [step1, setStep1] = useState("");
  const [category, setCategory] = useState("");
  const [step2, setStep2] = useState("");
  const [step3, setStep3] = useState("");

  const [searchby, setSearchby] = useState("mls");
  const [membership, setMembership] = useState("");

  const [file, setFile] = useState(null);
  const [data, setData] = useState("");

  const [loader, setLoader] = useState({ isActive: null });
  const { isActive } = loader;
  const [toaster, setToaster] = useState({
    types: null,
    isShow: null,
    toasterBody: "",
    message: "",
  });
  const { types, isShow, toasterBody, message } = toaster;

  const [search_value, setSearchValue] = useState({ associateName: "" });
  const [searchmlsnumber, setSearchmlsnumber] = useState({ mlsnumber: "" });

  
  const [select, setSelect] = useState("");
  const [transactionId, setTransactionId] = useState("");
  const [result, setResult] = useState([]);
  const [isLoading, setIsLoading] = useState(false);
  const [pageCount, setPageCount] = useState(0);
  const [array, setArray] = useState("");
  const [profile, setProfile] = useState("");

  const params = useParams();

  const options = useMemo(() => {
    const countries = countryList.getData();
    const utahState = {
      code: "UT",
      name: "Utah",
      districts: [
        "Alabama",
        "Alaska",
        "Arizona",
        "Arkansas",
        "California",
        "Colorado",
        "Connecticut",
        "Delaware",
        "District of Columbia",
        "Florida",
        "Georgia",
        "Hawaii",
        "Idaho",
        "Illinois",
        "Indiana",
        "Iowa",
        "Kansas",
        "Kentucky",
        "Louisiana",
        "Maine",
        "Maryland",
        "Massachusetts",
        "Michigan",
        "Minnesota",
        "Mississippi",
        "Missouri",
        "Montana",
        "Nebraska",
        "Nevada",
        "New Hampshire",
        "New Jersey",
        "New Mexico",
        "New York",
        "North Carolina",
        "North Dakota",
        "Ohio",
        "Oklahoma",
        "Oregon",
        "Pennsylvania",
        "Rhode Island",
        "South Carolina",
        "South Dakota",
        "Tennessee",
        "Texas",
        "Utah",
        "Vermont",
        "Virginia",
        "Washington",
        "West Virginia",
        "Wisconsin",
        "Wyoming",
      ],
    };
    return [...countries, utahState];
  }, []);

  const AddBasic = (stepno, value) => {
    if (stepno === "1") {
      setStep1(stepno);
      setCategory(value);
      //console.log(stepno)
      //console.log(value)
    }
    if (stepno === "2") {
      setStep2(stepno);
      setCategory(value);
      //console.log(stepno)
      //console.log(value)
    }

    if (stepno === "3") {
      setStep3(stepno);
      setCategory(value);
      //console.log(stepno)
      //console.log(value)
    }
  };

  const TransactionGetById = async () => {
    await user_service.transactionGetById(params.id).then((response) => {
      if (response) {
        // console.log(response.data.propertyDetail);
        setFormValues(response.data.propertyDetail);
      }
    });
  };

  const profileGetAll = async () => {
    setLoader({ isActive: true });
    await user_service
      .profileGet(jwt(localStorage.getItem("auth")).id)
      .then((response) => {
        if (response) {
          setLoader({ isActive: false });
          setProfile(response.data);
        }
      });
  };

  useEffect(() => {
    window.scrollTo(0, 0);
    TransactionGetById(params.id);
    ContactGetById(jwt(localStorage.getItem("auth")).id);
    profileGetAll();
  }, []);

  {
    /* <!-- Input onChange Start--> */
  }
  const handleChange = (e) => {
    const { name, value } = e.target;
    setFormValues({ ...formValues, [name]: value });
  };

  const handleChangePriceNew = (e) => {
    const { name, value } = e.target;
    const numericValue = value.replace(/[^\d.]/g, "");

    if (numericValue !== "") {
      const floatValue = parseFloat(numericValue);

      if (!isNaN(floatValue)) {
        setFormValues((prevValues) => ({
          ...prevValues,
          [name]: floatValue.toLocaleString("en-US", {
            style: "currency",
            currency: "USD",
            minimumFractionDigits: 0,
          }),
        }));
      }
    } else {
      setFormValues((prevValues) => ({
        ...prevValues,
        [name]: "",
      }));
    }
  };

  const handleChangesearch = (event) => {
    setSearchValue({ associateName: event.target.value });
  };

  const handleChangesearchmls = (event) => {
    setSearchmlsnumber({ mlsnumber: event.target.value });
  };

  {
    /* <!-- Input onChange End--> */
  }

  {
    /* <!-- Form Validation Start--> */
  }
  useEffect(() => {
    if (formValues && isSubmitClick) {
      validate();
    }
  }, [formValues]);

  const validate = () => {
    const values = formValues;
    const errors = {};
    if (!values.streetNumber) {
      errors.streetNumber = "Street Number is required";
    }

    if (!values.streetDirection) {
      errors.streetDirection = "Steeet Direction is required";
    }
    if (!values.streetAddress) {
      errors.streetAddress = "Street Name is required";
    }

    // if (!values.mlsId) {
    //     errors.mlsId = "unitNumber is required";
    // }
    if (!values.city) {
      errors.city = "City is required";
    }
    if (!values.state) {
      errors.state = "State is required";
    }

    if (!values.zipCode) {
      errors.zipCode = "Postal Code is required";
    }
    if (!values.propertyType) {
      errors.propertyType = "Property Type is required";
    }
    // if (!values.priceLease) {
    //     errors.priceLease = "PriceLease is required";
    // }
    setFormErrors(errors);
    return errors;
  };
  {
    /* <!-- Form Validation End--> */
  }

  const handleFileUpload = async (e) => {
    const selectedFile = e.target.files[0];

    if (selectedFile) {
      setFile(selectedFile);

      const fileNameWithoutExtension = selectedFile.name.replace(
        /\.[^/.]+$/,
        ""
      ); // Extracts the file name without extension
      const formData = new FormData();
      formData.append("file", selectedFile);

      const config = {
        headers: {
          Accept: "application/json",
          "Content-Type": "multipart/form-data",
          Authorization: `Bearer ${localStorage.getItem("auth")}`,
        },
      };

      try {
        setLoader({ isActive: true });

        const uploadResponse = await axios.post(
          "https://api.brokeragentbase.com/upload",
          formData,
          config
        );
        const uploadedFileData = uploadResponse.data;

        setLoader({ isActive: false });
        setData(uploadedFileData);
      } catch (error) {
        console.error("Error occurred during file upload:", error);
      }
    }
  };
  {
    /* <!-- Form onSubmit Start--> */
  }
  const handleSubmit = async (e) => {
    console.log(formValues?._id);
    if (formValues?._id) {
      e.preventDefault();
      setISSubmitClick(true);
      let checkValue = validate();
      if (_.isEmpty(checkValue)) {
        const currentDate = new Date();
        const formattedDate = currentDate.toISOString();

        const userData = {
          agentId: jwt(localStorage.getItem("auth")).id,
          listingsAgent: "Shawn",
          // category: "residential",
          unitNumber: formValues.unitNumber,
          propertyType: formValues.propertyType,
          streetNumber: formValues.streetNumber,
          associateName: profile.firstName + " " + profile.lastName,
          price: formValues.price,
          priceLease: formValues.priceLease,
          // garage: "1",
          // bed: "5",
          city: formValues.city,
          state: formValues.state,
          country: "Utah",
          subDivision: "Utah",
          streetAddress: formValues.streetAddress,
          zipCode: formValues.zipCode,
          schoolDistrict: "Salt lake Utah",
          areaLocation: "Utah",
          streetDirection: formValues.streetDirection,
          status: "Active",
          listDate: formattedDate,
          image: data,
        };

        try {
          setLoader({ isActive: true });

          const response = await user_service.listingUpdate(
            formValues?._id,
            userData
          );
          setLoader({ isActive: false });
          if (response) {
            setLoader({ isActive: false });
            setToaster({
              types: "SelectProperty",
              isShow: true,
              toasterBody: response.data.message,
              message: "Property Updated Successfully",
            });
            setTimeout(() => {
              //  navigate(`/uploadProperty/${params.id}/${response.data._id}`);
            }, 1000);
            TransactionGetById(params.id);
          } else {
            setLoader({ isActive: false });
            setToaster({
              types: "error",
              isShow: true,
              toasterBody: response.data.message,
              message: "Error",
            });
          }
        } catch (error) {
          setLoader({ isActive: false });
          setToaster({
            types: "error",
            isShow: true,
            toasterBody: error,
            message: "Error",
          });
        }
        setTimeout(() => {
          setToaster({
            types: "error",
            isShow: false,
            toasterBody: null,
            message: "Error",
          });
        }, 1000);
      }
    }
  };
  {
    /* <!-- Form onSubmit End--> */
  }

  const handleChanges = (e) => {
    const { name, value } = e.target;
    setFormValuesNew({ ...formValuesNew, [name]: value });
  };

  const handleChangePrice = (e) => {
    const { name, value } = e.target;
    const numericValue = value.replace(/[^\d.]/g, "");

    if (numericValue !== "") {
      const floatValue = parseFloat(numericValue);

      if (!isNaN(floatValue)) {
        setFormValuesNew((prevValues) => ({
          ...prevValues,
          [name]: floatValue.toLocaleString("en-US", {
            style: "currency",
            currency: "USD",
            minimumFractionDigits: 0,
          }),
        }));
      }
    } else {
      setFormValuesNew((prevValues) => ({
        ...prevValues,
        [name]: "",
      }));
    }
  };

  useEffect(() => {
    if (formValuesNew && isSubmitClickNew) {
      validateNew();
    }
  }, [formValuesNew]);

  const validateNew = () => {
    const values = formValuesNew;
    const errors = {};
    if (!values.streetNumber) {
      errors.streetNumber = "Street Number is required";
    }

    if (!values.streetDirection) {
      errors.streetDirection = "Steeet Direction is required";
    }
    if (!values.streetAddress) {
      errors.streetAddress = "Street Name is required";
    }

    // if (!values.mlsId) {
    //     errors.mlsId = "unitNumber is required";
    // }
    if (!values.city) {
      errors.city = "City is required";
    }
    if (!values.state) {
      errors.state = "State is required";
    }

    if (!values.zipCode) {
      errors.zipCode = "Postal Code is required";
    }
    if (!values.propertyType) {
      errors.propertyType = "Property Type is required";
    }
    // if (!values.priceLease) {
    //     errors.priceLease = "PriceLease is required";
    // }
    setFormErrorsNew(errors);
    return errors;
  };

  const handleNewSubmit = async (e) => {
    e.preventDefault();
    setISSubmitClickNew(true);
    let checkValue = validateNew();

    if (_.isEmpty(checkValue)) {
      const currentDate = new Date();
      const formattedDate = currentDate.toISOString();

      const userData = {
        agentId: jwt(localStorage.getItem("auth")).id,
        associateName: profile.firstName + " " + profile.lastName,
        listingsAgent: "Shawn",
        unitNumber: formValuesNew.unitNumber,
        propertyType: formValuesNew.propertyType,
        streetNumber: formValuesNew.streetNumber,
        price: formValuesNew.price,
        priceLease: formValuesNew.priceLease,
        city: formValuesNew.city,
        state: formValuesNew.state,
        country: "Utah",
        subDivision: "Utah",
        streetAddress: formValuesNew.streetAddress,
        zipCode: formValuesNew.zipCode,
        schoolDistrict: "Salt lake Utah",
        areaLocation: "Utah",
        streetDirection: formValuesNew.streetDirection,
        status: "Active",
        listDate: formattedDate,
        image: data,
      };

      try {
        setLoader({ isActive: true });

        const responseCreate = await user_service.listingsCreate(userData);

        if (responseCreate && responseCreate.data) {
          const newdata = responseCreate.data._id;

          const userDataUpdate = {
            // agentId: jwt(localStorage.getItem("auth")).id,
            propertyDetail: newdata,
          };

          const responseUpdate = await user_service.TransactionUpdate(
            params.id,
            userDataUpdate
          );

          if (responseUpdate && responseUpdate.data) {
            setLoader({ isActive: false });

            setToaster({
              types: "SelectProperty",
              isShow: true,
              toasterBody: responseUpdate.data.message,
              message: "Property Added Successfully",
            });

            setTimeout(() => {
              setToaster((prevToaster) => ({
                ...prevToaster,
                isShow: false,
              }));
            }, 1000);

            TransactionGetById(params.id);
          }
        }
      } catch (error) {
        setLoader({ isActive: false });
        setToaster({
          types: "error",
          isShow: true,
          toasterBody: error,
          message: "Error",
        });
      }
    }
  };

  const propertySearch = async () => {
    if (search_value.associateName) {
      try {
        setIsLoading(true);

        const response = await user_service.listingSearch(
          1,
          1,
          search_value.associateName
        );
        if (response) {
          //console.log(response);
          setResult(response.data.data);
          setPageCount(Math.ceil(response.data.totalRecords / 3));
        }

        setIsLoading(false);
      } catch (error) {
        console.error(error);
        setIsLoading(false);
      }
    }
  };

  const handlePageClick = async (data) => {
    let currentPage = data.selected + 1;
    let skip = currentPage - 1 + currentPage;
    setLoader({ isActive: true });
    await user_service
      .listingSearch(currentPage, 1, search_value.associateName)
      .then((response) => {
        setLoader({ isActive: false });
        if (response) {
          setResult(response.data.data);
          setPageCount(Math.ceil(response.data.totalRecords / 10));
        }
      });
  };

  const MLSSearch = async () => {
    if (searchmlsnumber.mlsnumber) {
      try {
        setIsLoading(true);
        let response;
        if (membership === "UtahRealEstate.com") {
          console.log(membership);

          response = await user_service.listingSearchMLS(
            searchmlsnumber.mlsnumber
          );
        } else if (membership === "Park City") {
          response = await user_service.listingSearchMLSspark(
            searchmlsnumber.mlsnumber,
            "parkcity"
          );
        } else if (membership === "Washington County") {
          response = await user_service.listingSearchMLSspark(
            searchmlsnumber.mlsnumber,
            "washingtoncounty"
          );
        } else if (membership === "Iron County") {
          response = await user_service.listingSearchMLSspark(
            searchmlsnumber.mlsnumber,
            "ironcountry"
          );
        } else {
          response = await user_service.listingSearchMLS(
            searchmlsnumber.mlsnumber
          );
        }

        if (response) {
          console.log("response");
          console.log(response);
          setResult(response.data);
          setPageCount(1);
        }

        setIsLoading(false);
      } catch (error) {
        console.error(error);
        setIsLoading(false);
      }
    }
  };

  const handleSearchListing = async (id) => {
    try {
      if (searchby === "mls") {
        setSelect(id);
      } else {
        const response = await user_service.listingsGetById(id);
        console.log(response);
        setSelect(response.data);
      }
    } catch (error) {
      console.error(error);
    }
  };

  const handleSearchListingMLS = async (mlsdata) => {
    if (mlsdata.value) {
      const mlspropdata = {
        agentId: jwt(localStorage.getItem("auth")).id,
        listingsAgent: mlsdata.value[0].ListAgentFullName ?? "N/A",
        category: mlsdata.value[0].PropertyType
          ? mlsdata.value[0].PropertyType
          : "N/A",
        mlsId: mlsdata.value[0].ListingId
          ? mlsdata.value[0].ListingId.toString()
          : "N/A",
        propertyType: mlsdata.value[0].PropertyType
          ? mlsdata.value[0].PropertyType
          : "N/A",
        mlsNumber: mlsdata.value[0].ListingId.toString(),
        associateName: mlsdata.value[0]?.AssociationName
          ? mlsdata.value[0].AssociationName
          : "N/A",
        price: mlsdata?.value[0]?.ListPrice
          ? mlsdata?.value[0]?.ListPrice.toString()
          : "N/A",
        priceLease: mlsdata.value[0]?.LeaseAmount
          ? mlsdata.value[0].LeaseAmount.toString()
          : "N/A",
        garage: mlsdata.value[0]?.GarageSpaces
          ? mlsdata.value[0].GarageSpaces.toString()
          : "N/A",
        bed: mlsdata.value[0]?.BedroomsTotal
          ? mlsdata.value[0].BedroomsTotal.toString()
          : "N/A",
        year_built: mlsdata.value[0]?.YearBuilt
          ? mlsdata.value[0].YearBuilt.toString()
          : "N/A",

        baths: mlsdata.value[0]?.BathroomsTotalInteger
          ? mlsdata.value[0].BathroomsTotalInteger.toString()
          : "N/A",

        square_feet: mlsdata.value[0]?.LotSizeSquareFeet
          ? mlsdata.value[0].LotSizeSquareFeet.toString()
          : "N/A",

        building_area_total: mlsdata.value[0]?.BuildingAreaTotal
          ? mlsdata.value[0].BuildingAreaTotal.toString()
          : "N/A",

          
        city: mlsdata.value[0]?.City ? mlsdata.value[0]?.City : "N/A",
        state: mlsdata.value[0]?.StateOrProvince
          ? mlsdata.value[0]?.StateOrProvince
          : "N/A",
        country: mlsdata.value[0]?.CountyOrParish
          ? mlsdata.value[0]?.CountyOrParish
          : "N/A",
        subDivision: mlsdata.value[0]?.SubdivisionName
          ? mlsdata.value[0]?.SubdivisionName
          : "N/A",
        streetAddress: mlsdata.value[0]?.UnparsedAddress
          ? mlsdata.value[0]?.UnparsedAddress
          : "N/A",
        zipCode: mlsdata.value[0]?.PostalCode
          ? mlsdata.value[0].PostalCode.toString()
          : "N/A",
        schoolDistrict: mlsdata.value[0]?.ElementarySchoolDistrict
          ? mlsdata.value[0]?.ElementarySchoolDistrict
          : "N/A",
        areaLocation: mlsdata.value[0]?.MLSAreaMajor
          ? mlsdata.value[0]?.MLSAreaMajor
          : "N/A",
        streetDirection: "N/A",
        status: mlsdata.value[0]?.MlsStatus
          ? mlsdata.value[0]?.MlsStatus
          : "N/A",
        listDate: mlsdata.value[0]?.OriginalEntryTimestamp ?? new Date(),
        image: selectedImageUrl
          ? selectedImageUrl
          : mlsdata?.media && mlsdata?.media[0]?.MediaURL
          ? mlsdata?.media[0]?.MediaURL
          : "default_image_url",
        listing_AgentID: mlsdata.value[0]?.ListAgentKey
          ? mlsdata.value[0].ListAgentKey
          : "",
        propertyFrom: "mls",
        additionaldataMLS: JSON.stringify(mlsdata),
      };

      console.log(mlspropdata);
      console.log("mlspropdata");

      try {
        setLoader({ isActive: true });
        const response = await user_service.listingsCreate(mlspropdata);
        setLoader({ isActive: false });

        if (response) {
          setToaster({
            types: "SelectProperty",
            isShow: true,
            toasterBody: response.data.message,
            message: "SelectProperty Successfully",
          });

          // Return the response or the required data from the response
          return response.data; // You can modify this to return the specific data you need
        } else {
          setToaster({
            types: "error",
            isShow: true,
            toasterBody: response.data.message,
            message: "Error",
          });

          // Return an error or null if needed
          return null;
        }
      } catch (error) {
        setLoader({ isActive: false });
        setToaster({
          types: "error",
          isShow: true,
          toasterBody: error,
          message: "Error",
        });

        // Reject the promise with the error
        throw error;
      }
    }
  };

  const handlePropertyUpdate = async () => {
    let resp; // Define a variable to store the response from handleSearchListingMLS
    let propertyId;
    if (searchby === "mls") {
      // Call handleSearchListingMLS and wait for the response
      resp = await handleSearchListingMLS(select);
      setSelect(resp);
      propertyId = resp._id;
      console.log(resp);
      console.log("resp");
    } else {
      propertyId = select._id;
    }
    console.log(propertyId);
    console.log("propertyId");

    // const contacts = transactionId?.contact1.map((post) => ({
    //   type: post.data.type,
    //   contactId: post.data._id,
    // }));

    try {
      const userData = {
        propertyDetail: propertyId,
      };
      setLoader({ isActive: true });
      await user_service
        .TransactionUpdate(params.id, userData)
        .then((response) => {
          if (response.status === 200) {
            setArray(response.data);
            setLoader({ isActive: false });
            setToaster({
              types: "Transaction_Updated",
              isShow: true,
              toasterBody: response.data.message,
              message: "Transaction Updated Successfully",
            });
            navigate(`/transaction-property/${params.id}`);
          } else {
            setLoader({ isActive: false });
            setToaster({
              types: "error",
              isShow: true,
              toasterBody: response.data.message,
              message: "Error",
            });
          }
        });
    } catch (error) {
      setLoader({ isActive: false });
      setToaster({
        types: "error",
        isShow: true,
        toasterBody: error.response
          ? error.response.data.message
          : error.message,
        message: "Error",
      });
    }
  };

  const changeOption = () => {
    setStep1("");
    setCategory("");
    // setStep2("")
    setStep3("");
  };
  const Cancel = () => {
    setStep2("");
    setResult("");
    setSelect("");
  };

  const CancelProperty = () => {
    setSelect("");
  };

  const clickHere = () => {
    setStep1("");
  };

  const Searchbyfunction = (e, type) => {
    e.preventDefault();
    setSearchby(type);
  };

  const ListingById = async () => {
    try {
      const response = await user_service.listingsGetById(params.pid);
      setLoader({ isActive: false });

      if (response) {
        setArray(response.data);
        // setAllContacts(response.data.contact1);
      }
    } catch (error) {
      console.error(error);
      setLoader({ isActive: false });
    }
  };

  useEffect(() => {
    window.scrollTo(0, 0);
    // setLoader({ isActive: true });

    // ListingById(params.pid);
  }, []);

  const [getContact, setGetContact] = useState(initialValues);
  const [mls_membership, setMls_membership] = useState([]);
  const [
    commaSeparatedValuesmls_membership,
    setCommaSeparatedValuesmls_membership,
  ] = useState("");

  const ContactGetById = async () => {
    await user_service
      .contactGetById(jwt(localStorage.getItem("auth")).id)
      .then((response) => {
        if (response) {
          setGetContact(response.data);

          if (response.data && response.data.additionalActivateFields) {
            const mls_membershipString =
              response.data.additionalActivateFields?.mls_membership;
            if (mls_membershipString) {
              // Check if mls_membershipString is a string
              if (typeof mls_membershipString === "string") {
                try {
                  // Attempt to parse the string as JSON
                  const mls_membershipParsed = JSON.parse(mls_membershipString);
                  // Set the parsed value to your state or variable
                  if (mls_membershipParsed == "") {
                    setMls_membership(mls_membership);
                  } else {
                    setMls_membership(mls_membershipParsed);
                  }
                  if (mls_membershipParsed == "") {
                    setCommaSeparatedValuesmls_membership("");
                  } else {
                    const boardMembershipValues = mls_membershipParsed.map(
                      (item) => item.value
                    );
                    //   console.log("boardMembershipValues")
                    //   console.log(boardMembershipValues)
                    setCommaSeparatedValuesmls_membership(
                      // boardMembershipValues.join(", ")
                      boardMembershipValues
                    );
                  }
                } catch (error) {
                  // Handle the error if parsing fails
                  console.error("Error parsing mls_membership as JSON:", error);
                  // You might want to set a default value or handle the error in another way
                  // For debugging, you can log the JSON parse error message:
                  console.error("JSON Parse Error:", error.message);
                  // Set a default value if parsing fails
                  setCommaSeparatedValuesmls_membership(""); // Replace "defaultValue" with your default value
                }
              } else {
                //console.log("sdfdsfdsfsd");
                // Handle the case where mls_membershipString is not a string (e.g., it's already an object)
                // You can directly set it to your state or variable
                setCommaSeparatedValuesmls_membership("");
              }
            } else {
              // Handle the case where mls_membershipString is undefined or null
              // Set a default value or handle it as needed
              setCommaSeparatedValuesmls_membership(""); // Replace "defaultValue" with your default value
            }
          }
        }
      });
  };
  const [selectedImageUrl, setSelectedImageUrl] = useState("");

  const handleImageUrl = (imageUrl) => {
    setSelectedImageUrl(imageUrl);
  };

  return (
    <div className="bg-secondary float-left w-100 pt-4 mb-4">
      <Loader isActive={isActive} />

      {isShow && (
        <Toaster
          types={types}
          isShow={isShow}
          toasterBody={toasterBody}
          message={message}
        />
      )}
      <main className="page-wrapper">
        {/* <!-- Page content--> */}
        {/* <!-- Page container--> */}
        <div className="content-overlay transaction_tab_view mt-0">
        <div className="row">
          <AllTab />
          <div className="col-md-12">
            <div className="">
              {!step1 && !step2 && !step3 && (
                <div className="bg-light rounded-3 p-3">
                  <div className="">
                    <div className="d-flex align-items-center justify-content-start mb-3">
                      <h3 className="mb-0">
                        Do you want to load a property from an MLS Listing?
                      </h3>
                      {/* <button
                        className="btn btn-primary btn-sm pull-right  ms-5 order-lg-3"
                        href="#"
                        onClick={() => AddBasic("1", "Enter Basic")}
                      >
                        Update Property
                      </button> */}
                    </div>
                    <p className="mb-4">
                      Your information can be automatically loaded with photos
                      and descriptions.
                    </p>
                    <div className="col-lg-12 mb-3">
                      <div className="border rounded-3 p-3">
                        <div className="d-flex align-items-center justify-content-between">
                          <div className="pe-2">
                            <label className="form-label fw-bold">
                              Yes, Find the MLS Listing
                            </label>
                            <div id="type-value-mls">
                              Find a listing by street name or MLS number
                            </div>
                          </div>
                          <a
                            className="btn btn-primary btn-sm  ms-5 order-lg-3"
                            href="#"
                            onClick={() => AddBasic("2", "Enter MLS")}
                          >
                            Search MLS
                          </a>
                        </div>
                      </div>
                    </div>

                    {/* <div className="col-lg-12 mb-2">
                      <div className="border rounded-3 p-3">
                        <div className="d-flex align-items-center justify-content-between">
                          <div className="pe-2">
                            <label className="form-label fw-bold">
                              No, Enter Basic Info
                            </label>
                            <div id="type-value">
                              For property not in MLS, or cannot be found.
                            </div>
                          </div>
                          <a
                            className="btn btn-primary btn-sm  ms-5 order-lg-3"
                            onClick={() => AddBasic("3", "Add Basic")}
                          >
                            Add Basic
                          </a>
                        </div>
                      </div>
                    </div> */}
                  </div>
                </div>
              )}

              {step1 && !step2 && !step3 && (
                <div className="">
                  <div className="border bg-light rounded-3 p-3 float-start w-100 mb-0">
                    <h6>Update Property</h6>
                    <div className="form-check">
                      <input
                        className="form-check-input"
                        id="form-check-2"
                        type="checkbox"
                        onChange={handleChange}
                        checked
                      />
                      <label id="form-check-label-one">
                        You selected the <b>'Basic Info'</b> property entry
                        option.
                        <a onClick={changeOption}>
                          <NavLink>&nbsp;&nbsp;Change Option</NavLink>
                        </a>
                      </label>
                      <p className="my-2">
                        What are the details for the property?
                      </p>
                      <p className="mb-2" id="">
                        Please enter as much information as possible.
                      </p>
                    </div>
                    <div className="row mt-4">
                      <div className="col-md-6">
                        <label className="col-form-label" id="form-label-one">
                          Street Number:
                        </label>
                        <input
                          className="form-control"
                          id="text-input-one"
                          type="text"
                          placeholder="Enter your street number"
                          name="streetNumber"
                          onChange={handleChange}
                          value={formValues.streetNumber}
                          style={{
                            border: formErrors?.streetNumber
                              ? "1px solid red"
                              : "",
                          }}
                        />
                        {formErrors.streetNumber && (
                          <div className="invalid-tooltip">
                            {formErrors.streetNumber}
                          </div>
                        )}
                      </div>

                      <div className="col-md-6">
                        <label className="col-form-label" id="form-label-one">
                          Street Direction:
                        </label>
                        <select
                          className="form-select"
                          id="text-input-one"
                          name="streetDirection"
                          placeholder="steeet direction"
                          value={formValues.streetDirection}
                          onChange={handleChange}
                          style={{
                            border: formErrors?.streetDirection
                              ? "1px solid red"
                              : "",
                          }}
                        >
                          <option></option>
                          <option>North</option>
                          <option>Northeast</option>
                          <option>East</option>
                          <option>Southeast</option>
                          <option>South</option>
                          <option>Southwest</option>
                          <option>West</option>
                          <option>Northwest</option>
                        </select>
                        {formErrors.streetDirection && (
                          <div className="invalid-tooltip">
                            {formErrors.streetDirection}
                          </div>
                        )}
                      </div>

                      <div className="col-md-6 mt-2">
                        <label className="col-form-label" for="pr-city">
                          Property Type
                          <span className="text-danger">*</span>
                        </label>
                        <select
                          className="form-select"
                          id="pr-city"
                          name="propertyType"
                          onChange={handleChange}
                          value={formValues.propertyType}
                          style={{
                            border: formErrors?.propertyType
                              ? "1px solid red"
                              : "",
                          }}
                        >
                          <option value=""></option>
                          <option value="land">Land</option>
                          <option value="residential">Residential</option>
                          <option value="commercial_lease">
                            Commercial Lease
                          </option>
                          <option value="commercial_sale">
                            Commercial Sale
                          </option>
                          <option value="commercial_industrial">
                            Commercial/Industrial
                          </option>
                          <option value="residential_lease">
                            Residential/Lease
                          </option>
                          <option value="farm">Farm</option>
                        </select>
                        {formErrors.propertyType && (
                          <div className="invalid-tooltip">
                            {formErrors.propertyType}
                          </div>
                        )}
                      </div>

                      <div className="col-md-6 mt-2">
                        <label className="col-form-label" id="form-label-one">
                          Street Name:
                        </label>
                        <input
                          className="form-control"
                          id="text-input-one"
                          type="text"
                          name="streetAddress"
                          placeholder="Enter your streetName"
                          onChange={handleChange}
                          value={formValues.streetAddress}
                          style={{
                            border: formErrors?.streetAddress
                              ? "1px solid red"
                              : "",
                          }}
                        />

                        {formErrors.streetAddress && (
                          <div className="invalid-tooltip">
                            {formErrors.streetAddress}
                          </div>
                        )}
                      </div>

                      <div className="col-md-6 mt-2">
                        <label className="col-form-label" id="form-label-one">
                          Unit Number:
                        </label>
                        <input
                          className="form-control"
                          id="text-input-one"
                          name="unitNumber"
                          type="text"
                          placeholder="Enter your unitNumber"
                          onChange={handleChange}
                          value={formValues.unitNumber}
                        />
                        {/* {formErrors.mlsId && (
                                                        <div className="invalid-tooltip">{formErrors.mlsId}</div>
                                                    )} */}
                      </div>

                      <div className="col-md-6 mt-2">
                        <label className="col-form-label" id="form-label-one">
                          City:
                        </label>
                        <input
                          className="form-control"
                          id="text-input-one"
                          name="city"
                          type="text"
                          placeholder="Enter your City"
                          onChange={handleChange}
                          autoComplete="on"
                          value={formValues.city}
                          style={{
                            border: formErrors?.city ? "1px solid red" : "",
                          }}
                        />

                        {formErrors.city && (
                          <div className="invalid-tooltip">
                            {formErrors.city}
                          </div>
                        )}
                      </div>

                      <div className="col-md-6 mt-2">
                        <label className="col-form-label" id="form-label-one">
                          State/Province:
                        </label>
                        <select
                          id="text-input-one"
                          className="form-select"
                          name="state"
                          onChange={handleChange}
                          value={formValues.state}
                        >
                          <option value="0">--Select--</option>
                          {options
                            .find((option) => option.code === "UT")
                            .districts.map((district, key) => (
                              <option value={district} key={key}>
                                {district}
                              </option>
                            ))}
                        </select>
                      </div>

                      <div className="col-md-6 mt-2">
                        <label className="col-form-label" id="form-label-one">
                          Postal Code:
                        </label>
                        <input
                          className="form-control"
                          id="text-input-one"
                          name="zipCode"
                          type="text"
                          placeholder="Enter your postal code"
                          onChange={handleChange}
                          value={formValues.zipCode}
                          style={{
                            border: formErrors?.zipCode ? "1px solid red" : "",
                          }}
                        />

                        {formErrors.zipCode && (
                          <div className="invalid-tooltip">
                            {formErrors.zipCode}
                          </div>
                        )}
                      </div>

                      <div className="col-md-6 mt-2">
                        <label className="col-form-label" id="form-label-one">
                          List Price (Sale):
                        </label>
                        <input
                          className="form-control"
                          id="text-input-one"
                          name="price"
                          type="text"
                          placeholder="Enter your list price sale"
                          onChange={handleChangePriceNew}
                          value={formValues.price}
                        />
                      </div>

                      <div className="col-md-6 mt-2">
                        <label className="col-form-label" id="form-label-one">
                          List Price (Lease) /mo:
                        </label>
                        <input
                          className="form-control"
                          id="text-input-one"
                          name="priceLease"
                          type="text"
                          placeholder="Enter your list price lease"
                          onChange={handleChangePriceNew}
                          value={formValues.priceLease}
                        />
                        {/* {formErrors.priceLease && (
                                                        <div className="invalid-tooltip">{formErrors.priceLease}</div>
                                                    )} */}
                      </div>

                      <div className="col-md-6 mt-3">
                        <label className="col-form-label" id="form-label-one">
                          Upload
                        </label>
                        <input
                          className="file-uploader file-uploader-grid"
                          id="inline-form-input"
                          type="file"
                          name="image"
                          onChange={handleFileUpload}
                        />
                      </div>
                      {formValues?.image ? (
                        <div className="col-md-6 mt-3">
                          <img src={formValues?.image} />
                        </div>
                      ) : (
                        ""
                      )}

                      <div className="d-flex mt-4">
                        <label className="col-form-label p-0">
                          Want to add by MLS search?
                        </label>
                        <p className="mb-0 mx-1">To search MLS,</p>
                        <NavLink className="" onClick={clickHere}>
                          Click Here.
                        </NavLink>
                      </div>
                    </div>
                  </div>
                  <div className="pull-right mt-3">
                    <a
                      className="btn btn-primary btn-sm ms-5 order-lg-3"
                      href="#"
                      onClick={handleSubmit}
                    >
                      Update Property Now{" "}
                    </a>
                  </div>
                </div>
              )}

              {category === "Add Basic" ? (
                <div className="">
                  <div className="border bg-light rounded-3 p-3 float-start w-100 mb-0">
                    <h6>Add New Property</h6>
                    <div className="form-check">
                      <input
                        className="form-check-input"
                        id="form-check-2"
                        type="checkbox"
                        onChange={handleChange}
                        checked
                      />
                      <label id="form-check-label-one">
                        You selected the <b>'Basic Info'</b> property entry
                        option.
                        <a onClick={changeOption}>
                          <NavLink>&nbsp;&nbsp;Change Option</NavLink>
                        </a>
                      </label>
                      <p className="my-2">
                        What are the details for the property?
                      </p>
                      <p className="mb-2" id="">
                        Please enter as much information as possible.
                      </p>
                    </div>
                    <div className="row mt-4">
                      <div className="col-md-6">
                        <label className="col-form-label" id="form-label-one">
                          Street Number:
                        </label>
                        <input
                          className="form-control"
                          id="text-input-one"
                          type="text"
                          placeholder="Enter your street number"
                          name="streetNumber"
                          onChange={handleChanges}
                          value={formValuesNew.streetNumber}
                          style={{
                            border: formErrorsNew?.streetNumber
                              ? "1px solid red"
                              : "",
                          }}
                        />
                        {formErrorsNew.streetNumber && (
                          <div className="invalid-tooltip">
                            {formErrorsNew.streetNumber}
                          </div>
                        )}
                      </div>

                      <div className="col-md-6">
                        <label className="col-form-label" id="form-label-one">
                          Street Direction:
                        </label>
                        <select
                          className="form-select"
                          id="text-input-one"
                          name="streetDirection"
                          placeholder="steeet direction"
                          value={formValuesNew.streetDirection}
                          onChange={handleChanges}
                          style={{
                            border: formErrorsNew?.streetDirection
                              ? "1px solid red"
                              : "",
                          }}
                        >
                          <option></option>
                          <option>North</option>
                          <option>Northeast</option>
                          <option>East</option>
                          <option>Southeast</option>
                          <option>South</option>
                          <option>Southwest</option>
                          <option>West</option>
                          <option>Northwest</option>
                        </select>
                        {formErrorsNew.streetDirection && (
                          <div className="invalid-tooltip">
                            {formErrorsNew.streetDirection}
                          </div>
                        )}
                      </div>

                      <div className="col-md-6 mt-2">
                        <label className="col-form-label" for="pr-city">
                          Property Type
                          <span className="text-danger">*</span>
                        </label>
                        <select
                          className="form-select"
                          id="pr-city"
                          name="propertyType"
                          onChange={handleChanges}
                          value={formValuesNew.propertyType}
                          style={{
                            border: formErrorsNew?.propertyType
                              ? "1px solid red"
                              : "",
                          }}
                        >
                          <option value=""></option>
                          <option value="land">Land</option>
                          <option value="residential">Residential</option>
                          <option value="commercial_lease">
                            Commercial Lease
                          </option>
                          <option value="commercial_sale">
                            Commercial Sale
                          </option>
                          <option value="commercial_industrial">
                            Commercial/Industrial
                          </option>
                          <option value="residential_lease">
                            Residential/Lease
                          </option>
                          <option value="farm">Farm</option>
                        </select>
                        {formErrorsNew.propertyType && (
                          <div className="invalid-tooltip">
                            {formErrorsNew.propertyType}
                          </div>
                        )}
                      </div>

                      <div className="col-md-6 mt-2">
                        <label className="col-form-label" id="form-label-one">
                          Street Name:
                        </label>
                        <input
                          className="form-control"
                          id="text-input-one"
                          type="text"
                          name="streetAddress"
                          placeholder="Enter your streetName"
                          onChange={handleChanges}
                          value={formValuesNew.streetAddress}
                          style={{
                            border: formErrorsNew?.streetAddress
                              ? "1px solid red"
                              : "",
                          }}
                        />

                        {formErrorsNew.streetAddress && (
                          <div className="invalid-tooltip">
                            {formErrorsNew.streetAddress}
                          </div>
                        )}
                      </div>

                      <div className="col-md-6 mt-2">
                        <label className="col-form-label" id="form-label-one">
                          Unit Number:
                        </label>
                        <input
                          className="form-control"
                          id="text-input-one"
                          name="unitNumber"
                          type="text"
                          placeholder="Enter your unitNumber"
                          onChange={handleChanges}
                          value={formValuesNew.unitNumber}
                        />
                        {/* {formErrors.mlsId && (
                                                        <div className="invalid-tooltip">{formErrors.mlsId}</div>
                                                    )} */}
                      </div>

                      <div className="col-md-6 mt-2">
                        <label className="col-form-label" id="form-label-one">
                          City:
                        </label>
                        <input
                          className="form-control"
                          id="text-input-one"
                          name="city"
                          type="text"
                          placeholder="Enter your City"
                          onChange={handleChanges}
                          autoComplete="on"
                          value={formValuesNew.city}
                          style={{
                            border: formErrorsNew?.city ? "1px solid red" : "",
                          }}
                        />

                        {formErrorsNew.city && (
                          <div className="invalid-tooltip">
                            {formErrorsNew.city}
                          </div>
                        )}
                      </div>

                      <div className="col-md-6 mt-2">
                        <label className="col-form-label" id="form-label-one">
                          State/Province:
                        </label>
                        <select
                          id="text-input-one"
                          className="form-select"
                          name="state"
                          onChange={handleChanges}
                          value={formValuesNew.state}
                        >
                          <option value="0">--Select--</option>
                          {options
                            .find((option) => option.code === "UT")
                            .districts.map((district, key) => (
                              <option value={district} key={key}>
                                {district}
                              </option>
                            ))}
                        </select>
                      </div>

                      <div className="col-md-6 mt-2">
                        <label className="col-form-label" id="form-label-one">
                          Postal Code:
                        </label>
                        <input
                          className="form-control"
                          id="text-input-one"
                          name="zipCode"
                          type="text"
                          placeholder="Enter your postal code"
                          onChange={handleChanges}
                          value={formValuesNew.zipCode}
                          style={{
                            border: formErrorsNew?.zipCode
                              ? "1px solid red"
                              : "",
                          }}
                        />

                        {formErrorsNew.zipCode && (
                          <div className="invalid-tooltip">
                            {formErrorsNew.zipCode}
                          </div>
                        )}
                      </div>

                      <div className="col-md-6 mt-2">
                        <label className="col-form-label" id="form-label-one">
                          List Price (Sale):
                        </label>
                        <input
                          className="form-control"
                          id="text-input-one"
                          name="price"
                          type="text"
                          placeholder="Enter your list price sale"
                          onChange={handleChangePrice}
                          value={formValuesNew.price}
                        />
                      </div>

                      <div className="col-md-6 mt-2">
                        <label className="col-form-label" id="form-label-one">
                          List Price (Lease) /mo:
                        </label>
                        <input
                          className="form-control"
                          id="text-input-one"
                          name="priceLease"
                          type="text"
                          placeholder="Enter your list price lease"
                          onChange={handleChangePrice}
                          value={formValuesNew.priceLease}
                        />
                        {/* {formErrors.priceLease && (
                                                        <div className="invalid-tooltip">{formErrors.priceLease}</div>
                                                    )} */}
                      </div>

                      <div className="col-md-6 mt-3">
                        <label className="col-form-label" id="form-label-one">
                          Upload
                        </label>
                        <input
                          className="file-uploader file-uploader-grid"
                          id="inline-form-input"
                          type="file"
                          name="image"
                          onChange={handleFileUpload}
                        />
                      </div>
                      {/* {formValues?.image ? (
                        <div className="col-md-6 mt-3">
                          <img src={formValuesNew?.image} />
                        </div>
                      ) : (
                        ""
                      )} */}

                      <div className="d-flex mt-4">
                        <label className="col-form-label p-0">
                          Want to add by MLS search?
                        </label>
                        <p className="mb-0 mx-1">To search MLS,</p>
                        <NavLink className="" onClick={clickHere}>
                          Click Here.
                        </NavLink>
                      </div>
                    </div>
                  </div>
                  <div className="pull-right mt-3">
                    <a
                      className="btn btn-primary btn-sm ms-5 order-lg-3"
                      href="#"
                      onClick={handleNewSubmit}
                    >
                      Add Property Now{" "}
                    </a>
                  </div>
                </div>
              ) : (
                ""
              )}

              {select ? (
                <div className="bg-light rounded-3 p-3 mb-0">
                  <div className="form-check">
                    <input
                      className="form-check-input"
                      id="form-check-2"
                      type="checkbox"
                      checked
                    />
                    <label id="form-check-label-one">
                      You selected the <b>'MLS Listing'</b> property entry
                      option.
                      <NavLink onClick={Cancel}>
                        &nbsp;&nbsp;Change Option
                      </NavLink>
                    </label>
                  </div>
                  <br />

                  <div className="form-check">
                    <input
                      className="form-check-input"
                      id="form-check-2"
                      type="checkbox"
                      checked
                    />
                    <label id="form-check-label-one">
                      You selected the <b>'MLS Listing'</b> property entry
                      option.
                      <NavLink onClick={CancelProperty}>
                        &nbsp;&nbsp;Change Option
                      </NavLink>
                    </label>
                  </div>
                  <NavLink
                    className="btn btn-primary btn-sm mt-3 order-lg-3"
                    href="#"
                    onClick={handlePropertyUpdate}
                  >
                    Set Property Now
                  </NavLink>
                </div>
              ) : (
                <div className="">
                  {step1 === "" && step2 ? (
                    <div className="bg-light float-start w-100 rounded-3 mb-2 p-3">
                      <div className="">
                        <div className="form-check">
                          <input
                            className="form-check-input"
                            id="form-check-2"
                            type="checkbox"
                            checked
                          />
                          <label id="form-check-label-one">
                            You selected the <b>'MLS Listing'</b> property entry
                            option.
                            <NavLink onClick={Cancel}>
                              &nbsp;&nbsp;Change Option
                            </NavLink>
                          </label>
                        </div>
                        <div className="float-left w-100 my-3">
                          <a
                            className="btn btn-primary btn-sm order-lg-3"
                            href="javascript:void(0)"
                            onClick={(e) => Searchbyfunction(e, "mls")}
                          >
                            Search By MLS Number
                          </a>
                          &nbsp;
                          {/* <a
                            className="btn btn-primary btn-sm order-lg-3 ms-2"
                            href="javascript:void(0)"
                            onClick={(e) => Searchbyfunction(e, "mylisting")}
                          >
                            Show My Listing
                          </a> */}
                        </div>
                        {searchby === "mls" ? (
                          <>
                            <h6 className="">Search By MLS Number:</h6>
                            {/* <p className="my-2">utahrealestate.com</p> */}
                            <label className="col-form-label">
                              Select Membership{" "}
                            </label>
                            <select
                              className="form-select mb-2"
                              name="membership"
                              onChange={(event) =>
                                setMembership(event.target.value)
                              }
                              value={membership}
                            >
                              <option value="">Please Select Membership</option>
                              {commaSeparatedValuesmls_membership &&
                              commaSeparatedValuesmls_membership.length > 0
                                ? commaSeparatedValuesmls_membership.map(
                                    (memberahip) => (
                                      <option>{memberahip}</option>
                                    )
                                  )
                                : ""}
                            </select>
                            {membership ? (
                              <>
                                <h6 className="mt-2 mb-2">
                                  Enter a MLS Number:
                                </h6>

                                <div className="col-md-12 mt-2">
                                  <input
                                    className="form-control w-100"
                                    id="text-input-onee"
                                    type="text"
                                    name="mlsnumber"
                                    placeholder="Enter MLS number"
                                    onChange={handleChangesearchmls}
                                    value={searchmlsnumber.mlsnumber}
                                  />
                                </div>
                                <div className="pull-right mt-4">
                                  <a
                                    className="btn btn-primary btn-sm order-lg-3"
                                    onClick={MLSSearch}
                                    disabled={isLoading}
                                  >
                                    {isLoading ? "Please wait" : "Search"}
                                  </a>
                                </div>
                              </>
                            ) : (
                              ""
                            )}
                          </>
                        ) : searchby === "mylisting" ? (
                          <>
                            <h6>Search From Added Property Listing:</h6>

                            <label className="col-form-label">
                              Enter a Associate Name:
                            </label>

                            <div className="col-md-12">
                              <div className="col-md-12">
                                <input
                                  className="form-control w-100"
                                  id="text-input-onee"
                                  type="text"
                                  name="associateName"
                                  placeholder="Enter your associate name"
                                  onChange={handleChangesearch}
                                  value={search_value.associateName}
                                />
                              </div>
                              <div className="pull-right">
                                <a
                                  className="btn btn-primary btn-sm  mt-3 order-lg-3"
                                  onClick={propertySearch}
                                  disabled={isLoading}
                                >
                                  {isLoading ? "Please wait" : "Search"}
                                </a>
                              </div>
                            </div>
                          </>
                        ) : (
                          ""
                        )}
                      </div>
                    </div>
                  ) : (
                    ""
                  )}

                  <div>
                    {isLoading ? (
                      <div className="mb-4 mt-5 text-center">
                        <a className="card-img-top">
                          <i className="fa fa-spinner fa-spin"></i>
                          &nbsp; Searching...
                        </a>
                      </div>
                    ) : result ? (
                      searchby === "mylisting" ? (
                        <>
                          {result.length > 0 ? (
                            <>
                              {result.map((post) => (
                                <div
                                  className="card card-hover card-horizontal transactions border-0 mb-4 p-3 float-start w-100 mt-3"
                                  onClick={(e) => handleSearchListing(post._id)}
                                >
                                  {post.image ? (
                                    <a className="card-img-top">
                                      <img
                                        className="w-100"
                                        src={post.image}
                                        alt="image"
                                      />
                                    </a>
                                  ) : (
                                    ""
                                  )}
                                  <div className="card-body position-relative pb-3">
                                    <span className="mb-2">
                                      {post.state}_{post.mlsNumber}
                                    </span>
                                    <br />
                                    <span className="mb-2">
                                      {post.streetAddress}
                                    </span>
                                    <br />
                                    <span className="mb-2">
                                      {post.areaLocation}
                                    </span>
                                    <br />
                                    <span className="mb-2">
                                      {post.listingsAgent}
                                    </span>
                                  </div>

                                  <div className="d-flex align-items-center justify-content-between">
                                    <button
                                      className="btn btn-primary  px-3 px-sm-4"
                                      id="selectButton"
                                      type="button"
                                    >
                                      Select Listing
                                    </button>
                                    <p className="mb-2 badge bg-success">
                                      {post.price}
                                    </p>
                                    <br />
                                    <h6 className="mt-0 pull-right">
                                      {post.status}
                                    </h6>
                                  </div>
                                </div>
                              ))}
                              <div className="justify-content-end mb-1">
                                <ReactPaginate
                                  previousLabel={"Previous"}
                                  nextLabel={"next"}
                                  breakLabel={"..."}
                                  pageCount={pageCount}
                                  marginPagesDisplayed={1}
                                  pageRangeDisplayed={2}
                                  onPageChange={handlePageClick}
                                  containerClassName={
                                    "pagination justify-content-center"
                                  }
                                  pageClassName={"page-item"}
                                  pageLinkClassName={"page-link"}
                                  previousClassName={"page-item"}
                                  previousLinkClassName={"page-link"}
                                  nextClassName={"page-item"}
                                  nextLinkClassName={"page-link"}
                                  breakClassName={"page-item"}
                                  breakLinkClassName={"page-link"}
                                  activeClassName={"active"}
                                />
                              </div>
                            </>
                          ) : (
                            ""
                          )}
                        </>
                      ) : searchby === "mls" &&
                        result.value &&
                        result.value.length > 0 ? (
                        <>
                          <div className="card card-hover card-horizontal transactions border-0 shadow-sm mb-4 p-3 mt-3 float-start w-100">
                            <div className="media_images">
                              {selectedImageUrl ? (
                                <>
                                  <div className="card-img-top">
                                    <img
                                      className="img-fluid w-100"
                                      src={selectedImageUrl}
                                    />
                                  </div>
                                </>
                              ) : (
                                <>
                                  {result?.media[0]?.MediaURL ? (
                                    <div className="card-img-top">
                                      <img
                                        className="img-fluid"
                                        src={result?.media[0]?.MediaURL}
                                      />
                                    </div>
                                  ) : (
                                    ""
                                  )}
                                </>
                              )}
                              <div className="property_thumb">
                              {result.media.map((item) => (
                                <img
                                  className="media-image"
                                  src={item.MediaURL}
                                  onClick={() => handleImageUrl(item.MediaURL)}
                                />
                              ))}
                              </div>
                            </div>

                            <div className="card-body position-relative pb-3">
                              <span className="mb-2">
                                {result.value[0]?.CountyOrParish}_
                                {result.value[0]?.ListingId}
                              </span>
                              <br />
                              <span className="mb-2">
                                {result.value[0]?.UnparsedAddress}
                              </span>
                              <br />
                              <span className="mb-2">
                                {result.value[0].City},{" "}
                                {result.value[0].StateOrProvince}{" "}
                                {result.value[0]?.PostalCode}
                              </span>
                              <br />
                              <span className="mb-2">{`${result?.value[0]?.ListAgentFullName} / ${result?.value[0]?.ListOfficeName ?? ""}`}</span>
                            </div>

                            {/* <div className="d-flex align-items-center justify-content-between"> */}
                            <div className="float-left text-left">
                              <button
                                className="btn btn-primary me-0"
                                id="selectButton"
                                type="button"
                                onClick={(e) => handleSearchListing(result)}
                              >
                                Select Listing
                              </button>

                              <p className="my-2 badge bg-success">
                                {result.value[0].ListPrice}
                              </p>
                              <br />
                              <h6 className="mt-0 pull-left">
                                {result.value[0].MlsStatus}
                              </h6>
                            </div>
                          </div>
                        </>
                      ) : (
                        ""
                      )
                    ) : (
                      ""
                    )}
                  </div>
                </div>
              )}
            </div>
          </div>
        </div>
        </div>
      </main>
    </div>
  );
};

export default SelectProperty;

import React, { useState, useEffect } from "react";
import AllTab from "../../Pages/AllTab";
import { NavLink, useParams } from "react-router-dom";
import _ from "lodash";
import user_service from "../service/user_service";
import Loader from "../../Pages/Loader/Loader.js";
import Toaster from "../../Pages/Toaster/Toaster.js";
import jwt from "jwt-decode";

const Details = () => {
  const [loader, setLoader] = useState({ isActive: null });
  const { isActive } = loader;
  const [toaster, setToaster] = useState({
    types: null,
    isShow: null,
    toasterBody: "",
    message: "",
  });
  const { types, isShow, toasterBody, message } = toaster;

  const [formValues, setFormValues] = useState({
    question: "",
    answer_type: "input",
    answer_options: "",
    input_title: "",
    trans_type: "residential_sale",
    category: "contacts",
    required: "no",
    is_documentRequired: "no",
    is_active: "1",
    agentId: "",
    rows: [],
  });

  const [formErrors, setFormErrors] = useState({});
  const [isSubmitClick, setISSubmitClick] = useState(false);

  useEffect(() => { 
    if (formValues && isSubmitClick) {
      validate();
    }
    Detailsaddeddata();
  }, [formValues]);

  const validate = () => {
    const values = formValues;
    const errors = {};

    if (!values.question) {
      errors.question = "Question is required";
    }

    if (!values.input_title) {
      errors.input_title = "Input title is required";
    }

    if (values.answer_type === "Select" && !values.rows.some((row) => row.trim())) {
        // Only set "Select Option is required" error if no row has a value
        errors.answer_type = "Select Option is required";
      }

    // if (values.answer_type === "Select") {
    //   errors.answer_type = "Select Option is required";
    // }

    setFormErrors(errors);
    return errors;
  };

  const handleInputChange = (e) => {
    console.log(e.target.name);
    console.log(e.target.value);

    setFormValues({
      ...formValues,
      [e.target.name]: e.target.value,
    });
  };

  const handleAddRow = () => {
    setFormValues({
      ...formValues,
      rows: [...formValues.rows, ""],
    });
  };

  const handleRowChange = (e, index) => {
    const updatedRows = [...formValues.rows];
    updatedRows[index] = e.target.value;

    setFormValues({
      ...formValues,
      rows: updatedRows,
    });
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
    setISSubmitClick(true);
    let checkValue = validate();
    if (_.isEmpty(checkValue)) {
      //console.log(checkValue);

      const userData = {
        question: formValues.question,
        agentId: jwt(localStorage.getItem("auth")).id,
        category: formValues.category,
        answer_type: formValues.answer_type,
        answer_options: JSON.stringify(formValues.rows),
        input_title: formValues.input_title,
        trans_type: formValues.trans_type,

        required: formValues.required,
        is_documentRequired: formValues.is_documentRequired,
        is_active: formValues.is_active,
      };
      console.log(userData);

      setLoader({ isActive: true });
      await user_service.Details(userData).then((response) => {
        setLoader({ isActive: false });
        setFormValues({
          question: "",
          answer_type: "input",
          answer_options: "",
          input_title: "",
          trans_type: "residential_sale",
          category: "contacts",
          required: "no",
          is_documentRequired: "no",
          is_active: "1",
          agentId: "",
          rows: [],
        });
        Detailsaddeddata();
        if (response) {
        }
      });
    }
  };

  const [detailsaddeddata, setDetailsaddeddata] = useState([]);
  const [requireddetailsaddeddata, setRequiredDetailsaddeddata] = useState([]);

  const Detailsaddeddata = async () => {
    const agentId = jwt(localStorage.getItem("auth")).id;
    await user_service.Detailsaddeddata(agentId).then((response) => {
      setLoader({ isActive: false });
      if (response) {
        const requiredYesArray = response.data.data.filter(
          (item) => item.required === "yes"
        );
        const groupedBycategory = response.data.data.reduce((acc, item) => {
          const category = item.category;
          if (!acc[category]) {
            acc[category] = [];
          }
          acc[category].push(item);
          return acc;
        }, {});

        setRequiredDetailsaddeddata(requiredYesArray);
        setDetailsaddeddata(groupedBycategory);
      }
    });
  };

  const handleRemove = async (id) => {
    console.log(id);
    setLoader({ isActive: true });
    await user_service.Detailsdeletedata(id).then((response) => {
      if (response) {
        setLoader({ isActive: false });
        Detailsaddeddata();
      }
    });
  };
  return (
    <div className="bg-secondary float-left w-100 pt-4 mb-4">
      <Loader isActive={isActive} />
      {isShow && (
        <Toaster
          types={types}
          isShow={isShow}
          toasterBody={toasterBody}
          message={message}
        />
      )}
      <main className="page-wrapper">
        {/* <AllTab /> */}

        {/* <!-- Page container--> */}
        <div className="">
            {/* <NavLink className="btn btn-primary pull-right" onClick={handleSubmit}>Save Item</NavLink> */}
          <div className="float-left w-100">
            <div className="bg-light rounded-3 border p-3 mb-3">
              <div className="col-md-12">
              <div className="">
                <label className="col-form-label">Question:</label>
                <input
                  className="form-control"
                  id="inline-form-input"
                  type="text"
                  name="question"
                  onChange={handleInputChange}
                  value={formValues.question}
                />
                {formErrors.question && (
                  <div className="invalid-tooltip">{formErrors.question}</div>
                )}
              </div>
             </div>
              <div className="">
                <div className="col-md-12">
                <label className="col-form-label">Answer Type:</label>
                <select
                  className="form-select"
                  id="pr-city"
                  name="answer_type"
                  onChange={handleInputChange}
                  value={formValues.answer_type}
                >
                  <option></option>
                  <option value="input">Input</option>
                  <option value="Date">Date</option>
                  <option value="Yes/No">Yes/No</option>
                  <option value="Select">Select</option>
                </select>

                {/* {(formValues.answer_type === 'Yes/No' || formValues.answer_type === 'Select') && (
                                    <div>
                                       
                                        {formValues.rows.map((row, index) => (
                                            <input
                                            key={index}
                                            className="form-control mt-2"
                                            type="text"
                                            value={row}
                                            // onChange={handleInputChange}
                                            onChange={(e) => handleRowChange(e, index)}
                                            />
                                            ))}
                                        
                                    </div>
                                )} */}

                {(formValues.answer_type === "Yes/No" ||
                  formValues.answer_type === "Select") && (
                    <div>
                      {formValues.rows.map((row, index) => (
                        <input
                          key={index}
                          className="form-control mt-2"
                          type="text"
                          value={row}
                          onChange={(e) => handleRowChange(e, index)}
                        />
                      ))}

                      {formErrors.answer_type && (
                        <div className="invalid-tooltip">
                          {formErrors.answer_type}
                        </div>
                      )}
                    </div>
                  )}

                {(formValues.answer_type === "Yes/No" ||
                  formValues.answer_type === "Select") && (
                    <button
                      className="btn btn-primary pull-right"
                      onClick={handleAddRow}
                    >
                      +
                    </button>
                  )}
                  </div>
              </div>
              <div className="col-md-12">
                <div className="">
                  <label className="col-form-label">Input Title:</label>
                  <input
                    className="form-control"
                    id="inline-form-input"
                    type="text"
                    name="input_title"
                    onChange={handleInputChange}
                    value={formValues.input_title}
                  />
                  {formErrors.input_title && (
                    <div className="invalid-tooltip">
                      {formErrors.input_title}
                    </div>
                  )}
                </div>
              </div>
              <div className="col-md-12">
                <div className="">
                  <label className="col-form-label">Type of Transaction:</label>
                  <select
                    className="form-select"
                    id="pr-city"
                    name="trans_type"
                    onChange={handleInputChange}
                    value={formValues.trans_type}
                  >
                    <option></option>
                    {/* <option value="residential_sale">Residential lease</option>
                    <option value="residential_lease">Residential Sale</option>
                    <option value="commercial_sale">Commercial Sale</option>
                    <option value="commercial_lease">Commercial Lease</option>
                    <option value="apartment_lease">Apartment Lease</option>
                    <option value="referred">Referred</option> */}


                    <option value="buy_residential">Buyer Residential</option>
                    <option value="buy_commercial">Buyer Commercial</option>
                    <option value="buy_investment">Buyer Investment</option>
                    <option value="buy_land">Buyer Land</option>
                    <option value="buy_referral">Buyer Referral</option>
                    <option value="buy_lease">Buyer Lease</option>

                    <option value="sale_residential">Seller Residential</option>
                    <option value="sale_commercial">Seller Commercial</option>
                    <option value="sale_investment">Seller Investment</option>
                    <option value="sale_referral">Seller Referral</option>
                    <option value="sale_lease">Seller Lease</option>

                    <option value="both_residential">Buyer & Seller Residential</option>
                    <option value="both_commercial">Buyer & Seller Commercial</option>
                    <option value="both_investment">Buyer & Seller Investment</option>
                    <option value="both_land">Buyer & Seller Land</option>
                    <option value="both_referral">Buyer & Seller Referral</option>
                    <option value="both_lease">Buyer & Seller Lease</option>

                    <option value="referral_residential">Referral Residential</option>
                    <option value="referral_commercial">Referral Commercial</option>
                    <option value="referral_investment">Referral Investment</option>
                    <option value="referral_land">Referral Land</option>
                    <option value="referral_lease">Referral Lease</option>

                  </select>
                </div>
              </div>
              <div className="col-md-12">
                <div className="">
                  <label className="col-form-label">Category:</label>
                  <select
                    className="form-select"
                    id="pr-city"
                    name="category"
                    onChange={handleInputChange}
                    value={formValues.category}
                  >
                    <option></option>
                    <option value="contacts">Contacts</option>
                    <option value="property">Property</option>
                    <option value="financials">Financials</option>
                    <option value="dates">Dates</option>
                  </select>
                </div>
              </div>
              <div className="col-md-12">
                <div className="">
                  <label className="col-form-label">Is this item required:</label>
                  <div className="form-check mb-0">
                    <input
                      className="form-check-input"
                      id="form-check-1"
                      type="checkbox"
                      name="required"
                      onChange={handleInputChange}
                      value="yes"
                    />
                    <label className="form-check-label" id="radio-level1">
                      Yes, this item required
                    </label>
                  </div>
                </div>
              </div>
              <div className="col-md-12">
                <div className="">
                  <label className="col-form-label">Active Status:</label>
                  <select
                    className="form-select"
                    id="pr-city"
                    name="is_active"
                    onChange={handleInputChange}
                    value={formValues.is_active}
                  >
                    <option value="1">Active</option>
                    <option value="0">inActive</option>
                  </select>
                </div>
              </div>
              <div className="col-md-12">
                <div className="">
                  <label className="col-form-label">Is this for document is required:</label>
                  <div className="form-check">
                    <input
                      className="form-check-input"
                      id="form-check-3"
                      type="checkbox"
                      name="is_documentRequired"
                      onChange={handleInputChange}
                      value="yes"
                    />
                    <label className="form-check-label" id="radio-level3">
                      Yes, this item required
                    </label>
                  </div>
                </div>
              </div>
            </div>
            <div className="float-start w-100 mt-3">
              <NavLink
                className="btn btn-primary pull-right"
                onClick={handleSubmit}
              >
                Save Item
              </NavLink>
            </div>
            </div>
        
          

          <div className="float-start w-100 transaction_property mt-4">
            <div className="float-start w-100 mb-4">
              <h3 className="text-white mb-0">Details Rules</h3>
              </div>
              <div className="float-left w-100 accordion mt-2" id="accordionExample4">
                {requireddetailsaddeddata.length > 0 ? (
                  <div className="accordion-item">
                    <h2 className="accordion-header" id="headingRequired">
                      <button
                        className="accordion-button"
                        type="button"
                        data-bs-toggle="collapse"
                        data-bs-target="#collapseRequired"
                        aria-expanded="true"
                        aria-controls="collapseRequired"
                      >
                        {requireddetailsaddeddata.length} Items Required Now
                      </button>
                    </h2>
                    {requireddetailsaddeddata.map((item) => {
                      var answerresp = "";
                      if (
                        item.transactionDetails &&
                        item.transactionDetails.answer
                      ) {
                        answerresp = item.transactionDetails.answer;
                      }
                      return (
                        <div
                          className="accordion-collapse collapse show"
                          aria-labelledby="headingRequired"
                          data-bs-parent="#accordionExample"
                          id="collapseRequired"
                          key={item._id}
                        >
                          <div className="accordion-body">
                            {item.question} &nbsp; &nbsp;
                            {/* {item.transactionDetails && item.transactionDetails.answer
                                                                ? 
                                                                <span className="" >{ item.transactionDetails.answer}</span>
                                                                : <span className=""> Not Set</span>
                                                                } */}
                            <span style={{ color: "green" }}>
                              {item.answer_type}
                            </span>
                            &nbsp;
                            <span className="SuppressText">
                              <img
                                onClick={(e) => handleRemove(item._id)}
                                className="pull-center"
                                src="https://cdn.pboffice.net/shared/transaction/page_select_drop.png"
                              />
                            </span>
                            {/* <NavLink
                                                                onClick={(e) => editInput(e, item._id)}
                                                                id={`editanswer${item._id}`}
                                                            >
                                                                edit
                                                            </NavLink> */}
                            {/* {console.log(item.answer_type)} */}
                            <div
                              className="col-sm-6 mb-4 d-none"
                              id={`propdetails${item._id}`}
                            >
                              {item.answer_type === "input" && (
                                <>
                                  <hr className="mt-3 w-100" />
                                  <p>{item.input_title}</p>
                                  {/* <input
                                                                            className="form-control"
                                                                            type="text"
                                                                            id="inline-form-input"
                                                                            name='answer'
                                                                            value={propertyUpdate.answer ? propertyUpdate.answer : answerresp}
                                                                            onChange={handleChange}
                                                                            data-datepicker-options='{"altInput": true, "altFormat": "F j, Y", "dateFormat": "Y-m-d"}'
                                                                        /> */}
                                </>
                              )}

                              {item.answer_type === "Select" &&
                                JSON.parse(item.answer_options) && (
                                  <>
                                    <hr className="mt-3 w-100" />
                                    <p>{item.input_title}</p>
                                    {/* <select

                                                                            className="form-select"
                                                                            id="pr-city"
                                                                            name='answer'
                                                                            value={propertyUpdate.answer ? propertyUpdate.answer : answerresp}
                                                                            onChange={handleChange}
                                                                        >
                                                                            {JSON.parse(item.answer_options).map((option, index) => (
                                                                                <>
                                                                                    <option key={index} value={option}>{option}</option>

                                                                                </>

                                                                            ))}
                                                                        </select> */}
                                  </>
                                )}

                              {item.answer_type === "Yes/No" &&
                                JSON.parse(item.answer_options) && (
                                  <>
                                    <hr className="mt-3 w-100" />
                                    <p>{item.input_title}</p>
                                    {/* <select

                                                                            className="form-select"
                                                                            id="pr-city"
                                                                            name='answer'
                                                                            value={propertyUpdate.answer ? propertyUpdate.answer : answerresp}
                                                                            onChange={handleChange}

                                                                        >
                                                                            {JSON.parse(item.answer_options).map((option, index) => (
                                                                                <>  
                                                                                    <option key={index} value={option}>{option}</option>

                                                                                </>

                                                                            ))}
                                                                        </select> */}
                                  </>
                                )}

                              {item.answer_type === "Date" && (
                                <>
                                  <hr className="mt-3 w-100" />
                                  <p>{item.input_title}</p>
                                  <label
                                    className="form-label"
                                    htmlFor="pr-birth-date"
                                  >
                                    Start Date
                                  </label>
                                  {/* <input
                                                                            className="form-control"
                                                                            type="date"
                                                                            id="inline-form-input"
                                                                            name='answer'
                                                                            value={propertyUpdate.answer ? propertyUpdate.answer : answerresp}
                                                                            onChange={handleChange}
                                                                            data-datepicker-options='{"altInput": true, "altFormat": "F j, Y", "dateFormat": "Y-m-d"}'
                                                                        /> */}
                                </>
                              )}
                              {/* <button
                                                                    className="btn btn-primary btn-sm pull-right"
                                                                    onClick={()=>saveDate(item._id)}
                                                                >
                                                                    Save
                                                                </button> */}
                            </div>
                          </div>
                        </div>
                      );
                    })}
                  </div>
                ) : (
                  ""
                )}

                {Object.entries(detailsaddeddata).map(([category, items]) => (
                  <div className="accordion-item" key={category}>
                    <h2 className="accordion-header" id={`heading${category}`}>
                      <button
                        className="accordion-button"
                        type="button"
                        data-bs-toggle="collapse"
                        data-bs-target={`#collapse${category}`}
                        aria-expanded="true"
                        aria-controls={`collapse${category}`}
                      >
                        {category.charAt(0).toUpperCase() + category.slice(1)}
                      </button>
                    </h2>
                    <div
                      className="accordion-collapse collapse show"
                      aria-labelledby={`heading${category}`}
                      data-bs-parent="#accordionExample"
                      id={`collapse${category}`}
                    >
                      {items.map((item) => (
                        <>
                          <div className="accordion-body" key={item._id}>
                            {item.question} &nbsp; &nbsp;
                            {/* {item.transactionDetails && item.transactionDetails.answer
                                                    ? 
                                                    <span className="" style={{color:  "green"}}>{ item.transactionDetails.answer}</span>
                                                    : <span className=""> Not Set</span>
                                                } */}
                            <span style={{ color: "green" }}>
                              {item.answer_type}
                            </span>
                            &nbsp;
                            <span className="SuppressText">
                              <img
                                onClick={(e) => handleRemove(item._id)}
                                className="pull-center"
                                src="https://cdn.pboffice.net/shared/transaction/page_select_drop.png"
                              />
                            </span>
                            {/* <NavLink
                                                    onClick={(e) => editInputProp(e, item._id)}
                                                    id={`editanswerprop${item._id}`}
                                                >
                                                    edit
                                                </NavLink> */}
                            {/* {console.log(item.answer_type)} */}
                            <div
                              className="col-sm-6 mb-4 d-none"
                              id={`propertydetails${item._id}`}
                            >
                              {item.answer_type === "input" && (
                                <>
                                  <hr className="mt-3 w-100" />
                                  <p>{item.input_title}</p>
                                  {/* <input
                                                                className="form-control"
                                                                type="text"
                                                                id="inline-form-input"
                                                                name='answer'
                                                                value={propertyUpdate.answer}
                                                                onChange={handleChange}
                                                                data-datepicker-options='{"altInput": true, "altFormat": "F j, Y", "dateFormat": "Y-m-d"}'
                                                            /> */}
                                </>
                              )}

                              {item.answer_type === "Select" &&
                                JSON.parse(item.answer_options) && (
                                  <>
                                    <hr className="mt-3 w-100" />
                                    <p>{item.input_title}</p>
                                    {/* <select

                                                                className="form-select"
                                                                id="pr-city"
                                                                name='answer'
                                                                value={propertyUpdate.answer}
                                                                onChange={handleChange}
                                                            >
                                                                {JSON.parse(item.answer_options).map((option, index) => (
                                                                    <>
                                                                        <option key={index} value={option}>{option}</option>

                                                                    </>

                                                                ))}
                                                            </select> */}
                                  </>
                                )}

                              {item.answer_type === "Yes/No" &&
                                JSON.parse(item.answer_options) && (
                                  <>
                                    <hr className="mt-3 w-100" />
                                    <p>{item.input_title}</p>
                                    {/* <select

                                                                className="form-select"
                                                                id="pr-city"
                                                                name='answer'
                                                                value={propertyUpdate.answer}
                                                                onChange={handleChange}
                                                            >
                                                                {JSON.parse(item.answer_options).map((option, index) => (
                                                                    <>
                                                                        <option key={index} value={option}>{option}</option>

                                                                    </>

                                                                ))}
                                                            </select> */}
                                  </>
                                )}

                              {item.answer_type === "Date" && (
                                <>
                                  <hr className="mt-3 w-100" />
                                  <p>{item.input_title}</p>
                                  <label
                                    className="form-label"
                                    htmlFor="pr-birth-date"
                                  >
                                    Start Date
                                  </label>
                                  {/* <input
                                                                className="form-control"
                                                                type="date"
                                                                id="inline-form-input"
                                                                name='answer'
                                                                value={propertyUpdate.answer}
                                                                            onChange={handleChange}
                                                                data-datepicker-options='{"altInput": true, "altFormat": "F j, Y", "dateFormat": "Y-m-d"}'
                                                            /> */}
                                </>
                              )}
                              {/* <button
                                                        className="btn btn-primary btn-sm pull-right"
                                                        onClick={()=>saveDate(item._id)}
                                                    >
                                                        Save
                                                    </button> */}
                            </div>
                          </div>
                        </>
                      ))}
                    </div>
                  </div>
                ))}
              </div>

          </div>
        </div>
      </main>
     
    </div>
  );
};

export default Details;

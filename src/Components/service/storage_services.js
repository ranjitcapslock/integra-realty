export const getFromStorage = (key) => {
    const data = (localStorage.getItem(key));
    if (data) {
      return data;
    } else {
      return undefined;
    }
  };
  
  export const isExistInStorage = (key) => {
    // if (JSON.parse(localStorage.getItem(key))) {
      if (localStorage.getItem(key)) {
      return true;
    } else {
      return false;
    }
  };
  
  export const authenticateUser = () => {
    return isExistInStorage("auth");
  };
  
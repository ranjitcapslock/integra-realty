import http from "./http_common";
import jwt from "jwt-decode";

class user_service {
  GetUserId() {
    const token = localStorage.getItem("auth");
    const user = jwt(token);
    let id = parseInt(user.UniqueId);

    return id;
  }

  async signup(data) {
    return await http.post(`/user/signup`, data);
  }
  async signin(data) {
    return await http.post(`/user/login`, data);
  }

  async contactLoginstatus() {
    return await http.get(`/contacts/newassociates-login`);
  }

  async contactBuyerAggrement() {
    return await http.get(`/contacts/buyer-broker-Aggrement`);
  }

  async TransactionstatsReport(page) {
    return await http.get(`/transactions/stats${page}`);
  }

  async TransactionBussinessReport(page) {
    return await http.get(`/transactions/bussiness${page}`);
  }

  async TransactionBussinessExpences(page) {
    return await http.get(`/transactions/bussiness-expences${page}`);
  }

  async TransactionDelete(id) {
    return await http.delete(`/transactions/${id}`);
  }

  async Testmoinals(id) {
    return await http.get(`/transactions/transactionemail/${id}`);
  }

  async TestmoinalsEmail(id) {
    return await http.get(`/testimonials/testimonialsEmail/${id}`);
  }

  async PropertyLanding(id) {
    return await http.get(`/transactions/transactionPropertyLanding/${id}`);
  }

  async TransactionGetMy(page, id) {
    return await http.get(
      `/transactions?page=${page}&limit=1000&agentId=${id}`
    );
  }

  async TransactionGetReport(page) {
    return await http.get(
      `/transactions/getAllForReport?page=${page}&limit=1000`
    );
  }

  async transactionGetById(id) {
    return await http.get(`/transactions/${id}`);
  }

  async transactionNotesByTid(page, id) {
    return await http.get(`/transactionNotes?page=${page}&transactionId=${id}`);
  }

  async transactionNotesById(id) {
    return await http.get(`/transactionNotes/${id}`);
  }

  async transactionNotes(data) {
    return await http.post(`/transactionNotes`, data);
  }

  async homePagePhoto(data) {
    return await http.post(`/homePhoto`, data);
  }

  async homePagePhotoUpdate(id, data) {
    return await http.put(`/homePhoto/${id}`, data);
  }

  async homePagePhotoGet() {
    return await http.get(`/homePhoto`);
  }

  async homePagePhotoDelete(id) {
    return await http.delete(`/homePhoto/${id}`);
  }

  async TransactionCreate(data) {
    return await http.post(`/transactions`, data);
  }

  async TransactionUpdate(id, data) {
    return await http.put(`/transactions/${id}`, data);
  }

  async TransactionemailUpdate(id) {
    return await http.put(`/transactions/updateuniqueemail/${id}`);
  }

  async TransactionGet(page, searchdata = "") {
    return await http.get(`/transactions${page}&searchdata=${searchdata}`);
  }

  async TestimonialsCreate(data) {
    return await http.post(`/testimonials`, data);
  }

  async TestimonialsGet(clientId) {
    return await http.get(`/testimonials?clientId=${clientId}`);
  }

  async TestimonialsGetAll(page) {
    return await http.get(`/testimonials/agent-testimonials?page=${page}`);
  }

  async TestimonialsGetId(id) {
    return await http.get(`/testimonials/${id}`);
  }
  async SearchContactGet(page, limit, firstName) {
    return await http.get(
      `/contacts?page=${page}&limit=${limit}&firstName=${firstName}`
    );
  }
  async SearchContactfilter(page, type, searchvalue = "") {
    return await http.get(
      `/contacts?page=${page}&contactType=${type}&firstName=${searchvalue}`
    );
  }

  async SearchContactfilterAssociate(
    page,
    type,
    contact_status,
    searchvalue = ""
  ) {
    return await http.get(
      `/contacts?page=${page}&contactType=${type}&contact_status=${contact_status}&firstName=${searchvalue}`
    );
  }

  async SearchContactEmail(type) {
    return await http.get(`/contacts?contactType=${type}`);
  }

  async SearchInapprovalassociates(page) {
    return await http.get(
      `/contacts?page=${page}&contact_status=inapproval&onboardfinal=done`
    );
  }

  async SearchNotcompleteassociates(page) {
    return await http.get(
      `/contacts?page=${page}&contact_status=inapproval&onboardfinal=`
    );
  }

  async Searchuiversalfilter(searchvalue) {
    return await http.get(
      `/contacts/searchuiversalfilter?searchkey=${searchvalue}`
    );
  }

  async getContactListings(agentId, type = "count") {
    return await http.get(
      `/contacts/getContactListings?type=${type}&contactId=${agentId}`
    );
  }

  async contactGet(page) {
    return await http.get(`/contacts?page=${page}`);
  }

  async contactGetassoc(page, contactType) {
    return await http.get(`/contacts?page=${page}&contactType=${contactType}`);
  }

  async contactGetassocLastLogin(page, lastlogin) {
    return await http.get(
      `/contacts?page=${page}&lastlogin=${lastlogin.toString()}`
    );
  }

  async contactGetassocDeleted(page, contact_status) {
    return await http.get(
      `/contacts?page=${page}&contact_status=${contact_status}`
    );
  }

  // async contactGetassocActive(page,contact_status,contactType) {
  //   return await http.get(`/contacts?page=${page}&contact_status=${contact_status}&contactType=${contactType}`);
  // }

  async contactUpdate(id, data) {
    return await http.put(`/contacts/${id}`, data);
  }

  async contact(data, firstName) {
    return await http.post(`/contacts?firstName=${firstName}`, data);
  }

  async resetpasswordlink(data) {
    return await http.post(`/contacts/resetpasswordlink`, data);
  }

  async onboardlink(data) {
    return await http.post(`/contacts/onboarding`, data);
  }

  async onboardlinkVendor(data) {
    return await http.post(`/contacts/onboarding-vendor`, data);
  }

  async onboardlinkresubmit(data) {
    return await http.post(`/contacts/onboardingresubmit`, data);
  }

  async resetpassword(data) {
    return await http.post(`/contacts/resetpassword`, data);
  }
  async invitecontact(data) {
    return await http.post(`/contacts/invitecontact`, data);
  }

  async sendDocumentMail(data) {
    return await http.post(`/transactionDocument/sendDocumentMail`, data);
  }

  async sendOnBoardMail(data) {
    return await http.post(`/contacts/sendOnboardMail`, data);
  }

  // async noticecontactGetAgentID(queryParams) {
  //   return await http.get(`/notice${queryParams}`);
  // }
  async noticecontactGet() {
    return await http.get(`/notice`);
  }

  async noticecontactGetAll() {
    return await http.get(`/notice/getAll`);
  }
  async noticecontact(data) {
    return await http.post(`/notice`, data);
  }

  async noticeBirthday(data) {
    return await http.post(`/notice/inviteBirthday`, data);
  }

  async birthdayNotification(data) {
    return await http.post(`/notice/birthday-Notification`, data);
  }

  async noticeAdmin(data) {
    return await http.post(`/notice/adminNotice`, data);
  }

  async noticegetall(id, isview) {
    return await http.get(`/notice?receiverId=${id}&isview=${isview}`);
  }
  async noticegetallconditional(query = "") {
    return await http.get(`/notice${query}`);
  }
  async noticeUpdate(id, data) {
    return await http.put(`/notice/${id}`, data);
  }

  async paperworkAdd(data) {
    return await http.post(`/agentpapernameAdd`, data);
  }
  async paperworkGet() {
    return await http.get(`/agentpapernameAdd`);
  }

  async paperworkGetId(id) {
    return await http.get(`/agentpapernameAdd/${id}`);
  }

  async paperworkUpdate(id, data) {
    return await http.put(`/agentpapernameAdd/${id}`, data);
  }

  async paperworkDelete(id) {
    return await http.delete(`/agentpapernameAdd/${id}`);
  }

  async officeDelete(id) {
    return await http.delete(`/agentNotes/${id}`);
  }
  async officeNote(data) {
    return await http.post(`/agentNotes`, data);
  }
  async officeNote(data) {
    return await http.post(`/agentNotes`, data);
  }

  async officeNoteUpdate(id, data) {
    return await http.put(`/agentNotes/${id}`, data);
  }

  async officeNoteGet(agentId) {
    return await http.get(`/agentNotes?agentId=${agentId}`);
  }

  async officeNoteGetId(id) {
    return await http.get(`/agentNotes/${id}`);
  }

  async agentAccountingDelete(id) {
    return await http.delete(`/agentAccounts/${id}`);
  }

  async agentAccounting(data) {
    return await http.post(`/agentAccounts`, data);
  }

  async agentAccountingUpdate(id, data) {
    return await http.put(`/agentAccounts/${id}`, data);
  }

  async agentAccountingGet() {
    return await http.get(`/agentAccounts`);
  }
  async agentAccountingGetId(id) {
    return await http.get(`/agentAccounts/${id}`);
  }

  async Details(data) {
    return await http.post(`/detailsAdd`, data);
  }

  async profileGet(id) {
    return await http.get(`/user/${id}`);
  }
  async profilePost(data, id) {
    return await http.put(`/user/${id}`, data);
  }

  async PropertyPost(data) {
    return await http.put(`/detailsAdd`, data);
  }

  async Quotesget() {
    return await http.get(`/posts/quotes`);
  }

  async postGet(page) {
    return await http.get(`/posts?page=${page}`);
  }
  async IndustrypostGet(page) {
    return await http.get(`/industryposts?page=${page}`);
  }

  async postDetail(id) {
    return await http.get(`/posts/${id}`);
  }
  async postFilter(category = "") {
    return await http.get(`/posts?category=${category}`);
  }

  async postCreate(data) {
    return await http.post(`/posts`, data);
  }

  async postUpdate(id, data) {
    return await http.put(`/posts/${id}`, data);
  }

  async postDelete(id) {
    return await http.delete(`/posts/${id}`);
  }

  async listingsGet(queryParams) {
    return await http.get(`/listings${queryParams}`);
  }

  async listingsGetMls() {
    return await http.get(`/listings`);
  }

  async listingUpdate(id, data) {
    return await http.put(`/listings/${id}`, data);
  }

  async listingSearch(page, limit, associateName) {
    return await http.get(
      `/listings?page=${page}&limit=${limit}&associateName=${associateName}`
    );
  }

  // async listingSearchMLS(mlsnumber) {
  //   return await http.get(`/listings/searchmls/${mlsnumber}`);
  // }

  // async listingSearchMLSspark(mlsnumber, plantype="") {
  //   return await http.get(`/listings/searchmlsspark/${mlsnumber}/${plantype}`);
  // }

  async listingSearchMLS(mlsnumber) {
    return await http.get(`/listings/searchmls/${mlsnumber}`); // Use the proxy path
  }

  async listingSearchMLSspark(mlsnumber, plantype = "") {
    return await http.get(`/listings/searchmlsspark/${mlsnumber}/${plantype}`); // Use the proxy path
  }
  async listingSearchBedRoom(bedroom) {
    return await http.get(`/listings/searchBedroom/${bedroom}`);
  }

  // async listingSearchBedRoom(queryParams="") {
  //   return await http.get(`/listings/searchBedroom${queryParams}`);
  // }

  async listingSearchMLSagentid(agent_mlsid, type) {
    return await http.get(`/listings/listingbyagentid/${agent_mlsid}/${type}`);
  }
  async listingSearchMLSagentidspark(agent_mlsid, type, plantype = "") {
    return await http.get(
      `/listings/listingbyagentidspark/${agent_mlsid}/${type}/${plantype}`
    );
  }

  async listingsGetById(id) {
    return await http.get(`/listings/${id}`);
  }
  async listingsCreate(data) {
    return await http.post(`/listings`, data);
  }

  async transactionPropertyGet() {
    return await http.get(`/transactionProperty`);
  }

  async transactionPropertyGetById(id) {
    return await http.get(`/transactionProperty/${id}`);
  }

  async transactionProperty(data) {
    return await http.post(`/transactionProperty`, data);
  }

  async transactionPropertyDetails(data) {
    return await http.post(`/transactionDetails`, data);
  }

  async transactionPropertyDetailsGet() {
    return await http.get(`/transactionDetails`);
  }
  async transactionPropertyUpdate(id, data) {
    return await http.put(`/transactionDetails/${id}`, data);
  }
  async transactionPropertyDelete(id) {
    return await http.delete(`/transactionDetails/${id}`);
  }
  async transactionDepositAdd(data) {
    return await http.post(`/transactionDeposits`, data);
  }
  async transactionDepositUpdate(id, data) {
    return await http.put(`/transactionDeposits/${id}`, data);
  }

  async transactionDepositGet(id) {
    return await http.get(`/transactionDeposits?transactionId=${id}`);
  }

  async transactionDepositGetId(id) {
    return await http.get(`/transactionDeposits/${id}`);
  }

  async transactionDepositDelete(id) {
    return await http.delete(`/transactionDeposits/${id}`);
  }
  async transactionDocumentId(id) {
    return await http.get(`/transactionDocument/${id}`);
  }
  async documentById() {
    return await http.get(`/transactionDocument`);
  }
  async transactionDocumentGet(id) {
    return await http.get(`/transactionDocument?transactionId=${id}`);
  }

  async transactionDocumentUpdate(id, data) {
    return await http.put(`/transactionDocument/${id}`, data);
  }

  async transactionDocumentDelete(id) {
    return await http.delete(`/transactionDocument/${id}`);
  }

  async transactionDocumentallUpdate(id, data) {
    return await http.put(
      `/transactionDocument/updatealldocuments/${id}`,
      data
    );
  }

  async transactionDocumentGetId(id) {
    return await http.get(`/transactionDocument/${id}`);
  }
  async uploadDocuments(data) {
    return await http.post(`/transactionDocument`, data);
  }

  async updateDocuments(id, data) {
    return await http.put(`/transactionDocument/${id}`, data);
  }
  async AdddocumentRule(data) {
    return await http.post(`/rule`, data);
  }

  async documentupdateRule(id, data) {
    return await http.put(`/rule/${id}`, data);
  }
  async DocumentRules(id) {
    return await http.get(`/rule?page=1&limit=1000`);
  }

  async PlatformDocuments() {
    return await http.get(`/document?page=1&limit=1000`);
  }
  async AddPlatformDocuments(data) {
    return await http.post(`/document`, data);
  }

  async expensesGet(id) {
    return await http.get(`/expenses?transactionId=${id}`);
  }

  async typeExpensesGetId(id) {
    return await http.get(`/expenses/${id}`);
  }

  async expenceDelete(id) {
    return await http.delete(`/expenses/${id}`);
  }

  async expensesCreate(data) {
    return await http.post(`/expenses`, data);
  }

  async expensesCreateUpdate(id, data) {
    return await http.put(`/expenses/${id}`, data);
  }
  async bannerDelete(id) {
    return await http.delete(`/banners/${id}`);
  }

  async bannerPost(data) {
    return await http.post(`/banners`, data);
  }

  async bannerGetById(id) {
    return await http.get(`/banners/${id}`);
  }

  async bannerGet() {
    return await http.get(`/banners`);
  }

  async bannerUpdate(id, data) {
    return await http.put(`/banners/${id}`, data);
  }

  async ShareDocGet() {
    return await http.get(`/officesharedDocSection`);
  }

  async ShareDocGetById(id) {
    return await http.get(`/officesharedDocSection/${id}`);
  }
  async ShareDocDelete(id) {
    return await http.delete(`/officesharedDocSection/${id}`);
  }

  async ShareDocPost(data) {
    return await http.post(`/officesharedDocSection`, data);
  }

  async StaffRoleGet() {
    return await http.get(`/staff-rule`);
  }

  async StaffRolePost(data) {
    return await http.post(`/staff-rule`, data);
  }

  async StaffRoleDelete(id) {
    return await http.delete(`/staff-rule/${id}`);
  }

  async ShareDocPostById(id, data) {
    return await http.put(`/officesharedDocSection/${id}`, data);
  }

  async OfficesharedDocContentDelete(id) {
    return await http.delete(`/officesharedDocContent/${id}`);
  }

  async OfficesharedDocContentPost(data) {
    return await http.post(`/officesharedDocContent`, data);
  }

  async OfficesharedDocContentUpdate(id, data) {
    return await http.put(`/officesharedDocContent/${id}`, data);
  }

  async OfficesharedDocContenGet(id) {
    return await http.get(`/officesharedDocContent/${id}`);
  }

  async OfficesharedDocContetDelete(id) {
    return await http.delete(`/officesharedDocContent/${id}`);
  }

  async memberPlanDelete(id) {
    return await http.delete(`/membershipPlans/${id}`);
  }

  async memberPlanUpdate(id, data) {
    return await http.put(`/membershipPlans/${id}`, data);
  }

  async membershipPlanPost(data) {
    return await http.post(`/membershipPlans`, data);
  }

  async membershipPlanGet() {
    return await http.get(`/membershipPlans`);
  }

  async membershipPlanGetById(id) {
    return await http.get(`/membershipPlans/${id}`);
  }

  async contactGetById(id) {
    return await http.get(`/contacts/${id}`);
  }

  async contactDelete(id) {
    return await http.delete(`/contacts/${id}`);
  }

  async contactGetByNickname(nickname) {
    return await http.get(`/contacts/nickname?nickname=${nickname}`);
  }

  async ContactGetByuniqueid(uid = "", cid = "") {
    return await http.get(`/contacts/contactbyuniqueid/${uid}/${cid}`);
  }

  async CalenderGet(page = "1") {
    return await http.get(`/calender?page=${page}`);
  }

  async MarketCalenderGet() {
    return await http.get(`/market-calender`);
  }

  async MarketCalenderPost(data) {
    return await http.post(`/market-calender`, data);
  }

  async MarketCalenderUpdate(id,data) {
    return await http.put(`/market-calender/${id}`, data);
  }


  async MarketCalenderDelete(id) {
    return await http.delete(`/market-calender/${id}`);
  }

  async MarketCalenderGetId(id) {
    return await http.get(`/market-calender/${id}`);
  }

  async TransactionCalenderGet(page, category = "") {
    return await http.get(`/calender/transaction`);
  }

  async CalenderSubscriptionURL(page = "1") {
    return await http.get(`/calender/subscriptions?page=${page}`);
  }

  async CalenderPost(data) {
    return await http.post(`/calender`, data);
  }

  async CalenderUpdate(id, data) {
    return await http.put(`/calender/${id}`, data);
  }

  async CalenderGetById(id) {
    return await http.get(`/calender/${id}`);
  }

  async calenderDelete(id) {
    return await http.delete(`/calender/${id}`);
  }

  async calenderSearch(title) {
    return await http.get(`/calender?title=${title}&page=1&limit=1000`);
  }

  async Detailsaddeddata(agentId = "", transactionId = "") {
    return await http.get(
      `/detailsAdd?agentId=${agentId}&transactionId=${transactionId}`
    );
  }

  async DetailsdatagetId(id) {
    return await http.get(`/detailsAdd/${id}`);
  }

  async Detailsdeletedata(id) {
    return await http.delete(`/detailsAdd/${id}`);
  }

  async AgentpapernameAdd() {
    return await http.get(`/agentpapernameAdd`);
  }

  async agentpaperworkDocument(data) {
    return await http.post(`/agentpaperworkDocument`, data);
  }

  async agentpaperworkDocumentGet() {
    return await http.get(`/agentpaperworkDocument`);
  }

  // async agentpaperworkDocumentgetId(additional_agentId,documentType) {
  //   return await http.get(`/agentpaperworkDocument/getByAgentPaperwork?additional_agentId=${additional_agentId}&documentType=${documentType}`);
  // }
  async agentpaperworkDocumentUpdate(id, data) {
    return await http.put(`/agentpaperworkDocument/${id}`, data);
  }

  async depositLedgerAllGet() {
    return await http.get(`/transactionDeposits/depositledger`);
  }

  async expenseLedgerAllGet(page) {
    return await http.get(`/expenses/expenseledger${page}`);
  }

  async notesAllGet(type) {
    return await http.get(`/transactionNotes/notesall?note_type=${type}`);
  }

  async bannerGetAll() {
    return await http.get(`/banners/homebanners`);
  }

  async increaseclickbanner(id) {
    return await http.get(`/banners/increaseclickbanner/${id}`);
  }

  async increaseviewsbanner(id) {
    return await http.get(`/banners/increaseviewsbanner/${id}`);
  }

  async themecolorsPost(data) {
    return await http.post(`/themecolors`, data);
  }

  async themecolorsUpdate(id, data) {
    return await http.put(`/themecolors/${id}`, data);
  }

  async themecolorsGet() {
    return await http.get(`/themecolors`);
  }

  async AddCommentPost(data) {
    return await http.post(`/postcomments`, data);
  }

  async AddCommentGet(id) {
    return await http.get(`/postcomments?postId=${id}`);
  }

  async AddCommentDelete(id) {
    return await http.delete(`/postcomments/${id}`);
  }

  async Getrequireddoucmentno(agentId, transactionId) {
    return await http.get(
      `/transactions/getrequireddoucmentno?agentId=${agentId}&transactionId=${transactionId}`
    );
  }

  async transactionFundingRequest(id) {
    return await http.get(`/transactFundingRequests?transactionId=${id}`);
  }

  async transactionFundingDelete(id) {
    return await http.delete(`/transactFundingRequests/${id}`);
  }

  async transactionFundingAll() {
    return await http.get(`/transactFundingRequests/getAllpending`);
  }

  async transactionFundingId(id) {
    return await http.get(`/transactFundingRequests/${id}`);
  }

  async fundingRequestPost(data) {
    return await http.post(`/transactFundingRequests`, data);
  }

  async fundingEmailPost(data) {
    return await http.post(`/transactFundingRequests/fundingemail`, data);
  }

  async fundingRequestUpdate(id, data) {
    return await http.put(`/transactFundingRequests/${id}`, data);
  }

  async fundingRequestGet() {
    return await http.get(`/transactFundingRequests`);
  }

  async Getfilledtransactionsmonthwise() {
    return await http.get(`/transactions/getfilledtransactionsmonthwise`);
  }

  async GetfilledtransactionsYearWise() {
    return await http.get(`/transactions/getfilledtransactionsyeartodatewise`);
  }

  async contactStatusGet(page) {
    return await http.get(`/contacts/contact-status?page=${page}`);
  }

  async documentReviewGet(page) {
    return await http.get(`/transactions/documen-review?page=${page}`);
  }

  async expireListingGet(page) {
    return await http.get(`/listings/expire-listing?page=${page}`);
  }

  async organizationTreePost(data) {
    return await http.post(`/organizationTree`, data);
  }
  async organizationTreeGet(agentId = "") {
    return await http.get(`/organizationTree?agentId=${agentId}`);
  }

  async organizationTreeDelete(id = "") {
    return await http.delete(`/organizationTree/${id}`);
  }
  async organizationTreeGetId(id) {
    return await http.get(`/organizationTree/${id}`);
  }

  async organizationTreeUpdate(id, data) {
    return await http.put(`/organizationTree/${id}`, data);
  }

  async calenderSubscriptionPost(data) {
    return await http.post(`/calenderSubscription/`, data);
  }

  async calenderSubscriptionUpdate(id, data) {
    return await http.put(`/calenderSubscription/${id}`, data);
  }
  async calenderSubscriptionGet(agentId) {
    return await http.get(`/calenderSubscription?agentId=${agentId}`);
  }

  async aboutMePost(data) {
    return await http.post(`/aboutMe/`, data);
  }

  async aboutMeUpdate(id, data) {
    return await http.put(`/aboutMe/${id}`, data);
  }

  async aboutMeGet(agentId) {
    return await http.get(`/aboutMe?agentId=${agentId}`);
  }

  async addVideocategory(data) {
    return await http.post(`/videoCategory`, data);
  }

  async addVideocategoryUpdate(id, data) {
    return await http.put(`/videoCategory/${id}`, data);
  }
  async addVideocategoryGet(type) {
    return await http.get(`/videoCategory?videoCategory=${type}`);
  }
  async videoCategoryId(id) {
    return await http.get(`/videoCategory/${id}`);
  }

  async videoCategoryDelete(id) {
    return await http.delete(`/videoCategory/${id}`);
  }

  async addLearn(data) {
    return await http.post(`/learnMedia`, data);
  }

  async addLearnUpdate(id, data) {
    return await http.put(`/learnMedia/${id}`, data);
  }

  async getLearnbyAgent(agentId, type) {
    return await http.get(`/learnMedia?agentId=${agentId}&playlist=${type}`);
  }
  async getLearnbyID(agentId, type, mediaid) {
    return await http.get(`/learnMedia/${mediaid}`);
  }

  async getLearnId(id) {
    return await http.get(`/learnMedia/${id}`);
  }

  async learnIdvideo(id) {
    return await http.get(`/learnMedia/${id}`);
  }

  async getLearnIdGet(videoCategory) {
    return await http.get(`/learnMedia?videoCategory=${videoCategory}`);
  }
  async learnmediaDelete(id) {
    return await http.delete(`/learnMedia/${id}`);
  }
  async learnmediaUpdate(id, data) {
    return await http.put(`/learnMedia/${id}`, data);
  }

  async PromotoProductsGet(office) {
    return await http.get(`/promotoProducts?office_name=${office}`);
  }
  async Addpromotocatalog(data) {
    return await http.post(`/promotoProducts`, data);
  }

  async subCategoryVendorId(id) {
    return await http.get(`/subCategoryVendor/${id}`);
  }

  async subVendorItemGet(category_id) {
    return await http.get(`/subCategoryVendor?category_id=${category_id}`);
  }


  async subCategoryVendorDelete(id) {
    return await http.delete(`/subCategoryVendor/${id}`);
  }

  async AddSubVendor(data) {
    return await http.post(`/subCategoryVendor`, data);
  }

  async UpdateSubVendor(id, data) {
    return await http.put(`/subCategoryVendor/${id}`, data);
  }

  async UpdatePromotocatalog(id, data) {
    return await http.put(`/promotoProducts/${id}`, data);
  }
  async promotoProductsGetId(id) {
    return await http.get(`/promotoProducts/${id}`);
  }
  async DeletepromotoProducts(id) {
    return await http.delete(`/promotoProducts/${id}`);
  }

  async addProductItems(data) {
    return await http.post(`/productItems`, data);
  }

  async productItemsGet(category_id) {
    return await http.get(`/productItems?category_id=${category_id}`);
  }

  async productItemsVendorGet(subCategoryId) {
    return await http.get(`/productItems?subCategoryId=${subCategoryId}`);
  }

 

  async productItemsDelete(id) {
    return await http.delete(`/productItems/${id}`);
  }
  async productItemsUpdate(id, data) {
    return await http.put(`/productItems/${id}`, data);
  }

  async productItemsGetId(id) {
    return await http.get(`/productItems/${id}`);
  }

  async lastloginContact(page) {
    return await http.get(`/contacts/lastlogin?page=${page}`);
  }

  async associateReportContact() {
    return await http.get(`/contacts/associate-Report`);
  }

  async sponsorReportContact(page) {
    return await http.get(`/contacts/sponsor-associate-report?page=${page}`);
  }

  async homeside(page) {
    return await http.get(`/contacts/homeside?page=${page}`);
  }

  async newassociates(page = 1) {
    return await http.get(`/contacts/newassociates?page=${page}`);
  }

  async platformdetailsPost(data) {
    return await http.post(`/platformdetails`, data);
  }

  async platformdetailsUpdate(id, data) {
    return await http.put(`/platformdetails/${id}`, data);
  }

  async platformdetailsGet() {
    return await http.get(`/platformdetails`);
  }

  async questionsPost(data) {
    return await http.post(`/questions`, data);
  }

  async questionsUpdate(id, data) {
    return await http.put(`/questions/${id}`, data);
  }

  async questionsGet(query = "") {
    return await http.get(`/questions${query}`);
  }

  async questionsGetId(id) {
    return await http.get(`/questions/${id}`);
  }

  async questionDelete(id) {
    return await http.delete(`/questions/${id}`);
  }

  async linksPost(data) {
    return await http.post(`/links`, data);
  }

  async linksUpdate(id, data) {
    return await http.put(`/links/${id}`, data);
  }

  async linksGet(query = "") {
    return await http.get(`/links${query}`);
  }

  async linksGetId(id) {
    return await http.get(`/links/${id}`);
  }

  async linkDelete(id) {
    return await http.delete(`/links/${id}`);
  }

  async BusinessCardGetId(id) {
    return await http.get(`/businesscard/${id}`);
  }

  async SuggestFeature(data) {
    return await http.post(`/suggestFeature`, data);
  }

  async SuggestFeatureGet() {
    return await http.get(`/suggestFeature`);
  }

  async SuggestFeatureGetId(id) {
    return await http.get(`/suggestFeature/${id}`);
  }
  async SuggestFeatureUpdate(id, data) {
    return await http.put(`/suggestFeature/${id}`, data);
  }

  async productitemreview(data) {
    return await http.post(`/productitemreview`, data);
  }

  async assocTaskPost(data) {
    return await http.post(`/assocTask`, data);
  }

  async assocTaskGet(page) {
    return await http.get(`/assocTask${page}`);
  }

  async assocTaskGetId(id) {
    return await http.get(`/assocTask/${id}`);
  }

  async assocTaskGetUpdate(id, data) {
    return await http.put(`/assocTask/${id} `, data);
  }

  async mlsBoardDocument(data) {
    return await http.post(`/onBoardDocument`, data);
  }
  async mlsBoardDocumentGet() {
    return await http.get(`/onBoardDocument`);
  }

  async mlsBoardDocumentGetId(mlsMembership = "", boardMembership = "") {
    return await http.get(
      `/onBoardDocument?mlsMembership=${mlsMembership}&boardMembership=${boardMembership}`
    );
  }

  async contactNotesPost(data) {
    return await http.post(`/contactNotes`, data);
  }

  async contactNotesUpdate(id, data) {
    return await http.put(`/contactNotes/${id} `, data);
  }

  async contactNotesDelete(id) {
    return await http.delete(`/contactNotes/${id}`);
  }

  async contactNotesGetId(id) {
    return await http.get(`/contactNotes?contactId=${id}`);
  }

  async contactSendEmailPost(data) {
    return await http.post(`/contactVideo/contactEmail/`, data);
  }

  async contactSendEmailget() {
    return await http.get(`/contactVideo/contactEmail`);
  }

  async commissionPost(data) {
    return await http.post(`/commission`, data);
  }

  async commissionUpdate(id, data) {
    return await http.put(`/commission/${id} `, data);
  }

  async commissionGet(id) {
    return await http.get(`/commission?transactionId=${id}`);
  }

  async referralPost(data) {
    return await http.post(`/transactionReferral`, data);
  }

  async openHousePost(data) {
    return await http.post(`/open-house`, data);
  }

  async openHouseGet(id) {
    return await http.get(`/open-house?transactionId=${id}`);
  }

  async OpenHouseGetDelete(id) {
    return await http.delete(`/open-house/${id}`);
  }

  async referralUpdate(id, data) {
    return await http.put(`/transactionReferral/${id} `, data);
  }
  async referralGet(id) {
    return await http.get(`/transactionReferral?transactionId=${id}`);
  }

  async transactionClosingPost(data) {
    return await http.post(`/transactionClosing`, data);
  }

  async transactionClosingUpdate(id, data) {
    return await http.put(`/transactionClosing/${id} `, data);
  }

  async transactionClosingGet(id) {
    return await http.get(`/transactionClosing?transactionId=${id}`);
  }

  async googleGet() {
    return await http.get(`/user/info`);
  }

  async UtiltityPost(data) {
    return await http.post(`/utility`, data);
  }

  async UtiltityGet(page) {
    return await http.get(`/utility?page=${page}`);
  }

  async UtiltityDelete(id) {
    return await http.delete(`/utility/${id}`);
  }

  async UtiltityGetByID(id) {
    return await http.get(`/utility/${id}`);
  }

  async UtiltityUpdate(id, data) {
    return await http.put(`/utility/${id}`, data);
  }

  async UtiltityAgentPost(data) {
    return await http.post(`/agent-utility`, data);
  }
  async UtiltityAgentGet(page) {
    return await http.get(`/agent-utility?page=${page}`);
  }

  async UtilityEmail(id) {
    return await http.get(`/agent-utility/utilityEmail/${id}`);
  }

  async UtilityDataAgentGet(page, zipcode) {
    return await http.get(`/utility?page=${page}&zipcode=${zipcode}`);
  }

  async reportBugAgentPost(data) {
    return await http.post(`/agent-reportBug`, data);
  }

  async reportBugAgentPost(data) {
    return await http.post(`/agent-reportBug`, data);
  }

  async reportBugAgentGet() {
    return await http.get(`/agent-reportBug`);
  }

  async contactGroupPost(data) {
    return await http.post(`/contactGroup`, data);
  }

  async contactGroupUpdate(id, data) {
    return await http.put(`/contactGroup/${id}`, data);
  }

  async contactGroupGet() {
    return await http.get(`/contactGroup`);
  }

  async contactGroupGetById(id) {
    return await http.get(`/contactGroup/${id}`);
  }

  async contactGroupMemberPost(data) {
    return await http.post(`/groupMember`, data);
  }

  async contactGroupMemberUpdate(id, data) {
    return await http.put(`/groupMember/${id}`, data);
  }
  async contactGroupMemberGetId(id) {
    return await http.get(`/groupMember/${id}`);
  }
  async contactGroupMemberGet(group_name) {
    return await http.get(`/groupMember?group_name=${group_name}`);
  }

  async contactGroupMemberGetAll() {
    return await http.get(`/groupMember`);
  }

  async contactGroupMemberDelete(id) {
    return await http.delete(`/groupMember/${id}`);
  }

  async reservationPost(data) {
    return await http.post(`/reservation`, data);
  }

  async reservationUpdate(id, data) {
    return await http.put(`/reservation/${id}`, data);
  }

  async reservationGetAll() {
    return await http.get(`/reservation`);
  }

  async reservationGetId(id) {
    return await http.get(`/reservation/${id}`);
  }

  async reservationDelete(id) {
    return await http.delete(`/reservation/${id}`);
  }

  async configureSchedulePost(data) {
    return await http.post(`/configureSchedule`, data);
  }

  async configureScheduleUpdate(id, data) {
    return await http.put(`/configureSchedule/${id}`, data);
  }
  async configureScheduleGet() {
    return await http.get(`/configureSchedule`);
  }

  async configureScheduleGetAll(reservationId) {
    return await http.get(`/configureSchedule?reservationId=${reservationId}`);
  }

  async calenderReservationPost(data) {
    return await http.post(`/calenderReservation`, data);
  }

  async calenderReservationgetAll(reservationId) {
    return await http.get(
      `/calenderReservation?reservationId=${reservationId}`
    );
  }

  async calenderReservationDelete(id) {
    return await http.delete(`/calenderReservation/${id}`);
  }

  async notificationPost(data) {
    return await http.post(`/notification`, data);
  }

  async notificationUpdate(id, data) {
    return await http.put(`/notification/${id}`, data);
  }

  async notificationGetAll(transactionId) {
    return await http.get(`/notification?transactionId=${transactionId}`);
  }
  async notificationGetById(agentId) {
    return await http.get(`/notification?agentId=${agentId}`);
  }

  async notificationGet() {
    return await http.get(`/notification`);
  }

  async pageContentMarketing(data) {
    return await http.post(`/pageContent`, data);
  }

  async pageContentMarketingUpdate(id, data) {
    return await http.put(`/pageContent/${id}`, data);
  }

  async pageContentMarketingDelete(id) {
    return await http.delete(`/pageContent/${id}`);
  }

  async pageContentMarketingGet() {
    return await http.get(`/pageContent`);
  }

  async pageContentMarketingGetId(id) {
    return await http.get(`/pageContent/${id}`);
  }

  async TaskPost(data) {
    return await http.post(`/tasks`, data);
  }

  async transactionTask(id) {
    return await http.get(`/tasks?transactionId=${id}`);
  }

  async transactionTaskGetId(id) {
    return await http.get(`/tasks/${id}`);
  }

  async transactionTaskUpdate(id, data) {
    return await http.put(`/tasks/${id}`, data);
  }

  async taskUpdateSerial(data) {
    return await http.put(`/tasks/update-serial-numbers`, data);
  }

  async transactionTaskDelete(id) {
    return await http.delete(`/tasks/${id}`);
  }
}
export default new user_service();

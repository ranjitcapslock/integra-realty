import axios from "axios";
import { BehaviorSubject } from "rxjs";

const authSubject = new BehaviorSubject(null);

export function signin(token) {
  authSubject.next(token);
  authSubject.subscribe((res) => {
    window.tokenData = res;
  });
}

export default axios.create ({
baseURL: "https://api.brokeragentbase.com",                                            
  // baseURL: "http://localhost:4000",             
  headers: {
    "Content-type": "application/json; charset=utf-8",
    Authorization: `Bearer ${localStorage.getItem("auth")}`,
    //"office": `${jwt(localStorage.getItem("auth")).id}`,
  }
}); 

import React, { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import user_service from "../service/user_service";
import Loader from "../../Pages/Loader/Loader.js";
import Toaster from "../../Pages/Toaster/Toaster";
import { NavLink } from "react-router-dom";
import jwt from "jwt-decode";
import { logDOM } from "@testing-library/react";
import defaultpropertyimage from "../../Components/img/defaultpropertyimage.jpeg";

import { Document, Page, pdfjs } from "react-pdf";
import jsPDF from "jspdf";
import html2canvas from "html2canvas";
import { PDFDocument } from "pdf-lib";
import "react-pdf/dist/esm/Page/AnnotationLayer.css";
import "react-pdf/dist/esm/Page/TextLayer.css";

const TransactionDoc = () => {
  const params = useParams();
  const [loader, setLoader] = useState({ isActive: null });
  const { isActive } = loader;
  const [toaster, setToaster] = useState({
    types: null,
    isShow: null,
    toasterBody: "",
    message: "",
  });
  const { types, isShow, toasterBody, message } = toaster;
  const [summary, setSummary] = useState("");
  const [getDocument, setGetDocument] = useState("");
  const [getDocumentAll, setGetDocumentAll] = useState([]);
  const [documentValues, setDocumentValues] = useState([]);
  const [singleDocument, setSingleDocument] = useState("");
  const [singleDocumentType, setSingleDocumentType] = useState("");
  const [documentType, setDocumentType] = useState("");
  const [approval, setApproval] = useState("");
  const [approvalNew, setApprovalNew] = useState(false);

  const [documentValuesId, setDocumentValuesId] = useState(params.itemId);

  const [formValues, setFormValues] = useState({
    message: "",
  });
  const [formValuesNew, setFormValuesNew] = useState("");

  const handleChange = (e) => {
    const { name, value } = e.target;
    setFormValues({ ...formValues, [name]: value });
  };

  useEffect(() => {
    window.scrollTo(0, 0);
    setLoader({ isActive: true });
    // window.location.reload();
    const TransactionGetById = async () => {
      await user_service.transactionGetById(params.id).then((response) => {
        if (response) {
          setLoader({ isActive: false });
          setSummary(response.data);
        }
      });
    };
    TransactionGetById(params.id);
  }, []);

  useEffect(() => {
    window.scrollTo(0, 0);
    transactionDocumentAll();
    transactionDocumentGetId();
  }, []);

  const transactionDocumentGetId = async () => {
    await user_service
      .transactionDocumentGetId(params.itemId)
      .then((response) => {
        if (response) {
          // console.log(response.data);
          setGetDocument(response.data);
          const document = response.data.document;
          setSingleDocument(document);
          const attachTypes = response.data.attachType;
          setSingleDocumentType(attachTypes);
          const documentTypes = response.data.documentName;
          setDocumentType(documentTypes);
          const approvalStatus = response.data.approvalStatus;
          setApproval(approvalStatus);
        }
      });
  };

  // useEffect(() => { window.scrollTo(0, 0);
  const transactionDocumentAll = async () => {
    await user_service.transactionDocumentGet(params.id).then((response) => {
      if (response) {
        setGetDocumentAll(response.data.data);
        const documentValuess = [];
        response.data.data?.map((postt) => {
          documentValuess.push(postt.documentName);
        });
        setDocumentValues(documentValuess);

        // response.data.data?.map((postt) => {
        //   setApproval(postt.approvalStatus);
        //   // TransactionDocumentId(postt._id);
        // });
      }
    });
  };
  // transactionDocumentAll();

  // }, [])

  const handleAllDoucment = (items) => {
    setDocumentValuesId(items._id);
    setSingleDocument(items.document);
    setSingleDocumentType(items.attachType);
    setDocumentType(items.documentName);
    setApproval(items.approvalStatus);
  };

  const handleSubmitNotes = async (e) => {
    e.preventDefault();
    const userDatadoc = {
      approvalStatus: "Reject",
      attachNote: formValues.message ?? formValuesNew?.message,
    };

    console.log(userDatadoc);
    const userDatan = {
      addedBy: jwt(localStorage.getItem("auth")).id,
      agentId: jwt(localStorage.getItem("auth")).id,
      transactionId: params.id,
      message: `Reviewed "${documentType}" and it needs attention. Comment: ${
        formValues.message ?? formValuesNew?.message
      }`,
      note_type: "Documents Reason",
      noteName: documentType,
      seenby: singleDocument,
    };

    console.log(userDatan);

    const response = await user_service.transactionDocumentUpdate(
      documentValuesId,
      userDatadoc
    );
    console.log(response);
    setLoader({ isActive: true });

    try {
      const responsen = await user_service.transactionNotes(userDatan);
      TransactionNotes(responsen.data._id);
      setLoader({ isActive: false });
      setToaster({
        type: "Reason Updated Successfully",
        isShow: true,
        message: "Reason Updated Successfully",
      });

      const firstItemToRedirect = getDocumentAll.find(
        (item) => item.approvalStatus !== "Yes"
      );

      // Redirect if such an item exists
      // if (firstItemToRedirect) {
      //   window.location.href = `/transactionDoc/${params.id}/${firstItemToRedirect._id}`;
      // }

      if (firstItemToRedirect) {
        const currentIndex = getDocumentAll.indexOf(firstItemToRedirect);
        const nextIndex = currentIndex + 1;
      
        if (nextIndex < getDocumentAll.length) {
          const nextDocumentToRedirect = getDocumentAll[nextIndex];
          window.location.href = `/transactionDoc/${params.id}/${nextDocumentToRedirect._id}`;
        } else {
          if (firstItemToRedirect) {
            window.location.href = `/transactionDoc/${params.id}/${firstItemToRedirect._id}`;
          }
        }
      }
      setTimeout(() => {
        setToaster((prevToaster) => ({ ...prevToaster, isShow: false }));
      }, 2000);
    } catch (error) {
      console.error(error);
    }
  };

  const TransactionNotes = async (id) => {
    await user_service.transactionNotesById(id).then((response) => {
      setLoader({ isActive: false });
      if (response) {
        console.log(response.data);
        setFormValuesNew(response.data);
      }
    });
  };

  const TransactionDocuments = async (id) => {
    await user_service.transactionDocumentGetId(id).then((response) => {
      setLoader({ isActive: false });
      if (response) {
        console.log(response.data);
        // setFormValuesNew(response.data);
      }
    });
  };

  const handleSubmit = async (e) => {
    e.preventDefault();

    const updateData = {
      approvalStatus: "Yes",
    };
    console.log(updateData);
    setLoader({ isActive: true });

    try {
      const response = await user_service.transactionDocumentUpdate(
        documentValuesId,
        updateData
      );
      if (response) {
        console.log(response.data);
        setApproval("");
        TransactionDocuments(response.data._id);
        setLoader({ isActive: false });
        // Find the first item with approvalStatus !== "Yes"

        const firstItemToRedirect = getDocumentAll.find(
          (item) => item.approvalStatus !== "Yes"
        );
        
        if (firstItemToRedirect) {
          const currentIndex = getDocumentAll.indexOf(firstItemToRedirect);
          const nextIndex = currentIndex + 1;
        
          if (nextIndex < getDocumentAll.length) {
            const nextDocumentToRedirect = getDocumentAll[nextIndex];
            window.location.href = `/transactionDoc/${params.id}/${nextDocumentToRedirect._id}`;
          } else {
            if (firstItemToRedirect) {
              window.location.href = `/transactionDoc/${params.id}/${firstItemToRedirect._id}`;
            }
          }
        }
        
      }
    } catch (error) {
      console.error(error);
    }
  };

 
  const handleCancelNew = () => {
    setApprovalNew(false);
  }


  const handleCancel = () => {
    setApprovalNew(true);

    // e.preventDefault();
    // const updateData = {
    //   approvalStatus: "Reject",
    //   is_active: "no",
    // };

    // setLoader({ isActive: true });
    // await user_service
    //   .transactionDocumentUpdate(documentValuesId, updateData)
    //   .then((response) => {
    //     if (response) {
    //       setLoader({ isActive: false });
    //       console.log(response);
    //   TransactionDocuments(response.data._id);

    //   const firstItemToRedirect = getDocumentAll.find(
    //     (item) => item.approvalStatus !== "Yes"
    //   );

    //   // Redirect if such an item exists
    //   if (firstItemToRedirect) {
    //     window.location.href = `/transactionDoc/${params.id}/${firstItemToRedirect._id}`;
    //   }

    //     }
    //   });
    // window.close();
  };

  const propertypic = (e) => {
    e.target.src = defaultpropertyimage;
  };
  const [numPages, setNumPages] = useState(null);
  const [pageNumber, setPageNumber] = useState(1);

  function onDocumentLoadSuccess({ numPages }) {
    setNumPages(numPages);
  }
  return (
    <div className="bg-secondary float-left w-100 pt-4 mb-4">
      <Loader isActive={isActive} />
      {isShow && (
        <Toaster
          types={types}
          isShow={isShow}
          toasterBody={toasterBody}
          message={message}
        />
      )}
      <main className="page-wrapper mt-4">
        {/* <AllTab /> */}
        <div className="float-start w-100 mb-4">
          <div className="row">
            <div className="col-md-3">
              <h6 className="mb-0 text-white">
                Document Name : {documentType}
              </h6>
            </div>
            {/* <div className="col-md-9">
              <div className="pull-right d-flex align-items-center justify-content-between w-100">
                <div className="d-flex">
              
                  {approvalNew ? (
                    <>
                      <label className="col-form-label">
                        REASON Shown to Associate
                      </label>
                      <input
                        className="form-control"
                        id="inline-form-input"
                        type="text"
                        name="message"
                        value={formValues.message ?? formValuesNew?.message}
                        onChange={handleChange}
                      />
                      <button
                        type="button"
                        className="btn btn-primary btn-sm float-right pull-right ms-3"
                        onClick={handleSubmitNotes}
                      >
                        Update
                      </button>
                    </>
                  ) : (
                    ""
                  )}
                </div>

                <div>
                  {approval === "Yes" ? (
                    ""
                  ) : (
                    <button
                      className="btn btn-primary btn-sm"
                      onClick={handleSubmit}
                    >
                      Approve & Next
                    </button>
                  )}
                  {approval === "Reject" ? (
                 ""
                  ) : (
                    <>
                    {
                      approvalNew ?
                    <>
                    </>
                      :
                    <button
                      className="btn btn-primary btn-sm ms-2"
                      onClick={handleCancel}
                    >
                      Reject
                    </button>
                    }
                    </>
                  )}
                 
                </div>
              </div>
            </div> */}
          </div>
        </div>

        <div className="row">
          {/* <div className="col-md-12"><h6 className="ms-5">{documentType}</h6></div> */}
          <div className="col-md-3">
            <div className="file_links float-left w-100">
              {getDocumentAll.map((item, index) => (
                <div className="" key={index}>
                  <a>
                    <p
                      style={{
                        color:
                          item.approvalStatus === "Yes"
                            ? "#009900"
                            : item.approvalStatus === "Reject"
                            ? "#990000"
                            : "#000",
                      }}
                      onClick={() => handleAllDoucment(item)}
                    >
                      {item.documentName}
                    </p>
                  </a>
                </div>
              ))}
            </div>
          </div>

          <div className="col-md-9">
            <div className="bg-light border rounded-3 p-5 mb-0">
              <div className="documentverify transaction_doc">
                <Document
                  className=""
                  file={singleDocument}
                  onLoadSuccess={onDocumentLoadSuccess}
                >
                  {/* <Page pageNumber={pageNumber} /> */}
                  {Array.from(new Array(numPages), (el, index) => (
                    <Page
                      key={`page_${index + 1}`}
                      pageNumber={index + 1}
                      //   onLoadSuccess={onDocumentLoadSuccess}
                    />
                  ))}
                </Document>
              </div>

              <div className="d-flex align-items-center justify-content-between w-100">
                {approvalNew ? (
                  <div className="float-start w-100">
                    <label className="col-form-label">
                      REASON Shown to Associate
                    </label>
                    <div className="d-flex">
                      <input
                        className="form-control"
                        id="inline-form-input"
                        type="text"
                        name="message"
                        value={formValues.message ?? formValuesNew?.message}
                        onChange={handleChange}
                      />
                      <button
                        type="button"
                        className="btn btn-primary btn-sm float-right pull-right ms-3"
                        onClick={handleSubmitNotes}
                      >
                        Update
                      </button>
                    </div>

                    <button
                        type="button"
                        className="btn btn-primary btn-sm float-right pull-right mt-4"
                        onClick={handleCancelNew}
                      >
                        Cancel
                      </button>
                  </div>
                ) : (
                  ""
                )}
              </div>
              <div className="pull-right">
                {approval === "Yes" ? (
                  ""
                ) : (
                  <>
                   {
                    approvalNew ?
                    <></>
                    :
                  <button
                    className="btn btn-primary btn-sm"
                    onClick={handleSubmit}
                  >
                    Approve & Next
                  </button>
                   }
                  
                  </>
                )}

                {approval === "Reject" ? (
                  ""
                ) : (
                  <>
                    {approvalNew ? (
                      <></>
                    ) : (
                      <button
                        className="btn btn-primary btn-sm ms-2"
                        onClick={handleCancel}
                      >
                        Reject
                      </button>
                    )}
                  </>
                )}
              </div>
            </div>
          </div>
        </div>
      </main>
    </div>
  );
};

export default TransactionDoc;

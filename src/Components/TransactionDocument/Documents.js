import React, { useRef, useState, useEffect } from "react";
// import AddDocuments from './AddDocuments';
import { useNavigate, useParams } from "react-router-dom";
import user_service from "../service/user_service";
import Loader from "../../Pages/Loader/Loader.js";
import Toaster from "../../Pages/Toaster/Toaster";
import AllTab from "../../Pages/AllTab";
import axios from "axios";
import jwt from "jwt-decode";
import moment from "moment";
// import { Editor } from "@tinymce/tinymce-react";
import { NavLink } from "react-router-dom";
import { Document, Page, pdfjs } from "react-pdf";
import "jspdf-autotable";
import jsPDF from "jspdf";
import html2canvas from "html2canvas";
import { PDFDocument } from "pdf-lib";
import "react-pdf/dist/esm/Page/AnnotationLayer.css";
import "react-pdf/dist/esm/Page/TextLayer.css";
import Pic from "../img/pic.png";

import SendDocumentMail from "./SendDocumentMail.js";

import Swal from "sweetalert2";
import "sweetalert2/dist/sweetalert2.css";
import "react-dropzone-uploader/dist/styles.css";
import Dropzone from "react-dropzone-uploader";
import OverlayTrigger from "react-bootstrap/OverlayTrigger";
import Tooltip from "react-bootstrap/Tooltip";

pdfjs.GlobalWorkerOptions.workerSrc = `//cdnjs.cloudflare.com/ajax/libs/pdf.js/${pdfjs.version}/pdf.worker.js`;

const Documents = () => {
  const navigate = useNavigate();
  const [transactionDetail, setTransactionDetail] = useState("");
  const [getDocument, setGetDocument] = useState([]);
  const [loader, setLoader] = useState({ isActive: null });
  const [issubmit, setIssubmit] = useState(false);
  const { isActive } = loader;
  const [toaster, setToaster] = useState({
    types: null,
    isShow: null,
    toasterBody: "",
    message: "",
  });

  const profilepic = (e) => {
    e.target.src = Pic;
  };

  const initialValues = {
    document: "",
    attachType: "",
  };
  const { types, isShow, toasterBody, message } = toaster;
  const [summary, setSummary] = useState([]);
  const [transactiontype, setTransactiontype] = useState([]);
  const [rulesdata, setRulesdata] = useState([]);
  const params = useParams();
  const [documentValues, setDocumentValues] = useState([]);
  const [documentValuesId, setDocumentValuesId] = useState([]);

  const [documentDetails, setDocumentDetails] = useState("");
  const [exemptionDescription, setExemptionDescription] = useState("");

  const [file, setFile] = useState(null);
  const [fileExtension, setFileExtension] = useState("");

  const [documentType, setDocumentType] = useState("");
  const [acceptedFileTypes, setAcceptedFileTypes] = useState([]);

  const [selectedCondition, setSelectedCondition] = useState(null);
  const [selectedConditionname, setSelectedConditionname] = useState(null);
  const [step1, setStep1] = useState("");
  const [step2, setStep2] = useState("");
  const [step3, setStep3] = useState("");

  const [image, setImage] = useState(null);
  const [isImage, setIsImage] = useState(false);
  const [numPages, setNumPages] = useState([]);
  const [selectedPage, setSelectedPage] = useState([]);
  const [selectedPageAll, setSelectedPageAll] = useState([]);

  const [selectedSinglePage, setSelectedSinglePage] = useState([]);

  const [selectedImage, setSelectedImage] = useState(null);
  const [isPdfClosed, setIsPdfClosed] = useState(true);
  const [isPageBlurred, setIsPageBlurred] = useState(false);
  const [pdfPath, setPdfPath] = useState("");
  const [allPdfPath, setAllPdfPath] = useState("");

  const [imageResponce, setImageResponce] = useState("");
  const [documentcustomname, setDocumentcustomname] = useState("");
  const [documentcustomnameerror, setDocumentcustomnameerror] = useState(false);
  const [copySuccessMap, setCopySuccessMap] = useState({});
  const [requireddetailsaddeddata, setRequiredDetailsaddeddata] = useState([]);
  const [selectAllPagesNew, setSelectAllPagesNew] = useState([]);
  const [numPagesList, setNumPagesList] = useState([]);
  const [formValues, setFormValues] = useState(initialValues);
  const [customdocumentaddeddata, setCustomdocumentaddeddata] = useState([]);
  const [unassignedDocumentdata, setUnassignedDocumentdata] = useState([]);

  const [getrequireddoucmentno, setGetrequireddoucmentno] = useState([]);
  const [transactionFundingRequest, setTransactionFundingRequest] =
    useState("");

  const [createDocumets, setCreateDocumets] = useState(false);
  const [attachDocumets, setAttachDocumets] = useState(false);

  const transactionDocumentAll = async () => {
    await user_service.transactionDocumentGet(params.id).then((response) => {
      // console.log(response);
      if (response) {
        setDocumentValues(response.data.data);
        response.data.data?.map((postt) => {
          setDocumentValuesId(postt);
        });
        const customdocumentaddeddataa = response.data.data.filter(
          (item) =>
            item.documentType === "customadditional" && item.is_active === "yes"
        );
        setCustomdocumentaddeddata(customdocumentaddeddataa);

        const unassigneddocumentdata = response.data.data.filter(
          (item) =>
            item.documentType === "unassigned" && item.is_active === "yes"
        );

        setUnassignedDocumentdata(unassigneddocumentdata);
      }
    });
  };

  const Getrequireddoucmentno = async () => {
    const agentId = jwt(localStorage.getItem("auth")).id;
    await user_service
      .Getrequireddoucmentno(agentId, params.id)
      .then((response) => {
        // setLoader({ isActive: false })
        if (response) {
          setGetrequireddoucmentno(response.data);
        }
      });
  };

  const TransactionFundingRequest = async () => {
    await user_service.transactionFundingRequest(params.id).then((response) => {
      if (response) {
        setLoader({ isActive: false });
        setTransactionFundingRequest(response.data.data[0]);
      }
    });
  };

  const TransactionGetById = async () => {
    await user_service.transactionGetById(params.id).then((response) => {
      setLoader({ isActive: false });
      if (response) {
        let tcategory = response.data.category;
        let ttype = response.data.type;
        let xtype = response.data.category + "_" + response.data.type;
        xtype.replace(/ /g, "_");
        xtype = xtype.toLowerCase();
        setTransactiontype(ttype);
        setSummary(response.data);
      }
    });
  };

  const DocumentRulesdata = async () => {
    const agentId = jwt(localStorage.getItem("auth")).id;
    await user_service.DocumentRules(agentId).then((response) => {
      setLoader({ isActive: false });
      if (response) {
        const groupedBytransactionPhase = response.data.data.reduce(
          (acc, item) => {
            const transactionPhase = item.transactionPhase;
            if (!acc[transactionPhase]) {
              acc[transactionPhase] = [];
            }
            acc[transactionPhase].push(item);
            return acc;
          },
          {}
        );
        setRulesdata(groupedBytransactionPhase);
      }
    });
  };

  useEffect(() => {
    window.scrollTo(0, 0);
    setLoader({ isActive: true });
    DocumentRulesdata(jwt(localStorage.getItem("auth")).id);
    transactionDocumentAll();
    TransactionGetById(params.id);
    DetailsaddeddataNew();
    TransactionFundingRequest(params.id);
    Getrequireddoucmentno();
  }, []);

  const handleUploadFile = (condition, conditionname) => {
    setSelectedCondition(condition);
    setSelectedConditionname(conditionname);
    setStep1("");
    setStep2("");
    setSelectedSinglePage([]);
    setSelectAllPagesNew([]);
  };

  const handleExemptionopen = (condition, conditionname) => {
    setSelectedCondition(condition);
    setSelectedConditionname(conditionname);
  };

  const handleexemption = async () => {
    try {
      const doc = new jsPDF();

      if (exemptionDescription) {
        const headingText = "Exemption Document";

        const fontSize = 11;
        const leftMargin = 14;
        const topMargin = 15;

        doc.setFontSize(fontSize);

        doc.text(headingText, leftMargin, topMargin);
        doc.text(exemptionDescription, leftMargin, topMargin + 10);

        //doc.autoTable({ body: tableDataEstimate, tableStylesNew });

        const pdfBlob = doc.output("blob");
        const pdfFormData = new FormData();
        pdfFormData.append("file", pdfBlob, "generatedexemption.pdf");

        const apiUrl = "https://api.brokeragentbase.com/upload";
        // const apiUrl = "http://localhost:4000/upload";

        const config = {
          headers: {
            "Content-Type": "multipart/form-data",
          },
        };

        const response = await axios.post(apiUrl, pdfFormData, config);
        setPdfPath(response.data);

        const attachType = "pdf";

        const userData = {
          document: response.data,
          attachType: attachType,
          transactionId: params.id,
          documentType: selectedCondition.value,
          documentName: selectedConditionname,
          exemption: "yes",
          agentId: jwt(localStorage.getItem("auth")).id,
        };

        const uploadResponse = await user_service.uploadDocuments(userData);
        if (uploadResponse) {
          const splitValues = selectedConditionname.split("_"); // ["buyer", "partner"]
          const userDatan = {
            addedBy: jwt(localStorage.getItem("auth")).id,
            agentId: jwt(localStorage.getItem("auth")).id,
            transactionId: params.id,
            message: `Document Added - ${splitValues.join("  ")}.`,
            // transaction_property_address: "ADDRESS 2",
            note_type: "Document",
          };
          const responsen = user_service.transactionNotes(userDatan);

          setStep1("");
          setStep2("");
          setStep3("");
          setFile(null);
          setIsImage("");
          setImage("");
          setImageResponce("");
          setFileExtension("");
          setDocumentType("");
          setAcceptedFileTypes([]);
          transactionDocumentAll();
          TransactionGetById(params.id);
          setIssubmit(false);
          document.getElementById("exemptionclose").click();
        }
      } else {
        console.log("nodata text");
      }
    } catch (error) {
      console.error("Error generating PDF:", error);
    }
  };

  const TransactionStep = (stepno, value) => {
    if (stepno === "1") {
      setStep1(stepno);
    }
    if (stepno === "2") {
      setStep2(stepno);
      setStep1("");
    }
  };

  const handleChangeStatus = ({ meta, file }, status) => {
    if (status === "done") {
      setFile((prevUploadedFiles) => {
        if (!prevUploadedFiles) {
          prevUploadedFiles = [];
        }
        return [...prevUploadedFiles, file];
      });

      // Extract file extension
      const fileExtension = meta.name.split(".").pop();
      setFileExtension(fileExtension);

      // Check if file extension is accepted
      if (!acceptedFileTypes.includes(fileExtension)) {
        console.log(fileExtension);
      }

      // Count number of pages in the PDF file
      if (file.type === "application/pdf") {
        const reader = new FileReader();
        reader.onload = async (e) => {
          const buffer = e.target.result;
          const typedArray = new Uint8Array(buffer);
          try {
            const pdf = await pdfjs.getDocument(typedArray).promise;
            const numPages = pdf.numPages;
            setNumPages((prevNumPagesList) => [...prevNumPagesList, numPages]); // Store numPages in array
          } catch (error) {
            console.error("Error reading PDF file:", error);
          }
        };
        reader.readAsArrayBuffer(file);
      }

      // Read file content and update image state
      const reader = new FileReader();
      reader.onload = (e) => {
        const uploadedFile = e.target.result;
        setImage(uploadedFile);
        setIsImage(file.type.includes("image"));
      };
      reader.readAsDataURL(file);

      // Update page-related states
      setSelectedPage([]);
      setSelectedPageAll([]);
    } else if (status === "removed") {
      setFile(null);
      setSelectedCondition(null);
      setSelectedConditionname(null);
      setFileExtension(null);
      setImage(null);
      setIsImage(false);
      setNumPagesList([]);
      setNumPages([]);
    }
  };

  const onDocumentLoadSuccess = ({ numPages }, index) => {
    setNumPagesList((prevNumPagesList) => {
      const updatedNumPagesList = [...prevNumPagesList];
      updatedNumPagesList[index] = numPages;
      return updatedNumPagesList;
    });
  };

  const handleImage = () => {
    setSelectedImage(image);
  };

  useEffect(() => {
    if (file) {
      setSelectAllPagesNew(Array(file.length).fill(false));
    }
  }, [file]);

  const handleCheckBox = (item, index, numPages) => {
    console.log(numPages);
    const uniqueKey = `${item.name}-${index}-${numPages}`;
    const existingIndex = selectAllPagesNew.findIndex(
      (page) => page.key === uniqueKey
    );

    if (existingIndex === -1) {
      setSelectAllPagesNew((prevSelectAllPages) => [
        ...prevSelectAllPages,
        { item, index, numPages, key: uniqueKey, value: true, count: 1 },
      ]);
    } else {
      setSelectAllPagesNew((prevSelectAllPages) => {
        const updatedSelectAllPages = [...prevSelectAllPages];
        updatedSelectAllPages[existingIndex].value =
          !prevSelectAllPages[existingIndex].value;
        return updatedSelectAllPages;
      });
    }

    setSelectedSinglePage([]);
    setIsPageBlurred(false);
  };

  const handleCloseAll = (index, pageIndex) => {
    // const uniqueKey = `${item.name}-${pageIndex}`;
    setSelectAllPagesNew((prevPages) => {
      const updatedPages = prevPages
        .map((page) => {
          console.log(page);
          if (page.index === index && page.numPages === pageIndex) {
            if (page.count > 1) {
              return { ...page, count: page.count - 1 };
            } else {
              return null;
            }
          }
          return page;
        })
        .filter(Boolean);
      console.log(updatedPages, "2222");

      return updatedPages;
    });
  };

  const handleClose = (index, pageIndex) => {
    setSelectedSinglePage((prevPages) => {
      const updatedPages = prevPages
        .map((page) => {
          if (page.index === index && page.pageIndex === pageIndex) {
            if (page.count > 1) {
              return { ...page, count: page.count - 1 };
            } else {
              return null;
            }
          }
          return page;
        })
        .filter(Boolean); // Filter out null values (pages to be removed)
      console.log(updatedPages, "2222");

      return updatedPages;
    });
    setSelectAllPagesNew([]);
  };

  const handleInput = (item, index, pageIndex) => {
    const uniqueKey = `${item.name}-${index}-${pageIndex}`;
    const existingPageIndex = selectedSinglePage.findIndex(
      (page) => page.key === uniqueKey
    );

    if (existingPageIndex === -1) {
      setSelectedSinglePage((prevPages) => [
        ...prevPages,
        { item, index, pageIndex, key: uniqueKey, count: 1 },
      ]);
    } else {
      // If the page already exists, increment its counter
      setSelectedSinglePage((prevPages) => [
        ...prevPages.slice(0, existingPageIndex),
        {
          ...prevPages[existingPageIndex],
          count: prevPages[existingPageIndex].count + 1,
        },
        ...prevPages.slice(existingPageIndex + 1),
      ]);
    }
    setSelectAllPagesNew([]);
    setIsPageBlurred(false);
  };

  const handlePageClick = (item, index, pageIndex) => {
    const uniqueKey = `${item.name}-${index}-${pageIndex}`;
    setSelectedPage((prevPages) => {
      if (!Array.isArray(prevPages)) {
        prevPages = [];
      }
      const existingPageIndex = prevPages.findIndex(
        (page) => page.key === uniqueKey
      );

      if (existingPageIndex === -1) {
        return [{ item, index, pageIndex, key: uniqueKey }];
      } else {
        return [];
      }
    });
    setSelectedPageAll([]);
  };

  const handlePagePreview = (item, index) => {
    const uniqueKey = `${item.name}-${index}`;
    setSelectedPageAll((prevPages) => {
      if (!Array.isArray(prevPages)) {
        prevPages = [];
      }
      const existingPageIndex = prevPages.findIndex(
        (page) => page.key === uniqueKey
      );

      if (existingPageIndex === -1) {
        return [{ item, index, key: uniqueKey }];
      } else {
        return [];
      }
    });
    setSelectedPage([]);
  };

  const handleSubmitAttach = async () => {
    if (!file || (!selectAllPagesNew && selectedSinglePage === null)) {
      console.log("Invalid file or selected page.");
      return;
    }

    if (
      Array.isArray(selectAllPagesNew) &&
      selectAllPagesNew.length > 0 &&
      selectAllPagesNew.some(Boolean)
    ) {
      setIssubmit(true);
      try {
        const mergedPdf = new jsPDF({
          orientation: "portrait",
          unit: "pt",
          format: "a4",
        });

        const a4Width = 595.28; // A4 width in points
        const a4Height = 841.89; // A4 height in points
        let firstPageAdded = false;

        for (const { item } of selectAllPagesNew) {
          if (!item) continue;

          const pdf = await pdfjs.getDocument(URL.createObjectURL(item))
            .promise;

          for (let pageNumber = 1; pageNumber <= pdf.numPages; pageNumber++) {
            const page = await pdf.getPage(pageNumber);
            const viewport = page.getViewport({ scale: 1 });

            // Create and render canvas
            const canvas = document.createElement("canvas");
            const canvasContext = canvas.getContext("2d");
            canvas.width = viewport.width;
            canvas.height = viewport.height;

            await page.render({ canvasContext, viewport }).promise;

            // Scale image to fit A4
            const imgData = canvas.toDataURL("image/jpeg");
            const scale = Math.min(
              a4Width / viewport.width,
              a4Height / viewport.height
            );
            const imgWidth = viewport.width * scale;
            const imgHeight = viewport.height * scale;

            const xOffset = (a4Width - imgWidth) / 2; // Center horizontally
            const yOffset = (a4Height - imgHeight) / 2; // Center vertically

            if (!firstPageAdded) {
              mergedPdf.addImage(
                imgData,
                "JPEG",
                xOffset,
                yOffset,
                imgWidth,
                imgHeight
              );
              firstPageAdded = true;
            } else {
              mergedPdf.addPage();
              mergedPdf.addImage(
                imgData,
                "JPEG",
                xOffset,
                yOffset,
                imgWidth,
                imgHeight
              );
            }
          }
        }

        const mergedPdfBytes = mergedPdf.output("arraybuffer");

        const formData = new FormData();
        formData.append(
          "file",
          new Blob([mergedPdfBytes], { type: "application/pdf" }),
          "merged_document.pdf"
        );

        const response = await axios.post(
          "https://api.brokeragentbase.com/upload",
          // "http://localhost:4000/upload",

          formData
        );

        setAllPdfPath(response.data);

        const attachType =
          acceptedFileTypes.length > 0
            ? `${fileExtension} (${acceptedFileTypes.join(", ")})`
            : fileExtension;

        const userData = {
          document: response.data,
          attachType: attachType,
          transactionId: params.id,
          documentType: selectAttachConditionname,
          documentName: selectAttachConditionname,
          attachNote: noteAdd.attachNote,
          agentId: jwt(localStorage.getItem("auth")).id,
        };
        setLoader({ isActive: true });
        try {
          const response = await user_service.uploadDocuments(userData);
          if (response) {
            const splitValues = selectAttachConditionname.split("_"); // ["buyer", "partner"]
            const userDatan = {
              addedBy: jwt(localStorage.getItem("auth")).id,
              agentId: jwt(localStorage.getItem("auth")).id,
              transactionId: params.id,
              message: `Document Added - ${splitValues.join("  ")}.`,
              // transaction_property_address: "ADDRESS 2",
              note_type: "Document",
            };
            const responsen = user_service.transactionNotes(userDatan);

            setLoader({ isActive: false });
            setToaster({
              type: "Document Added Successfully",
              isShow: true,
              toasterBody: response.data.message,
              message: "Document Added Successfully",
            });
            setTimeout(() => {
              setToaster((prevToaster) => ({
                ...prevToaster,
                isShow: false,
              }));
            }, 2000);
            setCreateDocumets(false);
            setAttachDocumets(false);
            setSelectAllPagesNew(Array(file.length).fill(false));
            setSelectedSinglePage([]);
            setIssubmit(false);
            transactionDocumentAll();
            TransactionGetById(params.id);
            // setStep1("");
            // setStep2("");
            // setStep3("");
            // setFile(null);
            // setFileExtension("");
            // setDocumentType("");
            // setAcceptedFileTypes([]);
            // transactionDocumentAll();
            // TransactionGetById(params.id);
            // setIssubmit(false);
            // document.getElementById("closeModalAttach").click();
            // document.getElementById("closeModal").click();
          }
        } catch (error) {
          setIssubmit(false);
          console.log("Error occurred while uploading documents:", error);
        }
      } catch (error) {
        setIssubmit(false);
        console.log("Error occurred while uploading documents:", error);
      }
    } else if (!file || (!selectedSinglePage && selectAllPagesNew === null)) {
      console.log("Invalid file or selected page.");
      setIssubmit(false);
      return;
    }

    if (
      Array.isArray(selectedSinglePage) &&
      selectedSinglePage.length > 0 &&
      selectedSinglePage.some(Boolean)
    ) {
      setIssubmit(true);
      try {
        const mergedPdf = new jsPDF({
          orientation: "portrait",
          unit: "pt",
          format: "a4", // A4 size
        });

        const a4Width = 595.28; // A4 width in points
        const a4Height = 841.89; // A4 height in points

        for (const { item, pageIndex } of selectedSinglePage) {
          if (!item) {
            console.log("Invalid item:", item);
            continue;
          }

          // Load the PDF file using PDF.js
          const pdf = await pdfjs.getDocument(URL.createObjectURL(item))
            .promise;

          // Get the specified page
          const page = await pdf.getPage(pageIndex + 1); // pageIndex is 0-based

          // Render the page on a canvas
          const viewport = page.getViewport({ scale: 1.5 }); // Increase scale for higher resolution
          const canvas = document.createElement("canvas");
          const canvasContext = canvas.getContext("2d");
          const renderContext = { canvasContext, viewport };

          canvas.width = viewport.width;
          canvas.height = viewport.height;

          await page.render(renderContext).promise;

          // Convert the canvas to an image
          const imageData = canvas.toDataURL("image/jpeg");

          // Resize image to fit A4 dimensions while maintaining aspect ratio
          const imgRatio = canvas.width / canvas.height;
          let imgWidth = a4Width;
          let imgHeight = a4Width / imgRatio;

          if (imgHeight > a4Height) {
            imgHeight = a4Height;
            imgWidth = a4Height * imgRatio;
          }

          // Add a new A4 page (except for the first one)
          if (mergedPdf.internal.getNumberOfPages() > 0) {
            mergedPdf.addPage("a4", "portrait");
          }

          // Add the image to the PDF, centered on the A4 page
          mergedPdf.addImage(
            imageData,
            "JPEG",
            (a4Width - imgWidth) / 2, // Center horizontally
            (a4Height - imgHeight) / 2, // Center vertically
            imgWidth,
            imgHeight
          );

          // Add page number
          const pageNumberText = `Page ${pageIndex + 1}`;
          mergedPdf.setFontSize(12);
          mergedPdf.text(pageNumberText, 20, a4Height - 30); // Bottom left
        }

        // Remove the extra first blank page
        if (mergedPdf.internal.getNumberOfPages() > 1) {
          mergedPdf.deletePage(1);
        }

        // Generate the final PDF as an ArrayBuffer
        const mergedPdfBytes = mergedPdf.output("arraybuffer");

        const formData = new FormData();
        formData.append(
          "file",
          new Blob([mergedPdfBytes], { type: "application/pdf" }),
          "merged_document.pdf"
        );

        const response = await axios.post(
          "https://api.brokeragentbase.com/upload",
          // "http://localhost:4000/upload",

          formData
        );

        setPdfPath(response.data);
        const attachType =
          acceptedFileTypes.length > 0
            ? `${fileExtension} (${acceptedFileTypes.join(", ")})`
            : fileExtension;

        const userData = {
          document: response.data,
          attachType: attachType,
          transactionId: params.id,
          documentType: selectAttachConditionname,
          documentName: selectAttachConditionname,
          attachNote: noteAdd.attachNote,
          agentId: jwt(localStorage.getItem("auth")).id,
        };

        console.log(userData);
        setLoader({ isActive: true });
        const uploadResponse = await user_service.uploadDocuments(userData);

        if (uploadResponse) {
          const splitValues = selectAttachConditionname.split("_"); // ["buyer", "partner"]
          const userDatan = {
            addedBy: jwt(localStorage.getItem("auth")).id,
            agentId: jwt(localStorage.getItem("auth")).id,
            transactionId: params.id,
            message: `Document Added - ${splitValues.join("  ")}.`,
            // transaction_property_address: "ADDRESS 2",
            note_type: "Document",
          };
          const responsen = user_service.transactionNotes(userDatan);

          setLoader({ isActive: false });
          setToaster({
            type: "Document Added Successfully",
            isShow: true,
            toasterBody: response.data.message,
            message: "Document Added Successfully",
          });
          setTimeout(() => {
            setToaster((prevToaster) => ({
              ...prevToaster,
              isShow: false,
            }));
          }, 2000);
          setCreateDocumets(false);
          setAttachDocumets(false);
          setSelectAllPagesNew(Array(file.length).fill(false));
          setSelectedSinglePage([]);
          setIssubmit(false);
          transactionDocumentAll();
          TransactionGetById(params.id);
          // setStep1("");
          // setStep2("");
          // setStep3("");
          // setFile(null);
          // setIsImage("");
          // setImage("");
          // setImageResponce("");
          // setFileExtension("");
          // setDocumentType("");
          // setAcceptedFileTypes([]);
          // transactionDocumentAll();
          // TransactionGetById(params.id);
          // setIssubmit(false);
          // document.getElementById("closeModalAttach").click();
          // document.getElementById("closeModal").click();
        }
      } catch (error) {
        console.log("Error occurred:", error);
        setIssubmit(false);
      }
    } else {
      console.log("Invalid file or selected page.");
      setIssubmit(false);
    }
  };

  const [assignDocumentId, setAssignDocumentsId] = useState("");
  const [selectAssignCondition, setSelectAssignCondition] = useState("");
  const [selectAssignConditionname, setSelectAssignConditionname] =
    useState("");
  const [assignDocumets, setAssignDocumets] = useState(false);

  const handleAssignUpload = (condition, conditionname) => {
    setSelectAssignCondition(condition);
    setSelectAssignConditionname(conditionname);
    setAssignDocumets(true);
  };

  const handleClickAssign = (id) => {
    setAssignDocumentsId(id);
  };

  const handleSubmitAssign = async () => {
    const userData = {
      documentType: selectAssignConditionname,
      documentName: selectAssignConditionname,
    };
    console.log(userData);
    setLoader({ isActive: true });
    try {
      const response = await user_service.updateDocuments(
        assignDocumentId,
        userData
      );
      if (response) {
        const splitValues = selectAttachConditionname.split("_"); // ["buyer", "partner"]
        const userDatan = {
          addedBy: jwt(localStorage.getItem("auth")).id,
          agentId: jwt(localStorage.getItem("auth")).id,
          transactionId: params.id,
          message: `Document Added - ${splitValues.join("  ")}.`,
          // transaction_property_address: "ADDRESS 2",
          note_type: "Document",
        };
        const responsen = user_service.transactionNotes(userDatan);

        setLoader({ isActive: false });
        setToaster({
          type: "Document Added Successfully",
          isShow: true,
          toasterBody: response.data.message,
          message: "Document Added Successfully",
        });
        setTimeout(() => {
          setToaster((prevToaster) => ({
            ...prevToaster,
            isShow: false,
          }));
        }, 2000);
        setCreateDocumets(false);
        setAttachDocumets(false);
        setAssignDocumets(false);
        setSelectAllPagesNew(Array(file.length).fill(false));
        setSelectedSinglePage([]);
        setIssubmit(false);
        transactionDocumentAll();
        TransactionGetById(params.id);
        document.getElementById("closeModalAssign").click();
      }
    } catch (error) {
      setIssubmit(false);
      console.log("Error occurred while uploading documents:", error);
    }
  };

  const handleSubmit = async () => {
    if (!file || (!selectAllPagesNew && selectedSinglePage === null)) {
      console.log("Invalid file or selected page.");
      return;
    }

    if (
      Array.isArray(selectAllPagesNew) &&
      selectAllPagesNew.length > 0 &&
      selectAllPagesNew.some(Boolean)
    ) {
      setIssubmit(true);
      try {
        const mergedPdf = new jsPDF({
          orientation: "portrait",
          unit: "pt",
          format: "a4",
        });

        const a4Width = 595.28; // A4 width in points
        const a4Height = 841.89; // A4 height in points
        let firstPageAdded = false;

        for (const { item } of selectAllPagesNew) {
          if (!item) continue;

          const pdf = await pdfjs.getDocument(URL.createObjectURL(item))
            .promise;

          for (let pageNumber = 1; pageNumber <= pdf.numPages; pageNumber++) {
            const page = await pdf.getPage(pageNumber);
            const viewport = page.getViewport({ scale: 1 });

            // Create and render canvas
            const canvas = document.createElement("canvas");
            const canvasContext = canvas.getContext("2d");
            canvas.width = viewport.width;
            canvas.height = viewport.height;

            await page.render({ canvasContext, viewport }).promise;

            // Scale image to fit A4
            const imgData = canvas.toDataURL("image/jpeg");
            const scale = Math.min(
              a4Width / viewport.width,
              a4Height / viewport.height
            );
            const imgWidth = viewport.width * scale;
            const imgHeight = viewport.height * scale;

            const xOffset = (a4Width - imgWidth) / 2; // Center horizontally
            const yOffset = (a4Height - imgHeight) / 2; // Center vertically

            if (!firstPageAdded) {
              mergedPdf.addImage(
                imgData,
                "JPEG",
                xOffset,
                yOffset,
                imgWidth,
                imgHeight
              );
              firstPageAdded = true;
            } else {
              mergedPdf.addPage();
              mergedPdf.addImage(
                imgData,
                "JPEG",
                xOffset,
                yOffset,
                imgWidth,
                imgHeight
              );
            }
          }
        }
        // Convert the PDF to array buffer
        const mergedPdfBytes = mergedPdf.output("arraybuffer");

        const formData = new FormData();
        formData.append(
          "file",
          new Blob([mergedPdfBytes], { type: "application/pdf" }),
          "merged_document.pdf"
        );

        const response = await axios.post(
          "https://api.brokeragentbase.com/upload",
          // "http://localhost:4000/upload",

          formData
        );

        setAllPdfPath(response.data);

        const attachType =
          acceptedFileTypes.length > 0
            ? `${fileExtension} (${acceptedFileTypes.join(", ")})`
            : fileExtension;

        const userData = {
          document: response.data,
          attachType: attachType,
          transactionId: params.id,
          documentType: selectedConditionname,
          documentName: selectedConditionname,
          agentId: jwt(localStorage.getItem("auth")).id,
        };
        console.log(userData);
        setLoader({ isActive: true });
        try {
          const response = await user_service.uploadDocuments(userData);
          if (response) {
            const splitValues = selectedConditionname.split("_"); // ["buyer", "partner"]
            const userDatan = {
              addedBy: jwt(localStorage.getItem("auth")).id,
              agentId: jwt(localStorage.getItem("auth")).id,
              transactionId: params.id,
              message: `Document Added - ${splitValues.join("  ")}.`,
              // transaction_property_address: "ADDRESS 2",
              note_type: "Document",
            };
            const responsen = user_service.transactionNotes(userDatan);
            setLoader({ isActive: false });
            setToaster({
              type: "Document Added Successfully",
              isShow: true,
              toasterBody: response.data.message,
              message: "Document Added Successfully",
            });
            setTimeout(() => {
              setToaster((prevToaster) => ({
                ...prevToaster,
                isShow: false,
              }));
            }, 2000);
            setStep1("");
            setStep2("");
            setStep3("");
            setFile(null);
            setFileExtension("");
            setDocumentType("");
            setAcceptedFileTypes([]);
            transactionDocumentAll();
            TransactionGetById(params.id);
            setIssubmit(false);
            document.getElementById("closeModal").click();
            document.getElementById("closeModalAttach").click();
          }
        } catch (error) {
          setIssubmit(false);
          console.log("Error occurred while uploading documents:", error);
        }
      } catch (error) {
        setIssubmit(false);
        console.log("Error occurred while uploading documents:", error);
      }
    } else if (!file || (!selectedSinglePage && selectAllPagesNew === null)) {
      console.log("Invalid file or selected page.");
      setIssubmit(false);
      return;
    }

    if (
      Array.isArray(selectedSinglePage) &&
      selectedSinglePage.length > 0 &&
      selectedSinglePage.some(Boolean)
    ) {
      setIssubmit(true);
      try {
        const mergedPdf = new jsPDF({
          orientation: "portrait",
          unit: "pt",
          format: "a4", // A4 size
        });

        const a4Width = 595.28; // A4 width in points
        const a4Height = 841.89; // A4 height in points

        for (const { item, pageIndex } of selectedSinglePage) {
          if (!item) {
            console.log("Invalid item:", item);
            continue;
          }

          // Load the PDF file using PDF.js
          const pdf = await pdfjs.getDocument(URL.createObjectURL(item))
            .promise;

          // Get the specified page
          const page = await pdf.getPage(pageIndex + 1); // pageIndex is 0-based

          // Render the page on a canvas
          const viewport = page.getViewport({ scale: 1.5 }); // Increase scale for higher resolution
          const canvas = document.createElement("canvas");
          const canvasContext = canvas.getContext("2d");
          const renderContext = { canvasContext, viewport };

          canvas.width = viewport.width;
          canvas.height = viewport.height;

          await page.render(renderContext).promise;

          // Convert the canvas to an image
          const imageData = canvas.toDataURL("image/jpeg");

          // Resize image to fit A4 dimensions while maintaining aspect ratio
          const imgRatio = canvas.width / canvas.height;
          let imgWidth = a4Width;
          let imgHeight = a4Width / imgRatio;

          if (imgHeight > a4Height) {
            imgHeight = a4Height;
            imgWidth = a4Height * imgRatio;
          }

          // Add a new A4 page (except for the first one)
          if (mergedPdf.internal.getNumberOfPages() > 0) {
            mergedPdf.addPage("a4", "portrait");
          }

          // Add the image to the PDF, centered on the A4 page
          mergedPdf.addImage(
            imageData,
            "JPEG",
            (a4Width - imgWidth) / 2, // Center horizontally
            (a4Height - imgHeight) / 2, // Center vertically
            imgWidth,
            imgHeight
          );

          // Add page number
          const pageNumberText = `Page ${pageIndex + 1}`;
          mergedPdf.setFontSize(12);
          mergedPdf.text(pageNumberText, 20, a4Height - 30); // Bottom left
        }

        // Remove the extra first blank page
        if (mergedPdf.internal.getNumberOfPages() > 1) {
          mergedPdf.deletePage(1);
        }

        // Generate the final PDF as an ArrayBuffer
        const mergedPdfBytes = mergedPdf.output("arraybuffer");

        const formData = new FormData();
        formData.append(
          "file",
          new Blob([mergedPdfBytes], { type: "application/pdf" }),
          "merged_document.pdf"
        );

        const response = await axios.post(
          "https://api.brokeragentbase.com/upload",
          // "http://localhost:4000/upload",
          formData
        );

        setPdfPath(response.data);

        const attachType =
          acceptedFileTypes.length > 0
            ? `${fileExtension} (${acceptedFileTypes.join(", ")})`
            : fileExtension;

        const userData = {
          document: response.data,
          attachType: attachType,
          transactionId: params.id,
          documentType: selectedConditionname,
          documentName: selectedConditionname,
          agentId: jwt(localStorage.getItem("auth")).id,
        };

        setLoader({ isActive: true });

        const uploadResponse = await user_service.uploadDocuments(userData);

        if (uploadResponse) {
          const splitValues = selectedConditionname.split("_"); // ["buyer", "partner"]
          const userDatan = {
            addedBy: jwt(localStorage.getItem("auth")).id,
            agentId: jwt(localStorage.getItem("auth")).id,
            transactionId: params.id,
            message: `Document Added - ${splitValues.join("  ")}.`,
            // transaction_property_address: "ADDRESS 2",
            note_type: "Document",
          };

          const responsen = user_service.transactionNotes(userDatan);
          setLoader({ isActive: false });
          setToaster({
            type: "Document Added Successfully",
            isShow: true,
            toasterBody: response.data.message,
            message: "Document Added Successfully",
          });
          setTimeout(() => {
            setToaster((prevToaster) => ({
              ...prevToaster,
              isShow: false,
            }));
          }, 2000);
          setStep1("");
          setStep2("");
          setStep3("");
          setFile(null);
          setIsImage("");
          setImage("");
          setImageResponce("");
          setFileExtension("");
          setDocumentType("");
          setAcceptedFileTypes([]);
          transactionDocumentAll();
          TransactionGetById(params.id);
          setIssubmit(false);
          document.getElementById("closeModal").click();
          document.getElementById("closeModalAttach").click();
        }
      } catch (error) {
        console.log("Error occurred:", error);
        setIssubmit(false);
      }
    } else {
      console.log("Invalid file or selected page.");
      setIssubmit(false);
    }
  };

  const handleSubmitAdditional = async () => {
    if (!file || (!selectAllPagesNew && selectedSinglePage === null)) {
      console.log("Invalid file or selected page.");
      return;
    }

    if (documentcustomname && documentcustomname !== "") {
      if (
        Array.isArray(selectAllPagesNew) &&
        selectAllPagesNew.length > 0 &&
        selectAllPagesNew.some(Boolean)
      ) {
        setIssubmit(true);
        try {
          const mergedPdf = new jsPDF({
            orientation: "portrait",
            unit: "pt",
            format: "a4",
          });

          const a4Width = 595.28; // A4 width
          const a4Height = 841.89; // A4 height
          let firstPageAdded = false;

          for (const { item } of selectAllPagesNew) {
            if (!item) {
              console.log("Invalid item:", item);
              continue;
            }

            const pdf = await pdfjs.getDocument(URL.createObjectURL(item))
              .promise;

            for (let pageNumber = 1; pageNumber <= pdf.numPages; pageNumber++) {
              const page = await pdf.getPage(pageNumber);
              const viewport = page.getViewport({ scale: 1.5 });
              const canvas = document.createElement("canvas");
              const canvasContext = canvas.getContext("2d");

              canvas.width = viewport.width;
              canvas.height = viewport.height;

              const renderContext = { canvasContext, viewport };
              await page.render(renderContext).promise;

              // Resize image to fit A4
              const imageData = canvas.toDataURL("image/jpeg");
              const imgRatio = canvas.width / canvas.height;
              let imgWidth = a4Width;
              let imgHeight = a4Width / imgRatio;

              if (imgHeight > a4Height) {
                imgHeight = a4Height;
                imgWidth = a4Height * imgRatio;
              }

              // Add image to PDF
              if (firstPageAdded) {
                mergedPdf.addPage("a4", "portrait");
              } else {
                firstPageAdded = true;
              }
              mergedPdf.addImage(
                imageData,
                "JPEG",
                (a4Width - imgWidth) / 2, // Center horizontally
                (a4Height - imgHeight) / 2, // Center vertically
                imgWidth,
                imgHeight
              );
            }
          }

          // Output merged PDF
          const mergedPdfBytes = mergedPdf.output("arraybuffer");

          const formData = new FormData();
          formData.append(
            "file",
            new Blob([mergedPdfBytes], { type: "application/pdf" }),
            "merged_document.pdf"
          );

          const response = await axios.post(
            "https://api.brokeragentbase.com/upload",
            // "http://localhost:4000/upload",
            formData
          );

          setAllPdfPath(response.data);

          const attachType =
            acceptedFileTypes.length > 0
              ? `${fileExtension} (${acceptedFileTypes.join(", ")})`
              : fileExtension;
          const userDatadoc = {
            document: response.data,
            attachType: "pdf",
            transactionId: params.id,
            documentType: "customadditional",
            documentName: documentcustomname,
            agentId: jwt(localStorage.getItem("auth")).id,
          };
          setLoader({ isActive: true });
          try {
            const response = await user_service.uploadDocuments(userDatadoc);
            if (response) {
              const splitValues = documentcustomname.split("_"); // ["buyer", "partner"]
              const userDatan = {
                addedBy: jwt(localStorage.getItem("auth")).id,
                agentId: jwt(localStorage.getItem("auth")).id,
                transactionId: params.id,
                message: `Additional Document Added - ${splitValues.join(
                  "  "
                )}.`,
                // transaction_property_address: "ADDRESS 2",
                note_type: "Document",
              };
              const responsen = user_service.transactionNotes(userDatan);
              setLoader({ isActive: false });
              setToaster({
                type: "Document Added Successfully",
                isShow: true,
                toasterBody: response.data.message,
                message: "Document Added Successfully",
              });
              setTimeout(() => {
                setToaster((prevToaster) => ({
                  ...prevToaster,
                  isShow: false,
                }));
              }, 2000);
              DetailsaddeddataNew();
              setStep1("");
              setStep2("");
              setFile(null);
              setFileExtension("");
              setDocumentType("");
              setAcceptedFileTypes([]);
              transactionDocumentAll();
              TransactionGetById(params.id);
              setIssubmit(false);
              document.getElementById("closeModalDetailadditional").click();
              document.getElementById("closeModal").click();
              document.getElementById("closeModalAttach").click();
            }
          } catch (error) {
            setIssubmit(false);
            console.log("Error occurred while uploading documents:", error);
          }
        } catch (error) {
          setIssubmit(false);
          console.log("Error occurred while uploading documents:", error);
        }
      } else if (!file || (!selectedSinglePage && selectAllPagesNew === null)) {
        console.log("Invalid file or selected page.");
        setIssubmit(false);
        return;
      }

      if (
        Array.isArray(selectedSinglePage) &&
        selectedSinglePage.length > 0 &&
        selectedSinglePage.some(Boolean)
      ) {
        setIssubmit(true);

        try {
          const mergedPdf = new jsPDF({
            orientation: "portrait",
            unit: "pt",
            format: "a4", // A4 size
          });

          const a4Width = 595.28; // A4 width in points
          const a4Height = 841.89; // A4 height in points

          for (const { item, pageIndex } of selectedSinglePage) {
            if (!item) {
              console.log("Invalid item:", item);
              continue;
            }

            // Load the PDF file using PDF.js
            const pdf = await pdfjs.getDocument(URL.createObjectURL(item))
              .promise;

            // Get the specified page
            const page = await pdf.getPage(pageIndex + 1); // pageIndex is 0-based

            // Render the page on a canvas
            const viewport = page.getViewport({ scale: 1.5 }); // Increase scale for higher resolution
            const canvas = document.createElement("canvas");
            const canvasContext = canvas.getContext("2d");
            const renderContext = { canvasContext, viewport };

            canvas.width = viewport.width;
            canvas.height = viewport.height;

            await page.render(renderContext).promise;

            // Convert the canvas to an image
            const imageData = canvas.toDataURL("image/jpeg");

            // Resize image to fit A4 dimensions while maintaining aspect ratio
            const imgRatio = canvas.width / canvas.height;
            let imgWidth = a4Width;
            let imgHeight = a4Width / imgRatio;

            if (imgHeight > a4Height) {
              imgHeight = a4Height;
              imgWidth = a4Height * imgRatio;
            }

            // Add a new A4 page (except for the first one)
            if (mergedPdf.internal.getNumberOfPages() > 0) {
              mergedPdf.addPage("a4", "portrait");
            }

            // Add the image to the PDF, centered on the A4 page
            mergedPdf.addImage(
              imageData,
              "JPEG",
              (a4Width - imgWidth) / 2, // Center horizontally
              (a4Height - imgHeight) / 2, // Center vertically
              imgWidth,
              imgHeight
            );

            // Add page number
            const pageNumberText = `Page ${pageIndex + 1}`;
            mergedPdf.setFontSize(12);
            mergedPdf.text(pageNumberText, 20, a4Height - 30); // Bottom left
          }

          // Remove the extra first blank page
          if (mergedPdf.internal.getNumberOfPages() > 1) {
            mergedPdf.deletePage(1);
          }

          // Generate the final PDF as an ArrayBuffer
          const mergedPdfBytes = mergedPdf.output("arraybuffer");

          // Prepare the PDF for upload
          const formData = new FormData();
          formData.append(
            "file",
            new Blob([mergedPdfBytes], { type: "application/pdf" }),
            "merged_document.pdf"
          );

          // Upload the PDF to the server
          const response = await axios.post(
            "https://api.brokeragentbase.com/upload",
            formData
          );

          setPdfPath(response.data);

          const attachType =
            acceptedFileTypes.length > 0
              ? `${fileExtension} (${acceptedFileTypes.join(", ")})`
              : fileExtension;

          const userData = {
            document: response.data,
            attachType: attachType,
            transactionId: params.id,
            documentType: "customadditional",
            documentName: documentcustomname,
            agentId: jwt(localStorage.getItem("auth")).id,
          };

          setLoader({ isActive: true });
          const uploadResponse = await user_service.uploadDocuments(userData);

          if (uploadResponse) {
            const splitValues = documentcustomname.split("_");
            const userDatan = {
              addedBy: jwt(localStorage.getItem("auth")).id,
              agentId: jwt(localStorage.getItem("auth")).id,
              transactionId: params.id,
              message: `Document Added - ${splitValues.join("  ")}.`,
              note_type: "Document",
            };

            await user_service.transactionNotes(userDatan);
            setLoader({ isActive: false });
            setToaster({
              type: "Document Added Successfully",
              isShow: true,
              toasterBody: response.data.message,
              message: "Document Added Successfully",
            });

            setTimeout(() => {
              setToaster((prevToaster) => ({
                ...prevToaster,
                isShow: false,
              }));
            }, 2000);

            // Reset form and close modals
            setStep1("");
            setStep2("");
            setStep3("");
            setFile(null);
            setIsImage("");
            setImage("");
            setImageResponce("");
            setFileExtension("");
            setDocumentType("");
            setAcceptedFileTypes([]);
            transactionDocumentAll();
            TransactionGetById(params.id);
            setIssubmit(false);
            document.getElementById("closeModal").click();
            document.getElementById("closeModalAttach").click();
          }
        } catch (error) {
          console.log("Error occurred:", error);
          setIssubmit(false);
        }
      } else {
        console.log("Invalid file or selected page.");
        setIssubmit(false);
      }
    } else {
      setDocumentcustomnameerror(true);
    }
  };

  const handleClickAdditional = () => {
    setStep1("");
    setStep2("");
    setDocumentcustomname("");
    setSelectedSinglePage([]);
    setSelectAllPagesNew([]);
  };

  const convertImageToPDF = async (imageData) => {
    try {
      const pdf = await PDFDocument.create();
      const page = pdf.addPage();

      let img;
      if (imageData.startsWith("data:image/png")) {
        console.log("Image data URL is a valid PNG.");
        img = await pdf.embedPng(imageData);
      } else if (imageData.startsWith("data:image/jpeg")) {
        console.log("Image data URL is a valid JPEG.");
        img = await pdf.embedJpg(imageData);
      } else {
        throw new Error("The input is not a PNG or JPEG file!");
      }

      // Calculate the width and height of the image to fit the page
      const pageWidth = page.getWidth();
      const pageHeight = page.getHeight();
      const imgWidth = img.width;
      const imgHeight = img.height;
      const scaleFactor = Math.min(
        pageWidth / imgWidth,
        pageHeight / imgHeight
      );

      // Draw the image on the page
      page.drawImage(img, {
        x: (pageWidth - imgWidth * scaleFactor) / 2,
        y: (pageHeight - imgHeight * scaleFactor) / 2,
        width: imgWidth * scaleFactor,
        height: imgHeight * scaleFactor,
      });

      // Save the PDF as a Blob
      const pdfBytes = await pdf.save();
      return new Blob([pdfBytes], { type: "application/pdf" });
    } catch (error) {
      console.error("Error converting image to PDF:", error.message);
      return null;
    }
  };

  const handleImageAttch = async () => {
    if (!file || !image) {
      console.log("Invalid file or selected page.");
      return;
    }

    const selectedImageElement = document.getElementById("selected_Attach");
    if (!selectedImageElement) {
      console.error("Element with ID 'selected_Image' not found.");
      return;
    }

    const canvas = await html2canvas(selectedImageElement);

    if (!canvas) {
      console.error("Failed to create canvas.");
      return;
    }

    const imageDataURL = canvas.toDataURL(
      file.type === "image/png" ? "image/png" : "image/jpeg"
    );
    console.log("Generated imageDataURL:", imageDataURL);

    if (
      !imageDataURL.startsWith("data:image/png") &&
      !imageDataURL.startsWith("data:image/jpeg")
    ) {
      console.error(
        "Generated imageDataURL is not a valid PNG or JPEG data URL."
      );
      return;
    }

    const pdfBlob = await convertImageToPDF(imageDataURL);
    if (!pdfBlob) {
      console.log("PDF conversion failed.");
      return;
    }

    const formData = new FormData();
    formData.append("file", pdfBlob, "converted.pdf");

    try {
      const response = await axios.post(
        "https://api.brokeragentbase.com/upload",
        // "http://localhost:4000/upload",
        formData,
        {
          headers: {
            "Content-Type": "multipart/form-data",
          },
        }
      );

      console.log(response.data);
      setImageResponce(response.data);

      if (response.data) {
        const attachType =
          acceptedFileTypes.length > 0
            ? `${fileExtension} (${acceptedFileTypes.join(", ")})`
            : fileExtension;
        const userData = {
          document: response.data,
          attachType: attachType,
          transactionId: params.id,
          documentType: "image",
          documentName: "image",
          agentId: jwt(localStorage.getItem("auth")).id,
        };

        try {
          const response = await user_service.uploadDocuments(userData);
          if (response) {
            const splitValues = selectedConditionname.split("_"); // ["buyer", "partner"]
            const userDatan = {
              addedBy: jwt(localStorage.getItem("auth")).id,
              agentId: jwt(localStorage.getItem("auth")).id,
              transactionId: params.id,
              message: `Document Added - ${splitValues.join("  ")}.`,
              // transaction_property_address: "ADDRESS 2",
              note_type: "Document",
            };

            const responsen = user_service.transactionNotes(userDatan);

            setStep1("");
            setStep2("");
            setStep3("");
            setFile(null);
            setIsImage("");
            setImage("");
            setImageResponce("");
            setFileExtension("");
            setDocumentType("");
            setAcceptedFileTypes([]);
            transactionDocumentAll();
            TransactionGetById(params.id);
            document.getElementById("closeModal").click();
            document.getElementById("closeModalAttach").click();
          }
        } catch (error) {
          console.log("Error occurred while uploading documents:", error);
        }
      }
    } catch (error) {
      console.error("Error uploading image:", error);
    }
  };

  const handleImageSubmit = async () => {
    if (!file || !image) {
      console.log("Invalid file or selected page.");
      return;
    }

    const selectedImageElement = document.getElementById("selected_Image");
    if (!selectedImageElement) {
      console.error("Element with ID 'selected_Image' not found.");
      return;
    }

    const canvas = await html2canvas(selectedImageElement);

    if (!canvas) {
      console.error("Failed to create canvas.");
      return;
    }

    const imageDataURL = canvas.toDataURL(
      file.type === "image/png" ? "image/png" : "image/jpeg"
    );
    console.log("Generated imageDataURL:", imageDataURL);

    if (
      !imageDataURL.startsWith("data:image/png") &&
      !imageDataURL.startsWith("data:image/jpeg")
    ) {
      console.error(
        "Generated imageDataURL is not a valid PNG or JPEG data URL."
      );
      return;
    }

    const pdfBlob = await convertImageToPDF(imageDataURL);
    if (!pdfBlob) {
      console.log("PDF conversion failed.");
      return;
    }

    const formData = new FormData();
    formData.append("file", pdfBlob, "converted.pdf");

    try {
      const response = await axios.post(
        "https://api.brokeragentbase.com/upload",
        // "http://localhost:4000/upload",
        formData,
        {
          headers: {
            "Content-Type": "multipart/form-data",
          },
        }
      );

      setImageResponce(response.data);

      if (response.data) {
        const attachType =
          acceptedFileTypes.length > 0
            ? `${fileExtension} (${acceptedFileTypes.join(", ")})`
            : fileExtension;
        const userData = {
          document: response.data,
          attachType: attachType,
          transactionId: params.id,
          documentType: selectedConditionname,
          documentName: selectedConditionname,
          agentId: jwt(localStorage.getItem("auth")).id,
        };

        try {
          const response = await user_service.uploadDocuments(userData);
          if (response) {
            const splitValues = selectedConditionname.split("_"); // ["buyer", "partner"]
            const userDatan = {
              addedBy: jwt(localStorage.getItem("auth")).id,
              agentId: jwt(localStorage.getItem("auth")).id,
              transactionId: params.id,
              message: `Document Added - ${splitValues.join("  ")}.`,
              // transaction_property_address: "ADDRESS 2",
              note_type: "Document",
            };

            const responsen = user_service.transactionNotes(userDatan);

            setStep1("");
            setStep2("");
            setStep3("");
            setFile(null);
            setIsImage("");
            setImage("");
            setImageResponce("");
            setFileExtension("");
            setDocumentType("");
            setAcceptedFileTypes([]);
            transactionDocumentAll();
            TransactionGetById(params.id);
            document.getElementById("closeModal").click();
            document.getElementById("closeModalAttach").click();
          }
        } catch (error) {
          console.log("Error occurred while uploading documents:", error);
        }
      }
    } catch (error) {
      console.error("Error uploading image:", error);
    }
  };

  const handleBackStep = () => {
    setStep1("");
    setStep2("");
    setStep3("");
  };

  const handleBack = () => {
    setStep1("");
    setStep2("");
    setIsImage("");
    setImage("");
    setImageResponce("");
    setFile(null);
    setFileExtension("");
    setDocumentType("");
    setAcceptedFileTypes([]);
    transactionDocumentAll();
    document.getElementById("closeModal").click();
    document.getElementById("closeModalDetailadditional").click();
    document.getElementById("closeModalAttach").click();
    document.getElementById("closeModalAssign").click();
  };

  const DetailsaddeddataNew = async () => {
    const agentId = jwt(localStorage.getItem("auth")).id;
    await user_service.Detailsaddeddata(agentId, params.id).then((response) => {
      setLoader({ isActive: false });
      if (response) {
        // const requiredYesArray = response.data.data.filter(
        //   (item) => item.is_documentRequired === "yes"
        // );

        const requiredYesArray = response.data.data.filter((item) => {
          return item.is_documentRequired === "yes" && item.transactionDetails;
        });

        // const groupedBycategory = response.data.data.reduce((acc, item) => {
        //     const category = item.category;
        //     if (!acc[category]) {
        //         acc[category] = [];
        //     }
        //     acc[category].push(item);
        //     return acc;
        // }, {});

        setRequiredDetailsaddeddata(requiredYesArray);
        // setDetailsaddeddata(groupedBycategory);
      }
    });
  };

  // details question related document

  const newImagedata = (id, conditionname, details) => {
    setTransactionDetail(id);
    setSelectedConditionname(conditionname);
    setDocumentDetails(details);
    // console.log(conditionname);
    setStep1("");
    setStep2("");
  };

  const handleCopyUrl = (contentId, resource_url) => {
    navigator.clipboard
      .writeText(resource_url)
      .then(() => {
        console.log("URL copied to clipboard");
        setCopySuccessMap((prevMap) => ({
          ...prevMap,
          [contentId]: true,
        }));

        // Reset the copy success message after a short delay
        setTimeout(() => {
          setCopySuccessMap((prevMap) => ({
            ...prevMap,
            [contentId]: false,
          }));
        }, 2000);
      })
      .catch((error) => {
        console.error("Error copying URL to clipboard", error);
        // Handle the error or show an error message to the user
      });
  };

  // Custom Additional document

  const handleImageSubmitAdditional = async () => {
    if (!file || !image) {
      console.log("Invalid file or selected page.");
      return;
    }

    if (documentcustomname && documentcustomname != "") {
      const canvas = await html2canvas(
        document.getElementById("selected_Images")
      );
      const imageDataURL = canvas.toDataURL("image/png");
      console.log(imageDataURL);
      const pdfBlob = await convertImageToPDF(imageDataURL);
      if (!pdfBlob) {
        console.log("PDF conversion failed.");
        return;
      }

      const formData = new FormData();
      formData.append("file", pdfBlob, "converted.pdf");

      try {
        const response = await axios.post(
          "https://api.brokeragentbase.com/upload",
          // "http://localhost:4000/upload",
          formData,
          {
            headers: {
              "Content-Type": "multipart/form-data",
            },
          }
        );

        setImageResponce(response.data);

        const userDatadoc = {
          document: response.data,
          attachType: "pdf",
          transactionId: params.id,
          documentType: "customadditional",
          documentName: documentcustomname,
          // details_questionid: transactionDetail,
          agentId: jwt(localStorage.getItem("auth")).id,
        };
        try {
          const response = await user_service.uploadDocuments(userDatadoc);
          if (response) {
            const splitValues = documentcustomname.split("_"); // ["buyer", "partner"]
            const userDatan = {
              addedBy: jwt(localStorage.getItem("auth")).id,
              agentId: jwt(localStorage.getItem("auth")).id,
              transactionId: params.id,
              message: `Additional Document Added - ${splitValues.join("  ")}.`,
              // transaction_property_address: "ADDRESS 2",
              note_type: "Document",
            };

            const responsen = user_service.transactionNotes(userDatan);

            DetailsaddeddataNew();
            setStep1("");
            setStep2("");
            setFile(null);
            setFileExtension("");
            setDocumentType("");
            setAcceptedFileTypes([]);
            transactionDocumentAll();
            TransactionGetById(params.id);
            document.getElementById("closeModalDetailadditional").click();
            document.getElementById("closeModal").click();
            document.getElementById("closeModalAttach").click();
          }
        } catch (error) {
          console.log("Error occurred while uploading documents:", error);
        }
      } catch (error) {
        console.error("Error uploading image:", error);
      }
    } else {
      setDocumentcustomnameerror(true);
    }
  };

  const handledocumentcustomname = (e) => {
    const { name, value } = e.target;
    setDocumentcustomname(value);
  };

  const [checkboxState, setCheckboxState] = useState({});

  const handleCheckboxChange = (itemId) => {
    setCheckboxState((prevState) => {
      const newState = { ...prevState };
      newState[itemId] = !newState[itemId];
      if (!newState[itemId]) {
        delete newState[itemId];
      }
      return newState;
    });
  };

  const clearcheckboxState = () => {
    setCheckboxState({});
  };

  const handleRemovedoc = async (id, documentUrl, documentName) => {
    Swal.fire({
      title: "Are you sure?",
      text: "It will be permanently deleted!",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Yes, delete it!",
    }).then(async (result) => {
      if (result.isConfirmed) {
        try {
          const splitValues = documentName.split("_");
          const userDatan = {
            addedBy: jwt(localStorage.getItem("auth")).id,
            agentId: jwt(localStorage.getItem("auth")).id,
            transactionId: params.id,
            message: "Document Removed",
            note_type: "Document",
            noteName: splitValues.join("  "),
            seenby: documentUrl,
          };

          const responsen = user_service.transactionNotes(userDatan);
          const updateData = {
            approvalStatus: "Reject",
            is_active: "no",
          };

          setLoader({ isActive: true });
          await user_service
            .transactionDocumentUpdate(id, updateData)
            .then((response) => {
              if (response) {
                Swal.fire(
                  "Deleted!",
                  "Document is deleted permanently.",
                  "success"
                );

                DetailsaddeddataNew();
                setStep1("");
                setStep2("");
                setFile(null);
                setFileExtension("");
                setDocumentType("");
                setAcceptedFileTypes([]);
                transactionDocumentAll();
                TransactionGetById(params.id);
                setIssubmit(false);
              }
            });
        } catch (error) {
          Swal.fire(
            "Error",
            "An error occurred while deleting the file.",
            "error"
          );
          console.error(error);
        }
      }
    });
  };

  const handleNewDocumentCreate = () => {
    if (selectAllPagesNew.length > 0 || selectedSinglePage.length > 0) {
      setCreateDocumets(true);
    } else {
      setCreateDocumets(false);
    }
  };

  const [selectAttachCondition, setSelectAttachCondition] = useState("");
  const [selectAttachConditionname, setSelectAttachConditionname] =
    useState("");

  const handleAttachUpload = (condition, conditionname) => {
    setSelectAttachCondition(condition);
    setSelectAttachConditionname(conditionname);
    setAttachDocumets(true);
  };

  const [noteAdd, setNotesAdd] = useState("");
  const handleChangeAttach = (e) => {
    const { name, value } = e.target;
    setNotesAdd({ ...noteAdd, [name]: value });
  };

  const handleClickAttach = () => {
    setStep1("");
    setStep2("");
    setNotesAdd("");
    setSelectAttachConditionname("");
    setAttachDocumets(false);
    setCreateDocumets(false);
    setDocumentcustomname("");
    setSelectedSinglePage([]);
    setSelectAllPagesNew([]);
  };

  const handleAttachChange = () => {
    setAttachDocumets(false);
  };

  const handleAssignChange = () => {
    setAssignDocumets(false);
  };

  const createemail = async (e) => {
    const userData = {};
    try {
      setLoader({ isActive: true });
      const response = await user_service.TransactionemailUpdate(
        params.id,
        userData
      );
      setLoader({ isActive: false });
      if (response) {
        const userDatan = {
          addedBy: jwt(localStorage.getItem("auth")).id,
          agentId: jwt(localStorage.getItem("auth")).id,
          transactionId: params.id,
          message: "Transaction Email Update",
          // transaction_property_address: "ADDRESS 2",
          note_type: "Property",
        };
        const responsen = await user_service.transactionNotes(userDatan);

        setLoader({ isActive: false });
        setToaster({
          types: "SelectProperty",
          isShow: true,
          toasterBody: response.data.message,
          message: "Email Updated Successfully",
        });
        setTimeout(() => {
          setToaster((prevToaster) => ({
            ...prevToaster,
            isShow: false,
          }));
        }, 2000);
        TransactionGetById(params.id);
      } else {
        setLoader({ isActive: false });
        setToaster({
          types: "error",
          isShow: true,
          toasterBody: response.data.message,
          message: "Error",
        });
      }
    } catch (error) {
      setLoader({ isActive: false });
      setToaster({
        types: "error",
        isShow: true,
        toasterBody: error,
        message: "Error",
      });
    }
  };

  return (
    <div className="bg-secondary float-left w-100 pt-4">
      <Loader isActive={isActive} />
      {isShow && (
        <Toaster
          types={types}
          isShow={isShow}
          toasterBody={toasterBody}
          message={message}
        />
      )}

      <main className="page-wrapper">
        {/* <!-- Page container--> */}
        <div className="">
          <div className="content-overlay transaction_tab_view mt-0">
            <div className="row">
              <AllTab />
              <div className="col-md-12">
                <div className="bg-light border rounded-3 mb-4 p-3">
                  <h3 className="text-left">Transaction Document</h3>
                  <p className="">
                    These are the minimum required documents for compliance, but
                    maintaining complete records for your transactions is
                    essential. Uploading all relevant documents to the
                    transaction file saves time on paperwork, supports
                    regulatory compliance, ensures audit trails, and helps meet
                    client expectations. It also streamlines sharing transaction
                    information and documents with team members, making
                    collaboration easier and more efficient.
                  </p>
                </div>
                {/* <StatusBar /> */}
                <div className="transaction_document bg-light p-3 border-0 rounded-3 shadow-sm float-start w-100">
                  <div className="float-start w-100 d-flex align-items-center justify-content-between mb-2">
                    <h5>Documents</h5>
                  </div>
                  {/* <!-- Primary alert --> */}
                  <div className="float-start w-100 d-lg-flex d-md-flex d-md-flex d-block align-items-center justify-content-between my-lg-3 my-md-3 my-sm-3 my-3 mb-1 p-0">
                    <div className="alert alert-primary w-100 p-3 mb-0">
                      <span id="document">
                        {getrequireddoucmentno.totalDocumentNoRequired &&
                        getrequireddoucmentno.totalDocumentNoRequired > 0
                          ? `${getrequireddoucmentno.totalDocumentNoRequired} Documents Required Now`
                          : ""}
                      </span>

                      <p className="mb-0" id="text-document-two">
                        To move forward, please use the 'Upload File' button to
                        attach the documents that are required now.
                      </p>
                    </div>
                    <div className="float-start w-100">
                      {summary.filing === "filed_complted" ? (
                        <span className="pull-right">
                          <button
                            type="button"
                            className="btn btn-primary mt-1 float-right"
                            title="Your transaction was completed"
                          >
                            Attach File
                          </button>
                        </span>
                      ) : (
                          <span className="ms-lg-5 ms-md-5 ms-sm-0 ms-0 mt-lg-0 mt-md-0 mt-sm-3 mt-3 float-start w-100">
                          <button
                            type="button"
                            className="btn btn-primary mt-1 float-right"
                            data-toggle="modal"
                            onClick={handleClickAttach}
                            data-target="#attach_file"
                          >
                            Attach File
                          </button>
                        </span>
                      )}
                    </div>
                  </div>
                  <div className="float-left w-100 d-lg-flex d-md-flex d-md-flex d-block align-items-center my-lg-3 my-md-3 my-sm-2 my-2">
                    {Object.keys(checkboxState).length > 0 ? (
                      <span className="pull-left me-4">
                        <button
                          type="button"
                          className="btn btn-primary mt-lg-3 mt-md-3 mt-sm-3 mt-0 float-left"
                          data-toggle="modal"
                          data-target="#SendSelectedDocumentsmodal"
                        >
                          Send Selected Documents
                        </button>
                      </span>
                    ) : (
                      <span className="pull-left me-4">
                        <OverlayTrigger
                          placement="right"
                          overlay={
                            <Tooltip>
                              Please check the document checkboxes before
                              sending
                            </Tooltip>
                          }
                        >
                          <button
                            type="button"
                            className="btn btn-primary mt-1 float-left"
                          >
                            Send Selected Documents
                          </button>
                        </OverlayTrigger>
                      </span>
                    )}
                    {summary.filing === "filed_complted" ? (
                      <span className="pull-left">
                        <button
                          type="button"
                          className="btn btn-primary mt-lg-0 mt-md-0 mt-sm-0 mt-2 float-left"
                          title="Your transaction was completed"
                        >
                          Upload Additional Documents
                        </button>
                      </span>
                    ) : (
                      <span className="pull-left">
                        <button
                          type="button"
                          className="btn btn-primary mt-lg-0 mt-md-0 mt-sm-0 mt-2 float-left"
                          data-toggle="modal"
                          onClick={handleClickAdditional}
                          data-target="#addDocumentAdditional"
                        >
                          Upload Additional Documents
                        </button>
                      </span>
                    )}
                  </div>

                  {customdocumentaddeddata.length > 0 ? (
                    <div
                      className="accordion float-start w-100 mt-3"
                      id="accordionExample"
                    >
                      <div className="accordion-item">
                        <h2 className="accordion-header" id="headingRequired">
                          <button
                            className="accordion-button"
                            type="button"
                            data-bs-toggle="collapse"
                            data-bs-target="#collapseRequired"
                            aria-expanded="true"
                            aria-controls="collapseRequired"
                          >
                            Additional Documents (
                            {customdocumentaddeddata.length})
                          </button>
                        </h2>
                        <div
                          id="collapseRequired"
                          className="accordion-collapse collapse show"
                          aria-labelledby="headingRequired"
                          data-bs-parent="#accordionExample"
                        >
                          <div className="accordion-body">
                            <div className="row">
                              {customdocumentaddeddata.map((item) => {
                                const isChecked =
                                  checkboxState[item._id] || false;
                                const isImage = /\.(png|jpg|jpeg)$/i.test(
                                  item.document
                                );
                                return (
                                  <div className="col-md-2">
                                    <div
                                      className="card border rounded-3 p-3 shadow"
                                      key={item._id}
                                    >
                                      <div className="document_actions d-flex align-items-center justify-content-between">
                                        <input
                                          type="checkbox"
                                          id={`checkbox-${item._id}`}
                                          name={`checkbox-${item._id}`}
                                          checked={isChecked}
                                          onChange={() =>
                                            handleCheckboxChange(item._id)
                                          }
                                        />
                                        <div className="dropdown">
                                          <button
                                            className="border-0 dropdown-toggle bg-transparent pe-0"
                                            type="button"
                                            id="dropdownMenuButton"
                                            data-bs-toggle="dropdown"
                                            aria-expanded="false"
                                          >
                                            <i class="h2 fi-dots-vertical opacity-80 mb-0"></i>
                                          </button>
                                          <ul
                                            className="dropdown-menu"
                                            aria-labelledby="dropdownMenuButton"
                                          >
                                            {item.document ? (
                                              <li>
                                                <a
                                                  href={item.document}
                                                  target="_blank"
                                                  rel="noopener noreferrer"
                                                  className="dropdown-item"
                                                >
                                                  <i className="fas fa-download"></i>{" "}
                                                  Download
                                                </a>
                                              </li>
                                            ) : (
                                              ""
                                            )}

                                            {localStorage.getItem("auth") &&
                                            (jwt(localStorage.getItem("auth"))
                                              .contactType === "admin" ||
                                              jwt(localStorage.getItem("auth"))
                                                .contactType ===
                                                "associate") ? (
                                              <li>
                                                <a
                                                  className="dropdown-item"
                                                  onClick={() =>
                                                    handleRemovedoc(
                                                      item._id,
                                                      item.document,
                                                      item.documentName
                                                    )
                                                  }
                                                >
                                                  <i className="fas fa-trash"></i>{" "}
                                                  Delete Document
                                                </a>
                                              </li>
                                            ) : (
                                              ""
                                            )}

                                            {/* <li>
                                              <a
                                                className="dropdown-item"
                                                onClick={() => {
                                                }}
                                              >
                                                <i className="fas fa-exclamation-circle"></i>{" "}
                                                Request Exemption
                                              </a>
                                            </li> */}
                                          </ul>
                                        </div>
                                      </div>
                                      {/* {copySuccessMap[item._id] && (
                                        <i className="fi-check text-success mt-2"></i>
                                      )} */}
                                      <div className="document_action document_action text-start">
                                        {item.document ? (
                                          <a
                                            href={item.document}
                                            target="_blank"
                                            rel="noopener noreferrer"
                                            className="mt-2 d-block"
                                          >
                                            <div className="w-100">
                                              {isImage ? (
                                                <img
                                                  src={item.document}
                                                  alt="Image"
                                                  style={{
                                                    width: "100px",
                                                    height: "115px",
                                                  }}
                                                />
                                              ) : (
                                                <Document
                                                  file={item.document}
                                                  className="document-preview"
                                                >
                                                  <Page
                                                    pageNumber={1}
                                                    width={180}
                                                  />
                                                </Document>
                                              )}
                                            </div>
                                          </a>
                                        ) : (
                                          <div
                                            className="upload-placeholder d-flex align-items-center justify-content-center"
                                            style={{
                                              height: "150px",
                                              border: "1px dashed #ccc",
                                            }}
                                          >
                                            <p className="text-muted">Upload</p>
                                          </div>
                                        )}

                                        <p className="float-start w-100 my-2 text-start">
                                          <span class="ms-0">
                                            {item.documentName
                                              .charAt(0)
                                              .toUpperCase() +
                                              item.documentName.slice(1)}
                                          </span>
                                        </p>
                                        {item.created ? (
                                          <p className="float-start w-100 mb-0 text-start">
                                            {moment(item.created).format(
                                              "MMMM DD, YYYY"
                                            )}
                                          </p>
                                        ) : (
                                          ""
                                        )}
                                      </div>
                                      <div className="float-left w-100 text-start">
                                        <button
                                          className="btn btn-primary p-2"
                                          style={{
                                            color: "#445985",
                                            fontSize: "12px",
                                            backgroundColor:
                                              item.approvalStatus === "Yes"
                                                ? "#9DCD75"
                                                : "#D0E9F9",
                                          }}
                                        >
                                          {item.approvalStatus === "Yes"
                                            ? "Approved"
                                            : "Uploaded"}
                                          &nbsp;
                                          <i
                                            className="fa fa-clone"
                                            onClick={() =>
                                              handleCopyUrl(
                                                item._id,
                                                item.document
                                              )
                                            }
                                          ></i>
                                          {copySuccessMap[item._id] && (
                                            <i className="fi-check text-success ms-2"></i>
                                          )}
                                        </button>
                                      </div>
                                    </div>
                                  </div>
                                );
                              })}
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  ) : (
                    ""
                  )}

                  <div className="accordion-body float-start w-100">
                    <div className="row">
                      {Object.entries(rulesdata).map(
                        ([transactionPhase, items]) => {
                          if (
                            items.some(
                              (post_rule) =>
                                post_rule.transactionType === transactiontype
                            )
                          ) {
                            return (
                              <React.Fragment key={transactionPhase}>
                                <h6 className="float-left w-100 mt-3 zdfvgzdfgzfd">
                                  {transactionPhase
                                    .replace(/-/g, " ")
                                    .charAt(0)
                                    .toUpperCase() + transactionPhase.slice(1)}
                                </h6>

                                {items
                                  .filter(
                                    (post_rule) =>
                                      post_rule.transactionType ===
                                      transactiontype
                                  )
                                  .map((post_rule, index) => {
                                    const conditionWheredata = JSON.parse(
                                      post_rule.conditionWhere
                                    );
                                    return (
                                      <div className="col-md-2">
                                        {conditionWheredata &&
                                          conditionWheredata.length > 0 &&
                                          conditionWheredata.map(
                                            (
                                              conditionWhere_rule,
                                              innerIndex
                                            ) => {
                                              const foundDocument =
                                                documentValues.find((doc) => {
                                                  return (
                                                    doc.documentType ===
                                                      conditionWhere_rule.value &&
                                                    doc.is_active === "yes"
                                                  );
                                                });

                                              const isChecked =
                                                foundDocument &&
                                                foundDocument._id
                                                  ? checkboxState[
                                                      foundDocument._id
                                                    ]
                                                  : false;

                                              let display = true;
                                              if (
                                                conditionWhere_rule.value ===
                                                "Lead-Based_Paint_Disclosure_and_Acknowledgement"
                                              ) {
                                                if (
                                                  summary.propert_built &&
                                                  summary.propert_built ===
                                                    "yes"
                                                ) {
                                                  display = true;
                                                } else {
                                                  display = false;
                                                }
                                              }
                                              if (display === true) {
                                                return (
                                                  <div className="card border rounded-3 p-3 shadow mb-lg-0 mb-md-0 mb-sm-0 mb-3">
                                                    <div
                                                      className=""
                                                      key={index}
                                                    >
                                                      <React.Fragment
                                                        key={innerIndex}
                                                      >
                                                        <div className="document_action document_action float-start w-100">
                                                          {documentValues.some(
                                                            (doc) =>
                                                              doc.documentType ===
                                                                conditionWhere_rule.value &&
                                                              doc.is_active ===
                                                                "yes"
                                                          ) ? (
                                                            <>
                                                              {documentValues.map(
                                                                (doc, idx) => {
                                                                  if (
                                                                    doc.documentType ===
                                                                      conditionWhere_rule.value &&
                                                                    doc.is_active ===
                                                                      "yes"
                                                                  ) {
                                                                    const fileExtension =
                                                                      doc.document
                                                                        .split(
                                                                          "."
                                                                        )
                                                                        .pop()
                                                                        .toLowerCase();

                                                                    return (
                                                                      <React.Fragment
                                                                        key={
                                                                          idx
                                                                        }
                                                                      >
                                                                        <div className="">
                                                                          <div className="document_actions d-flex align-items-center justify-content-between">
                                                                            {foundDocument && (
                                                                              <input
                                                                                className="pull-left mt-0 me-2"
                                                                                type="checkbox"
                                                                                id={`checkbox-${foundDocument._id}`}
                                                                                name={`checkbox-${foundDocument._id}`}
                                                                                checked={
                                                                                  isChecked
                                                                                }
                                                                                onChange={() =>
                                                                                  handleCheckboxChange(
                                                                                    foundDocument._id
                                                                                  )
                                                                                }
                                                                              />
                                                                            )}

                                                                            <div className="dropdown">
                                                                              <button
                                                                                className="border-0 dropdown-toggle bg-transparent pe-0"
                                                                                type="button"
                                                                                id="dropdownMenuButton"
                                                                                data-bs-toggle="dropdown"
                                                                                aria-expanded="false"
                                                                              >
                                                                                <i class="h2 fi-dots-vertical opacity-80 mb-0"></i>
                                                                              </button>
                                                                              <ul
                                                                                className="dropdown-menu"
                                                                                aria-labelledby="dropdownMenuButton"
                                                                              >
                                                                                <li>
                                                                                  <a
                                                                                    href={
                                                                                      doc.document
                                                                                    }
                                                                                    target="_blank"
                                                                                    rel="noopener noreferrer"
                                                                                    className="dropdown-item"
                                                                                  >
                                                                                    <i className="fas fa-download"></i>{" "}
                                                                                    Download
                                                                                  </a>
                                                                                </li>

                                                                                {localStorage.getItem(
                                                                                  "auth"
                                                                                ) &&
                                                                                (jwt(
                                                                                  localStorage.getItem(
                                                                                    "auth"
                                                                                  )
                                                                                )
                                                                                  .contactType ===
                                                                                  "admin" ||
                                                                                  jwt(
                                                                                    localStorage.getItem(
                                                                                      "auth"
                                                                                    )
                                                                                  )
                                                                                    .contactType ===
                                                                                    "associate") ? (
                                                                                  <li>
                                                                                    {summary.filing ===
                                                                                    "filed_complted" ? (
                                                                                      <a title="Your transaction was completed">
                                                                                        <i className="fas fa-trash"></i>
                                                                                        Delete
                                                                                        Document
                                                                                      </a>
                                                                                    ) : (
                                                                                      <a
                                                                                        className="dropdown-item"
                                                                                        onClick={(
                                                                                          e
                                                                                        ) =>
                                                                                          handleRemovedoc(
                                                                                            doc._id,
                                                                                            doc.document,
                                                                                            doc.documentName
                                                                                          )
                                                                                        }
                                                                                      >
                                                                                        <i className="fas fa-trash"></i>
                                                                                        Delete
                                                                                        Document
                                                                                      </a>
                                                                                    )}
                                                                                  </li>
                                                                                ) : (
                                                                                  ""
                                                                                )}
                                                                              </ul>
                                                                            </div>
                                                                          </div>
                                                                          <div>
                                                                            {doc.approvalStatus ===
                                                                            "yes" ? (
                                                                              ""
                                                                            ) : (
                                                                              <div className="float-start w-100 text-start">
                                                                                <a
                                                                                  className="pull-left mr-3"
                                                                                  href={
                                                                                    doc.document
                                                                                  }
                                                                                  target="_blank"
                                                                                >
                                                                                  {fileExtension ===
                                                                                  "pdf" ? (
                                                                                    <div className="w-100">
                                                                                      <Document
                                                                                        file={
                                                                                          doc.document
                                                                                        }
                                                                                        renderMode="canvas"
                                                                                      >
                                                                                        <Page
                                                                                          pageNumber={
                                                                                            1
                                                                                          }
                                                                                        />
                                                                                      </Document>
                                                                                    </div>
                                                                                  ) : (
                                                                                    <img
                                                                                      className="document_file"
                                                                                      src={
                                                                                        doc.document
                                                                                      }
                                                                                      alt="Document Preview"
                                                                                      style={{
                                                                                        width:
                                                                                          "100px",
                                                                                        height:
                                                                                          "115px",
                                                                                      }}
                                                                                    />
                                                                                  )}
                                                                                </a>
                                                                              </div>
                                                                            )}

                                                                            <div className="document_title">
                                                                              <OverlayTrigger
                                                                                placement="right"
                                                                                overlay={
                                                                                  <Tooltip>
                                                                                    {
                                                                                      conditionWhere_rule.label
                                                                                    }
                                                                                  </Tooltip>
                                                                                }
                                                                              >
                                                                                {conditionWhere_rule ? (
                                                                                  <p className="float-start w-100 my-2 text-start">
                                                                                    <span className="ms-0">
                                                                                      {
                                                                                        conditionWhere_rule.label
                                                                                      }
                                                                                    </span>
                                                                                  </p>
                                                                                ) : (
                                                                                  ""
                                                                                )}
                                                                              </OverlayTrigger>

                                                                              {doc.created ? (
                                                                                <p className="float-start w-100 mb-0 text-start">
                                                                                  {moment(
                                                                                    doc.created
                                                                                  ).format(
                                                                                    "MMMM DD, YYYY"
                                                                                  )}
                                                                                </p>
                                                                              ) : (
                                                                                ""
                                                                              )}
                                                                              {documentValues.some(
                                                                                (
                                                                                  doc
                                                                                ) =>
                                                                                  doc.documentType ===
                                                                                    conditionWhere_rule.value &&
                                                                                  doc.approvalStatus ===
                                                                                    "Reject"
                                                                              ) ? (
                                                                                <>
                                                                                  {documentValues.map(
                                                                                    (
                                                                                      doc,
                                                                                      idx
                                                                                    ) => {
                                                                                      if (
                                                                                        doc.documentType ===
                                                                                          conditionWhere_rule.value &&
                                                                                        doc.approvalStatus ===
                                                                                          "Reject"
                                                                                      ) {
                                                                                        return (
                                                                                          <React.Fragment
                                                                                            key={
                                                                                              idx
                                                                                            }
                                                                                          >
                                                                                            {doc.is_active ===
                                                                                            "no" ? (
                                                                                              <>

                                                                                              </>
                                                                                            ) : (
                                                                                              <p className="float-start text-start w-100 mt-0 mb-2">
                                                                                                {doc.attachNote ? (
                                                                                                  <>
                                                                                                    <OverlayTrigger
                                                                                                      placement="right"
                                                                                                      overlay={
                                                                                                        <Tooltip>
                                                                                                          {
                                                                                                            doc.attachNote
                                                                                                          }
                                                                                                        </Tooltip>
                                                                                                      }
                                                                                                    >
                                                                                                      <div className="w-100">
                                                                                                        <p>
                                                                                                          <b>
                                                                                                            Note:
                                                                                                          </b>
                                                                                                          {
                                                                                                            doc.attachNote
                                                                                                          }
                                                                                                        </p>
                                                                                                      </div>
                                                                                                    </OverlayTrigger>
                                                                                                  </>
                                                                                                ) : (
                                                                                                  ""
                                                                                                )}
                                                                                              </p>
                                                                                            )}
                                                                                          </React.Fragment>
                                                                                        );
                                                                                      }
                                                                                      return null;
                                                                                    }
                                                                                  )}
                                                                                </>
                                                                              ) : null}
                                                                            </div>
                                                                          </div>

                                                                          <div className="float-start w-100">
                                                                            <span className="float-start w-100 text-start status_tag top-0 ps-2">
                                                                              <small className="p-0">
                                                                                {doc.attachNote
                                                                                  ? doc.approvalStatus ===
                                                                                    "Reject"
                                                                                    ? "Denied"
                                                                                    : ""
                                                                                  : ""}
                                                                              </small>
                                                                            </span>

                                                                            <div className="float-left w-100 text-start">
                                                                              <button
                                                                                className="btn btn-primary p-2"
                                                                                style={{
                                                                                  color:
                                                                                    "#445985",
                                                                                  fontSize:
                                                                                    "12px",
                                                                                  backgroundColor:
                                                                                    doc.approvalStatus ===
                                                                                    "Yes"
                                                                                      ? "#9DCD75"
                                                                                      : "#D0E9F9",
                                                                                }}
                                                                              >
                                                                                {doc.approvalStatus ===
                                                                                "Yes"
                                                                                  ? "Approved"
                                                                                  : doc.exemption ===
                                                                                    "yes"
                                                                                  ? "Exempt"
                                                                                  : "Uploaded"}
                                                                                &nbsp;
                                                                                <i
                                                                                  className="fa fa-clone"
                                                                                  onClick={() =>
                                                                                    handleCopyUrl(
                                                                                      idx,
                                                                                      doc.document
                                                                                    )
                                                                                  }
                                                                                ></i>
                                                                                {copySuccessMap[
                                                                                  idx
                                                                                ] && (
                                                                                  <i className="fi-check text-success ms-2"></i>
                                                                                )}
                                                                              </button>
                                                                            </div>
                                                                          </div>
                                                                        </div>
                                                                      </React.Fragment>
                                                                    );
                                                                  }
                                                                }
                                                              )}
                                                            </>
                                                          ) : (
                                                            <>
                                                              {documentValues.some(
                                                                (doc) =>
                                                                  doc.documentType ===
                                                                    conditionWhere_rule.value &&
                                                                  doc.is_active ===
                                                                    "yes"
                                                              ) ? (
                                                                // If the condition is true, render an empty fragment
                                                                <>
                                                                  {documentValues.map(
                                                                    (
                                                                      doc,
                                                                      idx
                                                                    ) => {
                                                                      if (
                                                                        doc.documentType ===
                                                                          conditionWhere_rule.value &&
                                                                        doc.is_active ===
                                                                          "yes"
                                                                      ) {
                                                                        return (
                                                                          <></>
                                                                        ); // Render empty for each matching document
                                                                      }
                                                                      return null;
                                                                    }
                                                                  )}
                                                                </>
                                                              ) : (
                                                                <>
                                                                  <div className="float-start w-100 Upload_transactionDocument">
                                                                    <a
                                                                      className="text-center"
                                                                      data-toggle="modal"
                                                                      data-target="#addDocument"
                                                                      onClick={() =>
                                                                        handleUploadFile(
                                                                          conditionWhere_rule,
                                                                          conditionWhere_rule.value
                                                                        )
                                                                      }
                                                                    >
                                                                      UPLOAD
                                                                    </a>
                                                                    {/* <img
                                                                    src={
                                                                      DocumentImage
                                                                    }
                                                                    width="100"
                                                                    height="100"
                                                                    alt="Default"
                                                                  /> */}
                                                                  </div>

                                                                  <OverlayTrigger
                                                                    placement="right"
                                                                    overlay={
                                                                      <Tooltip>
                                                                        {
                                                                          conditionWhere_rule.label
                                                                        }
                                                                      </Tooltip>
                                                                    }
                                                                  >
                                                                    {conditionWhere_rule ? (
                                                                      <p className="float-start w-100 my-2 text-start">
                                                                        <span className="ms-0">
                                                                          {
                                                                            conditionWhere_rule.label
                                                                          }
                                                                        </span>
                                                                      </p>
                                                                    ) : (
                                                                      ""
                                                                    )}
                                                                  </OverlayTrigger>
                                                                </>
                                                              )}

                                                              {file &&
                                                              documentType ===
                                                                conditionWhere_rule
                                                                  .value
                                                                  .length >
                                                                  0 ? (
                                                                ""
                                                              ) : (
                                                                <div className="text-start float-start w-100">
                                                                  {post_rule.documentSetting ===
                                                                    "required" && (
                                                                    <a
                                                                      className=""
                                                                      data-toggle="modal"
                                                                      data-target="#ModalExemption"
                                                                      onClick={() =>
                                                                        handleExemptionopen(
                                                                          conditionWhere_rule,
                                                                          conditionWhere_rule.value
                                                                        )
                                                                      }
                                                                    >
                                                                      Request
                                                                      Exemption
                                                                    </a>
                                                                  )}
                                                                </div>
                                                              )}

                                                              {documentValues.some(
                                                                (doc) =>
                                                                  doc.documentType ===
                                                                    conditionWhere_rule.value &&
                                                                  doc.approvalStatus ===
                                                                    "Reject"
                                                              ) ? (
                                                                <>
                                                                  {documentValues.map(
                                                                    (
                                                                      doc,
                                                                      idx
                                                                    ) => {
                                                                      if (
                                                                        doc.documentType ===
                                                                          conditionWhere_rule.value &&
                                                                        doc.approvalStatus ===
                                                                          "Reject"
                                                                      ) {
                                                                        // Log the approval status

                                                                        return (
                                                                          <React.Fragment
                                                                            key={
                                                                              idx
                                                                            }
                                                                          >
                                                                            <span className="float-start p-0 w-100 text-start status_tag top-0 ps-2">
                                                                              <small className="p-0">
                                                                                {doc.attachNote
                                                                                  ? doc.approvalStatus ===
                                                                                    "Reject"
                                                                                    ? "Denied"
                                                                                    : ""
                                                                                  : ""}
                                                                              </small>
                                                                            </span>
                                                                          </React.Fragment>
                                                                        );
                                                                      }
                                                                      return null;
                                                                    }
                                                                  )}
                                                                </>
                                                              ) : (
                                                                <span className="status_tag mb-0 text-start mt-2 ps-2">
                                                                  <small className="p-0">
                                                                    {
                                                                      post_rule.documentSetting
                                                                    }
                                                                  </small>
                                                                </span>
                                                              )}
                                                            </>
                                                          )}
                                                        </div>
                                                      </React.Fragment>
                                                    </div>
                                                  </div>
                                                );
                                              }
                                            }
                                          )}
                                      </div>
                                    );
                                  })}
                              </React.Fragment>
                            );
                          }
                          return null;
                        }
                      )}
                    </div>
                  </div>

                  <div className="accordion-body float-start w-100 mt-3">
                    <div className="row">
                      <div className="col-md-2">
                        {transactionFundingRequest?.invoice_url ? (
                          <div class="card border rounded-3 p-3 shadow">
                            <div class="document_action document_action float-start w-100">
                              <div className="w-100">
                                <a
                                  className="pull-left mr-3"
                                  href={transactionFundingRequest?.invoice_url}
                                  target="_blank"
                                  rel="noopener noreferrer"
                                  download=""
                                >
                                  <Document
                                    className="document_file"
                                    file={
                                      transactionFundingRequest?.invoice_url
                                    }
                                    renderMode="canvas"
                                  >
                                    <Page pageNumber={1} />
                                  </Document>
                                </a>
                              </div>
                              <div class="document_title float-start w-100">
                                <p
                                  class="float-start w-100 my-2 text-start"
                                  style={{ color: "#3278BD", fontSize: "13px" }}
                                >
                                  Submit
                                </p>
                                <p
                                  class="float-start w-100 mb-0 text-start"
                                  style={{ fontSize: "13px" }}
                                >
                                  Funding Request
                                </p>
                                <div class="float-start w-100 text-start">
                                  <button
                                    className="btn btn-primary p-2"
                                    style={{
                                      color: "#445985",
                                      backgroundColor:
                                        summary.funding === "complete"
                                          ? "#9DCD75"
                                          : "#D0E9F9",
                                    }}
                                  >
                                    {summary.funding === "complete"
                                      ? "Approved"
                                      : "OnFile"}
                                    &nbsp;<i className="fa fa-clone"></i>
                                  </button>
                                </div>
                              </div>
                            </div>
                          </div>
                        ) : (
                          ""
                        )}
                      </div>

                      <div className="col-md-2">
                        {transactionFundingRequest?.confirm_invoice_url ? (
                          <div class="card border rounded-3 p-3 shadow">
                            <div class="document_action document_action float-start w-100">
                              <div>
                                <a
                                  className="pull-left mr-3"
                                  href={
                                    transactionFundingRequest?.confirm_invoice_url
                                  }
                                  target="_blank"
                                  rel="noopener noreferrer"
                                  download=""
                                >
                                  <div className="w-100">
                                    <Document
                                      className="document_file"
                                      file={
                                        transactionFundingRequest?.confirm_invoice_url
                                      }
                                      renderMode="canvas"
                                    >
                                      <Page pageNumber={1} />
                                    </Document>
                                  </div>
                                </a>
                              </div>
                              <div class="document_title">
                                <p
                                  class="float-start w-100 my-2 text-start"
                                  style={{ color: "#3278BD", fontSize: "13px" }}
                                >
                                  Review Confirmation
                                </p>
                                <p
                                  class="float-start w-100 mb-0 text-start"
                                  style={{ fontSize: "12px" }}
                                >
                                  Funding Confirmation
                                </p>
                              </div>

                              <div class="float-left w-100 text-start">
                                <button
                                  className="btn btn-primary p-2"
                                  style={{
                                    color: "#445985",
                                    backgroundColor:
                                      summary.confirmation === "complete"
                                        ? "#9DCD75"
                                        : "#D0E9F9",
                                  }}
                                >
                                  {summary.confirmation === "complete"
                                    ? "Approved"
                                    : "OnFile"}
                                  &nbsp;<i className="fa fa-clone"></i>
                                </button>
                              </div>
                            </div>
                          </div>
                        ) : (
                          ""
                        )}
                      </div>
                    </div>
                  </div>

                  {unassignedDocumentdata.length > 0 ? (
                    <div className="accordion-body float-start w-100 mt-3">
                      <div className="row">
                        <h5>Un-assigned Document</h5>
                        {summary.transaction_email ? (
                          <>
                            <h6 className="mb-1">Transaction Email Address</h6>
                            <div
                              style={{
                                display: "flex",
                                wordBreak: "break-all",
                              }}
                              className="mt-3"
                            >
                              <i class="h4 fi-mail opacity-100 mb-0 me-2 mt-1"></i>
                              <a className="mt-1" href={`mailto:${summary.transaction_email}`}>
                                {summary.transaction_email}
                              </a>

                              {/* <p style={{ color: "#2372c2" }} className="mt-1">
                                {summary.transaction_email}
                              </p> */}
                              <label
                                className="col-form-label p-0">
                                <i className="h2 fi-help opacity-80 ms-1 mb-0" style={{ cursor: "pointer" }}></i>
                                <span className="tooltiptext">
                                  <div className="col-md-12">
                                    <div className="gray-text transaction_email p-3">
                                      <h6 className="mb-1">How It Works</h6>
                                      <p className="mb-1">Each transaction file is assigned a unique email address, which you can use to send	 transaction documents directly to the file. Here’s how the process works:</p>
                                      <b>1. Forwarding Documents:</b>
                                      <p className="mb-1">Send or forward emails with attached documents to the unique transaction email address.</p>
                                      <b>2. File Processing:</b>
                                      <p className="mb-1">BrokerAgentBase.com will automatically add the attached files to the bottom of the transaction file. However, the documents are not automatically sorted into specific sections of the file.</p>
                                      <b>3. Manual Attachment:</b>
                                      <p className="mb-1">To organize and save the documents within the transaction file, you will need to use the "Manually Attach" link to move the files to the appropriate sections.</p>
                                      <p className="mb-1">This system helps centralize document management while still allowing you full control over organizing and applying the files where needed.</p>
                                    </div>
                                  </div>
                                </span>
                              </label>
                            </div>
                            <div className="mt-2">
                              <h6 className="mb-0 mt-2">
                               How It Works 
                            </h6>
                            <p className="mb-0" style={{textAlign:"justify"}}>
                            Each transaction file is assigned a unique email address, which you can use to send	 transaction documents directly to the file. Here’s how the process works:
                            </p>
                             <b>1. Forwarding Documents:</b>
                            <p className="mb-0" style={{textAlign:"justify"}}>
                              If you have any questions or need assistance
                              completing the form, please don’t hesitate to
                              reach out. Thank you for your prompt attention to
                              this matter.
                            </p>
                             <b>2. File Processing:</b>
                            <p className="mb-0" style={{textAlign:"justify"}}>
                            BrokerAgentBase.com will automatically add the attached files to 
                            the bottom of the transaction file. However,
                            the documents are not automatically sorted into specific sections of the file.</p>

                           <b>3. Manual Attachment:</b>
                            <p className="mb-0"style={{textAlign:"justify"}}>
                            To organize and save the documents within the transaction file, you will need to use the
                             "Manually Attach" link to move the files to the appropriate sections.</p>
                             <p style={{textAlign:"justify"}}>This system helps centralize document management while still allowing you full
                               control over organizing and applying the files where needed.</p>
                            </div>
                          </>
                        ) : (
                          ""
                        )}
                        {unassignedDocumentdata.map((item) => {
                          const isChecked = checkboxState[item._id] || false;
                          const isImage = /\.(png|jpg|jpeg)$/i.test(
                            item.document
                          );
                          return (
                            <div className="col-md-2 mt-2">
                              <div
                                className="card border rounded-3 p-3 shadow"
                                key={item._id}
                              >
                                <div className="document_actions d-flex align-items-center justify-content-between">
                                  <input
                                    type="checkbox"
                                    id={`checkbox-${item._id}`}
                                    name={`checkbox-${item._id}`}
                                    checked={isChecked}
                                    onChange={() =>
                                      handleCheckboxChange(item._id)
                                    }
                                  />
                                  <div className="dropdown">
                                    <button
                                      className="border-0 dropdown-toggle bg-transparent pe-0"
                                      type="button"
                                      id="dropdownMenuButton"
                                      data-bs-toggle="dropdown"
                                      aria-expanded="false"
                                    >
                                      <i class="h2 fi-dots-vertical opacity-80 mb-0"></i>
                                    </button>
                                    <ul
                                      className="dropdown-menu"
                                      aria-labelledby="dropdownMenuButton"
                                    >
                                      {item.document ? (
                                        <li>
                                          <a
                                            href={item.document}
                                            target="_blank"
                                            rel="noopener noreferrer"
                                            className="dropdown-item"
                                          >
                                            <i className="fas fa-download"></i>{" "}
                                            Download
                                          </a>
                                        </li>
                                      ) : (
                                        ""
                                      )}

                                      {/* {localStorage.getItem("auth") &&
                                      (jwt(localStorage.getItem("auth"))
                                        .contactType === "admin" ||
                                        jwt(localStorage.getItem("auth"))
                                          .contactType === "associate") ? (
                                        <li>
                                          <a
                                            className="dropdown-item"
                                            onClick={() =>
                                              handleRemovedoc(
                                                item._id,
                                                item.document,
                                                item.documentName
                                              )
                                            }
                                          >
                                            <i className="fas fa-trash"></i>{" "}
                                            Delete Document
                                          </a>
                                        </li>
                                      ) : (
                                        ""
                                      )} */}
                                    </ul>
                                  </div>
                                </div>
                                <div className="document_action document_action text-start">
                                  {item.document ? (
                                    <a
                                      href={item.document}
                                      target="_blank"
                                      rel="noopener noreferrer"
                                      className="mt-2 d-block"
                                    >
                                      <div className="w-100">
                                        {isImage ? (
                                          <img
                                            src={item.document}
                                            alt="Image"
                                            style={{
                                              width: "100px",
                                              height: "115px",
                                            }}
                                          />
                                        ) : (
                                          <Document
                                            file={item.document}
                                            className="document-preview"
                                          >
                                            <Page pageNumber={1} width={180} />
                                          </Document>
                                        )}
                                      </div>
                                    </a>
                                  ) : (
                                    <div
                                      className="upload-placeholder d-flex align-items-center justify-content-center"
                                      style={{
                                        height: "150px",
                                        border: "1px dashed #ccc",
                                      }}
                                    >
                                      <p className="text-muted">Upload</p>
                                    </div>
                                  )}

                                  <p className="float-start w-100 my-2 text-start">
                                    <span class="ms-0">
                                      {item.documentName
                                        .charAt(0)
                                        .toUpperCase() +
                                        item.documentName.slice(1)}
                                    </span>
                                  </p>
                                  {item.created ? (
                                    <p className="float-start w-100 mb-0 text-start">
                                      {moment(item.created).format(
                                        "MMMM DD, YYYY"
                                      )}
                                    </p>
                                  ) : (
                                    ""
                                  )}
                                </div>
                                <div className="float-left w-100 text-start">
                                  <button
                                    className="btn btn-primary p-2"
                                    style={{
                                      color: "#445985",
                                      fontSize: "12px",
                                      backgroundColor:
                                        item.approvalStatus === "Yes"
                                          ? "#9DCD75"
                                          : "#D0E9F9",
                                    }}
                                    data-toggle="modal"
                                    onClick={() => handleClickAssign(item._id)}
                                    data-target="#assigned_document"
                                  >
                                    Assigned Document
                                  </button>
                                </div>
                              </div>
                            </div>
                          );
                        })}
                      </div>
                    </div>
                  ) : (
                      <div className="float-start w-100 card border rounded-3 p-3 mt-3">
                      {summary.transaction_email ? (
                        <div className="">
                          <h6 className="text-center mb-2">
                            Transaction Email Address
                          </h6>
                            <div className="d-lg-flex d-md-flex d-sm-block d-block justify-content-center">
                              <a className="mt-0" href={`mailto:${summary.transaction_email}`}>
                                {summary.transaction_email}
                              </a>

                              {/* <p style={{ color: "#2372c2" }} className="mt-2">
                            {summary.transaction_email}
                          </p> */}
                              <label
                                className="col-form-label p-0">
                                <i className="h2 fi-help opacity-80 ms-1 mb-0" style={{ cursor: "pointer" }}></i>
                                <span className="tooltiptext">
                                  <div className="col-md-12">
                                    <div className="gray-text transaction_email p-3">
                                      <h6 className="mb-1">How It Works</h6>
                                      <p className="mb-1">Each transaction file is assigned a unique email address, which you can use to send	 transaction documents directly to the file. Here’s how the process works:</p>
                                      <b>1. Forwarding Documents:</b>
                                      <p className="mb-1">Send or forward emails with attached documents to the unique transaction email address.</p>
                                      <b>2. File Processing:</b>
                                      <p className="mb-1">BrokerAgentBase.com will automatically add the attached files to the bottom of the transaction file. However, the documents are not automatically sorted into specific sections of the file.</p>
                                      <b>3. Manual Attachment:</b>
                                      <p className="mb-1">To organize and save the documents within the transaction file, you will need to use the "Manually Attach" link to move the files to the appropriate sections.</p>
                                      <p className="mb-1">This system helps centralize document management while still allowing you full control over organizing and applying the files where needed.</p>
                                    </div>
                                  </div>
                                </span>
                              </label>
                            </div>
                          <div className="mt-2">
                            <h6 className="mb-0 mt-2">
                            How It Works 
                            </h6>
                            <p className="mb-0" style={{textAlign:"justify"}}>
                            Each transaction file is assigned a unique email address, which you can use to send	 transaction documents directly to the file. Here’s how the process works:
                            </p>
                             <b>1. Forwarding Documents:</b>
                            <p className="mb-0" style={{textAlign:"justify"}}>
                              If you have any questions or need assistance
                              completing the form, please don’t hesitate to
                              reach out. Thank you for your prompt attention to
                              this matter.
                            </p>
                             <b>2. File Processing:</b>
                            <p className="mb-0" style={{textAlign:"justify"}}>
                            BrokerAgentBase.com will automatically add the attached files to 
                            the bottom of the transaction file. However,
                            the documents are not automatically sorted into specific sections of the file.</p>

                           <b>3. Manual Attachment:</b>
                            <p className="mb-0"style={{textAlign:"justify"}}>
                            To organize and save the documents within the transaction file, you will need to use the
                             "Manually Attach" link to move the files to the appropriate sections.</p>
                             <p style={{textAlign:"justify"}}>This system helps centralize document management while still allowing you full
                               control over organizing and applying the files where needed.</p>
                             {/* <br/> */}
                          </div>
                        
                        </div>
                      ) : (
                        <div className="text-center my-3">
                          <button
                            className="btn btn-primary btn-sm"
                            type="button"
                            onClick={createemail}
                            title="Click this button to create the transaction email."
                          >
                            Create Email
                          </button>
                        </div>
                      )}
                      <p className="text-center mb-0">
                        No Un-assigned Document
                      </p>
                    </div>
                  )}

                  {/* Modal code */}
                  <div className="modal in" role="dialog" id="attach_file">
                    <div
                      className="modal-dialog modal-lg modal-dialog-scrollable"
                      role="document"
                    >
                      <div className="modal-content mt-5">
                        <div className="modal-header">
                          <h4 className="modal-title">
                            Transaction Add Document
                          </h4>
                          <button
                            className="btn-close"
                            id="closeModalAttach"
                            type="button"
                            data-dismiss="modal"
                            onClick={handleBack}
                            aria-label="Close"
                          ></button>
                        </div>
                        {attachDocumets ? (
                          <div className="float-none w-50 bg-light rounded-3 p-4">
                            <div className="content-overlay">
                              <div className="d-flex">
                                <img
                                  src="https://cache.workspace.lwolf.com/silk/tick.png"
                                  alt=""
                                />
                                &nbsp;
                                <b>
                                  {" "}
                                  You have selected the "
                                  {selectAttachCondition.label}" title.
                                </b>
                                &nbsp;
                                <NavLink onClick={handleAttachChange}>
                                  change
                                </NavLink>
                              </div>
                              <div className="float-left w-100 mt-3">
                                <label className="col-form-label pt-0 mb-0">
                                  Notes Add:
                                </label>
                                <input
                                  className="form-control"
                                  id="inline-form-input"
                                  type="text"
                                  name="attachNote"
                                  value={noteAdd.attachNote}
                                  onChange={handleChangeAttach}
                                />
                              </div>

                              <button
                                className="btn btn-primary mt-3"
                                onClick={handleSubmitAttach}
                                disabled={issubmit}
                              >
                                {issubmit ? "Please Wait..." : "Save Document"}
                              </button>
                            </div>
                          </div>
                        ) : (
                          <>
                            {createDocumets ? (
                              <div className="modal-body fs-sm">
                                <div>
                                  <h6 className="fs-base">
                                    What is the title of the new document?
                                  </h6>
                                </div>
                                {Object.entries(rulesdata).map(
                                  ([transactionPhase, items]) => {
                                    if (
                                      items.some(
                                        (post_rule) =>
                                          post_rule.transactionType ===
                                          transactiontype
                                      )
                                    ) {
                                      return (
                                        <React.Fragment key={transactionPhase}>
                                          {items
                                            .filter(
                                              (post_rule) =>
                                                post_rule.transactionType ===
                                                transactiontype
                                            )
                                            .map((post_rule, index) => {
                                              const conditionWheredata =
                                                JSON.parse(
                                                  post_rule.conditionWhere
                                                );

                                              return (
                                                <div key={index}>
                                                  {conditionWheredata &&
                                                    conditionWheredata.length >
                                                      0 &&
                                                    conditionWheredata.map(
                                                      (
                                                        conditionWhere_rule,
                                                        innerIndex
                                                      ) => {
                                                        let display = true;
                                                        if (
                                                          conditionWhere_rule.value ===
                                                          "Lead-Based_Paint_Disclosure_and_Acknowledgement"
                                                        ) {
                                                          if (
                                                            summary.propert_built &&
                                                            summary.propert_built ===
                                                              "yes"
                                                          ) {
                                                            display = true;
                                                          } else {
                                                            display = false;
                                                          }
                                                        }
                                                        if (display === true) {
                                                          const foundDocument =
                                                            documentValues.find(
                                                              (doc) =>
                                                                doc.documentType ===
                                                                  conditionWhere_rule.value &&
                                                                doc.is_active ===
                                                                  "yes"
                                                            );

                                                          if (foundDocument) {
                                                            return <></>;
                                                          } else {
                                                            return (
                                                              <div
                                                                className="float-left w-100 border rounded-3 p-3 d-flex align-items-center justify-content-between my-2"
                                                                key={innerIndex}
                                                              >
                                                                <p
                                                                  style={{
                                                                    cursor:
                                                                      "pointer",
                                                                  }}
                                                                  onClick={() =>
                                                                    handleAttachUpload(
                                                                      conditionWhere_rule,
                                                                      conditionWhere_rule.value
                                                                    )
                                                                  }
                                                                >
                                                                  {
                                                                    conditionWhere_rule.label
                                                                  }
                                                                </p>
                                                              </div>
                                                            );
                                                          }
                                                        }
                                                      }
                                                    )}
                                                </div>
                                              );
                                            })}
                                        </React.Fragment>
                                      );
                                    }
                                  }
                                )}
                              </div>
                            ) : (
                              <div className="modal-body fs-sm">
                                {step1 === "" && step2 === "" ? (
                                  <div className="">
                                    <h6 className="fs-base">
                                      How would you like to attach the document?
                                    </h6>
                                    <div className="alert alert-secondary alert-dismissible fade show col-md-12">
                                      <p>
                                        Upload a pdf File{" "}
                                        <small>from your Computer</small>
                                      </p>
                                      <div className="float-left w-100 mt-3">
                                        <button
                                          className="btn btn-primary"
                                          onClick={(e) =>
                                            TransactionStep("1", "upload")
                                          }
                                        >
                                          Upload Now
                                        </button>
                                      </div>
                                    </div>
                                  </div>
                                ) : null}

                                {step1 && step2 === "" ? (
                                  <div className="content-overlay mb-md-4">
                                    <div>
                                      {file ? (
                                        <div>
                                          {isImage && image && (
                                            <div className="float-left w-100">
                                              <div className="float-left w-100">
                                                <img
                                                  id="selected_Attach"
                                                  onClick={handleImage}
                                                  src={image}
                                                  alt="Uploaded Image"
                                                />
                                              </div>
                                              <NavLink
                                                className="btn btn-primary mt-3"
                                                onClick={handleImageAttch}
                                              >
                                                Upload File
                                              </NavLink>
                                            </div>
                                          )}

                                          {!isImage && (
                                            <>
                                              <div className="col-md-12 mt-3">
                                                <div className="">
                                                  <div className="">
                                                    {selectedSinglePage &&
                                                    selectedSinglePage.length >
                                                      0 ? (
                                                      <div className="col-md-12 pdf-checkbox">
                                                        <div
                                                          className={`pdf-preview ${
                                                            isPageBlurred
                                                              ? "blurred"
                                                              : ""
                                                          }`}
                                                        >
                                                          <div className="d-flex">
                                                            {selectedSinglePage.map(
                                                              ({
                                                                item,
                                                                index,
                                                                pageIndex,
                                                                key,
                                                                count,
                                                              }) =>
                                                                Array.from({
                                                                  length: count,
                                                                }).map(
                                                                  (_, i) => (
                                                                    <Document
                                                                      key={`${key}-${i}`}
                                                                      file={
                                                                        item
                                                                      }
                                                                      renderMode="canvas"
                                                                    >
                                                                      <div>
                                                                        <img
                                                                          onClick={() =>
                                                                            handleClose(
                                                                              index,
                                                                              pageIndex
                                                                            )
                                                                          }
                                                                          className="pull-center"
                                                                          src="https://cdn.pboffice.net/shared/transaction/page_select_drop.png"
                                                                        />
                                                                        <Page
                                                                          key={`${key}-${i}`}
                                                                          pageNumber={
                                                                            pageIndex +
                                                                            1
                                                                          }
                                                                        />
                                                                      </div>
                                                                    </Document>
                                                                  )
                                                                )
                                                            )}
                                                          </div>

                                                          {/* ))}  */}
                                                        </div>
                                                      </div>
                                                    ) : (
                                                      <>
                                                        {selectAllPagesNew &&
                                                        selectAllPagesNew.length >
                                                          0 ? (
                                                          <div className="col-md-12 pdf-checkbox">
                                                            <div
                                                              className={`pdf-preview d-flex ${
                                                                isPageBlurred
                                                                  ? "blurred"
                                                                  : ""
                                                              }`}
                                                            >
                                                              {selectAllPagesNew.map(
                                                                ({
                                                                  item,
                                                                  index,
                                                                  key,
                                                                  value,
                                                                }) =>
                                                                  value && (
                                                                    <Document
                                                                      key={`${key}-${index}`} // Ensure each Document component has a unique key
                                                                      file={
                                                                        item
                                                                      }
                                                                      renderMode="canvas"
                                                                    >
                                                                      {Array.from(
                                                                        Array(
                                                                          numPagesList[
                                                                            index
                                                                          ] || 0
                                                                        ),
                                                                        (
                                                                          _,
                                                                          pageIdx
                                                                        ) => (
                                                                          <div
                                                                            key={`${key}-${index}-${pageIdx}`}
                                                                          >
                                                                            {/* <img
                                                                              key={`${key}-${pageIdx}`}
                                                                              onClick={() =>
                                                                                handleCloseAll(
                                                                                  index,
                                                                                  pageIdx
                                                                                )
                                                                              }
                                                                              className="pull-center"
                                                                              src="https://cdn.pboffice.net/shared/transaction/page_select_drop.png"
                                                                            /> */}

                                                                            <Page
                                                                              key={`${key}-${index}-${pageIdx}`} // Ensure each Page component has a unique key
                                                                              pageNumber={
                                                                                pageIdx +
                                                                                1
                                                                              }
                                                                              onClick={() =>
                                                                                handlePagePreview(
                                                                                  item,
                                                                                  pageIdx
                                                                                )
                                                                              }
                                                                            />
                                                                          </div>
                                                                        )
                                                                      )}
                                                                    </Document>
                                                                  )
                                                              )}
                                                            </div>
                                                          </div>
                                                        ) : (
                                                          ""
                                                        )}
                                                      </>
                                                    )}
                                                  </div>
                                                </div>
                                              </div>

                                              <div className="row">
                                                <div className="col-md-8">
                                                  {file.map((item, index) => (
                                                    <div
                                                      key={index}
                                                      className=""
                                                    >
                                                      <div
                                                        style={{
                                                          fontSize: "12px",
                                                        }}
                                                      >
                                                        <label className="d-flex align-items-center justify-content-start">
                                                          <input
                                                            type="checkbox"
                                                            className="me-2"
                                                            onChange={() =>
                                                              handleCheckBox(
                                                                item,
                                                                index,
                                                                numPages
                                                              )
                                                            }
                                                          />
                                                          <b>Select All</b>
                                                        </label>
                                                        <div className="d-flex my-3">
                                                          <b>
                                                            {isImage
                                                              ? "Image Name:"
                                                              : "PDF Name:"}
                                                          </b>
                                                          <p className="">
                                                            {item.name}
                                                          </p>
                                                        </div>
                                                      </div>
                                                      <div className="mt-3">
                                                        <div className="pdf-thumbnails">
                                                          <div
                                                            className={`pdf-preview ${
                                                              isPageBlurred
                                                                ? "blurred"
                                                                : ""
                                                            }`}
                                                          >
                                                            <Document
                                                              file={item}
                                                              onLoadSuccess={({
                                                                numPages,
                                                              }) =>
                                                                onDocumentLoadSuccess(
                                                                  { numPages },
                                                                  index
                                                                )
                                                              }
                                                              renderMode="canvas"
                                                            >
                                                              <div className="row">
                                                                {Array.from(
                                                                  Array(
                                                                    numPagesList[
                                                                      index
                                                                    ] || 0
                                                                  ),
                                                                  (
                                                                    _,
                                                                    pageIdx
                                                                  ) => (
                                                                    <div
                                                                      key={
                                                                        pageIdx
                                                                      }
                                                                      className={`col-md-2 pdf-page mb-4 ${
                                                                        selectedPage ===
                                                                        pageIdx
                                                                          ? "selected"
                                                                          : ""
                                                                      }`}
                                                                      onClick={() =>
                                                                        handlePageClick(
                                                                          item,
                                                                          index,
                                                                          pageIdx
                                                                        )
                                                                      }
                                                                    >
                                                                      <div>
                                                                        {pageIdx +
                                                                          1}
                                                                      </div>{" "}
                                                                      <Page
                                                                        pageNumber={
                                                                          pageIdx +
                                                                          1
                                                                        }
                                                                        width={
                                                                          70
                                                                        }
                                                                        height={
                                                                          90
                                                                        }
                                                                      />
                                                                      <div
                                                                        className="btn btn-primary btn-sm add-page mt-2"
                                                                        onClick={() =>
                                                                          handleInput(
                                                                            item,
                                                                            index,
                                                                            pageIdx
                                                                          )
                                                                        }
                                                                        style={{
                                                                          transition:
                                                                            "opacity 0.5s",
                                                                        }}
                                                                      >
                                                                        Add Page
                                                                      </div>
                                                                    </div>
                                                                  )
                                                                )}
                                                              </div>
                                                            </Document>
                                                          </div>
                                                        </div>
                                                      </div>
                                                      <hr className="mt-2 mb-3" />
                                                    </div>
                                                  ))}
                                                </div>
                                                <div className="col-md-4">
                                                  <div className="float-left w-100 text-right mb-4">
                                                    <NavLink
                                                      className="btn btn-primary float-end"
                                                      onClick={
                                                        handleNewDocumentCreate
                                                      }
                                                      // onClick={handleSubmit}
                                                      // disabled={issubmit}
                                                    >
                                                      Create Document
                                                    </NavLink>
                                                  </div>

                                                  {selectedPage &&
                                                  selectedPage.length > 0 ? (
                                                    <div className="rounded-3 border p-3 mt-5">
                                                      {selectedPage.map(
                                                        (selected) => (
                                                          <div
                                                            key={selected.key}
                                                          >
                                                            <div className="d-flex">
                                                              <span>
                                                                Page{" "}
                                                                {selected.pageIndex +
                                                                  1}
                                                              </span>{" "}
                                                              &nbsp; of &nbsp;
                                                              <span>
                                                                {isImage
                                                                  ? "Image Name:"
                                                                  : "PDF Name:"}
                                                                {selected.item
                                                                  ?.name || ""}
                                                              </span>
                                                            </div>
                                                            <div
                                                              className={`pdf-preview preview_section ${
                                                                isPageBlurred
                                                                  ? "blurred"
                                                                  : ""
                                                              }`}
                                                            >
                                                              <Document
                                                                file={
                                                                  selected.item
                                                                }
                                                                renderMode="canvas"
                                                              >
                                                                <Page
                                                                  pageNumber={
                                                                    selected.pageIndex +
                                                                    1
                                                                  }
                                                                />
                                                              </Document>
                                                            </div>
                                                          </div>
                                                        )
                                                      )}
                                                    </div>
                                                  ) : (
                                                    <div>
                                                      {selectedPageAll &&
                                                      selectedPageAll.length >
                                                        0 ? (
                                                        <div className="rounded-3 border p-3 mt-5">
                                                          {selectedPageAll.map(
                                                            (selected) => (
                                                              <div
                                                                key={
                                                                  selected.key
                                                                }
                                                              >
                                                                <div className="d-flex">
                                                                  <span>
                                                                    Page{" "}
                                                                    {selected.index +
                                                                      1}
                                                                  </span>{" "}
                                                                  &nbsp; of
                                                                  &nbsp;
                                                                  <span>
                                                                    {isImage
                                                                      ? "Image Name:"
                                                                      : "PDF Name:"}
                                                                    {selected
                                                                      .item
                                                                      ?.name ||
                                                                      ""}
                                                                  </span>
                                                                </div>
                                                                <div
                                                                  className={`pdf-preview preview_section ${
                                                                    isPageBlurred
                                                                      ? "blurred"
                                                                      : ""
                                                                  }`}
                                                                >
                                                                  <Document
                                                                    file={
                                                                      selected.item
                                                                    }
                                                                    renderMode="canvas"
                                                                  >
                                                                    <Page
                                                                      pageNumber={
                                                                        selected.index +
                                                                        1
                                                                      }
                                                                    />
                                                                  </Document>
                                                                </div>
                                                              </div>
                                                            )
                                                          )}
                                                        </div>
                                                      ) : (
                                                        ""
                                                      )}
                                                    </div>
                                                  )}
                                                </div>
                                              </div>
                                            </>
                                          )}
                                        </div>
                                      ) : (
                                        <div>
                                          <di className="row">
                                            <div className="col-md-12">
                                              <h6>
                                                Select Files & Complete Upload
                                              </h6>
                                              <p>
                                                Need to select an Inbox file?
                                                &nbsp;
                                                <NavLink
                                                  onClick={handleBackStep}
                                                >
                                                  Go Back
                                                </NavLink>
                                              </p>

                                              <Dropzone
                                                onChangeStatus={
                                                  handleChangeStatus
                                                }
                                                accept={acceptedFileTypes
                                                  .map((type) => `.${type}`)
                                                  .join(",")}
                                                inputContent="Drop PDF files here (or click to browse)"
                                                styles={{
                                                  dropzone: {
                                                    minHeight: 200,
                                                    maxHeight: 250,
                                                  },
                                                  dropzoneActive: {
                                                    borderColor: "green",
                                                  },
                                                }}
                                              />
                                            </div>
                                          </di>
                                        </div>
                                      )}
                                    </div>
                                  </div>
                                ) : null}

                                {step2 ? (
                                  <div className="container content-overlay">
                                    <div className="bg-light border rounded-3 p-3 mb-2">
                                      <div className="row">
                                        <div className="d-flex align-items-center">
                                          <h1 className="h3 mb-3">
                                            Transaction View Received Document
                                          </h1>
                                        </div>
                                        <div className="row">
                                          <div className="mt-5 pull-left">
                                            <h6>
                                              Which file would you like to
                                              attach?
                                            </h6>
                                            <h6>
                                              Send a file to this transaction
                                              via email:
                                            </h6>
                                            <NavLink>
                                              northstar-kc6cd6aahd@sendtobackagent.com
                                            </NavLink>
                                            <br />
                                            <h5>
                                              Would you like to email a file in
                                              now?
                                            </h5>
                                            <p>
                                              The unique email address shown
                                              above will enable you to quickly
                                              receive a PDF file document for
                                              this transaction. Additional
                                              instructions can be found on the
                                              main
                                              <br />
                                              'Documents' tab for this
                                              transaction. Once emailed, please
                                              allow 30-60 seconds for the file
                                              to be processed and then return to
                                              this page.
                                            </p>
                                            <br />
                                            <h5>
                                              Looking for a file that isn't
                                              shown here?
                                            </h5>
                                            <p>
                                              The most recent files assigned to
                                              your account are shown on this
                                              page. If you are not able to find
                                              the file that you are expecting to
                                              attach to this transaction, please
                                              go
                                              <br />
                                              to the Inbox page, locate the
                                              file, and then use the 'Attach'
                                              option provided there.{" "}
                                              <NavLink>View Inbox</NavLink>
                                            </p>
                                            <br />
                                            <h5>File not here?</h5>
                                            <p>
                                              You need to go to the Inbox to
                                              find the right file and use the
                                              'Attach' link.{" "}
                                              <NavLink>View Inbox</NavLink>
                                            </p>
                                            <br />
                                            <h5>Need to upload a file?</h5>
                                            <p>
                                              Want to upload a file instead?
                                              <NavLink>Go Back</NavLink>
                                            </p>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                ) : null}

                                <form className="new_transaction d-none">
                                  <input
                                    type="text"
                                    name="category"
                                    id="category"
                                    value={step1.category}
                                  />
                                  <br />
                                  <input
                                    type="text"
                                    name="type"
                                    id="type"
                                    value={step2.type}
                                  />
                                  <br />
                                  <input
                                    type="text"
                                    name="phase"
                                    id="phase"
                                    value={step3.upload}
                                  />
                                  <br />
                                </form>
                              </div>
                            )}
                          </>
                        )}
                      </div>
                    </div>
                  </div>

                  <div className="modal in" role="dialog" id="addDocument">
                    <div
                      className="modal-dialog modal-lg modal-dialog-scrollable"
                      role="document"
                    >
                      <div className="modal-content mt-5">
                        <div className="modal-header mt-5">
                          <h4 className="modal-title">
                            Transaction Add Document
                          </h4>
                          <button
                            className="btn-close"
                            id="closeModal"
                            type="button"
                            data-dismiss="modal"
                            onClick={handleBack}
                            aria-label="Close"
                          ></button>
                        </div>

                        <div className="modal-body fs-sm">
                          {step1 === "" && step2 === "" ? (
                            <div className="">
                              <h6 className="fs-base">
                                How would you like to attach the document?
                              </h6>
                              <div className="alert alert-secondary alert-dismissible fade show col-md-12">
                                <p>
                                  Upload a File{" "}
                                  <small>from your Computer</small>
                                </p>
                                <div className="float-left w-100 mt-3">
                                  <button
                                    className="btn btn-primary"
                                    onClick={(e) =>
                                      TransactionStep("1", "upload")
                                    }
                                  >
                                    Upload Now
                                  </button>
                                </div>
                              </div>
                            </div>
                          ) : null}

                          {step1 && step2 === "" ? (
                            <div className="content-overlay mb-md-4">
                              <div>
                                {file ? (
                                  <div>
                                    {isImage && image && (
                                      <div className="float-left w-100">
                                        <div className="float-left w-100">
                                          <img
                                            id="selected_Image"
                                            onClick={handleImage}
                                            src={image}
                                            alt="Uploaded Image"
                                          />
                                        </div>
                                        <NavLink
                                          className="btn btn-primary mt-3"
                                          onClick={handleImageSubmit}
                                        >
                                          Upload File
                                        </NavLink>
                                      </div>
                                    )}

                                    {!isImage && (
                                      <>
                                        <div className="col-md-12 mt-3">
                                          <div className="">
                                            <div className="">
                                              {selectedSinglePage &&
                                              selectedSinglePage.length > 0 ? (
                                                <div className="col-md-12 pdf-checkbox">
                                                  <div
                                                    className={`pdf-preview ${
                                                      isPageBlurred
                                                        ? "blurred"
                                                        : ""
                                                    }`}
                                                  >
                                                    <div className="d-flex">
                                                      {selectedSinglePage.map(
                                                        ({
                                                          item,
                                                          index,
                                                          pageIndex,
                                                          key,
                                                          count,
                                                        }) =>
                                                          Array.from({
                                                            length: count,
                                                          }).map((_, i) => (
                                                            <Document
                                                              key={`${key}-${i}`}
                                                              file={item}
                                                              renderMode="canvas"
                                                            >
                                                              <div>
                                                                <img
                                                                  onClick={() =>
                                                                    handleClose(
                                                                      index,
                                                                      pageIndex
                                                                    )
                                                                  }
                                                                  className="pull-center"
                                                                  src="https://cdn.pboffice.net/shared/transaction/page_select_drop.png"
                                                                />
                                                                <Page
                                                                  key={`${key}-${i}`}
                                                                  pageNumber={
                                                                    pageIndex +
                                                                    1
                                                                  }
                                                                />
                                                              </div>
                                                            </Document>
                                                          ))
                                                      )}
                                                    </div>

                                                    {/* ))}  */}
                                                  </div>
                                                </div>
                                              ) : (
                                                <>
                                                  {selectAllPagesNew &&
                                                  selectAllPagesNew.length >
                                                    0 ? (
                                                    <div className="col-md-12 pdf-checkbox">
                                                      <div
                                                        className={`pdf-preview d-flex ${
                                                          isPageBlurred
                                                            ? "blurred"
                                                            : ""
                                                        }`}
                                                      >
                                                        {selectAllPagesNew.map(
                                                          ({
                                                            item,
                                                            index,
                                                            key,
                                                            value,
                                                          }) =>
                                                            value && (
                                                              <Document
                                                                key={`${key}-${index}`} // Ensure each Document component has a unique key
                                                                file={item}
                                                                renderMode="canvas"
                                                              >
                                                                {Array.from(
                                                                  Array(
                                                                    numPagesList[
                                                                      index
                                                                    ] || 0
                                                                  ),
                                                                  (
                                                                    _,
                                                                    pageIdx
                                                                  ) => (
                                                                    <div
                                                                      key={`${key}-${index}-${pageIdx}`}
                                                                    >
                                                                      {/* <img
                                                                        key={`${key}-${pageIdx}`}
                                                                        onClick={() =>
                                                                          handleCloseAll(
                                                                            index,
                                                                            pageIdx
                                                                          )
                                                                        }
                                                                        className="pull-center"
                                                                        src="https://cdn.pboffice.net/shared/transaction/page_select_drop.png"
                                                                      /> */}
                                                                      <Page
                                                                        key={`${key}-${index}-${pageIdx}`} // Ensure each Page component has a unique key
                                                                        pageNumber={
                                                                          pageIdx +
                                                                          1
                                                                        }
                                                                        onClick={() =>
                                                                          handlePagePreview(
                                                                            item,
                                                                            pageIdx
                                                                          )
                                                                        }
                                                                      />
                                                                    </div>
                                                                  )
                                                                )}
                                                              </Document>
                                                            )
                                                        )}
                                                      </div>
                                                    </div>
                                                  ) : (
                                                    ""
                                                  )}
                                                </>
                                              )}
                                            </div>
                                          </div>
                                        </div>

                                        <div className="row">
                                          <div className="col-md-8">
                                            {file.map((item, index) => (
                                              <div key={index} className="">
                                                <div
                                                  style={{ fontSize: "12px" }}
                                                >
                                                  <label className="d-flex align-items-center justify-content-start">
                                                    <input
                                                      type="checkbox"
                                                      className="me-2"
                                                      onChange={() =>
                                                        handleCheckBox(
                                                          item,
                                                          index,
                                                          numPages
                                                        )
                                                      }
                                                    />
                                                    <b>Select All</b>
                                                  </label>
                                                  <div className="d-flex my-3">
                                                    <b>
                                                      {isImage
                                                        ? "Image Name:"
                                                        : "PDF Name:"}
                                                    </b>
                                                    <p className="">
                                                      {item.name}
                                                    </p>
                                                  </div>
                                                </div>
                                                <div className="mt-3">
                                                  <div className="pdf-thumbnails">
                                                    <div
                                                      className={`pdf-preview ${
                                                        isPageBlurred
                                                          ? "blurred"
                                                          : ""
                                                      }`}
                                                    >
                                                      <Document
                                                        file={item}
                                                        onLoadSuccess={({
                                                          numPages,
                                                        }) =>
                                                          onDocumentLoadSuccess(
                                                            { numPages },
                                                            index
                                                          )
                                                        }
                                                        renderMode="canvas"
                                                      >
                                                        <div className="row">
                                                          {Array.from(
                                                            Array(
                                                              numPagesList[
                                                                index
                                                              ] || 0
                                                            ),
                                                            (_, pageIdx) => (
                                                              <div
                                                                key={pageIdx}
                                                                className={`col-md-2 pdf-page mb-4 ${
                                                                  selectedPage ===
                                                                  pageIdx
                                                                    ? "selected"
                                                                    : ""
                                                                }`}
                                                                onClick={() =>
                                                                  handlePageClick(
                                                                    item,
                                                                    index,
                                                                    pageIdx
                                                                  )
                                                                }
                                                              >
                                                                <div>
                                                                  {pageIdx + 1}
                                                                </div>{" "}
                                                                <Page
                                                                  pageNumber={
                                                                    pageIdx + 1
                                                                  }
                                                                  width={70}
                                                                  height={90}
                                                                />
                                                                <div
                                                                  className="btn btn-primary btn-sm add-page mt-2"
                                                                  onClick={() =>
                                                                    handleInput(
                                                                      item,
                                                                      index,
                                                                      pageIdx
                                                                    )
                                                                  }
                                                                  style={{
                                                                    transition:
                                                                      "opacity 0.5s",
                                                                  }}
                                                                >
                                                                  Add Page
                                                                </div>
                                                              </div>
                                                            )
                                                          )}
                                                        </div>
                                                      </Document>
                                                    </div>
                                                  </div>
                                                </div>
                                              </div>
                                            ))}
                                          </div>
                                          <div className="col-md-4">
                                            <div className="float-left w-100 text-right mb-4">
                                              <NavLink
                                                className="btn btn-primary float-end"
                                                onClick={handleSubmit}
                                                disabled={issubmit}
                                              >
                                                {issubmit
                                                  ? "Please Wait..."
                                                  : "Submit File"}
                                              </NavLink>
                                            </div>

                                            {selectedPage &&
                                            selectedPage.length > 0 ? (
                                              <div className="rounded-3 border p-3 mt-5">
                                                {selectedPage.map(
                                                  (selected) => (
                                                    <div key={selected.key}>
                                                      <div className="d-flex">
                                                        <span>
                                                          Page{" "}
                                                          {selected.pageIndex +
                                                            1}
                                                        </span>{" "}
                                                        &nbsp; of &nbsp;
                                                        <span>
                                                          {isImage
                                                            ? "Image Name:"
                                                            : "PDF Name:"}
                                                          {selected.item
                                                            ?.name || ""}
                                                        </span>
                                                      </div>
                                                      <div
                                                        className={`pdf-preview preview_section ${
                                                          isPageBlurred
                                                            ? "blurred"
                                                            : ""
                                                        }`}
                                                      >
                                                        <Document
                                                          file={selected.item}
                                                          renderMode="canvas"
                                                        >
                                                          <Page
                                                            pageNumber={
                                                              selected.pageIndex +
                                                              1
                                                            }
                                                          />
                                                        </Document>
                                                      </div>
                                                    </div>
                                                  )
                                                )}
                                              </div>
                                            ) : (
                                              <div>
                                                {selectedPageAll &&
                                                selectedPageAll.length > 0 ? (
                                                  <div className="rounded-3 border p-3 mt-5">
                                                    {selectedPageAll.map(
                                                      (selected) => (
                                                        <div key={selected.key}>
                                                          <div className="d-flex">
                                                            <span>
                                                              Page{" "}
                                                              {selected.index +
                                                                1}
                                                            </span>{" "}
                                                            &nbsp; of &nbsp;
                                                            <span>
                                                              {isImage
                                                                ? "Image Name:"
                                                                : "PDF Name:"}
                                                              {selected.item
                                                                ?.name || ""}
                                                            </span>
                                                          </div>
                                                          <div
                                                            className={`pdf-preview preview_section ${
                                                              isPageBlurred
                                                                ? "blurred"
                                                                : ""
                                                            }`}
                                                          >
                                                            <Document
                                                              file={
                                                                selected.item
                                                              }
                                                              renderMode="canvas"
                                                            >
                                                              <Page
                                                                pageNumber={
                                                                  selected.index +
                                                                  1
                                                                }
                                                              />
                                                            </Document>
                                                          </div>
                                                        </div>
                                                      )
                                                    )}
                                                  </div>
                                                ) : (
                                                  ""
                                                )}
                                              </div>
                                            )}
                                          </div>
                                        </div>
                                      </>
                                    )}
                                  </div>
                                ) : (
                                  <div>
                                    <div className="row">
                                      <div className="col-md-12">
                                        <h6>Select Files & Complete Upload</h6>
                                        <p>
                                          Need to select an Inbox file? &nbsp;
                                          <NavLink onClick={handleBackStep}>
                                            Go Back
                                          </NavLink>
                                        </p>

                                        <Dropzone
                                          onChangeStatus={handleChangeStatus}
                                          accept={acceptedFileTypes
                                            .map((type) => `.${type}`)
                                            .join(",")}
                                          inputContent="Drop PDF files here (or click to browse)"
                                          styles={{
                                            dropzone: {
                                              minHeight: 200,
                                              maxHeight: 250,
                                            },
                                            dropzoneActive: {
                                              borderColor: "green",
                                            },
                                          }}
                                        />
                                      </div>
                                    </div>
                                  </div>
                                )}
                              </div>
                            </div>
                          ) : null}

                          {step2 ? (
                            <div className="container content-overlay">
                              <div className="bg-light border rounded-3 p-3 mb-2">
                                <div className="row">
                                  <div className="d-flex align-items-center">
                                    <h1 className="h3 mb-3">
                                      Transaction View Received Document
                                    </h1>
                                  </div>
                                  <div className="row">
                                    <div className="mt-5 pull-left">
                                      <h6>
                                        Which file would you like to attach?
                                      </h6>
                                      <h6>
                                        Send a file to this transaction via
                                        email:
                                      </h6>
                                      <NavLink>
                                        northstar-kc6cd6aahd@sendtobackagent.com
                                      </NavLink>
                                      <br />
                                      <h5>
                                        Would you like to email a file in now?
                                      </h5>
                                      <p>
                                        The unique email address shown above
                                        will enable you to quickly receive a PDF
                                        file document for this transaction.
                                        Additional instructions can be found on
                                        the main
                                        <br />
                                        'Documents' tab for this transaction.
                                        Once emailed, please allow 30-60 seconds
                                        for the file to be processed and then
                                        return to this page.
                                      </p>
                                      <br />
                                      <h5>
                                        Looking for a file that isn't shown
                                        here?
                                      </h5>
                                      <p>
                                        The most recent files assigned to your
                                        account are shown on this page. If you
                                        are not able to find the file that you
                                        are expecting to attach to this
                                        transaction, please go
                                        <br />
                                        to the Inbox page, locate the file, and
                                        then use the 'Attach' option provided
                                        there. <NavLink>View Inbox</NavLink>
                                      </p>
                                      <br />
                                      <h5>File not here?</h5>
                                      <p>
                                        You need to go to the Inbox to find the
                                        right file and use the 'Attach' link.{" "}
                                        <NavLink>View Inbox</NavLink>
                                      </p>
                                      <br />
                                      <h5>Need to upload a file?</h5>
                                      <p>
                                        Want to upload a file instead?
                                        <NavLink>Go Back</NavLink>
                                      </p>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                          ) : null}

                          <form className="new_transaction d-none">
                            <input
                              type="text"
                              name="category"
                              id="category"
                              value={step1.category}
                            />
                            <br />
                            <input
                              type="text"
                              name="type"
                              id="type"
                              value={step2.type}
                            />
                            <br />
                            <input
                              type="text"
                              name="phase"
                              id="phase"
                              value={step3.upload}
                            />
                            <br />
                          </form>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div
                    className="modal in"
                    role="dialog"
                    id="addDocumentAdditional"
                  >
                    <div
                      className="modal-dialog modal-lg modal-dialog-scrollable"
                      role="document">
                      <div className="modal-content mt-5">
                        <div className="modal-header">
                          <h4 className="modal-title">
                            Upload Additional Document
                          </h4>
                          <button
                            className="btn-close"
                            id="closeModalDetailadditional"
                            type="button"
                            data-dismiss="modal"
                            onClick={handleBack}
                            aria-label="Close"
                          ></button>
                        </div>
                        <div className="modal-body fs-sm">
                          <div className="col-sm-12 mb-3">
                            <h6 className="fs-base" for="pr-sn">
                              Document Name
                            </h6>
                            <input
                              className="form-control form-control-lg"
                              id="inline-form-input"
                              type="text"
                              name="document_name"
                              value={documentcustomname}
                              onChange={handledocumentcustomname}
                              placeholder="Enter Additional Document Name"
                              required
                            />
                            {documentcustomnameerror && (
                              <div className="invalid-tooltip">
                                Document Name is required
                              </div>
                            )}
                          </div>
                          {step1 === "" && step2 === "" ? (
                            <div className="">
                              <h6 className="fs-base">
                                How would you like to attach the document?
                              </h6>
                              <div className="alert alert-secondary alert-dismissible fade show col-md-12">
                                <p>
                                  Upload a File{" "}
                                  <small>from your Computer</small>
                                </p>
                                <div className="float-left w-100 mt-3">
                                  <button
                                    className="btn btn-primary"
                                    onClick={(e) =>
                                      TransactionStep("1", "upload")
                                    }
                                  >
                                    Upload Now
                                  </button>
                                </div>
                              </div>
                            </div>
                          ) : null}

                          {step1 && step2 === "" ? (
                            <div className="content-overlay mb-md-4">
                              <div>
                                {file ? (
                                  <div className="">
                                    {isImage && image && (
                                      <div className="float-left w-100">
                                        <img
                                          id="selected_Images"
                                          onClick={handleImage}
                                          src={image}
                                          alt="Uploaded Image"
                                        />

                                        <div className="mt-3">
                                          <NavLink
                                            className="btn btn-primary"
                                            onClick={
                                              handleImageSubmitAdditional
                                            }
                                          >
                                            Upload File
                                          </NavLink>
                                        </div>
                                      </div>
                                    )}

                                    {!isImage && (
                                      <>
                                        <div className="col-md-12 mt-3">
                                          <div className="">
                                            <div className="">
                                              {selectedSinglePage &&
                                              selectedSinglePage.length > 0 ? (
                                                <div className="col-md-12 pdf-checkbox">
                                                  <div
                                                    className={`pdf-preview ${
                                                      isPageBlurred
                                                        ? "blurred"
                                                        : ""
                                                    }`}
                                                  >
                                                    <div className="d-flex">
                                                      {selectedSinglePage.map(
                                                        ({
                                                          item,
                                                          index,
                                                          pageIndex,
                                                          key,
                                                          count,
                                                        }) =>
                                                          Array.from({
                                                            length: count,
                                                          }).map((_, i) => (
                                                            <Document
                                                              key={`${key}-${i}`}
                                                              file={item}
                                                              renderMode="canvas"
                                                            >
                                                              <div>
                                                                <img
                                                                  onClick={() =>
                                                                    handleClose(
                                                                      index,
                                                                      pageIndex
                                                                    )
                                                                  }
                                                                  className="pull-center"
                                                                  src="https://cdn.pboffice.net/shared/transaction/page_select_drop.png"
                                                                />
                                                                <Page
                                                                  key={`${key}-${i}`}
                                                                  pageNumber={
                                                                    pageIndex +
                                                                    1
                                                                  }
                                                                />
                                                              </div>
                                                            </Document>
                                                          ))
                                                      )}
                                                    </div>

                                                    {/* ))}  */}
                                                  </div>
                                                </div>
                                              ) : (
                                                <>
                                                  {selectAllPagesNew &&
                                                  selectAllPagesNew.length >
                                                    0 ? (
                                                    <div className="col-md-12 pdf-checkbox">
                                                      <div
                                                        className={`pdf-preview d-flex ${
                                                          isPageBlurred
                                                            ? "blurred"
                                                            : ""
                                                        }`}
                                                      >
                                                        {selectAllPagesNew.map(
                                                          ({
                                                            item,
                                                            index,
                                                            key,
                                                            value,
                                                          }) =>
                                                            value && (
                                                              <Document
                                                                key={key}
                                                                file={item}
                                                                renderMode="canvas"
                                                              >
                                                                {Array.from(
                                                                  Array(
                                                                    numPagesList[
                                                                      index
                                                                    ] || 0
                                                                  ),
                                                                  (
                                                                    _,
                                                                    pageIdx
                                                                  ) => (
                                                                    <div
                                                                      key={
                                                                        pageIdx
                                                                      }
                                                                    >
                                                                      {/* <img
                                                                        key={`${key}-${pageIdx}`}
                                                                        onClick={() =>
                                                                          handleCloseAll(
                                                                            index,
                                                                            pageIdx
                                                                          )
                                                                        }
                                                                        className="pull-center"
                                                                        src="https://cdn.pboffice.net/shared/transaction/page_select_drop.png"
                                                                      /> */}
                                                                      <Page
                                                                        key={`${key}-${pageIdx}`}
                                                                        pageNumber={
                                                                          pageIdx +
                                                                          1
                                                                        }
                                                                        onClick={() =>
                                                                          handlePagePreview(
                                                                            item,
                                                                            pageIdx
                                                                          )
                                                                        }
                                                                      />
                                                                    </div>
                                                                  )
                                                                )}
                                                              </Document>
                                                            )
                                                        )}
                                                      </div>
                                                    </div>
                                                  ) : (
                                                    ""
                                                  )}
                                                </>
                                              )}
                                            </div>
                                          </div>
                                        </div>

                                        <div className="row">
                                          <div className="col-md-8">
                                            {file.map((item, index) => (
                                              <div key={index} className="">
                                                <div
                                                  style={{ fontSize: "12px" }}
                                                >
                                                  <label className="d-flex align-items-center justify-content-start">
                                                    <input
                                                      type="checkbox"
                                                      className="me-2"
                                                      onChange={() =>
                                                        handleCheckBox(
                                                          item,
                                                          index,
                                                          numPages[index]
                                                        )
                                                      }
                                                    />
                                                    <b>Select All</b>
                                                  </label>
                                                  <div className="d-flex my-3">
                                                    <b>
                                                      {isImage
                                                        ? "Image Name:"
                                                        : "PDF Name:"}
                                                    </b>
                                                    <p className="">
                                                      {item.name}
                                                    </p>
                                                  </div>
                                                </div>
                                                <div className="mt-3">
                                                  <div className="pdf-thumbnails">
                                                    <div
                                                      className={`pdf-preview ${
                                                        isPageBlurred
                                                          ? "blurred"
                                                          : ""
                                                      }`}
                                                    >
                                                      <Document
                                                        file={item}
                                                        onLoadSuccess={({
                                                          numPages,
                                                        }) =>
                                                          onDocumentLoadSuccess(
                                                            { numPages },
                                                            index
                                                          )
                                                        }
                                                        renderMode="canvas"
                                                      >
                                                        <div className="row">
                                                          {Array.from(
                                                            Array(
                                                              numPagesList[
                                                                index
                                                              ] || 0
                                                            ),
                                                            (_, pageIdx) => (
                                                              <div
                                                                key={pageIdx}
                                                                className={`col-md-2 pdf-page mb-4 ${
                                                                  selectedPage ===
                                                                  pageIdx
                                                                    ? "selected"
                                                                    : ""
                                                                }`}
                                                                onClick={() =>
                                                                  handlePageClick(
                                                                    item,
                                                                    index,
                                                                    pageIdx
                                                                  )
                                                                }
                                                              >
                                                                <div>
                                                                  {pageIdx + 1}
                                                                </div>{" "}
                                                                <Page
                                                                  pageNumber={
                                                                    pageIdx + 1
                                                                  }
                                                                  width={70}
                                                                  height={90}
                                                                />
                                                                <div
                                                                  className="btn btn-primary btn-sm add-page mt-2"
                                                                  onClick={() =>
                                                                    handleInput(
                                                                      item,
                                                                      index,
                                                                      pageIdx
                                                                    )
                                                                  }
                                                                  style={{
                                                                    transition:
                                                                      "opacity 0.5s",
                                                                  }}
                                                                >
                                                                  Add Page
                                                                </div>
                                                              </div>
                                                            )
                                                          )}
                                                        </div>
                                                      </Document>
                                                    </div>
                                                  </div>
                                                </div>
                                              </div>
                                            ))}
                                          </div>
                                          <div className="col-md-4">
                                            <div className="float-left w-100 text-right mb-4">
                                              <NavLink
                                                className="btn btn-primary float-end"
                                                onClick={handleSubmitAdditional}
                                                disabled={issubmit}
                                              >
                                                {issubmit
                                                  ? "Please Wait..."
                                                  : "Submit File"}
                                              </NavLink>
                                            </div>

                                            {selectedPage &&
                                            selectedPage.length > 0 ? (
                                              <div className="rounded-3 border p-3 mt-5">
                                                {selectedPage.map(
                                                  (selected) => (
                                                    <div key={selected.key}>
                                                      <div className="d-flex">
                                                        <span>
                                                          Page{" "}
                                                          {selected.pageIndex +
                                                            1}
                                                        </span>{" "}
                                                        &nbsp; of &nbsp;
                                                        <span>
                                                          {isImage
                                                            ? "Image Name:"
                                                            : "PDF Name:"}
                                                          {selected.item
                                                            ?.name || ""}
                                                        </span>
                                                      </div>
                                                      <div
                                                        className={`pdf-preview preview_section ${
                                                          isPageBlurred
                                                            ? "blurred"
                                                            : ""
                                                        }`}
                                                      >
                                                        <Document
                                                          file={selected.item}
                                                          renderMode="canvas"
                                                        >
                                                          <Page
                                                            pageNumber={
                                                              selected.pageIndex +
                                                              1
                                                            }
                                                          />
                                                        </Document>
                                                      </div>
                                                    </div>
                                                  )
                                                )}
                                              </div>
                                            ) : (
                                              <div>
                                                {selectedPageAll &&
                                                selectedPageAll.length > 0 ? (
                                                  <div className="rounded-3 border p-3 mt-5">
                                                    {selectedPageAll.map(
                                                      (selected) => (
                                                        <div key={selected.key}>
                                                          <div className="d-flex">
                                                            <span>
                                                              Page{" "}
                                                              {selected.index +
                                                                1}
                                                            </span>{" "}
                                                            &nbsp; of &nbsp;
                                                            <span>
                                                              {isImage
                                                                ? "Image Name:"
                                                                : "PDF Name:"}
                                                              {selected.item
                                                                ?.name || ""}
                                                            </span>
                                                          </div>
                                                          <div
                                                            className={`pdf-preview preview_section ${
                                                              isPageBlurred
                                                                ? "blurred"
                                                                : ""
                                                            }`}
                                                          >
                                                            <Document
                                                              file={
                                                                selected.item
                                                              }
                                                              renderMode="canvas"
                                                            >
                                                              <Page
                                                                pageNumber={
                                                                  selected.index +
                                                                  1
                                                                }
                                                              />
                                                            </Document>
                                                          </div>
                                                        </div>
                                                      )
                                                    )}
                                                  </div>
                                                ) : (
                                                  ""
                                                )}
                                              </div>
                                            )}
                                          </div>
                                        </div>
                                      </>
                                    )}
                                  </div>
                                ) : (
                                  <div>
                                    <div className="row">
                                      <div className="col-md-12">
                                        <h6>Select Files & Complete Upload</h6>
                                        <p>
                                          Need to select an Inbox file? &nbsp;
                                          <NavLink onClick={handleBackStep}>
                                            Go Back
                                          </NavLink>
                                        </p>

                                        <Dropzone
                                          onChangeStatus={handleChangeStatus}
                                          accept={acceptedFileTypes
                                            .map((type) => `.${type}`)
                                            .join(",")}
                                          inputContent="Drop PDF files here (or click to browse)"
                                          styles={{
                                            dropzone: {
                                              minHeight: 200,
                                              maxHeight: 250,
                                            },
                                            dropzoneActive: {
                                              borderColor: "green",
                                            },
                                          }}
                                        />
                                      </div>
                                    </div>
                                  </div>
                                )}
                              </div>
                            </div>
                          ) : null}

                          {step2 ? (
                            <div className="container content-overlay">
                              <div className="bg-light border rounded-3 p-3 mb-2">
                                <div className="row">
                                  <div className="d-flex align-items-center">
                                    <h1 className="h3 mb-3">
                                      Transaction View Received Document
                                    </h1>
                                  </div>
                                  <div className="row">
                                    <div className="mt-5 pull-left">
                                      <h6>
                                        Which file would you like to attach?
                                      </h6>
                                      <h6>
                                        Send a file to this transaction via
                                        email:
                                      </h6>
                                      <NavLink>
                                        northstar-kc6cd6aahd@sendtobackagent.com
                                      </NavLink>
                                      <br />
                                      <h5>
                                        Would you like to email a file in now?
                                      </h5>
                                      <p>
                                        The unique email address shown above
                                        will enable you to quickly receive a PDF
                                        file document for this transaction.
                                        Additional instructions can be found on
                                        the main
                                        <br />
                                        'Documents' tab for this transaction.
                                        Once emailed, please allow 30-60 seconds
                                        for the file to be processed and then
                                        return to this page.
                                      </p>
                                      <br />
                                      <h5>
                                        Looking for a file that isn't shown
                                        here?
                                      </h5>
                                      <p>
                                        The most recent files assigned to your
                                        account are shown on this page. If you
                                        are not able to find the file that you
                                        are expecting to attach to this
                                        transaction, please go
                                        <br />
                                        to the Inbox page, locate the file, and
                                        then use the 'Attach' option provided
                                        there. <NavLink>View Inbox</NavLink>
                                      </p>
                                      <br />
                                      <h5>File not here?</h5>
                                      <p>
                                        You need to go to the Inbox to find the
                                        right file and use the 'Attach' link.{" "}
                                        <NavLink>View Inbox</NavLink>
                                      </p>
                                      <br />
                                      <h5>Need to upload a file?</h5>
                                      <p>
                                        Want to upload a file instead?
                                        <NavLink>Go Back</NavLink>
                                      </p>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                          ) : null}

                          <form className="new_transaction d-none">
                            <input
                              type="text"
                              name="category"
                              id="category"
                              value={step1.category}
                            />
                            <br />
                            <input
                              type="text"
                              name="type"
                              id="type"
                              value={step2.type}
                            />
                            <br />
                            <input
                              type="text"
                              name="phase"
                              id="phase"
                              value={step3.upload}
                            />
                            <br />
                          </form>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="modal in" role="dialog" id="ModalExemption">
                    <div
                      className="modal-dialog modal-md modal-dialog-scrollable"
                      role="document"
                    >
                      <div className="modal-content">
                        <div className="modal-header">
                          <h4 className="modal-title" id="myModalLabel">
                            Exemption Document
                          </h4>
                          <button
                            className="btn-close"
                            type="button"
                            id="exemptionclose"
                            data-dismiss="modal"
                            aria-label="Close"
                          ></button>
                        </div>

                        <div className="modal-body fs-sm">
                          <div className="col-md-12">
                            <form>
                              <div className="mb-3">
                                <label className="form-label">
                                  Exemption Description
                                </label>
                                <br />
                                <input
                                  id="paperwork_document"
                                  type="text"
                                  className="form-control"
                                  name="exemptionDescription"
                                  onChange={(e) => {
                                    setExemptionDescription(e.target.value);
                                  }}
                                  value={exemptionDescription}
                                />
                              </div>
                            </form>
                          </div>
                          <div className="modal-footer pb-0 px-0">
                            <button
                              type="button"
                              className="btn btn-secondary btn-sm m-0"
                              id="closeModalexe"
                              data-dismiss="modal"
                            >
                              Cancel & Close
                            </button>
                            <button
                              type="button"
                              className="btn btn-info btn-sm m-0 ms-3"
                              id="save-button"
                              disabled={isActive}
                              onClick={handleexemption}
                            >
                              {isActive ? "Please Wait..." : "Exemption"}
                            </button>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div
                    className="modal in"
                    role="dialog"
                    id="assigned_document"
                  >
                    <div
                      className="modal-dialog modal-lg modal-dialog-scrollable"
                      role="document"
                    >
                      <div className="modal-content mt-5">
                        <div className="modal-header">
                          <h4 className="modal-title">
                            Transaction Add Document
                          </h4>
                          <button
                            className="btn-close"
                            id="closeModalAssign"
                            type="button"
                            data-dismiss="modal"
                            onClick={handleBack}
                            aria-label="Close"
                          ></button>
                        </div>
                        {assignDocumets ? (
                          <div className="float-none w-50 bg-light rounded-3 p-4">
                            <div className="content-overlay">
                              <div className="d-flex">
                                <img
                                  src="https://cache.workspace.lwolf.com/silk/tick.png"
                                  alt=""
                                />
                                &nbsp;
                                <b>
                                  {" "}
                                  You have selected the "
                                  {selectAssignCondition.label}" title.
                                </b>
                                &nbsp;
                                <NavLink onClick={handleAssignChange}>
                                  change
                                </NavLink>
                              </div>
                              {/* <div className="float-left w-100 mt-3">
                                <label className="col-form-label pt-0 mb-0">
                                  Notes Add:
                                </label>
                                <input
                                  className="form-control"
                                  id="inline-form-input"
                                  type="text"
                                  name="attachNote"
                                  value={noteAdd.attachNote}
                                  onChange={handleChangeAttach}
                                />
                              </div> */}

                              <button
                                className="btn btn-primary mt-3"
                                onClick={handleSubmitAssign}
                                disabled={issubmit}
                              >
                                {issubmit ? "Please Wait..." : "Save Document"}
                              </button>
                            </div>
                          </div>
                        ) : (
                          <div className="modal-body fs-sm">
                            <div>
                              <h6 className="fs-base">
                                What is the title of the new document?
                              </h6>
                            </div>
                            {Object.entries(rulesdata).map(
                              ([transactionPhase, items]) => {
                                if (
                                  items.some(
                                    (post_rule) =>
                                      post_rule.transactionType ===
                                      transactiontype
                                  )
                                ) {
                                  return (
                                    <React.Fragment key={transactionPhase}>
                                      {items
                                        .filter(
                                          (post_rule) =>
                                            post_rule.transactionType ===
                                            transactiontype
                                        )
                                        .map((post_rule, index) => {
                                          const conditionWheredata = JSON.parse(
                                            post_rule.conditionWhere
                                          );

                                          return (
                                            <div key={index}>
                                              {conditionWheredata &&
                                                conditionWheredata.length > 0 &&
                                                conditionWheredata.map(
                                                  (
                                                    conditionWhere_rule,
                                                    innerIndex
                                                  ) => {
                                                    let display = true;
                                                    if (
                                                      conditionWhere_rule.value ===
                                                      "Lead-Based_Paint_Disclosure_and_Acknowledgement"
                                                    ) {
                                                      if (
                                                        summary.propert_built &&
                                                        summary.propert_built ===
                                                          "yes"
                                                      ) {
                                                        display = true;
                                                      } else {
                                                        display = false;
                                                      }
                                                    }
                                                    if (display === true) {
                                                      const foundDocument =
                                                        documentValues.find(
                                                          (doc) =>
                                                            doc.documentType ===
                                                              conditionWhere_rule.value &&
                                                            doc.is_active ===
                                                              "yes"
                                                        );

                                                      if (foundDocument) {
                                                        return <></>;
                                                      } else {
                                                        return (
                                                          <div
                                                            className="float-left w-100 border rounded-3 p-3 d-flex align-items-center justify-content-between my-2"
                                                            key={innerIndex}
                                                          >
                                                            <p
                                                              style={{
                                                                cursor:
                                                                  "pointer",
                                                              }}
                                                              onClick={() =>
                                                                handleAssignUpload(
                                                                  conditionWhere_rule,
                                                                  conditionWhere_rule.value
                                                                )
                                                              }
                                                            >
                                                              {
                                                                conditionWhere_rule.label
                                                              }
                                                            </p>
                                                          </div>
                                                        );
                                                      }
                                                    }
                                                  }
                                                )}
                                            </div>
                                          );
                                        })}
                                    </React.Fragment>
                                  );
                                }
                              }
                            )}
                          </div>
                        )}
                      </div>
                    </div>
                  </div>

                  <SendDocumentMail
                    checkboxState={checkboxState}
                    clearcheckboxState={clearcheckboxState}
                  />
                </div>
              </div>
            </div>
          </div>
        </div>
      </main>
    </div>
  );
};

export default Documents;

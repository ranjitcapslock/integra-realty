import React, { useState, useEffect } from 'react';
import { useParams } from 'react-router-dom';
import AllTab from '../../Pages/AllTab';
import Loader from '../../Pages/Loader/Loader.js';
import Toaster from '../../Pages/Toaster/Toaster';
import { NavLink, useNavigate } from 'react-router-dom';
import axios from 'axios';
import { Document, Page, pdfjs } from 'react-pdf';
import jsPDF from 'jspdf';
import html2canvas from 'html2canvas';
import { PDFDocument } from 'pdf-lib';



import 'react-pdf/dist/esm/Page/AnnotationLayer.css';
import 'react-pdf/dist/esm/Page/TextLayer.css';

pdfjs.GlobalWorkerOptions.workerSrc = `//cdnjs.cloudflare.com/ajax/libs/pdf.js/${pdfjs.version}/pdf.worker.js`;

const AddDocuments = () => {
    const [loader, setLoader] = useState({ isActive: null });
    const { isActive } = loader;
    const [toaster, setToaster] = useState({
        types: null,
        isShow: null,
        toasterBody: '',
        message: '',
    });
    const { types, isShow, toasterBody, message } = toaster;

    const navigate = useNavigate();
    const [step1, setStep1] = useState('');
    const [step2, setStep2] = useState('');
    const [step3, setStep3] = useState('');

    const [file, setFile] = useState(null);
    const [image, setImage] = useState(null);
    const [isImage, setIsImage] = useState(false);
    const [numPages, setNumPages] = useState(1);
    const [selectedPage, setSelectedPage] = useState(1);
    const [selectedSinglePage, setSelectedSinglePage] = useState([]);
    const [renamedFileName, setRenamedFileName] = useState('');

    const [selectedImage, setSelectedImage] = useState(null);
    const [selectAllPages, setSelectAllPages] = useState(false);
    const [isPdfClosed, setIsPdfClosed] = useState(true);
    const [isPageBlurred, setIsPageBlurred] = useState(false);
    const [pdfPath, setPdfPath] = useState('');
    const [canvasPages, setCanvasPages] = useState('');
    const [imageResponce, setImageResponce] = useState('');


    const TransactionStep = (stepno, value) => {
        if (stepno === '1') {
            setStep1(stepno);
        }
        if (stepno === '2') {
            setStep2(stepno);
        }
    };




    const onFileChange = (event) => {
        const selectedFile = event.target.files[0];
        setFile(selectedFile);
        const selectedPage = parseInt(event.target.value);
        setSelectedSinglePage(selectedPage);
        const reader = new FileReader();
        reader.onload = (e) => {
            const uploadedFile = e.target.result;
            setImage(uploadedFile);
            setIsImage(selectedFile.type.includes('image'));
        };
        reader.readAsDataURL(selectedFile);
        setNumPages(1);
        setSelectedPage(null);
    };

    const onDocumentLoadSuccess = ({ numPages }) => {
        setNumPages(numPages);

    };



    const handleImage = () => {
        setSelectedImage(image);
        // setSelectedSinglePage(false); 
    };



    const handlePageClick = (pageIndex) => {

        setSelectedPage((prevSelectedPage) =>
            prevSelectedPage === pageIndex ? null : pageIndex
        );
        setIsPageBlurred(false);
        // setSelectedSinglePage(false);
    };

    const handleCheckBox = () => {

        setSelectAllPages(!selectAllPages);
        setSelectedSinglePage(false)
        setIsPageBlurred(false);
    };

    const handleInput = (pageIndex) => {
        setSelectedSinglePage(prevPages => {
            const newPages = Array.isArray(prevPages) ? [...prevPages, pageIndex] : [pageIndex];
            return newPages;
        });
        setSelectAllPages(false);
        setIsPageBlurred(false);
    };
    const handlePagePreview = (pageIndex) => {
        setSelectedPage((prevSelectedPage) =>
            prevSelectedPage === pageIndex ? null : pageIndex
        );
        setIsPageBlurred(false);
    }





    const handleSubmit = async () => {
        if (!file || (!selectAllPages && selectedSinglePage === null)) {
            console.log('Invalid file or selected page.');
            return;
        }

        if (selectAllPages) {
            const formData = new FormData();
            formData.append('file', file);
            console.log(file);

            const config = {
                headers: {
                    'Content-Type': 'multipart/form-data',
                },
            };

            try {
                const response = await axios.post('https://api.brokeragentbase.com/upload', formData, config);
                console.log('Upload successful:', response.data);
            } catch (error) {
                console.log('Error occurred while uploading:', error);
            }
        }


        else if (!file || selectAllPages === null || selectedSinglePage < 0 || selectedSinglePage >= numPages) {
            return;
        }

        const mergedPdf = new jsPDF();
        const pdf = await pdfjs.getDocument(URL.createObjectURL(file)).promise;
        const pageArray = selectedSinglePage.filter((pageIndex) => {
            const pageNumber = pageIndex + 1;
            return pageNumber >= 1 && pageNumber <= pdf.numPages;
        });

        for (const pageIndex of pageArray) {
            const pageNumber = pageIndex + 1;
            const page = await pdf.getPage(pageNumber);
            const viewport = page.getViewport({ scale: 1 });
            const [width, height] = [viewport.width, viewport.height];

            const canvas = document.createElement('canvas');
            const canvasContext = canvas.getContext('2d');

            canvas.width = width;
            canvas.height = height;

            const renderContext = {
                canvasContext,
                viewport,
            };

            await page.render(renderContext).promise;

            const imageData = canvas.toDataURL('image/jpeg');
            const pageWidth = width * 0.30;
            const pageHeight = height * 0.30;

            mergedPdf.addPage([pageWidth, pageHeight]);
            mergedPdf.addImage(imageData, 'JPEG', 0, 0, pageWidth, pageHeight);

            const pageNumberText = `Page ${pageNumber}`;
            mergedPdf.setFontSize(12);
            mergedPdf.text(pageNumberText, 10, 10);
        }

        // Remove the extra first page
        mergedPdf.deletePage(1);

        const mergedPdfBytes = mergedPdf.output('arraybuffer');

        const formData = new FormData();
        formData.append('file', new Blob([mergedPdfBytes], { type: 'application/pdf' }), 'merged_document.pdf');

        try {
            const response = await axios.post('https://api.brokeragentbase.com/upload', formData);
            console.log('Upload successful:', response.data);
            setPdfPath(response.data.path);
        } catch (error) {
            console.log('Error occurred while uploading:', error);
        }
    }

         
    const convertImageToPDF = async (imageData) => {
        try {
        // Create a PDF document
        const pdf = await PDFDocument.create();
        const page = pdf.addPage();
    
        // Load the image
        const img = await pdf.embedPng(imageData);
    
        // Calculate the width and height of the image to fit the page
        const pageWidth = page.getWidth();
        const pageHeight = page.getHeight();
        const imgWidth = img.width;
        const imgHeight = img.height;
        const scaleFactor = Math.min(pageWidth / imgWidth, pageHeight / imgHeight);
    
        // Draw the image on the page
        page.drawImage(img, {
            x: (pageWidth - imgWidth * scaleFactor) / 2,
            y: (pageHeight - imgHeight * scaleFactor) / 2,
            width: imgWidth * scaleFactor,
            height: imgHeight * scaleFactor,
        });
    
        // Save the PDF as a Blob
        const pdfBytes = await pdf.save();
        return new Blob([pdfBytes], { type: 'application/pdf' });
        } catch (error) {
        console.error('Error converting image to PDF:', error);
        return null;
        }
    };

        
    const handleImageSubmit = async () => {
        if (!file || !image) {
        console.log('Invalid file or selected page.');
        return;
        }
    
        const canvas = await html2canvas(document.getElementById('selected_Image'));
        const imageDataURL = canvas.toDataURL('image/png');
    
        const pdfBlob = await convertImageToPDF(imageDataURL);
        if (!pdfBlob) {
        console.log('PDF conversion failed.');
        return;
        }
    
        const formData = new FormData();
        formData.append('file', pdfBlob, 'converted.pdf');
        
        try {
        const response = await axios.post('https://api.brokeragentbase.com/upload', formData, {
            headers: {
            'Content-Type': 'multipart/form-data',
            },
        });
        
        // Handle the API response as needed
        console.log('Image upload response:', response.data);
        setImageResponce(response.data);
    
     
        } catch (error) {
        console.error('Error uploading image:', error);
        }
    };

    // const handleImageSubmit = async () => {
    //     if (!file || (!image)) {
    //         console.log('Invalid file or selected page.');
    //         return;
    //     }

    //     if (image) {
    //         const formData = new FormData();
    //         formData.append('file', file);
    //         console.log(file);

    //         const config = {
    //             headers: {
    //                 'Content-Type': 'multipart/form-data',
    //             },
    //         };

    //         try {
    //             const response = await axios.post('https://api.brokeragentbase.com/upload', formData, config);
    //             console.log('Upload successful:', response.data);
    //         } catch (error) {
    //             console.log('Error occurred while uploading:', error);
    //         }
    //     }
    // }


    const handleBack = () => {
        setStep1("");
    };




    const handleClose = (index) => {
        setSelectedSinglePage((prevPages) => {
          const updatedPages = [...prevPages];
          const pageIndex = updatedPages.indexOf(index);
          if (pageIndex !== -1) {
            updatedPages.splice(pageIndex, 1);
          }
          return updatedPages;
        });
      };
      


    return (
        <div className="bg-secondary float-left w-100 pt-4">
            <Loader isActive={isActive} />
            {isShow && (
                <Toaster types={types} isShow={isShow} toasterBody={toasterBody} message={message} />
            )}
            <main className="page-wrapper">
                <AllTab />
                <div className="container mb-md-4 ">
                    <div className="bg-light shadow-sm rounded-3 p-2 p-md-5 mb-2">
                        {step1 === '' && step2 === '' ? (
                            <div className="row">
                                <div className="d-flex align-items-center">
                                    <h1 className="h3 mb-3">Transaction Add Document</h1>
                                    <br />
                                </div>
                                <div className="row">
                                    <h6 className="ms-5  mt-5">
                                        How would you like to attach the document?
                                    </h6>
                                    <div className="alert alert-secondary alert-dismissible fade show col-md-9 ms-5">
                                        <h6 className="pull-left">
                                            Upload a File <small>from your Computer</small>
                                        </h6>
                                        <button
                                            className="btn btn-primary btn-sm  pull-right"
                                            onClick={(e) => TransactionStep('1', 'upload')}
                                        >
                                            Upload Now
                                        </button>
                                    </div>
                                    <div className="alert alert-secondary alert-dismissible fade show col-md-9 ms-5">
                                        <h6 className="pull-left">
                                            Select a File <small>from Received Files Inbox</small>
                                        </h6>
                                        <a
                                            className="btn btn-primary btn-sm  pull-right"
                                            onClick={(e) => TransactionStep('2', 'view')}
                                        >
                                            View Received
                                        </a>
                                    </div>
                                </div>
                            </div>
                        ) : null}

                        {step1 && step2 === '' ? (
                            <div className="container content-overlay mb-md-4">
                                <div className="bg-light shadow-sm rounded-3 p-2 p-md-5 mb-2">
                                    {file ? (
                                        <div>
                                            {isImage && image && (
                                                <div>
                                                    <img id="selected_Image" onClick={handleImage} src={image} alt="Uploaded Image" />
                                                    
                                                    <NavLink className="btn btn-primary pull-right" onClick={handleImageSubmit}>Add a File</NavLink>
                                                </div>
                                            )}

                                            {!isImage && (
                                                <div>
                                                    <NavLink className="pull-right" onClick={handleSubmit}>Add a File</NavLink>
                                                    <div className="col-md-12 pdf-checkbox">

                                                        <div className="row">

                                                            <div className="col-md-4">
                                                                {selectAllPages && (
                                                                    <div>
                                                                        <div className={`pdf-preview ${isPageBlurred ? 'blurred' : ''}`}>
                                                                            <Document file={file} renderMode="canvas">
                                                                                {Array.from(new Array(numPages), (el, index) => (
                                                                                    <div>
                                                                                        <img className="pull-center" src="https://cdn.pboffice.net/shared/transaction/page_select_drop.png" />
                                                                                        <Page key={index} pageNumber={index + 1} onClick={() => handlePagePreview(index)} />
                                                                                    </div>
                                                                                ))}
                                                                            </Document>
                                                                        </div>
                                                                    </div>
                                                                )}

                                                                {selectedSinglePage.length > 0 && (
                                                                    <div>
                                                                        <div className={`pdf-preview ${isPageBlurred ? 'blurred' : ''}`}>
                                                                            <Document file={file} renderMode="canvas">
                                                                                {selectedSinglePage.map(pageIndex => (
                                                                                    <div>
                                                                                        <img onClick={() =>handleClose(pageIndex)} className="pull-center" src="https://cdn.pboffice.net/shared/transaction/page_select_drop.png" />
                                                                                        <Page key={pageIndex} pageNumber={pageIndex + 1} />
                                                                                    </div>
                                                                                ))}
                                                                            </Document>
                                                                        </div>
                                                                    </div>
                                                                )}



                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div className="row d-block">
                                                        <div className="col-md-2">
                                                            <div style={{ fontSize: '12px' }}>

                                                                <label className="d-flex align-items-center justify-content-start">
                                                                    <input type="checkbox" checked={selectAllPages} onChange={handleCheckBox} />
                                                                    Select All
                                                                </label>
                                                              
                                                                <p>
                                                                    {isImage ? 'Image Name:' : 'PDF Name:'} {file.name}
                                                                </p>
                                                            </div>

                                                        </div>
                                                        <div className="col-md-4">
                                                            <div className="pdf-thumbnails">
                                                                <div className={`pdf-preview ${isPageBlurred ? 'blurred' : ''}`}>
                                                                    <Document file={file} onLoadSuccess={onDocumentLoadSuccess} renderMode="canvas">
                                                                        {Array.from(new Array(numPages), (el, index) => (
                                                                            <div id="pdf-file" key={index} className="">
                                                                                <div className={`pdf-page ${selectedPage === index ? 'selected' : ''}`}
                                                                                    onClick={() => handlePageClick(index)}>
                                                                                    <Page pageNumber={index + 1} width={70} height={90} />
                                                                                </div>
                                                                                <div className="btn btn-primary btn-sm add-page" onClick={() => handleInput(index)}>
                                                                                    Add Page
                                                                                </div>
                                                                            </div>
                                                                        ))}
                                                                    </Document>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div className="col-md-8 mt-5">
                                                            {selectedPage !== null && (
                                                                <div>
                                                                    <div className={`pdf-preview ${isPageBlurred ? 'blurred' : ''}`}>
                                                                        <Document file={file} renderMode="canvas">
                                                                            <div className="PreviewPageOptions">
                                                                                <div className="PagePrev">
                                                                                    <i className="fa fa-caret-left" aria-hidden="true"></i>
                                                                                </div>
                                                                                <div className="PageButton NotUsed" >
                                                                                    Page Selected
                                                                                </div>
                                                                                <div className="PageNext Active" >
                                                                                    <i className="fa fa-caret-right" aria-hidden="true"></i>
                                                                                </div>
                                                                            </div>
                                                                            <Page pageNumber={selectedPage + 1} />
                                                                        </Document>
                                                                        <p>Page {selectedPage + 1}</p>
                                                                    </div>
                                                                </div>
                                                            )}
                                                        </div>
                                                    </div>
                                                </div>
                                            )}
                                        </div>
                                    ) : (
                                        <div>
                                            <div className="container content-overlay mb-md-4">
                                                <div className="bg-light shadow-sm rounded-3 p-2 p-md-5 mb-2">
                                                    <div className="row">
                                                        <div className="d-flex align-items-center">
                                                            <h1 className="h3 mb-3">Transaction Document Upload</h1>
                                                        </div>
                                                        <div className="col-md-9 mt-5 mb-3">
                                                            <span>
                                                                <h6 className="text-center ms-5 pull-left">Select Files & Complete Upload</h6>
                                                                <p className="pull-right">
                                                                    Need to select an Inbox file? &nbsp;
                                                                    <NavLink onClick={handleBack}>Go Back</NavLink>
                                                                </p>
                                                            </span>

                                                            <input className="file-uploader file-uploader-grid"
                                                                type="file" accept=".pdf, .png, .jpg, .jpeg" onChange={onFileChange} />
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    )}
                                </div>
                            </div>


                        ) : null}

                        {step2 ? (
                            <div className="container mb-md-4 ">
                                <div className="bg-light shadow-sm rounded-3 p-2 p-md-5 mb-2">
                                    <div className="row">
                                        <div className="d-flex align-items-center">
                                            <h1 className="h3 mb-3">Transaction View Received Document</h1>
                                        </div>
                                        <div className="row">
                                            <div className="mt-5 pull-left">
                                                <h6>Which file would you like to attach?</h6>
                                                <h6>Send a file to this transaction via email:</h6>
                                                <NavLink>
                                                    northstar-kc6cd6aahd@sendtobackagent.com
                                                </NavLink>
                                                <br />
                                                <h5>Would you like to email a file in now?</h5>
                                                <p>
                                                    The unique email address shown above will enable you to quickly receive
                                                    a PDF file document for this transaction. Additional instructions can be
                                                    found on the main
                                                    <br />
                                                    'Documents' tab for this transaction. Once emailed, please allow 30-60
                                                    seconds for the file to be processed and then return to this page.
                                                </p>
                                                <br />
                                                <h5>Looking for a file that isn't shown here?</h5>
                                                <p>
                                                    The most recent files assigned to your account are shown on this page. If
                                                    you are not able to find the file that you are expecting to attach to this
                                                    transaction, please go
                                                    <br />
                                                    to the Inbox page, locate the file, and then use the 'Attach' option
                                                    provided there. <NavLink>View Inbox</NavLink>
                                                </p>
                                                <br />
                                                <h5>File not here?</h5>
                                                <p>
                                                    You need to go to the Inbox to find the right file and use the 'Attach'
                                                    link. <NavLink>View Inbox</NavLink>
                                                </p>
                                                <br />
                                                <h5>Need to upload a file?</h5>
                                                <p>
                                                    Want to upload a file instead?<NavLink>Go Back</NavLink>
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        ) : null}



                        <form className="new_transaction d-none">
                            <input type="text" name="category" id="category" value={step1.category} />
                            <br />
                            <input type="text" name="type" id="type" value={step2.type} />
                            <br />
                            <input type="text" name="phase" id="phase" value={step3.upload} />
                            <br />
                        </form>
                    </div>
                </div>
            </main>
            <footer className="footer pt-5">
                <div className="container pb-2">
                    <div className="row align-items-center pb-4">
                        <div className="col-md-12 col-xl-12">
                            <div className="text-nowrap border-top border-light py-4">
                                <a className="btn btn-icon btn-translucent-light btn-xs rounded-circle me-2" href="#">
                                    <i className="fi-facebook"></i>
                                </a>
                                <a className="btn btn-icon btn-translucent-light btn-xs rounded-circle me-2" href="#">
                                    <i className="fi-twitter"></i>
                                </a>
                                <a className="btn btn-icon btn-translucent-light btn-xs rounded-circle me-2" href="#">
                                    <i className="fi-instagram"></i>
                                </a>
                                <a className="btn btn-icon btn-translucent-light btn-xs rounded-circle me-2" href="#">
                                    <i className="fi-linkedin"></i>
                                </a>
                            </div>
                            <div className="font-italic text-center text-muted fs--1">
                                © {new Date().getFullYear()} Your Company. All rights reserved.
                            </div>
                        </div>
                    </div>
                </div>
            </footer>
        </div>
    );
};

export default AddDocuments;

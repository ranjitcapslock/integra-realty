import React, { useEffect, useState } from 'react';
import { useParams } from 'react-router-dom';
import user_service from '../service/user_service';
import Loader from '../../Pages/Loader/Loader.js';
import Toaster from '../../Pages/Toaster/Toaster';
import { NavLink } from 'react-router-dom';
import jwt from "jwt-decode";
import { logDOM } from '@testing-library/react';
import defaultpropertyimage from '../../Components/img/defaultpropertyimage.jpeg';


import { Document, Page, pdfjs } from 'react-pdf';
import jsPDF from 'jspdf';
import html2canvas from 'html2canvas';
import { PDFDocument } from 'pdf-lib';
import 'react-pdf/dist/esm/Page/AnnotationLayer.css';
import 'react-pdf/dist/esm/Page/TextLayer.css';

const TransactionDoc = () => {
    const params = useParams();
    const [loader, setLoader] = useState({ isActive: null });
    const { isActive } = loader;
    const [toaster, setToaster] = useState({ types: null, isShow: null, toasterBody: '', message: '' });
    const { types, isShow, toasterBody, message } = toaster;
    const [summary, setSummary] = useState('');
    const [getDocument, setGetDocument] = useState(null);
    const [getDocumentAll, setGetDocumentAll] = useState([]);
    const [documentValues, setDocumentValues] = useState([])
    const [singleDocument, setSingleDocument] = useState("")
    const [singleDocumentType, setSingleDocumentType] = useState("")
    const [documentType, setDocumentType] = useState("")
    const [approval, setApproval] = useState("")




    useEffect(() => { window.scrollTo(0, 0);
        setLoader({ isActive: true });
        // window.location.reload();
        const TransactionGetById = async () => {
            await user_service.transactionGetById(params.id).then((response) => {
                if (response) {
                    setLoader({ isActive: false });
                    setSummary(response.data);
                }
            })
        };
        TransactionGetById(params.id);
    }, [params.id]);

    useEffect(() => { window.scrollTo(0, 0);
        transactionDocumentAll();

        const transactionDocumentGetId = async () => {
            await user_service.transactionDocumentGetId(params.itemId).then((response) => {
                if (response) {
                    setGetDocument(response.data);
                    const document = response.data.document
                    setSingleDocument(document)
                    const attachTypes = response.data.attachType
                    setSingleDocumentType(attachTypes);
                    const documentTypes = response.data.documentType
                    setDocumentType(documentTypes);
                    const approvalStatus = response.data.approvalStatus
                    console.log(approvalStatus);
                    setApproval(approvalStatus);
                }
            });
        };
        transactionDocumentGetId();
    }, [params.itemId]);


    // useEffect(() => { window.scrollTo(0, 0);
        const transactionDocumentAll = async () => {
            await user_service.transactionDocumentGet(params.id).then((response) => {
                if (response) {
                    setGetDocumentAll(response.data.data);
                    const documentValuess = [];
                    response.data.data?.map((postt) => {
                        documentValuess.push(postt.documentType);
                    })
                    setDocumentValues(documentValuess)
                }
            });
        }
        // transactionDocumentAll();

    // }, [])

    const handleAllDoucment = (items) => {
        setSingleDocument(items.document)
        setSingleDocumentType(items.attachType)
        setDocumentType(items.documentType)
    }


    const handleSubmit = async (e) => {
        e.preventDefault();
      
        const updateData = {
          approvalStatus: "Yes"
        };
        console.log(updateData);
        setLoader({ isActive: true });
      
        try {
          const response = await user_service.transactionDocumentUpdate(params.itemId, updateData);
          if (response) {
            setLoader({ isActive: false });
      
            // Find the first item with approvalStatus !== "Yes"
            const firstItemToRedirect = getDocumentAll.find(item => item.approvalStatus !== "Yes");
      
            // Redirect if such an item exists
            if (firstItemToRedirect) {
              window.location.href = `/transactionDoc/${params.id}/${firstItemToRedirect._id}`;
            }
          }
        } catch (error) {
          // Handle any errors here
          console.error(error);
        }
      }
      

    const handleCancel = async (e) => {
        e.preventDefault();
        const updateData = {
            approvalStatus: "Reject"
        };

        setLoader({ isActive: true })
        await user_service.transactionDocumentUpdate(params.itemId, updateData).then((response) => {
            if (response) {
                setLoader({ isActive: false })

            }
        })
        window.close();
    };

    const propertypic = (e) => {
        e.target.src = defaultpropertyimage;
    }
    const [numPages, setNumPages] = useState(null);
    const [pageNumber, setPageNumber] = useState(1);

    function onDocumentLoadSuccess({ numPages }) {
        setNumPages(numPages);
    }
    return (
        <div className="bg-secondary float-left w-100 pt-4 mb-4">
            <Loader isActive={isActive} />
            {isShow && <Toaster types={types} isShow={isShow} toasterBody={toasterBody} message={message} />}
            <main className="page-wrapper">
                {/* <AllTab /> */}
                {/* <NavLink className="btn btn-secondary pull-right" onClick={handleCancel}>Reject</NavLink> */}
             
                    {/* {
                        approval === "Yes"
                            ?
                            "" :
                            <NavLink className="btn btn-primary pull-right" onClick={handleSubmit}>Approve & Next</NavLink>
                    } */}

                        <div className="d-flex align-items-center justify-content-between mb-4">
                            <h6 className="mb-0 text-white">Document Name : {documentType}</h6>
                            <div className="pull-right">
                                {
                                    approval === "Yes"
                                        ?
                                        "" :
                                        <button className="btn btn-primary btn-sm" onClick={handleSubmit}>Approve & Next</button>
                                }
                                <button className="btn btn-primary btn-sm ms-2" onClick={handleCancel} >Reject</button>
                            </div> 
                        </div>
                                 
           
                        <div className="row">
                            {/* <div className="col-md-12"><h6 className="ms-5">{documentType}</h6></div> */}
                            <div className="col-md-3">
                                <div className="file_links float-left w-100">
                                    {getDocumentAll.map((item, index) => (
                                        <div className="" key={index}>
                                            <a href={`/transactionDoc/${params.id}/${item._id}`}><p style={{ color: item.approvalStatus === "Yes" ? "#008000" : "#000", }} onClick={() => handleAllDoucment(item)}>{item.documentType}</p></a>
                                        </div>
                                    ))}
                                </div>
                            </div>

                            <div className="col-md-9">
                                <div className="bg-light border rounded-3 p-4 mb-2">
                                {/* {
                                    singleDocumentType === "pdf" ?
                                        <embed src={"https://drive.google.com/viewerng/viewer?embedded=true&url=" + singleDocument} style={{ width: '100%', height: '1000px', frameBorder: "0" }} />
                                        :
                                        <img src={singleDocument} alt="Document" />
                                } */}
                                        <embed src={"https://drive.google.com/viewerng/viewer?embedded=true&url=" + singleDocument} type='application/pdf' style={{ width: '100%', height: '1000px', frameBorder: "0" }} />
                                        <Document className="d-flex align-items-center justify-content-center"  file={singleDocument} renderMode="canvas">
                                            <Page pageNumber={1} />
                                        </Document>
                                   {/* <div className="documentverify">
                                       <Document className="d-flex align-items-center justify-content-center" file={singleDocument} onLoadSuccess={onDocumentLoadSuccess}>
                                            <Page pageNumber={pageNumber} />
                                        </Document>
                                    </div> */}

                                </div>   
                            
                            </div>
                        </div>
                 
                
            </main>
           
        </div>
    );
};

export default TransactionDoc;

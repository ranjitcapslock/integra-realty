import React, { useEffect, useRef } from "react";
import { Page } from "react-pdf";

const CustomTextLayer = ({ pageNumber }) => {
  const textLayerRef = useRef(null);

  useEffect(() => { window.scrollTo(0, 0);
    if (textLayerRef.current) {
      const textLayer = textLayerRef.current.getTextLayer();
      textLayer.renderLayer(pageNumber);
    }
  }, [pageNumber]);

  return (
    <Page
      pageNumber={pageNumber}
      renderAnnotationLayer={false}
      renderInteractiveForms={false}
    >
      <div className="custom-text-layer" ref={textLayerRef} />
    </Page>
  );
};

export default CustomTextLayer;

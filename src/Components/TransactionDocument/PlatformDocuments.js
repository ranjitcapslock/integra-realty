import React, { useState, useEffect } from 'react';
import user_service from '../service/user_service';
import Loader from '../../Pages/Loader/Loader.js';
import axios from 'axios';
import jwt from "jwt-decode";
import avtar from "../img/avtar.jpg";
import ReactPaginate from "react-paginate";
import Toaster from '../../Pages/Toaster/Toaster.js';
import { NavLink, useNavigate, useParams } from "react-router-dom";
import _ from "lodash";


const PlatformDocuments = () => {
    const navigate = useNavigate();
    const [getDocument, setGetDocument] = useState([])

    const [loader, setLoader] = useState({ isActive: null })
    const { isActive } = loader;
    const [toaster, setToaster] = useState({ types: null, isShow: null, toasterBody: "", message: "" });
    const { types, isShow, toasterBody, message } = toaster;

 
    const params = useParams();
  
    const initialValues = {
        name: "", slug:"", status: "",
    }
    const [formValues, setFormValues] = useState(initialValues);
    const [formErrors, setFormErrors] = useState({});
    const [isSubmitClick, setISSubmitClick] = useState(false);
    const handleChange = (e) => {
        const { name, value } = e.target;
        setFormValues({ ...formValues, [name]: value });
    };

    const PlatformDocuments = async () => {
        await user_service.PlatformDocuments().then((response) => {
            // console.log(response);
            if (response) {
                setGetDocument(response.data.data);
            }
        });
    }


    useEffect(() => { window.scrollTo(0, 0);
        setLoader({ isActive: true })

        PlatformDocuments()

        setLoader({ isActive: false })
    }, []);

   
    

{/* <!-- Form Validation Start--> */ }
useEffect(() => { 
    if (formValues && isSubmitClick) {
        validate()
    }
}, [formValues])

const validate = () => {
    const values = formValues
    const errors = {};
    if (!values.name) {
        errors.name = "Document name is required";
    }

    setFormErrors(errors)
    return errors;
}
{/* <!-- Form Validation End--> */ }

    const adddocument = async (e) => {
        e.preventDefault();
        setISSubmitClick(true)
        let checkValue = validate()
        if (_.isEmpty(checkValue)) {
            var slug = formValues.name.replace(/ /g, '_');
                // slug = fanhubslug.toLowerCase();

            const ruleData = {
                name: formValues.name,
                slug: slug,
                status: 1
            };
            // console.log(ruleData);

            try {
                setLoader({ isActive: true });
                // const response = await user_service.AdddocumentRule(ruleData);
                await user_service.AddPlatformDocuments(ruleData).then((response) => {
                    if (response) {
                        // console.log(response.data)
                        document.getElementById("adddocumentclose").click();
                        setLoader({ isActive: false });
                        setFormValues({name: ""});
                        setToaster({
                            type: "Document Added Successfully",
                            isShow: true,
                            toasterBody: response.data.message,
                            message: "Document Added Successfully",
                        });
                        
                        PlatformDocuments()

                       
                        // setTimeout(() => {
                        //     // navigate(`/transaction-summary/${response.data._id}`);
                        // }, 500);
                    } else {
                        setLoader({ isActive: false });
                        setToaster({
                            types: "error",
                            isShow: true,
                            toasterBody: response.data.message,
                            message: "Error",
                        });
                    }
                    setTimeout(() => {
                        setToaster({ types: "error", isShow: false, toasterBody: null, message: "Error" });
                    }, 2000);
                });

            } catch (error) {
                setLoader({ isActive: false });
                setToaster({
                    types: "error",
                    isShow: true,
                    toasterBody: error,
                    message: "Error",
                });
            }
        }
    };
    return (
        <div className="bg-secondary float-left w-100 pt-4">
             <Loader isActive={isActive} />
            {isShow && <Toaster types={types}
                isShow={isShow}
                toasterBody={toasterBody}
                message={message} />}
            <main className="page-wrapper">
                {/* <!-- Page container--> */}
                <div className="container mb-md-4 ">
                    <div className="bg-light shadow-sm rounded-3 p-2 p-md-5 mb-2">
                        <div className="row">
                            <div className="d-flex align-items-center"><h1 className="h3 mb-3">Platform Documents</h1><br /></div>
                            {/* <!-- secondary alert --> */}
                            <div className='col-md-9 bg-secondary d-flex py-2'>
                                <p className="text-property mb-0 justify-content-center d-flex align-items-center">Documents</p>
                                <p className="mb-0" id="text-document">Below is a list of documents that are suggested and/or required by your brokerage for use in this<br />
                                    type of transaction.</p>
                            </div>


                            <div className='col-md-3 py-2 justify-content-end d-flex align-items-center'>
                                <a className="btn btn-primary btn-sm  pull-right w-100" data-toggle="modal"
                                                data-target="#adddocument">Add Document</a>
                            </div>
                            
                            <div className='col-md-12 mt-3'>
                                <div className='col-md-6 pull-left'>
                                    <p><b>8 Documents</b></p>
                                </div>
                                <div className='col-md-6 pull-right'>
                                    <form className="">
                                        <span className='ms-5'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Search</span>
                                        <input id="Document-search" type="search" placeholder="Document name." aria-label="Search" />
                                    </form>
                                </div>
                            </div>
                            <br /> <br />
                            <div className='row'>
                            {   
                            getDocument.length > 0 ? 
                                getDocument.map((post) => (
                                    <div className='col-md-6'>
                                    <div className='document_bar mt-2'>
                                        <i className="fa fa-file-text pull-left" aria-hidden="true"></i>
                                        <p className='pull-left'>&nbsp;&nbsp;&nbsp;&nbsp;{post.name}</p><br />
                                            <br />
                                    </div>
                                    </div>
                                ))
                                :
                                "Not Any Document added yet."
                            }
                            </div> 
                        </div>
                    </div>
                </div>

                <div className="modal" role="dialog" id="adddocument">
                    <div className="modal-dialog" role="document">
                        <div className="modal-content">
                            <div className="modal-header">
                                <h6>Add A New Document</h6>
                            </div>
                            <div className="modal-body row">
                                <div className=" col-md-12" >
                                    <div>
                                        <form onSubmit={adddocument}>
                                            <div className="mb-3">
                                                <label className="form-label">Document Name:</label><br />
                                                <div className='col-md-10 '>
                                                    <div className="form-check mt-3">
                                                        <input className="form-control" id="name" type="text" name="name" value={formValues.name} onChange={handleChange} />
                                                    </div>
                                                    <div className="invalid-tooltip">{formErrors.name}</div>
                                                </div>
                                            </div>

                                        <div className="modal-footer">
                                            <button type="button" className="btn btn-default btn btn-secondary btn-sm" id="adddocumentclose" data-dismiss="modal">Cancel & Close</button>
                                            <button type="submit" className="btn btn-primary">Save changes</button>
                                        </div>
                                        </form>
                                    </div>

                                </div>

                            </div>


                        </div>
                    </div>
                </div>
            </main >
            

        </div >
    )
}

export default PlatformDocuments
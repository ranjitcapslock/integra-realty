import React, { useState, useEffect } from "react";
import user_service from "../service/user_service.js";
import _ from "lodash";
import Loader from "../../Pages/Loader/Loader.js";
import Toaster from "../../Pages/Toaster/Toaster.js";
import { useNavigate, useParams } from 'react-router-dom';




function Signupinvite() {
  const params = useParams();

    const initialValues = {
        firstName: "", nickName: "", middleName: "", middleInitials: "", prefix: "",
        lastName: "", maidenName: "", suffix: "", email: "", passwordHash: "",
        confirmPassword: "",
    };

    const navigate = useNavigate();
    const [formValues, setFormValues] = useState(initialValues);
    const [accountData, setAccountData] = useState(initialValues);
    const [additionalActivateFields, setAdditionalActivateFields] = useState("");
    const [formErrors, setFormErrors] = useState({});
    const [isSubmitClick, setISSubmitClick] = useState(false);
    const [loader, setLoader] = useState({ isActive: null });
    const { isActive } = loader;
    const [toaster, setToaster] = useState({ type: null, isShow: null, toasterBody: "", message: "" });
    const { type, isShow, toasterBody, message } = toaster;
    const [checkAll, setCheckAll] = useState("");

    useEffect(() => { window.scrollTo(0, 0);
        const ContactGetByuniqueid = async () => {
            // await user_service.ContactGetByuniqueid(params.uid, params.cid).then((response) => {
              await user_service.contactGetById(params.id).then((response) => {
                if (response && response.data) {
                    setFormValues(response.data);
                    setAdditionalActivateFields(response.data.additionalActivateFields || {});
                    setAccountData(response.data || {});
                }
            });
        }
        ContactGetByuniqueid(params.id);
    }, [params.id,]);

    const handleChange = (e) => {
        const { name, value } = e.target;
        setAccountData({ ...accountData, [name]: value });
    };

    const handleChanges = (e) => {
        const { name, value } = e.target;
        setAdditionalActivateFields({ ...additionalActivateFields, [name]: value });
    };

    const CheckBox = (e) => {
        setCheckAll(e.target.value);
    }

    useEffect(() => {
        if (accountData && isSubmitClick) {
            validate();
        }
    }, [accountData, isSubmitClick]);

    const validate = () => {
        const values = accountData;
        const errors = {};

        if (!values.email) {
            errors.email = "Email is required";
        }

        if (values.passwordHash) {
            if (values.passwordHash.length < 8 || values.passwordHash.length > 30) {
                errors.passwordHash = "Password length must be between 8 and 30 characters.";
            }
        } else {
            errors.passwordHash = "passwordHash is required";
        }

        if (values.confirmPassword !== values.passwordHash) {
            errors.confirmPassword = "Confirm password must match the password.";
        }

        if (values.confirmPassword) {
            if (values.confirmPassword.length < 8 || values.confirmPassword.length > 30) {
                errors.confirmPassword = "ConfirmPassword length must be between 8 and 30 characters.";
            }
        } else {
            errors.confirmPassword = "ConfirmPassword is required";
        }

        setFormErrors(errors);
        return errors;
    }

    const handleSubmit = async (e) => {
        e.preventDefault();
        setISSubmitClick(true);
        let checkValue = validate();

        if (_.isEmpty(checkValue)) {
            const userData = {
                prefix: accountData.prefix,
                _id: accountData._id,
                firstName: accountData.firstName,
                lastName: accountData.lastName,
                email: accountData.email,
                password: accountData.passwordHash,
                inviteuser:"inviteuser",
            };

            setLoader({ isActive: true });
            const response = await user_service.resetpassword(userData);

            if (response && response.data) {
                setLoader({ isActive: false });

                if (response.data.status === 0) {
                    setToaster({
                        type: "success",
                        isShow: true,
                        toasterBody: response.data.message,
                        message: "Account Setup Successfully. Please login.",
                    });
                    setTimeout(() => {
                        navigate("/signin");
                    }, 1000);
                } else {
                    setToaster({ type: "error", isShow: true, toasterBody: response.data.message, message: "Error" });
                }
            } else {
                setLoader({ isActive: false });
                setToaster({ type: "error", isShow: true, toasterBody: "Error", message: "Error" });
            }
        }
    }

    return (
        <div className="bg-secondary float-left w-100 pt-4">
            <Loader isActive={isActive} />
            {isShow && <Toaster type={type} isShow={isShow} toasterBody={toasterBody} message={message} />}
            <div className="container">
                <div className="justify-content-center pb-sm-2">
                    <div className="bg-light rounded-3 p-4 mb-3">
                        
                            <>
                                <h6>Please update login credentials to complete account setup.</h6>
                                <div className="add_listing shadow-sm w-100 m-auto p-4">
                                    <div className="row">
                                        <div className="col-sm-4 mb-3">
                                            <label className="form-label">Given/First Name</label>
                                            <input
                                                className="form-control"
                                                id="inline-form-input"
                                                type="text"
                                                name="firstName"
                                                value={accountData.firstName}
                                                onChange={handleChange}
                                                readOnly
                                            />
                                            {formErrors.firstName && (
                                                <div className="invalid-tooltip">{formErrors.firstName}</div>
                                            )}
                                        </div>

                                        <div className="col-sm-4 mb-3">
                                            <label className="form-label">Family/Last Name</label>
                                            <input
                                                className="form-control"
                                                id="inline-form-input"
                                                type="text"
                                                name="lastName"
                                                value={accountData.lastName}
                                                onChange={handleChange}
                                                readOnly
                                            />
                                            {formErrors.lastName && (
                                                <div className="invalid-tooltip">{formErrors.lastName}</div>
                                            )}
                                        </div>

                                        <div className="col-sm-12 mb-3">
                                            <label className="form-label">Email</label>
                                            <input
                                                className="form-control center"
                                                id="inline-form-input"
                                                type="text"
                                                name="email"
                                                value={accountData.email}
                                                onChange={handleChanges}
                                                readOnly
                                            />
                                            {formErrors.email && (
                                                <div className="invalid-tooltip">{formErrors.email}</div>
                                            )}
                                        </div>

                                        <h6 className="mt-5">SET ACCOUNT PASSWORD</h6>
                                        <p className="col-sm-12">To safeguard your account, passwords must be 8-30 characters long and contain mixed characters and numbers. Special characters are recommended, but not required.</p>

                                        <div className="col-sm-12 mb-3">
                                            <label className="form-label">Password</label>
                                            <input
                                                className="form-control"
                                                id="inline-form-input"
                                                type="password"
                                                name="passwordHash"
                                                value={accountData.passwordHash}
                                                onChange={handleChange}
                                            />
                                            {formErrors.passwordHash && (
                                                <div className="invalid-tooltip">{formErrors.passwordHash}</div>
                                            )}
                                            <p>Password Strength</p>
                                        </div>

                                        <div className="col-sm-12 mb-3">
                                            <label className="form-label">Verify</label>
                                            <input
                                                className="form-control"
                                                id="inline-form-input"
                                                type="password"
                                                name="confirmPassword"
                                                value={accountData.confirmPassword}
                                                onChange={handleChange}
                                            />
                                            {formErrors.confirmPassword && (
                                                <div className="invalid-tooltip">{formErrors.confirmPassword}</div>
                                            )}
                                            <p>8-10 Characters Required</p>
                                        </div>

                                    </div>

                                    <div className="d-flex align-items-center justify-content-between">
                                        <button className="btn btn-secondary px-3 px-sm-4" type="button">Cancel & Go Back</button>
                                        <button className="btn btn-primary px-3 px-sm-4" type="button" onClick={handleSubmit}>Verify & Complete Setup</button>
                                    </div>
                                </div>
                            </>
                        
                    </div>
                </div>
            </div>
        </div>
    )
}


export default Signupinvite;

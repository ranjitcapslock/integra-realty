import React, { useState, useEffect } from "react";
import image from "../img/logo.png";
import user_service from "../service/user_service";
import _ from "lodash";
import { BehaviorSubject } from "rxjs";
import { signin } from "../service/http_common";
import Loader from "../../Pages/Loader/Loader.js";
import Toaster from "../../Pages/Toaster/Toaster";
import backgroundimage from "../img/BG-image.jpg";

function Signin() {
  const image = "https://brokeragentbase.s3.amazonaws.com/assets/logo.png";
  const initialValues = { email: "", password: "" };
  const [formValues, setFormValues] = useState(initialValues);
  const [formErrors, setFormErrors] = useState({});
  const [isSubmitClick, setISSubmitClick] = useState(false);
  const [authToken, setAuthToken] = useState(null);
  const authSubject = new BehaviorSubject(null);
  const [loader, setLoader] = useState({ isActive: null });
  const { isActive } = loader;
  const [toaster, setToaster] = useState({
    types: null,
    isShow: null,
    toasterBody: "",
    message: "",
  });
  const { types, isShow, toasterBody, message } = toaster;

  const [passwordShown, setPasswordShown] = useState(false);
  const [show, setIsShow] = useState(false);

  const togglePassword = () => {
    setPasswordShown(passwordShown ? false : true);
  };

  {
    /* <!-- Input onChange Start--> */
  }
  const handleChange = (e) => {
    const { name, value } = e.target;
    setFormValues({ ...formValues, [name]: value });
  };
  {
    /* <!-- Input onChange End--> */
  }

  {
    /* <!-- Form Validation Start--> */
  }


  const validate = () => {
    const values = formValues;
    const errors = {};

    if (!values.email) {
      errors.email = "Email is required";
    } else if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i.test(values.email)) {
      errors.email = "Invalid email address";
    }

    // if (!values.email) {
    //   errors.email = "Email is required";
    // }
    // else if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.email)) {
    //   errors.email = 'Invalid email address'
    // }

    if (!values.password) {
      errors.password = "Password is required";
    }
    setFormErrors(errors);
    return errors;
  };

  useEffect(() => {
    if (formValues && isSubmitClick) {
      validate();
    }
  }, [formValues]);
  {
    /* <!-- Form Validation End--> */
  }

  {
    /* <!-- Form onSubmit Start--> */
  }
  const handleSubmit = async (e) => {
    e.preventDefault();
    setISSubmitClick(true);
    let checkValue = validate();
    if (_.isEmpty(checkValue)) {
      setIsShow(true);
      const userData = {
        email: formValues.email,
        password: formValues.password,
      };
      try {
        setLoader({ isActive: true });
        const response = await user_service.signin(userData);
        setLoader({ isActive: false });
        if (response) {
          if (response.data.contact_status === "inapproval") {
            setToaster({
              type: "Login Successfully",
              isShow: true,
              toasterBody: response.data.message,
              message: "Login Successfully",
            });
            setTimeout(() => {
              setToaster((prevToaster) => ({ ...prevToaster, isShow: false }));
            }, 2000);
            setTimeout(() => {
              window.location.href = `/onboarding/${response.data.additionalActivateFields.intranet_uniqueid}/${response.data.additionalActivateFields.intranet_confirmationcode}`;
            }, 2000);
          } else {
            signin(response.data.jwtToken);
            localStorage.setItem("auth", response.data.jwtToken);
            localStorage.setItem("active_office", response.data.active_office);
            localStorage.setItem(
              "active_office_id",
              response.data.active_office_id
            );

            setToaster({
              type: "Login Successfully",
              isShow: true,
              toasterBody: response.data.message,
              message: "Login Successfully",
            });
            setTimeout(() => {
              setToaster((prevToaster) => ({ ...prevToaster, isShow: false }));
            }, 2000);
            setTimeout(() => {
              window.location.href = "/";
            }, 2000);
          }
        }
      } catch (error) {
        setLoader({ isActive: false });
        setIsShow(false);
        setToaster({
          types: "error",
          isShow: true,
          toasterBody: error.response
            ? error.response.data.message
            : error.message,
          message: "",
        });
        setTimeout(() => {
          setToaster((prevToaster) => ({ ...prevToaster, isShow: false }));
        }, 2000);
      }
    }
  };
  {
    /* <!-- Form OnSubmit Start--> */
  }

  useEffect(() => {
    window.scrollTo(0, 0);
    const subscription = authSubject.subscribe((token) => {
      setAuthToken(token);
    }, []);

    return () => {
      subscription.unsubscribe();
    };
  }, []);

   const [resetPassword, setResetPassword] = useState(false)
   const handleRestPassword = () =>{
    setResetPassword(true)
   }

   const handleBack = ()=>{
    setResetPassword(false)
   }

   const initialValuesNew = { email: ""};
   const [formValuesNew, setFormValuesNew] = useState(initialValuesNew);
   const [formErrorsNew, setFormErrorsNew] = useState({});
   const [isSubmitClickNew, setISSubmitClickNew] = useState(false);

   const handleChangePassword = (e) => {
    const { name, value } = e.target;
    setFormValuesNew({ ...formValuesNew, [name]: value });
  };


  const validateNew = () => {
    const values = formValuesNew;
    const errors = {};

    if (!values.email) {
      errors.email = "Email is required";
    } else if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i.test(values.email)) {
      errors.email = "Invalid email address";
    } 
    setFormErrorsNew(errors);
    return errors;
  };

  useEffect(() => {
    if (formValuesNew && isSubmitClickNew) {
      validateNew();
    }
  }, [formValuesNew]);
  

  const handlesetuppassword = async (e) => {
    e.preventDefault();
    setISSubmitClickNew(true);
    
    let checkValueNew = validateNew();
    if (_.isEmpty(checkValueNew)) {
  
      const userData = {
        email: formValuesNew.email,
      };
      try {
        setLoader({ isActive: true });
        const response = await user_service.resetpasswordlink(userData);
        
        if (response) {
          setResetPassword(false);
          setLoader({ isActive: false });
          setToaster({ 
            type: "success", 
            isShow: true, 
            message: "Password Change Link Sent Successfully to your email."
          });
          setISSubmitClickNew(false)
        }
        
      } catch (error) {
        setLoader({ isActive: false });
        setToaster({
          types: "error",
          isShow: true,
          toasterBody: error.response
            ? error.response.data.message
            : error.message,
        });
        setTimeout(() => {
          setToaster((prevToaster) => ({ ...prevToaster, isShow: false }));
        }, 2000);
      }
  
      // Automatically hide the toaster after 2 seconds
      setTimeout(() => {
        setToaster({ 
          type: "error", 
          isShow: false, 
          toasterBody: null, 
          message: "", 
        });
      }, 2000);
    }
  }
  
  return (
    <div
      className="sigin_user_bg"
      style={{ background: `url(${backgroundimage})` }}
    >
      <Loader isActive={isActive} />
      {isShow && (
        <Toaster
          types={types}
          isShow={isShow}
          toasterBody={toasterBody}
          message={message}
        />
      )}
      <main className="page-wrapper d-flex align-items-center justify-content-center">
        {/* <!-- Page content--> */}
        <div className="container">
          <div className="float-left w-100 d-flex align-items-center justify-content-center">
            <div className="sigin_user_layout">
              {/* <div className="col-md-6 border-end-md p-2 p-sm-5">
                <img
                  className="d-block mx-auto"
                  src={image}
                  width="344"
                  alt="Illustartion"
                />
                <h4 className="h4 mb-2 mb-sm-2 mt-5">
                  Are you a customer of InDepth Realty?
                </h4>
                <hr></hr>

                <div className="mt-2 mt-sm-2">
                  You have accessed our secured intranet for agents and staff only. Please contact your agent or
                  our main office for more information on an active or past transaction. <br /><br />

                  Transactions can be shared securely to customers and vendors through the Roundtable system.
                  If an associate has shared a transaction with you, please visit Roundtable for Customers/Vendors.
                  <a href="/signup">Sign up here</a>
                </div>
              </div> */}

              {/* <!-- Form Values Start--> */}
              <div className="col-md-12">
                <img
                  className="d-block mx-auto"
                  src={image}
                  width="344"
                  alt="Illustartion"
                />
                <form className="needs-validation mt-4">
                 {
                    resetPassword ?
                    <div>
                    <div className="mb-4">
                    <label className="form-label mb-2">Email</label>
                    <input
                      className="form-control"
                      type="email"
                      name="email"
                      id="signin-email"
                      placeholder="Enter your email"
                      onChange={handleChangePassword}
                      autoComplete="on"
                      style={{
                        border: formErrorsNew?.email
                          ? "1px solid red"
                          : "1px solid #00000026",
                      }}
                      value={formValuesNew.email}
                    />
                    <div className="invalid-feedback">{formErrorsNew.email}</div>
                  </div>

                  <button className="btn btn-primary" onClick={handlesetuppassword}>Submit</button>
                  <button className="btn btn-secondary ms-3" onClick={handleBack}>Go Back</button>
                  
                    </div>
                    :
                <>
                  <div className="mb-3">
                    <label className="form-label mb-2">User Id</label>
                    <input
                      className="form-control"
                      type="email"
                      name="email"
                      id="signin-email"
                      placeholder="Enter your email"
                      onChange={handleChange}
                      autoComplete="on"
                      style={{
                        border: formErrors?.email
                          ? "1px solid red"
                          : "1px solid #00000026",
                      }}
                      value={formValues.email}
                    />
                    <div className="invalid-feedback">{formErrors.email}</div>
                  </div>
                  <div className="">
                    <div className="d-flex align-items-center justify-content-between mb-2">
                      <label className="form-label mb-0">Password</label>
                    </div>
                    <div className="password-toggle">
                            <input
                              className="form-control"
                              type={passwordShown ? "text" : "password"}
                              id="signin-password"
                              name="password"
                              placeholder="Enter password"
                              onChange={handleChange}
                              // maxLength={5}
                              autoComplete="on"
                              style={{
                                border: formErrors?.password
                                  ? "1px solid red"
                                  : "1px solid #00000026",
                              }}
                              value={formValues.password}
                            />
                            <div className="invalid-feedback">
                              {formErrors.password}
                            </div>
                      <label
                        className="password-toggle-btn passwordToggle"
                        aria-label="Show/hide password"
                        onClick={togglePassword}
                      >
                        <span className="password-toggle-indicator"></span>
                      </label>
                    </div>
                    <button
                      className="mt-3 btn btn-primary btn-lg w-100"
                      type="submit"
                      disabled={show}
                      onClick={handleSubmit}
                    >
                      {show ? "Please wait" : "Login"}
                    </button>
                 <div className="float-start w-100 d-flex align-items-center justify-content-between">
                    <label className="mt-2 mb-0 w-100" aria-label="Show/hide password">
                      <input className="" id="remember-me" type="checkbox" />{" "}
                       &nbsp;Keep me logged in
                      <a className="pull-right" onClick={handleRestPassword}>Reset Password</a>
                    </label>
                    {/* <a className="fs-sm" href="#">
                      Can't login? Get Help
                    </a> */}
                  </div>
                  <div className="float-start w-100">
                    <span><sub>Do not use on shared computers</sub></span>
                  </div>
                  </div>
                </>
                  }
                  </form>
              </div>
              {/* <!-- Form Values End--> */}
            </div>
          </div>
        </div>
      </main>
      {/* <!-- Back to top button-->
      <a className="btn-scroll-top" href="#top" data-scroll><span className="btn-scroll-top-tooltip text-muted fs-sm me-2">Top</span><i className="btn-scroll-top-icon fi-chevron-up">   </i></a> */}
    </div>
  );
}

export default Signin;

import React, { useState, useEffect } from "react";

import { NavLink } from "react-router-dom";
import user_service from "../service/user_service";
import Loader from "../../Pages/Loader/Loader.js";
import Toaster from "../../Pages/Toaster/Toaster.js";

const eventColors = {
  meeting: "#FF5733",
  duty: "#3366FF",
};
const Learn = () => {
  const [loader, setLoader] = useState({ isActive: null });
  const { isActive } = loader;
  const [toaster, setToaster] = useState({
    types: null,
    isShow: null,
    toasterBody: "",
    message: "",
  });
  const { types, isShow, toasterBody, message } = toaster;

  return (
    <div className="bg-secondary float-left w-100 mb-4">
      <Loader isActive={isActive} />
      {isShow && (
        <Toaster
          types={types}
          isShow={isShow}
          toasterBody={toasterBody}
          message={message}
        />
      )}
      <div className="">
        <main className="page-wrapper homepage_layout">
          <div className="">
            <div className="my-4">
              <h3 className="text-white">Training Resources</h3>
              <p className="text-white">At In Depth Realty, we're committed to helping our team excel in the 
                ever-evolving real estate industry. Our Training Resources Page is your
                 gateway to success, offering a wealth of tools and materials designed to
                  enhance your knowledge and skills.</p>
              <hr className="mb-3" />
              {/* <div className="col-md-6 ms-5">
                <h6 className="ms-3">Training Resources</h6>
                <hr className="mt-2" />
                <div className="row">
                  <div className="col-md-12 ms-4">
                    <div>
                      <NavLink to={`/calendar`}>Training Calendar</NavLink>
                    </div>
                    <div>
                      <NavLink to={`/control-panel/documents`}>
                        Learning Resources
                      </NavLink>
                    </div>
                    <div>
                      <NavLink to={`/learnMedia/workspaceOverview`}>
                        Workspace Overview
                      </NavLink>
                    </div>
                    <div>
                      <NavLink to={`/learnMedia/workspaceRecaps`}>
                        Workspace Recaps
                      </NavLink>
                    </div>
                    <div>
                      <NavLink to={`/learnMedia/rEALTORNews`}>
                        REALTOR® News
                      </NavLink>
                    </div>
                    <div>
                      <NavLink to={`/learnMedia/videoTraining`}>
                        Video Training
                      </NavLink>
                    </div>
                  </div>
                  <hr className="mt-5" />
                </div>
              </div> */}

              <div className="row">
                <div className="col-lg-2 col-md-2 col-sm-6 col-6 mb-lg-0 mb-md-0 mb-sm-3 mb-3">
                  <NavLink
                    className="icon-box card card-body h-100 border-0 shadow-sm card-hover h-100 text-center"
                    to="/calendar"
                  >
                    <div className="icon-box-media bg-faded-primary text-primary rounded-circle mb-3 mx-auto">
                      {/* <i className="fi-real-estate-house"></i> */}
                      <i className="fa fa-calendar" aria-hidden="true"></i>
                    </div>
                    <h3 className="icon-box-title fs-base mb-0">
                      Training <br/>Events
                    </h3>
                  </NavLink>
                </div>

                <div className="col-lg-2 col-md-2 col-sm-6 col-6 mb-lg-0 mb-md-0 mb-sm-3 mb-3">
                  <NavLink
                    className="icon-box card card-body h-100 border-0 shadow-sm card-hover h-100 text-center"
                    to="/control-panel/documents"
                  >
                    <div className="icon-box-media bg-faded-primary text-primary rounded-circle mb-3 mx-auto">
                      <i className="fa fa-book" aria-hidden="true"></i>
                    </div>
                    <h3 className="icon-box-title fs-base mb-0">
                      Training Resources
                    </h3>
                  </NavLink>
                </div>

                <div className="col-lg-2 col-md-2 col-sm-6 col-6 mb-lg-0 mb-md-0 mb-sm-3 mb-3">
                  <NavLink
                    className="icon-box card card-body h-100 border-0 shadow-sm card-hover h-100 text-center"
                    to="/learnMedia/workspaceOverview"
                  >
                    <div className="icon-box-media bg-faded-primary text-primary rounded-circle mb-3 mx-auto">
                    <i className="fa fa-handshake-o" aria-hidden="true"></i>
                    </div>
                    <h3 className="icon-box-title fs-base mb-0">
                      B.A.S.E Overview
                    </h3>
                  </NavLink>
                </div>

                {/* <div className="col-lg-2 col-md-2 col-sm-6 col-6 mb-lg-0 mb-md-0 mb-sm-3 mb-3">
                  <NavLink
                    className="icon-box card card-body h-100 border-0 shadow-sm card-hover h-100 text-center"
                    to="/learnMedia/workspaceRecaps"
                  >
                    <div className="icon-box-media bg-faded-primary text-primary rounded-circle mb-3 mx-auto">
                      <i className="fa fa-file-video-o" aria-hidden="true"></i>
                    </div>
                    <h3 className="icon-box-title fs-base mb-0">
                    B.A.S.E Recaps 
                    </h3>
                  </NavLink>
                </div> */}

                <div className="col-lg-2 col-md-2 col-sm-6 col-6 mb-lg-0 mb-md-0 mb-sm-3 mb-3">
                {/* <NavLink
                      className="icon-box card card-body h-100 border-0 shadow-sm card-hover h-100 text-center"
                      to="/learnMedia/rEALTORNews"
                    > */}
                  <NavLink
                      className="icon-box card card-body h-100 border-0 shadow-sm card-hover h-100 text-center"
                      to="/#postsection"
                    >
                      <div className="icon-box-media bg-faded-primary text-primary rounded-circle mb-3 mx-auto">
                      <i className="fa fa-volume-up" aria-hidden="true"></i>
                      </div>
                      <h3 className="icon-box-title fs-base mb-0">
                      Realtor News & Updates
                      </h3>
                    </NavLink>
                </div>

                <div className="col-lg-2 col-md-2 col-sm-6 col-6 mb-lg-0 mb-md-0 mb-sm-3 mb-3">
                <NavLink
                    className="icon-box card card-body h-100 border-0 shadow-sm card-hover h-100 text-center"
                    to="/learnMedia/videoTraining"
                  >
                    <div className="icon-box-media bg-faded-primary text-primary rounded-circle mb-3 mx-auto">
                    <i className="fa fa-video-camera" aria-hidden="true"></i>
                    </div>
                    <h3 className="icon-box-title fs-base mb-0">
                    Video<br/>Training
                    </h3>
                  </NavLink>
                </div>
              </div>
            </div>
          </div>

          <section className="float-left w-100">
            <div className="row row-cols-md-3 row-cols-1 gy-3 mt-4">
              <div className="col">
                <div className="card card-hover h-100 border-0 bg-light position-relative">
                  <div className="card-body pb-2">
                    <h5 className="mb-2 pb-1">How to Get Started?</h5>
                    <p className="mb-0">Start with beginners guide.</p>
                  </div>
                  <div className="card-footer py-1 border-0"><NavLink className="stretched-link btn btn-link mb-3 px-0 text-warning">FAQ's</NavLink></div>
                </div>
              </div>
              <div className="col">
                <div className="card card-hover h-100 border-0 bg-light position-relative">
                  <div className="card-body pb-2">
                    <h5 className="mb-2 pb-1">Need Help?</h5>
                    <p className="mb-0">you can reach out to broker@indepthrealty.com</p>
                  </div>
                  <div className="card-footer py-1 border-0"><NavLink className="stretched-link btn btn-link mb-3 px-0 text-accent">Contact Now</NavLink></div>
                </div>
              </div>
              <div className="col">
                <div className="card card-hover h-100 border-0 bg-light position-relative">
                  <div className="card-body pb-2">
                    <h5 className="mb-2 pb-1">How it works</h5>
                    <p className="mb-0">Here is a step-by-step guide for agents.</p>
                  </div>
                  <div className="card-footer py-1 border-0"><NavLink className="stretched-link btn btn-link mb-3 px-0 text-success">Get started</NavLink></div>
                </div>
              </div>
            </div>
          </section>
        </main>
      </div>

    </div>
  );
};

export default Learn;

import React, { useState, useEffect } from "react";
import user_service from "../service/user_service";
import Loader from "../../Pages/Loader/Loader.js";
import Toaster from "../../Pages/Toaster/Toaster";
import { NavLink, useNavigate } from "react-router-dom";
import ReactPaginate from "react-paginate";
import defaultpropertyimage from "../../Components/img/defaultpropertyimage.jpeg";

const SuggestFeature = () => {
  const [loader, setLoader] = useState({ isActive: null });
  const { isActive } = loader;
  const [toaster, setToaster] = useState({
    types: null,
    isShow: null,
    toasterBody: "",
    message: "",
  });
  const { types, isShow, toasterBody, message } = toaster;

  const [getSuggest, setGetSuggest] = useState([]);

  const navigate = useNavigate();

  useEffect(() => {
    window.scrollTo(0, 0);
    setLoader({ isActive: true });
   
    SuggestFeatureGetAll();
  }, []);

  const SuggestFeatureGetAll = async () => {
    await user_service.SuggestFeatureGet(1).then((response) => {
      setLoader({ isActive: false });
      if (response) {
        setGetSuggest(response.data.data);
      }
    });
  };

  const handleUpdate = async (id, status) => {
    const newStatus = status == "1" ? "0" : status == "0" ? "1" : "0";

    const userData = {
      is_active: newStatus,
      suggestion: id
    };
  
    console.log(userData);
    try {
      setLoader({ isActive: true });
      const response = await user_service.SuggestFeatureUpdate(id, userData);

      if (response) {
        console.log(response.data);
        setLoader({ isActive: false });

        setToaster({
          type: "Suggestion Updated Successfully",
          isShow: true,
          toasterBody: response.data.message,
          message: "Suggestion Updated Successfully",
        });

        SuggestFeatureGetAll();
        setTimeout(() => {
          setToaster((prevToaster) => ({ ...prevToaster, isShow: false }));
        }, 2000);
      }
    } catch (err) {
      setToaster({
        type: "API Error",
        isShow: true,
        toasterBody: err.response.data.message,
        message: "API Error",
      });

      setTimeout(() => {
        setToaster((prevToaster) => ({ ...prevToaster, isShow: false }));
      }, 2000);
    }
  };
  return (
    <div className="bg-secondary float-left w-100 pt-4">
      <Loader isActive={isActive} />
      {isShow && (
        <Toaster
          types={types}
          isShow={isShow}
          toasterBody={toasterBody}
          message={message}
        />
      )}
      <main className="page-wrapper">
        {/* <!-- Page content--> */}
        {/* <!-- Page container--> */}
        <div className="">
          {/* <!-- Page card like wrapper--> */}

          <div className="">
            {/* <!-- Content--> */}
            <div className="">
              <div className="d-flex align-items-center justify-content-start mb-4">
                <h3 className="text-white mb-0">Suggest Feature Listings</h3>
              </div>
          
              {/* <!-- Item--> */}
              {console.log(getSuggest)}
              {getSuggest.map((post) => (
              <div className="card card-horizontal border mb-3 p-3"
                  // onClick={(e) => SingleListing(post._id)}
                >
                  <div className="card-body position-relative p-0">
                    <p className="mb-2 fs-sm">
                      <strong>Name:</strong> {post.full_name}
                    </p>
                    <p className="mb-2 fs-sm">
                      <strong>Email:</strong> {post.email}
                    </p>
                    <p className="mb-2 fs-sm">
                      <strong>Job Position:</strong> {post.job_position}
                    </p>
                    <p className="mb-2 fs-sm">
                      <strong>Company:</strong> {post.company}
                    </p>
                    <p className="mb-2 fs-sm">
                      <strong>
                        May we contact you about this feature suggestion?:
                      </strong>
                      $ {post.can_contact}
                    </p>
                    <p className="mb-2 fs-sm">
                      <strong>Description:</strong> {post.description}
                    </p>
                    <p className="mb-2 fs-sm">
                      <strong>Solved Problem:</strong> {post.solvedproblem}
                    </p>
                  </div>
                  <div className="pull-right mt-3 d-flex align-items-center justify-content-center">
                    <button
                      type="button"
                      className="btn btn-info pull-right"
                      onClick={() => handleUpdate(post._id, post.is_active)}
                    >
                     {
                        post.is_active== "1" ?
                        "Verify Now " :
                        "Verified"
                     } 
                    </button>
                  </div>
                </div>
              ))}
            </div>
          </div>
        </div>
      </main>
    </div>
  );
};

export default SuggestFeature;

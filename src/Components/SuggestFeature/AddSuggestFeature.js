import React, { useState, useEffect } from "react";
import _ from "lodash";
import user_service from "../service/user_service";
import jwt from "jwt-decode";
import Loader from "../../Pages/Loader/Loader.js";
import Toaster from "../../Pages/Toaster/Toaster.js";
import { useNavigate, NavLink, useParams } from "react-router-dom";

function AddSuggestFeature() {
  const initialValues = {
    full_name: "",
    email: "",
    job_position: "",
    company: "",
    can_contact: "yes",
    description: "",
    solvedproblem: "",
  };

  const [formValues, setFormValues] = useState(initialValues);

  const [formErrors, setFormErrors] = useState({});
  const [isSubmitClick, setISSubmitClick] = useState(false);
  const [successmessage, setSuccessmessage] = useState("");
  
  const [loader, setLoader] = useState({ isActive: null });
  const { isActive } = loader;
  const [toaster, setToaster] = useState({
    types: null,
    isShow: null,
    toasterBody: "",
    message: "",
  });
  const { types, isShow, toasterBody, message } = toaster;

  const navigate = useNavigate();
  const params = useParams();

  // onChange Function start
  const handleChange = (e) => {
    const { name, value } = e.target;
    setFormValues({ ...formValues, [name]: value });
  };
  // onChange Function end

  {
    /* <!-- Form Validation Start--> */
  }
  useEffect(() => {
    if (formValues && isSubmitClick) {
      validate();
    }
  }, [formValues]);

  const validate = () => {
    const values = formValues;
    const errors = {};
    if (!values.full_name) {
      errors.full_name = "Full name is required";
    }


    if (!values.email) {
      errors.email = "Email is required";
    } else if (
      !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i.test(values.email)
    ) {
      errors.email = "Invalid email address";
    }


    // if (!values.email) {
    //   errors.email = "email is required";
    // } else if (
    //   !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.email)
    // ) {
    //   errors.email = "Invalid email address";
    // }

    if (!values.job_position) {
      errors.job_position = "Job_Position is required";
    }

    if (!values.company) {
      errors.company = "Company is required";
    }

    if (!values.can_contact) {
      errors.can_contact = "Can_contact me is required";
    }

    if (!values.description) {
      errors.description = "Description me is required";
    }
    if (!values.solvedproblem) {
      errors.solvedproblem = "Solvedproblem me is required";
    }

    setFormErrors(errors);
    return errors;
  };
  {
    /* <!-- Form Validation End--> */
  }

  const handleSubmit = async (e) => {
    if(formValues?._id){
    e.preventDefault();
    setISSubmitClick(true);
    let checkValue = validate();
    if (1 == 1) {
      const userData = {
        agentId: jwt(localStorage.getItem("auth")).id,
        full_name: formValues ? formValues.full_name : '', 
        job_position: formValues ? formValues.job_position : '', 
        company: formValues ? formValues.company : '', 
        email: formValues ? formValues.email : '', 
        can_contact: formValues ? formValues.can_contact : '', 
        description: formValues ? formValues.description : '', 
        solvedproblem: formValues ? formValues.solvedproblem : '', 
      };
      setLoader({ isActive: true });
      try {
        setLoader({ isActive: true });
        const response = await user_service.SuggestFeatureUpdate(formValues?._id,userData);
  
        if (response) {
          //console.log(response.data);
          setLoader({ isActive: false });
          SuggestFeatureGetAll(response.data._id);
          setSuccessmessage("Your suggestion has been submitted and will be reviewed by our team. Thank you!");
  
          setToaster({
            type: "Your suggestion has been submitted and will be reviewed by our team. Thank you!",
            isShow: true,
            toasterBody: response.data.message,
            message: "Your suggestion has been submitted and will be reviewed by our team. Thank you!",
          });
  
          setTimeout(() => {
            setToaster((prevToaster) => ({ ...prevToaster, isShow: false }));
          }, 2000);
          window.scrollTo(0, 0);
          setFormValues(initialValues);
  
        }
      } catch (err) {
        setToaster({
          type: "API Error",
          isShow: true,
          toasterBody: err.response.data.message,
          message: "API Error",
        });
  
        setTimeout(() => {
          setToaster((prevToaster) => ({ ...prevToaster, isShow: false }));
        }, 2000);
      }
    }
    }
    else{
      e.preventDefault();
      setISSubmitClick(true);
      let checkValue = validate();
      if (1 == 1) {
        const userData = {
          agentId: jwt(localStorage.getItem("auth")).id,
          full_name: formValues ? formValues.full_name : '', 
          job_position: formValues ? formValues.job_position : '', 
          company: formValues ? formValues.company : '', 
          email: formValues ? formValues.email : '', 
          can_contact: formValues ? formValues.can_contact : '', 
          description: formValues ? formValues.description : '', 
          solvedproblem: formValues ? formValues.solvedproblem : '', 
        };
        setLoader({ isActive: true });
        try {
          setLoader({ isActive: true });
          const response = await user_service.SuggestFeature(userData);
    
          if (response) {
            //console.log(response.data);
            setLoader({ isActive: false });
            SuggestFeatureGetAll(response.data._id);
            setSuccessmessage("Your suggestion has been submitted and will be reviewed by our team. Thank you!");
    
            setToaster({
              type: "Your suggestion has been submitted and will be reviewed by our team. Thank you!",
              isShow: true,
              toasterBody: response.data.message,
              message: "Your suggestion has been submitted and will be reviewed by our team. Thank you!",
            });
    
            setTimeout(() => {
              setToaster((prevToaster) => ({ ...prevToaster, isShow: false }));
            }, 2000);
            window.scrollTo(0, 0);
            setFormValues(initialValues);
    
          }
        } catch (err) {
          setToaster({
            type: "API Error",
            isShow: true,
            toasterBody: err.response.data.message,
            message: "API Error",
          });
    
          setTimeout(() => {
            setToaster((prevToaster) => ({ ...prevToaster, isShow: false }));
          }, 2000);
        }
      }
    }
   
  };
  
  const SuggestFeatureGetAll = async (id) => {
    await user_service.SuggestFeatureGetId(id).then((response) => {
      setLoader({ isActive: false });
      if (response) {
        console.log(response);
        setFormValues(response.data);
      }
    });
  };
  
 

  {
    /* <!-- Api call Form onSubmit End--> */
  }

  return (
    <div className="bg-secondary float-left w-100 pt-4">
      <Loader isActive={isActive} />
      {isShow && (
        <Toaster
          types={types}
          isShow={isShow}
          toasterBody={toasterBody}
          message={message}
        />
      )}
      <main className="page-wrapper profile_page_wrap">
        <div className="content-overlay">
          <div className="justify-content-center pb-sm-2">
            <div className="add_listing w-100">
              <div className="float-left w-100 d-flex align-items-center justify-content-between mb-4">
                <h3 className="text-white mb-0">
                  {/* <i className="fi-info-circle text-primary fs-5 mt-n1 me-2"></i> */}
                  Suggest a Feature</h3>
              </div>
              {/* <hr className="my-3 w-100" /> */}
              <div className="row">
                <h6 className="mb-0" style={{color:"green"}}>{successmessage}</h6>
                <div className="col-md-8">
                  <div className="border bg-light rounded-3 p-3">
                    <div className="float-left w-100">
                      <div className="col-sm-12">
                        <label className="form-label">Full Name</label>
                        <input
                          className="form-control"
                          id="inline-form-input"
                          type="text"
                          name="full_name"
                          value={formValues.full_name ?? ""}
                          onChange={handleChange}
                          style={{
                            border: formErrors?.full_name
                              ? "1px solid red"
                              : "",
                          }}
                        />

                        {formErrors.full_name && (
                          <div className="invalid-tooltip">
                            {formErrors.full_name}
                          </div>
                        )}
                      </div>

                      <div className="col-sm-12 mt-3">
                        <label className="form-label">Email</label>
                        <input
                          className="form-control"
                          id="inline-form-input"
                          type="text"
                          name="email"
                          value={formValues.email ?? ""}
                          onChange={handleChange}
                          style={{
                            border: formErrors?.email ? "1px solid red" : "",
                          }}
                        />

                        {formErrors.email && (
                          <div className="invalid-tooltip">
                            {formErrors.email}
                          </div>
                        )}
                      </div>

                      <div className="col-sm-12 mt-3">
                        <label className="form-label">Job Position</label>
                        <input
                          className="form-control"
                          id="inline-form-input"
                          type="text"
                          name="job_position"
                          value={formValues.job_position ?? ""}
                          onChange={handleChange}
                          style={{
                            border: formErrors?.job_position
                              ? "1px solid red"
                              : "",
                          }}
                        />

                        {formErrors.job_position && (
                          <div className="invalid-tooltip">
                            {formErrors.job_position}
                          </div>
                        )}
                      </div>

                      <div className="col-sm-12 mt-3">
                        <label className="form-label">Company</label>
                        <input
                          className="form-control"
                          id="inline-form-input"
                          type="text"
                          name="company"
                          value={formValues.company ?? ""}
                          onChange={handleChange}
                          style={{
                            border: formErrors?.company ? "1px solid red" : "",
                          }}
                        />

                        {formErrors.company && (
                          <div className="invalid-tooltip">
                            {formErrors.company}
                          </div>
                        )}
                      </div>

                      <div className="col-sm-12 mt-3">
                        <label className="form-label">May we contact you about this feature suggestion?</label>
                        <select
                          className="form-select"
                          name="can_contact"
                          value={formValues.can_contact ?? ""}
                          onChange={handleChange}
                          style={{
                            border: formErrors?.can_contact ==="no"
                              ? "1px solid red"
                              : "",
                          }}
                        >
                          <option value="yes">Yes</option>
                          <option value="no">No</option>
                        </select>

                        {formErrors.can_contact && (
                          <div className="invalid-tooltip">
                            {formErrors.can_contact}
                          </div>
                        )}
                      </div>

                      <div className="col-sm-12 mt-3">
                        <label className="form-label">Please describe the feature you would like to see.</label>
                        <textarea
                          className="form-control mt-0"
                          id="textarea-input"
                          rows="5"
                          name="description"
                          onChange={handleChange}
                          autoComplete="on"
                          value={formValues.description ?? ""}
                          style={{
                            border: formErrors?.description
                              ? "1px solid red"
                              : "",
                          }}
                        ></textarea>
                        {formErrors.description && (
                          <div className="invalid-tooltip">
                            {formErrors.description}
                          </div>
                        )}
                      </div>

                      <div className="col-sm-12 mt-3">
                        <label className="form-label">What problem would this feature solve?</label>
                        <textarea
                          className="form-control mt-0"
                          id="textarea-input"
                          rows="5"
                          name="solvedproblem"
                          onChange={handleChange}
                          autoComplete="on"
                          value={formValues.solvedproblem ?? ""}
                          style={{
                            border: formErrors?.solvedproblem
                              ? "1px solid red"
                              : "",
                          }}
                        ></textarea>
                        {formErrors.solvedproblem && (
                          <div className="invalid-tooltip">
                            {formErrors.solvedproblem}
                          </div>
                        )}
                      </div>

                     
                    </div>
                  </div>
                  <div className="pull-right mt-3">
                        <button
                          type="button"
                          className="btn btn-primary btn-sm float-right pull-right"
                          onClick={handleSubmit}>
                          Submit
                        </button>
                  </div>
                </div>

                <div className="col-md-4 mt-lg-0 mt-md-0 mt-sm-4 mt-4">
                  <div id="accordionCards" className="">
                    <div className="card bg-secondary mb-2 mt-0">
                      <div className="card-body">
                        <h3 className="h3 card-title pt-1 mb-0">❤️ We love your suggestions!</h3>
                      </div>
                      <div className="collapse show" id="cardCollapse1" data-bs-parent="#accordionCards">
                        <div className="card-body mt-n1 pt-0">
                          <p className="fs-sm">
                            <strong>Indepth Realty are committed to community driven features</strong><br /><br />
                            Your input is invaluable, and we're always looking for ways to make our product even better. We know that you use our product day in and day out, and you have unique insights into how it can best meet your needs.
                            <br /><br />
                            So, we're inviting you to share your product feature suggestions with us. Whether it's a small tweak that would make your daily tasks easier or a big idea that could revolutionize your experience, we want to hear it all! Your ideas can help shape the future of our product.
                          </p>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </main>
    </div>
  );
}
export default AddSuggestFeature;

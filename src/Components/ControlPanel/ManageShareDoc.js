import React, { useState, useEffect } from "react";
import _ from "lodash";
import user_service from "../service/user_service";
import Loader from "../../Pages/Loader/Loader.js";
import Toaster from "../../Pages/Toaster/Toaster.js";
import { NavLink } from "react-router-dom";
import jwt from "jwt-decode";
import PagesNavigationUI from "../../Pages/PagesNavigationUI.js";
import Swal from "sweetalert2";
import "sweetalert2/dist/sweetalert2.css";

function ManageShareDoc() {
  const [loader, setLoader] = useState({ isActive: null });
  const { isActive } = loader;
  const [toaster, setToaster] = useState({
    types: null,
    isShow: null,
    toasterBody: "",
    message: "",
  });
  const { types, isShow, toasterBody, message } = toaster;
  const [documentGet, setDocumentGet] = useState([]);
  const [copySuccessMaps, setCopySuccessMaps] = useState({});

  useEffect(() => {
    window.scrollTo(0, 0);
    setLoader({ isActive: true });
    ShareDocGet();
  }, []);

  const ShareDocGet = async () => {
    await user_service
      .ShareDocGet(localStorage.getItem("active_office"))
      .then((response) => {
        setLoader({ isActive: false });
        if (response) {
          console.log(response);
          setDocumentGet(response.data);
        }
      });
  };

  const [showData, setShowData] = useState("");
  const handleDataClick = (id) => {
    console.log(id);
    setShowData(id);
  };

  const handleCopyUrls = (contentId, resource_url) => {
    navigator.clipboard
      .writeText(resource_url)
      .then(() => {
        console.log("URL copied to clipboard");
        setCopySuccessMaps((prevMap) => ({
          ...prevMap,
          [contentId]: true,
        }));

        // Reset the copy success message after a short delay
        setTimeout(() => {
          setCopySuccessMaps((prevMap) => ({
            ...prevMap,
            [contentId]: false,
          }));
        }, 2000);
      })
      .catch((error) => {
        console.error("Error copying URL to clipboard", error);
        // Handle the error or show an error message to the user
      });
  };

  function AccordionItem({ post, handleDataClick, isChild }) {
    const [loader, setLoader] = useState({ isActive: null });
    const { isActive } = loader;
    const [toaster, setToaster] = useState({
      types: null,
      isShow: null,
      toasterBody: "",
      message: "",
    });
    const { types, isShow, toasterBody, message } = toaster;
    const [isOpen, setIsOpen] = useState(false);
    const [copySuccessMap, setCopySuccessMap] = useState({});
    // const [documentGet, setDocumentGet] = useState([]);

    const toggleAccordion = () => {
      setIsOpen(!isOpen);
    };

    const handleCopyUrl = (contentId, resource_url) => {
      navigator.clipboard
        .writeText(resource_url)
        .then(() => {
          console.log("URL copied to clipboard");
          setCopySuccessMap((prevMap) => ({
            ...prevMap,
            [contentId]: true,
          }));

          // Reset the copy success message after a short delay
          setTimeout(() => {
            setCopySuccessMap((prevMap) => ({
              ...prevMap,
              [contentId]: false,
            }));
          }, 2000);
        })
        .catch((error) => {
          console.error("Error copying URL to clipboard", error);
          // Handle the error or show an error message to the user
        });
    };

    const handleRemove = async (id) => {
      Swal.fire({
        title: "Are you sure?",
        text: "It will be permanently deleted!",
        icon: "warning",
        showCancelButton: true,
        confirmButtonColor: "#3085d6",
        cancelButtonColor: "#d33",
        confirmButtonText: "Yes, delete it!",
      }).then(async (result) => {
        if (result.isConfirmed) {
          try {
            const response = await user_service.ShareDocDelete(id);
            if (response) {
              Swal.fire(
                "Deleted!",
                "Document is deleted permanently.",
                "success"
              ).then(() => {
                // Update the state to remove the deleted document
                ShareDocGet();
              });
            }
          } catch (error) {
            Swal.fire(
              "Error",
              "An error occurred while deleting the file.",
              "error"
            );
            console.error(error);
          }
        }
      });
    };

    const [formData, setFormData] = useState([]);

    const StaffRoleGetData = async () => {
      await user_service.StaffRoleGet().then((response) => {
        if (response) {
          console.log(response);
          setFormData(response.data.data);
        }
      });
    };
    useEffect(() => {
      if (formData) {
        StaffRoleGetData();
      }
    }, []);

    return (
      <>
        <Loader isActive={isActive} />
        {isShow && (
          <Toaster
            types={types}
            isShow={isShow}
            toasterBody={toasterBody}
            message={message}
          />
        )}
        <div
          className={`accordion-item ${isChild ? "ms-3" : ""}`}
          key={post._id}
        >
          <h6 className="accordion-header" id={`headingRequired${post._id}`}>
            <p className="d-flex align-items-center justify-content-between my-2 float-start w-100">
              {/* <span className="p-2">🗂️</span> */}
              <button
                className="accordion-button"
                type="button"
                onClick={toggleAccordion}
                aria-expanded={isOpen}
                aria-controls={`collapseRequired${post._id}`}
              >
                {post.title}
              </button>
              <div className="">
              {localStorage.getItem("auth") &&
              jwt(localStorage.getItem("auth")).contactType == "admin" ? (
                  <NavLink
                    onClick={(e) => handleRemove(post._id)}
                    to="#"
                    className="btn btn-secondary text-center "
                    type="button"
                  >
                    <i className="fa fa-trash-o me-2" aria-hidden="true"></i>
                    Delete
                  </NavLink>
              
              ) : (
                <>
                  {localStorage.getItem("auth") &&
                    jwt(localStorage.getItem("auth")).contactType == "staff" &&
                    (Array.isArray(formData) && formData.length > 0
                      ? formData.map((item) =>
                          item.roleStaff === "calender" ? (
                            <NavLink
                              onClick={(e) => handleRemove(post._id)}
                              to="#"
                              className="btn btn-secondary text-center "
                              type="button"
                            >
                              <i
                                className="fa fa-trash-o me-2"
                                aria-hidden="true"
                              ></i>
                              Delete
                            </NavLink>
                          ) : (
                            ""
                          )
                        )
                      : "")}
                </>
              )}

              {localStorage.getItem("auth") &&
              jwt(localStorage.getItem("auth")).contactType == "admin" ? (
                <div className="btn-group pull-right">
                  <button
                    type="button"
                    className="btn btn-primary dropdown-toggle ms-2"
                    data-bs-toggle="dropdown"
                    aria-haspopup="true"
                    aria-expanded="false"
                  >
                    Option
                  </button>
                  <div className="dropdown-menu dropdown-menu-end my-1">
                    <NavLink
                      to={`/control-panel/add-section/`}
                      className="dropdown-item"
                    >
                      Add Section
                    </NavLink>
                    <NavLink
                      to={`/control-panel/documents/attach/`}
                      className="dropdown-item mt-1"
                    >
                      Add Document
                    </NavLink>
                    <NavLink
                      to={`/control-panel/documents/add-content/`}
                      className="dropdown-item mt-1"
                    >
                      Add Content
                    </NavLink>
                    <NavLink
                      to={`/control-panel/documents/add-link/`}
                      className="dropdown-item mt-1"
                    >
                      Add Link
                    </NavLink>
                  </div>
                </div>
              ) : (
                <>
                  {localStorage.getItem("auth") &&
                    jwt(localStorage.getItem("auth")).contactType == "staff" &&
                    (Array.isArray(formData) && formData.length > 0
                      ? formData.map((item) =>
                          item.roleStaff === "calender" ? (
                            <div className="btn-group pull-right">
                              <button
                                type="button"
                                className="btn btn-primary dropdown-toggle ms-2"
                                data-bs-toggle="dropdown"
                                aria-haspopup="true"
                                aria-expanded="false"
                              >
                                Option
                              </button>
                              <div className="dropdown-menu dropdown-menu-end my-1">
                                <NavLink
                                  to={`/control-panel/add-section/`}
                                  className="dropdown-item"
                                >
                                  Add Section
                                </NavLink>
                                <NavLink
                                  to={`/control-panel/documents/attach/`}
                                  className="dropdown-item mt-1"
                                >
                                  Add Document
                                </NavLink>
                                <NavLink
                                  to={`/control-panel/documents/add-content/`}
                                  className="dropdown-item mt-1"
                                >
                                  Add Content
                                </NavLink>
                                <NavLink
                                  to={`/control-panel/documents/add-link/`}
                                  className="dropdown-item mt-1"
                                >
                                  Add Link
                                </NavLink>
                              </div>
                            </div>
                          ) : (
                            ""
                          )
                        )
                      : "")}
                </>
              )}
              </div>
            </p>
            <hr className="float-start w-100" />
          </h6>
          <div
            className={`accordion-collapse collapse ${isOpen ? "show" : ""}`}
            aria-labelledby={`collapseRequired${post._id}`}
            data-bs-parent="#accordionExample"
            id={`collapseRequired${post._id}`}
          >
            <div className="accordion-body float-start w-100 mt-1">
              {post.children && post.children.length > 0
                ? post.children.map((childPost) => (
                    <>
                      <AccordionItem
                        key={childPost._id}
                        post={childPost}
                        handleDataClick={handleDataClick}
                        isChild={true}
                      />
                    </>
                  ))
                : null}
                 <div>
                {post.content && post.content.length > 0 ? (
                  <>
                    {/* Render "Files" and "Actions" headings only once */}
                    <div className="row d-flex justify-content-between">
                      <div className="col-md-10">
                        <p className="mb-2 text-left">Files</p>
                      </div>
                      <div className="col-md-2">
                        <p className="mb-2 text-center">Actions</p>
                      </div>
                    </div>
                    {post.content.map((content) => (
                      <div
                        key={content._id}
                        className="content float-start w-100 mb-3"
                      >
                        {content.type === "content" ? (
                          <div className="content-wrapper">
                            <NavLink
                              to={`/control-panel/documents/content/list/${content._id}`}
                            >
                              <span className="title">{content.title}</span>
                            </NavLink>
                            {(localStorage.getItem("auth") &&
                              jwt(localStorage.getItem("auth")).contactType ===
                                "admin") ||
                            jwt(localStorage.getItem("auth")).contactType ===
                              "staff" ? (
                              <NavLink
                                to={`/control-panel/documents/add-content/${content._id}`}
                                className="action-link"
                              >
                                <span className="edit">Edit</span>
                              </NavLink>
                            ) : (
                              <a
                                href={content.resource_url}
                                className="action-link"
                                target="_blank"
                                rel="noreferrer"
                              >
                                View/Download
                              </a>
                            )}
                          </div>
                        ) : (
                          <div className="row">
                            <div className="col-md-10">
                              <div className="float-start w-100">
                                <div className="files_view mb-0 d-flex align-items-center">
                                  <a
                                    className="float-start w-75"
                                    href={content.resource_url}
                                    target="_blank"
                                    rel="noreferrer"
                                  >
                                    {content.title}
                                  </a>
                                  &nbsp;
                                  <i
                                    className="fa fa-clone ms-2"
                                    onClick={() =>
                                      handleCopyUrl(
                                        content._id,
                                        content.resource_url
                                      )
                                    }
                                  ></i>
                                  {copySuccessMap[content._id] && (
                                    <p
                                      className="float-start w-100 mb-0 mt-3"
                                      style={{ color: "green" }}
                                    >
                                      URL copied successfully!
                                    </p>
                                  )}
                                </div>
                              </div>
                            </div>
                            <div className="col-md-2">
                              <div className="float-start w-100 text-center">
                                {(localStorage.getItem("auth") &&
                                  jwt(localStorage.getItem("auth"))
                                    .contactType === "admin") ||
                                jwt(localStorage.getItem("auth"))
                                  .contactType === "staff" ? (
                                  <NavLink
                                    to={`/control-panel/documents/content/edit/${content._id}`}
                                    className="p-0 border-0 mt-1"
                                  >
                                    <i className="h2 fi-edit opacity-80 mb-0"></i>
                                    {/* Edit */}
                                  </NavLink>
                                ) : (
                                  <a
                                    href={content.resource_url}
                                    className="p-0 border-0 mt-1"
                                    target="_blank"
                                    rel="noreferrer"
                                  >
                                    <i className="h2 fi-cloud-download opacity-80 mb-0"></i>
                                    {/* View/Download */}
                                  </a>
                                )}
                              </div>
                            </div>
                          </div>
                        )}
                      </div>
                    ))}
                  </>
                ) : (
                  <p>No content available</p>
                )}
                </div>
            </div>
          </div>
        </div>
      </>
    );
  }

  return (
    <div className="bg-secondary float-left w-100 mb-4 pt-4">
      <Loader isActive={isActive} />
      {isShow && (
        <Toaster
          types={types}
          isShow={isShow}
          toasterBody={toasterBody}
          message={message}
        />
      )}
      <div className="">
        <main className="page-wrapper homepage_layout">
          <div className="row">
            <div className="col-md-12">
              {(localStorage.getItem("auth") &&
                jwt(localStorage.getItem("auth")).contactType == "admin") ||
              jwt(localStorage.getItem("auth")).contactType == "staff" ? (
                <div className="btn-group pull-right">
                  <button
                    type="button"
                    className="btn btn-primary dropdown-toggle"
                    data-bs-toggle="dropdown"
                    aria-haspopup="true"
                    aria-expanded="false"
                  >
                    Action
                  </button>
                  <div className="dropdown-menu dropdown-menu-end my-1">
                    <NavLink
                      to={`/control-panel/add-section/`}
                      className="dropdown-item"
                    >
                      Add Section
                    </NavLink>
                    <NavLink
                      to={`/control-panel/documents/attach/`}
                      className="dropdown-item mt-1"
                    >
                      Add Document
                    </NavLink>
                    <NavLink
                      to={`/control-panel/documents/add-content/`}
                      className="dropdown-item mt-1"
                    >
                      Add Content
                    </NavLink>
                    <NavLink
                      to={`/control-panel/documents/add-link/`}
                      className="dropdown-item mt-1"
                    >
                      Add Link
                    </NavLink>
                  </div>
                </div>
              ) : (
                ""
              )}
              <h3 className="mb-4 text-white">Training Documents</h3>
              <p className="text-white">
                Offering a wealth of tools and materials designed to enhance
                your knowledge and skills.
              </p>
              {/* <hr className="mb-3" /> */}
              {/* <input
                id="search-input-1"
                type="search"
                className="form-control"
                placeholder="Search documents by folder name, title,blurb,etc"
                name="searchdata"
              /> */}
            </div>

            <div>
              {documentGet.data && documentGet.data.length > 0 ? (
                documentGet.data.map((item) => (
                  <div className="row" key={item.title}>
                    <div className="col-md-8">
                      <div className="my_documents bg-light border rounded-3 p-lg-3 p-md-3 p-sm-3 p-2 float-start w-100">
                        <h6 className="">{item.title}</h6>
                        {item.children && item.children.length > 0
                          ? item.children.map((post) => (
                              <AccordionItem
                                key={post._id}
                                post={post}
                                documentGet={documentGet}
                                handleDataClick={handleDataClick}
                              />
                            ))
                          : null}

                        {item.content && item.content.length > 0
                          ? item.content.map((post) => (
                              <>
                                <div className="float-start w-100 mt-3">
                                  {post.type == "content" ? (
                                    <div className="">
                                      <NavLink
                                        className="mt-0"
                                        to={`/control-panel/documents/content/list/${post._id}`}
                                      >
                                        <span>{post.title}</span>
                                      </NavLink>
                                      {(localStorage.getItem("auth") &&
                                        jwt(localStorage.getItem("auth"))
                                          .contactType == "admin") ||
                                      jwt(localStorage.getItem("auth"))
                                        .contactType == "staff" ? (
                                        <NavLink
                                          className="mt-0"
                                          to={`/control-panel/documents/add-content/${post._id}`}
                                        >
                                          <i class="h2 fi-edit opacity-80 mb-0 pull-right"></i>

                                          {/* <span className="pull-right">Edit</span> */}
                                        </NavLink>
                                      ) : (
                                        <a
                                          className="mt-0"
                                          href={`/control-panel/documents/content/list/${post._id}`}
                                        >
                                          <span className="pull-right">
                                            View/Download
                                          </span>
                                        </a>
                                      )}
                                    </div>
                                  ) : (
                                    ""
                                  )}

                                  {post.type == "document" ? (
                                    <div>
                                      <a
                                        className="mt-0"
                                        href={post.resource_url}
                                        target="_blank"
                                        rel="noreferrer"
                                      >
                                        {post.title}
                                      </a>{" "}
                                      &nbsp;
                                      <i
                                        className="fa fa-clone"
                                        onClick={() =>
                                          handleCopyUrls(
                                            post._id,
                                            post.resource_url
                                          )
                                        }
                                      ></i>
                                      {copySuccessMaps[post._id] && (
                                        <p style={{ color: "green" }}>
                                          URL copied successfully!
                                        </p>
                                      )}
                                      {/* <a
                                  href={post.resource_url}
                                    className="mt-0"
                                    target="_blank"
                                    rel="noreferrer"
                                  >
                                    <span>{post.title}</span>
                                  </a> */}
                                      {(localStorage.getItem("auth") &&
                                        jwt(localStorage.getItem("auth"))
                                          .contactType == "admin") ||
                                      jwt(localStorage.getItem("auth"))
                                        .contactType == "staff" ? (
                                        <NavLink
                                          className="mt-0"
                                          to={`/control-panel/documents/attach/${post._id}`}
                                        >
                                          <i class="h2 fi-edit opacity-80 mb-0 pull-right"></i>

                                          {/* <span className="pull-right">Edit</span> */}
                                        </NavLink>
                                      ) : (
                                        <a
                                          className="mt-0"
                                          href={post.resource_url}
                                          target="_blank"
                                          rel="noreferrer"
                                        >
                                          <span className="pull-right">
                                            View/Download
                                          </span>
                                        </a>
                                      )}
                                    </div>
                                  ) : (
                                    ""
                                  )}

                                  {post.type == "link" ? (
                                    <div className="">
                                      <a
                                        href={post.resource_url}
                                        className="mt-0"
                                        target="_blank"
                                        rel="noreferrer"
                                      >
                                        <span>{post.title}</span>
                                      </a>
                                      {localStorage.getItem("auth") &&
                                      jwt(localStorage.getItem("auth"))
                                        .contactType == "admin" ? (
                                        <NavLink
                                          className="mt-0"
                                          to={`/control-panel/documents/add-link/${post._id}`}
                                        >
                                          <i class="h2 fi-edit opacity-80 mb-0 pull-right"></i>

                                          {/* <span className="pull-right">Edit</span> */}
                                        </NavLink>
                                      ) : (
                                        <a
                                          className="mt-0"
                                          href={post.resource_url}
                                          target="_blank"
                                          rel="noreferrer"
                                        >
                                          <span className="pull-right">
                                            View/Download
                                          </span>
                                        </a>
                                      )}
                                    </div>
                                  ) : (
                                    ""
                                  )}
                                </div>
                              </>
                            ))
                          : null}
                      </div>
                    </div>
                    <div className="col-md-4 mt-lg-0 mt-md-0 mt-sm-0 mt-4">
                      <h6 className="mb-3 text-white">FAQ</h6>
                      <div className="accordion" id="accordionExample">
                        <div className="accordion-item">
                          <h2 className="accordion-header" id="headingOne">
                            <button
                              className="accordion-button"
                              type="button"
                              data-bs-toggle="collapse"
                              data-bs-target="#collapseOne"
                              aria-expanded="true"
                              aria-controls="collapseOne"
                            >
                              Are these training documents updated regularly?
                            </button>
                          </h2>
                          <div
                            className="accordion-collapse collapse show"
                            aria-labelledby="headingOne"
                            data-bs-parent="#accordionExample"
                            id="collapseOne"
                          >
                            <div className="accordion-body">
                              Yes, our training documents are regularly reviewed
                              and updated to ensure they align with the latest
                              industry standards, best practices, and legal
                              regulations. Real estate is an evolving field, and
                              it's crucial for our associates to have access to
                              the most up-to-date information. At InDepth REalty
                              we take pride in providing training materials that
                              are current and relevant to the ever-changing real
                              estate landscape.
                            </div>
                          </div>
                        </div>
                        <div className="accordion-item">
                          <h2 className="accordion-header" id="headingTwo">
                            <button
                              className="accordion-button collapsed"
                              type="button"
                              data-bs-toggle="collapse"
                              data-bs-target="#collapseTwo"
                              aria-expanded="false"
                              aria-controls="collapseTwo"
                            >
                              This is a placeholder FAQ
                            </button>
                          </h2>
                          <div
                            className="accordion-collapse collapse"
                            aria-labelledby="headingTwo"
                            data-bs-parent="#accordionExample"
                            id="collapseTwo"
                          >
                            <div className="accordion-body">
                              Lorem ipsum dolor, sit amet consectetur
                              adipisicing elit. Cumque dicta enim cupiditate
                              natus dolorum distinctio, impedit tenetur nisi
                              laboriosam ut animi delectus quod quos ipsum
                              corporis magnam, nobis neque mollitia.
                            </div>
                          </div>
                        </div>
                        <div className="accordion-item">
                          <h2 className="accordion-header" id="headingThree">
                            <button
                              className="accordion-button collapsed"
                              type="button"
                              data-bs-toggle="collapse"
                              data-bs-target="#collapseThree"
                              aria-expanded="false"
                              aria-controls="collapseThree"
                            >
                              This is another placeholder FAQ
                            </button>
                          </h2>
                          <div
                            className="accordion-collapse collapse"
                            aria-labelledby="headingThree"
                            data-bs-parent="#accordionExample"
                            id="collapseThree"
                          >
                            <div className="accordion-body">
                              Lorem ipsum dolor sit amet consectetur adipisicing
                              elit. Libero ut accusantium ea a ipsa, aliquam
                              nemo aperiam porro deserunt aspernatur sequi amet
                              voluptatibus, fugiat nobis. Atque voluptatibus
                              quibusdam placeat voluptas?
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                ))
              ) : (
                <div className="float-left w-100 text-center mt-3">
                  <i
                    className="fa fa-file-text-o text-white"
                    aria-hidden="true"
                    style={{ fontSize: "100px" }}
                  ></i>
                  <p className="float-left w-100 text-white mt-3">
                    No Documents
                  </p>
                </div>
              )}
            </div>
          </div>
        </main>
      </div>
    </div>
  );
}
export default ManageShareDoc;

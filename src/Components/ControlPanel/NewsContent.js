import React, { useState, useEffect } from "react";
import user_service from "../service/user_service";
import Loader from "../../Pages/Loader/Loader.js";
import Toaster from "../../Pages/Toaster/Toaster";
import { NavLink, useNavigate } from "react-router-dom";
import { now } from "lodash";
import { Editor } from "react-draft-wysiwyg";
import { EditorState, ContentState, convertToRaw } from "draft-js";
import draftToHtml from "draftjs-to-html";
import htmlToDraft from "html-to-draftjs";
import "react-draft-wysiwyg/dist/react-draft-wysiwyg.css";
const NewsContent = () => {



  const [loader, setLoader] = useState({ isActive: null });
  const { isActive } = loader;
  const [toaster, setToaster] = useState({
    types: null,
    isShow: null,
    toasterBody: "",
    message: "",
  });
  const { types, isShow, toasterBody, message } = toaster;

  const [formValues, setFormValues] = useState({ newsContent: "" });

  //   const navigate = useNavigate();

  const [editorState, setEditorState] = useState(EditorState.createEmpty());


  const PlatformdetailsGet = async () => {
    await user_service.platformdetailsGet().then((response) => {
      if (response) {
        console.log(response);
        setFormValues(response.data.data[0]);

        const description = response.data.data[0].newsContent;

        if (description) {
          const contentBlock = htmlToDraft(description);

          if (contentBlock) {
            const contentState = ContentState.createFromBlockArray(
              contentBlock.contentBlocks
            );
            setEditorState(EditorState.createWithContent(contentState));
          }
        } else {
          console.warn("description is undefined or null.");
        }
      }
    });
  };

  useEffect(() => {
    window.scrollTo(0, 0);
    PlatformdetailsGet();
  }, []);

  // const handleChange = (e) => {
  //   const { name, value } = e.target;
  //   setFormValues({ ...formValues, [name]: value });
  // };

  const handleChange = (editorState) => {
    setEditorState(editorState);
    const htmlContent = draftToHtml(
      convertToRaw(editorState.getCurrentContent())
    );
    setFormValues({ ...formValues, newsContent: htmlContent });
  };


  const handleSubmit = async (e) => {
    if (formValues?._id) {
      e.preventDefault();
      const userData = {
        newsContent: formValues.newsContent,
        newsDate: new Date().toISOString(),
      };
      console.log(userData);
      setLoader({ isActive: true });
      await user_service
        .platformdetailsUpdate(formValues?._id, userData)
        .then((response) => {
          if (response) {
            setFormValues(response.data);
            setLoader({ isActive: false });
            setToaster({
              type: "Update PlatformDetails",
              isShow: true,
              toasterBody: response.data.message,
              message: "Update PlatformDetails Successfully",
            });
            setTimeout(() => {
              setToaster((prevToaster) => ({ ...prevToaster, isShow: false }));
            }, 1000);
          } else {
            setLoader({ isActive: false });
            setToaster({
              types: "error",
              isShow: true,
              toasterBody: response.data.message,
              message: "Error",
            });
          }
        });
    }
  };

  return (
    <div className="bg-secondary float-left w-100 pt-4">
      <Loader isActive={isActive} />
      {isShow && (
        <Toaster
          types={types}
          isShow={isShow}
          toasterBody={toasterBody}
          message={message}
        />
      )}
      <main className="page-wrapper">
        {/* <!-- Page content--> */}
        {/* <!-- Page container--> */}
        <div className="">
          {/* <!-- Page card like wrapper--> */}

          <div className="">
            {/* <!-- Content--> */}
            <div className="">
              <div className="d-flex align-items-center justify-content-start mb-4">
                <h3 className="text-white mb-0">Add News Letter Content</h3>
              </div>
              <div className="row">
                <div className="col-md-12">
                  <div className="bg-light border rounded-3 p-3">
                    <div className="col-sm-12 mb-3">
                      <label className="form-label">News Content</label>
                      {/* <textarea
                        className="form-control mt-0"
                        id="textarea-input"
                        rows="5"
                        name="newsContent"
                        value={formValues.newsContent}
                        onChange={handleChange}
                      ></textarea> */}

                      <Editor
                        editorState={editorState}
                        onEditorStateChange={handleChange}
                        value={formValues.newsContent}

                        toolbar={{
                          options: [
                            "inline",
                            "blockType",
                            "fontSize",
                            "list",
                            "textAlign",
                            "history",
                            "link", // Add link option here
                          ],
                          inline: {
                            options: [
                              "bold",
                              "italic",
                              "underline",
                              "strikethrough",
                            ],
                          },
                          list: { options: ["unordered", "ordered"] },
                          textAlign: {
                            options: ["left", "center", "right"],
                          },
                          history: { options: ["undo", "redo"] },
                          link: {
                            // Configure link options
                            options: ["link", "unlink"],
                          },
                        }}
                        wrapperClassName="demo-wrapper"
                        editorClassName="demo-editor"
                        editorStyle={{ height: '400px' }}
                       />
                    </div>
                  </div>
                  <div className="pull-right mt-3">
                    <NavLink
                      type="button"
                      className="btn btn-secondary pull-right ms-3"
                      to="/control-panel"
                    >
                      Cancel
                    </NavLink>
                    <button
                      className="btn btn-primary"
                      type="button"
                      onClick={handleSubmit}
                    >
                      Submit
                    </button>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </main>
    </div>
  );
};

export default NewsContent;

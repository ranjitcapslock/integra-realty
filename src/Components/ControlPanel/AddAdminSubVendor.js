import React, { useState, useEffect, useRef } from "react";
import { useNavigate, NavLink, useParams } from "react-router-dom";
import user_service from "../service/user_service.js";
import Loader from "../../Pages/Loader/Loader.js";
import Toaster from "../../Pages/Toaster/Toaster.js";

import jwt from "jwt-decode";
import _ from "lodash";
import axios from "axios";
import { Editor } from "react-draft-wysiwyg";
import { EditorState, ContentState, convertToRaw } from "draft-js";
import draftToHtml from "draftjs-to-html";
import htmlToDraft from "html-to-draftjs";
import "react-draft-wysiwyg/dist/react-draft-wysiwyg.css";
import $, { event } from "jquery";
const AddAdminSubVendor = () => {
  const [loader, setLoader] = useState({ isActive: null });
  const { isActive } = loader;
  const [toaster, setToaster] = useState({
    types: null,
    isShow: null,
    toasterBody: "",
    message: "",
  });
  const { types, isShow, toasterBody, message } = toaster;

  const initialValues = {
    category: "vendor",
    title: "",
    description: "",
    image: "",
    status: "1",
    // video_url: video_url,
  };
  const [formValues, setFormValues] = useState(initialValues);

  const [formErrors, setFormErrors] = useState({});
  const [isSubmitClick, setISSubmitClick] = useState(false);

  const [acceptedFileTypes, setAcceptedFileTypes] = useState([]);
  const [promoteProduct, setPromoteProduct] = useState("");
  const [imageData, setImagedData] = useState("");

  const navigate = useNavigate();
  const params = useParams();

  const handleChange = (e) => {
    const { name, value } = e.target;
    setFormValues({ ...formValues, [name]: value });
  };

  useEffect(() => {
    window.scrollTo(0, 0);
    if (formValues && isSubmitClick) {
      validate();
    }
  }, [formValues]);

  const validate = () => {
    const values = formValues;
    const errors = {};

    if (!values.title) {
      errors.title = "Title is required";
    }

    if (!values.description) {
      errors.description = "Description is required";
    }

    if (imageData) {
    } else {
      if (!values.image) {
        errors.image = "Image is required";
      }
    }

    setFormErrors(errors);
    return errors;
  };

  const handleFileChange = async (e) => {
    const selectedFile = e.target.files[0];

    if (selectedFile) {
      //setFileExtension(selectedFile.name.split('.').pop());

      const formData = new FormData();
      formData.append("file", selectedFile);
      const config = {
        headers: {
          Accept: "application/json",
          "Content-Type": "multipart/form-data",
          Authorization: `Bearer ${localStorage.getItem("auth")}`,
        },
      };
      try {
        const uploadResponse = await axios.post(
          "https://api.brokeragentbase.com/upload",
          // "http://localhost:4000/upload",

          formData,
          config
        );
        const uploadedFileData = uploadResponse.data;
        setImagedData(uploadedFileData);
      } catch (error) {
        console.error("Error occurred during file upload:", error);
      }
    }
  };


  console.log(promoteProduct.category);
  const handleSubmit = async (e) => {
    if (params.addProduct) {
      e.preventDefault();
      setISSubmitClick(true);
      let checkValue = validate();
      if (_.isEmpty(checkValue)) {
        const userData = {
          agentId: jwt(localStorage.getItem("auth")).id,
          category: promoteProduct?.category,
          category_id: params.id,
          title: formValues.title,
          description: formValues.description,
          image: imageData,
          status: "1",
        };

        await user_service
          .UpdateSubVendor(params.addProduct, userData)
          .then((response) => {
            if (response) {
              setFormValues(response.data);
              setLoader({ isActive: false });
              setToaster({
                type: "Item Added Successfully",
                isShow: true,
                message: "Item Added Successfully",
              });
              setTimeout(() => {
                setToaster((prevToaster) => ({
                  ...prevToaster,
                  isShow: false,
                }));
                navigate(`/admin/svendor/${params.id}`);
              }, 2000);
            } else {
              setLoader({ isActive: false });
              setToaster({
                types: "error",
                isShow: true,
                toasterBody: response.data.message,
                message: "Error",
              });
            }
          });
      }
    } else {
      e.preventDefault();
      setISSubmitClick(true);
      let checkValue = validate();
      if (_.isEmpty(checkValue)) {
        const userData = {
          agentId: jwt(localStorage.getItem("auth")).id,
          category: promoteProduct?.category,
          category_id: params.id,
          title: formValues.title,
          description: formValues.description,
          image: imageData,
          status: "1",
        };

        console.log(userData);
        await user_service.AddSubVendor(userData).then((response) => {
          if (response) {
            setFormValues(response.data);
            setLoader({ isActive: false });
            setToaster({
              type: "Item Added Successfully",
              isShow: true,
              message: "Item Added Successfully",
            });
            setTimeout(() => {
              setToaster((prevToaster) => ({ ...prevToaster, isShow: false }));
              navigate(`/admin/sub-Vendor/${params.id}`);
            }, 2000);
          } else {
            setLoader({ isActive: false });
            setToaster({
              types: "error",
              isShow: true,
              toasterBody: response.data.message,
              message: "Error",
            });
          }
        });
      }
    }
  };

 

  useEffect(() => {
    window.scrollTo(0, 0);
    const PromoteProductGetId = async () => {
      await user_service.promotoProductsGetId(params.id).then((response) => {
        setLoader({ isActive: false });
        if (response) {
          setPromoteProduct(response.data);
        }
      });
    };
    PromoteProductGetId();
  }, []);


  useEffect(() => {
    window.scrollTo(0, 0);
    const SubCategoryVendorId = async () => {
      if(params.addProduct){
        await user_service
          .subCategoryVendorId(params.addProduct)
          .then((response) => {
            setLoader({ isActive: false });
            if (response) {
              setFormValues(response.data);
            }
          });
      }
    };
    SubCategoryVendorId();
  }, []);
  return (
    <div className="bg-secondary float-left w-100 pt-4 mb-4">
      <Loader isActive={isActive} />
      {isShow && (
        <Toaster
          types={types}
          isShow={isShow}
          toasterBody={toasterBody}
          message={message}
        />
      )}
      <main className="page-wrapper profile_page_wrap">
        <div className="content-overlay">
          <div className="row">
            <div className="col-md-8">
              <h2 className="text-white">{promoteProduct.title}</h2>
              <div className="bg-light border rounded-3 p-3">
                <div className="col-md-12">
                  <label className="form-label">
                    Title &nbsp;<b>*</b>
                    <br />
                  </label>
                  <input
                    className="form-control"
                    type="text"
                    id="text-input"
                    name="title"
                    value={formValues.title}
                    onChange={handleChange}
                    style={{
                      border: formErrors?.title
                        ? "1px solid red"
                        : "1px solid #00000026",
                    }}
                  />
                  {formErrors.title && (
                    <div className="invalid-tooltip">{formErrors.title}</div>
                  )}
                </div>

                <div className="col-md-12 mt-3">
                  <label className="form-label" for="pr-country">
                    Description &nbsp;<b>*</b>
                    <br />
                  </label>
                  <input
                    className="form-control"
                    id="inline-form-input"
                    type="text"
                    name="description"
                    value={formValues.description}
                    onChange={handleChange}
                    style={{
                      border: formErrors?.description
                        ? "1px solid red"
                        : "1px solid #00000026",
                    }}
                  />
                  {formErrors.description && (
                    <div className="invalid-tooltip">
                      {formErrors.description}
                    </div>
                  )}
                </div>
                <div className="col-md-12 mt-3">
                  <label className="form-label" for="pr-country">
                    Catalog Thumbnail &nbsp;<b>*</b>
                    <br />
                  </label>
                  <input
                    className="form-control"
                    id="inline-form-input"
                    type="file"
                    accept={acceptedFileTypes
                      .map((type) => `.${type}`)
                      .join(",")}
                    name="image"
                    onChange={handleFileChange}
                    // value={formValues.image}
                  />
                  <div className="float-left w-100 mt-3">
                  <>
                    {formErrors.image && (
                      <div className="invalid-tooltip">{formErrors.image}</div>
                    )}
                  </>
                 
                  {imageData ? <img src={imageData} /> :
                  <>
                   {formValues.image ? <img src={formValues.image} /> : ""}
                  </>}
                  </div>

                  {/* {formValues?.image ? (
                    <img className="mt-3" src={formValues?.image} />
                  ) : (
                    ""
                  )} */}
                </div>
              </div>
              <div className="pull-right mt-3">
                <NavLink
                  className="btn btn-secondary pull-right ms-3"
                  type="button"
                  to={`/admin/sub-Vendor/${params.id}`}
                >
                  Cancel
                </NavLink>
                {params.addProduct ? (
                  <button
                    className="btn btn-primary pull-right"
                    type="button"
                    onClick={handleSubmit}
                  >
                    Update Item
                  </button>
                ) : (
                  <button
                    className="btn btn-primary pull-right"
                    type="button"
                    onClick={handleSubmit}
                  >
                    Add Item
                  </button>
                )}

                {/* <button
                className="btn btn-primary pull-right ms-2"
                type="button"
                disabled
              >
                Add Catalog Item
              </button> */}
              </div>
            </div>
          </div>
        </div>
      </main>
    </div>
  );
};

export default AddAdminSubVendor;

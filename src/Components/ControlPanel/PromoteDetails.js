import React, { useState, useEffect } from "react";
import { NavLink, useParams } from "react-router-dom";
import user_service from "../service/user_service";
import Loader from "../../Pages/Loader/Loader.js";
import Toaster from "../../Pages/Toaster/Toaster.js";
import jwt from "jwt-decode";
import ImageSlider from "react-image-gallery";
import "react-image-gallery/styles/css/image-gallery.css";
import Picture from "../img/pic.png";
import PromoteImage from "../img/image.jpg";
import { Rating } from "react-simple-star-rating";
import {
  FaFacebook,
  FaTwitter,
  FaLinkedin,
  FaInstagram,
  FaYoutube,
  FaGoogle,
} from "react-icons/fa";

// import PromoteImage from "../img/image.jpg";

const PromoteDetails = () => {
  const [loader, setLoader] = useState({ isActive: null });
  const { isActive } = loader;

  const [toaster, setToaster] = useState({
    types: null,
    isShow: null,
    toasterBody: "",
    message: "",
  });
  const { types, isShow, toasterBody, message } = toaster;

  const params = useParams();

  const [productItem, setProductItem] = useState("");
  const [productItemImage, setProductItemImage] = useState([]);

  const [selectedImage, setSelectedImage] = useState(null);
  const [review, setReview] = useState("");

  const getProductItem = async () => {
    try {
      setLoader({ isActive: true });
      const response = await user_service.productItemsGetId(
        params.addProduct || params.details
      );
      if (response) {
        const ProductItems = response.data;
        setProductItem(ProductItems);

        const ImageData = ProductItems.image.map((item) => ({
          original: item.images,
          // thumbnail: item.images,
          // description: item.description,
        }));
        setProductItemImage(ImageData);

        PowerdbyAdmin(response.data.agentId)
      }
    } catch (error) {
      console.error("Error fetching product item:", error);
    } finally {
      setLoader({ isActive: false });
    }
  };

  const renderIcon = (type) => {
    switch (type) {
      case "Facebook":
        return <FaFacebook />;
      case "Twitter":
        return <FaTwitter />;
      case "Linkedin":
        return <FaLinkedin />;
      case "Instagram":
        return <FaInstagram />;
      case "Google":
        return <FaGoogle />;
      case "YouTube Channel":
        return <FaYoutube />;
      default:
        return null;
    }
  };

  useEffect(() => {
    window.scrollTo(0, 0);
    getProductItem();
    profileGetAll();
    PowerdbyAdmin()
  }, [params.details]);

  const [profile, setProfile] = useState("");
  const [profileAdmin, setProfileAdmin] = useState("");

  const profileGetAll = async () => {
    if (localStorage.getItem("auth")) {
      await user_service
        .profileGet(jwt(localStorage.getItem("auth")).id)
        .then((response) => {
          if (response) {
            // console.log(response);
            setProfile(response.data);
          }
        });
    }
  };

  const PowerdbyAdmin = async (agentId) => {
    if (agentId) {
      await user_service
        .contactGetById(agentId)
        .then((response) => {
          if (response) {
            // console.log(response);
            setProfileAdmin(response.data);
          }
        });
    }
  };

  const profilepic = (e) => {
    e.target.src = Picture;
  };

  const [rating, setRating] = useState(1);

  const handleRating = (newRating) => {
    setRating(newRating);
  };

  const onPointerEnter = () => {
    // Handle pointer enter event here
  };

  const onPointerLeave = () => {
    // Handle pointer leave event here
  };

  const onPointerMove = () => {
    // Handle pointer move event here
  };

  // const handleaddrating = async (id, startype, star) => {
  //   const updatedstar = parseInt(star) + 1;
  //   const userData = {
  //     [`${startype}`]: updatedstar,
  //   };
  //   await user_service.productItemsUpdate(id, userData).then((response) => {
  //     if (response) {
  //       console.log(response);

  //       const userDatarating = {
  //         agentId: jwt(localStorage.getItem("auth")).id,
  //         agentimage: profile?.image ?? "",
  //         agentname: profile?.firstName + " " + profile?.lastName,
  //         productitem_id: id,
  //         category: "rating",
  //         review:"" ,
  //       };
  //       user_service.productitemreview(userDatarating).then((response) => {
  //         if (response) {
  //           //setFormValues(response.data);
  //           setToaster({
  //             type: "Rating Added Successfully",
  //             isShow: true,
  //             toasterBody: response.data.message,
  //             message: "Rating Added Successfully",
  //           });
  //           setTimeout(() => {
  //             setToaster((prevToaster) => ({ ...prevToaster, isShow: false }));
  //             getProductItem();
  //           }, 1000);
  //         }
  //       });
  //     } else {
  //       setLoader({ isActive: false });
  //       setToaster({
  //         types: "error",
  //         isShow: true,
  //         toasterBody: response.data.message,
  //         message: "Error",
  //       });
  //     }
  //   });
  // };

  const submitreview = async (id) => {
    const userData = {
      agentId: jwt(localStorage.getItem("auth")).id,
      agentimage: profile?.image ?? "",
      agentname: profile?.firstName + " " + profile?.lastName,
      productitem_id: id,
      category: "review",
      review: review,
      starRating: rating,
    };

    try {
      console.log(userData);
      const response = await user_service.productitemreview(userData);

      if (response) {
        setReview("");
        setToaster({
          type: "success",
          isShow: true,
          toasterBody: response.data.message,
          message: "Review Added Successfully",
        });

        setTimeout(() => {
          setToaster((prevToaster) => ({ ...prevToaster, isShow: false }));
          getProductItem(); // Refresh the product item after successful review
        }, 1000);
      }
    } catch (error) {
      setLoader({ isActive: false });
      setToaster({
        type: "error",
        isShow: true,
        // toasterBody: error.response?.data?.message || "Something went wrong. Please try again later.",
        message: "review is not allowed to be empty",
      });

      setTimeout(() => {
        setToaster((prevToaster) => ({ ...prevToaster, isShow: false }));
      }, 2000);
    }
  };

  const formatDate = (dateString) => {
    const date = new Date(dateString);
    const day = String(date.getDate()).padStart(2, "0");
    const month = String(date.getMonth() + 1).padStart(2, "0"); // Months are zero-based
    const year = date.getFullYear();

    return `${month}/${day}/${year}`;
  };

  return (
    <div className="bg-secondary float-left w-100 pt-4 mb-4">
      <Loader isActive={isActive} />
      {isShow && (
        <Toaster
          types={types}
          isShow={isShow}
          toasterBody={toasterBody}
          message={message}
        />
      )}
      <main className="page-wrapper profile_page_wrap">
        <div className="content-overlay">
          <div className=""></div>
          {/* <hr className="mb-3"/> */}
          <div className="border bg-light rounded-3 w-100 p-4 d-inline-block">
            <div className="catelog_details">
              {/* <div className="product_details p-4">
                  <h6>{productItem?.description}</h6>
                </div> */}

              <div className="row">
                <div className="col-md-3">
                  {productItemImage.length > 0 ? (
                    <ImageSlider
                      className="img-fluid"
                      items={productItemImage}
                      showPlayButton={false}
                      showFullscreenButton={false}
                      useBrowserFullscreen={false}
                      height="300px"
                      width="400px"
                    />
                  ) : (
                    <img
                      className="img-fluid"
                      height="300px"
                      width="400px"
                      src={PromoteImage}
                      alt="No images available"
                    />
                  )}
                </div>
                <div className="col-md-9 mt-lg-0 mt-md-0 mt-sm-4 mt-4">
                  <div className="d-flex align-items-center justify-content-between">
                    <div className="">
                      <h4 className="text-dark pull-left mb-0">
                        {productItem?.productname}
                      </h4>
                      <br></br>
                      <p className="text-grey pull-left mb-0">
                        <small>{productItem?.category}</small>
                      </p>
                    </div>
                    <div className="social-links pull-right">
                      {productItem.social_links &&
                      productItem.social_links.length > 0 ? (
                        <div className="d-flex">
                          {productItem.social_links.map((link) =>
                            link.linkurl ? (
                              <p className="ms-3 mb-0" key={link._id}>
                                <a
                                  className="d-flex"
                                  href={link.linkurl}
                                  target="_blank"
                                  rel="noopener noreferrer"
                                >
                                  {renderIcon(link.type)}
                                  {/* <span className="ml-2">{link.type}</span> */}
                                </a>
                              </p>
                            ) : null
                          )}
                        </div>
                      ) : (
                        <p className="mb-0">No social links available</p>
                      )}
                    </div>
                  </div>
                  <hr className="my-3"></hr>

                  <div className="row">
                    <div className="col-md-4">
                      {productItem.contactName ? (
                        <>
                          <b>Contact</b>
                          <p>{productItem.contactName}</p>
                        </>
                      ) : (
                        ""
                      )}
                      {productItem.email ? (
                        <>
                          <b>Email</b>
                          <p>{productItem.email}</p>
                        </>
                      ) : (
                        ""
                      )}
                      {productItem.phone ? (
                        <>
                          <b>Phone</b>
                          <p>{productItem.phone}</p>
                        </>
                      ) : (
                        ""
                      )}
                      <div className="col-md-12">
                        <b>Creted</b>
                        <p>{formatDate(productItem.created)}</p>
                      </div>
                    </div>
                    <div className="col-md-6">
                      {productItem.zipcode && productItem.zipcode.length > 0 ? (
                        <div className="areas-served">
                          <div>
                            <b>Areas Served</b>
                          </div>
                          <p className="mt-2">
                            {productItem.zipcode.map((area, index) => (
                              <span key={index}>{area}</span>
                            ))}
                          </p>
                        </div>
                      ) : (
                        <p>No areas available</p>
                      )}
                      {productItem.address ? (
                        <>
                          <b>Address</b>
                          <p>{productItem.address}</p>
                        </>
                      ) : (
                        ""
                      )}
                      <div className="">
                        {/* {productItem?.product_quickDetails?.length > 0
                          ? productItem.product_quickDetails
                              .filter(
                                (item) =>
                                  item &&
                                  item.quickDetails &&
                                  item.quickDetails.length > 0
                              )
                              .map((item) => (
                                <ul key={item.id}>
                                  <li>{item.quickDetails}</li>
                                </ul>
                              ))
                          : null} */}

                        {productItem?.linkurl && (
                          <a
                            className="btn btn-primary pull-left"
                            href={productItem.linkurl}
                            target="_blank"
                            rel="noreferrer"
                          >
                            {/* {productItem.linktext} */}
                            Visit Website Now
                          </a>
                        )}
                      </div>
                    </div>
                  </div>

                  <div className="col-md-12">
                    <p>{productItem?.description}</p>
                  </div>

                  {/* <li className="mt-4" dangerouslySetInnerHTML={{__html: productItem.product_fullDetails,}}></li> */}
                </div>
              </div>
            </div>
            <div className="row">
              <div className="col-md-9 border-right-con border-top">
                <div className="col-md-12 mt-3 w-100 d-flex align-items-center justify-content-between">
                  <h6 className="mb-0 me-2">Rate To Our Product :</h6>
                  <Rating
                    onClick={handleRating}
                    onPointerEnter={onPointerEnter}
                    onPointerLeave={onPointerLeave}
                    onPointerMove={onPointerMove}
                  />
                </div>
                {/* <h6>Leave a review for others :</h6> */}
                <div className="col-sm-12 mb-3 leave_a_review mt-2">
                  {/* <img
                  className="rounded-circle profile_thumbnail me-3"
                  src={profile.image ?? Picture}
                  alt="profile picture"
                  onError={(e) => profilepic(e)}
                /> */}
                  <textarea
                    placeholder="Type a your review here"
                    rows="3"
                    className="form-control p-0 shadow-none"
                    type="text"
                    id="textarea-input"
                    name="review"
                    onChange={(event) => setReview(event.target.value)}
                  ></textarea>
                  <div className="float-left w-100 mt-3">
                    <button
                      className="btn btn-primary btn-sm pull-right"
                      type="button"
                      onClick={() => submitreview(productItem?._id)}
                    >
                      Post
                    </button>
                  </div>
                </div>
                <div className="col-sm-12 mt-2 float-start w-100">
                  {/* {productItem?.reviews.length>0 ? <h6>Reviews:</h6> : ""} */}
                  {productItem?.reviews?.length > 0 ? <h6>Reviews:</h6> : ""}
                  <div className="float-left w-100">
                    {productItem?.reviews
                      ? productItem?.reviews.map((item, index) =>
                          item.category === "review" ? (
                            <div
                              key={index}
                              className="notes-main d-flex align-items-center justify-content-between my-0 py-3"
                            >
                              <div className="d-flex align-items-center justify-content-between">
                                <img
                                  className="rounded-circle image-note"
                                  src={item.agentimage ?? Picture}
                                  alt="profile picture"
                                  onError={(e) => profilepic(e)}
                                />
                                <div className="ms-3 pe-3">
                                  <strong>{item?.agentname ?? ""}</strong>
                                  <p className="mb-0">{item.review}</p>
                                </div>
                              </div>

                              <p class="mb-1">
                                <div className="star-rating">
                                  {Array.from(
                                    { length: parseInt(item.starRating) },
                                    (_, i) => (
                                      <i
                                        key={i}
                                        style={{
                                          color: "#FFBC0B", // Gold color for stars
                                          fontSize: "20px",
                                          margin: "2px",
                                        }}
                                        className="fa fa-star"
                                        aria-hidden="true"
                                      ></i>
                                    )
                                  )}
                                </div>
                                <br></br>
                                <div className="">
                                  <p className="mb-0">
                                    <small>{formatDate(item.created)}</small>
                                  </p>
                                </div>
                              </p>
                            </div>
                          ) : null
                        )
                      : ""}
                  </div>
                </div>
              </div>
              <div className="col-md-3 ps-0">
                {/* <div class="notes-main py-3 mt-0 ps-3">
                  <h6 className="mb-0">Provided by</h6>
                </div>
                <div class="notes-main d-flex align-items-center justify-content-between my-0 border-0 ps-3">
                  {console.log(profileAdmin)}
                  <div class="d-flex align-items-center justify-content-between">
                    <img
                      class="rounded-circle image-note"
                      src={profileAdmin.image}
                      alt="profile picture"
                    />
                    <div class="ms-3">
                      <strong>{profileAdmin.firstName} {profileAdmin.lastName}</strong>
                    </div>
                  </div>
                </div> */}
              </div>
            </div>
          </div>
          <div className="pull-right mt-3">
            {localStorage.getItem("auth") &&
              jwt(localStorage.getItem("auth")).contactType === "admin" && (
                <>
                  {params.details ? (
                    <NavLink
                      type="button"
                      className="btn btn-primary pull-right ms-3"
                      to={`/update-markting/${params.id}/${params.details}/${productItem?._id}`}
                    >
                      <i className="fi-edit me-2"></i>
                      Edit
                    </NavLink>
                  ) : (
                    <NavLink
                      type="button"
                      className="btn btn-primary pull-right ms-3"
                      to={`/add-markting/${params.id}/${productItem?._id}`}
                    >
                      <i className="fi-edit me-2"></i>
                      Edit
                    </NavLink>
                  )}
                </>
              )}
            <NavLink
              className="btn btn-secondary pull-right"
              type="button"
              to={`/promote/catalog-product/${params.id}/${params.details}`}
            >
              Back
            </NavLink>
          </div>
        </div>
      </main>
    </div>
  );
};

export default PromoteDetails;

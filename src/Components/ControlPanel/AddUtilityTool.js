import React, { useState, useEffect } from "react";
// import avtar from "./Components/img/avtar.jpg";
import _ from "lodash";
import user_service from "../service/user_service.js";
import jwt from "jwt-decode";
import Loader from "../../Pages/Loader/Loader.js";
import Toaster from "../../Pages/Toaster/Toaster.js";
import MultiValueTextInput from "react-multivalue-text-input";
import ReactPaginate from "react-paginate";
import axios from "axios";

function AddUtilityTool() {
  const initialValues = {
    utilityLogo: "",
    contact: "",
    provider: "",
    providerLink: "",
    zipcode: [],
    type: "",
  };
  const [formValues, setFormValues] = useState(initialValues);
  const [formErrors, setFormErrors] = useState({});
  const [isSubmitClick, setISSubmitClick] = useState(false);
  const [loader, setLoader] = useState({ isActive: null });
  const { isActive } = loader;
  const [toaster, setToaster] = useState({
    types: null,
    isShow: null,
    toasterBody: "",
    message: "",
  });
  const { types, isShow, toasterBody, message } = toaster;
  const [file, setFile] = useState(null);
  const [data, setData] = useState("");
  const [fileExtension, setFileExtension] = useState("");
  const [acceptedFileTypes, setAcceptedFileTypes] = useState([]);
  const [showSubmitButton, setShowSubmitButton] = useState(false);

  const [formData, setFormData] = useState([]);
  const [pageCount, setPageCount] = useState(0);

  // onChange Function start

  const handleChange = (e) => {
    const { name, value } = e.target;
    setFormValues({ ...formValues, [name]: value });
  };

  const handleZipcodeChange = (values) => {
    setFormValues({ ...formValues, zipcode: values });
  };
  // onChange Function end

  {
    /* <!-- Form Validation Start--> */
  }
  useEffect(() => {
    if (formValues && isSubmitClick) {
      validate();
    }
  }, [formValues]);

  const validate = () => {
    const values = formValues;
    const errors = {};
    if (!values.provider) {
      errors.provider = "Provider is required";
    }

    if (!values.providerLink) {
      errors.providerLink = "Provider Link is required";
    }

    if (!values.type) {
      errors.type = "Type is required";
    }

    if (values.zipcode.length === 0) {
      errors.zipcode = "Zip-code is required";
    }
    // if (!values.zipcode) {
    //   errors.zipcode = "Zip-code is required";
    // }

    if (!values.contact) {
      errors.contact = "Contact is required";
    }
    if (!values.utilityLogo && !data) {
      errors.utilityLogo = "Utility Logo is required";
    }

    setFormErrors(errors);
    return errors;
  };
  {
    /* <!-- Form Validation End--> */
  }

  const handleFileUpload = async (e) => {
    const selectedFile = e.target.files[0];

    if (selectedFile) {
      setFile(selectedFile);
      setFileExtension(selectedFile.name.split(".").pop());
      setShowSubmitButton(true);

      const formData = new FormData();
      formData.append("file", selectedFile);
      console.log(selectedFile);
      const config = {
        headers: {
          Accept: "application/json",
          "Content-Type": "multipart/form-data",
          Authorization: `Bearer ${localStorage.getItem("auth")}`,
        },
      };
      try {
        setLoader({ isActive: true });
        const uploadResponse = await axios.post(
          "https://api.brokeragentbase.com/upload",
          formData,
          config
        );
        const uploadedFileData = uploadResponse.data;
        setLoader({ isActive: false });
        setData(uploadedFileData);
      } catch (error) {
        console.error("Error occurred during file upload:", error);
      }
    }
  };

  {
    /* <!--Api call Form onSubmit Start--> */
  }

  const handleSubmit = async (e) => {
    if (formValues._id) {
      e.preventDefault();
      const userData = {
        agentId: jwt(localStorage.getItem("auth")).id,
        utilityLogo: data,
        provider: formValues.provider,
        providerLink: formValues.providerLink,
        type: formValues.type,
        zipcode: formValues.zipcode,
        contact: formValues.contact,
      };
      console.log(userData);
      try {
        setLoader({ isActive: true });
        await user_service
          .UtiltityUpdate(formValues._id, userData)
          .then((response) => {
            if (response) {
              setData("");
              setFormValues("");
              setLoader({ isActive: false });
              setToaster({
                type: "Add Utility",
                isShow: true,
                toasterBody: response.data.message,
                message: "Utility Update Successfully",
              });
              setTimeout(() => {
                setToaster((prevToaster) => ({
                  ...prevToaster,
                  isShow: false,
                }));
              }, 1000);
              UtilityGetData();
            } else {
              setLoader({ isActive: false });
              setToaster({
                types: "error",
                isShow: true,
                toasterBody: response.data.message,
                message: "Error",
              });
            }
          });
      } catch (error) {
        setLoader({ isActive: false });
        setToaster({
          types: "error",
          isShow: true,
          toasterBody: error.response
            ? error.response.data.message
            : error.message,
          message: "Error",
        });
        setTimeout(() => {
          setToaster((prevToaster) => ({ ...prevToaster, isShow: false }));
        }, 2000);
      }
    } else {
      e.preventDefault();
      setISSubmitClick(true);
      let checkValue = validate();
      if (_.isEmpty(checkValue)) {
        const userData = {
          agentId: jwt(localStorage.getItem("auth")).id,
          utilityLogo: data,
          provider: formValues.provider,
          providerLink: formValues.providerLink,
          type: formValues.type,
          zipcode: formValues.zipcode,
          contact: formValues.contact,
        };
        try {
          setLoader({ isActive: true });
          await user_service.UtiltityPost(userData).then((response) => {
            if (response) {
              setData("");
              setFormValues("");
              setLoader({ isActive: false });
              setToaster({
                type: "Add Utility",
                isShow: true,
                toasterBody: response.data.message,
                message: "Add Utility Successfully",
              });
              setTimeout(() => {
                setToaster((prevToaster) => ({
                  ...prevToaster,
                  isShow: false,
                }));
                // navigate(`/question`);
              }, 1000);
              UtilityGetData();
            } else {
              setLoader({ isActive: false });
              setToaster({
                types: "error",
                isShow: true,
                toasterBody: response.data.message,
                message: "Error",
              });
            }
          });
        } catch (error) {
          setLoader({ isActive: false });
          setToaster({
            types: "error",
            isShow: true,
            toasterBody: error.response
              ? error.response.data.message
              : error.message,
            message: "Error",
          });
          setTimeout(() => {
            setToaster((prevToaster) => ({ ...prevToaster, isShow: false }));
          }, 2000);
        }
      }
    }
  };

  const UtilityGetData = async () => {
    await user_service.UtiltityGet(1).then((response) => {
      if (response) {
        setFormData(response.data.data);
        setPageCount(Math.ceil(response.data.totalRecords / 10));
      }
    });
  };

  useEffect(() => {
    if (formData) {
      UtilityGetData();
    }
  }, []);

  const EditUtility = async (id) => {
    if (id) {
      await user_service.UtiltityGetByID(id).then((response) => {
        setLoader({ isActive: false });
        if (response) {
          setFormValues(response.data);
        }
      });
    }
  };

  const handleDelete = async (id) => {
    setLoader({ isActive: true });
    await user_service.UtiltityDelete(id).then((response) => {
      if (response) {
        setLoader({ isActive: false });
        setToaster({
          types: "Delete",
          isShow: true,
          toasterBody: response.data.message,
        });

        setTimeout(() => {
          setToaster((prevToaster) => ({
            ...prevToaster,
            isShow: false,
          }));
        }, 2000);
        UtilityGetData();
      }
    });
  };

  const handlePageClick = async (data) => {
    try {
      const currentPage = data.selected + 1;
      setLoader({ isActive: true });
      const response = await user_service.UtiltityGet(currentPage);
      setLoader({ isActive: false });
      if (response && response.data) {
        setFormData(response.data.data);
        setPageCount(Math.ceil(response.data.totalRecords / 10));
      }
    } catch (error) {
      console.error("Error in handlePageClick:", error);
      // Handle errors as needed
    }
  };

  return (
    <>
      <div className="bg-secondary float-left w-100 pt-4 mb-4">
        <Loader isActive={isActive} />
        {isShow && (
          <Toaster
            types={types}
            isShow={isShow}
            toasterBody={toasterBody}
            message={message}
          />
        )}
        <main className="page-wrapper">
          <div className="d-flex align-items-center justify-content-start mb-4">
            <h3 className="text-white mb-0">Add Utility Tool</h3>
          </div>
          <div className="row">
            <div className="col-md-12">
              <div className="bg-light border rounded-3 p-3">
                <div className="col-sm-12 mb-3">
                  {formValues.utilityLogo ? (
                    <>
                      <label className="form-label">Update Utility Logo</label>
                      <input
                        className="form-control"
                        type="file"
                        accept={acceptedFileTypes
                          .map((type) => `.${type}`)
                          .join(",")}
                        name="utilityLogo"
                        value=""
                        onChange={handleFileUpload}
                      />
                    </>
                  ) : (
                    <>
                      <label className="form-label">Add Utility Logo</label>
                      <input
                        className="form-control"
                        id="inline-form-input"
                        type="file"
                        accept={acceptedFileTypes
                          .map((type) => `.${type}`)
                          .join(",")}
                        name="utilityLogo"
                        onChange={handleFileUpload}
                      />

                      {formErrors.utilityLogo && !data && (
                        <div className="invalid-tooltip">
                          {formErrors.utilityLogo}
                        </div>
                      )}
                    </>
                  )}

                  {data ? (
                    <img className="mt-3" src={data} alt="Utility Logo" />
                  ) : (
                    formValues.utilityLogo && (
                      <img
                        className="mt-3"
                        src={formValues.utilityLogo}
                        alt="Utility Logo"
                      />
                    )
                  )}
                </div>

                <div className="col-sm-12 mb-3">
                  <label className="form-label">Provider</label>
                  <input
                    className="form-control"
                    id="inline-form-input"
                    type="text"
                    name="provider"
                    value={formValues.provider ?? ""}
                    placeholder="Add Provider"
                    onChange={handleChange}
                    style={{
                      border: formErrors?.provider ? "1px solid red" : "",
                    }}
                  />
                  {formErrors.provider && (
                    <div className="invalid-tooltip">{formErrors.provider}</div>
                  )}
                </div>

                <div className="col-sm-12 mb-3">
                  <label className="form-label">Provider Link</label>
                  <input
                    className="form-control"
                    id="inline-form-input"
                    type="text"
                    name="providerLink"
                    value={formValues.providerLink ?? ""}
                    placeholder="Add Provider Link"
                    onChange={handleChange}
                    style={{
                      border: formErrors?.providerLink ? "1px solid red" : "",
                    }}
                  />
                  {formErrors.providerLink && (
                    <div className="invalid-tooltip">
                      {formErrors.providerLink}
                    </div>
                  )}
                </div>
                <div className="col-sm-12 mb-3">
                  <label className="form-label">Type</label>
                  <input
                    className="form-control"
                    id="inline-form-input"
                    type="text"
                    name="type"
                    value={formValues.type ?? ""}
                    placeholder="Add Type"
                    onChange={handleChange}
                    style={{
                      border: formErrors?.type ? "1px solid red" : "",
                    }}
                  />
                  {formErrors.type && (
                    <div className="invalid-tooltip">{formErrors.type}</div>
                  )}
                </div>

                <div className="col-sm-12 mb-3">
                  <label className="form-label">Contact</label>
                  <input
                    className="form-control"
                    id="inline-form-input"
                    type="text"
                    name="contact"
                    placeholder="Add Contact"
                    value={formValues.contact ?? ""}
                    onChange={handleChange}
                    style={{
                      border: formErrors?.contact ? "1px solid red" : "",
                    }}
                  />
                  {formErrors.contact && (
                    <div className="invalid-tooltip">{formErrors.contact}</div>
                  )}
                </div>

                <div className="col-sm-12 mb-3">
                  <label className="form-label">Zip-code</label>
                  <MultiValueTextInput
                    values={formValues.zipcode}
                    onItemAdded={(item, allItems) =>
                      handleZipcodeChange(allItems)
                    }
                    onItemDeleted={(item, allItems) =>
                      handleZipcodeChange(allItems)
                    }
                    className="form-control"
                    id="inline-form-input"
                    placeholder="Enter zip codes separated by commas"
                    style={{
                      border: formErrors?.zipcode ? "1px solid red" : "",
                    }}
                  />
                  {formErrors.zipcode && (
                    <div className="invalid-tooltip">{formErrors.zipcode}</div>
                  )}
                </div>
                <div className="pull-right mt-4">
                  <button
                    className="btn btn-primary"
                    type="button"
                    onClick={handleSubmit}
                  >
                    Submit
                  </button>
                </div>
              </div>
            </div>
          </div>
          <div className="row mt-4">
            <div className="col-md-12 bg-light border rounded-3 p-3 mt">
              <table
                id="DepositLedger"
                className="table table-striped mb-0"
                width="100%"
                border="0"
                cellSpacing="0"
                cellPadding="0"
              >
                <tbody>
                  <table
                    id="DepositLedger"
                    className="table table-striped mb-0"
                    width="100%"
                    border="0"
                    cellSpacing="0"
                    cellPadding="0"
                  >
                    <thead>
                      <tr>
                        <th>NEARBY UTILITIES</th>
                        <th>PROVIDER</th>
                        <th>TYPE</th>
                        <th>CONTACT</th>
                        <th>EDIT</th>
                        <th>DELETE</th>
                      </tr>
                    </thead>
                    <tbody>
                      {Array.isArray(formData) && formData.length > 0 ? (
                        formData.map((item) => (
                          <tr key={item.id}>
                            <td>
                              {item.utilityLogo ? (
                                <img
                                  style={{ width: "100px" }}
                                  src={item.utilityLogo}
                                />
                              ) : (
                                ""
                              )}
                            </td>
                            <td>
                              <a
                                href={item.providerLink}
                                target="_blank"
                                rel="noopener noreferrer"
                              >
                                {item.provider}
                              </a>
                            </td>
                            <td>{item.type}</td>
                            <td>{item.contact}</td>

                            <td>
                              <button
                                className="btn btn-primary btn-sm"
                                onClick={() => EditUtility(item._id)}
                              >
                                Edit
                              </button>
                            </td>

                            <td>
                              <button
                                className="btn btn-secondary btn-sm"
                                onClick={() => handleDelete(item._id)}
                              >
                                Delete
                              </button>
                            </td>
                          </tr>
                        ))
                      ) : (
                        <tr>
                          <td>No data to display.</td>
                        </tr>
                      )}
                    </tbody>
                  </table>
                </tbody>
              </table>
            </div>
          </div>
          <div className="justify-content-end mb-1">
            <ReactPaginate
              previousLabel={"Previous"}
              nextLabel={"Next"}
              breakLabel={"..."}
              pageCount={pageCount}
              marginPagesDisplayed={1}
              pageRangeDisplayed={2}
              onPageChange={handlePageClick}
              containerClassName={"pagination justify-content-center"}
              pageClassName={"page-item"}
              pageLinkClassName={"page-link"}
              previousClassName={"page-item"}
              previousLinkClassName={"page-link"}
              nextClassName={"page-item"}
              nextLinkClassName={"page-link"}
              breakClassName={"page-item"}
              breakLinkClassName={"page-link"}
              activeClassName={"active"}
            />
          </div>
        </main>
      </div>
    </>
  );
}
export default AddUtilityTool;

import React, { useState, useEffect } from "react";
import user_service from "../service/user_service";
import Loader from "../../Pages/Loader/Loader.js";
import Toaster from "../../Pages/Toaster/Toaster";
import { NavLink, useNavigate } from "react-router-dom";

const ReportBug = () => {
  const [loader, setLoader] = useState({ isActive: null });
  const { isActive } = loader;
  const [toaster, setToaster] = useState({
    types: null,
    isShow: null,
    toasterBody: "",
    message: "",
  });
  const { types, isShow, toasterBody, message } = toaster;

  const [getSuggest, setGetSuggest] = useState([]);

  const navigate = useNavigate();

  useEffect(() => {
    window.scrollTo(0, 0);
    setLoader({ isActive: true });
   
    SuggestFeatureGetAll();
  }, []);

  const SuggestFeatureGetAll = async () => {
    await user_service.reportBugAgentGet(1).then((response) => {
      setLoader({ isActive: false });
      if (response) {
        setGetSuggest(response.data.data);
      }
    });
  };

  const handleUpdate = async (id, status) => {
    const newStatus = status == "1" ? "0" : status == "0" ? "1" : "0";

    const userData = {
      is_active: newStatus,
      suggestion: id
    };
  
    console.log(userData);
    try {
      setLoader({ isActive: true });
      const response = await user_service.SuggestFeatureUpdate(id, userData);

      if (response) {
        console.log(response.data);
        setLoader({ isActive: false });

        setToaster({
          type: "Suggestion Updated Successfully",
          isShow: true,
          toasterBody: response.data.message,
          message: "Suggestion Updated Successfully",
        });

        SuggestFeatureGetAll();
        setTimeout(() => {
          setToaster((prevToaster) => ({ ...prevToaster, isShow: false }));
        }, 2000);
      }
    } catch (err) {
      setToaster({
        type: "API Error",
        isShow: true,
        toasterBody: err.response.data.message,
        message: "API Error",
      });

      setTimeout(() => {
        setToaster((prevToaster) => ({ ...prevToaster, isShow: false }));
      }, 2000);
    }
  };
  return (
    <div className="bg-secondary float-left w-100 pt-4">
      <Loader isActive={isActive} />
      {isShow && (
        <Toaster
          types={types}
          isShow={isShow}
          toasterBody={toasterBody}
          message={message}
        />
      )}
      <main className="page-wrapper">
        {/* <!-- Page content--> */}
        {/* <!-- Page container--> */}
        <div className="">
          {/* <!-- Page card like wrapper--> */}

          <div className="">
            {/* <!-- Content--> */}
            <div className="">
              <div className="d-flex align-items-center justify-content-start mb-4">
                <h3 className="text-white mb-0">Report Bug Listings</h3>
              </div>
          
              {/* <!-- Item--> */}
              {getSuggest && getSuggest.length > 0 ? (
                getSuggest.map((post) => (
              <div className="card card-horizontal border mb-3 p-3"
                >
                  <div className="card-body position-relative p-0">
                    <p className="mb-2 fs-sm">
                      <strong>Email:</strong> {post.email}
                    </p>
                    <p className="mb-2 fs-sm">
                      <strong>Title:</strong> {post.title}
                    </p>
                    <p className="mb-2 fs-sm">
                      <strong>Description:</strong> {post.description}
                    </p>
                   
                     {
                       post.uploadFile ?
                      <img width="100px"
                      height="100px"
                       src={ post.uploadFile}/>
                      :""
                     }
                 
                  </div>
                </div>
                  ))
                  ) : (
                    <p className="text-center mt-5">
                      <i
                        className="fa fa-calculator mb-5"
                        aria-hidden="true"
                        style={{ fontSize: "135px" }}
                      ></i>
                      <br />
                      <span className="text-white">No Data found</span>
                    </p>
                  )}
            </div>
          </div>
        </div>
      </main>
    </div>
  );
};

export default ReportBug;

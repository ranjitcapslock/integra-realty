import React, { useState, useEffect, useRef } from "react";
import Pic from "../img/pic.png";
import Modal from "../../Modal";
import user_service from "../service/user_service";
import ReactPaginate from "react-paginate";
import Loader from "../../Pages/Loader/Loader.js";
import Toaster from "../../Pages/Toaster/Toaster.js";
import { NavLink, useNavigate, useParams } from "react-router-dom";
import jwt from "jwt-decode";
import $, { each } from "jquery";
import { Editor } from "react-draft-wysiwyg";
import { EditorState, ContentState, convertToRaw } from "draft-js";
import draftToHtml from "draftjs-to-html";
import "react-draft-wysiwyg/dist/react-draft-wysiwyg.css";
import axios from "axios";
import _ from "lodash";

function Noticemessage() {
  const [selectedContacts, setSelectedContacts] = useState({});

  const [getContact, setGetContact] = useState([]);
  const [pageCount, setPageCount] = useState(0);
  const [totalRecords, setTotalRecords] = useState("");
  const [loader, setLoader] = useState({ isActive: null });
  const { isActive } = loader;
  const [toaster, setToaster] = useState({
    types: null,
    isShow: null,
    toasterBody: "",
    message: "",
  });
  const { types, isShow, toasterBody, message } = toaster;

  const [isSubmitClick, setISSubmitClick] = useState(false);

  const [notesmessage, setNotesmessage] = useState("");
  const editorRef = useRef(null);
  const log = () => {
    if (editorRef.current) {
      setNotesmessage(editorRef.current.getContent());
    }
  };

  const params = useParams();

  const initialValues = {
    // title: "",
    message: "",
    category: "Office",
    // expiration_date: "",
  };

  const [formValues, setFormValues] = useState(initialValues);
  const [formData, setFormData] = useState({});
  const [detail, setDetail] = useState("");
  const [formErrors, setFormErrors] = useState({});
  const [useridd, setUseridd] = useState();

  const [contactType, setContactType] = useState("");
  const [contactStatus, setContactStatus] = useState("active");

  const [contactName, setContactName] = useState("");
  const [file, setFile] = useState(null);
  const [data, setData] = useState("");

  const SearchGetAll = async () => {
    try {
      const response = await user_service.SearchContactfilterAssociate(
        1,
        contactType,
        contactStatus,
        contactName
      );
      if (response) {
        setGetContact(response.data.data);
        // setPageCount(Math.ceil(response.data.totalRecords / 10));
        // setTotalRecords(response.data.totalRecords);
      }
    } catch (error) {
      console.error("Error fetching data:", error);
    }
  };

  useEffect(() => {
    window.scrollTo(0, 0);
    if (contactName) {
      SearchGetAll(1);
    }
  }, [contactType, contactName]);

  const [contactsAssociate, setContactsAssociate] = useState([]);
  const handleContactAssociate = (contactAssociate) => {
    setContactsAssociate((prevContacts) => [...prevContacts, contactAssociate]);
    setGetContact([]);
    setContactName("");
  };

  const [contactsGroups, setContactsGroups] = useState([]);

  const handleGroupData = (data) => {
    setContactsGroups((prevContacts) => [...prevContacts, data]);
    // setContactGroupMember([]);
    // setContactName({});
  };

  const [officeGroups, setOfficeGroups] = useState([]);

  const handleOffice = (item) => {
    setOfficeGroups((prevContacts) => [...prevContacts, item]);
  };

  const profileGetAll = async () => {
    setLoader({ isActive: true });
    await user_service
      .profileGet(jwt(localStorage.getItem("auth")).id)
      .then((response) => {
        if (response) {
          setLoader({ isActive: false });
          console.log(response);
          setFormData(response.data);
        }
      });
  };

  const PostGetById = async () => {
    await user_service.postDetail(params.id).then((response) => {
      setLoader({ isActive: false });
      if (response) {
        setDetail(response.data);
      }
    });
  };

  useEffect(() => {
    window.scrollTo(0, 0);
    profileGetAll();
    if (params.type == "post") {
      PostGetById(params.id);
    }
  }, []);

  const [editorState, setEditorState] = useState(EditorState.createEmpty());

  const handleChanges = (editorState) => {
    setEditorState(editorState);
    const htmlContent = draftToHtml(
      convertToRaw(editorState.getCurrentContent())
    );
    setFormValues({ ...formValues, message: htmlContent });
  };

  const handleChange = (e) => {
    const { name, value } = e.target;
    setFormValues({ ...formValues, [name]: value });
  };

  const navigate = useNavigate();

  const handleFileUpload = async (e) => {
    const selectedFile = e.target.files[0];

    if (selectedFile) {
      setFile(selectedFile);

      const formData = new FormData();
      formData.append("file", selectedFile);
      console.log(selectedFile);
      const config = {
        headers: {
          Accept: "application/json",
          "Content-Type": "multipart/form-data",
          Authorization: `Bearer ${localStorage.getItem("auth")}`,
        },
      };
      try {
        setLoader({ isActive: true });
        const uploadResponse = await axios.post(
          "https://api.brokeragentbase.com/upload",
          formData,
          config
        );
        const uploadedFileData = uploadResponse.data;
        setLoader({ isActive: false });
        setToaster({
          type: "Upload File",
          isShow: true,
          message: "File Upload Successfully",
        });
        setTimeout(() => {
          setToaster((prevToaster) => ({ ...prevToaster, isShow: false }));
          // navigate("/control-panel/documents/");
        }, 2000);
        setData(uploadedFileData);
      } catch (error) {
        console.error("Error occurred during file upload:", error);
      }
    }
  };

  // useEffect(() => {
  //   if (formValues && isSubmitClick) {
  //     validate();
  //   }
  // }, [formValues]);

  // const validate = () => {
  //   const values = formValues;
  //   const errors = {};
  //   if (!values.title) {
  //     errors.title = "Subject is required";
  //   }

  //   setFormErrors(errors);
  //   return errors;
  // };
  const handleSubmit = async (e) => {
    e.preventDefault();
    // setISSubmitClick(true);
    // let checkValue = validate();
    // if (_.isEmpty(checkValue)) {
    const userData = {
      agentId: jwt(localStorage.getItem("auth")).id,
      office_id: localStorage.getItem("active_office_id"),
      office_name: localStorage.getItem("active_office"),
      // title: formValues.title,
      message: formValues.message,
      category: formValues.category,
      // expiration_date: formValues.expiration_date,
      contactType: contactsAssociate,
      contactGroup: contactsGroups,
      officeGroup: officeGroups,
      image: data,
    };
    console.log(userData);
    setLoader({ isActive: true });
    await user_service.noticecontact(userData).then((response) => {
      if (response) {
        setContactsGroups([]);
        setContactsAssociate([]);
        setOfficeGroups([]);
        setOrganizationGet([]);
        setFormValues({});
        setLoader({ isActive: false });
        // setISSubmitClick(false);
        setToaster({
          type: "Notice",
          isShow: true,
          // toasterBody: response.data.message,
          message: "Notice send Successfully",
        });
        setTimeout(() => {
          setToaster((prevToaster) => ({ ...prevToaster, isShow: false }));
          navigate("/");
        }, 2000);
      } else {
        setLoader({ isActive: false });
        setToaster({
          types: "error",
          isShow: true,
          toasterBody: response.data.message,
          message: "Error",
        });
        setTimeout(() => {
          setToaster((prevToaster) => ({ ...prevToaster, isShow: false }));
          // navigate("/control-panel/documents/");
        }, 2000);
      }
    });
    // }
  };

  const [contactGroupMember, setContactGroupMember] = useState([]);

  const ContactGroupMemberGetAll = async () => {
    try {
      setLoader({ isActive: true });
      const response = await user_service.contactGroupMemberGetAll();

      setLoader({ isActive: false });

      if (response && response.data) {
        console.log(response.data.data);
        setContactGroupMember(response.data.data);
      }
    } catch (error) {
      console.error("Error fetching data:", error);
      setLoader({ isActive: false });
    }
  };

  const [organizationGet, setOrganizationGet] = useState([]);

  const OrganizationTreeGets = async () => {
    // setLoader({ isActive: true });
    if (jwt(localStorage.getItem("auth")).id) {
      const agentId = jwt(localStorage.getItem("auth")).id;
      await user_service.organizationTreeGet().then((response) => {
        // setLoader({ isActive: false });
        if (response) {
          setOrganizationGet(response.data);
        }
      });
    }
  };

  const handleDelete = (id, memberIndex) => {
    const updatedMembers = contactsAssociate.filter(
      (_, index) => index !== memberIndex
    );
    setContactsAssociate(updatedMembers);
  };

  const handleDeleteGroup = (id, memberIndex) => {
    const updatedMembers = contactsGroups.filter(
      (_, index) => index !== memberIndex
    );
    setContactsGroups(updatedMembers);
  };

  const handleDeleteOffice = (id, officeIndex) => {
    const updatedOffice = officeGroups.filter(
      (_, index) => index !== officeIndex
    );
    setOfficeGroups(updatedOffice);
  };

  useEffect(() => {
    window.scrollTo(0, 0);
    setLoader({ isActive: true });
    ContactGroupMemberGetAll();
    OrganizationTreeGets();
  }, []);
  return (
    <div className="bg-secondary float-left w-100 pt-4 mb-4">
      <Loader isActive={isActive} />
      {isShow && (
        <Toaster
          types={types}
          isShow={isShow}
          toasterBody={toasterBody}
          message={message}
        />
      )}
      <main className="page-wrapper">
        <Modal />
        {/* <!-- Page container--> */}
        <div className="content-overlay">
          {/* <!-- Page card like wrapper--> */}
          <div className="">
            <div className="">
              <div className="col-md-12 d-flex align-items-center justify-content-between">
                <h3 className="mb-4 pull-left text-white">Send a Notice</h3>
              </div>
              {/* <!-- List of resumes--> */}
              <div className="col-md-12 bg-light rounded-3 p-3 border">
                <p className="w-100">
                  Send your notice message here. Use this space to craft your
                  message, add links, images, and personalize it before sending
                  it to your recipients. Remember to proofread and test before
                  hitting send!
                </p>
                <div className="">
                  <div className="row">
                    <div className="col-md-6">
                      <label className="col-form-label">To</label>
                      <select
                        className="form-select"
                        id="pr-city"
                        name="contactType"
                        onChange={(event) => setContactType(event.target.value)}
                        value={contactType}
                      >
                        <option value="">Select Recipient</option>
                        <option value="associate">Associates</option>
                        <option value="group">Group/Team</option>
                        <option value="office">Office</option>
                      </select>
                      {contactType === "associate" ? (
                        <>
                          <label className="form-label">
                            Find an associate by name:
                          </label>
                          <input
                            className="form-control"
                            id="inline-form-input"
                            type="text"
                            name="contactName"
                            onChange={(event) =>
                              setContactName(event.target.value)
                            }
                          />
                          {contactName ? (
                            <>
                              {getContact.length > 0 ? (
                                getContact.map((contact, index) => (
                                  <div
                                    className="member_Associate mb-1 mt-1"
                                    key={index}
                                    onClick={() =>
                                      handleContactAssociate(contact)
                                    }
                                  >
                                    <div
                                      className="float-left w-100 d-flex align-items-center justify-content-start"
                                      key={contact._id}
                                    >
                                      <img
                                        className="rounded-circlee profile_picture"
                                        height="30"
                                        width="30"
                                        src={
                                          contact.image &&
                                          contact.image !== "image"
                                            ? contact.image
                                            : Pic
                                        }
                                        alt="Profile"
                                      />

                                      {/* <div className="ms-3" onClick={()=>handleSubmit(post._id)}> */}
                                      <div className="ms-3">
                                        <strong>
                                          {contact.firstName} {contact.lastName}
                                        </strong>
                                        <br />
                                        <strong>{contact.active_office}</strong>
                                      </div>
                                    </div>
                                  </div>
                                ))
                              ) : (
                                <p></p>
                              )}
                            </>
                          ) : (
                            <></>
                          )}
                        </>
                      ) : (
                        ""
                      )}
                      {contactType === "group" ? (
                        <>
                          {contactGroupMember.length > 0 ? (
                            contactGroupMember.map((item, index) => (
                              <>
                                <div
                                  className="float-left w-100 mt-2 member_Associate"
                                  onClick={() => handleGroupData(item)}
                                >
                                  <div className="float-left w-100 d-flex align-items-center justify-content-between">
                                    <div className="float-left w-100 d-flex align-items-center justify-content-start">
                                      <img
                                        className="rounded-circlee profile_picture"
                                        height="30"
                                        width="30"
                                        src={Pic}
                                        alt="Profile"
                                      />
                                      <div className="ms-3">
                                        <strong>
                                          {item.groupDetails.group_name}
                                        </strong>
                                        <br />
                                        <strong>
                                          Integra Realty-{" "}
                                          {item.groupDetails.groupOffice}
                                        </strong>
                                        <br />
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </>
                            ))
                          ) : (
                            <p>No data to display.</p>
                          )}
                        </>
                      ) : (
                        ""
                      )}

                      {contactType === "office" ? (
                        <>
                          {organizationGet.data
                            ? organizationGet.data.map((item) => (
                                // console.log(item);
                                <div key={item._id}>
                                  <div className="py-3 d-flex">
                                    <NavLink className="">
                                      <p
                                        className="mb-0"
                                        onClick={() => handleOffice(item)}
                                      >
                                        <span className="pull-left">
                                          {item.tag}
                                        </span>
                                        &nbsp;{item.name}
                                      </p>
                                    </NavLink>
                                  </div>
                                  <hr className="" />
                                </div>
                              ))
                            : ""}
                        </>
                      ) : (
                        ""
                      )}
                    </div>
                    <div className="col-md-6">
                      <label className="form-label">Selected Recipients</label>
                      <div className="col-sm-12 member_group">
                        {contactsAssociate.length > 0 ? (
                          contactsAssociate.map((contact, index) => (
                            <div className="member_Associate mb-1" key={index}>
                              <div
                                className="float-left w-100 d-flex align-items-center justify-content-start"
                                key={contact._id}
                              >
                                <img
                                  className="rounded-circlee profile_picture"
                                  height="30"
                                  width="30"
                                  src={
                                    contact.image && contact.image !== "image"
                                      ? contact.image
                                      : Pic
                                  }
                                  alt="Profile"
                                />

                                {/* <div className="ms-3" onClick={()=>handleSubmit(post._id)}> */}
                                <div className="ms-3">
                                  <strong>
                                    {contact.firstName} {contact.lastName}
                                  </strong>
                                  <br />
                                  <strong>{contact.active_office}</strong>
                                </div>
                                <div
                                  className="groupMail_delete"
                                  onClick={() =>
                                    handleDelete(contact._id, index)
                                  }
                                >
                                  <i className="h2 fi-trash"></i>
                                </div>
                              </div>
                            </div>
                          ))
                        ) : (
                          <p></p>
                        )}

                        {contactsGroups.length > 0 ? (
                          contactsGroups.map((contact, index) => (
                            <div className="member_Associate mb-1" key={index}>
                              <div
                                className="float-left w-100 d-flex align-items-center justify-content-start"
                                key={contact._id}
                              >
                                <img
                                  className="rounded-circlee profile_picture"
                                  height="30"
                                  width="30"
                                  src={Pic}
                                  alt="Profile"
                                />

                                <div className="ms-3">
                                  <strong>
                                    {contact.groupDetails.group_name}
                                  </strong>
                                  <br />
                                  <strong>
                                    {contact.groupDetails.groupOffice}
                                  </strong>
                                </div>
                                <div
                                  className="groupMail_delete"
                                  onClick={() =>
                                    handleDeleteGroup(contact._id, index)
                                  }
                                >
                                  <i className="h2 fi-trash"></i>
                                </div>
                              </div>
                            </div>
                          ))
                        ) : (
                          <p></p>
                        )}

                        {officeGroups.length > 0 ? (
                          officeGroups.map((office, index) => (
                            <div className="member_Associate mb-1" key={index}>
                              <div
                                className="float-left w-100 d-flex align-items-center justify-content-start"
                                key={office._id}
                              >
                                <div className="ms-3 d-flex">
                                  <strong>{office.tag}</strong>
                                  <strong className="ms-2">
                                    {office.name}
                                  </strong>
                                </div>
                                <div
                                  className="groupMail_delete"
                                  onClick={() =>
                                    handleDeleteOffice(office._id, index)
                                  }
                                >
                                  <i className="h2 fi-trash"></i>
                                </div>
                              </div>
                            </div>
                          ))
                        ) : (
                          <p></p>
                        )}
                      </div>
                    </div>
                  </div>
                  <div className="row mt-5">
                    <div className="col-md-6 mt-3">
                      <label className="form-label" for="Category">
                        Category
                      </label>
                      <select
                        className="form-select"
                        id="Category"
                        name="category"
                        value={formValues.category}
                        onChange={handleChange}
                      >
                        <option value="Accounting">Accounting</option>
                        <option value="Broker">Broker</option>
                        <option value="Confirmation">Conformation</option>
                        <option value="Lost and Found">Lost &amp; Found</option>
                        <option value="Office">Office</option>
                        <option value="What’s New">What’s New</option>
                        <option value="New Site Feature">
                          New Site Feature
                        </option>
                        <option value="Peronal Message">Peronal Message</option>
                        <option value="Concern">Concern</option>
                        <option value="Support">Support</option>
                        <option value="Performance">Performance</option>
                        <option value="MLS Violation Notice">
                          MLS Violation Notice
                        </option>
                        <option value="MLS Update">MLS Update</option>
                        <option value="Board Info">Board Info</option>
                        <option value="Board Violation Notice">
                          Board Violation Notice
                        </option>
                        <option value="Violation">Violation</option>
                        <option value="Memo">Memo</option>
                        <option value="Issue">Issue</option>
                        <option value="Policy Update">Policy Update</option>
                        <option value="Thanks">Thanks</option>
                        <option value="HR">HR</option>
                        <option value="Birthday">Birthday</option>
                        <option value="Training Announcement">
                          Training Announcement
                        </option>
                        <option value="Company News">Company News</option>
                        <option value="Realtor">Realtor</option>
                        <option value="News">News</option>
                      </select>
                    </div>
                    <div className="col-md-6">
                      <label className="documentlabel pull-right mt-4">
                        {" "}
                        Upload File
                        <input
                          className="form-control"
                          id="inline-form-input"
                          type="file"
                          name="image"
                          onChange={handleFileUpload}
                          value={formValues.image}
                        />
                      </label>
                    </div>
                  </div>
                  {/* <div className="row mt-3">
                    <div className="col-md-4 mt-3">
                      <label className="form-label">Expiration Date</label>
                      <input
                        className="form-control"
                        type="date"
                        id="inline-form-input"
                        placeholder="Choose date"
                        name="expiration_date"
                        value={formValues.expiration_date}
                        onChange={handleChange}
                        data-datepicker-options='{"altInput": true, "altFormat": "F j, Y", "dateFormat": "Y-m-d"}'
                      />
                    </div>
                    <div className="col-md-8 mt-3">
                      <label className="form-label">
                        Subject — Please enter a unique, descriptive subject.
                      </label>
                      <input
                        className="form-control"
                        id="inline-form-input"
                        type="text"
                        name="title"
                        value={formValues.title}
                        onChange={handleChange}
                        style={{
                          border: formErrors?.title
                            ? "1px solid red"
                            : "1px solid #00000026",
                        }}
                      />
                      {formErrors.title && (
                        <div className="invalid-tooltip">
                          {formErrors.title}
                        </div>
                      )}
                    </div>
                  </div> */}

                  <div className="row">
                    <div className="col-md-12 mt-3">
                      <label className="form-label">Message</label>
                      <Editor
                        editorState={editorState}
                        onEditorStateChange={handleChanges}
                        value={formValues.message}
                        toolbar={{
                          options: [
                            "inline",
                            "blockType",
                            "fontSize",
                            "list",
                            "textAlign",
                            "history",
                            "link", // Add link option here
                          ],
                          inline: {
                            options: [
                              "bold",
                              "italic",
                              "underline",
                              "strikethrough",
                            ],
                          },
                          list: { options: ["unordered", "ordered"] },
                          textAlign: {
                            options: ["left", "center", "right"],
                          },
                          history: { options: ["undo", "redo"] },
                          link: {
                            // Configure link options
                            options: ["link", "unlink"],
                          },
                        }}
                        wrapperClassName="demo-wrapper"
                        editorClassName="demo-editor"
                      />
                      {/* {formErrors.message && (
                        <div className="invalid-tooltip">
                          {formErrors.message}
                        </div>
                      )} */}
                    </div>
                  </div>
                </div>
              </div>
              <div className="pull-right mt-4">
                <button
                  className="btn btn-primary pull-right ms-5"
                  type="button"
                  onClick={handleSubmit}
                  // disabled={isSubmitClick}
                >
                  Send Notice
                  {/* {isSubmitClick ? "Please wait..." : "Send Notice"}{" "} */}
                </button>
                <NavLink
                  className="btn btn-secondary pull-right"
                  type="button"
                  to="/notice"
                >
                  Previous Notice
                </NavLink>
              </div>
            </div>
          </div>
        </div>
      </main>
    </div>
  );
}
export default Noticemessage;

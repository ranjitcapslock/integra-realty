import React, { useState, useEffect, useRef } from "react";
import Pic from "../img/pic.png";
import Modal from "../../Modal";
import user_service from "../service/user_service";
import ReactPaginate from "react-paginate";
import Loader from "../../Pages/Loader/Loader.js";
import Toaster from "../../Pages/Toaster/Toaster.js";
import { NavLink, useNavigate, useParams } from "react-router-dom";
import jwt from "jwt-decode";
import $, { each } from "jquery";
import { Editor } from "react-draft-wysiwyg";
import { EditorState, ContentState, convertToRaw } from "draft-js";
import draftToHtml from "draftjs-to-html";
import htmlToDraft from "html-to-draftjs";
import "react-draft-wysiwyg/dist/react-draft-wysiwyg.css";
import axios from "axios";
import _ from "lodash";


function Invitecontacts() {
  const [selectedContacts, setSelectedContacts] = useState({});

  const [getContact, setGetContact] = useState([]);
  const [pageCount, setPageCount] = useState(0);
  const [totalRecords, setTotalRecords] = useState("");
  const [loader, setLoader] = useState({ isActive: null });
  const { isActive } = loader;
  const [toaster, setToaster] = useState({
    types: null,
    isShow: null,
    toasterBody: "",
    message: "",
  });
  const { types, isShow, toasterBody, message } = toaster;

  const [contact_status, setContact_status] = useState("active");

  const [currentPage, setCurrentPage] = useState(1);
  const [isLoading, setIsLoading] = useState(false);

  const [isSubmitClick, setISSubmitClick] = useState(false);

  const [notesmessage, setNotesmessage] = useState("");
  const editorRef = useRef(null);
  const log = () => {
    if (editorRef.current) {
      setNotesmessage(editorRef.current.getContent());
    }
  };

  const params = useParams();

  const initialValues = {
    subject: "",
    message: "",
    image: "",
  };

  const navigate = useNavigate();
  const [formValues, setFormValues] = useState(initialValues);
  const [formData, setFormData] = useState({});
  const [detail, setDetail] = useState("");
  const [formErrors, setFormErrors] = useState({});

  const profileGetAll = async () => {
    setLoader({ isActive: true });
    await user_service
      .profileGet(jwt(localStorage.getItem("auth")).id)
      .then((response) => {
        if (response) {
          setLoader({ isActive: false });
          setFormData(response.data);
          const contentBlock = htmlToDraft(response.data.email_signature);
          if (contentBlock) {
            const contentState = ContentState.createFromBlockArray(
              contentBlock.contentBlocks
            );
            setEditorState(EditorState.createWithContent(contentState));
          }
        }
      });
  };

  const PostGetById = async () => {
    await user_service.postDetail(params.id).then((response) => {
      setLoader({ isActive: false });
      if (response) {
        setDetail(response.data);
      }
    });
  };
  useEffect(() => {
    window.scrollTo(0, 0);
    profileGetAll();
    if (params.type == "post") {
      PostGetById(params.id);
    }
  }, []);

  useEffect(() => {
    window.scrollTo(0, 0);
    if (params.id) {
      setFormValues({
        subject: "Announcement for " + (detail?.postTitle || ""),
        message: `<b>${detail?.postTitle}</b> <p><a href="${
          window.location.origin
        }/post-detail/${detail?._id}">View Full Post</a></p><p>${
          formData?.email_signature || ""
        }</p>`,
      });
    } else {
      setFormValues({
        //subject: "Announcement for " + (detail?.postTitle || ""),
        message: `<p>${formData?.email_signature || ""}</p>`,
      });
    }
  }, [detail, formData]);

  const [editorState, setEditorState] = useState(EditorState.createEmpty());

  const handleChanges = (editorState) => {
    setEditorState(editorState);
    const htmlContent = draftToHtml(
      convertToRaw(editorState.getCurrentContent())
    );
    setFormValues({ ...formValues, message: htmlContent });
  };

  const handleChange = (e) => {
    const { name, value } = e.target;
    setFormValues({ ...formValues, [name]: value });
  };

  const [contactType, setContactType] = useState("");
  const [contactStatus, setContactStatus] = useState("active");

  const [contactName, setContactName] = useState("");
  const [file, setFile] = useState(null);
  const [data, setData] = useState("");

  const SearchGetAll = async () => {
    try {
      const response = await user_service.SearchContactfilterAssociate(
        1,
        contactType,
        contactStatus,
        contactName
      );
      if (response) {
        setGetContact(response.data.data);
        // setPageCount(Math.ceil(response.data.totalRecords / 10));
        // setTotalRecords(response.data.totalRecords);
      }
    } catch (error) {
      console.error("Error fetching data:", error);
    }
  };

  useEffect(() => {
    window.scrollTo(0, 0);
    if (contactName) {
      SearchGetAll(1);
    }
  }, [contactType, contactName]);

  const [contactsAssociate, setContactsAssociate] = useState([]);
  const handleContactAssociate = (contactAssociate) => {
    setContactsAssociate((prevContacts) => [...prevContacts, contactAssociate]);
    setGetContact([]);
    setContactName("");
  };

  const [contactsGroups, setContactsGroups] = useState([]);

  const handleGroupData = (data) => {
    setContactsGroups((prevContacts) => [...prevContacts, data]);
    // setContactGroupMember([]);
    // setContactName({});
  };

  const [officeGroups, setOfficeGroups] = useState([]);

  const handleOffice = (item) => {
    console.log(item);
    setOfficeGroups((prevContacts) => [...prevContacts, item]);
  };

  const handleFileUpload = async (e) => {
    const selectedFile = e.target.files[0];

    if (selectedFile) {
      setFile(selectedFile);

      const formData = new FormData();
      formData.append("file", selectedFile);
      console.log(selectedFile);
      const config = {
        headers: {
          Accept: "application/json",
          "Content-Type": "multipart/form-data",
          Authorization: `Bearer ${localStorage.getItem("auth")}`,
        },
      };
      try {
        setLoader({ isActive: true });
        const uploadResponse = await axios.post(
          "https://api.brokeragentbase.com/upload",
          formData,
          config
        );
        const uploadedFileData = uploadResponse.data;
        setLoader({ isActive: false });
        setToaster({
          type: "Upload File",
          isShow: true,
          message: "File Upload Successfully",
        });
        setTimeout(() => {
          setToaster((prevToaster) => ({ ...prevToaster, isShow: false }));
          // navigate("/control-panel/documents/");
        }, 2000);
        setData(uploadedFileData);
      } catch (error) {
        console.error("Error occurred during file upload:", error);
      }
    }
  };

  useEffect(() => {
    // window.scrollTo(0, 0);
    if (formValues && isSubmitClick) {
      validate();
    }
  }, [formValues]);

  const validate = () => {
    const values = formValues;
    const errors = {};
    if (!values.subject) {
      errors.subject = "Subject is required";
    }

    setFormErrors(errors);
    return errors;
  };
  const handleSubmit = async (e) => {
    e.preventDefault();

    setISSubmitClick(true);
    let checkValue = validate();
    if (_.isEmpty(checkValue)) {
    const userData = {
      agentId: jwt(localStorage.getItem("auth")).id,
      subject: formValues.subject,
      message: formValues.message,
      contactType: contactsAssociate,
      contactGroup: contactsGroups,
      officeGroup: officeGroups,
      image: data,
    };
    console.log(userData);
    setLoader({ isActive: true });
    await user_service.invitecontact(userData).then((response) => {
      if (response) {
        setContactsGroups([]);
        setOfficeGroups([]);
        setContactsAssociate([]);
        setOrganizationGet([])
        setFormValues({});
        setLoader({ isActive: false });
        setISSubmitClick(false);
        setToaster({
          type: "Email",
          isShow: true,
          // toasterBody: response.data.message,
          message: "Email Send Successfully",
        });
        setTimeout(() => {
          setToaster((prevToaster) => ({ ...prevToaster, isShow: false }));
          navigate("/");
        }, 2000);
      } else {
        setLoader({ isActive: false });
        setToaster({
          types: "error",
          isShow: true,
          toasterBody: response.data.message,
          message: "Error",
        });
        setTimeout(() => {
          setToaster((prevToaster) => ({ ...prevToaster, isShow: false }));
          // navigate("/control-panel/documents/");
        }, 2000);
      }
    });
  }
  };

  useEffect(() => {
    window.scrollTo(0, 0);
    setLoader({ isActive: true });
    ContactGroupMemberGetAll();
    OrganizationTreeGets();
  }, []);

  const [contactGroupMember, setContactGroupMember] = useState([]);

  const ContactGroupMemberGetAll = async () => {
    try {
      setLoader({ isActive: true });
      const response = await user_service.contactGroupMemberGetAll();
      setLoader({ isActive: false });
      if (response && response.data) {
        setContactGroupMember(response.data.data);
      }
    } catch (error) {
      console.error("Error fetching data:", error);
      setLoader({ isActive: false });
    }
  };

  const [organizationGet, setOrganizationGet] = useState([]);

  const OrganizationTreeGets = async () => {
    // setLoader({ isActive: true });
    if (jwt(localStorage.getItem("auth")).id) {
      const agentId = jwt(localStorage.getItem("auth")).id;
      await user_service.organizationTreeGet().then((response) => {
        // setLoader({ isActive: false });
        if (response) {
          setOrganizationGet(response.data);
        }
      });
    }
  };

  const handleDelete = (id, memberIndex) => {
    const updatedMembers = contactsAssociate.filter(
      (_, index) => index !== memberIndex
    );
    setContactsAssociate(updatedMembers);
  };

  const handleDeleteGroup = (id, memberIndex) => {
    const updatedMembers = contactsGroups.filter(
      (_, index) => index !== memberIndex
    );
    setContactsGroups(updatedMembers);
  };

  const handleDeleteOffice = (id, officeIndex) => {
    const updatedOffice = officeGroups.filter(
      (_, index) => index !== officeIndex
    );
    setOfficeGroups(updatedOffice);
  };
  

  return (
    <div className="bg-secondary float-left w-100 pt-4 mb-4">
      <Loader isActive={isActive} />
      {isShow && (
        <Toaster
          types={types}
          isShow={isShow}
          toasterBody={toasterBody}
          message={message}
        />
      )}
      <main className="page-wrapper">
        <Modal />
        {/* <!-- Page container--> */}
        <div className="content-overlay">
          {/* <!-- Page card like wrapper--> */}
          <div className="">
            <div className="">
              {/* <!-- List of resumes--> */}
              <div className="d-flex align-items-center justify-content-start w-100 mb-4">
                <h3 className="text-white mb-0">Email</h3>
              </div>
              <div className="bg-light border rounded-3 p-3">
                <p className="w-100">
                  Compose your email message here. Use this space to craft your
                  message, add links, images, and personalize it before sending
                  it to your recipients. Remember to proofread and test before
                  hitting send!
                </p>
                <div className="">
                  <div className="row">
                    <div className="col-md-4">
                      <label className="col-form-label">To</label>
                      <select
                        className="form-select"
                        id="pr-city"
                        name="contactType"
                        onChange={(event) => setContactType(event.target.value)}
                        value={contactType}
                      >
                        <option value="">Select Recipient</option>
                        <option value="associate">Associates</option>
                        <option value="group">Group/Team</option>
                        <option value="office">Office</option>
                      </select>
                      {contactType === "associate" ? (
                        <>
                          <label className="form-label">
                            Find an associate by name:
                          </label>
                          <input
                            className="form-control"
                            id="inline-form-input"
                            type="text"
                            name="contactName"
                            onChange={(event) =>
                              setContactName(event.target.value)
                            }
                          />
                          {contactName ? (
                            <>
                              {getContact.length > 0 ? (
                                getContact.map((contact, index) => (
                                  <div
                                    className="member_Associate mb-1 mt-1"
                                    key={index}
                                    onClick={() =>
                                      handleContactAssociate(contact)
                                    }
                                  >
                                    <div
                                      className="float-left w-100 d-flex align-items-center justify-content-start"
                                      key={contact._id}
                                    >
                                      <img
                                        className="rounded-circlee profile_picture"
                                        height="30"
                                        width="30"
                                        src={
                                          contact.image &&
                                          contact.image !== "image"
                                            ? contact.image
                                            : Pic
                                        }
                                        alt="Profile"
                                      />

                                      {/* <div className="ms-3" onClick={()=>handleSubmit(post._id)}> */}
                                      <div className="ms-3">
                                        <strong>
                                          {contact.firstName} {contact.lastName}
                                        </strong>
                                        <br />
                                        <strong>{contact.active_office}</strong>
                                      </div>
                                    </div>
                                  </div>
                                ))
                              ) : (
                                <p></p>
                              )}
                            </>
                          ) : (
                            <></>
                          )}
                        </>
                      ) : (
                        ""
                      )}
                      {contactType === "group" ? (
                        <>
                          {contactGroupMember.length > 0 ? (
                            contactGroupMember.map((item, index) => (
                              <>
                                <div
                                  className="float-left w-100 mt-2 member_Associate"
                                  onClick={() => handleGroupData(item)}
                                >
                                  <div className="float-left w-100 d-flex align-items-center justify-content-between">
                                    <div className="float-left w-100 d-flex align-items-center justify-content-start">
                                      <img
                                        className="rounded-circlee profile_picture"
                                        height="30"
                                        width="30"
                                        src={Pic}
                                        alt="Profile"
                                      />
                                      <div className="ms-3">
                                        <strong>
                                          {item.groupDetails.group_name}
                                        </strong>
                                        <br />
                                        <strong>
                                          Integra Realty-{" "}
                                          {item.groupDetails.groupOffice}
                                        </strong>
                                        <br />
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </>
                            ))
                          ) : (
                            <p>No data to display.</p>
                          )}
                        </>
                      ) : (
                        ""
                      )}

                      {contactType === "office" ? (
                        <>
                          {organizationGet.data
                            ? organizationGet.data.map((item) => (
                                // console.log(item);
                                <div key={item._id}>
                                  <div className="py-3 d-flex">
                                    <NavLink className="">
                                      <p
                                        className="mb-0"
                                        onClick={() => handleOffice(item)}
                                      >
                                        <span className="pull-left">
                                          {item.tag}
                                        </span>
                                        &nbsp;{item.name}
                                      </p>
                                    </NavLink>
                                  </div>
                                  <hr className="" />
                                </div>
                              ))
                            : ""}
                        </>
                      ) : (
                        ""
                      )}
                    </div>
                    <div className="col-md-6">
                      <label className="form-label">Selected Recipients</label>
                      <div className="col-sm-12 member_group">
                        {contactsAssociate.length > 0 ? (
                          contactsAssociate.map((contact, index) => (
                            <div className="member_Associate mb-1" key={index}>
                              <div
                                className="float-left w-100 d-flex align-items-center justify-content-start"
                                key={contact._id}
                              >
                                <img
                                  className="rounded-circlee profile_picture"
                                  height="30"
                                  width="30"
                                  src={
                                    contact.image && contact.image !== "image"
                                      ? contact.image
                                      : Pic
                                  }
                                  alt="Profile"
                                />

                                {/* <div className="ms-3" onClick={()=>handleSubmit(post._id)}> */}
                                <div className="ms-3">
                                  <strong>
                                    {contact.firstName} {contact.lastName}
                                  </strong>
                                  <br />
                                  <strong>{contact.active_office}</strong>
                                </div>
                                <div
                                  className="groupMail_delete"
                                  onClick={() =>
                                    handleDelete(contact._id, index)
                                  }
                                >
                                  <i className="h2 fi-trash"></i>
                                </div>
                              </div>
                            </div>
                          ))
                        ) : (
                          <p></p>
                        )}

                        {contactsGroups.length > 0 ? (
                          contactsGroups.map((contact, index) => (
                            <div className="member_Associate mb-1" key={index}>
                              <div
                                className="float-left w-100 d-flex align-items-center justify-content-start"
                                key={contact._id}
                              >
                                <img
                                  className="rounded-circlee profile_picture"
                                  height="30"
                                  width="30"
                                  src={Pic}
                                  alt="Profile"
                                />

                                <div className="ms-3">
                                  <strong>
                                    {contact.groupDetails.group_name}
                                  </strong>
                                  <br />
                                  <strong>
                                    {contact.groupDetails.groupOffice}
                                  </strong>
                                </div>
                                <div
                                  className="groupMail_delete"
                                  onClick={() =>
                                    handleDeleteGroup(contact._id, index)
                                  }
                                >
                                  <i className="h2 fi-trash"></i>
                                </div>
                              </div>
                            </div>
                          ))
                        ) : (
                          <p></p>
                        )}

                        {officeGroups.length > 0 ? (
                          officeGroups.map((office, index) => (
                            <div className="member_Associate mb-1" key={index}>
                              <div
                                className="float-left w-100 d-flex align-items-center justify-content-start"
                                key={office._id}
                              >
                              
                                <div className="ms-3 d-flex">
                                <strong>
                                    {office.tag}
                                  </strong>
                                  <strong className="ms-2">
                                    {office.name}
                                  </strong>
                                
                                </div>
                                <div
                                  className="groupMail_delete"
                                  onClick={() =>
                                    handleDeleteOffice(office._id, index)
                                  }
                                >
                                  <i className="h2 fi-trash"></i>
                                </div>
                              </div>
                            </div>
                          ))
                        ) : (
                          <p></p>
                        )}
                      </div>
                    </div>
                  </div>
                  <div className="row mt-5">
                    <div className="col-md-6">
                      <label className="col-form-label">
                        Subject — Please enter a unique, descriptive subject.
                      </label>

                      <input
                        className="form-control"
                        id="inline-form-input"
                        type="text"
                        name="subject"
                        value={formValues.subject}
                        onChange={handleChange}
                        style={{
                          border: formErrors?.subject
                            ? "1px solid red"
                            : "1px solid #00000026",
                        }}
                      />
                      {formErrors.subject && (
                        <div className="invalid-tooltip">
                          {formErrors.subject}
                        </div>
                      )}
                    </div>
                    <div className="col-md-6">
                      <label className="documentlabel pull-right mt-4">
                        {" "}
                        Upload File
                        <input
                          className="form-control"
                          id="inline-form-input"
                          type="file"
                          name="image"
                          onChange={handleFileUpload}
                          value={formValues.image}
                        />
                      </label>
                    </div>
                  </div>
                  <div className="row">
                    <div className="col-md-12">
                      <label className="col-form-label">Message</label>

                      <Editor
                          editorState={editorState}
                          onEditorStateChange={handleChanges}
                          value={formValues.message}
                          toolbar={{
                            options: [
                              "inline",
                              "blockType",
                              "fontSize",
                              "list",
                              "textAlign",
                              "history",
                              "link", // Add link option here
                            ],
                            inline: {
                              options: [
                                "bold",
                                "italic",
                                "underline",
                                "strikethrough",
                              ],
                            },
                            list: { options: ["unordered", "ordered"] },
                            textAlign: {
                              options: ["left", "center", "right"],
                            },
                            history: { options: ["undo", "redo"] },
                            link: {
                              // Configure link options
                              options: ["link", "unlink"],
                            },
                          }}
                          wrapperClassName="demo-wrapper"
                          editorClassName="demo-editor"
                        />
                      {formErrors.message && (
                        <div className="invalid-tooltip">
                          {formErrors.message}
                        </div>
                      )}
                    </div>
                  </div>
                </div>
              </div>
              <div className="pull-right mt-3">
                <NavLink
                  className="btn btn-secondary pull-right ms-3"
                  type="button"
                  to="/"
                >
                  Cancel
                </NavLink>
                <button
                  className="btn btn-primary pull-right"
                  type="button"
                  onClick={handleSubmit}
                  // disabled={isSubmitClick}
                >
                  Send Mail
                  {/* {isSubmitClick ? "Please wait..." : "Send Mail"}{" "} */}
                </button>
              </div>
            </div>
          </div>
        </div>
      </main>
    </div>
  );
}
export default Invitecontacts;

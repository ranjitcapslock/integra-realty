import React, { useState, useEffect } from 'react'
// import AddDocuments from './AddDocuments';
import { NavLink, useNavigate, useParams } from "react-router-dom";
import user_service from '../service/user_service';
import Loader from '../../Pages/Loader/Loader.js';
import Toaster from '../../Pages/Toaster/Toaster';
import AllTab from '../../Pages/AllTab';
import axios from 'axios';
import jwt from "jwt-decode";

const Banners = () => {
    const navigate = useNavigate();
    const [getBanner, setGetBanner] = useState([])
    const [dateDifference, setDateDifference] = useState({ years: 0, months: 0 });
    const [loader, setLoader] = useState({ isActive: null })
    const { isActive } = loader;
    const [toaster, setToaster] = useState({ types: null, isShow: null, toasterBody: "", message: "" });
    const { types, isShow, toasterBody, message } = toaster;
    const initialValues = {
        title: "", publication_level: "", url_link: "", isnewtab: "", startDate: "", endDate: "",
        max_impressions: "", max_clicks: "", weighting: "", marque_image: "", side_image: "", isactive: "", agentId: ""
    };
    const [formValues, setFormValues] = useState(initialValues);
    const [file, setFile] = useState(null);
    const [data, setData] = useState("")
    const [itemId, setItemId] = useState("")
    const [fileExtension, setFileExtension] = useState('');
    const [acceptedFileTypes, setAcceptedFileTypes] = useState([]);
    const calculateDateDifference = (startDate, endDate) => {
        const start = new Date(startDate);
        const end = new Date(endDate);
        const yearDiff = end.getFullYear() - start.getFullYear();
        const monthDiff = end.getMonth() - start.getMonth() + yearDiff * 12;
        const timeDiff = Math.abs(end.getTime() - start.getTime());
        const dayDiff = Math.floor(timeDiff / (1000 * 3600 * 24));
        return { years: yearDiff, months: monthDiff, days: dayDiff };
    };

    const formatRemainingTime = (timeDiff) => {
        const years = timeDiff.years > 0 ? `${timeDiff.years} year${timeDiff.years > 1 ? 's' : ''}, ` : '';
        const months = timeDiff.months > 0 ? `${timeDiff.months} month${timeDiff.months > 1 ? 's' : ''}, ` : '';
        const weeks = timeDiff.weeks > 0 ? `${timeDiff.weeks} week${timeDiff.weeks > 1 ? 's' : ''}, ` : '';
        const days = timeDiff.days > 0 ? `${timeDiff.days} day${timeDiff.days > 1 ? 's' : ''}` : '';
        return `${years}${months}${weeks}${days} ago`;
    };

    const formatRemainingTimeWithDays = (timeDiff) => {
        if (timeDiff.months > 0) {
            return formatRemainingTime(timeDiff);
        } else {
            return `${timeDiff.months} months`;
        }
    };

    useEffect(() => {
        window.scrollTo(0, 0);
        setLoader({ isActive: true });

        const BannerList = async () => {
            await user_service.bannerGet().then((response) => {
                setLoader({ isActive: false });
                if (response) {
                    setGetBanner(response.data);
                }
            });
        };

        BannerList();
    }, []);


    // const calculateDateDifference = (startDate, endDate) => {
    //     const start = new Date(startDate);
    //     const end = new Date(endDate);

    //     const yearDiff = end.getFullYear() - start.getFullYear();
    //     const monthDiff = end.getMonth() - start.getMonth() + yearDiff * 12;

    //     const timeDiff = Math.abs(end.getTime() - start.getTime());
    //     const dayDiff = Math.floor(timeDiff / (1000 * 3600 * 24));

    //     return { years: yearDiff, months: monthDiff, days: dayDiff };
    // };


    // useEffect(() => { window.scrollTo(0, 0);
    //     setLoader({ isActive: true });


    //     const BannerList = async () => {
    //         await user_service.bannerGet().then((response) => {
    //             setLoader({ isActive: false });
    //             if (response) {
    //                 setGetBanner(response.data);

    //                 if (response.data.data.length > 0) {
    //                     response.data.data.forEach((item) => {
    //                         console.log(item.startDate);
    //                         console.log(item.endDate);

    //                         const dateDiff = calculateDateDifference(item.startDate, item.endDate);
    //                         console.log("Date Difference:", dateDiff);
    //                     });
    //                 }
    //             }
    //         });
    //     };

    //     BannerList();
    // }, []);


    const handleFileUploadSide = async (e, id) => {
        setItemId(id);
        const selectedFiles = e.target.files; // Use e.target.files to access selected files

        if (selectedFiles && selectedFiles.length > 0) { // Check if files are selected
            const selectedFile = selectedFiles[0]; // Access the first selected file
            setFile(selectedFile);

            const fileExtension = selectedFile.name.split('.').pop(); // Access the file extension

            const formData = new FormData();
            formData.append('file', selectedFile);

            const config = {
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'multipart/form-data',
                    Authorization: `Bearer ${localStorage.getItem('auth')}`,
                },
            };

            try {
                setLoader({ isActive: true });
                const uploadResponse = await axios.post('https://api.brokeragentbase.com/upload', formData, config);
                const uploadedFileData = uploadResponse.data;
                setLoader({ isActive: false });
                setData(uploadedFileData);

                const userData = {
                    agentId: jwt(localStorage.getItem('auth')).id,
                    side_image: uploadedFileData
                };

                console.log(userData);
                try {
                    const response = await user_service.bannerUpdate(itemId, userData);
                    if (response) {
                        console.log(response);
                        const BannerList = async () => {
                            await user_service.bannerGet().then((response) => {
                                setLoader({ isActive: false });
                                if (response) {
                                    setGetBanner(response.data);
                                }
                            });
                        }
                        BannerList();

                        document.getElementById('closeModalSide').click();
                    }
                } catch (error) {
                    console.log('Error occurred while uploading documents:', error);
                }

            } catch (error) {
                console.error('Error occurred during file upload:', error);
            }
        } else {
            console.log('No file selected.');
        }
    }


  
    const handleFileUpload = async (e, id) => {
        setItemId(id);
        const selectedFile = e.target.files;

        if (selectedFile && selectedFile[0]) { // Check if selectedFile is defined and contains at least one file
            setFile(selectedFile);
            const fileExtension = selectedFile[0].name.split('.').pop(); // Access the file extension

            const formData = new FormData();
            formData.append('file', selectedFile[0]);
            const config = {
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'multipart/form-data',
                    Authorization: `Bearer ${localStorage.getItem('auth')}`,
                },
            };

            try {
                setLoader({ isActive: true })
                const uploadResponse = await axios.post('https://api.brokeragentbase.com/upload', formData, config);
                const uploadedFileData = uploadResponse.data;
                setLoader({ isActive: false })
                setData(uploadedFileData);

                const userData = {
                    agentId: jwt(localStorage.getItem('auth')).id,
                    marque_image: uploadedFileData,
                };

                console.log(userData);
                try {
                    const response = await user_service.bannerUpdate(itemId, userData);
                    if (response) {
                        console.log(response);
                        const BannerList = async () => {
                            await user_service.bannerGet().then((response) => {
                                setLoader({ isActive: false })
                                if (response) {
                                    setGetBanner(response.data)
                                }
                            });
                        }
                        BannerList()

                        document.getElementById('closeModal').click();
                    }
                } catch (error) {
                    console.log('Error occurred while uploading documents:', error);
                }
            } catch (error) {
                console.error('Error occurred during file upload:', error);
            }
        } else {
            console.log('No file selected.');
        }
    }


    return (
        <div className="bg-secondary float-left w-100 pt-4 mb-4">
            <Loader isActive={isActive} />
            {isShow && <Toaster types={types}
                isShow={isShow}
                toasterBody={toasterBody}
                message={message} />}
            <main className="page-wrapper profile_page_wrap">
                <div className="">
                    <div className="d-flex align-items-center justify-content-start mb-4">
                        <h3 className="mb-0 text-white">Promotional Banners </h3>
                    </div>
                    <div className="">
                        <div className="d-flex align-items-center justify-content-between callout accent" id="BtnFocus">
                            <div className="small-12 columns text-white">
                                106 Campaigns
                                (5 Active)
                            </div>

                            <div className="pull-right">
                                <NavLink to="/add-banner" className="btn btn-primary btn-sm  pull-right">Add Banner</NavLink>
                            </div>
                        </div>
                            {/* <hr /> */}
                            <div className="">
                                <div className="">
                                 


                                <div className="float-left w-100 mt-lg-5 mt-md-5 mt-sm-3 mt-3">
                                        {
                                            getBanner && getBanner.data ?
                                                getBanner.data.map((item) => {
                                                    return (
                                                        <div className="bg-light rounded-3 border p-3 mb-4">
                                                            <div className="row">
                                                                <div className="col-md-8" key={item._id}>
                                                                    <div className="views_click float-left w-100 d-flex align-items-center justify-content-between mb-3">
                                                                        <h6 className="mb-0">{item.title}</h6>
                                                                        <h6 className="ms-5 mb-0">{item.total_views ? item.total_views : 0} Views</h6>
                                                                        <h6 className="ms-5 mb-0">{item.total_clicks ? item.total_clicks : 0} Clicks</h6>
                                                                        <h6 className="ms-5 mb-0">
                                                                            <span className="pull-right tags-badge d-flex align-items-center justify-content-end w-100">
                                                                                <badge> {
                                                                                    item.banner_location ?
                                                                                        item.banner_location == "home" ?
                                                                                            "Home Page"
                                                                                            : item.banner_location == "promote" ?
                                                                                                "Promote/Store Page"
                                                                                                :
                                                                                                item.banner_location == "marketplace"
                                                                                                    ?
                                                                                                    "Market Place"
                                                                                                    :
                                                                                                    item.banner_location == "contact"
                                                                                                        ?
                                                                                                        "Contact"

                                                                                                        : "Homepage"
                                                                                        : "Homepage"
                                                                                }
                                                                                </badge>
                                                                            </span>
                                                                        </h6>


                                                                        {item ?
                                                                            item.endDate !== null ?
                                                                                item.marque_image || item.side_image ?
                                                                                    (
                                                                                        new Date(item.endDate) < new Date() ? (
                                                                                            <p style={{ color: '#990000', fontSize: 20 }} className="ms-5 mb-0">Expired {formatRemainingTimeWithDays(calculateDateDifference(item.startDate, item.endDate))}</p>
                                                                                        ) : (
                                                                                                <p style={{ color: "#009900", fontSize: 20 }} className="ms-5 mb-0">
                                                                                                Active until &nbsp;
                                                                                                {calculateDateDifference(item.startDate, item.endDate).months} months
                                                                                                from now.
                                                                                            </p>
                                                                                        )
                                                                                    ) : (
                                                                                        <p className="float-start w-100 text-center text-white">No Banners Assigned</p>
                                                                                    ) : (
                                                                                    ""
                                                                                ) : (
                                                                                <p className="float-start w-100 text-center text-white">Active, no expiration date.</p>
                                                                            )}
                                                                        {/* <p className="ms-5">Active until {calculateDateDifference(item.startDate, item.endDate).months.day} months from now.</p> */}
                                                                    </div>

                                                                    {
                                                                        item.marque_image ?
                                                                            <>

                                                                                <div className="NotSetup  text-center">
                                                                                    <img className="img-fluid w-100" src={item.marque_image} alt="Document Preview" />
                                                                                </div>

                                                                            </>
                                                                            :

                                                                            <div className="NotSetup Marquee text-center">
                                                                                <button type="button" className="btn btn-primary" data-toggle="modal" onClick={(e) => handleFileUpload(e, item._id)} data-target="#addDocumentDetail">
                                                                                    Large Banner Not Setup
                                                                                </button>
                                                                                <p className="mb-0 mt-2" style={{ fontsize: "14px" }}>Only the side banner to the right will be shown.</p>
                                                                            </div>

                                                                    }

                                                                </div>

                                                                <div className="col-md-4 text-center mt-lg-0 mt-md-0 mt-sm-4 mt-4">
                                                                    <NavLink className="edit_Campaign" to={`/add-banner/${item._id}`}>Edit Campaign</NavLink>
                                                                    {
                                                                        item.side_image ?
                                                                            <>
                                                                                <div className="NotSetup  text-center mt-3">
                                                                                    <img src={item.side_image} alt="Document Preview" />
                                                                                </div>
                                                                            </>
                                                                            : <div className="NotSetup Marquee text-center mt-3">

                                                                                <button type="button" className="btn btn-primary" data-toggle="modal" onClick={(e) => handleFileUploadSide(e, item._id)} data-target="#addBanner">
                                                                                    Not Setup
                                                                                </button>


                                                                                <p className="mb-0 mt-2">No side banner.</p>
                                                                            </div>
                                                                    }

                                                                </div>
                                                            </div>

                                                        </div>
                                                    );
                                                })
                                                :
                                                <div className="float-left w-100">
                                                <p className="float-left w-100 text-center text-white">No Banners Assigned</p>
                                                </div>
                                        }


                                    </div>



                                    <div className="modal in" role="dialog" id="addDocumentDetail">
                                        <div className="modal-dialog modal-dialog-centered modal-dialog-scrollable" role="document">
                                            <div className="modal-content">
                                            <div className="modal-header border-0 pb-0">
                                                {/* <h4 className="modal-title"></h4> */}
                                                <button className="btn-close" type="button" data-dismiss="modal" aria-label="Close"></button>
                                            </div>
                                            <div className="modal-body fs-sm pt-0">
                                                                <h6>Please select or place a file to upload.</h6>
                                                                <input className="form-control"
                                                                    id="inline-form-input"
                                                                    type="file"
                                                                    accept={acceptedFileTypes.map((type) => `.${type}`).join(',')}
                                                                    name="marque_image"
                                                                    onChange={handleFileUpload}
                                                                    value={formValues.marque_image}
                                                                />
                                                   
                                               
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div className="modal in" role="dialog" id="addBanner">
                                        <div className="modal-dialog modal-dialog-centered modal-dialog-scrollable" role="document">
                                            <div className="modal-content">
                                                <div className="modal-header border-0 pb-0">
                                                    {/* <h4 className="modal-title"></h4> */}
                                                    <button className="btn-close" type="button" data-dismiss="modal" aria-label="Close"></button>
                                                </div>
                                                <div className="modal-body fs-sm pt-0">
                                                       <h6>Please select or place a file to upload.</h6>
                                                                <input className="form-control"
                                                                    id="inline-form-input"
                                                                    type="file"
                                                                    accept={acceptedFileTypes.map((type) => `.${type}`).join(',')}
                                                                    name="marque_image"
                                                                    onChange={handleFileUploadSide}
                                                                    value={formValues.marque_image}
                                                                />
                                                    
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                     

                    </div>
                </div>
            </main>
        </div>
    )
}

export default Banners
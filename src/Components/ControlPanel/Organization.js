import React, { useState, useEffect, useRef } from "react";
import _ from 'lodash';
import user_service from '../service/user_service';
import jwt from "jwt-decode";
import Loader from "../../Pages/Loader/Loader.js";
import Toaster from "../../Pages/Toaster/Toaster.js";
import { useNavigate, NavLink, useParams } from "react-router-dom";
import Swal from "sweetalert2";
import "sweetalert2/dist/sweetalert2.css";

function Organization() {

    const [loader, setLoader] = useState({ isActive: null })
    const { isActive } = loader;
    const [toaster, setToaster] = useState({ types: null, isShow: null, toasterBody: "", message: "" });
    const { types, isShow, toasterBody, message } = toaster;

    const navigate = useNavigate();
    const [organizationGet, setOrganizationGet] = useState([])

    const OrganizationTreeGets = async () => {
        const agentId = jwt(localStorage.getItem('auth')).id
          setLoader({ isActive: true });
        await user_service.organizationTreeGet().then((response) => {
            setLoader({ isActive: false });
            if (response) {
                console.log(response);
                setOrganizationGet(response.data);
            }
        });
    };

    useEffect(() => { window.scrollTo(0, 0);
      
        OrganizationTreeGets();
    }, []);


    const handleRemove = async (id) => {
        Swal.fire({
          title: "Are you sure?",
          text: "It will be permanently deleted!",
          icon: "warning",
          showCancelButton: true,
          confirmButtonColor: "#3085d6",
          cancelButtonColor: "#d33",
          confirmButtonText: "Yes, delete it!",
        }).then(async (result) => {
          if (result.isConfirmed) {
            try {
              const response = await user_service.organizationTreeDelete(id);
              if (response) {
               
                    setLoader({ isActive: false });
                    if (response) {
                      Swal.fire(
                        "Deleted!",
                        "Your Office has been deleted.",
                        "success"
                      );
                      //setFormValues(response.data);
                    }
                    OrganizationTreeGets();
              }
            } catch (error) {
              Swal.fire(
                "Error",
                "An error occurred while deleting the file.",
                "error"
              );
              console.error(error);
            }
          }
        });
      };

    return (
        <div className="bg-secondary float-left w-100 pt-4 mb-4">
            <Loader isActive={isActive} />
            {isShow && <Toaster types={types} isShow={isShow} toasterBody={toasterBody} message={message} />}
            <main className="page-wrapper profile_page_wrap">
                <div className="content-overlay">
                    <div className="">
                        <div className="d-flex align-items-center justify-content-between mb-4">
                            <h3 className="text-white mb-0">Organization Tree</h3>
                            <NavLink to={`/control-panel/add-locations/`} className="btn btn-primary pull-right" type="button">Add A New Organization</NavLink>
                        </div>
                            <div className="bg-light rounded-3 p-3 border mb-3">
                                <div className="add_listing w-100 m-auto">
                                    <div>
                                        {organizationGet.data ?
                                            organizationGet.data.map((item) => (
                                                // console.log(item);
                                                <div className="row" key={item._id}>
                                                    <hr className="my-3" />
                                                    <div className="col-lg-6 col-md-6 col-sm-8 col-8 d-flex align-items-center justify-content-between">
                                                        <NavLink to={`/control-panel/add-locations/${item._id}`} className="mt-0"><p className="office_type mb-0">{item.name}</p></NavLink>&nbsp;<h6 className="pull-left mt-2 mb-2">{item.tag}</h6>
                                                        <p className="office_status pull-right mb-0">{item.type === "organization" ? "Organization" : "Office"}</p>
                                                    </div>
                                                    <div className="col-lg-6 col-md-6 col-sm-4 col-4 d-flex align-items-center justify-content-end">
                                                        <span><img
                                                            onClick={(e) => handleRemove(item._id)}
                                                            className="remove_icon pull-right"
                                                            src="https://cdn.pboffice.net/shared/transaction/page_select_drop.png"
                                                        /></span>
                                                    </div>
                                                    <p className="mb-0">{item.address}</p>
                                                </div>
                                            ))
                                            : ""
                                        }
                                    </div>

                                </div>                          
                        </div>
                    </div>
                </div>
            </main>
        </div>


    )
}
export default Organization;


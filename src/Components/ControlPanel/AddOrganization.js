import React, { useState, useEffect, useRef } from "react";
import _ from "lodash";
import user_service from "../service/user_service";
import jwt from "jwt-decode";
import Loader from "../../Pages/Loader/Loader.js";
import Toaster from "../../Pages/Toaster/Toaster.js";
import { useNavigate, NavLink, useParams } from "react-router-dom";

function AddOrganization() {
  const initialValues = {
    type: "organization",
    name: "",
    tag: "",
    code: "",
    address: "",
    phone: "",
    fax: "",
    email: "",
    web: "",
    brandName: "",
    legalName: "",
    licenseName: "",
    licenseNumber: "",
    taxID: "",
    naid: "",
  };
  const [formValues, setFormValues] = useState(initialValues);
  const [formErrors, setFormErrors] = useState({});
  const [isSubmitClick, setISSubmitClick] = useState(false);

  const [loader, setLoader] = useState({ isActive: null });
  const { isActive } = loader;
  const [toaster, setToaster] = useState({
    types: null,
    isShow: null,
    toasterBody: "",
    message: "",
  });
  const { types, isShow, toasterBody, message } = toaster;

  const navigate = useNavigate();
  const params = useParams();

  // onChange Function start
  const handleChange = (e) => {
    const { name, value } = e.target;
    setFormValues({ ...formValues, [name]: value });
  };
  // onChange Function end

  {
    /* <!-- Form Validation Start--> */
  }
  useEffect(() => {
   // window.scrollTo(0, 0);
    if (formValues && isSubmitClick) {
      validate();
    }
  }, [formValues]);

  const validate = () => {
    const values = formValues;
    const errors = {};

    if (!values.type) {
      errors.type = "Type is required.";
    }

    if (!values.name) {
      errors.name = "Name is required.";
    }

    if (!values.brandName) {
      errors.brandName = "BrandName is required.";
    }

    if (!values.legalName) {
      errors.legalName = "LegalName is required.";
    }

    setFormErrors(errors);
    return errors;
  };

  {
    /* <!-- Form Validation End--> */
  }

  const handleSubmit = async (e) => {
    if (params.id) {
      e.preventDefault();
      setISSubmitClick(true);
      let checkValue = validate();
      if (_.isEmpty(checkValue)) {
        const userData = {
          agentId: jwt(localStorage.getItem("auth")).id,
          type: formValues.type,

          name: formValues.name,

          tag: formValues.tag,
          code: formValues.code,

          address: formValues.address,

          phone: formValues.phone,
          fax: formValues.fax,

          email: formValues.email,

          web: formValues.web,
          brandName: formValues.brandName,

          legalName: formValues.legalName,

          licenseName: formValues.licenseName,
          licenseNumber: formValues.licenseNumber,

          taxID: formValues.taxID,

          naid: formValues.naid,
          is_active: "Yes",
        };
        console.log(userData);

        setLoader({ isActive: true });
        await user_service
          .organizationTreeUpdate(params.id, userData)
          .then((response) => {
            if (response) {
              console.log(response);
              setFormValues(response.data);
              setLoader({ isActive: false });
              setToaster({
                type: "Update OrganizationTree",
                isShow: true,
                toasterBody: response.data.message,
                message: "Update OrganizationTree Successfully",
              });
              setTimeout(() => {
                setToaster((prevToaster) => ({
                  ...prevToaster,
                  isShow: false,
                }));
                navigate("/control-panel/locations/");
              }, 1000);
            } else {
              setLoader({ isActive: false });
              setToaster({
                types: "error",
                isShow: true,
                toasterBody: response.data.message,
                message: "Error",
              });
            }
          });
      }
    } else {
      e.preventDefault();
      setISSubmitClick(true);
      let checkValue = validate();
      if (_.isEmpty(checkValue)) {
        const userData = {
          agentId: jwt(localStorage.getItem("auth")).id,
          type: formValues.type,

          name: formValues.name,

          tag: formValues.tag,
          code: formValues.code,

          address: formValues.address,

          phone: formValues.phone,
          fax: formValues.fax,

          email: formValues.email,

          web: formValues.web,
          brandName: formValues.brandName,

          legalName: formValues.legalName,

          licenseName: formValues.licenseName,
          licenseNumber: formValues.licenseNumber,

          taxID: formValues.taxID,

          naid: formValues.naid,
        };
        console.log(userData);

        setLoader({ isActive: true });
        await user_service.organizationTreePost(userData).then((response) => {
          if (response) {
            console.log(response);
            setFormValues(response.data);
            setLoader({ isActive: false });
            setToaster({
              type: "Add OrganizationTree",
              isShow: true,
              toasterBody: response.data.message,
              message: "Add OrganizationTree Successfully",
            });
            setTimeout(() => {
              setToaster((prevToaster) => ({ ...prevToaster, isShow: false }));
              navigate("/control-panel/locations/");
            }, 1000);
          } else {
            setLoader({ isActive: false });
            setToaster({
              types: "error",
              isShow: true,
              toasterBody: response.data.message,
              message: "Error",
            });
          }
        });
      }
    }
  };

  const handleBack = () => {
    navigate("/control-panel/locations/");
  };

  const OrganizationTreeGetId = async () => {
    await user_service.organizationTreeGetId(params.id).then((response) => {
      setLoader({ isActive: false });
      if (response) {
        const planData = response.data;
        setFormValues(planData);
      }
    });
  };
  useEffect(() => {
    window.scrollTo(0, 0);
    if (params.id) {
      setLoader({ isActive: true });
      OrganizationTreeGetId(params.id);
    }
  }, [params.id]);

  return (
    <div className="bg-secondary float-left w-100 pt-4 mb-4">
      <Loader isActive={isActive} />
      {isShow && (
        <Toaster
          types={types}
          isShow={isShow}
          toasterBody={toasterBody}
          message={message}
        />
      )}
      <main className="page-wrapper profile_page_wrap">
        <div className="content-overlay">
          <div className="d-flex align-items-center justify-content-start">
            <h3 className="mb-0 text-white">{formValues.name}</h3>
            </div>
            <div className="">
              {/* <hr className="mt-2" /> */}
                {/* <hr className="mt-5" /> */}
                <div className="add_listing w-100">
                <div className="d-flex align-items-center justify-content-start mb-4">
                <h3 className="mb-0 text-white">{formValues.type === "organization"
                    ? "Organization"
                    : "Office"}</h3>
                </div>
                <div className="bg-light rounded-3 border p-3 mb-3">
                   <div className="row">
                    <div className="col-md-6">
                      <div className="row">
                        <div className="col-sm-12">
                          <h6>General</h6>
                          <label className="form-label">Type</label>
                          {/* <input className="form-control" id="inline-form-input" type="text"
                                                        name="type"
                                                        value={formValues.type}
                                                        onChange={handleChange}
                                                    /> */}
                          <select
                            className="form-select"
                            name="type"
                            value={formValues.type}
                            onChange={handleChange}
                          >
                            <option value="organization">Organization</option>
                            <option value="office">Office</option>
                          </select>
                          {formErrors.type && (
                            <div className="invalid-tooltip">
                              {formErrors.type}
                            </div>
                          )}
                        </div>

                        <div className="col-sm-12 mt-3">
                          <label className="form-label">Name</label>
                          <input
                            className="form-control"
                            id="inline-form-input"
                            type="text"
                            name="name"
                            value={formValues.name}
                            onChange={handleChange}
                          />
                          {formErrors.name && (
                            <div className="invalid-tooltip">
                              {formErrors.name}
                            </div>
                          )}
                        </div>

                        <div className="col-sm-12 mt-3">
                          <label className="form-label">Tag</label>
                          <input
                            className="form-control"
                            id="inline-form-input"
                            type="text"
                            name="tag"
                            value={formValues.tag}
                            onChange={handleChange}
                          />
                        </div>

                        <div className="col-sm-12 mt-3">
                          <label className="form-label">Code</label>
                          <input
                            className="form-control"
                            id="inline-form-input"
                            type="text"
                            name="code"
                            value={formValues.code}
                            onChange={handleChange}
                          />
                        </div>
                      </div>

                      <div className="row mt-2">
                        <div className="col-sm-12 mt-3">
                          <h6>Contact</h6>
                          <label className="form-label">Address</label>
                          <input
                            className="form-control"
                            id="inline-form-input"
                            type="text"
                            name="address"
                            value={formValues.address}
                            onChange={handleChange}
                          />
                        </div>

                        <div className="col-sm-12 mt-3">
                          <label className="form-label">Phone</label>
                          <input
                            className="form-control"
                            id="inline-form-input"
                            type="text"
                            name="phone"
                            value={formValues.phone}
                            onChange={handleChange}
                          />
                        </div>

                        <div className="col-sm-12 mt-3">
                          <label className="form-label">Fax</label>
                          <input
                            className="form-control"
                            id="inline-form-input"
                            type="text"
                            name="fax"
                            value={formValues.fax}
                            onChange={handleChange}
                          />
                        </div>

                        <div className="col-sm-12 mt-3">
                          <label className="form-label">Email</label>
                          <input
                            className="form-control"
                            id="inline-form-input"
                            type="text"
                            name="email"
                            value={formValues.email}
                            onChange={handleChange}
                          />
                        </div>

                        <div className="col-sm-12 mt-3">
                          <label className="form-label">Web</label>
                          <input
                            className="form-control"
                            id="inline-form-input"
                            type="text"
                            name="web"
                            value={formValues.web}
                            onChange={handleChange}
                          />
                        </div>
                      </div>
                    </div>

                  <div className="col-md-6 mt-lg-0 mt-md-0 mt-sm-4 mt-4">
                      <div className="row">
                        <div className="col-sm-12">
                          <h6>Brand Info</h6>
                          <label className="form-label">BrandName</label>
                          <input
                            className="form-control"
                            id="inline-form-input"
                            type="text"
                            name="brandName"
                            value={formValues.brandName}
                            onChange={handleChange}
                          />
                          {formErrors.brandName && (
                            <div className="invalid-tooltip">
                              {formErrors.brandName}
                            </div>
                          )}
                        </div>

                        <div className="col-sm-12 mt-3">
                          <label className="form-label">LegalName</label>
                          <input
                            className="form-control"
                            id="inline-form-input"
                            type="text"
                            name="legalName"
                            value={formValues.legalName}
                            onChange={handleChange}
                          />
                          {formErrors.legalName && (
                            <div className="invalid-tooltip">
                              {formErrors.legalName}
                            </div>
                          )}
                        </div>
                      </div>

                      <div className="row mt-3">
                        <div className="col-sm-12 mt-3">
                          <h6>Business Info</h6>
                          <label className="form-label">LicenseName</label>
                          <input
                            className="form-control"
                            id="inline-form-input"
                            type="text"
                            name="licenseName"
                            value={formValues.licenseName}
                            onChange={handleChange}
                          />
                        </div>

                        <div className="col-sm-12 mt-3">
                          <label className="form-label">LicenseNumber</label>
                          <input
                            className="form-control"
                            id="inline-form-input"
                            type="text"
                            name="licenseNumber"
                            value={formValues.licenseNumber}
                            onChange={handleChange}
                          />
                        </div>

                        <div className="col-sm-12 mt-3">
                          <label className="form-label">TaxID</label>
                          <input
                            className="form-control"
                            id="inline-form-input"
                            type="text"
                            name="taxID"
                            value={formValues.taxID}
                            onChange={handleChange}
                          />
                        </div>

                        <div className="col-sm-12 mt-3">
                          <label className="form-label">NAID #</label>
                          <input
                            className="form-control"
                            id="inline-form-input"
                            type="text"
                            name="naid"
                            value={formValues.naid}
                            onChange={handleChange}
                          />
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            <div className="pull-right mt-3">
              <button className="btn btn-secondary pull-right ms-3"
                type="button"
                onClick={handleBack}>
                Cancel
              </button>
              <button className="btn btn-primary pull-right"
                type="button"
                onClick={handleSubmit}>
                {params.id ? "Update Organization" : "Add Organization"}
              </button>
             
           </div>
          </div>
        </div>
      </main>
    </div>
  );
}
export default AddOrganization;

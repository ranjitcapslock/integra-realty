import React, { useState, useEffect } from "react";
import { NavLink } from "react-router-dom";
import user_service from "../service/user_service";
import Loader from "../../Pages/Loader/Loader.js";
import Toaster from "../../Pages/Toaster/Toaster.js";
import ImageSlider from "react-image-gallery";
import "react-image-gallery/styles/css/image-gallery.css";
import jwt from "jwt-decode";
import Slider from "react-slick";
import "slick-carousel/slick/slick-theme.css";
import "slick-carousel/slick/slick.css";
import Pic from "../img/pic.png";
import Birthday from "../img/birthday.jpg";
// import { Calendar as BigCalendar, momentLocalizer } from "react-big-calendar";
import moment from "moment-timezone";
// import "react-big-calendar/lib/css/react-big-calendar.css";
import { useNavigate } from "react-router-dom";
import VendorImage from "../img/venders.png?egrewrygewr"

// const localizer = momentLocalizer(moment);


const images = [
  {
    original: "https://cache.workspace.lwolf.com/shared/marketplace/images/thumb2.jpg",
  },
  {
    original: "https://cache.workspace.lwolf.com/shared/marketplace/images/thumb1.jpg",
  },
];

const settings = {
  dots: true,
  infinite: true,
  speed: 500,
  slidesToShow: 1,
  slidesToScroll: 1,
  autoplay: true,
  autoplaySpeed: 7000,
  arrows: false,
};
const Vender = () => {
  const [calenderGet, setCalenderGet] = useState([]);


  const [loader, setLoader] = useState({ isActive: null });
  const { isActive } = loader;
  const [toaster, setToaster] = useState({
    types: null,
    isShow: null,
    toasterBody: "",
    message: "",
  });
  const { types, isShow, toasterBody, message } = toaster;
  const [products, setProducts] = useState([]);
  const navigate = useNavigate();

  
  const PromotoProductsGet = async () => {
      await user_service.PromotoProductsGet(localStorage.getItem("active_office")).then((response) => {
        setLoader({ isActive: false });
        if (response) {
          const groupedBycategory = response.data.data.reduce((acc, item) => {
            const category = item.category;
            if (!acc[category]) {
              acc[category] = [];
            }
            acc[category].push(item);
            return acc;
          }, {});
  
          setProducts(groupedBycategory);
        }
      });
    
                
  };

  useEffect(() => { window.scrollTo(0, 0); window.scrollTo(0, 0);
    setLoader({ isActive: true });
    PromotoProductsGet();
  }, []);

  const [currentDate, setCurrentDate] = useState("");
  useEffect(() => { window.scrollTo(0, 0);
    const interval = setInterval(() => {
      const options = {  dateStyle: "full" };
      setCurrentDate(new Date().toLocaleDateString("en-US", options));
    }, 1000);

    return () => {
      clearInterval(interval);
    };
  }, []);

  function populateSection(sectionId, items) {
   
    if (!items || items.length === 0) {
      return (
        <div className="NoItemProducts float-left w-100 text-center mt-4">
             <img src={VendorImage} />
             <p className="text-white">There's no data to show</p>
           </div>
      );
    }

    if (!items || items.length === 0) {
      return (
        <div className="NoItemProducts">
          <img src={VendorImage} />
          <p className="text-white">There's no data to show</p>
        </div>
      );
    }

    return (
      <div className="FeaturedBox" key={sectionId}>
         <div className="row">{items.map((item) => generateItemHTML(item))}</div> 
      </div>
    );
  }
  function generateItemHTML(item) {
    return (
      <div className="col-lg-3 col-md-6 col-sm-6 col-12 tns-item tns-slide-cloned tns-slide-active p-3" key={item._id}>
        <div className="card shadow-sm cursor-pointer card-hover border-0 h-100">
          <div className="card-img-top card-img-hover">
          <NavLink to={`/sub-vender/${item._id}`}>
              <img className="img-fluid rounded-0 w-100" src={item.image} alt="images" />
          </NavLink>
          </div>
          <div className="card-body position-relative text-center pb-3">
          <NavLink>{item.title}</NavLink>
          </div>
          <div className="read-more float-left w-100 text-center mb-3">
            <NavLink to={`/sub-vender/${item._id}`} className="">View Details</NavLink>
          </div>
        </div>
      </div>
    );
  }

  const increaseclick = async (banner_id) => {
    await user_service.increaseclickbanner(banner_id).then((response) => {
      if (response) {

      }
    })
  }


  const [upcomingbirthday, setUpcomingbirthday] = useState("");
  const [newassociates, setNewassociates] = useState("");


 

  // Helper function to format date as "Day, Month Day" (e.g., "Friday, September 21")
  function getFormattedDate(dateString) {
    const options = { weekday: "long", month: "long", day: "numeric" };
    const date = new Date(dateString);
    return date.toLocaleDateString("en-US", options);
  }

 


    const initialValues = {
      allDayEvent: "yes", category: "", draft: "", endDate: "", guestSeats: "", information: "", publicEvent: "",
      registration: "", startDate: "", title: "",
    }
    const [formErrors, setFormErrors] = useState({});
    const [formValues, setFormValues] = useState(initialValues);
    const [selectedDate, setSelectedDate] = useState(null);
    
  
  {/* <!-- Input onChange Start--> */ }
  const handleChange = (e) => {
    const { name, value } = e.target;
    setFormValues({ ...formValues, [name]: value });
  };

    {/* <!-- Form onSubmit Start--> */ }
    const handleSubmit = async (e, slotInfo) => {
      e.preventDefault();
  
      if (slotInfo) {
        const startDate = moment(slotInfo.start).format("YYYY-MM-DD");
        const endDate = moment(slotInfo.start).format("YYYY-MM-DD");
   
        const userData = {
          office_id: localStorage.getItem("active_office_id"),
          office_name: localStorage.getItem("active_office"),
          category: formValues.category,
          allDayEvent: "yes",
          title: formValues.title,
          information: "information",
          registration: "registration",
          draft: "draft",
          agentId: jwt(localStorage.getItem("auth")).id,
          startDate: startDate,
          endDate: endDate,
          guestSeats: "guestSeats",
          publicEvent: "publicEvent"
        };
  
        try {
          setLoader({ isActive: true });
          const response = await user_service.CalenderPost(userData);
          if (response && response.data) {
            setLoader({ isActive: false });
            setToaster({
              types: "Calender",
              isShow: true,
              toasterBody: response.data.message,
              message: "Event Post Successfully",
            });
            setTimeout(() => {
              setToaster((prevToaster) => ({ ...prevToaster, isShow: false }));
            }, 1000);
  
            fetchCalendar();
            document.getElementById("close").click();
          } else {
            setLoader({ isActive: false });
            setToaster({
              types: "error",
              isShow: true,
              toasterBody: response.data.message,
              message: "Error",
            });
          }
        } catch (error) {
          setLoader({ isActive: false });
          setToaster({
            types: "error",
            isShow: true,
            toasterBody: error.message,
            message: "Error",
          });
        }
        setTimeout(() => {
          setToaster({
            types: "error",
            isShow: false,
            toasterBody: null,
            message: "Error",
          });
        }, 1000);
      }
    };
  
    {/* <!-- Form onSubmit End--> */ }
  
    
    const [search_home, setSearch_home] = useState("");
    const [universaldata, setUniversaldata] = useState([]);
    const search_universal = async () =>{
      console.log(search_home)
     // setIsLoading(true);
       
        await user_service.Searchuiversalfilter(search_home).then((response) => {
            //setIsLoading(false);
            if (response) {
                console.log(response.data)
                setUniversaldata(response.data);
            }
        });
    }

   
  const generateEvents = () => {
    const generatedEvents = [];

    if (calenderGet && calenderGet.data) {
      calenderGet.data.forEach((date) => {
        let startDate;
        let endDate;

        if (date.startDate.includes("/")) {
          startDate = moment.tz(date.startDate, "M/D/YYYY", "America/Denver").toDate();
          endDate = moment.tz(date.endDate, "M/D/YYYY", "America/Denver").toDate();
        } else {
          startDate = moment.tz(date.startDate, "America/Denver").toDate();
          endDate = moment.tz(date.endDate, "America/Denver").toDate();
        }

        const eventId = date._id;

        generatedEvents.push({
          title: date.title,
          start: startDate,
          end: endDate,
          type: "meeting",
          id: eventId,
        });
      });
    }
    return generatedEvents;
  };

  const homeside = async () => {
    await user_service.homeside().then((response) => {
      if (response) {
        // console.log(response.data)
        setUpcomingbirthday(response.data.data);
      }
    });
  };


    const homesidenewassociates = async () => {
      await user_service.newassociates().then((response) => {
        if (response) {
          // console.log(response.data);
          setNewassociates(response.data.data);
        }
      });
    };

    const fetchCalendar = async () => {
      setLoader({ isActive: true });
      await user_service.CalenderGet().then((response) => {
        setLoader({ isActive: false });
        if (response) {
          setCalenderGet(response.data);
        }
      });
    };

    useEffect(() => { window.scrollTo(0, 0);
      homeside();
      homesidenewassociates();
      fetchCalendar();
    }, []);
    
    const [events, setEvents] = useState([]);

    useEffect(() => { window.scrollTo(0, 0);
      const generatedEvents = generateEvents();
      // console.log(generatedEvents);
      setEvents(generatedEvents);
    }, [calenderGet]);
  
    const eventColors = {
      meeting: "#FF5733",
      duty: "#3366FF",
    };
    const eventStyleGetter = (event) => {
      const backgroundColor = eventColors[event.type];
      return {
        style: {
          backgroundColor,
        },
      };
    };
    const CalenderPage = () => {
      navigate("/calendar");
    };

    const handleHome = () => {
      navigate("/");
    };
    const handleMyTransaction = () => {
      navigate("/transaction");
    };
    const handleContact = () => {
      navigate("/contact");
    };
    const handleListing = () => {
      navigate("/listing");
    };
  
    const addRules = () => {
      navigate("/add-document-rules");
    };
    const addDetails = () => {
      navigate("/t-details");
    };
    const addLearn = () => {
      navigate("/add-learn");
    };
  
    const logoImage = () => {
      navigate("/");
    };
  
    const handletasks = () => {
      navigate("/tasks");
    };
  
    const handlestore = () => {
      navigate("/promote");
    };
    const handleDocs = () => {
      navigate("/control-panel/documents");
    };
  
    const handTraining = () => {
      navigate("/learn");
    };
  return (
    <div className="bg-secondary float-left w-100 pt-4 mb-4">
      <Loader isActive={isActive} />
      {isShow && (
        <Toaster
          types={types}
          isShow={isShow}
          toasterBody={toasterBody}
          message={message}
        />
      )}
    <div className="">  
        <main className="page-wrapper homepage_layout commonpage_layout">
        <div className="">
          <div className="">
                  <div className="d-flex align-items-center justify-content-between mb-4">
                    <h3 className="pull-left mb-0 text-white">Catalog</h3>
                    {localStorage.getItem("auth") &&
                    jwt(localStorage.getItem("auth")).contactType == "admin" ? (
                      <NavLink
                        className="btn btn-info mb-2 pull-right"
                        to="/add-promotecatalog"
                      >
                        Add New Catalog
                      </NavLink>
                    ) : (
                      ""
                    )}
                    </div>
                    <div>
                    <hr className="mb-3 w-100" />
                    <div className="float-left w-100 mt-4" style={{ paddingleft: "2rem" }}>
                      <div id="GalleryGrid">
                        {/* <h5>Products</h5> */}
                        {/* {populateSection("ProductsSection", products.product)} */}
                        {/* <p>&nbsp;</p> */}
                    <h6 className="text-white">Vendors</h6>
                     {populateSection("VendorsSection", products.vendor)} 
                        <p>&nbsp;</p>
                      </div>
                    </div>
                  </div>
                
          </div>
        </div>

        <div className="modal" role="dialog" id="modal-show-calender">
          <div className="modal-dialog" role="document">
            <div className="modal-content">
              <div className="modal-body row">
                <div className="col-md-12 mt-5">
                  <h6>Add an Event</h6>

                  <div className="mb-3">
                    <label className="form-label" for="pr-city">Event<span className='text-danger'>*</span></label>
                    <input className="form-control" id="inline-form-input" type="text" placeholder="Title"
                      name="title"
                      onChange={handleChange}
                      value={formValues.title}
                      style={{
                        border: formErrors?.title ? '1px solid red' : ''
                      }}
                    />
                    <div className="invalid-tooltip">{formErrors.title}</div>
                  </div>

                  <div className="mb-3">
                    <label className="form-label" for="pr-city">Category</label>
                    <select className="form-select" id="pr-city"
                      name="category"
                      onChange={handleChange}
                      value={formValues.category}
                      style={{
                        border: formErrors?.category ? '1px solid red' : ''
                      }}
                    >
                      <option></option>
                      <option>Office</option>
                      <option>Personal</option>
                      <option>Staff</option>
                      <option>Training</option>
                      <option>Community</option>
                      <option>Courses</option>

                    </select>
                    <div className="invalid-tooltip">{formErrors.category}</div>

                  </div>
                  <button type="button" className="btn btn-default btn btn-secondary btn-sm" id="close" data-bs-dismiss="modal">Cancel & Close</button>
                  <button onClick={(e) => handleSubmit(e, selectedDate)} type="button" className="btn btn-primary btn-sm ms-3" id="save-button">Add Event</button>
                </div>

              </div>


            </div>
          </div>
        </div>

      </main>
     
    </div>
    </div>
  );
};

export default Vender;

import React, { useState, useEffect } from "react";
// import AddDocuments from './AddDocuments';
import { NavLink, useNavigate, useParams } from "react-router-dom";
import user_service from "../service/user_service";
import Loader from "../../Pages/Loader/Loader.js";
import Toaster from "../../Pages/Toaster/Toaster";

import RuleIcon from "../img/book_3251560.png";
import staffRuleIcon from "../img/rules_17852915.png";

import jwt from "jwt-decode";
import FeatureIcon from "../img/new-features_11230561.png";

const ControlPanel = () => {
  const navigate = useNavigate();
  const [loader, setLoader] = useState({ isActive: null });
  const { isActive } = loader;
  const [toaster, setToaster] = useState({
    types: null,
    isShow: null,
    toasterBody: "",
    message: "",
  });
  const { types, isShow, toasterBody, message } = toaster;

  const [formData, setFormData] = useState([]);

  const StaffRoleGetData = async () => {
    await user_service.StaffRoleGet().then((response) => {
      if (response) {
        console.log(response);
        setFormData(response.data.data);
      }
    });
  };
  useEffect(() => {
    if (formData) {
      StaffRoleGetData();
    }
  }, []);
  return (
    <div className="bg-secondary float-left w-100 pt-4 mb-4">
      <Loader isActive={isActive} />
      {isShow && (
        <Toaster
          types={types}
          isShow={isShow}
          toasterBody={toasterBody}
          message={message}
        />
      )}
      <main className="page-wrapper">
        <div className="mb-md-4">
          <div className="">
            <div className="row">
              <div className="callout accent">
                <h3 className="text-white">Office Tools</h3>
                <hr className="mb-3" />
                <div className="row row-cols-lg-6 row-cols-sm-3 row-cols-2 g-3 g-xl-4 mt-3">
                  {localStorage.getItem("auth") &&
                  jwt(localStorage.getItem("auth")).contactType == "admin" ? (
                    <div className="col">
                      <NavLink
                        className="icon-box card card-body h-100 border-0 shadow-sm card-hover h-100 text-center"
                        to="/calendar"
                      >
                        <div className="icon-box-media bg-faded-primary text-primary rounded-circle mb-3 mx-auto">
                          {/* <i className="fi-real-estate-house"></i> */}
                          <i className="fa fa-calendar" aria-hidden="true"></i>
                        </div>
                        <h3 className="icon-box-title fs-base mb-0">
                          Calendar Event Registration
                        </h3>
                      </NavLink>
                    </div>
                  ) : (
                    <>
                      {localStorage.getItem("auth") &&
                        jwt(localStorage.getItem("auth")).contactType ==
                          "staff" &&
                        (Array.isArray(formData) && formData.length > 0
                          ? formData.map((item) =>
                              item.roleStaff === "calender" ? (
                                <div className="col">
                                  <NavLink
                                    className="icon-box card card-body h-100 border-0 shadow-sm card-hover h-100 text-center"
                                    to="/calendar"
                                  >
                                    <div className="icon-box-media bg-faded-primary text-primary rounded-circle mb-3 mx-auto">
                                      {/* <i className="fi-real-estate-house"></i> */}
                                      <i
                                        className="fa fa-calendar"
                                        aria-hidden="true"
                                      ></i>
                                    </div>
                                    <h3 className="icon-box-title fs-base mb-0">
                                      Calendar Event Registration
                                    </h3>
                                  </NavLink>
                                </div>
                              ) : (
                                ""
                              )
                            )
                          : "")}
                    </>
                  )}

                  {localStorage.getItem("auth") &&
                  jwt(localStorage.getItem("auth")).contactType == "admin" ? (
                    <div className="col">
                      <NavLink
                        className="icon-box card card-body h-100 border-0 shadow-sm card-hover h-100 text-center"
                        to="/control-panel/documents"
                      >
                        <div className="icon-box-media bg-faded-primary text-primary rounded-circle mb-3 mx-auto">
                          <i className="fa fa-book" aria-hidden="true"></i>
                        </div>
                        <h3 className="icon-box-title fs-base mb-0">
                          Shared Docs
                        </h3>
                      </NavLink>
                    </div>
                  ) : (
                    <>
                      {localStorage.getItem("auth") &&
                        jwt(localStorage.getItem("auth")).contactType ==
                          "staff" &&
                        (Array.isArray(formData) && formData.length > 0
                          ? formData.map((item) =>
                              item.roleStaff === "calender" ? (
                                <div className="col">
                                  <NavLink
                                    className="icon-box card card-body h-100 border-0 shadow-sm card-hover h-100 text-center"
                                    to="/control-panel/documents"
                                  >
                                    <div className="icon-box-media bg-faded-primary text-primary rounded-circle mb-3 mx-auto">
                                      <i
                                        className="fa fa-book"
                                        aria-hidden="true"
                                      ></i>
                                    </div>
                                    <h3 className="icon-box-title fs-base mb-0">
                                      Shared Docs
                                    </h3>
                                  </NavLink>
                                </div>
                              ) : (
                                ""
                              )
                            )
                          : "")}
                    </>
                  )}

                  {localStorage.getItem("auth") &&
                  jwt(localStorage.getItem("auth")).contactType == "admin" ? (
                    <div className="col">
                      <NavLink
                        className="icon-box card card-body h-100 border-0 shadow-sm card-hover h-100 text-center"
                        to="/new-post"
                      >
                        <div className="icon-box-media bg-faded-primary text-primary rounded-circle mb-3 mx-auto">
                          <i className="fa fa-rss" aria-hidden="true"></i>
                        </div>
                        <h3 className="icon-box-title fs-base mb-0">
                          Add Posts
                        </h3>
                      </NavLink>
                    </div>
                  ) : (
                    <>
                      {localStorage.getItem("auth") &&
                        jwt(localStorage.getItem("auth")).contactType ==
                          "staff" &&
                        (Array.isArray(formData) && formData.length > 0
                          ? formData.map((item) =>
                              item.roleStaff === "calender" ? (
                                <div className="col">
                                  <NavLink
                                    className="icon-box card card-body h-100 border-0 shadow-sm card-hover h-100 text-center"
                                    to="/new-post"
                                  >
                                    <div className="icon-box-media bg-faded-primary text-primary rounded-circle mb-3 mx-auto">
                                      <i
                                        className="fa fa-rss"
                                        aria-hidden="true"
                                      ></i>
                                    </div>
                                    <h3 className="icon-box-title fs-base mb-0">
                                      Add Posts
                                    </h3>
                                  </NavLink>
                                </div>
                              ) : (
                                ""
                              )
                            )
                          : "")}
                    </>
                  )}

                  <div className="col">
                    <NavLink
                      className="icon-box card card-body h-100 border-0 shadow-sm card-hover h-100 text-center"
                      to="/control-panel/desk/"
                    >
                      <div className="icon-box-media bg-faded-primary text-primary rounded-circle mb-3 mx-auto">
                        <i className="fa fa-building" aria-hidden="true"></i>
                      </div>
                      <h3 className="icon-box-title fs-base mb-0">
                        Office Networking
                      </h3>
                    </NavLink>
                  </div>

                  <div className="col">
                    <NavLink
                      className="icon-box card card-body h-100 border-0 shadow-sm card-hover h-100 text-center"
                      to="/connect/reservations/"
                    >
                      <div className="icon-box-media bg-faded-primary text-primary rounded-circle mb-3 mx-auto">
                        <i className="fa fa-calendar" aria-hidden="true"></i>
                      </div>
                      <h3 className="icon-box-title fs-base mb-0">
                        Reservation Calendars
                      </h3>
                    </NavLink>
                  </div>

                  <div className="col">
                    <NavLink
                      className="icon-box card card-body h-100 border-0 shadow-sm card-hover h-100 text-center"
                      to="/control-panel/timezone/"
                    >
                      <div className="icon-box-media bg-faded-primary text-primary rounded-circle mb-3 mx-auto">
                        <i className="fa fa-clock-o" aria-hidden="true"></i>
                      </div>
                      <h3 className="icon-box-title fs-base mb-0">
                        Timezone Setting
                      </h3>
                    </NavLink>
                  </div>

                  {localStorage.getItem("auth") &&
                  jwt(localStorage.getItem("auth")).contactType == "admin" ? (
                    <div className="col">
                      <NavLink
                        className="icon-box card card-body h-100 border-0 shadow-sm card-hover h-100 text-center"
                        to="/news-content"
                      >
                        <div className="icon-box-media bg-faded-primary text-primary rounded-circle mb-3 mx-auto">
                          <i class="fa fa-newspaper-o" aria-hidden="true"></i>
                        </div>
                        <h3 className="icon-box-title fs-base mb-0">
                          Add News Letter Content
                        </h3>
                      </NavLink>
                    </div>
                  ) : (
                    ""
                  )}

                  {/* {localStorage.getItem("auth") &&
                  jwt(localStorage.getItem("auth")).contactType == "admin" ? (
                    <div className="col">
                      <NavLink
                        className="icon-box card card-body h-100 border-0 shadow-sm card-hover h-100 text-center"
                        to="/marketing-content"
                      >
                        <div className="icon-box-media bg-faded-primary text-primary rounded-circle mb-3 mx-auto">
                          <i class="fa fa-newspaper-o" aria-hidden="true"></i>
                        </div>
                        <h3 className="icon-box-title fs-base mb-0">
                          Add Marketing Calender Content
                        </h3>
                      </NavLink>
                    </div>
                  ) : (
                    ""
                  )} */}

                  {localStorage.getItem("auth") &&
                  jwt(localStorage.getItem("auth")).contactType == "admin" ? (
                    <div className="col">
                      <NavLink
                        className="icon-box card card-body h-100 border-0 shadow-sm card-hover h-100 text-center"
                        to="/marketing-content-listing"
                      >
                        <div className="icon-box-media bg-faded-primary text-primary rounded-circle mb-3 mx-auto">
                          <i class="fa fa-newspaper-o" aria-hidden="true"></i>
                        </div>
                        <h3 className="icon-box-title fs-base mb-0">
                          Marketing Calender Content Listing
                        </h3>
                      </NavLink>
                    </div>
                  ) : (
                    ""
                  )}

                  {localStorage.getItem("auth") &&
                  jwt(localStorage.getItem("auth")).contactType == "admin" ? (
                    <div className="col">
                      <NavLink
                        className="icon-box card card-body h-100 border-0 shadow-sm card-hover h-100 text-center"
                        to="/flyer-content"
                      >
                        <div className="icon-box-media bg-faded-primary text-primary rounded-circle mb-3 mx-auto">
                          <i class="fa fa-newspaper-o" aria-hidden="true"></i>
                        </div>
                        <h3 className="icon-box-title fs-base mb-0">
                          Add Flyer Page Content
                        </h3>
                      </NavLink>
                    </div>
                  ) : (
                    ""
                  )}

                  {localStorage.getItem("auth") &&
                  jwt(localStorage.getItem("auth")).contactType == "admin" ? (
                    <div className="col">
                      <NavLink
                        className="icon-box card card-body h-100 border-0 shadow-sm card-hover h-100 text-center"
                        to="/new-Feature"
                      >
                        <div className="icon-box-media bg-faded-primary text-primary rounded-circle mb-3 mx-auto">
                          <img src={FeatureIcon} />
                        </div>
                        <h3 className="icon-box-title fs-base mb-0">
                          Add New Features Release List
                        </h3>
                      </NavLink>
                    </div>
                  ) : (
                    ""
                  )}
                </div>

                <h3 className="mt-5 text-white">Marketing Tools</h3>
                <hr className="mb-3" />
                <div className="row row-cols-lg-6 row-cols-sm-3 row-cols-2 g-3 g-xl-4 mt-3">
                  {localStorage.getItem("auth") &&
                  jwt(localStorage.getItem("auth")).contactType == "admin" ? (
                    <>
                      <div className="col">
                        <NavLink
                          className="icon-box card card-body h-100 border-0 shadow-sm card-hover h-100 text-center"
                          to="/admin/invitecontacts/"
                        >
                          <div className="icon-box-media bg-faded-primary text-primary rounded-circle mb-3 mx-auto">
                            <i
                              className="fa fa-paper-plane"
                              aria-hidden="true"
                            ></i>
                          </div>
                          <h3 className="icon-box-title fs-base mb-0">
                            Invite
                          </h3>
                        </NavLink>
                      </div>

                      <div className="col">
                        <NavLink
                          className="icon-box card card-body h-100 border-0 shadow-sm card-hover h-100 text-center"
                          to="/admin/announce"
                        >
                          <div className="icon-box-media bg-faded-primary text-primary rounded-circle mb-3 mx-auto">
                            <i
                              className="fa fa-envelope-o"
                              aria-hidden="true"
                            ></i>
                          </div>
                          <h3 className="icon-box-title fs-base mb-0">
                            Compose Mail
                          </h3>
                        </NavLink>
                      </div>
                    </>
                  ) : (
                    ""
                  )}

                  {localStorage.getItem("auth") &&
                  jwt(localStorage.getItem("auth")).contactType == "admin" ? (
                    <div className="col">
                      <NavLink
                        className="icon-box card card-body h-100 border-0 shadow-sm card-hover h-100 text-center"
                        to="/control-panel/banners/"
                      >
                        <div className="icon-box-media bg-faded-primary text-primary rounded-circle mb-3 mx-auto">
                          <i className="fa fa-picture-o" aria-hidden="true"></i>
                        </div>
                        <h3 className="icon-box-title fs-base mb-0">Banners</h3>
                      </NavLink>
                    </div>
                  ) : (
                    <>
                      {localStorage.getItem("auth") &&
                        jwt(localStorage.getItem("auth")).contactType ==
                          "staff" &&
                        (Array.isArray(formData) && formData.length > 0
                          ? formData.map((item) =>
                              item.roleStaff === "calender" ? (
                                <div className="col">
                                  <NavLink
                                    className="icon-box card card-body h-100 border-0 shadow-sm card-hover h-100 text-center"
                                    to="/control-panel/banners/"
                                  >
                                    <div className="icon-box-media bg-faded-primary text-primary rounded-circle mb-3 mx-auto">
                                      <i
                                        className="fa fa-picture-o"
                                        aria-hidden="true"
                                      ></i>
                                    </div>
                                    <h3 className="icon-box-title fs-base mb-0">
                                      Banners
                                    </h3>
                                  </NavLink>
                                </div>
                              ) : (
                                ""
                              )
                            )
                          : "")}
                    </>
                  )}

                  {localStorage.getItem("auth") &&
                  jwt(localStorage.getItem("auth")).contactType == "admin" ? (
                    <div className="col">
                      <NavLink
                        className="icon-box card card-body h-100 border-0 shadow-sm card-hover h-100 text-center"
                        to="/admin/promote"
                      >
                        <div className="icon-box-media bg-faded-primary text-primary rounded-circle mb-3 mx-auto">
                          <i className="fa fa-volume-up" aria-hidden="true"></i>
                        </div>
                        <h3 className="icon-box-title fs-base mb-0">
                          Promote Products/Vendors
                        </h3>
                      </NavLink>
                    </div>
                  ) : (
                    <>
                      {localStorage.getItem("auth") &&
                        jwt(localStorage.getItem("auth")).contactType ==
                          "staff" &&
                        (Array.isArray(formData) && formData.length > 0
                          ? formData.map((item) =>
                              item.roleStaff === "products_vendor" ? (
                                <div className="col">
                                  <NavLink
                                    className="icon-box card card-body h-100 border-0 shadow-sm card-hover h-100 text-center"
                                    to="/admin/promote"
                                  >
                                    <div className="icon-box-media bg-faded-primary text-primary rounded-circle mb-3 mx-auto">
                                      <i
                                        className="fa fa-volume-up"
                                        aria-hidden="true"
                                      ></i>
                                    </div>
                                    <h3 className="icon-box-title fs-base mb-0">
                                      Promote Products/Vendors
                                    </h3>
                                  </NavLink>
                                </div>
                              ) : (
                                ""
                              )
                            )
                          : "")}
                    </>
                  )}

                  {localStorage.getItem("auth") &&
                  jwt(localStorage.getItem("auth")).contactType == "admin" ? (
                    <div className="col">
                      <NavLink
                        className="icon-box card card-body h-100 border-0 shadow-sm card-hover h-100 text-center"
                        to="/add-linkfiles/"
                      >
                        <div className="icon-box-media bg-faded-primary text-primary rounded-circle mb-3 mx-auto">
                          <i className="fa fa-link" aria-hidden="true"></i>
                        </div>
                        <h3 className="icon-box-title fs-base mb-0">
                          Add Dashboard Links
                        </h3>
                      </NavLink>
                    </div>
                  ) : (
                    <>
                      {localStorage.getItem("auth") &&
                        jwt(localStorage.getItem("auth")).contactType ==
                          "staff" &&
                        (Array.isArray(formData) && formData.length > 0
                          ? formData.map((item) =>
                              item.roleStaff === "products_vendor" ? (
                                <div className="col">
                                  <NavLink
                                    className="icon-box card card-body h-100 border-0 shadow-sm card-hover h-100 text-center"
                                    to="/add-linkfiles/"
                                  >
                                    <div className="icon-box-media bg-faded-primary text-primary rounded-circle mb-3 mx-auto">
                                      <i
                                        className="fa fa-link"
                                        aria-hidden="true"
                                      ></i>
                                    </div>
                                    <h3 className="icon-box-title fs-base mb-0">
                                      Add Dashboard Links
                                    </h3>
                                  </NavLink>
                                </div>
                              ) : (
                                ""
                              )
                            )
                          : "")}
                    </>
                  )}

                  {localStorage.getItem("auth") &&
                  jwt(localStorage.getItem("auth")).contactType == "admin" ? (
                    <div className="col">
                      <NavLink
                        className="icon-box card card-body h-100 border-0 shadow-sm card-hover h-100 text-center"
                        to="/add-question/"
                      >
                        <div className="icon-box-media bg-faded-primary text-primary rounded-circle mb-3 mx-auto">
                          <i
                            className="fa fa-question-circle"
                            aria-hidden="true"
                          ></i>
                        </div>
                        <h3 className="icon-box-title fs-base mb-0">
                          Add Question
                        </h3>
                      </NavLink>
                    </div>
                  ) : (
                    <>
                      {localStorage.getItem("auth") &&
                        jwt(localStorage.getItem("auth")).contactType ==
                          "staff" &&
                        (Array.isArray(formData) && formData.length > 0
                          ? formData.map((item) =>
                              item.roleStaff === "products_vendor" ? (
                                <div className="col">
                                  <NavLink
                                    className="icon-box card card-body h-100 border-0 shadow-sm card-hover h-100 text-center"
                                    to="/add-question/"
                                  >
                                    <div className="icon-box-media bg-faded-primary text-primary rounded-circle mb-3 mx-auto">
                                      <i
                                        className="fa fa-question-circle"
                                        aria-hidden="true"
                                      ></i>
                                    </div>
                                    <h3 className="icon-box-title fs-base mb-0">
                                      Add Question
                                    </h3>
                                  </NavLink>
                                </div>
                              ) : (
                                ""
                              )
                            )
                          : "")}
                    </>
                  )}

                  {localStorage.getItem("auth") &&
                  jwt(localStorage.getItem("auth")).contactType == "admin" ? (
                    <div className="col">
                      <NavLink
                        className="icon-box card card-body h-100 border-0 shadow-sm card-hover h-100 text-center"
                        to="/add-utility/"
                      >
                        <div className="icon-box-media bg-faded-primary text-primary rounded-circle mb-3 mx-auto">
                          <i class="fa fa-map-marker" aria-hidden="true"></i>
                        </div>
                        <h3 className="icon-box-title fs-base mb-0">
                          Add Utility Tool
                        </h3>
                      </NavLink>
                    </div>
                  ) : (
                    <>
                      {localStorage.getItem("auth") &&
                        jwt(localStorage.getItem("auth")).contactType ==
                          "staff" &&
                        (Array.isArray(formData) && formData.length > 0
                          ? formData.map((item) =>
                              item.roleStaff === "products_vendor" ? (
                                <div className="col">
                                  <NavLink
                                    className="icon-box card card-body h-100 border-0 shadow-sm card-hover h-100 text-center"
                                    to="/add-utility/"
                                  >
                                    <div className="icon-box-media bg-faded-primary text-primary rounded-circle mb-3 mx-auto">
                                      <i
                                        class="fa fa-map-marker"
                                        aria-hidden="true"
                                      ></i>
                                    </div>
                                    <h3 className="icon-box-title fs-base mb-0">
                                      Add Utility Tool
                                    </h3>
                                  </NavLink>
                                </div>
                              ) : (
                                ""
                              )
                            )
                          : "")}
                    </>
                  )}
                </div>

                {localStorage.getItem("auth") &&
                jwt(localStorage.getItem("auth")).contactType == "admin" ? (
                  <>
                    <h3 className="mt-5 text-white">Training Tools</h3>
                    <hr className="mb-3" />
                    <div className="row row-cols-lg-6 row-cols-sm-3 row-cols-2 g-3 g-xl-4 mt-3">
                      <div className="col">
                        <NavLink
                          className="icon-box card card-body h-100 border-0 shadow-sm card-hover h-100 text-center"
                          to="/add-learn"
                        >
                          <div className="icon-box-media bg-faded-primary text-primary rounded-circle mb-3 mx-auto">
                            <i
                              className="fa fa-file-video-o"
                              aria-hidden="true"
                            ></i>
                          </div>
                          <h3 className="icon-box-title fs-base mb-0">
                            Courses
                          </h3>
                        </NavLink>
                      </div>
                      <div className="col">
                        <NavLink
                          className="icon-box card card-body h-100 border-0 shadow-sm card-hover h-100 text-center"
                          to="/control-panel/insights/"
                        >
                          <div className="icon-box-media bg-faded-primary text-primary rounded-circle mb-3 mx-auto">
                            <i
                              className="fa fa-pie-chart"
                              aria-hidden="true"
                            ></i>
                          </div>
                          <h3 className="icon-box-title fs-base mb-0">
                            {" "}
                            System Insights
                          </h3>
                        </NavLink>
                      </div>
                    </div>
                  </>
                ) : (
                  ""
                )}

                {localStorage.getItem("auth") &&
                jwt(localStorage.getItem("auth")).contactType == "admin" ? (
                  <>
                    <h3 className="mt-5 text-white">Website Tools</h3>
                    <hr className="mb-3" />
                    <div className="row row-cols-lg-6 row-cols-sm-3 row-cols-2 g-3 g-xl-4 mt-3">
                      <div className="col">
                        <NavLink
                          className="icon-box card card-body h-100 border-0 shadow-sm card-hover h-100 text-center"
                          to="/control-panel/accent-photo/"
                        >
                          <div className="icon-box-media bg-faded-primary text-primary rounded-circle mb-3 mx-auto">
                            <i className="fa fa-camera" aria-hidden="true"></i>
                          </div>
                          <h3 className="icon-box-title fs-base mb-0">
                            Accent Photo
                          </h3>
                        </NavLink>
                      </div>

                      <div className="col">
                        <NavLink
                          className="icon-box card card-body h-100 border-0 shadow-sm card-hover h-100 text-center"
                          to="/control-panel/theme/"
                        >
                          <div className="icon-box-media bg-faded-primary text-primary rounded-circle mb-3 mx-auto">
                            <i
                              className="fa fa-paint-brush"
                              aria-hidden="true"
                            ></i>
                          </div>
                          <h3 className="icon-box-title fs-base mb-0">
                            Colors & Theme
                          </h3>
                        </NavLink>
                      </div>

                      <div className="col">
                        <NavLink
                          className="icon-box card card-body h-100 border-0 shadow-sm card-hover h-100 text-center"
                          to="/control-panel/logo/"
                        >
                          <div className="icon-box-media bg-faded-primary text-primary rounded-circle mb-3 mx-auto">
                            <i className="fa fa-star-o" aria-hidden="true"></i>
                          </div>
                          <h3 className="icon-box-title fs-base mb-0">
                            Logo & Brand
                          </h3>
                        </NavLink>
                      </div>
                      <div className="col">
                        <NavLink
                          className="icon-box card card-body h-100 border-0 shadow-sm card-hover h-100 text-center"
                          to="/control-panel/locations/"
                        >
                          <div className="icon-box-media bg-faded-primary text-primary rounded-circle mb-3 mx-auto">
                            <i className="fa fa-tree" aria-hidden="true"></i>
                          </div>
                          <h3 className="icon-box-title fs-base mb-0">
                            Organization Tree
                          </h3>
                        </NavLink>
                      </div>

                      <div className="col">
                        <NavLink
                          className="icon-box card card-body h-100 border-0 shadow-sm card-hover h-100 text-center"
                          to="/control-panel/homepage-photo/"
                        >
                          <div className="icon-box-media bg-faded-primary text-primary rounded-circle mb-3 mx-auto">
                            <i className="fa fa-tree" aria-hidden="true"></i>
                          </div>
                          <h3 className="icon-box-title fs-base mb-0">
                            Home page Photo
                          </h3>
                        </NavLink>
                      </div>

                      <div className="col">
                        {localStorage.getItem("auth") &&
                        jwt(localStorage.getItem("auth")).contactType ==
                          "admin" ? (
                          <NavLink
                            className="icon-box card card-body h-100 border-0 shadow-sm card-hover h-100 text-center"
                            to="/admin/platformDetails/"
                          >
                            <div className="icon-box-media bg-faded-primary text-primary rounded-circle mb-3 mx-auto">
                              <i
                                className="fa fa-id-badge"
                                aria-hidden="true"
                              ></i>
                            </div>
                            <h3 className="icon-box-title fs-base mb-0">
                              Site Account
                            </h3>
                          </NavLink>
                        ) : (
                          ""
                        )}
                      </div>

                      <div className="col">
                        {localStorage.getItem("auth") &&
                        jwt(localStorage.getItem("auth")).contactType ==
                          "admin" ? (
                          <NavLink
                            className="icon-box card card-body h-100 border-0 shadow-sm card-hover h-100 text-center"
                            to="/onBoard-document"
                          >
                            <div className="icon-box-media bg-faded-primary text-primary rounded-circle mb-3 mx-auto">
                              <i
                                className="fa fa-id-badge"
                                aria-hidden="true"
                              ></i>
                            </div>
                            <h3 className="icon-box-title fs-base mb-0">
                              OnBoardDocument
                            </h3>
                          </NavLink>
                        ) : (
                          ""
                        )}
                      </div>
                    </div>
                  </>
                ) : (
                  ""
                )}

                {localStorage.getItem("auth") &&
                jwt(localStorage.getItem("auth")).contactType == "admin" ? (
                  <>
                    <h3 className="mt-5 text-white">Transact Tools</h3>
                    <hr className="mb-3" />
                    <div className="row row-cols-lg-6 row-cols-sm-3 row-cols-2 g-3 g-xl-4 mt-3">
                      <div className="col">
                        <NavLink
                          className="icon-box card card-body h-100 border-0 shadow-sm card-hover h-100 text-center"
                          to="/control-panel/configs/"
                        >
                          <div className="icon-box-media bg-faded-primary text-primary rounded-circle mb-3 mx-auto">
                            <i className="fa fa-cogs" aria-hidden="true"></i>
                          </div>
                          <h3 className="icon-box-title fs-base mb-0">
                            {" "}
                            Configurations
                          </h3>
                        </NavLink>
                      </div>

                      <div className="col">
                        <NavLink
                          className="icon-box card card-body h-100 border-0 shadow-sm card-hover h-100 text-center"
                          to="/control-panel/transprofile/"
                        >
                          <div className="icon-box-media bg-faded-primary text-primary rounded-circle mb-3 mx-auto">
                            <i
                              className="fa fa-id-card-o"
                              aria-hidden="true"
                            ></i>
                          </div>
                          <h3 className="icon-box-title fs-base mb-0">
                            Profiles
                          </h3>
                        </NavLink>
                      </div>
                    </div>
                  </>
                ) : (
                  ""
                )}

                {localStorage.getItem("auth") &&
                jwt(localStorage.getItem("auth")).contactType == "admin" ? (
                  <>
                    <h3 className="mt-5 text-white">Financial Tools</h3>
                    <hr className="mb-3" />
                    <div className="row row-cols-lg-6 row-cols-sm-3 row-cols-2 g-3 g-xl-4 mt-3">
                      <div className="col">
                        <NavLink
                          className="icon-box card card-body h-100 border-0 shadow-sm card-hover h-100 text-center"
                          to="/control-panel/accent-photo/"
                        >
                          <div className="icon-box-media bg-faded-primary text-primary rounded-circle mb-3 mx-auto">
                            <i className="fa fa-users" aria-hidden="true"></i>
                          </div>
                          <h3 className="icon-box-title fs-base mb-0">
                            Member Plans
                          </h3>
                        </NavLink>
                      </div>
                    </div>
                  </>
                ) : (
                  ""
                )}

                {localStorage.getItem("auth") &&
                jwt(localStorage.getItem("auth")).contactType == "admin" ? (
                  <>
                    <h3 className="mt-5 text-white">Required Documents</h3>
                    <hr className="mb-3" />
                    <div className="row row-cols-lg-6 row-cols-sm-3 row-cols-2 g-3 g-xl-4 mt-3">
                      <div className="col">
                        <NavLink
                          className="icon-box card card-body h-100 border-0 shadow-sm card-hover h-100 text-center"
                          to="/add-document-rules"
                        >
                          <div className="icon-box-media bg-faded-primary text-primary rounded-circle mb-3 mx-auto">
                            <img src={RuleIcon} />
                          </div>
                          <h3 className="icon-box-title fs-base mb-0">
                            Add Rule
                          </h3>
                        </NavLink>
                      </div>

                      <div className="col">
                        <NavLink
                          className="icon-box card card-body h-100 border-0 shadow-sm card-hover h-100 text-center"
                          to="/t-details"
                        >
                          <div className="icon-box-media bg-faded-primary text-primary rounded-circle mb-3 mx-auto">
                            <i
                              className="fa fa-question-circle"
                              aria-hidden="true"
                            ></i>
                          </div>
                          <h3 className="icon-box-title fs-base mb-0">
                            Add Details Question
                          </h3>
                        </NavLink>
                      </div>
                      <div className="col">
                        <NavLink
                          className="icon-box card card-body h-100 border-0 shadow-sm card-hover h-100 text-center"
                          to="/add-learn"
                        >
                          <div className="icon-box-media bg-faded-primary text-primary rounded-circle mb-3 mx-auto">
                            <i
                              className="fa fa-file-video-o"
                              aria-hidden="true"
                            ></i>
                          </div>
                          <h3 className="icon-box-title fs-base mb-0">
                            Add Learn Media
                          </h3>
                        </NavLink>
                      </div>

                      <div className="col">
                        <NavLink
                          className="icon-box card card-body h-100 border-0 shadow-sm card-hover h-100 text-center"
                          to="/add-staffRule"
                        >
                          <div className="icon-box-media bg-faded-primary text-primary rounded-circle mb-3 mx-auto">
                            <img src={staffRuleIcon} />
                          </div>
                          <h3 className="icon-box-title fs-base mb-0">
                            Add Staff Rule
                          </h3>
                        </NavLink>
                      </div>
                    </div>
                  </>
                ) : (
                  ""
                )}

                {localStorage.getItem("auth") &&
                jwt(localStorage.getItem("auth")).contactType == "admin" ? (
                  <>
                    <h3 className="mt-5 text-white">Admins Notifications</h3>
                    <hr className="mb-3" />
                    <div className="row row-cols-lg-6 row-cols-sm-3 row-cols-2 g-3 g-xl-4 mt-3">
                      <div className="col">
                        <NavLink
                          className="icon-box card card-body h-100 border-0 shadow-sm card-hover h-100 text-center"
                          to="/suggestFeature/"
                        >
                          <div className="icon-box-media bg-faded-primary text-primary rounded-circle mb-3 mx-auto">
                            <i
                              className="fa fa-question-circle"
                              aria-hidden="true"
                            ></i>
                          </div>
                          <h3 className="icon-box-title fs-base mb-0">
                            Suggest Feature
                          </h3>
                        </NavLink>
                      </div>

                      <div className="col">
                        <NavLink
                          className="icon-box card card-body h-100 border-0 shadow-sm card-hover h-100 text-center"
                          to="/report-bug"
                        >
                          <div className="icon-box-media bg-faded-primary text-primary rounded-circle mb-3 mx-auto">
                            <i class="fa fa-bug" aria-hidden="true"></i>
                          </div>
                          <h3 className="icon-box-title fs-base mb-0">
                            Report A Bug/Issue
                          </h3>
                        </NavLink>
                      </div>
                    </div>
                  </>
                ) : (
                  ""
                )}
              </div>
            </div>
          </div>
        </div>
      </main>
    </div>
  );
};

export default ControlPanel;

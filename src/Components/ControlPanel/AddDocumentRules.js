import React, { useState, useEffect } from "react";
import avtar from "../img/avtar.jpg";
import ReactPaginate from "react-paginate";
import user_service from "../service/user_service";
import jwt from "jwt-decode";
import Loader from "../../Pages/Loader/Loader.js";
import Toaster from "../../Pages/Toaster/Toaster.js";
import { NavLink, useNavigate, useParams } from "react-router-dom";
import _ from "lodash";
import { MultiSelect } from "react-multi-select-component";

const AddDocumentRules = () => {
  const [loader, setLoader] = useState({ isActive: null });
  const { isActive } = loader;
  const [toaster, setToaster] = useState({
    types: null,
    isShow: null,
    toasterBody: "",
    message: "",
  });
  const { types, isShow, toasterBody, message } = toaster;
  const navigate = useNavigate();

  const initialValues = {
    documentSetting: "",
    conditionWhere: "",
    conditionExpect: "",
    transactionPhase: "",
    agentId: "",
  };
  const [formValues, setFormValues] = useState(initialValues);
  const handleChange = (e) => {
    const { name, value } = e.target;
    setFormValues({ ...formValues, [name]: value });
  };
  const [formErrors, setFormErrors] = useState({});
  const [tname, setTname] = useState();
  const [ttype, setTtype] = useState();
  const [isSubmitClick, setISSubmitClick] = useState(false);
  const [getDocument, setGetDocument] = useState([]);
  const [selectedconditionWhere, setSelectedconditionWhere] = useState([]);
  const [selectedconditionExpect, setSelectedconditionExpect] = useState([]);

  const [documentValues, setDocumentValues] = useState([]);
  const [isModalVisibleUpdate, setIsModalVisibleUpdate] = useState(false);

  const [ruleData, setRuleData] = useState("")

  const PlatformDocuments = async () => {
    await user_service.PlatformDocuments().then((response) => {
      // console.log(response);
      if (response) {
        setGetDocument(response.data.data);
        const documentValuess = [];
        response.data.data?.map((postt) => {
          let aa = { label: `${postt.name}`, value: `${postt.slug}` };
          documentValuess.push(aa);
        });
        setDocumentValues(documentValuess);
      }
    });
  };
  const [rulesdata, setRulesdata] = useState([]);
  const DocumentRules = async () => {
    const agentId = jwt(localStorage.getItem("auth")).id;
    await user_service.DocumentRules(agentId).then((response) => {
      setLoader({ isActive: false });
      if (response) {
        // console.log(response.data)
        setRulesdata(response.data);
      }
    });
  };

  useEffect(() => {
    window.scrollTo(0, 0);
    setLoader({ isActive: true });
    DocumentRules();
    PlatformDocuments();

    setLoader({ isActive: false });
  }, []);

  const openrulepopup = (tname, ttype) => {
    setTname(tname);
    setTtype(ttype);

    window.$("#addrules").modal("show");
  };

  {
    /* <!-- Form Validation Start--> */
  }
  useEffect(() => {
    if (formValues && isSubmitClick) {
      validate();
    }
  }, [formValues]);

  const validate = () => {
    const values = formValues;
    const errors = {};
    if (!values.documentSetting) {
      errors.documentSetting = "Setting is required";
    }

    if (selectedconditionWhere.length < 1) {
      errors.conditionWhere = "where condition is required";
    }

    // if (selectedconditionExpect.length < 1) {
    //     errors.conditionExpect = "expect is required";
    // }

    if (!values.transactionPhase) {
      errors.transactionPhase = "transaction phase is required";
    }

    setFormErrors(errors);
    return errors;
  };
  {
    /* <!-- Form Validation End--> */
  }

  const addRule = async (e) => {
    e.preventDefault();
    setISSubmitClick(true);
    let checkValue = validate();
    if (_.isEmpty(checkValue)) {
      var transname = tname.replace(/ /g, "_");
      transname = transname.toLowerCase();

      const ruleData = {
        agentId: jwt(localStorage.getItem("auth")).id,
        transactionType: ttype,
        documentSetting: formValues.documentSetting,
        conditionWhere: JSON.stringify(selectedconditionWhere),
        conditionExpect: JSON.stringify(selectedconditionExpect),
        transactionPhase: formValues.transactionPhase,
      };
      console.log(ruleData);
      // return;
      try {
        setLoader({ isActive: true });
        // const response = await user_service.AdddocumentRule(ruleData);
        await user_service.AdddocumentRule(ruleData).then((response) => {
          if (response) {
            // console.log(response.data)
            setLoader({ isActive: false });
            DocumentRules();
            PlatformDocuments();
            document.getElementById("closeModalrule").click();
            setToaster({
              type: "Rule created Successfully",
              isShow: true,
              toasterBody: response.data.message,
              message: "Rule created Successfully",
            });
            document.getElementById("closeModal").click();
            // setTimeout(() => {
            //     // navigate(`/transaction-summary/${}`);
            // }, 500);
          } else {
            setLoader({ isActive: false });
            setToaster({
              types: "error",
              isShow: true,
              toasterBody: response.data.message,
              message: "Error",
            });
          }
          setTimeout(() => {
            setToaster({
              types: "error",
              isShow: false,
              toasterBody: null,
              message: "Error",
            });
          }, 2000);
        });
      } catch (error) {
        setLoader({ isActive: false });
        setToaster({
          types: "error",
          isShow: true,
          toasterBody: error,
          message: "Error",
        });
      }
    }
  };

  const propertyTypes = [
    { key: "buy_residential", value: "Buyer Residential" },
    { key: "buy_commercial", value: "Buyer Commercial" },
    { key: "buy_investment", value: "Buyer Investment" },
    { key: "buy_land", value: "Buyer Land" },
    { key: "buy_referral", value: "Buyer Referral" },
    { key: "buy_lease", value: "Buyer Lease" },
    { key: "sale_residential", value: "Seller Residential" },
    { key: "sale_commercial", value: "Seller Commercial" },
    { key: "sale_investment", value: "Seller Investment" },
    { key: "sale_referral", value: "Seller Referral" },
    { key: "sale_lease", value: "Seller Lease" },
    { key: "both_residential", value: "Buyer & Seller Residential" },
    { key: "both_commercial", value: "Buyer & Seller Commercial" },
    { key: "both_investment", value: "Buyer & Seller Investment" },
    { key: "both_land", value: "Buyer & Seller Land" },
    { key: "both_referral", value: "Buyer & Seller Referral" },
    { key: "both_lease", value: "Buyer & Seller Lease" },
    { key: "referral_residential", value: "Referral Residential" },
    { key: "referral_commercial", value: "Referral Commercial" },
    { key: "referral_investment", value: "Referral Investment" },
    { key: "referral_land", value: "Referral Land" },
    { key: "referral_lease", value: "Referral Lease" },
  ];

   
  const handleUpdate = (data) => {
    const parsedConditionWhere = JSON.parse(data.conditionWhere || '[]');
    setSelectedconditionWhere(parsedConditionWhere);
    setFormValues((prevValues) => ({
      ...prevValues,
      transactionPhase: data.transactionPhase || "",
    }));
  
    setRuleData(data);
    setIsModalVisibleUpdate(true);
  };

  const UpdateRule = async(e)=>{
    e.preventDefault();

    console.log(ruleData._id);

    
      const ruleDataNew = {
        conditionWhere: JSON.stringify(selectedconditionWhere),
        transactionPhase: formValues.transactionPhase,
      };
      console.log(ruleDataNew);
      
      try {
        setLoader({ isActive: true });
        await user_service.documentupdateRule(ruleData._id, ruleDataNew).then((response) => {
          if (response) {
            // console.log(response.data)
            setLoader({ isActive: false });
            DocumentRules();
            PlatformDocuments();
            setToaster({
              type: "Rule created Successfully",
              isShow: true,
              toasterBody: response.data.message,
              message: "Rule created Successfully",
            });
            document.getElementById("closeModal").click();
          } else {
            setLoader({ isActive: false });
            setToaster({
              types: "error",
              isShow: true,
              toasterBody: response.data.message,
              message: "Error",
            });
          }
          setTimeout(() => {
            setToaster({
              types: "error",
              isShow: false,
              toasterBody: null,
              message: "Error",
            });
          }, 2000);
        });
      } catch (error) {
        setLoader({ isActive: false });
        setToaster({
          types: "error",
          isShow: true,
          toasterBody: error,
          message: "Error",
        });
      }
    
  }

  return (
    <div className="float-start w-100 bg-secondary float-left w-100 pt-4 mb-4">
      <Loader isActive={isActive} />
      <main className="page-wrapper">
        <div className="">
          <div className="float-start w-100 mb-4">
            <h3 className="mb-0 text-white">
              Add Rule For Transaction Documents
            </h3>
          </div>

          <div className="float-start w-100 bg-light rounded-3 border p-3 mb-2">
            <div className="">
              <div className="float-start w-100" id="auth-info">
                <h6 className="">How would you like to attach the document?</h6>

                {propertyTypes.map((item) => (
                  <div key={item.key}>
                    {/* {item.value} */}
                    <div className="float-start w-100 alert alert-secondary fade show">
                      <div className="">
                        <div
                          className="d-flex align-items-center justify-content-between"
                          data-bs-toggle="tooltip"
                          title="Edit"
                        >
                          <h6 className="pull-left m-0">{item.value}</h6>
                          <span className="pull-right d-flex align-items-center justify-content-between">
                            <button
                              className="btn btn-primary btn-sm  pull-left"
                              onClick={(e) =>
                                openrulepopup(item.value, item.key)
                              }
                            >
                              Add Rule
                            </button>
                            <a
                              className="nav-link py-0 ms-2"
                              href={`#${item.key}`}
                              data-bs-toggle="collapse"
                            >
                              {" "}
                              <i className="fa fa-chevron-down ml-2"></i>
                            </a>
                          </span>
                        </div>
                        <div
                          className="float-start w-100 collapse"
                          id={`${item.key}`}
                          data-bs-parent="#auth-info"
                        >
                          {rulesdata.data
                            ? rulesdata.data.length > 0
                              ? rulesdata.data.map((post_rule) => {
                                  if (post_rule.transactionType == item.key) {
                                    let conditionWheredata = JSON.parse(
                                      post_rule.conditionWhere
                                    );
                                    return (
                                      <>
                                        <p className="text-property mb-0 mt-3">
                                          {post_rule.transactionPhase}
                                        </p>
                                        {conditionWheredata
                                          ? conditionWheredata.length > 0
                                            ? conditionWheredata.map(
                                                (conditionWhere_rule) => (
                                                  <>
                                                    <div className="document_bar mt-2">
                                                      <p className="pull-left mb-0">
                                                        {
                                                          conditionWhere_rule.label
                                                        }
                                                      </p>
                                                      <button className="btn btn-primary btn-sm pull-right"
                                                      onClick={()=>handleUpdate(post_rule)}
                                                      data-toggle="modal"
                                                      data-target="#rule_data">
                                                        Update
                                                      </button>

                                                      {
                                                        <>
                                                          <span className="status_tag float-end w-auto">
                                                            {
                                                              post_rule.documentSetting
                                                            }
                                                          </span>
                                                        </>
                                                      }

                                                      <br />
                                                    </div>
                                                  </>
                                                )
                                              )
                                            : ""
                                          : ""}
                                      </>
                                    );
                                  }
                                })
                              : "Please set document rules."
                            : "Please set document rules."}
                        </div>
                      </div>
                    </div>
                  </div>
                ))}
              </div>
            </div>
          </div>
        </div>

        <div className="modal" role="dialog" id="addrules">
          <div
            className="modal-dialog modal-md modal-dialog-scrollable"
            role="document"
          >
            <div className="modal-content">
              <div className="modal-header">
                <h4 className="modal-title">Add Rule</h4>
                <button
                  className="btn-close"
                  type="button"
                  id="closeModalrule"
                  data-bs-dismiss="modal"
                  aria-label="Close"
                ></button>
              </div>
              <div className="modal-body fs-sm">
                <div className="">
                  <div>
                    <h6 className="text-right">{tname}</h6>
                    <form onSubmit={addRule}>
                      <div className="mb-3">
                        <label className="col-form-label p-0">Settings:</label>
                        <div className="col-md-12">
                          <div className="form-check mt-3">
                            <input
                              className="form-check-input ms-0"
                              id="form-radio-1"
                              type="radio"
                              name="documentSetting"
                              value="required"
                              onChange={handleChange}
                            />
                            <label
                              className="form-check-label-three"
                              for="form-radio-1"
                            >
                              Required
                            </label>
                          </div>
                          <div className="form-check mt-3">
                            <input
                              className="form-check-input ms-0"
                              id="form-radio-2"
                              type="radio"
                              name="documentSetting"
                              value="recommended"
                              onChange={handleChange}
                            />
                            <label
                              className="form-check-label-three"
                              for="form-radio-2"
                            >
                              Recommended
                            </label>
                          </div>
                          <div className="form-check mt-3">
                            <input
                              className="form-check-input ms-0"
                              id="form-radio-2"
                              type="radio"
                              name="documentSetting"
                              value="optional"
                              onChange={handleChange}
                            />
                            <label
                              className="form-check-label-three"
                              for="form-radio-2"
                            >
                              Optional
                            </label>
                          </div>
                          <div className="invalid-tooltip">
                            {formErrors.documentSetting}
                          </div>
                        </div>
                      </div>

                      <div className="">
                        <label className="col-form-label p-0">
                          Conditions:
                        </label>
                        <div className="col-md-12 mt-2">
                          <label className="form-label">Where-</label>
                          <MultiSelect
                            options={documentValues}
                            value={selectedconditionWhere}
                            onChange={setSelectedconditionWhere}
                            labelledBy="Select"
                            name="conditionWhere"
                          />

                          <div className="invalid-tooltip">
                            {formErrors.conditionWhere}
                          </div>

                        
                        </div>
                      </div>

                      <div className="mb-3 mt-3">
                        <label className="form-label">
                          Phase Of Transaction:
                        </label>
                        <div className="col-md-12">
                          {/* <label className="form-label" >Where-</label> */}
                          <select
                            className="form-select form-select-dark"
                            name="transactionPhase"
                            value={formValues.transactionPhase}
                            onChange={handleChange}
                          >
                            <option></option>
                            {/* <option value="start">Start</option>
                                                        <option value="contract">Contract</option>
                                                        <option value="Post-Closing">Post-Closing</option> */}

                            <option value="pre-listed">Pre-Listed</option>
                            <option value="active-listing">
                              Active Listing
                            </option>
                            <option value="showing-homes">Showing homes</option>
                            <option value="under-contract">
                              Under Contract
                            </option>
                            <option value="closed">Closed</option>
                            <option value="canceled">Canceled</option>
                          </select>
                          <div className="invalid-tooltip">
                            {formErrors.transactionPhase}
                          </div>
                        </div>
                      </div>
                      <div className="modal-footer px-0">
                        <button
                          type="submit"
                          className="btn btn-primary btn-sm"
                        >
                          Save changes
                        </button>
                        <button
                          type="button"
                          className="btn btn-secondary btn-sm"
                          data-dismiss="modal"
                        >
                          Cancel & Close
                        </button>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>


        <div className="modal" role="dialog" id="rule_data">
          <div
            className="modal-dialog modal-md modal-dialog-scrollable"
            role="document"
          >
            <div className="modal-content">
              <div className="modal-header">
                <h4 className="modal-title">Update Rule</h4>
                <button
                  className="btn-close"
                  type="button"
                  id="closeModal"
                  data-dismiss="modal"
                  aria-label="Close"
                ></button>
              </div>
              <div className="modal-body fs-sm">
                <div className="">
                  <div>
                    <form onSubmit={UpdateRule}>
             
                      <div className="">
                        <label className="col-form-label p-0">
                          Conditions:
                        </label>
                        <div className="col-md-12 mt-2">
                          <label className="form-label">Where-</label>
                          <MultiSelect
                            options={documentValues}
                            value={selectedconditionWhere}
                            onChange={setSelectedconditionWhere}
                            labelledBy="Select"
                            name="conditionWhere"
                          />

                          <div className="invalid-tooltip">
                            {formErrors.conditionWhere}
                          </div>
                        </div>
                      </div>

                      <div className="mb-3 mt-3">
                        <label className="form-label">
                          Phase Of Transaction:
                        </label>
                        <div className="col-md-12">
                          {/* <label className="form-label" >Where-</label> */}
                          <select
                            className="form-select form-select-dark"
                            name="transactionPhase"
                            value={formValues.transactionPhase}
                            onChange={handleChange}
                          >
                            <option></option>
                            {/* <option value="start">Start</option>
                                                        <option value="contract">Contract</option>
                                                        <option value="Post-Closing">Post-Closing</option> */}

                            <option value="pre-listed">Pre-Listed</option>
                            <option value="active-listing">
                              Active Listing
                            </option>
                            <option value="showing-homes">Showing homes</option>
                            <option value="under-contract">
                              Under Contract
                            </option>
                            <option value="closed">Closed</option>
                            <option value="canceled">Canceled</option>
                          </select>
                          <div className="invalid-tooltip">
                            {formErrors.transactionPhase}
                          </div>
                        </div>
                      </div>
                      <div className="modal-footer px-0">
                        <button
                          type="submit"
                          className="btn btn-primary btn-sm"
                        >
                          Save changes
                        </button>
                        <button
                          type="button"
                          className="btn btn-secondary btn-sm"
                          data-dismiss="modal"
                        >
                          Cancel & Close
                        </button>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

    
      </main>
    </div>
  );
};

export default AddDocumentRules;

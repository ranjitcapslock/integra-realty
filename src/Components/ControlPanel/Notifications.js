import React, { useState, useEffect } from "react";
import _ from "lodash";
import user_service from "../service/user_service";
import jwt from "jwt-decode";
import Loader from "../../Pages/Loader/Loader.js";
import Toaster from "../../Pages/Toaster/Toaster.js";
import { NavLink, useNavigate } from "react-router-dom";

function Notifications() {
  const [loader, setLoader] = useState({ isActive: null });
  const { isActive } = loader;
  const [toaster, setToaster] = useState({
    types: null,
    isShow: null,
    toasterBody: "",
    message: "",
  });
  const { types, isShow, toasterBody, message } = toaster;
  const navigate = useNavigate();

  const [reservationData, setReservationData] = useState([]);

  useEffect(() => {
    window.scrollTo(0, 0);
    setLoader({ isActive: true });
    reservationGetAllData();
  }, []);

  const reservationGetAllData = async () => {
    await user_service.noticecontactGet().then((response) => {
      setLoader({ isActive: false });
      if (response) {
        setReservationData(response.data.data);
      }
    });
  };

  const userId = jwt(localStorage.getItem("auth")).id; // Decode JWT to get user ID

  const [totalMatches, setTotalMatches] = useState(0);

  useEffect(() => {
    let total = 0;

    reservationData.forEach((item) => {
      const contactTypeMatches = item.contactType
        ? item.contactType.filter((contact) => contact._id === userId).length
        : 0;

      const contactGroupMatches = item.contactGroup
        ? item.contactGroup.reduce((acc, group) => {
            const groupMemberMatches = group.group_member
              ? group.group_member.filter((member) => member._id === userId)
                  .length
              : 0;
            return acc + groupMemberMatches;
          }, 0)
        : 0;

        const officeGroupMatches = item.officeGroup
        ? item.officeGroup.reduce((acc, office) => {
            const officeMatches = office.associates
              ? office.associates.filter((associate) => associate._id === userId)
                  .length
              : 0;
            return acc + officeMatches;
          }, 0)
        : 0;

        console.log(officeGroupMatches);


      total += contactTypeMatches + contactGroupMatches;
    });

    setTotalMatches(total);
  }, [reservationData, userId]);

  const [categoryData, setCategoryData] = useState("");
  const [messageData, setMessageData] = useState("");
  const [selectedItem, setSelectedItem] = useState("");
  const [contactId, setContactId] = useState("");
  const [contactType, setContactType] = useState("");
  const [isVisible, setIsVisible] = useState(false);

  const handleAgentData = (category, message, item, contactId, type) => {
    setCategoryData(category);
    setMessageData(message);
    setSelectedItem(item);
    setContactId(contactId);
    setContactType(type);
    setIsVisible(true);
  };

  const handleStatusChange = async (item, contactId, type) => {
    if (item && contactId) {
      let userData = {};
      if (type === "contactType") {
        userData = {
          contactType: [
            {
              _id: contactId,
              is_view: "0",
            },
          ],
        };
      } else if (type === "contactGroup") {
        userData = {
          contactGroup: [
            {
              group_member: [
                {
                  _id: contactId,
                  is_view: "0",
                },
              ],
            },
          ],
        };
      }
      try {
        setLoader({ isActive: true });
        const response = await user_service.noticeUpdate(item._id, userData);

        if (response) {
          setLoader({ isActive: false });
          setToaster({
            types: "Notice Update",
            isShow: true,
            message: "Notice Updated Successfully",
          });
          setTimeout(() => {
            setToaster((prevToaster) => ({ ...prevToaster, isShow: false }));
          }, 3000);
          reservationGetAllData();
          setIsVisible(false);
        }
      } catch (error) {
        setLoader({ isActive: false });
      }
    }
  };

  return (
    <div className="bg-secondary float-left w-100 pt-4 mb-4">
      <Loader isActive={isActive} />
      {isShow && (
        <Toaster
          types={types}
          isShow={isShow}
          toasterBody={toasterBody}
          message={message}
        />
      )}
      <main className="page-wrapper">
        <div className="content-overlay">
          <div className="d-flex align-items-center justify-content-start mb-4">
            <h3 className="mb-0 text-white">Notice</h3>
          </div>
          {totalMatches > 0 ? (
            <div className="promoted_product float-left w-100">
              <div className="row bg-light p-3 border-0 rounded-3">
                {reservationData.map((item, index) => {
                  const hasContactType =
                    item.contactType &&
                    item.contactType.some((contact) => contact._id === userId);
                  const hasContactGroup =
                    item.contactGroup &&
                    item.contactGroup.some(
                      (group) =>
                        group.group_member &&
                        group.group_member.some(
                          (member) => member._id === userId
                        )
                    );

                  if (hasContactType || hasContactGroup) {
                    return (
                      <div key={index} className="col-md-12 mb-4">
                        {hasContactType && (
                          <ul>
                            {item.contactType.map(
                              (contact, i) =>
                                contact._id === userId && (
                                  <li key={i}>
                                    <div className="card p-3">
                                      {item.category && (
                                        <a
                                          style={{ fontSize: "24px" }}
                                          href="#"
                                          data-toggle="modal"
                                          data-target="#notice"
                                          onClick={() =>
                                            handleAgentData(
                                              item.category,
                                              item.message,
                                              item,
                                              contact._id,
                                              "contactType"
                                            )
                                          }
                                        >
                                          {item.category}
                                        </a>
                                      )}

                                      {item.message && (
                                        <span
                                          dangerouslySetInnerHTML={{
                                            __html: item.message,
                                          }}
                                        ></span>
                                      )}
                                    </div>
                                  </li>
                                )
                            )}
                          </ul>
                        )}

                        {hasContactGroup && (
                          <ul>
                            {item.contactGroup.map(
                              (group, i) =>
                                group.group_member &&
                                group.group_member.map(
                                  (member, j) =>
                                    member._id === userId && (
                                      <li key={j}>
                                        <div className="card p-3">
                                          {item.category && (
                                            <a
                                              style={{ fontSize: "24px" }}
                                              href="#"
                                              data-toggle="modal"
                                              data-target="#notice"
                                              onClick={() =>
                                                handleAgentData(
                                                  item.category,
                                                  item.message,
                                                  item,
                                                  member._id,
                                                  "contactGroup"
                                                )
                                              }
                                            >
                                              {item.category}
                                            </a>
                                          )}

                                          {item.message && (
                                            <span
                                              dangerouslySetInnerHTML={{
                                                __html: item.message,
                                              }}
                                            ></span>
                                          )}
                                        </div>
                                      </li>
                                    )
                                )
                            )}
                          </ul>
                        )}
                      </div>
                    );
                  }

                  return null;
                })}
              </div>
            </div>
          ) : (
            <p className="bg-light p-3 border-0 rounded-3">No Notices</p>
          )}
        </div>

        {isVisible && (
          <div
            className="w-100 border rounded-3 p-3 d-flex align-items-center justify-content-center"
           
            style={{
              display: "block",
            }}
          >
            <div className="modal in mt-5" role="dialog" id="notice"
             data-backdrop="static" data-keyboard="false">
              <div
                className="modal-dialog modal-mg modal-dialog-scrollable"
                role="document"
              >
                <div className="modal-content mt-5">
                
                  <div className="modal-body fs-lg">
                    <div className="row">
                      <h4 className="modal-title">{categoryData} Notice</h4>
                      {messageData ? (
                        <p
                          dangerouslySetInnerHTML={{
                            __html: messageData,
                          }}
                        ></p>
                      ) : (
                        ""
                      )}

                      <button
                        className="btn btn-primary"
                        onClick={() =>
                          handleStatusChange(
                            selectedItem,
                            contactId,
                            contactType
                          )
                        }
                      >
                        Got it
                      </button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        )}
      </main>
    </div>
  );
}
export default Notifications;

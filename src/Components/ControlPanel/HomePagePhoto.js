import React, { useState, useEffect, useRef } from "react";
import _ from "lodash";
import user_service from "../service/user_service";
import Loader from "../../Pages/Loader/Loader.js";
import Toaster from "../../Pages/Toaster/Toaster.js";
import axios from "axios";
import jwt from "jwt-decode";
import { NavLink } from 'react-router-dom';
import Swal from "sweetalert2";
import "sweetalert2/dist/sweetalert2.css";

function HomePagePhoto() {
  const initialValues = {
    title: "",
    description: "",
    parent_section: "",
    publication_level: "",
    exceptions: "",
    pin: "",
    lock: "",
    resource_url: "",
  };
  const [loader, setLoader] = useState({ isActive: null });
  const { isActive } = loader;
  const [toaster, setToaster] = useState({
    types: null,
    isShow: null,
    toasterBody: "",
    message: "",
  });
  const { types, isShow, toasterBody, message } = toaster;

  const [file, setFile] = useState(null);

  const [fileExtension, setFileExtension] = useState("");

  const [acceptedFileTypes, setAcceptedFileTypes] = useState([]);
  const [showSubmitButton, setShowSubmitButton] = useState(false);
  const [data, setData] = useState("");
  const [dataLink, setDataLink] = useState("");
  const [dataQuestions, setDataQuestions] = useState("");

  const [imageData, setImageData] = useState({});

  const HomeImagePhotoGet = async () => {
    await user_service.homePagePhotoGet().then((response) => {
      if (response) {
        const groupedByImage = response.data.data.reduce((acc, item) => {
          const imageType = item.imageType; // Fixed variable name to be consistent
          if (!acc[imageType]) {
            acc[imageType] = [];
          }
          acc[imageType].push(item);
          return acc;
        }, {});
        setImageData(groupedByImage);
      }
    });
  };

  useEffect(() => {
    HomeImagePhotoGet();
  }, []);

  const handleFileUpload = async (e) => {
    // if (imageData.HomeImage && imageData.HomeImage[0]._id) {
    //   const selectedFile = e.target.files[0];

    //   if (selectedFile) {
    //     setFile(selectedFile);
    //     setFileExtension(selectedFile.name.split(".").pop());
    //     setShowSubmitButton(true);

    //     const formData = new FormData();
    //     formData.append("file", selectedFile);
    //     const config = {
    //       headers: {
    //         Accept: "application/json",
    //         "Content-Type": "multipart/form-data",
    //         Authorization: `Bearer ${localStorage.getItem("auth")}`,
    //       },
    //     };

    //     try {
    //       setLoader({ isActive: true });
    //       const uploadResponse = await axios.post(
    //          "https://api.brokeragentbase.com/upload",
    //        // "http://localhost:4000/upload",
    //         formData,
    //         config
    //       );
    //       const uploadedFileData = uploadResponse.data;
    //       setData(uploadedFileData);

    //       const userData = {
    //         agentId: jwt(localStorage.getItem("auth")).id,
    //         imageType: "HomeImage",
    //         homeImage: uploadedFileData,
    //       };

    //       setLoader({ isActive: true });
    //       await user_service
    //         .homePagePhotoUpdate(imageData.HomeImage[0]._id, userData)
    //         .then((response) => {
    //           if (response) {
    //             setLoader({ isActive: false });
    //             setToaster({
    //               type: "Image Upload Successfully",
    //               isShow: true,
    //               toasterBody: response.data.message,
    //               message: "Image Upload  Successfully",
    //             });

    //             setTimeout(() => {
    //               setToaster((prevToaster) => ({
    //                 ...prevToaster,
    //                 isShow: false,
    //               }));
    //             }, 3000);
    //              HomeImagePhotoGet();

    //           } else {
    //             setLoader({ isActive: false });
    //             setToaster({
    //               type: "error",
    //               isShow: true,
    //               toasterBody: response.data.message,
    //               message: "Error",
    //             });

    //             setTimeout(() => {
    //               setToaster((prevToaster) => ({
    //                 ...prevToaster,
    //                 isShow: false,
    //               }));
    //             }, 3000);
    //           }
    //         });
    //     } catch (error) {
    //       setLoader({ isActive: false });
    //       setToaster({
    //         types: "error",
    //         isShow: true,
    //         toasterBody: error.response
    //           ? error.response.data.message
    //           : error.message,
    //         message: "Error",
    //       });

    //       setTimeout(() => {
    //         setToaster((prevToaster) => ({ ...prevToaster, isShow: false }));
    //       }, 1000);
    //     }
    //   }
    // } else {
      const selectedFile = e.target.files[0];

      if (selectedFile) {
        setFile(selectedFile);
        setFileExtension(selectedFile.name.split(".").pop());
        setShowSubmitButton(true);

        const formData = new FormData();
        formData.append("file", selectedFile);
        const config = {
          headers: {
            Accept: "application/json",
            "Content-Type": "multipart/form-data",
            Authorization: `Bearer ${localStorage.getItem("auth")}`,
          },
        };

        try {
          setLoader({ isActive: true });
          const uploadResponse = await axios.post(
            "https://api.brokeragentbase.com/upload",
            // "http://localhost:4000/upload",
            formData,
            config
          );
          const uploadedFileData = uploadResponse.data;
          setData(uploadedFileData);

          const userData = {
            agentId: jwt(localStorage.getItem("auth")).id,
            imageType: "HomeImage",
            homeImage: uploadedFileData,
          };

          setLoader({ isActive: true });
          await user_service.homePagePhoto(userData).then((response) => {
            if (response) {
              setLoader({ isActive: false });
              setToaster({
                type: "Image Upload Successfully",
                isShow: true,
                toasterBody: response.data.message,
                message: "Image Upload  Successfully",
              });

              setTimeout(() => {
                setToaster((prevToaster) => ({
                  ...prevToaster,
                  isShow: false,
                }));
              }, 3000);
             HomeImagePhotoGet();

            } else {
              setLoader({ isActive: false });
              setToaster({
                type: "error",
                isShow: true,
                toasterBody: response.data.message,
                message: "Error",
              });

              setTimeout(() => {
                setToaster((prevToaster) => ({
                  ...prevToaster,
                  isShow: false,
                }));
              }, 3000);
            }
          });
        } catch (error) {
          setLoader({ isActive: false });
          setToaster({
            types: "error",
            isShow: true,
            toasterBody: error.response
              ? error.response.data.message
              : error.message,
            message: "Error",
          });

          setTimeout(() => {
            setToaster((prevToaster) => ({ ...prevToaster, isShow: false }));
          }, 1000);
        }
      }
    // }
  };

  const handleFileUploadLink = async (e) => {
    // if (imageData.HomeLinks && imageData.HomeLinks[0]._id) {
    //   const selectedFile = e.target.files[0];

    //   if (selectedFile) {
    //     setFile(selectedFile);
    //     setFileExtension(selectedFile.name.split(".").pop());
    //     setShowSubmitButton(true);

    //     const formData = new FormData();
    //     formData.append("file", selectedFile);
    //     const config = {
    //       headers: {
    //         Accept: "application/json",
    //         "Content-Type": "multipart/form-data",
    //         Authorization: `Bearer ${localStorage.getItem("auth")}`,
    //       },
    //     };

    //     try {
    //       setLoader({ isActive: true });
    //       const uploadResponse = await axios.post(
            // "https://api.brokeragentbase.com/upload",
    //        // "http://localhost:4000/upload",
    //         formData,
    //         config
    //       );
    //       const uploadedFileData = uploadResponse.data;
    //       setData(uploadedFileData);

    //       const userData = {
    //         agentId: jwt(localStorage.getItem("auth")).id,
    //         imageType: "HomeLinks",
    //         homeLinks: uploadedFileData,
    //       };

    //       setLoader({ isActive: true });
    //       await user_service
    //         .homePagePhotoUpdate(imageData.HomeLinks[0]._id, userData)
    //         .then((response) => {
    //           if (response) {
    //             setLoader({ isActive: false });
    //             setToaster({
    //               type: "Image Upload Successfully",
    //               isShow: true,
    //               toasterBody: response.data.message,
    //               message: "Image Upload  Successfully",
    //             });

    //             setTimeout(() => {
    //               setToaster((prevToaster) => ({
    //                 ...prevToaster,
    //                 isShow: false,
    //               }));
    //             }, 3000);
    //           HomeImagePhotoGet();

    //           } else {
    //             setLoader({ isActive: false });
    //             setToaster({
    //               type: "error",
    //               isShow: true,
    //               toasterBody: response.data.message,
    //               message: "Error",
    //             });

    //             setTimeout(() => {
    //               setToaster((prevToaster) => ({
    //                 ...prevToaster,
    //                 isShow: false,
    //               }));
    //             }, 3000);
    //           }
    //         });
    //     } catch (error) {
    //       setLoader({ isActive: false });
    //       setToaster({
    //         types: "error",
    //         isShow: true,
    //         toasterBody: error.response
    //           ? error.response.data.message
    //           : error.message,
    //         message: "Error",
    //       });

    //       setTimeout(() => {
    //         setToaster((prevToaster) => ({ ...prevToaster, isShow: false }));
    //       }, 1000);
    //     }
    //   }
    // } else {
      const selectedFile = e.target.files[0];

      if (selectedFile) {
        setFile(selectedFile);
        setFileExtension(selectedFile.name.split(".").pop());
        setShowSubmitButton(true);

        const formData = new FormData();
        formData.append("file", selectedFile);
        const config = {
          headers: {
            Accept: "application/json",
            "Content-Type": "multipart/form-data",
            Authorization: `Bearer ${localStorage.getItem("auth")}`,
          },
        };

        try {
          setLoader({ isActive: true });
          const uploadResponse = await axios.post(
            "https://api.brokeragentbase.com/upload",
            // "http://localhost:4000/upload",
            formData,
            config
          );
          const uploadedFileData = uploadResponse.data;
          setData(uploadedFileData);

          const userData = {
            agentId: jwt(localStorage.getItem("auth")).id,
            imageType: "HomeLinks",
            homeLinks: uploadedFileData,
          };

          setLoader({ isActive: true });
          await user_service.homePagePhoto(userData).then((response) => {
            if (response) {
              setLoader({ isActive: false });
              setToaster({
                type: "Image Upload Successfully",
                isShow: true,
                toasterBody: response.data.message,
                message: "Image Upload  Successfully",
              });

              setTimeout(() => {
                setToaster((prevToaster) => ({
                  ...prevToaster,
                  isShow: false,
                }));
              }, 3000);
                HomeImagePhotoGet();

            } else {
              setLoader({ isActive: false });
              setToaster({
                type: "error",
                isShow: true,
                toasterBody: response.data.message,
                message: "Error",
              });

              setTimeout(() => {
                setToaster((prevToaster) => ({
                  ...prevToaster,
                  isShow: false,
                }));
              }, 3000);
            }
          });
        } catch (error) {
          setLoader({ isActive: false });
          setToaster({
            types: "error",
            isShow: true,
            toasterBody: error.response
              ? error.response.data.message
              : error.message,
            message: "Error",
          });

          setTimeout(() => {
            setToaster((prevToaster) => ({ ...prevToaster, isShow: false }));
          }, 1000);
        }
      }
    // }
  };

  const handleFileUploadQuestions = async (e) => {
    // if (imageData.HomeQuestions && imageData.HomeQuestions[0]._id) {
    //   const selectedFile = e.target.files[0];

    //   if (selectedFile) {
    //     setFile(selectedFile);
    //     setFileExtension(selectedFile.name.split(".").pop());
    //     setShowSubmitButton(true);

    //     const formData = new FormData();
    //     formData.append("file", selectedFile);
    //     const config = {
    //       headers: {
    //         Accept: "application/json",
    //         "Content-Type": "multipart/form-data",
    //         Authorization: `Bearer ${localStorage.getItem("auth")}`,
    //       },
    //     };

    //     try {
    //       setLoader({ isActive: true });
    //       const uploadResponse = await axios.post(
            // "https://api.brokeragentbase.com/upload",
    //        // "http://localhost:4000/upload",
    //         formData,
    //         config
    //       );
    //       const uploadedFileData = uploadResponse.data;
    //       setData(uploadedFileData);

    //       const userData = {
    //         agentId: jwt(localStorage.getItem("auth")).id,
    //         imageType: "HomeQuestions",
    //         homeQuestions: uploadedFileData,
    //       };

    //       setLoader({ isActive: true });
    //       await user_service
    //         .homePagePhotoUpdate(imageData.HomeQuestions[0]._id, userData)
    //         .then((response) => {
    //           if (response) {
    //             setLoader({ isActive: false });
    //             setToaster({
    //               type: "Image Upload Successfully",
    //               isShow: true,
    //               toasterBody: response.data.message,
    //               message: "Image Upload  Successfully",
    //             });

    //             setTimeout(() => {
    //               setToaster((prevToaster) => ({
    //                 ...prevToaster,
    //                 isShow: false,
    //               }));
    //              }, 3000);
    //        HomeImagePhotoGet();

    //           } else {
    //             setLoader({ isActive: false });
    //             setToaster({
    //               type: "error",
    //               isShow: true,
    //               toasterBody: response.data.message,
    //               message: "Error",
    //             });

    //             setTimeout(() => {
    //               setToaster((prevToaster) => ({
    //                 ...prevToaster,
    //                 isShow: false,
    //               }));
    //             }, 3000);
    //           }
    //         });
    //     } catch (error) {
    //       setLoader({ isActive: false });
    //       setToaster({
    //         types: "error",
    //         isShow: true,
    //         toasterBody: error.response
    //           ? error.response.data.message
    //           : error.message,
    //         message: "Error",
    //       });

    //       setTimeout(() => {
    //         setToaster((prevToaster) => ({ ...prevToaster, isShow: false }));
    //       }, 1000);
    //     }
    //   }
    // } else {
      const selectedFile = e.target.files[0];

      if (selectedFile) {
        setFile(selectedFile);
        setFileExtension(selectedFile.name.split(".").pop());
        setShowSubmitButton(true);

        const formData = new FormData();
        formData.append("file", selectedFile);
        const config = {
          headers: {
            Accept: "application/json",
            "Content-Type": "multipart/form-data",
            Authorization: `Bearer ${localStorage.getItem("auth")}`,
          },
        };

        try {
          setLoader({ isActive: true });
          const uploadResponse = await axios.post(
            "https://api.brokeragentbase.com/upload",
            // "http://localhost:4000/upload",
            formData,
            config
          );
          const uploadedFileData = uploadResponse.data;
          setData(uploadedFileData);

          const userData = {
            agentId: jwt(localStorage.getItem("auth")).id,
            imageType: "HomeQuestions",
            homeQuestions: uploadedFileData,
          };

          setLoader({ isActive: true });
          await user_service.homePagePhoto(userData).then((response) => {
            if (response) {
              setLoader({ isActive: false });
              setToaster({
                type: "Image Upload Successfully",
                isShow: true,
                toasterBody: response.data.message,
                message: "Image Upload  Successfully",
              });

              setTimeout(() => {
                setToaster((prevToaster) => ({
                  ...prevToaster,
                  isShow: false,
                }));
              }, 3000);
              HomeImagePhotoGet();

            } else {
              setLoader({ isActive: false });
              setToaster({
                type: "error",
                isShow: true,
                toasterBody: response.data.message,
                message: "Error",
              });

              setTimeout(() => {
                setToaster((prevToaster) => ({
                  ...prevToaster,
                  isShow: false,
                }));
              }, 3000);
            }
          });
        } catch (error) {
          setLoader({ isActive: false });
          setToaster({
            types: "error",
            isShow: true,
            toasterBody: error.response
              ? error.response.data.message
              : error.message,
            message: "Error",
          });

          setTimeout(() => {
            setToaster((prevToaster) => ({ ...prevToaster, isShow: false }));
          }, 1000);
        }
      }
    // }
  };

  const handleFileUploadStats = async (e) => {
    // if (imageData.HomeStats && imageData.HomeStats[0]._id) {
    //   const selectedFile = e.target.files[0];

    //   if (selectedFile) {
    //     setFile(selectedFile);
    //     setFileExtension(selectedFile.name.split(".").pop());
    //     setShowSubmitButton(true);

    //     const formData = new FormData();
    //     formData.append("file", selectedFile);
    //     const config = {
    //       headers: {
    //         Accept: "application/json",
    //         "Content-Type": "multipart/form-data",
    //         Authorization: `Bearer ${localStorage.getItem("auth")}`,
    //       },
    //     };

    //     try {
    //       setLoader({ isActive: true });
    //       const uploadResponse = await axios.post(
    //         "https://api.brokeragentbase.com/upload",
    //        // "http://localhost:4000/upload",
    //         formData,
    //         config
    //       );
    //       const uploadedFileData = uploadResponse.data;
    //       setData(uploadedFileData);

    //       const userData = {
    //         agentId: jwt(localStorage.getItem("auth")).id,
    //         imageType: "HomeStats",
    //         homeStats: uploadedFileData,
    //       };

    //       setLoader({ isActive: true });
    //       await user_service
    //         .homePagePhotoUpdate(imageData.HomeStats[0]._id, userData)
    //         .then((response) => {
    //           if (response) {
    //             setLoader({ isActive: false });
    //             setToaster({
    //               type: "Image Upload Successfully",
    //               isShow: true,
    //               toasterBody: response.data.message,
    //               message: "Image Upload  Successfully",
    //             });

    //             setTimeout(() => {
    //               setToaster((prevToaster) => ({
    //                 ...prevToaster,
    //                 isShow: false,
    //               }));
    //             }, 3000);
    //          HomeImagePhotoGet();

    //           } else {
    //             setLoader({ isActive: false });
    //             setToaster({
    //               type: "error",
    //               isShow: true,
    //               toasterBody: response.data.message,
    //               message: "Error",
    //             });

    //             setTimeout(() => {
    //               setToaster((prevToaster) => ({
    //                 ...prevToaster,
    //                 isShow: false,
    //               }));
    //             }, 3000);
    //           }
    //         });
    //     } catch (error) {
    //       setLoader({ isActive: false });
    //       setToaster({
    //         types: "error",
    //         isShow: true,
    //         toasterBody: error.response
    //           ? error.response.data.message
    //           : error.message,
    //         message: "Error",
    //       });

    //       setTimeout(() => {
    //         setToaster((prevToaster) => ({ ...prevToaster, isShow: false }));
    //       }, 1000);
    //     }
    //   }
    // } else {
      const selectedFile = e.target.files[0];

      if (selectedFile) {
        setFile(selectedFile);
        setFileExtension(selectedFile.name.split(".").pop());
        setShowSubmitButton(true);

        const formData = new FormData();
        formData.append("file", selectedFile);
        const config = {
          headers: {
            Accept: "application/json",
            "Content-Type": "multipart/form-data",
            Authorization: `Bearer ${localStorage.getItem("auth")}`,
          },
        };

        try {
          setLoader({ isActive: true });
          const uploadResponse = await axios.post(
            "https://api.brokeragentbase.com/upload",
            // "http://localhost:4000/upload",
            formData,
            config
          );
          const uploadedFileData = uploadResponse.data;
          setData(uploadedFileData);

          const userData = {
            agentId: jwt(localStorage.getItem("auth")).id,
            imageType: "HomeStats",
            homeStats: uploadedFileData,
          };

          setLoader({ isActive: true });
          await user_service.homePagePhoto(userData).then((response) => {
            if (response) {
              setLoader({ isActive: false });
              setToaster({
                type: "Image Upload Successfully",
                isShow: true,
                toasterBody: response.data.message,
                message: "Image Upload  Successfully",
              });

              setTimeout(() => {
                setToaster((prevToaster) => ({
                  ...prevToaster,
                  isShow: false,
                }));
              }, 3000);
              HomeImagePhotoGet();

            } else {
              setLoader({ isActive: false });
              setToaster({
                type: "error",
                isShow: true,
                toasterBody: response.data.message,
                message: "Error",
              });

              setTimeout(() => {
                setToaster((prevToaster) => ({
                  ...prevToaster,
                  isShow: false,
                }));
              }, 3000);
            }
          });
        } catch (error) {
          setLoader({ isActive: false });
          setToaster({
            types: "error",
            isShow: true,
            toasterBody: error.response
              ? error.response.data.message
              : error.message,
            message: "Error",
          });

          setTimeout(() => {
            setToaster((prevToaster) => ({ ...prevToaster, isShow: false }));
          }, 1000);
        }
      }
    // }
  };


  const handleFileUploadMenus = async (e) => {
    // if (imageData.HomeMenus && imageData.HomeMenus[0]._id) {
    //   const selectedFile = e.target.files[0];

    //   if (selectedFile) {
    //     setFile(selectedFile);
    //     setFileExtension(selectedFile.name.split(".").pop());
    //     setShowSubmitButton(true);

    //     const formData = new FormData();
    //     formData.append("file", selectedFile);
    //     const config = {
    //       headers: {
    //         Accept: "application/json",
    //         "Content-Type": "multipart/form-data",
    //         Authorization: `Bearer ${localStorage.getItem("auth")}`,
    //       },
    //     };

    //     try {
    //       setLoader({ isActive: true });
    //       const uploadResponse = await axios.post(
    //          "https://api.brokeragentbase.com/upload",
    //        // "http://localhost:4000/upload",
    //         formData,
    //         config
    //       );
    //       const uploadedFileData = uploadResponse.data;
    //       setData(uploadedFileData);

    //       const userData = {
    //         agentId: jwt(localStorage.getItem("auth")).id,
    //         imageType: "HomeMenus",
    //         homeMenus: uploadedFileData,
    //       };

    //       setLoader({ isActive: true });
    //       await user_service
    //         .homePagePhotoUpdate(imageData.HomeMenus[0]._id, userData)
    //         .then((response) => {
    //           if (response) {
    //             setLoader({ isActive: false });
    //             setToaster({
    //               type: "Image Upload Successfully",
    //               isShow: true,
    //               toasterBody: response.data.message,
    //               message: "Image Upload  Successfully",
    //             });

    //             setTimeout(() => {
    //               setToaster((prevToaster) => ({
    //                 ...prevToaster,
    //                 isShow: false,
    //               }));
    //             }, 3000);
    // HomeImagePhotoGet();

    //           } else {
    //             setLoader({ isActive: false });
    //             setToaster({
    //               type: "error",
    //               isShow: true,
    //               toasterBody: response.data.message,
    //               message: "Error",
    //             });

    //             setTimeout(() => {
    //               setToaster((prevToaster) => ({
    //                 ...prevToaster,
    //                 isShow: false,
    //               }));
    //             }, 3000);
    //           }
    //         });
    //     } catch (error) {
    //       setLoader({ isActive: false });
    //       setToaster({
    //         types: "error",
    //         isShow: true,
    //         toasterBody: error.response
    //           ? error.response.data.message
    //           : error.message,
    //         message: "Error",
    //       });

    //       setTimeout(() => {
    //         setToaster((prevToaster) => ({ ...prevToaster, isShow: false }));
    //       }, 1000);
    //     }
    //   }
    // } else {
      const selectedFile = e.target.files[0];

      if (selectedFile) {
        setFile(selectedFile);
        setFileExtension(selectedFile.name.split(".").pop());
        setShowSubmitButton(true);

        const formData = new FormData();
        formData.append("file", selectedFile);
        const config = {
          headers: {
            Accept: "application/json",
            "Content-Type": "multipart/form-data",
            Authorization: `Bearer ${localStorage.getItem("auth")}`,
          },
        };

        try {
          setLoader({ isActive: true });
          const uploadResponse = await axios.post(
            "https://api.brokeragentbase.com/upload",
            // "http://localhost:4000/upload",
            formData,
            config
          );
          const uploadedFileData = uploadResponse.data;
          setData(uploadedFileData);

          const userData = {
            agentId: jwt(localStorage.getItem("auth")).id,
            imageType: "HomeMenus",
            homeMenus: uploadedFileData,
          };

          setLoader({ isActive: true });
          await user_service.homePagePhoto(userData).then((response) => {
            if (response) {
              setLoader({ isActive: false });
              setToaster({
                type: "Image Upload Successfully",
                isShow: true,
                toasterBody: response.data.message,
                message: "Image Upload  Successfully",
              });

              setTimeout(() => {
                setToaster((prevToaster) => ({
                  ...prevToaster,
                  isShow: false,
                }));
              }, 3000);
              HomeImagePhotoGet();
            } else {
              setLoader({ isActive: false });
              setToaster({
                type: "error",
                isShow: true,
                toasterBody: response.data.message,
                message: "Error",
              });

              setTimeout(() => {
                setToaster((prevToaster) => ({
                  ...prevToaster,
                  isShow: false,
                }));
              }, 3000);
            }
          });
        } catch (error) {
          setLoader({ isActive: false });
          setToaster({
            types: "error",
            isShow: true,
            toasterBody: error.response
              ? error.response.data.message
              : error.message,
            message: "Error",
          });

          setTimeout(() => {
            setToaster((prevToaster) => ({ ...prevToaster, isShow: false }));
          }, 1000);
        }
      }
    // }
  };


  const handleFileUploadCalender = async (e) => {
    // if (imageData.HomeCalender && imageData.HomeCalender[0]._id) {
    //   const selectedFile = e.target.files[0];

    //   if (selectedFile) {
    //     setFile(selectedFile);
    //     setFileExtension(selectedFile.name.split(".").pop());
    //     setShowSubmitButton(true);

    //     const formData = new FormData();
    //     formData.append("file", selectedFile);
    //     const config = {
    //       headers: {
    //         Accept: "application/json",
    //         "Content-Type": "multipart/form-data",
    //         Authorization: `Bearer ${localStorage.getItem("auth")}`,
    //       },
    //     };

    //     try {
    //       setLoader({ isActive: true });
    //       const uploadResponse = await axios.post(
    //          "https://api.brokeragentbase.com/upload",
    //        // "http://localhost:4000/upload",
    //         formData,
    //         config
    //       );
    //       const uploadedFileData = uploadResponse.data;
    //       setData(uploadedFileData);

    //       const userData = {
    //         agentId: jwt(localStorage.getItem("auth")).id,
    //         imageType: "HomeCalender",
    //         homeCalender: uploadedFileData,
    //       };

    //       setLoader({ isActive: true });
    //       await user_service
    //         .homePagePhotoUpdate(imageData.HomeCalender[0]._id, userData)
    //         .then((response) => {
    //           if (response) {
    //             setLoader({ isActive: false });
    //             setToaster({
    //               type: "Image Upload Successfully",
    //               isShow: true,
    //               toasterBody: response.data.message,
    //               message: "Image Upload  Successfully",
    //             });

    //             setTimeout(() => {
    //               setToaster((prevToaster) => ({
    //                 ...prevToaster,
    //                 isShow: false,
    //               }));
    //             }, 3000);
    // HomeImagePhotoGet();

    //           } else {
    //             setLoader({ isActive: false });
    //             setToaster({
    //               type: "error",
    //               isShow: true,
    //               toasterBody: response.data.message,
    //               message: "Error",
    //             });

    //             setTimeout(() => {
    //               setToaster((prevToaster) => ({
    //                 ...prevToaster,
    //                 isShow: false,
    //               }));
    //             }, 3000);
    //           }
    //         });
    //     } catch (error) {
    //       setLoader({ isActive: false });
    //       setToaster({
    //         types: "error",
    //         isShow: true,
    //         toasterBody: error.response
    //           ? error.response.data.message
    //           : error.message,
    //         message: "Error",
    //       });

    //       setTimeout(() => {
    //         setToaster((prevToaster) => ({ ...prevToaster, isShow: false }));
    //       }, 1000);
    //     }
    //   }
    // } else {
      const selectedFile = e.target.files[0];

      if (selectedFile) {
        setFile(selectedFile);
        setFileExtension(selectedFile.name.split(".").pop());
        setShowSubmitButton(true);

        const formData = new FormData();
        formData.append("file", selectedFile);
        const config = {
          headers: {
            Accept: "application/json",
            "Content-Type": "multipart/form-data",
            Authorization: `Bearer ${localStorage.getItem("auth")}`,
          },
        };

        try {
          setLoader({ isActive: true });
          const uploadResponse = await axios.post(
            "https://api.brokeragentbase.com/upload",
            // "http://localhost:4000/upload",
            formData,
            config
          );
          const uploadedFileData = uploadResponse.data;
          setData(uploadedFileData);

          const userData = {
            agentId: jwt(localStorage.getItem("auth")).id,
            imageType: "HomeCalender",
            homeCalender: uploadedFileData,
          };

          setLoader({ isActive: true });
          await user_service.homePagePhoto(userData).then((response) => {
            if (response) {
              setLoader({ isActive: false });
              setToaster({
                type: "Image Upload Successfully",
                isShow: true,
                toasterBody: response.data.message,
                message: "Image Upload  Successfully",
              });

              setTimeout(() => {
                setToaster((prevToaster) => ({
                  ...prevToaster,
                  isShow: false,
                }));
              }, 3000);
              HomeImagePhotoGet();
            } else {
              setLoader({ isActive: false });
              setToaster({
                type: "error",
                isShow: true,
                toasterBody: response.data.message,
                message: "Error",
              });

              setTimeout(() => {
                setToaster((prevToaster) => ({
                  ...prevToaster,
                  isShow: false,
                }));
              }, 3000);
            }
          });
        } catch (error) {
          setLoader({ isActive: false });
          setToaster({
            types: "error",
            isShow: true,
            toasterBody: error.response
              ? error.response.data.message
              : error.message,
            message: "Error",
          });

          setTimeout(() => {
            setToaster((prevToaster) => ({ ...prevToaster, isShow: false }));
          }, 1000);
        }
      }
    // }
  };

  const handleFileUploadBirthdays = async (e) => {
    // if (imageData.HomeBirthdays && imageData.HomeBirthdays[0]._id) {
    //   const selectedFile = e.target.files[0];

    //   if (selectedFile) {
    //     setFile(selectedFile);
    //     setFileExtension(selectedFile.name.split(".").pop());
    //     setShowSubmitButton(true);

    //     const formData = new FormData();
    //     formData.append("file", selectedFile);
    //     const config = {
    //       headers: {
    //         Accept: "application/json",
    //         "Content-Type": "multipart/form-data",
    //         Authorization: `Bearer ${localStorage.getItem("auth")}`,
    //       },
    //     };

    //     try {
    //       setLoader({ isActive: true });
    //       const uploadResponse = await axios.post(
    //          "https://api.brokeragentbase.com/upload",
    //        // "http://localhost:4000/upload",
    //         formData,
    //         config
    //       );
    //       const uploadedFileData = uploadResponse.data;
    //       setData(uploadedFileData);

    //       const userData = {
    //         agentId: jwt(localStorage.getItem("auth")).id,
    //         imageType: "HomeBirthdays",
    //         homeBirthdays: uploadedFileData,
    //       };

    //       setLoader({ isActive: true });
    //       await user_service
    //         .homePagePhotoUpdate(imageData.HomeBirthdays[0]._id, userData)
    //         .then((response) => {
    //           if (response) {
    //             setLoader({ isActive: false });
    //             setToaster({
    //               type: "Image Upload Successfully",
    //               isShow: true,
    //               toasterBody: response.data.message,
    //               message: "Image Upload  Successfully",
    //             });

    //             setTimeout(() => {
    //               setToaster((prevToaster) => ({
    //                 ...prevToaster,
    //                 isShow: false,
    //               }));
    //             }, 3000);
    // HomeImagePhotoGet();

    //           } else {
    //             setLoader({ isActive: false });
    //             setToaster({
    //               type: "error",
    //               isShow: true,
    //               toasterBody: response.data.message,
    //               message: "Error",
    //             });

    //             setTimeout(() => {
    //               setToaster((prevToaster) => ({
    //                 ...prevToaster,
    //                 isShow: false,
    //               }));
    //             }, 3000);
    //           }
    //         });
    //     } catch (error) {
    //       setLoader({ isActive: false });
    //       setToaster({
    //         types: "error",
    //         isShow: true,
    //         toasterBody: error.response
    //           ? error.response.data.message
    //           : error.message,
    //         message: "Error",
    //       });

    //       setTimeout(() => {
    //         setToaster((prevToaster) => ({ ...prevToaster, isShow: false }));
    //       }, 1000);
    //     }
    //   }
    // } else {
      const selectedFile = e.target.files[0];

      if (selectedFile) {
        setFile(selectedFile);
        setFileExtension(selectedFile.name.split(".").pop());
        setShowSubmitButton(true);

        const formData = new FormData();
        formData.append("file", selectedFile);
        const config = {
          headers: {
            Accept: "application/json",
            "Content-Type": "multipart/form-data",
            Authorization: `Bearer ${localStorage.getItem("auth")}`,
          },
        };

        try {
          setLoader({ isActive: true });
          const uploadResponse = await axios.post(
            "https://api.brokeragentbase.com/upload",
            // "http://localhost:4000/upload",
            formData,
            config
          );
          const uploadedFileData = uploadResponse.data;
          setData(uploadedFileData);

          const userData = {
            agentId: jwt(localStorage.getItem("auth")).id,
            imageType: "HomeBirthdays",
            homeBirthdays: uploadedFileData,
          };

          setLoader({ isActive: true });
          await user_service.homePagePhoto(userData).then((response) => {
            if (response) {
              setLoader({ isActive: false });
              setToaster({
                type: "Image Upload Successfully",
                isShow: true,
                toasterBody: response.data.message,
                message: "Image Upload  Successfully",
              });

              setTimeout(() => {
                setToaster((prevToaster) => ({
                  ...prevToaster,
                  isShow: false,
                }));
              }, 3000);
              HomeImagePhotoGet();
            } else {
              setLoader({ isActive: false });
              setToaster({
                type: "error",
                isShow: true,
                toasterBody: response.data.message,
                message: "Error",
              });

              setTimeout(() => {
                setToaster((prevToaster) => ({
                  ...prevToaster,
                  isShow: false,
                }));
              }, 3000);
            }
          });
        } catch (error) {
          setLoader({ isActive: false });
          setToaster({
            types: "error",
            isShow: true,
            toasterBody: error.response
              ? error.response.data.message
              : error.message,
            message: "Error",
          });

          setTimeout(() => {
            setToaster((prevToaster) => ({ ...prevToaster, isShow: false }));
          }, 1000);
        }
      }
    // }
  };

  const handleFileUploadAssociates = async (e) => {
    // if (imageData.HomeAssociates && imageData.HomeAssociates[0]._id) {
    //   const selectedFile = e.target.files[0];

    //   if (selectedFile) {
    //     setFile(selectedFile);
    //     setFileExtension(selectedFile.name.split(".").pop());
    //     setShowSubmitButton(true);

    //     const formData = new FormData();
    //     formData.append("file", selectedFile);
    //     const config = {
    //       headers: {
    //         Accept: "application/json",
    //         "Content-Type": "multipart/form-data",
    //         Authorization: `Bearer ${localStorage.getItem("auth")}`,
    //       },
    //     };

    //     try {
    //       setLoader({ isActive: true });
    //       const uploadResponse = await axios.post(
    //         "https://api.brokeragentbase.com/upload",
    //         // "http://localhost:4000/upload",
    //         formData,
    //         config
    //       );
    //       const uploadedFileData = uploadResponse.data;
    //       setData(uploadedFileData);

    //       const userData = {
    //         agentId: jwt(localStorage.getItem("auth")).id,
    //         imageType: "HomeAssociates",
    //         homeAssociates: uploadedFileData,
    //       };

    //       setLoader({ isActive: true });
    //       await user_service
    //         .homePagePhotoUpdate(imageData.HomeAssociates[0]._id, userData)
    //         .then((response) => {
    //           if (response) {
    //             setLoader({ isActive: false });
    //             setToaster({
    //               type: "Image Upload Successfully",
    //               isShow: true,
    //               toasterBody: response.data.message,
    //               message: "Image Upload  Successfully",
    //             });

    //             setTimeout(() => {
    //               setToaster((prevToaster) => ({
    //                 ...prevToaster,
    //                 isShow: false,
    //               }));
    //             }, 3000);
    //          HomeImagePhotoGet();
    //           } else {
    //             setLoader({ isActive: false });
    //             setToaster({
    //               type: "error",
    //               isShow: true,
    //               toasterBody: response.data.message,
    //               message: "Error",
    //             });

    //             setTimeout(() => {
    //               setToaster((prevToaster) => ({
    //                 ...prevToaster,
    //                 isShow: false,
    //               }));
    //             }, 3000);
    //           }
    //         });
    //     } catch (error) {
    //       setLoader({ isActive: false });
    //       setToaster({
    //         types: "error",
    //         isShow: true,
    //         toasterBody: error.response
    //           ? error.response.data.message
    //           : error.message,
    //         message: "Error",
    //       });

    //       setTimeout(() => {
    //         setToaster((prevToaster) => ({ ...prevToaster, isShow: false }));
    //       }, 1000);
    //     }
    //   }
    // } else {
      
    const selectedFile = e.target.files[0];

      if (selectedFile) {
        setFile(selectedFile);
        setFileExtension(selectedFile.name.split(".").pop());
        setShowSubmitButton(true);

        const formData = new FormData();
        formData.append("file", selectedFile);
        const config = {
          headers: {
            Accept: "application/json",
            "Content-Type": "multipart/form-data",
            Authorization: `Bearer ${localStorage.getItem("auth")}`,
          },
        };

        try {
          setLoader({ isActive: true });
          const uploadResponse = await axios.post(
            "https://api.brokeragentbase.com/upload",
            // "http://localhost:4000/upload",
            formData,
            config
          );
          const uploadedFileData = uploadResponse.data;
          setData(uploadedFileData);

          const userData = {
            agentId: jwt(localStorage.getItem("auth")).id,
            imageType: "HomeAssociates",
            homeAssociates: uploadedFileData,
          };

          setLoader({ isActive: true });
          await user_service.homePagePhoto(userData).then((response) => {
            if (response) {
              setLoader({ isActive: false });
              setToaster({
                type: "Image Upload Successfully",
                isShow: true,
                toasterBody: response.data.message,
                message: "Image Upload  Successfully",
              });

              setTimeout(() => {
                setToaster((prevToaster) => ({
                  ...prevToaster,
                  isShow: false,
                }));
              }, 3000);
               HomeImagePhotoGet();

            } else {
              setLoader({ isActive: false });
              setToaster({
                type: "error",
                isShow: true,
                toasterBody: response.data.message,
                message: "Error",
              });

              setTimeout(() => {
                setToaster((prevToaster) => ({
                  ...prevToaster,
                  isShow: false,
                }));
              }, 3000);
            }
          });
        } catch (error) {
          setLoader({ isActive: false });
          setToaster({
            types: "error",
            isShow: true,
            toasterBody: error.response
              ? error.response.data.message
              : error.message,
            message: "Error",
          });

          setTimeout(() => {
            setToaster((prevToaster) => ({ ...prevToaster, isShow: false }));
          }, 1000);
        }
      }
    // }
  };


  const handleDeletehomeimage = async (id) => {
    Swal.fire({
      title: "Are you sure?",
      text: "It will be permanently deleted!",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Yes, delete it!",
    }).then(async (result) => {
      if (result.isConfirmed) {
        try {
          const response = await user_service.homePagePhotoDelete(id);
          if (response) {
               
                if (response) {
                  Swal.fire(
                    "Deleted!",
                    "Image is deleted permanently.",
                    "success"
                  );
                  //setFormValues(response.data);
                }
                HomeImagePhotoGet();
                setLoader({ isActive: false });
          }
        } catch (error) {
          Swal.fire(
            "Error",
            "An error occurred while deleting the file.",
            "error"
          );
          console.error(error);
        }
      }
    });
  };

  const handleSetdefaultimage = async (id, status) => {
    Swal.fire({
      title: "Are you sure?",
      text: status === "1" ? "You want to set this default image!" : "You want to remove this default image!",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Yes !",
    }).then(async (result) => {
      if (result.isConfirmed) {
        try {

          const userData = {
            agentId: jwt(localStorage.getItem("auth")).id,
            is_active: status,
          };

          setLoader({ isActive: true });
          await user_service
            .homePagePhotoUpdate(id, userData)
            .then((response) => {
              if (response) {
                setLoader({ isActive: false });
                setToaster({
                  type: "Image Update Successfully",
                  isShow: true,
                  toasterBody: response.data.message,
                  message: "Image Update  Successfully",
                });

                setTimeout(() => {
                  setToaster((prevToaster) => ({
                    ...prevToaster,
                    isShow: false,
                  }));
                }, 3000);
             HomeImagePhotoGet();
              } else {
                setLoader({ isActive: false });
                setToaster({
                  type: "error",
                  isShow: true,
                  toasterBody: response.data.message,
                  message: "Error",
                });

                setTimeout(() => {
                  setToaster((prevToaster) => ({
                    ...prevToaster,
                    isShow: false,
                  }));
                }, 3000);
              }
            });
        } catch (error) {
          Swal.fire(
            "Error",
            "An error occurred while deleting the file.",
            "error"
          );
          console.error(error);
        }
      }
    });
  };

  return (
    <div className="bg-secondary float-left w-100 pt-4 mb-4">
      <Loader isActive={isActive} />
      {isShow && (
        <Toaster
          types={types}
          isShow={isShow}
          toasterBody={toasterBody}
          message={message}
        />
      )}
      <main className="page-wrapper profile_page_wrap">
        <div className="container content-overlay">
          <div className="d-flex align-items-center justify-content-start mb-4">
            <h3 className="mb-0 text-white">Add Home Page Image</h3>
          </div>

          <div className="">
            <div className="">
              <div className="bg-light rounded-3 p-3 border mb-3">
                <div className="add_listing">
                  <h5>Home Page Images</h5>
                  <p>A home page background image typically serves as a visual backdrop for the main content on the website's front page. It should be high-quality, eye-catching, and relevant to the website's purpose.  The image often covers the entire screen or a significant portion, providing a cohesive and engaging first impression. It can include landscapes, abstract designs, or subject-specific visuals like products or services. The image should also be optimized for fast loading while maintaining clarity and aesthetic appeal across various devices and screen sizes.</p>
                  <div className="row">
                    <div className="col-md-4">
                      <label className="btn btn-primary btn-lg w-100 mb-3 documentlabel d-inline-block">
                        <input
                          id="REPC_real_estate_purchase_contract"
                          type="file"
                          accept={acceptedFileTypes
                            .map((type) => `.${type}`)
                            .join(",")}
                          name="file"
                          onChange={handleFileUpload}
                          // value={getContact.file}
                        />
                        <i className="fi-plus me-2"></i>Homepage Background Photo
                      </label>
                    </div>

                    <div className="col-md-8 d-flex align-item-center">
                      {imageData.HomeImage && imageData.HomeImage.length > 0 ? 
                      imageData.HomeImage.map((item) => {
                        return (
                          <div className="me-3">
                            <img title = "Click on the image to set/reset default image" onClick={(e) => handleSetdefaultimage(item._id, item.is_active === "1" ? "0" : "1")} className={item.is_active === "1" ? "homeimageactive" : ""}
                              src={item.homeImage}
                            />
                            <br />
                              <NavLink 
                              onClick={(e) => handleDeletehomeimage(item._id)} 
                              to="#" className="text-center " type="button"><i className="fa fa-trash-o me-2" aria-hidden="true"></i>Delete</NavLink>
                          </div>
                        )
                      })

                      : (
                        ""
                      )}
                    </div>
                  </div>
                  <hr className="mt-2"/>

                  <div className="row mt-3">
                    <div className="col-md-4">
                      <label className="btn btn-primary btn-lg w-100 mb-3 documentlabel d-inline-block">
                        <input
                          id="REPC_real_estate_purchase_contract"
                          type="file"
                          accept={acceptedFileTypes
                            .map((type) => `.${type}`)
                            .join(",")}
                          name="file"
                          onChange={handleFileUploadLink}
                          // value={getContact.file}
                        />
                        <i className="fi-plus me-2"></i> My Links Background Image
                      </label>
                    </div>

                    <div className="col-md-8 d-flex align-item-center">
                      {imageData.HomeLinks && imageData.HomeLinks.length > 0 ? 
                      imageData.HomeLinks.map((item) => {
                        return (
                          <div className="me-3">
                            <img  title = "Click on the image to set/reset default image" onClick={(e) => handleSetdefaultimage(item._id, item.is_active === "1" ? "0" : "1")} className={item.is_active === "1" ? "homeimageactive" : ""}
                              src={item.homeLinks}
                              alt="Home"
                            />
                            <br />
                              <NavLink 
                              onClick={(e) => handleDeletehomeimage(item._id)} 
                              to="#" className="text-center " type="button"><i className="fa fa-trash-o me-2" aria-hidden="true"></i>Delete</NavLink>
                          </div>
                        )
                      })
                      
                      : (
                        ""
                      )}
                    </div>
                  </div>
                  <hr className="mt-2"/>

                  <div className="row mt-3">
                    <div className="col-md-4">
                      <label className="btn btn-primary btn-lg w-100 mb-3 documentlabel d-inline-block">
                        <input
                          id="REPC_real_estate_purchase_contract"
                          type="file"
                          accept={acceptedFileTypes
                            .map((type) => `.${type}`)
                            .join(",")}
                          name="file"
                          onChange={handleFileUploadQuestions}
                          // value={getContact.file}
                        />
                        <i className="fi-plus me-2"></i> Questions Backgroung Image
                      </label>
                    </div>

                    <div className="col-md-8 d-flex align-item-center">
                      {imageData.HomeQuestions &&
                      imageData.HomeQuestions.length > 0 ? 
                      imageData.HomeQuestions.map((item) => {
                        return (
                          <div className="me-3">
                            <img  title = "Click on the image to set/reset default image" onClick={(e) => handleSetdefaultimage(item._id, item.is_active === "1" ? "0" : "1")} className={item.is_active === "1" ? "homeimageactive" : ""}
                              src={item.homeQuestions}
                              alt="Home"
                            />
                            <br />
                              <NavLink 
                              onClick={(e) => handleDeletehomeimage(item._id)} 
                              to="#" className="text-center " type="button"><i className="fa fa-trash-o me-2" aria-hidden="true"></i>Delete</NavLink>
                          </div>
                        )
                      })

                      : (
                        ""
                      )}
                    </div>
                  </div>
                  <hr className="mt-2"/>

                  <div className="row mt-3">
                    <div className="col-md-4">
                      <label className="btn btn-primary btn-lg w-100 mb-3 documentlabel d-inline-block">
                        <input
                          id="REPC_real_estate_purchase_contract"
                          type="file"
                          accept={acceptedFileTypes
                            .map((type) => `.${type}`)
                            .join(",")}
                          name="file"
                          onChange={handleFileUploadStats}
                          // value={getContact.file}
                        />
                        <i className="fi-plus me-2"></i> Stats Backgroung Image
                      </label>
                    </div>

                    <div className="col-md-8 d-flex align-item-center">
                      {console.log(imageData.HomeStats)}
                      {imageData.HomeStats && imageData.HomeStats.length > 0 ? 
                      imageData.HomeStats.map((item) => {
                        return (
                          <div className="me-3">
                            <img  title = "Click on the image to set/reset default image" onClick={(e) => handleSetdefaultimage(item._id, item.is_active === "1" ? "0" : "1")} className={item.is_active === "1" ? "homeimageactive" : ""}
                              src={item.homeStats}
                              alt="Home"
                            />
                            <br />
                              <NavLink 
                              onClick={(e) => handleDeletehomeimage(item._id)} 
                              to="#" className="text-center " type="button"><i className="fa fa-trash-o me-2" aria-hidden="true"></i>Delete</NavLink>
                          </div>
                        )
                      })
                      : (
                        ""
                      )}
                    </div>
                  </div>
                  <hr className="mt-2"/>

                  <div className="row mt-3">
                    <div className="col-md-4">
                      <label className="btn btn-primary btn-lg w-100 mb-3 documentlabel d-inline-block">
                        <input
                          id="REPC_real_estate_purchase_contract"
                          type="file"
                          accept={acceptedFileTypes
                            .map((type) => `.${type}`)
                            .join(",")}
                          name="file"
                          onChange={handleFileUploadMenus}
                          // value={getContact.file}
                        />
                        <i className="fi-plus me-2"></i> Menu Backgroung Image
                      </label>
                    </div>

                    <div className="col-md-8 d-flex align-item-center">
                      {imageData?.HomeMenus &&
                      imageData?.HomeMenus.length > 0 ? 
                      imageData?.HomeMenus.map((item) => {
                        return (
                          <div className="me-3">
                            <img  title = "Click on the image to set/reset default image" onClick={(e) => handleSetdefaultimage(item._id, item.is_active === "1" ? "0" : "1")} className={item.is_active === "1" ? "homeimageactive" : ""}
                              src={item.homeMenus}
                              alt="Home"
                            />
                            <br />
                              <NavLink 
                              onClick={(e) => handleDeletehomeimage(item._id)} 
                              to="#" className="text-center " type="button"><i className="fa fa-trash-o me-2" aria-hidden="true"></i>Delete</NavLink>
                          </div>
                        )
                      })
                      : (
                        ""
                      )}
                    </div>
                  </div>
                  <hr className="mt-2"/>


                  <div className="row mt-3">
                    <div className="col-md-4">
                      <label className="btn btn-primary btn-lg w-100 mb-3 documentlabel d-inline-block">
                        <input
                          id="REPC_real_estate_purchase_contract"
                          type="file"
                          accept={acceptedFileTypes
                            .map((type) => `.${type}`)
                            .join(",")}
                          name="file"
                          onChange={handleFileUploadCalender}
                          // value={getContact.file}
                        />
                        <i className="fi-plus me-2"></i>Calender Backgroung Image
                      </label>
                    </div>

                    <div className="col-md-8 d-flex align-item-center">
                      {imageData.HomeCalender &&
                      imageData.HomeCalender.length > 0 ? 
                      imageData.HomeCalender.map((item) => {
                        return (
                          <div className="me-3">
                            <img  title = "Click on the image to set/reset default image" onClick={(e) => handleSetdefaultimage(item._id, item.is_active === "1" ? "0" : "1")} className={item.is_active === "1" ? "homeimageactive" : ""}
                              src={item.homeCalender}
                              alt="Home"
                            />
                            <br />
                              <NavLink 
                              onClick={(e) => handleDeletehomeimage(item._id)} 
                              to="#" className="text-center " type="button"><i className="fa fa-trash-o me-2" aria-hidden="true"></i>Delete</NavLink>
                          </div>
                        )
                      })
                      : (
                        ""
                      )}
                    </div>
                  </div>
                  <hr className="mt-2"/>

                  <div className="row mt-3">
                    <div className="col-md-4">
                      <label className="btn btn-primary btn-lg w-100 mb-3 documentlabel d-inline-block">
                        <input
                          id="REPC_real_estate_purchase_contract"
                          type="file"
                          accept={acceptedFileTypes
                            .map((type) => `.${type}`)
                            .join(",")}
                          name="file"
                          onChange={handleFileUploadBirthdays}
                          // value={getContact.file}
                        />
                        <i className="fi-plus me-2"></i> Birthdays Backgroung Image
                      </label>
                    </div>

                    <div className="col-md-8 d-flex align-item-center">
                      {imageData.HomeBirthdays &&
                      imageData.HomeBirthdays.length > 0 ? 
                      imageData.HomeBirthdays.map((item) => {
                        return (
                          <div className="me-3">
                            <img  title = "Click on the image to set/reset default image" onClick={(e) => handleSetdefaultimage(item._id, item.is_active === "1" ? "0" : "1")} className={item.is_active === "1" ? "homeimageactive" : ""}
                              src={item.homeBirthdays}
                              alt="Home"
                            />
                            <br />
                              <NavLink 
                              onClick={(e) => handleDeletehomeimage(item._id)} 
                              to="#" className="text-center " type="button"><i className="fa fa-trash-o me-2" aria-hidden="true"></i>Delete</NavLink>
                          </div>
                        )
                      })
                      : (
                        ""
                      )}
                    </div>
                  </div>
                  <hr className="mt-2"/>

                  <div className="row mt-3">
                    <div className="col-md-4">
                      <label className="btn btn-primary btn-lg w-100 mb-3 documentlabel d-inline-block">
                        <input
                          id="REPC_real_estate_purchase_contract"
                          type="file"
                          accept={acceptedFileTypes
                            .map((type) => `.${type}`)
                            .join(",")}
                          name="file"
                          onChange={handleFileUploadAssociates}
                          // value={getContact.file}
                        />
                        <i className="fi-plus me-2"></i> Associates  Backgroung Image
                      </label> 

                    </div>
                    <div className="col-md-8 d-flex align-item-center">
                      {imageData.HomeAssociates &&
                      imageData.HomeAssociates.length > 0 ? 
                      imageData.HomeAssociates.map((item) => {
                        return (
                          <div className="me-3">
                            <img  title = "Click on the image to set/reset default image" onClick={(e) => handleSetdefaultimage(item._id, item.is_active === "1" ? "0" : "1")} className={item.is_active === "1" ? "homeimageactive" : ""}
                              src={item.homeAssociates}
                              alt="Home"
                            />
                            <br />
                              <NavLink 
                              onClick={(e) => handleDeletehomeimage(item._id)} 
                              to="#" className="text-center " type="button"><i className="fa fa-trash-o me-2" aria-hidden="true"></i>Delete</NavLink>
                          </div>
                        )
                      })
                       : (
                        ""
                      )}
                    </div>
                  </div>
                </div>
              </div>
            </div>
            {/* <div className="col-md-4">

                        </div>  */}
          </div>
        </div>
      </main>
    </div>
  );
}
export default HomePagePhoto;

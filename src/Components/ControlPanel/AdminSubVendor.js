import React, { useState, useEffect } from "react";
import { NavLink, useParams, useNavigate } from "react-router-dom";
import user_service from "../service/user_service";
import Loader from "../../Pages/Loader/Loader.js";
import Toaster from "../../Pages/Toaster/Toaster.js";
import PromoteImage from "../img/image.jpg";
import Swal from "sweetalert2";
import "sweetalert2/dist/sweetalert2.css";

const AdminSubVendor = () => {
  const [loader, setLoader] = useState({ isActive: null });
  const { isActive } = loader;
  const [toaster, setToaster] = useState({
    types: null,
    isShow: null,
    toasterBody: "",
    message: "",
  });
  const { types, isShow, toasterBody, message } = toaster;
  const params = useParams();

  const [productItem, setProductItem] = useState([]);
  const [productItemData, setProductItemData] = useState("");
  const [products, setProducts] = useState([]);
  const navigate = useNavigate();

  useEffect(() => {
    window.scrollTo(0, 0);
    setLoader({ isActive: true });
    const ProductItem = async () => {
      await user_service.subVendorItemGet(params.id).then((response) => {
        setLoader({ isActive: false });
        if (response) {
            console.log(response);
          setProductItem(response.data.data);
        //   const newProductdata = response.data.data;
        //   setProductItemData(newProductdata?.[0]?.category || "");
        }
      });
    };

    ProductItem();
  }, []);

  const handleRemoveProduct = async (id) => {
    if (id) {
      const confirmation = await Swal.fire({
        title: "Are you sure?",
        text: "It will be permanently deleted!",
        icon: "warning",
        showCancelButton: true,
        confirmButtonColor: "#3085d6",
        cancelButtonColor: "#d33",
        confirmButtonText: "Yes, delete it!",
      });
  
      if (confirmation.isConfirmed) {
        setLoader({ isActive: true });
        await user_service.DeletepromotoProducts(id).then((response) => {
          if (response) {
            setLoader({ isActive: false });
            setToaster({
              type: "success",
              isShow: true,
              message: "PromotoProduct deleted Successfully",
            });
            setTimeout(() => {
              setToaster((prevToaster) => ({
                ...prevToaster,
                isShow: false,
              }));
              navigate(`/admin/promote/`);
            }, 1000);
          }
        });
      }
    }
  };

  

  useEffect(() => {
    window.scrollTo(0, 0);
    if (params.id) {
      const PromoteProductGetIdId = async () => {
        await user_service.promotoProductsGetId(params.id).then((response) => {
          setLoader({ isActive: false });
          if (response) {
            setProducts(response.data);
          }
        });
      };
      PromoteProductGetIdId();
    }
  }, []);


  const handleDeleteProduct = async (id) => {
    if (id) {
      const confirmation = await Swal.fire({
        title: "Are you sure?",
        text: "It will be permanently deleted!",
        icon: "warning",
        showCancelButton: true,
        confirmButtonColor: "#3085d6",
        cancelButtonColor: "#d33",
        confirmButtonText: "Yes, delete it!",
      });
  
      if (confirmation.isConfirmed) {
        setLoader({ isActive: true });
        await user_service.subCategoryVendorDelete(id).then((response) => {
          if (response) {
            setLoader({ isActive: false });
            setToaster({
              type: "success",
              isShow: true,
              message: "Vendor Items deleted Successfully",
            });
            setTimeout(() => {
              setToaster((prevToaster) => ({
                ...prevToaster,
                isShow: false,
              }));
              navigate(`/admin/promote/`);
            }, 1000);
          }
        });
      }
    }
  };
 
  return (
    <div className="bg-secondary float-left w-100 pt-4 mb-4">
      <Loader isActive={isActive} />
      {isShow && (
        <Toaster
          types={types}
          isShow={isShow}
          toasterBody={toasterBody}
          message={message}
        />
      )}
      <main className="page-wrapper profile_page_wrap">
        <div className="product_vendors content-overlay">
          <div className="float-left w-100 d-flex align-items-center justify-content-between mb-4">
            <h3 className="mb-0 text-white">Promote Products/Vendors</h3>
            <span className="pull-right">
              <NavLink
                to={`/add-subVendor/${params.id}`}
                className="btn btn-primary pull-right"
                type="button"
              >
                Add New Sub Category
              </NavLink>
            </span>
          </div>
        </div>
        <div className="">
          <div className="FeaturedBox promoted_product float-left w-100">
            <div className="row">
              {products.title ? (
                <h2 className="mt-0 text-white">{products.title}</h2>
              ) : (
                ""
              )}
              {productItem && productItem.length > 0 ? (
                productItem.map((items) => (
                  <div
                    // to={`/add-markting/${params.id}/${items._id}`}
                    className="col-lg-3 col-md-6 col-sm-6 col-12 tns-item tns-slide-cloned tns-slide-active p-3"
                  >
                    <div className="card shadow-sm cursor-pointer card-hover border-0 h-100">
                      <div className="card-img-top card-img-hover">
                        {items.image.length > 0 ? (
                          <>
                            <NavLink to={`/admin-product-items/${params.id}/${items._id}`}>
                            <img
                              className="img-fluid rounded-0 w-100"
                              src={items.image}
                              alt="images"
                            />
                            </NavLink>
                          </>
                        ) : (
                          <img
                            className="img-fluid rounded-0 w-100"
                            src={PromoteImage}
                            alt="image"
                          />
                        )}
                      </div>
                      <div className="card-body position-relative pb-3">
                        <p className="mb-0 fs-sm text-muted">
                          {items.title}
                          <br />
                          {items.description}
                        </p>
                        <div className="row mt-3">
                          <div className="col-lg-6 col-md-6 col-sm-6 col-6">
                            <NavLink
                              type="button"
                              className="text-center "
                              to={`/add-subVendor/${params.id}/${items._id}`}
                            >
                              <i className="fi-edit me-2"></i>
                              Edit
                            </NavLink>
                          </div>
                          <div className="col-lg-6 col-md-6 col-sm-6 col-6">
                            <a
                              type="button"
                              aria-current="page"
                              className="text-center  active"
                              onClick={() => handleDeleteProduct(items._id)}
                            >
                              <i
                                className="fa fa-trash-o me-2"
                                aria-hidden="true"
                              ></i>
                              Delete
                            </a>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                ))
              ) : (
                <p className="float-left w-100 text-center text-light">
                  No products found, please check back later
                </p>
              )}
            </div>
          </div>
        </div>
        <div className="mt-4">
          {/* <span className="pull-left">
            <NavLink
              to={`/promote`}
              className="btn btn-secondary pull-right ms-3"
              type="button"
            >
              Promote
            </NavLink>
            <NavLink
              to={`/admin/promote`}
              className="btn btn-secondary pull-right ms-3"
              type="button"
            >
              Category List
            </NavLink>
            <NavLink
              to={`/add-markting/${params.id}`}
              className="btn btn-primary pull-right"
              type="button"
            >
              Add New
            </NavLink>
          </span> */}
          <span className="pull-right">
            <>
              <button
                className="btn btn-secondary pull-right ms-3"
                type="button"
                onClick={() => handleRemoveProduct(products._id)}
              >
                Delete
              </button>
              <NavLink
                className="btn btn-primary pull-right ms-3"
                type="button"
                to={`/add-promotecatalog/${products._id}`}
              >
                Edit
              </NavLink>
              {/* { console.log(items.category_id)} */}
            </>
          </span>
        </div>
      </main>
    </div>
  );
};

export default AdminSubVendor;

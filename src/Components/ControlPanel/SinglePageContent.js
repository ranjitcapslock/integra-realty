import React, { useState, useEffect, useRef } from "react";
import _ from 'lodash';
import user_service from '../service/user_service';
import jwt from "jwt-decode";
import Loader from "../../Pages/Loader/Loader.js";
import Toaster from "../../Pages/Toaster/Toaster.js";
import { useNavigate, NavLink, useParams } from "react-router-dom";

function SinglePageContent() {
 
    const [formValues, setFormValues] = useState("");
    

    const [loader, setLoader] = useState({ isActive: null })
    const { isActive } = loader;
    const [toaster, setToaster] = useState({ types: null, isShow: null, toasterBody: "", message: "" });
    const { types, isShow, toasterBody, message } = toaster;



    const navigate = useNavigate();
    const params = useParams();
   
    const handleBack = () => {
        navigate("/control-panel/documents/")
    }


    useEffect(() => { window.scrollTo(0, 0);
        if (params.id) {
            setLoader({ isActive: true });
            const OfficesharedDocContenGetByid = async () => {
                await user_service.OfficesharedDocContenGet(params.id).then((response) => {
                    setLoader({ isActive: false });
                    if (response) {
                        console.log(response);
                        setFormValues(response.data)
                   
                    }
                });
            };
            OfficesharedDocContenGetByid();
        }
    }, [params.id]);


    return (
        <div className="bg-secondary float-left w-100 pt-4">
            <Loader isActive={isActive} />
            {isShow && <Toaster types={types} isShow={isShow} toasterBody={toasterBody} message={message} />}
            <div className="container">
                <div className="justify-content-center pb-sm-2">
                    <h2>{formValues.title}</h2>
                    <hr className="mt-2" />

                    <button className="btn btn-secondary pull-right mt-2" type="button" onClick={handleBack}>Manage Docs</button>
                    <div className="bg-light rounded-3 p-4 mb-3">
                        <hr className="mt-5" />

                        <div className="add_listing shadow-sm w-100 m-auto p-4">
                            <div className="row mt-5">
                            <p dangerouslySetInnerHTML={{ __html: formValues.content }}></p>
                            </div>

                        </div>

                      

                    </div>
                </div>
            </div>
        </div >
    )
}
export default SinglePageContent;

import React, { useState, useEffect } from "react";
import _ from 'lodash';
import user_service from '../service/user_service';
import Loader from "../../Pages/Loader/Loader.js";
import Toaster from "../../Pages/Toaster/Toaster.js";
import { NavLink } from "react-router-dom";




function MemberPlan() {

    const [loader, setLoader] = useState({ isActive: null })
    const { isActive } = loader;
    const [toaster, setToaster] = useState({ types: null, isShow: null, toasterBody: "", message: "" });
    const { types, isShow, toasterBody, message } = toaster;

    const [memberPlanGet, setMemberPlanget] = useState([])
    useEffect(() => { window.scrollTo(0, 0);
        setLoader({ isActive: true });

        const MembershipPlan = async () => {
            await user_service.membershipPlanGet().then((response) => {
                setLoader({ isActive: false });
                if (response) {
                    setMemberPlanget(response.data);
                }
            });
        };

        MembershipPlan();
    }, []);



    return (
        <div className="bg-secondary float-left w-100 pt-4">
        <Loader isActive={isActive} />
        {isShow && <Toaster types={types} isShow={isShow} toasterBody={toasterBody} message={message} />}
        <main className="page-wrapper profile_page_wrap">
            <div className="container content-overlay">
                <div className="justify-content-center pb-sm-2">
                    <div className="">
                    <h2>Member Plans</h2>
                    <hr className="mt-2" />
                    <NavLink to="/control-panel/member-plan/add/" className="btn btn-primary pull-right mt-2">Add New Member Plan</NavLink>

                    {/* <button className="btn btn-secondary pull-right" type="button" onClick={handleBack}>Cancel & Go Back</button> */}
                    <div className="bg-light rounded-3 p-4 mb-3">
                        <hr className="mt-5" />
                        <div className="add_listing shadow-sm w-100 m-auto p-4">
                          
                            <div className="row mt-5">
                                <h6>Below are the active member plans with number of associates and transactions with in the plan</h6>
                                {
                                    memberPlanGet && memberPlanGet.data
                                        ? memberPlanGet.data.map((item) => (
                                            <div className="row">
                                                <div className="col-md-12" key={item.id}>
                                                    <p className="mt-5"><span className="h4">{item.plan_name} </span> - <span className="h6">{item.office}</span></p>
                                                    <span>Associates: 0</span><br/>
                                                    <span>Transactions:: 0</span>

                                                    <button className="btn btn-secondary pull-right ms-5 mt-0">View Details</button>
                                                    <NavLink className="pull-right"  to={`/control-panel/member-plan/add/${item._id}`}>Edit</NavLink>
                                                </div> <hr className="mt-5" />
                                            </div>
                                        ))
                                        : null
                                }


                            </div>

                        </div>
                    </div>
                </div>
            </div>
            </div>
            </main>
        </div>
    )
}
export default MemberPlan;
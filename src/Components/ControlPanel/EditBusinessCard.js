import React, { useState, useEffect, useRef } from "react";
import Loader from "../../Pages/Loader/Loader.js";
import Toaster from "../../Pages/Toaster/Toaster.js";
import { NavLink, useNavigate, useParams } from "react-router-dom";
import _ from "lodash";

import "react-select2-wrapper/css/select2.css";
import jwt from "jwt-decode";
import axios from "axios";

import user_service from "../service/user_service";

const socialMediaTypes = [
  { type: "Twitter", linkurl: "" },
  { type: "Linkedin", linkurl: "" },
  { type: "Facebook", linkurl: "" },
  { type: "Instagram", linkurl: "" },
  { type: "Tumblr", linkurl: "" },
  { type: "YouTube Channel", linkurl: "" },
  { type: "Pinterest", linkurl: "" },
  { type: "TikTok", linkurl: "" },
  { type: "Vimeo", linkurl: "" },
];
function EditBusinessCard() {
  const initialValues = {
    office_id: localStorage.getItem("active_office_id"),
    office_name: localStorage.getItem("active_office"),
    firstName: "",
    lastName: "",
    phone: "",
    email: "",
    web: "",
    social_links: "",
    appointment: "",
    location: "",
    about_me: "",
    cover_image: "",
    designations:"",
  };
  const params = useParams();
  console.log(params.id);
  const navigate = useNavigate();
  const [formValues, setFormValues] = useState(initialValues);
  const [getContact, setGetContact] = useState("");
  const [socialLinks, setSocialLinks] = useState(socialMediaTypes);
  const [fileExtension, setFileExtension] = useState("");
  const [acceptedFileTypes, setAcceptedFileTypes] = useState([]);
  const [showSubmitButton, setShowSubmitButton] = useState(false);

  const [data, setData] = useState("");
  const [loader, setLoader] = useState({ isActive: null });
  const { isActive } = loader;
  const [toaster, setToaster] = useState({
    types: null,
    isShow: null,
    toasterBody: "",
    message: "",
  });
  const { types, isShow, toasterBody, message } = toaster;
  const [isSubmitClick, setISSubmitClick] = useState(false);
  const [formErrors, setFormErrors] = useState({});
  const [file, setFile] = useState(null);
  //   const [fileExtension, setFileExtension] = useState('');
  //   const [acceptedFileTypes, setAcceptedFileTypes] = useState([]);
  //   const [showSubmitButton, setShowSubmitButton] = useState(false);

  const handleChange = (e) => {
    const { name, value } = e.target;
    setFormValues({ ...formValues, [name]: value });
  };

  const handleChangeSocial = (event, index) => {
    const { value } = event.target;
    setSocialLinks((prevSocialLinks) => {
      const updatedSocialLinks = [...prevSocialLinks];
      updatedSocialLinks[index] = {
        ...updatedSocialLinks[index],
        linkurl: value,
      };
      return updatedSocialLinks;
    });
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
   // console.log(formValues)
    const userData = {
      office_id: localStorage.getItem("active_office_id"),
      office_name: localStorage.getItem("active_office"),
      agentId: jwt(localStorage.getItem("auth")).id,
      firstName: formValues.firstName,
      lastName: formValues.lastName,
      phone: formValues.phone,
      email: formValues.email,
      appointment: formValues.appointment,
      web: formValues.web,
      location: formValues.location,
      social_links: socialLinks,
      about_me: formValues.about_me,
      designations: formValues.designations,
      cover_image: data ? data.toString() : formValues.cover_image,
    };
    console.log("userData", userData);
    try {
      setLoader({ isActive: true });
      const response = await user_service.contactUpdate(params.id, userData);
      //    console.log(response.data.nickname);
      setFormValues(response.data);
      setToaster({
        type: "Profile Updated Successfully",
        isShow: true,
        toasterBody: response.data.message,
        message: "Profile Updated Successfully",
      });
 
      setTimeout(() => {
        setToaster((prevToaster) => ({ ...prevToaster, isShow: false }));
        navigate(`/contact/${response.data.nickname}`);
      }, 3000);
      ContactGetById(params.id);
      setLoader({ isActive: false });
    } catch (error) {
      setLoader({ isActive: false });
      setToaster({
        types: "error",
        isShow: true,
        toasterBody: error.response
          ? error.response.data.message
          : error.message,
        message: "Error",
      });

      setTimeout(() => {
        setToaster((prevToaster) => ({ ...prevToaster, isShow: false }));
      }, 1000);
    }
  };

  useEffect(() => {
    ContactGetById(params.id);
  }, []);
  const ContactGetById = async () => {
    try {
      const response = await user_service.contactGetById(params.id);
      const contactData = response.data;
      console.log(contactData);
      setFormValues(contactData);

      if (contactData.social_links.length === 0) {
        setFormValues([]);
      } else {
        setSocialLinks(contactData.social_links);
      }
    } catch (error) {
      console.error("Error fetching contact data:", error);
    }
  };

  //   const handleFileUpload = async (e) => {
  //     const selectedFile = e.target.files[0];

  //     if (selectedFile) {
  //         setFile(selectedFile);
  //         setFileExtension(selectedFile.name.split('.').pop());
  //         setShowSubmitButton(true);

  //         const formData = new FormData();
  //         formData.append('file', selectedFile);

  //         const config = {
  //             headers: {
  //                 Accept: 'application/json',
  //                 'Content-Type': 'multipart/form-data',
  //                 Authorization: `Bearer ${localStorage.getItem('auth')}`,
  //             },
  //         };

  //         try {

  //             const uploadResponse = await axios.post('https://api.brokeragentbase.com/upload', formData, config);
  //             const uploadedFileData = uploadResponse.data;
  //             console.log(uploadedFileData);
  //             setData(uploadedFileData);
  //         } catch (error) {
  //             console.error('Error occurred during file upload:', error);
  //             setLoader({ isActive: false });
  //         }
  //     }
  // };

  const handleFileUpload = async (e) => {
    const selectedFile = e.target.files[0];

    if (selectedFile) {
      setFile(selectedFile);
      setFileExtension(selectedFile.name.split(".").pop());
      setShowSubmitButton(true);

      const formData = new FormData();
      formData.append("file", selectedFile);

      const config = {
        headers: {
          Accept: "application/json",
          "Content-Type": "multipart/form-data",
          Authorization: `Bearer ${localStorage.getItem("auth")}`,
        },
      };

      try {
        const uploadResponse = await axios.post(
          "https://api.brokeragentbase.com/upload",
          formData,
          config
        );
        const uploadedFileData = uploadResponse.data;
        console.log(uploadedFileData);
        setData(uploadedFileData);
      } catch (error) {
        console.error("Error occurred during file upload:", error);
        setLoader({ isActive: false });
      }
    }
  };

  return (
    <div className="">
    <div className="bg-secondary float-left w-100 pt-4 mb-4">
      <Loader isActive={isActive} />
      {isShow && (
        <Toaster
          types={types}
          isShow={isShow}
          toasterBody={toasterBody}
          message={message}
        />
      )}
      <div className="">
        <main className="page-wrapper profile_page_wrap">
          <div className="float-left w-100 d-flex align-items-center justify-content-start mb-4">
              <h3 className="mb-0 text-white">Update Business Card</h3>
          </div>
          {/* <hr className="mt-2" /> */}
          <div className="row">
            <div className="col-md-12">
              <div className="bg-light rounded-3 p-3 border mb-3">
                <div className="row">
                  <div className="col-md-6">
                    <div className="col-md-12">
                      <div className="mb-3">
                        <label className="form-label">First Name</label>
                        {/* <br /> */}
                        <input
                          className="form-control"
                          type="text"
                          id="text-input"
                          name="firstName"
                          placeholder="enter a first name"
                          onChange={(event) => handleChange(event)}
                          autoComplete="on"
                          value={formValues.firstName}
                        />
                      </div>
                    </div>
                    <div className="col-md-12">
                      <div className="mb-3">
                        <label className="form-label">Last Name</label>
                        <input
                          className="form-control"
                          type="text"
                          id="text-input"
                          name="lastName"
                          placeholder="enter a last name"
                          onChange={(event) => handleChange(event)}
                          autoComplete="on"
                          value={formValues.lastName}
                        />
                      </div>
                    </div>
                    <div className="col-md-12">
                      <div className="mb-3">
                        <label className="form-label">Designation</label>
                        <input
                          className="form-control"
                          type="text"
                          id="text-input"
                          name="designations"
                          placeholder="Enter designation"
                          onChange={(event) => handleChange(event)}
                          autoComplete="on"
                          value={formValues.designations}
                        />
                      </div>
                    </div>
                    
                  </div>

                  <div className="col-md-6">
                    <div className="col-md-12">
                      <div className="mb-3">
                        <label className="form-label">Cover Image</label>
                        <input
                          className="file-uploader file-uploader-grid"
                          id="inline-form-input"
                          type="file"
                          name="cover_image"
                          onChange={handleFileUpload}
                        />
                        {formValues?.cover_image ? (
                          <>
                            <img
                              className="mt-3"
                              src={formValues?.cover_image}
                            />
                          </>
                        ) : (
                          ""
                        )}
                      </div>
                    </div>
                    <div className="col-md-12">
                      <div className="mb-3">
                        <label className="form-label">Email</label>
                        <input
                          className="form-control"
                          type="text"
                          id="text-input"
                          name="email"
                          placeholder="enter a valid email"
                          onChange={(event) => handleChange(event)}
                          autoComplete="on"
                          value={formValues.email}
                        />
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div className="col-md-12 mt-4">
                <h6 className="text-white mb-4">Media Info (Optional)</h6>
              </div>
              <div className="bg-light rounded-3 p-3 border mb-3">
                <div className="row mt-3">
                  <div className="col-md-6">
                    <div className="mb-3">
                      <label className="form-label">Phone</label>
                      <input
                        className="form-control"
                        type="tel"
                        id="tel-input"
                        name="phone"
                        placeholder="Enter Phone"
                        onChange={(event) => handleChange(event)}
                        autoComplete="on"
                        pattern="[0-9]+"
                        title="Please enter a valid phone number"
                        value={formValues.phone}
                      />
                    </div>
                  </div>
                  <div className="col-md-6">
                    <div className="mb-3">
                      <label className="form-label">Website</label>
                      <input
                        className="form-control"
                        type="text"
                        id="text-input"
                        name="web"
                        placeholder="http//"
                        onChange={(event) => handleChange(event)}
                        value={formValues.web}
                      />
                    </div>
                  </div>
                  <div className="col-md-6">
                    <div className="mb-3">
                      <label className="form-label">Location</label>
                      <input
                        className="form-control"
                        type="text"
                        id="text-input"
                        name="location"
                        placeholder="enter a valid email"
                        onChange={(event) => handleChange(event)}
                        autoComplete="on"
                        value={formValues.location}
                      />
                    </div>
                  </div>
                  <div className="col-md-6">
                    <div className="mb-3">
                      <label className="form-label">Appointment</label>
                      <input
                        className="form-control"
                        type="text"
                        id="text-input"
                        name="appointment"
                        placeholder="http//"
                        onChange={(event) => handleChange(event)}
                        value={formValues.appointment}
                      />
                    </div>
                  </div>
                </div>
              </div>
              

            <div className="col-md-12 mt-3">
                  <h6 className="text-white">Social media links</h6>
              <div className="bg-light rounded-3 p-3 border mb-3">
                <div className="row">
                  {socialLinks.map((socialLink, index) => (
                    <div className="col-md-6 mb-3" key={index}>
                      <label className="form-label">
                        {socialLink.type} URL
                      </label>
                      <input
                        className="form-control"
                        type="text"
                        name="linkurl"
                        value={socialLink.linkurl}
                        onChange={(event) => handleChangeSocial(event, index)}
                      />
                    </div>
                  ))}
                </div>
              </div>
            </div>

            <div className="col-md-12 mt-3">
                  <h6 className="text-white">Business</h6>
            </div>
              <div className="bg-light rounded-3 p-3 border mb-3">
                <div className="">
                    <label className="form-label">About me</label>
                    <textarea
                      className="form-control mt-0"
                      id="textarea-input"
                      rows="5"
                      name="about_me"
                      onChange={handleChange}
                      autoComplete="on"
                      value={formValues.about_me}
                    ></textarea>
                  </div>
                </div>
              </div>



            <div className="col-md-12">
              <div className="pull-right mt-3">
                <button
                  className="btn btn-primary pull-right"
                  type="button"
                  onClick={handleSubmit}
                  // disabled={show}
                >
                  {/* {show ? "Please wait" : "Save Details"} */}
                  Save Details
                </button>
              </div>
            </div>
          </div>
        </main>
      </div>
    </div>
    </div>

  );
}
export default EditBusinessCard;

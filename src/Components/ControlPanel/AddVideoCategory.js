import React, { useState, useEffect } from "react";
// import avtar from "./Components/img/avtar.jpg";
import _ from "lodash";
import user_service from "../service/user_service";
import jwt from "jwt-decode";
import Loader from "../../Pages/Loader/Loader.js";
import Toaster from "../../Pages/Toaster/Toaster.js";
import { NavLink, useNavigate, useParams } from "react-router-dom";
import swal from "sweetalert2";
import "sweetalert2/dist/sweetalert2.css";
// import youtubeicon from "../../Components/img/Youtube Icon.png";
// import uploadvideoicon from "../../Components/img/VideoUpload-dark.png";
// import uploadingicon from "../../Components/img/uploadiiiing.gif";
import $, { each } from "jquery";

import axios from "axios";

function AddVideoCategory() {
  const initialValues = {
    // title: "", shortdescription: "", description: "", playlist: "", thumbnailimg: "", is_active: "1"
    title: "",
    shortdescription: "",
    description: "",
    videoCategory: "",
    thumbnail_image: "",
    is_active: "1",
    // video_url: video_url,
  };

  const url = window.location.pathname.split("/").pop();

  const [formValues, setFormValues] = useState(initialValues);
  const [formErrors, setFormErrors] = useState({});
  const [isSubmitClick, setISSubmitClick] = useState(false);
  const [isSubmitClickNew, setISSubmitClickNew] = useState(false);

  const [loader, setLoader] = useState({ isActive: null });
  const { isActive } = loader;
  const [toaster, setToaster] = useState({
    types: null,
    isShow: null,
    toasterBody: "",
    message: "",
  });
  const { types, isShow, toasterBody, message } = toaster;

  const [checkData, setCheckData] = useState("");
  const navigate = useNavigate();
  const params = useParams();

  console.log(params.id);
  console.log(url);

  const [thumbnail_created_array, setThumbnail_created_array] = useState([]);
  // onChange Function start
  const handleChange = (e) => {
    const { name, value } = e.target;
    setFormValues({ ...formValues, [name]: value });
  };
  // onChange Function end

  const handleFileUpload = async (e, id) => {
    const selectedFile = e.target.files[0];
    //setFile(selectedFile);

    if (selectedFile) {
      var fileExtension = selectedFile.name.split(".").pop();
      if (fileExtension != "mp4") {
        // swal("", "Video File should be in mp4 format.", "error");
        // swalpopup("Video File should be in mp4 format.", 'error')
        alert("Video File should be in mp4 format.", "error");
        return;
      }
      setUploading(true);
      const formData = new FormData();
      formData.append("file", selectedFile);
      const config = {
        headers: {
          Accept: "application/json",
          "Content-Type": "multipart/form-data",
          Authorization: `Bearer ${localStorage.getItem("auth")}`,
        },
      };
      try {
        // setLoader({ isActive: true })
        const uploadResponse = await axios.post(
          "https://api.brokeragentbase.com/upload",
          formData,
          config
        );
        const uploadedFileData = uploadResponse.data;

        console.log(uploadedFileData);

        setExclusive_video_url(uploadedFileData);
        // if (e.target.name == "exclusive_video_url"){
        console.log("1");
        videoPreview(e.target.value);
        //}
        setUploading(false);
        setBtn_exclusive_upload_video(true);
        setStepon(true);
        // this.setState({
        //     // key: key,
        //     url: url,
        //     // challengevideo_key: key,
        //    usive_link: url,
        //     uploading: false,
        //     btn_exclusive_upload_video: true,
        //     stepon: true,
        // })

        // setLoader({ isActive: false })
        // setData(uploadedFileData);

        // const userData = {

        //     agentId: jwt(localStorage.getItem('auth')).id,
        //     marque_image: uploadedFileData,
        // };

        // console.log(userData);
        // try {
        //     const response = await user_service.bannerUpdate(itemId, userData);
        //     if (response) {
        //         console.log(response);
        //         const BannerList = async () => {
        //             await user_service.bannerGet().then((response) => {
        //                 setLoader({ isActive: false })
        //                 if (response) {
        //                     setGetBanner(response.data)
        //                 }
        //             });
        //         }
        //         BannerList()

        //         document.getElementById('closeModal').click();
        //     }
        // } catch (error) {
        //     console.log('Error occurred while uploading documents:', error);
        // }
      } catch (error) {
        console.error("Error occurred during file upload:", error);
      }
    }
  };

  const handleChangevideo = (e) => {
    $(".btn-select-file").addClass("disabled-file-uplaod");

    if (e.target.name == "exclusivevideo") {
      var file = $("#exclusivevideo")[0].files[0];
      var fileExtension = file.name.split(".").pop();
      if (fileExtension != "mp4") {
        // swal("", "Video File should be in mp4 format.", "error");
        // swalpopup("Video File should be in mp4 format.", 'error')
        alert("Video File should be in mp4 format.", "error");
        return;
      }
      var randomChars =
        "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
      var result = "";
      var length = 10;
      for (var i = 0; i < length; i++) {
        result += randomChars.charAt(
          Math.floor(Math.random() * randomChars.length)
        );
      }
      var filename = result + "." + fileExtension;

      const myNewFile = new File([file], filename, { type: file.type });
      //   var key = '';
      var url = "";
      setUploading(true);
      // this.setState({
      //     uploading: true
      // })

      // generateVideoThumbnails(myNewFile, "3", "file").then((res) => {
      //     this.setState({ thumbnail_created_array: res });

      // }).catch((Err) => {
      // })

      // put api function here to upload from backend

      // const results = this.props.dispatch(createsecuresignedurlData('challenge_videos',filename))
      //   results.then(data => {
      //   })

      // let request_audio;
      // request_audio = {
      //     method: 'get',
      //     url: `${process.env.REACT_APP_APIURL}/user/create-signed-url/exclusive/${filename}`,
      //     headers: {
      //         'Content-Type': 'application/json',
      //         'Content-type': 'video/mp4',
      //         'Content-type': 'multipart/form-data',
      //         'X-Auth-Token': `${localStorage.getItem('token')}`,
      //     },
      // };
      // axios(request_audio).then((data_url) => {
      //     const signed_url = data_url.data.Signed_url
      //     // post video to gcp

      //     var dataa = myNewFile;
      //     const request = new Request(signed_url, {
      //         method: "PUT",
      //         body: myNewFile,
      //         headers: new Headers({
      //             'Content-type': dataa.type,
      //         })
      //     });
      //     return fetch(request)
      //         .then(data => {
      //             url = `${process.env.REACT_APP_S3BUCKET_PLATFORNSPECIFIC}${this.state.bucket_name}${this.state.api_platform_shortname}/exclusive/${filename}`;
      //             this.setState({
      //                 // key: key,
      //                 url: url,
      //                 // challengevideo_key: key,
      //                 exclusive_video_url: url,
      //                 exclusive_link: url,
      //                 uploading: false,
      //                 btn_exclusive_upload_video: true,
      //                 stepon: true,
      //             })
      //         })
      //         .catch(err => {
      //             this.setState({ isSubmitVideo: false });
      //             this.setState({ video_loader: false })
      //         });
      // });
    }

    this.setState({
      [e.target.name]: e.target.value,
    });

    let errors = {};
    let formIsValid = true;
    if (this.state.exclusive_link) {
      formIsValid = false;
      errors["exclusive_link"] = "";
    }
    this.setState({ errors: errors });
  };

  {
    /* <!-- Form Validation Start--> */
  }
  useEffect(() => {
    if (formValues && isSubmitClick) {
      validate();
    }
  }, [formValues, url]);

  const validate = () => {
    const values = formValues;
    const errors = {};

    if (!values.title) {
      errors.title = "Title is required";
    }

    if (!values.shortdescription) {
      errors.shortdescription = "Short Description is required";
    }

    if (!values.description) {
      errors.description = "Description is required";
    }

    if (!values.videoCategory) {
      errors.videoCategory = "Video Category is required";
    }

    if (params.id) {
    } else {
      // If params.id is not present
      if (!values.thumbnailimg && !imgthumbnailfile) {
        errors.thumbnailimg = "Thumbnail image is required";
      }
    }

    // if (!values.thumbnail_image) {
    //     errors.thumbnail_image = "thumbnail_image is required";
    // }
    setFormErrors(errors);
    return errors;
  };
  {
    /* <!-- Form Validation End--> */
  }

  {
    /* <!--Api call Form onSubmit Start--> */
  }

  const handleSubmit = async (e) => {
    if (params.id) {
      e.preventDefault();
      setISSubmitClick(true);
      let checkValue = validate();
      if (_.isEmpty(checkValue)) {
        setISSubmitClickNew(true);

        const userData = {
          agentId: jwt(localStorage.getItem("auth")).id,
          title: formValues.title,
          shortdescription: formValues.shortdescription,
          description: formValues.description,
          // playlist: "videoTraining",
          videoCategory: formValues.videoCategory,
          thumbnail_image: imgthumbnailfile,
          is_active: formValues.is_active,
        };

        setLoader({ isActive: true });
        console.log(userData);

        await user_service
          .addVideocategoryUpdate(params.id, userData)
          .then((response) => {
            if (response) {
              setFormValues(response.data);
              setLoader({ isActive: false });
              setToaster({
                type: "Category Updated Successfully",
                isShow: true,
                toasterBody: response.data.message,
                message: "Category Updated Successfully",
              });
              setTimeout(() => {
                setToaster((prevToaster) => ({
                  ...prevToaster,
                  isShow: false,
                }));
                navigate(`/learnMedia/${formValues.videoCategory}`);
              }, 1000);
              setISSubmitClickNew(false);
            } else {
              setLoader({ isActive: false });
              setToaster({
                types: "error",
                isShow: true,
                toasterBody: response.data.message,
                message: "Error",
              });
            }
          });
      } else {
        console.log("validate");
      }
    } else {
      e.preventDefault();
      setISSubmitClick(true);
      let checkValue = validate();
      if (_.isEmpty(checkValue)) {
        setISSubmitClickNew(true);

        const userData = {
          agentId: jwt(localStorage.getItem("auth")).id,
          title: formValues.title,
          shortdescription: formValues.shortdescription,
          description: formValues.description,
          // playlist: "videoTraining",
          videoCategory: formValues.videoCategory,
          thumbnail_image: imgthumbnailfile,
          is_active: formValues.is_active,
        };

        setLoader({ isActive: true });
        await user_service.addVideocategory(userData).then((response) => {
          if (response) {
            setFormValues(response.data);
            setLoader({ isActive: false });
            setToaster({
              type: "Category Added Successfully",
              isShow: true,
              toasterBody: response.data.message,
              message: "Category Added Successfully",
            });
            setTimeout(() => {
              setToaster((prevToaster) => ({ ...prevToaster, isShow: false }));
              navigate(`/learnMedia/${formValues.videoCategory}`);
            }, 1000);
            setISSubmitClickNew(false);
            setISSubmitClick(false);
          } else {
            setLoader({ isActive: false });
            setToaster({
              types: "error",
              isShow: true,
              toasterBody: response.data.message,
              message: "Error",
            });
          }
        });
      } else {
        console.log("validate");
      }
    }
  };

  useEffect(() => {
    window.scrollTo(0, 0);

    const videoCategoryIdGetId = async () => {
      try {
        const response = await user_service.videoCategoryId(params.id);
        setLoader({ isActive: false });
        if (response) {
          setFormValues(response.data);
        }
      } catch (error) {
        console.error("Error fetching data:", error);
      }
    };

    videoCategoryIdGetId();
  }, [params.id]);

  const [btn_exclusive_upload_video, setBtn_exclusive_upload_video] =
    useState(false);
  const [exclusive_video_url, setExclusive_video_url] = useState("");
  const [uploading, setUploading] = useState(false);
  const [thisexclusive_type, setThisexclusive_type] = useState(1);
  const [exclusive_link, setExclusive_link] = useState(1);
  const [initial_state, setInitial_state] = useState(1);
  const [stepon, setStepon] = useState(1);
  const [btn_challenge_link, setBtn_challenge_link] = useState(1);
  const [videotype, setVideotype] = useState("custom");
  const [activeTab, setActiveTab] = useState("custom");

  const changeresetvideo = () => {
    setThisexclusive_type(1);
    setExclusive_video_url("");
    setExclusive_link(1);
    setUploading(false);
    setBtn_exclusive_upload_video(false);
    setInitial_state(1);
    setStepon(1);
  };

  const validateembededcode = (e) => {
    // console.log(e.target.value);
    // this.setState({ [e.target.name]: e.target.value });
    setExclusive_video_url(e.target.value);
    if (e.target.name == "exclusive_video_url") {
      console.log("1");
      videoPreview(e.target.value);
    }
    e.preventDefault();
  };

  const videoPreview = (framehtml) => {
    // var replacescript = framehtml.replace('<script async src="https://www.tiktok.com/embed.js"></script>', '');
    // var replacecss = replacescript.replace('style="max-width: 605px;min-width: 325px;"', '');
    //console.log("aaaaaaaaa")
    if (framehtml != "") {
      //console.log("qweqwe")
      if (/<iframe/.test(framehtml)) {
        console.log("yes 1");
        var a = framehtml.match(/<iframe/g).length;
        if (a == 1) {
          if (/<\/iframe>/.test(framehtml)) {
            if (/src="http/.test(framehtml)) {
              // this.setState({
              //     btn_challenge_link: true,
              // })
              setBtn_challenge_link(true);
            } else {
              swal("Oops", "Iframe Link is incorrect", "error");
              // this.setState({ exclusive_video_url: '' })
              setExclusive_video_url("");
            }
          } else {
            swal("Oops", "Iframe Link is incorrect", "error");
            // this.setState({ exclusive_video_url: '' })
            setExclusive_video_url("");
          }
        } else {
          swal("Oops", "You have enetered duplicate Iframe", "error");
          // this.setState({ challenge_audio_link: '' })
          setExclusive_video_url("");
        }
      } else {
        console.log("yes 2");
        var regExp =
          /^.*(youtu\.be\/|v\/|u\/\w\/|embed\/|watch\?v=|\&v=)([^#\&\?]*).*/;
        var match = framehtml.match(regExp);
        if (match && match[2].length == 11) {
          var url1 = match[2];
          // this.setState({ exclusive_video_url: '' })
          setExclusive_video_url("");
          setBtn_challenge_link(true);
          // this.setState({
          //     btn_challenge_link: true,
          // })
          console.log("here");
          // this.setState({ exclusive_video_url: `<iframe width="560" height="315" src="https://www.youtube.com/embed/${url1}?rel=0" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>` })
          setExclusive_video_url(
            `<iframe width="500" height="315" src="https://www.youtube.com/embed/${url1}?rel=0" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>`
          );
        } else {
          swal("Oops", "Link is incorrect", "error");
          // this.setState({ challenge_audio_link: '' })
          setExclusive_video_url("");
        }
      }
      console.log("qqqqqqqqq");
    }
  };

  const onsetvideotype = async (videotype) => {
    // Your async code here
    console.log(`Video type selected: ${videotype}`);
    setVideotype(videotype);
    // Perform any other actions related to the selected video type
  };

  const [imgthumbnailfile, setImgthumbnailfile] = useState("");
  const select_thumbnail = (e, thumb_image, serial) => {
    $(".thumb").removeClass("selected");
    $(".thumbnail_" + serial).addClass("selected");
    // this.setState({
    //     imgthumbnailfile: thumb_image,
    // })
    setImgthumbnailfile(thumb_image);
  };

  const [acceptedFileTypes, setAcceptedFileTypes] = useState([]);

  const handleFileUploadSide = async (e, id) => {
    const selectedFile = e.target.files[0];
    // setFile(selectedFile);

    if (selectedFile) {
      //setFileExtension(selectedFile.name.split('.').pop());

      const formData = new FormData();
      formData.append("file", selectedFile);
      const config = {
        headers: {
          Accept: "application/json",
          "Content-Type": "multipart/form-data",
          Authorization: `Bearer ${localStorage.getItem("auth")}`,
        },
      };
      try {
        setLoader({ isActive: true });
        const uploadResponse = await axios.post(
          "https://api.brokeragentbase.com/upload",
          formData,
          config
        );
        const uploadedFileData = uploadResponse.data;
        setLoader({ isActive: false });
        setImgthumbnailfile(uploadedFileData);

        // const userData = {

        //     agentId: jwt(localStorage.getItem('auth')).id,
        //     side_image: uploadedFileData
        // };

        // console.log(userData);
        // try {
        //     const response = await user_service.bannerUpdate(itemId, userData);
        //     if (response) {
        //         console.log(response);
        //         const BannerList = async () => {
        //             await user_service.bannerGet().then((response) => {
        //                 setLoader({ isActive: false })
        //                 if (response) {
        //                     setGetBanner(response.data)
        //                 }
        //             });
        //         }
        //         BannerList()

        //         document.getElementById('closeModalSide').click();
        //     }
        // } catch (error) {
        //     console.log('Error occurred while uploading documents:', error);
        // }
      } catch (error) {
        console.error("Error occurred during file upload:", error);
      }
    }
    setFormErrors((prevErrors) => ({
      ...prevErrors,
      thumbnailimg: "",
    }));
  };

  let i = 1;
  let thumb_i = 1;
  return (
    <div className="bg-secondary float-left w-100 pt-4 mb-4">
      <Loader isActive={isActive} />
      {isShow && (
        <Toaster
          types={types}
          isShow={isShow}
          toasterBody={toasterBody}
          message={message}
        />
      )}
      <main className="page-wrapper profile_page_wrap">
        <div className="content-overlay">
          <div className="d-flex align-items-center justify-content-start mb-4">
            {params.id ? (
              <h3 className="mb-0 text-white">Update a Category</h3>
            ) : (
              <h3 className="mb-0 text-white">Add A New Category</h3>
            )}
          </div>
          <div className="">
            <div className="bg-light rounded-3 border p-3 mb-3">
              <div className="">
                <>
                  {params.id ? (
                    <h5>Update Category Details</h5>
                  ) : (
                    <h5>Add Category Details</h5>
                  )}

                  <div className="col-md-12">
                    <label className="col-form-label">Title *</label>
                    <input
                      className="form-control"
                      id="inline-form-input"
                      type="text"
                      name="title"
                      placeholder="Add a title"
                      value={formValues.title}
                      onChange={handleChange}
                      style={{
                        border: formErrors?.title ? "1px solid red" : "",
                      }}
                    />
                    {formErrors.title && (
                      <div className="invalid-tooltip">{formErrors.title}</div>
                    )}
                  </div>

                  <div className="col-md-12 mt-3">
                    <label className="col-form-label">Short Description</label>
                    <input
                      className="form-control"
                      id="inline-form-input"
                      type="text"
                      placeholder="Add a short-description"
                      name="shortdescription"
                      value={
                        formValues.shortdescription
                          ? formValues.shortdescription
                          : ""
                      }
                      onChange={handleChange}
                      style={{
                        border: formErrors?.shortdescription
                          ? "1px solid red"
                          : "",
                      }}
                    />
                    {formErrors.shortdescription && (
                      <div className="invalid-tooltip">
                        {formErrors.shortdescription}
                      </div>
                    )}
                  </div>

                  <div className="col-md-12 mt-3">
                    <label className="col-form-label" for="pr-country">
                      Description
                    </label>
                    <input
                      className="form-control"
                      id="inline-form-input"
                      type="text"
                      name="description"
                      placeholder="Add a description"
                      value={formValues.description}
                      onChange={handleChange}
                      style={{
                        border: formErrors?.description ? "1px solid red" : "",
                      }}
                    />
                    {formErrors.description && (
                      <div className="invalid-tooltip">
                        {formErrors.description}
                      </div>
                    )}
                  </div>

                  <div className="col-md-12 mt-3">
                    <label className="col-form-label" for="pr-country">
                      Video Resource
                    </label>
                    <select
                      className="form-select"
                      id="pr-country"
                      name="videoCategory"
                      value={formValues.videoCategory}
                      onChange={handleChange}
                      style={{
                        border: formErrors?.videoCategory
                          ? "1px solid red"
                          : "",
                      }}
                    >
                      <option value="" disabled="">
                        Select Resource
                      </option>
                      <option value="workspaceOverview">
                        Workspace Overview
                      </option>
                      <option value="videoTraining">Video Training</option>
                    </select>

                    {formErrors.videoCategory && (
                      <div className="invalid-tooltip">
                        {formErrors.videoCategory}
                      </div>
                    )}
                  </div>

                  {/* <div className="col-md-12 mt-3">
                      <label className="col-form-label" for="pr-country">
                        Playlist
                      </label>

                      <select
                        className="form-select"
                        id="pr-country"
                        name="playlist"
                        value={formValues.playlist}
                        onChange={handleChange}
                      >
                        <option value="workspaceOverview">
                          Workspace Overview
                        </option>
                        <option value="workspaceRecaps">
                          Workspace Recaps
                        </option>
                        <option value="rEALTORNews">REALTOR® News</option>
                        <option value="videoTraining">Video Training</option>
                      </select>
                      {formErrors.playlist && (
                        <div className="invalid-tooltip">
                          {formErrors.playlist}
                        </div>
                      )}
                    </div> */}

                  <div className="col-md-12 mt-3">
                    <label className="col-form-label" for="pr-country">
                      Thumbnail Image
                    </label>
                    <input
                      className="form-control"
                      id="inline-form-input"
                      type="file"
                      accept={acceptedFileTypes
                        .map((type) => `.${type}`)
                        .join(",")}
                      name="thumbnailimg"
                      onChange={handleFileUploadSide}
                      value={formValues.thumbnailimg}
                      style={{
                        border: formErrors?.thumbnailimg ? "1px solid red" : "",
                      }}
                    />
                    {formErrors.thumbnailimg && !imgthumbnailfile && (
                      <div className="invalid-tooltip">
                        {formErrors.thumbnailimg}
                      </div>
                    )}

                    {imgthumbnailfile ? (
                      <img className="mt-3" src={imgthumbnailfile} />
                    ) : (
                      <img className="mt-3" src={formValues.thumbnail_image} />
                    )}
                    {/* {console.log(formValues.thumbnail_image)} */}

                    {thumbnail_created_array
                      ? thumbnail_created_array.length > 0
                        ? thumbnail_created_array.map((post) => {
                            let thumb_ii = thumb_i++;
                            return (
                              <div className="col-md-3">
                                <div className="NotSetup  text-center">
                                  <img
                                    className={`thumb thumbnail_${thumb_ii} ml-2`}
                                    // onClick={(e) => this.select_thumbnail(e, post, thumb_ii)}
                                    src={post}
                                  />
                                </div>
                              </div>
                            );
                          })
                        : ""
                      : ""}

                    {/* <div className="col-md-3">
                                                <div className="NotSetup Marquee text-center">

                                                    <button type="button" className="btn btn-primary">
                                                        Not Setup
                                                    </button>


                                                    <p>No side banner.</p>
                                                </div>
                                            </div> */}
                  </div>

                  <div className="col-md-12 mt-3">
                    <label className="col-form-label" for="pr-country">
                      Active Status
                    </label>
                    <select
                      className="form-select"
                      id="pr-country"
                      name="is_active"
                      value={formValues.is_active}
                      onChange={handleChange}
                    >
                      <option value="1">Active</option>
                      <option value="0">Hidden</option>
                    </select>
                    {formErrors.is_active && (
                      <div className="invalid-tooltip">
                        {formErrors.is_active}
                      </div>
                    )}
                  </div>
                </>
              </div>
            </div>
            <div className="pull-right mt-3">
              <NavLink
                className="btn btn-secondary pull-right ms-3"
                type="button"
                to="/learn"
              >
                Cancel
              </NavLink>

              <button
                className="btn btn-primary pull-right"
                type="button"
                onClick={handleSubmit}
                disabled={isSubmitClickNew}
              >
                {isSubmitClickNew ? "Please wait..." : "Add a Category"}{" "}
              </button>
            </div>
          </div>
        </div>
      </main>
    </div>
  );
}
export default AddVideoCategory;

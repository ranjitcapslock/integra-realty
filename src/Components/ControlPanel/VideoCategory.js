import React, { useState, useEffect } from "react";
// import avtar from "./Components/img/avtar.jpg";
import _ from "lodash";
import user_service from "../service/user_service";
import jwt from "jwt-decode";
import Loader from "../../Pages/Loader/Loader.js";
import Toaster from "../../Pages/Toaster/Toaster.js";
import { NavLink, useNavigate, useParams } from "react-router-dom";
import { Button } from "react-bootstrap";
function VideoCategory() {
  const url = window.location.pathname.split("/").pop();
  const [loader, setLoader] = useState({ isActive: null });
  const { isActive } = loader;
  const [toaster, setToaster] = useState({
    types: null,
    isShow: null,
    toasterBody: "",
    message: "",
  });
  const { types, isShow, toasterBody, message } = toaster;
  const [checkData, setCheckData] = useState("");
  const navigate = useNavigate();
  const params = useParams();
  const [learnbyAgent, setLearnbyAgent] = useState([]);

  const [learnbyId, setLearnbyId] = useState([]);

  // const getLearnbyIdApi = async () => {
  //   await user_service.getLearnIdGet(params.id).then((response) => {
  //     setLoader({ isActive: false });
  //     if (response) {
  //         setLearnbyAgent(response.data);

  //     }
  //   });
  // };

  const getLearnbyIdApi = async () => {
    await user_service.getLearnIdGet(params.id).then((response) => {
      setLoader({ isActive: false });
      if (response) {
        const groupedBycategory = response.data.data.reduce((acc, item) => {
          const category = item.playlist;
          if (!acc[category]) {
            acc[category] = [];
          }
          acc[category].push(item);
          return acc;
        }, {});

        console.log(groupedBycategory);
        setLearnbyAgent(groupedBycategory);
      }
    });
  };

  useEffect(() => {
    window.scrollTo(0, 0);
    setLoader({ isActive: true });
    getLearnbyIdApi();
    // getLearnbyAgentApi();
  }, [url]);

  const handleRemove = async (id) => {
    setLoader({ isActive: true });
    await user_service.learnmediaDelete(id).then((response) => {
      if (response) {
        setLoader({ isActive: false });
        getLearnbyIdApi();
        setToaster({
          type: "Media Deleted Successfully",
          isShow: true,
          // toasterBody: response.data.message,
          message: "Media Deleted Successfully",
        });
        setTimeout(() => {
          setToaster((prevToaster) => ({
            ...prevToaster,
            isShow: false,
          }));
        }, 2000);
      }
    });
  };

  return (
    <div className="bg-secondary float-left w-100 pt-4 mb-4">
      <Loader isActive={isActive} />
      {isShow && (
        <Toaster
          types={types}
          isShow={isShow}
          toasterBody={toasterBody}
          message={message}
        />
      )}
      <main className="page-wrapper profile_page_wrap">
        <div className="product_vendors content-overlay">
          <div className="float-left w-100 d-flex align-items-center justify-content-between mb-4">
            <h3 className="mb-0 text-white">
              {params.type == "workspaceOverview"
                ? "B.A.S.E Overview"
                : // params.type == "workspaceRecaps"? "B.A.S.E Recaps":
                params.type == "rEALTORNews"
                ? "REALTOR® News"
                : params.type == "videoTraining"
                ? "Video Training"
                : ""}
            </h3>
            <span className="pull-right">
              <NavLink
                to={`/add-learn`}
                className="btn btn-primary pull-right"
                type="button"
              >
                Add New Video
              </NavLink>
            </span>
          </div>
        </div>
        <div className="promoted_product float-left w-100">
          <div className="row">
            <div className="col-md-12">
              <div className="mb-3">
              <h4 className="text-white">Assigned Videos</h4>
                <div className="row mt-2">
                  {learnbyAgent.videoTraining &&
                  learnbyAgent.videoTraining.length > 0 ? (
                    learnbyAgent.videoTraining.map((item) => {
                      return (
                        <div className="col-lg-3 col-md-6 col-sm-6 col-12 related_media tns-item tns-slide-cloned tns-slide-active p-3">
                          <div className="card shadow-sm cursor-pointer card-hover border-0 h-100">
                            <div className="">
                            <NavLink
                                to={`/videotraningview/${item._id}`}>
                              <div className="card-img-top card-img-hover">
                                {item.videotype == "custom" ? (
                                  <>
                                    {item.thumbnail_image ? (
                                      <img
                                        className="img-fluid rounded-0 w-100"
                                        src={item.thumbnail_image}
                                      />
                                    ) : (
                                      <video
                                        className="img-fluid rounded-0 w-100"
                                        width="300"
                                        height="250"
                                        controls
                                      >
                                        <source
                                          src={item.video_url}
                                          type="video/mp4"
                                        />
                                      </video>
                                    )}
                                  </>
                                ) : (
                                  <>
                                    {item.thumbnail_image ? (
                                      <img
                                        className="img-fluid rounded-0 w-100"
                                        src={item.thumbnail_image}
                                      />
                                    ) : (
                                      <div
                                        id=""
                                        dangerouslySetInnerHTML={{
                                          __html: item.video_url,
                                        }}
                                      />
                                    )}
                                  </>
                                )}
                              </div>
                                <div className="card-body position-relative pb-0">
                                <h3 className="h6 mb-0 fs-base mb-0">
                                  {item.title.length > 55
                                    ? item.title.substring(0, 55) + "..."
                                    : item.title}
                                </h3>
                                <p className="fs-sm text-muted mt-1">
                                  <small>
                                    {item.description.length > 100
                                      ? item.title.substring(0, 100) + "..."
                                      : item.description}
                                  </small>
                                </p>
                                </div>
                                </NavLink>
                                <div className="card-body">
                                <div className="row">
                                  <div className="col-lg-6 col-md-6 col-sm-6 col-6">
                                    <NavLink
                                      to={`/add-learn/${item._id}`}
                                      type="button"
                                    >
                                      <i className="fi-edit me-2"></i>Edit
                                    </NavLink>
                                  </div>
                                  <div className="col-lg-6 col-md-6 col-sm-6 col-6">
                                    <a
                                      type="button"
                                      aria-current="page"
                                      className="text-center  active"
                                      onClick={() => handleRemove(item._id)}
                                    >
                                      <i
                                        className="fa fa-trash-o me-2"
                                        aria-hidden="true"
                                      ></i>
                                      Delete
                                    </a>
                                  </div>
                                </div>
                                </div>
                            </div>
                          </div>
                        </div>
                      );
                    })
                  ) : (
                    <div className="small-5 columns float-left w-100 text-center">
                      <span
                        className="float-left w-100 text-center"
                        style={{ color: "#ffffff" }}
                      >
                        No Media Assigned
                      </span>
                    </div>
                  )}
                </div>
              </div>
            </div>
          </div>
        </div>
      </main>
    </div>
  );
}
export default VideoCategory;

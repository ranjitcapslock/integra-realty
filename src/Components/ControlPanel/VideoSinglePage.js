import React, { useState, useEffect } from "react";
// import avtar from "./Components/img/avtar.jpg";
import _ from "lodash";
import user_service from "../service/user_service";
import jwt from "jwt-decode";
import Loader from "../../Pages/Loader/Loader.js";
import Toaster from "../../Pages/Toaster/Toaster.js";
import { NavLink, useNavigate, useParams } from "react-router-dom";
import { Button } from "react-bootstrap";
function VideoSinglePage() {
  const url = window.location.pathname.split("/").pop();
  const [loader, setLoader] = useState({ isActive: null });
  const { isActive } = loader;
  const [toaster, setToaster] = useState({
    types: null,
    isShow: null,
    toasterBody: "",
    message: "",
  });
  const { types, isShow, toasterBody, message } = toaster;
  const [checkData, setCheckData] = useState("");
  const navigate = useNavigate();
  const params = useParams();
  const [learnbyAgentNew, setLearnbyAgentNew] = useState([]);
  const [learnbyId, setLearnbyId] = useState([]);
  const [learnbyAgent, setLearnbyAgent] = useState([]);

  const getLearnbyId = async () => {
      await user_service.learnIdvideo(params.id).then((response) => {
        setLoader({ isActive: false });
        if (response) {
            console.log(response);
            setLearnbyAgent(response.data);
   
        }
      });
    };



  useEffect(() => {
    window.scrollTo(0, 0);
    setLoader({ isActive: true });
    getLearnbyId();
  }, []);

//   const addVideocategoryGetdata = async () => {
//     await user_service.addVideocategoryGet(localStorage.getItem("active_office")).then((response) => {
//       setLoader({ isActive: false });
//       if (response) {
//         const groupedBycategory = response.data.data.reduce((acc, item) => {
//           const category = item.videoCategory;
//           if (!acc[category]) {
//             acc[category] = [];
//           }
//           acc[category].push(item);
//           return acc;
//         }, {});

//         console.log(groupedBycategory);
//         setLearnbyAgentNew(groupedBycategory);
//       }
//     });
  
              
// };




  
  return (
    <div className="bg-secondary float-left w-100 pt-3 mb-4">
      <Loader isActive={isActive} />
      {isShow && (
        <Toaster
          types={types}
          isShow={isShow}
          toasterBody={toasterBody}
          message={message}
        />
      )}
      <main className="page-wrapper">
        <div className="">
          <div className="">
            <div className="">
              {/* <hr className="mb-3" /> */}
                <div className="pb-sm-2">
                  <div className="">
                    <div className="">
                    <div className="bg-light border rounded-3 p-3">
                      {learnbyAgent?.videotype == "custom" ? (
                        <div className="col-md-12">
                          <video
                            style={{ width: "100%", height: "450px" }}
                            controls
                          >
                            <source
                              src={learnbyAgent?.video_url}
                              type="video/mp4"
                            />
                          </video>
                        </div>
                      ) : (
                        <div className="col-md-12">
                          <div
                            id=""
                            dangerouslySetInnerHTML={{
                              __html: learnbyAgent?.video_url,
                            }}
                          />
                        </div>
                      )}
                      <h6 className="mt-3 text-dark">
                        {learnbyAgent?.title?.length > 550
                          ? learnbyAgent?.title.substring(0, 550) + "..."
                          : learnbyAgent?.title}
                      </h6>
                      <small className="text-dark">
                        {learnbyAgent?.description?.length > 1000
                          ? learnbyAgent?.title.substring(0, 1000) + "..."
                          : learnbyAgent?.description}
                      </small>
                      <br />
                    </div>
                    <NavLink
                      className="btn btn-secondary pull-right mt-2"
                      type="button"
                      to={`/videocategory/${learnbyAgent?.playlist}/${learnbyAgent?.videoCategory}`}
                    >
                      Back
                    </NavLink>
                    </div>
                  </div>
                </div>
            
            </div>
          </div>
        </div>
      </main>
    </div>
  );
}
export default VideoSinglePage;

import React, { useState, useEffect } from "react";
import avtar from "../img/avtar.jpg";
import ReactPaginate from "react-paginate";
import user_service from '../service/user_service';
import jwt from "jwt-decode";
import Loader from "../../Pages/Loader/Loader.js";
import Toaster from '../../Pages/Toaster/Toaster.js';
import { NavLink, useNavigate, useParams } from "react-router-dom";
import _ from "lodash";
import { MultiSelect } from "react-multi-select-component";


const AddChecklistRules = () => {
    const [loader, setLoader] = useState({ isActive: null })
    const { isActive } = loader;
    const [toaster, setToaster] = useState({ types: null, isShow: null, toasterBody: "", message: "" });
    const { types, isShow, toasterBody, message } = toaster;
    const navigate = useNavigate();

    const initialValues = {  conditionWhere: "" , transactionPhase: "", agentId: "" };
    const [formValues, setFormValues] = useState(initialValues);
    const handleChange = (e) => {
        const { name, value } = e.target;
        setFormValues({ ...formValues, [name]: value });
    };
    const [formErrors, setFormErrors] = useState({});
    const [tname, setTname] = useState();
    const [isSubmitClick, setISSubmitClick] = useState(false)
    const [getChecklist, setGetChecklist] = useState([])
    const [selectedconditionWhere, setSelectedconditionWhere] = useState([]);
    const [selectedconditionExpect, setSelectedconditionExpect] = useState([]);

    const [checklistValues, setChecklistValues] = useState([])
 
  
    const PlatformChecklists = async () => {
        await user_service.PlatformChecklists().then((response) => {
            // console.log(response);
            if (response) {
                setGetChecklist(response.data.data);
                const checklistValuess = [];
                response.data.data?.map((postt) => {
                    let aa = { label: `${postt.name}`, value: `${postt.slug}` }
                    checklistValuess.push(aa);
                })
                setChecklistValues(checklistValuess)
            }
        });
    }
    const [rulesdata, setRulesdata] = useState([]);
    const ChecklistRules = async () => {
        const agentId = jwt(localStorage.getItem("auth")).id;
        await user_service.ChecklistRules(agentId).then((response) => {
            setLoader({ isActive: false })
            if (response) {
                // console.log(response.data)
                setRulesdata(response.data);
            }
        });
    }

    useEffect(() => { window.scrollTo(0, 0);
        setLoader({ isActive: true })
        ChecklistRules()
        PlatformChecklists()

        setLoader({ isActive: false })
    }, []);

    const openrulepopup = (tname) => {

        setTname(tname);

        window.$('#addrules').modal('show')
    }
    
    {/* <!-- Form Validation Start--> */ }
    useEffect(() => { 
        if (formValues && isSubmitClick) {
            validate()
        }
    }, [formValues])

    const validate = () => {
        const values = formValues
        const errors = {};
        // if (!values.documentSetting) {
        //     errors.documentSetting = "Setting is required";
        // }

        if (selectedconditionWhere.length < 1) {
            errors.conditionWhere = "where condition is required";
        }

        // if (selectedconditionExpect.length < 1) {
        //     errors.conditionExpect = "expect is required";
        // }

        if (!values.transactionPhase) {
            errors.transactionPhase = "transaction phase is required";
        }

        setFormErrors(errors)
        return errors;
    }
    {/* <!-- Form Validation End--> */ }


    const addRule = async (e) => {
        e.preventDefault();
        setISSubmitClick(true)
        let checkValue = validate()
        if (_.isEmpty(checkValue)) {
            var transname = tname.replace(/ /g, '_');
            transname = transname.toLowerCase();

            const ruleData = {
                agentId: jwt(localStorage.getItem("auth")).id,
                transactionType: transname,
                // documentSetting: formValues.documentSetting,
                conditionWhere: JSON.stringify(selectedconditionWhere),
                // conditionExpect: JSON.stringify(selectedconditionExpect),
                transactionPhase: formValues.transactionPhase
            };
            console.log(ruleData);
            // return;
            try {
                setLoader({ isActive: true });
                // const response = await user_service.AdddocumentRule(ruleData);
                await user_service.AddchecklistRule(ruleData).then((response) => {
                    if (response) {
                        console.log(response.data)
                        setLoader({ isActive: false });
                        setToaster({
                            type: "Rule created Successfully",
                            isShow: true,
                            toasterBody: response.data.message,
                            message: "Rule created Successfully",
                        });
                        document.getElementById("closeModal").click();
                        // setTimeout(() => {
                        //     // navigate(`/transaction-summary/${response.data._id}`);
                        // }, 500);
                    } else {
                        setLoader({ isActive: false });
                        setToaster({
                            types: "error",
                            isShow: true,
                            toasterBody: response.data.message,
                            message: "Error",
                        });
                    }
                    setTimeout(() => {
                        setToaster({ types: "error", isShow: false, toasterBody: null, message: "Error" });
                    }, 2000);
                });

            } catch (error) {
                setLoader({ isActive: false });
                setToaster({
                    types: "error",
                    isShow: true,
                    toasterBody: error,
                    message: "Error",
                });
            }
        }
    };

    return (
        <div className="bg-secondary float-left w-100 pt-4">
            <Loader isActive={isActive} />
            <main className="page-wrapper">
                <div className="container mb-md-4 ">
                    <div className="bg-light shadow-sm rounded-3 p-2 p-md-5 mb-2">
                        <div className="row">
                            <div className="d-flex align-items-center"><h1 className="h3 mb-3">Add Rule For Transaction Checklist</h1><br /></div>
                            <div className="row" id="auth-info">
                                <h6 className="ms-5  mt-5">How would you like to set date/time for the transaction?</h6>
                                <div className="alert alert-secondary  fade show col-md-9 ms-5">
                                    <h6 className="pull-left m-0">Residential Sale <small></small></h6>
                                    <div className="pull-right d-flex" data-bs-toggle="tooltip" title="Edit">
                                        <button className="btn btn-primary btn-sm  pull-left" onClick={(e) => openrulepopup("Residential Sale")}>Add Rule</button>
                                        <a className="nav-link py-0 ml-4" href="#residential_sale" data-bs-toggle="collapse"> <i className="fa fa-chevron-down ml-2"></i></a></div>
                                    <br />
                                    <div className="float-left w-100 collapse" id="residential_sale" data-bs-parent="#auth-info">
                                        {
                                            rulesdata.data ?
                                                rulesdata.data.length > 0 ?
                                                    rulesdata.data.map(post_rule => {
                                                        if (post_rule.transactionType == "residential_sale") {
                                                            let conditionWheredata = JSON.parse(post_rule.conditionWhere);
                                                            return (
                                                                <>
                                                                    <p className='text-property mb-0 mt-3'>{post_rule.transactionPhase}</p>
                                                                    {
                                                                        conditionWheredata ?
                                                                            conditionWheredata.length > 0 ?
                                                                                conditionWheredata.map((conditionWhere_rule) => (
                                                                                    <>
                                                                                        <div className='document_bar mt-2'>
                                                                                            <p className='pull-left'>{conditionWhere_rule.label}</p>
                                                                                            {/* {
                                                                                                <>
                                                                                                    <span className='status_tag'>{post_rule.documentSetting}</span>
                                                                                                </>
                                                                                            } */}
                                                                                            <br />
                                                                                        </div>
                                                                                    </>
                                                                                ))
                                                                                : ""
                                                                            : ""
                                                                    }
                                                                </>
                                                            );
                                                        }

                                                    })
                                                    : "Please set checklist rules."
                                                : "Please set checklist rules."
                                        }
                                    </div>
                                </div>

                                <div className="alert alert-secondary fade show col-md-9 ms-5">
                                    <h6 className="pull-left m-0">Residential lease  <small></small></h6>
                                    <div className="pull-right d-flex" data-bs-toggle="tooltip" title="Edit">
                                        <button className="btn btn-primary btn-sm  pull-left" onClick={(e) => openrulepopup("Residential Lease")}>Add Rule</button>
                                        <a className="nav-link py-0" href="#residential_lease" data-bs-toggle="collapse"><i className="fa fa-chevron-down ml-2"></i></a></div>
                                    <br />
                                    <div className="float-left w-100 collapse" id="residential_lease" data-bs-parent="#auth-info">
                                        {
                                            rulesdata.data ?
                                                rulesdata.data.length > 0 ?
                                                    rulesdata.data.map(post_rule => {
                                                        if (post_rule.transactionType == "residential_lease") {
                                                            let conditionWheredata = JSON.parse(post_rule.conditionWhere);
                                                            return (
                                                                <>
                                                                    <p className='text-property mb-0 mt-3'>{post_rule.transactionPhase}</p>
                                                                    {
                                                                        conditionWheredata ?
                                                                            conditionWheredata.length > 0 ?
                                                                                conditionWheredata.map((conditionWhere_rule) => (
                                                                                    <>
                                                                                        <div className='document_bar mt-2'>
                                                                                            <p className='pull-left'>{conditionWhere_rule.label}</p>
                                                                                            {/* {
                                                                                                <>
                                                                                                    <span className='status_tag'>{post_rule.documentSetting}</span>
                                                                                                </>
                                                                                            } */}
                                                                                            <br />
                                                                                        </div>
                                                                                    </>
                                                                                ))
                                                                                : ""
                                                                            : ""
                                                                    }
                                                                </>
                                                            );
                                                        }

                                                    })
                                                    : "Please set checklist rules."
                                                : "Please set checklist rules."
                                        }
                                    </div>
                                </div>
                                <div className="alert alert-secondary fade show col-md-9 ms-5">
                                    <h6 className="pull-left m-0">Commercial Sale  <small></small></h6>
                                    <div className="pull-right d-flex" data-bs-toggle="tooltip" title="Edit">
                                        <button className="btn btn-primary btn-sm  pull-left" onClick={(e) => openrulepopup("Commercial Sale")}>Add Rule</button>
                                        <a className="nav-link py-0" href="#commercial_sale" data-bs-toggle="collapse"><i className="fa fa-chevron-down ml-2"></i></a></div>
                                    <br />
                                    <div className="float-left w-100 collapse" id="commercial_sale" data-bs-parent="#auth-info">
                                        {
                                            rulesdata.data ?
                                                rulesdata.data.length > 0 ?
                                                    rulesdata.data.map(post_rule => {
                                                        if (post_rule.transactionType == "commercial_sale") {
                                                            let conditionWheredata = JSON.parse(post_rule.conditionWhere);
                                                            return (
                                                                <>
                                                                    <p className='text-property mb-0 mt-3'>{post_rule.transactionPhase}</p>
                                                                    {
                                                                        conditionWheredata ?
                                                                            conditionWheredata.length > 0 ?
                                                                                conditionWheredata.map((conditionWhere_rule) => (
                                                                                    <>
                                                                                        <div className='document_bar mt-2'>
                                                                                            <p className='pull-left'>{conditionWhere_rule.label}</p>
                                                                                            {/* {
                                                                                                <>
                                                                                                    <span className='status_tag'>{post_rule.documentSetting}</span>
                                                                                                </>
                                                                                            } */}
                                                                                            <br />
                                                                                        </div>
                                                                                    </>
                                                                                ))
                                                                                : ""
                                                                            : ""
                                                                    }
                                                                </>
                                                            );
                                                        }

                                                    })
                                                    : "Please set checklist rules."
                                                : "Please set checklist rules."
                                        }
                                    </div>
                                </div>

                                <div className="alert alert-secondary fade show col-md-9 ms-5">
                                    <h6 className="pull-left m-0">Commercial Lease  <small></small></h6>
                                    <div className="pull-right d-flex" data-bs-toggle="tooltip" title="Edit">
                                        <button className="btn btn-primary btn-sm  pull-left" onClick={(e) => openrulepopup("Commercial lease")}>Add Rule</button>
                                        <a className="nav-link py-0" href="#commercial_lease" data-bs-toggle="collapse"><i className="fa fa-chevron-down ml-2"></i></a></div>
                                    <br />
                                    <div className="float-left w-100 collapse" id="commercial_lease" data-bs-parent="#auth-info">
                                        {
                                            rulesdata.data ?
                                                rulesdata.data.length > 0 ?
                                                    rulesdata.data.map(post_rule => {
                                                        if (post_rule.transactionType == "commercial_lease") {
                                                            let conditionWheredata = JSON.parse(post_rule.conditionWhere);
                                                            return (
                                                                <>
                                                                    <p className='text-property mb-0 mt-3'>{post_rule.transactionPhase}</p>
                                                                    {
                                                                        conditionWheredata ?
                                                                            conditionWheredata.length > 0 ?
                                                                                conditionWheredata.map((conditionWhere_rule) => (
                                                                                    <>
                                                                                        <div className='document_bar mt-2'>
                                                                                            <p className='pull-left'>{conditionWhere_rule.label}</p>
                                                                                            {/* {
                                                                                                <>
                                                                                                    <span className='status_tag'>{post_rule.documentSetting}</span>
                                                                                                </>
                                                                                            } */}
                                                                                            <br />
                                                                                        </div>
                                                                                    </>
                                                                                ))
                                                                                : ""
                                                                            : ""
                                                                    }
                                                                </>
                                                            );
                                                        }

                                                    })
                                                    : "Please set checklist rules."
                                                : "Please set checklist rules."
                                        }
                                    </div>
                                </div>

                                <div className="alert alert-secondary fade show col-md-9 ms-5">
                                    <h6 className="pull-left m-0">Apartment Lease  <small></small></h6>
                                    <div className="pull-right d-flex" data-bs-toggle="tooltip" title="Edit">
                                        <button className="btn btn-primary btn-sm  pull-left" onClick={(e) => openrulepopup("Apartment Lease")}>Add Rule</button>
                                        <a className="nav-link py-0" href="#apartment_lease" data-bs-toggle="collapse"><i className="fa fa-chevron-down ml-2"></i></a></div>
                                    <br />
                                    <div className="float-left w-100 collapse" id="apartment_lease" data-bs-parent="#auth-info">
                                        {
                                            rulesdata.data ?
                                                rulesdata.data.length > 0 ?
                                                    rulesdata.data.map(post_rule => {
                                                        if (post_rule.transactionType == "apartment_lease") {
                                                            let conditionWheredata = JSON.parse(post_rule.conditionWhere);
                                                            return (
                                                                <>
                                                                    <p className='text-property mb-0 mt-3'>{post_rule.transactionPhase}</p>
                                                                    {
                                                                        conditionWheredata ?
                                                                            conditionWheredata.length > 0 ?
                                                                                conditionWheredata.map((conditionWhere_rule) => (
                                                                                    <>
                                                                                        <div className='document_bar mt-2'>
                                                                                            <p className='pull-left'>{conditionWhere_rule.label}</p>
                                                                                            {/* {
                                                                                                <>
                                                                                                    <span className='status_tag'>{post_rule.documentSetting}</span>
                                                                                                </>
                                                                                            } */}
                                                                                            <br />
                                                                                        </div>
                                                                                    </>
                                                                                ))
                                                                                : ""
                                                                            : ""
                                                                    }
                                                                </>
                                                            );
                                                        }

                                                    })
                                                    : "Please set checklist rules."
                                                : "Please set checklist rules."
                                        }
                                    </div>
                                </div>
                                <div className="alert alert-secondary fade show col-md-9 ms-5">
                                    <h6 className="pull-left m-0">Referred  <small></small></h6>
                                    <div className="pull-right d-flex" data-bs-toggle="tooltip" title="Edit">
                                        <button className="btn btn-primary btn-sm  pull-left" onClick={(e) => openrulepopup("Referred")}>Add Rule</button>
                                        <a className="nav-link py-0" href="#referred" data-bs-toggle="collapse"><i className="fa fa-chevron-down ml-2"></i></a></div>
                                    <br />
                                    <div className="float-left w-100 collapse" id="referred" data-bs-parent="#auth-info">
                                        {
                                            rulesdata.data ?
                                                rulesdata.data.length > 0 ?
                                                    rulesdata.data.map(post_rule => {
                                                        if (post_rule.transactionType == "referred") {
                                                            let conditionWheredata = JSON.parse(post_rule.conditionWhere);
                                                            return (
                                                                <>
                                                                    <p className='text-property mb-0 mt-3'>{post_rule.transactionPhase}</p>
                                                                    {
                                                                        conditionWheredata ?
                                                                            conditionWheredata.length > 0 ?
                                                                                conditionWheredata.map((conditionWhere_rule) => (
                                                                                    <>
                                                                                        <div className='document_bar mt-2'>
                                                                                            <p className='pull-left'>{conditionWhere_rule.label}</p>
                                                                                            {/* {
                                                                                                <>
                                                                                                    <span className='status_tag'>{post_rule.documentSetting}</span>
                                                                                                </>
                                                                                            } */}
                                                                                            <br />
                                                                                        </div>
                                                                                    </>
                                                                                ))
                                                                                : ""
                                                                            : ""
                                                                    }
                                                                </>
                                                            );
                                                        }

                                                    })
                                                    : "Please set checklist rules."
                                                : "Please set checklist rules."
                                        }
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>

                <div className="modal" role="dialog" id="addrules">
                    <div className="modal-dialog" role="document">
                        <div className="modal-content">
                            <div className="modal-header">
                                <h6>Add Rule</h6>
                                <h6 className='text-right'>{tname}</h6>
                            </div>
                            <div className="modal-body row">
                                <div className=" col-md-12 mt-5" >
                                    <div>
                                        <form onSubmit={addRule}>
                                            {/* <div className="mb-3">
                                                <label className="form-label">Settings:</label><br />
                                                <div className='col-md-10 '>
                                                    <div className="form-check mt-3">
                                                        <input className="form-check-input" id="form-radio-1" type="radio" name="documentSetting" value="required" onChange={handleChange} />
                                                        <label className="form-check-label-three" for="form-radio-1">Required</label>
                                                    </div>
                                                    <div className="form-check mt-3">
                                                        <input className="form-check-input" id="form-radio-2" type="radio" name="documentSetting" value="recommended" onChange={handleChange} />
                                                        <label className="form-check-label-three" for="form-radio-2">Recommended</label>
                                                    </div>
                                                    <div className="form-check mt-3">
                                                        <input className="form-check-input" id="form-radio-2" type="radio" name="documentSetting" value="optional" onChange={handleChange} />
                                                        <label className="form-check-label-three" for="form-radio-2">Optional</label>
                                                    </div>
                                                    <div className="invalid-tooltip">{formErrors.documentSetting}</div>
                                                </div>
                                            </div> */}

                                            <div className="mb-3">
                                                <label className="form-label">Checklist Rules:</label><br />
                                                <div className='col-md-10 p-3'>
                                                    <label className="form-label" >Select Checklist</label>
                                                    <MultiSelect
                                                        options={checklistValues}
                                                        value={selectedconditionWhere}
                                                        onChange={setSelectedconditionWhere}
                                                        labelledBy="Select"
                                                        name="conditionWhere"
                                                    />

                                                    <div className="invalid-tooltip">{formErrors.conditionWhere}</div>
                                                </div>
                                            </div>

                                            <div className="mb-3">
                                                <label className="form-label">Phase Of Transaction:</label><br />
                                                <div className='col-md-10 p-3'>
                                                    {/* <label className="form-label" >Where-</label> */}
                                                    <select className="form-select form-select-dark" name="transactionPhase"
                                                        value={formValues.transactionPhase} onChange={handleChange}>
                                                        <option></option>
                                                        <option value="start">Start</option>
                                                        <option value="contract">Contract</option>
                                                        <option value="showing">Showing</option>
                                                        <option value="Pre-Closing">Pre-Closing</option>
                                                        <option value="Post-Closing">Post-Closing</option>
                                                    </select>
                                                    <div className="invalid-tooltip">{formErrors.transactionPhase}</div>
                                                </div>
                                            </div>
                                            <div className="modal-footer">
                                                <button type="button" className="btn btn-default btn btn-secondary btn-sm" id="closeModal" data-dismiss="modal">Cancel & Close</button>
                                                <button type="submit" className="btn btn-primary">Save changes</button>
                                            </div>
                                        </form>
                                    </div>

                                </div>

                            </div>


                        </div>
                    </div>
                </div>



            </main >
            

        </div >
    )
}

export default AddChecklistRules
import React, { useState, useEffect } from "react";
import _ from 'lodash';
import user_service from '../service/user_service';
import Loader from "../../Pages/Loader/Loader.js";
import Toaster from "../../Pages/Toaster/Toaster.js";
import { useNavigate } from "react-router-dom";

import InputColor from 'react-input-color';
import jwt from "jwt-decode";


function ColorTheme() {
    const navigate = useNavigate();
    const [loader, setLoader] = useState({ isActive: null })
    const { isActive } = loader;
    const [toaster, setToaster] = useState({ types: null, isShow: null, toasterBody: "", message: "" });
    const { types, isShow, toasterBody, message } = toaster;

    const [checkAll, setCheckAll] = useState("");
    const initialValues = {
        Branding_Main_Color: "#445985", Branding_Accent_Color: "#707070", Branding_Link_Color: "#3278BD", Branding_Confirm_Color: "#5A8E22",
        Branding_Focus_Color: "#E96B12", Branding_Info_Color: "#3278BD", Branding_Alert_Color: "#FF0000", Branding_Back_Color: "#F0F0F0",
        Branding_Header_Color: "#000000",Branding_Footer_Color:"#000000",

        Transact_Res_Buyer_Color: "#88CCA0", Transact_Res_Seller_Color: "#0F7947", Transact_Res_Tenant_Color: "#81A8D6",
        Transact_Res_Landlord_Color: "#004A80", Transact_Apartment_Color: "#CCCC33", Transact_Com_Seller_Color: "#770000",
        Transact_Com_Buyer_Color: "#BB3A3A", Transact_Com_Tenant_Color: "#1CBBB4", Transact_Com_Landlord_Color: "#005952",
        Transact_Referral_Only_Color: "#630460",

        Calendar_Personal_Color: "#9A9B9D", Calendar_Transaction_Color: "#CB2027", Calendar_Group_Color: "#216A63", Calendar_Office_Color: "#CC6600",
        Calendar_Training_Color: "#009900", Calendar_Community_Color: "#660099", Calendar_Birthday_Color: "#006699",

        Associate_Color: "#9A9B9D", Associate_MLS_Color: "#115F67", Associate_License_Color: "#005885", Associate_Staff_Color: "#CB2027",
        Associate_Yearly_Color: "#CC6633", Associate_Contact_Color: "#FFD700", Associate_Community_Color: "#6AC7DB", Associate_Buyer_Color: "#82CA9C",
        Associate_Seller_Color: "#007236", Associate_LandLord_Color: "#004A80", Associate_Tenant_Color: "#7DA7D9", Associate_Vendor_Color: "005500",
        Associate_Co_Op_Color: "005500", Associate_Team_Color: "E88124",
    };
    const [formValues, setFormValues] = useState(initialValues);

    // onChange Function start
    const handleChange = (colorValue, name) => {
        let newColorValue = colorValue;
    
        // Check if the colorValue is an object (like from a color picker component)
        if (typeof colorValue === 'object' && colorValue.hex) {
            newColorValue = colorValue.hex;
        }
    
        // Ensure the color value is in lowercase
        const lowercasedColor = newColorValue.toLowerCase();
    
        // Update the form values with the new color
        setFormValues({ ...formValues, [name]: lowercasedColor });
    };


    // onChange Function end

  
    

    const handleSubmit = async (e) => {
        if (formValues._id) {
            e.preventDefault();
            const userData = {
                agentId: jwt(localStorage.getItem('auth')).id,
                Branding_Main_Color: formValues.Branding_Main_Color,
                Branding_Accent_Color: formValues.Branding_Accent_Color,
                Branding_Link_Color: formValues.Branding_Link_Color,

                Branding_Confirm_Color: formValues.Branding_Confirm_Color,
                Branding_Focus_Color: formValues.Branding_Focus_Color,
                Branding_Info_Color: formValues.Branding_Info_Color,
                Branding_Alert_Color: formValues.Branding_Alert_Color,
                Branding_Back_Color: formValues.Branding_Back_Color,
                Branding_Header_Color: formValues.Branding_Header_Color,
                Branding_Footer_Color: formValues.Branding_Footer_Color,


                Transact_Res_Buyer_Color: formValues.Transact_Res_Buyer_Color,
                Transact_Res_Seller_Color: formValues.Transact_Res_Seller_Color,
                Transact_Res_Tenant_Color: formValues.Transact_Res_Tenant_Color,
                Transact_Res_Landlord_Color: formValues.Transact_Res_Landlord_Color,
                Transact_Apartment_Color: formValues.Transact_Apartment_Color,
                Transact_Com_Seller_Color: formValues.Transact_Com_Seller_Color,
                Transact_Com_Buyer_Color: formValues.Transact_Com_Buyer_Color,
                Transact_Com_Tenant_Color: formValues.Transact_Com_Tenant_Color,
                Transact_Com_Landlord_Color: formValues.Transact_Com_Landlord_Color,
                Transact_Referral_Only_Color: formValues.Transact_Referral_Only_Color,

                Calendar_Personal_Color: formValues.Calendar_Personal_Color,
                Calendar_Transaction_Color: formValues.Calendar_Transaction_Color,
                Calendar_Group_Color: formValues.Calendar_Group_Color,
                Calendar_Office_Color: formValues.Calendar_Office_Color,
                Calendar_Training_Color: formValues.Calendar_Training_Color,
                Calendar_Community_Color: formValues.Calendar_Community_Color,
                Calendar_Birthday_Color: formValues.Calendar_Birthday_Color,

                Associate_Color: formValues.Associate_Color,
                Associate_MLS_Color: formValues.Associate_MLS_Color,
                Associate_License_Color: formValues.Associate_License_Color,
                Associate_Staff_Color: formValues.Associate_Staff_Color,
                Associate_Yearly_Color: formValues.Associate_Yearly_Color,
                Associate_Contact_Color: formValues.Associate_Contact_Color,
                Associate_Community_Color: formValues.Associate_Community_Color,
                Associate_Buyer_Color: formValues.Associate_Buyer_Color,
                Associate_Seller_Color: formValues.Associate_Seller_Color,
                Associate_LandLord_Color: formValues.Associate_LandLord_Color,
                Associate_Tenant_Color: formValues.Associate_Tenant_Color,
                Associate_Vendor_Color: formValues.Associate_Vendor_Color,
                Associate_Co_Op_Color: formValues.Associate_Co_Op_Color,
                Associate_Team_Color: formValues.Associate_Team_Color,
            };
            console.log(userData);
            setLoader({ isActive: true })
            await user_service.themecolorsUpdate(formValues._id, userData).then((response) => {
                if (response) {
                    console.log(response);
                    setFormValues(response.data);
                    applyThemeColors(response.data);
                    setLoader({ isActive: false })
                    // setToaster({ type: "Add Section", isShow: true, toasterBody: response.data.message, message: "Add Section Successfully", });
                    // setTimeout(() => {
                    //     setToaster((prevToaster) => ({ ...prevToaster, isShow: false }))
                    // }, 1000);
                }
                else {
                    setLoader({ isActive: false })
                    setToaster({ types: "error", isShow: true, toasterBody: response.data.message, message: "Error", });
                }
            });
        }
        else {
            e.preventDefault();
            const userData = {
                agentId: jwt(localStorage.getItem('auth')).id,
                Branding_Main_Color: formValues.Branding_Main_Color ? formValues.Branding_Main_Color : "#445985",
                Branding_Accent_Color: formValues.Branding_Accent_Color ? formValues.Branding_Accent_Color : "#707070",
                Branding_Link_Color: formValues.Branding_Link_Color ? formValues.Branding_Link_Color : "#3278BD",

                Branding_Confirm_Color: formValues.Branding_Confirm_Color ? formValues.Branding_Confirm_Color : "#5A8E22",
                Branding_Focus_Color: formValues.Branding_Focus_Color ? formValues.Branding_Focus_Color : "#E96B12",
                Branding_Info_Color: formValues.Branding_Info_Color ? formValues.Branding_Info_Color : "#3278BD",
                Branding_Alert_Color: formValues.Branding_Alert_Color ? formValues.Branding_Alert_Color : "#FF0000",
                Branding_Back_Color: formValues.Branding_Back_Color ? formValues.Branding_Back_Color : "#F0F0F0",
                Branding_Header_Color: formValues.Branding_Header_Color ? formValues.Branding_Header_Color : "#000000",
                Branding_Footer_Color: formValues.Branding_Footer_Color ? formValues.Branding_Footer_Color : "#000000",
                Transact_Res_Buyer_Color: formValues.Transact_Res_Buyer_Color ? formValues.Transact_Res_Buyer_Color : "#88CCA0",
                Transact_Res_Seller_Color: formValues.Transact_Res_Seller_Color ? formValues.Transact_Res_Seller_Color : "#0F7947",
                Transact_Res_Tenant_Color: formValues.Transact_Res_Tenant_Color ? formValues.Transact_Res_Tenant_Color : "#81A8D6",
                Transact_Res_Landlord_Color: formValues.Transact_Res_Landlord_Color ? formValues.Transact_Res_Landlord_Color : "#004A80",
                Transact_Apartment_Color: formValues.Transact_Apartment_Color ? formValues.Transact_Apartment_Color : "#CCCC33",
                Transact_Com_Seller_Color: formValues.Transact_Com_Seller_Color ? formValues.Transact_Com_Seller_Color : "#770000",
                Transact_Com_Buyer_Color: formValues.Transact_Com_Buyer_Color ? formValues.Transact_Com_Buyer_Color : "#BB3A3A",
                Transact_Com_Tenant_Color: formValues.Transact_Com_Tenant_Color ? formValues.Transact_Com_Tenant_Color : "#1CBBB4",
                Transact_Com_Landlord_Color: formValues.Transact_Com_Landlord_Color ? formValues.Transact_Com_Landlord_Color : "#005952",
                Transact_Referral_Only_Color: formValues.Transact_Referral_Only_Color ? formValues.Transact_Referral_Only_Color : "#630460",

                Calendar_Personal_Color: formValues.Calendar_Personal_Color ? formValues.Calendar_Personal_Color : "#9A9B9D",
                Calendar_Transaction_Color: formValues.Calendar_Transaction_Color ? formValues.Calendar_Transaction_Color : "#CB2027",
                Calendar_Group_Color: formValues.Calendar_Group_Color ? formValues.Calendar_Group_Color : "#216A63",
                Calendar_Office_Color: formValues.Calendar_Office_Color ? formValues.Calendar_Office_Color : "#CC6600",
                Calendar_Training_Color: formValues.Calendar_Training_Color ? formValues.Calendar_Training_Color : "#009900",
                Calendar_Community_Color: formValues.Calendar_Community_Color ? formValues.Calendar_Community_Color : "#660099",
                Calendar_Birthday_Color: formValues.Calendar_Birthday_Color ? formValues.Calendar_Birthday_Color : "#006699",

                Associate_Color: formValues.Associate_Color ? formValues.Associate_Color : "#9A9B9D",
                Associate_MLS_Color: formValues.Associate_MLS_Color ? formValues.Associate_MLS_Color : "#115F67",
                Associate_License_Color: formValues.Associate_License_Color ? formValues.Associate_License_Color : "#005885",
                Associate_Staff_Color: formValues.Associate_Staff_Color ? formValues.Associate_Staff_Color : "#CB2027",
                Associate_Yearly_Color: formValues.Associate_Yearly_Color ? formValues.Associate_Yearly_Color : "#CC6633",
                Associate_Contact_Color: formValues.Associate_Contact_Color ? formValues.Associate_Contact_Color : "#FFD700",
                Associate_Community_Color: formValues.Associate_Community_Color ? formValues.Associate_Community_Color : "#6AC7DB",
                Associate_Buyer_Color: formValues.Associate_Buyer_Color ? formValues.Associate_Buyer_Color : "#82CA9C",
                Associate_Seller_Color: formValues.Associate_Seller_Color ? formValues.Associate_Seller_Color : "#007236",
                Associate_LandLord_Color: formValues.Associate_LandLord_Color ? formValues.Associate_LandLord_Color : "#004A80",
                Associate_Tenant_Color: formValues.Associate_Tenant_Color ? formValues.Associate_Tenant_Color : "#7DA7D9",
                Associate_Vendor_Color: formValues.Associate_Vendor_Color ? formValues.Associate_Vendor_Color : "005500",
                Associate_Co_Op_Color: formValues.Associate_Co_Op_Color ? formValues.Associate_Co_Op_Color : "005500",
                Associate_Team_Color: formValues.Associate_Team_Color ? formValues.Associate_Team_Color : "E88124",

            };
            console.log(userData);
            try {

                await user_service.themecolorsPost(userData).then((response) => {
                    if (response) {
                        // console.log(response);
                        setFormValues(response.data);
                        applyThemeColors(response.data);
                        setLoader({ isActive: false })
                        setToaster({ type: "ThemeColor Add", isShow: true, toasterBody: response.data.message, message: "Add ThemeColor Successfully", });
                        setTimeout(() => {
                            setToaster((prevToaster) => ({ ...prevToaster, isShow: false }))
                        }, 1000);
                    }
                    else {
                        setLoader({ isActive: false })
                        setToaster({ types: "error", isShow: true, toasterBody: response.data.message, message: "Error", });
                    }
                });
            } catch (error) {
                setLoader({ isActive: false })
                setToaster({
                    type: "API Error",
                    isShow: true,
                    toasterBody: error.response.data.message,
                    message: "API Error",
                });

                setTimeout(() => {
                    setToaster((prevToaster) => ({ ...prevToaster, isShow: false }))
                }, 2000);
            }
        }
    }

    const ThemeColorGet = async () => {
        try {
            const response = await user_service.themecolorsGet();
            if (response && response.data) {
                const formattedData = response.data.data;

                // Make sure formattedData[0] exists before accessing properties
                if (formattedData && formattedData.length > 0) {
                    setFormValues(formattedData[0]);
                    applyThemeColors(formattedData[0]);
                } else {
                    console.error("Formatted data is empty or undefined.");
                }
            }
        } catch (error) {
            console.error("Error fetching theme colors:", error);
        }
    };
    useEffect(() => {
        window.scrollTo(0, 0);
        ThemeColorGet();
    }, []);

   
    
    const applyThemeColors = (colors) => {
        const root = document.documentElement;
        root.style.setProperty('--branding-main-color', colors.Branding_Main_Color);
        root.style.setProperty('--branding-accent-color', colors.Branding_Accent_Color);
        root.style.setProperty('--branding-link-color', colors.Branding_Link_Color);
        root.style.setProperty('--branding-confirm-color', colors.Branding_Confirm_Color);
        root.style.setProperty('--branding-focus-color', colors.Branding_Focus_Color);
        root.style.setProperty('--branding-info-color', colors.Branding_Info_Color);
        root.style.setProperty('--branding-alert-color', colors.Branding_Alert_Color);
        root.style.setProperty('--branding-back-color', colors.Branding_Back_Color);
        root.style.setProperty('--branding-header-color', colors.Branding_Header_Color);
        root.style.setProperty('--branding-footer-color', colors.Branding_Footer_Color);


        root.style.setProperty('--transact-res-buyer-color', colors.Transact_Res_Buyer_Color);
        root.style.setProperty('--transact-res-seller-color', colors.Transact_Res_Seller_Color);
        root.style.setProperty('--transact-res-tenant-color', colors.Transact_Res_Tenant_Color);
        root.style.setProperty('--transact-res-landlord-color', colors.Transact_Res_Landlord_Color);
        root.style.setProperty('--transact-apartment-color', colors.Transact_Apartment_Color);
        root.style.setProperty('--transact-com-seller-color', colors.Transact_Com_Seller_Color);
        root.style.setProperty('--transact-com-buyer-color', colors.Transact_Com_Buyer_Color);
        root.style.setProperty('--transact-com-tenant-color', colors.Transact_Com_Tenant_Color);
        root.style.setProperty('--transact-com-landlord-color', colors.Transact_Com_Landlord_Color);
        root.style.setProperty('--transact-referral-only-color', colors.Transact_Referral_Only_Color);
        root.style.setProperty('--calendar-personal-color', colors.Calendar_Personal_Color);
        root.style.setProperty('--calendar-transaction-color', colors.Calendar_Transaction_Color);
        root.style.setProperty('--calendar-group-color', colors.Calendar_Group_Color);
        root.style.setProperty('--calendar-office-color', colors.Calendar_Office_Color);
        root.style.setProperty('--calendar-training-color', colors.Calendar_Training_Color);
        root.style.setProperty('--calendar-community-color', colors.Calendar_Community_Color);
        root.style.setProperty('--calendar-birthday-color', colors.Calendar_Birthday_Color);
        root.style.setProperty('--associate-color', colors.Associate_Color);
        root.style.setProperty('--associate-mls-color', colors.Associate_MLS_Color);
        root.style.setProperty('--associate-license-color', colors.Associate_License_Color);
        root.style.setProperty('--associate-staff-color', colors.Associate_Staff_Color);
        root.style.setProperty('--associate-yearly-color', colors.Associate_Yearly_Color);
        root.style.setProperty('--associate-contact-color', colors.Associate_Contact_Color);
        root.style.setProperty('--associate-community-color', colors.Associate_Community_Color);
        root.style.setProperty('--associate-buyer-color', colors.Associate_Buyer_Color);
        root.style.setProperty('--associate-seller-color', colors.Associate_Seller_Color);
        root.style.setProperty('--associate-landlord-color', colors.Associate_LandLord_Color);
        root.style.setProperty('--associate-tenant-color', colors.Associate_Tenant_Color);
        root.style.setProperty('--associate-vendor-color', colors.Associate_Vendor_Color);
        root.style.setProperty('--associate-co-op-color', colors.Associate_Co_Op_Color);
        root.style.setProperty('--associate-team-color', colors.Associate_Team_Color);
    };

    const handleRemove = async () => {
        if (formValues._id) {

            const userData = {
                agentId: jwt(localStorage.getItem('auth')).id,
                Branding_Main_Color: "#445985",
                Branding_Accent_Color: "#707070",
                Branding_Link_Color: "#3278BD",
                Branding_Confirm_Color: "#5A8E22",
                Branding_Focus_Color: "#E96B12",
                Branding_Info_Color: "#3278BD",
                Branding_Alert_Color: "#FF0000",
                Branding_Back_Color: "#F0F0F0",
                Branding_Header_Color: "#000000",
                Branding_Footer_Color: "#000000",

                Transact_Res_Buyer_Color: "#88CCA0",
                Transact_Res_Seller_Color: "#0F7947",
                Transact_Res_Tenant_Color: "#81A8D6",
                Transact_Res_Landlord_Color: "#004A80",
                Transact_Apartment_Color: "#CCCC33",
                Transact_Com_Seller_Color: "#770000",
                Transact_Com_Buyer_Color: "#BB3A3A",
                Transact_Com_Tenant_Color: "#1CBBB4",
                Transact_Com_Landlord_Color: "#005952",
                Transact_Referral_Only_Color: "#630460",

                Calendar_Personal_Color: "#9A9B9D",
                Calendar_Transaction_Color: "#CB2027",
                Calendar_Group_Color: "#216A63",
                Calendar_Office_Color: "#CC6600",
                Calendar_Training_Color: "#009900",
                Calendar_Community_Color: "#660099",
                Calendar_Birthday_Color: "#006699",

                Associate_Color: "#9A9B9D",
                Associate_MLS_Color: "#115F67",
                Associate_License_Color: "#005885",
                Associate_Staff_Color: "#CB2027",
                Associate_Yearly_Color: "#CC6633",
                Associate_Contact_Color: "#FFD700",
                Associate_Community_Color: "#6AC7DB",
                Associate_Buyer_Color: "#82CA9C",
                Associate_Seller_Color: "#007236",
                Associate_LandLord_Color: "#004A80",
                Associate_Tenant_Color: "#7DA7D9",
                Associate_Vendor_Color: "005500",
                Associate_Co_Op_Color: "005500",
                Associate_Team_Color: "E88124",
            };

            setLoader({ isActive: true })
            await user_service.themecolorsUpdate(formValues._id, userData).then((response) => {
                if (response) {
                    console.log(response);
                    setFormValues(response.data);
                    setLoader({ isActive: false })
                    setToaster({ type: "ThemeColor Delete", isShow: true, toasterBody: response.data.message, message: "ThemeColor Delete Successfully", });
                    setTimeout(() => {
                        setToaster((prevToaster) => ({ ...prevToaster, isShow: false }))
                    }, 1000);
                }
                else {
                    setLoader({ isActive: false })
                    setToaster({ types: "error", isShow: true, toasterBody: response.data.message, message: "Error", });
                }
            });
        }
    }
    const handleBack = () => {
        navigate("/control-panel/")
    }

    return (
        <div className="bg-secondary float-left w-100 pt-4 mb-4">
            <Loader isActive={isActive} />
            {isShow && <Toaster types={types}
                isShow={isShow}
                toasterBody={toasterBody}
                message={message} />}
            <main className="page-wrapper profile_page_wrap">
                <div className="content-overlay">
                    <div className="d-flex align-items-center justify-content-start mb-4">
                        <h3 className="mb-0 text-white">Color Theme for Corporate</h3>
                    </div>
                    {/* <hr className="mt-5" /> */}
                    <div className="add_listing w-100">
                        <div className="bg-light rounded-3 p-3 border mb-3">
                            <h6 className="mb-4">Branding</h6>
                            <div className="row">
                                <div className="col-lg-4 col-md-4 col-sm-4 col-4 d-flex align-items-center justify-content-start">
                                    <p className="">Main Color</p>
                                </div>
                                <div className="col-lg-8 col-md-8 col-sm-8 col-8 d-flex align-items-center justify-content-start">
                                    <InputColor
                                        initialValue={formValues.Branding_Main_Color || formValues.Branding_Main_Color}
                                        name="Branding_Main_Color"
                                        value={formValues.Branding_Main_Color}
                                        onChange={(color) => handleChange(color, "Branding_Main_Color")}
                                        placement="right"
                                    />

                                </div>

                                <div className="col-lg-4 col-md-4 col-sm-4 col-4 d-flex align-items-center justify-content-start">
                                    <p className="">Accent Color</p>
                                </div>
                                <div className="col-lg-8 col-md-8 col-sm-8 col-8 d-flex align-items-center justify-content-start">
                                    <InputColor
                                        initialValue={formValues.Branding_Accent_Color || formValues.Branding_Accent_Color}
                                        name="Branding_Accent_Color"
                                        value={formValues.Branding_Accent_Color}
                                        onChange={(color) => handleChange(color.hex, "Branding_Accent_Color")}
                                        placement="right"
                                    />
                                </div>

                                <div className="col-lg-4 col-md-4 col-sm-4 col-4 d-flex align-items-center justify-content-start">
                                    <p className="">Link Color</p>
                                </div>
                                <div className="col-lg-8 col-md-8 col-sm-8 col-8 d-flex align-items-center justify-content-start">
                                    <InputColor
                                        initialValue={formValues.Branding_Link_Color || formValues.Branding_Link_Color}
                                        name="Branding_Link_Color"
                                        value={formValues.Branding_Link_Color}
                                        onChange={(color) => handleChange(color, "Branding_Link_Color")}
                                        placement="right"
                                    />
                                </div>

                                <div className="col-lg-4 col-md-4 col-sm-4 col-4 d-flex align-items-center justify-content-start">
                                    <p className="">Confirm Color</p>
                                </div>
                                <div className="col-lg-8 col-md-8 col-sm-8 col-8 d-flex align-items-center justify-content-start">
                                    <InputColor
                                        initialValue={formValues.Branding_Confirm_Color || formValues.Branding_Confirm_Color}
                                        name="Branding_Confirm_Color"
                                        value={formValues.Branding_Confirm_Color}
                                        onChange={(color) => handleChange(color, "Branding_Confirm_Color")}
                                        placement="right"
                                    />
                                </div>

                                <div className="col-lg-4 col-md-4 col-sm-4 col-4 d-flex align-items-center justify-content-start">
                                    <p className="">Focus Color</p>
                                </div>
                                <div className="col-lg-8 col-md-8 col-sm-8 col-8 d-flex align-items-center justify-content-start">
                                    <InputColor
                                        initialValue={formValues.Branding_Focus_Color || formValues.Branding_Focus_Color}
                                        name="Branding_Focus_Color"
                                        value={formValues.Branding_Focus_Color}
                                        onChange={(color) => handleChange(color, "Branding_Focus_Color")}
                                        placement="right"
                                    />
                                </div>

                                <div className="col-lg-4 col-md-4 col-sm-4 col-4 d-flex align-items-center justify-content-start">
                                    <p className="">Info Color</p>
                                </div>
                                <div className="col-lg-8 col-md-8 col-sm-8 col-8 d-flex align-items-center justify-content-start">
                                    <InputColor
                                        initialValue={formValues.Branding_Info_Color || formValues.Branding_Info_Color}
                                        name="Branding_Info_Color"
                                        value={formValues.Branding_Info_Color}
                                        onChange={(color) => handleChange(color, "Branding_Info_Color")}
                                        placement="right"
                                    />
                                </div>


                                <div className="col-lg-4 col-md-4 col-sm-4 col-4 d-flex align-items-center justify-content-start">
                                    <p className="">Alert Color</p>
                                </div>
                                <div className="col-lg-8 col-md-8 col-sm-8 col-8 d-flex align-items-center justify-content-start">
                                    <InputColor
                                        initialValue={formValues.Branding_Alert_Color || formValues.Branding_Alert_Color}
                                        name="Branding_Alert_Color"
                                        value={formValues.Branding_Alert_Color}
                                        onChange={(color) => handleChange(color, "Branding_Alert_Color")}
                                        placement="right"
                                    />
                                </div>

                                <div className="col-lg-4 col-md-4 col-sm-4 col-4 d-flex align-items-center justify-content-start">
                                    <p className="">Back Color</p>
                                </div>
                                <div className="col-lg-8 col-md-8 col-sm-8 col-8 d-flex align-items-center justify-content-start">
                                    <InputColor
                                        initialValue={formValues.Branding_Back_Color || formValues.Branding_Back_Color}
                                        name="Branding_Back_Color"
                                        value={formValues.Branding_Back_Color}
                                        onChange={(color) => handleChange(color, "Branding_Back_Color")}
                                        placement="right"
                                    />
                                </div>


                                <div className="col-lg-4 col-md-4 col-sm-4 col-4 d-flex align-items-center justify-content-start">
                                    <p className="">Header Style</p>
                                </div>

                                <div className="col-lg-8 col-md-8 col-sm-8 col-8 d-flex align-items-center justify-content-start">
                                    <InputColor
                                        initialValue={formValues.Branding_Header_Color || formValues.Branding_Header_Color}
                                        name="Branding_Header_Color"
                                        value={formValues.Branding_Header_Color}
                                        onChange={(color) => handleChange(color, "Branding_Header_Color")}
                                        placement="right"
                                    />
                                </div>

                                <div className="col-lg-4 col-md-4 col-sm-4 col-4 d-flex align-items-center justify-content-start">
                                    <p className="">Footer Style</p>
                                </div>

                                <div className="col-lg-8 col-md-8 col-sm-8 col-8 d-flex align-items-center justify-content-start">
                                    <InputColor
                                        initialValue={formValues.Branding_Footer_Color || formValues.Branding_Footer_Color}
                                        name="Branding_Footer_Color"
                                        value={formValues.Branding_Footer_Color}
                                        onChange={(color) => handleChange(color, "Branding_Footer_Color")}
                                        placement="right"
                                    />
                                </div>
                            </div>


                            <div className="row">
                                <h6 className="my-4">Transact</h6>
                                <div className="col-lg-4 col-md-4 col-sm-4 col-4 d-flex align-items-center justify-content-start">
                                    <p className="">Res. Buyer Color</p>
                                </div>

                                <div className="col-lg-8 col-md-8 col-sm-8 col-8 d-flex align-items-center justify-content-start">
                                    <InputColor
                                        initialValue={formValues.Transact_Res_Buyer_Color || formValues.Transact_Res_Buyer_Color}
                                        name="Transact_Res_Buyer_Color"
                                        value={formValues.Transact_Res_Buyer_Color}
                                        onChange={(color) => handleChange(color, "Transact_Res_Buyer_Color")}
                                        placement="right"
                                    />
                                </div>

                                <div className="col-lg-4 col-md-4 col-sm-4 col-4 d-flex align-items-center justify-content-start">
                                    <p className="">Res. Seller Color</p>
                                </div>
                                <div className="col-lg-8 col-md-8 col-sm-8 col-8 d-flex align-items-center justify-content-start">
                                    <InputColor
                                        initialValue={formValues.Transact_Res_Seller_Color || formValues.Transact_Res_Seller_Color}
                                        name="Transact_Res_Seller_Color"
                                        value={formValues.Transact_Res_Seller_Color}
                                        onChange={(color) => handleChange(color, "Transact_Res_Seller_Color")}
                                        placement="right"
                                    />
                                </div>

                                <div className="col-lg-4 col-md-4 col-sm-4 col-4 d-flex align-items-center justify-content-start">
                                    <p className="">Res. Tenant Color</p>
                                </div>
                                <div className="col-lg-8 col-md-8 col-sm-8 col-8 d-flex align-items-center justify-content-start">
                                    <InputColor
                                        initialValue={formValues.Transact_Res_Tenant_Color || formValues.Transact_Res_Tenant_Color}
                                        name="Transact_Res_Tenant_Color"
                                        value={formValues.Transact_Res_Tenant_Color}
                                        onChange={(color) => handleChange(color, "Transact_Res_Tenant_Color")}
                                        placement="right"
                                    />
                                </div>


                                <div className="col-lg-4 col-md-4 col-sm-4 col-4 d-flex align-items-center justify-content-start">
                                    <p className="">Res. Landlord Color</p>
                                </div>
                                <div className="col-lg-8 col-md-8 col-sm-8 col-8 d-flex align-items-center justify-content-start">
                                    <InputColor
                                        initialValue={formValues.Transact_Res_Landlord_Color || formValues.Transact_Res_Landlord_Color}
                                        name="Transact_Res_Landlord_Color"
                                        value={formValues.Transact_Res_Landlord_Color}
                                        onChange={(color) => handleChange(color, "Transact_Res_Landlord_Color")}
                                        placement="right"
                                    />
                                </div>

                                <div className="col-lg-4 col-md-4 col-sm-4 col-4 d-flex align-items-center justify-content-start">
                                    <p>Apartment Color</p>
                                </div>
                                <div className="col-lg-8 col-md-8 col-sm-8 col-8 d-flex align-items-center justify-content-start">
                                    <InputColor
                                        initialValue={formValues.Transact_Apartment_Color || formValues.Transact_Apartment_Color}
                                        name="Transact_Apartment_Color"
                                        value={formValues.Transact_Apartment_Color}
                                        onChange={(color) => handleChange(color, "Transact_Apartment_Color")}
                                        placement="right"
                                    />
                                </div>


                                <div className="col-lg-4 col-md-4 col-sm-4 col-4 d-flex align-items-center justify-content-start">
                                    <p className="">Com. Seller Color</p>
                                </div>
                                <div className="col-lg-8 col-md-8 col-sm-8 col-8 d-flex align-items-center justify-content-start">
                                    <InputColor
                                        initialValue={formValues.Transact_Com_Seller_Color || formValues.Transact_Com_Seller_Color}
                                        name="Transact_Com_Seller_Color"
                                        value={formValues.Transact_Com_Seller_Color}
                                        onChange={(color) => handleChange(color, "Transact_Com_Seller_Color")}
                                        placement="right"
                                    />
                                </div>

                                <div className="col-lg-4 col-md-4 col-sm-4 col-4 d-flex align-items-center justify-content-start">
                                    <p className="">Com. Buyer Color</p>
                                </div>
                                <div className="col-lg-8 col-md-8 col-sm-8 col-8 d-flex align-items-center justify-content-start">
                                    <InputColor
                                        initialValue={formValues.Transact_Com_Buyer_Color || formValues.Transact_Com_Buyer_Color}
                                        name="Transact_Com_Buyer_Color"
                                        value={formValues.Transact_Com_Buyer_Color}
                                        onChange={(color) => handleChange(color, "Transact_Com_Buyer_Color")}
                                        placement="right"
                                    />
                                </div>


                                <div className="col-lg-4 col-md-4 col-sm-4 col-4 d-flex align-items-center justify-content-start">
                                    <p className="">Com. Tenant Color</p>
                                </div>

                                <div className="col-lg-8 col-md-8 col-sm-8 col-8 d-flex align-items-center justify-content-start">
                                    <InputColor
                                        initialValue={formValues.Transact_Com_Tenant_Color || formValues.Transact_Com_Tenant_Color}
                                        name="Transact_Com_Tenant_Color"
                                        value={formValues.Transact_Com_Tenant_Color}
                                        onChange={(color) => handleChange(color, "Transact_Com_Tenant_Color")}
                                        placement="right"
                                    />
                                </div>


                                <div className="col-lg-4 col-md-4 col-sm-4 col-4 d-flex align-items-center justify-content-start">
                                    <p className="">Com. Landlord Color</p>
                                </div>

                                <div className="col-lg-8 col-md-8 col-sm-8 col-8 d-flex align-items-center justify-content-start">
                                    <InputColor
                                        initialValue={formValues.Transact_Com_Landlord_Color || formValues.Transact_Com_Landlord_Color}
                                        name="Transact_Com_Landlord_Color"
                                        value={formValues.Transact_Com_Landlord_Color}
                                        onChange={(color) => handleChange(color, "Transact_Com_Landlord_Color")}
                                        placement="right"
                                    />
                                </div>

                                <div className="col-lg-4 col-md-4 col-sm-4 col-4 d-flex align-items-center justify-content-start">
                                    <p className="">Referral Only Color</p>
                                </div>

                                <div className="col-lg-8 col-md-8 col-sm-8 col-8 d-flex align-items-center justify-content-start">
                                    <InputColor
                                        initialValue={formValues.Transact_Referral_Only_Color || formValues.Transact_Referral_Only_Color}
                                        name="Transact_Referral_Only_Color"
                                        value={formValues.Transact_Referral_Only_Color}
                                        onChange={(color) => handleChange(color, "Transact_Referral_Only_Color")}
                                        placement="right"
                                    />
                                </div>

                            </div>


                            <div className="row">
                                <h6 className="my-4">Calendar</h6>
                                <div className="col-lg-4 col-md-4 col-sm-4 col-4 d-flex align-items-center justify-content-start">
                                    <p className="">Personal Color</p>
                                </div>

                                <div className="col-lg-8 col-md-8 col-sm-8 col-8 d-flex align-items-center justify-content-start">
                                    <InputColor
                                        initialValue={formValues.Calendar_Personal_Color || formValues.Calendar_Personal_Color}
                                        name="Calendar_Personal_Color"
                                        value={formValues.Calendar_Personal_Color}
                                        onChange={(color) => handleChange(color, "Calendar_Personal_Color")}
                                        placement="right"
                                    />
                                </div>

                                <div className="col-lg-4 col-md-4 col-sm-4 col-4 d-flex align-items-center justify-content-start">
                                    <p className="">Transaction Color</p>
                                </div>
                                <div className="col-lg-8 col-md-8 col-sm-8 col-8 d-flex align-items-center justify-content-start">
                                    <InputColor
                                        initialValue={formValues.Calendar_Transaction_Color || formValues.Calendar_Transaction_Color}
                                        name="Calendar_Transaction_Color"
                                        value={formValues.Calendar_Transaction_Color}
                                        onChange={(color) => handleChange(color, "Calendar_Transaction_Color")}
                                        placement="right"
                                    />
                                </div>

                                <div className="col-lg-4 col-md-4 col-sm-4 col-4 d-flex align-items-center justify-content-start">
                                    <p className="">Group Color</p>
                                </div>
                                <div className="col-lg-8 col-md-8 col-sm-8 col-8 d-flex align-items-center justify-content-start">
                                    <InputColor
                                        initialValue={formValues.Calendar_Group_Color || formValues.Calendar_Group_Color}
                                        name="Calendar_Group_Color"
                                        value={formValues.Calendar_Group_Color}
                                        onChange={(color) => handleChange(color, "Calendar_Group_Color")}
                                        placement="right"
                                    />
                                </div>

                                <div className="col-lg-4 col-md-4 col-sm-4 col-4 d-flex align-items-center justify-content-start">
                                    <p className="">Office Color</p>
                                </div>
                                <div className="col-lg-8 col-md-8 col-sm-8 col-8 d-flex align-items-center justify-content-start">
                                    <InputColor
                                        initialValue={formValues.Calendar_Office_Color || formValues.Calendar_Office_Color}
                                        name="Calendar_Office_Color"
                                        value={formValues.Calendar_Office_Color}
                                        onChange={(color) => handleChange(color, "Calendar_Office_Color")}
                                        placement="right"
                                    />
                                </div>

                                <div className="col-lg-4 col-md-4 col-sm-4 col-4 d-flex align-items-center justify-content-start">
                                    <p className="">Training Color</p>
                                </div>
                                <div className="col-lg-8 col-md-8 col-sm-8 col-8 d-flex align-items-center justify-content-start">
                                    <InputColor
                                        initialValue={formValues.Calendar_Training_Color || formValues.Calendar_Training_Color}
                                        name="Calendar_Training_Color"
                                        value={formValues.Calendar_Training_Color}
                                        onChange={(color) => handleChange(color, "Calendar_Training_Color")}
                                        placement="right"
                                    />
                                </div>

                                <div className="col-lg-4 col-md-4 col-sm-4 col-4 d-flex align-items-center justify-content-start">
                                    <p className="">Community Color</p>
                                </div>
                                <div className="col-lg-8 col-md-8 col-sm-8 col-8 d-flex align-items-center justify-content-start">
                                    <InputColor
                                        initialValue={formValues.Calendar_Community_Color || formValues.Calendar_Community_Color}
                                        name="Calendar_Community_Color"
                                        value={formValues.Calendar_Community_Color}
                                        onChange={(color) => handleChange(color, "Calendar_Community_Color")}
                                        placement="right"
                                    />
                                </div>


                                <div className="col-lg-4 col-md-4 col-sm-4 col-4 d-flex align-items-center justify-content-start">
                                    <p className="">Birthday Color</p>
                                </div>
                                <div className="col-lg-8 col-md-8 col-sm-8 col-8 d-flex align-items-center justify-content-start">
                                    <InputColor
                                        initialValue={formValues.Calendar_Birthday_Color || formValues.Calendar_Birthday_Color}
                                        name="Calendar_Birthday_Color"
                                        value={formValues.Calendar_Birthday_Color}
                                        onChange={(color) => handleChange(color, "Calendar_Birthday_Color")}
                                        placement="right"
                                    />
                                </div>

                            </div>


                            <div className="row">
                                <h6 className="my-4">Associate</h6>
                                <div className="col-lg-4 col-md-4 col-sm-4 col-4 d-flex align-items-center justify-content-start">
                                    <p className="">Associate Color</p>
                                </div>

                                <div className="col-lg-8 col-md-8 col-sm-8 col-8 d-flex align-items-center justify-content-start">
                                    <InputColor
                                        initialValue={formValues.Associate_Color || formValues.Associate_Color}
                                        name="Associate_Color"
                                        value={formValues.Associate_Color}
                                        onChange={(color) => handleChange(color, "Associate_Color")}
                                        placement="right"
                                    />
                                </div>

                                <div className="col-lg-4 col-md-4 col-sm-4 col-4 d-flex align-items-center justify-content-start">
                                    <p className="">MLS Color</p>
                                </div>
                                <div className="col-lg-8 col-md-8 col-sm-8 col-8 d-flex align-items-center justify-content-start">
                                    <InputColor
                                        initialValue={formValues.Associate_MLS_Color || formValues.Associate_MLS_Color}
                                        name="Associate_MLS_Color"
                                        value={formValues.Associate_MLS_Color}
                                        onChange={(color) => handleChange(color, "Associate_MLS_Color")}
                                        placement="right"
                                    />
                                </div>

                                <div className="col-lg-4 col-md-4 col-sm-4 col-4 d-flex align-items-center justify-content-start">
                                    <p className="">License Color</p>
                                </div>
                                <div className="col-lg-8 col-md-8 col-sm-8 col-8 d-flex align-items-center justify-content-start">
                                    <InputColor
                                        initialValue={formValues.Associate_License_Color || formValues.Associate_License_Color}
                                        name="Associate_License_Color"
                                        value={formValues.Associate_License_Color}
                                        onChange={(color) => handleChange(color, "Associate_License_Color")}
                                        placement="right"
                                    />
                                </div>

                                <div className="col-lg-4 col-md-4 col-sm-4 col-4 d-flex align-items-center justify-content-start">
                                    <p className="">Staff Color</p>
                                </div>
                                <div className="col-lg-8 col-md-8 col-sm-8 col-8 d-flex align-items-center justify-content-start">
                                    <InputColor
                                        initialValue={formValues.Associate_Staff_Color || formValues.Associate_Staff_Color}
                                        name="Associate_Staff_Color"
                                        value={formValues.Associate_Staff_Color}
                                        onChange={(color) => handleChange(color, "Associate_Staff_Color")}
                                        placement="right"
                                    />
                                </div>

                                <div className="col-lg-4 col-md-4 col-sm-4 col-4 d-flex align-items-center justify-content-start">
                                    <p className="">Yearly Color</p>
                                </div>
                                <div className="col-lg-8 col-md-8 col-sm-8 col-8 d-flex align-items-center justify-content-start">
                                    <InputColor
                                        initialValue={formValues.Associate_Yearly_Color || formValues.Associate_Yearly_Color}
                                        name="Associate_Yearly_Color"
                                        value={formValues.Associate_Yearly_Color}
                                        onChange={(color) => handleChange(color, "Associate_Yearly_Color")}
                                        placement="right"
                                    />
                                </div>

                                <div className="col-lg-4 col-md-4 col-sm-4 col-4 d-flex align-items-center justify-content-start">
                                    <p className="mb-0">Contact Color</p>
                                </div>
                                <div className="col-lg-8 col-md-8 col-sm-8 col-8 d-flex align-items-center justify-content-start">
                                    <InputColor
                                        initialValue={formValues.Associate_Contact_Color || formValues.Associate_Contact_Color}
                                        name="Associate_Contact_Color"
                                        value={formValues.Associate_Contact_Color}
                                        onChange={(color) => handleChange(color, "Associate_Contact_Color")}
                                        placement="right"
                                    />
                                </div>


                                <div className="col-lg-4 col-md-4 col-sm-4 col-4 d-flex align-items-center justify-content-start">
                                    <p className="">Community Color</p>
                                </div>
                                <div className="col-lg-8 col-md-8 col-sm-8 col-8 d-flex align-items-center justify-content-start">
                                    <InputColor
                                        initialValue={formValues.Associate_Community_Color || formValues.Associate_Community_Color}
                                        name="Associate_Community_Color"
                                        value={formValues.Associate_Community_Color}
                                        onChange={(color) => handleChange(color, "Associate_Community_Color")}
                                        placement="right"
                                    />
                                </div>

                                <div className="col-lg-4 col-md-4 col-sm-4 col-4 d-flex align-items-center justify-content-start">
                                    <p className="">Buyer Color</p>
                                </div>
                                <div className="col-lg-8 col-md-8 col-sm-8 col-8 d-flex align-items-center justify-content-start">
                                    <InputColor
                                        initialValue={formValues.Associate_Buyer_Color || formValues.Associate_Buyer_Color}
                                        name="Associate_Buyer_Color"
                                        value={formValues.Associate_Buyer_Color}
                                        onChange={(color) => handleChange(color, "Associate_Buyer_Color")}
                                        placement="right"
                                    />
                                </div>


                                <div className="col-lg-4 col-md-4 col-sm-4 col-4 d-flex align-items-center justify-content-start">
                                    <p className="">Seller Color</p>
                                </div>

                                <div className="col-lg-8 col-md-8 col-sm-8 col-8 d-flex align-items-center justify-content-start">
                                    <InputColor
                                        initialValue={formValues.Associate_Seller_Color || formValues.Associate_Seller_Color}
                                        name="Associate_Seller_Color"
                                        value={formValues.Associate_Seller_Color}
                                        onChange={(color) => handleChange(color, "Associate_Seller_Color")}
                                        placement="right"
                                    />
                                </div>


                                <div className="col-lg-4 col-md-4 col-sm-4 col-4 d-flex align-items-center justify-content-start">
                                    <p className="">LandLord Color</p>
                                </div>

                                <div className="col-lg-8 col-md-8 col-sm-8 col-8 d-flex align-items-center justify-content-start">
                                    <InputColor
                                        initialValue={formValues.Associate_LandLord_Color || formValues.Associate_LandLord_Color}
                                        name="Associate_LandLord_Color"
                                        value={formValues.Associate_LandLord_Color}
                                        onChange={(color) => handleChange(color, "Associate_LandLord_Color")}
                                        placement="right"
                                    />
                                </div>

                                <div className="col-lg-4 col-md-4 col-sm-4 col-4 d-flex align-items-center justify-content-start">
                                    <p className="">Tenant Color</p>
                                </div>

                                <div className="col-lg-8 col-md-8 col-sm-8 col-8 d-flex align-items-center justify-content-start">
                                    <InputColor
                                        initialValue={formValues.Associate_Tenant_Color || formValues.Associate_Tenant_Color}
                                        name="Associate_Tenant_Color"
                                        value={formValues.Associate_Tenant_Color}
                                        onChange={(color) => handleChange(color, "Associate_Tenant_Color")}
                                        placement="right"
                                    />
                                </div>

                                <div className="col-lg-4 col-md-4 col-sm-4 col-4 d-flex align-items-center justify-content-start">
                                    <p className="">Vendor Color</p>
                                </div>

                                <div className="col-lg-8 col-md-8 col-sm-8 col-8 d-flex align-items-center justify-content-start">
                                    <InputColor
                                        initialValue={formValues.Associate_Vendor_Color || formValues.Associate_Vendor_Color}
                                        namer="Associate_Vendor_Color"
                                        value={formValues.Associate_Vendor_Color}
                                        onChange={(color) => handleChange(color, "Associate_Vendor_Color")}
                                        placement="right"
                                    />
                                </div>

                                <div className="col-lg-4 col-md-4 col-sm-4 col-4 d-flex align-items-center justify-content-start">
                                    <p className="">Co-Op Color</p>
                                </div>

                                <div className="col-lg-8 col-md-8 col-sm-8 col-8 d-flex align-items-center justify-content-start">
                                    <InputColor
                                        initialValue={formValues.Associate_Co_Op_Color || formValues.Associate_Co_Op_Color}
                                        name="Associate_Co_Op_Color"
                                        value={formValues.Associate_Co_Op_Color}
                                        onChange={(color) => handleChange(color, "Associate_Co_Op_Color")}
                                        placement="right"
                                    />
                                </div>

                                <div className="col-lg-4 col-md-4 col-sm-4 col-4 d-flex align-items-center justify-content-start">
                                    <p className="">Team Color</p>
                                </div>

                                <div className="col-lg-8 col-md-8 col-sm-8 col-8 d-flex align-items-center justify-content-start">
                                    <InputColor
                                        initialValue={formValues.Associate_Team_Color || formValues.Associate_Team_Color}
                                        name="Associate_Team_Color"
                                        value={formValues.Associate_Team_Color}
                                        onChange={(color) => handleChange(color, "Associate_Team_Color")}
                                        placement="right"
                                    />
                                </div>

                            </div>


                        </div>
                        <div className="pull-right mt-3">
                            {/* <hr className="mt-2" /> */}
                            <button className="btn btn-secondary pull-right ms-3" type="button" onClick={handleBack}>Cancel</button>
                            <button className="btn btn-primary pull-right ms-3" type="button" onClick={handleSubmit}>Update Theme</button>
                            <button className="btn btn-secondary pull-left" type="button" onClick={(e) => handleRemove()}>Reset Colors</button>

                        </div>
                    </div>
                </div>
            </main>
        </div>
    )
}
export default ColorTheme;


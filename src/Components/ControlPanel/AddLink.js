import React, { useState, useEffect, useRef } from "react";
import _ from "lodash";
import user_service from "../service/user_service";
import jwt from "jwt-decode";
import Loader from "../../Pages/Loader/Loader.js";
import Toaster from "../../Pages/Toaster/Toaster.js";
import { useNavigate, NavLink, useParams } from "react-router-dom";
// import { Editor } from "@tinymce/tinymce-react";

function AddLink() {
  const initialValues = {
    title: "",
    description: "",
    parent_section: "",
    publication_level: "",
    exceptions: "",
    pin: "",
    lock: "",
    resource_url: "",
  };
  const [formValues, setFormValues] = useState(initialValues);
  const [formErrors, setFormErrors] = useState({});
  const [isSubmitClick, setISSubmitClick] = useState(false);

  const [loader, setLoader] = useState({ isActive: null });
  const { isActive } = loader;
  const [toaster, setToaster] = useState({
    types: null,
    isShow: null,
    toasterBody: "",
    message: "",
  });
  const { types, isShow, toasterBody, message } = toaster;

  const [mainNavLinkText, setMainNavLinkText] = useState("Corporate");
  const [showData, setShowData] = useState("Document");
  const [showDataId, setShowDataId] = useState("");
  const [checkManager, setCheckManager] = useState("");
  const [checkPin, setCheckPin] = useState("");
  const [documentGet, setDocumentGet] = useState([]);
  const [showByDefaultId, setShowByDefaultId] = useState("");
  const [showByTitle, setShowByTitle] = useState("");

  const [notesmessage, setNotesmessage] = useState("");
  // const editorRef = useRef(null);
  // const log = () => {
  //   if (editorRef.current) {
  //     setNotesmessage(editorRef.current.getContent());
  //   }
  // };

  const navigate = useNavigate();
  const params = useParams();
  console.log(params.id);

  // onChange Function start
  const handleChange = (e) => {
    const { name, value } = e.target;
    setFormValues({ ...formValues, [name]: value });
  };
  // onChange Function end

  {
    /* <!-- Form Validation Start--> */
  }
  useEffect(() => {
    if (formValues && isSubmitClick) {
      validate();
    }
  }, [formValues]);

  const validate = () => {
    const values = formValues;
    const errors = {};

    if (!values.title) {
      errors.title = "Title is required.";
    }

    if (!values.description) {
      errors.description = "Description is required.";
    }

    if (!values.resource_url) {
      errors.resource_url = "Resource_url is required.";
    }

    setFormErrors(errors);
    return errors;
  };

  {
    /* <!-- Form Validation End--> */
  }

  const handleSubmit = async (e) => {
    if (params.id) {
      e.preventDefault();
      setISSubmitClick(true);
      let checkValue = validate();
      if (_.isEmpty(checkValue)) {
        const userData = {
          agentId: jwt(localStorage.getItem("auth")).id,
          office_id: localStorage.getItem("active_office_id"),
          office_name: localStorage.getItem("active_office"),
          title: formValues.title,
          type: "link",
          description: formValues.description,
          parent_section: showDataId
            ? showDataId
            : showByDefaultId
            ? showByDefaultId
            : "",
          publication_level: mainNavLinkText,
          exceptions: "",
          resource_url: formValues.resource_url,
          pin: checkPin ? checkPin : "no",
          lock: checkManager ? checkManager : "no",
          is_active: "yes",
        };
        setLoader({ isActive: true });
        await user_service
          .OfficesharedDocContentUpdate(params.id, userData)
          .then((response) => {
            if (response) {
              console.log(response);
              setFormValues(response.data);
              setLoader({ isActive: false });
              setToaster({
                type: "Add Section",
                isShow: true,
                toasterBody: response.data.message,
                message: "Add Section Successfully",
              });
              setTimeout(() => {
                setToaster((prevToaster) => ({
                  ...prevToaster,
                  isShow: false,
                }));
                navigate("/control-panel/documents/");
              }, 1000);
            } else {
              setLoader({ isActive: false });
              setToaster({
                types: "error",
                isShow: true,
                toasterBody: response.data.message,
                message: "Error",
              });
            }
          });
      }
    } else {
      e.preventDefault();
      setISSubmitClick(true);
      let checkValue = validate();
      if (_.isEmpty(checkValue)) {
        const userData = {
          agentId: jwt(localStorage.getItem("auth")).id,
          office_id: localStorage.getItem("active_office_id"),
          office_name: localStorage.getItem("active_office"),
          title: formValues.title,
          type: "link",
          description: formValues.description,
          parent_section: showDataId
            ? showDataId
            : showByDefaultId
            ? showByDefaultId
            : "",
          publication_level: mainNavLinkText,
          exceptions: "",
          resource_url: formValues.resource_url,
          pin: checkPin ? checkPin : "no",
          lock: checkManager ? checkManager : "no",
          is_active: "yes",
        };
        setLoader({ isActive: true });
        await user_service
          .OfficesharedDocContentPost(userData)
          .then((response) => {
            if (response) {
              console.log(response);
              setFormValues(response.data);
              setLoader({ isActive: false });
              setToaster({
                type: "Add Section",
                isShow: true,
                toasterBody: response.data.message,
                message: "Add Section Successfully",
              });
              setTimeout(() => {
                setToaster((prevToaster) => ({
                  ...prevToaster,
                  isShow: false,
                }));
                navigate("/control-panel/documents/");
              }, 1000);
            } else {
              setLoader({ isActive: false });
              setToaster({
                types: "error",
                isShow: true,
                toasterBody: response.data.message,
                message: "Error",
              });
            }
          });
      }
    }
  };

  useEffect(() => {
    window.scrollTo(0, 0);
    setLoader({ isActive: true });

    const ShareDocGet = async () => {
      await user_service.ShareDocGet().then((response) => {
        setLoader({ isActive: false });
        if (response) {
          setDocumentGet(response.data);
          const getResponceId = response.data.data[0]._id;
          const getResponcetitle = response.data.data[0].title;
          setShowByDefaultId(getResponceId);
          setShowByTitle(getResponcetitle);
        }
      });
    };
    ShareDocGet();
  }, []);

  const handleDataClick = (item, id) => {
    setShowByTitle(item);
    setShowDataId(id);
    document.getElementById("closeModal").click();
  };

  {
    /* <!-- Api call Form onSubmit End--> */
  }

  const handleBack = () => {
    navigate("/control-panel/documents/");
  };

  const checkManagerLock = (e) => {
    setCheckManager(e.target.name);
    setCheckManager(e.target.value);
  };

  const checkDocumentOrder = (e) => {
    setCheckPin(e.target.name);
    setCheckPin(e.target.value);
  };

  const handleMainNavLinkClick = (e, item) => {
    setMainNavLinkText(item);
    document.getElementById("closeLocation").click();
  };

  useEffect(() => {
    window.scrollTo(0, 0);
    if (params.id) {
      setLoader({ isActive: true });
      const OfficesharedDocContenGetByid = async () => {
        await user_service
          .OfficesharedDocContenGet(params.id)
          .then((response) => {
            setLoader({ isActive: false });
            if (response) {
              setFormValues(response.data);
              const ShareDocGetBySectionId = async () => {
                await user_service
                  .ShareDocGetById(response.data.parent_section)
                  .then((responsee) => {
                    setLoader({ isActive: false });
                    if (responsee) {
                      setShowData(responsee.data.title);
                    }
                  });
              };
              ShareDocGetBySectionId();
            }
          });
      };
      OfficesharedDocContenGetByid();
    }
  }, [params.id]);



  const handleRemove = async () => {
    setLoader({ isActive: true });
    await user_service
      .OfficesharedDocContentDelete(params.id)
      .then((response) => {
        if (response) {
          const ShareDocGet = async () => {
            await user_service.ShareDocGet().then((response) => {
              setLoader({ isActive: false });
              if (response) {
                setDocumentGet(response.data);
                setToaster({
                  types: "Document_Delete",
                  isShow: true,
                  toasterBody: response.data.message,
                  message: "Document_Delete Successfully",
                });
                setTimeout(() => {
                  setToaster((prevToaster) => ({
                    ...prevToaster,
                    isShow: false,
                  }));
                  navigate("/control-panel/documents/");
                }, 1000);
              }
            });
          };
          ShareDocGet();
        }
      });
  };
  
  return (
    <div className="bg-secondary float-left w-100 pt-4 mb-4">
      <Loader isActive={isActive} />
      {isShow && (
        <Toaster
          types={types}
          isShow={isShow}
          toasterBody={toasterBody}
          message={message}
        />
      )}
      <main className="page-wrapper profile_page_wrap">
        <div className="container content-overlay">
          <div className="d-flex align-items-center justify-content-start mb-4">
            <h3 className="mb-0 text-white">Add New Link</h3>
          </div>

          <div className="">
            <div className="">
              <div className="bg-light rounded-3 p-3 border mb-3">
                <div className="add_listing">
                  <h5>Link Details</h5>
                  <div className="row">
                    <div className="col-md-6">
                      <div className="col-sm-12">
                        <h6>Title</h6>
                        <label className="form-label">
                          What is the display title of the document?
                        </label>
                        <input
                          className="form-control"
                          id="inline-form-input"
                          type="text"
                          name="title"
                          value={formValues.title}
                          onChange={handleChange}
                        />
                        {formErrors.title && (
                          <div className="invalid-tooltip">
                            {formErrors.title}
                          </div>
                        )}
                      </div>

                      <div className="col-sm-12 mt-3">
                        <label className="form-label">
                          What is the URL to the resource?
                        </label>
                        <input
                          className="form-control"
                          id="inline-form-input"
                          type="text"
                          name="resource_url"
                          value={formValues.resource_url}
                          onChange={handleChange}
                        />
                        {formErrors.resource_url && (
                          <div className="invalid-tooltip">
                            {formErrors.resource_url}
                          </div>
                        )}
                      </div>

                      <div className="col-sm-12 mt-3">
                        <h6>Description</h6>
                        <label className="form-label">
                          What does this file contain?
                        </label>
                        <input
                          className="form-control"
                          id="inline-form-input"
                          type="text"
                          name="description"
                          value={formValues.description}
                          onChange={handleChange}
                        />
                        {formErrors.description && (
                          <div className="invalid-tooltip">
                            {formErrors.description}
                          </div>
                        )}
                      </div>

                      <div className="col-sm-12 mt-3">
                        <h6>Section</h6>
                        <label className="form-label">
                          In which section is this document filed?
                        </label>
                        <div className="d-flex">
                          <p>
                            {showByTitle ? showByTitle : ""}&nbsp;&nbsp;
                            <NavLink>
                              <span
                                type="button"
                                data-toggle="modal"
                                data-target="#addSection"
                              >
                                change
                              </span>
                            </NavLink>
                          </p>
                        </div>
                      </div>

                      <div className="col-sm-12 mt-3">
                        <h6>Publication Level</h6>
                        <label className="form-label">
                          At what location level is this document published?
                        </label>
                        <div className="d-flex">
                          <p>
                            {mainNavLinkText}&nbsp;&nbsp;
                            <NavLink>
                              <span
                                type="button"
                                data-toggle="modal"
                                data-target="#addLocation"
                              >
                                change
                              </span>
                            </NavLink>
                          </p>
                        </div>
                      </div>

                      <div className="col-sm-12 mt-3">
                        <h6>Exceptions</h6>
                        <label className="form-label">
                          Hide this document at these locations:
                        </label>
                        <div className="d-flex">
                          <NavLink>
                            <span
                              type="button"
                              data-toggle="modal"
                              data-target="#addExceptions"
                            >
                              Add Exceptions
                            </span>
                          </NavLink>
                        </div>
                      </div>
                    </div>

                    <div className="col-md-6 mt-lg-0 mt-md-0 mt-sm-4 mt-4">
                      <div
                        className="col-sm-12"
                        onClick={(e) => checkDocumentOrder(e)}
                      >
                        <h6>Document Order</h6>
                        <label className="form-label">
                          Pin this document to the top of the section?
                        </label>
                        <div className="d-flex">
                          <div className="form-check">
                            <input
                              className="form-check-input"
                              id="form-check-1"
                              type="radio"
                              name="pin"
                              value="No"
                              checked
                            />
                            <label
                              className="form-check-label"
                              id="radio-level1"
                            >
                              No
                            </label>
                          </div>
                          <div className="form-check ms-3">
                            <input
                              className="form-check-input"
                              id="form-check-2"
                              type="radio"
                              name="pin"
                              value="Yes"
                            />
                            <label
                              className="form-check-label"
                              id="radio-level2"
                            >
                              Yes
                            </label>
                          </div>
                        </div>
                      </div>

                      <div
                        className="col-sm-12 mt-3"
                        onClick={(e) => checkManagerLock(e)}
                      >
                        <h6>Manager Lock</h6>
                        <label className="form-label">
                          Lock lower locations from hiding this document?
                        </label>
                        <div className="d-flex">
                          <div className="form-check">
                            <input
                              className="form-check-input"
                              id="form-check-3"
                              type="radio"
                              name="lock"
                              value="No"
                              checked
                            />
                            <label
                              className="form-check-label"
                              id="radio-level3"
                            >
                              No
                            </label>
                          </div>
                          <div className="form-check ms-3">
                            <input
                              className="form-check-input"
                              id="form-check-4"
                              type="radio"
                              name="lock"
                              value="Yes"
                            />
                            <label
                              className="form-check-label"
                              id="radio-level4"
                            >
                              Yes
                            </label>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div className="pull-right mt-3">
                {localStorage.getItem("auth") &&
                  jwt(localStorage.getItem("auth")).contactType === "admin" &&
                  params.id && (
                    <button
                      className="btn btn-secondary pull-right mt-2 ms-2"
                      type="button"
                      onClick={(e) => handleRemove()}
                    >
                      Remove
                    </button>
                  )}
                <button
                  className="btn btn-secondary pull-right mt-2 ms-3"
                  type="button"
                  onClick={handleBack}
                >
                  Cancel
                </button>
                <button
                  className="btn btn-primary pull-right mt-2"
                  type="button"
                  onClick={handleSubmit}
                >
                  Add Link Now
                </button>
              </div>
            </div>
            {/* <div className="col-md-4">

                        </div>  */}
          </div>
        </div>
      </main>
      <div className="modal in" role="dialog" id="addSection">
        <div
          className="modal-dialog modal-dialog-centered modal-dialog-scrollable"
          role="document"
        >
          <div className="modal-content">
            <div className="modal-header border-0 pb-0">
              {/* <h4 className="modal-title"></h4> */}
              <button
                id="closeModal"
                className="btn-close"
                type="button"
                data-dismiss="modal"
                aria-label="Close"
              ></button>
            </div>
            <div className="modal-body fs-sm pt-0">
              <div className="content-overlay">
                <div className="bg-light">
                  <h6>Choose a section from the tree.</h6>
                  <div>
                    {documentGet.data && documentGet.data.length > 0 ? (
                      <NestedArray
                        items={documentGet.data}
                        handleDataClick={handleDataClick}
                      />
                    ) : null}
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className="modal in" role="dialog" id="addLocation">
        <div
          className="modal-dialog modal-dialog-centered modal-dialog-scrollable"
          role="document"
        >
          <div className="modal-content">
            <div className="modal-header border-0 pb-0">
              {/* <h4 className="modal-title"></h4> */}
              <button
                className="btn-close"
                type="button"
                data-dismiss="modal"
                aria-label="Close"
              ></button>
            </div>
            <div className="modal-body fs-sm pt-0">
              <div className="content-overlay">
                <div className="bg-light">
                  <p className="mb-2">Choose a location. </p>
                  {/* <button type="button" className="btn btn-default btn btn-primary btn-sm mt-0 pull-right" id="closeLocation" data-dismiss="modal">X</button> */}
                  <p>Choose the location from the tree.</p>
                  <div className="pull-right mt-4">
                    <NavLink className="btn btn-sm btn-primary">
                      <span
                        onClick={(e) => handleMainNavLinkClick(e, "Corporate")}
                      >
                        Corporate
                      </span>
                    </NavLink>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className="modal in" role="dialog" id="addExceptions">
        <div
          className="modal-dialog modal-dialog-centered modal-dialog-scrollable"
          role="document"
        >
          <div className="modal-content">
            <div className="modal-header border-0 pb-0">
              {/* <h4 className="modal-title"></h4> */}
              <button
                className="btn-close"
                type="button"
                data-dismiss="modal"
                aria-label="Close"
              ></button>
            </div>
            <div className="modal-body fs-sm pt-0">
              <div className="content-overlay">
                <div className="bg-light">
                  <div className="">
                    <div>
                      <p>Choose a location.</p>
                      <p>Choose the location from the tree.</p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
export default AddLink;

function NestedArray({ items, handleDataClick }) {
  return (
    <ul>
      {items.map((item) => (
        <li key={item._id}>
          <div
            className="mb-4 ms-2"
            onClick={() => handleDataClick(item.title, item._id)}
          >
            <NavLink>
              <p className="mt-4">{item.title}</p>
            </NavLink>
          </div>

          {item.children && item.children.length > 0 && (
            <NestedArray
              items={item.children}
              handleDataClick={handleDataClick}
            />
          )}
          {/* 
                    {item.content && item.content.length > 0 && (
                        <ul>
                            {item.content.map((contentItem) => (
                                <li key={contentItem._id}>
                                    <div className="mb-4 ms-2" onClick={() => handleDataClick(contentItem.title, contentItem._id)}>
                                        <NavLink>
                                            <p className="mt-4 ms-5">{contentItem.title}</p>
                                        </NavLink>
                                    </div>
                                </li>
                            ))}
                        </ul>
                    )} */}
        </li>
      ))}
    </ul>
  );
}

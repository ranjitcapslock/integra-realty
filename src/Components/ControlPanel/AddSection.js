import React, { useState, useEffect } from "react";
import _ from 'lodash';
import user_service from '../service/user_service';
import jwt from "jwt-decode";
import Loader from "../../Pages/Loader/Loader.js";
import Toaster from "../../Pages/Toaster/Toaster.js";
import { useNavigate, NavLink, useParams } from "react-router-dom";

function AddSection() {
    const initialValues = {
        office_id: localStorage.getItem("active_office_id"),
        office_name: localStorage.getItem("active_office"),
        title: "", blurb: "", parent_section: "", publication_level: "",
        security: ""
    };
    const [formValues, setFormValues] = useState(initialValues);
    const [formErrors, setFormErrors] = useState({});
    const [isSubmitClick, setISSubmitClick] = useState(false)

    const [loader, setLoader] = useState({ isActive: null })
    const { isActive } = loader;
    const [toaster, setToaster] = useState({ types: null, isShow: null, toasterBody: "", message: "" });
    const { types, isShow, toasterBody, message } = toaster;

    const [mainNavLinkText, setMainNavLinkText] = useState("Corporate");
    const [checkRadioSecurity, setCheckRadioSecurity] = useState("no");
    const [documentGet, setDocumentGet] = useState([])
    const [showData, setShowData] = useState("");
    const [showDataId, setShowDataId] = useState("");
    const [showByDefaultId, setShowByDefaultId] = useState("");
    const [showByTitle, setShowByTitle] = useState("");
    const navigate = useNavigate();
    const params = useParams();
    // onChange Function start
    const handleChange = (e) => {
        const { name, value } = e.target;
        setFormValues({ ...formValues, [name]: value })
    };
    // onChange Function end


    {/* <!-- Form Validation Start--> */ }
    useEffect(() => {
        if (formValues && isSubmitClick) {
            validate()
        }
    }, [formValues])




    const validate = () => {
        const values = formValues
        const errors = {};

        if (!values.title) {
            errors.title = "Title is required.";
        }

        if (!values.blurb) {
            errors.blurb = "Blurb is required.";
        }

        setFormErrors(errors)
        return errors;
    }
    {/* <!-- Form Validation End--> */ }
    const handleSubmit = async (e) => {
        if (params.id) {
            e.preventDefault();
            setISSubmitClick(true)
            let checkValue = validate()
            if (_.isEmpty(checkValue)) {
                const userData = {
                    agentId: jwt(localStorage.getItem('auth')).id,
                    title: formValues.title,
                    blurb: formValues.blurb,
                    publication_level: mainNavLinkText,
                    security: checkRadioSecurity ? checkRadioSecurity : "no",
                    parent_section: showDataId ? showDataId : showByDefaultId ? showByDefaultId : null,
                    office_id: localStorage.getItem("active_office_id"),
                    office_name: localStorage.getItem("active_office"),
                    is_active: "yes",
                };
                //console.log(userData);
                setLoader({ isActive: true })
                await user_service.ShareDocPostById(params.id, userData).then((response) => {
                    if (response) {
                        console.log(response);
                        setFormValues(response.data);
                        setLoader({ isActive: false })
                        setToaster({ type: "Add Section", isShow: true, toasterBody: response.data.message, message: "Add Section Successfully", });
                        setTimeout(() => {
                            setToaster((prevToaster) => ({ ...prevToaster, isShow: false }))
                            navigate("/control-panel/documents/");
                        }, 1000);
                    }
                    else {
                        setLoader({ isActive: false })
                        setToaster({ types: "error", isShow: true, toasterBody: response.data.message, message: "Error", });
                    }
                });
            }
        }
        else {
            e.preventDefault();
            setISSubmitClick(true)
            let checkValue = validate()
            if (_.isEmpty(checkValue)) {
                const userData = {
                    agentId: jwt(localStorage.getItem('auth')).id,
                    title: formValues.title,
                    blurb: formValues.blurb,
                    publication_level: mainNavLinkText,
                    security: checkRadioSecurity ? checkRadioSecurity : "no",
                    parent_section: showDataId ? showDataId : showByDefaultId ? showByDefaultId : null,
                    is_active: "yes",
                    office_id: localStorage.getItem("active_office_id"),
                    office_name: localStorage.getItem("active_office"),
                };
                console.log(userData);
                setLoader({ isActive: true })
                await user_service.ShareDocPost(userData).then((response) => {
                    if (response) {
                        // console.log(response);
                        setFormValues(response.data);
                        setLoader({ isActive: false })
                        setToaster({ type: "Add Section", isShow: true, toasterBody: response.data.message, message: "Add Section Successfully", });
                        setTimeout(() => {
                            setToaster((prevToaster) => ({ ...prevToaster, isShow: false }))
                            navigate("/control-panel/documents/");
                        }, 1000);
                    }
                    else {
                        setLoader({ isActive: false })
                        setToaster({ types: "error", isShow: true, toasterBody: response.data.message, message: "Error", });
                    }
                });
            }
        }
    }



    {/* <!-- Api call Form onSubmit End--> */ }


    const handleBack = () => {
        navigate("/control-panel/documents/")
    }


    const checkSecurity = (e) => {
        setCheckRadioSecurity(e.target.name)
        setCheckRadioSecurity(e.target.value)
    }
    const handleMainNavLinkClick = (e, item) => {
        setMainNavLinkText(item);
        document.getElementById('closeLocation').click();
    };





    useEffect(() => {
        window.scrollTo(0, 0);
        setLoader({ isActive: true });
        const ShareDocGet = async () => {
            await user_service.ShareDocGet(localStorage.getItem("active_office")).then((response) => {
                setLoader({ isActive: false });
                if (response) {
                    console.log(response.data.data);
                    setDocumentGet(response.data);
                    if (response.data.data.length > 0) {

                        const getResponceId = response.data.data[0]._id;
                        const getResponcetitle = response.data.data[0].title;
                        setShowByDefaultId(getResponceId);
                        setShowByTitle(getResponcetitle)
                    }
                }

            });
        };
        ShareDocGet();
    }, []);


    const ShareDocGetByid = async () => {
        await user_service.ShareDocGetById(params.id).then((response) => {
            setLoader({ isActive: false });
            if (response) {
                setFormValues(response.data)

                const ShareDocGetBySectionId = async () => {
                    await user_service.ShareDocGetById(response.data.parent_section).then((responsee) => {
                        setLoader({ isActive: false });
                        if (responsee) {
                            setShowData(responsee.data.title)
                        }
                    });
                };
                ShareDocGetBySectionId()
            }
        });
    };

    useEffect(() => {
        if (params.id) {
            setLoader({ isActive: true });
            ShareDocGetByid(params.id);
        }
    }, [params.id]);


    const handleDataClick = (item, id) => {
        setShowData(item)
        setShowDataId(id)
        document.getElementById('closeModal').click();
    };


    return (
        <div className="bg-secondary float-left w-100 pt-4 mb-4">
            <Loader isActive={isActive} />
            {isShow && <Toaster types={types} isShow={isShow} toasterBody={toasterBody} message={message} />}
            <main className="page-wrapper">
                <div className="content-overlay">
                    <div className="d-flex align-items-center justify-content-start mb-4">
                        <h3 className="mb-0 text-white">Create a new section</h3>
                    </div>
                        <div className="">
                            {/* <hr className="mt-2" /> */}
                            <div className="">
                                <div className="row">
                                    <div className="col-md-8">
                                        <div className="float-left w-100 bg-light rounded-3 border p-3">
                                        <div className="col-sm-12">
                                            <h6 className="mb-2">Title</h6>
                                            <label className="form-label">What is the display title of the section?</label>
                                            <input className="form-control" id="inline-form-input" type="text"
                                                name="title"
                                                value={formValues.title}
                                                onChange={handleChange}
                                            />
                                            {formErrors.title && (
                                                <div className="invalid-tooltip">{formErrors.title}</div>
                                            )}
                                        </div>
                                        <div className="col-sm-12 mt-3">
                                            <h6 className="mb-2">Blurb</h6>
                                            <label className="form-label">Brief summary of the section contents</label>
                                            <input className="form-control" id="inline-form-input" type="text"
                                                name="blurb"
                                                value={formValues.blurb}
                                                onChange={handleChange}
                                            />
                                            {formErrors.blurb && (
                                                <div className="invalid-tooltip">{formErrors.blurb}</div>
                                            )}
                                        </div>
                                        <div className="col-sm-12 mt-3">
                                            <h6 className="mb-2">Parent Section</h6>
                                            <label className="form-label">Within what section is this section filed under?</label>
                                            <div className="d-flex">
                                                <p className="mb-0">{showData ? showData : showByTitle}&nbsp;&nbsp;<NavLink><span type="button" data-toggle="modal" data-target="#addSection">change</span></NavLink></p>
                                            </div>
                                        </div>
                                        <div className="col-sm-12 mt-3">
                                            <h6 className="mb-2">Publication Level</h6>
                                            <label className="form-label">At what location level is this section published?</label>
                                            <div className="d-flex">
                                                <p className="mb-0">{mainNavLinkText}&nbsp;&nbsp;<NavLink><span type="button" data-toggle="modal" data-target="#addLocation">change</span></NavLink></p>
                                            </div>
                                        </div>
                                        <div className="col-sm-12 mt-3" onClick={(e) => checkSecurity(e)}>
                                       
                                        <h6 className="mb-2">Security</h6>
                                        <label className="form-label">Secure this section for staff only</label>
                                        <div className="d-flex">
                                            <div className="form-check">
                                                <input className="form-check-input" id="form-check-1" type="radio"
                                                    name="security"
                                                    value="no"
                                                />
                                                <label className="form-check-label" id="radio-level1">No</label>
                                            </div>
                                            <div className="form-check ms-3">
                                                <input className="form-check-input" id="form-check-2" type="radio"
                                                    name="security"
                                                    value="yes"

                                                />
                                                <label className="form-check-label" id="radio-level2">Yes, staff only.</label>
                                            </div>
                                        </div>
                                        </div>
                                    </div>
                                    <div className="col-md-12 mt-3">
                                            <button className="btn btn-secondary pull-right ms-3 mt-2" type="button" onClick={handleBack}>Cancel & Go Back</button>
                                    <button className="btn btn-primary pull-right mt-2" type="button" onClick={handleSubmit}>Save New Section</button>
                                    </div>
                                    </div>
                                </div>

                               
                            </div>
                        </div>
                </div>
            </main>

            <div className="modal in" role="dialog" id="addSection">
                <div className="modal-dialog modal-dialog-centered modal-dialog-scrollable" role="document">
                    <div className="modal-content">
                        <div className="modal-header border-0 pb-0">
                         {/* <h4 className="modal-title"></h4> */}
                        <button className="btn-close" type="button" data-dismiss="modal" aria-label="Close"></button>
                        </div>
                        <div className="modal-body fs-sm pt-0">
                            <div className="content-overlay">
                                <div className="bg-light">
                                            <h6>Choose a section from the tree.</h6>
                                            <div>
                                                {documentGet.data && documentGet.data.length > 0 ? (
                                                    <NestedArray items={documentGet.data} handleDataClick={handleDataClick} />
                                                ) : null}
                                            </div>
                                    </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div className="modal in" role="dialog" id="addLocation">
                <div className="modal-dialog modal-dialog-centered modal-dialog-scrollable" role="document">
                    <div className="modal-content">
                        <div className="modal-header border-0 pb-0">
                            {/* <h4 className="modal-title"></h4> */}
                            <button id="closeLocation" className="btn-close" type="button" data-dismiss="modal" aria-label="Close"></button>
                        </div>
                        <div className="modal-body fs-sm pt-0">
                            <div className="content-overlay">
                                <div className="bg-light">
                                            <p className="mb-2">Choose a location. </p>
                                            {/* <button type="button" className="btn btn-default btn btn-primary btn-sm mt-0 pull-right" id="closeLocation" data-dismiss="modal">X</button> */}
                                            <p>Choose the location from the tree.</p>
                                            <div className="pull-right mt-4">
                                                <NavLink className="btn btn-sm btn-primary">
                                                    <span onClick={(e) => handleMainNavLinkClick(e, "Corporate")}>Corporate</span>
                                                </NavLink>
                                             </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
            </div>
                </div>
        








    )
}
export default AddSection;



function NestedArray({ items, handleDataClick }) {
    return (
        <ul>
            {items.map((item) => (
                <li key={item._id}>
                    <div className="mb-4 ms-2" onClick={() => handleDataClick(item.title, item._id)}>
                        <NavLink>
                            <p className="mt-4">{item.title}</p>
                        </NavLink>
                    </div>

                    {item.children && item.children.length > 0 && (
                        <NestedArray items={item.children} handleDataClick={handleDataClick} />
                    )}

                    {/* {item.content && item.content.length > 0 && (
                        <ul>
                            {item.content.map((contentItem) => (
                                <li key={contentItem._id}>
                                    <div className="mb-4 ms-2" onClick={() => handleDataClick(contentItem.title, contentItem._id)}>
                                        <NavLink>
                                            <p className="mt-4 ms-5">{contentItem.title}</p>
                                        </NavLink>
                                    </div>
                                </li>
                            ))}
                        </ul>
                    )} */}
                </li>
            ))}
        </ul>
    );
}
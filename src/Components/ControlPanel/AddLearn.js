import React, { useState, useEffect } from "react";
// import avtar from "./Components/img/avtar.jpg";
import _ from "lodash";
import user_service from "../service/user_service";
import jwt from "jwt-decode";
import Loader from "../../Pages/Loader/Loader.js";
import Toaster from "../../Pages/Toaster/Toaster.js";
import { NavLink, useNavigate, useParams } from "react-router-dom";
import swal from "sweetalert2";
import "sweetalert2/dist/sweetalert2.css";
import youtubeicon from "../../Components/img/Youtube Icon.png";
import uploadvideoicon from "../../Components/img/VideoUpload-dark.png";
import uploadingicon from "../../Components/img/uploadiiiing.gif";
import $, { each } from "jquery";

import axios from "axios";
import { FormGroup, FormControl, FormLabel } from "react-bootstrap";

function AddLearn() {
  const initialValues = {
    // title: "", shortdescription: "", description: "", playlist: "", thumbnailimg: "", is_active: "1"
    title: "",
    shortdescription: "",
    description: "",
    playlist: "",
    videoCategory: "",
    thumbnail_image: "",
    is_active: "1",
    // video_url: video_url,
  };
  const [formValues, setFormValues] = useState(initialValues);
  const [formErrors, setFormErrors] = useState({});
  const [isSubmitClick, setISSubmitClick] = useState(false);
  const [isSubmitClickNew, setISSubmitClickNew] = useState(false);

  const [loader, setLoader] = useState({ isActive: null });
  const { isActive } = loader;
  const [toaster, setToaster] = useState({
    types: null,
    isShow: null,
    toasterBody: "",
    message: "",
  });
  const { types, isShow, toasterBody, message } = toaster;
  const url = window.location.pathname.split("/").pop();
  const [learnbyAgentNew, setLearnbyAgentNew] = useState("");

  const [videoCategoryNew, setVideoCategoryNew] = useState("");
  const navigate = useNavigate();
  const params = useParams();

  const [thumbnail_created_array, setThumbnail_created_array] = useState([]);
  // onChange Function start
  const handleChange = (e) => {
    const { name, value } = e.target;
    setFormValues({ ...formValues, [name]: value });
  };
  // onChange Function end

  const [learnbyAgent, setLearnbyAgent] = useState([]);

  const handleFileUpload = async (e, id) => {
    const selectedFile = e.target.files[0];
    //setFile(selectedFile);

    if (selectedFile) {
      var fileExtension = selectedFile.name.split(".").pop();
      if (fileExtension != "mp4") {
        // swal("", "Video File should be in mp4 format.", "error");
        // swalpopup("Video File should be in mp4 format.", 'error')
        alert("Video File should be in mp4 format.", "error");
        return;
      }
      setUploading(true);
      const formData = new FormData();
      formData.append("file", selectedFile);
      const config = {
        headers: {
          Accept: "application/json",
          "Content-Type": "multipart/form-data",
          Authorization: `Bearer ${localStorage.getItem("auth")}`,
        },
      };
      try {
        // setLoader({ isActive: true })
        const uploadResponse = await axios.post(
          "https://api.brokeragentbase.com/upload",
          formData,
          config
        );
        const uploadedFileData = uploadResponse.data;

        console.log(uploadedFileData);

        setExclusive_video_url(uploadedFileData);
        // if (e.target.name == "exclusive_video_url"){
        console.log("1");
        videoPreview(e.target.value);
        //}
        setUploading(false);
        setBtn_exclusive_upload_video(true);
        setStepon(true);
        // this.setState({
        //     // key: key,
        //     url: url,
        //     // challengevideo_key: key,
        //    usive_link: url,
        //     uploading: false,
        //     btn_exclusive_upload_video: true,
        //     stepon: true,
        // })

        // setLoader({ isActive: false })
        // setData(uploadedFileData);

        // const userData = {

        //     agentId: jwt(localStorage.getItem('auth')).id,
        //     marque_image: uploadedFileData,
        // };

        // console.log(userData);
        // try {
        //     const response = await user_service.bannerUpdate(itemId, userData);
        //     if (response) {
        //         console.log(response);
        //         const BannerList = async () => {
        //             await user_service.bannerGet().then((response) => {
        //                 setLoader({ isActive: false })
        //                 if (response) {
        //                     setGetBanner(response.data)
        //                 }
        //             });
        //         }
        //         BannerList()

        //         document.getElementById('closeModal').click();
        //     }
        // } catch (error) {
        //     console.log('Error occurred while uploading documents:', error);
        // }
      } catch (error) {
        console.error("Error occurred during file upload:", error);
      }
    }
  };

  {
    /* <!-- Form Validation Start--> */
  }

  useEffect(() => {
    if (formValues && isSubmitClick) {
      validate();
    }
  }, [formValues]);

  const validate = () => {
    const values = formValues;
    const errors = {};

    if (!values.title) {
      errors.title = "Title is required";
    }

    if (!values.shortdescription) {
      errors.shortdescription = "Short Description is required";
    }

    if (!values.description) {
      errors.description = "Description is required";
    }

    if (!values.playlist) {
      errors.playlist = "Playlist is required";
    }
    if (!values.videoCategory || values.videoCategory === "") {
      errors.videoCategory = "Category is required";
    }

    // if (!values.thumbnailimg && !imgthumbnailfile) {
    //   errors.thumbnailimg = "Thumbnail image is required";
    // }

    if (params.id) {
    } else {
      // If params.id is not present
      if (!values.thumbnailimg && !imgthumbnailfile) {
        errors.thumbnailimg = "Thumbnail image is required";
      }
    }
    setFormErrors(errors);
    return errors;
  };
  {
    /* <!-- Form Validation End--> */
  }

  const addVideocategoryGetdata = async () => {
    const type = formValues.playlist ?? "";
    await user_service.addVideocategoryGet(type).then((response) => {
      if (response) {
        setLoader({ isActive: false });
        console.log(response);
        setLearnbyAgentNew(response.data);
        // console.log(response.data);
        // console.log("learnbyAgent");
      }
    });
  };

  useEffect(() => {
    setLoader({ isActive: true });
    addVideocategoryGetdata();
  }, [formValues.playlist]);

  useEffect(() => {
    window.scrollTo(0, 0);
    setLoader({ isActive: true });
    //  addVideocategoryGetdata();
  }, [url]);

  const handleSubmit = async (e) => {
    if (params.id) {
      e.preventDefault();
      setISSubmitClick(true);
      let checkValue = validate();
      if (_.isEmpty(checkValue)) {
        setISSubmitClickNew(true);

        let video_url = "";
        if (videotype == "embeded") {
          if (exclusive_video_url) {
            var regExp =
              /^.*(youtu\.be\/|v\/|u\/\w\/|embed\/|watch\?v=|\&v=)([^#\&\?]*).*/;
            //console.log(exclusive_video_url)

            if (exclusive_video_url.includes("<iframe")) {
              // console.log('iframe');
              video_url = exclusive_video_url;
            } else if (exclusive_video_url.match(regExp)) {
              var match = exclusive_video_url.match(regExp);
              if (match && match[2].length == 11) {
                var url1 = match[2];
                video_url = `<iframe width="560" height="315" src="https://www.youtube.com/embed/${url1}?rel=0" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>`;
              }
              // this.setState({ challenge_link: `<iframe width="560" height="315" src="https://www.youtube.com/embed/${url1}?rel=0" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>` })
            } else {
              // swalpopup("Please paste the code into the embed video input box.", 'error')
              alert("Please paste the code into the embed video input box.");
              return;
            }
          }
        } else {
          video_url = exclusive_video_url;
        }

        const userData = {
          agentId: jwt(localStorage.getItem("auth")).id,
          title: formValues.title,
          shortdescription: formValues.shortdescription,
          description: formValues.description,
          playlist: formValues.playlist,
          videoCategory: formValues.videoCategory,
          thumbnail_image: imgthumbnailfile,
          videotype: videotype,
          is_active: formValues.is_active,
          video_url: video_url,
        };
        console.log(userData);

        //setLoader({ isActive: true });
        await user_service
          .addLearnUpdate(params.id, userData)
          .then((response) => {
            if (response) {
              //setFormValues(response.data);
              setLoader({ isActive: false });
              setToaster({
                type: "Media Updated Successfully",
                isShow: true,
                toasterBody: response.data.message,
                message: "Media Updated Successfully",
              });
              setTimeout(() => {
                if (formValues.playlist === "workspaceOverview") {
                  navigate(
                    `/videoWorkspaceOverview/${formValues.playlist}/${formValues.videoCategory}`
                  );
                } else if (formValues.playlist === "videoTraining") {
                  navigate(
                    `/videocategory/${formValues.playlist}/${formValues.videoCategory}`
                  );
                } else {
                  navigate(
                    `/videocategory/${formValues.playlist}/${formValues.videoCategory}`
                  );
                }
                setToaster((prevToaster) => ({
                  ...prevToaster,
                  isShow: false,
                }));
              }, 1000);
              setISSubmitClick(false);
              setISSubmitClickNew(false);
            } else {
              setLoader({ isActive: false });
              setToaster({
                types: "error",
                isShow: true,
                toasterBody: response.data.message,
                message: "Error",
              });
            }
          });
      } else {
        console.log("validate");
      }
    } else {
      e.preventDefault();
      setISSubmitClick(true);
      let checkValue = validate();
      if (_.isEmpty(checkValue)) {
        setISSubmitClickNew(true);

        let video_url = "";
        if (videotype == "embeded") {
          if (exclusive_video_url) {
            var regExp =
              /^.*(youtu\.be\/|v\/|u\/\w\/|embed\/|watch\?v=|\&v=)([^#\&\?]*).*/;
            //console.log(exclusive_video_url)

            if (exclusive_video_url.includes("<iframe")) {
              // console.log('iframe');
              video_url = exclusive_video_url;
            } else if (exclusive_video_url.match(regExp)) {
              var match = exclusive_video_url.match(regExp);
              if (match && match[2].length == 11) {
                var url1 = match[2];
                video_url = `<iframe width="560" height="315" src="https://www.youtube.com/embed/${url1}?rel=0" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>`;
              }
              // this.setState({ challenge_link: `<iframe width="560" height="315" src="https://www.youtube.com/embed/${url1}?rel=0" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>` })
            } else {
              // swalpopup("Please paste the code into the embed video input box.", 'error')
              alert("Please paste the code into the embed video input box.");
              return;
            }
          }
        } else {
          video_url = exclusive_video_url;
        }

        const userData = {
          agentId: jwt(localStorage.getItem("auth")).id,
          title: formValues.title,
          shortdescription: formValues.shortdescription,
          description: formValues.description,
          playlist: formValues.playlist,
          videoCategory: formValues.videoCategory,
          thumbnail_image: imgthumbnailfile,
          videotype: videotype,
          is_active: formValues.is_active,
          video_url: video_url,
        };
        console.log(userData);

        //setLoader({ isActive: true });
        await user_service.addLearn(userData).then((response) => {
          if (response) {
            //setFormValues(response.data);
            setLoader({ isActive: false });
            setToaster({
              type: "Media Added Successfully",
              isShow: true,
              toasterBody: response.data.message,
              message: "Media Added Successfully",
            });
            setTimeout(() => {
              if (formValues.playlist === "workspaceOverview") {
                navigate(
                  `/videoWorkspaceOverview/${formValues.playlist}/${formValues.videoCategory}`
                );
              } else if (formValues.playlist === "videoTraining") {
                navigate(
                  `/videocategory/${formValues.playlist}/${formValues.videoCategory}`
                );
              } else {
                navigate(
                  `/videocategory/${formValues.playlist}/${formValues.videoCategory}`
                );
              }
              setToaster((prevToaster) => ({ ...prevToaster, isShow: false }));
            }, 1000);
            setISSubmitClick(false);
            setISSubmitClickNew(false);
          } else {
            setLoader({ isActive: false });
            setToaster({
              types: "error",
              isShow: true,
              toasterBody: response.data.message,
              message: "Error",
            });
          }
        });
      } else {
        console.log("validate");
      }
    }
  };
  {
    /* <!-- Api call Form onSubmit End--> */
  }

  useEffect(() => {
    window.scrollTo(0, 0);

    const videoIdGetId = async () => {
      try {
        const response = await user_service.learnIdvideo(params.id);
        setLoader({ isActive: false });
        if (response) {
          setFormValues(response.data);
        }
      } catch (error) {
        console.error("Error fetching data:", error);
      }
    };
    videoIdGetId();
  }, [params.id]);

  const handleRemove = async () => {
    setLoader({ isActive: true });
    await user_service.bannerDelete(params.id).then((response) => {
      if (response) {
        const BannerList = async () => {
          await user_service.bannerGet().then((response) => {
            setLoader({ isActive: false });
            if (response) {
              setFormValues(response.data);
              setToaster({
                type: "Banner Delete Successfully",
                isShow: true,
                toasterBody: response.data.message,
                message: "Banner Delete Successfully",
              });
              setTimeout(() => {
                setToaster((prevToaster) => ({
                  ...prevToaster,
                  isShow: false,
                }));
                navigate("/control-panel/banners");
              }, 1000);
            }
          });
        };
        BannerList();
      }
    });
  };

  const handleRemoveImage = async (marque) => {
    console.log(marque);

    if (marque === "marque_image") {
      console.log(marque);

      const userData = {
        agentId: jwt(localStorage.getItem("auth")).id,
        marque_image: "",
      };

      console.log(userData);

      try {
        setLoader({ isActive: true });
        const response = await user_service.bannerUpdate(params.id, userData);
        if (response) {
          console.log(response);
          setLoader({ isActive: false });
          setToaster({
            type: "Marquee_image Delete Successfully",
            isShow: true,
            toasterBody: response.data.message,
            message: "Marquee_image Delete Successfully",
          });
          setTimeout(() => {
            setToaster((prevToaster) => ({ ...prevToaster, isShow: false }));
            navigate("/control-panel/banners");
          }, 1000);
          const BannerList = async () => {
            await user_service.bannerGet().then((response) => {
              setLoader({ isActive: false });
              if (response) {
                setFormValues(response.data);
              }
            });
          };
          BannerList();
        }
      } catch (error) {
        console.log("Error occurred while uploading documents:", error);
      }
    }

    if (marque === "side_image") {
      const userData = {
        agentId: jwt(localStorage.getItem("auth")).id,
        side_image: "",
      };

      try {
        setLoader({ isActive: true });
        const response = await user_service.bannerUpdate(params.id, userData);
        if (response) {
          console.log(response);
          setLoader({ isActive: false });
          setToaster({
            type: "Side_image Delete Successfully",
            isShow: true,
            toasterBody: response.data.message,
            message: "Side_image Delete Successfully",
          });
          setTimeout(() => {
            setToaster((prevToaster) => ({ ...prevToaster, isShow: false }));
            navigate("/control-panel/banners");
          }, 1000);
          const BannerList = async () => {
            await user_service.bannerGet().then((response) => {
              setLoader({ isActive: false });
              if (response) {
                setFormValues(response.data);
              }
            });
          };
          BannerList();

          document.getElementById("closeModal").click();
        }
      } catch (error) {
        console.log("Error occurred while uploading documents:", error);
      }
    }
  };

  const [btn_exclusive_upload_video, setBtn_exclusive_upload_video] =
    useState(false);
  const [exclusive_video_url, setExclusive_video_url] = useState("");
  const [uploading, setUploading] = useState(false);
  const [thisexclusive_type, setThisexclusive_type] = useState(1);
  const [exclusive_link, setExclusive_link] = useState(1);
  const [initial_state, setInitial_state] = useState(1);
  const [stepon, setStepon] = useState(1);
  const [btn_challenge_link, setBtn_challenge_link] = useState(1);
  const [videotype, setVideotype] = useState("custom");
  const [activeTab, setActiveTab] = useState("custom");

  const changeresetvideo = () => {
    setThisexclusive_type(1);
    setExclusive_video_url("");
    setExclusive_link(1);
    setUploading(false);
    setBtn_exclusive_upload_video(false);
    setInitial_state(1);
    setStepon(1);
  };

  const validateembededcode = (e) => {
    // console.log(e.target.value);
    // this.setState({ [e.target.name]: e.target.value });
    setExclusive_video_url(e.target.value);
    if (e.target.name == "exclusive_video_url") {
      console.log("1");
      videoPreview(e.target.value);
    }
    e.preventDefault();
  };

  const videoPreview = (framehtml) => {
    // var replacescript = framehtml.replace('<script async src="https://www.tiktok.com/embed.js"></script>', '');
    // var replacecss = replacescript.replace('style="max-width: 605px;min-width: 325px;"', '');
    //console.log("aaaaaaaaa")
    if (framehtml != "") {
      //console.log("qweqwe")
      if (/<iframe/.test(framehtml)) {
        console.log("yes 1");
        var a = framehtml.match(/<iframe/g).length;
        if (a == 1) {
          if (/<\/iframe>/.test(framehtml)) {
            if (/src="http/.test(framehtml)) {
              // this.setState({
              //     btn_challenge_link: true,
              // })
              setBtn_challenge_link(true);
            } else {
              swal("Oops", "Iframe Link is incorrect", "error");
              // this.setState({ exclusive_video_url: '' })
              setExclusive_video_url("");
            }
          } else {
            swal("Oops", "Iframe Link is incorrect", "error");
            // this.setState({ exclusive_video_url: '' })
            setExclusive_video_url("");
          }
        } else {
          swal("Oops", "You have enetered duplicate Iframe", "error");
          // this.setState({ challenge_audio_link: '' })
          setExclusive_video_url("");
        }
      } else {
        console.log("yes 2");
        var regExp =
          /^.*(youtu\.be\/|v\/|u\/\w\/|embed\/|watch\?v=|\&v=)([^#\&\?]*).*/;
        var match = framehtml.match(regExp);
        if (match && match[2].length == 11) {
          var url1 = match[2];
          // this.setState({ exclusive_video_url: '' })
          setExclusive_video_url("");
          setBtn_challenge_link(true);
          // this.setState({
          //     btn_challenge_link: true,
          // })
          console.log("here");
          // this.setState({ exclusive_video_url: `<iframe width="560" height="315" src="https://www.youtube.com/embed/${url1}?rel=0" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>` })
          setExclusive_video_url(
            `<iframe width="500" height="315" src="https://www.youtube.com/embed/${url1}?rel=0" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>`
          );
        } else {
          swal("Oops", "Link is incorrect", "error");
          // this.setState({ challenge_audio_link: '' })
          setExclusive_video_url("");
        }
      }
      console.log("qqqqqqqqq");
    }
  };

  const onsetvideotype = async (videotype) => {
    // Your async code here
    console.log(`Video type selected: ${videotype}`);
    setVideotype(videotype);
    // Perform any other actions related to the selected video type
  };

  const [imgthumbnailfile, setImgthumbnailfile] = useState("");
  const select_thumbnail = (e, thumb_image, serial) => {
    $(".thumb").removeClass("selected");
    $(".thumbnail_" + serial).addClass("selected");
    // this.setState({
    //     imgthumbnailfile: thumb_image,
    // })
    setImgthumbnailfile(thumb_image);
  };

  const [acceptedFileTypes, setAcceptedFileTypes] = useState([]);

  const handleFileUploadSide = async (e, id) => {
    const selectedFile = e.target.files[0];
    // setFile(selectedFile);

    if (selectedFile) {
      //setFileExtension(selectedFile.name.split('.').pop());

      const formData = new FormData();
      formData.append("file", selectedFile);
      const config = {
        headers: {
          Accept: "application/json",
          "Content-Type": "multipart/form-data",
          Authorization: `Bearer ${localStorage.getItem("auth")}`,
        },
      };
      try {
        setLoader({ isActive: true });
        const uploadResponse = await axios.post(
          "https://api.brokeragentbase.com/upload",
          formData,
          config
        );
        const uploadedFileData = uploadResponse.data;
        setLoader({ isActive: false });
        setImgthumbnailfile(uploadedFileData);

        // const userData = {

        //     agentId: jwt(localStorage.getItem('auth')).id,
        //     side_image: uploadedFileData
        // };

        // console.log(userData);
        // try {
        //     const response = await user_service.bannerUpdate(itemId, userData);
        //     if (response) {
        //         console.log(response);
        //         const BannerList = async () => {
        //             await user_service.bannerGet().then((response) => {
        //                 setLoader({ isActive: false })
        //                 if (response) {
        //                     setGetBanner(response.data)
        //                 }
        //             });
        //         }
        //         BannerList()

        //         document.getElementById('closeModalSide').click();
        //     }
        // } catch (error) {
        //     console.log('Error occurred while uploading documents:', error);
        // }
      } catch (error) {
        console.error("Error occurred during file upload:", error);
      }
    }
  };

  let i = 1;
  let thumb_i = 1;
  const handleVideoClick = (id) => {
    console.log(id);
  };

  return (
    <div className="bg-secondary float-left w-100 pt-4 mb-4">
      <Loader isActive={isActive} />
      {isShow && (
        <Toaster
          types={types}
          isShow={isShow}
          toasterBody={toasterBody}
          message={message}
        />
      )}
      <main className="page-wrapper profile_page_wrap">
        <div className="content-overlay">
          <div className="d-flex align-items-center justify-content-start mb-4">
            <h3 className="mb-0 text-white">Learn Media</h3>
          </div>
          {params.id ? (
            <div className="bg-light rounded-3 border p-3 mb-3">
              <>
                <h5>Media Details</h5>
                <div className="col-md-12">
                  <label className="col-form-label">Title *</label>
                  <input
                    className="form-control"
                    id="inline-form-input"
                    type="text"
                    name="title"
                    placeholder="Add a title"
                    value={formValues.title}
                    onChange={handleChange}
                    style={{
                      border: formErrors?.title ? "1px solid red" : "",
                    }}
                  />
                  {formErrors.title && (
                    <div className="invalid-tooltip">{formErrors.title}</div>
                  )}
                </div>

                <div className="col-md-12 mt-3">
                  <label className="col-form-label">Short Description</label>
                  <input
                    className="form-control"
                    id="inline-form-input"
                    type="text"
                    name="shortdescription"
                    placeholder="Add a short-description"
                    value={
                      formValues.shortdescription
                        ? formValues.shortdescription
                        : ""
                    }
                    onChange={handleChange}
                    style={{
                      border: formErrors?.shortdescription
                        ? "1px solid red"
                        : "",
                    }}
                  />
                  {formErrors.shortdescription && (
                    <div className="invalid-tooltip">
                      {formErrors.shortdescription}
                    </div>
                  )}
                </div>

                <div className="col-md-12 mt-3">
                  <label className="col-form-label" for="pr-country">
                    Description
                  </label>
                  <input
                    className="form-control"
                    id="inline-form-input"
                    type="text"
                    name="description"
                    placeholder="Add a description"
                    value={formValues.description}
                    onChange={handleChange}
                    style={{
                      border: formErrors?.description ? "1px solid red" : "",
                    }}
                  />
                  {formErrors.description && (
                    <div className="invalid-tooltip">
                      {formErrors.description}
                    </div>
                  )}
                </div>

                {/* <select
                    className="form-select mb-2"
                    name="represent"
                    onChange={(event) => setRepresent(event.target.value)}
                    value={represent}
                  >
                    <option value="" disabled="">
                      Choose Category
                    </option>
                    <option value="buyer">Buyer</option>
                    <option value="seller">Seller</option>
                    <option value="both">Buyer & Seller</option>
                    <option value="referral">Referral</option>
                  </select> */}

                <div className="col-md-12 mt-3">
                  <label className="col-form-label" for="pr-country">
                    Video Resource
                  </label>

                  <select
                    className="form-select"
                    id="pr-country"
                    name="playlist"
                    value={formValues.playlist}
                    onChange={handleChange}
                    style={{
                      border: formErrors?.playlist ? "1px solid red" : "",
                    }}
                  >
                    <option value="" disabled="">
                      Choose Playlist
                    </option>
                    <option value="workspaceOverview">
                      Workspace Overview
                    </option>
                    {/* <option value="workspaceRecaps">
                          Workspace Recaps
                        </option> */}
                    {/* <option value="rEALTORNews">REALTOR® News</option> */}
                    <option value="videoTraining">Video Training</option>
                  </select>
                  {formErrors.playlist && (
                    <div className="invalid-tooltip">{formErrors.playlist}</div>
                  )}
                </div>

                <div className="col-md-12 mt-3">
                  <label className="col-form-label" for="pr-country">
                    Resource Category
                  </label>
                  <select
                    className="form-select"
                    id="pr-country"
                    name="videoCategory"
                    value={formValues.videoCategory}
                    onChange={handleChange}
                    style={{
                      border: formErrors?.videoCategory ? "1px solid red" : "",
                    }}
                  >
                    {/* {formValues.playlist === "videoTraining" ? ( */}
                    <option value="" disabled="">
                      Choose Playlist
                    </option>
                    {learnbyAgentNew.data && learnbyAgentNew.data.length > 0 ? (
                      learnbyAgentNew.data.map((item, index) => (
                        <>
                          <option value={item._id}>{item.title}</option>
                        </>
                      ))
                    ) : (
                      <option value="" disabled="">
                        Choose Category
                      </option>
                    )}
                    {formErrors.videoCategory && (
                      <div className="invalid-tooltip">
                        {formErrors.videoCategory}
                      </div>
                    )}
                  </select>
                </div>

                <div className="col-md-12 mt-3">
                  <label className="col-form-label" for="pr-country">
                    Thumbnail Image
                  </label>
                  <input
                    className="form-control"
                    id="inline-form-input"
                    type="file"
                    accept={acceptedFileTypes
                      .map((type) => `.${type}`)
                      .join(",")}
                    name="thumbnailimg"
                    onChange={handleFileUploadSide}
                    value={formValues.thumbnailimg}
                  />

                  {formErrors.thumbnailimg && !imgthumbnailfile && (
                    <div className="invalid-tooltip">
                      {formErrors.thumbnailimg}
                    </div>
                  )}

                  {imgthumbnailfile ? (
                    <img className="mt-3" src={imgthumbnailfile} />
                  ) : (
                    <img className="mt-3" src={formValues.thumbnail_image} />
                  )}

                  {thumbnail_created_array
                    ? thumbnail_created_array.length > 0
                      ? thumbnail_created_array.map((post) => {
                          let thumb_ii = thumb_i++;
                          return (
                            <div className="col-md-3">
                              <div className="NotSetup  text-center">
                                <img
                                  className={`thumb thumbnail_${thumb_ii} ml-2`}
                                  // onClick={(e) => this.select_thumbnail(e, post, thumb_ii)}
                                  src={post}
                                />
                              </div>
                            </div>
                          );
                        })
                      : ""
                    : ""}
                </div>

                <div className="col-md-12 mt-3">
                  <label className="col-form-label" for="pr-country">
                    Active Status
                  </label>
                  <select
                    className="form-select"
                    id="pr-country"
                    name="is_active"
                    value={formValues.is_active}
                    onChange={handleChange}
                  >
                    <option value="1">Active</option>
                    <option value="0">Hidden</option>
                  </select>
                  {formErrors.is_active && (
                    <div className="invalid-tooltip">
                      {formErrors.is_active}
                    </div>
                  )}
                </div>
              </>
              <div className="pull-right mt-4">
                <NavLink
                  className="btn btn-secondary pull-right ms-3"
                  type="button"
                  to="/learn"
                >
                  Cancel
                </NavLink>

               
                  <button
                    className="btn btn-primary pull-right"
                    type="button"
                    onClick={handleSubmit}
                    disabled={isSubmitClickNew}
                  >
                    {isSubmitClickNew ? "Please wait..." : "Update"}{" "}
                  </button>
               
              </div>
            </div>
          ) : (
            <div className="">
              <div className="bg-light rounded-3 border p-3 mb-3">
                <div className="">
                  <div className="tab-content mt-4">
                    <div className="d-flex align-items-center justify-content-center mt-4">
                      <ul
                        className="single_tour nav nav-tabs mb-0"
                        role="tablist"
                      >
                        <li className="nav-item me-sm-3 mb-3">
                          <a
                            className="nav-link"
                            href="#tab100"
                            data-bs-toggle="tab"
                            role="tab"
                            aria-controls="reviews-about-you"
                            aria-selected="true"
                            onClick={() => onsetvideotype("custom")}
                          >
                            Upload a video
                          </a>
                        </li>
                        <li className="nav-item me-sm-3 mb-3">
                          <a
                            className="nav-link"
                            href="#tab400"
                            data-bs-toggle="tab"
                            role="tab"
                            aria-controls="reviews-about-you"
                            aria-selected="false"
                            onClick={() => onsetvideotype("embeded")}
                          >
                            Embed a Video
                          </a>
                        </li>
                      </ul>
                    </div>
                    <>
                      {videotype === "custom" ? (
                        <div className="tab-pane active" id="tab100">
                          <div className="publishing-feed float-left w-100">
                            <h5>Upload a Video</h5>
                          </div>
                          <div className="float-left w-100">
                            <div className="price-section sale-percentage">
                              {btn_exclusive_upload_video ? (
                                <div className="mt-0 text-center float-none my-4">
                                  {/* <iframe width="470" height="300" src={exclusive_video_url} title="Video player" frameborder="0" allow="accelerometer;  clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe> */}

                                  <video
                                    className="w-100"
                                    height="250px"
                                    controls
                                  >
                                    <source
                                      src={exclusive_video_url}
                                      type="video/mp4"
                                    />
                                  </video>

                                  <div className="col-md-12">
                                    <div className="text-center float-left w-100 start-chlng-lnk mt-3">
                                      {/* <button className="btn btn-warning btn-width-create-page" onClick={() => changeresetvideo('1')}>Change</button> */}
                                      <button
                                        className="btn btn-primary"
                                        onClick={() => changeresetvideo("1")}
                                      >
                                        Change
                                      </button>
                                    </div>
                                  </div>
                                </div>
                              ) : (
                                <>
                                  <div className="col-md-12 text-center">
                                    {uploading == true ? (
                                      <img
                                        className="img-fluid mb-3 float-none"
                                        width=""
                                        src={uploadingicon}
                                      ></img>
                                    ) : (
                                      <label for="exclusivevideo" className="">
                                        <img
                                          className="img-fluid mb-3 upload"
                                          width="150"
                                          src={uploadvideoicon}
                                        ></img>
                                      </label>
                                    )}
                                    <p className="m-0 upload-files">
                                      Upload Your Video
                                    </p>
                                    <p>
                                      <small className="small-para">
                                        Video File Must be in MP4
                                      </small>
                                    </p>
                                    <div className="select-plan-details registered my-3">
                                      <label for="exclusivevideo" className="">
                                        {/* <a type="button" className="btn btn-warning btn-select-file mb-0" disabled={uploading}>{uploading ? 'Please wait...' : 'Select File'}</a> */}
                                        <a
                                          type="button"
                                          className="btn btn-primary mb-0"
                                          disabled={uploading}
                                        >
                                          {uploading
                                            ? "Please wait..."
                                            : "Select File"}
                                        </a>
                                      </label>
                                      <input
                                        className="hide-input d-none"
                                        type="file"
                                        id="exclusivevideo"
                                        name="exclusivevideo"
                                        value=""
                                        accept="video/*"
                                        onChange={(e) => handleFileUpload(e)}
                                      />
                                    </div>
                                  </div>
                                </>
                              )}
                              {/* {this.eventsValidator.message("Video", this.state.exclusive_video_url, "required")} */}
                            </div>
                          </div>
                        </div>
                      ) : (
                        ""
                      )}
                      <div className="tab-pane mb-4" id="tab400">
                        <div className="publishing-feed float-left w-100">
                          <h5>Embed a Video</h5>
                        </div>

                        <div className="text-center">
                          {exclusive_video_url ? (
                            <>
                              {/* {renderHTML(exclusive_video_url)} */}
                              {/* {console.log(exclusive_video_url)}
                                                                    {console.log("exclusive_video_url")} */}
                              <div
                                id=""
                                dangerouslySetInnerHTML={{
                                  __html: exclusive_video_url,
                                }}
                              />
                            </>
                          ) : (
                            <>
                              <label for="exclusivevideo" className="">
                                <img
                                  className="img-fluid mb-3 upload"
                                  width="150"
                                  src={youtubeicon}
                                ></img>
                              </label>

                              <p className="m-0 upload-files">
                                Embed Your Video{" "}
                                <i
                                  className="fa fa-info-circle ml-1"
                                  aria-hidden="true"
                                ></i>
                                <div className="tooltippp bg-gray-join-icon p-1 ml-1">
                                  <span className="tooltippptext">
                                    <div className="row">
                                      <div className="col-md-12">
                                        <p className="gray-text">
                                          1. Go to the YouTube video you want to
                                          embed. <br />
                                          2. Click Share.
                                          <br />
                                          3. From the list of Share options,
                                          copy the video URL.
                                          <br />
                                          4. Paste the code into the embed video
                                          input box.
                                          <br />
                                        </p>
                                      </div>
                                    </div>
                                  </span>
                                </div>
                              </p>

                              <p>
                                <small className="small-para">
                                  Paste the code into the embed video input box.
                                </small>
                              </p>
                            </>
                          )}
                          <div className="registered mt-3">
                            <div className="select-plan-details price-section sale-percentage">
                              <input
                                className="form-control"
                                type="text"
                                name="exclusive_video_url"
                                placeholder="Paste the embed code here."
                                value={exclusive_video_url}
                                onChange={validateembededcode}
                              />
                            </div>
                            {/* {this.eventsValidator.message('Exclusive Video', exclusive_video_url, 'required')} */}
                          </div>
                        </div>
                      </div>
                    </>
                  </div>

                  {exclusive_video_url ? (
                    <>
                      <h5>Media Details</h5>
                      <div className="col-md-12">
                        <label className="col-form-label">Title *</label>
                        <input
                          className="form-control"
                          id="inline-form-input"
                          type="text"
                          name="title"
                          placeholder="Add a title"
                          value={formValues.title}
                          onChange={handleChange}
                          style={{
                            border: formErrors?.title ? "1px solid red" : "",
                          }}
                        />
                        {formErrors.title && (
                          <div className="invalid-tooltip">
                            {formErrors.title}
                          </div>
                        )}
                      </div>

                      <div className="col-md-12 mt-3">
                        <label className="col-form-label">
                          Short Description
                        </label>
                        <input
                          className="form-control"
                          id="inline-form-input"
                          type="text"
                          name="shortdescription"
                          placeholder="Add a short-description"
                          value={
                            formValues.shortdescription
                              ? formValues.shortdescription
                              : ""
                          }
                          onChange={handleChange}
                          style={{
                            border: formErrors?.shortdescription
                              ? "1px solid red"
                              : "",
                          }}
                        />
                        {formErrors.shortdescription && (
                          <div className="invalid-tooltip">
                            {formErrors.shortdescription}
                          </div>
                        )}
                      </div>

                      <div className="col-md-12 mt-3">
                        <label className="col-form-label" for="pr-country">
                          Description
                        </label>
                        <input
                          className="form-control"
                          id="inline-form-input"
                          type="text"
                          name="description"
                          placeholder="Add a description"
                          value={formValues.description}
                          onChange={handleChange}
                          style={{
                            border: formErrors?.description
                              ? "1px solid red"
                              : "",
                          }}
                        />
                        {formErrors.description && (
                          <div className="invalid-tooltip">
                            {formErrors.description}
                          </div>
                        )}
                      </div>

                      {/* <select
                    className="form-select mb-2"
                    name="represent"
                    onChange={(event) => setRepresent(event.target.value)}
                    value={represent}
                  >
                    <option value="" disabled="">
                      Choose Category
                    </option>
                    <option value="buyer">Buyer</option>
                    <option value="seller">Seller</option>
                    <option value="both">Buyer & Seller</option>
                    <option value="referral">Referral</option>
                  </select> */}

                      <div className="col-md-12 mt-3">
                        <label className="col-form-label" for="pr-country">
                          Video Resource
                        </label>

                        <select
                          className="form-select"
                          id="pr-country"
                          name="playlist"
                          value={formValues.playlist}
                          onChange={handleChange}
                          style={{
                            border: formErrors?.playlist ? "1px solid red" : "",
                          }}
                        >
                          <option value="" disabled="">
                            Choose Playlist
                          </option>
                          <option value="workspaceOverview">
                            Workspace Overview
                          </option>
                          {/* <option value="workspaceRecaps">
                          Workspace Recaps
                        </option> */}
                          {/* <option value="rEALTORNews">REALTOR® News</option> */}
                          <option value="videoTraining">Video Training</option>
                        </select>
                        {formErrors.playlist && (
                          <div className="invalid-tooltip">
                            {formErrors.playlist}
                          </div>
                        )}
                      </div>

                      <div className="col-md-12 mt-3">
                        <label className="col-form-label" for="pr-country">
                          Resource Category
                        </label>
                        <select
                          className="form-select"
                          id="pr-country"
                          name="videoCategory"
                          value={formValues.videoCategory}
                          onChange={handleChange}
                          style={{
                            border: formErrors?.videoCategory
                              ? "1px solid red"
                              : "",
                          }}
                        >
                          {/* {formValues.playlist === "videoTraining" ? ( */}
                          <option value="" disabled="">
                            Choose Playlist
                          </option>
                          {learnbyAgentNew.data &&
                          learnbyAgentNew.data.length > 0 ? (
                            learnbyAgentNew.data.map((item, index) => (
                              <>
                                <option value={item._id}>{item.title}</option>
                              </>
                            ))
                          ) : (
                            <option value="" disabled="">
                              Choose Category
                            </option>
                          )}
                          {formErrors.videoCategory && (
                            <div className="invalid-tooltip">
                              {formErrors.videoCategory}
                            </div>
                          )}
                        </select>
                      </div>

                      <div className="col-md-12 mt-3">
                        <label className="col-form-label" for="pr-country">
                          Thumbnail Image
                        </label>
                        <input
                          className="form-control"
                          id="inline-form-input"
                          type="file"
                          accept={acceptedFileTypes
                            .map((type) => `.${type}`)
                            .join(",")}
                          name="thumbnailimg"
                          onChange={handleFileUploadSide}
                          value={formValues.thumbnailimg}
                        />

                        {formErrors.thumbnailimg && !imgthumbnailfile && (
                          <div className="invalid-tooltip">
                            {formErrors.thumbnailimg}
                          </div>
                        )}

                        {imgthumbnailfile ? (
                          <img className="mt-3" src={imgthumbnailfile} />
                        ) : (
                          <img
                            className="mt-3"
                            src={formValues.thumbnail_image}
                          />
                        )}

                        {thumbnail_created_array
                          ? thumbnail_created_array.length > 0
                            ? thumbnail_created_array.map((post) => {
                                let thumb_ii = thumb_i++;
                                return (
                                  <div className="col-md-3">
                                    <div className="NotSetup  text-center">
                                      <img
                                        className={`thumb thumbnail_${thumb_ii} ml-2`}
                                        // onClick={(e) => this.select_thumbnail(e, post, thumb_ii)}
                                        src={post}
                                      />
                                    </div>
                                  </div>
                                );
                              })
                            : ""
                          : ""}
                      </div>

                      <div className="col-md-12 mt-3">
                        <label className="col-form-label" for="pr-country">
                          Active Status
                        </label>
                        <select
                          className="form-select"
                          id="pr-country"
                          name="is_active"
                          value={formValues.is_active}
                          onChange={handleChange}
                        >
                          <option value="1">Active</option>
                          <option value="0">Hidden</option>
                        </select>
                        {formErrors.is_active && (
                          <div className="invalid-tooltip">
                            {formErrors.is_active}
                          </div>
                        )}
                      </div>
                    </>
                  ) : (
                    ""
                  )}
                </div>
              </div>
              <div className="pull-right mt-3">
                <NavLink
                  className="btn btn-secondary pull-right ms-3"
                  type="button"
                  to="/learn"
                >
                  Cancel
                </NavLink>

                {exclusive_video_url ? (
                  <button
                    className="btn btn-primary pull-right"
                    type="button"
                    onClick={handleSubmit}
                    disabled={isSubmitClickNew}
                  >
                    {isSubmitClickNew ? "Please wait..." : "Add Video"}{" "}
                  </button>
                ) : (
                  ""
                )}
              </div>
            </div>
          )}
        </div>
      </main>
    </div>
  );
}
export default AddLearn;

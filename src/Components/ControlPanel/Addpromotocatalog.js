import React, { useState, useEffect } from "react";
// import avtar from "./Components/img/avtar.jpg";
import _ from "lodash";
import user_service from "../service/user_service";
import jwt from "jwt-decode";
import Loader from "../../Pages/Loader/Loader.js";
import Toaster from "../../Pages/Toaster/Toaster.js";
import { NavLink, useNavigate, useParams } from "react-router-dom";
import { Calendar as BigCalendar, momentLocalizer } from "react-big-calendar";

import moment from "moment-timezone";
import swal from "sweetalert2";
import "sweetalert2/dist/sweetalert2.css";
import $, { each } from "jquery";
import axios from "axios";
const localizer = momentLocalizer(moment);

function Addpromotocatalog() {
  const initialValues = {
    office_id: localStorage.getItem("active_office_id"),
    office_name: localStorage.getItem("active_office"),
    category: "product",
    title: "",
    description: "",
    image: "",
    is_active: "1",
    // video_url: video_url,
  };
  const [formValues, setFormValues] = useState(initialValues);
  const [formErrors, setFormErrors] = useState({});
  const [isSubmitClick, setISSubmitClick] = useState(false);
  const [submitClick, setSubmitClick] = useState(false);
  
  const [loader, setLoader] = useState({ isActive: null });
  const { isActive } = loader;
  const [toaster, setToaster] = useState({
    types: null,
    isShow: null,
    toasterBody: "",
    message: "",
  });
  const { types, isShow, toasterBody, message } = toaster;
  const [checkData, setCheckData] = useState("");
  const [calenderGet, setCalenderGet] = useState([]);
  const navigate = useNavigate();
  const params = useParams();
  const [promoteProduct, setPromoteProduct] = useState("");
  const handleChange = (e) => {
    const { name, value } = e.target;
    setFormValues({ ...formValues, [name]: value });
  };
  // onChange Function end
  const fetchCalendar = async () => {
    setLoader({ isActive: true });
    await user_service.CalenderGet().then((response) => {
      setLoader({ isActive: false });
      if (response) {
        setCalenderGet(response.data);
      }
    });
  };

  useEffect(() => { window.scrollTo(0, 0);
    fetchCalendar();
  }, []);

  const [selectedDate, setSelectedDate] = useState(null);
  const handleSelectSlot = (slotInfo) => {
    if(localStorage.getItem("auth") && jwt(localStorage.getItem("auth")).contactType == "admin" ){
      setSelectedDate(slotInfo);
      window.$('#modal-show-calender').modal('show');
    }
  };
  const handleEventClick = (event) => {
    event.preventDefault();
    const eventId = event.currentTarget.getAttribute('data-eventid');
    navigate(`/event-detail/${eventId}`);
  };

  const [currentDate, setCurrentDate] = useState("");


  useEffect(() => { window.scrollTo(0, 0);
    const interval = setInterval(() => {
      const options = {  dateStyle: "full" };
      setCurrentDate(new Date().toLocaleDateString("en-US", options));
    }, 1000);

    return () => {
      clearInterval(interval);
    };
  }, []);

  const [events, setEvents] = useState([]);

  useEffect(() => { window.scrollTo(0, 0);
    const generatedEvents = generateEvents();
    // console.log(generatedEvents);
    setEvents(generatedEvents);
  }, [calenderGet]);

  const eventColors = {
    meeting: "#FF5733",
    duty: "#3366FF",
  };
  const eventStyleGetter = (event) => {
    const backgroundColor = eventColors[event.type];
    return {
      style: {
        backgroundColor,
      },
    };
  };


  const generateEvents = () => {
    const generatedEvents = [];

    if (calenderGet && calenderGet.data) {
      calenderGet.data.forEach((date) => {
        let startDate;
        let endDate;

        if (date.startDate.includes("/")) {
          startDate = moment.tz(date.startDate, "M/D/YYYY", "America/Denver").toDate();
          endDate = moment.tz(date.endDate, "M/D/YYYY", "America/Denver").toDate();
        } else {
          startDate = moment.tz(date.startDate, "America/Denver").toDate();
          endDate = moment.tz(date.endDate, "America/Denver").toDate();
        }

        const eventId = date._id;

        generatedEvents.push({
          title: date.title,
          start: startDate,
          end: endDate,
          type: "meeting",
          id: eventId,
        });
      });
    }
    return generatedEvents;
  };


  const CalenderPage = () => {
    navigate("/calendar");
  };


  useEffect(() => {
    window.scrollTo(0, 0);
    if (formValues && isSubmitClick) {
      validate();
    }
  }, [formValues]);

  const validate = () => {
    const values = formValues;
    const errors = {};

    if (!values.title) {
      errors.title = "Title is required";
    }

    if (!values.description) {
      errors.description = "Description is required";
    }

    if (!values.category) {
      errors.category = "Category is required";
    }
    if (!imgthumbnailfile) {
      errors.image = "image is required";
    }
    setFormErrors(errors);
    return errors;
  };
  {
    /* <!-- Form Validation End--> */
  }

  const handleSubmit = async (e) => {
    console.log("222222");
    if(params.id){
      console.log(params.id);
      e.preventDefault();
    
        setSubmitClick(true);
        const userData = {
          agentId: jwt(localStorage.getItem("auth")).id,
          title: formValues.title,
          category: formValues.category,
          description: formValues.description,
          office_id: localStorage.getItem("active_office_id"),
          office_name: localStorage.getItem("active_office"),
          image: imgthumbnailfile,
          is_active: formValues.is_active,
        };
  
        // setLoader({ isActive: true })
        console.log(userData);
  
        await user_service.UpdatePromotocatalog(params.id, userData).then((response) => {
          if (response) {
            setFormValues(response.data);
            setLoader({ isActive: false });
            setToaster({
              type: "Catalog Added Successfully",
              isShow: true,
              toasterBody: response.data.message,
              message: "Catalog Added Successfully",
            });
            setSubmitClick(false);
            setTimeout(() => {
              setToaster((prevToaster) => ({ ...prevToaster, isShow: false }));
              navigate(`/admin/promote`);
            }, 2000);
          } else {
            setLoader({ isActive: false });
            setToaster({
              types: "error",
              isShow: true,
              toasterBody: response.data.message,
              message: "Error",
            });
          }
        });
       

    }
    else{
      e.preventDefault();
      setISSubmitClick(true);
      let checkValue = validate();
      if (_.isEmpty(checkValue)) {
        const userData = {
          agentId: jwt(localStorage.getItem("auth")).id,
          title: formValues.title,
          category: formValues.category,
          description: formValues.description,
          office_id: localStorage.getItem("active_office_id"),
          office_name: localStorage.getItem("active_office"),
          image: imgthumbnailfile,
          is_active: formValues.is_active,
        };
        await user_service.Addpromotocatalog(userData).then((response) => {
          if (response) {
            setFormValues(response.data);
            setLoader({ isActive: false });
            setToaster({
              type: "Catalog Added Successfully",
              isShow: true,
              toasterBody: response.data.message,
              message: "Catalog Added Successfully",
            });
            setTimeout(() => {
              setToaster((prevToaster) => ({ ...prevToaster, isShow: false }));
              navigate(`/admin/promote`);
            }, 2000);
          } else {
            setLoader({ isActive: false });
            setToaster({
              types: "error",
              isShow: true,
              toasterBody: response.data.message,
              message: "Error",
            });
          }
        });
      }
    }
  };
  {
    /* <!-- Api call Form onSubmit End--> */
  }

  const [learnbyAgent, setLearnbyAgent] = useState([]);
  const getLearnbyAgent = async () => {
    await user_service
      .getLearnbyAgent(jwt(localStorage.getItem("auth")).id)
      .then((response) => {
        setLoader({ isActive: false });
        if (response) {
          setLearnbyAgent(response.data);
          // console.log(learnbyAgent);
          // console.log("learnbyAgent");
        }
      });
  };
  useEffect(() => {
    window.scrollTo(0, 0);
    setLoader({ isActive: true });

    getLearnbyAgent();
  }, []);

  const handleRemove = async () => {
    setLoader({ isActive: true });
    await user_service.bannerDelete(params.id).then((response) => {
      if (response) {
        const BannerList = async () => {
          await user_service.bannerGet().then((response) => {
            setLoader({ isActive: false });
            if (response) {
              setFormValues(response.data);
              setToaster({
                type: "Banner Delete Successfully",
                isShow: true,
                toasterBody: response.data.message,
                message: "Banner Delete Successfully",
              });
              setTimeout(() => {
                setToaster((prevToaster) => ({
                  ...prevToaster,
                  isShow: false,
                }));
                navigate("/control-panel/banners");
              }, 1000);
            }
          });
        };
        BannerList();
      }
    });
  };


  const handleRemoveImage = async (marque) => {
    console.log(marque);

    if (marque === "marque_image") {
      console.log(marque);

      const userData = {
        agentId: jwt(localStorage.getItem("auth")).id,
        marque_image: "",
      };

      console.log(userData);

      try {
        setLoader({ isActive: true });
        const response = await user_service.bannerUpdate(params.id, userData);
        if (response) {
          console.log(response);
          setLoader({ isActive: false });
          setToaster({
            type: "Marquee_image Delete Successfully",
            isShow: true,
            toasterBody: response.data.message,
            message: "Marquee_image Delete Successfully",
          });
          setTimeout(() => {
            setToaster((prevToaster) => ({ ...prevToaster, isShow: false }));
            navigate("/control-panel/banners");
          }, 1000);
          const BannerList = async () => {
            await user_service.bannerGet().then((response) => {
              setLoader({ isActive: false });
              if (response) {
                setFormValues(response.data);
              }
            });
          };
          BannerList();
        }
      } catch (error) {
        console.log("Error occurred while uploading documents:", error);
      }
    }

    if (marque === "side_image") {
      const userData = {
        agentId: jwt(localStorage.getItem("auth")).id,
        side_image: "",
      };

      try {
        setLoader({ isActive: true });
        const response = await user_service.bannerUpdate(params.id, userData);
        if (response) {
          console.log(response);
          setLoader({ isActive: false });
          setToaster({
            type: "Side_image Delete Successfully",
            isShow: true,
            toasterBody: response.data.message,
            message: "Side_image Delete Successfully",
          });
          setTimeout(() => {
            setToaster((prevToaster) => ({ ...prevToaster, isShow: false }));
            navigate("/control-panel/banners");
          }, 1000);
          const BannerList = async () => {
            await user_service.bannerGet().then((response) => {
              setLoader({ isActive: false });
              if (response) {
                setFormValues(response.data);
              }
            });
          };
          BannerList();

          document.getElementById("closeModal").click();
        }
      } catch (error) {
        console.log("Error occurred while uploading documents:", error);
      }
    }
  };

  const [btn_exclusive_upload_video, setBtn_exclusive_upload_video] =
    useState(false);
  const [exclusive_video_url, setExclusive_video_url] = useState("");
  const [uploading, setUploading] = useState(false);
  const [thisexclusive_type, setThisexclusive_type] = useState(1);
  const [exclusive_link, setExclusive_link] = useState(1);
  const [initial_state, setInitial_state] = useState(1);
  const [stepon, setStepon] = useState(1);
  const [btn_challenge_link, setBtn_challenge_link] = useState(1);
  const [videotype, setVideotype] = useState("custom");

  const changeresetvideo = () => {
    setThisexclusive_type(1);
    setExclusive_video_url("");
    setExclusive_link(1);
    setUploading(false);
    setBtn_exclusive_upload_video(false);
    setInitial_state(1);
    setStepon(1);
  };

 
  const [imgthumbnailfile, setImgthumbnailfile] = useState("");

  const [acceptedFileTypes, setAcceptedFileTypes] = useState([]);

  const handleFileUploadSide = async (e, id) => {
    const selectedFile = e.target.files[0];
    // setFile(selectedFile);

    if (selectedFile) {
      //setFileExtension(selectedFile.name.split('.').pop());

      const formData = new FormData();
      formData.append("file", selectedFile);
      const config = {
        headers: {
          Accept: "application/json",
          "Content-Type": "multipart/form-data",
          Authorization: `Bearer ${localStorage.getItem("auth")}`,
        },
      };
      try {
        setLoader({ isActive: true });
        const uploadResponse = await axios.post(
          "https://api.brokeragentbase.com/upload",
          // "http://localhost:4000/upload",

          formData,
          config
        );
        const uploadedFileData = uploadResponse.data;
        setLoader({ isActive: false });
        setImgthumbnailfile(uploadedFileData);

        // const userData = {

        //     agentId: jwt(localStorage.getItem('auth')).id,
        //     side_image: uploadedFileData
        // };

        // console.log(userData);
        // try {
        //     const response = await user_service.bannerUpdate(itemId, userData);
        //     if (response) {
        //         console.log(response);
        //         const BannerList = async () => {
        //             await user_service.bannerGet().then((response) => {
        //                 setLoader({ isActive: false })
        //                 if (response) {
        //                     setGetBanner(response.data)
        //                 }
        //             });
        //         }
        //         BannerList()

        //         document.getElementById('closeModalSide').click();
        //     }
        // } catch (error) {
        //     console.log('Error occurred while uploading documents:', error);
        // }
      } catch (error) {
        console.error("Error occurred during file upload:", error);
      }
    }
  };

  useEffect(() => {
    window.scrollTo(0, 0);
    if (params.id) {
      const PromoteProductGetIdId = async () => {
        await user_service.promotoProductsGetId(params.id).then((response) => {
          setLoader({ isActive: false });
          if (response) {
            setFormValues(response.data);
          }
        });
      };
      PromoteProductGetIdId();
    }
  }, []);

  return (
    <div className="bg-secondary float-left w-100 pt-4 mb-4">
      <Loader isActive={isActive} />
      {isShow && (
        <Toaster
          types={types}
          isShow={isShow}
          toasterBody={toasterBody}
          message={message}
        />
      )}
      <div className="">
        <main className="page-wrapper homepage_layout">
          <div className="float-left w-100">
            <h3 className="mb-4 text-white">Add a Catalog Item</h3>
          </div>
            <div className="row event-calender">
              <div className="col-md-8">
                <div className="bg-light border rounded-3 p-3">
                <div className="col-md-12">
                  <label className="form-label">
                    Title &nbsp;<b>*</b>
                    <br />
                  </label>
                  <input
                    className="form-control"
                    id="inline-form-input"
                    type="text"
                    name="title"
                    value={formValues.title}
                    onChange={handleChange}
                    style={{
                      border: formErrors?.title ? "1px solid red" : "",
                    }}
                  />
                  {formErrors.title && (
                    <div className="invalid-tooltip">{formErrors.title}</div>
                  )}
                </div>

                <div className="col-md-12 mt-3">
                  <label className="form-label" for="pr-country">
                    Description &nbsp;<b>*</b>
                    <br />
                  </label>
                  <input
                    className="form-control"
                    id="inline-form-input"
                    type="text"
                    name="description"
                    value={formValues.description}
                    onChange={handleChange}
                  />
                  {formErrors.description && (
                    <div className="invalid-tooltip">
                      {formErrors.description}
                    </div>
                  )}
                </div>

                <div className="col-md-12 mt-3">
                  <label className="form-label" for="pr-country">
                    Catalog Category &nbsp;<b>*</b>
                    <br />
                  </label>

                  <select
                    className="form-select"
                    id="pr-country"
                    name="category"
                    value={formValues.category}
                    onChange={handleChange}
                  >
                    {/* <option></option> */}
                    <option value="product">Product</option>
                    <option value="vendor">Vendor</option>
                  </select>
                  {formErrors.category && (
                    <div className="invalid-tooltip">{formErrors.category}</div>
                  )}
                </div>

                <div className="col-md-12 mt-3">
                  <label className="form-label" for="pr-country">
                    Catalog Thumbnail &nbsp;<b>*</b>
                    <br />
                  </label>
                  <input
                    className="form-control"
                    id="inline-form-input"
                    type="file"
                    accept={acceptedFileTypes.map((type) => `.${type}`).join(",")}
                    name="thumbnailimg"
                    onChange={handleFileUploadSide}
                    value={formValues.thumbnailimg}
                  />
                  {formErrors.imgthumbnailfile && (
                    <div className="invalid-tooltip">
                      {formErrors.imgthumbnailfile}
                    </div>
                  )}

                  {console.log(formValues?.image)}

                    {formValues?.image ? (
                    <img className="mt-3" src={formValues?.image} />
                  ) : (
                    ""
                  )}
                  {imgthumbnailfile ? (
                    <img className="d-none" src={imgthumbnailfile} />
                  ) : (
                    ""
                  )}

                  {/* <div className="col-md-3">
                                                  <div className="NotSetup Marquee text-center">

                                                      <button type="button" className="btn btn-primary">
                                                          Not Setup
                                                      </button>


                                                      <p>No side banner.</p>
                                                  </div>
                                              </div> */}
                </div>

                <div className="col-md-12 mt-3 mb-3">
                  <label className="form-label" for="pr-country">
                    Status
                  </label>
                  <select
                    className="form-select"
                    id="pr-country"
                    name="is_active"
                    value={formValues.is_active}
                    onChange={handleChange}
                  >
                    <option value="1">Active</option>
                    <option value="0">Draft</option>
                  </select>
                  {formErrors.is_active && (
                    <div className="invalid-tooltip">{formErrors.is_active}</div>
                  )}
                </div>
              </div>
              <div className="pull-right mt-3">
              <NavLink
              className="btn btn-secondary pull-right ms-3"
              type="button"
              to="/admin/promote">
              Cancel
            </NavLink>
                <button
                className="btn btn-primary pull-right"
                type="button"
                onClick={handleSubmit}
                disabled={submitClick}>
                {submitClick ? "Please wait..." : "Add Catalog Item"}{" "}
              </button>
          
              {/* <button
                className="btn btn-primary pull-right ms-2"
                type="button"
                disabled
              >
                Add Catalog Item
              </button> */}
            </div>
              </div>
            <div className="col-md-4 mt-lg-0 mt-md-0 mt-sm-4 mt-4">
              <div className="border bg-light rounded-3 p-3">
                  <div className="d-flex align-items-center justify-content-between pb-2">
                    <h6 className="mb-0">{currentDate}</h6>
                    <span onClick={CalenderPage}>
                      <i className="fa fa-plus" aria-hidden="true"></i>
                    </span>
                  </div>
                  <hr/>
                  <div className="caledar_view mt-2" style={{ height: "285px" }}>
                    <BigCalendar
                      localizer={localizer}
                      events={events}
                      startAccessor="start"
                      endAccessor="end"
                      onSelectSlot={handleSelectSlot}
                      selectable={true}
                      components={{
                        event: ({ event }) => (
                          <div
                            className="rbc-event-content"
                            title={event.title}
                            onClick={handleEventClick}
                            data-eventid={event.id}
                          >
                            {event.title}
                          </div>
                        ),
                      }}
                      views={{
                        month: true,
                        week: false,
                        day: false,
                        agenda: false,
                      }}
                      // components={{
                      //   toolbar: CustomToolbar,
                      // }}
                      eventPropGetter={eventStyleGetter}
                      style={{ flex: 1 }}
                    />
                  </div>
                  </div>
              </div>
          </div>
        </main>
      </div>
    </div>
  );
}
export default Addpromotocatalog;

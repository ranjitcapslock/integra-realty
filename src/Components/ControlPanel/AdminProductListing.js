import React, { useState, useEffect } from "react";
import { NavLink, useParams, useNavigate } from "react-router-dom";
import user_service from "../service/user_service";
import Loader from "../../Pages/Loader/Loader.js";
import Toaster from "../../Pages/Toaster/Toaster.js";
import PromoteImage from "../img/image.jpg";
import Swal from "sweetalert2";
import "sweetalert2/dist/sweetalert2.css";


const AdminProductListing = () => {
  const [loader, setLoader] = useState({ isActive: null });
  const { isActive } = loader;
  const [toaster, setToaster] = useState({
    types: null,
    isShow: null,
    toasterBody: "",
    message: "",
  });
  const { types, isShow, toasterBody, message } = toaster;
  const params = useParams();

  const [productItem, setProductItem] = useState([]);
  const [productItemData, setProductItemData] = useState("");
  const [products, setProducts] = useState([]);
  const navigate = useNavigate();

  useEffect(() => {
    setLoader({ isActive: true });
    if (params.details) {
      const ProductItem = async () => {
        await user_service
          .productItemsVendorGet(params.details)
          .then((response) => {
            setLoader({ isActive: false });
            if (response) {
              setProductItem(response.data);
            }
          });
      };
      ProductItem();
    } else {
      const ProductItem = async () => {
        await user_service.productItemsGet(params.id).then((response) => {
          setLoader({ isActive: false });
          if (response) {
            setProductItem(response.data);
          }
        });
      };
      ProductItem();
    }
  }, []);

  // useEffect(() => {
  //   window.scrollTo(0, 0);
  //   setLoader({ isActive: true });
  //   const ProductItem = async () => {
  //     await user_service.productItemsGet(params.details).then((response) => {
  //       setLoader({ isActive: false });
  //       if (response) {
  //         setProductItem(response.data);
  //         const newProductdata = response.data.data;
  //         setProductItemData(newProductdata?.[0]?.category || "");
  //       }
  //     });
  //   };

  //   ProductItem();
  // }, []);

  const handleRemoveProduct = async (id) => {
    if (id) {
      const confirmation = await Swal.fire({
        title: "Are you sure?",
        text: "It will be permanently deleted!",
        icon: "warning",
        showCancelButton: true,
        confirmButtonColor: "#3085d6",
        cancelButtonColor: "#d33",
        confirmButtonText: "Yes, delete it!",
      });
  
      if (confirmation.isConfirmed) {
        setLoader({ isActive: true });
        await user_service.DeletepromotoProducts(id).then((response) => {
          if (response) {
            setLoader({ isActive: false });
            setToaster({
              type: "success",
              isShow: true,
              message: "PromotoProduct deleted Successfully",
            });
            setTimeout(() => {
              setToaster((prevToaster) => ({
                ...prevToaster,
                isShow: false,
              }));
              navigate(`/admin/promote/`);
            }, 1000);
          }
        });
      }
    }
  };
 
  useEffect(() => {
    window.scrollTo(0, 0);
    if (params.id) {
      const PromoteProductGetIdId = async () => {
        await user_service.promotoProductsGetId(params.id).then((response) => {
          setLoader({ isActive: false });
          if (response) {
            setProducts(response.data);
          }
        });
      };
      PromoteProductGetIdId();
    }
  }, []);

  const handleDeleteProduct = async (id) => {
    if (id) {
      const confirmation = await Swal.fire({
        title: "Are you sure?",
        text: "It will be permanently deleted!",
        icon: "warning",
        showCancelButton: true,
        confirmButtonColor: "#3085d6",
        cancelButtonColor: "#d33",
        confirmButtonText: "Yes, delete it!",
      });
  
      if (confirmation.isConfirmed) {
        setLoader({ isActive: true });
        await user_service.productItemsDelete(id).then((response) => {
          if (response) {
            setLoader({ isActive: false });
            setToaster({
              type: "success",
              isShow: true,
              message: "ProductItems deleted Successfully",
            });
            setTimeout(() => {
              setToaster((prevToaster) => ({
                ...prevToaster,
                isShow: false,
              }));
              navigate(`/admin/promote/`);
            }, 1000);
          }
        });
      }
    }
  };

  return (
    <div className="bg-secondary float-left w-100 pt-4 mb-4">
      <Loader isActive={isActive} />
      {isShow && (
        <Toaster
          types={types}
          isShow={isShow}
          toasterBody={toasterBody}
          message={message}
        />
      )}
      <main className="page-wrapper profile_page_wrap">
        <div className="product_vendors content-overlay">
          <div className="float-left w-100 d-flex align-items-center justify-content-between mb-4">
            <h3 className="mb-0 text-white">Promote Products/Vendors</h3>
            <span className="pull-right">
              <NavLink
                to={`/promote`}
                className="btn btn-secondary pull-right ms-3"
                type="button"
                //   onClick={handleSubmit}
              >
                Promote
              </NavLink>
              <NavLink
                to={`/admin/promote`}
                className="btn btn-secondary pull-right ms-3"
                type="button"
              >
                Category List
              </NavLink>
              {params.details ? (
                <NavLink
                  to={`/add-markting/${params.id}/${params.details}`}
                  className="btn btn-primary pull-right"
                  type="button"
                >
                  Add New
                </NavLink>
              ) : (
                <NavLink
                  to={`/add-markting/${params.id}`}
                  className="btn btn-primary pull-right"
                  type="button"
                >
                  Add New
                </NavLink>
              )}
            </span>
          </div>
        </div>
        <div className="">
          <div className="FeaturedBox promoted_product float-left w-100">
            <div className="row">
              <h5 className="text-white">{products.title}</h5>
              {productItem.data && productItem.data.length > 0 ? (
                productItem.data.map((items) => (
                  <div
                    // to={`/add-markting/${params.id}/${items._id}`}
                    className="col-lg-3 col-md-6 col-sm-6 col-12 tns-item tns-slide-cloned tns-slide-active p-3"
                  >
                    <div className="card shadow-sm cursor-pointer card-hover border-0 h-100">
                      <div className="card-img-top card-img-hover">
                        {items.image.length > 0 ? (
                          <>
                            <img
                              className="img-fluid rounded-0 w-100"
                              src={items.image[0].images}
                              alt="images"
                            />
                          </>
                        ) : (
                          <img
                            className="img-fluid rounded-0 w-100"
                            src={PromoteImage}
                            alt="image"
                          />
                        )}
                      </div>
                      <div className="card-body position-relative pb-3">
                        <p className="mb-0 fs-sm text-muted">
                          {items.productname}
                          <br />
                          {items.productblurb}
                        </p>
                        <div className="row mt-3">
                          <div className="col-lg-6 col-md-6 col-sm-6 col-6">
                            {params.details ?
                            <NavLink
                              type="button"
                              className="text-center "
                              to={`/update-markting/${params.id}/${params.details}/${items._id}`}
                            >
                              <i className="fi-edit me-2"></i>
                              {/* <i className="fa fa-eye me-2" aria-hidden="true"></i> */}
                              Edit
                            </NavLink>
                            :
                            <NavLink
                            type="button"
                            className="text-center "
                            to={`/update-markting/${params.id}/${items._id}`}
                          >
                            <i className="fi-edit me-2"></i>
                            {/* <i className="fa fa-eye me-2" aria-hidden="true"></i> */}
                            Edit
                          </NavLink>
                            }
                          </div>
                          <div className="col-lg-6 col-md-6 col-sm-6 col-6">
                            <a
                              type="button"
                              aria-current="page"
                              className="text-center  active"
                              onClick={() => handleDeleteProduct(items._id)}
                            >
                              <i
                                className="fa fa-trash-o me-2"
                                aria-hidden="true"
                              ></i>
                              Delete
                            </a>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                ))
              ) : (
                <p className="float-left w-100 text-center text-light">
                  No products found, please check back later
                </p>
              )}
            </div>
          </div>
        </div>
        <div className="mt-4">
          {params.details ? (
            <></>
          ) : (
            <span className="pull-right">
              <>
                <button
                  className="btn btn-secondary pull-right ms-3"
                  type="button"
                  onClick={() => handleRemoveProduct(products._id)}
                >
                  Delete
                </button>

                <NavLink
                  className="btn btn-primary pull-right ms-3"
                  type="button"
                  to={`/add-promotecatalog/${products._id}`}
                >
                  Edit
                </NavLink>

                {/* { console.log(items.category_id)} */}
              </>
            </span>
          )}
        </div>
      </main>
    </div>
  );
};

export default AdminProductListing;

import React, { useState, useEffect } from "react";
import { NavLink, useParams } from "react-router-dom";
import user_service from "../service/user_service";
import Loader from "../../Pages/Loader/Loader.js";
import Toaster from "../../Pages/Toaster/Toaster.js";
import PromoteImage from "../img/image.jpg";
const PromoteProduct = () => {
  const [loader, setLoader] = useState({ isActive: null });
  const { isActive } = loader;
  const [toaster, setToaster] = useState({
    types: null,
    isShow: null,
    toasterBody: "",
    message: "",
  });
  const { types, isShow, toasterBody, message } = toaster;
  const params = useParams();

  const [productItem, setProductItem] = useState([]);
  const [promoteProduct, setPromoteProduct] = useState("");
  const [selectedImage, setSelectedImage] = useState(null);

  const handleImageClick = (imageSrc) => {
    setSelectedImage(imageSrc);
    document.getElementById("closeModal").click();
  };
  useEffect(() => {
    setLoader({ isActive: true });
    if (params.details) {
      console.log(params.details);
      const ProductItem = async () => {
        await user_service
          .productItemsVendorGet(params.details)
          .then((response) => {
            setLoader({ isActive: false });
            if (response) {
              setProductItem(response.data);
            }
          });
      };
      ProductItem();
    }
  }, []);

  useEffect(() => {
    if (params.id) {
      const PromoteProductGetIdId = async () => {
        await user_service.promotoProductsGetId(params.id).then((response) => {
          setLoader({ isActive: false });
          if (response) {
            setPromoteProduct(response.data);
          }
        });
      };
      PromoteProductGetIdId();
    }
  }, []);

  return (
    <div className="bg-secondary float-left w-100 pt-4 mb-4">
      <Loader isActive={isActive} />
      {isShow && (
        <Toaster
          types={types}
          isShow={isShow}
          toasterBody={toasterBody}
          message={message}
        />
      )}
      <main className="page-wrapper profile_page_wrap">
        <div className="content-overlay">
          <div className="bg-light border-0 rounded-3 p-3">
            <h3 className="mb-2">Product & Service Catalog</h3>
            <p>
              Offering a world of products and services designed to meet your
              every need in our comprehensive catalog.
            </p>
            <div className="bg-light rounded-3 border-0 p-3">
              <div className="FeaturedBox catalog_product">
                <div className="col-md-12">
                  <h5>{promoteProduct.title}</h5>
                  <div className="row">
                    {productItem.data && productItem.data.length > 0 ? (
                      productItem.data.map((items) => (
                        <>
                          <div className="col-lg-3 col-md-6 col-sm-6 col-12 tns-item tns-slide-cloned tns-slide-active p-3">
                            <div className="card shadow-sm card-hover cursor-pointer border-0">
                              {items.image.length > 0 ? (
                                <div className="card-img-top card-img-hover">
                                  {params.details ? (
                                    <NavLink
                                      to={`/promote/catalog-details/${params.id}/${params.details}/${items._id}`}
                                    >
                                      <img
                                        className="img-fluid w-100 rounded-0"
                                        src={items.image[0].images}
                                        alt="image"
                                      />
                                    </NavLink>
                                  ) : (
                                 ""
                                  )}
                                </div>
                              ) : (
                                <div className="card-img-top card-img-hover">
                                  <NavLink
                                    to={`/promote/catalog-details/${params.id}/${params.details}/${items._id}`}
                                    className=""
                                  >
                                    {/* <img src={items.image[0].images} alt="images" /> */}
                                    <img
                                      className="img-fluid w-100 rounded-0"
                                      src={PromoteImage}
                                      alt="image"
                                    />
                                  </NavLink>
                                </div>
                              )}

                              <div className="card-body position-relative text-center pb-3">
                                <h3 className="h6 mb-2 fs-base">
                                  {items.productname}
                                </h3>
                                <p className="mb-0 fs-sm text-muted">
                                  {items.productblurb}
                                </p>
                              </div>
                              <div className="read-more float-left w-100 text-center mb-3">
                                <NavLink
                                  to={`/promote/catalog-details/${params.id}/${params.details}/${items._id}`}
                                  className=""
                                >
                                  View Details
                                </NavLink>
                              </div>
                            </div>
                          </div>
                        </>
                      ))
                    ) : (
                      <div className="items_assign">
                        Apologies, but nothing was found; please check back
                        later.
                      </div>
                    )}
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="pull-right mt-4">
            {promoteProduct.category === "vendor" ? (
              <NavLink
                className="btn btn-secondary pull-right"
                type="button"
                to={`/vender`}
              >
                Back
              </NavLink>
            ) : (
              <NavLink
                className="btn btn-secondary pull-right"
                type="button"
                to={`/promote`}
              >
                Back
              </NavLink>
            )}
          </div>
          <div className="modal" role="dialog" id="addDocument">
            <div className="modal-dialog" role="document">
              <div className="modal-content">
                <div className="modal-body row mt-5">
                  <div className="content-overlay mb-md-4 ">
                    <button
                      type="button"
                      className="btn btn-default btn btn-secondary btn-sm pull-right"
                      id="closeModal"
                      data-dismiss="modal"
                    >
                      X
                    </button>
                    <div className="bg-light shadow-sm rounded-3 p-2 p-md-5 mb-2">
                      <div className="row">
                        {selectedImage && (
                          <img src={selectedImage} alt="Selected Image" />
                        )}
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </main>
    </div>
  );
};

export default PromoteProduct;

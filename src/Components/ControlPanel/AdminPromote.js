import React, { useState, useEffect } from "react";

import { NavLink } from "react-router-dom";
import user_service from "../service/user_service";
import Loader from "../../Pages/Loader/Loader.js";
import Toaster from "../../Pages/Toaster/Toaster.js";
import jwt from "jwt-decode";

const AdminPromote = () => {
  const [loader, setLoader] = useState({ isActive: null });
  const { isActive } = loader;
  const [toaster, setToaster] = useState({
    types: null,
    isShow: null,
    toasterBody: "",
    message: "",
  });
  const { types, isShow, toasterBody, message } = toaster;
  const [products, setProducts] = useState([]);
  const PromotoProductsGet = async () => {
    await user_service
      .PromotoProductsGet(localStorage.getItem("active_office"))
      .then((response) => {
        setLoader({ isActive: false });
        if (response) {
          const groupedBycategory = response.data.data.reduce((acc, item) => {
            const category = item.category;
            if (!acc[category]) {
              acc[category] = [];
            }
            acc[category].push(item);
            return acc;
          }, {});
          setProducts(groupedBycategory);
        }
      });
  };

  useEffect(() => {
    window.scrollTo(0, 0);
    setLoader({ isActive: true });
    PromotoProductsGet();
  }, []);

  function populateSection(sectionId, items) {
    if (!items) {
      return null; // Return null or any loading indicator if items are undefined
    }

    return (
      <div className="FeaturedBox" key={sectionId}>
        <div className="row">{items.map((item) => generateItemHTML(item))}</div>
      </div>
    );
  }
  function generateItemHTML(item) {
    console.log(item);
    return (
      <div
        className="col-lg-3 col-md-6 col-sm-6 col-12 tns-item tns-slide-cloned tns-slide-active p-3"
        key={item._id}
      >
        <div className="card shadow-sm cursor-pointer card-hover border-0 h-100">
          <div className="card-img-top card-img-hover">

              <NavLink to={`/admin/sub-Vendor/${item._id}`}>
                <img
                  className="img-fluid rounded-0 w-100"
                  src={item.image}
                  alt="images"
                />
              </NavLink>
            {/* {item.category === "vendor" ? (
              <NavLink to={`/admin/sub-Vendor/${item._id}`}>
                <img
                  className="img-fluid rounded-0 w-100"
                  src={item.image}
                  alt="images"
                />
              </NavLink>
            ) : (
              <NavLink to={`/admin-product-items/${item._id}`}>
                <img
                  className="img-fluid rounded-0 w-100"
                  src={item.image}
                  alt="images"
                />
              </NavLink>
            )} */}
          </div>
          <div className="card-body position-relative text-center pb-3">
            <NavLink type="button">{item.title}</NavLink>
          </div>
        </div>
      </div>
    );
  }
  return (
    <div className="bg-secondary float-left w-100 pt-4 mb-4">
      <Loader isActive={isActive} />
      {isShow && (
        <Toaster
          types={types}
          isShow={isShow}
          toasterBody={toasterBody}
          message={message}
        />
      )}
      <main className="page-wrapper profile_page_wrap">
        <div className="content-overlay">
          <div className="">
            <div className="">
              <div className="float-left w-100 d-flex align-items-center justify-content-start mb-4">
                <h3 className="text-white mb-0">Catalog</h3>
              </div>
              <hr className="mt-2" />
              <div className="mt-4">
                <div>
                  <div id="GalleryGrid">
                    <h5 className="text-white">Products</h5>
                    {populateSection("ProductsSection", products.product)}
                    <p>&nbsp;</p>
                    <h5 className="text-white">Vendors</h5>
                    {populateSection("VendorsSection", products.vendor)}
                  </div>
                </div>
              </div>
              <div className="pull-right mt-4">
                {localStorage.getItem("auth") &&
                jwt(localStorage.getItem("auth")).contactType == "admin" ? (
                  <NavLink className="btn btn-info" to="/add-promotecatalog">
                    Add New Catalog
                  </NavLink>
                ) : (
                  ""
                )}
              </div>
            </div>
          </div>
        </div>
      </main>
    </div>
  );
};

export default AdminPromote;

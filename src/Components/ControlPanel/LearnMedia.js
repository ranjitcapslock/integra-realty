import React, { useState, useEffect } from "react";
// import avtar from "./Components/img/avtar.jpg";
import _ from "lodash";
import user_service from "../service/user_service";
import jwt from "jwt-decode";
import Loader from "../../Pages/Loader/Loader.js";
import Toaster from "../../Pages/Toaster/Toaster.js";
import { NavLink, useNavigate, useParams } from "react-router-dom";
import { Button } from "react-bootstrap";
function LearnMedia() {
  const url = window.location.pathname.split("/").pop();
  const [loader, setLoader] = useState({ isActive: null });
  const { isActive } = loader;
  const [toaster, setToaster] = useState({
    types: null,
    isShow: null,
    toasterBody: "",
    message: "",
  });
  const { types, isShow, toasterBody, message } = toaster;
  const [checkData, setCheckData] = useState("");
  const navigate = useNavigate();
  const params = useParams();
  const [learnbyAgentNew, setLearnbyAgentNew] = useState([]);
  const [learnbyId, setLearnbyId] = useState([]);
  const [learnbyAgent, setLearnbyAgent] = useState([]);

  // const addVideocategoryGetdata = async () => {
  //   await user_service.addVideocategoryGet().then((response) => {
  //     setLoader({ isActive: false });
  //     if (response) {
  //       setLearnbyAgentNew(response.data);
  //     }
  //   });
  // };

  //   const addVideocategoryGetdata = async () => {
  //     await user_service.addVideocategoryGet(localStorage.getItem("active_office")).then((response) => {
  //       setLoader({ isActive: false });
  //       if (response) {
  //         const groupedBycategory = response.data.data.reduce((acc, item) => {
  //           const category = item.videoCategory;
  //           if (!acc[category]) {
  //             acc[category] = [];
  //           }
  //           acc[category].push(item);
  //           return acc;
  //         }, {});

  //         console.log(groupedBycategory);
  //         setLearnbyAgentNew(groupedBycategory);
  //       }
  //     });

  // };

  const addVideocategoryGetdata = async () => {
    const type = params.type ?? "";
    await user_service.addVideocategoryGet(type).then((response) => {
      if (response) {
        setLoader({ isActive: false });
        console.log(response);
        setLearnbyAgentNew(response.data);
        // console.log(response.data);
        // console.log("learnbyAgent");
      }
    });
  };

  useEffect(() => {
    window.scrollTo(0, 0);
    setLoader({ isActive: true });
    addVideocategoryGetdata();
    // getLearnbyAgentApi();
    // getLearnbyIdApi();
  }, [url]);

  const handleRemove = async (id) => {
    console.log(id);
    setLoader({ isActive: true });
    await user_service.learnmediaDelete(id).then((response) => {
      if (response) {
        setLoader({ isActive: false });
        // getLearnbyAgentApi();
        // getLearnbyIdApi();
      }
    });
  };

  const handleDeleteCategory = async (id) => {
    if (id) {
      setLoader({ isActive: true });
      await user_service.videoCategoryDelete(id).then((response) => {
        if (response) {
          // console.log(response);
          setLoader({ isActive: false });
          addVideocategoryGetdata();
          setToaster({
            type: "Category Deleted Successfully",
            isShow: true,
            // toasterBody: response.data.message,
            message: "Category Deleted Successfully",
          });
          setTimeout(() => {
            setToaster((prevToaster) => ({
              ...prevToaster,
              isShow: false,
            }));
          }, 2000);
        }
      });
    }
  };

  return (
    <div className="bg-secondary float-left w-100 pt-3 mb-4">
      <Loader isActive={isActive} />
      {isShow && (
        <Toaster
          types={types}
          isShow={isShow}
          toasterBody={toasterBody}
          message={message}
        />
      )}
      <main className="page-wrapper profile_page_wrap">
        <div className="content-overlay">
          <div className="justify-content-center">
            <div className="">
              <div className="d-flex align-items-center justify-content-between mb-4">
                <h3 className="text-white pull-left mb-0">
                  {params.type == "workspaceOverview"
                    ? "B.A.S.E Overview"
                    : params.type == "rEALTORNews"
                    ? "REALTOR® News"
                    : params.type == "videoTraining"
                    ? "Video Training"
                    : ""}
                </h3>
                {localStorage.getItem("auth") &&
                jwt(localStorage.getItem("auth")).contactType == "admin" ? (
                  <div className="pull-right">
                    <NavLink
                      className="btn btn-info btn-sm pull-right"
                      type="button"
                      to="/addvideocategory"
                    >
                      Add A New Category
                    </NavLink>
                  </div>
                ) : (
                  ""
                )}
              </div>
              <hr className="mb-3" />
              {params.type == "videoTraining" ? (
                <div className="justify-content-center pb-sm-2">
                  <div className="">
                    <div className="">
                      <div className="">
                       <h4 className="text-white">Assigned Category</h4>
                        <div className="row">
                          {learnbyAgentNew.data &&
                          learnbyAgentNew.data.length > 0 ? (
                            learnbyAgentNew.data.map((item, index) => (
                              <div
                                key={index}
                                className="col-lg-3 col-md-6 col-sm-6 col-12 tns-item tns-slide-cloned tns-slide-active p-3"
                              >
                                <div className="card shadow-sm cursor-pointer card-hover border-0 h-100">
                                  <NavLink
                                    to={`/videocategory/${params.type}/${item._id}`}
                                  >
                                    <div className="card-img-top card-img-hover">
                                      {item.videoCategory ===
                                      "videoTraining" ? (
                                        <>
                                          {item.thumbnail_image ? (
                                            <img
                                              className="img-fluid rounded-0 w-100"
                                              src={item.thumbnail_image}
                                              alt="Video Thumbnail"
                                            />
                                          ) : (
                                            ""
                                          )}
                                        </>
                                      ) : (
                                        ""
                                      )}
                                    </div>
                                    <div className="card-body position-relative pb-0">
                                      <h3 className="h6 mb-0 fs-base mb-0">
                                        {item.title.length > 55
                                          ? item.title.substring(0, 55) + "..."
                                          : item.title}
                                      </h3>
                                      <p className="fs-sm text-muted mt-1 mb-0">
                                        <small>
                                          {item.description.length > 100
                                            ? item.description.substring(
                                                0,
                                                100
                                              ) + "..."
                                            : item.description}
                                        </small>
                                      </p>
                                    </div>
                                  </NavLink>
                                   <div className="card-body">
                                   <div className="row">
                                    <div className="col-lg-6 col-md-6 col-sm-6 col-6">
                                      <NavLink
                                        to={`/addvideocategory/${item._id}`}
                                        type="button"
                                      >
                                        <i className="fi-edit me-2"></i>Edit
                                      </NavLink>
                                    </div>
                                    <div className="col-lg-6 col-md-6 col-sm-6 col-6">
                                      <a
                                        type="button"
                                        aria-current="page"
                                        className="text-center  active"
                                        onClick={() =>
                                          handleDeleteCategory(item._id)
                                        }
                                      >
                                        <i
                                          className="fa fa-trash-o me-2"
                                          aria-hidden="true"
                                        ></i>
                                        Delete
                                      </a>
                                    </div>
                                  </div>
                                  </div>
                                </div>
                              </div>
                            ))
                          ) : (
                            <div className="small-5 columns float-left w-100 text-center mt-4">
                              <p
                                className="text-center"
                                style={{ color: "#ffffff" }}
                              >
                                No Media Assigned
                              </p>
                            </div>
                          )}
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              ) : (
                <div className="justify-content-center pb-sm-2">
                  <div className="">
                    <div className="">
                      <div className="">
                      <h4 className="text-white">Assigned Items</h4>
                        <div className="row">
                          {learnbyAgentNew.data &&
                          learnbyAgentNew.data.length > 0 ? (
                            learnbyAgentNew.data.map((item, index) => (
                              <div
                                key={index}
                                className="col-lg-3 col-md-6 col-sm-6 col-12 tns-item tns-slide-cloned tns-slide-active p-3"
                              >
                                <div className="card shadow-sm cursor-pointer card-hover border-0 h-100">
                                  <NavLink
                                    to={`/videoWorkspaceOverview/${params.type}/${item._id}`}
                                  >
                                    <div className="card-img-top card-img-hover">
                                      {item.videoCategory ===
                                      "workspaceOverview" ? (
                                        <>
                                          {item.thumbnail_image ? (
                                            <img
                                              className="img-fluid rounded-0 w-100"
                                              src={item.thumbnail_image}
                                              alt="Video Thumbnail"
                                            />
                                          ) : (
                                            ""
                                          )}
                                        </>
                                      ) : (
                                        ""
                                      )}
                                    </div>
                                    <div className="card-body position-relative pb-0">
                                      <h3 className="h6 mb-0 fs-base mb-0">
                                        {item.title.length > 55
                                          ? item.title.substring(0, 55) + "..."
                                          : item.title}
                                      </h3>
                                      <p className="fs-sm text-muted mt-1 mb-0">
                                        <small>
                                          {item.description.length > 100
                                            ? item.description.substring(
                                                0,
                                                100
                                              ) + "..."
                                            : item.description}
                                        </small>
                                      </p>
                                    </div>
                                  </NavLink>

                                  <div className="card-body">
                                   <div className="row">
                                    <div className="col-lg-6 col-md-6 col-sm-6 col-6">
                                      <NavLink
                                        to={`/addvideocategory/${item._id}`}
                                        type="button"
                                      >
                                        <i className="fi-edit me-2"></i>Edit
                                      </NavLink>
                                    </div>
                                    <div className="col-lg-6 col-md-6 col-sm-6 col-6">
                                      <a
                                        type="button"
                                        aria-current="page"
                                        className="text-center  active"
                                        onClick={() =>
                                          handleDeleteCategory(item._id)
                                        }
                                      >
                                        <i
                                          className="fa fa-trash-o me-2"
                                          aria-hidden="true"
                                        ></i>
                                        Delete
                                      </a>
                                    </div>
                                  </div>
                                  </div>
                                </div>
                              </div>
                            ))
                          ) : (
                            <div className="small-5 columns float-left w-100 text-center mt-4">
                              <span
                                className="text-center"
                                style={{ color: "#ffffff" }}
                              >
                                No Media Assigned
                              </span>
                            </div>
                          )}
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              )}
            </div>
          </div>
        </div>
      </main>
    </div>
  );
}
export default LearnMedia;

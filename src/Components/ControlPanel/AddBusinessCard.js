// import React, { useState, useEffect, useRef } from "react";
// import Loader from "../../Pages/Loader/Loader.js";
// import Toaster from "../../Pages/Toaster/Toaster.js";
// import { NavLink, useNavigate, useParams } from "react-router-dom";
// import _ from "lodash";

// import "react-select2-wrapper/css/select2.css";
// import jwt from "jwt-decode";
// import axios from "axios";

// import user_service from "../service/user_service";

// function AddBusinessCard() {
//   const initialValues = {
//     office_id: localStorage.getItem("active_office_id"),
//     office_name: localStorage.getItem("active_office"),
//     firstName: "",
//     lastName: "",
//     phone: "",
//     email: "",
//     web: "",
//     social_links: "",
//     appointment: "",
//     location: "",
//   };

//   const navigate = useNavigate();
//   const [formValues, setFormValues] = useState(initialValues);

//   const [file, setFile] = useState(null);
//   const [data, setData] = useState("");
//   const [loader, setLoader] = useState({ isActive: null });
//   const { isActive } = loader;
//   const [toaster, setToaster] = useState({
//     types: null,
//     isShow: null,
//     toasterBody: "",
//     message: "",
//   });
//   const { types, isShow, toasterBody, message } = toaster;
//   const [isSubmitClick, setISSubmitClick] = useState(false);
//   const [formErrors, setFormErrors] = useState({});

//   const handleChange = (e) => {
//     const { name, value } = e.target;
//     setFormValues({ ...formValues, [name]: value });
//   };

//   {
//     /* <!-- Form Validation Start--> */
//   }
//   useEffect(() => {
//     window.scrollTo(0, 0);
//     if (formValues && isSubmitClick) {
//       validate();
//     }
//   }, [formValues]);

//   const validate = () => {
//     const values = formValues;
//     const errors = {};
//     if (!values.full_name) {
//       errors.full_name = "Full name is required";
//     }

//     if (!values.email) {
//       errors.email = "email is required";
//     } else if (
//       !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.email)
//     ) {
//       errors.email = "Invalid email address";
//     }

//     if (!values.phone) {
//       errors.phone = "Phone is required";
//     }

//     if (!values.designation) {
//       errors.designation = "Designation is required";
//     }

//     if (!values.about_me) {
//       errors.about_me = "About me is required";
//     }

//     setFormErrors(errors);
//     return errors;
//   };
//   {
//     /* <!-- Form Validation End--> */
//   }

//   const handleSubmit = async (e) => {
//     e.preventDefault();
//     setISSubmitClick(true);
//     let checkValue = validate();
//     if (_.isEmpty(checkValue)) {
//       const userData = {
//         office_id: localStorage.getItem("active_office_id"),
//         office_name: localStorage.getItem("active_office"),
//         agentId: jwt(localStorage.getItem("auth")).id,
//         full_name: formValues.full_name,
//         cover_image: data,
//         phone: formValues.phone,
//         email: formValues.email,
//         designation: formValues.designation,
//         personal_number: formValues.personal_number,
//         phone: formValues.phone,
//         website: formValues.website,
//         appointment: formValues.appointment,
//         location: formValues.location,
//         instagram: formValues.instagram,
//         linkedIn: formValues.linkedIn,
//         facebook: formValues.facebook,
//         tiktok: formValues.tiktok,
//         twitter: formValues.twitter,
//         youtube: formValues.youtube,
//         about_me: formValues.about_me,
//       };
//       console.log("userData", userData);
//       try {
//         setLoader({ isActive: true });
//         const response = await user_service.BusinessCardPost(userData);

//         if (response) {
//           console.log(response.data);
//           setLoader({ isActive: false });

//           setTimeout(() => {
//             setToaster((prevToaster) => ({ ...prevToaster, isShow: false }));
//             navigate(`/contact/${response.data._id}`);
//           }, 2000);
//         }
//       } catch (err) {
//         setToaster({
//           type: "API Error",
//           isShow: true,
//           toasterBody: err.response.data.message,
//           message: "API Error",
//         });

//         setTimeout(() => {
//           setToaster((prevToaster) => ({ ...prevToaster, isShow: false }));
//         }, 2000);
//       }
//     }
//   };

//   useEffect(() => {
//     const ContactGetById = async () => {
//       await user_service.contactGetByNickname(params.id).then((response) => {
//         if (response) {
//           setFormValues(response.data.data[0]);

//           if (response.data.data[0] && response.data.data[0].additionalActivateFields) {
//             setGetContact((prevFormValues) => ({
//               ...prevFormValues,
//               office:
//                 response.data.data[0].additionalActivateFields.office ||
//                 prevFormValues.office,
//               //   intranet_id: response.data.data[0].additionalActivateFields.intranet_id || prevFormValues.intranet_id,
//               intranet_id:
//                 response.data.data[0].additionalActivateFields.intranet_id ||
//                 response.data.data[0].email,
//               subscription_level:
//                 response.data.data[0].additionalActivateFields.subscription_level ||
//                 prevFormValues.subscription_level,
//               admin_note:
//                 response.data.data[0].additionalActivateFields.admin_note ||
//                 prevFormValues.admin_note,
//               sponsor_associate:
//                 response.data.data[0].additionalActivateFields.sponsor_associate ||
//                 prevFormValues.sponsor_associate,
//               staff_recruiter:
//                 response.data.data[0].additionalActivateFields.staff_recruiter ||
//                 prevFormValues.staff_recruiter,
//               nrds_id:
//                 response.data.data[0].additionalActivateFields.nrds_id ||
//                 prevFormValues.nrds_id,
//               associate_member:
//                 response.data.data[0].additionalActivateFields.associate_member ||
//                 prevFormValues.associate_member,
//               plan_date:
//                 response.data.data[0].additionalActivateFields.plan_date ||
//                 prevFormValues.plan_date,
//               billing_date:
//                 response.data.data[0].additionalActivateFields.billing_date ||
//                 prevFormValues.billing_date,
//               account_name:
//                 response.data.data[0].additionalActivateFields.account_name ||
//                 prevFormValues.account_name,
//               invoicing:
//                 response.data.data[0].additionalActivateFields.invoicing ||
//                 prevFormValues.invoicing,
//               licensed_since:
//                 response.data.data[0].additionalActivateFields.licensed_since ||
//                 prevFormValues.licensed_since,
//               associate_since:
//                 response.data.data[0].additionalActivateFields.associate_since ||
//                 prevFormValues.associate_since,
//               staff_since:
//                 response.data.data[0].additionalActivateFields.staff_since ||
//                 prevFormValues.staff_since,
//               iscorporation:
//                 response.data.data[0].additionalActivateFields.iscorporation ||
//                 prevFormValues.iscorporation,
//               agentTax_ein:
//                 response.data.data[0].additionalActivateFields.agentTax_ein ||
//                 prevFormValues.agentTax_ein,
//               agent_funding:
//                 response.data.data[0].additionalActivateFields.agent_funding ||
//                 prevFormValues.agent_funding,
//               ssn_itin:
//                 response.data.data[0].additionalActivateFields.ssn_itin ||
//                 prevFormValues.ssn_itin,
//               date_birth:
//                 response.data.data[0].additionalActivateFields.date_birth ||
//                 prevFormValues.date_birth,
//               private_idcard_type:
//                 response.data.data[0].additionalActivateFields.private_idcard_type ||
//                 prevFormValues.private_idcard_type,
//               private_idstate:
//                 response.data.data[0].additionalActivateFields.private_idstate ||
//                 prevFormValues.private_idstate,
//               private_idnumber:
//                 response.data.data[0].additionalActivateFields.private_idnumber ||
//                 prevFormValues.private_idnumber,
//               license_state:
//                 response.data.data[0].additionalActivateFields.license_state ||
//                 prevFormValues.license_state,
//               license_type:
//                 response.data.data[0].additionalActivateFields.license_type ||
//                 prevFormValues.license_type,
//               license_number:
//                 response.data.data[0].additionalActivateFields.license_number ||
//                 prevFormValues.license_number,
//               date_issued:
//                 response.data.data[0].additionalActivateFields.date_issued ||
//                 prevFormValues.date_issued,
//               date_expire:
//                 response.data.data[0].additionalActivateFields.date_expire ||
//                 prevFormValues.date_expire,
//               office_affiliation:
//                 response.data.data[0].additionalActivateFields.office_affiliation ||
//                 prevFormValues.office_affiliation,

//               agent_id:
//                 response.data.data[0].additionalActivateFields.agent_id ||
//                 prevFormValues.agent_id,
//             }));

//             // const mls_membershipString =
//             //   response.data.additionalActivateFields.mls_membership;

//             // if (mls_membershipString) {
//             //   // Check if mls_membershipString is a string
//             //   if (typeof mls_membershipString === "string") {
//             //     try {
//             //       // Attempt to parse the string as JSON
//             //       const mls_membershipParsed = JSON.parse(mls_membershipString);
//             //       // Set the parsed value to your state or variable
//             //       if (mls_membershipParsed == "") {
//             //         setMls_membership(mls_membership);
//             //       } else {
//             //         setMls_membership(mls_membershipParsed);
//             //       }
//             //     } catch (error) {
//             //       // Handle the error if parsing fails
//             //       console.error("Error parsing mls_membership as JSON:", error);
//             //       // You might want to set a default value or handle the error in another way
//             //       // For debugging, you can log the JSON parse error message:
//             //       console.error("JSON Parse Error:", error.message);
//             //       // Set a default value if parsing fails
//             //       setMls_membership(mls_membership); // Replace "defaultValue" with your default value
//             //     }
//             //   } else {
//             //     // Handle the case where mls_membershipString is not a string (e.g., it's already an object)
//             //     // You can directly set it to your state or variable
//             //     setMls_membership(mls_membership);
//             //   }
//             // } else {

//             //   setMls_membership(mls_membership);
//             // }

//             // const board_membershipString =
//             //   response.data.additionalActivateFields.board_membership;
//             // if (board_membershipString) {
//             //   // Check if board_membershipString is a string
//             //   if (typeof board_membershipString === "string") {
//             //     try {
//             //       // Attempt to parse the string as JSON
//             //       const board_membershipParsed = JSON.parse(
//             //         board_membershipString
//             //       );
//             //       // Set the parsed value to your state or variable
//             //       if (board_membershipParsed == "") {
//             //         setBoard_membership(board_membership);
//             //       } else {
//             //         setBoard_membership(board_membershipParsed);
//             //       }
//             //     } catch (error) {
//             //       // Handle the error if parsing fails
//             //       console.error(
//             //         "Error parsing board_membership as JSON:",
//             //         error
//             //       );
//             //       // You might want to set a default value or handle the error in another way
//             //       // For debugging, you can log the JSON parse error message:
//             //       console.error("JSON Parse Error:", error.message);
//             //       // Set a default value if parsing fails
//             //       setBoard_membership(board_membership); // Replace "defaultValue" with your default value
//             //     }
//             //   } else {
//             //     console.log("sdfdsfdsfsd");
//             //     // Handle the case where board_membershipString is not a string (e.g., it's already an object)
//             //     // You can directly set it to your state or variable
//             //     setBoard_membership(board_membership);
//             //   }
//             // } else {

//             //   setBoard_membership(board_membership);
//             // }
//           }
//         }
//       });
//     };
//     ContactGetById(params.id);
//   }, []);

//   return (
//     <div className="bg-secondary float-left w-100 pt-4">
//       <Loader isActive={isActive} />
//       {isShow && (
//         <Toaster
//           types={types}
//           isShow={isShow}
//           toasterBody={toasterBody}
//           message={message}
//         />
//       )}
//       <div className="container content-overlay mb-md-4">
//         <main className="page-wrapper profile_page_wrap">
//           <h2 className="text-center">Create Business Card</h2>
//           <hr className="mt-2" />

//           <div className="row mt-5 ms-5">
//             <div className="col-md-6">
//               <div className="mb-3">
//                 <label className="form-label">Full Name *</label>
//                 <br />

//                 <input
//                   className="form-control"
//                   type="text"
//                   id="text-input"
//                   name="full_name"
//                   placeholder="First and Last"
//                   onChange={(event) => handleChange(event)}
//                   autoComplete="on"
//                   style={{
//                     border: formErrors?.full_name
//                       ? "1px solid red"
//                       : "1px solid #00000026",
//                   }}
//                   value={formValues.full_name}
//                 />
//                 <div className="invalid-tooltip">{formErrors.full_name}</div>
//               </div>

//               {/* <!-- Phone Input --> */}
//               <div className="mb-3 mt-5">
//                 <label className="form-label mt-4">Phone *</label>
//                 <input
//                   className="form-control"
//                   type="tel"
//                   id="tel-input"
//                   name="phone"
//                   placeholder="Enter Phone"
//                   onChange={(event) => handleChange(event)}
//                   autoComplete="on"
//                   pattern="[0-9]+"
//                   required
//                   title="Please enter a valid phone number"
//                   value={formValues.phone}
//                 />
//               </div>
//             </div>

//             <div className="col-md-6">
//               <div className="mb-3">
//                 <label className="form-label">Cover Image *</label>
//                 <input
//                   className="file-uploader file-uploader-grid"
//                   id="inline-form-input"
//                   type="file"
//                   name="cover_image"
//                   onChange={handleFileUpload}
//                   value={formValues.cover_image}
//                   style={{
//                     border: formErrors?.cover_image
//                       ? "1px solid red"
//                       : "1px solid #00000026",
//                   }}
//                 />
//                 <div className="invalid-tooltip">{formErrors.cover_image}</div>
//               </div>

//               {/* <!-- Email input --> */}
//               <div className="mb-3">
//                 <label className="form-label">Email</label>
//                 <input
//                   className="form-control"
//                   id="email-input"
//                   type="text"
//                   name="email"
//                   placeholder="Enter email"
//                   onChange={(event) => handleChange(event)}
//                   autoComplete="on"
//                   style={{
//                     border: formErrors?.email
//                       ? "1px solid red"
//                       : "1px solid #00000026",
//                   }}
//                   value={formValues.email}
//                 />
//                 <div className="invalid-tooltip">{formErrors.email}</div>
//               </div>
//             </div>

//             <div className="col-md-12">
//               <div className="mb-3">
//                 <label className="form-label">Designation *</label>
//                 <input
//                   className="form-control"
//                   id="text"
//                   type="text"
//                   name="designation"
//                   onChange={(event) => handleChange(event)}
//                   autoComplete="on"
//                   style={{
//                     border: formErrors?.designation
//                       ? "1px solid red"
//                       : "1px solid #00000026",
//                   }}
//                   value={formValues.designation}
//                 />
//                 <div className="invalid-tooltip">{formErrors.designation}</div>
//               </div>
//             </div>
//           </div>

//           <div className="row mt-5 ms-5">
//             <h4>Media Info (Optional)</h4>
//             <hr className="mt-2" />

//             <div className="col-md-6 mt-5">
//               {/* <!-- Phone Input --> */}
//               <div className="mb-3">
//                 <label className="form-label">Phone Number</label>
//                 <input
//                   className="form-control"
//                   type="tel"
//                   id="tel-input"
//                   name="personal_number"
//                   placeholder="Personal/ Business number"
//                   onChange={(event) => handleChange(event)}
//                   autoComplete="on"
//                   pattern="[0-9]+"
//                   title="Please enter a valid phone number"
//                   value={formValues.personal_number}
//                 />
//               </div>

//               <div className="mb-3">
//                 <label className="form-label">Appointment</label>
//                 <br />
//                 <input
//                   className="form-control"
//                   type="text"
//                   id="text-input"
//                   name="appointment"
//                   placeholder="http//"
//                   onChange={(event) => handleChange(event)}
//                   value={formValues.appointment}
//                 />
//               </div>
//             </div>

//             <div className="col-md-6 mt-5">
//               <div className="mb-3">
//                 <label className="form-label">Website</label>
//                 <br />
//                 <input
//                   className="form-control"
//                   type="text"
//                   id="text-input"
//                   name="website"
//                   placeholder="http//"
//                   onChange={(event) => handleChange(event)}
//                   value={formValues.website}
//                 />
//               </div>

//               <div className="mb-3">
//                 <label className="form-label">Location</label>
//                 <br />
//                 <input
//                   className="form-control"
//                   type="text"
//                   id="text-input"
//                   name="location"
//                   placeholder="http//"
//                   onChange={(event) => handleChange(event)}
//                   value={formValues.location}
//                 />
//               </div>
//             </div>
//           </div>

//           <div className="row mt-5 ms-5">
//             <h4>Social info</h4>
//             <hr className="mt-2" />

//             <div className="col-md-6 mt-5">
//               {/* <!-- Phone Input --> */}
//               <div className="mb-3">
//                 <label className="form-label">Instagram</label>
//                 <br />
//                 <input
//                   className="form-control"
//                   type="text"
//                   id="text-input"
//                   name="instagram"
//                   placeholder="http//"
//                   onChange={(event) => handleChange(event)}
//                   value={formValues.instagram}
//                 />
//               </div>

//               <div className="mb-3">
//                 <label className="form-label">Facebook</label>
//                 <br />
//                 <input
//                   className="form-control"
//                   type="text"
//                   id="text-input"
//                   name="facebook"
//                   placeholder="http//"
//                   onChange={(event) => handleChange(event)}
//                   value={formValues.facebook}
//                 />
//               </div>

//               <div className="mb-3">
//                 <label className="form-label">Twitter</label>
//                 <br />
//                 <input
//                   className="form-control"
//                   type="text"
//                   id="text-input"
//                   name="twitter"
//                   placeholder="http//"
//                   onChange={(event) => handleChange(event)}
//                   value={formValues.twitter}
//                 />
//               </div>
//             </div>

//             <div className="col-md-6 mt-5">
//               <div className="mb-3">
//                 <label className="form-label">LinkedIn</label>
//                 <br />
//                 <input
//                   className="form-control"
//                   type="text"
//                   id="text-input"
//                   name="linkedIn"
//                   placeholder="http//"
//                   onChange={(event) => handleChange(event)}
//                   value={formValues.linkedIn}
//                 />
//               </div>

//               <div className="mb-3">
//                 <label className="form-label">TikTok</label>
//                 <br />
//                 <input
//                   className="form-control"
//                   type="text"
//                   id="text-input"
//                   name="tiktok"
//                   placeholder="http//"
//                   onChange={(event) => handleChange(event)}
//                   value={formValues.tiktok}
//                 />
//               </div>

//               <div className="mb-3">
//                 <label className="form-label">Youtube </label>
//                 <br />
//                 <input
//                   className="form-control"
//                   type="text"
//                   id="text-input"
//                   name="youtube"
//                   placeholder="http//"
//                   onChange={(event) => handleChange(event)}
//                   value={formValues.youtube}
//                 />
//               </div>
//             </div>
//           </div>

//           <div className="row mt-5 ms-5">
//             <h4>Business</h4>
//             <hr className="mt-2" />
//             <div className="col-md-12 mt-3">
//               <div className="mb-3">
//                 <label className="form-label">About me *</label>
//                 <textarea
//                   className="form-control"
//                   id="textarea-input"
//                   rows="5"
//                   name="about_me"
//                   placeholder="This is important and will be shown on your business card."
//                   value={formValues.about_me}
//                   onChange={handleChange}
//                 ></textarea>
//                 <div className="invalid-tooltip">{formErrors.about_me}</div>
//               </div>
//             </div>
//             <div>
//               <button
//                 type="button"
//                 className="btn btn-info btn-lg mb-2 pull-left"
//                 onClick={handleSubmit}
//               >
//                 Submit
//               </button>
//             </div>
//           </div>
//         </main>
//       </div>
//     </div>
//   );
// }
// export default AddBusinessCard;

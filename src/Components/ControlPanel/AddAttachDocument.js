import React, { useState, useEffect } from "react";
import _ from "lodash";
import user_service from "../service/user_service";
import jwt from "jwt-decode";
import Loader from "../../Pages/Loader/Loader.js";
import Toaster from "../../Pages/Toaster/Toaster.js";
import { useNavigate, NavLink, useParams } from "react-router-dom";
import axios from "axios";
import { log } from "util";

function AddAttachDocument() {
  const initialValues = {
    title: "",
    blurb: "",
    parent_section: "",
    publication_level: "",
    security: "",
  };
  const [formValues, setFormValues] = useState(initialValues);
  const [loader, setLoader] = useState({ isActive: null });
  const { isActive } = loader;
  const [toaster, setToaster] = useState({
    types: null,
    isShow: null,
    toasterBody: "",
    message: "",
  });
  const { types, isShow, toasterBody, message } = toaster;
  const [mainNavLinkText, setMainNavLinkText] = useState("Corporate");
  const [showData, setShowData] = useState("Document");
  const [showDataId, setShowDataId] = useState("");
  const [checkManager, setCheckManager] = useState("");
  const [checkDocument, setCheckDocument] = useState("");
  const [documentGet, setDocumentGet] = useState([]);
  const [showByDefaultId, setShowByDefaultId] = useState("");
  const [showByTitle, setShowByTitle] = useState("");
  const [file, setFile] = useState(null);
  const [data, setData] = useState("");
  const navigate = useNavigate();
  const params = useParams();
  console.log(params.id);

  const handleDataClick = (item, id) => {
    setShowByTitle(item);
    setShowDataId(id);
    document.getElementById("closeModal").click();
  };

  const handleMainNavLinkClick = (e, item) => {
    console.log(item);
    setMainNavLinkText(item);
    document.getElementById("closeLocation").click();
  };

  useEffect(() => {
    window.scrollTo(0, 0);
    setLoader({ isActive: true });
    const ShareDocGet = async () => {
      await user_service.ShareDocGet().then((response) => {
        setLoader({ isActive: false });
        if (response) {
          setDocumentGet(response.data);
          const getResponceId = response.data.data[0]._id;
          const getResponcetitle = response.data.data[0].title;
          setShowByDefaultId(getResponceId);
          setShowByTitle(getResponcetitle);
        }
      });
    };
    ShareDocGet();
  }, []);

  const handleFileUpload = async (e) => {
    const selectedFile = e.target.files[0];

    if (selectedFile) {
      setFile(selectedFile);

      const fileNameWithoutExtension = selectedFile.name.replace(
        /\.[^/.]+$/,
        ""
      ); // Extracts the file name without extension
      const formData = new FormData();
      formData.append("file", selectedFile);

      const config = {
        headers: {
          Accept: "application/json",
          "Content-Type": "multipart/form-data",
          Authorization: `Bearer ${localStorage.getItem("auth")}`,
        },
      };

      try {
        setLoader({ isActive: true });

        const uploadResponse = await axios.post(
          "https://api.brokeragentbase.com/upload",
          // "http://localhost:4000/upload",
          formData,
          config
        );
        const uploadedFileData = uploadResponse.data;

        setLoader({ isActive: false });
        setData(uploadedFileData);
        if (params.id) {
          const userData = {
            agentId: jwt(localStorage.getItem("auth")).id,
            office_id: localStorage.getItem("active_office_id"),
            office_name: localStorage.getItem("active_office"),
            title: formValues.title
              ? formValues.title
              : fileNameWithoutExtension,
            type: "document",
            description: formValues.description
              ? formValues.description
              : fileNameWithoutExtension,
            parent_section: showDataId
              ? showDataId
              : showByDefaultId
              ? showByDefaultId
              : "",
            publication_level: mainNavLinkText,
            exceptions: "",
            resource_url: uploadedFileData,
            pin: checkDocument ? checkDocument : "no",
            lock: checkManager ? checkManager : "no",
            is_active: "yes",
          };
          setLoader({ isActive: true });
          await user_service
            .OfficesharedDocContentUpdate(params.id, userData)
            .then((response) => {
              if (response) {
                console.log(response);
                setFormValues(response.data);
                setLoader({ isActive: false });
                setToaster({
                  type: "Add Section",
                  isShow: true,
                  toasterBody: response.data.message,
                  message: "Add Section Successfully",
                });
                setTimeout(() => {
                  setToaster((prevToaster) => ({
                    ...prevToaster,
                    isShow: false,
                  }));
                  navigate("/control-panel/documents/");
                }, 1000);
              } else {
                setLoader({ isActive: false });
                setToaster({
                  types: "error",
                  isShow: true,
                  toasterBody: response.data.message,
                  message: "Error",
                });
              }
            });
        } else {
          const userData = {
            agentId: jwt(localStorage.getItem("auth")).id,
            office_id: localStorage.getItem("active_office_id"),
            office_name: localStorage.getItem("active_office"),
            title: formValues.title
              ? formValues.title
              : fileNameWithoutExtension,
            type: "document",
            description: formValues.description
              ? formValues.description
              : fileNameWithoutExtension,
            parent_section: showDataId
              ? showDataId
              : showByDefaultId
              ? showByDefaultId
              : "",
            publication_level: mainNavLinkText,
            exceptions: "",
            resource_url: uploadedFileData,
            pin: checkDocument ? checkDocument : "no",
            lock: checkManager ? checkManager : "no",
            is_active: "yes",
          };

          setLoader({ isActive: true });
          await user_service
            .OfficesharedDocContentPost(userData)
            .then((response) => {
              if (response) {
                console.log(response);
                setFormValues(response.data);
                setLoader({ isActive: false });
                setToaster({
                  type: "Add Section",
                  isShow: true,
                  toasterBody: response.data.message,
                  message: "Add Section Successfully",
                });
                setTimeout(() => {
                  setToaster((prevToaster) => ({
                    ...prevToaster,
                    isShow: false,
                  }));
                  navigate("/control-panel/documents/");
                }, 1000);
              } else {
                setLoader({ isActive: false });
                setToaster({
                  types: "error",
                  isShow: true,
                  toasterBody: response.data.message,
                  message: "Error",
                });
              }
            });
        }
      } catch (error) {
        console.error("Error occurred during file upload:", error);
      }
    }
  };

  const handleRemove = async () => {
    setLoader({ isActive: true });
    await user_service
      .OfficesharedDocContentDelete(params.id)
      .then((response) => {
        if (response) {
          const ShareDocGet = async () => {
            await user_service.ShareDocGet().then((response) => {
              setLoader({ isActive: false });
              if (response) {
                setDocumentGet(response.data);
                setToaster({
                  types: "Document_Delete",
                  isShow: true,
                  toasterBody: response.data.message,
                  message: "Document_Delete Successfully",
                });
                setTimeout(() => {
                  setToaster((prevToaster) => ({
                    ...prevToaster,
                    isShow: false,
                  }));
                  navigate("/control-panel/documents/");
                }, 1000);
              }
            });
          };
          ShareDocGet();
        }
      });
  };

  const handleBack = () => {
    navigate("/control-panel/documents/");
  };

  return (
    <div className="bg-secondary float-left w-100 pt-4 mb-4">
      <Loader isActive={isActive} />
      {isShow && (
        <Toaster
          types={types}
          isShow={isShow}
          toasterBody={toasterBody}
          message={message}
        />
      )}
      <main className="page-wrapper profile_page_wrap">
        <div className="content-overlay">
          <div className="d-flex align-items-center justify-content-start mb-4">
            <h3 className="text-white mb-0">Upload an Office Document</h3>
          </div>
          <div className="row">
            <div className="col-md-8">
              <div className="bg-light rounded-3 border p-3">
                {/* <hr className="mt-2" /> */}
                {/* <button className="btn btn-primary pull-right ms-5 mt-2" type="button" onClick={handleSubmit}>Add Page Content Now</button> */}
                <div className="">
                  {/* <hr className="mt-5" /> */}
                  <div className="add_listing">
                    <div className="">
                      <div className="">
                        <div className="col-sm-12">
                          <label className="form-label">
                            Which document section should these file(s) be
                            added?
                          </label>
                          <div className="d-flex">
                            <p className="mb-0">
                              {showByTitle ? showByTitle : ""}&nbsp;&nbsp;
                              <NavLink>
                                <span
                                  type="button"
                                  data-toggle="modal"
                                  data-target="#addSection"
                                >
                                  change
                                </span>
                              </NavLink>
                            </p>
                          </div>
                        </div>
                        <div className="col-sm-12 mt-3">
                          <label className="form-label">
                            Which office level should the file(s) be published
                            to?
                          </label>
                          <div className="d-flex">
                            <p className="mb-0">
                              {mainNavLinkText}&nbsp;&nbsp;
                              <NavLink>
                                <span
                                  type="button"
                                  data-toggle="modal"
                                  data-target="#addLocation"
                                >
                                  change
                                </span>
                              </NavLink>
                            </p>
                          </div>
                          <p>
                            Ready to upload and publish the files? You can
                            upload up to 100 files at a time.
                          </p>
                          <p>0 files (0 KB)</p>
                        </div>
                        <div className="col-sm-12 mt-3">
                          <label className="form-label">Drop files here.</label>
                          <input
                            className="file-uploader file-uploader-grid"
                            id="inline-form-input"
                            type="file"
                            name="image"
                            onChange={handleFileUpload}
                          />
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div className="pull-right mt-3">
                <button
                  className="btn btn-secondary pull-right mt-2 ms-2"
                  type="button"
                  onClick={handleBack}
                >
                  Cancel
                </button>
                {localStorage.getItem("auth") &&
                  jwt(localStorage.getItem("auth")).contactType === "admin" &&
                  params.id && (
                    <button
                      className="btn btn-secondary pull-right mt-2"
                      type="button"
                      onClick={(e) => handleRemove()}
                    >
                      Remove
                    </button>
                  )}
              </div>
            </div>
            <div className="col-md-4"></div>
          </div>
        </div>
      </main>

      <div className="modal in" role="dialog" id="addSection">
        <div
          className="modal-dialog modal-dialog-centered modal-dialog-scrollable"
          role="document"
        >
          <div className="modal-content">
            <div className="modal-header border-0 pb-0">
            
              <button
                id="closeModal"
                className="btn-close"
                type="button"
                data-dismiss="modal"
                aria-label="Close"
              ></button>
            </div>
            <div className="modal-body fs-sm pt-0">
              <div className="content-overlay">
                <div className="bg-light">
                  <h6>Choose a section from the tree. </h6>
                  <div>
                    {documentGet.data && documentGet.data.length > 0 ? (
                      <NestedArray
                        items={documentGet.data}
                        handleDataClick={handleDataClick}
                      />
                    ) : null}
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div className="modal in" role="dialog" id="addLocation">
        <div
          className="modal-dialog modal-dialog-centered modal-dialog-scrollable"
          role="document"
        >
          <div className="modal-content">
            <div className="modal-header border-0 pb-0">
              {/* <h4 className="modal-title"></h4> */}
              <button
                className="btn-close"
                type="button"
                data-dismiss="modal"
                aria-label="Close"
              ></button>
            </div>
            <div className="modal-body fs-sm pt-0">
              <div className="content-overlay">
                <div className="bg-light">
                  <p>Choose a location.</p>
                  <p>Choose the location from the tree.</p>
                  <div className="pull-right">
                    <NavLink className="btn btn-sm btn-primary">
                      <span
                        onClick={(e) => handleMainNavLinkClick(e, "Corporate")}
                      >
                        Corporate
                      </span>
                    </NavLink>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
export default AddAttachDocument;

function NestedArray({ items, handleDataClick }) {
  return (
    <ul>
      {items.map((item) => (
        <li key={item._id}>
          <div
            className="mb-4 ms-2"
            onClick={() => handleDataClick(item.title, item._id)}
          >
            <NavLink>
              <p className="mt-4">{item.title}</p>
            </NavLink>
          </div>

          {item.children && item.children.length > 0 && (
            <NestedArray
              items={item.children}
              handleDataClick={handleDataClick}
            />
          )}

          {/* {item.content && item.content.length > 0 && (
                                <ul>
                                    {item.content.map((contentItem) => (
                                        <li key={contentItem._id}>
                                            <div className="mb-4 ms-2" onClick={() => handleDataClick(contentItem.title, contentItem._id)}>
                                                <NavLink>
                                                    <p className="mt-4 ms-5">{contentItem.title}</p>
                                                </NavLink>
                                            </div>
                                        </li>
                                    ))}
                                </ul>
                            )} */}
        </li>
      ))}
    </ul>
  );
}

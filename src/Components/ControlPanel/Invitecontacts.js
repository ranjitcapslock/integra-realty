import React, { useState, useEffect, useRef } from "react";
import Pic from "../img/pic.png";
import Modal from "../../Modal";
import user_service from "../service/user_service";
import ReactPaginate from "react-paginate";
import Loader from "../../Pages/Loader/Loader.js";
import Toaster from "../../Pages/Toaster/Toaster.js";
import { NavLink, useNavigate } from "react-router-dom";
import jwt from "jwt-decode";
import { Editor } from "react-draft-wysiwyg";
import { EditorState, ContentState, convertToRaw } from "draft-js";
import draftToHtml from "draftjs-to-html";
import htmlToDraft from "html-to-draftjs";
import "react-draft-wysiwyg/dist/react-draft-wysiwyg.css";

function Invitecontacts() {
  const initialValues = {
    subject: "",
    message: "",
  };

  const [formValues, setFormValues] = useState(initialValues);

  const [selectedContacts, setSelectedContacts] = useState({});

  const [getContact, setGetContact] = useState([]);
  const [pageCount, setPageCount] = useState(0);
  const [totalRecords, setTotalRecords] = useState("");
  const [loader, setLoader] = useState({ isActive: null });
  const { isActive } = loader;
  const [toaster, setToaster] = useState({
    types: null,
    isShow: null,
    toasterBody: "",
    message: "",
  });
  const { types, isShow, toasterBody, message } = toaster;

  const [notesmessage, setNotesmessage] = useState("");
  const editorRef = useRef(null);
  const log = () => {
    if (editorRef.current) {
      setNotesmessage(editorRef.current.getContent());
    }
  };


  const [query, setQuery] = useState("");
  const [contactType, setContactType] = useState("");
  const [select, setSelect] = useState("");
  const [results, setResults] = useState([]);
  const [currentPage, setCurrentPage] = useState(1);
  const [isLoading, setIsLoading] = useState(false);

  const [isSubmitClick, setISSubmitClick] = useState(false);

  // paginate Function
  useEffect(() => {
    window.scrollTo(0, 0);
    const ContactGetAll = async () => {
      setLoader({ isActive: true });
      await user_service.contactGet(1).then((response) => {
        setLoader({ isActive: false });
        if (response) {
          setGetContact(response.data.data);
          setPageCount(Math.ceil(response.data.totalRecords / 10));
          setTotalRecords(response.data.totalRecords);
        }
      });
    };
    ContactGetAll();
  }, []);

  const toggleContactSelection = (id) => {
    setSelectedContacts((prevState) => ({
      ...prevState,
      [id]: !prevState[id], // Toggle the selection status
    }));
  };

  const checkAllContacts = () => {
    const allSelected = Object.values(selectedContacts).every(
      (isSelected) => isSelected
    );

    const updatedSelectedContacts = {};
    getContact.forEach((contact) => {
      updatedSelectedContacts[contact._id] = !allSelected;
    });

    setSelectedContacts(updatedSelectedContacts);
  };

  const getSelectedContactIds = () => {
    const selectedIds = Object.keys(selectedContacts).filter(
      (id) => selectedContacts[id]
    );
    return selectedIds;
  };

  // 2. Function to invite selected contacts
  const handleInviteSelectedContacts = async () => {
    const selectedIds = getSelectedContactIds();
    if (selectedIds.length === 0) {
      // No contacts selected, show a message or take appropriate action
      setLoader({ isActive: false });
      setToaster({
        types: "error",
        isShow: true,
        toasterBody: "No contact selected, Please select any contact.",
        message: "Error",
      });
      setTimeout(() => {
        setToaster((prevToaster) => ({ ...prevToaster, isShow: false }));
        // navigate("/control-panel/documents/");
      }, 2000);
    }

    // setLoader({ isActive: true });
    const userData = {
      agentId: jwt(localStorage.getItem("auth")).id,
      ids: selectedIds, // Pass the array of selected contact IDs
    };

    try {
      const response = await user_service.invitecontact(userData);
      if (response) {
        setLoader({ isActive: false });
        setToaster({
          type: "Invitation",
          isShow: true,
          toasterBody: response.data.message,
          message: "Invitation Send Successfully",
        });
        setTimeout(() => {
          setToaster((prevToaster) => ({ ...prevToaster, isShow: false }));
        }, 2000);
      } else {
        setLoader({ isActive: false });
        setToaster({
          types: "error",
          isShow: true,
          toasterBody: response.data.message,
          message: "Error",
        });
      }
    } catch (error) {
      // Handle error appropriately
      console.error("Error sending invitations:", error);
      setLoader({ isActive: false });
    }
  };

  const handlePageClick = async (data) => {
    let currentPage = data.selected + 1;
    let skip = currentPage - 1 + currentPage;
    setLoader({ isActive: true });
    // console.log(currentPage);
    // await user_service.contactGet(currentPage).then((response) => {
    await user_service
      .SearchContactfilter(currentPage, query, contactType)
      .then((response) => {
        setLoader({ isActive: false });
        if (response) {
          // setResults(response.data.data);
          setGetContact(response.data.data);
          setPageCount(Math.ceil(response.data.totalRecords / 10));
          setTotalRecords(response.data.totalRecords);
        }
      });
  };

  {
    /* <!-- paginate Function Search Api call Start--> */
  }
  useEffect(() => {
    window.scrollTo(0, 0);
    if (query || contactType) {
      SearchGetAll(1);
    }
  }, [query, contactType]);

  const SearchGetAll = async () => {
    setIsLoading(true);
    await user_service
      .SearchContactfilter(1, query, contactType)
      .then((response) => {
        setIsLoading(false);
        if (response) {
          // setResults(response.data.data);
          setGetContact(response.data.data);
          setPageCount(Math.ceil(response.data.totalRecords / 10));
          setTotalRecords(response.data.totalRecords);
        }
      });
  };

  const navigate = useNavigate();

  const handleSubmit = (id) => {
    //console.log(id);
    navigate(`/contact-profile/${id}`);
  };



  const handleSearch = (event) => {
    event.preventDefault();
    setCurrentPage(1);
    SearchGetAll(1);
  };

  const handleinvite = async (e, id) => {
    e.preventDefault();
    const ids = [id];
    const userData = {
      agentId: jwt(localStorage.getItem("auth")).id,
      subject: formValues.subject,
      message: formValues.message,
      ids: ids,
    };
    setLoader({ isActive: true });
    await user_service.invitecontact(userData).then((response) => {
      if (response) {
        setLoader({ isActive: false });
        setToaster({
          type: "Invitation",
          isShow: true,
          toasterBody: response.data.message,
          message: "Invitation Send Successfully",
        });
        setTimeout(() => {
          setToaster((prevToaster) => ({ ...prevToaster, isShow: false }));
        }, 2000);
      } else {
        setLoader({ isActive: false });
        setToaster({
          types: "error",
          isShow: true,
          toasterBody: response.data.message,
          message: "Error",
        });
      }
    });
  };

  const [editorState, setEditorState] = useState(EditorState.createEmpty());

  const handleChanges = (editorState) => {
    setEditorState(editorState);
    const htmlContent = draftToHtml(convertToRaw(editorState.getCurrentContent()));
    setFormValues({ ...formValues, message: htmlContent });
  };

  // const handleChanges = (e) => {
  //   setFormValues({ ...formValues, message: e });
  // };

  const handleChange = (e) => {
    const { name, value } = e.target;
    setFormValues({ ...formValues, [name]: value });
  };


  return (
    <div className="bg-secondary float-left w-100 pt-4 mb-4">
      <Loader isActive={isActive} />
      {isShow && (
        <Toaster
          types={types}
          isShow={isShow}
          toasterBody={toasterBody}
          message={message}
        />
      )}
      <main className="page-wrapper">
        <Modal />
        {/* <!-- Page container--> */}
          {/* <!-- Breadcrumb--> */}
          {/* <nav className="mb-3 mb-md-4 pt-md-3" aria-label="Breadcrumb">
            <ol className="breadcrumb breadcrumb-dark">
              <li className="breadcrumb-item">
                <a href="/">Home</a>
              </li>
              <li className="breadcrumb-item">
                <a href="/">Control Panel</a>
              </li>
              <li className="breadcrumb-item active" aria-current="page">
                Invite Contact
              </li>
            </ol>
          </nav> */}

          {/* <!-- Page card like wrapper--> */}
          <div className="">
            {/* <!-- List of resumes--> */}
              <div className="d-flex align-items-center justify-content-start mb-4">
            <h3 className="mb-0 text-white">Contact</h3>
              </div>
              <p className="text-white w-100">
                Compose your email message here. Use this space to craft your
                message, add links, images, and personalize it before sending
                it to your recipients. Remember to proofread and test before
                hitting send!
              </p>
              <div className="bg-light rounded-3 border p-3 mb-3">
                <label className="form-label">
                  Subject — Please enter a unique, descriptive subject.
                </label>
                {/* <span className="pull-right">
                  Uniquely identifies product. (Max length: 100 chars)</span> */}
                <input
                  className="form-control"
                  id="inline-form-input"
                  type="text"
                  name="subject"
                  value={formValues.subject}
                  onChange={handleChange}/>

                <div className="col-md-12 mt-3">
                  <label className="form-label">Message</label>
                       <Editor
                            editorState={editorState}
                            onEditorStateChange={handleChanges}
                            value={formValues.message}
                          toolbar={{
                            options: [
                              "inline",
                              "blockType",
                              "fontSize",
                              "list",
                              "textAlign",
                              "history",
                              "link", // Add link option here
                            ],
                            inline: {
                              options: [
                                "bold",
                                "italic",
                                "underline",
                                "strikethrough",
                              ],
                            },
                            list: { options: ["unordered", "ordered"] },
                            textAlign: {
                              options: ["left", "center", "right"],
                            },
                            history: { options: ["undo", "redo"] },
                            link: {
                              // Configure link options
                              options: ["link", "unlink"],
                            },
                          }}
                          wrapperClassName="demo-wrapper"
                          editorClassName="demo-editor"
                        />
                </div>
                 <div className="col-md-12 mt-3">
                <form className="float-left w-100" onSubmit={handleSearch}>
                  <p className="mt-4">
                    Quick Search: <a href="">Show All</a>
                  </p>
                  <div className="row">
                  <div className="col-lg-6 col-md-6 col-sm-6 col-6">
                    <input
                      className="form-control"
                      type="text"
                      id="search-input-1"
                      placeholder="Search All"
                      valvalue={query}
                      onChange={(event) => setQuery(event.target.value)}
                    />
                  </div>
                  <div className="col-lg-6 col-md-6 col-sm-6 col-6">
                    <select
                      className="form-select"
                      id="pr-city"
                      name="contactType"
                      onChange={(event) => setContactType(event.target.value)}
                      value={contactType}
                    >
                      <option value="all">Search All Available</option>
                      <option selected="selected" value="associate">
                        Associates &amp; Staff
                      </option>
                      <option value="private">Personal</option>
                      <option value="community">Community</option>
                    </select>
                  </div>
                      
                  <div className="col-md-12 mt-3">
                    <span className="float-left w-100">
                      <b>Total Matches : {totalRecords}</b>
                    </span>
                  </div>
                  </div>
                </form>
                </div>
              
             </div>
                 <div className="bg-light rounded-3 border p-3 mb-3 float-left w-100">
                <div className="float-left w-100 pull-right mt-3 mb-4">
                  <button
                    className="btn btn-primary"
                    onClick={checkAllContacts}>
                    Check All
                  </button>
                  <button
                    className="btn btn-primary ms-3"
                    onClick={handleInviteSelectedContacts}>
                    Invite
                  </button>
                </div>
                <div className="invite_contacts float-left w-100">
                  {/* <!-- Item--> */}
                  {getContact.map((post) => (
                    <div className="card card-hover mb-2 mt-3">
                      <div className="card-body">
                        <div className="float-left w-100">
                          <div className="float-left w-100 d-flex align-items-center justify-content-between">
                            {/* <img className="d-none d-sm-block" src={Pic} width="100" alt="Resume picture" /> */}
                            <input
                              className="checkinvite m-4"
                              type="checkbox"
                              checked={selectedContacts[post._id] || false}
                              onChange={() => toggleContactSelection(post._id)}
                            />
                            <div
                              className="float-left w-100 d-flex align-items-center justify-content-start"
                              key={post._id}
                            >
                              <img
                                className="rounded-circle profile_picture"
                                src={post.image || Pic}
                                alt="Profile"
                              />

                              <div
                                className="ms-3"
                                onClick={() => handleSubmit(post._id)}
                              >
                                <strong>{post.firstName}</strong>
                                <strong>{post.lastName}</strong>
                                <p className="mb-1">{post.phone}</p>
                                <p className="mb-0">
                                  <NavLink>
                                    <i
                                      className="fa fa-envelope"
                                      aria-hidden="true"
                                    >
                                      <span className="ms-1">{post.email}</span>
                                    </i>
                                  </NavLink>
                                </p>
                              </div>
                            </div>

                            {/* <button className=" d-flex  justify-content-center btn btn-success" style={{ marginLeft: "500px", marginTop: "-48px", fontSize: "10px" }}>Vendor</button> */}
                            <span className="pull-right tags-badge d-flex align-items-center justify-content-end w-100">
                              <badge className="" style={{ fontSize: "14px" }}>
                                {post.contactType
                                  ? post.contactType
                                  : "Not Assigned"}
                              </badge>
                            </span>
                            <div className="d-flex align-items-center justify-content-end w-100">
                              <span>
                                <button
                                  className="btn btn-primary pull-right ms-5 mt-2"
                                  type="button"
                                  onClick={(e) => handleinvite(e, post._id)}
                                >
                                  Invite
                                </button>
                              </span>
                            </div>
                            <div></div>
                          </div>
                        </div>
                      </div>
                    </div>
                  ))}
                </div>
              </div>
          {/* <div className="col-md-4">
                </div> */}
          <div className="pull-right mt-3">
            <ReactPaginate
              className=""
              previousLabel={"Previous"}
              nextLabel={"next"}
              breakLabel={"..."}
              pageCount={pageCount}
              marginPagesDisplayed={1}
              pageRangeDisplayed={2}
              onPageChange={handlePageClick}
              containerClassName={
                "pagination justify-content-center mb-0"
              }
              pageClassName={"page-item"}
              pageLinkClassName={"page-link"}
              previousClassName={"page-item"}
              previousLinkClassName={"page-link"}
              nextClassName={"page-item"}
              nextLinkClassName={"page-link"}
              breakClassName={"page-item"}
              breakLinkClassName={"page-link"}
              activeClassName={"active"}
            />
          </div>
            </div>
                {/* <!-- Item--> */}
      </main>
    </div>
  );
}
export default Invitecontacts;

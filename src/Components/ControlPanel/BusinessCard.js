import React, { useState, useEffect } from "react";
import _ from "lodash";
import user_service from "../service/user_service";
import axios from "axios";
// import CoverImage from "../img/Capture.jpg";
import Pic from "../img/pic.png";

import Loader from "../../Pages/Loader/Loader.js";
import Toaster from "../../Pages/Toaster/Toaster.js";
import { a, useNavigate, useParams, NavLink } from "react-router-dom";
import PagesNavigationUI from "../../Pages/PagesNavigationUI.js";

import jwt from "jwt-decode";

function BusinessCard() {
  const navigate = useNavigate();
  const [loader, setLoader] = useState({ isActive: null });
  const { isActive } = loader;
  const [toaster, setToaster] = useState({
    types: null,
    isShow: null,
    toasterBody: "",
    message: "",
  });
  const { types, isShow, toasterBody, message } = toaster;
  const params = useParams();
  console.log(params.cardId);
  const [businessCard, setBusinessCard] = useState("");
  const [getContact, setGetContact] = useState("");

  useEffect(() => {
    const ContactGetById = async () => {
      await user_service.contactGetByNickname(params.id).then((response) => {
        if (response) {
          setBusinessCard(response.data.data[0]);

          if (
            response.data.data[0] &&
            response.data.data[0].additionalActivateFields
          ) {
            setGetContact((prevFormValues) => ({
              ...prevFormValues,
              office:
                response.data.data[0].additionalActivateFields.office ||
                prevFormValues.office,
              //   intranet_id: response.data.data[0].additionalActivateFields.intranet_id || prevFormValues.intranet_id,
              intranet_id:
                response.data.data[0].additionalActivateFields.intranet_id ||
                response.data.data[0].email,
              subscription_level:
                response.data.data[0].additionalActivateFields
                  .subscription_level || prevFormValues.subscription_level,
              admin_note:
                response.data.data[0].additionalActivateFields.admin_note ||
                prevFormValues.admin_note,
              sponsor_associate:
                response.data.data[0].additionalActivateFields
                  .sponsor_associate || prevFormValues.sponsor_associate,
              staff_recruiter:
                response.data.data[0].additionalActivateFields
                  .staff_recruiter || prevFormValues.staff_recruiter,
              nrds_id:
                response.data.data[0].additionalActivateFields.nrds_id ||
                prevFormValues.nrds_id,
              associate_member:
                response.data.data[0].additionalActivateFields
                  .associate_member || prevFormValues.associate_member,
              plan_date:
                response.data.data[0].additionalActivateFields.plan_date ||
                prevFormValues.plan_date,
              billing_date:
                response.data.data[0].additionalActivateFields.billing_date ||
                prevFormValues.billing_date,
              account_name:
                response.data.data[0].additionalActivateFields.account_name ||
                prevFormValues.account_name,
              invoicing:
                response.data.data[0].additionalActivateFields.invoicing ||
                prevFormValues.invoicing,
              licensed_since:
                response.data.data[0].additionalActivateFields.licensed_since ||
                prevFormValues.licensed_since,
              associate_since:
                response.data.data[0].additionalActivateFields
                  .associate_since || prevFormValues.associate_since,
              staff_since:
                response.data.data[0].additionalActivateFields.staff_since ||
                prevFormValues.staff_since,
              iscorporation:
                response.data.data[0].additionalActivateFields.iscorporation ||
                prevFormValues.iscorporation,
              agentTax_ein:
                response.data.data[0].additionalActivateFields.agentTax_ein ||
                prevFormValues.agentTax_ein,
              agent_funding:
                response.data.data[0].additionalActivateFields.agent_funding ||
                prevFormValues.agent_funding,
              ssn_itin:
                response.data.data[0].additionalActivateFields.ssn_itin ||
                prevFormValues.ssn_itin,
              date_birth:
                response.data.data[0].additionalActivateFields.date_birth ||
                prevFormValues.date_birth,
              private_idcard_type:
                response.data.data[0].additionalActivateFields
                  .private_idcard_type || prevFormValues.private_idcard_type,
              private_idstate:
                response.data.data[0].additionalActivateFields
                  .private_idstate || prevFormValues.private_idstate,
              private_idnumber:
                response.data.data[0].additionalActivateFields
                  .private_idnumber || prevFormValues.private_idnumber,
              license_state:
                response.data.data[0].additionalActivateFields.license_state ||
                prevFormValues.license_state,
              license_type:
                response.data.data[0].additionalActivateFields.license_type ||
                prevFormValues.license_type,
              license_number:
                response.data.data[0].additionalActivateFields.license_number ||
                prevFormValues.license_number,
              date_issued:
                response.data.data[0].additionalActivateFields.date_issued ||
                prevFormValues.date_issued,
              date_expire:
                response.data.data[0].additionalActivateFields.date_expire ||
                prevFormValues.date_expire,
              office_affiliation:
                response.data.data[0].additionalActivateFields
                  .office_affiliation || prevFormValues.office_affiliation,

              agent_id:
                response.data.data[0].additionalActivateFields.agent_id ||
                prevFormValues.agent_id,
            }));

            // const mls_membershipString =
            //   response.data.additionalActivateFields.mls_membership;

            // if (mls_membershipString) {
            //   // Check if mls_membershipString is a string
            //   if (typeof mls_membershipString === "string") {
            //     try {
            //       // Attempt to parse the string as JSON
            //       const mls_membershipParsed = JSON.parse(mls_membershipString);
            //       // Set the parsed value to your state or variable
            //       if (mls_membershipParsed == "") {
            //         setMls_membership(mls_membership);
            //       } else {
            //         setMls_membership(mls_membershipParsed);
            //       }
            //     } catch (error) {
            //       // Handle the error if parsing fails
            //       console.error("Error parsing mls_membership as JSON:", error);
            //       // You might want to set a default value or handle the error in another way
            //       // For debugging, you can log the JSON parse error message:
            //       console.error("JSON Parse Error:", error.message);
            //       // Set a default value if parsing fails
            //       setMls_membership(mls_membership); // Replace "defaultValue" with your default value
            //     }
            //   } else {
            //     // Handle the case where mls_membershipString is not a string (e.g., it's already an object)
            //     // You can directly set it to your state or variable
            //     setMls_membership(mls_membership);
            //   }
            // } else {

            //   setMls_membership(mls_membership);
            // }

            // const board_membershipString =
            //   response.data.additionalActivateFields.board_membership;
            // if (board_membershipString) {
            //   // Check if board_membershipString is a string
            //   if (typeof board_membershipString === "string") {
            //     try {
            //       // Attempt to parse the string as JSON
            //       const board_membershipParsed = JSON.parse(
            //         board_membershipString
            //       );
            //       // Set the parsed value to your state or variable
            //       if (board_membershipParsed == "") {
            //         setBoard_membership(board_membership);
            //       } else {
            //         setBoard_membership(board_membershipParsed);
            //       }
            //     } catch (error) {
            //       // Handle the error if parsing fails
            //       console.error(
            //         "Error parsing board_membership as JSON:",
            //         error
            //       );
            //       // You might want to set a default value or handle the error in another way
            //       // For debugging, you can log the JSON parse error message:
            //       console.error("JSON Parse Error:", error.message);
            //       // Set a default value if parsing fails
            //       setBoard_membership(board_membership); // Replace "defaultValue" with your default value
            //     }
            //   } else {
            //     console.log("sdfdsfdsfsd");
            //     // Handle the case where board_membershipString is not a string (e.g., it's already an object)
            //     // You can directly set it to your state or variable
            //     setBoard_membership(board_membership);
            //   }
            // } else {

            //   setBoard_membership(board_membership);
            // }
          }
        }
      });
    };
    ContactGetById(params.id);
  }, []);

  const generateVCard = (contactInfo) => {
    const socialLinks = contactInfo?.social_links || [];

    // Ensure that both type and linkurl are present for each social link
    const socialLinksString = socialLinks
      .filter((item) => item.type && item.linkurl)
      .map((item) => `${item.type}:${encodeURIComponent(item.linkurl)}`)
      .join("\n");

    const vCardData = `BEGIN:VCARD
VERSION:3.0
FN:${contactInfo?.firstName} ${contactInfo?.lastName}
EMAIL:${contactInfo?.email}
TEL:${contactInfo?.phone ?? ""}
ORG:${contactInfo?.appointment ?? ""}
URL:${contactInfo?.web ?? ""}
ADR:${contactInfo?.location ?? ""}
SOCIAL:${socialLinksString ?? ""}
END:VCARD`;

    // Download the vCard file
    const blob = new Blob([vCardData], { type: "text/vcard;charset=utf-8" });
    const link = document.createElement("a");
    link.href = window.URL.createObjectURL(blob);
    link.setAttribute(
      "download",
      `${contactInfo.firstName}-${contactInfo.lastName}.vcf`
    );

    document.body.appendChild(link);
    link.click();
    document.body.removeChild(link);
  };

  const handleContactInfo = () => {
    const contactInfo = businessCard;
    if (contactInfo) {
      generateVCard(contactInfo);
    }
  };

  const [copySuccessMap, setCopySuccessMap] = useState({});
  const handleCopyUrl = (contentId, resource_url) => {
    navigator.clipboard
      .writeText(resource_url)
      .then(() => {
        console.log("URL copied to clipboard");
        setCopySuccessMap((prevMap) => ({
          ...prevMap,
          [contentId]: true,
        }));

        // Reset the copy success message after a short delay
        setTimeout(() => {
          setCopySuccessMap((prevMap) => ({
            ...prevMap,
            [contentId]: false,
          }));
        }, 2000);
      })
      .catch((error) => {
        console.error("Error copying URL to clipboard", error);
        // Handle the error or show an error message to the user
      });
  };


  const [formData, setFormData] = useState([]);

  const StaffRoleGetData = async () => {
    await user_service.StaffRoleGet().then((response) => {
      if (response) {
        console.log(response);
        setFormData(response.data.data);
      }
    });
  };
  useEffect(() => {
    if (formData) {
      StaffRoleGetData();
    }
  }, []);
  return (
    <div className="right_sidebar_innerlayout">
      <div className="bg-secondary float-left w-100 pt-4 mb-0">
        <Loader isActive={isActive} />
        {isShow && (
          <Toaster
            types={types}
            isShow={isShow}
            toasterBody={toasterBody}
            message={message}
          />
        )}
        <main className="page-wrapper">
          <div className="container">
            <div className="bg-light">
              {localStorage.getItem("auth") &&
              (jwt(localStorage.getItem("auth")).contactType == "admin" ||
                jwt(localStorage.getItem("auth")).id == businessCard?._id) ? (
                <>
                  <div className="d-lg-flex d-md-flex d-sm-flex align-items-center justify-content-between mb-4">
                    <h3 className="float-lg-start float-md-start float-sm-start float-start mb-lg-0 mb-md-0 mb-sm-3 mb-3">
                      Business Card
                    </h3>
                    {/* {console.log(window.location.href)} */}
                    <span className="float-lg-end float-md-end float-sm-start float-start mb-lg-0 mb-md-0 mb-sm-3 mb-3">
                      <button
                        className="btn btn-info"
                        type="button"
                        onClick={() =>
                          handleCopyUrl(businessCard?._id, window.location.href)
                        }
                      >
                        Share &nbsp; <i className="fa fa-clone"></i>
                        {copySuccessMap[businessCard?._id] && (
                          <i className="fi-check text-success ms-2"></i>
                        )}
                      </button>
                      <NavLink
                        to={`/contact-profile/${businessCard?._id}`}
                        type="button"
                        className="btn btn-info ms-3"
                      >
                        Back
                      </NavLink>
                      <NavLink
                        to={`/edit-businessCard/${businessCard?._id}`}
                        type="button"
                        className="btn btn-info ms-3"
                      >
                        Edit Business Card
                      </NavLink>
                    </span>
                  </div>
                  {/* <hr className="my-3 w-100" /> */}
                </>
              ) : (
                <>
                  {localStorage.getItem("auth") &&
                    jwt(localStorage.getItem("auth")).contactType == "staff" &&
                    (Array.isArray(formData) && formData.length > 0
                      ? formData.map((item) =>
                          item.roleStaff === "add_contact" ? (
                            <div className="d-lg-flex d-md-flex d-sm-flex align-items-center justify-content-between mb-4">
                              <h3 className="float-lg-start float-md-start float-sm-start float-start mb-lg-0 mb-md-0 mb-sm-3 mb-3">
                                Business Card
                              </h3>
                              {/* {console.log(window.location.href)} */}
                              <span className="float-lg-end float-md-end float-sm-start float-start mb-lg-0 mb-md-0 mb-sm-3 mb-3">
                                <button
                                  className="btn btn-info"
                                  type="button"
                                  onClick={() =>
                                    handleCopyUrl(
                                      businessCard?._id,
                                      window.location.href
                                    )
                                  }
                                >
                                  Share &nbsp; <i className="fa fa-clone"></i>
                                  {copySuccessMap[businessCard?._id] && (
                                    <i className="fi-check text-success ms-2"></i>
                                  )}
                                </button>
                                <NavLink
                                  to={`/contact-profile/${businessCard?._id}`}
                                  type="button"
                                  className="btn btn-info ms-3"
                                >
                                  Back
                                </NavLink>
                                <NavLink
                                  to={`/edit-businessCard/${businessCard?._id}`}
                                  type="button"
                                  className="btn btn-info ms-3"
                                >
                                  Edit Business Card
                                </NavLink>
                              </span>
                            </div>
                          ) : (
                            ""
                          )
                        )
                      : "")}
                </>
              )}
              {/* <h4 className="ms-3"></h4>
                            <hr /> */}
              <div className="bg-light border-0 rounded-3 p-3 mt-0 float-start w-100">
                <div className="add_listing w-100">
                  <div className="row mt-0">
                    {/* <PagesNavigationUI />  */}
                    <div className="p-0">
                      <img
                        className="blogImage"
                        src={businessCard?.cover_image}
                      />
                      <div className="contactProfile businesscard text-center">
                        <img
                          className="rounded-circle getContact_picture text-center"
                          src={businessCard?.image || Pic}
                          alt="image"
                        />
                        <h4 className="text-center mt-2 mb-2">
                          {businessCard?.firstName ?? ""}&nbsp;{" "}
                          {businessCard?.lastName ?? ""}
                        </h4>

                        <h6 className="user_designation text-center mb-2">
                          {businessCard?.designations
                            ? businessCard?.designations
                            : "Realtor/ Associate"}
                        </h6>
                        <p className="text-center mb-0">In Depth Realty</p>
                      </div>
                    </div>
                    <div className="row row-cols-lg-6 row-cols-sm-3 row-cols-2 g-3 g-xl-4 mt-0">
                      <div className="float-left w-100 mt-0">
                        <h6 className="text-left">Contact Links</h6>
                      </div>

                      {businessCard?.phone ? (
                        <>
                          <div className="col">
                            <a
                              className="icon-box card card-body h-100 border-0 shadow-sm card-hover h-100 text-center"
                              // href="tel:"
                              href={`tel:${businessCard?.phone ?? ""}`}
                            >
                              <div className="icon-box-media bg-faded-primary text-primary rounded-circle mb-3 mx-auto">
                                <i
                                  className="fa fa-phone"
                                  aria-hidden="true"
                                ></i>
                              </div>
                              <h3 className="icon-box-title fs-base mb-0">
                                Call
                              </h3>
                            </a>
                          </div>
                        </>
                      ) : (
                        ""
                      )}

                      {businessCard?.email ? (
                        <>
                          <div className="col">
                            <a
                              className="icon-box card card-body h-100 border-0 shadow-sm card-hover h-100 text-center"
                              // href="mailto:"
                              href={`mailto:${businessCard?.email ?? ""}`}
                            >
                              <div className="icon-box-media bg-faded-primary text-primary rounded-circle mb-3 mx-auto">
                                <i className="fa fa-at" aria-hidden="true"></i>
                              </div>
                              <h3 className="icon-box-title fs-base mb-0">
                                Email
                              </h3>
                            </a>
                          </div>
                        </>
                      ) : (
                        ""
                      )}

                      {businessCard?.phone ? (
                        <>
                          <div className="col">
                            <a
                              className="icon-box card card-body h-100 border-0 shadow-sm card-hover h-100 text-center"
                              // href="sms:"
                              href={`sms:${businessCard?.phone ?? ""}`}
                            >
                              <div className="icon-box-media bg-faded-primary text-primary rounded-circle mb-3 mx-auto">
                                <i
                                  className="fa fa-file-text"
                                  aria-hidden="true"
                                ></i>
                              </div>
                              <h3 className="icon-box-title fs-base mb-0">
                                Text
                              </h3>
                            </a>
                          </div>
                        </>
                      ) : (
                        ""
                      )}

                      <div className="col">
                        <a
                          className="icon-box card card-body h-100 border-0 shadow-sm card-hover h-100 text-center"
                          href="https://calendly.com/chatwithintegrarealty"
                          target="_blank"
                          rel="noopener noreferrer"
                        >
                          <div className="icon-box-media bg-faded-primary text-primary rounded-circle mb-3 mx-auto">
                            <i
                              className="fa fa-calendar"
                              aria-hidden="true"
                            ></i>
                          </div>
                          <h3 className="icon-box-title fs-base mb-0">
                            Appointment
                          </h3>
                        </a>
                      </div>

                      {businessCard?.web ? (
                        <>
                          <div className="col">
                            <a
                              className="icon-box card card-body h-100 border-0 shadow-sm card-hover h-100 text-center"
                              href={businessCard?.web ?? ""}
                              target="_blank"
                              rel="noopener noreferrer"
                            >
                              <div className="icon-box-media bg-faded-primary text-primary rounded-circle mb-3 mx-auto">
                                <i
                                  className="fa fa-globe"
                                  aria-hidden="true"
                                ></i>
                              </div>
                              <h3 className="icon-box-title fs-base mb-0">
                                Website
                              </h3>
                            </a>
                          </div>
                        </>
                      ) : (
                        ""
                      )}

                      <div className="col">
                        <a
                          className="icon-box card card-body h-100 border-0 shadow-sm card-hover h-100 text-center"
                          href="https://goodlifeutah.com/integraoffices-copy"
                          target="_blank"
                          rel="noopener noreferrer"
                        >
                          <div className="icon-box-media bg-faded-primary text-primary rounded-circle mb-3 mx-auto">
                            <i
                              className="fa fa-map-marker"
                              aria-hidden="true"
                            ></i>
                          </div>
                          <h3 className="icon-box-title fs-base mb-0">
                            Location
                          </h3>
                        </a>
                      </div>
                    </div>

                    <div className="text-center mt-5">
                      <button
                        className="btn btn-info btn-lg text-center"
                        type="button"
                        onClick={handleContactInfo}
                      >
                        Save My Contact Info
                      </button>
                    </div>

                    {businessCard && businessCard.social_links && (
                      <div className="row row-cols-lg-6 row-cols-sm-3 row-cols-2 g-3 g-xl-4">
                        <div className="float-left w-100">
                          <h6 className="text-left">Social Links</h6>
                        </div>
                        {businessCard.social_links.map((item, index) => {
                          return item.linkurl ? (
                            <div className="col">
                              <div key={index} className="">
                                <a
                                  className="icon-box card card-body h-100 border-0 shadow-sm card-hover h-100 text-center"
                                  href={item.linkurl}
                                  target="_blank"
                                  rel="noreferrer"
                                >
                                  <div className="icon-box-media bg-faded-primary text-primary rounded-circle mb-3 mx-auto">
                                    {item.type === "Twitter" ? (
                                      <i
                                        className="fa fa-twitter"
                                        aria-hidden="true"
                                      ></i>
                                    ) : item.type === "Linkedin" ? (
                                      <i
                                        className="fa fa-linkedin"
                                        aria-hidden="true"
                                      ></i>
                                    ) : item.type === "Facebook" ? (
                                      <i
                                        className="fa fa-facebook"
                                        aria-hidden="true"
                                      ></i>
                                    ) : item.type === "YouTube Channel" ? (
                                      <i
                                        className="fa fa-youtube-play"
                                        aria-hidden="true"
                                      ></i>
                                    ) : item.type === "Instagram" ? (
                                      <i
                                        className="fa fa-instagram"
                                        aria-hidden="true"
                                      ></i>
                                    ) : item.type === "Tumblr" ? (
                                      <i
                                        className="fa fa-tumblr-square"
                                        aria-hidden="true"
                                      ></i>
                                    ) : item.type === "Pinterest" ? (
                                      <i
                                        className="fa fa-pinterest-square"
                                        aria-hidden="true"
                                      ></i>
                                    ) : item.type === "TikTok" ? (
                                      <i
                                        className="fa fa-tint"
                                        aria-hidden="true"
                                      ></i>
                                    ) : item.type === "Vimeo" ? (
                                      <i
                                        className="fa fa-vimeo-square"
                                        aria-hidden="true"
                                      ></i>
                                    ) : (
                                      ""
                                    )}
                                  </div>
                                  <h3 className="icon-box-title fs-base mb-0">
                                    {item.type}
                                  </h3>
                                </a>
                              </div>
                            </div>
                          ) : (
                            ""
                          );
                        })}
                      </div>
                    )}

                    {businessCard?.about_me ? (
                      <div className="mt-5">
                        <h6 className="text-left">About Me</h6>
                        <p className="">{businessCard.about_me}</p>
                      </div>
                    ) : (
                      ""
                    )}
                  </div>
                </div>
              </div>
            </div>
          </div>
        </main>
      </div>
    </div>
  );
}
export default BusinessCard;

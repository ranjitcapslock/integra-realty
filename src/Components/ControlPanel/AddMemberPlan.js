import React, { useState, useEffect } from "react";
import _ from 'lodash';
import user_service from '../service/user_service';
import jwt from "jwt-decode";
import Loader from "../../Pages/Loader/Loader.js";
import Toaster from "../../Pages/Toaster/Toaster.js";
import { useNavigate, NavLink, useParams } from "react-router-dom";


function AddMemberPlan() {
    const initialValues = {
        office: "", plan_name: "", overdue: "", grace_period: "", termination_days: "", invoice_cycle: "",
        agentId: "",
    };
    const [formValues, setFormValues] = useState(initialValues);
    const [formErrors, setFormErrors] = useState({});
    const [isSubmitClick, setISSubmitClick] = useState(false)

    const [loader, setLoader] = useState({ isActive: null })
    const { isActive } = loader;
    const [toaster, setToaster] = useState({ types: null, isShow: null, toasterBody: "", message: "" });
    const { types, isShow, toasterBody, message } = toaster;

    const [mainNavLinkText, setMainNavLinkText] = useState("InDepth Realty");


    const navigate = useNavigate();
    const params = useParams();

    // onChange Function start
    const handleChange = (e) => {
        const { name, value } = e.target;
        setFormValues({ ...formValues, [name]: value })
    };
    // onChange Function end


    {/* <!-- Form Validation Start--> */ }
    useEffect(() => { 
        if (formValues && isSubmitClick) {
            validate()
        }
    }, [formValues])




    const validate = () => {
        const values = formValues
        const errors = {};

        if (!values.plan_name) {
            errors.plan_name = "MemberPlan name is required.";
        }

        setFormErrors(errors)
        return errors;
    }
    {/* <!-- Form Validation End--> */ }
    const handleSubmit = async (e) => {
        if (params.id) {
            e.preventDefault();
            setISSubmitClick(true)
            let checkValue = validate()
            if (_.isEmpty(checkValue)) {
                const userData = {
                    agentId: jwt(localStorage.getItem('auth')).id,
                    office: mainNavLinkText,
                    plan_name: formValues.plan_name,
                    overdue: formValues.overdue ? formValues.overdue : "30",
                    grace_period: formValues.grace_period ? formValues.grace_period : "60",
                    termination_days: formValues.termination_days ? formValues.termination_days : "90",
                    invoice_cycle: formValues.invoice_cycle ? formValues.invoice_cycle : "MONTHLY",
                    is_active: "Yes",
                };
                setLoader({ isActive: true })
                await user_service.memberPlanUpdate(params.id, userData).then((response) => {
                    if (response) {
                        setFormValues(response.data);
                        setLoader({ isActive: false })
                        setToaster({ type: "MemberPlan Update Successfully", isShow: true, toasterBody: response.data.message, message: "MemberPlan Update Successfully", });
                        setTimeout(() => {
                            setToaster((prevToaster) => ({ ...prevToaster, isShow: false }))
                            navigate("/control-panel/member-plan/");
                        }, 1000);
                    }
                    else {
                        setLoader({ isActive: false })
                        setToaster({ types: "error", isShow: true, toasterBody: response.data.message, message: "Error", });
                    }
                });
            };
        }
        else {
            e.preventDefault();
            setISSubmitClick(true)
            let checkValue = validate()
            if (_.isEmpty(checkValue)) {
                const userData = {
                    agentId: jwt(localStorage.getItem('auth')).id,
                    office: mainNavLinkText,
                    plan_name: formValues.plan_name,
                    overdue: formValues.overdue ? formValues.overdue : "30",
                    grace_period: formValues.grace_period ? formValues.grace_period : "60",
                    termination_days: formValues.termination_days ? formValues.termination_days : "90",
                    invoice_cycle: formValues.invoice_cycle ? formValues.invoice_cycle : "MONTHLY",
                    is_active: "Yes",
                };
                setLoader({ isActive: true })
                await user_service.membershipPlanPost(userData).then((response) => {
                    if (response) {
                        setFormValues(response.data);
                        setLoader({ isActive: false })
                        setToaster({ type: "MemberPlan Add", isShow: true, toasterBody: response.data.message, message: "MemberPlan Add Successfully", });
                        setTimeout(() => {
                            setToaster((prevToaster) => ({ ...prevToaster, isShow: false }))
                            navigate("/control-panel/member-plan/");
                        }, 1000);
                    }
                    else {
                        setLoader({ isActive: false })
                        setToaster({ types: "error", isShow: true, toasterBody: response.data.message, message: "Error", });
                    }
                });
            };
        }
    }

    {/* <!-- Api call Form onSubmit End--> */ }


    const handleBack = () => {
        navigate("/control-panel/member-plan")
    }



    const handleMainNavLinkClick = (e, item) => {
        setMainNavLinkText(item);
        document.getElementById('closeModal').click();
    };

    useEffect(() => { window.scrollTo(0, 0);
        if (params.id) {
            setLoader({ isActive: true });
            const MembershipPlanGetById = async () => {
                await user_service.membershipPlanGetById(params.id).then((response) => {
                    setLoader({ isActive: false })
                    if (response) {
                        const planData = response.data;
                        setFormValues(planData);
                        console.log(planData.office);
                        setMainNavLinkText(planData.office)
                    }
                });
            }
            MembershipPlanGetById(params.id)
        }
    }, [params.id]);


    const handleRemove = async () => {
        setLoader({ isActive: true })
        await user_service.memberPlanDelete(params.id).then((response) => {
            if (response) {

                const MembershipPlan = async () => {
                    await user_service.membershipPlanGet().then((response) => {
                        setLoader({ isActive: false });
                        if (response) {
                            setFormValues(response.data);
                            setToaster({
                                types: "MemberPlan_Delete",
                                isShow: true,
                                toasterBody: response.data.message,
                                message: "MemberPlan_Delete Successfully",
                            });
                            setTimeout(() => {
                                setToaster((prevToaster) => ({ ...prevToaster, isShow: false }))
                                navigate("/control-panel/member-plan/");
                            }, 1000);
                            document.getElementById('removeModal').click();

                        }
                    });
                };

                MembershipPlan();
            }

        });
    }

    return (
        <div className="bg-secondary float-left w-100 pt-4">
            <Loader isActive={isActive} />
            {isShow && <Toaster types={types} isShow={isShow} toasterBody={toasterBody} message={message} />}
            <main className="page-wrapper profile_page_wrap">
                <div className="container content-overlay">
                    <div className="justify-content-center pb-sm-2">
                        <div className="">
                            {
                                params.id ?
                                    <>
                                        <h4>Update Member Plan</h4>
                                        <hr className="" />
                                    </>
                                    : ""
                            }
                            <button className="btn btn-primary pull-right ms-5 mt-2" type="button" onClick={handleSubmit}>{params.id ? "Update MemberPlan" : "Add Member Plan"}</button>
                            <button className="btn btn-secondary pull-right mt-2" type="button" onClick={handleBack}>Cancel & Go Back</button>
                            <div className="bg-light rounded-3 p-4 mb-3">
                                <hr className="mt-5" />

                                <div className="add_listing shadow-sm w-100 m-auto p-4">



                                    {
                                        params.id ?
                                            <div className="row mt-5">

                                                <div className="col-md-6">

                                                    <div className="col-sm-12">
                                                        <label className="form-label">Please enter name of this member plan</label>
                                                        <input className="form-control" id="inline-form-input" type="text"
                                                            name="plan_name"
                                                            value={formValues.plan_name}
                                                            onChange={handleChange}
                                                        />
                                                        {formErrors.plan_name && (
                                                            <div className="invalid-tooltip">{formErrors.plan_name}</div>
                                                        )}
                                                    </div>

                                                    <div className="col-sm-12 mt-3">
                                                        <label className="form-label">When will the account be overdue?</label>
                                                        <select
                                                            className="form-select"
                                                            name="overdue"
                                                            value={formValues.overdue ? formValues.overdue : "30"}
                                                            onChange={handleChange}

                                                        >
                                                            <option value="30">30</option>
                                                            <option value="60">60</option>
                                                            <option value="90">90</option>

                                                        </select>

                                                    </div>

                                                    <div className="col-sm-12 mt-3">
                                                        <label className="form-label">How long is the grace period?</label>
                                                        <select
                                                            className="form-select"
                                                            name="grace_period"
                                                            value={formValues.grace_period ? formValues.grace_period : "60"}
                                                            onChange={handleChange}

                                                        >
                                                            <option value="30">30</option>
                                                            <option value="60">60</option>
                                                            <option value="90">90</option>
                                                        </select>

                                                    </div>


                                                    <div className="col-sm-12 mt-3">
                                                        <label className="form-label">When will the account be terminated?</label>
                                                        <select
                                                            className="form-select"
                                                            name="termination_days"
                                                            value={formValues.termination_days ? formValues.termination_days : "90"}
                                                            onChange={handleChange}

                                                        >
                                                            <option value="30">30</option>
                                                            <option value="60">60</option>
                                                            <option value="90">90</option>
                                                        </select>

                                                    </div>

                                                    <div className="col-sm-12 mt-3">
                                                        <label className="form-label">Please select the invoice cycle</label>
                                                        <select
                                                            className="form-select"
                                                            name="invoice_cycle"
                                                            value={formValues.invoice_cycle ? formValues.invoice_cycle : "MONTHLY"}
                                                            onChange={handleChange}

                                                        >
                                                            <option value="MONTHLY">MONTHLY</option>
                                                            <option value="NONE">NONE</option>
                                                            <option value="QUARTERLY">QUARTERLY</option>
                                                            <option value="YEARLY">YEARLY</option>
                                                        </select>

                                                    </div>
                                                </div>

                                                <div className="col-md-6">
                                                    <p style={{ fontSize: 20 }}>This member plan  belongs to <span className="h6">{mainNavLinkText}</span>. Do you want to add this member plan to different office?</p>
                                                    <p style={{ fontSize: 18 }}>Please select a new office</p>
                                                    <div className="d-flex">
                                                        <p>{mainNavLinkText}&nbsp;&nbsp;<NavLink><span type="button" data-toggle="modal" data-target="#addMemberPlan">change</span></NavLink></p>
                                                    </div>
                                                    <p style={{ fontSize: 18 }}>Do you want to remove this member plan from  <span className="h6">{mainNavLinkText}</span></p>

                                                    <button className="btn btn-primary" type="button" data-toggle="modal" data-target="#removeMemberPlan">Remove Member Plan</button>
                                                </div>
                                            </div>
                                            :
                                            <>
                                                <div className="row mt-5">
                                                    <h6>At what location level is this member plan should be added?</h6>
                                                    <div className="d-flex">
                                                        <p>{mainNavLinkText}&nbsp;&nbsp;<NavLink><span type="button" data-toggle="modal" data-target="#addMemberPlan">change</span></NavLink></p>

                                                    </div>
                                                    <div className="row mt-4">

                                                        <div className="col-sm-12">
                                                            <label className="form-label">Please enter name of this member plan</label>
                                                            <input className="form-control" id="inline-form-input" type="text"
                                                                name="plan_name"
                                                                value={formValues.plan_name}
                                                                onChange={handleChange}
                                                            />
                                                            {formErrors.plan_name && (
                                                                <div className="invalid-tooltip">{formErrors.plan_name}</div>
                                                            )}
                                                        </div>

                                                        <div className="col-sm-12 mt-3">
                                                            <label className="form-label">When will the account be overdue?</label>
                                                            <select
                                                                className="form-select"
                                                                name="overdue"
                                                                value={formValues.overdue ? formValues.overdue : "30"}
                                                                onChange={handleChange}

                                                            >
                                                                <option value="30">30</option>
                                                                <option value="60">60</option>
                                                                <option value="90">90</option>

                                                            </select>

                                                        </div>

                                                        <div className="col-sm-12 mt-3">
                                                            <label className="form-label">How long is the grace period?</label>
                                                            <select
                                                                className="form-select"
                                                                name="grace_period"
                                                                value={formValues.grace_period ? formValues.grace_period : "60"}
                                                                onChange={handleChange}

                                                            >
                                                                <option value="30">30</option>
                                                                <option value="60">60</option>
                                                                <option value="90">90</option>
                                                            </select>

                                                        </div>


                                                        <div className="col-sm-12 mt-3">
                                                            <label className="form-label">When will the account be terminated?</label>
                                                            <select
                                                                className="form-select"
                                                                name="termination_days"
                                                                value={formValues.termination_days ? formValues.termination_days : "90"}
                                                                onChange={handleChange}

                                                            >
                                                                <option value="30">30</option>
                                                                <option value="60">60</option>
                                                                <option value="90">90</option>
                                                            </select>

                                                        </div>

                                                        <div className="col-sm-12 mt-3">
                                                            <label className="form-label">Please select the invoice cycle</label>
                                                            <select
                                                                className="form-select"
                                                                name="invoice_cycle"
                                                                value={formValues.invoice_cycle ? formValues.invoice_cycle : "MONTHLY"}
                                                                onChange={handleChange}

                                                            >
                                                                <option value="MONTHLY">MONTHLY</option>
                                                                <option value="NONE">NONE</option>
                                                                <option value="QUARTERLY">QUARTERLY</option>
                                                                <option value="YEARLY">YEARLY</option>
                                                            </select>

                                                        </div>
                                                    </div>
                                                </div>
                                            </>
                                    }



                                    <div className="modal-one" role="dialog" id="addMemberPlan">
                                        <div className="modal-dialog" role="document">
                                            <div className="modal-content">
                                                <div className="modal-body row">
                                                    <div className="content-overlay">
                                                        <div className="bg-light">
                                                            <div className="row">
                                                                <div>

                                                                    <p>
                                                                        Choose a location.
                                                                    </p>
                                                                    <button type="button" className="btn btn-default btn btn-primary btn-sm mt-0 pull-right" id="closeModal" data-dismiss="modal">X</button>
                                                                    <p>
                                                                        Choose the location from the tree.
                                                                    </p>
                                                                    <ul>
                                                                        <NavLink><span onClick={(e) => handleMainNavLinkClick(e, "InDepth Realty")}>
                                                                            Integra Reality</span></NavLink>
                                                                        <ul>
                                                                            <li className="office">
                                                                                <NavLink>
                                                                                    <span onClick={(e) => handleMainNavLinkClick(e, "Corporate")}>Corporate</span>
                                                                                </NavLink>
                                                                            </li>
                                                                            <li className="office">
                                                                                <NavLink>
                                                                                    <span onClick={(e) => handleMainNavLinkClick(e, "Elite")}>Elite</span>
                                                                                </NavLink>
                                                                            </li>
                                                                            <li className="office">
                                                                                <NavLink>
                                                                                    <span onClick={(e) => handleMainNavLinkClick(e, "Arizona")}>Arizona</span>
                                                                                </NavLink>
                                                                            </li>
                                                                            <li className="office">
                                                                                <NavLink>
                                                                                    <span onClick={(e) => handleMainNavLinkClick(e, "Colorado")}>Colorado</span>
                                                                                </NavLink>
                                                                            </li>
                                                                            <li className="office">
                                                                                <NavLink>
                                                                                    <span onClick={(e) => handleMainNavLinkClick(e, "Idaho")}>Idaho</span>
                                                                                </NavLink>
                                                                            </li>
                                                                        </ul>

                                                                    </ul>
                                                                </div>
                                                            </div>

                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div className="modal-one" role="dialog" id="removeMemberPlan">
                                        <div className="modal-dialog" role="document">
                                            <div className="modal-content">
                                                <div className="modal-body row">
                                                    <div className="content-overlay">
                                                        <div className="bg-light">
                                                            <div className="row">
                                                                <div>
                                                                    <button type="button" className="btn btn-default btn btn-primary btn-sm mt-0 pull-right" id="removeModal" data-dismiss="modal">X</button>
                                                                    <button onClick={(e) => handleRemove()} className="btn btn-primary btn-sm"
                                                                    >Yes, remove this member plan</button><br />
                                                                    <button className="btn btn-secondary btn-sm  mt-2" id="removeModal" data-dismiss="modal">No, cancel</button>

                                                                </div>
                                                            </div>

                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </main>
        </div>
    )
}
export default AddMemberPlan;
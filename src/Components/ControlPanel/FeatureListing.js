import React, { useState, useEffect } from "react";
import user_service from "../service/user_service";
import Loader from "../../Pages/Loader/Loader.js";
import Toaster from "../../Pages/Toaster/Toaster";
import { NavLink, useNavigate } from "react-router-dom";
import { Editor } from "react-draft-wysiwyg";
import { EditorState, ContentState, convertToRaw } from "draft-js";
import draftToHtml from "draftjs-to-html";
import htmlToDraft from "html-to-draftjs";
import "react-draft-wysiwyg/dist/react-draft-wysiwyg.css";

const FeatureListing = () => {
  const [loader, setLoader] = useState({ isActive: null });
  const { isActive } = loader;
  const [toaster, setToaster] = useState({
    types: null,
    isShow: null,
    toasterBody: "",
    message: "",
  });
  const { types, isShow, toasterBody, message } = toaster;

  const [formValues, setFormValues] = useState({ newsFeature: "" });

  const navigate = useNavigate();

  const PlatformdetailsGet = async () => {
    await user_service.platformdetailsGet().then((response) => {
      if (response) {
        setFormValues(response.data.data[0]);
      }
    });
  };

  useEffect(() => {
    window.scrollTo(0, 0);
    PlatformdetailsGet();
  }, []);

  return (
    <div className="bg-secondary float-left w-100 pt-4">
      <Loader isActive={isActive} />
      {isShow && (
        <Toaster
          types={types}
          isShow={isShow}
          toasterBody={toasterBody}
          message={message}
        />
      )}
      <main className="page-wrapper">
        <div className="">
          {/* <!-- Content--> */}
          <div className="d-flex align-items-center justify-content-start mb-4">
            <h3 className="text-white mb-0">New Features</h3>
          </div>
          <div className="row">
            <div className="col-md-12">
              <div className="bg-light border rounded-3 p-3">
                {formValues.newsFeature ? (
                  <span
                    dangerouslySetInnerHTML={{
                      __html: formValues.newsFeature,
                    }}
                  ></span>
                ) : (
                  <span>No data found in new feature.</span>
                )}
              </div>
              <div className="pull-right mt-3">
                <NavLink
                  type="button"
                  className="btn btn-secondary pull-right ms-3"
                  to="/"
                >
                  Cancel
                </NavLink>
              </div>
            </div>
          </div>
        </div>
      </main>
    </div>
  );
};

export default FeatureListing;

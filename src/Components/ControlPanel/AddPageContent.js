import React, { useState, useEffect, useRef } from "react";
import _ from "lodash";
import user_service from "../service/user_service";
import jwt from "jwt-decode";
import Loader from "../../Pages/Loader/Loader.js";
import Toaster from "../../Pages/Toaster/Toaster.js";
import { useNavigate, NavLink, useParams } from "react-router-dom";
import { Editor } from "react-draft-wysiwyg";
import { EditorState, ContentState, convertToRaw } from "draft-js";
import draftToHtml from "draftjs-to-html";
import htmlToDraft from "html-to-draftjs";
import "react-draft-wysiwyg/dist/react-draft-wysiwyg.css";

function AddPageContent() {
  const initialValues = {
    title: "",
    blurb: "",
    parent_section: "",
    publication_level: "",
    security: "",
  };
  const [formValues, setFormValues] = useState(initialValues);
  const [formErrors, setFormErrors] = useState({});
  const [isSubmitClick, setISSubmitClick] = useState(false);
  const [loader, setLoader] = useState({ isActive: null });
  const { isActive } = loader;
  const [toaster, setToaster] = useState({
    types: null,
    isShow: null,
    toasterBody: "",
    message: "",
  });
  const { types, isShow, toasterBody, message } = toaster;
  const [mainNavLinkText, setMainNavLinkText] = useState("Corporate");
  const [showData, setShowData] = useState("");
  const [showByTitle, setShowByTitle] = useState("");
  const [showDataId, setShowDataId] = useState("");
  const [checkManager, setCheckManager] = useState("");
  const [checkDocument, setCheckDocument] = useState("");
  const [documentGet, setDocumentGet] = useState([]);
  const [showByDefaultId, setShowByDefaultId] = useState("");
  const [showByContent, setShowByContent] = useState("");
  const [notesmessage, setNotesmessage] = useState("");
  const editorRef = useRef(null);
  const log = () => {
    if (editorRef.current) {
      console.log(editorRef.current);
      setNotesmessage(editorRef.current.getContent());
    }
  };

  const navigate = useNavigate();
  const params = useParams();

  // onChange Function start
  const handleChange = (e) => {
    const { name, value } = e.target;
    setFormValues({ ...formValues, [name]: value });
  };
  // onChange Function end

  {
    /* <!-- Form Validation Start--> */
  }
  useEffect(() => {
    if (formValues && isSubmitClick) {
      validate();
    }
  }, [formValues]);

  const validate = () => {
    const values = formValues;
    const errors = {};

    if (!values.title) {
      errors.title = "Title is required.";
    }

    if (!values.description) {
      errors.description = "Description is required.";
    }

    if (!values.content) {
      errors.content = "Content is required.";
    }

    setFormErrors(errors);
    return errors;
  };
  {
    /* <!-- Form Validation End--> */
  }

  const handleSubmit = async (e) => {
    if (params.id) {
      e.preventDefault();
      setISSubmitClick(true);
      let checkValue = validate();
      if (_.isEmpty(checkValue)) {
        const userData = {
          agentId: jwt(localStorage.getItem("auth")).id,
          office_id: localStorage.getItem("active_office_id"),
          office_name: localStorage.getItem("active_office"),
          title: formValues.title,
          type: "content",
          description: formValues.description,
          parent_section: showDataId
            ? showDataId
            : showByDefaultId
            ? showByDefaultId
            : "",
          publication_level: mainNavLinkText,
          exceptions: "",
          content: formValues.content,
          pin: checkDocument ? checkDocument : "No",
          lock: checkManager ? checkManager : "No",
          is_active: "Yes",
        };
        setLoader({ isActive: true });
        await user_service
          .OfficesharedDocContentUpdate(params.id, userData)
          .then((response) => {
            if (response) {
              console.log(response);
              setFormValues(response.data);
              setLoader({ isActive: false });
              setToaster({
                type: "Add Section",
                isShow: true,
                toasterBody: response.data.message,
                message: "Add Section Successfully",
              });
              setTimeout(() => {
                setToaster((prevToaster) => ({
                  ...prevToaster,
                  isShow: false,
                }));
                navigate("/control-panel/documents/");
              }, 1000);
            } else {
              setLoader({ isActive: false });
              setToaster({
                types: "error",
                isShow: true,
                toasterBody: response.data.message,
                message: "Error",
              });
            }
          });
      }
    } else {
      e.preventDefault();
      setISSubmitClick(true);
      let checkValue = validate();
      if (_.isEmpty(checkValue)) {
        const userData = {
          agentId: jwt(localStorage.getItem("auth")).id,
          office_id: localStorage.getItem("active_office_id"),
          office_name: localStorage.getItem("active_office"),
          title: formValues.title,
          type: "content",
          description: formValues.description,
          parent_section: showDataId
            ? showDataId
            : showByDefaultId
            ? showByDefaultId
            : "",
          publication_level: mainNavLinkText,
          exceptions: "",
          content: formValues.content,
          pin: checkDocument ? checkDocument : "no",
          lock: checkManager ? checkManager : "no",
          is_active: "yes",
        };
        setLoader({ isActive: true });
        await user_service
          .OfficesharedDocContentPost(userData)
          .then((response) => {
            if (response) {
              console.log(response);
              setFormValues(response.data);
              setLoader({ isActive: false });
              setToaster({
                type: "Add Section",
                isShow: true,
                toasterBody: response.data.message,
                message: "Add Section Successfully",
              });
              setTimeout(() => {
                setToaster((prevToaster) => ({
                  ...prevToaster,
                  isShow: false,
                }));
                navigate("/control-panel/documents/");
              }, 1000);
            } else {
              setLoader({ isActive: false });
              setToaster({
                types: "error",
                isShow: true,
                toasterBody: response.data.message,
                message: "Error",
              });
            }
          });
      }
    }
  };

  useEffect(() => {
    window.scrollTo(0, 0);
    setLoader({ isActive: true });
    const ShareDocGet = async () => {
      await user_service.ShareDocGet().then((response) => {
        setLoader({ isActive: false });
        if (response) {
          setDocumentGet(response.data);

          const getResponceId = response.data.data[0]._id;
          const getResponcetitle = response.data.data[0].title;
          setShowByDefaultId(getResponceId);
          setShowByTitle(getResponcetitle);
        }
      });
    };
    ShareDocGet();
  }, []);

  {
    /* <!-- Api call Form onSubmit End--> */
  }

  const handleBack = () => {
    navigate("/control-panel/documents/");
  };

  const checkManagerLock = (e) => {
    setCheckManager(e.target.name);
    setCheckManager(e.target.value);
  };

  const checkDocumentOrder = (e) => {
    setCheckDocument(e.target.name);
    setCheckDocument(e.target.value);
  };

  const handleMainNavLinkClick = (e, item) => {
    console.log(item);
    setMainNavLinkText(item);
    document.getElementById("closeLocation").click();
  };

  const [editorState, setEditorState] = useState(EditorState.createEmpty());

  const handleChanges = (editorState) => {
    setEditorState(editorState);
    const htmlContent = draftToHtml(
      convertToRaw(editorState.getCurrentContent())
    );
    setFormValues({ ...formValues, content: htmlContent });
  };

  // const handleChanges = (e) => {
  //   console.log(e);
  //   setFormValues({ ...formValues, content: e });
  // };

  const handleDataClick = (item, id) => {
    setShowData(item);
    setShowDataId(id);
    document.getElementById("closeModal").click();
  };

  useEffect(() => {
    window.scrollTo(0, 0);
    if (params.id) {
      setLoader({ isActive: true });
      const OfficesharedDocContenGetByid = async () => {
        await user_service
          .OfficesharedDocContenGet(params.id)
          .then((response) => {
            setLoader({ isActive: false });
            if (response) {
              setFormValues(response.data);
              const contentBlock = htmlToDraft(response.data.content);
              if (contentBlock) {
                const contentState = ContentState.createFromBlockArray(
                  contentBlock.contentBlocks
                );
                setEditorState(EditorState.createWithContent(contentState));
              }
              const ShareDocGetBySectionId = async () => {
                await user_service
                  .ShareDocGetById(response.data.parent_section)
                  .then((responsee) => {
                    setLoader({ isActive: false });
                    if (responsee) {
                      setShowData(responsee.data.title);
                    }
                  });
              };
              ShareDocGetBySectionId();
            }
          });
      };
      OfficesharedDocContenGetByid();
    }
  }, [params.id]);

  const handleRemove = async () => {
    setLoader({ isActive: true });
    await user_service
      .OfficesharedDocContentDelete(params.id)
      .then((response) => {
        if (response) {
          const ShareDocGet = async () => {
            await user_service.ShareDocGet().then((response) => {
              setLoader({ isActive: false });
              if (response) {
                setDocumentGet(response.data);
                setToaster({
                  types: "Document_Delete",
                  isShow: true,
                  toasterBody: response.data.message,
                  message: "Document_Delete Successfully",
                });
                setTimeout(() => {
                  setToaster((prevToaster) => ({
                    ...prevToaster,
                    isShow: false,
                  }));
                  navigate("/control-panel/documents/");
                }, 1000);
              }
            });
          };
          ShareDocGet();
        }
      });
  };
  return (
    <div className="bg-secondary float-left w-100 pt-4 mb-4">
      <Loader isActive={isActive} />
      {isShow && (
        <Toaster
          types={types}
          isShow={isShow}
          toasterBody={toasterBody}
          message={message}
        />
      )}
      <main className="page-wrapper profile_page_wrap">
        <div className="container content-overlay">
          <div className="">
            <div className="mb-4">
              <h3 className="text-white">Add Page Content Link</h3>
              <h6 className="text-white">Page Content Details</h6>
            </div>
            {/* <hr className="mt-2" /> */}
            <div className="">
              {/* <hr className="mt-5" /> */}
              <div className="add_listing">
                <div className="row">
                  <div className="col-md-6">
                    <div className="bg-light rounded-3 p-3 border">
                      <div className="">
                        <h6>Title</h6>
                        <label className="form-label">
                          What is the display title of the document?
                        </label>
                        <input
                          className="form-control"
                          id="inline-form-input"
                          type="text"
                          name="title"
                          value={formValues.title}
                          onChange={handleChange}
                        />
                        {formErrors.title && (
                          <div className="invalid-tooltip">
                            {formErrors.title}
                          </div>
                        )}
                      </div>
                      <div className="col-sm-12 mt-3">
                        <h6>Description</h6>
                        <label className="form-label">
                          What does this file contain?
                        </label>
                        <input
                          className="form-control"
                          id="inline-form-input"
                          type="text"
                          name="description"
                          value={formValues.description}
                          onChange={handleChange}
                        />
                        {formErrors.description && (
                          <div className="invalid-tooltip">
                            {formErrors.description}
                          </div>
                        )}
                      </div>
                      <div className="col-sm-12 mt-3">
                        <h6>Section</h6>
                        <label className="form-label">
                          In which section is this document filed?
                        </label>
                        <div className="d-flex">
                          <p>
                            {showData ? showData : showByTitle}&nbsp;&nbsp;
                            <NavLink>
                              <span
                                type="button"
                                data-toggle="modal"
                                data-target="#addSection"
                              >
                                change
                              </span>
                            </NavLink>
                          </p>
                        </div>
                      </div>
                      <div className="col-sm-12 mt-3">
                        <h6>Publication Level</h6>
                        <label className="form-label">
                          At what location level is this document published?
                        </label>
                        <div className="d-flex">
                          <p>
                            {mainNavLinkText}&nbsp;&nbsp;
                            <NavLink>
                              <span
                                type="button"
                                data-toggle="modal"
                                data-target="#addLocation"
                              >
                                change
                              </span>
                            </NavLink>
                          </p>
                        </div>
                      </div>
                      <div className="col-sm-12 mt-3">
                        <h6>Exceptions</h6>
                        <label className="form-label">
                          Hide this document at these locations:
                        </label>
                        <div className="d-flex">
                          <NavLink>
                            <span
                              type="button"
                              data-toggle="modal"
                              data-target="#addExceptions"
                            >
                              Add Exceptions
                            </span>
                          </NavLink>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="col-md-6 mt-lg-0 mt-md-0 mt-sm-4 mt-4">
                    <div className="bg-light rounded-3 p-3 border">
                      <div className="">
                        <h6>Content</h6>
                        <label className="form-label">
                          What is the content to this resource?
                        </label>

                        <Editor
                          editorState={editorState}
                          onEditorStateChange={handleChanges}
                          value={formValues.content}
                          toolbar={{
                            options: [
                              "inline",
                              "blockType",
                              "fontSize",
                              "list",
                              "textAlign",
                              "history",
                              "link", // Add link option here
                            ],
                            inline: {
                              options: [
                                "bold",
                                "italic",
                                "underline",
                                "strikethrough",
                              ],
                            },
                            list: { options: ["unordered", "ordered"] },
                            textAlign: {
                              options: ["left", "center", "right"],
                            },
                            history: { options: ["undo", "redo"] },
                            link: {
                              // Configure link options
                              options: ["link", "unlink"],
                            },
                          }}
                          wrapperClassName="demo-wrapper"
                          editorClassName="demo-editor"
                        />

                        {formErrors.content && (
                          <div className="invalid-tooltip">
                            {formErrors.content}
                          </div>
                        )}
                      </div>
                      <div
                        className="col-sm-12 mt-3"
                        onClick={(e) => checkDocumentOrder(e)}
                      >
                        <h6>Document Order</h6>
                        <label className="form-label">
                          Pin this document to the top of the section?
                        </label>
                        <div className="d-flex">
                          <div className="form-check">
                            <input
                              className="form-check-input"
                              id="form-check-1"
                              type="radio"
                              name="pin"
                              value="No"
                              checked
                            />
                            <label
                              className="form-check-label"
                              id="radio-level1"
                            >
                              No
                            </label>
                          </div>
                          <div className="form-check ms-3">
                            <input
                              className="form-check-input"
                              id="form-check-2"
                              type="radio"
                              name="pin"
                              value="Yes"
                            />
                            <label
                              className="form-check-label"
                              id="radio-level2"
                            >
                              Yes
                            </label>
                          </div>
                        </div>
                      </div>
                      <div
                        className="col-sm-12 mt-3"
                        onClick={(e) => checkManagerLock(e)}
                      >
                        <h6>Manager Lock</h6>
                        <label className="form-label">
                          Lock lower locations from hiding this document?
                        </label>
                        <div className="d-flex">
                          <div className="form-check">
                            <input
                              className="form-check-input"
                              id="form-check-3"
                              type="radio"
                              name="lock"
                              value="No"
                              checked
                            />
                            <label
                              className="form-check-label"
                              id="radio-leve3"
                            >
                              No
                            </label>
                          </div>
                          <div className="form-check ms-3">
                            <input
                              className="form-check-input"
                              id="form-check-4"
                              type="radio"
                              name="lock"
                              value="Yes"
                            />
                            <label
                              className="form-check-label"
                              id="radio-level4"
                            >
                              Yes
                            </label>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>

            <div className="pull-right mt-3">
              {localStorage.getItem("auth") &&
                jwt(localStorage.getItem("auth")).contactType === "admin" &&
                params.id && (
                  <button
                    className="btn btn-secondary pull-right mt-2 ms-2"
                    type="button"
                    onClick={(e) => handleRemove()}
                  >
                    Remove
                  </button>
                )}

              <button
                className="btn btn-primary pull-right mt-2 ms-3"
                type="button"
                onClick={handleSubmit}
              >
                Add Page Content Now
              </button>
              <button
                className="btn btn-secondary pull-right mt-2"
                type="button"
                onClick={handleBack}
              >
                Cancel & Go Back
              </button>
            </div>
          </div>
        </div>
      </main>

      <div className="modal in" role="dialog" id="addSection">
        <div
          className="modal-dialog modal-dialog-centered modal-dialog-scrollable"
          role="document"
        >
          <div className="modal-content">
            <div className="modal-header border-0 pb-0">
              {/* <h4 className="modal-title"></h4> */}
              <button
                className="btn-close"
                type="button"
                data-dismiss="modal"
                aria-label="Close"
              ></button>
            </div>
            <div className="modal-body fs-sm pt-0">
              <div className="content-overlay">
                <div className="bg-light">
                  <div className="">
                    <div>
                      <h6>Choose a section from the tree.</h6>
                      <div>
                        {documentGet.data && documentGet.data.length > 0 ? (
                          <NestedArray
                            items={documentGet.data}
                            handleDataClick={handleDataClick}
                          />
                        ) : null}
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div className="modal in" role="dialog" id="addLocation">
        <div
          className="modal-dialog modal-dialog-centered modal-dialog-scrollable"
          role="document"
        >
          <div className="modal-content">
            <div className="modal-header border-0 pb-0">
              {/* <h4 className="modal-title"></h4> */}
              <button
                className="btn-close"
                type="button"
                data-dismiss="modal"
                aria-label="Close"
              ></button>
            </div>
            <div className="modal-body fs-sm pt-0">
              <div className="content-overlay">
                <div className="bg-light">
                  <div className="">
                    <div>
                      <p>Choose a location.</p>
                      <p>Choose the location from the tree.</p>
                      <div className="pull-right">
                        <NavLink className="btn btn-sm btn-primary">
                          <span
                            onClick={(e) =>
                              handleMainNavLinkClick(e, "Corporate")
                            }
                          >
                            Corporate
                          </span>
                        </NavLink>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div className="modal in" role="dialog" id="addExceptions">
        <div
          className="modal-dialog modal-dialog-centered modal-dialog-scrollable"
          role="document"
        >
          <div className="modal-content">
            <div className="modal-header border-0 pb-0">
              {/* <h4 className="modal-title"></h4> */}
              <button
                id="closeModal"
                className="btn-close"
                type="button"
                data-dismiss="modal"
                aria-label="Close"
              ></button>
            </div>
            <div className="modal-body fs-sm pt-0">
              <div className="content-overlay">
                <div className="bg-light">
                  <div className="">
                    <div>
                      <p>Choose a location.</p>
                      <p>Choose the location from the tree.</p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
export default AddPageContent;

function NestedArray({ items, handleDataClick }) {
  return (
    <ul>
      {items.map((item) => (
        <li key={item._id}>
          <div
            className="mb-4 ms-2"
            onClick={() => handleDataClick(item.title, item._id)}
          >
            <NavLink>
              <p className="mt-4">{item.title}</p>
            </NavLink>
          </div>

          {item.children && item.children.length > 0 && (
            <NestedArray
              items={item.children}
              handleDataClick={handleDataClick}
            />
          )}

          {/* {item.content && item.content.length > 0 && (
                        <ul>
                            {item.content.map((contentItem) => (
                                <li key={contentItem._id}>
                                    <div className="mb-4 ms-2" onClick={() => handleDataClick(contentItem.title, contentItem._id)}>
                                        <NavLink>
                                            <p className="mt-4 ms-5">{contentItem.title}</p>
                                        </NavLink>
                                    </div>
                                </li>
                            ))}
                        </ul>
                    )} */}
        </li>
      ))}
    </ul>
  );
}

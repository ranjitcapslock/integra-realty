import React, { useState, useEffect } from "react";
// import avtar from "./Components/img/avtar.jpg";
import _ from 'lodash';
import user_service from "../service/user_service";
import jwt from "jwt-decode";
import Loader from "../../Pages/Loader/Loader.js";
import Toaster from "../../Pages/Toaster/Toaster.js";
import { NavLink, useNavigate, useParams } from "react-router-dom";
import moment from "moment-timezone";

function AddBanner() { 
    const initialValues = {
        title: "", publication_level: "", url_link: "", isnewtab: "", startDate: "", endDate: "",
        max_impressions: "", max_clicks: "", weighting: "", marque_image: "", side_image: "", banner_location :"home", isactive: "", agentId: ""
    };
    const [formValues, setFormValues] = useState(initialValues);
    const [formErrors, setFormErrors] = useState({});
    const [isSubmitClick, setISSubmitClick] = useState(false)
    const [loader, setLoader] = useState({ isActive: null })
    const { isActive } = loader;
    const [toaster, setToaster] = useState({ types: null, isShow: null, toasterBody: "", message: "" });
    const { types, isShow, toasterBody, message } = toaster;

    const [checkData, setCheckData] = useState("")
    const navigate = useNavigate();
    const params = useParams();



    // onChange Function start
    const handleChange = (e) => {
        const { name, value } = e.target;
        setFormValues({ ...formValues, [name]: value })
    };
    // onChange Function end

    const handleCheckBox = (e) => {
        setCheckData(e.target.name);
        setCheckData(e.target.value);
    }

    {/* <!-- Form Validation Start--> */ }
    useEffect(() => { 
        if (formValues && isSubmitClick) {
            validate()
        }
    }, [formValues])




    const validate = () => {
        const values = formValues
        const errors = {};

        if (!values.title) {
            errors.title = "Title is required";
        }

        setFormErrors(errors)
        return errors;
    }
    {/* <!-- Form Validation End--> */ }



    {/* <!--Api call Form onSubmit Start--> */ }

    useEffect(() => { window.scrollTo(0, 0);
        const date = new Date();
        const formattedDate = date.toLocaleDateString('en-GB', {
            year: 'numeric',
            month: '2-digit',
            day: '2-digit',
        }).split('/').reverse().join('-');

        setFormValues({ startDate: formattedDate });
    }, []);

    const handleSubmit = async (e) => {
        if (params.id) {
            e.preventDefault();
            setISSubmitClick(true)
            let checkValue = validate()
            if (_.isEmpty(checkValue)) {
                const userData = {
                    // agentId: jwt(localStorage.getItem("auth")).id,
                    office_id: localStorage.getItem("active_office_id"),
                    office_name: localStorage.getItem("active_office"),
                    title: formValues.title,
                    publication_level: formValues.publication_level,
                    url_link: formValues.url_link,
                    isnewtab: checkData ? checkData : "No",
                    startDate: formValues.startDate,
                    endDate: formValues.endDate ? formValues.endDate : "",
                    max_impressions: formValues.max_impressions ? formValues.max_impressions : 0,
                    max_clicks: formValues.max_clicks ? formValues.max_clicks : 0,
                    weighting: formValues.weighting ? formValues.weighting : 0,
                    banner_location: formValues.banner_location ? formValues.banner_location : "home",
                };
                setLoader({ isActive: true })
                await user_service.bannerUpdate(params.id, userData).then((response) => {
                    if (response) {
                        setFormValues(response.data);
                        setLoader({ isActive: false })
                        setToaster({ type: "Banner Update Successfully", isShow: true, toasterBody: response.data.message, message: "Banner Update Successfully", });
                        setTimeout(() => {
                            setToaster((prevToaster) => ({ ...prevToaster, isShow: false }))
                            navigate("/control-panel/banners");
                        }, 1000);
                    }
                    else {
                        setLoader({ isActive: false })
                        setToaster({ types: "error", isShow: true, toasterBody: response.data.message, message: "Error", });
                    }
                });
            }
        }

        else {
            e.preventDefault();
            setISSubmitClick(true)
            let checkValue = validate()
            if (_.isEmpty(checkValue)) {
                const userData = {
                    agentId: jwt(localStorage.getItem("auth")).id,
                    office_id: localStorage.getItem("active_office_id"),
                    office_name: localStorage.getItem("active_office"),
                    title: formValues.title,
                    publication_level: formValues.publication_level,
                    url_link: formValues.url_link,
                    isnewtab: checkData ? checkData : "No",
                    startDate: formValues.startDate,
                    endDate: formValues.endDate ? formValues.endDate : "",
                    max_impressions: formValues.max_impressions ? formValues.max_impressions : 0,
                    max_clicks: formValues.max_clicks ? formValues.max_clicks : 0,
                    weighting: formValues.weighting ? formValues.weighting : 0,
                    banner_location: formValues.banner_location ? formValues.banner_location : "home",

                    isactive: true,
                };
                setLoader({ isActive: true })
                await user_service.bannerPost(userData).then((response) => {
                    if (response) {
                        console.log(response);
                        setLoader({ isActive: false })
                        setToaster({ type: "Banner Successfully", isShow: true, toasterBody: response.data.message, message: "Banner Successfully", });
                        setTimeout(() => {
                            setToaster((prevToaster) => ({ ...prevToaster, isShow: false }))
                            navigate("/control-panel/banners");
                        }, 1000);
                    }
                    else {
                        setLoader({ isActive: false })
                        setToaster({ types: "error", isShow: true, toasterBody: response.data.message, message: "Error", });
                    }
                });
            }
        }
    }
    {/* <!-- Api call Form onSubmit End--> */ }


    useEffect(() => { window.scrollTo(0, 0);
        if (params.id) {
            setLoader({ isActive: true });
            const BannerGetById = async () => {
                await user_service.bannerGetById(params.id).then((response) => {
                    setLoader({ isActive: false })
                    if (response) {
                        setFormValues(response.data);
                    }
                });
            }
            BannerGetById(params.id)
        }
    }, [params.id]);

    const handleRemove = async () => {
        setLoader({ isActive: true })
        await user_service.bannerDelete(params.id).then((response) => {
            if (response) {
                const BannerList = async () => {
                    await user_service.bannerGet().then((response) => {
                        setLoader({ isActive: false })
                        if (response) {
                            setFormValues(response.data);
                            setToaster({ type: "Banner Delete Successfully", isShow: true, toasterBody: response.data.message, message: "Banner Delete Successfully", });
                            setTimeout(() => {
                                setToaster((prevToaster) => ({ ...prevToaster, isShow: false }))
                                navigate("/control-panel/banners");
                            }, 1000);
                        }
                    });
                }
                BannerList()
            }

        });
    }

    const handleRemoveImage = async (marque) => {
        console.log(marque);


        if (marque === "marque_image") {
            console.log(marque);

            const userData = {
                agentId: jwt(localStorage.getItem('auth')).id,
                marque_image: "",
            };

            console.log(userData);

            try {
                setLoader({ isActive: true })
                const response = await user_service.bannerUpdate(params.id, userData);
                if (response) {
                    console.log(response);
                    setLoader({ isActive: false })
                    setToaster({ type: "Marquee_image Delete Successfully", isShow: true, toasterBody: response.data.message, message: "Marquee_image Delete Successfully", });
                    setTimeout(() => {
                        setToaster((prevToaster) => ({ ...prevToaster, isShow: false }))
                        navigate("/control-panel/banners");
                    }, 1000);
                    const BannerList = async () => {
                        await user_service.bannerGet().then((response) => {
                            setLoader({ isActive: false })
                            if (response) {
                                setFormValues(response.data)
                            }
                        });
                    }
                    BannerList()


                }
            } catch (error) {
                console.log('Error occurred while uploading documents:', error);
            }
        }

        if (marque === "side_image") {

            const userData = {
                agentId: jwt(localStorage.getItem('auth')).id,
                side_image: "",
            };

            try {
                setLoader({ isActive: true })
                const response = await user_service.bannerUpdate(params.id, userData);
                if (response) {
                    console.log(response);
                    setLoader({ isActive: false })
                    setToaster({ type: "Side_image Delete Successfully", isShow: true, toasterBody: response.data.message, message: "Side_image Delete Successfully", });
                    setTimeout(() => {
                        setToaster((prevToaster) => ({ ...prevToaster, isShow: false }))
                        navigate("/control-panel/banners");
                    }, 1000);
                    const BannerList = async () => {
                        await user_service.bannerGet().then((response) => {
                            setLoader({ isActive: false })
                            if (response) {
                                setFormValues(response.data)
                            }
                        });
                    }
                    BannerList()

                    document.getElementById('closeModal').click();
                }
            } catch (error) {
                console.log('Error occurred while uploading documents:', error);
            }
        }
    }


    return (
        <div className="bg-secondary float-left w-100 pt-4 mb-4">
            <Loader isActive={isActive} />
            {isShow && <Toaster types={types} isShow={isShow} toasterBody={toasterBody} message={message} />}
            <main className="page-wrapper profile_page_wrap">
                <div className="content-overlay">
                        <div className="mb-4">
                        <h3 className="mb-0 text-white">{params.id ? "Update Campaign" : "New Campaign"}</h3>
                            {/* <hr className="mb-3" /> */}
                        </div>
                            {/* <hr /> */}
                    <div className="bg-light rounded-3 border p-3">
                        <div className="">
                            <div className="">
                                <div className="row">
                                    <div className="col-md-12">
                                        <label className="col-form-label p-0" >Campaign Title*</label>
                                        <p className="mb-2">An internal reference name, not shown to associates.</p>
                                            <input className="form-control" id="inline-form-input" type="text"
                                            name="title" value={formValues.title}
                                            onChange={handleChange}
                                            style={{
                                                border: formErrors?.title ? '1px solid red' : ''
                                            }}
                                        />
                                        {formErrors.title && (
                                            <div className="invalid-tooltip">{formErrors.title}</div>
                                        )}
                                    </div>

                                    <div className="col-md-12 mt-1">
                                        <label className="col-form-label" >Banner Location</label>
                                        <select className="form-select" id="pr-city"
                                            name="banner_location"
                                            onChange={handleChange}
                                            value={formValues.banner_location}>
                                            <option value="home">Home Page</option>
                                            <option value="promote">Promote/Store Page</option>
                                            <option value="marketplace"> Market Place</option>
                                            <option value="contact">Contacts</option>
                                        </select>
                                    </div>

                                    <div className="col-md-12 mt-1">
                                        <label className="col-form-label" >Publication Level</label>
                                        <input className="form-control" id="inline-form-input" type="text"
                                            name="publication_level" value={formValues.publication_level ? formValues.publication_level : "Corporate"}
                                            onChange={handleChange}/>
                                    </div>

                                    <div className="col-md-12 mt-1">
                                        <div className="form-check" onClick={(e) => handleCheckBox(e)}>
                                            <input className="form-check-input"
                                                name="isnewtab"
                                                type="checkbox"
                                                value="Yes"/>
                                            <label className="form-check-label"><small>Open in New Window</small></label>
                                        </div>
                                    </div>

                                    <div className="col-md-12 mt-1">
                                        <label className="col-form-label" for="pr-country">URL Link</label>
                                        <input className="form-control" id="inline-form-input" type="text"
                                            name="url_link" value={formValues.url_link}
                                            onChange={handleChange}
                                        />
                                    </div>

                                    <div className="col-md-12 mt-1">
                                        <label className="col-form-label" for="pr-birth-date">Start Date</label>
                                        <input className="form-control" type="date" id="inline-form-input"
                                            name="startDate" value={formValues.startDate || ""}
                                            onChange={handleChange}
                                        />

                                    </div>

                                    <div className="col-md-12 mt-1">
                                        <label className="col-form-label" for="pr-birth-date">End Date</label>
                                        <input className="form-control" type="date" id="inline-form-input"
                                            name="endDate" value={formValues.endDate || ""}
                                            onChange={handleChange}
                                        />

                                    </div>


                                    <div className="col-md-12 mt-1">
                                        <label className="col-form-label" for="pr-country">Maximum Impressions</label>
                                        <input className="form-control" id="inline-form-input" type="number"
                                            name="max_impressions" value={formValues.max_impressions}
                                            onChange={handleChange}
                                        />
                                    </div>


                                    <div className="col-md-12 mt-1">
                                        <label className="col-form-label" for="pr-country">Maximum Clicks</label>
                                        <input className="form-control" id="inline-form-input" type="number"
                                            name="max_clicks" value={formValues.max_clicks}
                                            onChange={handleChange}
                                        />

                                    </div>


                                    <div className="col-md-12 mt-1">
                                        <label className="col-form-label" for="pr-country">Campaign Weighting</label>
                                        <input className="form-control" id="inline-form-input" type="number"
                                            name="weighting" value={formValues.weighting}
                                            onChange={handleChange}
                                        />
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="pull-right mt-4">
                        <button className="btn btn-secondary ms-3 mb-3" type="button" onClick={(e) => handleRemove()}><NavLink>Remove Campaign</NavLink></button>

                        {formValues.marque_image ?
                            <>
                                <button className="btn btn-secondary ms-3 mb-3" type="button" onClick={() => handleRemoveImage("marque_image")}>
                                    <NavLink>Remove Marquee</NavLink>
                                </button>
                            </>
                            : ""
                        }

                        {
                            formValues.side_image ?
                                <>

                                    <button className="btn btn-secondary ms-3 mb-3" type="button" onClick={() => handleRemoveImage("side_image")}>
                                        <NavLink>Remove Side</NavLink>
                                    </button>
                                </>
                                : ""
                        }
                        <NavLink className="btn btn-secondary ms-3 mb-3" type="button" to="/control-panel/banners/">Cancel</NavLink>

                        <button className="btn btn-primary ms-3 mb-3" type="button" onClick={handleSubmit}>Update Campaign</button>
                    </div>
                </div>
            </main>
        </div>
    )
}
export default AddBanner;
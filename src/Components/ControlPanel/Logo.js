import React, { useState, useEffect } from "react";
import _ from 'lodash';
import user_service from '../service/user_service';
import axios from "axios";

import Loader from "../../Pages/Loader/Loader.js";
import Toaster from "../../Pages/Toaster/Toaster.js";
import { NavLink, useNavigate } from "react-router-dom";

import jwt from "jwt-decode";


function Logo() {
    const navigate = useNavigate();
    const [loader, setLoader] = useState({ isActive: null })
    const { isActive } = loader;
    const [toaster, setToaster] = useState({ types: null, isShow: null, toasterBody: "", message: "" });
    const { types, isShow, toasterBody, message } = toaster;

    const [clickLogo, setClickLogo] = useState("");
    const [file, setFile] = useState(null);
    const [data, setData] = useState("");



    const handleClickLogo = (logo) => {
        console.log(logo);
        setClickLogo(logo)

    }


    const handleBack = () => {
        setClickLogo("")
    }


    const handleFileUpload = async (e) => {
        const selectedFile = e.target.files[0];

        if (selectedFile) {
        if (selectedFile.type === 'image/png') {
             // Create a new file with the desired name
            const newFileName = "logo.png"; // Replace "newFileName.jpg" with the desired name
            const newFile = new File([selectedFile], newFileName, {
                type: selectedFile.type,
            });

            setFile(newFile);
            const formData = new FormData();
            formData.append('file', newFile);

            const config = {
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'multipart/form-data',
                    'Authorization': `Bearer ${localStorage.getItem('auth')}`,
                },
            };

            try {
                 const uploadResponse = await axios.post('https://api.brokeragentbase.com/uploadasset', formData, config);
                //const uploadResponse = await axios.post('http://localhost:4000/uploadasset', formData, config);
                if (uploadResponse) {
                    setData(uploadResponse.data);
                    window.location.reload();
                }
            } catch (error) {
                console.error('Error occurred during file upload:', error);
            }
        }else{
            setLoader({ isActive: false })
            setToaster({ types: "error", isShow: true, toasterBody: "Please upload only png image for logo .", message: "Error", });      
        }

        }
    };


    return (
        <div className="bg-secondary float-left w-100 pt-4 mb-4">
            <Loader isActive={isActive} />
            {isShow && <Toaster types={types}
                isShow={isShow}
                toasterBody={toasterBody}
                message={message} />}
            <main className="page-wrapper profile_page_wrap">
                <div className="content-overlay">
                    <div className="d-flex align-items-center justify-content-start mb-4">
                        <h4 className="mb-0 text-white">Logo for InDepth Realty</h4>
                    </div>
                        <div className="">
                            {/* <hr /> */}
                            {/* <button className="btn btn-secondary pull-right mt-2" type="button" onClick={handleBack}>Cancel</button> */}
                            <div className="bg-light rounded-3 p-3 border">
                                <div className="add_listing">
                                    {
                                        clickLogo ?
                                            <div className="row">
                                                <div className="col-md-8">
                                                    {
                                                        clickLogo == "Company_Logo" ?
                                                            <h6>Upload Company Logo</h6>
                                                            :
                                                            <h6>Upload Custom Logo</h6>
                                                    }
                                                    <p style={{  }}>Please upload a high resolution version of your logo without any whitespace or
                                                        padding on the edges. The logo will be automatically resized to a variety
                                                        of sizes for different systems.
                                                        Transparent PNG-format logos with a horizontal orientation are preferred.
                                                        For further assistance, please <NavLink>contact support.</NavLink>
                                                    </p>
                                                </div>

                                                <div className="mt-3">
                                                    <label className="form-label">Select a high resolution logo to upload:</label>
                                                    <input className="file-uploader file-uploader-grid"
                                                        id="inline-form-input"
                                                        type="file"
                                                        name="image"
                                                        onChange={handleFileUpload}
                                                    />
                                                </div>
                                                <div>

                                                    <button className="btn btn-secondary pull-left ms-5 mt-5" type="button" onClick={handleBack}>Cancel</button>
                                                    <button className="btn btn-primary pull-left ms-5  mt-5" type="button">Upload Logo Now</button>
                                                </div>

                                                {
                                                    clickLogo == "Intranet_Logo" ?
                                                        <div className="mt-3">
                                                            <h6>How should the logo be displayed?</h6>
                                                            <h6>Should the logo be displayed in a white box?</h6>
                                                            <div className="">
                                                                <div className="form-check">
                                                                    <input className="form-check-input" id="form-check-One" type="radio"
                                                                        name="main_color"
                                                                        value="maintop_edge"

                                                                    />
                                                                    <label className="form-check-label" id="radio-level1">Yes, please include a white frame around logo.</label>
                                                                </div>

                                                                <div className="form-check">
                                                                    <input className="form-check-input" id="form-check-Two" type="radio"
                                                                        name="main_color"
                                                                        value="main_background" />
                                                                    <label className="form-check-label" id="radio-level2">No, please display transparent PNG logo without a frame.</label>
                                                                </div>
                                                            </div>

                                                            <button className="btn btn-primary pull-left  mt-5" type="button" >Save Setting</button>

                                                        </div>
                                                        :
                                                        ""
                                                }

                                            </div>
                                            :
                                            <div className="row">
                                                <div className="col-md-12">
                                                    <div className="row">

                                                        <div className="col-md-6">
                                                            <h6>Company Logo</h6>
                                                        </div>
                                                        {/* <div className="col-md-6">
                                                            <h6 style={{ color: "red" }}>Not Print Ready!</h6>
                                                        </div> */}
                                                    </div>
                                                    <div className="col-md-12">
                                                        <p style={{  }}>Default logo used on all branded documents and<br /> sites,
                                                            displayed on an all white background. Intranet<br /> default
                                                            . e.g. Cover sheets, funding docs, legal forms.
                                                        </p>
                                                    </div>

                                                    <button className="btn btn-primary pull-left mt-2" type="button" onClick={(e) => handleClickLogo("Company_Logo")}>Upload Company Logo</button>


                                                </div>


                                                {/* <div className="col-md-6">
                                                    <div className="row">

                                                        <div className="col-md-6">
                                                            <h6>Intranet Logo</h6>
                                                        </div>

                                                    </div>
                                                    <div className="col-md-12">
                                                        <p style={{ fontSize: 20 }}>Specialty logo used for display on intranet and selected
                                                            system-connected websites, supports
                                                            transparency. Not used on system generated documents or emails.
                                                        </p>
                                                    </div>

                                                    <button className="btn btn-primary pull-left ms-5 mt-2" type="button" onClick={(e) => handleClickLogo("Intranet_Logo")}>Upload Intranet Logo</button>
                                                </div> */}

                                                <div className="col-md-12 mt-3">
                                                    <p style={{  }}>Is the Company Logo ready for print?  <NavLink>No, not ready for print.</NavLink></p>
                                                    <p style={{  }}><a href="">View Print Logo </a>   Use this link to verify and view the hi-res logo being used for print.</p>
                                                </div>

                                                <div className="col-md-6">
                                                    <p style={{  }}>Need a temporary logo for right now?</p>
                                                    <div className="d-flex">
                                                        <button className="btn btn-secondary" type="button">Generate Logo Now </button><p className="ms-2 my-2">Auto-create a basic logo.</p>
                                                    </div>
                                                </div>

                                            </div>
                                    }

                                </div>
                            </div>
                        </div>
                </div>
            </main>
        </div >
    )
}
export default Logo;


import React, { useState, useEffect } from "react";
import user_service from "../service/user_service.js";
import Loader from "../../Pages/Loader/Loader.js";
import Toaster from "../../Pages/Toaster/Toaster.js";
import { NavLink, useNavigate } from "react-router-dom";
import jwt from "jwt-decode";
import { EditorState, ContentState } from "draft-js";
import draftToHtml from 'draftjs-to-html'; // Import the package
import { Editor } from "react-draft-wysiwyg";
import { convertFromHTML, convertToRaw } from "draft-js"; // Import helper functions for HTML conversion
import "react-draft-wysiwyg/dist/react-draft-wysiwyg.css";
import Swal from "sweetalert2";
import "sweetalert2/dist/sweetalert2.css";
import axios from 'axios';

const MarketingContentListing = () => {
  const [loader, setLoader] = useState({ isActive: null });
  const { isActive } = loader;
  const [acceptedFileTypes, setAcceptedFileTypes] = useState([]);
  const [marketingbanner, setMarketingbanner] = useState("");

  const [activeButton, setActiveButton] = useState("marketing");

  const [itemId, setItemId] = useState("")

  const [toaster, setToaster] = useState({
    types: null,
    isShow: null,
    toasterBody: "",
    message: "",
  });
  const { types, isShow, toasterBody, message } = toaster;

  const [formValues, setFormValues] = useState("");

  const navigate = useNavigate();

  const [editorState, setEditorState] = useState(EditorState.createEmpty());

  const PageContentMarketingGet = async () => {
    await user_service.pageContentMarketingGet().then((response) => {
      if (response) {
        console.log(response);
        setFormValues(response.data.data);
      }
    });
  };

  useEffect(() => {
    window.scrollTo(0, 0);
    PageContentMarketingGet();
  }, []);

  const [contentText, setContentText] = useState("");
  const [contentId, setContentId] = useState("");
  const [contentPopUp, setContentPopUp] = useState(false);

  const convertHTMLToEditorState = (htmlContent) => {
    const blocksFromHTML = convertFromHTML(htmlContent);
    const contentState = ContentState.createFromBlockArray(
      blocksFromHTML.contentBlocks,
      blocksFromHTML.entityMap
    );
    return EditorState.createWithContent(contentState);
  };

  const handleEditContent = (id, text) => {
    setContentId(id);
    const editorContentState = convertHTMLToEditorState(text); // Convert HTML to ContentState
    setEditorState(editorContentState);
    setContentText(text);
    setContentPopUp(true);
  };

  const handleEditorChange = (state) => {
    setEditorState(state); // Update the editor's state
  };

  const handleSubmit = async (e) => {
    if (contentId) {
      e.preventDefault();
      const rawContentState = convertToRaw(editorState.getCurrentContent());
      const contentAsHTML = draftToHtml(rawContentState);
      const userData = {
        content: contentAsHTML,  // Send the HTML content to the server
      };
      setLoader({ isActive: true });
      await user_service.pageContentMarketingUpdate(contentId, userData).then((response) => {
        if (response) {
          setFormValues(response.data);
          setContentPopUp(false)
          PageContentMarketingGet()
          setLoader({ isActive: false });
          setToaster({
            type: "Content Added",
            isShow: true,
            toasterBody: response.data.message,
            message: "Content Added Successfully",
          });
          setTimeout(() => {
            setToaster((prevToaster) => ({ ...prevToaster, isShow: false }));
          }, 1000);
        } else {
          setLoader({ isActive: false });
          setToaster({
            types: "error",
            isShow: true,
            toasterBody: response.data.message,
            message: "Error",
          });
        }
      });
    }
  };


  const handleClose = () => {
    setContentPopUp(false);
  };

  const handleRemove = async (contentid) => {
    if (contentid) {
      const confirmation = await Swal.fire({
        title: "Are you sure?",
        text: "It will be permanently deleted!",
        icon: "warning",
        showCancelButton: true,
        confirmButtonColor: "#3085d6",
        cancelButtonColor: "#d33",
        confirmButtonText: "Yes, delete it!",
      });
  
      if (confirmation.isConfirmed) {
        setLoader({ isActive: true });
  
        await user_service.pageContentMarketingDelete(contentid).then((response) => {
          if (response) {
            setLoader({ isActive: false });
            // setDateData("");
            setToaster({
              type: "success",
              isShow: true,
              message: "Content Deleted Successfully",
            });
            setTimeout(() => {
              setToaster((prevToaster) => ({
                ...prevToaster,
                isShow: false,
              }));
            }, 1000);
            PageContentMarketingGet();
          }
        });
      }
    }
  };


  const handleFileUpload = async (e, id) => {
    setItemId(id);
    console.log(id)
    const selectedFile = e.target.files;

    if (selectedFile && selectedFile[0]) { // Check if selectedFile is defined and contains at least one file
        // setFile(selectedFile);
        const fileExtension = selectedFile[0].name.split('.').pop(); // Access the file extension

        const formData = new FormData();
        formData.append('file', selectedFile[0]);
        const config = {
            headers: {
                Accept: 'application/json',
                'Content-Type': 'multipart/form-data',
                Authorization: `Bearer ${localStorage.getItem('auth')}`,
            },
        };

        try {
            setLoader({ isActive: true })
            const uploadResponse = await axios.post('https://api.brokeragentbase.com/upload', formData, config);
            // const uploadResponse = await axios.post('http://localhost:4000/upload', formData, config);
            const uploadedFileData = uploadResponse.data;
            setLoader({ isActive: false })
            setMarketingbanner(uploadedFileData);
            document.getElementById('closeModal').click();
            const userData = {
                marketingbanner: uploadedFileData,
            };

            // console.log(userData);
            try {
                const response = await user_service.pageContentMarketingUpdate(itemId, userData);
                if (response) {
                    console.log(response);
                    setMarketingbanner("");
                    setItemId("");
                    document.getElementById('closeModal').click();
                    setToaster({
                      type: "Content Added",
                      isShow: true,
                      toasterBody: "UPdated Successfully",
                      message: "Content Added Successfully",
                    });
                    setTimeout(() => {
                      setToaster((prevToaster) => ({ ...prevToaster, isShow: false }));
                    }, 1000);

                    PageContentMarketingGet();
                }
            } catch (error) {
                console.log('Error occurred while uploading documents:', error);
            }
        } catch (error) {
            console.error('Error occurred during file upload:', error);
        }
    } else {
        console.log('No file selected.');
    }
}
  return (
    <div className="bg-secondary float-left w-100 pt-4">
      <Loader isActive={isActive} />
      {isShow && (
        <Toaster
          types={types}
          isShow={isShow}
          toasterBody={toasterBody}
          message={message}
        />
      )}
      <main className="page-wrapper">
        {/* <!-- Page content--> */}
        {/* <!-- Page container--> */}
        <div className="">
          {/* <!-- Page card like wrapper--> */}

            {/* <!-- Content--> */}
            <div className="">
              <div className="align-items-center justify-content-start mb-4">
                <NavLink to={`/marketing-content`} className="btn btn-primary pull-right">Add Content / Banner</NavLink>
                <h3 className="text-white mb-0">Marketing / Events Calender Content and Banner</h3>
              </div>
              <div className="row">
                <div className="col-md-10">
                  <div className="stories_blog_section mt-0">
                    <div className="tabbable" id="tabs-459476">
                      <ul className="nav nav-tabs mb-3">
                        <li
                          className={`nav-item ${
                            activeButton === "marketing" ? "active" : ""
                          }`}
                          onClick={(e) => setActiveButton("marketing")}
                        >
                          <a className="nav-link" href="#tab2" data-toggle="tab">
                            Mareketing Calander  
                          </a>
                        </li>
                        
                        <li
                          className="nav-item"
                          // onClick={(e) => IndustryPostGetAll("biggerpockets")}
                        >
                          <a className="nav-link" href="#tab4" data-toggle="tab">
                            Events Calender
                          </a>
                        </li>
                      </ul>

                      <div className="tab-content showblog_category">
                        <div className="tab-pane active" id="tab2">
                          <div className="bg-light border rounded-3 p-3 mb-4">
                            <span>Marketing Calander Default Content and Banner</span>
                          </div>
                          {formValues && formValues.length > 0
                            ? formValues.map((item) => {
                                if(item.page==="marketing"){
                                  return (
                                    <div className="bg-light border rounded-3 p-3 mb-4">
                                      
                                      <button
                                        onClick={(e) =>
                                          handleRemove(item._id, item.content)
                                        }
                                        className="btn btn-primary pull-right mt-0 ms-3"
                                        type="button"
                                      >
                                        Delete
                                      </button>
  
                                      <button
                                        onClick={(e) =>
                                          handleEditContent(item._id, item.content)
                                        }
                                        className="btn btn-primary pull-right mt-0"
                                        type="button"
                                        data-toggle="modal"
                                        data-target="#market-calender-text"
                                      >
                                        Edit
                                      </button>
  
                                      <p>
                                        <b className="col-form-label">Month:</b> {item.month}
                                      </p>
  
                                      {
                                        item.marketingbanner ?
                                          <div className="">
                                            <label className="col-form-label">Marketing Banner :</label>
                                              <div className="NotSetup Marquee text-center mb-4 mt-3 float-start w-100" onClick={(e) => handleFileUpload(e, item._id)} data-toggle="modal"  data-target="#addDocumentDetail">
                                                <img className = "img-fluid w-100" src={item.marketingbanner} alt="Document Preview" />
                                              </div>
                                          </div>
                                        :
                                        <div className="">
                                            <label className="col-form-label">Marketing Banner :</label>
                                            <div className="NotSetup Marquee text-center mb-4 mt-3 float-start w-100">
                                              <button type="button" className="btn btn-primary" onClick={(e) => handleFileUpload(e, item._id)} data-toggle="modal"  data-target="#addDocumentDetail">
                                                  Marketing Calender Banner Not Setup
                                              </button>
                                              <p className="mb-0 mt-2" style={{ fontsize: "14px" }}>This banner will show on marketing calander page.</p>
                                          </div>
                                        </div>
                                      }
                                      {item.content && (
                                        <div className="">
                                          <label className="col-form-label">Content:</label>
                                          <span
                                            dangerouslySetInnerHTML={{
                                              __html: item.content,
                                            }}
                                          ></span>
                                        </div>
                                      )}
                                      
                                    </div>
                                  );

                                }
                              })
                            : 
                            <p>Content Data Not Found</p>}
                        </div>
                        
                      
                        <div className="tab-pane" id="tab4">
                        <div className="bg-light border rounded-3 p-3 mb-4 float-start w-100">
                            <span>Events Calander Default Content and Banner</span>
                          </div>
                          {formValues && formValues.length > 0
                            ? formValues.map((item) => {
                                if(item.page==="events"){
                                  return (
                                    <div className="bg-light border rounded-3 p-3 mb-4">
                                      
                                      <button
                                        onClick={(e) =>
                                          handleRemove(item._id, item.content)
                                        }
                                        className="btn btn-primary pull-right mt-0 m-3"
                                        type="button"
                                      >
                                        Delete
                                      </button>
  
                                      <button
                                        onClick={(e) =>
                                          handleEditContent(item._id, item.content)
                                        }
                                        className="btn btn-primary pull-right mt-0"
                                        type="button"
                                        data-toggle="modal"
                                        data-target="#market-calender-text"
                                      >
                                        Edit
                                      </button>
  
                                      <p>
                                        <label className="col-form-label">Month:</label> {item.month}
                                      </p>
  
                                      {
                                        item.marketingbanner ?
                                          <div className="d-flex">
                                            <b>Marketing Banner :</b>
                                              <div className="NotSetup Marquee text-center mb-4 mt-3 " onClick={(e) => handleFileUpload(e, item._id)} data-toggle="modal"  data-target="#addDocumentDetail">
                                                <img className = "img-fluid w-100" src={item.marketingbanner} alt="Document Preview" />
                                              </div>
                                          </div>
                                        :
                                        <div className="d-flex">
                                            <b>Marketing Banner :</b>
                                          <div className="NotSetup Marquee text-center mb-4 mt-3">
                                              <button type="button" className="btn btn-primary" onClick={(e) => handleFileUpload(e, item._id)} data-toggle="modal"  data-target="#addDocumentDetail">
                                                  Marketing Calender Banner Not Setup
                                              </button>
                                              <p className="mb-0 mt-2" style={{ fontsize: "14px" }}>This banner will show on marketing calander page.</p>
                                          </div>
                                        </div>
                                      }
                                      {item.content && (
                                        <div className="d-flex">
                                          <b>Content:</b>
                                          <span
                                            dangerouslySetInnerHTML={{
                                              __html: item.content,
                                            }}
                                          ></span>
                                        </div>
                                      )}
                                      
                                    </div>
                                  );

                                }
                              })
                            : ""}
                        </div>

                      </div>
                    </div>
                  </div>



                </div>
              </div>
            </div>
        </div>

        {contentPopUp ? (
          <div className="modal" role="dialog" id="market-calender-text">
            <div
              className="modal-dialog modal-md modal-dialog-scrollable"
              role="document"
            >
              <div className="modal-content">
                <div className="modal-header">
                  <h4 className="modal-title">Update Market Event Text</h4>
                </div>
                <div className="modal-body fs-sm">
                  <div className="col-md-12">
                    <div className="col-sm-12 mb-3">
                      <label className="form-label">Content</label>
                      <Editor
                        editorState={editorState}
                        onEditorStateChange={handleEditorChange}
                        // value={formValues.content?? contentText}
                        toolbar={{
                          options: [
                            "inline",
                            "blockType",
                            "fontSize",
                            "list",
                            "textAlign",
                            "history",
                            "link", // Add link option here
                          ],
                          inline: {
                            options: [
                              "bold",
                              "italic",
                              "underline",
                              "strikethrough",
                            ],
                          },
                          list: { options: ["unordered", "ordered"] },
                          textAlign: {
                            options: ["left", "center", "right"],
                          },
                          history: { options: ["undo", "redo"] },
                          link: {
                            // Configure link options
                            options: ["link", "unlink"],
                          },
                        }}
                        wrapperClassName="demo-wrapper"
                        editorClassName="demo-editor"
                        editorStyle={{ height: "400px" }}
                      />
                    </div>

                    <div className="pull-right mt-3">
                      {localStorage.getItem("auth") &&
                      jwt(localStorage.getItem("auth")).contactType ==
                        "admin" ? (
                        <button
                          onClick={(e) => handleSubmit(e)}
                          type="button"
                          className="btn btn-primary btn-sm "
                          id="save-button"
                        >
                          Update an Event
                        </button>
                      ) : (
                        ""
                      )}
                      <button
                        type="button"
                        className="btn btn-secondary btn-sm ms-3"
                        id="close"
                        onClick={handleClose}
                      >
                        Cancel & Close
                      </button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        ) : (
          ""
        )}


              <div className="modal in" role="dialog" id="addDocumentDetail">
                <div className="modal-dialog modal-dialog-centered modal-dialog-scrollable" role="document">
                    <div className="modal-content">
                      <div className="modal-header border-0 pb-0">
                          {/* <h4 className="modal-title"></h4> */}
                          <button className="btn-close" type="button" id="closeModal" data-dismiss="modal" aria-label="Close"></button>
                      </div>
                      <div className="modal-body fs-sm pt-0">
                          <h6>Please select or place a file to upload.</h6>
                          <input className="form-control"
                              id="inline-form-input"
                              type="file"
                              accept={acceptedFileTypes.map((type) => `.${type}`).join(',')}
                              name="marque_image"
                              onChange={handleFileUpload}
                              value={formValues.marque_image}
                          />
                      </div>
                    </div>
                </div>
              </div>    
      </main>
    </div>
  );
};

export default MarketingContentListing;

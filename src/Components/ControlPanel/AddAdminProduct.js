import React, { useState, useEffect, useRef } from "react";
import { useNavigate, NavLink, useParams } from "react-router-dom";
import user_service from "../service/user_service";
import Loader from "../../Pages/Loader/Loader.js";
import Toaster from "../../Pages/Toaster/Toaster.js";
import MultiValueTextInput from "react-multivalue-text-input";

import jwt from "jwt-decode";
import _ from "lodash";
import axios from "axios";
import { Editor } from "react-draft-wysiwyg";
import { EditorState, ContentState, convertToRaw } from "draft-js";
import draftToHtml from "draftjs-to-html";
import htmlToDraft from "html-to-draftjs";
import "react-draft-wysiwyg/dist/react-draft-wysiwyg.css";
import $, { event } from "jquery";
const AddAdminProduct = () => {
  const [loader, setLoader] = useState({ isActive: null });
  const { isActive } = loader;
  const [toaster, setToaster] = useState({
    types: null,
    isShow: null,
    toasterBody: "",
    message: "",
  });
  const { types, isShow, toasterBody, message } = toaster;

  const initialValues = {
    office: "",
    productname: "",
    productblurb: "",
    description: "",
    linkurl: "",
    email: "",
    contactName: "",
    phone: "",
    address:"",
    address2:"",
    city:"",
    state:"",
    zip:"",
    zipcode: [],
    product_fullDetails: "",
  };

  const initialImage = {
    image: [],
  };
  const [formValues, setFormValues] = useState(initialValues);

  const socialMediaTypes = [
    { type: "Twitter", linkurl: "" },
    { type: "Linkedin", linkurl: "" },
    { type: "Facebook", linkurl: "" },
    { type: "Instagram", linkurl: "" },
    { type: "Google", linkurl: "" },
    { type: "YouTube Channel", linkurl: "" },
  ];

  const [socialLinks, setSocialLinks] = useState(socialMediaTypes);

  const [imageData, setImageData] = useState(initialImage);
  const [formErrors, setFormErrors] = useState({});
  const [isSubmitClick, setISSubmitClick] = useState(false);

  const [mainNavLinkText, setMainNavLinkText] = useState("Corporate");
  const [files, setFiles] = useState([]);
  const [isLoading, setIsLoading] = useState(false);
  const [imageShow, setImageShow] = useState([]);
  const [acceptedFileTypes, setAcceptedFileTypes] = useState([]);
  const [promoteProduct, setPromoteProduct] = useState("");

  const navigate = useNavigate();
  const params = useParams();

  const [notesmessage, setNotesmessage] = useState("");
  const editorRef = useRef(null);
  const log = () => {
    if (editorRef.current) {
      setNotesmessage(editorRef.current.getContent());
    }
  };

  useEffect(() => {
    window.scrollTo(0, 0);
    const PromoteProductGetIdId = async () => {
      await user_service.promotoProductsGetId(params.id).then((response) => {
        setLoader({ isActive: false });
        if (response) {
          setPromoteProduct(response.data);
        }
      });
    };
    PromoteProductGetIdId();
  }, []);

  // useEffect(() => {
  //   window.scrollTo(0, 0);
  //   ProductItemsGetIdId();
  // }, []);
  
  // const ProductItemsGetIdId = async () => {
  //   if (params.addProduct) {
  //     await user_service
  //       .productItemsGetId(params.addProduct)
  //       .then((response) => {
  //         setLoader({ isActive: false });
  //         if (response) {
  //           // (response.data.image);
  //           const Promotedata = response.data;
  //           setFormValues(Promotedata);
  //           const contentBlock = htmlToDraft(
  //             response.data.product_fullDetails
  //           );
  //           if (contentBlock) {
  //             const contentState = ContentState.createFromBlockArray(
  //               contentBlock.contentBlocks
  //             );
  //             setEditorState(EditorState.createWithContent(contentState));
  //           }

  //           console.log(Promotedata);
  //           const newPromote = Promotedata.image;
  //           console.log(newPromote);
  //           setImageShow(newPromote);
  //         }
  //       });
  //   }
  //   else{
  //     await user_service
  //     .productItemsGetId(params.details)
  //     .then((response) => {
  //       setLoader({ isActive: false });
  //       if (response) {
  //         // (response.data.image);
  //         const Promotedata = response.data;
  //         setFormValues(Promotedata);
  //         const contentBlock = htmlToDraft(
  //           response.data.product_fullDetails
  //         );
  //         if (contentBlock) {
  //           const contentState = ContentState.createFromBlockArray(
  //             contentBlock.contentBlocks
  //           );
  //           setEditorState(EditorState.createWithContent(contentState));
  //         }

  //         console.log(Promotedata);
  //         const newPromote = Promotedata.image;
  //         console.log(newPromote);
  //         setImageShow(newPromote);
  //       }
  //     });
  //   }
  // };

  const handleMainNavLinkClick = (e, item) => {
    console.log(item);
    setMainNavLinkText(item);
    document.getElementById("closeLocation").click();
  };

  const handleChange = (e) => {
    const { name, value } = e.target;
    setFormValues({ ...formValues, [name]: value });

    $("input[name='phone']").keyup(function () {
      $(this).val(
        $(this)
          .val()
          .replace(/^(\d{3})(\d{3})(\d+)$/, "$1-$2-$3")
      );
      const formattedNumber = value.replace(
        /^(\d{3})(\d{3})(\d+)$/,
        "$1-$2-$3"
      );
      setFormValues({ ...formValues, ["phone"]: formattedNumber });
    });
  };

  const handleZipcodeChange = (values) => {
    setFormValues({ ...formValues, zipcode: values });
  };

  const handleChangeSocial = (event, index) => {
    const { value } = event.target;
    setSocialLinks((prevSocialLinks) => {
      const updatedSocialLinks = [...prevSocialLinks];
      updatedSocialLinks[index] = {
        ...updatedSocialLinks[index],
        linkurl: value,
      };
      return updatedSocialLinks;
    });
  };

  const [editorState, setEditorState] = useState(EditorState.createEmpty());

  const handleChanges = (editorState) => {
    setEditorState(editorState);
    const htmlContent = draftToHtml(
      convertToRaw(editorState.getCurrentContent())
    );
    setFormValues({ ...formValues, product_fullDetails: htmlContent });
  };


  useEffect(() => {
    if (formValues && isSubmitClick) {
      validate();
    }
  }, [formValues]);

  const validate = () => {
    const values = formValues;
    const errors = {};

    if (!values.productname) {
      errors.productname = "Product Name is required.";
    } else if (values.productname.length > 100) {
      errors.productname = "Product Name must be 100 characters or less.";
    }
    setFormErrors(errors);
    return errors;
  };

  /* <!-- Form Validation End--> */


  const handleSubmit = async (e) => {
    e.preventDefault();
    setISSubmitClick(true);
  
    let checkValue = validate();
    if (_.isEmpty(checkValue)) {
      const userData = {
        agentId: jwt(localStorage.getItem("auth")).id,
        office_id: localStorage.getItem("active_office_id"),
        office_name: localStorage.getItem("active_office"),
        category: promoteProduct.description,
        category_id: params.id,
        // Ensure subCategoryId is a string, even if params.details is undefined
        subCategoryId: params.details,
        productname: formValues.productname,
        productblurb: formValues.productblurb,
        description: formValues.description,
        linkurl: formValues.linkurl,
        email: formValues.email,
        contactName: formValues.contactName,
        phone: formValues.phone,
        address: formValues.address,
        address2: formValues.address2,
        city: formValues.city,
        state: formValues.state,
        zip: formValues.zip,
        social_links: socialLinks,
        zipcode: formValues.zipcode,
        product_fullDetails: formValues.product_fullDetails,
      };
      console.log(userData);
      try {
        setLoader({ isActive: true });
        const response = await user_service.addProductItems(userData);
        if (response) {
          console.log(response);
          setFormValues(response.data);
          setLoader({ isActive: false });
          setToaster({
            type: "Add ProductItems",
            isShow: true,
            toasterBody: response.data.message,
            message: "Add ProductItems Successfully",
          });
  
          // Redirect after a short delay
          setTimeout(() => {
            setToaster((prevToaster) => ({ ...prevToaster, isShow: false }));
            navigate(`/admin-product-items/${params.id}/${params.details}`);
          }, 1000);
        }
      } catch (error) {
        setLoader({ isActive: false });
        setToaster({
          types: "error",
          isShow: true,
          message: "This combination already exists. Please try again with a different product.",
        });
        setTimeout(() => {
          setToaster((prevToaster) => ({ ...prevToaster, isShow: false }));
        }, 2000);
      }
    }
  };
  
  return (
    <div className="bg-secondary float-left w-100 pt-4 mb-4">
      <Loader isActive={isActive} />
      {isShow && (
        <Toaster
          types={types}
          isShow={isShow}
          toasterBody={toasterBody}
          message={message}
        />
      )}
      <main className="page-wrapper profile_page_wrap">
        <div className="content-overlay">
          <div className="mb-3">
            {/* {params.addProduct || params.details ? (
              <>
                <h3 className="text-light mb-4">
                  Update {formValues.productname} Product
                </h3>
              
              </>
            ) : (
              <>
                <h3 className="text-light mb-4">
                  Add New Product to Ads & Marketing
                </h3>
              </>
            )} */}
               <h3 className="text-light mb-4">
                  Add New Product to Ads & Marketing
                </h3>
          </div>

          <div className="row">
            <div className="col-md-8">
              <div className="bg-light rounded-3 border p-3">
                <div className="col-md-12">
                  <h6>Publication Level</h6>
                  <label className="col-form-label">
                    At what location level is this document published?
                  </label>
                  <div className="d-flex">
                    <p>
                      {mainNavLinkText}&nbsp;&nbsp;
                      <NavLink>
                        <span
                          type="button"
                          data-toggle="modal"
                          data-target="#addLocation"
                        >
                          change
                        </span>
                      </NavLink>
                    </p>
                  </div>
                </div>

                <div className="col-md-12">
                  <label className="col-form-label">Product Name *</label>
                  <small className="pull-right">
                    Uniquely identifies product. (Max length: 100 chars)
                  </small>
                  <input
                    className="form-control"
                    id="inline-form-input"
                    type="text"
                    name="productname"
                    value={formValues.productname}
                    onChange={handleChange}
                  />
                  {formErrors.productname && (
                    <div className="invalid-tooltip">
                      {formErrors.productname}
                    </div>
                  )}
                </div>

                <div className="col-md-12 mt-3">
                  <label className="col-form-label">Product Blurb</label>
                  <small className="pull-right">
                    Very short summary. (Max length: 50 chars)
                  </small>
                  <input
                    className="form-control"
                    id="inline-form-input"
                    type="text"
                    name="productblurb"
                    value={formValues.productblurb}
                    onChange={handleChange}
                  />
                </div>

                <div className="col-md-12 mt-3">
                  <label className="col-form-label">Product Description</label>
                  <small className="pull-right">
                    Brief info, shown on popup detail box. (Max length: 500
                    chars)
                  </small>
                  <textarea
                    className="form-control mt-0"
                    id="textarea-input"
                    rows="5"
                    name="description"
                    value={formValues.description}
                    onChange={handleChange}
                  ></textarea>
                </div>

                <div className="col-md-12 mt-3">
                  <label className="col-form-label">Product Link URL</label>
                  <small className="pull-right">
                    Hyperlink for product or service, SSL preferred. (Max: 4,000
                    chars)
                  </small>
                  <input
                    className="form-control"
                    id="inline-form-input"
                    type="text"
                    name="linkurl"
                    value={formValues.linkurl}
                    onChange={handleChange}
                  />
                </div>

                {/* <div className="col-md-12 mt-3">
                  <label className="col-form-label">Product Quick Details</label>
                  <span className="pull-right">
                    The key benefits or difference of this product. (6 line
                    items)
                  </span>
                  {formValues.product_quickDetails.map((item, index) => (
                    <div key={index} className="mb-3">
                      <input
                        className="form-control"
                        type="text"
                        name="quickDetails"
                        value={item.quickDetails}
                        onChange={(e) => handleChangesDetails(e, index)}
                      />
                    </div>
                  ))}
                </div> */}

                <div className="col-md-12 mt-3">
                  <label className="col-form-label">ContactName</label>
                  <input
                    className="form-control"
                    id="inline-form-input"
                    type="text"
                    name="contactName"
                    value={formValues.contactName}
                    onChange={handleChange}
                  />
                </div>

                <div className="col-md-12 mt-3">
                  <label className="col-form-label">Email</label>
                  <input
                    className="form-control"
                    id="inline-form-input"
                    type="text"
                    name="email"
                    value={formValues.email}
                    onChange={handleChange}
                  />
                </div>

                <div className="row mt-3">
                  <div className="col-md-6 ">
                    <label className="col-form-label">Street Address</label>
                    <input
                      className="form-control"
                      id="inline-form-input"
                      type="text"
                      name="address"
                      placeholder="Enter Street Address"
                      value={formValues.address}
                      onChange={handleChange}
                    />
                  </div>
                  <div className="col-md-6 ">
                    <label className="col-form-label">Street Address 2</label>
                    <input
                      className="form-control"
                      id="inline-form-input"
                      type="text"
                      name="address2"
                      placeholder="Enter Street Address 2"
                      value={formValues.address2}
                      onChange={handleChange}
                    />
                  </div>

                  <div className="col-md-6 ">
                    <label className="col-form-label">City</label>
                    <input
                      className="form-control"
                      id="inline-form-input"
                      type="text"
                      name="city"
                      placeholder="Enter City"
                      value={formValues.city}
                      onChange={handleChange}
                    />
                  </div>

                  <div className="col-md-6 ">
                    <label className="col-form-label">State</label>
                    <input
                      className="form-control"
                      id="inline-form-input"
                      type="text"
                      name="state"
                      placeholder="Enter State"
                      value={formValues.state}
                      onChange={handleChange}
                    />
                  </div>
                  <div className="col-md-6 ">
                    <label className="col-form-label">Zipcode</label>
                    <input
                      className="form-control"
                      id="inline-form-input"
                      type="text"
                      name="zip"
                      placeholder="Enter Zipcode"
                      value={formValues.zip}
                      onChange={handleChange}
                    />
                  </div>

                </div>

                <div className="col-md-12 mt-3">
                  <label className="col-form-label">Phone</label>
                  <input
                    className="form-control"
                    id="inline-form-input"
                    type="text"
                    name="phone"
                    value={formValues.phone}
                    onChange={handleChange}
                  />
                </div>

                <div className="col-sm-12 mt-3">
                  <label className="col-form-label">Served Areas</label>
                  <MultiValueTextInput
                    values={formValues.zipcode}
                    onItemAdded={(item, allItems) =>
                      handleZipcodeChange(allItems)
                    }
                    onItemDeleted={(item, allItems) =>
                      handleZipcodeChange(allItems)
                    }
                    className="form-control"
                    id="inline-form-input"
                    placeholder="Enter Areas separated by commas"
                  />
                </div>

                 <div className="row mt-3">
                    <label className="col-form-label">
                        Social Media Channels
                      </label>
                      {socialLinks.map((socialLink, index) => (
                        <div className="col-md-6" key={index}>
                          <label className="form-label">
                            {socialLink.type} URL
                          </label>
                          <input
                            className="form-control"
                            type="text"
                            name="linkurl"
                            value={socialLink.linkurl}
                            onChange={(event) =>
                              handleChangeSocial(event, index)
                            }
                          />
                        </div>
                      ))}
                </div>


                <div className="col-md-12 mt-3">
                  <label className="col-form-label">Product Full Details</label>
                  <small className="pull-right">
                    Full product details shown on detail page only. (No length
                    limit)
                  </small>

                  <Editor
                    editorState={editorState}
                    onEditorStateChange={handleChanges}
                    value={formValues.product_fullDetails}
                    toolbar={{
                      options: [
                        "inline",
                        "blockType",
                        "fontSize",
                        "list",
                        "textAlign",
                        "history",
                        "link", // Add link option here
                      ],
                      inline: {
                        options: [
                          "bold",
                          "italic",
                          "underline",
                          "strikethrough",
                        ],
                      },
                      list: { options: ["unordered", "ordered"] },
                      textAlign: {
                        options: ["left", "center", "right"],
                      },
                      history: { options: ["undo", "redo"] },
                      link: {
                        // Configure link options
                        options: ["link", "unlink"],
                      },
                    }}
                    wrapperClassName="demo-wrapper"
                    editorClassName="demo-editor"
                  />

                  <hr />
                  <p className="mt-4">
                    Options to add photos will be available after the new
                    product has been created.
                  </p>
                </div>
              </div>
              <div className="pull-right mt-4">
              {/* {params.addProduct || params.details ? (
                  <>
                    <button
                      className="btn btn-primary pull-right ms-2"
                      type="button"
                      onClick={handleSubmit}
                    >
                      Update Product in
                    </button>
                  </>
                ) : (
                  <button
                  className="btn btn-primary pull-right ms-2"
                  type="button"
                  onClick={handleSubmit}
                >
                  Add Product
                </button>
                )} */}
                  <button
                    className="btn btn-primary pull-right ms-2"
                    type="button"
                    onClick={handleSubmit}
                  >
                    Add Product
                  </button>
                   {/* {params.addProduct || params.details ? (
                  <>
                    <button
                      className="btn btn-primary pull-right ms-2"
                      type="button"
                      onClick={handleRemoveProduct}
                    >
                      Remove Product
                    </button>
                  </>
                ) : (
                  ""
                )} */}

                <NavLink
                  className="btn btn-secondary pull-right ms-2"
                  type="button"
                  to={`/admin-product-items/${params.id}/${params.details}`}
                  // onClick={handleSubmit}
                >
                  Cancel
                </NavLink>
              </div>
            </div>
            {/* {params.addProduct || params.details ? (
              <div className="col-md-4 mt-lg-0 mt-md-0 mt-sm-4 mt-4">
                <div className="bg-light rounded-3 border p-3">
                  <div className="">
                    <h6>Product Photos</h6>
                    <div className="my-3 file-uploader pt-3">
                      <label className="col-form-label">
                        Select a photo to upload:
                      </label>
                      <br />
                      <input
                        id="fileUpload"
                        type="file"
                        name="image"
                        onChange={handleFileChange}
                        accept={acceptedFileTypes
                          .map((type) => `.${type}`)
                          .join(",")}
                      />
                    </div>

                    <div className="productItemImage">
                      {imageData.image.length > 0 && (
                        <>
                          {imageData.image.map((uploadedImage, index) => (
                            <>
                              {uploadedImage ? (
                                <div className="product_images">
                                  <img
                                    className="m-0 mb-3 shadow-sm"
                                    src={uploadedImage.images}
                                  />
                                  <span className="delete_data">
                                    <img
                                      className="m-0"
                                      onClick={(e) =>
                                        handleRemove(uploadedImage.images)
                                      }
                                      src="https://cdn.pboffice.net/shared/transaction/page_select_drop.png"
                                    />
                                  </span>
                                </div>
                              ) : (
                                ""
                              )}
                            </>
                          ))}
                        </>
                      )}
                    </div>

                    <div className="productItemImage">
                      {imageShow
                        ? imageShow.map((uploadedImage, index) =>
                            uploadedImage ? (
                              <div className="product_images">
                                <img
                                  className="m-0 mb-3 shadow-sm"
                                  src={uploadedImage.images}
                                />
                                <span className="delete_data">
                                  <img
                                    className="m-0"
                                    onClick={() => handleRemoveApiImage(index)}
                                    src="https://cdn.pboffice.net/shared/transaction/page_select_drop.png"
                                  />
                                </span>
                              </div>
                            ) : null
                          )
                        : ""}
                    </div>

                    {isLoading && <p>Uploading...</p>}
                  </div>
                </div>
                <div className="pull-right mt-3">
                  {imageData.image.length > 0 ? (
                    <>
                      <button
                        className="btn btn-primary pull-right"
                        type="button"
                        onClick={handleSubmitImage}
                      >
                        Image Submit
                      </button>
                    </>
                  ) : (
                    ""
                  )}
                </div>
              </div>
            ) : (
              ""
            )} */}
          </div>
        </div>
      </main>
      <div className="modal in" role="dialog" id="addLocation">
        <div
          className="modal-dialog modal-lg modal-dialog-scrollable"
          role="document"
        >
          <div className="modal-content">
            <div className="modal-header border">
              {/* <h4 className="modal-title">Add Contact</h4> */}
              <button
                className="btn-close"
                type="button"
                data-dismiss="modal"
                aria-label="Close"
              ></button>
            </div>
            <div className="modal-body fs-sm">
              <div className="content-overlay">
                <div className="bg-light">
                  <div className="row">
                    <div>
                      <h6>Choose the publication level from the tree.</h6>
                      <div className="pull-right">
                        <NavLink className="btn btn-primary">
                          <span
                            onClick={(e) =>
                              handleMainNavLinkClick(e, "Corporate")
                            }
                          >
                            Corporate
                          </span>
                        </NavLink>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default AddAdminProduct;

import React, { useState, useEffect } from "react";
import user_service from "../service/user_service.js";
import Loader from "../../Pages/Loader/Loader.js";
import Toaster from "../../Pages/Toaster/Toaster.js";
import { NavLink, useNavigate, useParams } from "react-router-dom";
import jwt from "jwt-decode";
import { Editor } from "react-draft-wysiwyg";
import { EditorState, ContentState, convertToRaw } from "draft-js";
import draftToHtml from "draftjs-to-html";
import htmlToDraft from "html-to-draftjs";
import "react-draft-wysiwyg/dist/react-draft-wysiwyg.css";
import axios from "axios";
import _ from "lodash";
import Swal from "sweetalert2";
import "sweetalert2/dist/sweetalert2.css";

const MarketingContent = () => {
  const [loader, setLoader] = useState({ isActive: null });
  const { isActive } = loader;
  const [acceptedFileTypes, setAcceptedFileTypes] = useState([]);
  const [marketingbanner, setMarketingbanner] = useState("");

  const [toaster, setToaster] = useState({
    types: null,
    isShow: null,
    toasterBody: "",
    message: "",
  });
  const { types, isShow, toasterBody, message } = toaster;

  const [formValues, setFormValues] = useState({
    content: "",
    month: "",
    page: "",
  });

  const params = useParams();

  console.log(params);

  const navigate = useNavigate();

  const [editorState, setEditorState] = useState(EditorState.createEmpty());

  const [formErrors, setFormErrors] = useState({});
  const [isSubmitClick, setISSubmitClick] = useState(false);

  const PageContent = async () => {
    try {
      if (params.id) {
        const response = await user_service.pageContentMarketingGetId(
          params.id
        );
        if (response && response.data) {
          const contentBlock = htmlToDraft(response.data.content);
          if (contentBlock) {
            const contentState = ContentState.createFromBlockArray(
              contentBlock.contentBlocks
            );
            setEditorState(EditorState.createWithContent(contentState));
          }
          setFormValues(response.data);
        }
      }
    } catch (error) {
      console.error("Error fetching page content:", error);
    }
  };

  useEffect(() => {
    window.scrollTo(0, 0);
    PageContent();
  }, []);

  const handleChanges = (e) => {
    const { name, value } = e.target;
    setFormValues({ ...formValues, [name]: value });
  };

  const handleChange = (editorState) => {
    setEditorState(editorState);
    const htmlContent = draftToHtml(
      convertToRaw(editorState.getCurrentContent())
    );
    setFormValues({ ...formValues, content: htmlContent });
  };

  const validate = () => {
    const values = formValues;
    const errors = {};
    if (!values.month) {
      errors.month = "Please Select month";
    }

    if (!values.content) {
      errors.page = "Please enter any content";
    }

    if (!values.page) {
      errors.page = "Please select the calander type";
    }
    setFormErrors(errors);
    return errors;
  };

  useEffect(() => {
    if (formValues && isSubmitClick) {
      validate();
    }
  }, [formValues]);

  const handleSubmit = async (e) => {
    if (params.id) {
      e.preventDefault();
      let checkValue = validate();
      if (_.isEmpty(checkValue)) {
        const userData = {
          agentId: jwt(localStorage.getItem("auth")).id,
          content: formValues.content,
          marketingbanner: marketingbanner,
          month: formValues.month,
          page: formValues.page,
          created: new Date().toISOString(),
          status: "1",
        };
        console.log(userData);
        setLoader({ isActive: true });
        await user_service
          .pageContentMarketingUpdate(params.id, userData)
          .then((response) => {
            if (response) {
              setFormValues(response.data);
              setLoader({ isActive: false });
              setToaster({
                type: "Content Added",
                isShow: true,
                toasterBody: response.data.message,
                message: "Content Added Successfully",
              });
              setTimeout(() => {
                setToaster((prevToaster) => ({
                  ...prevToaster,
                  isShow: false,
                }));
                navigate("/marketing-content-listing");
              }, 1000);
            } else {
              setLoader({ isActive: false });
              setToaster({
                types: "error",
                isShow: true,
                toasterBody: response.data.message,
                message: "Error",
              });
            }
          });
      }
    } else {
      e.preventDefault();
      let checkValue = validate();
      if (_.isEmpty(checkValue)) {
        const userData = {
          agentId: jwt(localStorage.getItem("auth")).id,
          content: formValues.content,
          marketingbanner: marketingbanner,
          month: formValues.month,
          page: formValues.page,
          created: new Date().toISOString(),
          status: "1",
        };
        console.log(userData);
        setLoader({ isActive: true });
        await user_service.pageContentMarketing(userData).then((response) => {
          if (response) {
            setFormValues(response.data);
            setLoader({ isActive: false });
            setToaster({
              type: "Content Added",
              isShow: true,
              toasterBody: response.data.message,
              message: "Content Added Successfully",
            });
            setTimeout(() => {
              setToaster((prevToaster) => ({ ...prevToaster, isShow: false }));
              navigate("/marketing-content-listing");
            }, 1000);
          } else {
            setLoader({ isActive: false });
            setToaster({
              types: "error",
              isShow: true,
              toasterBody: response.data.message,
              message: "Error",
            });
          }
        });
      }
    }
  };

  const handleFileUpload = async (e, id) => {
    const selectedFile = e.target.files;

    if (selectedFile && selectedFile[0]) {
      const fileExtension = selectedFile[0].name.split(".").pop(); // Access the file extension

      const formData = new FormData();
      formData.append("file", selectedFile[0]);
      const config = {
        headers: {
          Accept: "application/json",
          "Content-Type": "multipart/form-data",
          Authorization: `Bearer ${localStorage.getItem("auth")}`,
        },
      };

      try {
        setLoader({ isActive: true });
        const uploadResponse = await axios.post(
          "https://api.brokeragentbase.com/upload",
          formData,
          config
        );
        // const uploadResponse = await axios.post(
        //   "http://localhost:4000/upload",
        //   formData,
        //   config
        // );
        const uploadedFileData = uploadResponse.data;
        setLoader({ isActive: false });
        setMarketingbanner(uploadedFileData);
        document.getElementById("closeModal").click();
      } catch (error) {
        console.error("Error occurred during file upload:", error);
      }
    } else {
      console.log("No file selected.");
    }
  };

  const handleDeleteContent = async () => {
    if (params.id) {
      const confirmation = await Swal.fire({
        title: "Are you sure?",
        text: "It will be permanently deleted!",
        icon: "warning",
        showCancelButton: true,
        confirmButtonColor: "#3085d6",
        cancelButtonColor: "#d33",
        confirmButtonText: "Yes, delete it!",
      });

      if (confirmation.isConfirmed) {
        setLoader({ isActive: true });

        await user_service
          .pageContentMarketingDelete(params.id)
          .then((response) => {
            if (response) {
              setLoader({ isActive: false });
              // setDateData("");
              setToaster({
                type: "success",
                isShow: true,
                message: "Content Deleted Successfully",
              });
              setTimeout(() => {
                setToaster((prevToaster) => ({
                  ...prevToaster,
                  isShow: false,
                }));
                navigate(`/control-panel/`);
              }, 1000);
            }
          });
      }
    }
  };

  return (
    <div className="bg-secondary float-left w-100 pt-4">
      <Loader isActive={isActive} />
      {isShow && (
        <Toaster
          types={types}
          isShow={isShow}
          toasterBody={toasterBody}
          message={message}
        />
      )}
      <main className="page-wrapper">
        {/* <!-- Page content--> */}
        {/* <!-- Page container--> */}
        <div className="">
          {/* <!-- Page card like wrapper--> */}

          <div className="">
            {/* <!-- Content--> */}
            <div className="">
              <div className="d-flex align-items-center justify-content-start mb-4">
                {params.id ? (
                  <h3 className="text-white mb-0">
                    Update Marketing Calender Content
                  </h3>
                ) : (
                  <h3 className="text-white mb-0">
                    Add Marketing / Event Calender Content
                  </h3>
                )}
              </div>
              <div className="row">
                <div className="col-md-12">
                  <div className="bg-light border rounded-3 p-3">
                    <label className="form-label mb-3">
                      Please select a month for which you want to add content
                      and banner.
                    </label>
                    <div className="col-sm-6 mb-4">
                      <select
                        className="form-select"
                        name="month"
                        value={formValues.month}
                        onChange={handleChanges}
                      >
                        <option value="">Select Month</option>
                        <option value="January">January</option>
                        <option value="February">February</option>
                        <option value="March">March</option>
                        <option value="April">April</option>
                        <option value="May">May</option>
                        <option value="June">June</option>
                        <option value="July">July</option>
                        <option value="August">August</option>
                        <option value="September">September</option>
                        <option value="October">October</option>
                        <option value="November">November</option>
                        <option value="December">December</option>
                      </select>
                      <div className="invalid-tooltip">{formErrors.month}</div>
                    </div>

                    <label className="form-label mb-3">
                      Please select the calender type.
                    </label>
                    <div className="col-sm-6 mb-4">
                      <select
                        className="form-select"
                        name="page"
                        value={formValues.page}
                        onChange={handleChanges}
                      >
                        <option value="">Please Select Calander type</option>
                        <option value="marketing">Marketing Calender</option>
                        <option value="events">Events Calender</option>
                      </select>
                      <div className="invalid-tooltip">{formErrors.page}</div>
                    </div>
                    <div className="col-sm-12 mb-3">
                      <label className="form-label mb-3">
                        Marketing Calander Top Banner
                      </label>
                      <br />
                      {marketingbanner ? (
                        <></>
                      ) : (
                        <>
                          {formValues.marketingbanner ? (
                            <img
                              src={formValues.marketingbanner}
                              className="mb-2"
                            />
                          ) : (
                            ""
                          )}
                        </>
                      )}
                      {marketingbanner ? (
                        <div
                          className="NotSetup Marquee text-center mb-4"
                          data-toggle="modal"
                          data-target="#addDocumentDetail"
                        >
                          <img
                            className="img-fluid w-100"
                            src={marketingbanner}
                            alt="Document Preview"
                          />
                        </div>
                      ) : (
                        <div className="NotSetup Marquee text-center mb-4">
                          <button
                            type="button"
                            className="btn btn-primary"
                            data-toggle="modal"
                            data-target="#addDocumentDetail"
                          >
                            Marketing Calender Banner Upload
                          </button>
                          <p className="mb-0 mt-2" style={{ fontsize: "14px" }}>
                            This banner will show on marketing calander page.
                          </p>
                        </div>
                      )}
                    </div>

                    <div className="col-sm-12 mb-3 mt-3">
                      <label className="form-label">Content</label>
                      <Editor
                        editorState={editorState}
                        onEditorStateChange={handleChange}
                        value={formValues.content}
                        toolbar={{
                          options: [
                            "inline",
                            "blockType",
                            "fontSize",
                            "list",
                            "textAlign",
                            "history",
                            "link", // Add link option here
                          ],
                          inline: {
                            options: [
                              "bold",
                              "italic",
                              "underline",
                              "strikethrough",
                            ],
                          },
                          list: { options: ["unordered", "ordered"] },
                          textAlign: {
                            options: ["left", "center", "right"],
                          },
                          history: { options: ["undo", "redo"] },
                          link: {
                            // Configure link options
                            options: ["link", "unlink"],
                          },
                        }}
                        wrapperClassName="demo-wrapper"
                        editorClassName="demo-editor"
                        editorStyle={{ height: "400px" }}
                      />
                      <div className="invalid-tooltip">
                        {formErrors.content}
                      </div>
                    </div>
                  </div>
                  <div className="pull-right mt-3">
                    <NavLink
                      type="button"
                      className="btn btn-secondary pull-right ms-3"
                      to="/control-panel"
                    >
                      Cancel
                    </NavLink>
                    {params.id ? (
                      <button
                        className="btn btn-primary"
                        type="button"
                        onClick={handleSubmit}
                      >
                        Update Marketing Content
                      </button>
                    ) : (
                      <button
                        className="btn btn-primary"
                        type="button"
                        onClick={handleSubmit}
                      >
                        Add Marketing Content
                      </button>
                    )}

                    {params.id ? (
                      <button
                        className="btn btn-primary ms-3"
                        type="button"
                        onClick={handleDeleteContent}
                      >
                        Delete Marketing Content
                      </button>
                    ) : (
                      ""
                    )}
                  </div>
                </div>
              </div>
            </div>

            <div className="modal in" role="dialog" id="addDocumentDetail">
              <div
                className="modal-dialog modal-dialog-centered modal-dialog-scrollable"
                role="document"
              >
                <div className="modal-content">
                  <div className="modal-header border-0 pb-0">
                    {/* <h4 className="modal-title"></h4> */}
                    <button
                      className="btn-close"
                      type="button"
                      id="closeModal"
                      data-dismiss="modal"
                      aria-label="Close"
                    ></button>
                  </div>
                  <div className="modal-body fs-sm pt-0">
                    <h6>Please select or place a file to upload.</h6>
                    <input
                      className="form-control"
                      id="inline-form-input"
                      type="file"
                      accept={acceptedFileTypes
                        .map((type) => `.${type}`)
                        .join(",")}
                      name="marque_image"
                      onChange={handleFileUpload}
                      value={formValues.marque_image}
                    />
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </main>
    </div>
  );
};

export default MarketingContent;

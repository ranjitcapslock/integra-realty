import React, { useState, useEffect } from "react";
import _ from "lodash";
import user_service from "../service/user_service";
import jwt from "jwt-decode";
import Loader from "../../Pages/Loader/Loader.js";
import Toaster from "../../Pages/Toaster/Toaster.js";
import { useNavigate, NavLink, useParams } from "react-router-dom";
import axios from "axios";

function MlsBoardDocument() {
  const initialValues = { 
    mlsMembership: "",
    boardMembership: "",
    documentName: "firstDocument",
    document: "",
    image: "",
  };

  const [formValues, setFormValues] = useState(initialValues);
  const [formErrors, setFormErrors] = useState({});
  const [isSubmitClick, setISSubmitClick] = useState(false);
  const [onBoardGet, setOnBoardGet] = useState();

  const [file, setFile] = useState(null);
  const [fileExtension, setFileExtension] = useState('');
  const [acceptedFileTypes, setAcceptedFileTypes] = useState([]);
  const [showSubmitButton, setShowSubmitButton] = useState(false);
  const [dataNew, setDataNew] = useState("")
  const [loader, setLoader] = useState({ isActive: null });
  const { isActive } = loader;
  const [toaster, setToaster] = useState({
    types: null,
    isShow: null,
    toasterBody: "",
    message: "",
  });
  const { types, isShow, toasterBody, message } = toaster;

  const navigate = useNavigate();
  const params = useParams();

  // onChange Function start
  const handleChange = (e) => {
    const { name, value } = e.target;
    setFormValues({ ...formValues, [name]: value });
  };
  // onChange Function end

  {
    /* <!-- Form Validation Start--> */
  }
  useEffect(() => {
    if (formValues && isSubmitClick) {
      validate();
    }
  }, [formValues]);

  const validate = () => {
    const values = formValues;
    const errors = {};
    if (!values.mlsMembership) {
      errors.mlsMembership = "Mls Membership is required";
    }

    if (!values.boardMembership) {
      errors.boardMembership = "Board Membership is required";
    }

    // if (!values.image) {
    //     errors.image= "File  is required";
    //   }

    if (!values.document) {
        errors.document= "Document name is required";
      }

    setFormErrors(errors);
    return errors;
  };
  {
    /* <!-- Form Validation End--> */
  }

  const handleSubmit = async (e) => {
    e.preventDefault();
    setISSubmitClick(true);
    let checkValue = validate();
    if (_.isEmpty(checkValue)) {
      const userData = {
        agentId: jwt(localStorage.getItem("auth")).id,
        office_id: localStorage.getItem("active_office_id"),
        office_name: localStorage.getItem("active_office"),
        mlsMembership: formValues.mlsMembership,
        boardMembership: formValues.boardMembership,
        documentName: formValues.documentName,
        document: formValues.document,
        image: dataNew.toString(),
      };

      const userData1 = {
        paperwork_location: "Corporate",
        paperwork_title: formValues.document,
        paperwork_formurl: dataNew.toString(),
        required: "yes",
        can_agentupload: "yes",
        paperwork_expire: "never",
        // custom_expire_year: formValues.custom_expire_year,
        is_active: "1",
        agentId: jwt(localStorage.getItem("auth")).id,
    };
    // console.log(userData1)
    // setLoader({ isActive: true })
    
    //   console.log(userData);
      try {
        setLoader({ isActive: true });
        const response = await user_service.mlsBoardDocument(userData);
        if (response) {

          const response2 = await user_service.paperworkAdd(userData1);


          setFormValues(response.data);
          setLoader({ isActive: false });
          setToaster({
            type: "Document Successfully",
            isShow: true,
            toasterBody: "Document Add Successfully",
            message: "Document Add Successfully",
          });

          MlsBoardDocumentGetAll();
          setTimeout(() => {
            setToaster((prevToaster) => ({ ...prevToaster, isShow: false }));
          }, 2000);
        }
      }  catch (error) {
        setLoader({ isActive: false });
        setToaster({
          types: "error",
          isShow: true,
          toasterBody: error.response
            ? error.response.data.message
            : error.message,
          message: "Error",
        });
        setTimeout(() => {
          setToaster((prevToaster) => ({ ...prevToaster, isShow: false }));
        }, 3000);
      }
    }
  };

  {
    /* <!-- Api call Form onSubmit End--> */
  }

  const MlsBoardDocumentGetAll = async () => {
    setLoader({ isActive: true });
    await user_service.mlsBoardDocumentGet().then((response) => {
      setLoader({ isActive: false });
      if (response) {
        setOnBoardGet(response.data.data);
      }
    });
  };

  useEffect(() => {
    MlsBoardDocumentGetAll();
  }, []);

  const handleFileUpload = async (e) => {
    const selectedFile = e.target.files[0];

    if (selectedFile) {
      setFile(selectedFile);
      setFileExtension(selectedFile.name.split(".").pop());
      setShowSubmitButton(true);

      const formData = new FormData();
      formData.append("file", selectedFile);

      const config = {
        headers: {
          Accept: "application/json",
          "Content-Type": "multipart/form-data",
          Authorization: `Bearer ${localStorage.getItem("auth")}`,
        },
      };

      try {
        const uploadResponse = await axios.post(
          "https://api.brokeragentbase.com/upload",
          formData,
          config
        );
        const uploadedFileData = uploadResponse.data;
        console.log(uploadedFileData);
        setDataNew(uploadedFileData);
        setLoader({ isActive: false });
              setToaster({
                type: "Document Successfully",
                isShow: true,
                toasterBody: "Document Add Successfully",
                message: "Document Add Successfully",
              });
              setTimeout(() => {
                setToaster((prevToaster) => ({ ...prevToaster, isShow: false }));
              }, 2000);

       
      } catch (error) {
        console.error("Error occurred during file upload:", error);
        setLoader({ isActive: false });
      }
    }
  };

  return (
    <div className="bg-secondary float-left w-100 pt-4 mb-4">
      <Loader isActive={isActive} />
      {isShow && (
        <Toaster
          types={types}
          isShow={isShow}
          toasterBody={toasterBody}
          message={message}
        />
      )}
      <main className="page-wrapper profile_page_wrap">
        <div className="content-overlay">
          <div className="justify-content-center pb-sm-2">
            <div className="add_listing w-100">
              <div className="float-left w-100 d-flex align-items-center justify-content-between mb-4">
                <h3 className="text-white mb-0">Documents</h3>
              </div>
              {/* <hr className="my-3 w-100" /> */}
              <div className="row">
                <div className="col-md-10">
                  <div className="border bg-light rounded-3 p-3">
                    <div className="float-left w-100">
                      <div className="col-sm-12">
                        <label className="form-label">MLS</label>
                        <select
                          className="form-select"
                          id="pr-city"
                          name="mlsMembership"
                          onChange={handleChange}
                          value={formValues.mlsMembership}
                          style={{
                            border: formErrors?.mlsMembership
                              ? "1px solid red"
                              : "1px solid #00000026",
                          }}
                        >
                          <option>--select option--</option>
                          <option value="UtahRealEstate.com">
                            UtahRealEstate (Utah)
                          </option>
                          <option value="Park City">Park City</option>
                          <option value="Washington County">
                            Washington County
                          </option>
                          <option value="Iron County">Iron County</option>
                          <option value="Tooele County">Tooele County</option>
                          <option value="Cache Valley">Cache Valley</option>
                          <option value="Brigham-Tremonton Board">
                            Brigham-Tremonton Board
                          </option>
                        </select>

                        {formErrors.mlsMembership && (
                          <div className="invalid-tooltip">
                            {formErrors.mlsMembership}
                          </div>
                        )}
                      </div>

                      <div className="col-sm-12 mt-3">
                        <label className="form-label">Board</label>
                        <select
                          className="form-select"
                          id="pr-city"
                          name="boardMembership"
                          onChange={handleChange}
                          value={formValues.boardMembership}
                          style={{
                            border: formErrors?.boardMembership
                              ? "1px solid red"
                              : "1px solid #00000026",
                          }}
                        >
                          <option>--select option--</option>
                          <option value="Salt Lake Board">
                            Salt Lake Board
                          </option>
                          <option value="Utah Central Association UCAR">
                            Utah Central Association UCAR
                          </option>
                          <option value="Northern Wasatch">
                            Northern Wasatch
                          </option>
                          <option value="Washington County">
                            Washington County
                          </option>
                          <option value="Park City">Park City</option>
                          <option value="Iron County">Iron County</option>
                        </select>

                        {formErrors.boardMembership && (
                          <div className="invalid-tooltip">
                            {formErrors.boardMembership}
                          </div>
                        )}
                      </div>

                       
                      <div className="col-md-12 mt-3 d-flex">
                      <label className="btn btn-primary documentlabel">
                              <input
                                id="REPC_real_estate_purchase_contract"
                                type="file"
                                accept={acceptedFileTypes
                                  .map((type) => `.${type}`)
                                  .join(",")}
                                name="file"
                                onChange={handleFileUpload}
                                // value={formValues.image}
                                />

                              <i className="fi-plus me-2"></i> Upload Document
                             
                             
                            </label>
                            <div className="ms-3">
                            {
                                  dataNew ?
                                  <>
                                  <i className="fi-check text-primary me-2"></i>
                                
                                </>
                                :""

                              }
                              </div>

                        {/* <p className="mt-0 p-2"><strong>{profile.name}</strong></p> */}
                      </div>

                      <div className="col-md-12 mt-3">
                      <label className="form-label">Document Name</label>
                      <input
                            className="form-control form-control-lg"
                            id="inline-form-input"
                            type="text"
                            name="document"
                            placeholder="Enter document name"
                            value={formValues.document}
                            onChange={handleChange}
                            style={{
                                border: formErrors?.document
                                  ? "1px solid red"
                                  : "1px solid #00000026",
                              }}
                            />
                              {formErrors.document && (
                          <div className="invalid-tooltip">
                            {formErrors.document}
                          </div>
                        )}
                      </div>

                      <div className="col-sm-12 d-none">
                        <label className="form-label">Document</label>
                        <select
                          className="form-select"
                          id="pr-city"
                          name="documentName"
                          onChange={handleChange}
                          value={formValues.documentName}
                          style={{
                            border: formErrors?.documentName
                              ? "1px solid red"
                              : "1px solid #00000026",
                          }}
                        >
                          <option value="firstDocument">FirstDocument</option>
                          <option value="secondDocument">SecondDocument</option>
                          <option value="thirdDocument">ThirdDocument</option>
                          <option value="fourthDocument">FourthDocument</option>
                          <option value="fifthDocument">FifthDocument</option>
                          <option value="sixDocument">SixDocument</option>
                        </select>

                        {formErrors.documentName && (
                          <div className="invalid-tooltip">
                            {formErrors.documentName}
                          </div>
                        )}
                      </div>
                    </div>
                  </div>
                  {
                      dataNew ?

                  <div className="pull-right mt-3">
                    <button
                      type="button"
                      className="btn btn-primary btn-sm float-right pull-right"
                      onClick={handleSubmit}
                    >
                      Submit
                    </button>
                  </div>
                  :""
                  }
             

                {/* <div className="col-md-4">
                  <div id="accordionCards" className="">
                    <div className="card bg-secondary mb-2 mt-0">
                      <div className="card-body">
                        <h3 className="h3 card-title pt-1 mb-0">
                          ❤️ We love your suggestions!
                        </h3>
                      </div>
                      <div
                        className="collapse show"
                        id="cardCollapse1"
                        data-bs-parent="#accordionCards"
                      >
                        <div className="card-body mt-n1 pt-0">
                          <p className="fs-sm">
                            <strong>
                              Indepth Realty are committed to community driven
                              features
                            </strong>
                            <br />
                            <br />
                            Your input is invaluable, and we're always looking
                            for ways to make our product even better. We know
                            that you use our product day in and day out, and you
                            have unique insights into how it can best meet your
                            needs.
                            <br />
                            <br />
                            So, we're inviting you to share your product feature
                            suggestions with us. Whether it's a small tweak that
                            would make your daily tasks easier or a big idea
                            that could revolutionize your experience, we want to
                            hear it all! Your ideas can help shape the future of
                            our product.
                          </p>
                        </div>
                      </div>
                    </div>
                  </div>
                </div> */}
                {console.log(onBoardGet)}
                {Array.isArray(onBoardGet) && onBoardGet.length > 0 ? (
         
                    <div className="add_listing border bg-light rounded-3 w-100 p-3 mt-3">
                        <div className="table-responsive">
                          <table
                            id="DepositLedger"
                            className="table table-striped"
                            width="100%"
                            border="0"
                            cellSpacing="0"
                            cellPadding="0"
                          >
                            <thead>
                              <tr>
                                <th>S.NO</th>
                                <th>Mls Membership</th>
                                <th>Board Membership</th>
                                <th>Document Name</th>
                                <th>View</th>
                              </tr>
                            </thead>
                            <tbody>
                              {onBoardGet.map((item, index) => (
                                <tr key={index}>
                                  <td>
                                    <span className="SuppressText">
                                      {index + 1}{" "}
                                    </span>
                                  </td>
                                  <td>
                                    <span className="SuppressText">
                                      {item.mlsMembership}
                                    </span>
                                  </td>
                                  <td>
                                    <span className="SuppressText">
                                      {item.boardMembership}
                                    </span>
                                  </td>
                                  <td>
                                    <span className="SuppressText">
                                      {item.document}
                                    </span>
                                  </td> 
                                  <td>
                                    <a href={item.image} target="_blank">
                                    <span className="SuppressText">
                                      View
                                    </span>
                                    </a>
                                  </td>                                
                                </tr>
                              ))}
                            </tbody>
                          </table>
                        </div>
                      </div>
                  
                ) : (
                  ""
                )}
                </div>
              </div>
            </div>
          </div>
        </div>
      </main>
    </div>
  );
}
export default MlsBoardDocument;

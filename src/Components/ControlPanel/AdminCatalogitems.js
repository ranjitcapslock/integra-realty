import React, { useState, useEffect } from "react";
// import avtar from "./Components/img/avtar.jpg";
import _ from 'lodash';
import user_service from "../service/user_service";
import jwt from "jwt-decode";
import Loader from "../../Pages/Loader/Loader.js";
import Toaster from "../../Pages/Toaster/Toaster.js";
import { NavLink, useNavigate, useParams } from "react-router-dom";
import swal from 'sweetalert2';
import 'sweetalert2/dist/sweetalert2.css';
import $, { each } from 'jquery';

import axios from 'axios';

import { FormGroup, FormControl, FormLabel } from 'react-bootstrap';

function AdminCatalogitems() {
    const initialValues = {
                   
                    office:"",
                    office_id:"",
                    category:"",
                    title:"",
                    description:"",
                    image:"",
                    is_active: "1",
                    // video_url: video_url,
    };
    const [formValues, setFormValues] = useState(initialValues);
    const [formErrors, setFormErrors] = useState({});
    const [isSubmitClick, setISSubmitClick] = useState(false)
    const [loader, setLoader] = useState({ isActive: null })
    const { isActive } = loader;
    const [toaster, setToaster] = useState({ types: null, isShow: null, toasterBody: "", message: "" });
    const { types, isShow, toasterBody, message } = toaster;

    const [checkData, setCheckData] = useState("")
    const navigate = useNavigate();
    const params = useParams();


    
    const [thumbnail_created_array, setThumbnail_created_array] = useState([]);
    // onChange Function start
    const handleChange = (e) => {
        const { name, value } = e.target;
        setFormValues({ ...formValues, [name]: value })
    };
    // onChange Function end

   
    {/* <!-- Form Validation Start--> */ }
    useEffect(() => { 
        if (formValues && isSubmitClick) {
            validate()
        }
    }, [formValues])




    const validate = () => {
        const values = formValues
        const errors = {};

        if (!values.title) {
            errors.title = "Title is required";
        }

        if (!values.description) {
            errors.description = "Description is required";
        }

        if (!values.category) {
            errors.category = "Category is required";
        }
        if (!imgthumbnailfile) {
            errors.image = "image is required";
        }
        setFormErrors(errors)
        return errors;
    }
    {/* <!-- Form Validation End--> */ }


    const handleSubmit = async (e) => {
            e.preventDefault();
            setISSubmitClick(true)
            let checkValue = validate()
            if (_.isEmpty(checkValue)) {
               
                const userData = {
                    agentId: jwt(localStorage.getItem("auth")).id,
                    title: formValues.title,
                    category:formValues.category,
                    description: formValues.description,
                    office: "corporate",
                    // office_id:formValues.office_id,
                    image:imgthumbnailfile,
                    is_active: formValues.is_active,
                };

                // setLoader({ isActive: true })
                console.log(userData)

                await user_service.Addpromotocatalog(userData).then((response) => {
                    if (response) {
                        setFormValues(response.data);
                        setLoader({ isActive: false })
                        setToaster({ type: "Catalog Added Successfully", isShow: true, toasterBody: response.data.message, message: "Catalog Added Successfully", });
                        setTimeout(() => {
                            setToaster((prevToaster) => ({ ...prevToaster, isShow: false }))
                            navigate(`/promote`);
                        }, 2000);
                    }
                    else {
                        setLoader({ isActive: false })
                        setToaster({ types: "error", isShow: true, toasterBody: response.data.message, message: "Error", });
                    }
                });
            }else{
                setISSubmitClick(false)
                console.log("validate")
            }
    }
    {/* <!-- Api call Form onSubmit End--> */ }

    
    const [learnbyAgent, setLearnbyAgent] = useState([]);
    const getLearnbyAgent = async () => {
        await user_service.getLearnbyAgent(jwt(localStorage.getItem("auth")).id).then((response) => {
            setLoader({ isActive: false })
            if (response) {
                setLearnbyAgent(response.data);
                // console.log(learnbyAgent);
                // console.log("learnbyAgent");
            }
        });
    }
    useEffect(() => { window.scrollTo(0, 0);
            setLoader({ isActive: true });
            
            getLearnbyAgent();
    }, []);

    const handleRemove = async () => {
        setLoader({ isActive: true })
        await user_service.bannerDelete(params.id).then((response) => {
            if (response) {
                const BannerList = async () => {
                    await user_service.bannerGet().then((response) => {
                        setLoader({ isActive: false })
                        if (response) {
                            setFormValues(response.data);
                            setToaster({ type: "Banner Delete Successfully", isShow: true, toasterBody: response.data.message, message: "Banner Delete Successfully", });
                            setTimeout(() => {
                                setToaster((prevToaster) => ({ ...prevToaster, isShow: false }))
                                navigate("/control-panel/banners");
                            }, 1000);
                        }
                    });
                }
                BannerList()
            }

        });
    }

  

    const [imgthumbnailfile, setImgthumbnailfile] = useState("");
     
    const [acceptedFileTypes, setAcceptedFileTypes] = useState([]);

    const handleFileUploadSide = async (e, id) => {
        const selectedFile = e.target.files[0];
       // setFile(selectedFile);

        if (selectedFile) {
            //setFileExtension(selectedFile.name.split('.').pop());

            const formData = new FormData();
            formData.append('file', selectedFile);
            const config = {
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'multipart/form-data',
                    Authorization: `Bearer ${localStorage.getItem('auth')}`,
                },
            };
            try {
                setLoader({ isActive: true })
                const uploadResponse = await axios.post('https://api.brokeragentbase.com/upload', formData, config);
                const uploadedFileData = uploadResponse.data;
                setLoader({ isActive: false })
                setImgthumbnailfile(uploadedFileData);

                // const userData = {

                //     agentId: jwt(localStorage.getItem('auth')).id,
                //     side_image: uploadedFileData
                // };

                // console.log(userData);
                // try {
                //     const response = await user_service.bannerUpdate(itemId, userData);
                //     if (response) {
                //         console.log(response);
                //         const BannerList = async () => {
                //             await user_service.bannerGet().then((response) => {
                //                 setLoader({ isActive: false })
                //                 if (response) {
                //                     setGetBanner(response.data)
                //                 }
                //             });
                //         }
                //         BannerList()

                //         document.getElementById('closeModalSide').click();
                //     }
                // } catch (error) {
                //     console.log('Error occurred while uploading documents:', error);
                // }

            }
            catch (error) {
                console.error('Error occurred during file upload:', error);
            }
        }
    }

    return (
        
        <div className="bg-secondary float-left w-100 pt-4">
            <Loader isActive={isActive} />
            {isShow && <Toaster types={types} isShow={isShow} toasterBody={toasterBody} message={message} />}
            <main className="page-wrapper profile_page_wrap">
                <div className="container content-overlay">
                    <div className="justify-content-center pb-sm-2">
                        <div className="">
                            <h2 className="h4">Catalog Details</h2><hr className="mb-3" />
                            <div className="mb-5">
                                {/* <button className="btn btn-secondary" type="button" onClick={(e) => handleRemove()}><NavLink>Remove Video</NavLink></button> */}
                               {
                                imgthumbnailfile ?
                                    <button className="btn btn-primary pull-right ms-5" type="button" onClick={handleSubmit} disabled={isSubmitClick}>{isSubmitClick ? "Please wait..." : "Add catalog"} </button>
                                :""
                               } 
                                <NavLink className="btn btn-secondary pull-right" type="button" to="/promote">Cancel</NavLink>
                            </div>

                            <div className="justify-content-center pb-sm-2">
                                <div className="bg-light rounded-3 p-4 mb-3">
                                    <div className="row mt-5">
                                        <>
                                            <h5>Catalog Details</h5>
                                            <div className="col-md-12">
                                            <label className="form-label" >Title &nbsp;&nbsp;<NavLink>REQUIRED</NavLink><br />
                                              </label>
                                            <input className="form-control" id="inline-form-input" type="text"
                                                name="title" value={formValues.title}
                                                onChange={handleChange}
                                                style={{
                                                    border: formErrors?.title ? '1px solid red' : ''
                                                }}
                                            />
                                            {formErrors.title && (
                                                <div className="invalid-tooltip">{formErrors.title}</div>
                                            )}
                                        </div>

                                        <div className="col-md-12 mt-3">
                                            <label className="form-label" for="pr-country">Description</label>
                                            <input className="form-control" id="inline-form-input" type="text"
                                                name="description" value={formValues.description}
                                                onChange={handleChange}
                                            />
                                            {formErrors.description && (
                                                <div className="invalid-tooltip">{formErrors.description}</div>
                                            )}
                                        </div>


                                        <div className="col-md-12 mt-3">
                                            <label className="form-label" for="pr-country">Category</label>
                                            
                                            <select className="form-select" id="pr-country"
                                                name="category" value={formValues.category}
                                                onChange={handleChange}>
                                                <option value="product">Product</option>
                                                <option value="vendor">Vendor</option>
                                            </select>
                                            {formErrors.category && (
                                                <div className="invalid-tooltip">{formErrors.category}</div>
                                            )}
                                        
                                        </div>


                                        <div className="col-md-12 mt-3">
                                            <label className="form-label" for="pr-country">Image</label>
                                            <input className="form-control"
                                                    id="inline-form-input"
                                                    type="file"
                                                    accept={acceptedFileTypes.map((type) => `.${type}`).join(',')}
                                                    name="thumbnailimg"
                                                    onChange={handleFileUploadSide}
                                                    value={formValues.thumbnailimg}
                                                />
                                                {formErrors.imgthumbnailfile && (
                                                    <div className="invalid-tooltip">{formErrors.imgthumbnailfile}</div>
                                                )}
                                             {
                                                imgthumbnailfile ?
                                                 <img className="mt-3" src = {imgthumbnailfile} />
                                                 :""
                                             }   


                                            {/* <div className="col-md-3">
                                                <div className="NotSetup Marquee text-center">

                                                    <button type="button" className="btn btn-primary">
                                                        Not Setup
                                                    </button>


                                                    <p>No side banner.</p>
                                                </div>
                                            </div> */}

                                        </div>


                                        <div className="col-md-12 mt-3">
                                            <label className="form-label" for="pr-country">Active Status</label>
                                            <select className="form-select" id="pr-country"
                                                name="is_active" value={formValues.is_active}
                                                onChange={handleChange}>
                                                <option value="1">Active</option>
                                                <option value="0">Hidden</option>
                                                
                                            </select>
                                            {formErrors.is_active && (
                                                <div className="invalid-tooltip">{formErrors.is_active}</div>
                                            )}
                                        </div>
                                        </>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </main>

            
        </div>
    )
}
export default AdminCatalogitems;
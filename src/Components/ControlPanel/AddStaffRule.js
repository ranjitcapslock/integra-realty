import React, { useState, useEffect } from "react";
import _ from "lodash";
import user_service from "../service/user_service";
import jwt from "jwt-decode";
import Loader from "../../Pages/Loader/Loader.js";
import Toaster from "../../Pages/Toaster/Toaster.js";
import { NavLink, useNavigate, useParams } from "react-router-dom";

function AddStaffRule() {
  const initialValues = {
    question: "",
    answer: "",
    attach_type: "",
    link_title: "",
    link_url: "",
    file_url: "",
  };
  const [formValues, setFormValues] = useState(initialValues);
  const [formErrors, setFormErrors] = useState({});
  const [isSubmitClick, setISSubmitClick] = useState(false);
  const [loader, setLoader] = useState({ isActive: null });
  const { isActive } = loader;
  const [toaster, setToaster] = useState({
    types: null,
    isShow: null,
    toasterBody: "",
    message: "",
  });
  const { types, isShow, toasterBody, message } = toaster;
 
  const [formData, setFormData] = useState([]);

  // onChange Function start
  const handleChange = (e) => {
    const { name, value } = e.target;
    setFormValues({ ...formValues, [name]: value });
  };
  // onChange Function end

  {
    /* <!-- Form Validation Start--> */
  }
  useEffect(() => {
    if (formValues && isSubmitClick) {
      validate();
    }
  }, [formValues]);

  const validate = () => {
    const values = formValues;
    const errors = {};
    if (!values.roleStaff) {
      errors.roleStaff = "Staff Role is required";
    }
    setFormErrors(errors);
    return errors;
  };
  {
    /* <!-- Form Validation End--> */
  }

  useEffect(() => {
    profileGetAll();

    if (formData) {
      StaffRoleGetData();
    }
  }, []);

  const [profile, setProfile] = useState("");
  const profileGetAll = async () => {
    if (localStorage.getItem("auth")) {
      await user_service
        .profileGet(jwt(localStorage.getItem("auth")).id)
        .then((response) => {
          if (response) {
            console.log(response);
            setProfile(response.data);
          }
        });
    }
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
    setISSubmitClick(true);
    let checkValue = validate();
    if (_.isEmpty(checkValue)) {
      const userData = {
        agentId: jwt(localStorage.getItem("auth")).id,
        roleStaff: formValues.roleStaff,
        organization: profile.additionalActivateFields?.account_name ?? "",
        organizationlavel: profile.active_office ?? "Corporate",
      };
      console.log(userData);
      try {
        setLoader({ isActive: true });
        await user_service.StaffRolePost(userData).then((response) => {
          if (response) {
            console.log(response.data);
            setLoader({ isActive: false });
            setToaster({
              type: "Staff Role",
              isShow: true,
              toasterBody: response.data.message,
              message: "Role Added Successfully",
            });
            setTimeout(() => {
              setToaster((prevToaster) => ({
                ...prevToaster,
                isShow: false,
              }));
            }, 1000);
            StaffRoleGetData();
          } else {
            setLoader({ isActive: false });
            setToaster({
              types: "error",
              isShow: true,
              toasterBody: response.data.message,
              message: "Error",
            });
          }
        });
      } catch (error) {
        setLoader({ isActive: false });
        setToaster({
          types: "error",
          isShow: true,
          toasterBody: error.response
            ? error.response.data.message
            : error.message,
          message: "Error",
        });
        setTimeout(() => {
          setToaster((prevToaster) => ({ ...prevToaster, isShow: false }));
        }, 2000);
      }
    }
  };

  const StaffRoleGetData = async () => {
    await user_service.StaffRoleGet().then((response) => {
      if (response) {
        console.log(response);
        setFormData(response.data.data);
      }
    });
  };

  const roleLabels = {
    banner: "Banner",
    shared_documents: "Shared Documents",
    add_post: "Add Posts",
    add_contact: "Add new agents",
    calender: "Calendar",
    products_vendor: "Products and vendors",
    add_dashboard: "Add Dashboard",
    add_question: "Add Questions",
  };

    const removeItem = async (id) => {
        console.log(id);
        setLoader({ isActive: true });
        await user_service.StaffRoleDelete(id).then((response) => {
          if (response) {
            setLoader({ isActive: false });
            setToaster({
              types: "Delete",
              isShow: true,
              toasterBody: response.data.message,
              // message: "",
            });
            StaffRoleGetData()
            setTimeout(() => {
            }, 1000);
          }
        });
      };

  return (
    <>
      <div className="bg-secondary float-left w-100 pt-4 mb-4">
        <Loader isActive={isActive} />
        {isShow && (
          <Toaster
            types={types}
            isShow={isShow}
            toasterBody={toasterBody}
            message={message}
          />
        )}
        <main className="page-wrapper">
          <div className="">
            <div className="">
              <div className="">
                <div className="d-flex align-items-center justify-content-start mb-4">
                  <h3 className="text-white mb-0">Add a Role</h3>
                </div>
                <div className="row">
                  <div className="col-md-6">
                    <div className="bg-light border rounded-3 p-3">
                      <div className="col-sm-12 mb-3">
                        <label className="form-label">Organization Level</label>
                        <input
                          className="form-control"
                          id="inline-form-input"
                          type="text"
                          name="active_office"
                          value={profile.active_office ?? "Corporate"}
                          readOnly
                        />
                      </div>

                      <div className="col-sm-12 mb-3">
                        <label className="form-label">Staff Roles</label>
                        <select
                          className="form-select"
                          id="pr-country"
                          name="roleStaff"
                          value={profile.roleStaff}
                          onChange={handleChange}
                          style={{
                            border: formErrors?.roleStaff
                              ? "1px solid red"
                              : "1px solid #00000026",
                          }}
                        >
                          <option></option>
                          <option value="banner">Banner</option>
                          <option value="shared_documents">
                            Shared Documents
                          </option>
                          <option value="add_post">Add Posts</option>
                          <option value="add_contact">Add new agents</option>
                          <option value="calender">Calendar</option>
                          <option value="products_vendor">
                            Products and vendors
                          </option>
                          <option value="add_dashboard">Add Dashboard</option>
                          <option value="add_question">Add Questions</option>
                        </select>

                        <div className="invalid-tooltip">
                          {formErrors.roleStaff}
                        </div>
                      </div>
                    </div>
                    <div className="pull-right mt-3">
                      <NavLink
                        type="button"
                        className="btn btn-secondary pull-right ms-3"
                        to="/question"
                      >
                        Cancel
                      </NavLink>
                      <button
                        className="btn btn-primary"
                        type="button"
                        onClick={handleSubmit}
                      >
                        Submit
                      </button>
                    </div>
                  </div>
                  <div className="col-md-6 bg-light border rounded-3 p-3">
                    <table
                      id="DepositLedger"
                      className="table table-striped mb-0"
                      width="100%"
                      border="0"
                      cellSpacing="0"
                      cellPadding="0"
                    >
                      <tbody>
                        <table
                          id="DepositLedger"
                          className="table table-striped mb-0"
                          width="100%"
                          border="0"
                          cellSpacing="0"
                          cellPadding="0"
                        >
                          <thead>
                            <tr>
                              <th>Role</th>
                              <th>Organization</th>
                              <th>Remove</th>
                            </tr>
                          </thead>
                          <tbody>
                            {Array.isArray(formData) && formData.length > 0 ? (
                              formData.map((item) => (
                                <tr key={item.id}>
                                  <td>
                                    {roleLabels[item.roleStaff] ||
                                      item.roleStaff}
                                  </td>
                                  <td>{item.organization}</td>
                                  <td>
                                    <button className="btn btn-secondary btn-sm"
                                      onClick={() => removeItem(item._id)}
                                    >
                                      Remove
                                    </button>
                                  </td>
                                </tr>
                              ))
                            ) : (
                              <tr>
                                <td>No data to display.</td>
                              </tr>
                            )}
                          </tbody>
                        </table>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </main>
      </div>
    </>
  );
}
export default AddStaffRule;

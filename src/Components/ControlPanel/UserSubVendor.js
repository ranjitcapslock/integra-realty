import React, { useState, useEffect } from "react";
import { NavLink, useParams, useNavigate } from "react-router-dom";
import user_service from "../service/user_service";
import Loader from "../../Pages/Loader/Loader.js";
import Toaster from "../../Pages/Toaster/Toaster.js";
import PromoteImage from "../img/image.jpg";
const UserSubVendor = () => {
  const [loader, setLoader] = useState({ isActive: null });
  const { isActive } = loader;
  const [toaster, setToaster] = useState({
    types: null,
    isShow: null,
    toasterBody: "",
    message: "",
  });
  const { types, isShow, toasterBody, message } = toaster;
  const params = useParams();

  const [productItem, setProductItem] = useState([]);
  const [productItemData, setProductItemData] = useState("");
  const [products, setProducts] = useState([]);
  const navigate = useNavigate();

  useEffect(() => {
    window.scrollTo(0, 0);
    setLoader({ isActive: true });
    const ProductItem = async () => {
      await user_service.subVendorItemGet(params.id).then((response) => {
        setLoader({ isActive: false });
        if (response) {
          console.log(response);
          setProductItem(response.data.data);
        }
      });
    };

    ProductItem();
  }, []);

  useEffect(() => {
    window.scrollTo(0, 0);
    if (params.id) {
      const PromoteProductGetIdId = async () => {
        await user_service.promotoProductsGetId(params.id).then((response) => {
          setLoader({ isActive: false });
          if (response) {
            setProducts(response.data);
          }
        });
      };
      PromoteProductGetIdId();
    }
  }, []);

  //   const handleDeleteProduct = async (id) => {
  //     console.log(id);
  //     if (id) {
  //       setLoader({ isActive: true });
  //       await user_service.subCategoryVendorDelete(id).then((response) => {
  //         if (response) {
  //           // console.log(response);
  //           setLoader({ isActive: false });
  //           setToaster({
  //             type: "Add VendorItems",
  //             isShow: true,
  //             toasterBody: response.data.message,
  //             message: "VendorItems deleted Successfully",
  //           });
  //           setTimeout(() => {
  //             setToaster((prevToaster) => ({
  //               ...prevToaster,
  //               isShow: false,
  //             }));
  //             navigate(`/admin/promote/`);
  //           }, 1000);
  //         }
  //       });
  //     }
  //   };
  return (
    <div className="bg-secondary float-left w-100 pt-4 mb-4">
      <Loader isActive={isActive} />
      {isShow && (
        <Toaster
          types={types}
          isShow={isShow}
          toasterBody={toasterBody}
          message={message}
        />
      )}
      <main className="page-wrapper profile_page_wrap">
        <div className="">
          <div className="FeaturedBox promoted_product float-left w-100">
            <div className="row">
              <div class="align-items-center justify-content-between mb-4">
                <h3 class="pull-left mb-0 text-white">Assigned Items Sub-Category</h3>
              </div>
              {products.title ? (
                <h3 className="pull-left mb-0  text-white">{products.title}</h3>
              ) : (
                ""
              )}
              {productItem && productItem.length > 0 ? (
                productItem.map((items) => (
                  <div
                    className="col-lg-3 col-md-6 col-sm-6 col-12 tns-item tns-slide-cloned tns-slide-active p-3"
                  >
                    <div className="card shadow-sm cursor-pointer card-hover border-0 h-100">
                      <div className="card-img-top card-img-hover">
                        {items.image.length > 0 ? (
                          <>
                            <NavLink
                              to={`/promote/catalog-product/${params.id}/${items._id}`}
                            >
                              <img
                                className="img-fluid rounded-0 w-100"
                                src={items.image}
                                alt="images"
                              />
                            </NavLink>
                          </>
                        ) : (
                          <img
                            className="img-fluid rounded-0 w-100"
                            src={PromoteImage}
                            alt="image"
                          />
                        )}
                      </div>
                      <div className="card-body position-relative pb-3">
                        <p className="mb-0 fs-sm text-muted">
                          {items.title}
                          <br />
                          {items.description}
                        </p>
                        {/* <div className="row mt-3">
                          <div className="col-lg-6 col-md-6 col-sm-6 col-6">
                            <NavLink
                              type="button"
                              className="text-center "
                              to={`/add-subVendor/${params.id}/${items._id}`}
                            >
                              <i className="fi-edit me-2"></i>
                              Edit
                            </NavLink>
                          </div>
                          <div className="col-lg-6 col-md-6 col-sm-6 col-6">
                            <a
                              type="button"
                              aria-current="page"
                              className="text-center  active"
                              onClick={() => handleDeleteProduct(items._id)}
                            >
                              <i
                                className="fa fa-trash-o me-2"
                                aria-hidden="true"
                              ></i>
                              Delete
                            </a>
                          </div>
                        </div> */}
                      </div>
                    </div>
                  </div>
                ))
              ) : (
                <p className="float-left w-100 text-center text-light">
                  No products found, please check back later.
                </p>
              )}
            </div>
          </div>
        </div>
        <div className="mt-4">
          <span className="pull-right">
            <>
              {products.category === "product" ? (
                <NavLink
                  className="btn btn-primary pull-right ms-3"
                  type="button"
                  to={`/promote`}
                >
                  Back
                </NavLink>
              ) : (
                <NavLink
                  className="btn btn-primary pull-right ms-3"
                  type="button"
                  to={`/vender`}
                >
                  Back
                </NavLink>
              )}
            </>
          </span>
        </div>
      </main>
    </div>
  );
};

export default UserSubVendor;

import React, { useState, useMemo, useEffect } from "react";
import { NavLink, useParams } from "react-router-dom";
import user_service from "../service/user_service";
import Loader from "../../Pages/Loader/Loader.js";
import Toaster from "../../Pages/Toaster/Toaster";
import AllTab from "../../Pages/AllTab.js";
import axios from "axios";
import _ from "lodash";
import countryList from "country-list";
import DateTimePicker from "react-datetime-picker";
import "react-datetime-picker/dist/DateTimePicker.css";
import jwt from "jwt-decode";
import Pic from "../img/pic.png";
// import StatusBar from "../../Pages/StatusBar.js";

const NewProperty = () => {
  const [loader, setLoader] = useState({ isActive: null });
  const { isActive } = loader;
  const [toaster, setToaster] = useState({
    types: null,
    isShow: null,
    toasterBody: "",
    message: "",
  });
  const { types, isShow, toasterBody, message } = toaster;
  const [transactionId, setTransactionId] = useState([]);

  const params = useParams();
  const [formValuesNew, setFormValuesNew] = useState("");

  const [file, setFile] = useState(null);
  const [data, setData] = useState("");
  const [reloadstatusbar, setReloadstatusbar] = useState(false);
  const [getrequireddoucmentno, setGetrequireddoucmentno] = useState("");

  const [fileNew, setFileNew] = useState(null);
  const [dataNew, setDataNew] = useState("");

  const initialValues = {
    listingsAgent: "",
    category: "",
    mlsId: "",
    propertyType: "",
    mlsNumber: "",
    agentId: "",
    streetDirection: "North",

    associateName: "",
    price: "",
    garage: "",
    bed: "",
    subDivision: "",
    streetAddress: "",
    city: "",
    state: "Utah",
    zipCode: "",

    schoolDistrict: "",
    country: "",
    areaLocation: "",
    status: "",
    listDate: "",
    image: "",
    year_built: "",
    stage: "",
    baths: "",
    square_feet: "",
    building_area_total: "",
    seller_deadline: "",
    diligence_deadline: "",
    appraisal_edeadline: "",
    financing_deadline: "",
    settlement_deadline: "",
    propert_built: "",
    buyeragent_brokerage: "",
    buyer_licensed: "",
    expiration_date: "",
    previous_price: "",
    trust_deedbalance1: "",
    trust_deedbalance2: "",
    amount: "",
    description: "",
    occupancy: "",
    occupancy_name: "",
    occupancy_phone: "",
    occupancy_email: "",
    instruction: "",
    hoa: "yes",
    hoa_due: "",
    hoa_duesfrequency: "",
    hoa_firstname: "",
    hoa_lastname: "",
    hoa_contact: "",
    hoa_company: "",
    hoa_address: "",
    hoa_website: "",
    transfer_fee: "",
    transfer_feeamount: "",
    transfer_amount: "",
    comment: "",
    hoa_document: "",

    electric_company: "",
    garbage_company: "",
    gas_company: "",
    sewer_Company: "",
    sewer_septic: "Sewer",
    water_Company: "",

    home_Warranty_Company: "",
    home_Warranty_contact: "",
    home_warranty_website: "",
    home_warranty_date_ordered: "",
    home_warranty_who_ordered: "",
    home_warranty_both_Pay: "",
    home_warranty_price: "",
    paying_Home_warranty: "",
    home_Warranty_Ordered_transaction: "yes",
    home_Warranty_Ordered: "",

    home_Inspection_Property: "yes",
    home_Inspection_Company: "",
    home_Inspector_Name: "",
    home_Inspector_Phone: "",
    home_Companies_Website: "",
    home_Inspector_Price: "",
    home_Inspection_Ordered: "",
    home_Date_Ordered: "",
    home_Who_Ordered: "",
    home_Inspection_Datetime: "",
  };
  const [formValues, setFormValues] = useState(initialValues);
  const [transactionFundingRequest, setTransactionFundingRequest] =
    useState("");

  const TransactionGetById = async () => {
    try {
      const response = await user_service.transactionGetById(params.id);
      if (response) {
        setLoader({ isActive: false });
        console.log(response.data);
        const data = response.data;

        setFormValuesNew(data);

        // setFormValuesQuestion({
        //   represent: data.represent || "",
        //   type: data.type || "",
        //   ownership: data.ownership  || "",
        //   phase: data.phase  || "",
        // });
        const newData = response.data;
        setFormValues(newData.propertyDetail);
        // const SingleListingById = async () => {
        //   await user_service.listingsGetById(newData?.propertyDetail?._id).then((response) => {
        //     setLoader({ isActive: false });
        //     if (response) {
        //       setSummary(response.data);
        //     }
        //   });
        // };
        // SingleListingById()
      }
    } catch (error) {
      console.error(error);
    }
  };

  const Getrequireddoucmentno = async () => {
    const agentId = jwt(localStorage.getItem("auth")).id;
    await user_service
      .Getrequireddoucmentno(agentId, params.id)
      .then((response) => {
        // setLoader({ isActive: false })
        if (response) {
          // console.log(response.data)
          setGetrequireddoucmentno(response.data);
        }
      });
  };

  const handleChange = (e) => {
    const { name, value } = e.target;
    setFormValues({ ...formValues, [name]: value });
  };

  // const handleChangePrice = (e) => {
  //   const { name, value } = e.target;
  //   const numericValue = value.replace(/[^\d.]/g, "");

  //   if (numericValue !== "") {
  //     const floatValue = parseFloat(numericValue);

  //     if (!isNaN(floatValue)) {
  //       setFormValues((prevValues) => ({
  //         ...prevValues,
  //         [name]:floatValue.toLocaleString("en-US"),
  //       }));
  //     }
  //   } else {
  //     setFormValues((prevValues) => ({
  //       ...prevValues,
  //       [name]: "",
  //     }));
  //   }
  // };

  const handleChangeStage = async (event) => {
    const select = event.target.value;
    setFormValues({
      ...formValues,
      stage: select,
    });

    try {
      const userData = {
        phase: select,
      };
      await user_service
        .TransactionUpdate(params.id, userData)
        .then((response) => {
          if (response.status === 200) {
            const userDatan = {
              addedBy: jwt(localStorage.getItem("auth")).id,
              agentId: jwt(localStorage.getItem("auth")).id,
              transactionId: params.id,
              message: "Transaction phase updated to " + select,
              // transaction_property_address: "ADDRESS 2",
              note_type: "Status",
            };
            const responsen = user_service.transactionNotes(userDatan);

            setReloadstatusbar("true");
            // setReloadstatusbar((prev) => !prev);
            TransactionGetById(params.id);
            //  window.location.reload();
            setToaster({
              type: "Add Transaction",
              isShow: true,
              toasterBody: response.data.message,
              message: "Transaction Phase Update Successfully",
            });
            setTimeout(() => {
              setToaster((prevToaster) => ({ ...prevToaster, isShow: false }));
              setReloadstatusbar(false);
            }, 3000);
          } else {
            setLoader({ isActive: false });
            setToaster({
              types: "error",
              isShow: true,
              toasterBody: response.data.message,
              message: "Error",
            });
          }
        });
    } catch (error) {
      setLoader({ isActive: false });
      setToaster({
        types: "error",
        isShow: true,
        toasterBody: error.response
          ? error.response.data.message
          : error.message,
        message: "Error",
      });
    }
  };

  const handleFileUpload = async (e) => {
    const selectedFile = e.target.files[0];

    if (selectedFile) {
      if (!selectedFile.type.startsWith("image/")) {
        setToaster({
          types: "Select Property",
          isShow: true,
          message: "Please select an image file",
        });
        setTimeout(() => {
          setToaster((prevToaster) => ({
            ...prevToaster,
            isShow: false,
          }));
        }, 2000);
        return;
      }

      setFile(selectedFile);

      const fileNameWithoutExtension = selectedFile.name.replace(
        /\.[^/.]+$/,
        ""
      );
      const formData = new FormData();
      formData.append("file", selectedFile);

      const config = {
        headers: {
          Accept: "application/json",
          "Content-Type": "multipart/form-data",
          Authorization: `Bearer ${localStorage.getItem("auth")}`,
        },
      };

      try {
        setLoader({ isActive: true });

        const uploadResponse = await axios.post(
          "https://api.brokeragentbase.com/upload",
          formData,
          config
        );
        const uploadedFileData = uploadResponse.data;

        setLoader({ isActive: false });
        setData(uploadedFileData);
      } catch (error) {
        console.error("Error occurred during file upload:", error);
      }
    }
  };

  const createemail = async (e) => {
    const userData = {};
    try {
      setLoader({ isActive: true });
      const response = await user_service.TransactionemailUpdate(
        params.id,
        userData
      );
      setLoader({ isActive: false });
      if (response) {
        const userDatan = {
          addedBy: jwt(localStorage.getItem("auth")).id,
          agentId: jwt(localStorage.getItem("auth")).id,
          transactionId: params.id,
          message: "Transaction Email Update",
          // transaction_property_address: "ADDRESS 2",
          note_type: "Property",
        };
        const responsen = await user_service.transactionNotes(userDatan);

        setLoader({ isActive: false });
        setToaster({
          types: "SelectProperty",
          isShow: true,
          toasterBody: response.data.message,
          message: "Email Updated Successfully",
        });
        setTimeout(() => {
          setToaster((prevToaster) => ({
            ...prevToaster,
            isShow: false,
          }));
        }, 2000);
        TransactionGetById(params.id);
      } else {
        setLoader({ isActive: false });
        setToaster({
          types: "error",
          isShow: true,
          toasterBody: response.data.message,
          message: "Error",
        });
      }
    } catch (error) {
      setLoader({ isActive: false });
      setToaster({
        types: "error",
        isShow: true,
        toasterBody: error,
        message: "Error",
      });
    }
  };

  const handleSubmit = async (e) => {
    if (formValues?._id) {
      e.preventDefault();
      const userData = {
        // agentId: jwt(localStorage.getItem("auth")).id,
        office_id: formValues.office_id,
        email_id: formValues.email_id,
        streetAddress: formValues.streetAddress,
        address2: formValues.address2,
        city: formValues.city,
        state: formValues.state,
        zipCode: formValues.zipCode,
        stage: formValues.stage,
        mlsNumber: formValues.mlsNumber,
        listingsAgent: formValues.listingsAgent,
        image: data,

        price: formValues.price,
        year_built: formValues.year_built,
        bed: formValues.bed,
        baths: formValues.baths,
        square_feet: formValues.square_feet,
        building_area_total: formValues.building_area_total,
        // transactionId: params.id,
        subDivision: formValues.subDivision,
        seller_deadline: formValues.seller_deadline,
        diligence_deadline: formValues.diligence_deadline,
        financing_deadline: formValues.financing_deadline,
        appraisal_edeadline: formValues.appraisal_edeadline,
        settlement_deadline: formValues.settlement_deadline,
        propert_built: formValues.propert_built,
        buyeragent_brokerage: formValues.buyeragent_brokerage,
        buyer_licensed: formValues.buyer_licensed,
      };
      try {
        setLoader({ isActive: true });

        const response = await user_service.listingUpdate(
          formValues?._id,
          userData
        );
        setLoader({ isActive: false });
        if (response) {
          const userDatan = {
            addedBy: jwt(localStorage.getItem("auth")).id,
            agentId: jwt(localStorage.getItem("auth")).id,
            transactionId: formValues?._id,
            message: "Transaction Property Details Update",
            // transaction_property_address: "ADDRESS 2",
            note_type: "Property",
          };
          const responsen = await user_service.transactionNotes(userDatan);

          setLoader({ isActive: false });
          setToaster({
            types: "SelectProperty",
            isShow: true,
            toasterBody: response.data.message,
            message: "Property Updated Successfully",
          });
          setTimeout(() => {
            setToaster((prevToaster) => ({
              ...prevToaster,
              isShow: false,
            }));
          }, 2000);
          TransactionGetById(params.id);
        } else {
          setLoader({ isActive: false });
          setToaster({
            types: "error",
            isShow: true,
            toasterBody: response.data.message,
            message: "Error",
          });
        }
      } catch (error) {
        setLoader({ isActive: false });
        setToaster({
          types: "error",
          isShow: true,
          toasterBody: error,
          message: "Error",
        });
      }
    }
  };

  const handleChanget = (e) => {
    const { name, value } = e.target;
    setFormValuesNew({ ...formValuesNew, [name]: value });
  };

  const [formErrorsNew, setFormErrorsNew] = useState({
    seller_deadline: "Seller deadline is required",
    diligence_deadline: "Diligence deadline is required",
    financing_deadline: "Financing deadline is required",
    appraisal_edeadline: "Appraisal edeadline is required",
    settlement_deadline: "Settlement deadline is required",
    propert_built: "Propert built is required",
    buyeragent_brokerage: "Buyeragent brokerage is required",
    buyer_licensed: "Buyer licensed is required",
  });

  const handleChangeData = (e) => {
    const { name, value } = e.target;
    setFormValuesNew({ ...formValuesNew, [name]: value });
    // Check if the field is required and empty, update error state
    if (!value && formErrorsNew[name]) {
      setFormErrorsNew({ ...formErrorsNew, [name]: `${name} is required` });
    } else if (formErrorsNew[name]) {
      // Clear error if field is no longer empty
      setFormErrorsNew({ ...formErrorsNew, [name]: "" });
    }
  };

  const CheckPropertbuilt = (event) => {
    const select = event.target.value;
    setFormValuesNew({
      ...formValuesNew,
      propert_built: select,
    });
    if (!select) {
      setFormErrorsNew({
        ...formErrorsNew,
        propert_built: "Propert built is required",
      });
    } else {
      setFormErrorsNew({ ...formErrorsNew, propert_built: "" });
    }
  };

  const CheckBrokerage = (event) => {
    const select = event.target.value;
    setFormValuesNew({
      ...formValuesNew,
      buyeragent_brokerage: select,
    });

    if (!select) {
      setFormErrorsNew({
        ...formErrorsNew,
        buyeragent_brokerage: "Buyeragent brokerage is required",
      });
    } else {
      setFormErrorsNew({ ...formErrorsNew, buyeragent_brokerage: "" });
    }
  };

  const CheckLicensed = (event) => {
    const select = event.target.value;
    setFormValuesNew({
      ...formValuesNew,
      buyer_licensed: select,
    });
    if (!select) {
      setFormErrorsNew({
        ...formErrorsNew,
        buyer_licensed: "Buyer licensed is required",
      });
    } else {
      setFormErrorsNew({ ...formErrorsNew, buyer_licensed: "" });
    }
  };

  const [formErrors, setFormErrors] = useState({
    hoa_due: "Due is required",
    hoa_duesfrequency: "Hoa dues frequency is required",
    hoa_firstname: "First Name is required",
    hoa_lastname: "Last Name is required",
    hoa_contact: "Contact is required",
    hoa_company: "Company is required",
  });

  const handleSubmitTransactionQuestion = async (e) => {
    e.preventDefault();
    const userData = {
      represent: formValuesNew.represent ?? "",
      type: formValuesNew.type ?? "",
      ownership: formValuesNew.ownership ?? "",
      phase: formValuesNew.phase ?? "",
    };
    try {
      setLoader({ isActive: true });

      const response = await user_service.TransactionUpdate(
        params.id,
        userData
      );
      setLoader({ isActive: false });
      if (response) {
        const userDatan = {
          addedBy: jwt(localStorage.getItem("auth")).id,
          agentId: jwt(localStorage.getItem("auth")).id,
          transactionId: params.id,
          message: "Transaction  Question Information Update",
          // transaction_property_address: "ADDRESS 2",
          note_type: "Property Settlement",
        };
        const responsen = await user_service.transactionNotes(userDatan);

        setLoader({ isActive: false });
        setToaster({
          types: "SelectProperty",
          isShow: true,
          toasterBody: response.data.message,
          message: "Property Updated Successfully",
        });
        setTimeout(() => {
          setToaster((prevToaster) => ({
            ...prevToaster,
            isShow: false,
          }));
        }, 2000);
        TransactionGetById(params.id);
      } else {
        setLoader({ isActive: false });
        setToaster({
          types: "error",
          isShow: true,
          toasterBody: response.data.message,
          message: "Error",
        });
      }
    } catch (error) {
      setLoader({ isActive: false });
      setToaster({
        types: "error",
        isShow: true,
        toasterBody: error,
        message: "Error",
      });
    }
  };

  const handleSubmitTransaction = async (e) => {
    if (formValuesNew?._id) {
      e.preventDefault();

      if (1 == 1) {
        const userData = {
          seller_deadline: formValuesNew.seller_deadline,
          diligence_deadline: formValuesNew.diligence_deadline,
          financing_deadline: formValuesNew.financing_deadline,
          appraisal_edeadline: formValuesNew.appraisal_edeadline,
          settlement_deadline: formValuesNew.settlement_deadline,
          propert_built: formValuesNew.propert_built,
          buyeragent_brokerage: formValuesNew.buyeragent_brokerage,
          buyer_licensed: formValuesNew.buyer_licensed,
        };

        try {
          setLoader({ isActive: true });

          const response = await user_service.TransactionUpdate(
            params.id,
            userData
          );
          setLoader({ isActive: false });
          if (response) {
            const userDatan = {
              addedBy: jwt(localStorage.getItem("auth")).id,
              agentId: jwt(localStorage.getItem("auth")).id,
              transactionId: params.id,
              message: "Transaction Property Settlement Information Update",
              // transaction_property_address: "ADDRESS 2",
              note_type: "Property Settlement",
            };
            const responsen = await user_service.transactionNotes(userDatan);

            setLoader({ isActive: false });
            setToaster({
              types: "SelectProperty",
              isShow: true,
              toasterBody: response.data.message,
              message: "Property Updated Successfully",
            });
            setTimeout(() => {
              setToaster((prevToaster) => ({
                ...prevToaster,
                isShow: false,
              }));
            }, 2000);
            TransactionGetById(params.id);
          } else {
            setLoader({ isActive: false });
            setToaster({
              types: "error",
              isShow: true,
              toasterBody: response.data.message,
              message: "Error",
            });
          }
        } catch (error) {
          setLoader({ isActive: false });
          setToaster({
            types: "error",
            isShow: true,
            toasterBody: error,
            message: "Error",
          });
        }
      }
    }
  };

  const handleChangeNew = (e) => {
    const { name, value } = e.target;
    setFormValues({ ...formValues, [name]: value });
  };

  const validatelisting = () => {
    const values = formValues;
    const errors = {};
    if (!values.listDate) {
      errors.listDate = "list Date is required";
    }

    if (!values.priceLease) {
      errors.priceLease = "List Price is required";
    }

    setFormErrors(errors);
    return errors;
  };
  const handleSubmitListing = async (e) => {
    if (formValues?._id) {
      e.preventDefault();
      let checkValue = validatelisting();
      if (_.isEmpty(checkValue)) {
        const userData = {
          listDate: formValues.listDate,
          expiration_date: formValues.expiration_date,
          previous_price: formValues.previous_price,
          priceLease: formValues.priceLease,
          trust_deedbalance1: formValues.trust_deedbalance1,
          trust_deedbalance2: formValues.trust_deedbalance2,
          amount: formValues.amount,
          description: formValues.description,
        };

        try {
          setLoader({ isActive: true });

          const response = await user_service.listingUpdate(
            formValues?._id,
            userData
          );
          setLoader({ isActive: false });
          if (response) {
            const userDatan = {
              addedBy: jwt(localStorage.getItem("auth")).id,
              agentId: jwt(localStorage.getItem("auth")).id,
              transactionId: params.id,
              message: "Transaction Property Listing Information Update",
              // transaction_property_address: "ADDRESS 2",
              note_type: "Property Update",
            };
            const responsen = await user_service.transactionNotes(userDatan);

            setLoader({ isActive: false });
            setToaster({
              types: "SelectProperty",
              isShow: true,
              toasterBody: response.data.message,
              message: "Property Updated Successfully",
            });
            setTimeout(() => {
              setToaster((prevToaster) => ({
                ...prevToaster,
                isShow: false,
              }));
            }, 2000);
            TransactionGetById(params.id);
          } else {
            setLoader({ isActive: false });
            setToaster({
              types: "error",
              isShow: true,
              toasterBody: response.data.message,
              message: "Error",
            });
          }
        } catch (error) {
          setLoader({ isActive: false });
          setToaster({
            types: "error",
            isShow: true,
            toasterBody: error,
            message: "Error",
          });
        }
      }
    }
  };

  const CheckOccupied = (event) => {
    const select = event.target.value;
    setFormValues({
      ...formValues,
      occupancy: select,
    });
  };

  const CheckHoaFrequency = (event) => {
    const select = event.target.value;
    setFormValues({
      ...formValues,
      hoa_duesfrequency: select,
    });
    if (!select) {
      setFormErrors({
        ...formErrors,
        hoa_duesfrequency: "Hoa dues frequency is required",
      });
    } else {
      setFormErrors({ ...formErrors, hoa_duesfrequency: "" });
    }
  };

  const handleChangeOccupancy = (e) => {
    const { name, value } = e.target;
    setFormValues({ ...formValues, [name]: value });
  };

  const handleSubmitOccupancy = async (e) => {
    if (formValues?._id) {
      e.preventDefault();
      const userData = {
        occupancy: formValues.occupancy,
        occupancy_name: formValues.occupancy_name,
        occupancy_email: formValues.occupancy_email,
        occupancy_phone: formValues.occupancy_phone,
        instruction: formValues.instruction,
      };

      try {
        setLoader({ isActive: true });

        const response = await user_service.listingUpdate(
          formValues?._id,
          userData
        );
        setLoader({ isActive: false });
        if (response) {
          const userDatan = {
            addedBy: jwt(localStorage.getItem("auth")).id,
            agentId: jwt(localStorage.getItem("auth")).id,
            transactionId: params.id,
            message: "Transaction Property Occupancy Update",
            // transaction_property_address: "ADDRESS 2",
            note_type: "Property Update",
          };
          const responsen = await user_service.transactionNotes(userDatan);

          setLoader({ isActive: false });
          setToaster({
            types: "SelectProperty",
            isShow: true,
            toasterBody: response.data.message,
            message: "Property Updated Successfully",
          });
          setTimeout(() => {
            setToaster((prevToaster) => ({
              ...prevToaster,
              isShow: false,
            }));
          }, 2000);
          TransactionGetById(params.id);
        } else {
          setLoader({ isActive: false });
          setToaster({
            types: "error",
            isShow: true,
            toasterBody: response.data.message,
            message: "Error",
          });
        }
      } catch (error) {
        setLoader({ isActive: false });
        setToaster({
          types: "error",
          isShow: true,
          toasterBody: error,
          message: "Error",
        });
      }
    }
  };

  const CheckHoa = (event) => {
    const select = event.target.value;
    setFormValues({
      ...formValues,
      hoa: select,
    });
  };

  const CheckHoaReinvestment = (event) => {
    const select = event.target.value;
    setFormValues({
      ...formValues,
      transfer_fee: select,
    });
  };

  const [contactsHoa, setContactsHoa] = useState("");
  const [getContactHoa, setGetContactHoa] = useState([]);

  const handleChangeHoa = (e) => {
    const { name, value } = e.target;
    setFormValues((prevValues) => ({
      ...prevValues,
      [name]: value,
    }));

    if (name === "hoa_firstname" && value.trim()) {
      SearchGetAllHoa(value); // Pass the updated value to the API
    }

    if (!value && formErrors[name]) {
      setFormErrors({ ...formErrors, [name]: `${name} is required` });
    } else if (formErrors[name]) {
      // Clear error if field is no longer empty
      setFormErrors({ ...formErrors, [name]: "" });
    }
  };

  const SearchGetAllHoa = async (searchValue) => {
    try {
      const response = await user_service.SearchContactGet(1, 10, searchValue);

      if (response && response.data) {
        setGetContactHoa(response.data.data);
      } else {
        setGetContactHoa([]);
      }
    } catch (error) {
      console.error("Error fetching contacts:", error);
      setGetContactHoa([]);
    }
  };

  const handleContactAssociateHoa = (contact) => {
    setContactsHoa(contact);
    setGetContactHoa([]);
    setFormValues((prevValues) => ({
      ...prevValues,
      hoa_firstname: contact.firstName || "",
      hoa_lastname: contact.lastName || "",
      hoa_contact: contact.phone || "",
      hoa_company: contact.company || "",
    }));
  };

  const formatCurrencyCommission = (value, currencySymbol) => {
    if (!isNaN(value)) {
      if (currencySymbol === "%") {
        return `%${value}`;
      } else {
        return value.toLocaleString("en-US", {
          style: "currency",
          currency: "USD",
          minimumFractionDigits: 0,
        });
      }
    } else {
      // Handle the case when value is not a number (e.g., NaN)
      return "";
    }
  };

  const CheckTransferfeeAmount = (event) => {
    const select = event.target.value;
    const currencySymbol = select === "percentage" ? "%" : "$";

    setFormValues((prevValues) => {
      const grossCommissionValue = prevValues.transfer_amount || "";
      const numericValue = parseFloat(
        grossCommissionValue.replace(/[^\d.]/g, "")
      );

      return {
        ...prevValues,
        transfer_feeamount: select,
        transfer_amount: formatCurrencyCommission(numericValue, currencySymbol),
      };
    });
  };

  const handleChangeFeeAmount = (e) => {
    const { name, value } = e.target;
    const numericValue = parseFloat(value.replace(/[^\d.]/g, ""));

    if (!isNaN(numericValue)) {
      const currencySymbol =
        formValues.transfer_feeamount === "percentage" ? "%" : "$";
      const formattedValue = formatCurrencyCommission(
        numericValue,
        currencySymbol
      );

      setFormValues((prevValues) => ({
        ...prevValues,
        [name]: formattedValue,
      }));
    } else {
      setFormValues((prevValues) => ({
        ...prevValues,
        [name]: "",
      }));
    }
  };

  const handleFileUploadDocument = async (e) => {
    const selectedFile = e.target.files[0];

    if (selectedFile) {
      setFileNew(selectedFile);

      const fileNameWithoutExtension = selectedFile.name.replace(
        /\.[^/.]+$/,
        ""
      ); // Extracts the file name without extension
      const formData = new FormData();
      formData.append("file", selectedFile);

      const config = {
        headers: {
          Accept: "application/json",
          "Content-Type": "multipart/form-data",
          Authorization: `Bearer ${localStorage.getItem("auth")}`,
        },
      };

      try {
        setLoader({ isActive: true });

        const uploadResponse = await axios.post(
          "https://api.brokeragentbase.com/upload",
          formData,
          config
        );
        const uploadedFileData = uploadResponse.data;

        setLoader({ isActive: false });
        setDataNew(uploadedFileData);
      } catch (error) {
        console.error("Error occurred during file upload:", error);
      }
    }
  };

  const handleSubmitHoa = async (e) => {
    if (formValues?._id) {
      if (formValues?.hoa === "no") {
        const userData = {
          hoa: formValues.hoa,
        };

        try {
          setLoader({ isActive: true });

          const response = await user_service.listingUpdate(
            formValues?._id,
            userData
          );
          setLoader({ isActive: false });
          if (response) {
            const userDatan = {
              addedBy: jwt(localStorage.getItem("auth")).id,
              agentId: jwt(localStorage.getItem("auth")).id,
              transactionId: params.id,
              message:
                "Transaction Property Home Owner Association Information Update",
              // transaction_property_address: "ADDRESS 2",
              note_type: "Property Update",
            };
            const responsen = await user_service.transactionNotes(userDatan);

            setLoader({ isActive: false });
            setToaster({
              types: "SelectProperty",
              isShow: true,
              toasterBody: response.data.message,
              message: "Property Updated Successfully",
            });
            setTimeout(() => {
              setToaster((prevToaster) => ({
                ...prevToaster,
                isShow: false,
              }));
            }, 2000);
            TransactionGetById(params.id);
          } else {
            setLoader({ isActive: false });
            setToaster({
              types: "error",
              isShow: true,
              toasterBody: response.data.message,
              message: "Error",
            });
          }
        } catch (error) {
          setLoader({ isActive: false });
          setToaster({
            types: "error",
            isShow: true,
            toasterBody: error,
            message: "Error",
          });
        }
      } else {
        e.preventDefault();
        // for (const key in formErrors) {
        //   if (formErrors[key]) {
        //     // If there's any error, display the toaster message and exit
        //     setToaster({
        //       types: "error",
        //       isShow: true,
        //       toasterBody: "Please fill in all required fields.",
        //       message: "Error",
        //     });

        //     setTimeout(() => {
        //       setToaster((prevToaster) => ({
        //         ...prevToaster,
        //         isShow: false,
        //       }));
        //     }, 2000);

        //     return;
        //   }
        // }
        const userData = {
          agentId: jwt(localStorage.getItem("auth")).id,
          hoa: formValues.hoa,
          hoa_due: formValues.hoa_due,
          hoa_duesfrequency: formValues.hoa_duesfrequency,
          hoa_firstname: formValues.hoa_firstname,
          hoa_lastname: formValues.hoa_lastname,
          hoa_contact: formValues.hoa_contact,
          hoa_company: formValues.hoa_company,
          hoa_address: formValues.hoa_address,
          hoa_website: formValues.hoa_website,
          transfer_fee: formValues.transfer_fee,
          transfer_feeamount: formValues.transfer_feeamount,
          transfer_amount: formValues.transfer_amount,
          comment: formValues.comment,
          hoa_document: dataNew,
        };
        try {
          setLoader({ isActive: true });

          const response = await user_service.listingUpdate(
            formValues?._id,
            userData
          );
          setLoader({ isActive: false });
          if (response) {
            const userDatan = {
              addedBy: jwt(localStorage.getItem("auth")).id,
              agentId: jwt(localStorage.getItem("auth")).id,
              transactionId: params.id,
              message:
                "Transaction Property Home Owner Association Information Update",
              // transaction_property_address: "ADDRESS 2",
              note_type: "Property Update",
            };
            const responsen = await user_service.transactionNotes(userDatan);

            setLoader({ isActive: false });
            setToaster({
              types: "SelectProperty",
              isShow: true,
              toasterBody: response.data.message,
              message: "Property Updated Successfully",
            });
            setTimeout(() => {
              setToaster((prevToaster) => ({
                ...prevToaster,
                isShow: false,
              }));
            }, 2000);
            TransactionGetById(params.id);
          } else {
            setLoader({ isActive: false });
            setToaster({
              types: "error",
              isShow: true,
              toasterBody: response.data.message,
              message: "Error",
            });
          }
        } catch (error) {
          setLoader({ isActive: false });
          setToaster({
            types: "error",
            isShow: true,
            toasterBody: error,
            message: "Error",
          });
        }
      }
    }
  };

  const handleChangeHomeWarranty = (e) => {
    const { name, value } = e.target;
    setFormValues({ ...formValues, [name]: value });
  };

  const CheckHome_warranty_transaction = (event) => {
    const select = event.target.value;
    setFormValues({
      ...formValues,
      home_Warranty_Ordered_transaction: select,
    });
  };

  const CheckHome_warranty_Ordered = (event) => {
    const select = event.target.value;
    setFormValues({
      ...formValues,
      home_Warranty_Ordered: select,
    });
  };

  const handleSubmitHomeWarranty = async (e) => {
    if (formValues?._id) {
      if (formValues?.home_Warranty_Ordered_transaction === "no") {
        e.preventDefault();
        const userData = {
          home_Warranty_Ordered_transaction:
            formValues.home_Warranty_Ordered_transaction,
        };

        try {
          setLoader({ isActive: true });
          const response = await user_service.listingUpdate(
            formValues?._id,
            userData
          );
          setLoader({ isActive: false });
          if (response) {
            const userDatan = {
              addedBy: jwt(localStorage.getItem("auth")).id,
              agentId: jwt(localStorage.getItem("auth")).id,
              transactionId: params.id,
              message: "Transaction Property Home Warranty Update",
              // transaction_property_address: "ADDRESS 2",
              note_type: "Property Update",
            };
            const responsen = await user_service.transactionNotes(userDatan);

            setLoader({ isActive: false });
            setToaster({
              types: "SelectProperty",
              isShow: true,
              message: "Property Updated Successfully",
            });
            setTimeout(() => {
              setToaster((prevToaster) => ({
                ...prevToaster,
                isShow: false,
              }));
            }, 2000);
            TransactionGetById(params.id);
          } else {
            setLoader({ isActive: false });
            setToaster({
              types: "error",
              isShow: true,
              message: "Error",
            });
          }
        } catch (error) {
          setLoader({ isActive: false });
          setToaster({
            types: "error",
            isShow: true,
            message: "Error",
          });
        }
      } else {
        e.preventDefault();
        const userData = {
          home_Warranty_Ordered_transaction:
            formValues.home_Warranty_Ordered_transaction,
          home_Warranty_Company: formValues.home_Warranty_Company,
          home_warranty_price: formValues.home_warranty_price,
          paying_Home_warranty: formValues.paying_Home_warranty,
          home_Warranty_contact: formValues.home_Warranty_contact,
          home_warranty_website: formValues.home_warranty_website,
          home_Warranty_Ordered: formValues.home_Warranty_Ordered,
          home_warranty_date_ordered: formValues.home_warranty_date_ordered,
          home_warranty_who_ordered: formValues.home_warranty_who_ordered,
          home_warranty_both_Pay: formValues.home_warranty_both_Pay,
        };

        try {
          setLoader({ isActive: true });
          const response = await user_service.listingUpdate(
            formValues?._id,
            userData
          );
          setLoader({ isActive: false });
          if (response) {
            const userDatan = {
              addedBy: jwt(localStorage.getItem("auth")).id,
              agentId: jwt(localStorage.getItem("auth")).id,
              transactionId: params.id,
              message: "Transaction Property Home Warranty Update",
              // transaction_property_address: "ADDRESS 2",
              note_type: "Property Update",
            };
            const responsen = await user_service.transactionNotes(userDatan);

            setLoader({ isActive: false });
            setToaster({
              types: "SelectProperty",
              isShow: true,
              message: "Property Updated Successfully",
            });
            setTimeout(() => {
              setToaster((prevToaster) => ({
                ...prevToaster,
                isShow: false,
              }));
            }, 2000);
            TransactionGetById(params.id);
          } else {
            setLoader({ isActive: false });
            setToaster({
              types: "error",
              isShow: true,
              message: "Error",
            });
          }
        } catch (error) {
          setLoader({ isActive: false });
          setToaster({
            types: "error",
            isShow: true,
            message: "Error",
          });
        }
      }
    }
  };

  const handleDateChange = (value) => {
    setFormValues({
      ...formValues,
      home_Inspection_Datetime: value,
    });
  };

  const [contacts, setContacts] = useState("");
  const [getContactData, setGetContactData] = useState([]);

  const handleContactAssociate = (contact) => {
    setContacts(contact);
    setGetContactData([]);
    setFormValues((prevValues) => ({
      ...prevValues,
      home_Inspection_Company: contact.company || "",
      home_Inspector_Name: contact.firstName || "",
      home_Inspector_Phone: contact.phone || "",
    }));
  };

  const handleChangeHomeInspection = (e) => {
    const { name, value } = e.target;
    setFormValues((prevValues) => ({
      ...prevValues,
      [name]: value,
    }));

    if (name === "home_Inspector_Name" && value.trim()) {
      SearchGetAll(value); // Pass the updated value to the API
    }
  };

  const SearchGetAll = async (searchValue) => {
    try {
      const response = await user_service.SearchContactGet(1, 10, searchValue);

      if (response && response.data) {
        setGetContactData(response.data.data);
      } else {
        setGetContactData([]);
      }
    } catch (error) {
      console.error("Error fetching contacts:", error);
      setGetContactData([]);
    }
  };

  // const handleChangeHomeInspection = (e) => {
  //   const { name, value } = e.target;
  //   setFormValues({ ...formValues, [name]: value });
  // };

  const CheckHomeInspectionProperty = (event) => {
    const select = event.target.value;
    setFormValues({
      ...formValues,
      home_Inspection_Property: select,
    });
  };

  const CheckHomeInspectionOrdered = (event) => {
    const select = event.target.value;
    setFormValues({
      ...formValues,
      home_Inspection_Ordered: select,
    });
  };

  const handleSubmitHomeInspection = async (e) => {
    if (formValues?._id) {
      if (formValues?.home_Inspection_Property === "no") {
        e.preventDefault();
        const userData = {
          home_Inspection_Property: formValues.home_Inspection_Property,
        };
        try {
          setLoader({ isActive: true });
          const response = await user_service.listingUpdate(
            formValues?._id,
            userData
          );
          setLoader({ isActive: false });
          if (response) {
            const userDatan = {
              addedBy: jwt(localStorage.getItem("auth")).id,
              agentId: jwt(localStorage.getItem("auth")).id,
              transactionId: params.id,
              message: "Transaction Property Home Inspection Update",
              // transaction_property_address: "ADDRESS 2",
              note_type: "Property Update",
            };
            const responsen = await user_service.transactionNotes(userDatan);

            setLoader({ isActive: false });
            setToaster({
              types: "SelectProperty",
              isShow: true,
              message: "Property Updated Successfully",
            });
            setTimeout(() => {
              setToaster((prevToaster) => ({
                ...prevToaster,
                isShow: false,
              }));
            }, 2000);
            TransactionGetById(params.id);
          } else {
            setLoader({ isActive: false });
            setToaster({
              types: "error",
              isShow: true,
              message: "Error",
            });
          }
        } catch (error) {
          setLoader({ isActive: false });
          setToaster({
            types: "error",
            isShow: true,
            message: "Error",
          });
        }
      } else {
        e.preventDefault();
        const userData = {
          home_Inspection_Property: formValues.home_Inspection_Property,
          home_Inspection_Company: formValues.home_Inspection_Company,
          home_Inspector_Name: formValues.home_Inspector_Name,
          home_Inspector_Phone: formValues.home_Inspector_Phone,
          home_Companies_Website: formValues.home_Companies_Website,
          home_Inspector_Price: formValues.home_Inspector_Price,
          home_Inspection_Ordered: formValues.home_Inspection_Ordered,
          home_Date_Ordered: formValues.home_Date_Ordered,
          home_Who_Ordered: formValues.home_Who_Ordered,
          home_Inspection_Datetime: formValues.home_Inspection_Datetime,
        };
        try {
          setLoader({ isActive: true });
          const response = await user_service.listingUpdate(
            formValues?._id,
            userData
          );
          setLoader({ isActive: false });
          if (response) {
            const userDatan = {
              addedBy: jwt(localStorage.getItem("auth")).id,
              agentId: jwt(localStorage.getItem("auth")).id,
              transactionId: params.id,
              message: "Transaction Property Home Inspection Update",
              // transaction_property_address: "ADDRESS 2",
              note_type: "Property Update",
            };
            const responsen = await user_service.transactionNotes(userDatan);

            setLoader({ isActive: false });
            setToaster({
              types: "SelectProperty",
              isShow: true,
              message: "Property Updated Successfully",
            });
            setContacts("");
            setGetContact([]);
            setTimeout(() => {
              setToaster((prevToaster) => ({
                ...prevToaster,
                isShow: false,
              }));
            }, 2000);
            TransactionGetById(params.id);
          } else {
            setLoader({ isActive: false });
            setToaster({
              types: "error",
              isShow: true,
              message: "Error",
            });
          }
        } catch (error) {
          setLoader({ isActive: false });
          setToaster({
            types: "error",
            isShow: true,
            message: "Error",
          });
        }
      }
    }
  };

  const handleChangeUtilities = (e) => {
    const { name, value } = e.target;
    setFormValues({ ...formValues, [name]: value });
  };

  const handleSubmitUlities = async (e) => {
    if (formValues?._id) {
      e.preventDefault();
      const userData = {
        electric_company: formValues.electric_company,
        garbage_company: formValues.garbage_company,
        gas_company: formValues.gas_company,
        sewer_Company: formValues.sewer_Company,
        sewer_septic: formValues.sewer_septic ?? "Sewer",
        water_Company: formValues.water_Company,
      };
      try {
        setLoader({ isActive: true });
        const response = await user_service.listingUpdate(
          formValues?._id,
          userData
        );
        setLoader({ isActive: false });
        if (response) {
          const userDatan = {
            addedBy: jwt(localStorage.getItem("auth")).id,
            agentId: jwt(localStorage.getItem("auth")).id,
            transactionId: params.id,
            message: "Transaction Property Utilities Update",
            // transaction_property_address: "ADDRESS 2",
            note_type: "Property Update",
          };
          const responsen = await user_service.transactionNotes(userDatan);

          setLoader({ isActive: false });
          setToaster({
            types: "SelectProperty",
            isShow: true,
            message: "Property Updated Successfully",
          });
          setTimeout(() => {
            setToaster((prevToaster) => ({
              ...prevToaster,
              isShow: false,
            }));
          }, 2000);
          TransactionGetById(params.id);
        } else {
          setLoader({ isActive: false });
          setToaster({
            types: "error",
            isShow: true,
            message: "Error",
          });
        }
      } catch (error) {
        setLoader({ isActive: false });
        setToaster({
          types: "error",
          isShow: true,
          message: "Error",
        });
      }
    }
  };

  const handleDelete = async () => {
    if (formValues?._id) {
      const userData = {
        hoa_document: "",
      };

      try {
        setLoader({ isActive: true });

        const response = await user_service.listingUpdate(
          formValues?._id,
          userData
        );
        setLoader({ isActive: false });
        if (response) {
          setLoader({ isActive: false });
          setToaster({
            types: "SelectProperty",
            isShow: true,
            toasterBody: response.data.message,
            message: "Uplaod deleted Successfully",
          });
          setTimeout(() => {
            setToaster((prevToaster) => ({
              ...prevToaster,
              isShow: false,
            }));
          }, 2000);
          TransactionGetById(params.id);
        } else {
          setLoader({ isActive: false });
          setToaster({
            types: "error",
            isShow: true,
            toasterBody: response.data.message,
            message: "Error",
          });
        }
      } catch (error) {
        setLoader({ isActive: false });
        setToaster({
          types: "error",
          isShow: true,
          toasterBody: error,
          message: "Error",
        });
      }
    }
  };

  const TransactionFundingRequest = async () => {
    await user_service.transactionFundingRequest(params.id).then((response) => {
      if (response) {
        setLoader({ isActive: false });
        console.log(response.data);
        console.log("response.data");
        setTransactionFundingRequest(response.data.data[0]);
      }
    });
  };

  const [membership, setMembership] = useState("");

  const [result, setResult] = useState([]);
  const [searchmlsnumber, setSearchmlsnumber] = useState({ mlsNumber: "" });

  const [select, setSelect] = useState("");

  const [isLoading, setIsLoading] = useState(false);

  useEffect(() => {
    window.scrollTo(0, 0);
    ContactGetById(jwt(localStorage.getItem("auth")).id);
  }, []);

  const handleChangesearchmls = (event) => {
    setSearchmlsnumber({ mlsnumber: event.target.value });
  };

  const MLSSearch = async () => {
    if (searchmlsnumber.mlsnumber) {
      try {
        setIsLoading(true);
        let response;
        if (membership === "UtahRealEstate.com") {
          console.log(membership);

          response = await user_service.listingSearchMLS(
            searchmlsnumber.mlsnumber
          );
        } else if (membership === "Park City") {
          response = await user_service.listingSearchMLSspark(
            searchmlsnumber.mlsnumber,
            "parkcity"
          );
        } else if (membership === "Washington County") {
          response = await user_service.listingSearchMLSspark(
            searchmlsnumber.mlsnumber,
            "washingtoncounty"
          );
        } else if (membership === "Iron County") {
          response = await user_service.listingSearchMLSspark(
            searchmlsnumber.mlsnumber,
            "ironcountry"
          );
        } else {
          response = await user_service.listingSearchMLS(
            searchmlsnumber.mlsnumber
          );
        }

        if (response) {
          setResult(response.data);
        }

        setIsLoading(false);
      } catch (error) {
        console.error(error);
        setIsLoading(false);
      }
    }
  };

  const [getContact, setGetContact] = useState(initialValues);
  const [mls_membership, setMls_membership] = useState([]);
  const [
    commaSeparatedValuesmls_membership,
    setCommaSeparatedValuesmls_membership,
  ] = useState("");

  const ContactGetById = async () => {
    await user_service
      .contactGetById(jwt(localStorage.getItem("auth")).id)
      .then((response) => {
        if (response) {
          setGetContact(response.data);

          if (response.data && response.data.additionalActivateFields) {
            const mls_membershipString =
              response.data.additionalActivateFields?.mls_membership;
            if (mls_membershipString) {
              // Check if mls_membershipString is a string
              if (typeof mls_membershipString === "string") {
                try {
                  // Attempt to parse the string as JSON
                  const mls_membershipParsed = JSON.parse(mls_membershipString);
                  // Set the parsed value to your state or variable
                  if (mls_membershipParsed == "") {
                    setMls_membership(mls_membership);
                  } else {
                    setMls_membership(mls_membershipParsed);
                  }
                  if (mls_membershipParsed == "") {
                    setCommaSeparatedValuesmls_membership("");
                  } else {
                    const boardMembershipValues = mls_membershipParsed.map(
                      (item) => item.value
                    );
                    //   console.log("boardMembershipValues")
                    //   console.log(boardMembershipValues)
                    setCommaSeparatedValuesmls_membership(
                      // boardMembershipValues.join(", ")
                      boardMembershipValues
                    );
                  }
                } catch (error) {
                  // Handle the error if parsing fails
                  console.error("Error parsing mls_membership as JSON:", error);
                  // You might want to set a default value or handle the error in another way
                  // For debugging, you can log the JSON parse error message:
                  console.error("JSON Parse Error:", error.message);
                  // Set a default value if parsing fails
                  setCommaSeparatedValuesmls_membership(""); // Replace "defaultValue" with your default value
                }
              } else {
                //console.log("sdfdsfdsfsd");
                // Handle the case where mls_membershipString is not a string (e.g., it's already an object)
                // You can directly set it to your state or variable
                setCommaSeparatedValuesmls_membership("");
              }
            } else {
              // Handle the case where mls_membershipString is undefined or null
              // Set a default value or handle it as needed
              setCommaSeparatedValuesmls_membership(""); // Replace "defaultValue" with your default value
            }
          }
        }
      });
  };

  const handleSubmitMlsListing = async (result) => {
    if (result.value) {
      const authId = jwt(localStorage.getItem("auth")).id;
      const listing = result.value[0];
      const mlspropdata = {
        agentId: authId,
        listingsAgent: listing?.ListAgentFullName ?? "N/A",
        category: listing?.PropertyType ?? "N/A",
        mlsId: listing?.ListingId?.toString() ?? "N/A",
        propertyType: listing?.PropertyType ?? "N/A",
        mlsNumber: listing?.ListingId?.toString() ?? "N/A",
        associateName: result.value[0]?.AssociationName
          ? result.value[0].AssociationName
          : "N/A",
        price: listing?.ListPrice?.toString() ?? "N/A",
        priceLease: listing?.LeaseAmount?.toString() ?? "N/A",
        garage: listing?.GarageSpaces?.toString() ?? "N/A",
        bed: listing?.BedroomsTotal?.toString() ?? "N/A",
        year_built: listing?.YearBuilt?.toString() ?? "N/A",
        baths: listing?.BathroomsTotalInteger?.toString() ?? "N/A",
        square_feet: listing?.LotSizeSquareFeet?.toString() ?? "N/A",
        building_area_total: listing?.BuildingAreaTotal?.toString() ?? "N/A",
        city: listing?.City ?? "N/A",
        state: listing?.StateOrProvince ?? "N/A",
        country: listing?.CountyOrParish ?? "N/A",
        subDivision: listing?.SubdivisionName ?? "N/A",
        streetAddress: listing?.UnparsedAddress ?? "N/A",
        zipCode: listing?.PostalCode?.toString() ?? "N/A",
        schoolDistrict: listing?.ElementarySchoolDistrict ?? "N/A",
        areaLocation: listing?.MLSAreaMajor ?? "N/A",
        streetDirection: "N/A",
        status: listing?.MlsStatus ?? "N/A",
        listDate: listing?.OriginalEntryTimestamp ?? new Date(),
        image: result.media[0]?.MediaURL ?? "N/A",
        listing_AgentID: listing?.ListAgentKey ?? "",
        propertyFrom: "mls",
        additionaldataMLS: JSON.stringify(result),
      };

      try {
        setLoader({ isActive: true });

        const response = await user_service.listingsCreate(mlspropdata);

        if (response) {
          console.log(response.data);
          const userData = { propertyDetail: response.data._id };
          const responseNew = await user_service.TransactionUpdate(
            params.id,
            userData
          );

          if (responseNew) {
            const userDatan = {
              addedBy: authId,
              agentId: authId,
              transactionId: params.id,
              message: "Property Added",
              note_type: "Property Add",
            };
            await user_service.transactionNotes(userDatan);

            setToaster({
              types: "SelectProperty",
              isShow: true,
              toasterBody: response.data.message,
              message: "Property Added Successfully",
            });

            setTimeout(() => {
              setToaster((prevToaster) => ({
                ...prevToaster,
                isShow: false,
              }));
            }, 2000);

            TransactionGetById(params.id);
          }
        } else {
          setToaster({
            types: "error",
            isShow: true,
            message: "Error",
          });
        }

        setLoader({ isActive: false });
      } catch (error) {
        setLoader({ isActive: false });
        setToaster({
          types: "error",
          isShow: true,
          message: "Error",
        });
      }
    }
  };

  const options = useMemo(() => {
    const countries = countryList.getData();
    const utahState = {
      code: "UT",
      name: "Utah",
      districts: [
        "Alabama",
        "Alaska",
        "Arizona",
        "Arkansas",
        "California",
        "Colorado",
        "Connecticut",
        "Delaware",
        "District of Columbia",
        "Florida",
        "Georgia",
        "Hawaii",
        "Idaho",
        "Illinois",
        "Indiana",
        "Iowa",
        "Kansas",
        "Kentucky",
        "Louisiana",
        "Maine",
        "Maryland",
        "Massachusetts",
        "Michigan",
        "Minnesota",
        "Mississippi",
        "Missouri",
        "Montana",
        "Nebraska",
        "Nevada",
        "New Hampshire",
        "New Jersey",
        "New Mexico",
        "New York",
        "North Carolina",
        "North Dakota",
        "Ohio",
        "Oklahoma",
        "Oregon",
        "Pennsylvania",
        "Rhode Island",
        "South Carolina",
        "South Dakota",
        "Tennessee",
        "Texas",
        "Utah",
        "Vermont",
        "Virginia",
        "Washington",
        "West Virginia",
        "Wisconsin",
        "Wyoming",
      ],
    };
    return [...countries, utahState];
  }, []);

  const initialValuesproperty = {
    listingsAgent: "",
    category: "",
    unitNumber: "",
    propertyType: "",
    mlsNumber: "",
    agentId: "",
    streetDirection: "North",
    associateName: "",
    price: "",
    garage: "",
    bed: "",
    subDivision: "",
    streetAddress: "",
    city: "",
    state: "Utah",
    zipCode: "",
    schoolDistrict: "",
    country: "",
    areaLocation: "",
    status: "",
    listDate: "",
    image: "",
  };

  // const initialValues = {agentId:"", mlsNumber: "", areaLocation: "", streetAddress: "", state: "Utah", mlsId: "", city: "", zipCode: "", price: "", price: "", };
  const [formValuesproperty, setFormValuesproperty] = useState(
    initialValuesproperty
  );
  const [formErrorsproperty, setFormErrorsproperty] = useState({});
  const [isSubmitClickproperty, setISSubmitClickproperty] = useState(false);
  const [profile, setProfile] = useState("");

  const handleChangeproperty = (e) => {
    const { name, value } = e.target;
    setFormValuesproperty({ ...formValuesproperty, [name]: value });
  };

  const [isVisible, setIsVisible] = useState(false);

  const handleModal = () => {
    setIsVisible(true);
  };

  const handleBackProperty = () => {
    setIsVisible(false);
  };

  useEffect(() => {
    if (formValuesproperty && isSubmitClickproperty) {
      validateproperty();
    }
  }, [formValuesproperty]);

  const validateproperty = () => {
    const values = formValuesproperty;
    const errorsproperty = {};
    // if (!values.streetNumber) {
    //   errorsproperty.streetNumber = "Street Number is required";
    // }

    if (!values.streetAddress) {
      errorsproperty.streetAddress = "Property address is required";
    }

    if (!values.city) {
      errorsproperty.city = "City is required";
    }
    if (!values.state) {
      errorsproperty.state = "State is required";
    }

    if (!values.zipCode) {
      errorsproperty.zipCode = "Postal Code is required";
    }
    if (!values.propertyType) {
      errorsproperty.propertyType = "Property Type is required";
    }

    // if (data) {
    // } else {
    //   if (!values.image) {
    //     errorsproperty.image = "Property Image is required";
    //   }
    // }
    // if (!values.priceLease) {
    //   errorsproperty.priceLease = "PriceLease is required";
    // }
    setFormErrorsproperty(errorsproperty);

    // Scroll to the first error element
    const firstErrorKey = Object.keys(errorsproperty)[0];
    if (firstErrorKey) {
      const firstErrorElement = document.querySelector(
        `[name="${firstErrorKey}"]`
      );
      if (firstErrorElement) {
        // Scroll to the top of the error element
        window.scrollTo({
          top: firstErrorElement.offsetTop,
          behavior: "smooth", // You can use "auto" instead of "smooth" for an instant scroll
        });
      }
    }
    return errorsproperty;
  };

  const profileGetAll = async () => {
    setLoader({ isActive: true });
    await user_service
      .profileGet(jwt(localStorage.getItem("auth")).id)
      .then((response) => {
        if (response) {
          console.log(response);
          setLoader({ isActive: false });
          setProfile(response.data);
        }
      });
  };

  const handleSubmitProperty = async (e) => {
    e.preventDefault();
    setISSubmitClickproperty(true);
    let checkValueproperty = validateproperty();
    if (_.isEmpty(checkValueproperty)) {
      const currentDate = new Date();
      const formattedDate = currentDate.toISOString();

      const userData = {
        agentId: jwt(localStorage.getItem("auth")).id,
        // listingsAgent: "Shawn",
        // category: formValuesproperty.category,
        unitNumber: formValuesproperty.unitNumber,
        propertyType: formValuesproperty.propertyType,
        streetNumber: formValuesproperty.streetNumber,
        associateName: profile.firstName + " " + profile.lastName,
        price: formValuesproperty.price,
        priceLease: formValuesproperty.priceLease,
        city: formValuesproperty.city,
        state: formValuesproperty.state,
        country: "Utah",
        subDivision: "Utah",
        streetAddress: formValuesproperty.streetAddress,
        zipCode: formValuesproperty.zipCode,
        schoolDistrict: "Salt lake Utah",
        areaLocation: "Utah",
        streetDirection: formValuesproperty.streetDirection,
        status: "Active",
        listDate: formattedDate,
        image: data,
      };

      try {
        setLoader({ isActive: true });

        const response = await user_service.listingsCreate(userData);

        if (response) {
          console.log(response.data);
          const userData = { propertyDetail: response.data._id };
          const responseNew = await user_service.TransactionUpdate(
            params.id,
            userData
          );

          if (responseNew) {
            const userDatan = {
              addedBy: jwt(localStorage.getItem("auth")).id,
              agentId: jwt(localStorage.getItem("auth")).id,
              transactionId: params.id,
              message: "Property Add",
              note_type: "Add Property",
            };
            await user_service.transactionNotes(userDatan);

            setToaster({
              types: "SelectProperty",
              isShow: true,
              toasterBody: response.data.message,
              message: "Property Added Successfully",
            });

            setTimeout(() => {
              setToaster((prevToaster) => ({
                ...prevToaster,
                isShow: false,
              }));
            }, 2000);

            TransactionGetById(params.id);
            setIsVisible(false);
          }
        } else {
          setToaster({
            types: "error",
            isShow: true,
            message: "Error",
          });
        }

        setLoader({ isActive: false });
      } catch (error) {
        setLoader({ isActive: false });
        setToaster({
          types: "error",
          isShow: true,
          message: "Error",
        });
      }
    }
  };

  useEffect(() => {
    const fetchData = async () => {
      try {
        setLoader({ isActive: true });
        await Promise.all([
          TransactionGetById(params.id),
          Getrequireddoucmentno(),
          profileGetAll(),
          TransactionFundingRequest(params.id),
        ]);
      } catch (error) {
        console.error("Error in fetching data:", error);
        setLoader({ isActive: false });
      } finally {
        setLoader({ isActive: false });
      }
    };

    fetchData();
  }, [params.id]);
  return (
    <div className="bg-secondary float-left w-100 pt-4 mb-4">
      <Loader isActive={isActive} />
      {isShow && (
        <Toaster
          types={types}
          isShow={isShow}
          toasterBody={toasterBody}
          message={message}
        />
      )}

      <main className="">
        <div className="">
          <div className="content-overlay transaction_tab_view mt-lg-0 mt-md-0 mt-sm-4 mt-4">
            <div className="row">
              <AllTab />
              <div className="col-md-12">
                <div className="bg-light border rounded-3 mb-4 p-3">
                  <div className="col-md-12 py-2">
                    <h3 className="text-left mb-0 justify-content-start d-flex align-items-center">
                      Property
                    </h3>
                  </div>
                </div>
                {/* <StatusBar reloadstatusbar={reloadstatusbar} /> */}
                {formValuesNew?.propertyDetail === "" &&
                formValuesNew?.referralSend === "no" ? (
                  <div className="">
                    <div className="bg-light float-start w-100 rounded-3 mb-2 p-3">
                      <div className="">
                        <div className="d-flex">
                          <h6 className="">Search By Mls Number</h6>
                        </div>
                        <>
                          <label className="col-form-label">
                            Select Membership{" "}
                          </label>
                          <select
                            className="form-select mb-2"
                            name="membership"
                            onChange={(event) =>
                              setMembership(event.target.value)
                            }
                            value={membership}
                          >
                            <option value="">Please Select Membership</option>
                            {commaSeparatedValuesmls_membership &&
                            commaSeparatedValuesmls_membership.length > 0
                              ? commaSeparatedValuesmls_membership.map(
                                  (memberahip) => <option>{memberahip}</option>
                                )
                              : ""}
                          </select>

                          {membership ? (
                            <>
                              <h6 className="mt-2 mb-2">Enter a MLS Number:</h6>

                              <div className="col-md-12 mt-2">
                                <input
                                  className="form-control w-100"
                                  id="text-input-onee"
                                  type="text"
                                  name="mlsnumber"
                                  placeholder="Enter MLS number"
                                  onChange={handleChangesearchmls}
                                  value={searchmlsnumber.mlsnumber}
                                />
                              </div>

                              <div className="pull-left w-100 mb-4 mt-3">
                                <a
                                  className="btn btn-primary btn-sm order-lg-3"
                                  onClick={MLSSearch}
                                  disabled={isLoading}
                                >
                                  {isLoading ? "Please wait" : "Search"}
                                </a>
                              </div>
                            </>
                          ) : (
                            ""
                          )}

                          {result &&
                            result.value &&
                            result.value.length > 0 && (
                              <div className="card card-hover card-horizontal transactions border-0 shadow-sm mb-4 p-3 mt-3 float-start w-100">
                                <div className="media_images">
                                  {result?.media[0]?.MediaURL && (
                                    <div className="card-img-top">
                                      <img
                                        className="img-fluid"
                                        src={result.media[0].MediaURL}
                                      />
                                    </div>
                                  )}

                                  {/* <div className="property_thumb">
                               {result.media.map((mediaItem, mediaIndex) => (
                                 <img
                                   key={mediaIndex}
                                   className="media-image"
                                   src={mediaItem.MediaURL}
                                   onClick={() =>
                                     handleImageUrl(mediaItem.MediaURL)
                                   }
                                 />
                               ))}
                             </div> */}
                                </div>

                                <div className="card-body position-relative pb-3">
                                  <span className="mb-2">
                                    {result.value[0]?.CountyOrParish}_
                                    {result.value[0]?.ListingId}
                                  </span>
                                  <br />
                                  <span className="mb-2">
                                    {result.value[0]?.UnparsedAddress}
                                  </span>
                                  <br />
                                  <span className="mb-2">
                                    {result.value[0].City},{" "}
                                    {result.value[0].StateOrProvince}{" "}
                                    {result.value[0]?.PostalCode}
                                  </span>
                                  <br />
                                  <span className="mb-2">
                                    {`${
                                      result?.value[0]?.ListAgentFullName
                                    } / ${
                                      result?.value[0]?.ListOfficeName ?? ""
                                    }`}
                                  </span>
                                </div>

                                <div className="float-left text-left">
                                  <button
                                    className="btn btn-primary me-0"
                                    id="selectButton"
                                    type="button"
                                    data-toggle="modal"
                                    data-target="#mls_listing"
                                    onClick={() =>
                                      handleSubmitMlsListing(result)
                                    }
                                  >
                                    Submit Listing
                                  </button>

                                  {console.log(result)}

                                  <p className="my-2 badge bg-success">
                                    {result.value[0]?.ListPrice}
                                  </p>
                                  <br />
                                  <h6 className="mt-0 pull-left">
                                    {result.value[0]?.MlsStatus}
                                  </h6>
                                </div>
                              </div>
                            )}

                          <div className="social-login mb-2">
                            <div className="float-left w-100 mb-0">
                              <div className="mt-0">
                                <h4 className="or-border">
                                  <span>or</span>
                                </h4>
                              </div>
                            </div>
                          </div>

                          <div className="text-center pull-right w-100 mb-2 ">
                            <button
                              className="btn btn-info btn-sm text-white"
                              data-toggle="modal"
                              data-target="#addProperty"
                              onClick={handleModal}
                            >
                              Add a Property
                            </button>
                          </div>
                        </>
                      </div>
                    </div>
                  </div>
                ) : (
                  <>
                    <div className="bg-light p-3 border-0 rounded-3 shadow-sm float-start w-100">
                      <h3 className="mb-3">Property Details:</h3>
                      <hr className="my-3" />

                      {/* TOP SECTION start */}
                      <div className="row">
                        <div className="col-md-6 mb-3">
                          <label className="col-form-label">
                            Office Transaction ID
                          </label>
                          <input
                            className="form-control"
                            id=""
                            type="text"
                            name="reference_no"
                            value={formValuesNew.reference_no}
                            onChange={handleChange}
                            placeholder="Enter Your Office Id"
                            readOnly
                          />
                        </div>
                        <div className="col-md-6 mb-3">
                          <label
                            className="col-form-label d-flex"
                            
                          >
                            Transaction Email ID
                            {formValuesNew.transaction_email ? (
                              <div>
                                <i className="h2 fi-help opacity-80 ms-1 mt-1" style={{ cursor: "pointer" }}></i>
                                <span className="tooltiptext">
                                  <div className="col-md-12">
                                    <div className="gray-text p-3">
                                    <h6 className="mb-1">How It Works</h6>
                                   <p className="mb-1">Each transaction file is assigned a unique email address, which you can use to send	 transaction documents directly to the file. Here’s how the process works:</p> 
                                    <b>1. Forwarding Documents:</b>
                                   <p className="mb-1">Send or forward emails with attached documents to the unique transaction email address.</p> 
                                   <b>2. File Processing:</b>
                                   <p className="mb-1">BrokerAgentBase.com will automatically add the attached files to the bottom of the transaction file. However, the documents are not automatically sorted into specific sections of the file.</p> 
                                   <b>3. Manual Attachment:</b>
                                   <p className="mb-1">To organize and save the documents within the transaction file, you will need to use the "Manually Attach" link to move the files to the appropriate sections.</p> 
                                 
                                   <p className="mb-1">This system helps centralize document management while still allowing you full control over organizing and applying the files where needed.</p> 

                                    </div>
                                  </div>
                                </span>
                              </div>
                            ) : (
                              <span className="">
                                <button
                                  className="btn btn-primary btn-sm pull-right"
                                  type="button"
                                  onClick={createemail}
                                  title="Click this button to create the transaction email."
                                >
                                  Create Email
                                </button>
                              </span>
                            )}
                          </label>

                          <input
                            className="form-control"
                            id=""
                            type="text"
                            name="transaction_email"
                            value={formValuesNew?.transaction_email}
                            onChange={handleChange}
                            placeholder="Enter Transaction Email ID"
                            readOnly
                          />
                        </div>
                      </div>
                      <div className="row">
                        <div className="col-md-6 mb-3">
                          <label className="col-form-label">
                            Address Line 1*
                          </label>
                          <input
                            className="form-control"
                            id=""
                            type="text"
                            name="streetAddress"
                            value={formValues.streetAddress}
                            onChange={handleChange}
                            placeholder="Enter Address"
                          />
                        </div>

                        <div className="col-md-6 mb-3">
                          <label className="col-form-label">
                            Address Line 2
                          </label>
                          <input
                            className="form-control"
                            id=""
                            type="text"
                            name="address2"
                            value={formValues.address2}
                            onChange={handleChange}
                            placeholder="Enter Your Address"
                          />
                        </div>
                      </div>

                      <div className="row">
                        <div className="col-md-4 mb-3">
                          <label className="col-form-label">City*</label>
                          <input
                            className="form-control"
                            id=""
                            type="text"
                            name="city"
                            value={formValues.city}
                            onChange={handleChange}
                            placeholder="Enter Your City"
                          />
                        </div>
                        <div className="col-md-4 mb-3">
                          <label className="col-form-label">State*</label>
                          <input
                            className="form-control"
                            id=""
                            type="text"
                            name="state"
                            value={formValues.state}
                            onChange={handleChange}
                            placeholder="Enter Your State"
                          />
                        </div>
                        <div className="col-md-4 mb-3">
                          <label className="col-form-label">ZIP*</label>
                          <input
                            className="form-control"
                            id=""
                            type="text"
                            name="zipCode"
                            value={formValues.zipCode}
                            onChange={handleChange}
                            placeholder="Enter Your Zip-Code"
                          />
                        </div>
                      </div>
                      <div className="row">
                        <div className="col-md-6 mb-3">
                          <label className="col-form-label">What stage</label>
                          <div className="d-lg-flex d-md-block d-sm-block d-block">
                            <div className="form-check">
                              <input
                                className="form-check-input"
                                id=""
                                type="radio"
                                name="whatstage"
                                value="pre-listing"
                                onChange={handleChangeStage}
                                checked={formValuesNew?.phase === "pre-listing"}
                              />
                              <label className="form-label" id="">
                                Prospect/Pre-Listing
                              </label>
                            </div>

                            <div className="form-check ms-lg-3 ms-md-2">
                              <input
                                className="form-check-input"
                                id=""
                                type="radio"
                                name="whatstage"
                                value="active-listing"
                                onChange={handleChangeStage}
                                checked={
                                  formValuesNew?.phase === "active-listing"
                                }
                              />
                              <label className="form-label" id="">
                                Active
                              </label>
                            </div>

                            <div className="form-check ms-lg-3 ms-md-2">
                              <input
                                className="form-check-input"
                                id=""
                                type="radio"
                                name="whatstage"
                                value="showing-homes"
                                onChange={handleChangeStage}
                                checked={
                                  formValuesNew?.phase === "showing-homes"
                                }
                              />
                              <label className="form-label" id="">
                                Preparing
                              </label>
                            </div>

                            <div className="form-check ms-lg-3 ms-md-2">
                              <input
                                className="form-check-input"
                                id=""
                                type="radio"
                                name="whatstage"
                                value="under-contract"
                                onChange={handleChangeStage}
                                checked={
                                  formValuesNew?.phase === "under-contract"
                                }
                              />
                              <label className="form-label" id="">
                                Under Contract
                              </label>
                            </div>

                            <div className="form-check ms-lg-3 ms-md-2">
                              <input
                                className="form-check-input"
                                id=""
                                type="radio"
                                name="whatstage"
                                value="closed"
                                onChange={handleChangeStage}
                                checked={formValuesNew?.phase === "closed"}
                              />
                              <label className="form-label" id="">
                                Closed
                              </label>
                            </div>

                            <div className="form-check ms-lg-3 ms-md-2">
                              <input
                                className="form-check-input"
                                id=""
                                type="radio"
                                name="whatstage"
                                value="canceled"
                                onChange={handleChangeStage}
                                checked={formValuesNew?.phase === "canceled"}
                              />
                              <label className="form-label" id="">
                                Cancelled
                              </label>
                            </div>
                          </div>
                        </div>
                        <div className="col-md-4">
                          <label className="col-form-label">MLS#</label>
                          <div className="row">
                            <div className="col-lg-8 col-md-8 col-sm-12 col-12">
                              <input
                                className="form-control"
                                id=""
                                type="text"
                                name="mlsNumber"
                                value={formValues.mlsNumber}
                                onChange={handleChange}
                                readOnly
                              />
                            </div>
                            <div className="col-lg-4 col-md-4 col-sm-12 col-12">
                              <NavLink
                                className="btn btn-secondary btn-sm"
                                to={`/select-property/${params.id}`}
                              >
                                Update From MLS
                              </NavLink>
                            </div>
                          </div>
                        </div>
                        <div className="col-md-2">
                          <label className="col-form-label">Listed BY</label>
                          <input
                            className="form-control"
                            id=""
                            type="text"
                            name="listingsAgent"
                            value={formValues.listingsAgent}
                            onChange={handleChange}
                            placeholder="Enter Agent Name"
                          />
                        </div>
                      </div>
                      <div className="row mt-3">
                        <div className="col-md-2">
                          <label className="col-form-label" id="">
                            Property Photo
                          </label>
                          {formValues.image ? (
                            <>
                              <img
                                className="prop-property-img"
                                src={formValues.image}
                              />
                            </>
                          ) : (
                            <>
                              {data ? (
                                <img className="prop-property-img" src={data} />
                              ) : (
                                <input
                                  className="file-uploader file-uploader-grid"
                                  type="file"
                                  multiple
                                  name="image"
                                  onChange={handleFileUpload}
                                  data-max-files="4"
                                  data-max-file-size="2MB"
                                  accept="image/*"
                                  data-label-idle='<div className="btn btn-primary mb-3"><i className="fi-cloud-upload me-1"></i>Upload photos / video</div><div>or drag them in</div>'
                                />
                              )}
                            </>
                          )}
                        </div>

                        <div className="col-md-10">
                          <div className="row mt-3">
                            <div className="col-md-2 mb-3">
                              <label className="col-form-label" id="">
                                List Price
                              </label>
                              <div className="d-flex">
                                {formValues.price ? (
                                  <span className="mt-2 me-2">$</span>
                                ) : (
                                  ""
                                )}
                                <input
                                  className="form-control"
                                  id=""
                                  type="number"
                                  name="price"
                                  placeholder="Enter Price"
                                  value={formValues.price}
                                  onChange={handleChange}
                                />
                              </div>
                            </div>
                            <div className="col-md-2 mb-3">
                              <label className="col-form-label" id="">
                                Year Built
                              </label>
                              <input
                                className="form-control"
                                id=""
                                type="text"
                                name="year_built"
                                placeholder="Enter Year Built"
                                value={formValues?.year_built}
                                onChange={handleChange}
                                required
                              />
                            </div>
                            <div className="col-md-2 mb-3">
                              <label className="col-form-label" id="">
                                Beds
                              </label>
                              <input
                                className="form-control"
                                id=""
                                type="text"
                                name="bed"
                                placeholder="Enter Bed"
                                value={formValues.bed}
                                onChange={handleChange}
                              />
                            </div>
                            <div className="col-md-2 mb-3">
                              <label className="col-form-label" id="">
                                Baths
                              </label>
                              <input
                                className="form-control"
                                id=""
                                type="text"
                                name="baths"
                                placeholder="Enter Bath"
                                value={formValues?.baths}
                                onChange={handleChange}
                              />
                            </div>
                            {/* <div className="col-md-2 mb-3">
                              <label className="col-form-label" id="">
                                Square Feet
                              </label>
                              <input
                                className="form-control"
                                id=""
                                type="text"
                                name="square_feet"
                                placeholder="Enter Square Feet"
                                value={formValues?.square_feet}
                                onChange={handleChange}
                              />
                            </div> */}

                            <div className="col-md-2 mb-3">
                              <label className="col-form-label" id="">
                                Building Area(Square Feet)
                              </label>
                              <input
                                className="form-control"
                                id=""
                                type="text"
                                name="building_area_total"
                                placeholder="Enter Building Area"
                                value={formValues?.building_area_total}
                                onChange={handleChange}
                              />
                            </div>
                            <div className="col-md-2 mb-3">
                              <label className="col-form-label" id="">
                                Subdivision
                              </label>
                              <input
                                className="form-control"
                                id=""
                                type="text"
                                name="subDivision"
                                placeholder="Enter Subdivision"
                                value={formValues.subDivision}
                                onChange={handleChange}
                              />
                            </div>
                          </div>
                        </div>
                        {formValuesNew.filing === "filed_complted" ? (
                          <div className="pull-right w-100">
                            <button
                              className="btn btn-primary btn-sm pull-right"
                              type="button"
                              title="Your transaction was completed"
                            >
                              Save
                            </button>
                          </div>
                        ) : (
                          <>
                            {transactionFundingRequest?.invoice_url ? (
                              localStorage.getItem("auth") &&
                              jwt(localStorage.getItem("auth")).contactType ==
                                "admin" ? (
                                <>
                                  <div className="pull-right w-100">
                                    <button
                                      className="btn btn-primary btn-sm pull-right"
                                      type="button"
                                      onClick={handleSubmit}
                                    >
                                      Save
                                    </button>
                                  </div>
                                </>
                              ) : (
                                <></>
                              )
                            ) : (
                              <div className="pull-right w-100">
                                <button
                                  className="btn btn-primary btn-sm pull-right"
                                  type="button"
                                  onClick={handleSubmit}
                                >
                                  Save
                                </button>
                              </div>
                            )}
                          </>
                        )}
                      </div>
                    </div>

                    {/* {localStorage.getItem("auth") &&
                    jwt(localStorage.getItem("auth")).contactType == "admin" ? ( */}
                    <div className="bg-light border rounded-3 p-3 mt-5 float-start w-100">
                      <h3 className="mb-3">Transaction Question:</h3>
                      <hr className="my-3" />
                      <div className="row mt-3">
                        <div className="col-md-6 mb-3">
                          <label className="col-form-label">
                            Who do you represent in the transaction?
                          </label>
                          <select
                            className="form-select"
                            name="represent"
                            value={formValuesNew.represent}
                            onChange={handleChanget}
                          >
                            <option value="buyer">Buyer</option>
                            <option value="seller">Seller</option>
                            <option value="both"> Buyer & Seller</option>
                            <option value="referral">Referral</option>
                          </select>
                        </div>

                        {formValuesNew.represent === "buyer" ? (
                          <div className="col-md-6 mb-3">
                            <label className="form-label">
                              Please select the transaction type you’re working
                              on?
                            </label>
                            <select
                              className="form-select"
                              name="type"
                              value={formValuesNew.type}
                              onChange={handleChanget}
                            >
                              <option value="buy_residential">
                                Residential
                              </option>
                              <option value="buy_commercial">Commercial</option>
                              <option value="buy_investment">Investment</option>
                              <option value="buy_land">Land</option>
                              <option value="buy_referral">Referral</option>
                              <option value="buy_lease">Lease</option>
                            </select>
                          </div>
                        ) : (
                          ""
                        )}

                        {formValuesNew.represent === "seller" ? (
                          <div className="col-md-6 mb-3">
                            <label className="col-form-label">
                              Please select the transaction type you’re working
                              on?
                            </label>
                            <select
                              className="form-select"
                              name="type"
                              value={formValuesNew.type}
                              onChange={handleChanget}
                            >
                              <option value="sale_residential">
                                Residential
                              </option>
                              <option value="sale_commercial">
                                Commercial
                              </option>
                              <option value="sale_investment">
                                Investment
                              </option>
                              <option value="sale_referral">Referral</option>
                              <option value="sale_lease">Lease</option>
                            </select>
                          </div>
                        ) : (
                          ""
                        )}

                        {formValuesNew.represent === "both" ? (
                          <div className="col-md-6 mb-3">
                            <label className="col-form-label">
                              Please select the transaction type you’re working
                              on?
                            </label>
                            <select
                              className="form-select"
                              name="type"
                              value={formValuesNew.type}
                              onChange={handleChanget}
                            >
                              <option value="both_residential">
                                Residential
                              </option>
                              <option value="both_commercial">
                                Commercial
                              </option>
                              <option value="both_investment">
                                Investment
                              </option>
                              <option value="both_land">Land</option>
                              <option value="both_referral">Referral</option>
                              <option value="both_lease">Lease</option>
                            </select>
                          </div>
                        ) : (
                          ""
                        )}

                        {formValuesNew.represent === "referral" ? (
                          <div className="col-md-6 mb-3">
                            <label className="col-form-label">
                              Please select the transaction type you’re working
                              on?
                            </label>
                            <select
                              className="form-select"
                              name="type"
                              value={formValuesNew.type}
                              onChange={handleChanget}
                            >
                              <option value="referral_residential">
                                Residential
                              </option>
                              <option value="referral_commercial">
                                Commercial
                              </option>
                              <option value="referral_investment">
                                Investment
                              </option>
                              <option value="referral_referral">
                                Referral
                              </option>
                              <option value="referral_lease">Lease</option>
                            </select>
                          </div>
                        ) : (
                          ""
                        )}

                        <div className="col-md-6 mb-3">
                          <label className="col-form-label">
                            Who is the buyer/Seller? Please select an ownership
                            style.
                          </label>
                          <select
                            className="form-select"
                            name="ownership"
                            value={formValuesNew.ownership}
                            onChange={handleChanget}
                          >
                            <option value="individual">Individual</option>
                            <option value="corporation">
                              Entity/Corporation
                            </option>
                            <option value="trust">Trust</option>
                          </select>
                        </div>

                        <div className="col-md-6 mb-3">
                          <label className="col-form-label">
                            What phase is this transaction in?
                          </label>
                          <select
                            className="form-select"
                            name="phase"
                            value={formValuesNew.phase}
                            onChange={handleChanget}
                          >
                            <option value="pre-listed">Pre-Listed</option>
                            <option value="active-listing">
                              Active Listing
                            </option>
                            <option value="showing-homes">Showing homes</option>
                            <option value="under-contract">
                              Under Contract
                            </option>
                            <option value="closed">Closed</option>
                            <option value="canceled">Canceled</option>
                          </select>
                        </div>

                        {formValuesNew.filing === "filed_complted" ? (
                          <div className="pull-right w-100">
                            <button
                              className="btn btn-primary btn-sm pull-right"
                              type="button"
                              title="Your transaction was completed"
                            >
                              Save
                            </button>
                          </div>
                        ) : (
                          <>
                            {transactionFundingRequest?.invoice_url ? (
                              localStorage.getItem("auth") &&
                              jwt(localStorage.getItem("auth")).contactType ==
                                "admin" ? (
                                <>
                                  <div className="pull-right w-100">
                                    <button
                                      className="btn btn-primary btn-sm pull-right"
                                      type="button"
                                      onClick={handleSubmitTransactionQuestion}
                                    >
                                      Save
                                    </button>
                                  </div>
                                </>
                              ) : (
                                <></>
                              )
                            ) : (
                              <div className="pull-right w-100">
                                <button
                                  className="btn btn-primary btn-sm pull-right"
                                  type="button"
                                  onClick={handleSubmitTransactionQuestion}
                                >
                                  Save
                                </button>
                              </div>
                            )}
                          </>
                        )}
                      </div>
                    </div>
                    {/* ) : (
                      ""
                    )} */}

                    <div className="bg-light border rounded-3 p-3 mt-5 float-start w-100">
                      <h3 className="mb-3">
                        Disclosure / Settlement Information:
                      </h3>
                      <hr className="my-3" />
                      <div className="row mt-3">
                        <div className="col-md-3 mb-3">
                          <label className="col-form-label d-flex">
                            Seller's Disclosure Deadline*
                          </label>
                          <input
                            className="form-control"
                            id=""
                            type="date"
                            name="seller_deadline"
                            placeholder="Enter Seller's Disclosure Deadline"
                            value={formValuesNew.seller_deadline}
                            onChange={handleChangeData}
                            style={{
                              border:
                                formValuesNew.referralSend === "no"
                                  ? ""
                                  : formValuesNew.type === "buy_referral" ||
                                    formValuesNew.type === "sale_referral" ||
                                    formValuesNew.type === "both_referral"
                                  ? "1px solid #00000026"
                                  : formValuesNew.seller_deadline
                                  ? "1px solid #00000026"
                                  : formErrorsNew.seller_deadline
                                  ? "1px solid red"
                                  : "1px solid #00000026",
                            }}
                          />

                          {formValuesNew.referralSend === "no" ? (
                            <></>
                          ) : (
                            <>
                              {formValuesNew.type === "buy_referral" ||
                              formValuesNew.type === "sale_referral" ||
                              formValuesNew.type === "both_referral" ? (
                                <></>
                              ) : (
                                <>
                                  {!formValuesNew.seller_deadline && (
                                    <div className="invalid-tooltip">
                                      {formErrorsNew.seller_deadline}
                                    </div>
                                  )}
                                </>
                              )}
                            </>
                          )}
                        </div>

                        <div className="col-md-3 mb-3">
                          <label className="col-form-label d-flex">
                            Due Diligence Deadline *
                          </label>
                          <input
                            className="form-control"
                            id=""
                            type="date"
                            name="diligence_deadline"
                            placeholder="Enter Due Diligence Deadline"
                            value={formValuesNew.diligence_deadline}
                            onChange={handleChangeData}
                            style={{
                              border:
                                formValuesNew.referralSend === "no"
                                  ? ""
                                  : formValuesNew.type === "buy_referral" ||
                                    formValuesNew.type === "sale_referral" ||
                                    formValuesNew.type === "both_referral"
                                  ? "1px solid #00000026"
                                  : formValuesNew.diligence_deadline
                                  ? "1px solid #00000026"
                                  : formErrorsNew.diligence_deadline
                                  ? "1px solid red"
                                  : "1px solid #00000026",
                            }}
                          />

                          {formValuesNew.referralSend === "no" ? (
                            <></>
                          ) : (
                            <>
                              {formValuesNew.type === "buy_referral" ||
                              formValuesNew.type === "sale_referral" ||
                              formValuesNew.type === "both_referral" ? (
                                <></>
                              ) : (
                                <>
                                  {!formValuesNew.diligence_deadline && (
                                    <div className="invalid-tooltip">
                                      {formErrorsNew.diligence_deadline}
                                    </div>
                                  )}
                                </>
                              )}
                            </>
                          )}
                        </div>

                        <div className="col-md-3 mb-3">
                          <label className="col-form-label d-flex">
                            Financing Deadline *
                          </label>
                          <input
                            className="form-control"
                            id=""
                            type="date"
                            name="financing_deadline"
                            placeholder="Enter Financing Deadline"
                            value={formValuesNew.financing_deadline}
                            onChange={handleChangeData}
                            style={{
                              border:
                                formValuesNew.referralSend === "no"
                                  ? ""
                                  : formValuesNew.type === "buy_referral" ||
                                    formValuesNew.type === "sale_referral" ||
                                    formValuesNew.type === "both_referral"
                                  ? "1px solid #00000026"
                                  : formValuesNew.financing_deadline
                                  ? "1px solid #00000026"
                                  : formErrorsNew.financing_deadline
                                  ? "1px solid red"
                                  : "1px solid #00000026",
                            }}
                          />

                          {formValuesNew.referralSend === "no" ? (
                            <></>
                          ) : (
                            <>
                              {formValuesNew.type === "buy_referral" ||
                              formValuesNew.type === "sale_referral" ||
                              formValuesNew.type === "both_referral" ? (
                                <></>
                              ) : (
                                <>
                                  {!formValuesNew.financing_deadline && (
                                    <div className="invalid-tooltip">
                                      {formErrorsNew.financing_deadline}
                                    </div>
                                  )}
                                </>
                              )}
                            </>
                          )}
                        </div>

                        <div className="col-md-3 mb-3">
                          <label className="col-form-label d-flex">
                            Appraisal Deadline *
                          </label>
                          <input
                            className="form-control"
                            id=""
                            type="date"
                            name="appraisal_edeadline"
                            placeholder="Enter Appraisal Deadline"
                            value={formValuesNew.appraisal_edeadline}
                            onChange={handleChangeData}
                            style={{
                              border:
                                formValuesNew.referralSend === "no"
                                  ? ""
                                  : formValuesNew.type === "buy_referral" ||
                                    formValuesNew.type === "sale_referral" ||
                                    formValuesNew.type === "both_referral"
                                  ? "1px solid #00000026"
                                  : formValuesNew.appraisal_edeadline
                                  ? "1px solid #00000026"
                                  : formErrorsNew.appraisal_edeadline
                                  ? "1px solid red"
                                  : "1px solid #00000026",
                            }}
                          />

                          {formValuesNew.referralSend === "no" ? (
                            <></>
                          ) : (
                            <>
                              {formValuesNew.type === "buy_referral" ||
                              formValuesNew.type === "sale_referral" ||
                              formValuesNew.type === "both_referral" ? (
                                <></>
                              ) : (
                                <>
                                  {!formValuesNew.appraisal_edeadline && (
                                    <div className="invalid-tooltip">
                                      {formErrorsNew.appraisal_edeadline}
                                    </div>
                                  )}
                                </>
                              )}
                            </>
                          )}
                        </div>
                      </div>
                      <div className="row mt-3">
                        <div className="col-md-3 mb-3">
                          <label className="col-form-label d-flex">
                            Settlement Deadline *
                          </label>
                          <input
                            className="form-control"
                            id=""
                            type="date"
                            name="settlement_deadline"
                            placeholder="Enter Settlement Deadline"
                            value={formValuesNew.settlement_deadline}
                            onChange={handleChangeData}
                            style={{
                              border:
                                formValuesNew.referralSend === "no"
                                  ? ""
                                  : formValuesNew.type === "buy_referral" ||
                                    formValuesNew.type === "sale_referral" ||
                                    formValuesNew.type === "both_referral"
                                  ? "1px solid #00000026"
                                  : formValuesNew.settlement_deadline
                                  ? "1px solid #00000026"
                                  : formErrorsNew.settlement_deadline
                                  ? "1px solid red"
                                  : "1px solid #00000026",
                            }}
                          />

                          {formValuesNew.referralSend === "no" ? (
                            <></>
                          ) : (
                            <>
                              {formValuesNew.type === "buy_referral" ||
                              formValuesNew.type === "sale_referral" ||
                              formValuesNew.type === "both_referral" ? (
                                <></>
                              ) : (
                                <>
                                  {!formValuesNew.settlement_deadline && (
                                    <div className="invalid-tooltip">
                                      {formErrorsNew.settlement_deadline}
                                    </div>
                                  )}
                                </>
                              )}
                            </>
                          )}
                        </div>

                        <div className="col-md-3 mb-3">
                          <label className="col-form-label d-flex">
                            Was the property built before 1978? *
                          </label>
                          <div className="d-flex">
                            <div className="form-check">
                              <input
                                className="form-check-input"
                                id=""
                                type="radio"
                                name="propertybuilt"
                                value="yes"
                                onChange={CheckPropertbuilt}
                                checked={formValuesNew?.propert_built === "yes"}
                              />
                              <label className="form-label" id="">
                                Yes
                              </label>
                            </div>
                            <div className="form-check ms-3">
                              <input
                                className="form-check-input"
                                id=""
                                type="radio"
                                name="propertybuilt"
                                value="no"
                                onChange={CheckPropertbuilt}
                                checked={formValuesNew?.propert_built === "no"}
                              />
                              <label className="form-label" id="">
                                No
                              </label>
                            </div>
                          </div>

                          {formValuesNew.referralSend === "no" ? (
                            <></>
                          ) : (
                            <>
                              {formValuesNew.type === "buy_referral" ||
                              formValuesNew.type === "sale_referral" ||
                              formValuesNew.type === "both_referral" ? (
                                <></>
                              ) : (
                                <>
                                  {!formValuesNew.propert_built && (
                                    <div className="invalid-tooltip">
                                      {formErrorsNew.propert_built}
                                    </div>
                                  )}
                                </>
                              )}
                            </>
                          )}
                        </div>

                        <div className="col-md-6 mb-3">
                          <label className="col-form-label d-flex">
                            Is the Buyer's also a member of your brokerage? *
                          </label>
                          <div className="d-flex">
                            <div className="form-check">
                              <input
                                className="form-check-input"
                                id=""
                                type="radio"
                                name="brokerage"
                                value="yes"
                                onChange={CheckBrokerage}
                                checked={
                                  formValuesNew?.buyeragent_brokerage === "yes"
                                }
                              />
                              <label className="form-label" id="">
                                Yes
                              </label>
                            </div>
                            <div className="form-check ms-3">
                              <input
                                className="form-check-input"
                                id=""
                                type="radio"
                                name="brokerage"
                                value="no"
                                onChange={CheckBrokerage}
                                checked={
                                  formValuesNew?.buyeragent_brokerage === "no"
                                }
                              />
                              <label className="form-label" id="">
                                No
                              </label>
                            </div>
                          </div>

                          {formValuesNew.referralSend === "no" ? (
                            <></>
                          ) : (
                            <>
                              {formValuesNew.type === "buy_referral" ||
                              formValuesNew.type === "sale_referral" ||
                              formValuesNew.type === "both_referral" ? (
                                <></>
                              ) : (
                                <>
                                  {!formValuesNew.buyeragent_brokerage && (
                                    <div className="invalid-tooltip">
                                      {formErrorsNew.buyeragent_brokerage}
                                    </div>
                                  )}
                                </>
                              )}
                            </>
                          )}
                        </div>
                      </div>

                      <div className="row mt-3">
                        <div className="col-md-3 mb-3">
                          <label className="col-form-label d-flex">
                            Is the Buyer a licensed real state agent? *
                          </label>
                          <div className="d-flex">
                            <div className="form-check">
                              <input
                                className="form-check-input"
                                id=""
                                type="radio"
                                name="licensed"
                                value="yes"
                                onChange={CheckLicensed}
                                checked={
                                  formValuesNew?.buyer_licensed === "yes"
                                }
                              />
                              <label className="form-label" id="">
                                Yes
                              </label>
                            </div>
                            <div className="form-check ms-3">
                              <input
                                className="form-check-input"
                                id=""
                                type="radio"
                                name="licensed"
                                value="no"
                                onChange={CheckLicensed}
                                checked={formValuesNew?.buyer_licensed === "no"}
                              />
                              <label className="form-label" id="">
                                No
                              </label>
                            </div>
                          </div>

                          {formValuesNew.referralSend === "no" ? (
                            <></>
                          ) : (
                            <>
                              {formValuesNew.type === "buy_referral" ||
                              formValuesNew.type === "sale_referral" ||
                              formValuesNew.type === "both_referral" ? (
                                <></>
                              ) : (
                                <>
                                  {!formValuesNew.buyer_licensed && (
                                    <div className="invalid-tooltip">
                                      {formErrorsNew.buyer_licensed}
                                    </div>
                                  )}
                                </>
                              )}
                            </>
                          )}
                        </div>

                        {formValuesNew.filing === "filed_complted" ? (
                          <div className="pull-right w-100">
                            <button
                              className="btn btn-primary btn-sm pull-right"
                              type="button"
                              title="Your transaction was completed"
                            >
                              Save
                            </button>
                          </div>
                        ) : (
                          <>
                            {transactionFundingRequest?.invoice_url ? (
                              localStorage.getItem("auth") &&
                              jwt(localStorage.getItem("auth")).contactType ==
                                "admin" ? (
                                <>
                                  <div className="pull-right w-100">
                                    <button
                                      className="btn btn-primary btn-sm pull-right"
                                      type="button"
                                      onClick={handleSubmitTransaction}
                                    >
                                      Save
                                    </button>
                                  </div>
                                </>
                              ) : (
                                <></>
                              )
                            ) : (
                              <div className="pull-right w-100">
                                <button
                                  className="btn btn-primary btn-sm pull-right"
                                  type="button"
                                  onClick={handleSubmitTransaction}
                                >
                                  Save
                                </button>
                              </div>
                            )}
                          </>
                        )}
                      </div>
                    </div>

                    <div className="bg-light border rounded-3 p-3 mt-5 float-start w-100">
                      <h3 className="mb-3">Listing Information:</h3>
                      <hr className="my-3" />

                      <div className="row">
                        <div className="col-md-3 mb-3">
                          <label className="col-form-label d-flex">
                            Listing Date
                          </label>
                          <input
                            className="form-control"
                            id=""
                            type="date"
                            name="listDate"
                            placeholder="Enter Listing Date"
                            value={formValues.listDate}
                            onChange={handleChangeNew}
                          />
                          <div className="invalid-tooltip">
                            {formErrors.listDate}
                          </div>
                        </div>
                        <div className="col-md-3 mb-3">
                          <label className="col-form-label d-flex">
                            Expiration Date
                          </label>
                          <input
                            className="form-control"
                            id=""
                            type="date"
                            name="expiration_date"
                            placeholder="Enter Expiration Date"
                            value={formValues.expiration_date}
                            onChange={handleChangeNew}
                          />
                        </div>
                        <div className="col-md-3 mb-3">
                          <label className="col-form-label">
                            Privious Price
                          </label>
                          <input
                            className="form-control"
                            id="inline-form-input"
                            type="text"
                            name="previous_price"
                            placeholder="Enter Privious Price"
                            value={formValues.previous_price}
                            onChange={handleChangeNew}
                          />
                          <div className="invalid-tooltip"></div>
                        </div>
                        <div className="col-md-3 mb-3">
                          <label className="col-form-label">Listed Price</label>
                          <input
                            className="form-control"
                            id="inline-form-input"
                            type="number"
                            name="priceLease"
                            placeholder="Enter Listed Price"
                            value={formValues.priceLease}
                            onChange={handleChangeNew}
                          />
                          <div className="invalid-tooltip">
                            {formErrors.priceLease}
                          </div>
                        </div>
                      </div>
                      <div className="row">
                        <div className="col-md-3 mb-3">
                          <label className="col-form-label d-flex">
                            Trust Deed Balance 1{" "}
                          </label>
                          <input
                            className="form-control"
                            id="inline-form-input"
                            type="text"
                            name="trust_deedbalance1"
                            placeholder="Enter Trust Deed Balance 1"
                            value={formValues.trust_deedbalance1}
                            onChange={handleChangeNew}
                          />
                        </div>
                        <div className="col-md-3 mb-3">
                          <label className="col-form-label d-flex">
                            Trust Deed Balance 2{" "}
                          </label>
                          <input
                            className="form-control"
                            id="inline-form-input"
                            type="text"
                            name="trust_deedbalance2"
                            placeholder="Enter Trust Deed Balance 2"
                            value={formValues.trust_deedbalance2}
                            onChange={handleChangeNew}
                          />
                        </div>
                      </div>
                      <div className="row">
                        <h4 className="mb-0">Other Lines</h4>
                        <div className="col-md-3 mb-3">
                          <label className="col-form-label d-flex">
                            Amount
                          </label>
                          <input
                            className="form-control"
                            id="inline-form-input"
                            type="text"
                            name="amount"
                            placeholder="Enter Amount"
                            value={formValues.amount}
                            onChange={handleChangeNew}
                          />
                        </div>
                        <div className="col-md-3 mb-3">
                          <label className="col-form-label">Description</label>
                          <textarea
                            className="form-control mt-0"
                            id="textarea-input"
                            rows="2"
                            name="description"
                            placeholder="Enter Description"
                            value={formValues.description}
                            onChange={handleChangeNew}
                            autocomplete="on"
                          ></textarea>
                        </div>

                        {formValuesNew.filing === "filed_complted" ? (
                          <div className="pull-right w-100">
                            <button
                              className="btn btn-primary btn-sm pull-right"
                              type="button"
                              title="Your transaction was completed"
                            >
                              Save
                            </button>
                          </div>
                        ) : (
                          <>
                            {transactionFundingRequest?.invoice_url ? (
                              localStorage.getItem("auth") &&
                              jwt(localStorage.getItem("auth")).contactType ==
                                "admin" ? (
                                <>
                                  <div className="pull-right w-100">
                                    <button
                                      className="btn btn-primary btn-sm pull-right"
                                      type="button"
                                      onClick={handleSubmitListing}
                                    >
                                      Save
                                    </button>
                                  </div>
                                </>
                              ) : (
                                <></>
                              )
                            ) : (
                              <div className="pull-right w-100">
                                <button
                                  className="btn btn-primary btn-sm pull-right"
                                  type="button"
                                  onClick={handleSubmitListing}
                                >
                                  Save
                                </button>
                              </div>
                            )}
                          </>
                        )}
                      </div>
                    </div>

                    <div className="bg-light border rounded-3 p-3 mt-5 float-start w-100">
                      <h3 className="mb-3">Occupancy</h3>
                      <hr className="my-3" />
                      <div className="">
                        <div className="row">
                          <div className="col-md-12">
                            <label className="col-form-label">Occupancy</label>
                            <div className="d-flex">
                              <div className="form-check">
                                <input
                                  className="form-check-input"
                                  id=""
                                  type="radio"
                                  name="occupancy"
                                  value="owner_occupied"
                                  onChange={CheckOccupied}
                                  checked={
                                    formValues?.occupancy === "owner_occupied"
                                  }
                                />
                                <label className="form-label" id="">
                                  Owner Occupied
                                </label>
                              </div>
                              <div className="form-check ms-3">
                                <input
                                  className="form-check-input"
                                  id=""
                                  type="radio"
                                  name="occupancy"
                                  value="vacant"
                                  onChange={CheckOccupied}
                                  checked={formValues?.occupancy === "vacant"}
                                />
                                <label className="form-label" id="">
                                  Vacant
                                </label>
                              </div>
                              <div className="form-check ms-3">
                                <input
                                  className="form-check-input"
                                  id=""
                                  type="radio"
                                  name="occupancy"
                                  value="tenant_occupied"
                                  onChange={CheckOccupied}
                                  checked={
                                    formValues?.occupancy === "tenant_occupied"
                                  }
                                />
                                <label className="form-label" id="">
                                  Tenant Occupied
                                </label>
                              </div>
                            </div>
                          </div>
                          {formValues.occupancy === "tenant_occupied" ? (
                            <div className="row">
                              <div className="col-md-4">
                                <label className="col-form-label d-flex">
                                  Occupancy Name:
                                </label>
                                <input
                                  className="form-control"
                                  id=""
                                  type="text"
                                  name="occupancy_name"
                                  placeholder="Enter Occupancy Name"
                                  value={formValues.occupancy_name}
                                  onChange={handleChangeOccupancy}
                                />
                              </div>
                              <div className="col-md-4">
                                <label className="col-form-label d-flex">
                                  Phone
                                </label>
                                <input
                                  className="form-control"
                                  id=""
                                  type="number"
                                  name="occupancy_phone"
                                  placeholder="Enter Phone"
                                  value={formValues.occupancy_phone}
                                  onChange={handleChangeOccupancy}
                                />
                              </div>

                              <div className="col-md-4">
                                <label className="col-form-label d-flex">
                                  Email Address
                                </label>
                                <input
                                  className="form-control"
                                  id=""
                                  type="text"
                                  name="occupancy_email"
                                  placeholder="Enter  Email Address "
                                  value={formValues.occupancy_email}
                                  onChange={handleChangeOccupancy}
                                />
                              </div>
                            </div>
                          ) : (
                            ""
                          )}

                          <div className="col-md-12 mb-3">
                            <label className="col-form-label">
                              Instructions
                            </label>
                            <textarea
                              className="form-control mt-0"
                              id="textarea-input"
                              rows="5"
                              name="instruction"
                              placeholder="Enter Instruction"
                              value={formValues.instruction}
                              onChange={handleChangeOccupancy}
                              autocomplete="on"
                            ></textarea>
                          </div>
                        </div>
                      </div>

                      {formValuesNew.filing === "filed_complted" ? (
                        <div className="pull-right w-100">
                          <button
                            className="btn btn-primary btn-sm pull-right"
                            type="button"
                            title="Your transaction was completed"
                          >
                            Save
                          </button>
                        </div>
                      ) : (
                        <>
                          {transactionFundingRequest?.invoice_url ? (
                            localStorage.getItem("auth") &&
                            jwt(localStorage.getItem("auth")).contactType ==
                              "admin" ? (
                              <>
                                <div className="pull-right w-100">
                                  <button
                                    className="btn btn-primary btn-sm pull-right"
                                    type="button"
                                    onClick={handleSubmitOccupancy}
                                  >
                                    Save
                                  </button>
                                </div>
                              </>
                            ) : (
                              <></>
                            )
                          ) : (
                            <div className="pull-right w-100">
                              <button
                                className="btn btn-primary btn-sm pull-right"
                                type="button"
                                onClick={handleSubmitOccupancy}
                              >
                                Save
                              </button>
                            </div>
                          )}
                        </>
                      )}
                    </div>

                    <div className="bg-light border rounded-3 p-3 mt-5 float-start w-100">
                      <h3 className="mb-3">Home Warranty</h3>
                      <hr className="my-3" />
                      <div className="row mt-3">
                        <div className="col-md-4 mb-3">
                          <label className="col-form-label">
                            Will a home warranty be ordered for this
                            transaction?
                          </label>
                          <div className="d-flex">
                            <div className="form-check ps-3">
                              <input
                                className="form-check-input"
                                id="form-radio-4"
                                type="radio"
                                name="home_Warranty_Ordered_transaction"
                                value="yes"
                                onChange={(e) =>
                                  CheckHome_warranty_transaction(e)
                                }
                                checked={
                                  formValues?.home_Warranty_Ordered_transaction ===
                                    "yes" ||
                                  !formValues.home_Warranty_Ordered_transaction
                                }
                              />
                              <label
                                className="form-check-label ms-2"
                                id="radio-level1"
                              >
                                Yes
                              </label>
                            </div>

                            <div className="form-check">
                              <input
                                className="form-check-input "
                                id="form-radio-4"
                                type="radio"
                                name="home_Warranty_Ordered_transaction"
                                value="no"
                                onChange={(e) =>
                                  CheckHome_warranty_transaction(e)
                                }
                                checked={
                                  formValues?.home_Warranty_Ordered_transaction ===
                                  "no"
                                }
                              />
                              <label
                                className="form-check-label ms-2"
                                id="radio-level1"
                              >
                                No
                              </label>
                            </div>
                          </div>
                        </div>

                        {formValues?.home_Warranty_Ordered_transaction ===
                        "no" ? (
                          <></>
                        ) : (
                          <>
                            <div className="col-md-4 mb-3">
                              <label className="col-form-label">
                                Home Warranty Company
                              </label>
                              <input
                                className="form-control"
                                id="inline-form-input"
                                type="text"
                                name="home_Warranty_Company"
                                value={formValues.home_Warranty_Company}
                                onChange={handleChangeHomeWarranty}
                              />
                            </div>

                            <div className="col-md-4 mb-3">
                              <label className="col-form-label">
                                Home Warranty Company Phone
                              </label>
                              <input
                                className="form-control"
                                id="inline-form-input"
                                type="number"
                                name="home_Warranty_contact"
                                value={formValues.home_Warranty_contact}
                                onChange={handleChangeHomeWarranty}
                              />
                            </div>

                            <div className="col-md-4 mb-3">
                              <label className="col-form-label">
                                Companies Website/URL
                              </label>
                              <input
                                className="form-control"
                                id="inline-form-input"
                                type="text"
                                name="home_warranty_website"
                                value={formValues.home_warranty_website}
                                onChange={handleChangeHomeWarranty}
                              />
                            </div>

                            <div className="col-md-4 mb-3">
                              <label className="col-form-label">
                                Home Warranty Price
                              </label>
                              <input
                                className="form-control"
                                id="inline-form-input"
                                type="number"
                                name="home_warranty_price"
                                value={formValues.home_warranty_price}
                                onChange={handleChangeHomeWarranty}
                              />
                            </div>

                            <div className="col-md-4 mb-3">
                              <label className="col-form-label">
                                Who is Paying for Home Warranty
                              </label>
                              <input
                                className="form-control"
                                id="inline-form-input"
                                type="text"
                                name="paying_Home_warranty"
                                value={formValues.paying_Home_warranty}
                                onChange={handleChangeHomeWarranty}
                              />
                            </div>

                            <div className="col-md-4 mb-3">
                              <label className="col-form-label">
                                Has the Home Warranty been ordered?
                              </label>
                              <div className="d-flex">
                                <div className="form-check ps-3">
                                  <input
                                    className="form-check-input"
                                    id="form-radio-4"
                                    type="radio"
                                    name="home_Warranty_Ordered"
                                    value="yes"
                                    onChange={(e) =>
                                      CheckHome_warranty_Ordered(e)
                                    }
                                    checked={
                                      formValues?.home_Warranty_Ordered ===
                                      "yes"
                                    }
                                  />
                                  <label
                                    className="form-check-label ms-2"
                                    id="radio-level1"
                                  >
                                    Yes
                                  </label>
                                </div>

                                <div className="form-check">
                                  <input
                                    className="form-check-input "
                                    id="form-radio-4"
                                    type="radio"
                                    name="home_Warranty_Ordered"
                                    value="no"
                                    onChange={(e) =>
                                      CheckHome_warranty_Ordered(e)
                                    }
                                    checked={
                                      formValues?.home_Warranty_Ordered === "no"
                                    }
                                  />
                                  <label
                                    className="form-check-label ms-2"
                                    id="radio-level1"
                                  >
                                    No
                                  </label>
                                </div>
                              </div>
                            </div>

                            <div className="col-md-4 mb-3">
                              <label className="col-form-label">
                                Date Ordered
                              </label>
                              <input
                                className="form-control"
                                id="inline-form-input"
                                type="date"
                                name="home_warranty_date_ordered"
                                value={formValues.home_warranty_date_ordered}
                                onChange={handleChangeHomeWarranty}
                              />
                            </div>

                            <div className="col-md-4 mb-3">
                              <label className="col-form-label">
                                Who Ordered
                              </label>
                              <input
                                className="form-control"
                                id="inline-form-input"
                                type="text"
                                name="home_warranty_who_ordered"
                                value={formValues.home_warranty_who_ordered}
                                onChange={handleChangeHomeWarranty}
                              />
                            </div>

                            <div className="col-md-4 mb-3">
                              <label className="col-form-label">
                                Paid in Advance/ Paid at Settlement
                              </label>
                              <select
                                className="form-select"
                                id="pr-city"
                                name="home_warranty_both_Pay"
                                value={formValues.home_warranty_both_Pay}
                                onChange={handleChangeHomeWarranty}
                              >
                                <option>Select</option>
                                <option value="paid_Advance">
                                  Paid in Advance
                                </option>
                                <option value="paid_Settlement">
                                  Paid at Settlement
                                </option>
                              </select>
                            </div>
                          </>
                        )}

                        <div>
                          {formValuesNew.filing === "filed_complted" ? (
                            <div className="pull-right w-100">
                              <button
                                className="btn btn-primary btn-sm pull-right"
                                type="button"
                                title="Your transaction was completed"
                              >
                                Save
                              </button>
                            </div>
                          ) : (
                            <>
                              {transactionFundingRequest?.invoice_url ? (
                                localStorage.getItem("auth") &&
                                jwt(localStorage.getItem("auth")).contactType ==
                                  "admin" ? (
                                  <>
                                    <div className="pull-right w-100">
                                      <button
                                        className="btn btn-primary btn-sm pull-right"
                                        type="button"
                                        onClick={handleSubmitHomeWarranty}
                                      >
                                        Save
                                      </button>
                                    </div>
                                  </>
                                ) : (
                                  <></>
                                )
                              ) : (
                                <div className="pull-right w-100">
                                  <button
                                    className="btn btn-primary btn-sm pull-right"
                                    type="button"
                                    onClick={handleSubmitHomeWarranty}
                                  >
                                    Save
                                  </button>
                                </div>
                              )}
                            </>
                          )}
                        </div>
                      </div>
                    </div>

                    <div className="bg-light border rounded-3 p-3 mt-5 float-start w-100">
                      <h3 className="mb-3">Home Inspection</h3>
                      <hr className="my-3" />
                      <div className="row mt-3">
                        <div className="col-md-4 mb-3">
                          <label className="col-form-label">
                            Will a home inspection be done on this property?
                          </label>
                          <div className="d-flex">
                            <div className="form-check ps-3">
                              <input
                                className="form-check-input"
                                id="form-radio-4"
                                type="radio"
                                name="home_Inspection_Property"
                                value="yes"
                                onChange={(e) => CheckHomeInspectionProperty(e)}
                                checked={
                                  formValues?.home_Inspection_Property ===
                                    "yes" ||
                                  !formValues.home_Inspection_Property
                                }
                              />
                              <label
                                className="form-check-label ms-2"
                                id="radio-level1"
                              >
                                Yes
                              </label>
                            </div>

                            <div className="form-check">
                              <input
                                className="form-check-input "
                                id="form-radio-4"
                                type="radio"
                                name="home_Inspection_Property"
                                value="no"
                                onChange={(e) => CheckHomeInspectionProperty(e)}
                                checked={
                                  formValues?.home_Inspection_Property === "no"
                                }
                              />
                              <label
                                className="form-check-label ms-2"
                                id="radio-level1"
                              >
                                No
                              </label>
                            </div>
                          </div>
                        </div>

                        {formValues?.home_Inspection_Property === "no" ? (
                          <></>
                        ) : (
                          <>
                            <div className="col-md-4 mb-3">
                              <label className="col-form-label">
                                Home Inspection Company
                              </label>
                              <input
                                className="form-control"
                                id="inline-form-input"
                                type="text"
                                name="home_Inspection_Company"
                                value={formValues.home_Inspection_Company}
                                onChange={handleChangeHomeInspection}
                              />
                            </div>

                            <div className="col-md-4 mb-3">
                              <label className="col-form-label">
                                Home Inspector Name
                              </label>
                              <input
                                className="form-control"
                                id="inline-form-input"
                                type="text"
                                name="home_Inspector_Name"
                                value={formValues.home_Inspector_Name}
                                onChange={handleChangeHomeInspection}
                              />

                              <>
                                {getContactData.length > 0 ? (
                                  getContactData.map((contact, index) => (
                                    <div
                                      className="member_Associate mb-1 mt-1"
                                      key={index}
                                      onClick={() =>
                                        handleContactAssociate(contact)
                                      }
                                    >
                                      <div
                                        className="float-left w-100 d-flex align-items-center justify-content-start"
                                        key={contact._id}
                                      >
                                        <img
                                          className="rounded-circlee profile_picture"
                                          height="30"
                                          width="30"
                                          src={
                                            contact.image &&
                                            contact.image !== "image"
                                              ? contact.image
                                              : Pic
                                          }
                                          alt="Profile"
                                        />

                                        {/* <div className="ms-3" onClick={()=>handleSubmit(post._id)}> */}
                                        <div className="ms-3">
                                          <strong>
                                            {contact.firstName}{" "}
                                            {contact.lastName}
                                          </strong>
                                          <br />
                                          <strong>
                                            {contact.active_office}
                                          </strong>
                                        </div>
                                      </div>
                                    </div>
                                  ))
                                ) : (
                                  <p></p>
                                )}
                              </>
                            </div>

                            <div className="col-md-4 mb-3">
                              <label className="col-form-label">
                                Home Inspector Phone
                              </label>
                              <input
                                className="form-control"
                                id="inline-form-input"
                                type="number"
                                name="home_Inspector_Phone"
                                value={formValues.home_Inspector_Phone}
                                onChange={handleChangeHomeInspection}
                              />
                            </div>

                            <div className="col-md-4 mb-3">
                              <label className="col-form-label">
                                Companies Website/URL
                              </label>
                              <input
                                className="form-control"
                                id="inline-form-input"
                                type="text"
                                name="home_Companies_Website"
                                value={formValues.home_Companies_Website}
                                onChange={handleChangeHomeInspection}
                              />
                            </div>

                            <div className="col-md-4 mb-3">
                              <label className="col-form-label">
                                Home Inspection Price
                              </label>
                              <input
                                className="form-control"
                                id="inline-form-input"
                                type="number"
                                name="home_Inspector_Price"
                                value={formValues.home_Inspector_Price}
                                onChange={handleChangeHomeInspection}
                              />
                            </div>

                            <div className="col-md-4 mb-3">
                              <label className="col-form-label">
                                Has the Home Inspection been ordered?
                              </label>
                              <div className="d-flex">
                                <div className="form-check ps-3">
                                  <input
                                    className="form-check-input"
                                    id="form-radio-4"
                                    type="radio"
                                    name="home_Inspection_Ordered"
                                    value="yes"
                                    onChange={(e) =>
                                      CheckHomeInspectionOrdered(e)
                                    }
                                    checked={
                                      formValues?.home_Inspection_Ordered ===
                                      "yes"
                                    }
                                  />
                                  <label
                                    className="form-check-label ms-2"
                                    id="radio-level1"
                                  >
                                    Yes
                                  </label>
                                </div>

                                <div className="form-check">
                                  <input
                                    className="form-check-input "
                                    id="form-radio-4"
                                    type="radio"
                                    name="home_Inspection_Ordered"
                                    value="no"
                                    onChange={(e) =>
                                      CheckHomeInspectionOrdered(e)
                                    }
                                    checked={
                                      formValues?.home_Inspection_Ordered ===
                                      "no"
                                    }
                                  />
                                  <label
                                    className="form-check-label ms-2"
                                    id="radio-level1"
                                  >
                                    No
                                  </label>
                                </div>
                              </div>
                            </div>

                            <div className="col-md-4 mb-3">
                              <label className="col-form-label">
                                Date Ordered
                              </label>
                              <input
                                className="form-control"
                                id="inline-form-input"
                                type="date"
                                name="home_Date_Ordered"
                                value={formValues.home_Date_Ordered}
                                onChange={handleChangeHomeInspection}
                              />
                            </div>

                            <div className="col-md-4 mb-3">
                              <label className="col-form-label">
                                Who Ordered
                              </label>
                              <input
                                className="form-control"
                                id="inline-form-input"
                                type="text"
                                name="home_Who_Ordered"
                                value={formValues.home_Who_Ordered}
                                onChange={handleChangeHomeInspection}
                              />
                            </div>

                            <div className="col-md-4 mb-3">
                              <label className="col-form-label">
                                Home Inspection Date-Time
                              </label>
                              <DateTimePicker
                                onChange={handleDateChange}
                                value={formValues.home_Inspection_Datetime}
                                className="form-control"
                                id="inline-form-input"
                              />
                            </div>
                          </>
                        )}

                        <div>
                          {formValuesNew.filing === "filed_complted" ? (
                            <div className="pull-right w-100">
                              <button
                                className="btn btn-primary btn-sm pull-right"
                                type="button"
                                title="Your transaction was completed"
                              >
                                Save
                              </button>
                            </div>
                          ) : (
                            <>
                              {transactionFundingRequest?.invoice_url ? (
                                localStorage.getItem("auth") &&
                                jwt(localStorage.getItem("auth")).contactType ==
                                  "admin" ? (
                                  <>
                                    <div className="pull-right w-100">
                                      <button
                                        className="btn btn-primary btn-sm pull-right"
                                        type="button"
                                        onClick={handleSubmitHomeInspection}
                                      >
                                        Save
                                      </button>
                                    </div>
                                  </>
                                ) : (
                                  <></>
                                )
                              ) : (
                                <div className="pull-right w-100">
                                  <button
                                    className="btn btn-primary btn-sm pull-right"
                                    type="button"
                                    onClick={handleSubmitHomeInspection}
                                  >
                                    Save
                                  </button>
                                </div>
                              )}
                            </>
                          )}
                        </div>
                      </div>
                    </div>

                    <div className="bg-light border rounded-3 p-3 mt-5 float-start w-100">
                      <h3 className="mb-3">Utilities</h3>
                      <hr className="my-3" />
                      <div className="row mt-3">
                        <div className="col-md-3 mb-3">
                          <label className="col-form-label">
                            Electric Company
                          </label>
                          <input
                            className="form-control"
                            id="inline-form-input"
                            type="text"
                            name="electric_company"
                            value={formValues.electric_company}
                            onChange={handleChangeUtilities}
                          />
                        </div>

                        <div className="col-md-3 mb-3">
                          <label className="col-form-label">
                            Garbage Company
                          </label>
                          <input
                            className="form-control"
                            id="inline-form-input"
                            type="text"
                            name="garbage_company"
                            value={formValues.garbage_company}
                            onChange={handleChangeUtilities}
                          />
                        </div>

                        <div className="col-md-3 mb-3">
                          <label className="col-form-label">Gas Company</label>
                          <input
                            className="form-control"
                            id="inline-form-input"
                            type="text"
                            name="gas_company"
                            value={formValues.gas_company}
                            onChange={handleChangeUtilities}
                          />
                        </div>

                        <div className="col-md-3 mb-3">
                          <label className="col-form-label">
                            Sewer Company
                          </label>
                          <input
                            className="form-control"
                            id="inline-form-input"
                            type="text"
                            name="sewer_Company"
                            value={formValues.sewer_Company}
                            onChange={handleChangeUtilities}
                          />
                        </div>

                        <div className="col-md-3 mb-3">
                          <label className="col-form-label">
                            Sewer or Septic
                          </label>
                          <select
                            className="form-select"
                            id="pr-city"
                            name="sewer_septic"
                            onChange={handleChangeUtilities}
                            value={formValues.sewer_septic}
                          >
                            <option>Select</option>
                            <option value="Sewer">Sewer</option>
                            <option value="Septic">Septic</option>
                          </select>
                        </div>

                        <div className="col-md-3 mb-3">
                          <label className="col-form-label">
                            Water Company
                          </label>
                          <input
                            className="form-control"
                            id="inline-form-input"
                            type="text"
                            name="water_Company"
                            value={formValues.water_Company}
                            onChange={handleChangeUtilities}
                          />
                        </div>

                        <div>
                          {formValuesNew.filing === "filed_complted" ? (
                            <div className="pull-right w-100">
                              <button
                                className="btn btn-primary btn-sm pull-right"
                                type="button"
                                title="Your transaction was completed"
                              >
                                Save
                              </button>
                            </div>
                          ) : (
                            <>
                              {transactionFundingRequest?.invoice_url ? (
                                localStorage.getItem("auth") &&
                                jwt(localStorage.getItem("auth")).contactType ==
                                  "admin" ? (
                                  <>
                                    <div className="pull-right w-100">
                                      <button
                                        className="btn btn-primary btn-sm pull-right"
                                        type="button"
                                        onClick={handleSubmitUlities}
                                      >
                                        Save
                                      </button>
                                    </div>
                                  </>
                                ) : (
                                  <></>
                                )
                              ) : (
                                <div className="pull-right w-100">
                                  <button
                                    className="btn btn-primary btn-sm pull-right"
                                    type="button"
                                    onClick={handleSubmitUlities}
                                  >
                                    Save
                                  </button>
                                </div>
                              )}
                            </>
                          )}
                        </div>
                      </div>
                    </div>

                    <div className="bg-light border rounded-3 p-3 mt-5 float-start w-100">
                      <h3 className="mb-3">
                        Home Owner Association Information
                      </h3>
                      <hr className="my-3" />
                      <div className="row mt-3">
                        <div className="col-md-3 mb-3">
                          <label className="col-form-label">
                            Does the property have an HOA? *
                          </label>
                          <div className="d-flex">
                            <div className="form-check ms-3">
                              <input
                                className="form-check-input"
                                id=""
                                type="radio"
                                name="property_hoa"
                                value="yes"
                                onChange={CheckHoa}
                                checked={
                                  formValues?.hoa === "yes" || !formValues.hoa
                                }
                              />
                              <label className="form-label" id="">
                                Yes
                              </label>
                            </div>
                            <div className="form-check ms-3">
                              <input
                                className="form-check-input"
                                id=""
                                type="radio"
                                name="property_hoa"
                                value="no"
                                onChange={CheckHoa}
                                checked={formValues?.hoa === "no"}
                              />
                              <label className="form-label" id="">
                                No
                              </label>
                            </div>
                          </div>
                        </div>
                        {formValues?.hoa === "no" ? (
                          <></>
                        ) : (
                          <>
                            <div className="col-md-3 mb-3">
                              <label className="col-form-label d-flex">
                                HOA Due *
                              </label>
                              <input
                                className="form-control"
                                id="inline-form-input"
                                type="text"
                                name="hoa_due"
                                placeholder="Enter HOA Due"
                                value={formValues.hoa_due}
                                onChange={handleChangeHoa}
                                style={{
                                  border:
                                    formValuesNew.type === "buy_referral" ||
                                    formValuesNew.type === "sale_referral" ||
                                    formValuesNew.type === "both_referral"
                                      ? "1px solid #00000026"
                                      : formValues.hoa_due
                                      ? "1px solid #00000026"
                                      : formErrorsNew.hoa_due
                                      ? "1px solid red"
                                      : "1px solid #00000026",
                                }}
                              />
                              {formValuesNew.type === "buy_referral" ||
                              formValuesNew.type === "sale_referral" ||
                              formValuesNew.type === "both_referral" ? (
                                <></>
                              ) : (
                                <>
                                  {!formValues.hoa_due && (
                                    <div className="invalid-tooltip">
                                      {formErrorsNew.hoa_due}
                                    </div>
                                  )}
                                </>
                              )}
                            </div>

                            <div className="col-md-6 mb-3">
                              <label className="col-form-label d-flex">
                                HOA Dues Frequency? *
                              </label>
                              <div className="d-lg-flex d-md-flex">
                                <div className="form-check ms-3">
                                  <input
                                    className="form-check-input"
                                    id=""
                                    type="radio"
                                    name="frequency"
                                    value="monthly"
                                    onChange={CheckHoaFrequency}
                                    checked={
                                      formValues?.hoa_duesfrequency ===
                                      "monthly"
                                    }
                                  />
                                  <label className="form-label" id="">
                                    Monthly
                                  </label>
                                </div>
                                <div className="form-check ms-3">
                                  <input
                                    className="form-check-input"
                                    id=""
                                    type="radio"
                                    name="frequency"
                                    value="quarterly"
                                    onChange={CheckHoaFrequency}
                                    checked={
                                      formValues?.hoa_duesfrequency ===
                                      "quarterly"
                                    }
                                  />
                                  <label className="form-label" id="">
                                    Quarterly
                                  </label>
                                </div>
                                <div className="form-check ms-3">
                                  <input
                                    className="form-check-input"
                                    id=""
                                    type="radio"
                                    name="frequency"
                                    value="semi-annually"
                                    onChange={CheckHoaFrequency}
                                    checked={
                                      formValues?.hoa_duesfrequency ===
                                      "semi-annually"
                                    }
                                  />
                                  <label className="form-label" id="">
                                    Semi-Annually
                                  </label>
                                </div>
                                <div className="form-check ms-3">
                                  <input
                                    className="form-check-input"
                                    id=""
                                    type="radio"
                                    name="frequency"
                                    value="annually"
                                    onChange={CheckHoaFrequency}
                                    checked={
                                      formValues?.hoa_duesfrequency ===
                                      "annually"
                                    }
                                  />
                                  <label className="form-label" id="">
                                    Annually
                                  </label>
                                </div>
                              </div>

                              {formValuesNew.referralSend === "no" ? (
                                <></>
                              ) : (
                                <>
                                  {formValuesNew.type === "buy_referral" ||
                                  formValuesNew.type === "sale_referral" ||
                                  formValuesNew.type === "both_referral" ? (
                                    <></>
                                  ) : (
                                    <>
                                      {!formValues.hoa_duesfrequency && (
                                        <div className="invalid-tooltip">
                                          {formErrors.hoa_duesfrequency}
                                        </div>
                                      )}
                                    </>
                                  )}
                                </>
                              )}
                            </div>
                          </>
                        )}
                      </div>

                      {formValues?.hoa === "no" ? (
                        <></>
                      ) : (
                        <>
                          <div className="row">
                            <div className="col-md-3 mb-3">
                              <label className="col-form-label">
                                HOA First Name *
                              </label>
                              <input
                                className="form-control"
                                id="inline-form-input"
                                type="text"
                                name="hoa_firstname"
                                placeholder="Enter HOA First Name"
                                value={formValues.hoa_firstname}
                                onChange={handleChangeHoa}
                                style={{
                                  border:
                                    formValuesNew.referralSend === "no"
                                      ? ""
                                      : formValuesNew.type === "buy_referral" ||
                                        formValuesNew.type ===
                                          "sale_referral" ||
                                        formValuesNew.type === "both_referral"
                                      ? "1px solid #00000026"
                                      : formValues.hoa_firstname
                                      ? "1px solid #00000026"
                                      : formErrors.hoa_firstname
                                      ? "1px solid red"
                                      : "1px solid #00000026",
                                }}
                              />

                              {formValuesNew.referralSend === "no" ? (
                                <></>
                              ) : (
                                <>
                                  {formValuesNew.type === "buy_referral" ||
                                  formValuesNew.type === "sale_referral" ||
                                  formValuesNew.type === "both_referral" ? (
                                    <></>
                                  ) : (
                                    <>
                                      {!formValues.hoa_firstname && (
                                        <div className="invalid-tooltip">
                                          {formErrors.hoa_firstname}
                                        </div>
                                      )}
                                    </>
                                  )}
                                </>
                              )}

                              <>
                                {getContactHoa.length > 0 ? (
                                  getContactHoa.map((contact, index) => (
                                    <div
                                      className="member_Associate mb-1 mt-1"
                                      key={index}
                                      onClick={() =>
                                        handleContactAssociateHoa(contact)
                                      }
                                    >
                                      <div
                                        className="float-left w-100 d-flex align-items-center justify-content-start"
                                        key={contact._id}
                                      >
                                        <img
                                          className="rounded-circlee profile_picture"
                                          height="30"
                                          width="30"
                                          src={
                                            contact.image &&
                                            contact.image !== "image"
                                              ? contact.image
                                              : Pic
                                          }
                                          alt="Profile"
                                        />

                                        {/* <div className="ms-3" onClick={()=>handleSubmit(post._id)}> */}
                                        <div className="ms-3">
                                          <strong>
                                            {contact.firstName}{" "}
                                            {contact.lastName}
                                          </strong>
                                          <br />
                                          <strong>
                                            {contact.active_office}
                                          </strong>
                                        </div>
                                      </div>
                                    </div>
                                  ))
                                ) : (
                                  <p></p>
                                )}
                              </>
                            </div>
                            <div className="col-md-3 mb-3">
                              <label className="col-form-label d-flex">
                                HOA Last Name *
                              </label>
                              <input
                                className="form-control"
                                id="inline-form-input"
                                type="text"
                                name="hoa_lastname"
                                placeholder="Enter HOA Last Name"
                                value={formValues.hoa_lastname}
                                onChange={handleChangeHoa}
                                style={{
                                  border:
                                    formValuesNew.referralSend === "no"
                                      ? ""
                                      : formValuesNew.type === "buy_referral" ||
                                        formValuesNew.type ===
                                          "sale_referral" ||
                                        formValuesNew.type === "both_referral"
                                      ? "1px solid #00000026"
                                      : formValues.hoa_lastname
                                      ? "1px solid #00000026"
                                      : formErrors.hoa_lastname
                                      ? "1px solid red"
                                      : "1px solid #00000026",
                                }}
                              />

                              {formValuesNew.referralSend === "no" ? (
                                <></>
                              ) : (
                                <>
                                  {formValuesNew.type === "buy_referral" ||
                                  formValuesNew.type === "sale_referral" ||
                                  formValuesNew.type === "both_referral" ? (
                                    <></>
                                  ) : (
                                    <>
                                      {!formValues.hoa_lastname && (
                                        <div className="invalid-tooltip">
                                          {formErrors.hoa_lastname}
                                        </div>
                                      )}
                                    </>
                                  )}
                                </>
                              )}
                            </div>
                            <div className="col-md-3 mb-3">
                              <label className="col-form-label d-flex">
                                HOA Contact *
                              </label>
                              <input
                                className="form-control"
                                id="inline-form-input"
                                type="number"
                                name="hoa_contact"
                                placeholder="Enter HOA Contact"
                                value={formValues.hoa_contact}
                                onChange={handleChangeHoa}
                                style={{
                                  border:
                                    formValuesNew.referralSend === "no"
                                      ? ""
                                      : formValuesNew.type === "buy_referral" ||
                                        formValuesNew.type ===
                                          "sale_referral" ||
                                        formValuesNew.type === "both_referral"
                                      ? "1px solid #00000026"
                                      : formValues.hoa_contact
                                      ? "1px solid #00000026"
                                      : formErrors.hoa_contact
                                      ? "1px solid red"
                                      : "1px solid #00000026",
                                }}
                              />
                              {formValuesNew.referralSend === "no" ? (
                                <></>
                              ) : (
                                <>
                                  {formValuesNew.type === "buy_referral" ||
                                  formValuesNew.type === "sale_referral" ||
                                  formValuesNew.type === "both_referral" ? (
                                    <></>
                                  ) : (
                                    <>
                                      {!formValues.hoa_contact && (
                                        <div className="invalid-tooltip">
                                          {formErrors.hoa_contact}
                                        </div>
                                      )}
                                    </>
                                  )}
                                </>
                              )}
                            </div>
                            <div className="col-md-3 mb-3">
                              <label className="col-form-label d-flex">
                                HOA Company *
                              </label>
                              <input
                                className="form-control"
                                id="inline-form-input"
                                type="text"
                                name="hoa_company"
                                placeholder="Enter  HOA Company"
                                value={formValues.hoa_company}
                                onChange={handleChangeHoa}
                                style={{
                                  border:
                                    formValuesNew.referralSend === "no"
                                      ? ""
                                      : formValuesNew.type === "buy_referral" ||
                                        formValuesNew.type ===
                                          "sale_referral" ||
                                        formValuesNew.type === "both_referral"
                                      ? "1px solid #00000026"
                                      : formValues.hoa_company
                                      ? "1px solid #00000026"
                                      : formErrors.hoa_company
                                      ? "1px solid red"
                                      : "1px solid #00000026",
                                }}
                              />
                              {formValuesNew.referralSend === "no" ? (
                                <></>
                              ) : (
                                <>
                                  {formValuesNew.type === "buy_referral" ||
                                  formValuesNew.type === "sale_referral" ||
                                  formValuesNew.type === "both_referral" ? (
                                    <></>
                                  ) : (
                                    <>
                                      {!formValues.hoa_company && (
                                        <div className="invalid-tooltip">
                                          {formErrors.hoa_company}
                                        </div>
                                      )}
                                    </>
                                  )}
                                </>
                              )}
                            </div>
                          </div>
                          <div className="row">
                            <div className="col-md-8 mb-3">
                              <label className="col-form-label">
                                HOA Address
                              </label>
                              <input
                                className="form-control"
                                id="inline-form-input"
                                type="text"
                                name="hoa_address"
                                placeholder="Enter HOA Address"
                                value={formValues.hoa_address}
                                onChange={handleChangeHoa}
                              />
                            </div>
                            <div className="col-md-4 mb-3">
                              <label className="col-form-label d-flex">
                                HOA Website
                              </label>
                              <input
                                className="form-control"
                                id="inline-form-input"
                                type="text"
                                name="hoa_website"
                                placeholder="Enter HOA Website"
                                value={formValues.hoa_website}
                                onChange={handleChangeHoa}
                              />
                            </div>
                          </div>

                          <div className="row">
                            <div className="col-md-4">
                              <label className="col-form-label">
                                Does the HOA have a Reinvestment / Transfer fee
                                ?
                              </label>
                              <div className="d-flex">
                                <div className="form-check">
                                  <input
                                    className="form-check-input"
                                    id=""
                                    type="radio"
                                    name="reinvestment"
                                    value="yes"
                                    onChange={CheckHoaReinvestment}
                                    checked={formValues?.transfer_fee === "yes"}
                                  />
                                  <label className="form-label" id="">
                                    Yes
                                  </label>
                                </div>
                                <div className="form-check ms-3">
                                  <input
                                    className="form-check-input"
                                    id=""
                                    type="radio"
                                    name="reinvestment"
                                    value="no"
                                    onChange={CheckHoaReinvestment}
                                    checked={formValues?.transfer_fee === "no"}
                                  />
                                  <label className="form-label" id="">
                                    No
                                  </label>
                                </div>
                              </div>
                            </div>

                            <div className="col-md-4">
                              <label className="col-form-label d-flex">
                                Reinvestment fee / Transfer fee amount
                                <div className="form-check mb-0 ms-2">
                                  <input
                                    className="form-check-input"
                                    id="form-check-1"
                                    type="radio"
                                    name="transferfeeAmount"
                                    value="percentage"
                                    onChange={CheckTransferfeeAmount}
                                    checked={
                                      formValues?.transfer_feeamount ===
                                      "percentage"
                                    }
                                  />
                                  <label
                                    className="form-check-label"
                                    id="radio-level1"
                                  >
                                    %
                                  </label>
                                </div>
                                <div className="form-check mb-0 ms-2">
                                  <input
                                    className="form-check-input"
                                    id="form-check-1"
                                    type="radio"
                                    name="transferfeeAmount"
                                    value="doller"
                                    onChange={CheckTransferfeeAmount}
                                    checked={
                                      formValues?.transfer_feeamount ===
                                        "doller" ||
                                      !formValues?.transfer_feeamount
                                    }
                                  />
                                  <label
                                    className="form-check-label"
                                    id="radio-level1"
                                  >
                                    $
                                  </label>
                                </div>
                              </label>

                              <div className="">
                                <input
                                  className="form-control"
                                  id="inline-form-input"
                                  type="text"
                                  name="transfer_amount"
                                  placeholder="00"
                                  value={formValues.transfer_amount}
                                  onChange={handleChangeFeeAmount}
                                />
                              </div>
                            </div>
                            <div className="col-md-12 mb-3">
                              <label className="col-form-label">Comments</label>
                              <textarea
                                className="form-control mt-0"
                                id="textarea-input"
                                rows="5"
                                name="comment"
                                placeholder="00"
                                value={formValues.comment}
                                onChange={handleChangeHoa}
                                autocomplete="on"
                              ></textarea>
                            </div>
                          </div>
                        </>
                      )}
                      <div className="row">
                        {formValues?.hoa === "no" ? (
                          <></>
                        ) : (
                          <div className="col-md-3">
                            <label className="col-form-label">
                              HOA Document
                            </label>
                            <div className="d-flex align-items-center justify-content-start mb-4">
                              <input
                                className="file-uploader file-uploader-grid me-2"
                                type="file"
                                multiple
                                name="hoa_document"
                                onChange={handleFileUploadDocument}
                                data-max-files="4"
                                data-max-file-size="2MB"
                                accept="image/png, image/jpeg, video/mp4, video/mov"
                                data-label-idle='<div className="btn btn-primary mb-3"><i className="fi-cloud-upload me-1"></i>Upload photos / video</div><div>or drag them in</div>'
                              />
                              {formValues.hoa_document ? (
                                <>
                                  <img src={formValues.hoa_document} />
                                </>
                              ) : (
                                "Uploaded"
                              )}
                              <i
                                className="fi-trash opacity-80 pull-left ms-2"
                                onClick={handleDelete}
                              ></i>
                            </div>
                            {/* <a href="#">
                      <i className="fi-plus me-2"></i>Add More
                    </a> */}
                          </div>
                        )}

                        {formValuesNew.filing === "filed_complted" ? (
                          <div className="pull-right w-100">
                            <button
                              className="btn btn-primary btn-sm pull-right"
                              type="button"
                              title="Your transaction was completed"
                            >
                              Save
                            </button>
                          </div>
                        ) : (
                          <>
                            {transactionFundingRequest?.invoice_url ? (
                              localStorage.getItem("auth") &&
                              jwt(localStorage.getItem("auth")).contactType ==
                                "admin" ? (
                                <>
                                  <div className="pull-right w-100">
                                    <button
                                      className="btn btn-primary btn-sm pull-right"
                                      type="button"
                                      onClick={handleSubmitHoa}
                                    >
                                      Save
                                    </button>
                                  </div>
                                </>
                              ) : (
                                <></>
                              )
                            ) : (
                              <div className="pull-right w-100">
                                <button
                                  className="btn btn-primary btn-sm pull-right"
                                  type="button"
                                  onClick={handleSubmitHoa}
                                >
                                  Save
                                </button>
                              </div>
                            )}
                          </>
                        )}
                      </div>
                    </div>
                  </>
                )}
              </div>
            </div>
          </div>
        </div>
      </main>
      {isVisible ? (
        <div className="modal" role="dialog" id="addProperty">
          <div
            className="modal-dialog modal-lg modal-dialog-scrollable"
            role="document"
          >
            <div className="modal-content">
              <div className="modal-header">
                <h4 className="modal-title">Add Property</h4>
              </div>
              <div className="modal-body fs-sm">
                <div className="">
                  <div className="mb-0">
                    <div className="row">
                      <div className="col-sm-6 col-6 mt-3">
                        <label
                          className="col-form-label pt-0 pb-1"
                          id="col-form-label-one"
                        >
                          {/* Street Name: */}
                          Property Address:
                        </label>
                        <input
                          className="form-control border-0 rounded-0 border-bottom"
                          id="text-input-one"
                          type="text"
                          name="streetAddress"
                          placeholder="Enter your Property Address"
                          onChange={handleChangeproperty}
                          value={formValuesproperty.streetAddress}
                          style={{
                            border: formErrorsproperty?.streetAddress
                              ? "1px solid red"
                              : "",
                          }}
                        />

                        {formErrorsproperty.streetAddress && (
                          <div className="invalid-tooltip">
                            {formErrorsproperty.streetAddress}
                          </div>
                        )}
                      </div>

                      <div className="col-sm-6 col-6 mt-3">
                        <label
                          className="col-form-label pt-0 pb-1"
                          id="col-form-label-one"
                        >
                          Property Type
                          <span className="text-danger">*</span>
                        </label>
                        <select
                          className="form-select"
                          id="pr-city"
                          name="propertyType"
                          onChange={handleChangeproperty}
                          value={formValuesproperty.propertyType}
                          style={{
                            border: formErrorsproperty?.propertyType
                              ? "1px solid red"
                              : "",
                          }}
                        >
                          <option value=""></option>
                          <option value="land">Land</option>
                          <option value="residential">Residential</option>
                          <option value="commercial_lease">
                            Commercial Lease
                          </option>
                          <option value="commercial_sale">
                            Commercial Sale
                          </option>
                          <option value="commercial_industrial">
                            Commercial/Industrial
                          </option>
                          <option value="residential_lease">
                            Residential/Lease
                          </option>
                          <option value="farm">Farm</option>
                        </select>
                        {formErrorsproperty.propertyType && (
                          <div className="invalid-tooltip">
                            {formErrorsproperty.propertyType}
                          </div>
                        )}
                      </div>

                      <div className="col-sm-6 col-6 mt-3">
                        <label
                          className="col-form-label pt-0 pb-1"
                          id="col-form-label-one"
                        >
                          City:
                        </label>
                        <input
                          className="form-control border-0 rounded-0 border-bottom"
                          id="text-input-one"
                          name="city"
                          type="text"
                          placeholder="Enter your City"
                          onChange={handleChangeproperty}
                          autoComplete="on"
                          value={formValuesproperty.city}
                          style={{
                            border: formErrorsproperty?.city
                              ? "1px solid red"
                              : "",
                          }}
                        />

                        {formErrorsproperty.city && (
                          <div className="invalid-tooltip">
                            {formErrorsproperty.city}
                          </div>
                        )}
                      </div>

                      <div className="col-sm-6 col-6 mt-3">
                        <label
                          className="col-form-label pt-0 pb-1"
                          id="col-form-label-one"
                        >
                          State/Province:
                        </label>
                        <select
                          id="text-input-one"
                          className="form-select border-0 rounded-0 border-bottom"
                          name="state"
                          onChange={handleChangeproperty}
                          value={formValuesproperty.state}
                        >
                          <option value="0">--Select--</option>
                          {options
                            .find((option) => option.code === "UT")
                            .districts.map((district, key) => (
                              <option value={district} key={key}>
                                {district}
                              </option>
                            ))}
                        </select>
                      </div>

                      <div className="col-sm-6 col-6 mt-3">
                        <label
                          className="col-form-label pt-0 pb-1"
                          id="col-form-label-one"
                        >
                          Postal Code:
                        </label>
                        <input
                          className="form-control border-0 rounded-0 border-bottom"
                          id="text-input-one"
                          name="zipCode"
                          type="text"
                          placeholder="Enter your postal code"
                          onChange={handleChangeproperty}
                          value={formValuesproperty.zipCode}
                          style={{
                            border: formErrorsproperty?.zipCode
                              ? "1px solid red"
                              : "",
                          }}
                        />

                        {formErrorsproperty.zipCode && (
                          <div className="invalid-tooltip">
                            {formErrorsproperty.zipCode}
                          </div>
                        )}
                      </div>

                      <div className="col-sm-6 col-6 mt-3">
                        <label
                          className="col-form-label pt-0 pb-1"
                          id="col-form-label-one"
                        >
                          List Price (Sale):
                        </label>
                        <input
                          className="form-control border-0 rounded-0 border-bottom"
                          id="text-input-one"
                          name="price"
                          type="number"
                          placeholder="Enter your Price"
                          onChange={handleChangeproperty}
                          value={formValuesproperty.price}
                        />
                      </div>

                      <div className="col-sm-6 col-6 mt-3">
                        <label
                          className="col-form-label pt-0 pb-1"
                          id="col-form-label-one"
                        >
                          List Price (Lease) /mo:
                        </label>
                        <input
                          className="form-control border-0 rounded-0 border-bottom"
                          id="text-input-one"
                          name="priceLease"
                          type="number"
                          placeholder="Enter your list price lease"
                          onChange={handleChangeproperty}
                          value={formValuesproperty.priceLease}
                        />
                      </div>
                      {data ? (
                        <div className="col-md-6 mt-3">
                          <img src={data} />
                        </div>
                      ) : (
                        <div className="col-md-6 mt-3">
                          <label className="col-form-label" id="form-label-one">
                            Upload Property Image
                          </label>
                          <input
                            className="file-uploader file-uploader-grid"
                            id="inline-form-input"
                            type="file"
                            name="image"
                            value={formValuesproperty.image}
                            accept="image/*"
                            onChange={handleFileUpload}
                          />

                          {/* <div className="invalid-tooltip">
                                              {formErrorsproperty.image}
                                            </div> */}
                        </div>
                      )}
                    </div>
                  </div>
                  <div className="text-center mt-4">
                    <a
                      className="btn btn-info btn-sm"
                      href="#"
                      onClick={handleSubmitProperty}
                    >
                      Add Property Now{" "}
                    </a>

                    <button
                      type="button"
                      className="btn btn-secondary btn-sm ms-3"
                      onClick={handleBackProperty}
                    >
                      Cancel & Close
                    </button>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      ) : (
        ""
      )}
    </div>
  );
};

export default NewProperty;

import React, { useState, useMemo, useEffect } from "react";
import _ from "lodash";
import user_service from "../service/user_service";
import jwt from "jwt-decode";
import Loader from "../../Pages/Loader/Loader.js";
import Toaster from "../../Pages/Toaster/Toaster.js";
import { useNavigate, NavLink, useParams } from "react-router-dom";
import avtar from "../img/avtar.jpg";
import ReactPaginate from "react-paginate";
import countryList from "country-list";
import defaultpropertyimage from "../img/defaultpropertyimage.jpeg";
import styled, { keyframes } from "styled-components";
import axios from "axios";

const fadeInRightAnimation = keyframes`
  from {
    opacity: 0;
    transform: translateX(20px);
  }
  to {
    opacity: 1;
    transform: translateX(0);
  }
`;
const FadeInRightDiv = styled.div`
  animation: ${fadeInRightAnimation} 0.5s;
`;
function NewTransaction() {
  const initialValues = {
    firstName: "",
    lastName: "",
    company: "",
    email: "",
    phone: "",
    contactType: "private",
    agentId: "",
    contactEntityType: "",
    corporation_name: "",
    trust_name: "",
  };
  const [formValues, setFormValues] = useState(initialValues);

  // const initialValuesNew = {
  //   represent: "",
  //   type: "",
  //   ownership: "",
  //   phase: "",
  //   contact: "",
  //   agentId: "",
  // };

  const [formValuesNew, setFormValuesNew] = useState({
    filename: "",
  });
  const [isSubmitClickNew, setISSubmitClickNew] = useState(false);
  const [isSubmit, setISSubmit] = useState(false);

  const [formErrors, setFormErrors] = useState({});
  const [isSubmitClick, setISSubmitClick] = useState(false);

  const [summary, setSummary] = useState("");
  const [arrayData, setArrayData] = useState("");
  const [arrayDataAssociate, setArrayDataAssociate] = useState("");

  const [select, setSelect] = useState("");
  const [selectAssociate, setSelectAssociate] = useState("");

  const [clientId, setClientId] = useState("");
  const [represent, setRepresent] = useState("");
  const [transactionOptions, setTransactionOptions] = useState([]);
  const [ownerships, setOwnerShips] = useState("ownership");
  const [contact, setContact] = useState("");
  const [contactAdd, setContactAdd] = useState(false);

  const [propertyId, setPropertyId] = useState("");

  const [changeId, setChangeId] = useState(false);
  const [contactTypeAssociate, setContactTypeAssociate] = useState("associate");
  const [query, setQuery] = useState("");
  const [getAllAssociate, setGetAllAssociate] = useState("");

  const [checkBox, setSetCheckbox] = useState(false);
  const [results, setResults] = useState([]);
  const [pageCount, setPageCount] = useState(0);
  const [currentPage, setCurrentPage] = useState(1);
  const [isLoading, setIsLoading] = useState(false);
  const [showPopup, setShowPopup] = useState(false);
  const [clientdone, setClientdone] = useState(false);

  const [step, setStep] = useState(1);
  const [animating, setAnimating] = useState(false);
  const [selectedCategory, setSelectedCategory] = useState("");
  const [selectedType, setSelectedType] = useState("");
  const [selectedPhase, setSelectedPhase] = useState("");

  const [selectedOwner, setSelectedOwner] = useState("");
  const [file, setFile] = useState(null);
  const [data, setData] = useState("");
  const [loader, setLoader] = useState({ isActive: null });
  const { isActive } = loader;
  const [toaster, setToaster] = useState({
    types: null,
    isShow: null,
    toasterBody: "",
    message: "",
  });
  const { types, isShow, toasterBody, message } = toaster;

  const navigate = useNavigate();
  const params = useParams();

  const handleCheck = () => {
    setSetCheckbox(checkBox ? true : false);
  };

  useEffect(() => {
    window.scrollTo(0, 0);
    // TransactionGetById(params.id);
    ContactGetById(jwt(localStorage.getItem("auth")).id);
  }, []);

  // onChange Function start
  const handleOwner = (e) => {
    const { name, value } = e.target;
    setOwnerShips({ ...ownerships, [name]: value });
  };

  // onChange Function start
  const handleChange = (e) => {
    const { name, value } = e.target;
    setFormValues({ ...formValues, [name]: value });
  };

  // onChange Function end

  {
    /* <!-- Form Validation Start--> */
  }
  useEffect(() => {
    if (formValues && isSubmitClick) {
      validate();
    }
  }, [formValues]);

  const validate = () => {
    const values = formValues;
    const errors = {};
    if (!values.firstName) {
      errors.firstName = "name is required";
    }

    if (!values.lastName) {
      errors.lastName = "lastName is required";
    }

    if (!values.email) {
      errors.email = "Email is required";
    } else if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i.test(values.email)) {
      errors.email = "Invalid email address";
    }

    if (checkvalueContact === "entity") {
      if (!values.corporation_name) {
        errors.corporation_name = "Corporation name is required";
      }
    }

    if (checkvalueContact === "trust") {
      if (!values.trust_name) {
        errors.trust_name = "Trust name is required";
      }
    }

    setFormErrors(errors);
    return errors;
  };
  {
    /* <!-- Form Validation End--> */
  }

  {
    /* <!--Api call Form onSubmit Start--> */
  }
  const handleNewContact = async (e, id) => {
    e.preventDefault();
    setISSubmitClick(true);
    let checkValue = validate();
    if (_.isEmpty(checkValue)) {
      const userData = {
        firstName: formValues.firstName,
        lastName: formValues.lastName,
        nickname: formValues.lastName + "" + formValues.firstName,
        company: formValues.company,
        email: formValues.email,
        phone: formValues.phone,
        contactType: "private",
        agentId: jwt(localStorage.getItem("auth")).id,
        contactEntityType: checkvalueContact,
        corporation_name: formValues.corporation_name,
        trust_name: formValues.trust_name,
      };
      try {
        setLoader({ isActive: true });
        const response = await user_service.contact(
          userData,
          formValues.firstName,
          id
        );
        const contactData = response.data;
        setSummary(contactData);
        setArrayData(contactData._id);
        setClientId(id);
        setSelect("");
        // setContactAdd("")
        setLoader({ isActive: false });
        setToaster({
          type: "Contact Successfully",
          isShow: true,
          message: "Contact Successfully",
        });
        setTimeout(() => {
          setToaster((prevToaster) => ({ ...prevToaster, isShow: false }));
        }, 3000);
        // document.getElementById("closeModal").click();
      } catch (error) {
        setLoader({ isActive: false });
        setToaster({
          types: "error",
          isShow: true,
          toasterBody: error.response
            ? error.response.data.message
            : error.message,
          // message: "Error",
        });
        setTimeout(() => {
          setToaster((prevToaster) => ({ ...prevToaster, isShow: false }));
        }, 3000);
      }
    }
  };
  {
    /* <!-- Api call Form onSubmit End--> */
  }

  {
    /* <!-- paginate Function Api call Start--> */
  }
  useEffect(() => {
    if (formValues) {
      SearchGetAll();
    }
  }, [formValues]);

  const SearchGetAll = async () => {
    await user_service
      .SearchContactGet(1, 2, formValues.firstName)
      .then((response) => {
        if (response) {
          setResults(response.data.data);
          setPageCount(Math.ceil(response.data.totalRecords / 10));
        }
      });
  };

  const handlePageClick = async (data) => {
    let currentPage = data.selected + 1;
    let skip = currentPage - 1 + currentPage;
    setLoader({ isActive: true });
    await user_service
      .SearchContactGet(currentPage, 2, formValues.firstName)
      .then((response) => {
        setLoader({ isActive: false });
        if (response) {
          // setSummary(response.data);
          setResults(response.data.data);
          setPageCount(Math.ceil(response.data.totalRecords / 10));
        }
      });
  };

  const handleSearchTransaction = async (id) => {
    await user_service.contactGetById(id).then((response) => {
      setClientId(id);
      setSelect(response.data);
    });
  };

  const handleSearchTransactionAssociate = async (id) => {
    console.log(id);
    await user_service.contactGetById(id).then((response) => {
      const contactData = response.data;
      setSelectAssociate(response.data);
      if (contactData._id) {
        console.log(contactData._id);
        setArrayDataAssociate(contactData._id);
      }
    });
  };

  const handleSearch = (event) => {
    event.preventDefault();
    setCurrentPage(1);
  };

  const cancel = () => {
    setSelect("");
  };

  const handleCancel = () => {
    setContactAdd(false);
  };

  const handleDelete = (e, id) => {
    if (represent) {
      if (animating) return;
      setAnimating(true);

      const currentFs = document.querySelector(`#step${step}`);
      const previousFs = document.querySelector(`#step${step - 1}`);
      const progressBarItem = document.querySelector(
        `#progressbar li:nth-child(${step})`
      );
      progressBarItem.classList.remove("active");
      previousFs.style.display = "block";
      let scale, left, opacity;
      const animationInterval = setInterval(() => {
        currentFs.style.opacity -= 0.1;
        scale = 0.8 + (1 - currentFs.style.opacity) * 0.2;
        left = (1 - currentFs.style.opacity) * 50 + "%";
        opacity = 1 - currentFs.style.opacity;
        currentFs.style.left = left;
        previousFs.style.transform = `scale(${scale})`;
        previousFs.style.opacity = opacity;

        if (currentFs.style.opacity <= 0) {
          clearInterval(animationInterval);
          currentFs.style.display = "none";
          setAnimating(false);
          setStep(step - 1);
        }
      }, 40);
      setContactAdd(true);
      setStep1("");
      setShowPopup(false);
      setSelect("");
      setSummary([]);
      setClientId("");
      setContact("");
      setClientdone(false);
    }
  };

  const [checkvalue, setCheckValue] = useState("");
  const [checkvalueContact, setCheckValueContact] = useState("");

  const [checkReferral, setCheckReferral] = useState("no");
  const [checkReferralNew, setCheckReferralNew] = useState("");

  const CheckRegistration = (e) => {
    const { name, value } = e.target;

    setCheckValue(value);

    if (value === "address") {
      setFormValuesNew({
        ...formValuesNew,
        filename: selectproperty?.streetAddress || "",
      });
    } else if (value === "primary_client") {
      setFormValuesNew({
        ...formValuesNew,
        filename: summary?.firstName + " " + summary?.lastName || "",
      });
    }
  };

  const CheckRegistrationContact = (e) => {
    const { name, value } = e.target;
    setCheckValueContact(value);
  };

  const handleChanges = (e) => {
    const { name, value } = e.target;
    setFormValuesNew({ ...formValuesNew, [name]: value });
  };

  useEffect(() => {
    if (formValuesNew && isSubmitClickNew) {
      validateNewdata();
    }
  }, [formValuesNew]);

  const validateNewdata = () => {
    const values = formValuesNew;
    const errors = {};
    if (!values.filename) {
      errors.filename = "File name is required";
    }

    setFormErrors(errors);
    return errors;
  };

  const AddTransaction = async (e) => {
    e.preventDefault();
    setISSubmitClickNew(true);
    let checkValue = validateNewdata();

    if (_.isEmpty(checkValue)) {
      setISSubmit(true);
      let contacts = [];
      let contacts3 = [];

      if (arrayData) {
        // Ensure contact1 is empty when selectedCategory is "referral"

        if (Array.isArray(arrayData)) {
          contacts = arrayData.map((contactId) => ({
            type: selectedCategory,
            contactId,
          }));
        } else {
          contacts = [
            {
              type: selectedCategory,
              contactId: arrayData,
            },
          ];
        }

        if (selectedCategory === "referral") {
          // Add the referral agent as the first item in contact3
          contacts3.push({
            type: selectedCategory + "_Agent",
            contactId: jwt(localStorage.getItem("auth")).id,
          });

          // If contact1 is not empty, add its first item to contact3 (it will be empty in this case)
          if (contacts.length > 0) {
            contacts3.push({
              type: "receiving_Agent",
              contactId: contacts[0].contactId,
            });
          }
        } else {
          // For other categories, populate contact3 normally
          if (Array.isArray(arrayData)) {
            contacts3 = arrayData.map((contactId) => ({
              type: selectedCategory + "_Agent",
              contactId: jwt(localStorage.getItem("auth")).id,
            }));
          } else {
            if (arrayDataAssociate) {
              contacts3 = [
                {
                  type: selectedCategory + "_Agent",
                  contactId: arrayDataAssociate,
                },
                {
                  type: selectedCategory + "_Agent",
                  contactId: jwt(localStorage.getItem("auth")).id,
                },
              ];
            } else {
              contacts3 = [
                {
                  type: selectedCategory + "_Agent",
                  contactId: jwt(localStorage.getItem("auth")).id,
                },
              ];
            }
          }
        }
      }
      const userData = {
        agentId: jwt(localStorage.getItem("auth")).id,
        office_id: localStorage.getItem("active_office_id"),
        office_name: localStorage.getItem("active_office"),
        type: selectedType,
        phase: selectedPhase,
        contact1: selectedCategory === "referral" ? [] : contacts,
        ownership: selectedOwner,
        referralSend:
          checkReferralNew === "yes" || checkReferralNew === "no"
            ? checkReferral
            : "",
        contact3: contacts3, 
        represent: selectedCategory,
        filename:
          formValuesNew.filename ||
          selectproperty?.streetAddress ||
          summary?.firstName + " " + summary?.lastName ||
          "",
        propertyDetail: propertyId ? propertyId.toString() : null,
      };
      try {
        setLoader({ isActive: true });
        const response = await user_service.TransactionCreate(userData);
        if (response) {
          const responseData = await user_service.PropertyLanding(
            response.data._id
          );

          const userDatan = {
            addedBy: jwt(localStorage.getItem("auth")).id,
            agentId: jwt(localStorage.getItem("auth")).id,
            transactionId: response.data._id,
            message: "Transaction Created",
            note_type: "Status",
          };

          await user_service.transactionNotes(userDatan);

          setLoader({ isActive: false });
          setToaster({
            type: "Transaction Successfully",
            isShow: true,
            toasterBody: response.data.message,
            message: "Transaction Successfully",
          });
          setTimeout(() => {
            setToaster((prevToaster) => ({ ...prevToaster, isShow: false }));
            navigate(`/transaction-summary/${response.data._id}`);
          }, 500);

          setISSubmitClickNew(false);
          setISSubmit(false);
        } else {
          setLoader({ isActive: false });
          setToaster({
            types: "error",
            isShow: true,
            toasterBody: response.data.message,
            message: "Error",
          });
        }
      } catch (error) {
        setLoader({ isActive: false });
        setToaster({
          types: "error",
          isShow: true,
          toasterBody: error.response
            ? error.response.data.message
            : error.message,
          message: "Error",
        });
        setTimeout(() => {
          setToaster((prevToaster) => ({ ...prevToaster, isShow: false }));
        }, 3000);
      }
    }
  };

  // const AddTransaction = async (e) => {
  //   e.preventDefault();
  //   setISSubmitClickNew(true);
  //   let checkValue = validateNewdata();
  //   if (_.isEmpty(checkValue)) {
  //     setISSubmit(true);
  //     let contacts = [];
  //     let contacts3 = [];
  //     if (arrayData) {
  //       // Check if arrayData is an array before mapping
  //       if (Array.isArray(arrayData)) {
  //         contacts = arrayData.map((contactId) => ({
  //           type: selectedCategory,
  //           contactId,
  //         }));
  //       } else {
  //         contacts = [
  //           {
  //             type: selectedCategory,
  //             contactId: arrayData,
  //           },
  //         ];
  //       }
  //     }

  //     if (arrayData) {
  //       if (Array.isArray(arrayData)) {
  //         contacts3 = arrayData.map((contactId) => ({
  //           type: selectedCategory + "_Agent",
  //           contactId: jwt(localStorage.getItem("auth")).id,
  //         }));
  //       } else {
  //         if (arrayDataAssociate) {
  //           contacts3 = [
  //             {
  //               type: selectedCategory + "_Agent",
  //               contactId: arrayDataAssociate,
  //             },
  //             {
  //               type: selectedCategory + "_Agent",
  //               contactId: jwt(localStorage.getItem("auth")).id,
  //             },
  //           ];
  //         } else {
  //           contacts3 = [
  //             {
  //               type: selectedCategory + "_Agent",
  //               contactId: jwt(localStorage.getItem("auth")).id,
  //             },
  //           ];
  //         }
  //       }
  //     }

  //     const userData = {
  //       agentId: jwt(localStorage.getItem("auth")).id,
  //       office_id: localStorage.getItem("active_office_id"),
  //       office_name: localStorage.getItem("active_office"),
  //       type: selectedType,
  //       phase: selectedPhase,
  //       contact1: contacts,
  //       ownership: selectedOwner,
  //       referralSend: checkReferral,
  //       contact3: contacts3,
  //       represent: selectedCategory,
  //       filename:
  //         formValuesNew.filename ||
  //         selectproperty?.streetAddress ||
  //         summary?.firstName + " " + summary?.lastName ||
  //         "",
  //       propertyDetail: propertyId.toString(),
  //     };

  //     try {
  //       setLoader({ isActive: true });
  //       const response = await user_service.TransactionCreate(userData);
  //       if (response) {
  //         const responseData = await user_service.PropertyLanding(
  //           response.data._id
  //         );
  //         console.log(responseData);
  //         const userDatan = {
  //           addedBy: jwt(localStorage.getItem("auth")).id,
  //           agentId: jwt(localStorage.getItem("auth")).id,
  //           transactionId: response.data._id,
  //           message: "Transaction Created",
  //           // transaction_property_address: "ADDRESS 2",
  //           note_type: "Status",
  //         };

  //         const responsen = await user_service.transactionNotes(userDatan);

  //         setLoader({ isActive: false });
  //         setToaster({
  //           type: "Transaction Successfully",
  //           isShow: true,
  //           toasterBody: response.data.message,
  //           message: "Transaction Successfully",
  //         });
  //         setTimeout(() => {
  //           setToaster((prevToaster) => ({ ...prevToaster, isShow: false }));
  //           navigate(`/transaction-summary/${response.data._id}`);
  //         }, 500);
  //         setISSubmitClickNew(false);
  //         setISSubmit(false);
  //       } else {
  //         setLoader({ isActive: false });
  //         setToaster({
  //           types: "error",
  //           isShow: true,
  //           toasterBody: response.data.message,
  //           message: "Error",
  //         });
  //       }
  //     } catch (error) {
  //       setLoader({ isActive: false });
  //       setToaster({
  //         types: "error",
  //         isShow: true,
  //         toasterBody: error.response
  //           ? error.response.data.message
  //           : error.message,
  //         message: "Error",
  //       });
  //       setTimeout(() => {
  //         setToaster((prevToaster) => ({ ...prevToaster, isShow: false }));
  //       }, 3000);
  //     }
  //   }
  // };

  const handleChangeReferral = (e) => {
    setCheckReferralNew(e.target.name);
    setCheckReferralNew(e.target.value);
  };
  // select propery section code start

  const initialValuesproperty = {
    listingsAgent: "",
    category: "",
    unitNumber: "",
    propertyType: "",
    mlsNumber: "",
    agentId: "",
    streetDirection: "North",
    associateName: "",
    price: "",
    garage: "",
    bed: "",
    subDivision: "",
    streetAddress: "",
    city: "",
    state: "Utah",
    zipCode: "",
    schoolDistrict: "",
    country: "",
    areaLocation: "",
    status: "",
    listDate: "",
    image: "",
  };

  // const initialValues = {agentId:"", mlsNumber: "", areaLocation: "", streetAddress: "", state: "Utah", mlsId: "", city: "", zipCode: "", price: "", price: "", };
  const [formValuesproperty, setFormValuesproperty] = useState(
    initialValuesproperty
  );
  const [formErrorsproperty, setFormErrorsproperty] = useState({});
  const [isSubmitClickproperty, setISSubmitClickproperty] = useState(false);
  const [step1, setStep1] = useState("");
  const [category, setCategory] = useState("");
  const [categoryProperty, setCategoryProperty] = useState(false);

  const [step2, setStep2] = useState("");
  const [searchby, setSearchby] = useState("mls");
  const [membership, setMembership] = useState("");

  const options = useMemo(() => {
    const countries = countryList.getData();
    const utahState = {
      code: "UT",
      name: "Utah",
      districts: [
        "Alabama",
        "Alaska",
        "Arizona",
        "Arkansas",
        "California",
        "Colorado",
        "Connecticut",
        "Delaware",
        "District of Columbia",
        "Florida",
        "Georgia",
        "Hawaii",
        "Idaho",
        "Illinois",
        "Indiana",
        "Iowa",
        "Kansas",
        "Kentucky",
        "Louisiana",
        "Maine",
        "Maryland",
        "Massachusetts",
        "Michigan",
        "Minnesota",
        "Mississippi",
        "Missouri",
        "Montana",
        "Nebraska",
        "Nevada",
        "New Hampshire",
        "New Jersey",
        "New Mexico",
        "New York",
        "North Carolina",
        "North Dakota",
        "Ohio",
        "Oklahoma",
        "Oregon",
        "Pennsylvania",
        "Rhode Island",
        "South Carolina",
        "South Dakota",
        "Tennessee",
        "Texas",
        "Utah",
        "Vermont",
        "Virginia",
        "Washington",
        "West Virginia",
        "Wisconsin",
        "Wyoming",
      ],
    };
    return [...countries, utahState];
  }, []);

  // const [loader, setLoader] = useState({ isActive: null })
  // const { isActive } = loader;
  // const [toaster, setToaster] = useState({ types: null, isShow: null, toasterBody: "", message: "" });
  // const { types, isShow, toasterBody, message } = toaster;

  const [search_value, setSearchValue] = useState({ associateName: "" });
  const [searchmlsnumber, setSearchmlsnumber] = useState({ mlsnumber: "" });
  const [selectproperty, setSelectproperty] = useState("");
  const [transactionId, setTransactionId] = useState("");
  const [result, setResult] = useState([]);
  const [resultProperty, setResultProperty] = useState([]);
  const [profile, setProfile] = useState("");

  // const [isLoading, setIsLoading] = useState(false);
  const [pageCountProperty, setPageCountProperty] = useState(0);
  const [array, setArray] = useState("");

  // const params = useParams();
  const AddBasic = (stepno, value) => {
    if (stepno === "1") {
      setStep1(stepno);
      setCategory(value);
      setCategoryProperty(true);
      setMembership("");
      setResultProperty("");
    }
  };

  const handleBackProperty = () => {
    setCategoryProperty(false);
    setStep1("");
    setStep2("");

    setMembership(false);
  };

  {
    /* <!-- Input onChange Start--> */
  }

  // const handleChangePrice = (e) => {
  //   const { name, value } = e.target;
  //   const numericValue = value.replace(/[^\d.]/g, "");

  //   if (numericValue !== "") {
  //     const floatValue = parseFloat(numericValue);

  //     if (!isNaN(floatValue)) {
  //       setFormValuesproperty((prevValues) => ({
  //         ...prevValues,
  //         [name]:floatValue.toLocaleString("en-US"),
  //       }));
  //     }
  //   } else {
  //     setFormValuesproperty((prevValues) => ({
  //       ...prevValues,
  //       [name]: "",
  //     }));
  //   }
  // };

  const handleChangeproperty = (e) => {
    const { name, value } = e.target;
    setFormValuesproperty({ ...formValuesproperty, [name]: value });
  };

  const handleChangesearch = (event) => {
    setSearchValue({ associateName: event.target.value });
  };

  const handleChangesearchmls = (event) => {
    setSearchmlsnumber({ mlsnumber: event.target.value });
  };

  useEffect(() => {
    window.scrollTo(0, 0);
    const profileGetAll = async () => {
      setLoader({ isActive: true });
      await user_service
        .profileGet(jwt(localStorage.getItem("auth")).id)
        .then((response) => {
          if (response) {
            console.log(response);
            setLoader({ isActive: false });
            setProfile(response.data);
          }
        });
    };
    profileGetAll();
  }, []);

  {
    /* <!-- Input onChange End--> */
  }

  {
    /* <!-- Form Validation Start--> */
  }
  useEffect(() => {
    if (formValuesproperty && isSubmitClickproperty) {
      validateproperty();
    }
  }, [formValuesproperty]);

  const validateproperty = () => {
    const values = formValuesproperty;
    const errorsproperty = {};
    // if (!values.streetNumber) {
    //   errorsproperty.streetNumber = "Street Number is required";
    // }

    if (!values.streetAddress) {
      errorsproperty.streetAddress = "Property address is required";
    }

    if (!values.city) {
      errorsproperty.city = "City is required";
    }
    if (!values.state) {
      errorsproperty.state = "State is required";
    }

    if (!values.zipCode) {
      errorsproperty.zipCode = "Postal Code is required";
    }
    if (!values.propertyType) {
      errorsproperty.propertyType = "Property Type is required";
    }

    // if (data) {
    // } else {
    //   if (!values.image) {
    //     errorsproperty.image = "Property Image is required";
    //   }
    // }
    // if (!values.priceLease) {
    //   errorsproperty.priceLease = "PriceLease is required";
    // }
    setFormErrorsproperty(errorsproperty);
    setFormErrors(errorsproperty);

    // Scroll to the first error element
    const firstErrorKey = Object.keys(errorsproperty)[0];
    if (firstErrorKey) {
      const firstErrorElement = document.querySelector(
        `[name="${firstErrorKey}"]`
      );
      if (firstErrorElement) {
        // Scroll to the top of the error element
        window.scrollTo({
          top: firstErrorElement.offsetTop,
          behavior: "smooth", // You can use "auto" instead of "smooth" for an instant scroll
        });
      }
    }
    return errorsproperty;
  };
  {
    /* <!-- Form Validation End--> */
  }

  const handleFileUpload = async (e) => {
    const selectedFile = e.target.files[0];

    if (selectedFile) {
      if (!selectedFile.type.startsWith("image/")) {
        setToaster({
          types: "Select Property",
          isShow: true,
          // toasterBody: response.data.message,
          message: "Please select an image file",
        });
        setTimeout(() => {
          setToaster((prevToaster) => ({
            ...prevToaster,
            isShow: false,
          }));
        }, 2000);
        return;
      }

      setFile(selectedFile);

      const fileNameWithoutExtension = selectedFile.name.replace(
        /\.[^/.]+$/,
        ""
      );
      const formData = new FormData();
      formData.append("file", selectedFile);

      const config = {
        headers: {
          Accept: "application/json",
          "Content-Type": "multipart/form-data",
          Authorization: `Bearer ${localStorage.getItem("auth")}`,
        },
      };

      try {
        setLoader({ isActive: true });

        const uploadResponse = await axios.post(
          "https://api.brokeragentbase.com/upload",
          formData,
          config
        );
        const uploadedFileData = uploadResponse.data;

        setLoader({ isActive: false });
        setData(uploadedFileData);
      } catch (error) {
        console.error("Error occurred during file upload:", error);
      }
    }
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
    setISSubmitClickproperty(true);
    let checkValueproperty = validateproperty();
    if (_.isEmpty(checkValueproperty)) {
      const currentDate = new Date();
      const formattedDate = currentDate.toISOString();

      const userData = {
        agentId: jwt(localStorage.getItem("auth")).id,
        // listingsAgent: "Shawn",
        // category: formValuesproperty.category,
        unitNumber: formValuesproperty.unitNumber,
        propertyType: formValuesproperty.propertyType,
        streetNumber: formValuesproperty.streetNumber,
        associateName: profile.firstName + " " + profile.lastName,
        price: formValuesproperty.price,
        // priceLease: formValuesproperty.priceLease,
        city: formValuesproperty.city,
        state: formValuesproperty.state,
        country: "Utah",
        subDivision: "Utah",
        streetAddress: formValuesproperty.streetAddress,
        zipCode: formValuesproperty.zipCode,
        schoolDistrict: "Salt lake Utah",
        areaLocation: "Utah",
        streetDirection: formValuesproperty.streetDirection,
        status: "Active",
        listDate: formattedDate,
        image: data,
      };

      console.log(userData);

      try {
        setLoader({ isActive: true });
        const response = await user_service.listingsCreate(userData);
        setLoader({ isActive: false });
        if (response) {
          setLoader({ isActive: false });
          setToaster({
            types: "SelectProperty",
            isShow: true,
            toasterBody: response.data.message,
            message: "Property Added Successfully",
          });
          setPropertyId(response.data._id);
          setSelectproperty(response.data);
          setTimeout(() => {
            setToaster((prevToaster) => ({
              ...prevToaster,
              isShow: false,
            }));
          }, 500);
          setISSubmitClickproperty(false);
        } else {
          setLoader({ isActive: false });
          setToaster({
            types: "error",
            isShow: true,
            toasterBody: response.data.message,
            message: "Error",
          });
        }
      } catch (error) {
        setLoader({ isActive: false });
        setToaster({
          types: "error",
          isShow: true,
          toasterBody: error,
          message: "Error",
        });
      }
      setTimeout(() => {
        setToaster({
          types: "error",
          isShow: false,
          toasterBody: null,
          message: "Error",
        });
      }, 1000);
    }
  };
  {
    /* <!-- Form onSubmit End--> */
  }

  const propertySearch = async () => {
    if (search_value.associateName) {
      try {
        setIsLoading(true);

        const response = await user_service.listingSearch(
          1,
          1,
          search_value.associateName
        );
        if (response) {
          setResultProperty(response.data.data);
          setPageCountProperty(Math.ceil(response.data.totalRecords / 3));
        }

        setIsLoading(false);
      } catch (error) {
        console.error(error);
        setIsLoading(false);
      }
    }
  };

  const handlePageClickproperty = async (data) => {
    let currentPage = data.selected + 1;
    let skip = currentPage - 1 + currentPage;
    setLoader({ isActive: true });
    await user_service
      .listingSearch(currentPage, 1, search_value.associateName)
      .then((response) => {
        setLoader({ isActive: false });
        if (response) {
          setResultProperty(response.data.data);
          setPageCountProperty(Math.ceil(response.data.totalRecords / 10));
        }
      });
  };

  const MLSSearch = async () => {
    if (searchmlsnumber.mlsnumber) {
      try {
        setIsLoading(true);
        let response;
        if (membership === "UtahRealEstate.com") {
          response = await user_service.listingSearchMLS(
            searchmlsnumber.mlsnumber
          );
        } else if (membership === "Park City") {
          response = await user_service.listingSearchMLSspark(
            searchmlsnumber.mlsnumber,
            "parkcity"
          );
        } else if (membership === "Washington County") {
          response = await user_service.listingSearchMLSspark(
            searchmlsnumber.mlsnumber,
            "washingtoncounty"
          );
        } else if (membership === "Iron County") {
          response = await user_service.listingSearchMLSspark(
            searchmlsnumber.mlsnumber,
            "ironcountry"
          );
        } else {
          response = await user_service.listingSearchMLS(
            searchmlsnumber.mlsnumber
          );
        }

        if (response) {
          console.log(response);
          setResultProperty(response.data);
          setPageCountProperty(1);
        }

        setIsLoading(false);
      } catch (error) {
        console.error(error);
        setIsLoading(false);
      }
    }
  };

  const handlePropertyUpdate = async (propertyId) => {
    try {
      if (searchby === "mls") {
        setSelectproperty(propertyId);
        if (propertyId) {
          const resp = await handleSearchListingMLS(propertyId);
          setSelectproperty(resp);

          const updatedPropertyId = resp?._id || null;
          setPropertyId(updatedPropertyId);
        }
      } else {
        const responseGetById = await user_service.listingsGetById(propertyId); // Use a different variable name
        setSelectproperty(responseGetById.data);

        if (responseGetById.data) {
          const updateDataListing = {
            id: responseGetById.data._id,
          };
          const responseUpdate = await user_service.listingUpdate(
            propertyId,
            updateDataListing
          );
          setPropertyId(responseUpdate.data._id);
        }

        // if (responseGetById.data) {
        //   const resp = await handleSearchListingMLS(responseGetById.data);
        //   setSelectproperty(resp);

        //   const updatedPropertyId = resp?._id || null;
        //   setPropertyId(updatedPropertyId);
        //   console.log(updatedPropertyId);
        // }
      }
    } catch (error) {
      console.error("Error in handlePropertyUpdate:", error);
      // Handle errors as needed, e.g., show a user-friendly error message
    }
  };

  const [selectedImageUrl, setSelectedImageUrl] = useState("");

  const handleImageUrl = (imageUrl) => {
    setSelectedImageUrl(imageUrl);
  };

  const handleSearchListingMLS = async (mlsdata) => {
    if (mlsdata.value) {
      const mlspropdata = {
        agentId: jwt(localStorage.getItem("auth")).id,
        listingsAgent: mlsdata.value[0].ListAgentFullName ?? "N/A",
        category: mlsdata.value[0].PropertyType
          ? mlsdata.value[0].PropertyType
          : "N/A",
        mlsId: mlsdata.value[0].ListingId
          ? mlsdata.value[0].ListingId.toString()
          : "N/A",
        propertyType: mlsdata.value[0].PropertyType
          ? mlsdata.value[0].PropertyType
          : "N/A",
        mlsNumber: mlsdata.value[0].ListingId.toString(),
        associateName: mlsdata.value[0]?.AssociationName
          ? mlsdata.value[0].AssociationName
          : "N/A",
        price: mlsdata?.value[0]?.ListPrice
          ? mlsdata?.value[0]?.ListPrice.toString()
          : "N/A",
        priceLease: mlsdata.value[0]?.LeaseAmount
          ? mlsdata.value[0].LeaseAmount.toString()
          : "N/A",
        garage: mlsdata.value[0]?.GarageSpaces
          ? mlsdata.value[0].GarageSpaces.toString()
          : "N/A",
        bed: mlsdata.value[0]?.BedroomsTotal
          ? mlsdata.value[0].BedroomsTotal.toString()
          : "N/A",
        year_built: mlsdata.value[0]?.YearBuilt
          ? mlsdata.value[0].YearBuilt.toString()
          : "N/A",

        baths: mlsdata.value[0]?.BathroomsTotalInteger
          ? mlsdata.value[0].BathroomsTotalInteger.toString()
          : "N/A",

        square_feet: mlsdata.value[0]?.LotSizeSquareFeet
          ? mlsdata.value[0].LotSizeSquareFeet.toString()
          : "N/A",
        
        building_area_total: mlsdata.value[0]?.BuildingAreaTotal
          ? mlsdata.value[0].BuildingAreaTotal.toString()
          : "N/A",

        city: mlsdata.value[0]?.City ? mlsdata.value[0]?.City : "N/A",
        state: mlsdata.value[0]?.StateOrProvince
          ? mlsdata.value[0]?.StateOrProvince
          : "N/A",
        country: mlsdata.value[0]?.CountyOrParish
          ? mlsdata.value[0]?.CountyOrParish
          : "N/A",
        subDivision: mlsdata.value[0]?.SubdivisionName
          ? mlsdata.value[0]?.SubdivisionName
          : "N/A",
        streetAddress: mlsdata.value[0]?.UnparsedAddress
          ? mlsdata.value[0]?.UnparsedAddress
          : "N/A",
        zipCode: mlsdata.value[0]?.PostalCode
          ? mlsdata.value[0].PostalCode.toString()
          : "N/A",
        schoolDistrict: mlsdata.value[0]?.ElementarySchoolDistrict
          ? mlsdata.value[0]?.ElementarySchoolDistrict
          : "N/A",
        areaLocation: mlsdata.value[0]?.MLSAreaMajor
          ? mlsdata.value[0]?.MLSAreaMajor
          : "N/A",
        streetDirection: "N/A",
        status: mlsdata.value[0]?.MlsStatus
          ? mlsdata.value[0]?.MlsStatus
          : "N/A",
        listDate: mlsdata.value[0]?.OriginalEntryTimestamp ?? new Date(),
        image: selectedImageUrl
          ? selectedImageUrl
          : mlsdata?.media && mlsdata?.media[0]?.MediaURL
          ? mlsdata?.media[0]?.MediaURL
          : "default_image_url",
        listing_AgentID: mlsdata.value[0]?.ListAgentKey
          ? mlsdata.value[0].ListAgentKey
          : "",
        propertyFrom: "mls",
        additionaldataMLS: JSON.stringify(mlsdata),
      };

      console.log(mlspropdata);
      console.log("mlspropdata");

      try {
        setLoader({ isActive: true });
        const response = await user_service.listingsCreate(mlspropdata);
        setLoader({ isActive: false });

        if (response) {
          setToaster({
            types: "SelectProperty",
            isShow: true,
            toasterBody: response.data.message,
            message: "SelectProperty Successfully",
          });
          setTimeout(() => {
            setToaster((prevToaster) => ({ ...prevToaster, isShow: false }));
          }, 1000);

          // Return the response or the required data from the response
          return response.data; // You can modify this to return the specific data you need
        } else {
          setToaster({
            types: "error",
            isShow: true,
            toasterBody: response.data.message,
            message: "Error",
          });

          // Return an error or null if needed
          return null;
        }
      } catch (error) {
        setLoader({ isActive: false });
        setToaster({
          types: "error",
          isShow: true,
          toasterBody: error,
          message: "Error",
        });

        // Reject the promise with the error
        throw error;
      }
    }
  };

  const changeOption = () => {
    setStep1("");
  };
  const Cancel = () => {
    setStep2("");
    setResult("");
    setSelect("");
    setPropertyId("");
  };

  const CancelProperty = () => {
    setSelectproperty("");
    setPropertyId("");
  };

  const clickHere = () => {
    setStep1("");
  };
  const handleRemoveContact = () => {
    setSummary("");
    setFormValues("");
    setContactAdd(true);
  };

  const Searchbyfunction = (e, type) => {
    e.preventDefault();
    setSearchby(type);
  };

  const [getContact, setGetContact] = useState(initialValues);
  const [mls_membership, setMls_membership] = useState([]);
  const [
    commaSeparatedValuesmls_membership,
    setCommaSeparatedValuesmls_membership,
  ] = useState("");

  const ContactGetById = async () => {
    await user_service
      .contactGetById(jwt(localStorage.getItem("auth")).id)
      .then((response) => {
        if (response) {
          setGetContact(response.data);

          if (response.data && response.data.additionalActivateFields) {
            const mls_membershipString =
              response.data.additionalActivateFields?.mls_membership;
            if (mls_membershipString) {
              // Check if mls_membershipString is a string
              if (typeof mls_membershipString === "string") {
                try {
                  // Attempt to parse the string as JSON
                  const mls_membershipParsed = JSON.parse(mls_membershipString);
                  // Set the parsed value to your state or variable
                  if (mls_membershipParsed == "") {
                    setMls_membership(mls_membership);
                  } else {
                    setMls_membership(mls_membershipParsed);
                  }
                  if (mls_membershipParsed == "") {
                    setCommaSeparatedValuesmls_membership("");
                  } else {
                    const boardMembershipValues = mls_membershipParsed.map(
                      (item) => item.value
                    );

                    setCommaSeparatedValuesmls_membership(
                      // boardMembershipValues.join(", ")
                      boardMembershipValues
                    );
                  }
                } catch (error) {
                  // Handle the error if parsing fails
                  console.error("Error parsing mls_membership as JSON:", error);
                  // You might want to set a default value or handle the error in another way
                  // For debugging, you can log the JSON parse error message:
                  console.error("JSON Parse Error:", error.message);
                  // Set a default value if parsing fails
                  setCommaSeparatedValuesmls_membership(""); // Replace "defaultValue" with your default value
                }
              } else {
                //console.log("sdfdsfdsfsd");
                // Handle the case where mls_membershipString is not a string (e.g., it's already an object)
                // You can directly set it to your state or variable
                setCommaSeparatedValuesmls_membership("");
              }
            } else {
              // Handle the case where mls_membershipString is undefined or null
              // Set a default value or handle it as needed
              setCommaSeparatedValuesmls_membership(""); // Replace "defaultValue" with your default value
            }
          }
        }
      });
  };
  // select property section code end

  const propertypic = (e) => {
    e.target.src = defaultpropertyimage;
  };

  const prevStep = () => {
    if (animating) return;
    setAnimating(true);

    const currentFs = document.querySelector(`#step${step}`);
    const previousFs = document.querySelector(`#step${step - 1}`);
    const progressBarItem = document.querySelector(
      `#progressbar li:nth-child(${step})`
    );
    progressBarItem.classList.remove("active");
    previousFs.style.display = "block";
    let scale, left, opacity;
    const animationInterval = setInterval(() => {
      currentFs.style.opacity -= 0.1;
      scale = 0.8 + (1 - currentFs.style.opacity) * 0.2;
      left = (1 - currentFs.style.opacity) * 50 + "%";
      opacity = 1 - currentFs.style.opacity;
      currentFs.style.left = left;
      previousFs.style.transform = `scale(${scale})`;
      previousFs.style.opacity = opacity;

      if (currentFs.style.opacity <= 0) {
        clearInterval(animationInterval);
        currentFs.style.display = "none";
        setAnimating(false);
        setStep(step - 1);
      }
    }, 40);
  };

  // const handleSubmit = () => {
  //   console.log('Form submitted');
  // };

  const handleCategory = (category) => {
    // Define the animation logic as a separate function
    if (animating) return;
    setAnimating(true);

    const currentFs = document.querySelector(`#step${step}`);
    const nextFs = document.querySelector(`#step${step + 1}`);
    const progressBarItem = document.querySelector(
      `#progressbar li:nth-child(${step + 1})`
    );
    progressBarItem.classList.add("active");
    nextFs.style.display = "block";
    let scale, left, opacity;
    const animationInterval = setInterval(() => {
      currentFs.style.opacity -= 0.1;
      scale = 1 - (1 - currentFs.style.opacity) * 0.2;
      left = parseFloat(currentFs.style.opacity) * 50 + "%";
      opacity = 1 - currentFs.style.opacity;
      currentFs.style.transform = `scale(${scale})`;
      nextFs.style.left = left;
      nextFs.style.opacity = opacity;
      if (currentFs.style.opacity <= 0) {
        clearInterval(animationInterval);
        currentFs.style.display = "none";
        setAnimating(false);
        setStep(step + 1);
      }
    }, 40);

    setSelectedCategory(category);
  };

  const handleType = (type) => {
    // Define the animation logic as a separate function
    if (animating) return;
    setAnimating(true);

    const currentFs = document.querySelector(`#step${step}`);
    const nextFs = document.querySelector(`#step${step + 1}`);
    const progressBarItem = document.querySelector(
      `#progressbar li:nth-child(${step + 1})`
    );
    progressBarItem.classList.add("active");
    nextFs.style.display = "block";
    let scale, left, opacity;
    const animationInterval = setInterval(() => {
      currentFs.style.opacity -= 0.1;
      scale = 1 - (1 - currentFs.style.opacity) * 0.2;
      left = parseFloat(currentFs.style.opacity) * 50 + "%";
      opacity = 1 - currentFs.style.opacity;
      currentFs.style.transform = `scale(${scale})`;
      nextFs.style.left = left;
      nextFs.style.opacity = opacity;
      if (currentFs.style.opacity <= 0) {
        clearInterval(animationInterval);
        currentFs.style.display = "none";
        setAnimating(false);
        setStep(step + 1);
      }
    }, 40);

    setSelectedType(type);
  };

  const handleOwnership = (owner) => {
    // Define the animation logic as a separate function
    if (animating) return;
    setAnimating(true);

    const currentFs = document.querySelector(`#step${step}`);
    const nextFs = document.querySelector(`#step${step + 1}`);
    const progressBarItem = document.querySelector(
      `#progressbar li:nth-child(${step + 1})`
    );
    progressBarItem.classList.add("active");
    nextFs.style.display = "block";
    let scale, left, opacity;
    const animationInterval = setInterval(() => {
      currentFs.style.opacity -= 0.1;
      scale = 1 - (1 - currentFs.style.opacity) * 0.2;
      left = parseFloat(currentFs.style.opacity) * 50 + "%";
      opacity = 1 - currentFs.style.opacity;
      currentFs.style.transform = `scale(${scale})`;
      nextFs.style.left = left;
      nextFs.style.opacity = opacity;
      if (currentFs.style.opacity <= 0) {
        clearInterval(animationInterval);
        currentFs.style.display = "none";
        setAnimating(false);
        setStep(step + 1);
      }
    }, 40);

    setSelectedOwner(owner);
  };

  const handlePhase = (phase) => {
    // Define the animation logic as a separate function
    if (animating) return;
    setAnimating(true);

    const currentFs = document.querySelector(`#step${step}`);
    const nextFs = document.querySelector(`#step${step + 1}`);
    const progressBarItem = document.querySelector(
      `#progressbar li:nth-child(${step + 1})`
    );
    progressBarItem.classList.add("active");
    nextFs.style.display = "block";
    let scale, left, opacity;
    const animationInterval = setInterval(() => {
      currentFs.style.opacity -= 0.1;
      scale = 1 - (1 - currentFs.style.opacity) * 0.2;
      left = parseFloat(currentFs.style.opacity) * 50 + "%";
      opacity = 1 - currentFs.style.opacity;
      currentFs.style.transform = `scale(${scale})`;
      nextFs.style.left = left;
      nextFs.style.opacity = opacity;
      if (currentFs.style.opacity <= 0) {
        clearInterval(animationInterval);
        currentFs.style.display = "none";
        setAnimating(false);
        setStep(step + 1);
      }
    }, 40);

    setSelectedPhase(phase);
  };

  const selectContact = async (id) => {
    try {
      if (animating) return;
      setAnimating(true);

      const currentFs = document.querySelector(`#step${step}`);
      const nextFs = document.querySelector(`#step${step + 1}`);
      const progressBarItem = document.querySelector(
        `#progressbar li:nth-child(${step + 1})`
      );
      progressBarItem.classList.add("active");
      nextFs.style.display = "block";
      let scale, left, opacity;
      const animationInterval = setInterval(() => {
        currentFs.style.opacity -= 0.1;
        scale = 1 - (1 - currentFs.style.opacity) * 0.2;
        left = parseFloat(currentFs.style.opacity) * 50 + "%";
        opacity = 1 - currentFs.style.opacity;
        currentFs.style.transform = `scale(${scale})`;
        nextFs.style.left = left;
        nextFs.style.opacity = opacity;
        if (currentFs.style.opacity <= 0) {
          clearInterval(animationInterval);
          currentFs.style.display = "none";
          setAnimating(false);
          setStep(step + 1);
        }
      }, 40);

      const response = await user_service.contactGetById(id);
      const contactData = response.data;
      //console.log(selectContact)
      setSummary(contactData);
      setArrayData(contactData._id);
      setClientId(id);
      // setSelect(contactData);
      setClientdone(true);
      document.getElementById("closeModal").click();
    } catch (error) {
      //console.log(error);
    }
  };

  const selectContactAdd = async (id) => {
    try {
      if (animating) return;
      setAnimating(true);

      const currentFs = document.querySelector(`#step${step}`);
      const nextFs = document.querySelector(`#step${step + 1}`);
      const progressBarItem = document.querySelector(
        `#progressbar li:nth-child(${step + 1})`
      );
      progressBarItem.classList.add("active");
      nextFs.style.display = "block";
      let scale, left, opacity;
      const animationInterval = setInterval(() => {
        currentFs.style.opacity -= 0.1;
        scale = 1 - (1 - currentFs.style.opacity) * 0.2;
        left = parseFloat(currentFs.style.opacity) * 50 + "%";
        opacity = 1 - currentFs.style.opacity;
        currentFs.style.transform = `scale(${scale})`;
        nextFs.style.left = left;
        nextFs.style.opacity = opacity;
        if (currentFs.style.opacity <= 0) {
          clearInterval(animationInterval);
          currentFs.style.display = "none";
          setAnimating(false);
          setStep(step + 1);
        }
      }, 40);

      const response = await user_service.contactGetById(id);
      const contactData = response.data;
      //console.log(contactData)
      setSummary(contactData);
      setArrayData(contactData._id);
      setClientId(id);
      // setSelect("");
      setClientdone(true);
      setCheckReferralNew(true);

      document.getElementById("closeModal").click();
    } catch (error) {
      //console.log(error);
    }
  };

  const handleAdd = () => {
    setContactAdd(true);
    setSummary("");
  };

  const handleChangeClick = () => {
    setChangeId(true);
    setSelectAssociate("");
    setGetAllAssociate([]);
  };
  const handleChangeCancel = () => {
    setChangeId("");
    setSelectAssociate("");
    setGetAllAssociate([]);
  };

  const SearchGetAllAssociate = async () => {
    setIsLoading(true);

    let contactTypee = contactTypeAssociate;
    if (contactTypeAssociate === "associate") {
      contactTypee += `&agentId=${jwt(localStorage.getItem("auth")).id}`;
    }

    try {
      const response = await user_service.SearchContactfilter(
        0,
        contactTypee,
        query
      );
      setIsLoading(false);

      if (response) {
        setGetAllAssociate(response.data.data);
      }
    } catch (error) {
      console.error("Error fetching data:", error);
      setIsLoading(false);
    }
  };

  useEffect(() => {
    window.scrollTo(0, 0);
    if (query || contactTypeAssociate) {
      SearchGetAllAssociate(1);
    }
  }, [query, contactTypeAssociate]);

  return (
    <div className="bg-secondary float-left w-100 pt-4 mb-4">
      <Loader isActive={isActive} />
      {isShow && (
        <Toaster
          types={types}
          isShow={isShow}
          toasterBody={toasterBody}
          message={message}
        />
      )}
      <main className="page-wrapper profile_page_wrap">
        <div className="">
          <h3 className="mb-3 p-0">Add a New Transaction</h3>
          <hr className="mb-0" />
          <div className="row">
            <div className="col-md-9 mt-4">
              <div className="">
                <div id="msform" className="msform">
                  <div className="">
                    <ul id="progressbar" className="progressbar d-none">
                      <li className={step === 1 ? "active" : ""}></li>
                      <li className={step === 2 ? "active" : ""}>
                        Social Profiles
                      </li>
                      <li className={step === 3 ? "active" : ""}>
                        Personal Details
                      </li>
                      <li className={step === 4 ? "active" : ""}>
                        Phase Details
                      </li>
                      <li className={step === 5 ? "active" : ""}>
                        Select Contacts
                      </li>
                      <li className={step === 6 ? "active" : ""}>
                        Select Property
                      </li>
                    </ul>

                    {/* Step 1  */}
                    <fieldset id="step1" className={step === 1 ? "show" : ""}>
                      <div className="col-sm-12 mb-3">
                        <h6 className="text-center">
                          Who do you represent in the transaction?
                        </h6>
                        <div className="choose_options w-100 text-center d-table mt-3">
                          <button
                            onClick={() => handleCategory("buyer")}
                            className={`btn btn-primary rounded-4 m-2 ${
                              selectedCategory === "buyer" ? "active" : ""
                            }`}
                          >
                            Buyer
                          </button>

                          <button
                            onClick={() => handleCategory("seller")}
                            className={`btn btn-primary rounded-4 m-2 ${
                              selectedCategory === "seller" ? "active" : ""
                            }`}
                          >
                            Seller
                          </button>
                          <button
                            onClick={() => handleCategory("both")}
                            className={`btn btn-primary rounded-4 m-2 ${
                              selectedCategory === "both" ? "active" : ""
                            }`}
                          >
                            Buyer & Seller
                          </button>
                          <button
                            onClick={() => handleCategory("referral")}
                            className={`btn btn-primary rounded-4 m-2 ${
                              selectedCategory === "referral" ? "active" : ""
                            }`}
                          >
                            Referral
                          </button>
                        </div>
                      </div>
                    </fieldset>

                    {/* Step 2 */}

                    <fieldset id="step2" className={step === 2 ? "show" : ""}>
                      <div className="col-sm-12 mb-3">
                        {selectedCategory === "referral" ? (
                          <>
                            <h6 className="text-center">
                              Please select the Transaction type you're
                              referring?
                            </h6>
                          </>
                        ) : (
                          <h6 className="text-center">
                            Please select the transaction type you’re working
                            on?
                          </h6>
                        )}

                        <div className="choose_options w-100 text-center d-table mt-3">
                          {selectedCategory === "buyer" ? (
                            <>
                              <button
                                onClick={() => handleType("buy_residential")}
                                className="btn btn-primary rounded-4 m-2"
                              >
                                Residential
                              </button>
                              <button
                                onClick={() => handleType("buy_commercial")}
                                className="btn btn-primary rounded-4 m-2"
                              >
                                Commercial
                              </button>
                              <button
                                onClick={() => handleType("buy_investment")}
                                className="btn btn-primary rounded-4 m-2"
                              >
                                Investment
                              </button>
                              <button
                                onClick={() => handleType("buy_land")}
                                className="btn btn-primary rounded-4 m-2"
                              >
                                Land
                              </button>
                              <button
                                onClick={() => handleType("buy_referral")}
                                className="btn btn-primary rounded-4 m-2"
                              >
                                Referral
                              </button>
                              <button
                                onClick={() => handleType("buy_lease")}
                                className="btn btn-primary rounded-4 m-2"
                              >
                                Lease
                              </button>
                            </>
                          ) : (
                            ""
                          )}

                          {selectedCategory === "seller" ? (
                            <>
                              <button
                                onClick={() => handleType("sale_residential")}
                                className="btn btn-primary rounded-4 m-2"
                              >
                                Residential
                              </button>
                              <button
                                onClick={() => handleType("sale_commercial")}
                                className="btn btn-primary rounded-4 m-2"
                              >
                                Commercial
                              </button>
                              <button
                                onClick={() => handleType("sale_investment")}
                                className="btn btn-primary rounded-4 m-2"
                              >
                                Investment
                              </button>

                              {/* <button
                                    onClick={() => handleType("sale_mobile")}
                                    className="btn btn-primary rounded-4 m-2"
                                  >
                                     Mobile
                                  </button> */}

                              <button
                                onClick={() => handleType("sale_referral")}
                                className="btn btn-primary rounded-4 m-2"
                              >
                                Referral
                              </button>
                              <button
                                onClick={() => handleType("sale_lease")}
                                className="btn btn-primary rounded-4 m-2"
                              >
                                Lease
                              </button>
                            </>
                          ) : (
                            ""
                          )}

                          {selectedCategory === "both" ? (
                            <>
                              <button
                                onClick={() => handleType("both_residential")}
                                className="btn btn-primary rounded-4 m-2"
                              >
                                Residential
                              </button>
                              <button
                                onClick={() => handleType("both_commercial")}
                                className="btn btn-primary rounded-4 m-2"
                              >
                                Commercial
                              </button>
                              <button
                                onClick={() => handleType("both_investment")}
                                className="btn btn-primary rounded-4 m-2"
                              >
                                Investment
                              </button>
                              <button
                                onClick={() => handleType("both_land")}
                                className="btn btn-primary rounded-4 m-2"
                              >
                                Land
                              </button>
                              <button
                                onClick={() => handleType("both_referral")}
                                className="btn btn-primary rounded-4 m-2"
                              >
                                Referral
                              </button>
                              <button
                                onClick={() => handleType("both_lease")}
                                className="btn btn-primary rounded-4 m-2"
                              >
                                Lease
                              </button>
                            </>
                          ) : (
                            ""
                          )}

                          {selectedCategory === "referral" ? (
                            <div>
                              <button
                                onClick={() =>
                                  handleType("referral_residential")
                                }
                                className="btn btn-primary rounded-4 m-2"
                              >
                                Residential
                              </button>
                              <button
                                onClick={() =>
                                  handleType("referral_commercial")
                                }
                                className="btn btn-primary rounded-4 m-2"
                              >
                                Commercial
                              </button>
                              <button
                                onClick={() =>
                                  handleType("referral_investment")
                                }
                                className="btn btn-primary rounded-4 m-2"
                              >
                                Investment
                              </button>
                              <button
                                onClick={() => handleType("referral_land")}
                                className="btn btn-primary rounded-4 m-2"
                              >
                                Land
                              </button>
                              <button
                                onClick={() => handleType("referral_lease")}
                                className="btn btn-primary rounded-4 m-2"
                              >
                                Lease
                              </button>
                            </div>
                          ) : (
                            ""
                          )}
                        </div>
                      </div>
                      <div className="float-left w-100 text-center">
                        <button
                          type="button"
                          className="btn btn-primary btn-sm"
                          onClick={prevStep}
                        >
                          Previous
                        </button>
                      </div>
                      {/* <button type="button" className="btn btn-primary btn-sm ms-3" onClick={nextStep}>
                          Next
                        </button> */}
                    </fieldset>

                    {/* Step 3 */}
                    <fieldset id="step3" className={step === 3 ? "show" : ""}>
                      <div className="col-sm-12 mb-3">
                        <h6 className="text-center">
                          Who is the buyer/Seller? Please select an ownership
                          style.
                        </h6>

                        <div className="choose_options w-100 text-center d-table mt-3">
                          <button
                            onClick={() => handleOwnership("individual")}
                            className="btn btn-primary rounded-4 m-2"
                          >
                            Individual
                          </button>

                          <button
                            onClick={() => handleOwnership("corporation")}
                            className="btn btn-primary rounded-4 m-2"
                          >
                            Entity/Corporation
                          </button>

                          <button
                            onClick={() => handleOwnership("trust")}
                            className="btn btn-primary rounded-4 m-2"
                          >
                            Trust
                          </button>
                        </div>
                        {/* {selectedCategory === "buyer" ? (
                              <>
                                <div className="choose_options w-100 text-center d-table mt-3">
                                  <button
                                    onClick={() =>
                                      handleOwnership("conventional")
                                    }
                                    className="btn btn-primary rounded-4 m-2"
                                  >
                                    Conventional
                                  </button>

                                  <button
                                    onClick={() =>
                                      handleOwnership("corporation")
                                    }
                                    className="btn btn-primary rounded-4 m-2"
                                  >
                                    Corporation
                                  </button>

                                  <button
                                    onClick={() => handleOwnership("trust")}
                                    className="btn btn-primary rounded-4 m-2"
                                  >
                                    Trust
                                  </button>
                                </div>
                              </>
                            ) : (
                              ""
                            )} */}

                        {/* 
                            {selectedCategory === "seller" ? (
                              <>
                                <div className="choose_options w-100 text-center d-table mt-3">
                                  <button
                                    onClick={() =>
                                      handleOwnership("conventional")
                                    }
                                    className="btn btn-primary rounded-4 m-2"
                                  >
                                    Conventional
                                  </button>

                                  <button
                                    onClick={() =>
                                      handleOwnership("newConstruction")
                                    }
                                    className="btn btn-primary rounded-4 m-2"
                                  >
                                    New Construction
                                  </button>

                                  <button
                                    onClick={() =>
                                      handleOwnership("corporation")
                                    }
                                    className="btn btn-primary rounded-4 m-2"
                                  >
                                    Corporation
                                  </button>

                                  <button
                                    onClick={() => handleOwnership("trust")}
                                    className="btn btn-primary rounded-4 m-2"
                                  >
                                    Trust
                                  </button>

                                  <button
                                    onClick={() => handleOwnership("probate")}
                                    className="btn btn-primary rounded-4 m-2"
                                  >
                                    Probate
                                  </button>

                                  <button
                                    onClick={() =>
                                      handleOwnership("foreclosure")
                                    }
                                    className="btn btn-primary rounded-4 m-2"
                                  >
                                    Foreclosure/REO
                                  </button>

                                  <button
                                    onClick={() => handleOwnership("hud")}
                                    className="btn btn-primary rounded-4 m-2"
                                  >
                                    HUD
                                  </button>

                                  <button
                                    onClick={() => handleOwnership("shortSale")}
                                    className="btn btn-primary rounded-4 m-2"
                                  >
                                    Short Sale
                                  </button>
                                </div>
                              </>
                            ) : (
                              ""
                            )} */}

                        {/* {selectedCategory === "both" ? (
                              <>
                                <div className="choose_options w-100 text-center d-table mt-3">
                                  <button
                                    onClick={() =>
                                      handleOwnership("conventional")
                                    }
                                    className="btn btn-primary rounded-4 m-2"
                                  >
                                    Conventional
                                  </button>

                                  <button
                                    onClick={() =>
                                      handleOwnership("corporation")
                                    }
                                    className="btn btn-primary rounded-4 m-2"
                                  >
                                    Corporation
                                  </button>

                                  <button
                                    onClick={() => handleOwnership("trust")}
                                    className="btn btn-primary rounded-4 m-2"
                                  >
                                    Trust
                                  </button>
                                </div>
                              </>
                            ) : (
                              ""
                            )} */}

                        {/* {selectedCategory === "referral" ? (
                              <>
                                <div className="choose_options w-100 text-center d-table mt-3">
                                  <button
                                    onClick={() =>
                                      handleOwnership("conventional")
                                    }
                                    className="btn btn-primary rounded-4 m-2"
                                  >
                                    Conventional
                                  </button>

                                  <button
                                    onClick={() =>
                                      handleOwnership("newConstruction")
                                    }
                                    className="btn btn-primary rounded-4 m-2"
                                  >
                                    New Construction
                                  </button>

                                  <button
                                    onClick={() =>
                                      handleOwnership("corporation")
                                    }
                                    className="btn btn-primary rounded-4 m-2"
                                  >
                                    Corporation
                                  </button>

                                  <button
                                    onClick={() => handleOwnership("trust")}
                                    className="btn btn-primary rounded-4 m-2"
                                  >
                                    Trust
                                  </button>

                                  <button
                                    onClick={() => handleOwnership("probate")}
                                    className="btn btn-primary rounded-4 m-2"
                                  >
                                    Probate
                                  </button>

                                  <button
                                    onClick={() =>
                                      handleOwnership("foreclosure")
                                    }
                                    className="btn btn-primary rounded-4 m-2"
                                  >
                                    Foreclosure/REO
                                  </button>

                                  <button
                                    onClick={() => handleOwnership("hud")}
                                    className="btn btn-primary rounded-4 m-2"
                                  >
                                    HUD
                                  </button>

                                  <button
                                    onClick={() => handleOwnership("shortSale")}
                                    className="btn btn-primary rounded-4 m-2"
                                  >
                                    Short Sale
                                  </button>
                                </div>
                              </>
                            ) : (
                              ""
                            )} */}
                      </div>
                      <div className="float-left w-100 text-center">
                        <button
                          type="button"
                          className="btn btn-primary btn-sm"
                          onClick={prevStep}
                        >
                          Previous
                        </button>
                      </div>
                      {/* <button type="button" className="btn btn-primary btn-sm ms-3" onClick={nextStep}>
                          Next
                        </button> */}
                    </fieldset>

                    {/*  Step 4 */}
                    <fieldset id="step4" className={step === 4 ? "show" : ""}>
                      <div className="col-sm-12 mb-3">
                        <h6 className="text-center">
                          What phase is this transaction in?
                        </h6>
                        <div className="choose_options w-100 text-center d-table mt-3">
                          <>
                            <button
                              onClick={() => handlePhase("pre-listed")}
                              className="btn btn-primary rounded-4 m-2"
                            >
                              Pre-Listed
                            </button>
                            <button
                              onClick={() => handlePhase("active-listing")}
                              className="btn btn-primary rounded-4 m-2"
                            >
                              Active Listing
                            </button>

                            <button
                              onClick={() => handlePhase("showing-homes")}
                              className="btn btn-primary rounded-4 m-2"
                            >
                              Showing homes
                            </button>

                            <button
                              onClick={() => handlePhase("under-contract")}
                              className="btn btn-primary rounded-4 m-2"
                            >
                              Under Contract
                            </button>
                            <button
                              onClick={() => handlePhase("closed")}
                              className="btn btn-primary rounded-4 m-2"
                            >
                              Closed
                            </button>

                            <button
                              onClick={() => handlePhase("canceled")}
                              className="btn btn-primary rounded-4 m-2"
                            >
                              Canceled
                            </button>
                          </>
                        </div>
                      </div>
                      <div className="float-left w-100 text-center">
                        <button
                          type="button"
                          className="btn btn-primary btn-sm"
                          onClick={prevStep}
                        >
                          Previous
                        </button>
                      </div>
                      {/* <div className="float-left w-100 text-center">
                            <button
                              type="button"
                              className="btn btn-primary btn-sm"
                              onClick={prevStep}
                            >
                              Previous
                            </button>
                          </div> */}
                    </fieldset>

                    {/* Step 5 */}
                    <fieldset
                      id="step5"
                      className={step === 5 ? "contact_list show" : ""}
                    >
                      <div className="col-sm-12 mb-3 mh-100">
                        {selectedCategory === "referral" ? (
                          <>
                            <h6 className="text-center">
                              Who is the agent you are referring to?
                            </h6>
                          </>
                        ) : (
                          <h6 className="text-center">
                            Clients info
                          </h6>
                        )}

                        <div className="choose_options bg-light border rounded-3 p-3">
                          <div className="">
                            <div className="row">
                              {/* select a contact start*/}
                              {select ? (
                                <>
                                  <div className="col-md-6 w-100">
                                    <div className="card bg-secondary mt-0">
                                      {select.represent ? (
                                        <h6>{select.represent}</h6>
                                      ) : (
                                        ""
                                      )}
                                      <div className="card-body">
                                        <img
                                          className="rounded-circle pull-right"
                                          onClick={cancel}
                                          src="https://cdn.pboffice.net/shared/transaction/page_select_drop.png"
                                        />
                                        <div className="view_profile">
                                          <img
                                            className="rounded-circle pull-left"
                                            src={
                                              select.image ||
                                              "https://integra.backagent.net/common/handler/pub/photo.ashx?V=1703c0890a0462fd68520b884a04f86f.0&C=people&S=2&P=%2f!Default%2fThumb%2fphoto_pending.jpg"
                                            }
                                            alt="Profile"
                                          />

                                          <span>
                                            <strong>
                                              <a
                                                href="#"
                                                className="card-title mt-0"
                                              >
                                                {select.firstName}&nbsp;
                                                {select.lastName}
                                              </a>
                                            </strong>
                                            <p className="mb-0">
                                              {select.company}
                                              <br />
                                              {select.phone}
                                              <br />
                                            </p>
                                          </span>
                                        </div>
                                      </div>
                                    </div>
                                    <div className="pull-right mt-3">
                                      <button
                                        type="button"
                                        className="btn btn-info btn-sm"
                                        onClick={(e) =>
                                          selectContact(select._id)
                                        }
                                      >
                                        Select Contact
                                      </button>
                                      <button
                                        type="button"
                                        className="btn btn-secondary btn-sm ms-2"
                                        id="closeModal"
                                        data-dismiss="modal"
                                        onClick={cancel}
                                      >
                                        Cancel & Close
                                      </button>
                                    </div>
                                  </div>
                                </>
                              ) : (
                                <>
                                  {contactAdd ? (
                                    <>
                                      {summary ? (
                                        <div className="col-sm-12 mb-3">
                                          <label className="col-form-label pt-0 pb-1">
                                            Selected Contact
                                          </label>
                                          <div className="card  col-md-12 mt-0 mb-3">
                                            <div className="card-body">
                                              <div className="view_profile">
                                                <img
                                                  className="rounded-circle pull-right"
                                                  onClick={handleRemoveContact}
                                                  src="https://cdn.pboffice.net/shared/transaction/page_select_drop.png"
                                                />
                                                <img
                                                  className="pull-left"
                                                  src={
                                                    summary.image ||
                                                    "https://integra.backagent.net/common/handler/pub/photo.ashx?V=1703c0890a0462fd68520b884a04f86f.0&C=people&S=2&P=%2f!Default%2fThumb%2fphoto_pending.jpg"
                                                  }
                                                  alt="Profile"
                                                />
                                                <small>
                                                  <span key={summary._id}>
                                                    <a className="card-title mt-0">
                                                      {summary.firstName}
                                                      &nbsp;
                                                      {summary.lastName}
                                                    </a>
                                                    {summary.company ? (
                                                      <p>{summary.company}</p>
                                                    ) : (
                                                      ""
                                                    )}

                                                    {summary.phone ? (
                                                      <p>{summary.phone}</p>
                                                    ) : (
                                                      ""
                                                    )}

                                                    {summary.corporation_name ? (
                                                      <p>
                                                        {
                                                          summary.corporation_name
                                                        }
                                                      </p>
                                                    ) : (
                                                      ""
                                                    )}
                                                    {summary.trust_name ? (
                                                      <p>
                                                        {summary.trust_name}
                                                      </p>
                                                    ) : (
                                                      ""
                                                    )}
                                                  </span>
                                                </small>

                                                <button
                                                  type="button"
                                                  className="btn btn-info btn-sm"
                                                  onClick={(e) =>
                                                    selectContactAdd(
                                                      summary._id
                                                    )
                                                  }
                                                >
                                                  Select Contact
                                                </button>
                                              </div>
                                            </div>
                                          </div>
                                        </div>
                                      ) : (
                                        <div className="col-md-12">
                                          <div className="col-md-12">
                                            <h6 className="text-center">
                                              Add New Contact
                                            </h6>
                                          </div>

                                          <form onSubmit={handleSearch}>
                                            <div className="row">
                                              <div className="col-md-6 mb-3">
                                                <label className="col-form-label pt-0 pb-1">
                                                  First Name
                                                </label>

                                                <input
                                                  className="form-control border-0 rounded-0 border-bottom"
                                                  type="text"
                                                  id="text-input"
                                                  name="firstName"
                                                  placeholder="Enter your first name"
                                                  onChange={(event) =>
                                                    handleChange(event)
                                                  }
                                                  autoComplete="on"
                                                  style={{
                                                    border:
                                                      formErrors?.firstName
                                                        ? "1px solid red"
                                                        : "1px solid #00000026",
                                                  }}
                                                  value={formValues.firstName}
                                                />
                                                <div className="invalid-tooltip">
                                                  {formErrors.firstName}
                                                </div>
                                              </div>

                                              <div className="col-md-6 mb-3">
                                                <label className="col-form-label pt-0 pb-1">
                                                  Last Name
                                                </label>
                                                <input
                                                  className="form-control border-0 rounded-0 border-bottom"
                                                  type="text"
                                                  id="text-input"
                                                  name="lastName"
                                                  placeholder="Enter your last name"
                                                  onChange={(event) =>
                                                    handleChange(event)
                                                  }
                                                  autoComplete="on"
                                                  style={{
                                                    border: formErrors?.lastName
                                                      ? "1px solid red"
                                                      : "1px solid #00000026",
                                                  }}
                                                  value={formValues.lastName}
                                                />
                                                <div className="invalid-tooltip">
                                                  {formErrors.lastName}
                                                </div>
                                              </div>

                                              {/* <div className="col-md-6 mb-3">
                                                <label className="col-form-label pt-0 pb-1">
                                                  Company
                                                </label>
                                                <input
                                                  className="form-control border-0 rounded-0 border-bottom"
                                                  type="text"
                                                  id="text-input"
                                                  name="company"
                                                  placeholder="Enter your company"
                                                  onChange={(event) =>
                                                    handleChange(event)
                                                  }
                                                  autoComplete="on"
                                                  value={formValues.company}
                                                />
                                              </div> */}

                                              <div className="col-md-6 mb-3">
                                                <label className="col-form-label pt-0 pb-1">
                                                  Email
                                                </label>
                                                <input
                                                  className="form-control border-0 rounded-0 border-bottom"
                                                  id="email-input"
                                                  type="text"
                                                  name="email"
                                                  placeholder="Enter your email"
                                                  onChange={(event) =>
                                                    handleChange(event)
                                                  }
                                                  autoComplete="on"
                                                  style={{
                                                    border: formErrors?.email
                                                      ? "1px solid red"
                                                      : "1px solid #00000026",
                                                  }}
                                                  value={formValues.email}
                                                />
                                                <div className="invalid-tooltip">
                                                  {formErrors.email}
                                                </div>
                                              </div>

                                              <div className="col-md-6 mb-2">
                                                <label className="col-form-label pt-0 pb-1">
                                                  Phone
                                                </label>
                                                <input
                                                  className="form-control border-0 rounded-0 border-bottom"
                                                  type="tel"
                                                  id="tel-input"
                                                  name="phone"
                                                  placeholder="Enter your phone"
                                                  onChange={(event) =>
                                                    handleChange(event)
                                                  }
                                                  autoComplete="on"
                                                  pattern="[0-9]+"
                                                  required
                                                  title="Please enter a valid phone number"
                                                  value={formValues.phone}
                                                />
                                              </div>

                                              <div className="col-md-6 mb-2">
                                                <div className="d-flex">
                                                  <div className="form-check ps-3">
                                                    <input
                                                      className="form-check-input"
                                                      id="form-radio-4"
                                                      type="radio"
                                                      name="contactEntity"
                                                      value="entity"
                                                      onChange={(e) =>
                                                        CheckRegistrationContact(
                                                          e
                                                        )
                                                      }
                                                      style={{
                                                        border:
                                                          "1px solid black",
                                                      }}
                                                    />
                                                    <label
                                                      className="form-check-label ms-2"
                                                      id="radio-level1"
                                                    >
                                                      Entity/Corporate
                                                    </label>
                                                  </div>

                                                  <div className="form-check">
                                                    <input
                                                      style={{
                                                        border:
                                                          "1px solid black",
                                                      }}
                                                      className="form-check-input "
                                                      id="form-radio-4"
                                                      type="radio"
                                                      name="contactEntity"
                                                      value="trust"
                                                      onChange={(e) =>
                                                        CheckRegistrationContact(
                                                          e
                                                        )
                                                      }
                                                    />
                                                    <label
                                                      className="form-check-label ms-2"
                                                      id="radio-level1"
                                                    >
                                                      Trust
                                                    </label>
                                                  </div>
                                                </div>
                                              </div>

                                              {checkvalueContact ===
                                              "entity" ? (
                                                <div className="col-md-6 mb-3">
                                                  <label className="col-form-label pt-0 pb-1">
                                                    Entity/Corporation Name
                                                  </label>
                                                  <input
                                                    className="form-control border-0 rounded-0 border-bottom"
                                                    type="text"
                                                    id="text-input"
                                                    name="corporation_name"
                                                    onChange={(event) =>
                                                      handleChange(event)
                                                    }
                                                    autoComplete="on"
                                                    value={
                                                      formValues.corporation_name
                                                    }
                                                    style={{
                                                      border:
                                                        formErrors?.corporation_name
                                                          ? "1px solid red"
                                                          : "1px solid #00000026",
                                                    }}
                                                  />
                                                  <div className="invalid-tooltip">
                                                    {
                                                      formErrors.corporation_name
                                                    }
                                                  </div>
                                                </div>
                                              ) : (
                                                ""
                                              )}

                                              {checkvalueContact === "trust" ? (
                                                <div className="col-md-6 mb-3">
                                                  <label className="col-form-label pt-0 pb-1">
                                                    Trust Name
                                                  </label>
                                                  <input
                                                    className="form-control border-0 rounded-0 border-bottom"
                                                    type="text"
                                                    id="text-input"
                                                    name="trust_name"
                                                    onChange={(event) =>
                                                      handleChange(event)
                                                    }
                                                    autoComplete="on"
                                                    value={
                                                      formValues.trust_name
                                                    }
                                                    style={{
                                                      border:
                                                        formErrors?.trust_name
                                                          ? "1px solid red"
                                                          : "1px solid #00000026",
                                                    }}
                                                  />
                                                  <div className="invalid-tooltip">
                                                    {formErrors.trust_name}
                                                  </div>
                                                </div>
                                              ) : (
                                                ""
                                              )}

                                              {/* <div className="col-md-6 mb-2">
                                                <label className="col-form-label pt-0 pb-1">
                                                  Contact Type
                                                </label>
                                                <select
                                                  className="form-select border-0 rounded-0 border-bottom"
                                                  name="contactType"
                                                  value={formValues.contactType}
                                                  onChange={handleChange}
                                                  style={{
                                                    border:
                                                      formErrors?.contactType
                                                        ? "1px solid red"
                                                        : "1px solid #00000026",
                                                  }}
                                                >
                                                  <option value="private">
                                                    Private
                                                  </option>
                                                  {localStorage.getItem(
                                                    "auth"
                                                  ) &&
                                                  jwt(
                                                    localStorage.getItem("auth")
                                                  ).contactType == "admin" ? (
                                                    <option value="associate">
                                                      Associate
                                                    </option>
                                                  ) : (
                                                    ""
                                                  )}
                                                </select>
                                                <div className="invalid-tooltip">
                                                  {formErrors.contactType}
                                                </div>
                                              </div> */}

                                              <div className="col-md-12 mb-3">
                                                <div className="form-check">
                                                  <input
                                                    className="form-check-input"
                                                    name="checkbox"
                                                    type="checkbox"
                                                    id="agree-to-terms"
                                                    onClick={handleCheck}
                                                    value={checkBox.checkBox}
                                                  />
                                                  <div className="invalid-tooltip">
                                                    {checkBox.checkBox}
                                                  </div>
                                                  <label className="form-check-label">
                                                    <small>
                                                      No email address
                                                      available.
                                                    </small>
                                                  </label>
                                                </div>
                                              </div>

                                              <div className="col-md-12">
                                                <div className="pull-right">
                                                  <button
                                                    type="button"
                                                    className="btn btn-info btn-sm"
                                                    id="save-button"
                                                    onClick={handleNewContact}
                                                  >
                                                    Add New Contact
                                                  </button>
                                                  <button
                                                    type="button"
                                                    className="btn btn-secondary btn-sm ms-2"
                                                    onClick={handleCancel}
                                                  >
                                                    Cancel & Close
                                                  </button>
                                                </div>
                                              </div>
                                            </div>
                                          </form>
                                        </div>
                                      )}
                                    </>
                                  ) : (
                                    <div className="col-md-12">
                                      <div className="col-md-12">
                                        <h6 className="text-center">
                                          Select a Contact
                                        </h6>
                                      </div>
                                      <div className="col-md-12">
                                        <form className="form-group shadow-none border-0 rounded-0 border-bottom mb-2">
                                          <div className="input-group input-group-sm p-0">
                                            <span className="input-group-text text-muted p-0">
                                              <i className="fi-search"></i>
                                            </span>
                                            <input
                                              type="text"
                                              className="form-control"
                                              placeholder="Search a Contact..."
                                              name="firstName"
                                              value={formValues.firstName}
                                              onChange={handleChange}
                                            />
                                          </div>
                                        </form>
                                      </div>
                                      {isLoading ? (
                                        <p>Loading...</p>
                                      ) : (
                                        results.map((post) => {
                                          return (
                                            <>
                                              <div className="col-md-12">
                                                <ul
                                                  className="list-group"
                                                  onClick={(e) =>
                                                    handleSearchTransaction(
                                                      post._id
                                                    )
                                                  }
                                                >
                                                  <li className="list-group-item d-flex justify-content-start align-items-center mt-0 mb-2">
                                                    <img
                                                      className="rounded-circle avtar"
                                                      src={post.image || avtar}
                                                      alt="Profile"
                                                    />
                                                    <span className="float-left text-left ms-2">
                                                      {" "}
                                                      <strong>
                                                        {post.firstName}
                                                        {post.lastName}
                                                      </strong>
                                                      <p className="fs-sm text-muted mb-0">
                                                        {" "}
                                                        {post.company}
                                                        {post.phone}
                                                      </p>
                                                    </span>
                                                  </li>
                                                </ul>
                                              </div>
                                            </>
                                          );
                                        })
                                      )}
                                      <div className="pagination justify-content-center mb-0 mt-3">
                                        <ReactPaginate
                                          previousLabel={"Previous"}
                                          nextLabel={"next"}
                                          breakLabel={"..."}
                                          pageCount={pageCount}
                                          marginPagesDisplayed={1}
                                          pageRangeDisplayed={2}
                                          onPageChange={handlePageClick}
                                          containerClassName={
                                            "pagination justify-content-center mb-2"
                                          }
                                          pageClassName={"page-item"}
                                          pageLinkClassName={"page-link"}
                                          previousClassName={"page-item"}
                                          previousLinkClassName={"page-link"}
                                          nextClassName={"page-item"}
                                          nextLinkClassName={"page-link"}
                                          breakClassName={"page-item"}
                                          breakLinkClassName={"page-link"}
                                          activeClassName={"active"}
                                        />
                                      </div>
                                      <div className="social-login">
                                        <div className="float-left w-100 mb-0">
                                          <div className="mt-0">
                                            <h4 className="or-border">
                                              <span>or</span>
                                            </h4>
                                          </div>
                                        </div>
                                      </div>
                                      <div className="col-md-12 text-center mt-3">
                                        <NavLink
                                          onClick={handleAdd}
                                          className="btn btn-info btn-sm"
                                        >
                                          Add a Contact
                                        </NavLink>
                                      </div>
                                    </div>
                                  )}
                                </>
                              )}
                              {/* select a contact end*/}
                            </div>
                          </div>
                        </div>
                      </div>
                      {/* <div className="float-left w-100 text-center">
                            <button
                              type="button"
                              className="btn btn-primary btn-sm"
                              onClick={prevStep}
                            >
                              Previous
                            </button>
                          </div> */}
                    </fieldset>

                    {/* Step 6 */}
                    <fieldset
                      id="step6"
                      className={step === 6 ? "contact_list show" : ""}
                    >
                      {clientdone !== false ? (
                        <>
                          {/* select property section  */}

                          <div className="col-sm-12 mb-3 mh-100">
                            <div className="choose_options bg-light border rounded-3 p-3">
                              {checkReferralNew === "no" ? (
                                <>
                                  <div id="accordionCards" className="">
                                    <div className="card border-0 mb-2 mt-0">
                                      <div
                                        className="collapse show"
                                        id="cardCollapse1"
                                        data-bs-parent="#accordionCards"
                                      >
                                        <div className="card-body mt-n1 p-0">
                                          <ul className="list-unstyled mb-2">
                                            <div className="row">
                                              <div className="col-md-6">
                                                <small>
                                                  <li className="d-flex align-items-center">
                                                    {summary ? (
                                                      <div
                                                        className="col-sm-12"
                                                        key={summary?._id}
                                                      >
                                                        <i className="fi-check text-success me-2"></i>
                                                        <b className="ps-1">
                                                          Referral info
                                                        </b>
                                                        <div className="card select_contact col-md-12 my-2">
                                                          <div className="card-body p-2">
                                                            <div className="view_profile">
                                                              <img
                                                                className="pull-left"
                                                                src={
                                                                  summary?.image ||
                                                                  "https://integra.backagent.net/common/handler/pub/photo.ashx?V=1703c0890a0462fd68520b884a04f86f.0&C=people&S=2&P=%2f!Default%2fThumb%2fphoto_pending.jpg"
                                                                }
                                                                alt="Profile"
                                                              />
                                                              <span>
                                                                <small>
                                                                  <a className="card-title mt-0">
                                                                    {
                                                                      summary?.firstName
                                                                    }
                                                                    &nbsp;
                                                                    {
                                                                      summary?.lastName
                                                                    }
                                                                  </a>
                                                                  <p className="mb-0">
                                                                    {
                                                                      summary?.company
                                                                    }
                                                                    <br />
                                                                    {
                                                                      summary?.phone
                                                                    }
                                                                    {/* <br /> */}
                                                                  </p>
                                                                </small>
                                                              </span>
                                                            </div>
                                                          </div>
                                                        </div>
                                                      </div>
                                                    ) : (
                                                      <>
                                                        <a
                                                          className="nav-link fw-normal ps-1 p-0"
                                                          href="#basic-info"
                                                          data-scroll=""
                                                          data-scroll-offset="20"
                                                        >
                                                          Contacts:
                                                          <p>
                                                            {select.firstName}
                                                          </p>
                                                        </a>

                                                        {}
                                                      </>
                                                    )}
                                                  </li>
                                                </small>
                                              </div>
                                              <div className="col-md-6">
                                                <small>
                                                  <li className="d-flex align-items-center mb-1">
                                                    {selectedCategory ? (
                                                      <>
                                                        <i className="fi-check text-success me-2"></i>

                                                        <a
                                                          className="nav-link fw-normal ps-1 p-0"
                                                          href="#basic-info"
                                                          data-scroll=""
                                                          data-scroll-offset="20"
                                                        >
                                                          {selectedCategory ===
                                                          "buyer" ? (
                                                            <>
                                                              <b>Category</b>:
                                                              &nbsp;
                                                              {selectedCategory ===
                                                              "buyer"
                                                                ? "Buyer"
                                                                : ""}
                                                            </>
                                                          ) : (
                                                            ""
                                                          )}

                                                          {selectedCategory ===
                                                          "seller" ? (
                                                            <>
                                                              <b>Category</b>:
                                                              &nbsp;
                                                              {selectedCategory ===
                                                              "seller"
                                                                ? "Seller"
                                                                : ""}
                                                            </>
                                                          ) : (
                                                            ""
                                                          )}

                                                          {selectedCategory ===
                                                          "both" ? (
                                                            <>
                                                              <b>Category</b>:
                                                              &nbsp;
                                                              {selectedCategory ===
                                                              "both"
                                                                ? "Buyer & Seller"
                                                                : ""}
                                                            </>
                                                          ) : (
                                                            ""
                                                          )}
                                                          {selectedCategory ===
                                                          "referral" ? (
                                                            <>
                                                              <b>Category</b>:
                                                              &nbsp;
                                                              {selectedCategory ===
                                                              "referral"
                                                                ? "Referral"
                                                                : ""}
                                                            </>
                                                          ) : (
                                                            ""
                                                          )}
                                                        </a>
                                                      </>
                                                    ) : (
                                                      <>
                                                        <img src="https://cdn.pboffice.net/shared/transaction/page_select_drop.png" />
                                                        <a
                                                          className="nav-link fw-normal ps-1 p-0"
                                                          href="#basic-info"
                                                          data-scroll=""
                                                          data-scroll-offset="20"
                                                        >
                                                          Category
                                                        </a>
                                                      </>
                                                    )}
                                                  </li>
                                                </small>

                                                <small>
                                                  <li className="d-flex align-items-center mb-2">
                                                    {selectedType ? (
                                                      <>
                                                        <i className="fi-check text-success me-2"></i>
                                                        <a
                                                          className="nav-link fw-normal ps-1 p-0"
                                                          href="#basic-info"
                                                          data-scroll=""
                                                          data-scroll-offset="20"
                                                        >
                                                          {selectedCategory ===
                                                          "buyer" ? (
                                                            <>
                                                              <b>Type</b>:
                                                              &nbsp;
                                                              {selectedType ===
                                                              "buy_residential"
                                                                ? "Buyer Residential"
                                                                : ""}
                                                              {selectedType ===
                                                              "buy_commercial"
                                                                ? "Buyer Commercial"
                                                                : ""}
                                                              {selectedType ===
                                                              "buy_investment"
                                                                ? "Buyer Investment"
                                                                : ""}
                                                              {selectedType ===
                                                              "buy_land"
                                                                ? "Buyer Land"
                                                                : ""}
                                                              {selectedType ===
                                                              "buy_referral"
                                                                ? "Buyer Referral"
                                                                : ""}
                                                              {selectedType ===
                                                              "buy_lease"
                                                                ? "Buyer Lease"
                                                                : ""}
                                                            </>
                                                          ) : (
                                                            ""
                                                          )}

                                                          {selectedCategory ===
                                                          "seller" ? (
                                                            <>
                                                              <b>Type</b>:
                                                              &nbsp;
                                                              {selectedType ===
                                                              "sale_residential"
                                                                ? "Seller Residential"
                                                                : ""}
                                                              {selectedType ===
                                                              "sale_commercial"
                                                                ? "Seller Commercial"
                                                                : ""}
                                                              {selectedType ===
                                                              "sale_investment"
                                                                ? "Seller Investment"
                                                                : ""}
                                                              {/* {selectedType ===
                                                          "sale_mobile"
                                                            ? "Seller Mobile"
                                                            : ""} */}
                                                              {/* {selectedType ===
                                                          "sale_land"
                                                            ? "Seller Land"
                                                            : ""} */}
                                                              {selectedType ===
                                                              "sale_referral"
                                                                ? "Seller Referral"
                                                                : ""}
                                                              {selectedType ===
                                                              "sale_lease"
                                                                ? "Seller Lease"
                                                                : ""}
                                                            </>
                                                          ) : (
                                                            ""
                                                          )}

                                                          {selectedCategory ===
                                                          "both" ? (
                                                            <>
                                                              <b>Type</b>:
                                                              &nbsp;
                                                              {selectedType ===
                                                              "both_residential"
                                                                ? "Buyer & Seller Residential"
                                                                : ""}
                                                              {selectedType ===
                                                              "both_commercial"
                                                                ? "Buyer & Seller Commercial"
                                                                : ""}
                                                              {selectedType ===
                                                              "both_investment"
                                                                ? "Buyer & Seller Investment"
                                                                : ""}
                                                              {selectedType ===
                                                              "both_land"
                                                                ? "Buyer & Seller Land"
                                                                : ""}
                                                              {selectedType ===
                                                              "both_referral"
                                                                ? "Buyer & Seller Referral"
                                                                : ""}
                                                              {selectedType ===
                                                              "both_lease"
                                                                ? "Buyer & Seller Lease"
                                                                : ""}
                                                            </>
                                                          ) : (
                                                            ""
                                                          )}

                                                          {selectedCategory ===
                                                          "referral" ? (
                                                            <>
                                                              <b>Type</b>:
                                                              &nbsp;
                                                              {selectedType ===
                                                              "referral_residential"
                                                                ? "Residential Referral"
                                                                : ""}
                                                              {selectedType ===
                                                              "referral_commercial"
                                                                ? "Commercial Referral"
                                                                : ""}
                                                              {selectedType ===
                                                              "referral_investment"
                                                                ? "Investment Referral"
                                                                : ""}
                                                              {selectedType ===
                                                              "referral_land"
                                                                ? "Land Referral"
                                                                : ""}
                                                              {selectedType ===
                                                              "referral_lease"
                                                                ? "Lease Referral"
                                                                : ""}
                                                            </>
                                                          ) : (
                                                            ""
                                                          )}
                                                        </a>
                                                      </>
                                                    ) : (
                                                      <>
                                                        <img src="https://cdn.pboffice.net/shared/transaction/page_select_drop.png" />
                                                        <a
                                                          className="nav-link fw-normal ps-1 p-0"
                                                          href="#basic-info"
                                                          data-scroll=""
                                                          data-scroll-offset="20"
                                                        >
                                                          Type
                                                        </a>
                                                      </>
                                                    )}
                                                  </li>
                                                </small>

                                                <small>
                                                  <li className="d-flex align-items-center mb-1">
                                                    {selectedOwner ? (
                                                      <>
                                                        <i className="fi-check text-success me-2"></i>
                                                        <a
                                                          className="nav-link fw-normal ps-1 p-0"
                                                          href="#details"
                                                          data-scroll=""
                                                          data-scroll-offset="20"
                                                        >
                                                          {selectedOwner ? (
                                                            <>
                                                              <b>Ownership</b>:
                                                              &nbsp;
                                                              {selectedOwner ===
                                                              "individual"
                                                                ? "Individual"
                                                                : ""}
                                                              {selectedOwner ===
                                                              "corporation"
                                                                ? "Entity/Corporation"
                                                                : ""}
                                                              {selectedOwner ===
                                                              "trust"
                                                                ? "Trust"
                                                                : ""}
                                                            </>
                                                          ) : (
                                                            ""
                                                          )}
                                                        </a>
                                                      </>
                                                    ) : (
                                                      <>
                                                        <img
                                                          src="https://cdn.pboffice.net/shared/transaction/page_select_drop.png"
                                                          alt="Select Drop"
                                                        />
                                                        <a
                                                          className="nav-link fw-normal ps-1 p-0"
                                                          href="#basic-info"
                                                          data-scroll=""
                                                          data-scroll-offset="20"
                                                        >
                                                          Ownership
                                                        </a>
                                                      </>
                                                    )}
                                                  </li>
                                                </small>

                                                <small>
                                                  <li className="d-flex align-items-center mb-1">
                                                    {selectedPhase ? (
                                                      <>
                                                        <i className="fi-check text-success me-2"></i>

                                                        <a
                                                          className="nav-link fw-normal ps-1 p-0"
                                                          href="#basic-info"
                                                          data-scroll=""
                                                          data-scroll-offset="20"
                                                        >
                                                          {selectedPhase ? (
                                                            <>
                                                              <b>
                                                                Selected Phase
                                                              </b>
                                                              : &nbsp;
                                                              {selectedPhase ===
                                                              "pre-listed"
                                                                ? "Pre-Listed"
                                                                : ""}
                                                              {selectedPhase ===
                                                              "active-listing"
                                                                ? "Active Listing"
                                                                : ""}
                                                              {selectedPhase ===
                                                              "showing-homes"
                                                                ? "Showing homes"
                                                                : ""}
                                                              {selectedPhase ===
                                                              "under-contract"
                                                                ? "Under Contract"
                                                                : ""}
                                                              {selectedPhase ===
                                                              "closed"
                                                                ? "Closed"
                                                                : ""}
                                                              {selectedPhase ===
                                                              "canceled"
                                                                ? "Canceled"
                                                                : ""}
                                                            </>
                                                          ) : (
                                                            ""
                                                          )}
                                                        </a>
                                                      </>
                                                    ) : (
                                                      <>
                                                        <img src="https://cdn.pboffice.net/shared/transaction/page_select_drop.png" />
                                                        <a
                                                          className="nav-link fw-normal ps-1 p-0"
                                                          href="#basic-info"
                                                          data-scroll=""
                                                          data-scroll-offset="20"
                                                        >
                                                          Phase
                                                        </a>
                                                      </>
                                                    )}
                                                  </li>
                                                </small>
                                              </div>
                                            </div>
                                          </ul>
                                        </div>
                                      </div>
                                    </div>

                                    <>
                                      <b>
                                        How would you like to name your file?
                                      </b>
                                      <div className="mb-2">
                                        <label className="form-label">
                                          File name
                                        </label>
                                        <br />
                                        <input
                                          className="form-control"
                                          type="text"
                                          id="text-input"
                                          name="filename"
                                          placeholder="Enter your file name"
                                          onChange={(event) =>
                                            handleChanges(event)
                                          }
                                          autoComplete="on"
                                          style={{
                                            border: formErrors?.filename
                                              ? "1px solid red"
                                              : "1px solid #00000026",
                                          }}
                                          value={formValuesNew.filename}
                                        />
                                        <div className="invalid-tooltip">
                                          {formErrors.filename}
                                        </div>
                                      </div>

                                      <div
                                        className="col-sm-6"
                                        onClick={(e) => CheckRegistration(e)}
                                      >
                                        <div className="d-flex">
                                          <div className="form-check ps-3">
                                            <input
                                              className="form-check-input"
                                              id="form-radio-4"
                                              type="radio"
                                              name="registration"
                                              value="address"
                                              onChange={(e) =>
                                                CheckRegistration(e)
                                              }
                                              style={{
                                                border: "1px solid black",
                                              }}
                                            />
                                            <label
                                              className="form-check-label ms-2"
                                              id="radio-level1"
                                            >
                                              Address
                                            </label>
                                          </div>

                                          <div className="form-check">
                                            <input
                                              style={{
                                                border: "1px solid black",
                                              }}
                                              className="form-check-input "
                                              id="form-radio-4"
                                              type="radio"
                                              name="registration"
                                              value="primary_client"
                                              onChange={(e) =>
                                                CheckRegistration(e)
                                              }
                                            />
                                            <label
                                              className="form-check-label ms-2"
                                              id="radio-level1"
                                            >
                                              Primary Client
                                            </label>
                                          </div>
                                        </div>
                                      </div>
                                      <div className="float-left w-100 mt-1 text-center">
                                        <button
                                          id="type-value"
                                          className="btn btn-info btn-sm"
                                          onClick={AddTransaction}
                                        >
                                          Create New Transaction
                                          {/* {isSubmit
                                        ? "Please wait..."
                                        : "Create New Transaction"}{" "} */}
                                        </button>
                                      </div>
                                    </>
                                  </div>
                                </>
                              ) : (
                                <div>

                                  {step1 === "" && step2 === "" ? (
                                    <div>
                                      {selectedCategory === "referral" ? (
                                        <>
                                          {checkReferralNew === "yes" ? (
                                            <>
                                              {propertyId ? (
                                                <></>
                                              ) : (
                                                <div className="">
                                                  {/* <div className="form-check">
                                                                <input className="form-check-input" id="form-check-2" type="checkbox" checked />
                                                                <label id="form-check-label-one">
                                                                 <small>You selected the <b>{searchby === "mls" ? 'MLS Listing' : 'My Listing' }</b> property entry option.
                                                                    <NavLink onClick={Cancel}>&nbsp;&nbsp;Change Option</NavLink></small>
                                                                </label>
                                                            </div> */}
                                                  <ul
                                                    className="nav nav-tabs d-flex align-items-center align-items-sm-center justify-content-center mt-0 mb-3"
                                                    role="tablist"
                                                  >
                                                    <li
                                                      className="nav-item me-sm-3"
                                                      role="presentation"
                                                    >
                                                      <a
                                                        className="nav-link text-center active"
                                                        href="#tab"
                                                        data-bs-toggle="tab"
                                                        role="tab"
                                                        aria-controls="reviews-about-you"
                                                        aria-selected="true"
                                                        onClick={(e) =>
                                                          Searchbyfunction(
                                                            e,
                                                            "mls"
                                                          )
                                                        }
                                                      >
                                                        Search By MLS Number
                                                      </a>
                                                    </li>
                                                    <li
                                                      className="nav-item"
                                                      role="presentation"
                                                    >
                                                      <a
                                                        className="nav-link text-center"
                                                        href="#tab2"
                                                        data-bs-toggle="tab"
                                                        role="tab"
                                                        aria-controls="reviews-by-you"
                                                        aria-selected="false"
                                                        tabindex="-1"
                                                        onClick={(e) =>
                                                          Searchbyfunction(
                                                            e,
                                                            "mylisting"
                                                          )
                                                        }
                                                      >
                                                        Show My Listing
                                                      </a>
                                                    </li>
                                                  </ul>

                                                  {searchby === "mls" ? (
                                                    <>
                                                      {membership ? (
                                                        <>
                                                          <label className="col-form-label fs-sm pt-0 mt-3">
                                                            Enter a MLS Number
                                                          </label>
                                                          <div className="col-md-12">
                                                            <div className="d-flex align-items-center justify-content-between">
                                                              <input
                                                                className="form-control w-100 border-0 rounded-0 border-bottom"
                                                                id="text-input-onee"
                                                                type="text"
                                                                name="mlsnumber"
                                                                placeholder="Enter MLS number"
                                                                onChange={
                                                                  handleChangesearchmls
                                                                }
                                                                value={
                                                                  searchmlsnumber.mlsnumber
                                                                }
                                                              />
                                                              <div className="pull-right ms-3">
                                                                <a
                                                                  className="btn btn-info btn-sm order-lg-3"
                                                                  onClick={
                                                                    MLSSearch
                                                                  }
                                                                  disabled={
                                                                    isLoading
                                                                  }
                                                                >
                                                                  {isLoading
                                                                    ? "Please wait"
                                                                    : "Search"}
                                                                </a>
                                                              </div>
                                                            </div>
                                                          </div>
                                                        </>
                                                      ) : (
                                                        ""
                                                      )}
                                                      <select
                                                        className="form-select border-0 rounded-0 border-bottom"
                                                        name="membership"
                                                        onChange={(event) =>
                                                          setMembership(
                                                            event.target.value
                                                          )
                                                        }
                                                        value={membership}
                                                      >
                                                        <option value="">
                                                          Please Select
                                                          Membership
                                                        </option>
                                                        {commaSeparatedValuesmls_membership &&
                                                        commaSeparatedValuesmls_membership.length >
                                                          0
                                                          ? commaSeparatedValuesmls_membership.map(
                                                              (memberahip) => (
                                                                <option>
                                                                  {memberahip}
                                                                </option>
                                                              )
                                                            )
                                                          : ""}
                                                      </select>
                                                    </>
                                                  ) : searchby ===
                                                    "mylisting" ? (
                                                    <>
                                                      <h6 className="mb-2">
                                                        Search From Added
                                                        Property Listing:
                                                      </h6>
                                                      <label className="col-form-label fs-sm pt-0">
                                                        Enter a Associate Name:
                                                      </label>
                                                      <div className="col-md-12">
                                                        <form className="form-group shadow-none border-0 rounded-0 border-bottom mb-2 pt-0">
                                                          <div className="input-group input-group-sm p-0">
                                                            <span className="input-group-text text-muted p-0">
                                                              <i className="fi-search"></i>
                                                            </span>
                                                            <input
                                                              type="text"
                                                              className="form-control pb-0"
                                                              placeholder="Enter your associate name..."
                                                              name="associateName"
                                                              onChange={
                                                                handleChangesearch
                                                              }
                                                              value={
                                                                search_value.associateName
                                                              }
                                                            />
                                                            <div className="pull-right">
                                                              <a
                                                                className="btn btn-info btn-sm"
                                                                onClick={
                                                                  propertySearch
                                                                }
                                                                disabled={
                                                                  isLoading
                                                                }
                                                              >
                                                                {isLoading
                                                                  ? "Please wait"
                                                                  : "Search"}
                                                              </a>
                                                            </div>
                                                          </div>
                                                        </form>
                                                      </div>
                                                    </>
                                                  ) : (
                                                    ""
                                                  )}
                                                </div>
                                              )}

                                              {propertyId ? (
                                                <></>
                                              ) : (
                                                <>
                                                  {selectproperty ? (
                                                    <div></div>
                                                  ) : (
                                                    <>
                                                      <div className="social-login mb-2">
                                                        <div className="float-left w-100 mb-0">
                                                          <div className="mt-0">
                                                            <h4 className="or-border">
                                                              <span>or</span>
                                                            </h4>
                                                          </div>
                                                        </div>
                                                      </div>

                                                      <div className="text-center float-left w-100 mb-2">
                                                        <a
                                                          className="btn btn-info btn-sm text-white"
                                                          onClick={() =>
                                                            AddBasic(
                                                              "1",
                                                              "Enter Basic"
                                                            )
                                                          }
                                                        >
                                                          Add a Property
                                                        </a>
                                                      </div>
                                                    </>
                                                  )}
                                                </>
                                              )}
                                            </>
                                          ) : (
                                            <>
                                              <h6 className="text-center">
                                                Do you have an address or MLS #?
                                              </h6>

                                              <div
                                                className="d-flex justify-content-center"
                                                onClick={(e) =>
                                                  handleChangeReferral(e)
                                                }
                                              >
                                                <div className="form-check ps-3">
                                                  <input
                                                    style={{
                                                      border: "1px solid black",
                                                    }}
                                                    className="form-check-input"
                                                    id="form-radio-yes"
                                                    type="radio"
                                                    name="mls"
                                                    value="yes"
                                                  />
                                                  <label
                                                    className="form-check-label ms-2"
                                                    htmlFor="form-radio-yes"
                                                  >
                                                    Yes
                                                  </label>
                                                </div>
                                                <div className="form-check ms-2">
                                                  <input
                                                    style={{
                                                      border: "1px solid black",
                                                    }}
                                                    className="form-check-input"
                                                    id="form-radio-no"
                                                    type="radio"
                                                    name="mls"
                                                    value="no"
                                                  />
                                                  <label
                                                    className="form-check-label ms-2"
                                                    htmlFor="form-radio-no"
                                                  >
                                                    No
                                                  </label>
                                                </div>
                                              </div>
                                            </>
                                          )}
                                        </>
                                      ) : (
                                        <>
                                          {propertyId ? (
                                            <></>
                                          ) : (
                                            <div className="">
                                              <ul
                                                className="nav nav-tabs d-flex align-items-center align-items-sm-center justify-content-center mt-0 mb-5"
                                                role="tablist"
                                              >
                                                <li
                                                  className="nav-item me-sm-3"
                                                  role="presentation"
                                                >
                                                  <a
                                                    className="nav-link text-center active"
                                                    href="#tab"
                                                    data-bs-toggle="tab"
                                                    role="tab"
                                                    aria-controls="reviews-about-you"
                                                    aria-selected="true"
                                                    onClick={(e) =>
                                                      Searchbyfunction(e, "mls")
                                                    }
                                                  >
                                                    Search By MLS Number
                                                  </a>
                                                </li>
                                                <li
                                                  className="nav-item"
                                                  role="presentation"
                                                >
                                                  <a
                                                    className="nav-link text-center"
                                                    href="#tab2"
                                                    data-bs-toggle="tab"
                                                    role="tab"
                                                    aria-controls="reviews-by-you"
                                                    aria-selected="false"
                                                    tabindex="-1"
                                                    onClick={(e) =>
                                                      Searchbyfunction(
                                                        e,
                                                        "mylisting"
                                                      )
                                                    }
                                                  >
                                                    Show My Listing
                                                  </a>
                                                </li>
                                              </ul>
                                              <div className="propertyTransaction m-auto col-lg-6 col-md-6 col-sm-12 col-12">
                                               {
                                                searchby === "mylisting" ?
                                                <></>
                                                :
                                                   <div>
                                                  <h6 className="mt-3 mb-1">
                                                        Autofill With MLS Data
                                                      </h6>
                                                      <p>
                                                        Enter the MLS number to
                                                        search for information
                                                        to autofill the form.
                                                      </p>
                                                  <div className="col-md-12">
                                                  <select
                                                    className="form-select"
                                                    name="membership"
                                                    onChange={(event) =>
                                                      setMembership(
                                                        event.target.value
                                                      )
                                                    }
                                                    value={membership}
                                                  >
                                                    <option value="">
                                                      Please Select Membership
                                                    </option>
                                                    {commaSeparatedValuesmls_membership &&
                                                    commaSeparatedValuesmls_membership.length >
                                                      0
                                                      ? commaSeparatedValuesmls_membership.map(
                                                          (memberahip) => (
                                                            <option>
                                                              {memberahip}
                                                            </option>
                                                          )
                                                        )
                                                      : ""}
                                                  </select>

                                                  </div>
                                                  </div>
                                               }

                                               {searchby === "mls" ? (
                                                <>
                                                  {membership ? (
                                                    <div className="mt-3">
                                                     
                                                      <label className="col-form-label fs-sm pt-0 ms-3">
                                                        MLS Number
                                                      </label>
                                                      <div className="col-md-12">
                                                        <div className="d-flex align-items-center justify-content-between">
                                                          <input
                                                            className="form-control"
                                                            id="text-input-onee"
                                                            type="text"
                                                            name="mlsnumber"
                                                            placeholder="Enter MLS number"
                                                            onChange={
                                                              handleChangesearchmls
                                                            }
                                                            value={
                                                              searchmlsnumber.mlsnumber
                                                            }
                                                          />
                                                          <div className="pull-right ms-3">
                                                            <a
                                                              className="btn btn-info btn-sm order-lg-3"
                                                              onClick={
                                                                MLSSearch
                                                              }
                                                              disabled={
                                                                isLoading
                                                              }
                                                            >
                                                              {isLoading
                                                                ? "Please wait"
                                                                : "Search"}
                                                            </a>
                                                          </div>
                                                        </div>
                                                      </div>
                                                    </div>
                                                  ) : (
                                                    ""
                                                  )}

                                                
                                                </>
                                              ) : searchby === "mylisting" ? (
                                                <>
                                                  <h6 className="mb-2">
                                                    Search From Added Property
                                                    Listing:
                                                  </h6>
                                                  <label className="col-form-label fs-sm pt-0">
                                                    Enter a Associate Name:
                                                  </label>
                                                  <div className="col-md-12">
                                                    <form className="form-group shadow-none border-0 rounded-0 border-bottom mb-2 pt-0">
                                                      <div className="input-group input-group-sm p-0">
                                                        <span className="input-group-text text-muted p-0">
                                                          <i className="fi-search"></i>
                                                        </span>
                                                        <input
                                                          type="text"
                                                          className="form-control pb-0"
                                                          placeholder="Enter your associate name..."
                                                          name="associateName"
                                                          onChange={
                                                            handleChangesearch
                                                          }
                                                          value={
                                                            search_value.associateName
                                                          }
                                                        />
                                                        <div className="pull-right">
                                                          <a
                                                            className="btn btn-info btn-sm"
                                                            onClick={
                                                              propertySearch
                                                            }
                                                            disabled={isLoading}
                                                          >
                                                            {isLoading
                                                              ? "Please wait"
                                                              : "Search"}
                                                          </a>
                                                        </div>
                                                      </div>
                                                    </form>
                                                  </div>
                                                </>
                                              ) : (
                                                ""
                                              )}

                                              </div>
                                          
                                            
                                            </div>
                                          )}

                                          {/* {propertyId ? (
                                            <></>
                                          ) : (
                                            <>
                                              {selectproperty ? (
                                                <div></div>
                                              ) : (
                                                <>
                                                  <div className="social-login mb-2">
                                                    <div className="float-left w-100 mb-0">
                                                      <div className="mt-0">
                                                        <h4 className="or-border">
                                                          <span>or</span>
                                                        </h4>
                                                      </div>
                                                    </div>
                                                  </div>

                                                  <div className="text-center float-left w-100 mb-2">
                                                    <a
                                                      className="btn btn-info btn-sm text-white"
                                                      onClick={() =>
                                                        AddBasic(
                                                          "1",
                                                          "Enter Basic"
                                                        )
                                                      }
                                                    >
                                                      Add a Property
                                                    </a>
                                                  </div>
                                                </>
                                              )}
                                            </>
                                          )} */}
                                        </>
                                      )}
                                      </div>
                                  ) : (
                                    ""
                                  )}
                                  

                                  {step1 && step2 === "" && propertyId == "" ? (
                                    <div className="">
                                      <div className="mb-0">
                                        <div className="row">
                                          <div className="col-md-6 col-sm-6 col-12 mt-3">
                                            <label
                                              className="col-form-label pt-0 pb-1"
                                              id="col-form-label-one"
                                            >
                                              {/* Street Name: */}
                                              Property Address:
                                            </label>
                                            <input
                                              className="form-control border-0 rounded-0 border-bottom"
                                              id="text-input-one"
                                              type="text"
                                              name="streetAddress"
                                              placeholder="Enter your Property Address"
                                              onChange={handleChangeproperty}
                                              value={
                                                formValuesproperty.streetAddress
                                              }
                                              style={{
                                                border:
                                                  formErrorsproperty?.streetAddress
                                                    ? "1px solid red"
                                                    : "",
                                              }}
                                            />

                                            {formErrorsproperty.streetAddress && (
                                              <div className="invalid-tooltip">
                                                {
                                                  formErrorsproperty.streetAddress
                                                }
                                              </div>
                                            )}
                                          </div>

                                            <div className="col-md-6 col-sm-6 col-12 mt-3">
                                            <label
                                              className="col-form-label pt-0 pb-1"
                                              id="col-form-label-one"
                                            >
                                              Property Type
                                              <span className="text-danger">
                                                *
                                              </span>
                                            </label>
                                            <select
                                              className="form-select"
                                              id="pr-city"
                                              name="propertyType"
                                              onChange={handleChangeproperty}
                                              value={
                                                formValuesproperty.propertyType
                                              }
                                              style={{
                                                border:
                                                  formErrorsproperty?.propertyType
                                                    ? "1px solid red"
                                                    : "",
                                              }}
                                            >
                                              <option value=""></option>
                                              <option value="land">Land</option>
                                              <option value="residential">
                                                Residential
                                              </option>
                                              <option value="commercial_lease">
                                                Commercial Lease
                                              </option>
                                              <option value="commercial_sale">
                                                Commercial Sale
                                              </option>
                                              <option value="commercial_industrial">
                                                Commercial/Industrial
                                              </option>
                                              <option value="residential_lease">
                                                Residential/Lease
                                              </option>
                                              <option value="farm">Farm</option>
                                            </select>
                                            {formErrorsproperty.propertyType && (
                                              <div className="invalid-tooltip">
                                                {
                                                  formErrorsproperty.propertyType
                                                }
                                              </div>
                                            )}
                                          </div>

                                            <div className="col-md-6 col-sm-6 col-12 mt-3">
                                            <label
                                              className="col-form-label pt-0 pb-1"
                                              id="col-form-label-one"
                                            >
                                              City:
                                            </label>
                                            <input
                                              className="form-control border-0 rounded-0 border-bottom"
                                              id="text-input-one"
                                              name="city"
                                              type="text"
                                              placeholder="Enter your City"
                                              onChange={handleChangeproperty}
                                              autoComplete="on"
                                              value={formValuesproperty.city}
                                              style={{
                                                border: formErrorsproperty?.city
                                                  ? "1px solid red"
                                                  : "",
                                              }}
                                            />

                                            {formErrorsproperty.city && (
                                              <div className="invalid-tooltip">
                                                {formErrorsproperty.city}
                                              </div>
                                            )}
                                          </div>

                                            <div className="col-md-6 col-sm-6 col-12 mt-3">
                                            <label
                                              className="col-form-label pt-0 pb-1"
                                              id="col-form-label-one"
                                            >
                                              State/Province:
                                            </label>
                                            <select
                                              id="text-input-one"
                                              className="form-select"
                                              name="state"
                                              onChange={handleChangeproperty}
                                              value={formValuesproperty.state}
                                            >
                                              <option value="0">
                                                --Select--
                                              </option>
                                              {options
                                                .find(
                                                  (option) =>
                                                    option.code === "UT"
                                                )
                                                .districts.map(
                                                  (district, key) => (
                                                    <option
                                                      value={district}
                                                      key={key}
                                                    >
                                                      {district}
                                                    </option>
                                                  )
                                                )}
                                            </select>
                                          </div>

                                            <div className="col-md-6 col-sm-6 col-12 mt-3">
                                            <label
                                              className="col-form-label pt-0 pb-1"
                                              id="col-form-label-one"
                                            >
                                              Postal Code:
                                            </label>
                                            <input
                                              className="form-control border-0 rounded-0 border-bottom"
                                              id="text-input-one"
                                              name="zipCode"
                                              type="text"
                                              placeholder="Enter your postal code"
                                              onChange={handleChangeproperty}
                                              value={formValuesproperty.zipCode}
                                              style={{
                                                border:
                                                  formErrorsproperty?.zipCode
                                                    ? "1px solid red"
                                                    : "",
                                              }}
                                            />

                                            {formErrorsproperty.zipCode && (
                                              <div className="invalid-tooltip">
                                                {formErrorsproperty.zipCode}
                                              </div>
                                            )}
                                          </div>

                                            <div className="col-md-6 col-sm-6 col-12 mt-3">
                                            <label
                                              className="col-form-label pt-0 pb-1"
                                              id="col-form-label-one"
                                            >
                                              List Price (Sale):
                                            </label>
                                            <input
                                              className="form-control border-0 rounded-0 border-bottom"
                                              id="text-input-one"
                                              name="price"
                                              type="number"
                                              placeholder="Enter your Price"
                                              onChange={handleChangeproperty}
                                              value={formValuesproperty.price}
                                            />
                                          </div>

                                            {/* <div className="col-md-6 col-sm-6 col-12 mt-3">
                                            <label
                                              className="col-form-label pt-0 pb-1"
                                              id="col-form-label-one"
                                            >
                                              List Price (Lease) /mo:
                                            </label>
                                            <input
                                              className="form-control border-0 rounded-0 border-bottom"
                                              id="text-input-one"
                                              name="priceLease"
                                              type="number"
                                              placeholder="Enter your list price lease"
                                              onChange={handleChangeproperty}
                                              value={
                                                formValuesproperty.priceLease
                                              }
                                            />
                                          </div> */}
                                          {data ? (
                                            <div className="col-md-6 mt-3">
                                              <img src={data} />
                                            </div>
                                          ) : (
                                            <div className="col-md-6 mt-3">
                                              <label
                                                className="col-form-label"
                                                id="form-label-one"
                                              >
                                                Upload Property Image
                                              </label>
                                              <input
                                                className="file-uploader file-uploader-grid"
                                                id="inline-form-input"
                                                type="file"
                                                name="image"
                                                value={formValuesproperty.image}
                                                accept="image/*"
                                                onChange={handleFileUpload}
                                              />

                                              {/* <div className="invalid-tooltip">
                                              {formErrorsproperty.image}
                                            </div> */}
                                            </div>
                                          )}
                                        </div>
                                      </div>
                                      <div className="pull-right mt-4">
                                        <a
                                          className="btn btn-info btn-sm"
                                          href="#"
                                          onClick={handleSubmit}
                                        >
                                          Add Property Now{" "}
                                        </a>

                                        <button
                                          type="button"
                                          className="btn btn-secondary btn-sm ms-3"
                                          onClick={handleBackProperty}
                                        >
                                          Cancel & Close
                                        </button>
                                      </div>
                                    </div>
                                  ) : (
                                    ""
                                  )}
                                

                                  {selectproperty ? (
                                    <>
                                      <div className="bg-light mb-0"></div>
                                      {propertyId ? (
                                        <>
                                          <div id="accordionCards" className="">
                                            <div className="card border-0 mb-2 mt-0">
                                              <div
                                                className="collapse show"
                                                id="cardCollapse1"
                                                data-bs-parent="#accordionCards"
                                              >
                                                <div className="card-body mt-n1 p-0">
                                                  <ul className="list-unstyled mb-2">
                                                    <div className="row">
                                                      <div className="col-md-6">
                                                        <small>
                                                          <li className="d-flex align-items-center">
                                                            {summary ? (
                                                              <div
                                                                className="col-sm-12"
                                                                key={
                                                                  summary?._id
                                                                }
                                                              >
                                                                <i className="fi-check text-success me-2"></i>
                                                                <b className="ps-1">
                                                                  {selectedCategory ===
                                                                  "referral"
                                                                    ? "Referral info"
                                                                    : "Clients info"}
                                                                </b>
                                                                <div className="card select_contact col-md-12 my-2">
                                                                  <div className="card-body p-2">
                                                                    <div className="view_profile">
                                                                      <img
                                                                        className="pull-left"
                                                                        src={
                                                                          summary?.image ||
                                                                          "https://integra.backagent.net/common/handler/pub/photo.ashx?V=1703c0890a0462fd68520b884a04f86f.0&C=people&S=2&P=%2f!Default%2fThumb%2fphoto_pending.jpg"
                                                                        }
                                                                        alt="Profile"
                                                                      />
                                                                      <span>
                                                                        <small>
                                                                          <a className="card-title mt-0">
                                                                            {
                                                                              summary?.firstName
                                                                            }
                                                                            &nbsp;
                                                                            {
                                                                              summary?.lastName
                                                                            }
                                                                          </a>

                                                                          {summary?.phone ? (
                                                                            <p className="mb-0">
                                                                              {
                                                                                summary?.phone
                                                                              }
                                                                            </p>
                                                                          ) : (
                                                                            ""
                                                                          )}

                                                                          {summary?.company ? (
                                                                            <p className="mb-0">
                                                                              {
                                                                                summary?.company
                                                                              }
                                                                            </p>
                                                                          ) : (
                                                                            ""
                                                                          )}
                                                                          {summary?.corporation_name ? (
                                                                            <p className="mb-0">
                                                                              {
                                                                                summary?.corporation_name
                                                                              }
                                                                            </p>
                                                                          ) : (
                                                                            ""
                                                                          )}

                                                                          {summary?.trust_name ? (
                                                                            <p className="mb-0">
                                                                              {
                                                                                summary?.trust_name
                                                                              }
                                                                            </p>
                                                                          ) : (
                                                                            ""
                                                                          )}
                                                                        </small>
                                                                      </span>
                                                                    </div>
                                                                  </div>
                                                                </div>
                                                              </div>
                                                            ) : (
                                                              <>
                                                                {/* <img
                                                            src="https://cdn.pboffice.net/shared/transaction/page_select_drop.png"
                                                            alt="Select Drop"
                                                          /> */}
                                                                <a
                                                                  className="nav-link fw-normal ps-1 p-0"
                                                                  href="#basic-info"
                                                                  data-scroll=""
                                                                  data-scroll-offset="20"
                                                                >
                                                                  Contacts:
                                                                  <p>
                                                                    {
                                                                      select.firstName
                                                                    }
                                                                  </p>
                                                                </a>

                                                                {}
                                                              </>
                                                            )}
                                                          </li>
                                                        </small>
                                                      </div>
                                                      <div className="col-md-6">
                                                        <small>
                                                          <li className="d-flex align-items-center mb-1">
                                                            {selectedCategory ? (
                                                              <>
                                                                <i className="fi-check text-success me-2"></i>

                                                                <a
                                                                  className="nav-link fw-normal ps-1 p-0"
                                                                  href="#basic-info"
                                                                  data-scroll=""
                                                                  data-scroll-offset="20"
                                                                >
                                                                  {selectedCategory ===
                                                                  "buyer" ? (
                                                                    <>
                                                                      <b>
                                                                        Category
                                                                      </b>
                                                                      : &nbsp;
                                                                      {selectedCategory ===
                                                                      "buyer"
                                                                        ? "Buyer"
                                                                        : ""}
                                                                    </>
                                                                  ) : (
                                                                    ""
                                                                  )}

                                                                  {selectedCategory ===
                                                                  "seller" ? (
                                                                    <>
                                                                      <b>
                                                                        Category
                                                                      </b>
                                                                      : &nbsp;
                                                                      {selectedCategory ===
                                                                      "seller"
                                                                        ? "Seller"
                                                                        : ""}
                                                                    </>
                                                                  ) : (
                                                                    ""
                                                                  )}

                                                                  {selectedCategory ===
                                                                  "both" ? (
                                                                    <>
                                                                      <b>
                                                                        Category
                                                                      </b>
                                                                      : &nbsp;
                                                                      {selectedCategory ===
                                                                      "both"
                                                                        ? "Buyer & Seller"
                                                                        : ""}
                                                                    </>
                                                                  ) : (
                                                                    ""
                                                                  )}
                                                                  {selectedCategory ===
                                                                  "referral" ? (
                                                                    <>
                                                                      <b>
                                                                        Category
                                                                      </b>
                                                                      : &nbsp;
                                                                      {selectedCategory ===
                                                                      "referral"
                                                                        ? "Referral"
                                                                        : ""}
                                                                    </>
                                                                  ) : (
                                                                    ""
                                                                  )}
                                                                </a>
                                                              </>
                                                            ) : (
                                                              <>
                                                                <img src="https://cdn.pboffice.net/shared/transaction/page_select_drop.png" />
                                                                <a
                                                                  className="nav-link fw-normal ps-1 p-0"
                                                                  href="#basic-info"
                                                                  data-scroll=""
                                                                  data-scroll-offset="20"
                                                                >
                                                                  Category
                                                                </a>
                                                              </>
                                                            )}
                                                          </li>
                                                        </small>

                                                        <small>
                                                          <li className="d-flex align-items-center mb-2">
                                                            {selectedType ? (
                                                              <>
                                                                <i className="fi-check text-success me-2"></i>
                                                                <a
                                                                  className="nav-link fw-normal ps-1 p-0"
                                                                  href="#basic-info"
                                                                  data-scroll=""
                                                                  data-scroll-offset="20"
                                                                >
                                                                  {selectedCategory ===
                                                                  "buyer" ? (
                                                                    <>
                                                                      <b>
                                                                        Type
                                                                      </b>
                                                                      : &nbsp;
                                                                      {selectedType ===
                                                                      "buy_residential"
                                                                        ? "Buyer Residential"
                                                                        : ""}
                                                                      {selectedType ===
                                                                      "buy_commercial"
                                                                        ? "Buyer Commercial"
                                                                        : ""}
                                                                      {selectedType ===
                                                                      "buy_investment"
                                                                        ? "Buyer Investment"
                                                                        : ""}
                                                                      {selectedType ===
                                                                      "buy_land"
                                                                        ? "Buyer Land"
                                                                        : ""}
                                                                      {selectedType ===
                                                                      "buy_referral"
                                                                        ? "Buyer Referral"
                                                                        : ""}
                                                                      {selectedType ===
                                                                      "buy_lease"
                                                                        ? "Buyer Lease"
                                                                        : ""}
                                                                    </>
                                                                  ) : (
                                                                    ""
                                                                  )}

                                                                  {selectedCategory ===
                                                                  "seller" ? (
                                                                    <>
                                                                      <b>
                                                                        Type
                                                                      </b>
                                                                      : &nbsp;
                                                                      {selectedType ===
                                                                      "sale_residential"
                                                                        ? "Seller Residential"
                                                                        : ""}
                                                                      {selectedType ===
                                                                      "sale_commercial"
                                                                        ? "Seller Commercial"
                                                                        : ""}
                                                                      {selectedType ===
                                                                      "sale_investment"
                                                                        ? "Seller Investment"
                                                                        : ""}
                                                                      {/* {selectedType ===
                                                                  "sale_mobile"
                                                                    ? "Seller Mobile"
                                                                    : ""} */}
                                                                      {/* {selectedType ===
                                                                  "sale_land"
                                                                    ? "Seller Land"
                                                                    : ""} */}
                                                                      {selectedType ===
                                                                      "sale_referral"
                                                                        ? "Seller Referral"
                                                                        : ""}
                                                                      {selectedType ===
                                                                      "sale_lease"
                                                                        ? "Seller Lease"
                                                                        : ""}
                                                                    </>
                                                                  ) : (
                                                                    ""
                                                                  )}

                                                                  {selectedCategory ===
                                                                  "both" ? (
                                                                    <>
                                                                      <b>
                                                                        Type
                                                                      </b>
                                                                      : &nbsp;
                                                                      {selectedType ===
                                                                      "both_residential"
                                                                        ? "Buyer & Seller Residential"
                                                                        : ""}
                                                                      {selectedType ===
                                                                      "both_commercial"
                                                                        ? "Buyer & Seller Commercial"
                                                                        : ""}
                                                                      {selectedType ===
                                                                      "both_investment"
                                                                        ? "Buyer & Seller Investment"
                                                                        : ""}
                                                                      {selectedType ===
                                                                      "both_land"
                                                                        ? "Buyer & Seller Land"
                                                                        : ""}
                                                                      {selectedType ===
                                                                      "both_referral"
                                                                        ? "Buyer & Seller Referral"
                                                                        : ""}
                                                                      {selectedType ===
                                                                      "both_lease"
                                                                        ? "Buyer & Seller Lease"
                                                                        : ""}
                                                                    </>
                                                                  ) : (
                                                                    ""
                                                                  )}

                                                                  {selectedCategory ===
                                                                  "referral" ? (
                                                                    <>
                                                                      <b>
                                                                        Type
                                                                      </b>
                                                                      : &nbsp;
                                                                      {selectedType ===
                                                                      "referral_residential"
                                                                        ? "Residential Referral"
                                                                        : ""}
                                                                      {selectedType ===
                                                                      "referral_commercial"
                                                                        ? "Commercial Referral"
                                                                        : ""}
                                                                      {selectedType ===
                                                                      "referral_investment"
                                                                        ? "Investment Referral"
                                                                        : ""}
                                                                      {selectedType ===
                                                                      "referral_land"
                                                                        ? "Land Referral"
                                                                        : ""}
                                                                      {selectedType ===
                                                                      "referral_lease"
                                                                        ? "Lease Referral"
                                                                        : ""}
                                                                    </>
                                                                  ) : (
                                                                    ""
                                                                  )}
                                                                </a>
                                                              </>
                                                            ) : (
                                                              <>
                                                                <img src="https://cdn.pboffice.net/shared/transaction/page_select_drop.png" />
                                                                <a
                                                                  className="nav-link fw-normal ps-1 p-0"
                                                                  href="#basic-info"
                                                                  data-scroll=""
                                                                  data-scroll-offset="20"
                                                                >
                                                                  Type
                                                                </a>
                                                              </>
                                                            )}
                                                          </li>
                                                        </small>

                                                        <small>
                                                          <li className="d-flex align-items-center mb-1">
                                                            {selectedOwner ? (
                                                              <>
                                                                <i className="fi-check text-success me-2"></i>
                                                                <a
                                                                  className="nav-link fw-normal ps-1 p-0"
                                                                  href="#details"
                                                                  data-scroll=""
                                                                  data-scroll-offset="20"
                                                                >
                                                                  {selectedOwner ? (
                                                                    <>
                                                                      <b>
                                                                        Ownership
                                                                      </b>
                                                                      : &nbsp;
                                                                      {selectedOwner ===
                                                                      "individual"
                                                                        ? "Individual"
                                                                        : ""}
                                                                      {selectedOwner ===
                                                                      "corporation"
                                                                        ? "Entity/Corporation"
                                                                        : ""}
                                                                      {selectedOwner ===
                                                                      "trust"
                                                                        ? "Trust"
                                                                        : ""}
                                                                    </>
                                                                  ) : (
                                                                    ""
                                                                  )}
                                                                </a>
                                                              </>
                                                            ) : (
                                                              <>
                                                                <img
                                                                  src="https://cdn.pboffice.net/shared/transaction/page_select_drop.png"
                                                                  alt="Select Drop"
                                                                />
                                                                <a
                                                                  className="nav-link fw-normal ps-1 p-0"
                                                                  href="#basic-info"
                                                                  data-scroll=""
                                                                  data-scroll-offset="20"
                                                                >
                                                                  Ownership
                                                                </a>
                                                              </>
                                                            )}
                                                          </li>
                                                        </small>

                                                        <small>
                                                          <li className="d-flex align-items-center mb-1">
                                                            {selectedPhase ? (
                                                              <>
                                                                <i className="fi-check text-success me-2"></i>

                                                                <a
                                                                  className="nav-link fw-normal ps-1 p-0"
                                                                  href="#basic-info"
                                                                  data-scroll=""
                                                                  data-scroll-offset="20"
                                                                >
                                                                  {selectedPhase ? (
                                                                    <>
                                                                      <b>
                                                                        Selected
                                                                        Phase
                                                                      </b>
                                                                      : &nbsp;
                                                                      {selectedPhase ===
                                                                      "pre-listed"
                                                                        ? "Pre-Listed"
                                                                        : ""}
                                                                      {selectedPhase ===
                                                                      "active-listing"
                                                                        ? "Active Listing"
                                                                        : ""}
                                                                      {selectedPhase ===
                                                                      "showing-homes"
                                                                        ? "Showing homes"
                                                                        : ""}
                                                                      {selectedPhase ===
                                                                      "under-contract"
                                                                        ? "Under Contract"
                                                                        : ""}
                                                                      {selectedPhase ===
                                                                      "closed"
                                                                        ? "Closed"
                                                                        : ""}
                                                                      {selectedPhase ===
                                                                      "canceled"
                                                                        ? "Canceled"
                                                                        : ""}
                                                                    </>
                                                                  ) : (
                                                                    ""
                                                                  )}
                                                                </a>
                                                              </>
                                                            ) : (
                                                              <>
                                                                <img src="https://cdn.pboffice.net/shared/transaction/page_select_drop.png" />
                                                                <a
                                                                  className="nav-link fw-normal ps-1 p-0"
                                                                  href="#basic-info"
                                                                  data-scroll=""
                                                                  data-scroll-offset="20"
                                                                >
                                                                  Phase
                                                                </a>
                                                              </>
                                                            )}
                                                          </li>
                                                        </small>
                                                      </div>
                                                    </div>

                                                    <small>
                                                      <i className="fi-check text-success me-2"></i>
                                                      <b className="ps-1">
                                                        Selected Property
                                                      </b>
                                                    </small>
                                                    <div className="select_property card card-hover card-horizontal transactions shadow-sm mb-0 mt-2 p-2">
                                                      <a className="card-img-top">
                                                        <img
                                                          className=""
                                                          src={
                                                            selectproperty.image &&
                                                            selectproperty.image !==
                                                              "image"
                                                              ? selectproperty.image
                                                              : defaultpropertyimage
                                                          }
                                                          alt="Property"
                                                          onError={(e) =>
                                                            propertypic(e)
                                                          }
                                                        />
                                                      </a>
                                                      <div className="card-body position-relative px-2 pt-0 pb-0">
                                                        <span className="mb-2">
                                                          <small>
                                                            {
                                                              selectproperty.state
                                                            }
                                                            _
                                                            {
                                                              selectproperty.streetNumber
                                                            }
                                                          </small>
                                                        </span>
                                                        <br />
                                                        <span className="mb-2">
                                                          <small>
                                                            {
                                                              selectproperty.streetAddress
                                                            }
                                                          </small>
                                                        </span>
                                                        <br />
                                                        <span className="mb-2">
                                                          <small>
                                                            {
                                                              selectproperty.areaLocation
                                                            }
                                                          </small>
                                                        </span>
                                                        <br />
                                                        <span className="mb-2">
                                                          <small>
                                                            {
                                                              selectproperty.listingsAgent
                                                            }
                                                          </small>
                                                        </span>
                                                      </div>

                                                      {/* <div className="d-flex align-items-center justify-content-between"> */}
                                                      <div className="">
                                                        <p className="mb-2 badge bg-success">
                                                          {selectproperty.price}
                                                        </p>
                                                        <br />
                                                        <label className="col-form-label fw-bold pt-0">
                                                          {
                                                            selectproperty.status
                                                          }
                                                        </label>
                                                      </div>
                                                    </div>
                                                  </ul>
                                                </div>
                                              </div>
                                            </div>
                                          </div>
                                        </>
                                      ) : (
                                        ""
                                      )}
                                      {/* {propertyId ? (
                                        ""
                                      ) : (
                                        <div className="text-center">
                                        <NavLink
                                          className="btn btn-info btn-sm mt-3"
                                          href="#"
                                          onClick={handlePropertyUpdate}
                                        >
                                          Set Property Now
                                        </NavLink>
                                          </div>
                                      )} */}
                                    </>
                                  ) : (
                                    <div className="row">
                                      <div>
                                        {isLoading ? (
                                          <div className="mb-4 mt-5 text-center">
                                            <a className="card-img-top">
                                              <i className="fa fa-spinner fa-spin"></i>
                                              &nbsp; Searching...
                                            </a>
                                          </div>
                                        ) : resultProperty ? (
                                          propertyId ? (
                                            <></>
                                          ) : searchby === "mylisting" ? (
                                            <>
                                              {resultProperty.length > 0 ? (
                                                <>
                                                  {resultProperty.map(
                                                    (post) => (
                                                      <div
                                                        className="select_property card card-hover card-horizontal transactions shadow-sm mb-0 mt-0 p-2"
                                                        key={post._id} // Add a unique key for each iteration
                                                        onClick={() =>
                                                          handlePropertyUpdate(
                                                            post._id
                                                          )
                                                        }
                                                      >
                                                        <a className="card-img-top">
                                                          <img
                                                            className=""
                                                            src={
                                                              post.image &&
                                                              post.image !==
                                                                "image"
                                                                ? post.image
                                                                : defaultpropertyimage
                                                            }
                                                            alt="Property"
                                                            onError={(e) =>
                                                              propertypic(e)
                                                            }
                                                          />
                                                        </a>
                                                        <div className="card-body position-relative px-2 pt-0 pb-0">
                                                          <span className="mb-2">
                                                            <small>
                                                              {post.state}_
                                                              {
                                                                post.streetNumber
                                                              }
                                                            </small>
                                                          </span>
                                                          <br />
                                                          <span className="mb-2">
                                                            <small>
                                                              {
                                                                post.streetAddress
                                                              }
                                                            </small>
                                                          </span>
                                                          <br />
                                                          <span className="mb-2">
                                                            <small>
                                                              {
                                                                post.areaLocation
                                                              }
                                                            </small>
                                                          </span>
                                                          <br />
                                                          <span className="mb-2">
                                                            <small>
                                                              {
                                                                post.listingsAgent
                                                              }
                                                            </small>
                                                          </span>
                                                        </div>

                                                        <div className="">
                                                          <p className="mb-2 badge bg-success">
                                                            {post.price}
                                                          </p>
                                                          <br />
                                                          <label className="col-form-label fw-bold pt-0">
                                                            {post.status}
                                                          </label>
                                                          <button
                                                            className="btn btn-info btn-sm"
                                                            id=""
                                                            type="button"
                                                          >
                                                            Select Listing
                                                          </button>
                                                        </div>
                                                      </div>
                                                    )
                                                  )}
                                                  <div className="justify-content-end mt-3">
                                                    <ReactPaginate
                                                      previousLabel={"Previous"}
                                                      nextLabel={"next"}
                                                      breakLabel={"..."}
                                                      pageCount={
                                                        pageCountProperty
                                                      }
                                                      marginPagesDisplayed={1}
                                                      pageRangeDisplayed={2}
                                                      onPageChange={
                                                        handlePageClickproperty
                                                      }
                                                      containerClassName={
                                                        "pagination justify-content-center mb-2"
                                                      }
                                                      pageClassName={
                                                        "page-item"
                                                      }
                                                      pageLinkClassName={
                                                        "page-link"
                                                      }
                                                      previousClassName={
                                                        "page-item"
                                                      }
                                                      previousLinkClassName={
                                                        "page-link"
                                                      }
                                                      nextClassName={
                                                        "page-item"
                                                      }
                                                      nextLinkClassName={
                                                        "page-link"
                                                      }
                                                      breakClassName={
                                                        "page-item"
                                                      }
                                                      breakLinkClassName={
                                                        "page-link"
                                                      }
                                                      activeClassName={"active"}
                                                    />
                                                  </div>
                                                </>
                                              ) : (
                                                ""
                                              )}
                                            </>
                                          ) : searchby === "mls" &&
                                            resultProperty.value &&
                                            resultProperty.value.length > 0 ? (
                                            <>
                                              <div className="select_property card card-hover card-horizontal transactions shadow-sm mb-0 mt-5 p-2">
                                                <div className="media_images">
                                                  {selectedImageUrl ? (
                                                    <div className="card-img-top">
                                                      <img
                                                        className="img-fluid w-100"
                                                        src={selectedImageUrl}
                                                      />
                                                    </div>
                                                  ) : (
                                                    <>
                                                      {resultProperty?.media[0]
                                                        ?.MediaURL ? (
                                                        <div className="card-img-top">
                                                          <img
                                                            className="img-fluid"
                                                            src={
                                                              resultProperty
                                                                ?.media[0]
                                                                ?.MediaURL
                                                                ? resultProperty
                                                                    ?.media[0]
                                                                    ?.MediaURL
                                                                : defaultpropertyimage
                                                            }
                                                            alt="Property"
                                                            onError={(e) =>
                                                              propertypic(e)
                                                            }
                                                          />
                                                        </div>
                                                      ) : (
                                                        ""
                                                      )}
                                                    </>
                                                  )}

                                                  <div className="property_thumb">
                                                    {resultProperty?.media.map(
                                                      (item) => (
                                                        <img
                                                          className="media-image"
                                                          src={item.MediaURL}
                                                          onClick={() =>
                                                            handleImageUrl(
                                                              item.MediaURL
                                                            )
                                                          }
                                                        />
                                                      )
                                                    )}
                                                  </div>
                                                </div>

                                                <div className="card-body position-relative px-2 pt-0 pb-0">
                                                  <small>
                                                    <span className="mb-2">
                                                      {
                                                        resultProperty.value[0]
                                                          ?.CountyOrParish
                                                      }
                                                      _
                                                      {
                                                        resultProperty.value[0]
                                                          ?.ListingId
                                                      }
                                                    </span>
                                                    <br />
                                                    <span className="mb-2">
                                                      {
                                                        resultProperty.value[0]
                                                          ?.UnparsedAddress
                                                      }
                                                    </span>
                                                    <br />
                                                    <span className="mb-2">
                                                      {
                                                        resultProperty.value[0]
                                                          .City
                                                      }
                                                      ,
                                                      {
                                                        resultProperty.value[0]
                                                          .StateOrProvince
                                                      }{" "}
                                                      {
                                                        resultProperty.value[0]
                                                          ?.PostalCode
                                                      }
                                                    </span>
                                                    <br />
                                                    <span className="mb-2">
                                                      {`${
                                                        resultProperty?.value[0]
                                                          ?.ListAgentFullName
                                                      } / ${
                                                        resultProperty?.value[0]
                                                          ?.ListOfficeName ?? ""
                                                      }`}
                                                    </span>
                                                  </small>
                                                </div>
                                                <div className="float-left text-left">
                                                  <button
                                                    className="btn btn-info btn-sm"
                                                    id=""
                                                    type="button"
                                                    onClick={() =>
                                                      handlePropertyUpdate(
                                                        resultProperty
                                                      )
                                                    }
                                                  >
                                                    Select Listing
                                                  </button>
                                                  <p className="mb-2 badge bg-success">
                                                    {
                                                      resultProperty.value[0]
                                                        .ListPrice
                                                    }
                                                  </p>
                                                  <br />
                                                  <h6 className="mt-0">
                                                    {
                                                      resultProperty.value[0]
                                                        .MlsStatus
                                                    }
                                                  </h6>
                                                </div>
                                              </div>
                                            </>
                                          ) : (
                                            ""
                                          )
                                        ) : (
                                          ""
                                        )}
                                      </div>
                                      {
                                        step1 ?
                                        <></>
                                        :
                                        <>
                                      {propertyId ? (
                                            <></>
                                          ) : (
                                            <>
                                              {selectproperty ? (
                                                <div></div>
                                              ) : (
                                                <>
                                                  <div className="social-login mb-2 my-3">
                                                    <div className="float-left w-100 mb-0">
                                                      <div className="mt-0">
                                                        <h4 className="or-border">
                                                          <span>or</span>
                                                        </h4>
                                                      </div>
                                                    </div>
                                                  </div>

                                                  <div className="text-center float-left w-100 mb-2 mt-2">
                                                    <a
                                                      className="btn btn-info btn-sm text-white"
                                                      onClick={() =>
                                                        AddBasic(
                                                          "1",
                                                          "Enter Basic"
                                                        )
                                                      }
                                                    >
                                                      Add a Property
                                                    </a>
                                                  </div>
                                                </>
                                              )}
                                            </>
                                          )}
                                        </>
                                      }
                                    </div>
                                  )}

                                  {propertyId ? (
                                    <>
                                      <b>
                                        How would you like to name your file?
                                      </b>
                                      <div className="mb-2">
                                        <label className="form-label">
                                          File name
                                        </label>
                                        <br />
                                        <input
                                          className="form-control"
                                          type="text"
                                          id="text-input"
                                          name="filename"
                                          placeholder="Enter your file name"
                                          onChange={(event) =>
                                            handleChanges(event)
                                          }
                                          autoComplete="on"
                                          style={{
                                            border: formErrors?.filename
                                              ? "1px solid red"
                                              : "1px solid #00000026",
                                          }}
                                          value={formValuesNew.filename}
                                        />
                                        <div className="invalid-tooltip">
                                          {formErrors.filename}
                                        </div>
                                      </div>

                                      <div
                                        className="col-sm-6"
                                        // onClick={(e) => CheckRegistration(e)}
                                      >
                                        <div className="d-flex">
                                          <div className="form-check ps-3">
                                            <input
                                              className="form-check-input"
                                              id="form-radio-4"
                                              type="radio"
                                              name="registration"
                                              value="address"
                                              onChange={(e) =>
                                                CheckRegistration(e)
                                              }
                                              style={{
                                                border: "1px solid black",
                                              }}
                                            />
                                            <label
                                              className="form-check-label ms-2"
                                              id="radio-level1"
                                            >
                                              Address
                                            </label>
                                          </div>

                                          <div className="form-check">
                                            <input
                                              style={{
                                                border: "1px solid black",
                                              }}
                                              className="form-check-input "
                                              id="form-radio-4"
                                              type="radio"
                                              name="registration"
                                              value="primary_client"
                                              onChange={(e) =>
                                                CheckRegistration(e)
                                              }
                                            />
                                            <label
                                              className="form-check-label ms-2"
                                              id="radio-level1"
                                            >
                                              Primary Client
                                            </label>
                                          </div>
                                        </div>
                                      </div>
                                      <div className="float-left w-100 mt-1 text-center">
                                        <button
                                          id="type-value"
                                          className="btn btn-info btn-sm"
                                          onClick={AddTransaction}
                                        >
                                          Create New Transaction
                                          {/* {isSubmit
                                        ? "Please wait..."
                                        : "Create New Transaction"}{" "} */}
                                        </button>
                                      </div>
                                    </>
                                  ) : (
                                    ""
                                  )}
                                </div>
                              )}
                            </div>
                          </div>

                          {/* select property section end */}
                        </>
                      ) : (
                        ""
                      )}
                      {/* <div className="float-left w-100 text-center">
                            <button
                              type="button"
                              className="btn btn-primary btn-sm"
                              onClick={prevStep}
                            >
                              Previous
                            </button>
                          </div> */}
                    </fieldset>
                  </div>
                </div>
              </div>
            </div>
            {localStorage.getItem("auth") &&
            jwt(localStorage.getItem("auth")).contactType == "admin" ? (
              <div className="col-md-3 mt-4">
                <div className="bg-light border rounded-3 p-3">
                  <label className="col-form-label pt-0 mb-0">
                    Create transaction for:
                  </label>
                  {changeId ? (
                    <>
                      <label className="col-form-label pt-0 mb-0">
                        Find an associate by name:
                      </label>

                      {selectAssociate ? (
                        <div className="col-md-6 w-100">
                          <div className="card bg-secondary mt-0">
                            {selectAssociate.represent ? (
                              <h6>{selectAssociate.represent}</h6>
                            ) : (
                              ""
                            )}
                            <div className="card-body">
                              <div className="view_profile">
                                <img
                                  className="rounded-circle pull-left"
                                  src={
                                    selectAssociate.image ||
                                    "https://integra.backagent.net/common/handler/pub/photo.ashx?V=1703c0890a0462fd68520b884a04f86f.0&C=people&S=2&P=%2f!Default%2fThumb%2fphoto_pending.jpg"
                                  }
                                  alt="Profile"
                                />
                                <span>
                                  <strong>
                                    <a href="#" className="card-title mt-0">
                                      {selectAssociate.firstName}&nbsp;
                                      {selectAssociate.lastName}
                                    </a>
                                  </strong>
                                  <p className="mb-0">
                                    {selectAssociate.company}
                                    <br />
                                    {selectAssociate.phone}
                                    <br />
                                  </p>
                                </span>
                              </div>
                            </div>
                          </div>
                        </div>
                      ) : (
                        <>
                          <input
                            className="form-control"
                            id=""
                            type="text"
                            valvalue={query}
                            onChange={(event) => setQuery(event.target.value)}
                          />
                          {isLoading ? (
                            ""
                          ) : (
                            <div className="activateaccountstaff_recruiter">
                              {query &&
                                getAllAssociate.map((associate) => (
                                  <div
                                    className="activateaccountstaff"
                                    key={associate._id}
                                  >
                                    <ul
                                      className="list-group"
                                      onClick={(e) =>
                                        handleSearchTransactionAssociate(
                                          associate._id
                                        )
                                      }
                                    >
                                      <li className="list-group-item d-flex justify-content-start align-items-center">
                                        <div>
                                          <img
                                            className="rounded-circle profile_picture"
                                            src={associate.image || avtar}
                                            alt="Profile"
                                          />
                                          <br />
                                        </div>
                                        <div>
                                          <strong>
                                            {associate.firstName}&nbsp;
                                            {associate.lastName}
                                          </strong>
                                          <br />
                                          <strong>
                                            {associate.contactType
                                              ? associate.contactType
                                                  .charAt(0)
                                                  .toUpperCase() +
                                                associate.contactType.slice(1)
                                              : ""}
                                          </strong>
                                          <br />
                                        </div>
                                      </li>
                                    </ul>
                                  </div>
                                ))}
                            </div>
                          )}
                        </>
                      )}

                      <NavLink
                        className="btn btn-primary mt-3"
                        onClick={handleChangeCancel}
                      >
                        Cancel
                      </NavLink>
                    </>
                  ) : (
                    <>
                      <p className="">
                        {profile.firstName} {profile.lastName}
                      </p>
                      <NavLink
                        className="btn btn-primary"
                        onClick={handleChangeClick}
                      >
                        Change
                      </NavLink>
                    </>
                  )}
                </div>
              </div>
            ) : (
              ""
            )}
          </div>
        </div>
      </main>
    </div>
  );
}
export default NewTransaction;

import React, { useEffect, useState } from "react";
import Loader from "../../Pages/Loader/Loader.js";
import Toaster from "../../Pages/Toaster/Toaster.js";
import { useParams, useNavigate } from "react-router-dom";
import user_service from "../service/user_service";
import AllTab from "../../Pages/AllTab";
import ReactReadMoreReadLess from "react-read-more-read-less";
import jwt from "jwt-decode";

const OpenHouseListing = () => {
  const [loader, setLoader] = useState({ isActive: null });
  const { isActive } = loader;
  const [toaster, setToaster] = useState({
    types: null,
    isShow: null,
    toasterBody: "",
    message: "",
  });
  const { types, isShow, toasterBody, message } = toaster;
  const navigate = useNavigate();

  const [summary, setSummary] = useState([]);
  const [sounds, setSounds] = useState([]);

  const params = useParams();

  const TransactionGetById = async () => {
    await user_service.transactionGetById(params.id).then((response) => {
      setLoader({ isActive: false });
      if (response) {
        setSummary(response.data);
      }
    });
  };

  useEffect(() => {
    TransactionGetById(params.id);
    OpenHouseGetById();
  }, []);

  const OpenHouseGetById = async () => {
    if (params.id) {
      await user_service.openHouseGet(params.id).then((response) => {
        setLoader({ isActive: false });
        if (response) {
          setSounds(response.data.data);
        }
      });
    }
  };

  const handleOpenHouse = () => {
    navigate(`/transaction/openHouse/${params.id}`);
  };

  const handleDelete = async (id) => {
    setLoader({ isActive: true });
    await user_service.OpenHouseGetDelete(id).then((response) => {
      if (response) {
        setLoader({ isActive: false });
        setToaster({
          types: "Delete",
          isShow: true,
          toasterBody: response.data.message,
          // message: "",
        });

        setTimeout(() => {
          setToaster((prevToaster) => ({
            ...prevToaster,
            isShow: false,
          }));
        }, 2000);
        OpenHouseGetById();
      }
    });
  };

  return (
    <div className="bg-secondary float-left w-100 pt-4">
      <Loader isActive={isActive} />
      {isShow && (
        <Toaster
          types={types}
          isShow={isShow}
          toasterBody={toasterBody}
          message={message}
        />
      )}
      <main className="page-wrapper">
        <div className="content-overlay transaction_tab_view mt-0">

        <div className="row">
          <AllTab />
          {/* <!-- Page container--> */}
          <div className="col-md-12">
            <button
              className="btn btn-primary pull-right m-3"
              onClick={handleOpenHouse}
            >
              Add Open House
            </button>
            <div className="bg-light border rounded-3 mb-4 p-3">
              <h3 className="text-left mb-0 py-2">Open House</h3>
            </div>
            <div className="content-overlay">
              <div className="col-md-12 pb-md-3">
                {sounds.length > 0 ? (
                  sounds.map((post, index) => (
                    <div key={index} className="card card-hover mb-2">
                      <div className="card-body">
                        <ul className="list-unstyled">
                          <li>
                            <b>Name: </b>
                            {post.fullName}
                          </li>
                          <li>
                            <b>Number: </b>
                            {post.phoneNumber}
                          </li>
                          <li>
                            <b>Email: </b>
                            {post.email}
                          </li>
                          <li>
                            <b>Timeframe: </b>
                            <ReactReadMoreReadLess
                              charLimit={200}
                              readMoreText={"Read more ▼"}
                              readLessText={"Read less ▲"}
                              readMoreClassName="read-more-less--more text-bold text-dark"
                              readLessClassName="read-more-less--less text-bold text-dark"
                            >
                              {post.timeframe}
                            </ReactReadMoreReadLess>
                          </li>

                          <li className="mt-2">
                            <b>Sell a Property: </b>
                            <ReactReadMoreReadLess
                              charLimit={200}
                              readMoreText={"Read more ▼"}
                              readLessText={"Read less ▲"}
                              readMoreClassName="read-more-less--more text-bold text-dark"
                              readLessClassName="read-more-less--less text-bold text-dark"
                            >
                              {post.sellProperty}
                            </ReactReadMoreReadLess>
                          </li>
                        </ul>

                        {localStorage.getItem("auth") &&
                        jwt(localStorage.getItem("auth")).contactType ==
                          "admin" ? (
                          <div className="pull-right">
                            <button
                              className="btn btn-secondary btn-sm ms-3"
                              onClick={() => handleDelete(post._id)}
                            >
                              Delete
                            </button>
                          </div>
                        ) : (
                          ""
                        )}
                      </div>
                    </div>
                  ))
                ) : (
                    <p className="text-center text-white py-4">No open house data available.</p>
                )}
              </div>
            </div>
          </div>
        </div>
        </div>
      </main>
    </div>
  );
};

export default OpenHouseListing;

import React, { useEffect, useState } from "react";
import Loader from "../../Pages/Loader/Loader.js";
import Toaster from "../../Pages/Toaster/Toaster.js";
import {useNavigate, useParams } from "react-router-dom";
import user_service from "../service/user_service";
import AllTab from "../../Pages/AllTab";

const OpenHouse = () => {
  const [loader, setLoader] = useState({ isActive: null });
  const { isActive } = loader;
  const [toaster, setToaster] = useState({
    types: null,
    isShow: null,
    toasterBody: "",
    message: "",
  });

  const navigate = useNavigate();

  const { types, isShow, toasterBody, message } = toaster;

  const [step, setStep] = useState(1);

  const [formValues, setFormValues] = useState({
    fullName: "",
    phoneNumber: "",
    email: "",
    timeframe: "",
    sellProperty: "",
    realStateAgent: "",
    agentInfo:"" 
  });

  const [formErrors, setFormErrors] = useState({});
  const [isSubmitClick, setISSubmitClick] = useState(false);

  const [summary, setSummary] = useState([]);
  const [sounds, setSounds] = useState("");

  const params = useParams();

  const TransactionGetById = async () => {
    // console.log(summary.id);

    await user_service.transactionGetById(params.id).then((response) => {
      setLoader({ isActive: false });
      if (response) {
        setSummary(response.data);
      }
    });
  };

  useEffect(() => {
    TransactionGetById(params.id);
  },[]);

  const handleChange = (e) => {
    const { name, value } = e.target;
    setFormValues({ ...formValues, [name]: value });
  };

  const nextStep = () => {
    if (validate()) {
      setStep(step + 1);
    }
  };

  const prevStep = () => setStep(step - 1);

  const handleSubmitSound = (sounds) => {
    if (validate()) {
        setSounds(sounds)
      nextStep();
    }
  };
  useEffect(() => {
    if (formValues && isSubmitClick) {
      validate();
    }
  }, [formValues]);

  const validate = () => {
    const values = formValues;
    const errors = {};

    if (!values.fullName) {
      errors.fullName = "Full Name is required!";
    }

    if (!values.phoneNumber) {
        errors.phoneNumber = "Phone Number is required!";
      } else if (!/^\d{10}$/.test(values.phoneNumber)) {
        errors.phoneNumber = "Phone Number must be exactly 10 digits!";
      }

      if (!values.email) {
        errors.email = "Email is required";
      } else if (
        !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i.test(values.email)
      ) {
        errors.email = "Invalid email address";
      }
  

    // if (!values.email) {
    //   errors.email = "Email is required!";
    // } else if (
    //   !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.email)
    // ) {
    //   errors.email = "Invalid email address!";
    // }

    if (!values.timeframe) {
      errors.timeframe = "Timeframe is required!";
    }

    if (!values.sellProperty) {
      errors.sellProperty = "Sell Property is required!";
    }

    if (!values.realStateAgent) {
      errors.realStateAgent = "RealvState Agent is required!";
    }

    setFormErrors(errors);
    return errors;
  };

  const handleSubmit = async() => {
    const userData = {
        fullName: formValues.fullName,
        phoneNumber: formValues.phoneNumber,
        email: formValues.email,
        timeframe: formValues.timeframe,
        sellProperty: formValues.sellProperty,
        realStateAgent: formValues.realStateAgent,
        financePro: sounds,
        transactionId: params.id,
        agentInfo:formValues.agentInfo
      };
      try {
        setLoader({ isActive: true });
        const response = await user_service.openHousePost(userData);
        if (response) {
          setLoader({ isActive: false });
          setToaster({
            types: "Open House",
            isShow: true,
            toasterBody: response.data.message,
            message: "Open House Added Successfully",
          });
          setTimeout(() => {
            setToaster((prevToaster) => ({
              ...prevToaster,
              isShow: false,
            }));
            navigate(`/transaction/openHouse-listing/${params.id}`);


          }, 2000);
        } else {
          setToaster({
            types: "error",
            isShow: true,
            toasterBody: "Error while adding referral.",
            message: "Error",
          });
        }
      } catch (error) {
        setLoader({ isActive: false });
        setToaster({
          types: "error",
          isShow: true,
          toasterBody: error.response?.data.message || "An error occurred",
          message: "Error",
        });

        setTimeout(() => {
          setToaster((prevToaster) => ({
            ...prevToaster,
            isShow: false,
          }));
        }, 2000);
      }
   
  };

  return (
    <div className="bg-secondary float-left w-100 pt-4">
      <Loader isActive={isActive} />
      {isShow && (
        <Toaster
          types={types}
          isShow={isShow}
          toasterBody={toasterBody}
          message={message}
        />
      )}
      <main className="page-wrapper">
        <div className="content-overlay transaction_tab_view mt-0">
        <div className="row">
          <AllTab />
          {/* <!-- Page container--> */}
          <div className="col-md-12">
            <div className="bg-light steper rounded-3 py-4 px-4 mb-3">
              <h3 className="text-left mb-0 py-2">Add Open House</h3>
            </div>
            <div className="float-start w-100 bg-light steper rounded-3 py-5 px-md-4 mb-3 mt-4">
                <div className="steps pt-4 pb-1 w-lg-75 w-md-75 w-sm-100 w-100 mb-5">
                <div className={`step ${step >= 1 ? "active" : ""}`}>
                  <div className="step-progress">
                    <span className="step-progress-start"></span>
                    <span className="step-progress-end"></span>
                    <span className="step-number">1</span>
                  </div>
                </div>
                <div className={`step ${step >= 2 ? "active" : ""}`}>
                  <div className="step-progress">
                    <span className="step-progress-start"></span>
                    <span className="step-progress-end"></span>
                    <span className="step-number">2</span>
                  </div>
                </div>

                <div className={`step ${step >= 3 ? "active" : ""}`}>
                  <div className="step-progress">
                    <span className="step-progress-start"></span>
                    <span className="step-progress-end"></span>
                    <span className="step-number">3</span>
                  </div>
                </div>

                <div className={`step ${step >= 4 ? "active" : ""}`}>
                  <div className="step-progress">
                    <span className="step-progress-start"></span>
                    <span className="step-progress-end"></span>
                    <span className="step-number">4</span>
                  </div>
                </div>
                <div className={`step ${step >= 5 ? "active" : ""}`}>
                  <div className="step-progress">
                    <span className="step-progress-start"></span>
                    <span className="step-progress-end"></span>
                    <span className="step-number">5</span>
                  </div>
                </div>

                <div className={`step ${step >= 6 ? "active" : ""}`}>
                  <div className="step-progress">
                    <span className="step-progress-start"></span>
                    <span className="step-progress-end"></span>
                    <span className="step-number">6</span>
                  </div>
                </div>

                <div className={`step ${step >= 7 ? "active" : ""}`}>
                  <div className="step-progress">
                    <span className="step-progress-start"></span>
                    <span className="step-progress-end"></span>
                    <span className="step-number">7</span>
                  </div>
                </div>
                {/* Add more steps here if needed */}
              </div>
              {step === 1 && (
                <div className="content-overlay">
                    <div className="float-none m-auto mt-3 w-lg-75 w-md-75 w-sm-100 w-100 border rounded-3 p-4">
                      <label className="col-form-label">Full Name</label>
                      <input
                        className="form-control"
                        type="text"
                        name="fullName" // Corrected the name attribute
                        placeholder="Full Name*"
                        value={formValues.fullName}
                        onChange={handleChange}
                        style={{
                          border: formErrors?.fullName
                            ? "1px solid red"
                            : "1px solid #00000026",
                        }}
                      />
                      <div className="invalid-tooltip">{formErrors.fullName}</div>
                    <div className="float-left w-100 mt-4 mb-0 float-right">
                        <button
                        className="btn btn-info btn-sm"
                          type="button"
                          onClick={nextStep}
                          disabled={!formValues.fullName} // Disable the button if fullName is empty
                        >
                          Next
                        </button>
                      </div>
                    </div>
               
                </div>
              )}
              {step === 2 && (
                <div className="content-overlay">
                    <div className="float-none m-auto mt-3 w-lg-75 w-md-75 w-sm-100 w-100 border rounded-3 p-4">
                      <label className="col-form-label">Mobile Number</label>
                      <input
                        className="form-control"
                        type="number"
                        name="phoneNumber"
                        placeholder="Mobile Number*"
                        value={formValues.phoneNumber}
                        onChange={handleChange}
                      //   style={{
                      //     border: formErrors?.phoneNumber
                      //       ? "1px solid red"
                      //       : "1px solid #00000026",
                      //   }}
                      />

                      <div className="invalid-tooltip">
                        {!formErrors.phoneNumber && (
                          <div className="invalid-tooltip">
                            {formErrors.phoneNumber}
                          </div>
                        )}
                      </div>
                    <div className="float-left w-100 mt-4 mb-0 float-right">
                      <button
                        type="button"
                        className="btn btn-primary btn-sm"
                        onClick={prevStep}
                      >
                        Previous
                      </button>
                      <button
                        className="btn btn-info btn-sm ms-2"
                        type="button"
                        onClick={nextStep}
                        disabled={!/^\d{10}$/.test(formValues.phoneNumber)}
                      >
                        Next
                      </button>
                    </div>
                    </div>
                   
                </div>
              )}
              {step === 3 && (
                <div className="content-overlay">
                    <div className="float-none m-auto mt-3 w-lg-75 w-md-75 w-sm-100 w-100 border rounded-3 p-4">
                      <label className="col-form-label">Email</label>
                      <input
                        className="form-control"
                        type="text"
                        name="email"
                        placeholder="Email*"
                        value={formValues.email}
                        onChange={handleChange}
                      //   style={{
                      //     border: formErrors?.email
                      //       ? "1px solid red"
                      //       : "1px solid #00000026",
                      //   }}
                      />
                      <div className="invalid-tooltip"> {!formErrors.email && (
                        <div className="invalid-tooltip">
                          {formErrors.email}
                        </div>
                      )}</div>
                    <div className="float-left w-100 mt-4 mb-0 float-right">
                      <button
                        type="button"
                        className="btn btn-primary btn-sm"
                        onClick={prevStep}
                      >
                        Previous
                      </button>
                      <button
                        className="btn btn-info btn-sm ms-2"
                        type="button"
                        onClick={nextStep}
                        disabled={!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(formValues.email)}
                      >
                        Next
                      </button>
                    </div>
                    </div>

                
                </div>
              )}
              {step === 4 && (
                <div className="content-overlay">
                  <div className="float-none m-auto mt-3 w-50">
                      <label className="col-form-label">
                        We can connect you with a home finance pro
                      </label>
                      <div className="float-start w-100">
                        <button
                          className="btn btn-info btn-sm"
                          type="button"
                          onClick={() => handleSubmitSound("sounds")}
                        >
                          SOUNDS GOOD
                        </button>
                      </div>
                    <div className="float-left w-100 mt-4 mb-0 float-right">
                      <button
                        type="button"
                        className="btn btn-primary btn-sm"
                        onClick={prevStep}
                      >
                        Previous
                      </button>
                      <button
                        className="btn btn-info btn-sm ms-2"
                        type="button"
                        onClick={nextStep}
                      >
                        Next
                      </button>
                    </div>
                    </div>
                  </div>
               
              )}
              {step === 5 && (
                <div className="content-overlay">
                  <div className="float-none m-auto mt-3 w-50">
                      <label className="col-form-label">
                        Who is your real estate agent?
                      </label>
                      <div className="float-start w-100">
                        {
                          formValues.realStateAgent === "shareagentInfo" ?
                            <>
                              <label className="col-form-label">Share agent info</label>
                              <input
                                className="form-control"
                                type="text"
                                name="agentInfo"
                                placeholder="Provide Details"
                                value={formValues.agentInfo}
                                onChange={handleChange}
                              />
                            </>
                            :
                            <>
                              <select
                                className="form-select"
                                name="realStateAgent"
                                value={formValues.realStateAgent}
                                onChange={handleChange}

                              >
                                <option></option>
                                <option value="shareagentInfo">Share agent info</option>
                                <option value="agent">I am an agent</option>
                                <option value="notAgent">
                                  I'm not working with an agent
                                </option>
                              </select>
                              <div className="invalid-tooltip">
                                {!formErrors.realStateAgent && (
                                  <div className="invalid-tooltip">
                                    {formErrors.realStateAgent}
                                  </div>
                                )}
                              </div>
                            </>
                        }


                      <div className="float-left w-100 mt-4 mb-0 float-right">
                          <button
                            type="button"
                          className="btn btn-primary btn-sm btn-sm"
                            onClick={prevStep}
                          >
                            Previous
                          </button>

                          <button
                          className="btn btn-info btn-sm ms-2"
                            type="button"
                            onClick={nextStep}
                            disabled={!formValues.realStateAgent} // Disable the button if fullName is empty
                          >
                            Next
                          </button>
                  
                      </div>
                    </div>
                  </div>
                </div>
              )}
              {step === 6 && (
                <div className="content-overlay">
                  <div className="float-none m-auto mt-3 w-50">
                      <label className="col-form-label">
                        What's your timeframe to buy a home?
                      </label>
                      <input
                        className="form-control"
                        type="text"
                        name="timeframe" // Corrected the name attribute
                        value={formValues.timeframe}
                        onChange={handleChange}
                      />
                      <div className="invalid-tooltip">
                        {!formErrors.timeframe && (
                          <div className="invalid-tooltip">
                            {formErrors.timeframe}
                          </div>
                        )}
                      </div>

                    <div className="float-left w-100 mt-4 mb-0 float-right">
                      <button
                        type="button"
                        className="btn btn-primary btn-sm"
                        onClick={prevStep}
                      >
                        Previous
                      </button>
                      <button
                        className="btn btn-info btn-sm ms-2"
                        type="button"
                        onClick={nextStep}
                        disabled={!formValues.timeframe} // Disable the button if fullName is empty
                      >
                        Next
                      </button>

                    </div>
                    </div>

              
                </div>
              )}
              {step === 7 && (
                <div className="content-overlay">
                  <div className="float-none m-auto mt-3 w-50">
                      <label className="col-form-label">
                        Do you need to sell a property in order to purchase your
                        next property?
                      </label>
                      <input
                        className="form-control"
                        type="text"
                        name="sellProperty"
                        value={formValues.sellProperty}
                        onChange={handleChange}
                      />
                      <div className="invalid-tooltip">
                        {!formErrors.sellProperty && (
                          <div className="invalid-tooltip">
                            {formErrors.sellProperty}
                          </div>
                        )}
                      </div>
                    <div className="float-left w-100 mt-4 mb-0 float-right">
                      <button
                        className="btn btn-info btn-sm"
                        type="button"
                        onClick={handleSubmit}
                        disabled={!formValues.sellProperty} // Disable the button if fullName is empty
                      >
                        Submit
                      </button>
                    </div>
                  </div>
                </div>
              )}
            </div>
          </div>
        </div>
        </div>
      </main>
    </div>
  );
};

export default OpenHouse;

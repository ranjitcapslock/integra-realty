import React, { useState,useEffect } from 'react';
import { useNavigate,useParams } from "react-router-dom";

import { DayPicker } from 'react-day-picker';
import 'react-day-picker/dist/style.css';
import user_service from '../service/user_service';
import Loader from '../../Pages/Loader/Loader.js';
import Toaster from '../../Pages/Toaster/Toaster';
import AllTab from '../../Pages/AllTab';


const TransactionDate = () => {
    const [selected, setSelected] = useState("<Date>");

    const [loader, setLoader] = useState({ isActive: null })
    const { isActive } = loader;
    const [toaster, setToaster] = useState({ types: null, isShow: null, toasterBody: "", message: "" });
    const { types, isShow, toasterBody, message } = toaster;
    
    const [summary, setSummary] = useState([]);
    const params = useParams();

    useEffect(() => { window.scrollTo(0, 0);
        setLoader({ isActive: true })
        const TransactionGetById = async () => {

            await user_service.transactionGetById(params.id).then((response) => {
                setLoader({ isActive: false })
                if (response) {
                    setSummary(response.data);
                }
            });
        }
        TransactionGetById(params.id)
    }, []);

   
    return (
        <div className="bg-secondary float-left w-100 pt-4">
              <Loader isActive={isActive} />
            {isShow && <Toaster types={types}
                isShow={isShow}
                toasterBody={toasterBody}
                message={message} />}
            <main className="page-wrapper">
                <AllTab transaction_data = {summary}/>
                {/* <!-- Page container--> */}
                <div className="container mb-md-4 ">
                    <div className="bg-light shadow-sm rounded-3 p-2 p-md-5 mb-2">
                        <div className="row">
                            <div className="d-flex align-items-center"><h1 className="h3 mb-3">Transaction Dates & Info</h1><br /></div>
                            {/* <!-- secondary alert --> */}
                            <div className="col-md-9 bg-secondary d-flex py-2">
                                <p className="text-property mb-0 justify-content-center d-flex align-items-center">Dates & Info</p>
                                <p className="mb-0" id="text-document">Details about the transaction are maintained here.Required items are highlighted at the top,and<br />
                                    additional detail can be tracked in the groups below.</p>
                            </div>


                            <div className="col-md-3 py-2 justify-content-end d-flex align-items-center">
                                <a className="btn btn-primary btn-sm  pull-right w-100" href="#">Add a Date</a>
                            </div>


                            {/* <!-- Primary alert --> */}
                            <div className="col-md-9  mt-3">
                                <div className="alert alert-primary col-md-9">
                                    <span id="document">2 Items Required Now</span>
                                    <p id="text-document-two">Please select the appropriate answer for each question below:</p>
                                    <hr />
                                    <span>
                                        <img src="https://cdn.pboffice.net/silk/asterisk_orange.png" alt="property" />
                                        <p id="text-document-three">When did you first being working with your client?&nbsp;&nbsp; &nbsp;Not Set <a href='#'>change</a></p>
                                    </span>

                                    <span className="dates">
                                        <img className="orangeImage" src="https://cdn.pboffice.net/silk/asterisk_orange.png" alt="property" />
                                        <p id="text-document-four">Will another party get a referral fee from you form this transactio?&nbsp;&nbsp; &nbsp;Not Set <a href='#'>change</a></p>
                                    </span>
                                </div>
                            </div>

                            <div className="col-md-9">
                                <span id="document">Do you have additional information to add?</span>
                                <p id="text-document-two">The following information allows you to enter many more details about the transactio.The information collected here is optional<br />
                                    unless otherwise requested above as a required item.</p>
                            </div>

                            <div className="col-md-9 d-flex">
                                <div className="col-md-4 bg-secondary p-3">
                                    <p><b>Contacts</b>
                                        <a className="pull-right" href="#">change</a></p>
                                    <p className="pull-left"><b>Corporate Account</b>: Not Set</p>
                                    <p className="pull-left"><b>First-Time Buyer</b>:Not Set</p>
                                    <p className="pull-left"><b>Price Range Low</b>:Not Set</p>
                                </div><br />


                                <div className="col-md-6 bg-secondary ms-5 p-3">
                                    <p><b>Dates</b>
                                        <a className="pull-right" href="#">change</a></p>
                                    <p className="pull-left border border-black w-100"><b>Start Date</b>:<a className="pull-right">Not Set</a></p>
                                    <p className="pull-left border border-black w-100"><b>Target Date</b>:<a className="pull-right">Not Set</a></p>
                                </div>
                            </div>
                            <div className='col-md-3'>
                                <DayPicker
                                    mode="single"
                                    selected={selected}
                                    onSelect={setSelected}
                                />
                            </div>
                        </div>
                    </div>
                </div>
            </main >
           

        </div >
    )
}

export default TransactionDate
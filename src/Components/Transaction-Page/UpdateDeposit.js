import React, { useEffect, useState } from 'react';
import { useParams } from 'react-router-dom';
import user_service from '../service/user_service';
import Loader from '../../Pages/Loader/Loader.js';
import Toaster from '../../Pages/Toaster/Toaster';
import { NavLink } from 'react-router-dom';
import AllTab from '../../Pages/AllTab';
import moment from "moment-timezone";
import jwt from "jwt-decode";
import Swal from "sweetalert2";
import "sweetalert2/dist/sweetalert2.css";

const UpdateDeposit = ({ props }) => {
    const params = useParams();
    const [loader, setLoader] = useState({ isActive: null });
    const { isActive } = loader;
    const [toaster, setToaster] = useState({ types: null, isShow: null, toasterBody: '', message: '' });
    const { types, isShow, toasterBody, message } = toaster;
    const [summary, setSummary] = useState('');
    const [deposit, setDeposit] = useState([]);

    const [depositAdd, setDepositAdd] = useState(0);
    const [release, setRelease] = useState(0);
    const [lastMonthHolding, setLastMonthHolding] = useState(0);
    const [holdingNow, setHoldingNow] = useState(0)

    useEffect(() => { window.scrollTo(0, 0);
        TransactionGetDeposit(params.id)
        TransactionGetById(params.id);
        setLoader({ isActive: true })
    }, [])

    const TransactionGetById = async () => {
        await user_service.transactionGetById(params.id).then((response) => {
            if (response) {
                setLoader({ isActive: false })
                setSummary(response.data);
            }
        });
    };

    const TransactionGetDeposit = async () => {
        await user_service.transactionDepositGet(params.id).then((response) => {
            if (response && typeof response.data === 'object') {
                setDeposit(response.data);

                let currentdate = new Date();
                let currentmonth = currentdate.getMonth();
                let currentyear = currentdate.getFullYear();



                let lastmonthsaving = 0;
                let lastmonthwithdraw = 0;
                let currentmonthsaving = 0;
                let totalsaving = 0;
                let totaldebit = 0;



                response.data.data.forEach((item) => {
                    const itemDate = new Date(item.entry_date);
                    const itemYear = itemDate.getFullYear();
                    const itemMonth = itemDate.getMonth();

                    if (currentyear === itemYear && currentmonth === itemMonth) {
                        if (item.entry_type === "deposit") {
                            currentmonthsaving += parseInt(item.amount);
                            totalsaving += parseInt(item.amount);
                        } else {
                            currentmonthsaving -= parseInt(item.amount);
                            totaldebit += parseInt(item.amount);
                        }
                    }
                    else {
                        if (item.entry_type === "deposit") {
                            lastmonthsaving += parseInt(item.amount);

                        } else if (item.entry_type === "release") {
                            lastmonthwithdraw += parseInt(item.amount);
                        }

                    }
                });
                setLastMonthHolding(lastmonthsaving - lastmonthwithdraw);
                setHoldingNow(lastmonthsaving - lastmonthwithdraw + totalsaving - totaldebit);
                setDepositAdd(totalsaving);
                setRelease(totaldebit);
            }
        });
    }; 

    const handleDeleteTransactionDepositpermanent = async (id) => {
        Swal.fire({
          title: "Are you sure?",
          text: "It will be permanently deleted!",
          icon: "warning",
          showCancelButton: true,
          confirmButtonColor: "#3085d6",
          cancelButtonColor: "#d33",
          confirmButtonText: "Yes, delete it!",
        }).then(async (result) => {
          if (result.isConfirmed) {
            try {
              const response = await user_service.transactionDepositDelete(id);
              if (response) {
                   
                    if (response) {
                      Swal.fire(
                        "Deleted!",
                        "Deposit is deleted permanently.",
                        "success"
                      );
                      //setFormValues(response.data);
                    }
                    TransactionGetDeposit(params.id)
                    TransactionGetById(params.id);
                    setLoader({ isActive: false });
              }
            } catch (error) {
              Swal.fire(
                "Error",
                "An error occurred while deleting the file.",
                "error"
              );
              console.error(error);
            }
          }
        });
      };


    return (
        <div className="bg-secondary float-left w-100 pt-4 mb-4">
            <Loader isActive={isActive} />
            {isShow && <Toaster types={types} isShow={isShow} toasterBody={toasterBody} message={message} />}
            <main className="page-wrapper">
              

                <div className="content-overlay transaction_tab_view mt-0">
                        <div className="row">
                            <AllTab/>
                        
                            <div className="col-md-12">
                            <div className="float-left w-100 d-flex align-items-center justify-content-between mb-4">
                                <h3 className="text-white mb-0" id="">
                                Ledger of Deposits
                                </h3>
                            </div>
                                <div className="bg-light border rounded-3 p-3">
                                    <div className="">
                                        <p className="subtitle">Log earnest money / trust fund deposits and releases here.</p>
                                        <div className="table-responsive">
                                        <table id="DepositLedger" className="table table-striped mb-0" width="100%" border="0" cellSpacing="0" cellPadding="0">
                                            <thead>
                                                <tr>
                                                    <th>Entry Date</th>
                                                    <th>Earnest Money Date</th>
                                                    <th className="hright">Payer</th>
                                                    <th className="hright">Check #</th>
                                                    <th>Notes</th>
                                                    <th className="hright">Amount</th>
                                                    <th>Action</th>
                                                    <th>Remove</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                {

                                                    deposit.data ?
                                                        deposit.data.length > 0 ?

                                                            deposit.data.map((item) => {

                                                                return (
                                                                    <tr>
                                                                        <td><span className="SuppressText">{moment(item.entry_date).format('M/D/YYYY')}</span></td>
                                                                        <td><span className="SuppressText">{moment(item.date_earnest).format('M/D/YYYY')}</span></td>
                                                                        <td><span className="SuppressText">{item.payer ? item.payer : "N/A"}</span></td>
                                                                    
                                                                        <td><span className="SuppressText">{item.check ? item.check : "N/A"}</span></td>
                                                                        <td><span className="SuppressText">{item.memo ? item.memo : "N/A"}</span></td>
                                                                        <td><span className="SuppressText">${item.amount}</span></td>
                                                                        <td><NavLink to={`/deposit-add/${params.id}/${item._id}`}>Edit</NavLink></td>
                                                                        <td>
                                                                        {localStorage.getItem("auth") &&
                                                                            jwt(localStorage.getItem("auth")).contactType ==
                                                                                "admin" ? (
                                                                                    <div className="">
                                                                                    <NavLink onClick={(e) => handleDeleteTransactionDepositpermanent(item._id)} to="#" className="text-center " type="button"><i className="fa fa-trash-o me-2" aria-hidden="true"></i>Delete</NavLink>
                                                                                    </div>
                                                                            ) : (
                                                                                ""
                                                                            )}    
                                                                        </td>

                                                                    </tr>
                                                                )
                                                            })
                                                            :
                                                            ""
                                                        :
                                                        ""
                                                }
                                            </tbody>
                                        </table>
                                        </div>
                                        <br />
                                        <div className="table-responsive">
                                        <table id="DepositTotals" className="table table-striped" width="80%" border="0" cellSpacing="0" cellPadding="0">
                                            <colgroup>
                                                <col width="25%" />
                                                <col width="25%" />
                                                <col width="25%" />
                                                <col width="25%" />
                                            </colgroup>
                                            <thead>
                                                <tr>
                                                    <th className="hright">Last Month Holding</th>
                                                    <th className="hright">Deposits this month</th>
                                                    <th className="hright">Released this month</th>
                                                    <th className="hright">Holding Now</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td className="hright">${lastMonthHolding}</td>
                                                    <td className="hright">${depositAdd}</td>
                                                    <td className="hright">${release}</td>
                                                    <td className="hright">${holdingNow}</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                        </div>
                                    </div>
                                </div>
                                <div className="pull-right mt-4">
                                    <NavLink to={`/deposit-add/${params.id}`} className="btn btn-primary btn-sm">Add Entry</NavLink>
                                    <NavLink className="btn btn-secondary btn-sm ms-3" to={`/transaction-review/${params.id}`}>Return to Review</NavLink>
                                </div>
                            </div>

                        </div>
                </div>
            </main>
            
        </div>
    );
};

export default UpdateDeposit;

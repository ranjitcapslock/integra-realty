import React, { useState, useEffect } from "react";
import { useParams } from "react-router-dom";
import user_service from "../service/user_service";
import Loader from "../../Pages/Loader/Loader.js";
import Toaster from "../../Pages/Toaster/Toaster";
import Pic from "../img/pic.png";
// import defaultpropertyimage from "../../Components/img/defaultpropertyimage.jpeg";
// import moment from "moment-timezone";
import DateTimePicker from "react-datetime-picker";
import "react-datetime-picker/dist/DateTimePicker.css";
import "react-clock/dist/Clock.css";
import AllTab from "../../Pages/AllTab.js";
import jwt from "jwt-decode";
// import _ from "lodash";
import axios from "axios";
// import StatusBar from "../../Pages/StatusBar.js";

const Commissions = () => {
  const [loader, setLoader] = useState({ isActive: null });
  const { isActive } = loader;
  const [toaster, setToaster] = useState({
    types: null,
    isShow: null,
    toasterBody: "",
    message: "",
  });
  const { types, isShow, toasterBody, message } = toaster;
  const [transactionId, setTransactionId] = useState([]);

  const params = useParams();

  const [formValues, setFormValues] = useState({
    purchase_price: "",
    gross_commission: "",
    totalgrosscommission_type: "dollar",
    buyerscommission_type: "buyerdoller",
    buyers_agentcommission: "",
    listing_agentcommission: "",
    listingcommission_type: "listingdoller",
    acceptance_date: "",
    earnest_amount: "",
    earnest_deadline: "",
    money_beingheld: "",
    new_title: "",
    cash_deal: "",
    licensed_realestateagent: "",
    property_interest: "",
    transaction_coordinator: "",
    tc_fee: "",
  });

  // const [formErrors, setFormErrors] = useState({});

  const [formErrors, setFormErrors] = useState({
    purchase_price: "Purchase price is required",
    acceptance_date: "Acceptance date is required",
    earnest_amount: "Earnest amount is required",
    earnest_deadline: "Earnest deadline is required",
    money_beingheld: "Money beingheld is required",
    cash_deal: "Cash deal is required",
    licensed_realestateagent: "Licensed real estate agent is required",
    property_interest: "This field is required",
    transaction_coordinator: "Transaction coordinator is required",
    tc_fee: "TC Fee is required",
  });

  const [formValuesNew, setFormValuesNew] = useState({
    referral_type: "no_referral",
    referral_firstname: "",
    referral_lastname: "",
    referral_commission: "",
    referral_email: "",
    referral_phone: "",
    referral_brokeragename: "",
    referral_companycommission: "referral-doller",
    pdfw9: "",
    referral_agreement: "",
  });

  useEffect(() => {}, [formValuesNew.referral_type]);

  // Initialize form errors

  // Conditionally add errors based on referral type

  const [formErrorsReferral, setFormErrorsReferral] = useState({});

  useEffect(() => {
    if (formValuesNew.referral_type === "external_referral") {
      setFormErrorsReferral({
        referral_firstname: "First Name is required",
        referral_lastname: "Last Name is required",
        referral_email: "Referral email is required",
        referral_phone: "Phone is required",
        referral_brokeragename: "Brokerage Name is required",
        referral_commission: "Referral Company Commissions is required",
        pdfw9: "W9 file is required",
        referral_agreement: "Referral Agreement file is required",
      });
    } else if (formValuesNew.referral_type === "internal_referral") {
      setFormErrorsReferral({
        referral_firstname: "First Name is required",
        referral_lastname: "Last Name is required",
        referral_email: "Referral email is required",
        referral_phone: "Phone is required",
        referral_brokeragename: "Brokerage Name is required",
        referral_commission: "Referral Company Commissions is required",
      });
    } else {
      setFormErrorsReferral({
        referral_type: "Referral Type is required",
      });
    }
  }, [formValuesNew.referral_type]);

  // const [isSubmitClickReferral, setISSubmitClickReferral] = useState(false);

  const [formValuesClose, setFormValuesClose] = useState({
    title_company: "",
    companyname: "",
    file_gfnumber: "",
    contact_number: "",
    contactemail: "",
    message: "",
    closing_location: "",
    closing_address: "",
    closing_datetime: "",
  });

  const [formErrorsClose, setFormErrorsClose] = useState({
    title_company: "Company Title is required",
    companyname: "Escrow officer is required",
    file_gfnumber: "File GF Number is required",
    contact_number: "Contact Number is required",
    contactemail: "Contact Email is required",
    // message: "Messages is required",
  });

  const [data, setData] = useState("");
  const [file, setFile] = useState(null);
  const [fileExtension, setFileExtension] = useState("");
  const [acceptedFileTypes, setAcceptedFileTypes] = useState([]);
  const [showSubmitButton, setShowSubmitButton] = useState(false);

  const [dataAgreement, setDataAgreement] = useState("");
  const [fileAgreement, setFileAgreement] = useState(null);
  const [fileExtensionAgreement, setFileExtensionAgreement] = useState("");
  const [acceptedFileTypesAgreement, setAcceptedFileTypesAgreement] = useState(
    []
  );
  const [showSubmitButtonAgreement, setShowSubmitButtonAgreement] =
    useState(false);

  const [transactionFundingRequest, setTransactionFundingRequest] =
    useState("");

  const TransactionFundingRequest = async () => {
    await user_service.transactionFundingRequest(params.id).then((response) => {
      if (response) {
        setLoader({ isActive: false });
        setTransactionFundingRequest(response.data.data[0]);
      }
    });
  };

  useEffect(() => {
    window.scrollTo(0, 0);
    setLoader({ isActive: true });
    TransactionFundingRequest(params.id);
  }, []);

  const handleChange = (e) => {
    const { name, value } = e.target;
    setFormValues({ ...formValues, [name]: value });

    if (!value && formErrors[name]) {
      setFormErrors({ ...formErrors, [name]: `${name} is required` });
    } else if (formErrors[name]) {
      setFormErrors({ ...formErrors, [name]: "" });
    }
  };

  const [isPercentage, setIsPercentage] = useState("$");
  const [isPercentageBuyer, setIsPercentageBuyer] = useState("$");
  const [isPercentageReferral, setIsPercentageReferral] = useState("$");

  const [isPercentageListing, setIsPercentageListing] = useState("$");

  const handleChangePrice = (e) => {
    const { name, value } = e.target;
    const numericValue = value.replace(/[^\d.]/g, "");

    if (numericValue !== "") {
      const floatValue = parseFloat(numericValue);

      if (!isNaN(floatValue)) {
        setFormValues((prevValues) => ({
          ...prevValues,
          [name]: floatValue.toLocaleString("en-US", {
            style: "currency",
            currency: "USD",
            minimumFractionDigits: 0,
          }),
        }));
      }
    } else {
      setFormValues((prevValues) => ({
        ...prevValues,
        [name]: "",
      }));
    }

    if (!value) {
      setFormErrors({ ...formErrors, [name]: `${name} is required` });
    } else if (formErrors[name]) {
      const updatedErrors = { ...formErrors };
      delete updatedErrors[name];
      setFormErrors(updatedErrors);
    }
  };

  const handleRadioChange = (e) => {
    setIsPercentage(e.target.value === "percentage");
    setFormValues((prevState) => ({
      ...prevState,
      totalgrosscommission_type: e.target.value,
      gross_commission: formValues.gross_commission,
    }));
  };

  const CheckBuyerCommission = (e) => {
    setIsPercentageBuyer(e.target.value === "buyerpercentage");
    setFormValues((prevState) => ({
      ...prevState,
      buyerscommission_type: e.target.value,
      buyers_agentcommission: formValues.buyers_agentcommission,
    }));
  };

  const handleChangeBuyers = (e) => {
    const { name, value } = e.target;
    const newValue = value.replace(/[^\d,.]/g, "");
    setFormValues({
      ...formValues,
      [name]: newValue,
    });
  };

  const CheckListingCommission = (e) => {
    setIsPercentageListing(e.target.value === "listingpercentage");
    setFormValues((prevState) => ({
      ...prevState,
      listingcommission_type: e.target.value,
      listing_agentcommission: formValues.listing_agentcommission,
    }));
  };

  const CheckBeingheldCommission = (event) => {
    const select = event.target.value;
    setFormValues({
      ...formValues,
      money_beingheld: select,
    });
    if (!select) {
      setFormErrors({
        ...formErrors,
        money_beingheld: "Money beingheld is required",
      });
    } else {
      setFormErrors({ ...formErrors, money_beingheld: "" });
    }
  };

  const CheckCashDeal = (event) => {
    const select = event.target.value;
    setFormValues({
      ...formValues,
      cash_deal: select,
    });

    if (!select) {
      setFormErrors({ ...formErrors, cash_deal: "Cash deal is required" });
    } else {
      setFormErrors({ ...formErrors, cash_deal: "" });
    }
  };

  const CheckLicensed = (event) => {
    const select = event.target.value;
    setFormValues({
      ...formValues,
      licensed_realestateagent: select,
    });

    if (!select) {
      setFormErrors({
        ...formErrors,
        licensed_realestateagent: "Licensed real estate agent is required",
      });
    } else {
      setFormErrors({ ...formErrors, licensed_realestateagent: "" });
    }
  };

  const CheckProperty = (event) => {
    const select = event.target.value;
    setFormValues({
      ...formValues,
      property_interest: select,
    });

    if (!select) {
      setFormErrors({
        ...formErrors,
        property_interest: "Property interest is required",
      });
    } else {
      setFormErrors({ ...formErrors, property_interest: "" });
    }
  };

  const CheckTransactionCoordinator = (event) => {
    const select = event.target.value;
    setFormValues({
      ...formValues,
      transaction_coordinator: select,
    });

    if (!select) {
      setFormErrors({
        ...formErrors,
        transaction_coordinator: "Transaction coordinator is required",
      });
    } else {
      setFormErrors({ ...formErrors, transaction_coordinator: "" });
    }
  };

  const handleSubmit = async (e) => {
    if (formValues?._id) {
      e.preventDefault();
      const userData = {
        agentId: jwt(localStorage.getItem("auth")).id,
        purchase_price: formValues.purchase_price,
        totalgrosscommission_type:
          formValues.totalgrosscommission_type ?? "dollar",
        gross_commission: formValues.gross_commission,
        buyerscommission_type:
          formValues.buyerscommission_type ?? "buyerdoller",
        buyers_agentcommission: formValues.buyers_agentcommission,
        listingcommission_type:
          formValues.listingcommission_type ?? "listingdoller",
        listing_agentcommission: formValues.listing_agentcommission,
        acceptance_date: formValues.acceptance_date,
        earnest_amount: formValues.earnest_amount,
        earnest_deadline: formValues.earnest_deadline,
        money_beingheld: formValues.money_beingheld,
        cash_deal: formValues.cash_deal,
        new_title: formValues.new_title,
        transactionId: params.id,
        licensed_realestateagent: formValues.licensed_realestateagent,
        property_interest: formValues.property_interest,
        transaction_coordinator: formValues.transaction_coordinator,
        tc_fee: formValues.tc_fee,
      };
      try {
        setLoader({ isActive: true });
        const response = await user_service.commissionUpdate(
          formValues._id,
          userData
        );
        if (response) {
          const userDatan = {
            addedBy: jwt(localStorage.getItem("auth")).id,
            agentId: jwt(localStorage.getItem("auth")).id,
            transactionId: params.id,
            message: "Transaction Commissions Details Update",
            note_type: "Commissions",
          };
          const responsen = user_service.transactionNotes(userDatan);

          setLoader({ isActive: false });
          setToaster({
            types: "Commissions",
            isShow: true,
            message: "Commissions Updated Successfully",
          });
          setTimeout(() => {
            setToaster((prevToaster) => ({
              ...prevToaster,
              isShow: false,
            }));
          }, 2000);
        } else {
          setToaster({
            types: "error",
            isShow: true,
            message: "Error",
          });
        }
      } catch (error) {
        setLoader({ isActive: false });
        setToaster({
          types: "error",
          isShow: true,
          message: "Error",
        });

        setTimeout(() => {
          setToaster((prevToaster) => ({
            ...prevToaster,
            isShow: false,
          }));
        }, 2000);
      }
      // }
    } else {
      e.preventDefault();
      // for (const key in formErrors) {
      //   if (formErrors[key]) {
      //     // If there's any error, display the toaster message and exit
      //     setToaster({
      //       types: "error",
      //       isShow: true,
      //       toasterBody: "Please fill in all required fields.",
      //       message: "Error",
      //     });

      //     setTimeout(() => {
      //       setToaster((prevToaster) => ({
      //         ...prevToaster,
      //         isShow: false,
      //       }));
      //     }, 2000);

      //     return;
      //   }
      // }

      // If no errors, proceed with form submission
      const userData = {
        agentId: jwt(localStorage.getItem("auth")).id,
        purchase_price: formValues.purchase_price,
        totalgrosscommission_type:
          formValues.totalgrosscommission_type ?? "dollar",
        gross_commission: formValues.gross_commission,
        buyerscommission_type:
          formValues.buyerscommission_type ?? "buyerdoller",
        buyers_agentcommission: formValues.buyers_agentcommission,
        listingcommission_type:
          formValues.listingcommission_type ?? "listingdoller",
        listing_agentcommission: formValues.listing_agentcommission,
        acceptance_date: formValues.acceptance_date,
        earnest_amount: formValues.earnest_amount,
        earnest_deadline: formValues.earnest_deadline,
        money_beingheld: formValues.money_beingheld,
        cash_deal: formValues.cash_deal,
        transactionId: params.id,
        licensed_realestateagent: formValues.licensed_realestateagent,
        property_interest: formValues.property_interest,
        transaction_coordinator: formValues.transaction_coordinator,
        tc_fee: formValues.tc_fee,
      };
      try {
        setLoader({ isActive: true });
        const response = await user_service.commissionPost(userData);

        if (response) {
          const userDatan = {
            addedBy: jwt(localStorage.getItem("auth")).id,
            agentId: jwt(localStorage.getItem("auth")).id,
            transactionId: params.id,
            message: "Transaction Commissions Details Add ",
            note_type: "Commissions",
          };
          const responsen = user_service.transactionNotes(userDatan);

          setLoader({ isActive: false });
          setToaster({
            types: "Commissions",
            isShow: true,
            message: "Commissions Added Successfully",
          });
          setTimeout(() => {
            setToaster((prevToaster) => ({
              ...prevToaster,
              isShow: false,
            }));
          }, 2000);
        } else {
          setToaster({
            types: "error",
            isShow: true,
            message: "Error",
          });
        }
      } catch (error) {
        setLoader({ isActive: false });
        setToaster({
          types: "error",
          isShow: true,
          message: "Error",
        });

        setTimeout(() => {
          setToaster((prevToaster) => ({
            ...prevToaster,
            isShow: false,
          }));
        }, 2000);
      }
    }
  };

  const CommissionGet = async () => {
    setLoader({ isActive: true });
    await user_service.commissionGet(params.id).then((response) => {
      if (response) {
        setLoader({ isActive: false });
        setFormValues(response.data.data[0] || {});
      }
    });
  };

  const TransactionGetById = async () => {
    try {
      const response = await user_service.transactionGetById(params.id);
      if (response) {
        setLoader({ isActive: false });
        setTransactionId(response.data);
      }
    } catch (error) {
      console.error(error);
    }
  };

  useEffect(() => {
    window.scrollTo(0, 0);
    setLoader({ isActive: true });
    TransactionGetById(params.id);
    CommissionGet();
    ReferralGet();
    TransactionClosing();
  }, []);

  const handleChanges = (e) => {
    const { name, value } = e.target;
    setFormValuesNew({ ...formValuesNew, [name]: value });
    if (!value && formErrorsReferral[name]) {
      setFormErrorsReferral({
        ...formErrorsReferral,
        [name]: `${name} is required`,
      });
    } else if (formErrorsReferral[name]) {
      setFormErrorsReferral({ ...formErrorsReferral, [name]: "" });
    }
  };

  const CheckReferralType = (event) => {
    const select = event.target.value;
    console.log(select);
    setFormValuesNew({
      ...formValuesNew,
      referral_type: select,
    });
  };

  const CheckCompanyCommission = (e) => {
    setIsPercentageReferral(e.target.value === "referral-percentage");
    setFormValuesNew((prevState) => ({
      ...prevState,
      referral_companycommission: e.target.value,
      referral_commission: formValuesNew.referral_commission,
    }));
  };

  const handleChangesReferralcommission = (e) => {
    const { name, value } = e.target;
    const newValue = value.replace(/[^\d,.]/g, "");

    setFormValuesNew({
      ...formValuesNew,
      [name]: newValue,
    });
    setFormErrorsReferral((prevFormErrors) => ({
      ...prevFormErrors,
      referral_commission: newValue
        ? ""
        : "Referral Company Commissions is required",
    }));
  };

  const handleFileUpload = async (e) => {
    const selectedFile = e.target.files[0];

    if (selectedFile) {
      setFile(selectedFile);
      setFileExtension(selectedFile.name.split(".").pop());
      setShowSubmitButton(true);

      const formData = new FormData();
      formData.append("file", selectedFile);
      const config = {
        headers: {
          Accept: "application/json",
          "Content-Type": "multipart/form-data",
          Authorization: `Bearer ${localStorage.getItem("auth")}`,
        },
      };

      try {
        setLoader({ isActive: true });
        const uploadResponse = await axios.post(
          "https://api.brokeragentbase.com/upload",
          formData,
          config
        );
        const uploadedFileData = uploadResponse.data;
        setData(uploadedFileData);
        setLoader({ isActive: false });
        setToaster({
          type: "Document Uploaded Successfully",
          isShow: true,
          // toasterBody: response.data.message,
          message: "Document Uploaded Successfully",
        });
        setTimeout(() => {
          setToaster((prevToaster) => ({ ...prevToaster, isShow: false }));
        }, 1000);
      } catch (error) {
        setLoader({ isActive: false });
        setToaster({
          types: "error",
          isShow: true,
          message: "Error",
        });

        setTimeout(() => {
          setToaster((prevToaster) => ({ ...prevToaster, isShow: false }));
        }, 1000);
      }
    }
    setFormErrorsReferral((prevErrors) => ({
      ...prevErrors,
      pdfw9: "",
    }));
  };

  const handleFileAgreement = async (e) => {
    const selectedFile = e.target.files[0];

    if (selectedFile) {
      setFileAgreement(selectedFile);
      setFileExtensionAgreement(selectedFile.name.split(".").pop());
      setShowSubmitButtonAgreement(true);

      const formData = new FormData();
      formData.append("file", selectedFile);
      const config = {
        headers: {
          Accept: "application/json",
          "Content-Type": "multipart/form-data",
          Authorization: `Bearer ${localStorage.getItem("auth")}`,
        },
      };

      try {
        setLoader({ isActive: true });
        const uploadResponse = await axios.post(
          "https://api.brokeragentbase.com/upload",
          formData,
          config
        );
        const uploadedFileData = uploadResponse.data;
        setDataAgreement(uploadedFileData);
        setLoader({ isActive: false });
        setToaster({
          type: "Document Uploaded Successfully",
          isShow: true,
          // toasterBody: response.data.message,
          message: "Document Uploaded Successfully",
        });
        setTimeout(() => {
          setToaster((prevToaster) => ({ ...prevToaster, isShow: false }));
        }, 1000);
      } catch (error) {
        setLoader({ isActive: false });
        setToaster({
          types: "error",
          isShow: true,
          message: "Error",
        });

        setTimeout(() => {
          setToaster((prevToaster) => ({ ...prevToaster, isShow: false }));
        }, 1000);
      }
    }

    setFormErrorsReferral((prevErrors) => ({
      ...prevErrors,
      referral_agreement: "",
    }));
  };

  const handleSubmitReferral = async (e) => {
    if (formValuesNew?._id) {
      e.preventDefault();
      const userData = {
        agentId: jwt(localStorage.getItem("auth")).id,
        referral_type: formValuesNew.referral_type ?? "",
        referral_firstname: formValuesNew.referral_firstname ?? "",
        referral_lastname: formValuesNew.referral_lastname ?? "",
        referral_email: formValuesNew.referral_email ?? "",
        referral_phone: formValuesNew.referral_phone ?? "",
        referral_brokeragename: formValuesNew.referral_brokeragename ?? "",
        referral_commission: formValuesNew.referral_commission ?? "",
        referral_companycommission:
          formValuesNew.referral_companycommission ?? "",
        pdfw9: data ?? "",
        transactionId: params.id ?? "",
        referral_agreement: dataAgreement ?? "",
      };

      try {
        setLoader({ isActive: true });
        const response = await user_service.referralUpdate(
          formValuesNew?._id,
          userData
        );

        if (response) {
          const userDatan = {
            addedBy: jwt(localStorage.getItem("auth")).id,
            agentId: jwt(localStorage.getItem("auth")).id,
            transactionId: params.id,
            message: "Transaction Referral Details Update ",
            note_type: "Referral",
          };
          const responsen = user_service.transactionNotes(userDatan);

          setLoader({ isActive: false });
          setToaster({
            types: "Referral",
            isShow: true,
            toasterBody: response.data.message,
            message: "Referral Updated Successfully",
          });
          setTimeout(() => {
            setToaster((prevToaster) => ({
              ...prevToaster,
              isShow: false,
            }));
          }, 2000);
        } else {
          setToaster({
            types: "error",
            isShow: true,
            toasterBody: "Error while adding referral.",
            message: "Error",
          });
        }
      } catch (error) {
        setLoader({ isActive: false });
        setToaster({
          types: "error",
          isShow: true,
          toasterBody: error.response?.data.message || "An error occurred",
          message: "Error",
        });

        setTimeout(() => {
          setToaster((prevToaster) => ({
            ...prevToaster,
            isShow: false,
          }));
        }, 2000);
      }
    } else {
      e.preventDefault();
      // let checkValue = validateReferral();
      // if (_.isEmpty(checkValue)) {
      if (
        formValuesNew.referral_type === "external_referral" ||
        formValuesNew.referral_type === "internal_referral"
      ) {
        e.preventDefault();
        // setISSubmitClickReferral(true);
        // let checkValue = validateReferral();
        // if (_.isEmpty(checkValue)) {

        // for (const key in formErrorsReferral) {
        //   if (formErrorsReferral[key]) {
        //     // If there's any error, display the toaster message and exit
        //     setToaster({
        //       types: "error",
        //       isShow: true,
        //       toasterBody: "Please fill in all required fields.",
        //       message: "Error",
        //     });

        //     setTimeout(() => {
        //       setToaster((prevToaster) => ({
        //         ...prevToaster,
        //         isShow: false,
        //       }));
        //     }, 2000);

        //     return;
        //   }
        // }
        const userData = {
          agentId: jwt(localStorage.getItem("auth")).id,
          referral_type: formValuesNew.referral_type ?? "",
          referral_firstname: formValuesNew.referral_firstname ?? "",
          referral_lastname: formValuesNew.referral_lastname ?? "",
          referral_email: formValuesNew.referral_email ?? "",
          referral_phone: formValuesNew.referral_phone ?? "",
          referral_brokeragename: formValuesNew.referral_brokeragename ?? "",
          referral_commission: formValuesNew.referral_commission ?? "",
          referral_companycommission:
            formValuesNew.referral_companycommission ?? "referral-doller",
          pdfw9: data ?? "",
          transactionId: params.id ?? "",
          referral_agreement: dataAgreement ?? "",
        };
        try {
          setLoader({ isActive: true });
          const response = await user_service.referralPost(userData);
          if (response) {
            const userDatan = {
              addedBy: jwt(localStorage.getItem("auth")).id,
              agentId: jwt(localStorage.getItem("auth")).id,
              transactionId: params.id,
              message: "Transaction Referral Details Add ",
              // transaction_property_address: "ADDRESS 2",
              note_type: "Referral",
            };
            const responsen = user_service.transactionNotes(userDatan);
            setLoader({ isActive: false });
            setToaster({
              types: "Referral",
              isShow: true,
              message: "Referral Added Successfully",
            });
            setTimeout(() => {
              setToaster((prevToaster) => ({
                ...prevToaster,
                isShow: false,
              }));
            }, 2000);
          } else {
            setToaster({
              types: "error",
              isShow: true,
              message: "Error",
            });
          }
        } catch (error) {
          setLoader({ isActive: false });
          setToaster({
            types: "error",
            isShow: true,
            message: "Error",
          });

          setTimeout(() => {
            setToaster((prevToaster) => ({
              ...prevToaster,
              isShow: false,
            }));
          }, 2000);
        }
        // }
      }
      // setFormErrorsReferral
      else {
        // if (formValuesNew.referral_type) {
        const userData = {
          agentId: jwt(localStorage.getItem("auth")).id,
          referral_type: formValuesNew.referral_type ?? "no_referral",
          referral_firstname: formValuesNew.referral_firstname ?? "",
          referral_lastname: formValuesNew.referral_lastname ?? "",
          referral_email: formValuesNew.referral_email ?? "",
          referral_phone: formValuesNew.referral_phone ?? "",
          referral_brokeragename: formValuesNew.referral_brokeragename ?? "",
          referral_commission: formValuesNew.referral_commission ?? "",
          referral_companycommission:
            formValuesNew.referral_companycommission ?? "referral-doller",
          pdfw9: data ?? "",
          transactionId: params.id,
          referral_agreement: dataAgreement ?? "",
        };

        try {
          setLoader({ isActive: true });
          const response = await user_service.referralPost(userData);

          if (response) {
            const userDatan = {
              addedBy: jwt(localStorage.getItem("auth")).id,
              agentId: jwt(localStorage.getItem("auth")).id,
              transactionId: params.id,
              message: "Transaction Referral Details Add ",
              // transaction_property_address: "ADDRESS 2",
              note_type: "Referral",
            };
            const responsen = user_service.transactionNotes(userDatan);

            setLoader({ isActive: false });
            setToaster({
              types: "Referral",
              isShow: true,
              message: "Referral Added Successfully",
            });
            setTimeout(() => {
              setToaster((prevToaster) => ({
                ...prevToaster,
                isShow: false,
              }));
            }, 2000);
          } else {
            setToaster({
              types: "error",
              isShow: true,
              message: "Error",
            });
          }
        } catch (error) {
          setLoader({ isActive: false });
          setToaster({
            types: "error",
            isShow: true,
            message: "Error",
          });

          setTimeout(() => {
            setToaster((prevToaster) => ({
              ...prevToaster,
              isShow: false,
            }));
          }, 2000);
        }
      }
    }
  };

  const ReferralGet = async () => {
    setLoader({ isActive: true });
    await user_service.referralGet(params.id).then((response) => {
      if (response) {
        setLoader({ isActive: false });
        setFormValuesNew(response.data.data[0] || {});
      }
    });
  };

  const handleDateChange = (value) => {
    setFormValuesClose({
      ...formValuesClose,
      closing_datetime: value,
    });
  };
  


  const [contacts, setContacts] = useState("");
  const [getContact, setGetContact] = useState([]);

  const handleClose = (e) => {
    const { name, value } = e.target;

    // Update form values
    setFormValuesClose((prevValues) => ({
      ...prevValues,
      [name]: value,
    }));

    if (name === "companyname" && value.trim()) {
      SearchGetAll(value); // Pass the updated value to the API
    }

    // Handle validation errors
    if (!value && formErrorsClose[name]) {
      setFormErrorsClose((prevErrors) => ({
        ...prevErrors,
        [name]: `${name} is required`,
      }));
    } else if (formErrorsClose[name]) {
      setFormErrorsClose((prevErrors) => ({
        ...prevErrors,
        [name]: "",
      }));
    }
  };

  const SearchGetAll = async (searchValue) => {
    try {
      // Ensure the searchValue is passed dynamically
      const response = await user_service.SearchContactGet(1, 10, searchValue);

      if (response && response.data) {
        setGetContact(response.data.data);
      } else {
        setGetContact([]); // Reset if no data is found
      }
    } catch (error) {
      console.error("Error fetching contacts:", error);
      setGetContact([]); // Reset on error
    }
  };

  const handleContactAssociate = (contact) => {
    setContacts(contact);
    setGetContact([]);
    setFormValuesClose((prevValues) => ({
      ...prevValues,
      title_company: contact.company || "",
      companyname: contact.firstName || "",
      contact_number: contact.phone || "",
      contactemail: contact.email || "",
    }));
  };

  // const handleContactAssociate = (contact) => {
  //   setContacts(contact);
  //   setGetContact([]);
  // };

  // const handleClose = (e) => {
  //   const { name, value } = e.target;
  //   setFormValuesClose({ ...formValuesClose, [name]: value });
  //   setGetContact([])
  //   if(contacts){

  //   }
  //   else{
  //     if (!value && formErrorsClose[name]) {
  //       setFormErrorsClose({ ...formErrorsClose, [name]: `${name} is required` });
  //     } else if (formErrorsClose[name]) {
  //       setFormErrorsClose({ ...formErrorsClose, [name]: "" });
  //     }
  //   }
  // };

  // useEffect(() => {
  //     SearchGetAll();
  // }, []);

  const handleSubmitClose = async (e) => {
    if (formValuesClose?._id) {
      e.preventDefault();
      const userData = {
        agentId: jwt(localStorage.getItem("auth")).id,
        title_company: formValuesClose.title_company,
        companyname: formValuesClose.companyname,
        file_gfnumber: formValuesClose.file_gfnumber,
        contact_number: formValuesClose.contact_number,
        contactemail: formValuesClose.contactemail,
        message: formValuesClose.message,
        transactionId: params.id,
        closing_location: formValuesClose.closing_location,
        closing_address: formValuesClose.closing_address,
        closing_datetime: formValuesClose.closing_datetime,
      };
      try {
        setLoader({ isActive: true });
        const response = await user_service.transactionClosingUpdate(
          formValuesClose?._id,
          userData
        );
        if (response) {
          const userDatan = {
            addedBy: jwt(localStorage.getItem("auth")).id,
            agentId: jwt(localStorage.getItem("auth")).id,
            transactionId: params.id,
            message: "Transaction Closing Company Details Update ",
            // transaction_property_address: "ADDRESS 2",
            note_type: "Closing Company",
          };
          const responsen = user_service.transactionNotes(userDatan);

          setLoader({ isActive: false });
          setContacts("");
          setGetContact([]);
          setToaster({
            types: "Closing Company",
            isShow: true,
            message: "Closing Company Updated Successfully",
          });
          setTimeout(() => {
            setToaster((prevToaster) => ({
              ...prevToaster,
              isShow: false,
            }));
          }, 2000);
        } else {
          setToaster({
            types: "error",
            isShow: true,
            message: "Error while adding commission.",
          });
        }
      } catch (error) {
        setLoader({ isActive: false });
        setToaster({
          types: "error",
          isShow: true,
          message: "Error",
        });

        setTimeout(() => {
          setToaster((prevToaster) => ({
            ...prevToaster,
            isShow: false,
          }));
        }, 2000);
      }
    } else {
      if (
        transactionId.type === "buy_referral" ||
        transactionId.type === "sale_referral" ||
        transactionId.type === "both_referral"
      ) {
        e.preventDefault();
        const userData = {
          agentId: jwt(localStorage.getItem("auth")).id,
          title_company: formValuesClose.title_company,
          companyname: formValuesClose.companyname,
          file_gfnumber: formValuesClose.file_gfnumber,
          contact_number: formValuesClose.contact_number,
          contactemail: formValuesClose.contactemail,
          message: formValuesClose.message,
          transactionId: params.id,
          closing_location: formValuesClose.closing_location,
          closing_address: formValuesClose.closing_address,
          closing_datetime: formValuesClose.closing_datetime,
        };
        try {
          setLoader({ isActive: true });
          const response = await user_service.transactionClosingPost(userData);

          if (response) {
            const userDatan = {
              addedBy: jwt(localStorage.getItem("auth")).id,
              agentId: jwt(localStorage.getItem("auth")).id,
              transactionId: params.id,
              message: "Transaction Closing Company Details Add ",
              note_type: "Closing Company",
            };
            const responsen = user_service.transactionNotes(userDatan);

            setLoader({ isActive: false });
            setContacts("");
            setGetContact([]);
            setToaster({
              types: "Closing Company",
              isShow: true,
              message: "Closing Company Added Successfully",
            });
            setTimeout(() => {
              setToaster((prevToaster) => ({
                ...prevToaster,
                isShow: false,
              }));
            }, 2000);
          } else {
            setToaster({
              types: "error",
              isShow: true,
              message: "Error while adding commission.",
            });
          }
        } catch (error) {
          setLoader({ isActive: false });
          setToaster({
            types: "error",
            isShow: true,
            message: "Error",
          });

          setTimeout(() => {
            setToaster((prevToaster) => ({
              ...prevToaster,
              isShow: false,
            }));
          }, 2000);
        }
      } else {
        e.preventDefault();
        for (const key in formErrorsClose) {
          if (contacts) {
            if (formErrorsClose.file_gfnumber) {
              setToaster({
                types: "error",
                isShow: true,
                message: "Please fill in all required fields.",
              });

              setTimeout(() => {
                setToaster((prevToaster) => ({
                  ...prevToaster,
                  isShow: false,
                }));
              }, 2000);

              return;
            }
          } else {
            if (formErrorsClose[key]) {
              setToaster({
                types: "error",
                isShow: true,
                message: "Please fill in all required fields.",
              });

              setTimeout(() => {
                setToaster((prevToaster) => ({
                  ...prevToaster,
                  isShow: false,
                }));
              }, 2000);

              return;
            }
          }
        }
        const userData = {
          agentId: jwt(localStorage.getItem("auth")).id,
          title_company: formValuesClose.title_company || contacts.company,
          companyname: formValuesClose.companyname || contacts.firstName,
          file_gfnumber: formValuesClose.file_gfnumber,
          contact_number: formValuesClose.contact_number || contacts.phone,
          contactemail: formValuesClose.contactemail || contacts.email,
          message: formValuesClose.message,
          closing_location: formValuesClose.closing_location,
          closing_address: formValuesClose.closing_address,
          closing_datetime: formValuesClose.closing_datetime,
          transactionId: params.id,
        };
        try {
          setLoader({ isActive: true });
          const response = await user_service.transactionClosingPost(userData);

          if (response) {
            // setFormValuesNew(response.data._id);
            const userDatan = {
              addedBy: jwt(localStorage.getItem("auth")).id,
              agentId: jwt(localStorage.getItem("auth")).id,
              transactionId: params.id,
              message: "Transaction Closing Company Details Add ",
              // transaction_property_address: "ADDRESS 2",
              note_type: "Closing Company",
            };
            const responsen = user_service.transactionNotes(userDatan);

            setLoader({ isActive: false });
            setContacts("");
            setGetContact([]);
            setToaster({
              types: "Closing Company",
              isShow: true,
              message: "Closing Company Added Successfully",
            });
            setTimeout(() => {
              setToaster((prevToaster) => ({
                ...prevToaster,
                isShow: false,
              }));
            }, 2000);
          } else {
            setToaster({
              types: "error",
              isShow: true,
              message: "Error",
            });
          }
        } catch (error) {
          setLoader({ isActive: false });
          setToaster({
            types: "error",
            isShow: true,
            message: "Error",
          });

          setTimeout(() => {
            setToaster((prevToaster) => ({
              ...prevToaster,
              isShow: false,
            }));
          }, 2000);
        }
      }
    }
  };

  const TransactionClosing = async () => {
    setLoader({ isActive: true });
    await user_service.transactionClosingGet(params.id).then((response) => {
      if (response) {
        setLoader({ isActive: false });
        setFormValuesClose(response.data.data[0] || {});
      }
    });
  };

  return (
    <div className="bg-secondary float-left w-100 pt-4 mb-4">
      <Loader isActive={isActive} />
      {isShow && (
        <Toaster
          types={types}
          isShow={isShow}
          toasterBody={toasterBody}
          message={message}
        />
      )}

      <main className="page-wrapper profile_page_wrap">
        <div className="content-overlay transaction_tab_view mt-0 commission_details">
          <div className="row">
            <AllTab />
            <div className="col-md-12">
              {/* <StatusBar /> */}
              <div className="bg-light p-3 border-0 rounded-3 shadow-sm float-start w-100">
                <h3 className="mb-3">Commissions Details:</h3>
                <hr className="my-3"></hr>
                <div className="row">
                  <div className="col-md-3 mb-3">
                    <label className="col-form-label">Purchase Price*</label>
                    <input
                      className="form-control"
                      id="inline-form-input"
                      type="text"
                      name="purchase_price"
                      value={formValues.purchase_price}
                      onChange={handleChangePrice}
                      placeholder="00"
                      style={{
                        border:
                          transactionId.referralSend === "no"
                            ? ""
                            : transactionId.type === "buy_referral" ||
                              transactionId.type === "sale_referral" ||
                              transactionId.type === "both_referral"
                            ? "1px solid #00000026"
                            : formValues.purchase_price
                            ? "1px solid #00000026"
                            : formErrors.purchase_price
                            ? "1px solid red"
                            : "1px solid #00000026",
                      }}
                    />

                    {transactionId.referralSend === "no" ? (
                      <></>
                    ) : (
                      <>
                        {transactionId.type === "buy_referral" ||
                        transactionId.type === "sale_referral" ||
                        transactionId.type === "both_referral" ? (
                          <></>
                        ) : (
                          <>
                            {!formValues.purchase_price && (
                              <div className="invalid-tooltip">
                                {formErrors.purchase_price}
                              </div>
                            )}
                          </>
                        )}
                      </>
                    )}
                  </div>

                  <div className="col-md-2 mb-3">
                    <label className="col-form-label d-flex">
                      Total Gross Commissions?
                      <div className="form-check mb-0">
                        <input
                          className="form-check-input"
                          type="radio"
                          value="percentage"
                          onChange={handleRadioChange}
                          checked={
                            formValues?.totalgrosscommission_type ===
                            "percentage"
                          }
                        />
                        <label className="form-check-label" id="radio-level1">
                          %
                        </label>
                      </div>
                      <div className="form-check mb-0 ms-1">
                        <input
                          className="form-check-input"
                          type="radio"
                          value="dollar"
                          onChange={handleRadioChange}
                          checked={
                            formValues.totalgrosscommission_type === "dollar" ||
                            !formValues?.totalgrosscommission_type
                          }
                        />
                        <label className="form-check-label" id="radio-level1">
                          $
                        </label>
                      </div>
                    </label>

                    <div className="d-flex">
                      {formValues.totalgrosscommission_type === "percentage" ? (
                        <></>
                      ) : (
                        <span className="mt-2">
                          {formValues.totalgrosscommission_type === "dollar"
                            ? "$"
                            : isPercentageBuyer ?? ""}
                        </span>
                      )}
                      <input
                        className="form-control"
                        type="number"
                        name="gross_commission"
                        value={formValues.gross_commission}
                        onChange={handleChangeBuyers}
                      />
                      <span className="mt-2">
                        {formValues.totalgrosscommission_type === "percentage"
                          ? "%"
                          : ""}
                      </span>
                    </div>
                  </div>

                  <div className="col-md-2 mb-3">
                    <label className="col-form-label d-flex">
                      Buyers Agent Commissions?
                      <div className="form-check mb-0">
                        <input
                          className="form-check-input"
                          type="radio"
                          value="buyerpercentage"
                          onChange={CheckBuyerCommission}
                          checked={
                            formValues?.buyerscommission_type ===
                            "buyerpercentage"
                          }
                        />
                        <label className="form-check-label" id="radio-level1">
                          %
                        </label>
                      </div>
                      <div className="form-check mb-0 ms-1">
                        <input
                          className="form-check-input"
                          type="radio"
                          value="buyerdoller"
                          onChange={CheckBuyerCommission}
                          checked={
                            formValues?.buyerscommission_type ===
                              "buyerdoller" ||
                            !formValues?.buyerscommission_type
                          }
                        />
                        <label className="form-check-label" id="radio-level1">
                          $
                        </label>
                      </div>
                    </label>

                    <div className="d-flex">
                      {formValues.buyerscommission_type ===
                      "buyerpercentage" ? (
                        <></>
                      ) : (
                        <span className="mt-2">
                          {formValues.buyerscommission_type === "buyerdoller"
                            ? "$"
                            : isPercentageBuyer ?? ""}
                        </span>
                      )}
                      <input
                        className="form-control"
                        type="number"
                        name="buyers_agentcommission"
                        value={formValues.buyers_agentcommission}
                        onChange={handleChangeBuyers}
                      />
                      <span className="mt-2">
                        {formValues.buyerscommission_type === "buyerpercentage"
                          ? "%"
                          : ""}
                      </span>
                    </div>
                  </div>

                  <div className="col-md-2 mb-3">
                    <label className="col-form-label d-flex">
                      Listing Agent Commissions?
                      <div className="form-check mb-0">
                        <input
                          className="form-check-input"
                          type="radio"
                          value="listingpercentage"
                          onChange={CheckListingCommission}
                          checked={
                            formValues?.listingcommission_type ===
                            "listingpercentage"
                          }
                        />
                        <label className="form-check-label" id="radio-level1">
                          %
                        </label>
                      </div>
                      <div className="form-check mb-0 ms-1">
                        <input
                          className="form-check-input"
                          type="radio"
                          value="listingdoller"
                          onChange={CheckListingCommission}
                          checked={
                            formValues?.listingcommission_type ===
                              "listingdoller" ||
                            !formValues?.listingcommission_type
                          }
                        />
                        <label className="form-check-label" id="radio-level1">
                          $
                        </label>
                      </div>
                    </label>
                    <div className="d-flex">
                      {formValues.listingcommission_type ===
                      "listingpercentage" ? (
                        <></>
                      ) : (
                        <span className="mt-2">
                          {formValues.listingcommission_type === "listingdoller"
                            ? "$"
                            : isPercentageListing ?? ""}
                        </span>
                      )}
                      <input
                        className="form-control"
                        type="number"
                        name="listing_agentcommission"
                        value={formValues.listing_agentcommission}
                        onChange={handleChangeBuyers}
                      />
                      <span className="mt-2">
                        {formValues.listingcommission_type ===
                        "listingpercentage"
                          ? "%"
                          : ""}
                      </span>
                    </div>
                  </div>
                </div>

                <div className="row">
                  <div className="col-md-3 mb-3">
                    <label className="col-form-label">
                      What is the contract acceptance date?*
                    </label>
                    <input
                      className="form-control"
                      id="inline-form-input"
                      type="date"
                      name="acceptance_date"
                      value={formValues.acceptance_date}
                      onChange={handleChange}
                      style={{
                        border:
                          transactionId.referralSend === "no"
                            ? ""
                            : transactionId.type === "buy_referral" ||
                              transactionId.type === "sale_referral" ||
                              transactionId.type === "both_referral"
                            ? "1px solid #00000026"
                            : formValues.acceptance_date
                            ? "1px solid #00000026"
                            : formErrors.acceptance_date
                            ? "1px solid red"
                            : "1px solid #00000026",
                      }}
                    />
                    {transactionId.referralSend === "no" ? (
                      <></>
                    ) : (
                      <>
                        {transactionId.type === "buy_referral" ||
                        transactionId.type === "sale_referral" ||
                        transactionId.type === "both_referral" ? (
                          <></>
                        ) : (
                          <>
                            {!formValues.acceptance_date && (
                              <div className="invalid-tooltip">
                                {formErrors.acceptance_date}
                              </div>
                            )}
                          </>
                        )}
                      </>
                    )}
                  </div>

                  <div className="col-md-3">
                    <label className="col-form-label">
                      What is the earnest money amount?*
                    </label>
                    <input
                      className="form-control"
                      id="inline-form-input"
                      type="text"
                      name="earnest_amount"
                      value={formValues.earnest_amount}
                      onChange={handleChangePrice}
                      style={{
                        border:
                          transactionId.referralSend === "no"
                            ? ""
                            : transactionId.type === "buy_referral" ||
                              transactionId.type === "sale_referral" ||
                              transactionId.type === "both_referral"
                            ? "1px solid #00000026"
                            : formValues.earnest_amount
                            ? "1px solid #00000026"
                            : formErrors.earnest_amount
                            ? "1px solid red"
                            : "1px solid #00000026",
                      }}
                    />
                    {transactionId.referralSend === "no" ? (
                      <></>
                    ) : (
                      <>
                        {transactionId.type === "buy_referral" ||
                        transactionId.type === "sale_referral" ||
                        transactionId.type === "both_referral" ? (
                          <></>
                        ) : (
                          <>
                            {!formValues.earnest_amount && (
                              <div className="invalid-tooltip">
                                {formErrors.earnest_amount}
                              </div>
                            )}
                          </>
                        )}
                      </>
                    )}
                  </div>

                  <div className="col-md-3">
                    <label className="col-form-label">
                      What is the earnest money deadline?*
                    </label>
                    <input
                      className="form-control"
                      id="inline-form-input"
                      type="date"
                      name="earnest_deadline"
                      value={formValues.earnest_deadline}
                      onChange={handleChange}
                      style={{
                        border:
                          transactionId.referralSend === "no"
                            ? ""
                            : transactionId.type === "buy_referral" ||
                              transactionId.type === "sale_referral" ||
                              transactionId.type === "both_referral"
                            ? "1px solid #00000026"
                            : formValues.earnest_deadline
                            ? "1px solid #00000026"
                            : formErrors.earnest_deadline
                            ? "1px solid red"
                            : "1px solid #00000026",
                      }}
                    />
                    {transactionId.referralSend === "no" ? (
                      <></>
                    ) : (
                      <>
                        {transactionId.type === "buy_referral" ||
                        transactionId.type === "sale_referral" ||
                        transactionId.type === "both_referral" ? (
                          <></>
                        ) : (
                          <>
                            {!formValues.earnest_deadline && (
                              <div className="invalid-tooltip">
                                {formErrors.earnest_deadline}
                              </div>
                            )}
                          </>
                        )}
                      </>
                    )}
                  </div>
                </div>

                <div className="row mt-3">
                  <div className="col-md-4 mb-3">
                    <label className="col-form-label" id="radio-level1">
                      Where is the earnest money being held?*
                    </label>
                    <div className="">
                      <div className="form-check">
                        <input
                          className="form-check-input"
                          id="form-check-1"
                          type="radio"
                          name="being_held"
                          value="your_brokerage"
                          onChange={CheckBeingheldCommission}
                          checked={
                            formValues?.money_beingheld === "your_brokerage"
                          }
                        />
                        <label className="form-label" id="radio-level1">
                          Buyers Brokerage
                        </label>
                      </div>

                      <div className="form-check">
                        <input
                          className="form-check-input"
                          id="form-check-1"
                          type="radio"
                          name="being_held"
                          onChange={CheckBeingheldCommission}
                          value="buyers_company"
                          checked={
                            formValues?.money_beingheld === "buyers_company"
                          }
                        />
                        <label className="form-label" id="radio-level1">
                          Buyers Title Company
                        </label>
                      </div>

                      <div className="form-check">
                        <input
                          className="form-check-input"
                          id="form-check-1"
                          type="radio"
                          name="being_held"
                          onChange={CheckBeingheldCommission}
                          value="sellers_brokerage"
                          checked={
                            formValues?.money_beingheld === "sellers_brokerage"
                          }
                        />
                        <label className="form-label" id="radio-level1">
                          Sellers Brokerage
                        </label>
                      </div>

                      <div className="form-check">
                        <input
                          className="form-check-input"
                          id="form-check-1"
                          type="radio"
                          name="being_held"
                          onChange={CheckBeingheldCommission}
                          value="sellers_companys"
                          checked={
                            formValues?.money_beingheld === "sellers_companys"
                          }
                        />
                        <label className="form-label" id="radio-level1">
                          Sellers Title Company
                        </label>
                      </div>

                      <div className="form-check">
                        <input
                          className="form-check-input"
                          id="form-check-1"
                          type="radio"
                          name="being_held"
                          onChange={CheckBeingheldCommission}
                          value="other"
                          checked={formValues?.money_beingheld === "other"}
                        />
                        <label className="form-label" id="radio-level1">
                          Other
                        </label>
                      </div>

                      {formValues?.money_beingheld === "other" ? (
                        <div className="">
                          <label className="col-form-label">Enter New</label>
                          <input
                            className="form-control"
                            id="inline-form-input"
                            type="text"
                            name="new_title"
                            value={formValues.new_title}
                            onChange={handleChange}
                          />
                        </div>
                      ) : (
                        ""
                      )}
                    </div>

                    {transactionId.referralSend === "no" ? (
                      <></>
                    ) : (
                      <>
                        {transactionId.type === "buy_referral" ||
                        transactionId.type === "sale_referral" ||
                        transactionId.type === "both_referral" ? (
                          <></>
                        ) : (
                          <>
                            {!formValues.money_beingheld && (
                              <div className="invalid-tooltip">
                                {formErrors.money_beingheld}
                              </div>
                            )}
                          </>
                        )}
                      </>
                    )}
                  </div>

                  <div className="col-md-8 mb-3">
                    <label className="col-form-label" id="radio-level1">
                      Is this a cash Deal?
                    </label>
                    <div className="form-check mb-0">
                      <input
                        className="form-check-input"
                        id="form-check-1"
                        type="radio"
                        name="cashdeal"
                        onChange={CheckCashDeal}
                        value="yes"
                        checked={formValues?.cash_deal === "yes"}
                      />

                      <label className="form-label" id="radio-level1">
                        Yes
                      </label>
                    </div>

                    <div className="form-check mb-0">
                      <input
                        className="form-check-input"
                        id="form-check-1"
                        type="radio"
                        name="cashdeal"
                        onChange={CheckCashDeal}
                        checked={formValues?.cash_deal === "no"}
                        value="no"
                      />
                      <label className="form-label" id="radio-level1">
                        No
                      </label>
                    </div>

                    {transactionId.referralSend === "no" ? (
                      <></>
                    ) : (
                      <>
                        {transactionId.type === "buy_referral" ||
                        transactionId.type === "sale_referral" ||
                        transactionId.type === "both_referral" ? (
                          <></>
                        ) : (
                          <>
                            {!formValues.cash_deal && (
                              <div className="invalid-tooltip">
                                {formErrors.cash_deal}
                              </div>
                            )}
                          </>
                        )}
                      </>
                    )}
                  </div>
                </div>

                <div className="row mt-3">
                  <div className="col-md-4 mb-3">
                    <label className="col-form-label" id="radio-level1">
                      If your client a licensed real estate agent in any state?
                    </label>
                    <div className="d-flex">
                      <div className="form-check">
                        <input
                          className="form-check-input"
                          id="form-check-1"
                          type="radio"
                          name="licensedrealestate"
                          value="yes"
                          onChange={CheckLicensed}
                          checked={
                            formValues?.licensed_realestateagent === "yes"
                          }
                        />

                        <label className="form-label" id="radio-level1">
                          Yes
                        </label>
                      </div>

                      <div className="form-check ms-2">
                        <input
                          className="form-check-input"
                          id="form-check-1"
                          type="radio"
                          name="licensedrealestate"
                          value="no"
                          onChange={CheckLicensed}
                          checked={
                            formValues?.licensed_realestateagent === "no"
                          }
                        />
                        <label className="form-label" id="radio-level1">
                          No
                        </label>
                      </div>
                    </div>

                    {transactionId.referralSend === "no" ? (
                      <></>
                    ) : (
                      <>
                        {transactionId.type === "buy_referral" ||
                        transactionId.type === "sale_referral" ||
                        transactionId.type === "both_referral" ? (
                          <></>
                        ) : (
                          <>
                            {!formValues.licensed_realestateagent && (
                              <div className="invalid-tooltip">
                                {formErrors.licensed_realestateagent}
                              </div>
                            )}
                          </>
                        )}
                      </>
                    )}
                  </div>

                  <div className="col-md-8 mb-3">
                    <label className="col-form-label" id="radio-level1">
                      Are you the buyer or seller in this transaction or have
                      any interest in the property (Even a 1% interest?)
                    </label>
                    <div className="d-flex">
                      <div className="form-check">
                        <input
                          className="form-check-input"
                          id="form-check-1"
                          type="radio"
                          name="property"
                          value="yes"
                          onChange={CheckProperty}
                          checked={formValues?.property_interest === "yes"}
                        />

                        <label className="form-label" id="radio-level1">
                          Yes
                        </label>
                      </div>

                      <div className="form-check ms-2">
                        <input
                          className="form-check-input"
                          id="form-check-1"
                          type="radio"
                          name="property"
                          onChange={CheckProperty}
                          value="no"
                          checked={formValues?.property_interest === "no"}
                        />
                        <label className="form-label" id="radio-level1">
                          No
                        </label>
                      </div>
                    </div>

                    {transactionId.referralSend === "no" ? (
                      <></>
                    ) : (
                      <>
                        {transactionId.type === "buy_referral" ||
                        transactionId.type === "sale_referral" ||
                        transactionId.type === "both_referral" ? (
                          <></>
                        ) : (
                          <>
                            {!formValues.property_interest && (
                              <div className="invalid-tooltip">
                                {formErrors.property_interest}
                              </div>
                            )}
                          </>
                        )}
                      </>
                    )}
                  </div>

                  <div className="col-md-6">
                    <label className="col-form-label" id="radio-level1">
                      Are you using a TC/ Transaction Coordinator?
                    </label>
                    <div className="d-flex">
                      <div className="form-check">
                        <input
                          className="form-check-input"
                          id="form-check-1"
                          type="radio"
                          name="coordinator"
                          value="yes"
                          onChange={CheckTransactionCoordinator}
                          checked={
                            formValues?.transaction_coordinator === "yes"
                          }
                        />

                        <label className="form-label" id="radio-level1">
                          Yes
                        </label>
                      </div>

                      <div className="form-check ms-2">
                        <input
                          className="form-check-input"
                          id="form-check-1"
                          type="radio"
                          name="coordinator"
                          onChange={CheckTransactionCoordinator}
                          checked={formValues?.transaction_coordinator === "no"}
                          value="no"
                        />
                        <label className="form-label" id="radio-level1">
                          No
                        </label>
                      </div>
                    </div>

                    {transactionId.referralSend === "no" ? (
                      <></>
                    ) : (
                      <>
                        {transactionId.type === "buy_referral" ||
                        transactionId.type === "sale_referral" ||
                        transactionId.type === "both_referral" ? (
                          <></>
                        ) : (
                          <>
                            {!formValues.transaction_coordinator && (
                              <div className="invalid-tooltip">
                                {formErrors.transaction_coordinator}
                              </div>
                            )}
                          </>
                        )}
                      </>
                    )}

                    <div className="">
                      <label className="col-form-label">
                        What is the TC Fee: $
                      </label>
                      <input
                        className="form-control w-md-50"
                        id="inline-form-input"
                        type="text"
                        name="tc_fee"
                        value={formValues.tc_fee}
                        onChange={handleChangePrice}
                        style={{
                          border:
                            transactionId.referralSend === "no"
                              ? ""
                              : transactionId.type === "buy_referral" ||
                                transactionId.type === "sale_referral" ||
                                transactionId.type === "both_referral"
                              ? "1px solid #00000026"
                              : formValues.tc_fee
                              ? "1px solid #00000026"
                              : formErrors.tc_fee
                              ? "1px solid red"
                              : "1px solid #00000026",
                        }}
                      />

                      {transactionId.referralSend === "no" ? (
                        <></>
                      ) : (
                        <>
                          {transactionId.type === "buy_referral" ||
                          transactionId.type === "sale_referral" ||
                          transactionId.type === "both_referral" ? (
                            <></>
                          ) : (
                            <>
                              {!formValues.tc_fee && (
                                <div className="invalid-tooltip">
                                  {formErrors.tc_fee}
                                </div>
                              )}
                            </>
                          )}
                        </>
                      )}
                    </div>
                  </div>

                  {transactionId.filing === "filed_complted" ? (
                    <div className="">
                      <button
                        className="btn btn-primary mt-3 pull-right"
                        type="button"
                        title="Your transaction was completed"
                      >
                        Save
                      </button>
                    </div>
                  ) : (
                    <>
                      {transactionFundingRequest?.invoice_url ? (
                        localStorage.getItem("auth") &&
                        jwt(localStorage.getItem("auth")).contactType ==
                          "admin" ? (
                          <>
                            <div className="">
                              <button
                                className="btn btn-primary mt-3 pull-right"
                                type="button"
                                onClick={handleSubmit}
                              >
                                Save
                              </button>
                            </div>
                          </>
                        ) : (
                          <></>
                        )
                      ) : (
                        <div className="">
                          <button
                            className="btn btn-primary mt-3 pull-right"
                            type="button"
                            onClick={handleSubmit}
                          >
                            Save
                          </button>
                        </div>
                      )}
                    </>
                  )}
                </div>
              </div>

              <div className="bg-light p-3 border-0 rounded-3 shadow-sm float-start w-100">
                <h3 className="mb-3">Referral Details:</h3>
                <hr className="my-3"></hr>

                <div className="row">
                  <div className="mb-3">
                    <label className="col-form-label" id="radio-level1">
                      Referral Type *
                    </label>
                    <div className="d-flex">
                      <div className="form-check">
                        <input
                          className="form-check-input"
                          id="form-check-1"
                          type="radio"
                          name="referraltype"
                          value="no_referral"
                          onChange={CheckReferralType}
                          checked={
                            formValuesNew?.referral_type === "no_referral" ||
                            !formValuesNew?.referral_type
                          }
                        />
                        <label className="form-label" id="radio-level1">
                          No Referral
                        </label>
                      </div>

                      <div className="form-check ms-3">
                        <input
                          className="form-check-input"
                          id="form-check-1"
                          type="radio"
                          name="referraltype"
                          value="external_referral"
                          onChange={CheckReferralType}
                          checked={
                            formValuesNew?.referral_type === "external_referral"
                          }
                        />
                        <label className="form-label" id="radio-level1">
                          External Referral
                        </label>
                      </div>

                      <div className="form-check ms-3">
                        <input
                          className="form-check-input"
                          id="form-check-1"
                          type="radio"
                          name="referraltype"
                          value="internal_referral"
                          onChange={CheckReferralType}
                          checked={
                            formValuesNew?.referral_type === "internal_referral"
                          }
                        />
                        <label className="form-label" id="radio-level1">
                          Internal Referral
                        </label>
                      </div>
                    </div>

                    {transactionId.referralSend === "no" ? (
                      <></>
                    ) : (
                      <>
                        {transactionId.type === "buy_referral" ||
                        transactionId.type === "sale_referral" ||
                        transactionId.type === "both_referral" ? (
                          <></>
                        ) : (
                          <>
                            {!formValuesNew.referral_type && (
                              <div className="invalid-tooltip">
                                {formErrorsReferral.referral_type}
                              </div>
                            )}
                          </>
                        )}
                      </>
                    )}
                  </div>

                  {formValuesNew?.referral_type === "no_referral" ||
                  !formValuesNew?.referral_type ? (
                    <></>
                  ) : (
                    <>
                      <div className="row">
                        <div className="col-md-3 mb-3">
                          <label className="col-form-label">First Name</label>
                          <input
                            className="form-control"
                            id="inline-form-input"
                            type="text"
                            name="referral_firstname"
                            value={formValuesNew.referral_firstname}
                            placeholder="Name"
                            onChange={handleChanges}
                            style={{
                              border:
                                transactionId.referralSend === "no"
                                  ? ""
                                  : transactionId.type === "buy_referral" ||
                                    transactionId.type === "sale_referral" ||
                                    transactionId.type === "both_referral"
                                  ? "1px solid #00000026"
                                  : formValuesNew.referral_firstname
                                  ? "1px solid #00000026"
                                  : formErrorsReferral.referral_firstname
                                  ? "1px solid red"
                                  : "1px solid #00000026",
                            }}
                          />

                          {transactionId.referralSend === "no" ? (
                            <></>
                          ) : (
                            <>
                              {transactionId.type === "buy_referral" ||
                              transactionId.type === "sale_referral" ||
                              transactionId.type === "both_referral" ? (
                                <></>
                              ) : (
                                <>
                                  {!formValuesNew.referral_firstname && (
                                    <div className="invalid-tooltip">
                                      {formErrorsReferral.referral_firstname}
                                    </div>
                                  )}
                                </>
                              )}
                            </>
                          )}
                        </div>

                        <div className="col-md-3 mb-3">
                          <label className="col-form-label">Last Name</label>
                          <input
                            className="form-control"
                            id="inline-form-input"
                            type="text"
                            name="referral_lastname"
                            value={formValuesNew.referral_lastname}
                            placeholder="Last Name"
                            onChange={handleChanges}
                            style={{
                              border:
                                transactionId.referralSend === "no"
                                  ? ""
                                  : transactionId.type === "buy_referral" ||
                                    transactionId.type === "sale_referral" ||
                                    transactionId.type === "both_referral"
                                  ? "1px solid #00000026"
                                  : formValuesNew.referral_lastname
                                  ? "1px solid #00000026"
                                  : formErrorsReferral.referral_lastname
                                  ? "1px solid red"
                                  : "1px solid #00000026",
                            }}
                          />

                          {transactionId.referralSend === "no" ? (
                            <></>
                          ) : (
                            <>
                              {transactionId.type === "buy_referral" ||
                              transactionId.type === "sale_referral" ||
                              transactionId.type === "both_referral" ? (
                                <></>
                              ) : (
                                <>
                                  {!formValuesNew.referral_lastname && (
                                    <div className="invalid-tooltip">
                                      {formErrorsReferral.referral_lastname}
                                    </div>
                                  )}
                                </>
                              )}
                            </>
                          )}
                        </div>

                        <div className="col-md-3 mb-3">
                          <label className="col-form-label">Email</label>
                          <input
                            className="form-control"
                            id="inline-form-input"
                            type="text"
                            name="referral_email"
                            value={formValuesNew.referral_email}
                            placeholder="Email"
                            onChange={handleChanges}
                            style={{
                              border:
                                transactionId.referralSend === "no"
                                  ? ""
                                  : transactionId.type === "buy_referral" ||
                                    transactionId.type === "sale_referral" ||
                                    transactionId.type === "both_referral"
                                  ? "1px solid #00000026"
                                  : formValuesNew.referral_email
                                  ? "1px solid #00000026"
                                  : formErrorsReferral.referral_email
                                  ? "1px solid red"
                                  : "1px solid #00000026",
                            }}
                          />

                          {transactionId.referralSend === "no" ? (
                            <></>
                          ) : (
                            <>
                              {transactionId.type === "buy_referral" ||
                              transactionId.type === "sale_referral" ||
                              transactionId.type === "both_referral" ? (
                                <></>
                              ) : (
                                <>
                                  {!formValuesNew.referral_email && (
                                    <div className="invalid-tooltip">
                                      {formErrorsReferral.referral_email}
                                    </div>
                                  )}
                                </>
                              )}
                            </>
                          )}
                        </div>

                        <div className="col-md-3 mb-3">
                          <label className="col-form-label">Phone</label>
                          <input
                            className="form-control"
                            id="inline-form-input"
                            type="number"
                            name="referral_phone"
                            value={formValuesNew.referral_phone}
                            placeholder="Phone"
                            onChange={handleChanges}
                            style={{
                              border:
                                transactionId.referralSend === "no"
                                  ? ""
                                  : transactionId.type === "buy_referral" ||
                                    transactionId.type === "sale_referral" ||
                                    transactionId.type === "both_referral"
                                  ? "1px solid #00000026"
                                  : formValuesNew.referral_phone
                                  ? "1px solid #00000026"
                                  : formErrorsReferral.referral_phone
                                  ? "1px solid red"
                                  : "1px solid #00000026",
                            }}
                          />

                          {transactionId.referralSend === "no" ? (
                            <></>
                          ) : (
                            <>
                              {transactionId.type === "buy_referral" ||
                              transactionId.type === "sale_referral" ||
                              transactionId.type === "both_referral" ? (
                                <></>
                              ) : (
                                <>
                                  {!formValuesNew.referral_phone && (
                                    <div className="invalid-tooltip">
                                      {formErrorsReferral.referral_phone}
                                    </div>
                                  )}
                                </>
                              )}
                            </>
                          )}
                        </div>
                      </div>

                      <div className="row">
                        <div className="col-md-3 mt-5">
                          <label className="col-form-label">
                            Referral Brokerage Name
                          </label>
                          <input
                            className="form-control"
                            id="inline-form-input"
                            type="text"
                            name="referral_brokeragename"
                            value={formValuesNew.referral_brokeragename}
                            placeholder="Brokerage Name"
                            onChange={handleChanges}
                            style={{
                              border:
                                transactionId.referralSend === "no"
                                  ? ""
                                  : transactionId.type === "buy_referral" ||
                                    transactionId.type === "sale_referral" ||
                                    transactionId.type === "both_referral"
                                  ? "1px solid #00000026"
                                  : formValuesNew.referral_brokeragename
                                  ? "1px solid #00000026"
                                  : formErrorsReferral.referral_brokeragename
                                  ? "1px solid red"
                                  : "1px solid #00000026",
                            }}
                          />

                          {transactionId.referralSend === "no" ? (
                            <></>
                          ) : (
                            <>
                              {transactionId.type === "buy_referral" ||
                              transactionId.type === "sale_referral" ||
                              transactionId.type === "both_referral" ? (
                                <></>
                              ) : (
                                <>
                                  {!formValuesNew.referral_brokeragename && (
                                    <div className="invalid-tooltip">
                                      {
                                        formErrorsReferral.referral_brokeragename
                                      }
                                    </div>
                                  )}
                                </>
                              )}
                            </>
                          )}
                        </div>

                        <div className="col-md-2">
                          <label className="col-form-label d-flex">
                            Referral Company Commissions?
                            <div className="form-check mb-0">
                              <input
                                className="form-check-input"
                                type="radio"
                                value="referral-percentage"
                                onChange={CheckCompanyCommission}
                                checked={
                                  formValuesNew?.referral_companycommission ===
                                  "referral-percentage"
                                }
                              />
                              <label
                                className="form-check-label"
                                id="radio-level1"
                              >
                                %
                              </label>
                            </div>
                            &nbsp;
                            <div className="form-check mb-0">
                              <input
                                className="form-check-input"
                                type="radio"
                                value="referral-doller"
                                onChange={CheckCompanyCommission}
                                checked={
                                  formValuesNew?.referral_companycommission ===
                                    "referral-doller" ||
                                  !formValuesNew.referral_companycommission
                                }
                              />
                              <label
                                className="form-check-label"
                                id="radio-level1"
                              >
                                $
                              </label>
                            </div>
                          </label>
                          <div className="d-flex">
                            {formValuesNew?.referral_companycommission ===
                            "referral-percentage" ? (
                              <></>
                            ) : (
                              <span className="mt-2">
                                {formValuesNew?.referral_companycommission ===
                                "referral-doller"
                                  ? "$"
                                  : isPercentageReferral ?? ""}
                              </span>
                            )}
                            <input
                              className="form-control"
                              type="number"
                              placeholder="00"
                              name="referral_commission"
                              value={formValuesNew.referral_commission}
                              onChange={handleChangesReferralcommission}
                              style={{
                                border:
                                  transactionId.referralSend === "no"
                                    ? ""
                                    : transactionId.type === "buy_referral" ||
                                      transactionId.type === "sale_referral" ||
                                      transactionId.type === "both_referral"
                                    ? "1px solid #00000026"
                                    : formValuesNew.referral_commission
                                    ? "1px solid #00000026"
                                    : formErrorsReferral.referral_commission
                                    ? "1px solid red"
                                    : "1px solid #00000026",
                              }}
                            />
                            <span className="mt-2">
                              {formValuesNew.referral_companycommission ===
                              "referral-percentage"
                                ? "%"
                                : ""}
                            </span>
                          </div>

                          {transactionId.referralSend === "no" ? (
                            <></>
                          ) : (
                            <>
                              {transactionId.type === "buy_referral" ||
                              transactionId.type === "sale_referral" ||
                              transactionId.type === "both_referral" ? (
                                <></>
                              ) : (
                                <>
                                  {!formValuesNew.referral_commission && (
                                    <div className="invalid-tooltip">
                                      {formErrorsReferral.referral_commission}
                                    </div>
                                  )}
                                </>
                              )}
                            </>
                          )}
                        </div>

                        {formValuesNew?.referral_type ===
                        "internal_referral" ? (
                          <></>
                        ) : (
                          <>
                            {formValuesNew.referral_type ===
                              "external_referral" && (
                              <div className="col-md-3 mb-3 mt-4">
                                <h6 className="col-form-label">
                                  If External Referral: Upload W9 PDF only
                                </h6>
                                <label className="btn btn-primary documentlabel d-inline-block">
                                  <input
                                    id="REPC_real_estate_purchase_contract"
                                    type="file"
                                    accept={acceptedFileTypes
                                      .map((type) => `.${type}`)
                                      .join(",")}
                                    onChange={handleFileUpload}
                                    name="pdfw9"
                                    value={formValues.pdfw9}
                                  />
                                  <i className="fi-plus me-2"></i> Upload
                                </label>

                                {transactionId.referralSend === "no" ? (
                                  <></>
                                ) : (
                                  <>
                                    {transactionId.type === "buy_referral" ||
                                    transactionId.type === "sale_referral" ||
                                    transactionId.type === "both_referral" ? (
                                      <></>
                                    ) : (
                                      <>
                                        {formValuesNew.pdfw9 ? (
                                          <></>
                                        ) : (
                                          <>
                                            {formErrorsReferral.pdfw9 && (
                                              <div className="invalid-tooltip">
                                                {formErrorsReferral.pdfw9}
                                              </div>
                                            )}
                                          </>
                                        )}
                                      </>
                                    )}
                                  </>
                                )}
                              </div>
                            )}

                            {formValuesNew.referral_type ===
                              "external_referral" && (
                              <div className="col-md-3 mb-3 mt-4">
                                <h6 className="col-form-label">
                                  If External Referral: Upload Referral
                                  Agreement
                                </h6>
                                <label className="btn btn-primary documentlabel d-inline-block">
                                  <input
                                    id="REPC_real_estate_purchase_contract"
                                    type="file"
                                    accept={acceptedFileTypesAgreement
                                      .map((type) => `.${type}`)
                                      .join(",")}
                                    name="referral_agreement"
                                    onChange={handleFileAgreement}
                                    value={formValues.referral_agreement}
                                  />
                                  <i className="fi-plus me-2"></i> Upload
                                </label>

                                {transactionId.referralSend === "no" ? (
                                  <></>
                                ) : (
                                  <>
                                    {transactionId.type === "buy_referral" ||
                                    transactionId.type === "sale_referral" ||
                                    transactionId.type === "both_referral" ? (
                                      <></>
                                    ) : (
                                      <>
                                        {formValuesNew.referral_agreement ? (
                                          <></>
                                        ) : (
                                          <>
                                            {formErrorsReferral.referral_agreement && (
                                              <div className="invalid-tooltip">
                                                {
                                                  formErrorsReferral.referral_agreement
                                                }
                                              </div>
                                            )}
                                          </>
                                        )}
                                      </>
                                    )}
                                  </>
                                )}
                              </div>
                            )}
                          </>
                        )}
                      </div>
                    </>
                  )}

                  {transactionId.filing === "filed_complted" ? (
                    <div className="col-md-12">
                      <button
                        className="btn btn-primary mt-3 pull-right"
                        type="button"
                        title="Your transaction was completed"
                      >
                        Save
                      </button>
                    </div>
                  ) : (
                    <>
                      {transactionFundingRequest?.invoice_url ? (
                        localStorage.getItem("auth") &&
                        jwt(localStorage.getItem("auth")).contactType ==
                          "admin" ? (
                          <>
                            <div className="col-md-12">
                              <button
                                className="btn btn-primary mt-3 pull-right"
                                type="button"
                                onClick={handleSubmitReferral}
                              >
                                Save
                              </button>
                            </div>
                          </>
                        ) : (
                          <></>
                        )
                      ) : (
                        <div className="col-md-12">
                          <button
                            className="btn btn-primary mt-3 pull-right"
                            type="button"
                            onClick={handleSubmitReferral}
                          >
                            Save
                          </button>
                        </div>
                      )}
                    </>
                  )}
                </div>
              </div>

              <div className="bg-light border rounded-3 p-3 mt-5">
                <h3 className="mb-3">Closing Company</h3>
                <hr className="my-3"></hr>

                <div className="">
                  <div className="row">
                    <div className="col-md-3 mb-3">
                      <label className="col-form-label">Title Company *</label>
                      <input
                        className="form-control"
                        id="inline-form-input"
                        type="text"
                        name="title_company"
                        value={formValuesClose.title_company}
                        onChange={handleClose}
                        style={{
                          border:
                            transactionId.referralSend === "no"
                              ? ""
                              : transactionId.type === "buy_referral" ||
                                transactionId.type === "sale_referral" ||
                                transactionId.type === "both_referral"
                              ? "1px solid #00000026"
                              : formValuesClose.title_company
                              ? "1px solid #00000026"
                              : formErrorsClose.title_company // Check if there's an error
                              ? "1px solid red"
                              : "1px solid #00000026",
                        }}
                      />

                      {transactionId.referralSend === "no" ? (
                        <></>
                      ) : (
                        <>
                          {transactionId.type === "buy_referral" ||
                          transactionId.type === "sale_referral" ||
                          transactionId.type === "both_referral" ? (
                            <></>
                          ) : (
                            <>
                              {!formValuesClose.title_company && (
                                <div className="invalid-tooltip">
                                  {formErrorsClose.title_company}
                                </div>
                              )}
                            </>
                          )}
                        </>
                      )}
                    </div>

                    <div className="col-md-3 mb-3">
                      <label className="col-form-label">Escrow officer *</label>
                      <input
                        className="form-control"
                        id="inline-form-input"
                        type="text"
                        name="companyname"
                        value={formValuesClose.companyname}
                        onChange={handleClose}
                        style={{
                          border:
                            transactionId.referralSend === "no"
                              ? ""
                              : transactionId.type === "buy_referral" ||
                                transactionId.type === "sale_referral" ||
                                transactionId.type === "both_referral"
                              ? "1px solid #00000026"
                              : formValuesClose.companyname
                              ? "1px solid #00000026"
                              : formErrorsClose.companyname // Check if there's an error
                              ? "1px solid red"
                              : "1px solid #00000026",
                        }}
                      />
                      {transactionId.referralSend === "no" ? (
                        <></>
                      ) : (
                        <>
                          {transactionId.type === "buy_referral" ||
                          transactionId.type === "sale_referral" ||
                          transactionId.type === "both_referral" ? (
                            <></>
                          ) : (
                            <>
                              {!formValuesClose.companyname && (
                                <div className="invalid-tooltip">
                                  {formErrorsClose.companyname}
                                </div>
                              )}
                            </>
                          )}
                        </>
                      )}

                      {/* {contactData ? (
                        <></> 
                      ) : ( */}
                      <>
                        {getContact.length > 0 ? (
                          getContact.map((contact, index) => (
                            <div
                              className="member_Associate mb-1 mt-1"
                              key={index}
                              onClick={() => handleContactAssociate(contact)}
                            >
                              <div
                                className="float-left w-100 d-flex align-items-center justify-content-start"
                                key={contact._id}
                              >
                                <img
                                  className="rounded-circlee profile_picture"
                                  height="30"
                                  width="30"
                                  src={
                                    contact.image && contact.image !== "image"
                                      ? contact.image
                                      : Pic
                                  }
                                  alt="Profile"
                                />

                                {/* <div className="ms-3" onClick={()=>handleSubmit(post._id)}> */}
                                <div className="ms-3">
                                  <strong>
                                    {contact.firstName} {contact.lastName}
                                  </strong>
                                  <br />
                                  <strong>{contact.active_office}</strong>
                                </div>
                              </div>
                            </div>
                          ))
                        ) : (
                          <p></p>
                        )}
                      </>
                      {/* )}  */}
                    </div>

                    <div className="col-md-3 mb-3">
                      <label className="col-form-label">
                        General File Number ?
                        <span className="tooltiptext">
                          <div className="col-md-12">
                            <p className="gray-text p-2">
                              GF stands for Guarantee File and is the working
                              file number in a real estate transaction assigned
                              by your Title company to make sure everyone is
                              working on the same file. You can find this file #
                              on all of the documents your title company
                              provides you including the closing disclosure.
                            </p>
                          </div>
                        </span>
                      </label>
                      <input
                        className="form-control"
                        id="inline-form-input"
                        type="text"
                        name="file_gfnumber"
                        value={formValuesClose.file_gfnumber}
                        onChange={handleClose}
                        style={{
                          border:
                            transactionId.referralSend === "no"
                              ? ""
                              : transactionId.type === "buy_referral" ||
                                transactionId.type === "sale_referral" ||
                                transactionId.type === "both_referral"
                              ? "1px solid #00000026"
                              : formValuesClose.file_gfnumber
                              ? "1px solid #00000026"
                              : formErrorsClose.file_gfnumber
                              ? "1px solid red"
                              : "1px solid #00000026",
                        }}
                      />
                      {transactionId.referralSend === "no" ? (
                        <></>
                      ) : (
                        <>
                          {transactionId.type === "buy_referral" ||
                          transactionId.type === "sale_referral" ||
                          transactionId.type === "both_referral" ? (
                            <></>
                          ) : (
                            <>
                              {!formValuesClose.file_gfnumber && (
                                <div className="invalid-tooltip">
                                  {formErrorsClose.file_gfnumber}
                                </div>
                              )}
                            </>
                          )}
                        </>
                      )}
                    </div>

                    <div className="col-md-3 mb-3">
                      <label className="col-form-label">Contact Number *</label>
                      <input
                        className="form-control"
                        id="inline-form-input"
                        type="text"
                        name="contact_number"
                        value={formValuesClose.contact_number}
                        onChange={handleClose}
                        style={{
                          border:
                            transactionId.referralSend === "no"
                              ? ""
                              : transactionId.type === "buy_referral" ||
                                transactionId.type === "sale_referral" ||
                                transactionId.type === "both_referral"
                              ? "1px solid #00000026"
                              : formValuesClose.contact_number
                              ? "1px solid #00000026"
                              : formErrorsClose.contact_number // Check if there's an error
                              ? "1px solid red"
                              : "1px solid #00000026",
                        }}
                      />

                      {transactionId.referralSend === "no" ? (
                        <></>
                      ) : (
                        <>
                          {transactionId.type === "buy_referral" ||
                          transactionId.type === "sale_referral" ||
                          transactionId.type === "both_referral" ? (
                            <></>
                          ) : (
                            <>
                              {!formValuesClose.contact_number && (
                                <div className="invalid-tooltip">
                                  {formErrorsClose.contact_number}
                                </div>
                              )}
                            </>
                          )}
                        </>
                      )}
                    </div>
                  </div>
                  <div className="row">
                    <div className="col-md-3 mb-3">
                      <label className="col-form-label">Contact Email *</label>
                      <input
                        className="form-control"
                        id="inline-form-input"
                        type="text"
                        name="contactemail"
                        value={formValuesClose.contactemail}
                        onChange={handleClose}
                        style={{
                          border:
                            transactionId.referralSend === "no"
                              ? ""
                              : transactionId.type === "buy_referral" ||
                                transactionId.type === "sale_referral" ||
                                transactionId.type === "both_referral"
                              ? "1px solid #00000026"
                              : formValuesClose.contactemail // Check if `contactemail` or `contacts.company` is present
                              ? "1px solid #00000026"
                              : formErrorsClose.contactemail // Check if there's an error
                              ? "1px solid red"
                              : "1px solid #00000026",
                        }}
                      />

                      {transactionId.referralSend === "no" ? (
                        <></>
                      ) : (
                        <>
                          {transactionId.type === "buy_referral" ||
                          transactionId.type === "sale_referral" ||
                          transactionId.type === "both_referral" ? (
                            <></>
                          ) : (
                            <>
                              {!formValuesClose.contactemail && (
                                <div className="invalid-tooltip">
                                  {formErrorsClose.contactemail}
                                </div>
                              )}
                            </>
                          )}
                        </>
                      )}
                    </div>

                    <div className="col-md-3 mb-3">
                      <label className="col-form-label">Closing Location</label>
                      <input
                        className="form-control"
                        id="inline-form-input"
                        type="text"
                        name="closing_location"
                        value={formValuesClose.closing_location}
                        onChange={handleClose}
                      />
                    </div>

                    <div className="col-md-3 mb-3">
                      <label className="col-form-label">
                        Closing Location Address
                      </label>
                      <input
                        className="form-control"
                        id="inline-form-input"
                        type="text"
                        name="closing_address"
                        value={formValuesClose.closing_address}
                        onChange={handleClose}
                      />
                    </div>

                    <div className="col-md-3 mb-3">
                      <label className="col-form-label">Closing Date-Time</label>
                      <DateTimePicker
                        onChange={handleDateChange}
                        value={formValuesClose.closing_datetime}
                        className="form-control"
                        id="inline-form-input"
                      />
                    </div>

                    <div className="col-md-6 mb-3">
                      <label className="col-form-label">
                        Message Box for Notes
                      </label>
                      <textarea
                        className="form-control mt-0"
                        id="textarea-input"
                        rows="2"
                        name="message"
                        value={formValuesClose.message}
                        onChange={handleClose}
                        autoComplete="on"
                      />
                    </div>

                    {transactionId.filing === "filed_complted" ? (
                      <div className="col-md-12">
                        <button
                          className="btn btn-primary mt-3 pull-right"
                          type="button"
                          title="Your transaction was completed"
                        >
                          Save
                        </button>
                      </div>
                    ) : (
                      <>
                        {transactionFundingRequest?.invoice_url ? (
                          localStorage.getItem("auth") &&
                          jwt(localStorage.getItem("auth")).contactType ==
                            "admin" ? (
                            <>
                              <div className="col-md-12">
                                <button
                                  className="btn btn-primary mt-3 pull-right"
                                  type="button"
                                  onClick={handleSubmitClose}
                                >
                                  Save
                                </button>
                              </div>
                            </>
                          ) : (
                            <></>
                          )
                        ) : (
                          <div className="col-md-12">
                            <button
                              className="btn btn-primary mt-3 pull-right"
                              type="button"
                              onClick={handleSubmitClose}
                            >
                              Save
                            </button>
                          </div>
                        )}
                      </>
                    )}
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </main>
    </div>
  );
};

export default Commissions;

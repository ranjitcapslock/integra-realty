import React, { useEffect, useState } from "react";

import { useParams, NavLink, useNavigate } from "react-router-dom";
import user_service from "../service/user_service";
import Loader from "../../Pages/Loader/Loader.js";
import Toaster from "../../Pages/Toaster/Toaster";
import AllTab from "../../Pages/AllTab";
import jwt from "jwt-decode";
import axios from "axios";
import "jspdf-autotable";
import { jsPDF } from "jspdf";
import image from "../../Components/img/logoNew.png";
import _, { sum } from "lodash";

import ConfettiCanvas from "react-confetti-canvas";

import defaultpropertyimage from "../img/defaultpropertyimage.jpeg";

const FundingRequest = () => {
  // const image = "https://brokeragentbase.s3.amazonaws.com/assets/logo.png";
  const [loader, setLoader] = useState({ isActive: null });
  const { isActive } = loader;
  const [toaster, setToaster] = useState({
    types: null,
    isShow: null,
    toasterBody: "",
    message: "",
  });
  const { types, isShow, toasterBody, message } = toaster;

  const initialRows = [];
  const initialPayoutItem = [];

  const initialValues = {
    companyName: "",
    email: "",
    contactName: "",
    lastName: "",
    phone: "",
    commission: "",
    fileNumber: "",
    invoice_url: "",
    estimate: "",
    _id: "",
  };

  const [summary, setSummary] = useState("");
  const [request, setRequest] = useState(false);
  const [requestreview, setRequestreview] = useState(false);
  const [show, setIsShow] = useState(false);
  const [isSubmitted, setIsSubmitted] = useState(false);

  useEffect(() => {
    if (isSubmitted) {
      const canvas = document.getElementById("confetti");
      if (canvas) {
        canvas.height = 700;
        canvas.width = 1024;
      }
    }
  }, [isSubmitted]);

  const [transactionFesData, setTransactionData] = useState({
    transactionFee: "",
  });

  const [transactionFesPersonal, setTransactionFeePersonal] = useState({
    transactionPersonal: "",
  });

  const [transactionWire, setTransactionWire] = useState({
    wireFee: "",
  });

  const [transactionHighValueFee, setTransactionHighValueFee] = useState("");

  const [transactionBuyerFee, setTransactionBuyerFee] = useState({
    buyerFee: "",
  });

  const [transactionSellerFee, setTransactionSellerFee] = useState({
    sellerFee: "",
  });

  const [transactionRisk, setTransactionRisk] = useState({
    managementFee: "",
  });

  const [formValues, setFormValues] = useState(initialValues);
  const [formValuesNew, setFormValuesNew] = useState("");

  const validate = () => {
    const values = formValues;
    const errors = {};
    // if (!values.funding_method) {
    //     errors.funding_method = "Funding method is required";
    // }

    if (!values.companyName) {
      errors.companyName = "companyName is required";
    }

    if (!values.phone) {
      errors.phone = "Phone is required";
    }

    if (!values.email) {
      errors.email = "Email is required";
    } else if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i.test(values.email)) {
      errors.email = "Invalid email address";
    }

    // if (!values.email) {
    //   errors.email = "Email is required";
    // }

    if (!values.contactName) {
      errors.contactName = "contactName is required";
    }

    setFormErrors(errors);
    return errors;
  };

  const [formErrorsNew, setFormErrorsNew] = useState({
    commission: "Commission is required",
    companyName: "Company Name is required",
    email: "Email is required",
    contactName: "Contact Name is required",
    lastName: "",
    phone: "Phone is required",
    fileNumber: "File Number is required",
  });
  const [formErrors, setFormErrors] = useState({});

  const [isSubmitClick, setISSubmitClick] = useState(false);

  const [pdfPath, setPdfPath] = useState("");

  const [findingGet, setFindingGet] = useState("");

  const [rows, setRows] = useState(initialRows);

  const [selectedOption, setSelectedOption] = useState("");
  const [totalAmount, setTotalAmount] = useState(0);
  const [totalAmountNew, setTotalAmountNew] = useState(0);

  const [contactData, setContactData] = useState("");

  const [formValuesClose, setFormValuesClose] = useState("");
  const [netAmount, setNetAmount] = useState("");

  const [rowsPayout, setRowsPayout] = useState(initialPayoutItem);
  const [selectedPayout, setSelectedPayout] = useState("");
  const [submitData, setSubmitData] = useState("");
  const navigate = useNavigate();

  const [getContact, setGetContact] = useState("");
  const [getCommission, setGetCommission] = useState("");
  const [commissionChanged, setCommissionChanged] = useState(false);

  const params = useParams();

  const TransactionGetById = async () => {
    try {
      setLoader({ isActive: true });
      const response = await user_service.transactionGetById(params.id);
      if (response) {
        const responseData = response.data;
        setSummary(responseData);

        const contact2Data =
          responseData.contact2 &&
          responseData.contact2[0] &&
          responseData.contact2[0].data;
        const contactDatas = contact2Data || "";
        setContactData(contactDatas);

        const contact3Data =
          responseData.contact3 &&
          responseData.contact3[0] &&
          responseData.contact3[0].data;
        const contactDatas3 = contact3Data || "";
        console.log(contactDatas3._id);
        ContactGetId(contactDatas3._id);
      }

      setLoader({ isActive: false });
    } catch (error) {
      console.error("Error fetching transaction details:", error);
      setLoader({ isActive: false });
    }
  };

  const CommissionGet = async () => {
    setLoader({ isActive: true });
    await user_service.commissionGet(params.id).then((response) => {
      if (response) {
        setLoader({ isActive: false });
        console.log(response);
        setGetCommission(response.data.data[0] || {});
      }
    });
  };

  const TransactionClosing = async () => {
    setLoader({ isActive: true });
    try {
      const response = await user_service.transactionClosingGet(params.id);
      if (
        response &&
        response.data &&
        response.data.data &&
        response.data.data.length > 0
      ) {
        setLoader({ isActive: false });
        const responseData = response.data.data[0];
        setFormValuesClose(responseData);

        setFormValues({
          companyName: responseData?.title_company || "",
          phone: responseData?.contact_number || "",
          contactName: responseData?.companyname || "",
          email: responseData?.contactemail || "",
          fileNumber: responseData?.file_gfnumber || "",
        });
      } else {
        setLoader({ isActive: false });
      }
    } catch (error) {
      console.error("Error fetching transaction closing data:", error);
      setLoader({ isActive: false });
    }
  };

  const ContactGetId = async (contactId) => {
    try {
      if (contactId) {
        const response = await user_service.contactGetById(contactId);
        if (response) {
          console.log(response.data);
          setGetContact(response.data);
        }
      }
    } catch (error) {
      console.error("Error fetching contact details:", error);
    }
  };

  useEffect(() => {
    if (summary.phase === "canceled") {
      let transactionFeeForCalculation =
        getContact?.legacy_account === "yes" ? "0" : "0";

      setTransactionData({
        ...transactionFesData,
        transactionFee: transactionFeeForCalculation,
      });
    } else {
      let totalFeesAmount = Number(formValues.commission) || 0;

      if (rows && rows.length > 0) {
        totalFeesAmount += rows.reduce(
          (acc, obj) => acc + Number(obj.amount) || 0,
          0
        );
      }

      if (
        findingGet?.commission !== null &&
        findingGet?.commission !== undefined
      ) {
        const totaldata =
          totalFeesAmount + parseFloat(findingGet?.commission || 0);
        console.log(totaldata);
        setTotalAmount(totaldata);
        // setTotalAmount(findingGet.commission);
      }

      if (commissionChanged) {
        setTotalAmount(totalFeesAmount);
      }

      let transactionFeeForCalculation =
        getContact?.legacy_account === "yes" ? "495" : "595";

      if (
        summary.type === "buy_commercial" ||
        summary.type === "sale_commercial" ||
        summary.type === "both_commercial" ||
        summary.type === "referral_commercial"
      ) {
        const commission = parseFloat(formValues.commission) || 0;
        const transactionFee = Math.max(commission * 0.1, 495);
        setTransactionData({
          ...transactionFesData,
          transactionFee: transactionFee.toFixed(2),
        });

        // Set wire fee
        const wireFeeForCalculation = "20";
        setTransactionWire({
          ...transactionWire,
          wireFee: wireFeeForCalculation,
        });

        if (totalFeesAmount > 495) {
          // Calculate net pay amount
          const netPayAmount =
            Number(totalFeesAmount) -
              Number(transactionFesData.transactionFee) -
              Number(transactionWire.wireFee) || 0;
          setNetAmount(netPayAmount < 0 ? 0 : netPayAmount);
        }
      } else {
        if (summary.represent === "both") {
          if (getCommission.property_interest === "yes") {
            setTransactionData({
              ...transactionFesData,
              transactionFee: transactionFeeForCalculation,
            });

            const wireFeeForCalculation = "20";
            setTransactionWire({
              ...transactionWire,
              wireFee: wireFeeForCalculation,
            });

            const propertyInterset = "350";
            setTransactionFeePersonal({
              ...transactionFesPersonal,
              transactionPersonal: propertyInterset,
            });

            if (totalFeesAmount > 495) {
              // Calculate net pay amount
              const netPayAmount =
                Number(totalFeesAmount) -
                  Number(transactionFesData.transactionFee) -
                  Number(transactionWire.wireFee) -
                  Number(transactionFesPersonal.transactionPersonal) || 0;
              setNetAmount(netPayAmount < 0 ? 0 : netPayAmount);
            }
          } else {
            const buyerFeeForCalculation = "495";
            const sellerFeeForCalculation = "495";
            const riskFeeForCalculation = "350";
            const wireFeeForCalculation = "20";

            setTransactionBuyerFee({
              ...transactionBuyerFee,
              buyerFee: buyerFeeForCalculation,
            });

            setTransactionSellerFee({
              ...transactionSellerFee,
              sellerFee: sellerFeeForCalculation,
            });

            setTransactionRisk({
              ...transactionRisk,
              managementFee: riskFeeForCalculation,
            });

            setTransactionWire({
              ...transactionWire,
              wireFee: wireFeeForCalculation,
            });

            if (totalFeesAmount > 495) {
              const netPayAmount =
                Number(totalFeesAmount) -
                  Number(transactionBuyerFee.buyerFee) -
                  Number(transactionSellerFee.sellerFee) -
                  Number(transactionRisk.managementFee) -
                  Number(transactionWire.wireFee) || 0;
              setNetAmount(netPayAmount < 0 ? 0 : netPayAmount);
            }
          }
          // Auto-fill fees for both Buyer and Seller
        } else {
          if (getCommission.property_interest === "yes") {
            setTransactionData({
              ...transactionFesData,
              transactionFee: transactionFeeForCalculation,
            });

            const wireFeeForCalculation = "20";
            setTransactionWire({
              ...transactionWire,
              wireFee: wireFeeForCalculation,
            });

            const propertyInterset = "350";
            setTransactionFeePersonal({
              ...transactionFesPersonal,
              transactionPersonal: propertyInterset,
            });

            const baseFee = 300;
            const stepUpAmount = 500000;

            let purchasePrice = getCommission.purchase_price || 0;

            const parsedPurchasePrice = parseFloat(
              String(purchasePrice).replace(/[$,]/g, "")
            );

            let additionalSteps = 0;
            let additionalFee = 0;

            const difference = parsedPurchasePrice - 1500000;

            if (parsedPurchasePrice > 1500000) {
              if (difference > 0) {
                additionalSteps = difference / stepUpAmount;
                additionalSteps = Math.ceil(additionalSteps);
                additionalFee = baseFee * additionalSteps;
              } else {
                additionalFee = 0;
              }
            } else {
              additionalFee = 0;
            }
            setTransactionHighValueFee(additionalFee);

            if (totalFeesAmount > 495) {
              // Calculate net pay amount
              const netPayAmount =
                Number(totalFeesAmount) -
                  Number(transactionFesData.transactionFee) -
                  Number(transactionWire.wireFee) -
                  Number(transactionFesPersonal.transactionPersonal) -
                  additionalFee || 0;
              setNetAmount(netPayAmount < 0 ? 0 : netPayAmount);
            }

            // const totaldata = totalFeesAmount + parseFloat(findingGet?.commission || 0);
            // setTotalAmount(totaldata);
          } else {
            if (
              (summary.represent === "buyer" ||
                summary.represent === "seller" ||
                summary.represent === "referral") &&
              getContact?.legacy_account === "yes"
            ) {
              // Set transaction fee for legacy account
              setTransactionData({
                ...transactionFesData,
                transactionFee: transactionFeeForCalculation,
              });
            } else if (
              (summary.represent === "buyer" ||
                summary.represent === "seller" ||
                summary.represent === "referral") &&
              getContact?.legacy_account === "no"
            ) {
              // For non-legacy accounts

              // Set transaction fee and high-value fee
              setTransactionData({
                ...transactionFesData,
                transactionFee: "595",
              });
            }

            // Wire Fee Logic
            const wireFeeForCalculation = "20";
            setTransactionWire({
              ...transactionWire,
              wireFee: wireFeeForCalculation,
            });
            const baseFee = 300;
            const stepUpAmount = 500000;

            let purchasePrice = getCommission.purchase_price || 0;

            const parsedPurchasePrice = parseFloat(
              String(purchasePrice).replace(/[$,]/g, "")
            );
            let additionalSteps = 0;
            let additionalFee = 0;

            const difference = parsedPurchasePrice - 1500000;

            if (parsedPurchasePrice > 1500000) {
              if (difference > 0) {
                additionalSteps = difference / stepUpAmount;
                additionalSteps = Math.ceil(additionalSteps);
                additionalFee = baseFee * additionalSteps;
              } else {
                additionalFee = 0;
              }
            } else {
              additionalFee = 0;
            }
            setTransactionHighValueFee(additionalFee);

            if (totalFeesAmount > 495) {
              // Calculate net pay amount
              const netPayAmount =
                Number(totalFeesAmount) -
                  Number(transactionFesData.transactionFee) -
                  Number(transactionWire.wireFee) -
                  additionalFee || 0;
              setNetAmount(netPayAmount < 0 ? 0 : netPayAmount);
            }
          }
        }
      }
    }
  }, [
    rows,
    formValues,
    getContact,
    totalAmount,
    netAmount,
    summary.represent,
    summary.type,
    findingGet?.estimate,
    commissionChanged,
  ]);

  useEffect(() => {
    if (
      summary.type === "buy_commercial" ||
      summary.type === "sale_commercial" ||
      summary.type === "both_commercial" ||
      summary.type === "referral_commercial"
    ) {
      let totalPayoutAmount = 0;

      if (transactionFesData && transactionFesData?.transactionFee) {
        totalPayoutAmount = Number(transactionFesData.transactionFee) || 0;
      }

      if (rowsPayout && rowsPayout.length > 0) {
        let result = rowsPayout.reduce(
          (acc, obj) => acc + Number(obj.amount) || 0,
          0
        );
        totalPayoutAmount = result + totalPayoutAmount;
        const total = findingGet?.estimate - result;
        setNetAmount(total < 0 ? 0 : total);
      }

      setTotalAmountNew(totalPayoutAmount);

      let transactionFeeForPayout =
        getContact?.legacy_account === "yes" ? "495" : "495";
      // Set wire fee

      const wireFeeForPayout =
        getContact?.legacy_account === "yes" ? "20" : "20";

      if (totalAmount > Number(transactionFeeForPayout)) {
        const netPayAmount =
          Number(totalAmount) - Number(totalPayoutAmount) || 0;
        setNetAmount(netPayAmount < 0 ? 0 : netPayAmount);
      }

      if (totalAmount > 495) {
        const netPayAmountNew =
          Number(totalAmount) -
            Number(totalPayoutAmount) -
            Number(wireFeeForPayout) || 0;

        setNetAmount(netPayAmountNew < 0 ? 0 : netPayAmountNew);
      }

      if (totalAmountNew) {
        const netPayAmountNew =
          Number(netAmount) +
            Number(totalPayoutAmount) +
            Number(wireFeeForPayout) || 0;
        setTotalAmountNew(netPayAmountNew < 0 ? 0 : netPayAmountNew);
      }
    } else {
      if (summary.represent === "both") {
        if (getCommission.property_interest === "yes") {
          let totalPayoutAmount = 0;

          if (transactionFesData && transactionFesData?.transactionFee) {
            totalPayoutAmount = Number(transactionFesData.transactionFee) || 0;
          }

          if (rowsPayout && rowsPayout.length > 0) {
            let result = rowsPayout.reduce(
              (acc, obj) => acc + Number(obj.amount) || 0,
              0
            );
            totalPayoutAmount = result + totalPayoutAmount;
            const total = findingGet?.estimate - result;
            setNetAmount(total < 0 ? 0 : total);
          }

          setTotalAmountNew(totalPayoutAmount);

          let transactionFeeForPayout =
            getContact?.legacy_account === "yes" ? "495" : "595";

          if (
            (summary.represent === "buyer" ||
              summary.represent === "seller" ||
              summary.represent === "referral") &&
            getContact?.legacy_account === "yes"
          ) {
            transactionFeeForPayout = "495";
          } else if (
            (summary.represent === "buyer" ||
              summary.represent === "seller" ||
              summary.represent === "referral") &&
            getContact?.legacy_account === "no"
          ) {
            transactionFeeForPayout = "595";
          }

          // Wire Fee Logic
          const wireFeeForPayout =
            getContact?.legacy_account === "yes" ? "20" : "20";

          const PropertyForPayout =
            getContact?.legacy_account === "yes" ? "350" : "350";

          const baseFee = 300;
          const stepUpAmount = 500000;
          let purchasePrice = getCommission.purchase_price || 0;
          const parsedPurchasePrice = parseFloat(
            String(purchasePrice).replace(/[$,]/g, "")
          );
          let additionalSteps = 0;
          let additionalFee = 0;

          const difference = parsedPurchasePrice - 1500000;
          if (parsedPurchasePrice > 1500000) {
            if (difference > 0) {
              additionalSteps = difference / stepUpAmount;
              additionalSteps = Math.ceil(additionalSteps);
              additionalFee = baseFee * additionalSteps;
            } else {
              additionalFee = 0;
            }
          } else {
            additionalFee = 0;
          }
          setTransactionHighValueFee(additionalFee);

          if (totalAmount > Number(transactionFeeForPayout)) {
            const netPayAmount =
              Number(totalAmount) - Number(totalPayoutAmount) || 0;
            setNetAmount(netPayAmount < 0 ? 0 : netPayAmount);
          }

          if (totalAmount > 495) {
            const netPayAmountNew =
              Number(totalAmount) -
                Number(totalPayoutAmount) -
                Number(wireFeeForPayout) -
                Number(PropertyForPayout) -
                additionalFee || 0;

            setNetAmount(netPayAmountNew < 0 ? 0 : netPayAmountNew);
          }

          if (totalAmountNew) {
            const netPayAmountNew =
              Number(netAmount) +
                Number(totalPayoutAmount) +
                Number(wireFeeForPayout) +
                Number(PropertyForPayout) +
                additionalFee || 0;
            setTotalAmountNew(netPayAmountNew < 0 ? 0 : netPayAmountNew);
          }
        } else {
          let totalPayoutAmounts = 0;

          // Calculate total payout amounts for both Buyer and Seller
          if (transactionBuyerFee && transactionBuyerFee?.buyerFee) {
            totalPayoutAmounts += Number(transactionBuyerFee.buyerFee) || 0;
          }

          if (transactionSellerFee && transactionSellerFee?.sellerFee) {
            totalPayoutAmounts += Number(transactionSellerFee.sellerFee) || 0;
          }

          if (transactionRisk && transactionRisk?.managementFee) {
            totalPayoutAmounts += Number(transactionRisk.managementFee) || 0;
          }

          if (transactionWire && transactionWire?.wireFee) {
            totalPayoutAmounts += Number(transactionWire.wireFee) || 0;
          }

          if (rowsPayout && rowsPayout.length > 0) {
            let result = rowsPayout.reduce(
              (acc, obj) => acc + Number(obj.amount) || 0,
              0
            );
            totalPayoutAmounts = result + totalPayoutAmounts;
            const total = findingGet?.estimate - result;
            setNetAmount(total < 0 ? 0 : total);
          }
          setTotalAmountNew(totalPayoutAmounts);

          if (totalAmount > 495) {
            const netPayAmount = Number(totalAmount) - totalPayoutAmounts;
            setNetAmount(netPayAmount < 0 ? 0 : netPayAmount);
          }

          if (totalAmountNew) {
            const netPayAmountNews =
              Number(netAmount) + Number(totalPayoutAmounts);
            setTotalAmountNew(netPayAmountNews < 0 ? 0 : netPayAmountNews);
          }
        }
      } else {
        if (getCommission.property_interest === "yes") {
          let totalPayoutAmount = 0;

          if (transactionFesData && transactionFesData?.transactionFee) {
            totalPayoutAmount = Number(transactionFesData.transactionFee) || 0;
          }

          if (rowsPayout && rowsPayout.length > 0) {
            let result = rowsPayout.reduce(
              (acc, obj) => acc + Number(obj.amount) || 0,
              0
            );
            totalPayoutAmount = result + totalPayoutAmount;
            const total = findingGet?.estimate - result;
            setNetAmount(total < 0 ? 0 : total);
          }

          setTotalAmountNew(totalPayoutAmount);

          let transactionFeeForPayout =
            getContact?.legacy_account === "yes" ? "495" : "595";

          if (
            (summary.represent === "buyer" ||
              summary.represent === "seller" ||
              summary.represent === "referral") &&
            getContact?.legacy_account === "yes"
          ) {
            transactionFeeForPayout = "495";
          } else if (
            (summary.represent === "buyer" ||
              summary.represent === "seller" ||
              summary.represent === "referral") &&
            getContact?.legacy_account === "no"
          ) {
            transactionFeeForPayout = "595";
          }

          // Wire Fee Logic
          const wireFeeForPayout =
            getContact?.legacy_account === "yes" ? "20" : "20";

          const PropertyForPayout =
            getContact?.legacy_account === "yes" ? "350" : "350";

          const baseFee = 300;
          const stepUpAmount = 500000;
          let purchasePrice = getCommission.purchase_price || 0;
          const parsedPurchasePrice = parseFloat(
            String(purchasePrice).replace(/[$,]/g, "")
          );
          let additionalSteps = 0;
          let additionalFee = 0;

          const difference = parsedPurchasePrice - 1500000;
          if (parsedPurchasePrice > 1500000) {
            if (difference > 0) {
              additionalSteps = difference / stepUpAmount;
              additionalSteps = Math.ceil(additionalSteps);
              additionalFee = baseFee * additionalSteps;
            } else {
              additionalFee = 0;
            }
          } else {
            additionalFee = 0;
          }
          setTransactionHighValueFee(additionalFee);

          if (totalAmount > Number(transactionFeeForPayout)) {
            const netPayAmount =
              Number(totalAmount) - Number(totalPayoutAmount) || 0;
            setNetAmount(netPayAmount < 0 ? 0 : netPayAmount);
          }

          if (totalAmount > 495) {
            const netPayAmountNew =
              Number(totalAmount) -
                Number(totalPayoutAmount) -
                Number(wireFeeForPayout) -
                Number(PropertyForPayout) -
                additionalFee || 0;

            setNetAmount(netPayAmountNew < 0 ? 0 : netPayAmountNew);
          }

          if (totalAmountNew) {
            const netPayAmountNew =
              Number(netAmount) +
                Number(totalPayoutAmount) +
                Number(wireFeeForPayout) +
                Number(PropertyForPayout) +
                additionalFee || 0;
            setTotalAmountNew(netPayAmountNew < 0 ? 0 : netPayAmountNew);
          }
        } else {
          let totalPayoutAmount = 0;

          if (transactionFesData && transactionFesData?.transactionFee) {
            totalPayoutAmount = Number(transactionFesData.transactionFee) || 0;
          }

          if (rowsPayout && rowsPayout.length > 0) {
            let result = rowsPayout.reduce(
              (acc, obj) => acc + Number(obj.amount) || 0,
              0
            );
            totalPayoutAmount = result + totalPayoutAmount;
            const total = findingGet?.estimate - result;
            setNetAmount(total < 0 ? 0 : total);
          }

          setTotalAmountNew(totalPayoutAmount);

          let transactionFeeForPayout =
            getContact?.legacy_account === "yes" ? "495" : "595";

          if (
            (summary.represent === "buyer" ||
              summary.represent === "seller" ||
              summary.represent === "referral") &&
            getContact?.legacy_account === "yes"
          ) {
            transactionFeeForPayout = "495";
          } else if (
            (summary.represent === "buyer" ||
              summary.represent === "seller" ||
              summary.represent === "referral") &&
            getContact?.legacy_account === "no"
          ) {
            transactionFeeForPayout = "595";
          }

          // Wire Fee Logic
          const wireFeeForPayout =
            getContact?.legacy_account === "yes" ? "20" : "20";

          const baseFee = 300;
          const stepUpAmount = 500000;
          let purchasePrice = getCommission.purchase_price || 0;
          const parsedPurchasePrice = parseFloat(
            String(purchasePrice).replace(/[$,]/g, "")
          );
          let additionalSteps = 0;
          let additionalFee = 0;

          const difference = parsedPurchasePrice - 1500000;
          if (parsedPurchasePrice > 1500000) {
            if (difference > 0) {
              additionalSteps = difference / stepUpAmount;
              additionalSteps = Math.ceil(additionalSteps);
              additionalFee = baseFee * additionalSteps;

              additionalSteps = Math.ceil(additionalSteps);

              additionalFee = baseFee * additionalSteps;
            } else {
              additionalFee = 0;
            }
          } else {
            additionalFee = 0;
          }
          setTransactionHighValueFee(additionalFee);

          if (totalAmount > Number(transactionFeeForPayout)) {
            const netPayAmount =
              Number(totalAmount) - Number(totalPayoutAmount) || 0;
            setNetAmount(netPayAmount < 0 ? 0 : netPayAmount);
          }

          if (totalAmount > 495) {
            const netPayAmountNew =
              Number(totalAmount) -
                Number(totalPayoutAmount) -
                Number(wireFeeForPayout) -
                additionalFee || 0;

            setNetAmount(netPayAmountNew < 0 ? 0 : netPayAmountNew);
          }

          if (totalAmountNew) {
            const netPayAmountNew =
              Number(netAmount) +
                Number(totalPayoutAmount) +
                Number(wireFeeForPayout) +
                additionalFee || 0;
            setTotalAmountNew(netPayAmountNew < 0 ? 0 : netPayAmountNew);
          }
        }
      }
    }
  }, [
    rowsPayout,
    formValues,
    getContact,
    transactionFesData,
    transactionBuyerFee,
    transactionSellerFee,
    transactionRisk,
    transactionWire,
    summary.represent,
    summary.type,
    findingGet?.estimate,
    totalAmount,
    totalAmountNew,
    netAmount,
  ]);

  const FundingRequestGet = async () => {
    if (params.id) {
      try {
        const response = await user_service.transactionFundingRequest(
          params.id
        );
        setLoader({ isActive: false });

        if (
          response &&
          response.data &&
          response.data.data &&
          response.data.data[0]
        ) {
          setFindingGet(response.data.data[0]);
          // console.log( response.data.data[0].commission);
          // setFormValues({

          //   commission: response.data.data[0].commission || "",
          // });

          let bonusAmount = [];
          let payoutItems = [];
          let bonusData = 0;
          let payoutData = 0;
          response.data.data[0].incomeItem.map((item) => {
            bonusData = parseInt(bonusData) + parseInt(item.amount);
            if (item.type) {
              bonusAmount.push(item);
            }
          });

          response.data.data[0].payoutItem.map((item) => {
            payoutData = parseInt(payoutData) + parseInt(item.amount);

            if (item.type) {
              payoutItems.push(item);
            }
          });

          setRows(bonusAmount);
          setRowsPayout(payoutItems);
        }
      } catch (error) {}
    }
  };

  useEffect(() => {
    setLoader({ isActive: true });
    TransactionGetById(params.id);
    FundingRequestGet();
    CommissionGet(params.id);
    TransactionClosing(params.id);
  }, []);

  const handleChanges = (e) => {
    const { name, value } = e.target;
    const newValue = parseFloat(value) || "";

    if (name === "commission") {
      setCommissionChanged(true);
    }

    setFormValues({
      ...formValues,
      [name]: newValue,
    });
    setFormErrorsNew((prevFormErrors) => ({
      ...prevFormErrors,
      commission: newValue ? "" : "Commission is required",
    }));
  };

  const handleChange = (e) => {
    const { name, value } = e.target;
    setFormValues({ ...formValues, [name]: value });

    if (!value && formErrorsNew[name]) {
      setFormErrorsNew({ ...formErrorsNew, [name]: `${name} is required` });
    } else if (formErrorsNew[name]) {
      // Clear error if field is no longer empty
      setFormErrorsNew({ ...formErrorsNew, [name]: "" });
    }
  };

  const handleChangeNew = (e) => {
    const { name, value } = e.target;
    setFormValuesNew({ ...formValuesNew, [name]: value });
  };

  const handleOptionChange = (e) => {
    const newValue = e.target.value;

    let chargedToName = "";

    if (summary?.represent === "buyer") {
      chargedToName = `${summary?.contact2?.[0]?.data?.firstName} ${summary?.contact2?.[0]?.data?.lastName}`;
    } else {
      chargedToName = `${summary?.contact1?.[0]?.data?.firstName} ${summary?.contact1?.[0]?.data?.lastName}`;
    }

    const isTypeExists = rows.some((row) => row.value === newValue);

    if (newValue && !isTypeExists) {
      const newRow = {
        type: newValue,
        amount: "",
        chargedto: newValue === "other" ? chargedToName : chargedToName,
      };
      setRows([...rows, newRow]);
    }
  };

  const handleChangeRow = (e, index, fieldName) => {
    const updatedValue = e.target.value;
    const updatedRows = [...rows];
    updatedRows[index][fieldName] = updatedValue;
    setRows(updatedRows);
  };

  const handleChangeCharge = (e, index, fieldCharge) => {
    const { value } = e.target;
    const updatedRows = [...rows];
    updatedRows[index][fieldCharge] = value;
    setRows(updatedRows);
  };

  const handleChangeBonus = (e, index, fieldType) => {
    const { value } = e.target;
    const updatedRows = [...rows];
    updatedRows[index][fieldType] = value;
    setRows(updatedRows);
  };

  const handleRemove = (index) => {
    const updatedRows = [...rows];
    updatedRows.splice(index, 1);
    setRows(updatedRows);
  };

  const handleChangeRowPayout = (e, index, fieldName) => {
    const updatedValue = e.target.value;
    const updatedRows = [...rowsPayout];
    updatedRows[index][fieldName] = updatedValue;
    setRowsPayout(updatedRows);
  };
  const handleInputChange = (e) => {
    const newValue = e.target.value;
    let chargedToName = "";

    if (summary?.represent === "buyer") {
      chargedToName = `${summary?.contact2?.[0]?.data?.firstName} ${summary?.contact2?.[0]?.data?.lastName}`;
    } else {
      chargedToName = `${summary?.contact1?.[0]?.data?.firstName} ${summary?.contact1?.[0]?.data?.lastName}`;
    }

    const isTypeExists = rowsPayout.some((row) => row.value === newValue);

    if (newValue && !isTypeExists) {
      const newRow = {
        type: newValue,
        amount: "",
        payto:
          newValue === "externalReferral" || newValue === "otherPayout"
            ? chargedToName
            : chargedToName,
      };
      setRowsPayout([...rowsPayout, newRow]);
    }
  };

  const handleChangePayout = (e, index, payoutType) => {
    const { value } = e.target;

    const updatedRowsPayout = [...rowsPayout];
    updatedRowsPayout[index][payoutType] = value;
    setRowsPayout(updatedRowsPayout);
  };

  const handleRemovePayout = (index) => {
    const updatedRows = [...rowsPayout];
    updatedRows.splice(index, 1);
    setRowsPayout(updatedRows);
  };

  const handleSubmit = async (e) => {
    if (findingGet?._id) {
      e.preventDefault();

      setIsShow(true);
      if (totalAmount === totalAmountNew) {
        const userData = {
          email: formValues.email,
          phone: formValues.phone,
          companyName: formValues.companyName,
          funding_method: formValues.funding_method,

          contactName: formValues.contactName,
          commission: JSON.stringify(formValues.commission)
            ? JSON.stringify(formValues.commission)
            : findingGet?.commission,
          fileNumber: formValues.fileNumber ?? findingGet.fileNumber,
          incomeItem: rows,
          payoutItem: rowsPayout,
          estimate: JSON.stringify(netAmount),

          transactionFee: transactionFesData.transactionFee,
          comments: formValuesNew.comments ?? findingGet?.comments,
          transactionId: summary._id,
          // invoice_url: fundingGet?.invoice_url ?? "",
          wireFee: transactionWire?.wireFee ?? "",
          buyerFee: transactionBuyerFee?.buyerFee ?? "",
          sellerFee: transactionSellerFee?.sellerFee ?? "",
          highValueFee: JSON.stringify(transactionHighValueFee) ?? "",
          transactionPersonal: transactionFesPersonal.transactionPersonal ?? "",
          managementFee: transactionRisk?.managementFee ?? "",
          addressProperty: summary?.propertyDetail?.streetAddress ?? "",
          stateProperty: summary?.propertyDetail?.state ?? "",
          typeProperty: summary?.propertyDetail?.propertyType ?? "",
          represent_Property:
            summary?.represent === "buyer"
              ? "Buyer"
              : summary?.represent === "seller"
              ? "Seller"
              : summary?.represent === "both"
              ? "Buyer&Seller"
              : summary?.represent === "referral"
              ? "Referral"
              : "",
          zipcode_Property: summary?.propertyDetail?.zipCode ?? "",
          //invoice_url: "",
        };

        try {
          setLoader({ isActive: true });
          const response = await user_service.fundingRequestUpdate(
            findingGet?._id,
            userData
          );
          if (response) {
            setSubmitData(response.data);
            setLoader({ isActive: false });
            generatePDF(response.data);
            setIsShow(false);
            handleRequestreview();
          } else {
            setLoader({ isActive: false });
            setIsShow(false);
            setToaster({
              type: "error",
              isShow: true,
              toasterBody: "Error: Funding Update Failed",
              message: "Error",
            });
          }
        } catch (error) {
          setIsShow(false);

          setToaster({
            type: "error",
            isShow: true,
            toasterBody: "Error: " + error.message,
            message: "Error",
          });
          setTimeout(() => {
            setToaster((prevToaster) => ({ ...prevToaster, isShow: false }));
          }, 2000);
        }
      } else {
        setToaster({
          type: "error",
          isShow: true,
          toasterBody:
            "Income Item total amount or Payout Item total amount do not match. Please review your entries.",
          message: "Validation Error",
        });

        setTimeout(() => {
          setToaster((prevToaster) => ({ ...prevToaster, isShow: false }));
        }, 5000);
      }
    }
     else {
      if (summary.phase === "canceled") {
        const userData = {
          email: formValues.email,
          agentId: jwt(localStorage.getItem("auth")).id,
          phone: formValues.phone,
          companyName: formValues.companyName,
          funding_method: formValues.funding_method,
          contactName: formValues.contactName,
          commission: formValues.commission ?? "0",
          fileNumber: formValues.fileNumber,
          incomeItem: rows,
          estimate: "0",
          // payoutItem: rowsPayout,
          transactionFee: transactionFesData.transactionFee,
          comments: formValuesNew.comments,
          transactionId: summary?._id,
          addressProperty: summary?.propertyDetail?.streetAddress ?? "",
          stateProperty: summary?.propertyDetail?.state ?? "",
          typeProperty: summary?.propertyDetail?.propertyType ?? "",
          represent_Property:
            summary?.represent === "buyer"
              ? "Buyer"
              : summary?.represent === "seller"
              ? "Seller"
              : summary?.represent === "both"
              ? "Buyer&Seller"
              : summary?.represent === "referral"
              ? "Referral"
              : "",
          zipcode_Property: summary?.propertyDetail?.zipCode ?? "",
        };
     
        try {
          setLoader({ isActive: true });
          const response = await user_service.fundingRequestPost(userData);

          if (response) {
            const userDatan = {
              addedBy: jwt(localStorage.getItem("auth")).id,
              agentId: jwt(localStorage.getItem("auth")).id,
              transactionId: params.id,
              message: "Standard Funding Request submitted.",
              // transaction_property_address: "ADDRESS 2",
              note_type: "Funding Request",
            };
            const responsen = await user_service.transactionNotes(userDatan);

            response.data.totalAmount = totalAmount;
            setSubmitData(response.data);
            setLoader({ isActive: false });
            generatePDF(response.data);
            handleRequestreview();
          }
        } catch (error) {
          setToaster({
            type: "error",
            isShow: true,
            message: "Error",
          });

          setTimeout(() => {
            setToaster((prevToaster) => ({
              ...prevToaster,
              isShow: false,
            }));
          }, 2000);
        }
      } else {
        if (formValuesClose) {
          if (totalAmount === totalAmountNew) {
            if (
              summary.type === "buy_commercial" ||
              summary.type === "sale_commercial" ||
              summary.type === "both_commercial" ||
              summary.type === "referral_commercial"
            ) {
              const userData = {
                email: formValues.email,
                agentId: jwt(localStorage.getItem("auth")).id,
                phone: formValues.phone,
                companyName: formValues.companyName,
                funding_method: formValues.funding_method,
                contactName: formValues.contactName,
                commission: JSON.stringify(formValues.commission),
                fileNumber: formValues.fileNumber,
                incomeItem: rows,
                estimate: JSON.stringify(netAmount),
                payoutItem: rowsPayout,
                transactionFee: transactionFesData.transactionFee,
                comments: formValuesNew.comments,
                transactionId: summary?._id,
                wireFee: transactionWire?.wireFee ?? "",
                buyerFee: transactionBuyerFee?.buyerFee ?? "",
                sellerFee: transactionSellerFee?.sellerFee ?? "",
                managementFee: transactionRisk?.managementFee ?? "",
                addressProperty: summary?.propertyDetail?.streetAddress ?? "",
                stateProperty: summary?.propertyDetail?.state ?? "",
                typeProperty: summary?.propertyDetail?.propertyType ?? "",
                represent_Property:
                  summary?.represent === "buyer"
                    ? "Buyer"
                    : summary?.represent === "seller"
                    ? "Seller"
                    : summary?.represent === "both"
                    ? "Buyer&Seller"
                    : summary?.represent === "referral"
                    ? "Referral"
                    : "",
                zipcode_Property: summary?.propertyDetail?.zipCode ?? "",
              };
      
              try {
                setLoader({ isActive: true });
                const response = await user_service.fundingRequestPost(
                  userData
                );

                if (response) {
                  const userDatan = {
                    addedBy: jwt(localStorage.getItem("auth")).id,
                    agentId: jwt(localStorage.getItem("auth")).id,
                    transactionId: params.id,
                    message: "Standard Funding Request submitted.",
                    // transaction_property_address: "ADDRESS 2",
                    note_type: "Funding Request",
                  };
                  const responsen = await user_service.transactionNotes(
                    userDatan
                  );

                  response.data.totalAmount = totalAmount;
                  setSubmitData(response.data);
                  setLoader({ isActive: false });
                  generatePDF(response.data);
                  handleRequestreview();
                }
              } catch (error) {
                setToaster({
                  type: "error",
                  isShow: true,
                  message: "Error",
                });

                setTimeout(() => {
                  setToaster((prevToaster) => ({
                    ...prevToaster,
                    isShow: false,
                  }));
                }, 2000);
              }
            } else {
              const userData = {
                email: formValues.email,
                agentId: jwt(localStorage.getItem("auth")).id,
                phone: formValues.phone,
                companyName: formValues.companyName,
                funding_method: formValues.funding_method,
                contactName: formValues.contactName,
                commission: JSON.stringify(formValues.commission),
                fileNumber: formValues.fileNumber,
                incomeItem: rows,
                estimate: JSON.stringify(netAmount),
                payoutItem: rowsPayout,
                transactionFee: transactionFesData.transactionFee,
                comments: formValuesNew.comments,
                transactionId: summary?._id,
                wireFee: transactionWire?.wireFee ?? "",
                highValueFee:
                  JSON.stringify(transactionHighValueFee ?? "") ?? "",
                transactionPersonal:
                  transactionFesPersonal?.transactionPersonal ?? "",
                buyerFee: transactionBuyerFee?.buyerFee ?? "",
                sellerFee: transactionSellerFee?.sellerFee ?? "",
                managementFee: transactionRisk?.managementFee ?? "",
                addressProperty: summary?.propertyDetail?.streetAddress ?? "",
                stateProperty: summary?.propertyDetail?.state ?? "",
                typeProperty: summary?.propertyDetail?.propertyType ?? "",
                represent_Property:
                  summary?.represent === "buyer"
                    ? "Buyer"
                    : summary?.represent === "seller"
                    ? "Seller"
                    : summary?.represent === "both"
                    ? "Buyer&Seller"
                    : summary?.represent === "referral"
                    ? "Referral"
                    : "",
                zipcode_Property: summary?.propertyDetail?.zipCode ?? "",
              };
              try {
                setLoader({ isActive: true });
                const response = await user_service.fundingRequestPost(
                  userData
                );

                if (response) {
                  const userDatan = {
                    addedBy: jwt(localStorage.getItem("auth")).id,
                    agentId: jwt(localStorage.getItem("auth")).id,
                    transactionId: params.id,
                    message: "Standard Funding Request submitted.",
                    // transaction_property_address: "ADDRESS 2",
                    note_type: "Funding Request",
                  };
                  const responsen = await user_service.transactionNotes(
                    userDatan
                  );

                  response.data.totalAmount = totalAmount;
                  setSubmitData(response.data);
                  setLoader({ isActive: false });
                  generatePDF(response.data);
                  handleRequestreview();
                }
              } catch (error) {
                setToaster({
                  type: "error",
                  isShow: true,
                  message: "Error",
                });

                setTimeout(() => {
                  setToaster((prevToaster) => ({
                    ...prevToaster,
                    isShow: false,
                  }));
                }, 2000);
              }
            }
          } else {
            setToaster({
              type: "error",
              isShow: true,
              message: "Income Item total amount or Payout Item total amount do not match. Please review your entries.",
            });

            setTimeout(() => {
              setToaster((prevToaster) => ({ ...prevToaster, isShow: false }));
            }, 5000);
          }
        } else {
          for (const key in formErrorsNew) {
            if (formErrorsNew[key]) {
              // If there's any error, display the toaster message and exit
              setToaster({
                types: "error",
                isShow: true,
                message: "Error",
              });

              setTimeout(() => {
                setToaster((prevToaster) => ({
                  ...prevToaster,
                  isShow: false,
                }));
              }, 2000);

              return;
            }
          }
          if (totalAmount === totalAmountNew) {
            if (
              summary.type === "buy_commercial" ||
              summary.type === "sale_commercial" ||
              summary.type === "both_commercial" ||
              summary.type === "referral_commercial"
            ) {
              const userDataNew = {
                agentId: jwt(localStorage.getItem("auth")).id,
                title_company: formValues.companyName,
                companyname: formValues.contactName,
                file_gfnumber: formValues.fileNumber,
                contact_number: formValues.phone,
                contactemail: formValues.email,
                transactionId: params.id,
              };

              const userData = {
                email: formValues.email,
                agentId: jwt(localStorage.getItem("auth")).id,
                phone: formValues.phone,
                companyName: formValues.companyName,
                funding_method: formValues.funding_method,
                contactName: formValues.contactName,
                commission: JSON.stringify(formValues.commission),
                fileNumber: formValues.fileNumber,
                incomeItem: rows,
                estimate: JSON.stringify(netAmount),
                payoutItem: rowsPayout,
                transactionFee: transactionFesData.transactionFee,
                comments: formValuesNew.comments,
                transactionId: summary?._id,
                wireFee: transactionWire?.wireFee ?? "",
                buyerFee: transactionBuyerFee?.buyerFee ?? "",
                sellerFee: transactionSellerFee?.sellerFee ?? "",
                managementFee: transactionRisk?.managementFee ?? "",
                addressProperty: summary?.propertyDetail?.streetAddress ?? "",
                stateProperty: summary?.propertyDetail?.state ?? "",
                typeProperty: summary?.propertyDetail?.propertyType ?? "",
                represent_Property:
                  summary?.represent === "buyer"
                    ? "Buyer"
                    : summary?.represent === "seller"
                    ? "Seller"
                    : summary?.represent === "both"
                    ? "Buyer&Seller"
                    : summary?.represent === "referral"
                    ? "Referral"
                    : "",
                zipcode_Property: summary?.propertyDetail?.zipCode ?? "",
              };
              const responseData = await user_service.transactionClosingPost(
                userDataNew
              );
              try {
                setLoader({ isActive: true });
                const response = await user_service.fundingRequestPost(
                  userData
                );

                if (response) {
                  const userDatan = {
                    addedBy: jwt(localStorage.getItem("auth")).id,
                    agentId: jwt(localStorage.getItem("auth")).id,
                    transactionId: params.id,
                    message: "Standard Funding Request submitted.",
                    // transaction_property_address: "ADDRESS 2",
                    note_type: "Funding Request",
                  };
                  const responsen = await user_service.transactionNotes(
                    userDatan
                  );

                  response.data.totalAmount = totalAmount;
                  setSubmitData(response.data);
                  setLoader({ isActive: false });
                  generatePDF(response.data);
                  handleRequestreview();
                }
              } catch (error) {
                setToaster({
                  type: "error",
                  isShow: true,
                  message: "Error",
                });

                setTimeout(() => {
                  setToaster((prevToaster) => ({
                    ...prevToaster,
                    isShow: false,
                  }));
                }, 2000);
              }
            } else {
              const userDataNew = {
                agentId: jwt(localStorage.getItem("auth")).id,
                title_company: formValues.companyName,
                companyname: formValues.contactName,
                file_gfnumber: formValues.fileNumber,
                contact_number: formValues.phone,
                contactemail: formValues.email,
                transactionId: params.id,
              };
           
              const userData = {
                email: formValues.email,
                agentId: jwt(localStorage.getItem("auth")).id,
                phone: formValues.phone,
                companyName: formValues.companyName,
                funding_method: formValues.funding_method,
                contactName: formValues.contactName,
                commission: JSON.stringify(formValues.commission),
                fileNumber: formValues.fileNumber,
                incomeItem: rows,
                estimate: JSON.stringify(netAmount),
                payoutItem: rowsPayout,
                transactionFee: transactionFesData.transactionFee,
                comments: formValuesNew.comments,
                transactionId: summary?._id,
                wireFee: transactionWire?.wireFee ?? "",
                highValueFee:
                  JSON.stringify(transactionHighValueFee ?? "") ?? "",
                transactionPersonal:
                  transactionFesPersonal?.transactionPersonal ?? "",
                buyerFee: transactionBuyerFee?.buyerFee ?? "",
                sellerFee: transactionSellerFee?.sellerFee ?? "",
                managementFee: transactionRisk?.managementFee ?? "",
                addressProperty: summary?.propertyDetail?.streetAddress ?? "",
                stateProperty: summary?.propertyDetail?.state ?? "",
                typeProperty: summary?.propertyDetail?.propertyType ?? "",
                represent_Property:
                  summary?.represent === "buyer"
                    ? "Buyer"
                    : summary?.represent === "seller"
                    ? "Seller"
                    : summary?.represent === "both"
                    ? "Buyer&Seller"
                    : summary?.represent === "referral"
                    ? "Referral"
                    : "",
                zipcode_Property: summary?.propertyDetail?.zipCode ?? "",
              };
              const responseData = await user_service.transactionClosingPost(
                userDataNew
              );
             
              try {
                setLoader({ isActive: true });
                const response = await user_service.fundingRequestPost(
                  userData
                );

                if (response) {
                  const userDatan = {
                    addedBy: jwt(localStorage.getItem("auth")).id,
                    agentId: jwt(localStorage.getItem("auth")).id,
                    transactionId: params.id,
                    message: "Standard Funding Request submitted.",
                    // transaction_property_address: "ADDRESS 2",
                    note_type: "Funding Request",
                  };
                  const responsen = await user_service.transactionNotes(
                    userDatan
                  );

                  response.data.totalAmount = totalAmount;
                  setSubmitData(response.data);
                  setLoader({ isActive: false });
                  generatePDF(response.data);
                  handleRequestreview();
                }
              } catch (error) {
                setToaster({
                  type: "error",
                  isShow: true,
                  message: "Error",
                });

                setTimeout(() => {
                  setToaster((prevToaster) => ({
                    ...prevToaster,
                    isShow: false,
                  }));
                }, 2000);
              }
            }
          } else {
            setToaster({
              type: "error",
              isShow: true,
              message: "Income Item total amount or Payout Item total amount do not match. Please review your entries.",
            });

            setTimeout(() => {
              setToaster((prevToaster) => ({ ...prevToaster, isShow: false }));
            }, 5000);
          }
        }
      }
    }
  };

  const generatePDF = async (submitData) => {
    try {
      const doc = new jsPDF();

      const styles = `
            <style>
              body {
                font-family: Arial, sans-serif;
                margin: 0;
                padding: 20px;
              }
              h1 {
                text-align: center;
                font-size: 24px;
              }
              table {
                width: 100%;
                border-collapse: collapse;
                margin-top: 20px;
              }
              th, td {
                border: 1px solid #ddd;
                padding: 8px;
                text-align: left;
              }
              th {
                background-color: #ccc;
              }
              tr:nth-child(even) {
                background-color: #f5f5f5;
              }
            </style>
          `;

      const imageWidth = 50;
      const imageHeight = 25;
      const leftMargin = 10;
      const topMargin = 10;
      doc.addImage(
        image,
        "JPEG",
        leftMargin,
        topMargin,
        imageWidth,
        imageHeight,
        "FAST"
      );

      const pageWidth = doc.internal.pageSize.getWidth();
      const pageHeight = doc.internal.pageSize.getHeight();

      if (
        summary.type === "buy_commercial" ||
        summary.type === "sale_commercial" ||
        summary.type === "both_commercial" ||
        summary.type === "referral_commercial"
      ) {
        if (getCommission.property_interest === "yes") {
          doc.html(
            styles +
              '<h2 style="font-size: 4px; margin-top: 37px; margin-left: 15px;">Funding Request</h2>',
            {
              callback: async () => {
                if (summary) {
                  const representName =
                    summary?.represent === "seller"
                      ? "Seller"
                      : summary?.represent === "buyer"
                      ? "Buyer"
                      : summary?.represent === "both"
                      ? "Buyer & Seller"
                      : summary?.represent === "referral"
                      ? "Referral"
                      : "";

                  let contactTypeData = "";
                  if (contactData.type === "seller") {
                    contactTypeData = "Seller";
                  } else if (contactData.type === "buyer") {
                    contactTypeData = "Buyer";
                  } else if (contactData.type === "buyersale") {
                    contactTypeData = "Buyer & Seller";
                  } else if (contactData.type === "referral_sellerAgent") {
                    contactTypeData = "Buyer";
                  }

                  let contactnameData = "";
                  if (contactData.type === "referral_sellerAgent") {
                    contactnameData = "";
                  } else {
                    contactnameData = `${summary?.contact2[0]?.data.firstName} ${summary?.contact2[0]?.data.lastName}`;
                  }

                  const tableDatas = [
                    [
                      "Agent",
                      "Type",
                      representName,
                      "Sale Price",
                      contactTypeData,
                    ],

                    [
                      `${summary?.contact3[0]?.data.firstName} ${summary?.contact3[0]?.data.lastName}`,
                      `${
                        summary?.category ? summary?.category : ""
                      } ${representName}`,
                      `${summary?.contact1[0]?.data.firstName} ${summary?.contact1[0]?.data.lastName}`,
                      // summary?.propertyDetail.price,
                      getCommission.purchase_price,
                      contactnameData,
                    ],
                  ];

                  const tableStylesNew = {
                    theme: "grid",
                    tableWidth: "auto",
                    styles: { fontSize: 10 },
                    margin: { top: 50 },
                  };

                  doc.autoTable({
                    head: [],
                    body: tableDatas,
                    ...tableStylesNew,
                  });
                }

                const tableStylesNew = {
                  theme: "grid",
                  tableWidth: "auto",
                  styles: { fontSize: 10 },
                  margin: { top: 50 },
                };

                const headingText = "Settlement Charges";

                const fontSize = 11;
                const leftMargin = 14;
                const topMargin = 71;

                doc.setFontSize(fontSize);

                doc.text(headingText, leftMargin, topMargin);

                const tableHeaders = ["Charged To", "For Service", "Amount"];

                let contactName = "";
                let commissionType = "";

                if (summary?.represent === "buyer") {
                  contactName =
                    `Seller - ${summary?.contact2[0]?.data.firstName} ${summary?.contact2[0]?.data.lastName}` ||
                    "";
                  commissionType = "Buyer Commission";
                } else if (summary?.represent === "seller") {
                  contactName =
                    `Seller - ${summary?.contact1[0]?.data.firstName} ${summary?.contact1[0]?.data.lastName}` ||
                    "";
                  commissionType = "Seller Commission";
                } else if (summary?.represent === "both") {
                  contactName =
                    `Seller - ${summary?.contact1[0]?.data.firstName} ${summary?.contact1[0]?.data.lastName}` ||
                    "";
                  commissionType = "Buyer & Seller Commission";
                } else if (summary?.represent === "referral") {
                  contactName =
                    `Seller - ${summary?.contact1[0]?.data.firstName} ${summary?.contact1[0]?.data.lastName}` ||
                    "";
                  commissionType = "Referral Commission";
                }

                const commissionRow = [
                  contactName,
                  commissionType,
                  `$${submitData?.commission || 0}`,
                ];

                if (summary.phase === "canceled") {
                  const totalCharges = parseFloat(submitData?.commission);

                  const totalChargesRow = [
                    "Total Charges",
                    "",

                    `$${totalCharges.toFixed(2)}`,
                  ];

                  const tableDataCommission = [
                    tableHeaders,
                    commissionRow,
                    totalChargesRow,
                  ];
                  doc.autoTable({ body: tableDataCommission, tableStylesNew });

                  const tablePayoutHeader = ["Pay To", "For Service", "Amount"];
                  const estimateRow = [
                    `${summary?.contact3[0]?.data.firstName} ${summary?.contact3[0]?.data.lastName}` ||
                      "",
                    summary?.category || "Net Pay Due to Agent (Estimate)",
                    `$${Number(submitData?.estimate || 0).toFixed(2)}`, // Assuming estimate is a numeric value
                  ];
                  const transactionRow = [
                    "In Depth Realty",
                    "Transaction Fee",
                    `$${submitData?.transactionFee || 0}`,
                  ];

                  const totalChargesPauout =
                    parseFloat(submitData?.estimate) +
                    parseFloat(submitData?.transactionFee);

                  const totalChargesRowPayout = [
                    "Total Charges",
                    "",

                    `$${totalChargesPauout.toFixed(2)}`,
                  ];

                  const tableDataEstimate = [
                    tablePayoutHeader,
                    estimateRow,
                    transactionRow,
                    totalChargesRowPayout,
                  ];
                  doc.autoTable({ body: tableDataEstimate, tableStylesNew });
                } else {
                  const incomeItemRows = submitData?.incomeItem.map((item) => [
                    item.chargedto,
                    item.type,
                    `$${item.amount}`,
                  ]);

                  const totalCharges =
                    parseFloat(submitData?.commission) +
                    submitData?.incomeItem.reduce(
                      (total, item) => total + parseFloat(item.amount),
                      0
                    );

                  // Define the "Total Charges" row
                  const totalChargesRow = [
                    "Total Charges",
                    "",

                    `$${totalCharges.toFixed(2)}`,
                  ];
                  const tableDataCommission = [
                    tableHeaders,
                    commissionRow,
                    ...incomeItemRows,
                    totalChargesRow,
                  ];
                  doc.autoTable({ body: tableDataCommission, tableStylesNew });

                  const tablePayoutHeader = ["Pay To", "For Service", "Amount"];

                  const estimateRow = [
                    `${summary?.contact3[0]?.data.firstName} ${summary?.contact3[0]?.data.lastName}` ||
                      "",
                    summary?.category || "Net Pay Due to Agent (Estimate)",
                    `$${Number(submitData?.estimate || 0).toFixed(2)}`, // Assuming estimate is a numeric value
                  ];

                  const transactionRow = [
                    "In Depth Realty",
                    "Transaction Fee",
                    `$${submitData?.transactionFee || 0}`,
                  ];

                  const transactionWireFee = [
                    "In Depth Realty",
                    "Wire Fee",
                    `$${submitData?.wireFee || 0}`,
                  ];

                  const pauoutItemRows = submitData?.payoutItem.map((item) => [
                    item.payto,
                    item.type,
                    `$${item.amount}`,
                  ]);

                  const totalChargesPauout =
                    parseFloat(submitData?.estimate) +
                    parseFloat(submitData?.transactionFee) +
                    parseFloat(submitData?.wireFee) +
                    submitData?.payoutItem.reduce(
                      (total, item) => total + parseFloat(item.amount),
                      0
                    );

                  const totalChargesRowPayout = [
                    "Total Charges",
                    "",

                    `$${totalChargesPauout.toFixed(2)}`,
                  ];

                  const tableDataEstimate = [
                    tablePayoutHeader,
                    estimateRow,
                    transactionRow,
                    transactionWireFee,
                    ...pauoutItemRows,
                    totalChargesRowPayout,
                  ];
                  doc.autoTable({ body: tableDataEstimate, tableStylesNew });
                }

                const commentText = submitData?.comments || "";
                const splitTextIntoChunks = (text, chunkSize) => {
                  const words = text.split(" ");
                  const chunks = [];
                  for (let i = 0; i < words.length; i += chunkSize) {
                    chunks.push(words.slice(i, i + chunkSize).join(" "));
                  }
                  return chunks;
                };
                const commentChunks = splitTextIntoChunks(commentText, 15);

                const commentsX = 10;
                let commentsY = 10 + 180;
                doc.setFontSize(10);
                doc.text("Notes:", commentsX, commentsY);
                commentsY += 10;

                commentChunks.forEach((chunk) => {
                  doc.text(chunk, commentsX, commentsY);
                  commentsY += 15; // Move to the next line
                });

                const pdfBlob = doc.output("blob");
                const pdfFormData = new FormData();
                pdfFormData.append("file", pdfBlob, "generated.pdf");

                const apiUrl = "https://api.brokeragentbase.com/upload";
                const config = {
                  headers: {
                    "Content-Type": "multipart/form-data",
                  },
                };

                const response = await axios.post(apiUrl, pdfFormData, config);
                setPdfPath(response.data);

                const userData = {
                  invoice_url: response.data,
                };

                if (submitData?._id) {
                  try {
                    // Update Funding Request
                    const updateResponse =
                      await user_service.fundingRequestUpdate(
                        submitData?._id,
                        userData
                      );

                    if (updateResponse) {
                      // Update successful, now send email
                      const uploadResponse =
                        await user_service.fundingEmailPost(
                          updateResponse.data
                        );
                      // setToaster({
                      //   type: "success",
                      //   isShow: true,
                      //   message: "Funding Updated Successfully",
                      // });

                      setTimeout(() => {
                        setToaster((prevToaster) => ({
                          ...prevToaster,
                          isShow: false,
                        }));
                        // navigate(`/transaction-review/${params.id}`);
                      }, 2000);
                    } else {
                      // Update failed
                      setLoader({ isActive: false });

                      setToaster({
                        type: "error",
                        isShow: true,
                        toasterBody: "Error updating Funding",
                        message: "Error",
                      });
                    }
                  } catch (error) {
                    console.error("Error updating Funding:", error);

                    setLoader({ isActive: false });

                    setToaster({
                      type: "error",
                      isShow: true,
                      toasterBody: "An error occurred while updating Funding",
                      message: "Error",
                    });
                  }
                }
              },
            }
          );
        } else {
          doc.html(
            styles +
              '<h2 style="font-size: 4px; margin-top: 37px; margin-left: 15px;">Funding Request</h2>',
            {
              callback: async () => {
                if (summary) {
                  const representName =
                    summary?.represent === "seller"
                      ? "Seller"
                      : summary?.represent === "buyer"
                      ? "Buyer"
                      : summary?.represent === "both"
                      ? "Buyer & Seller"
                      : summary?.represent === "referral"
                      ? "Referral"
                      : "";

                  let contactTypeData = "";
                  if (contactData.type === "seller") {
                    contactTypeData = "Seller";
                  } else if (contactData.type === "buyer") {
                    contactTypeData = "Buyer";
                  } else if (contactData.type === "buyersale") {
                    contactTypeData = "Buyer & Seller";
                  } else if (contactData.type === "referral_sellerAgent") {
                    contactTypeData = "Buyer";
                  }

                  let contactnameData = "";
                  if (contactData.type === "referral_sellerAgent") {
                    contactnameData = "";
                  } else {
                    contactnameData = `${summary?.contact2[0]?.data.firstName} ${summary?.contact2[0]?.data.lastName}`;
                  }

                  const tableDatas = [
                    [
                      "Agent",
                      "Type",
                      representName,
                      "Sale Price",
                      contactTypeData,
                    ],

                    [
                      `${summary?.contact3[0]?.data.firstName} ${summary?.contact3[0]?.data.lastName}`,
                      `${
                        summary?.category ? summary?.category : ""
                      } ${representName}`,
                      `${summary?.contact1[0]?.data.firstName} ${summary?.contact1[0]?.data.lastName}`,
                      getCommission.purchase_price,
                      contactnameData,
                      // summary?.propertyDetail.price,
                    ],
                  ];

                  const tableStylesNew = {
                    theme: "grid",
                    tableWidth: "auto",
                    styles: { fontSize: 10 },
                    margin: { top: 50 },
                  };

                  doc.autoTable({
                    head: [],
                    body: tableDatas,
                    ...tableStylesNew,
                  });
                }

                const tableStylesNew = {
                  theme: "grid",
                  tableWidth: "auto",
                  styles: { fontSize: 10 },
                  margin: { top: 50 },
                };

                const headingText = "Settlement Charges";

                const fontSize = 11;
                const leftMargin = 14;
                const topMargin = 71;

                doc.setFontSize(fontSize);

                doc.text(headingText, leftMargin, topMargin);

                const tableHeaders = ["Charged To", "For Service", "Amount"];

                let contactName = "";
                let commissionType = "";

                if (summary?.represent === "buyer") {
                  contactName =
                    `Seller - ${summary?.contact2[0]?.data.firstName} ${summary?.contact2[0]?.data.lastName}` ||
                    "";
                  commissionType = "Buyer Commission";
                } else if (summary?.represent === "seller") {
                  contactName =
                    `Seller - ${summary?.contact1[0]?.data.firstName} ${summary?.contact1[0]?.data.lastName}` ||
                    "";
                  commissionType = "Seller Commission";
                } else if (summary?.represent === "both") {
                  contactName =
                    `Seller - ${summary?.contact1[0]?.data.firstName} ${summary?.contact1[0]?.data.lastName}` ||
                    "";
                  commissionType = "Buyer & Seller Commission";
                } else if (summary?.represent === "referral") {
                  contactName =
                    `Seller - ${summary?.contact1[0]?.data.firstName} ${summary?.contact1[0]?.data.lastName}` ||
                    "";
                  commissionType = "Referral Commission";
                }

                const commissionRow = [
                  contactName,
                  commissionType,
                  `$${submitData?.commission || 0}`,
                ];

                if (summary.phase === "canceled") {
                  const totalCharges = parseFloat(submitData?.commission);

                  const totalChargesRow = [
                    "Total Charges",
                    "",

                    `$${totalCharges.toFixed(2)}`,
                  ];

                  const tableDataCommission = [
                    tableHeaders,
                    commissionRow,
                    totalChargesRow,
                  ];
                  doc.autoTable({ body: tableDataCommission, tableStylesNew });

                  const tablePayoutHeader = ["Pay To", "For Service", "Amount"];
                  const estimateRow = [
                    `${summary?.contact3[0]?.data.firstName} ${summary?.contact3[0]?.data.lastName}` ||
                      "",
                    summary?.category || "Net Pay Due to Agent (Estimate)",
                    `$${Number(submitData?.estimate || 0).toFixed(2)}`, // Assuming estimate is a numeric value
                  ];
                  const transactionRow = [
                    "In Depth Realty",
                    "Transaction Fee",
                    `$${submitData?.transactionFee || 0}`,
                  ];

                  const totalChargesPauout =
                    parseFloat(submitData?.estimate) +
                    parseFloat(submitData?.transactionFee);

                  const totalChargesRowPayout = [
                    "Total Charges",
                    "",

                    `$${totalChargesPauout.toFixed(2)}`,
                  ];

                  const tableDataEstimate = [
                    tablePayoutHeader,
                    estimateRow,
                    transactionRow,
                    totalChargesRowPayout,
                  ];
                  doc.autoTable({ body: tableDataEstimate, tableStylesNew });
                } else {
                  const incomeItemRows = submitData?.incomeItem.map((item) => [
                    item.chargedto,
                    item.type,
                    `$${item.amount}`,
                  ]);

                  const totalCharges =
                    parseFloat(submitData?.commission) +
                    submitData?.incomeItem.reduce(
                      (total, item) => total + parseFloat(item.amount),
                      0
                    );

                  // Define the "Total Charges" row
                  const totalChargesRow = [
                    "Total Charges",
                    "",

                    `$${totalCharges.toFixed(2)}`,
                  ];
                  const tableDataCommission = [
                    tableHeaders,
                    commissionRow,
                    ...incomeItemRows,
                    totalChargesRow,
                  ];
                  doc.autoTable({ body: tableDataCommission, tableStylesNew });

                  const tablePayoutHeader = ["Pay To", "For Service", "Amount"];
                  const estimateRow = [
                    `${summary?.contact3[0]?.data.firstName} ${summary?.contact3[0]?.data.lastName}` ||
                      "",
                    summary?.category || "Net Pay Due to Agent (Estimate)",
                    `$${Number(submitData?.estimate || 0).toFixed(2)}`,
                  ];

                  const transactionRow = [
                    "In Depth Realty",
                    "Transaction Fee",
                    `$${submitData?.transactionFee || 0}`,
                  ];

                  const transactionWireFee = [
                    "In Depth Realty",
                    "Wire Fee",
                    `$${submitData?.wireFee || 0}`,
                  ];

                  const pauoutItemRows = submitData?.payoutItem.map((item) => [
                    item.payto,
                    item.type,
                    `$${item.amount}`,
                  ]);

                  const totalChargesPauout =
                    parseFloat(submitData?.estimate) +
                    parseFloat(submitData?.transactionFee) +
                    parseFloat(submitData?.wireFee) +
                    submitData?.payoutItem.reduce(
                      (total, item) => total + parseFloat(item.amount),
                      0
                    );

                  const totalChargesRowPayout = [
                    "Total Charges",
                    "",

                    `$${totalChargesPauout.toFixed(2)}`,
                  ];

                  const tableDataEstimate = [
                    tablePayoutHeader,
                    estimateRow,
                    transactionRow,
                    transactionWireFee,
                    ...pauoutItemRows,
                    totalChargesRowPayout,
                  ];
                  doc.autoTable({ body: tableDataEstimate, tableStylesNew });
                }

                const commentText = submitData?.comments || "";
                const splitTextIntoChunks = (text, chunkSize) => {
                  const words = text.split(" ");
                  const chunks = [];
                  for (let i = 0; i < words.length; i += chunkSize) {
                    chunks.push(words.slice(i, i + chunkSize).join(" "));
                  }
                  return chunks;
                };
                const commentChunks = splitTextIntoChunks(commentText, 15);

                const commentsX = 10;
                let commentsY = 10 + 180;
                doc.setFontSize(10);
                doc.text("Notes:", commentsX, commentsY);
                commentsY += 10;

                commentChunks.forEach((chunk) => {
                  doc.text(chunk, commentsX, commentsY);
                  commentsY += 15; // Move to the next line
                });

                const pdfBlob = doc.output("blob");
                const pdfFormData = new FormData();
                pdfFormData.append("file", pdfBlob, "generated.pdf");

                const apiUrl = "https://api.brokeragentbase.com/upload";
                const config = {
                  headers: {
                    "Content-Type": "multipart/form-data",
                  },
                };

                const response = await axios.post(apiUrl, pdfFormData, config);
                setPdfPath(response.data);

                const userData = {
                  invoice_url: response.data,
                };

                if (submitData?._id) {
                  try {
                    // Update Funding Request
                    const updateResponse =
                      await user_service.fundingRequestUpdate(
                        submitData?._id,
                        userData
                      );

                    if (updateResponse) {
                      // Update successful, now send email
                      const uploadResponse =
                        await user_service.fundingEmailPost(
                          updateResponse.data
                        );

                      setLoader({ isActive: false });

                      // setToaster({
                      //   type: "success",
                      //   isShow: true,
                      //   message: "Funding Updated Successfully",
                      // });

                      setTimeout(() => {
                        setToaster((prevToaster) => ({
                          ...prevToaster,
                          isShow: false,
                        }));
                        // navigate(`/transaction-review/${params.id}`);
                      }, 2000);
                    } else {
                      // Update failed
                      setLoader({ isActive: false });

                      setToaster({
                        type: "error",
                        isShow: true,
                        toasterBody: "Error updating Funding",
                        message: "Error",
                      });
                    }
                  } catch (error) {
                    console.error("Error updating Funding:", error);

                    setLoader({ isActive: false });

                    setToaster({
                      type: "error",
                      isShow: true,
                      toasterBody: "An error occurred while updating Funding",
                      message: "Error",
                    });
                  }
                }
              },
            }
          );
        }
      } else {
        if (summary?.represent === "both") {
          if (getCommission.property_interest === "yes") {
            doc.html(
              styles +
                '<h2 style="font-size: 4px; margin-top: 37px; margin-left: 15px;">Funding Request</h2>',
              {
                callback: async () => {
                  if (summary) {
                    const representName =
                      summary?.represent === "seller"
                        ? "Seller"
                        : summary?.represent === "buyer"
                        ? "Buyer"
                        : summary?.represent === "both"
                        ? "Buyer & Seller"
                        : summary?.represent === "referral"
                        ? "Referral"
                        : "";

                    let contactTypeData = "";
                    if (contactData.type === "seller") {
                      contactTypeData = "Seller";
                    } else if (contactData.type === "buyer") {
                      contactTypeData = "Buyer";
                    } else if (contactData.type === "buyersale") {
                      contactTypeData = "Buyer & Seller";
                    } else if (contactData.type === "referral_sellerAgent") {
                      contactTypeData = "Buyer";
                    }

                    let contactnameData = "";
                    if (contactData.type === "referral_sellerAgent") {
                      contactnameData = "";
                    } else {
                      contactnameData = `${summary?.contact2[0]?.data.firstName} ${summary?.contact2[0]?.data.lastName}`;
                    }

                    const tableDatas = [
                      [
                        "Agent",
                        "Type",
                        representName,
                        "Sale Price",
                        contactTypeData,
                      ],

                      [
                        `${summary?.contact3[0]?.data.firstName} ${summary?.contact3[0]?.data.lastName}`,
                        `${
                          summary?.category ? summary?.category : ""
                        } ${representName}`,
                        `${summary?.contact1[0]?.data.firstName} ${summary?.contact1[0]?.data.lastName}`,
                        getCommission.purchase_price,
                        contactnameData,
                        // summary?.propertyDetail.price,
                      ],
                    ];

                    const tableStylesNew = {
                      theme: "grid",
                      tableWidth: "auto",
                      styles: { fontSize: 10 },
                      margin: { top: 50 },
                    };

                    doc.autoTable({
                      head: [],
                      body: tableDatas,
                      ...tableStylesNew,
                    });
                  }

                  const tableStylesNew = {
                    theme: "grid",
                    tableWidth: "auto",
                    styles: { fontSize: 10 },
                    margin: { top: 50 },
                  };

                  const headingText = "Settlement Charges";

                  const fontSize = 11;
                  const leftMargin = 14;
                  const topMargin = 71;

                  doc.setFontSize(fontSize);

                  doc.text(headingText, leftMargin, topMargin);

                  const tableHeaders = ["Charged To", "For Service", "Amount"];

                  let contactName = "";
                  let commissionType = "";

                  if (summary?.represent === "buyer") {
                    contactName =
                      `Seller - ${summary?.contact2[0]?.data.firstName} ${summary?.contact2[0]?.data.lastName}` ||
                      "";
                    commissionType = "Buyer Commission";
                  } else if (summary?.represent === "seller") {
                    contactName =
                      `Seller - ${summary?.contact1[0]?.data.firstName} ${summary?.contact1[0]?.data.lastName}` ||
                      "";
                    commissionType = "Seller Commission";
                  } else if (summary?.represent === "both") {
                    contactName =
                      `Seller - ${summary?.contact1[0]?.data.firstName} ${summary?.contact1[0]?.data.lastName}` ||
                      "";
                    commissionType = "Buyer & Seller Commission";
                  } else if (summary?.represent === "referral") {
                    contactName =
                      `Seller - ${summary?.contact1[0]?.data.firstName} ${summary?.contact1[0]?.data.lastName}` ||
                      "";
                    commissionType = "Referral Commission";
                  }

                  const commissionRow = [
                    contactName,
                    commissionType,
                    `$${submitData?.commission || 0}`,
                  ];

                  if (summary.phase === "canceled") {
                    const totalCharges = parseFloat(submitData?.commission);

                    const totalChargesRow = [
                      "Total Charges",
                      "",

                      `$${totalCharges.toFixed(2)}`,
                    ];
                    const tableDataCommission = [
                      tableHeaders,
                      commissionRow,
                      totalChargesRow,
                    ];
                    doc.autoTable({
                      body: tableDataCommission,
                      tableStylesNew,
                    });

                    const tablePayoutHeader = [
                      "Pay To",
                      "For Service",
                      "Amount",
                    ];
                    const estimateRow = [
                      `${summary?.contact3[0]?.data.firstName} ${summary?.contact3[0]?.data.lastName}` ||
                        "",
                      summary?.category || "Net Pay Due to Agent (Estimate)",
                      `$${Number(submitData?.estimate || 0).toFixed(2)}`, // Assuming estimate is a numeric value
                    ];
                    const transactionRow = [
                      "In Depth Realty",
                      "Transaction Fee",
                      `$${submitData?.transactionFee || 0}`,
                    ];

                    const totalChargesPauout =
                      parseFloat(submitData?.estimate) +
                      parseFloat(submitData?.transactionFee);

                    const totalChargesRowPayout = [
                      "Total Charges",
                      "",

                      `$${totalChargesPauout.toFixed(2)}`,
                    ];

                    const tableDataEstimate = [
                      tablePayoutHeader,
                      estimateRow,
                      transactionRow,
                      totalChargesRowPayout,
                    ];
                    doc.autoTable({ body: tableDataEstimate, tableStylesNew });
                  } else {
                    const incomeItemRows = submitData?.incomeItem.map(
                      (item) => [item.chargedto, item.type, `$${item.amount}`]
                    );

                    const totalCharges =
                      parseFloat(submitData?.commission) +
                      submitData?.incomeItem.reduce(
                        (total, item) => total + parseFloat(item.amount),
                        0
                      );

                    const totalChargesRow = [
                      "Total Charges",
                      "",

                      `$${totalCharges.toFixed(2)}`,
                    ];
                    const tableDataCommission = [
                      tableHeaders,
                      commissionRow,
                      ...incomeItemRows,
                      totalChargesRow,
                    ];
                    doc.autoTable({
                      body: tableDataCommission,
                      tableStylesNew,
                    });

                    const tablePayoutHeader = [
                      "Pay To",
                      "For Service",
                      "Amount",
                    ];

                    const estimateRow = [
                      `${summary?.contact3[0]?.data.firstName} ${summary?.contact3[0]?.data.lastName}` ||
                        "",
                      summary?.category || "Net Pay Due to Agent (Estimate)",
                      `$${Number(submitData?.estimate || 0).toFixed(2)}`, // Assuming estimate is a numeric value
                    ];

                    const transactionRow = [
                      "In Depth Realty",
                      "Transaction Fee",
                      `$${submitData?.transactionFee || 0}`,
                    ];

                    const transactionWireFee = [
                      "In Depth Realty",
                      "Wire Fee",
                      `$${submitData?.wireFee || 0}`,
                    ];

                    const transactionHighvalueFee = [
                      "In Depth Realty",
                      "High Value Transaction Fee",
                      `$${submitData?.highValueFee || 0}`,
                    ];

                    const transactionPropertyvalueFee = [
                      "In Depth Realty",
                      "Personal Real Estate Risk Management Fee",
                      `$${submitData?.transactionPersonal || 0}`,
                    ];

                    const pauoutItemRows = submitData?.payoutItem.map(
                      (item) => [item.payto, item.type, `$${item.amount}`]
                    );

                    const totalChargesPauout =
                      parseFloat(submitData?.estimate) +
                      parseFloat(submitData?.transactionFee) +
                      parseFloat(submitData?.wireFee) +
                      parseFloat(submitData?.highValueFee) +
                      parseFloat(submitData?.transactionPersonal) +
                      submitData?.payoutItem.reduce(
                        (total, item) => total + parseFloat(item.amount),
                        0
                      );

                    const totalChargesRowPayout = [
                      "Total Charges",
                      "",

                      `$${totalChargesPauout.toFixed(2)}`,
                    ];

                    const tableDataEstimate = [
                      tablePayoutHeader,
                      estimateRow,
                      transactionRow,
                      transactionWireFee,
                      transactionPropertyvalueFee,
                      transactionHighvalueFee,
                      ...pauoutItemRows,
                      totalChargesRowPayout,
                    ];
                    doc.autoTable({ body: tableDataEstimate, tableStylesNew });
                  }

                  const commentText = submitData?.comments || "";
                  const splitTextIntoChunks = (text, chunkSize) => {
                    const words = text.split(" ");
                    const chunks = [];
                    for (let i = 0; i < words.length; i += chunkSize) {
                      chunks.push(words.slice(i, i + chunkSize).join(" "));
                    }
                    return chunks;
                  };
                  const commentChunks = splitTextIntoChunks(commentText, 15);

                  const commentsX = 10;
                  let commentsY = 10 + 180;
                  doc.setFontSize(10);
                  doc.text("Notes:", commentsX, commentsY);
                  commentsY += 10;

                  commentChunks.forEach((chunk) => {
                    doc.text(chunk, commentsX, commentsY);
                    commentsY += 15; // Move to the next line
                  });

                  const pdfBlob = doc.output("blob");
                  const pdfFormData = new FormData();
                  pdfFormData.append("file", pdfBlob, "generated.pdf");

                  const apiUrl = "https://api.brokeragentbase.com/upload";
                  const config = {
                    headers: {
                      "Content-Type": "multipart/form-data",
                    },
                  };

                  const response = await axios.post(
                    apiUrl,
                    pdfFormData,
                    config
                  );
                  setPdfPath(response.data);

                  const userData = {
                    invoice_url: response.data,
                  };

                  if (submitData?._id) {
                    try {
                      // Update Funding Request
                      const updateResponse =
                        await user_service.fundingRequestUpdate(
                          submitData?._id,
                          userData
                        );

                      if (updateResponse) {
                        // Update successful, now send email
                        const uploadResponse =
                          await user_service.fundingEmailPost(
                            updateResponse.data
                          );

                        setLoader({ isActive: false });

                        // setToaster({
                        //   type: "success",
                        //   isShow: true,
                        //   message: "Funding Updated Successfully",
                        // });

                        setTimeout(() => {
                          setToaster((prevToaster) => ({
                            ...prevToaster,
                            isShow: false,
                          }));
                          // navigate(`/transaction-review/${params.id}`);
                        }, 2000);
                      } else {
                        // Update failed
                        setLoader({ isActive: false });

                        setToaster({
                          type: "error",
                          isShow: true,
                          toasterBody: "Error updating Funding",
                          message: "Error",
                        });
                      }
                    } catch (error) {
                      console.error("Error updating Funding:", error);

                      setLoader({ isActive: false });

                      setToaster({
                        type: "error",
                        isShow: true,
                        toasterBody: "An error occurred while updating Funding",
                        message: "Error",
                      });
                    }
                  }
                },
              }
            );
          } else {
            doc.html(
              styles +
                '<h2 style="font-size: 4px; margin-top: 37px; margin-left: 15px;">Funding Request</h2>',
              {
                callback: async () => {
                  if (summary) {
                    const representName =
                      summary?.represent === "both" ? "Buyer & Seller" : "";

                    let contactTypeData = "";
                    if (contactData.type === "seller") {
                      contactTypeData = "Seller";
                    } else if (contactData.type === "buyer") {
                      contactTypeData = "Buyer";
                    } else if (contactData.type === "buyersale") {
                      contactTypeData = "Buyer & Seller";
                    } else if (contactData.type === "referral_sellerAgent") {
                      contactTypeData = "Referral";
                    }

                    const tableDatas = [
                      [
                        "Agent",
                        "Type",
                        representName,
                        "Sale Price",
                        contactTypeData,
                      ],
                      [
                        `${summary?.contact3[0]?.data.firstName} ${summary?.contact3[0]?.data.lastName}`,
                        `${
                          summary?.category ? summary?.category : ""
                        } ${representName}`,
                        `${summary?.contact1[0]?.data.firstName} ${summary?.contact1[0]?.data.lastName}`,
                        getCommission.purchase_price,
                        `${summary?.contact2[0]?.data.firstName} ${summary?.contact2[0]?.data.lastName}`,
                        // summary?.propertyDetail.price,
                      ],
                    ];

                    const tableStylesNew = {
                      theme: "grid",
                      tableWidth: "auto",
                      styles: { fontSize: 10 },
                      margin: { top: 50 },
                    };

                    doc.autoTable({
                      head: [],
                      body: tableDatas,
                      ...tableStylesNew,
                    });
                  }

                  const tableStylesNew = {
                    theme: "grid",
                    tableWidth: "auto",
                    styles: { fontSize: 10 },
                    margin: { top: 50 },
                  };

                  const headingText = "Settlement Charges";

                  const fontSize = 11;
                  const leftMargin = 14;
                  const topMargin = 71;

                  doc.setFontSize(fontSize);

                  doc.text(headingText, leftMargin, topMargin);

                  const tableHeaders = ["Charged To", "For Service", "Amount"];

                  let commissionType = "";
                  if (summary?.represent === "buyer") {
                    commissionType = "Buyer Commission";
                  } else if (summary?.represent === "seller") {
                    commissionType = "Seller Commission";
                  } else if (summary?.represent === "both") {
                    commissionType = "Buyer & Seller Commission";
                  } else if (summary?.represent === "referral") {
                    commissionType = "Referral Commission";
                  }

                  const commissionRow = [
                    `Seller - ${summary?.contact1[0]?.data.firstName} ${summary?.contact1[0]?.data.lastName}` ||
                      "",
                    commissionType,
                    `$${submitData?.commission || 0}`,
                  ];

                  if (summary.phase === "canceled") {
                    const totalCharges = parseFloat(submitData?.commission);

                    const totalChargesRow = [
                      "Total Charges",
                      "",

                      `$${totalCharges.toFixed(2)}`,
                    ];
                    const tableDataCommission = [
                      tableHeaders,
                      commissionRow,
                      totalChargesRow,
                    ];
                    doc.autoTable({
                      body: tableDataCommission,
                      tableStylesNew,
                    });

                    const tablePayoutHeader = [
                      "Pay To",
                      "For Service",
                      "Amount",
                    ];
                    const estimateRow = [
                      `${summary?.contact3[0]?.data.firstName} ${summary?.contact3[0]?.data.lastName}` ||
                        "",
                      summary?.category || "Net Pay Due to Agent (Estimate)",
                      `$${Number(submitData?.estimate || 0).toFixed(2)}`, // Assuming estimate is a numeric value
                    ];
                    const transactionRow = [
                      "In Depth Realty",
                      "Transaction Fee",
                      `$${submitData?.transactionFee || 0}`,
                    ];

                    const totalChargesPauout =
                      parseFloat(submitData?.estimate) +
                      parseFloat(submitData?.transactionFee);

                    const totalChargesRowPayout = [
                      "Total Charges",
                      "",

                      `$${totalChargesPauout.toFixed(2)}`,
                    ];

                    const tableDataEstimate = [
                      tablePayoutHeader,
                      estimateRow,
                      transactionRow,
                      totalChargesRowPayout,
                    ];
                    doc.autoTable({ body: tableDataEstimate, tableStylesNew });
                  } else {
                    const incomeItemRows = submitData?.incomeItem.map(
                      (item) => [item.chargedto, item.type, `$${item.amount}`]
                    );

                    const totalCharges =
                      parseFloat(submitData?.commission) +
                      submitData?.incomeItem.reduce(
                        (total, item) => total + parseFloat(item.amount),
                        0
                      );

                    // Define the "Total Charges" row
                    const totalChargesRow = [
                      "Total Charges",
                      "",

                      `$${totalCharges.toFixed(2)}`,
                    ];
                    const tableDataCommission = [
                      tableHeaders,
                      commissionRow,
                      ...incomeItemRows,
                      totalChargesRow,
                    ];
                    doc.autoTable({
                      body: tableDataCommission,
                      tableStylesNew,
                    });

                    const tablePayoutHeader = [
                      "Pay To",
                      "For Service",
                      "Amount",
                    ];

                    const estimateRow = [
                      `${summary?.contact3[0]?.data.firstName} ${summary?.contact3[0]?.data.lastName}` ||
                        "",
                      summary?.category || "Net Pay Due to Agent (Estimate)",
                      `$${Number(submitData?.estimate || 0).toFixed(2)}`,
                    ];

                    const transactionRowBuyer = [
                      "In Depth Realty",
                      "Buyer Transaction fee",
                      `$${submitData?.buyerFee || 0}`,
                    ];

                    const transactionRowSeller = [
                      "In Depth Realty",
                      "Seller Transaction fee",
                      `$${submitData?.sellerFee || 0}`,
                    ];

                    const transactionRowRisk = [
                      "In Depth Realty",
                      "Dual Agency Risk Management Fee",
                      `$${submitData?.managementFee || 0}`,
                    ];

                    const transactionWireFee = [
                      "In Depth Realty",
                      "Wire fee:",
                      `$${submitData?.wireFee || 0}`,
                    ];

                    const pauoutItemRows = submitData?.payoutItem.map(
                      (item) => [item.payto, item.type, `$${item.amount}`]
                    );

                    const totalChargesPauout =
                      parseFloat(submitData?.estimate) +
                      parseFloat(submitData?.buyerFee) +
                      parseFloat(submitData?.sellerFee) +
                      parseFloat(submitData?.managementFee) +
                      parseFloat(submitData?.wireFee) +
                      submitData?.payoutItem.reduce(
                        (total, item) => total + parseFloat(item.amount),
                        0
                      );

                    const totalChargesRowPayout = [
                      "Total Charges",
                      "",

                      `$${totalChargesPauout.toFixed(2)}`,
                    ];

                    const tableDataEstimate = [
                      tablePayoutHeader,
                      estimateRow,
                      transactionRowBuyer,
                      transactionRowSeller,
                      transactionRowRisk,
                      transactionWireFee,
                      ...pauoutItemRows,
                      totalChargesRowPayout,
                    ];
                    doc.autoTable({ body: tableDataEstimate, tableStylesNew });
                  }

                  const commentText = submitData?.comments || "";
                  const splitTextIntoChunks = (text, chunkSize) => {
                    const words = text.split(" ");
                    const chunks = [];
                    for (let i = 0; i < words.length; i += chunkSize) {
                      chunks.push(words.slice(i, i + chunkSize).join(" "));
                    }
                    return chunks;
                  };
                  const commentChunks = splitTextIntoChunks(commentText, 15);

                  const commentsX = 10;
                  let commentsY = 10 + 180;
                  doc.setFontSize(10);
                  doc.text("Notes:", commentsX, commentsY);
                  commentsY += 10;

                  commentChunks.forEach((chunk) => {
                    doc.text(chunk, commentsX, commentsY);
                    commentsY += 15; // Move to the next line
                  });

                  const pdfBlob = doc.output("blob");
                  const pdfFormData = new FormData();
                  pdfFormData.append("file", pdfBlob, "generated.pdf");

                  const apiUrl = "https://api.brokeragentbase.com/upload";
                  const config = {
                    headers: {
                      "Content-Type": "multipart/form-data",
                    },
                  };

                  const response = await axios.post(
                    apiUrl,
                    pdfFormData,
                    config
                  );
                  setPdfPath(response.data);

                  const userData = {
                    invoice_url: response.data,
                  };

                  if (submitData?._id) {
                    try {
                      // Update Funding Request
                      const updateResponse =
                        await user_service.fundingRequestUpdate(
                          submitData?._id,
                          userData
                        );

                      if (updateResponse) {
                        // Update successful, now send email
                        const uploadResponse =
                          await user_service.fundingEmailPost(
                            updateResponse.data
                          );

                        setLoader({ isActive: false });

                        // setToaster({
                        //   type: "success",
                        //   isShow: true,
                        //   message: "Funding Updated Successfully",
                        // });

                        setTimeout(() => {
                          setToaster((prevToaster) => ({
                            ...prevToaster,
                            isShow: false,
                          }));
                          // navigate(`/transaction-review/${params.id}`);
                        }, 2000);
                      } else {
                        // Update failed
                        setLoader({ isActive: false });

                        setToaster({
                          type: "error",
                          isShow: true,
                          toasterBody: "Error updating Funding",
                          message: "Error",
                        });
                      }
                    } catch (error) {
                      console.error("Error updating Funding:", error);

                      setLoader({ isActive: false });

                      setToaster({
                        type: "error",
                        isShow: true,
                        toasterBody: "An error occurred while updating Funding",
                        message: "Error",
                      });
                    }
                  }
                },
              }
            );
          }
        } else {
          if (getCommission.property_interest === "yes") {
            doc.html(
              styles +
                '<h2 style="font-size: 4px; margin-top: 37px; margin-left: 15px;">Funding Request</h2>',
              {
                callback: async () => {
                  if (summary) {
                    const representName =
                      summary?.represent === "seller"
                        ? "Seller"
                        : summary?.represent === "buyer"
                        ? "Buyer"
                        : summary?.represent === "both"
                        ? "Buyer & Seller"
                        : summary?.represent === "referral"
                        ? "Referral"
                        : "";

                    let contactTypeData = "";
                    if (contactData.type === "seller") {
                      contactTypeData = "Seller";
                    } else if (contactData.type === "buyer") {
                      contactTypeData = "Buyer";
                    } else if (contactData.type === "buyersale") {
                      contactTypeData = "Buyer & Seller";
                    } else if (contactData.type === "referral_sellerAgent") {
                      contactTypeData = "Buyer";
                    }

                    let contactnameData = "";
                    if (contactData.type === "referral_sellerAgent") {
                      contactnameData = "";
                    } else {
                      contactnameData = `${summary?.contact2[0]?.data.firstName} ${summary?.contact2[0]?.data.lastName}`;
                    }

                    const tableDatas = [
                      [
                        "Agent",
                        "Type",
                        representName,
                        "Sale Price",
                        contactTypeData,
                      ],

                      [
                        `${summary?.contact3[0]?.data.firstName} ${summary?.contact3[0]?.data.lastName}`,
                        `${
                          summary?.category ? summary?.category : ""
                        } ${representName}`,
                        `${summary?.contact1[0]?.data.firstName} ${summary?.contact1[0]?.data.lastName}`,
                        getCommission.purchase_price,
                        contactnameData,
                        // summary?.propertyDetail.price,
                      ],
                    ];

                    const tableStylesNew = {
                      theme: "grid",
                      tableWidth: "auto",
                      styles: { fontSize: 10 },
                      margin: { top: 50 },
                    };

                    doc.autoTable({
                      head: [],
                      body: tableDatas,
                      ...tableStylesNew,
                    });
                  }

                  const tableStylesNew = {
                    theme: "grid",
                    tableWidth: "auto",
                    styles: { fontSize: 10 },
                    margin: { top: 50 },
                  };

                  const headingText = "Settlement Charges";

                  const fontSize = 11;
                  const leftMargin = 14;
                  const topMargin = 71;

                  doc.setFontSize(fontSize);

                  doc.text(headingText, leftMargin, topMargin);

                  const tableHeaders = ["Charged To", "For Service", "Amount"];

                  let contactName = "";
                  let commissionType = "";

                  if (summary?.represent === "buyer") {
                    contactName =
                      `Seller - ${summary?.contact2[0]?.data.firstName} ${summary?.contact2[0]?.data.lastName}` ||
                      "";
                    commissionType = "Buyer Commission";
                  } else if (summary?.represent === "seller") {
                    contactName =
                      `Seller - ${summary?.contact1[0]?.data.firstName} ${summary?.contact1[0]?.data.lastName}` ||
                      "";
                    commissionType = "Seller Commission";
                  } else if (summary?.represent === "both") {
                    contactName =
                      `Seller - ${summary?.contact1[0]?.data.firstName} ${summary?.contact1[0]?.data.lastName}` ||
                      "";
                    commissionType = "Buyer & Seller Commission";
                  } else if (summary?.represent === "referral") {
                    contactName =
                      `Seller - ${summary?.contact1[0]?.data.firstName} ${summary?.contact1[0]?.data.lastName}` ||
                      "";
                    commissionType = "Referral Commission";
                  }

                  const commissionRow = [
                    contactName,
                    commissionType,
                    `$${submitData?.commission || 0}`,
                  ];

                  if (summary.phase === "canceled") {
                    const totalCharges = parseFloat(submitData?.commission);

                    const totalChargesRow = [
                      "Total Charges",
                      "",

                      `$${totalCharges.toFixed(2)}`,
                    ];
                    const tableDataCommission = [
                      tableHeaders,
                      commissionRow,
                      totalChargesRow,
                    ];
                    doc.autoTable({
                      body: tableDataCommission,
                      tableStylesNew,
                    });

                    const tablePayoutHeader = [
                      "Pay To",
                      "For Service",
                      "Amount",
                    ];
                    const estimateRow = [
                      `${summary?.contact3[0]?.data.firstName} ${summary?.contact3[0]?.data.lastName}` ||
                        "",
                      summary?.category || "Net Pay Due to Agent (Estimate)",
                      `$${Number(submitData?.estimate || 0).toFixed(2)}`, // Assuming estimate is a numeric value
                    ];
                    const transactionRow = [
                      "In Depth Realty",
                      "Transaction Fee",
                      `$${submitData?.transactionFee || 0}`,
                    ];

                    const totalChargesPauout =
                      parseFloat(submitData?.estimate) +
                      parseFloat(submitData?.transactionFee);

                    const totalChargesRowPayout = [
                      "Total Charges",
                      "",

                      `$${totalChargesPauout.toFixed(2)}`,
                    ];

                    const tableDataEstimate = [
                      tablePayoutHeader,
                      estimateRow,
                      transactionRow,
                      totalChargesRowPayout,
                    ];
                    doc.autoTable({ body: tableDataEstimate, tableStylesNew });
                  } else {
                    const incomeItemRows = submitData?.incomeItem.map(
                      (item) => [item.chargedto, item.type, `$${item.amount}`]
                    );

                    const totalCharges =
                      parseFloat(submitData?.commission) +
                      submitData?.incomeItem.reduce(
                        (total, item) => total + parseFloat(item.amount),
                        0
                      );

                    // Define the "Total Charges" row
                    const totalChargesRow = [
                      "Total Charges",
                      "",

                      `$${totalCharges.toFixed(2)}`,
                    ];
                    const tableDataCommission = [
                      tableHeaders,
                      commissionRow,
                      ...incomeItemRows,
                      totalChargesRow,
                    ];
                    doc.autoTable({
                      body: tableDataCommission,
                      tableStylesNew,
                    });

                    const tablePayoutHeader = [
                      "Pay To",
                      "For Service",
                      "Amount",
                    ];

                    const estimateRow = [
                      `${summary?.contact3[0]?.data.firstName} ${summary?.contact3[0]?.data.lastName}` ||
                        "",
                      summary?.category || "Net Pay Due to Agent (Estimate)",
                      `$${Number(submitData?.estimate || 0).toFixed(2)}`, // Assuming estimate is a numeric value
                    ];

                    const transactionRow = [
                      "In Depth Realty",
                      "Transaction Fee",
                      `$${submitData?.transactionFee || 0}`,
                    ];

                    const transactionWireFee = [
                      "In Depth Realty",
                      "Wire Fee",
                      `$${submitData?.wireFee || 0}`,
                    ];

                    const transactionHighvalueFee = [
                      "In Depth Realty",
                      "High Value Transaction Fee",
                      `$${submitData?.highValueFee || 0}`,
                    ];

                    const transactionPropertyvalueFee = [
                      "In Depth Realty",
                      "Personal Real Estate Risk Management Fee",
                      `$${submitData?.transactionPersonal || 0}`,
                    ];

                    const pauoutItemRows = submitData?.payoutItem.map(
                      (item) => [item.payto, item.type, `$${item.amount}`]
                    );

                    const totalChargesPauout =
                      parseFloat(submitData?.estimate) +
                      parseFloat(submitData?.transactionFee) +
                      parseFloat(submitData?.wireFee) +
                      parseFloat(submitData?.highValueFee) +
                      parseFloat(submitData?.transactionPersonal) +
                      submitData?.payoutItem.reduce(
                        (total, item) => total + parseFloat(item.amount),
                        0
                      );

                    const totalChargesRowPayout = [
                      "Total Charges",
                      "",

                      `$${totalChargesPauout.toFixed(2)}`,
                    ];

                    const tableDataEstimate = [
                      tablePayoutHeader,
                      estimateRow,
                      transactionRow,
                      transactionWireFee,
                      transactionPropertyvalueFee,
                      transactionHighvalueFee,
                      ...pauoutItemRows,
                      totalChargesRowPayout,
                    ];
                    doc.autoTable({ body: tableDataEstimate, tableStylesNew });
                  }

                  const commentText = submitData?.comments || "";
                  const splitTextIntoChunks = (text, chunkSize) => {
                    const words = text.split(" ");
                    const chunks = [];
                    for (let i = 0; i < words.length; i += chunkSize) {
                      chunks.push(words.slice(i, i + chunkSize).join(" "));
                    }
                    return chunks;
                  };
                  const commentChunks = splitTextIntoChunks(commentText, 15);

                  const commentsX = 10;
                  let commentsY = 10 + 180;
                  doc.setFontSize(10);
                  doc.text("Notes:", commentsX, commentsY);
                  commentsY += 10;

                  commentChunks.forEach((chunk) => {
                    doc.text(chunk, commentsX, commentsY);
                    commentsY += 15; // Move to the next line
                  });

                  const pdfBlob = doc.output("blob");
                  const pdfFormData = new FormData();
                  pdfFormData.append("file", pdfBlob, "generated.pdf");

                  const apiUrl = "https://api.brokeragentbase.com/upload";
                  const config = {
                    headers: {
                      "Content-Type": "multipart/form-data",
                    },
                  };

                  const response = await axios.post(
                    apiUrl,
                    pdfFormData,
                    config
                  );
                  setPdfPath(response.data);

                  const userData = {
                    invoice_url: response.data,
                  };

                  if (submitData?._id) {
                    try {
                      // Update Funding Request
                      const updateResponse =
                        await user_service.fundingRequestUpdate(
                          submitData?._id,
                          userData
                        );

                      if (updateResponse) {
                        // Update successful, now send email
                        const uploadResponse =
                          await user_service.fundingEmailPost(
                            updateResponse.data
                          );

                        setLoader({ isActive: false });

                        // setToaster({
                        //   type: "success",
                        //   isShow: true,
                        //   message: "Funding Updated Successfully",
                        // });

                        setTimeout(() => {
                          setToaster((prevToaster) => ({
                            ...prevToaster,
                            isShow: false,
                          }));
                          // navigate(`/transaction-review/${params.id}`);
                        }, 2000);
                      } else {
                        // Update failed
                        setLoader({ isActive: false });

                        setToaster({
                          type: "error",
                          isShow: true,
                          toasterBody: "Error updating Funding",
                          message: "Error",
                        });
                      }
                    } catch (error) {
                      console.error("Error updating Funding:", error);

                      setLoader({ isActive: false });

                      setToaster({
                        type: "error",
                        isShow: true,
                        toasterBody: "An error occurred while updating Funding",
                        message: "Error",
                      });
                    }
                  }
                },
              }
            );
          } else {
            doc.html(
              styles +
                '<h2 style="font-size: 4px; margin-top: 37px; margin-left: 15px;">Funding Request</h2>',
              {
                callback: async () => {
                  if (summary) {
                    const representName =
                      summary?.represent === "seller"
                        ? "Seller"
                        : summary?.represent === "buyer"
                        ? "Buyer"
                        : summary?.represent === "both"
                        ? "Buyer & Seller"
                        : summary?.represent === "referral"
                        ? "Referral"
                        : "";

                    let contactTypeData = "";
                    if (contactData.type === "seller") {
                      contactTypeData = "Seller";
                    } else if (contactData.type === "buyer") {
                      contactTypeData = "Buyer";
                    } else if (contactData.type === "buyersale") {
                      contactTypeData = "Buyer & Seller";
                    } else if (contactData.type === "referral_sellerAgent") {
                      contactTypeData = "Buyer";
                    }

                    let contactnameData = "";
                    if (contactData.type === "referral_sellerAgent") {
                      contactnameData = "";
                    } else {
                      contactnameData = `${summary?.contact2[0]?.data.firstName} ${summary?.contact2[0]?.data.lastName}`;
                    }

                    const tableDatas = [
                      [
                        "Agent",
                        "Type",
                        representName,
                        "Sale Price",
                        contactTypeData,
                      ],

                      [
                        `${summary?.contact3[0]?.data.firstName} ${summary?.contact3[0]?.data.lastName}`,
                        `${
                          summary?.category ? summary?.category : ""
                        } ${representName}`,
                        `${summary?.contact1[0]?.data.firstName} ${summary?.contact1[0]?.data.lastName}`,
                        getCommission.purchase_price,
                        contactnameData,
                        // summary?.propertyDetail.price,
                      ],
                    ];

                    const tableStylesNew = {
                      theme: "grid",
                      tableWidth: "auto",
                      styles: { fontSize: 10 },
                      margin: { top: 50 },
                    };

                    doc.autoTable({
                      head: [],
                      body: tableDatas,
                      ...tableStylesNew,
                    });
                  }

                  const tableStylesNew = {
                    theme: "grid",
                    tableWidth: "auto",
                    styles: { fontSize: 10 },
                    margin: { top: 50 },
                  };

                  const headingText = "Settlement Charges";

                  const fontSize = 11;
                  const leftMargin = 14;
                  const topMargin = 71;

                  doc.setFontSize(fontSize);

                  doc.text(headingText, leftMargin, topMargin);

                  const tableHeaders = ["Charged To", "For Service", "Amount"];

                  let contactName = "";
                  let commissionType = "";

                  if (summary?.represent === "buyer") {
                    contactName =
                      `Seller - ${summary?.contact2[0]?.data.firstName} ${summary?.contact2[0]?.data.lastName}` ||
                      "";
                    commissionType = "Buyer Commission";
                  } else if (summary?.represent === "seller") {
                    contactName =
                      `Seller - ${summary?.contact1[0]?.data.firstName} ${summary?.contact1[0]?.data.lastName}` ||
                      "";
                    commissionType = "Seller Commission";
                  } else if (summary?.represent === "both") {
                    contactName =
                      `Seller - ${summary?.contact1[0]?.data.firstName} ${summary?.contact1[0]?.data.lastName}` ||
                      "";
                    commissionType = "Buyer & Seller Commission";
                  } else if (summary?.represent === "referral") {
                    contactName =
                      `Seller - ${summary?.contact1[0]?.data.firstName} ${summary?.contact1[0]?.data.lastName}` ||
                      "";
                    commissionType = "Referral Commission";
                  }

                  const commissionRow = [
                    contactName,
                    commissionType,
                    `$${submitData?.commission || 0}`,
                  ];

                  if (summary.phase === "canceled") {
                    const totalCharges = parseFloat(submitData?.commission);

                    const totalChargesRow = [
                      "Total Charges",
                      "",

                      `$${totalCharges.toFixed(2)}`,
                    ];
                    const tableDataCommission = [
                      tableHeaders,
                      commissionRow,
                      totalChargesRow,
                    ];
                    doc.autoTable({
                      body: tableDataCommission,
                      tableStylesNew,
                    });

                    const tablePayoutHeader = [
                      "Pay To",
                      "For Service",
                      "Amount",
                    ];
                    const estimateRow = [
                      `${summary?.contact3[0]?.data.firstName} ${summary?.contact3[0]?.data.lastName}` ||
                        "",
                      summary?.category || "Net Pay Due to Agent (Estimate)",
                      `$${Number(submitData?.estimate || 0).toFixed(2)}`, // Assuming estimate is a numeric value
                    ];
                    const transactionRow = [
                      "In Depth Realty",
                      "Transaction Fee",
                      `$${submitData?.transactionFee || 0}`,
                    ];

                    const totalChargesPauout =
                      parseFloat(submitData?.estimate) +
                      parseFloat(submitData?.transactionFee);

                    const totalChargesRowPayout = [
                      "Total Charges",
                      "",

                      `$${totalChargesPauout.toFixed(2)}`,
                    ];

                    const tableDataEstimate = [
                      tablePayoutHeader,
                      estimateRow,
                      transactionRow,
                      totalChargesRowPayout,
                    ];
                    doc.autoTable({ body: tableDataEstimate, tableStylesNew });
                  } else {
                    const incomeItemRows = submitData?.incomeItem.map(
                      (item) => [item.chargedto, item.type, `$${item.amount}`]
                    );

                    const totalCharges =
                      parseFloat(submitData?.commission) +
                      submitData?.incomeItem.reduce(
                        (total, item) => total + parseFloat(item.amount),
                        0
                      );

                    // Define the "Total Charges" row
                    const totalChargesRow = [
                      "Total Charges",
                      "",

                      `$${totalCharges.toFixed(2)}`,
                    ];
                    const tableDataCommission = [
                      tableHeaders,
                      commissionRow,
                      ...incomeItemRows,
                      totalChargesRow,
                    ];
                    doc.autoTable({
                      body: tableDataCommission,
                      tableStylesNew,
                    });

                    const tablePayoutHeader = [
                      "Pay To",
                      "For Service",
                      "Amount",
                    ];

                    const estimateRow = [
                      `${summary?.contact3[0]?.data.firstName} ${summary?.contact3[0]?.data.lastName}` ||
                        "",
                      summary?.category || "Net Pay Due to Agent (Estimate)",
                      `$${Number(submitData?.estimate || 0).toFixed(2)}`, // Assuming estimate is a numeric value
                    ];

                    const transactionRow = [
                      "In Depth Realty",
                      "Transaction Fee",
                      `$${submitData?.transactionFee || 0}`,
                    ];

                    const transactionWireFee = [
                      "In Depth Realty",
                      "Wire Fee",
                      `$${submitData?.wireFee || 0}`,
                    ];

                    const transactionHighvalueFee = [
                      "In Depth Realty",
                      "High Value Transaction Fee",
                      `$${submitData?.highValueFee || 0}`,
                    ];

                    const pauoutItemRows = submitData?.payoutItem.map(
                      (item) => [item.payto, item.type, `$${item.amount}`]
                    );

                    const totalChargesPauout =
                      parseFloat(submitData?.estimate) +
                      parseFloat(submitData?.transactionFee) +
                      parseFloat(submitData?.wireFee) +
                      parseFloat(submitData?.highValueFee) +
                      submitData?.payoutItem.reduce(
                        (total, item) => total + parseFloat(item.amount),
                        0
                      );

                    const totalChargesRowPayout = [
                      "Total Charges",
                      "",

                      `$${totalChargesPauout.toFixed(2)}`,
                    ];

                    const tableDataEstimate = [
                      tablePayoutHeader,
                      estimateRow,
                      transactionRow,
                      transactionWireFee,
                      transactionHighvalueFee,
                      ...pauoutItemRows,
                      totalChargesRowPayout,
                    ];
                    doc.autoTable({ body: tableDataEstimate, tableStylesNew });
                  }

                  const commentText = submitData?.comments || "";
                  const splitTextIntoChunks = (text, chunkSize) => {
                    const words = text.split(" ");
                    const chunks = [];
                    for (let i = 0; i < words.length; i += chunkSize) {
                      chunks.push(words.slice(i, i + chunkSize).join(" "));
                    }
                    return chunks;
                  };
                  const commentChunks = splitTextIntoChunks(commentText, 15);

                  const commentsX = 10;
                  let commentsY = 10 + 180;
                  doc.setFontSize(10);
                  doc.text("Notes:", commentsX, commentsY);
                  commentsY += 10;

                  commentChunks.forEach((chunk) => {
                    doc.text(chunk, commentsX, commentsY);
                    commentsY += 15; // Move to the next line
                  });

                  const pdfBlob = doc.output("blob");
                  const pdfFormData = new FormData();
                  pdfFormData.append("file", pdfBlob, "generated.pdf");

                  const apiUrl = "https://api.brokeragentbase.com/upload";
                  const config = {
                    headers: {
                      "Content-Type": "multipart/form-data",
                    },
                  };

                  const response = await axios.post(
                    apiUrl,
                    pdfFormData,
                    config
                  );
                  setPdfPath(response.data);

                  const userData = {
                    invoice_url: response.data,
                  };

                  if (submitData?._id) {
                    try {
                      // Update Funding Request
                      const updateResponse =
                        await user_service.fundingRequestUpdate(
                          submitData?._id,
                          userData
                        );

                      if (updateResponse) {
                        // Update successful, now send email
                        const uploadResponse =
                          await user_service.fundingEmailPost(
                            updateResponse.data
                          );

                        setLoader({ isActive: false });

                        // setToaster({
                        //   type: "success",
                        //   isShow: true,
                        //   message: "Funding Updated Successfully",
                        // });

                        setTimeout(() => {
                          setToaster((prevToaster) => ({
                            ...prevToaster,
                            isShow: false,
                          }));
                          // navigate(`/transaction-review/${params.id}`);
                        }, 2000);
                      } else {
                        // Update failed
                        setLoader({ isActive: false });

                        setToaster({
                          type: "error",
                          isShow: true,
                          toasterBody: "Error updating Funding",
                          message: "Error",
                        });
                      }
                    } catch (error) {
                      console.error("Error updating Funding:", error);

                      setLoader({ isActive: false });

                      setToaster({
                        type: "error",
                        isShow: true,
                        toasterBody: "An error occurred while updating Funding",
                        message: "Error",
                      });
                    }
                  }
                },
              }
            );
          }
        }
      }
    } catch (error) {
      console.error("Error generating PDF:", error);
    }
  };

  const validatereq = () => {
    const values = formValues;
    const errors = {};
    if (!values.funding_method) {
      errors.funding_method = "Funding method is required";
    }

    setFormErrors(errors);
    return errors;
  };
  const handleRequest = () => {
    let checkValue = validatereq();
    if (_.isEmpty(checkValue)) {
      setRequest(true);
    }
  };

  const timestamp = summary.propertyDetail?.timeStamp;
  const date = new Date(timestamp);
  const formattedDateTime = date.toLocaleString();

  const propertypic = (e) => {
    e.target.src = defaultpropertyimage;
  };

  const handleUpdatetransaction = async () => {
    try {
      const userDataa = {
        trans_status: "submited",
        approval: "inprogress",
      };
      await user_service
        .TransactionUpdate(params.id, userDataa)
        .then((response) => {
          if (response.status === 200) {
            TransactionGetById(params.id);
            setIsSubmitted(true);
            const authData = jwt(localStorage.getItem("auth"));
            if (authData.contactType === "admin") {
              setTimeout(() => {
                setToaster((prevToaster) => ({
                  ...prevToaster,
                  isShow: false,
                }));
                // navigate(`/transaction-review/${params.id}`);
              }, 3000);
            } else {
              setTimeout(() => {
                setToaster((prevToaster) => ({
                  ...prevToaster,
                  isShow: false,
                }));
                // navigate(`/transaction-summary/${params.id}`);
              }, 3000);
            }
          } else {
            setLoader({ isActive: false });
            setToaster({
              types: "error",
              isShow: true,
              toasterBody: response.data.message,
              message: "Error",
            });
          }
        });
    } catch (error) {
      setLoader({ isActive: false });
      setToaster({
        types: "error",
        isShow: true,
        toasterBody: error.response
          ? error.response.data.message
          : error.message,
        message: "Error",
      });
    }
  };

  const handleRequestreview = () => {
    window.scrollTo(0, 0);
    setRequest(false);
    setRequestreview(true);
    TransactionGetById(params.id);
    FundingRequestGet();
  };

  return (
    <div className="bg-secondary float-left w-100 pt-4 mb-4">
      <Loader isActive={isActive} />
      {isShow && (
        <Toaster
          types={types}
          isShow={isShow}
          toasterBody={toasterBody}
          message={message}
        />
      )}
      <main className="page-wrapper">
        {/* <AllTab transaction_data={summary} /> */}
        {/* <!-- Page content--> */}
        {/* <!-- Page container--> */}
        <div className="content-overlay transaction_tab_view">
          <div className="row">
            <AllTab />
            <div className="col-md-12">
              <div className="add_listing border rounded-3 p-3 bg-light">
                {request ? (
                  <div className="row">
                    <div className="col-md-12">
                      <div className="d-flex align-items-center justify-content-between border-bottom pb-2 mb-4">
                        <h3 className="mb-0">Funding Request</h3>
                      </div>
                    </div>
                    <div className="col-md-9">
                      <div className="card-img-top d-flex align-items-center justify-content-start">
                        <img
                          className="getContact_picture"
                          src={
                            summary?.propertyDetail?.image &&
                            summary?.propertyDetail?.image !== "image"
                              ? summary?.propertyDetail?.image
                              : defaultpropertyimage
                          }
                          alt="Property"
                          onError={(e) => propertypic(e)}
                        />

                        {/* <img className="img-fluid w-25" src="https://assets.utahrealestate.com/photos/640x480/1879237_00471ed7a0a2863747bfcc23029e8c8d_64a492f808485.jpg" alt="Property"/> */}
                        <p className="ms-3">
                          Please verify the company's contact information
                        </p>
                      </div>

                      {/* <p>TENANT'S BROKER - Verify tenant's broker contact info.</p> */}
                      <div className="row mt-4">
                        <div className="col-sm-4 mb-3">
                          <label className="col-form-label pt-0 mb-0">
                            Company Name
                          </label>
                          <input
                            className="form-control"
                            id="inline-form-input"
                            type="text"
                            name="companyName"
                            value={formValues.companyName}
                            onChange={handleChange}
                            style={{
                              border: formValues.companyName
                                ? "1px solid #00000026"
                                : formErrorsNew.companyName
                                ? "1px solid red"
                                : "1px solid #00000026",
                            }}
                          />
                          {formValuesClose ? (
                            <></>
                          ) : (
                            <>
                              {formErrorsNew.companyName && (
                                <div className="invalid-tooltip">
                                  {formErrorsNew.companyName}
                                </div>
                              )}
                            </>
                          )}
                          {/* {formErrorsNew.companyName && (
                          <div className="invalid-tooltip">{formErrorsNew.companyName}</div>
                        )} */}
                        </div>

                        <div className="col-sm-4 mb-3">
                          <label className="col-form-label pt-0 mb-0">
                            Phone
                          </label>
                          <input
                            className="form-control"
                            id="inline-form-input"
                            type="text"
                            name="phone"
                            value={formValues.phone}
                            onChange={handleChange}
                            style={{
                              border: formValues.phone
                                ? "1px solid #00000026"
                                : formErrorsNew.phone
                                ? "1px solid red"
                                : "1px solid #00000026",
                            }}
                          />
                          {formValuesClose ? (
                            <></>
                          ) : (
                            <>
                              {formErrorsNew.phone && (
                                <div className="invalid-tooltip">
                                  {formErrorsNew.phone}
                                </div>
                              )}
                            </>
                          )}
                          {/* {formErrorsNew.phone && (
                          <div className="invalid-tooltip">{formErrorsNew.phone}</div>
                        )} */}
                        </div>
                        <div className="col-sm-4 mb-3">
                          <label className="col-form-label pt-0 mb-0">
                            File Number (GF#)
                          </label>
                          <input
                            className="form-control"
                            id="inline-form-input"
                            type="text"
                            name="fileNumber"
                            value={formValues.fileNumber}
                            onChange={handleChange}
                            style={{
                              border: formValues.fileNumber
                                ? "1px solid #00000026"
                                : formErrorsNew.fileNumber
                                ? "1px solid red"
                                : "1px solid #00000026",
                            }}
                          />
                          {formValuesClose ? (
                            <></>
                          ) : (
                            <>
                              {formErrorsNew.fileNumber && (
                                <div className="invalid-tooltip">
                                  {formErrorsNew.fileNumber}
                                </div>
                              )}
                            </>
                          )}
                          {/* {formErrorsNew.fileNumber && (
                          <div className="invalid-tooltip">{formErrorsNew.fileNumber}</div>
                        )} */}
                        </div>
                      </div>

                      <div className="row">
                        <div className="col-sm-4 mb-3">
                          <label className="col-form-label pt-0 mb-0">
                            Contact Name
                          </label>
                          <input
                            className="form-control"
                            id="inline-form-input"
                            type="text"
                            name="contactName"
                            value={formValues.contactName}
                            onChange={handleChange}
                            style={{
                              border: formValues.contactName
                                ? "1px solid #00000026"
                                : formErrorsNew.contactName
                                ? "1px solid red"
                                : "1px solid #00000026",
                            }}
                          />
                          {formValuesClose ? (
                            <></>
                          ) : (
                            <>
                              {formErrorsNew.contactName && (
                                <div className="invalid-tooltip">
                                  {formErrorsNew.contactName}
                                </div>
                              )}
                            </>
                          )}
                          {/* {formErrorsNew.contactName && (
                          <div className="invalid-tooltip">{formErrorsNew.contactName}</div>
                        )} */}
                        </div>

                        <div className="col-sm-8">
                          <label className="col-form-label pt-0 mb-0">
                            Email
                          </label>
                          <input
                            className="form-control"
                            id="inline-form-input"
                            type="text"
                            name="email"
                            value={formValues.email}
                            onChange={handleChange}
                            style={{
                              border: formValues.email
                                ? "1px solid #00000026"
                                : formErrorsNew.email
                                ? "1px solid red"
                                : "1px solid #00000026",
                            }}
                          />
                          {formValuesClose ? (
                            <></>
                          ) : (
                            <>
                              {formErrorsNew.email && (
                                <div className="invalid-tooltip">
                                  {formErrorsNew.email}
                                </div>
                              )}
                            </>
                          )}
                          {/* {formErrorsNew.email && (
                          <div className="invalid-tooltip">{formErrorsNew.email}</div>
                        )} */}
                        </div>
                      </div>

                      <div className="row mt-4">
                        <div className="col-md-12">
                          <p>
                            SETTLEMENT CHARGES - Itemize commissions and fees
                            due for your side of this transaction.
                          </p>
                        </div>
                        <div className="col-md-4">
                          <label className="col-form-label pt-0 mb-0">
                            Income Item
                          </label>
                        </div>
                        <div className="col-md-4">
                          <label className="col-form-label pt-0 mb-0">
                            Amount
                          </label>
                        </div>
                        <div className="col-md-4">
                          <label className="col-form-label pt-0 mb-0">
                            Charged To
                          </label>
                        </div>
                      </div>

                      {summary.phase === "canceled" ? (
                        <div className="row mt-3">
                          <div className="col-md-4 d-flex align-items-center justify-content-start">
                            <p className="mb-0">Agent Commission</p>
                          </div>
                          <div className="col-md-4">
                            <input
                              className="form-control"
                              id="inline-form-input"
                              type="number"
                              name="commission"
                              value="0"
                              onChange={handleChanges}
                              readOnly
                            />
                          </div>
                          <div className="col-md-4 d-flex align-items-center justify-content-start">
                            <i
                              className="fa fa-calculator"
                              aria-hidden="true"
                            ></i>
                            <span className="ms-2">Seller</span>
                          </div>
                        </div>
                      ) : (
                        <>
                          <div className="row mt-3">
                            <div className="col-md-4 d-flex align-items-center justify-content-start">
                              <p className="mb-0">Agent Commission</p>
                            </div>
                            <div className="col-md-4">
                              <input
                                className="form-control"
                                id="inline-form-input"
                                type="number"
                                name="commission"
                                value={
                                  formValues.commission
                                    ? formValues.commission
                                    : findingGet?.commission
                                }
                                onChange={handleChanges}
                                style={{
                                  border:
                                    findingGet?.commission ||
                                    formValues.commission
                                      ? "1px solid #00000026"
                                      : formErrorsNew.commission
                                      ? "1px solid red"
                                      : "1px solid #00000026",
                                }}
                              />
                              {findingGet ? (
                                <></>
                              ) : (
                                <>
                                  {formErrorsNew.commission && (
                                    <div className="invalid-tooltip">
                                      {formErrorsNew.commission}
                                    </div>
                                  )}
                                </>
                              )}
                            </div>
                            <div className="col-md-4 d-flex align-items-center justify-content-start">
                              <i
                                className="fa fa-calculator"
                                aria-hidden="true"
                              ></i>
                              <span className="ms-2">Seller</span>
                            </div>
                          </div>
                          {rows.map((row, index) => (
                            <div key={index}>
                              <div className="row mt-4">
                                <div className="col-md-4 d-flex align-items-center justify-content-start">
                                  {row.type === "bonus" ? (
                                    <p className="mb-0">{row.type}</p>
                                  ) : (
                                    <input
                                      className="form-control"
                                      id={`type-${index}`}
                                      type="text"
                                      name="type"
                                      value={row.type}
                                      onChange={(e) =>
                                        handleChangeRow(e, index, "type")
                                      }
                                    />
                                  )}
                                </div>

                                <div className="col-md-4">
                                  <input
                                    className="form-control"
                                    id={`amount-${index}`}
                                    type="number"
                                    name="amount"
                                    value={row.amount}
                                    onChange={(e) =>
                                      handleChangeBonus(e, index, "amount")
                                    }
                                  />
                                </div>

                                <div className="col-md-4 d-flex align-items-center justify-content-start">
                                  {/* <span className="ms-0">{row.chargedto}</span> */}
                                  <input
                                    className="form-control"
                                    id={`chargedto-${index}`}
                                    type="text"
                                    name="chargedto"
                                    value={row.chargedto}
                                    onChange={(e) =>
                                      handleChangeCharge(e, index, "chargedto")
                                    }
                                  />
                                  <img
                                    className="ms-3"
                                    onClick={() => handleRemove(index)}
                                    src="https://cdn.pboffice.net/shared/transaction/page_select_drop.png"
                                  />
                                </div>
                              </div>
                            </div>
                          ))}
                        </>
                      )}

                      {summary.phase === "canceled" ? (
                        <div className="col-md-4  align-items-center justify-content-start">
                          <h6 className="mb-0">Total: ${totalAmount}</h6>
                        </div>
                      ) : (
                        <div className="row mt-4">
                          <div className="col-md-8 mt-0">
                            <select
                              className="form-select"
                              name="incomeItem"
                              value={selectedOption}
                              onChange={handleOptionChange}
                              onBlur={() => setSelectedOption("")}
                            >
                              <option value="">-- Add an income --</option>
                              <option value="bonus">Bonus</option>
                              <option value="transactionFee">
                                Transaction Fee
                              </option>
                              <option value="other">Other…</option>
                            </select>
                          </div>
                          <div className="col-md-4 d-flex align-items-center justify-content-start">
                            <h6 className="mb-0">Total: ${totalAmount}</h6>
                          </div>
                        </div>
                      )}

                      <div className="row mt-4">
                        <div className="col-md-12 mt-2">
                          <p>
                            PAYOUTS - Itemize all payouts from the total
                            settlement charges.
                          </p>
                        </div>
                        <div className="col-md-4">
                          <h6>Payout Item</h6>
                        </div>
                        <div className="col-md-4">
                          <h6>Amount</h6>
                        </div>
                        <div className="col-md-4">
                          <h6>Pay To</h6>
                        </div>
                      </div>

                      {summary.phase === "canceled" ? (
                        <div className="row">
                          <div className="col-md-4">
                            <p>Net Pay Due to Agent (Estimate)</p>
                          </div>
                          <div className="col-md-4">
                            {netAmount ? <p>${0}</p> : ""}
                          </div>
                          <div className="col-md-4 d-flex align-items-center justify-content-start">
                            <p className="mb-0">
                              {summary?.contact3?.[0]?.data?.firstName +
                                " " +
                                summary?.contact3?.[0]?.data?.lastName}
                            </p>
                          </div>
                        </div>
                      ) : (
                        <div className="row">
                          <div className="col-md-4">
                            <p>Net Pay Due to Agent (Estimate)</p>
                          </div>
                          <div className="col-md-4">
                            {netAmount ? <p>${netAmount.toFixed(2)}</p> : ""}
                          </div>
                          <div className="col-md-4 d-flex align-items-center justify-content-start">
                            <p className="mb-0">
                              {summary?.contact3?.[0]?.data?.firstName +
                                " " +
                                summary?.contact3?.[0]?.data?.lastName}
                            </p>
                          </div>
                        </div>
                      )}

                      {summary.type === "buy_commercial" ||
                      summary.type === "sale_commercial" ||
                      summary.type === "both_commercial" ||
                      summary.type === "referral_commercial" ? (
                        <>
                          {summary.phase === "canceled" ? (
                            <div className="row mt-0">
                              <div className="col-md-4 d-flex align-items-center justify-content-start">
                                <p className="mb-0">Transaction Fee</p>
                              </div>
                              <div className="col-md-4">
                                <input
                                  className="form-control"
                                  id="inline-form-input"
                                  type="text"
                                  name="transactionFee"
                                  value="0"
                                  // onChange={handleChange}
                                />
                              </div>
                              <div className="col-md-4 d-flex align-items-center justify-content-start">
                                <p className="mb-0">In Depth Realty</p>
                              </div>

                              <div className="col-md-4  align-items-center justify-content-start">
                                <h6 className="mb-0">
                                  Total: ${totalAmountNew ? "0" : "0"}
                                  {console.log(totalAmountNew)}
                                </h6>
                              </div>
                            </div>
                          ) : (
                            <>
                              <div className="row mt-0">
                                <div className="col-md-4 d-flex align-items-center justify-content-start">
                                  <p className="mb-0">
                                    Commercial Transaction Fee
                                  </p>
                                </div>
                                <div className="col-md-4">
                                  <input
                                    className="form-control"
                                    id="inline-form-input"
                                    type="text"
                                    name="transactionFee"
                                    value={transactionFesData.transactionFee}
                                    // onChange={handleChange}
                                  />
                                </div>
                                <div className="col-md-4 d-flex align-items-center justify-content-start">
                                  <p className="mb-0">In Depth Realty</p>
                                </div>
                              </div>

                              <div className="row mt-2">
                                <div className="col-md-4 d-flex align-items-center justify-content-start">
                                  <p className="mb-0"> Wire fee:</p>
                                </div>
                                <div className="col-md-4">
                                  <input
                                    className="form-control"
                                    id="inline-form-input"
                                    type="text"
                                    name="wireFee"
                                    value={transactionWire.wireFee}
                                    // onChange={handleChange}
                                  />
                                </div>
                                <div className="col-md-4 d-flex align-items-center justify-content-start">
                                  <p className="mb-0">In Depth Realty</p>
                                </div>
                              </div>

                              {rowsPayout.map((row, index) => (
                                <div key={index}>
                                  <div className="row mt-4">
                                    <div className="col-md-4">
                                      <input
                                        className="form-control"
                                        id={`type-${index}`}
                                        type="text"
                                        name="type"
                                        value={row.type}
                                        onChange={(e) =>
                                          handleChangeRowPayout(
                                            e,
                                            index,
                                            "type"
                                          )
                                        }
                                      />
                                    </div>

                                    <div className="col-md-4">
                                      <input
                                        className="form-control"
                                        id={`amount-${index}`}
                                        type="text"
                                        name="amount"
                                        value={row.amount}
                                        onChange={(e) =>
                                          handleChangePayout(e, index, "amount")
                                        }
                                      />
                                    </div>

                                    {row.type === "brokerageFee" ? (
                                      <>
                                        <div className="col-md-4">
                                          <span className="ms-2">
                                            {
                                              (row.payto =
                                                row.payto === "brokerageFee"
                                                  ? "In Depth Realty"
                                                  : "In Depth Realty")
                                            }
                                          </span>
                                          <img
                                            className="ms-3"
                                            onClick={() =>
                                              handleRemovePayout(index)
                                            }
                                            src="https://cdn.pboffice.net/shared/transaction/page_select_drop.png"
                                          />
                                        </div>
                                      </>
                                    ) : (
                                      <div className="col-md-4">
                                        <div className="d-flex">
                                          <div className="col-md-6">
                                            <input
                                              className="form-control"
                                              name="payto"
                                              value={row.payto}
                                              onChange={(e) =>
                                                handleChangePayout(
                                                  e,
                                                  index,
                                                  "payto"
                                                )
                                              }
                                              onBlur={() =>
                                                setSelectedPayout("")
                                              }
                                            />
                                          </div>
                                          <div className="col-md-6 d-flex align-items-center justify-content-start">
                                            <img
                                              className="ms-3"
                                              onClick={() =>
                                                handleRemovePayout(index)
                                              }
                                              src="https://cdn.pboffice.net/shared/transaction/page_select_drop.png"
                                            />
                                          </div>
                                        </div>
                                      </div>
                                    )}
                                  </div>
                                </div>
                              ))}
                              <div className="row mt-4">
                                <div className="col-md-8">
                                  <select
                                    className="form-select"
                                    name="payoutItem"
                                    value={selectedPayout}
                                    onChange={handleInputChange}
                                    onBlur={() => setSelectedPayout("")}
                                  >
                                    <option>-- add a payout --</option>
                                    <option value="brokerageFee">
                                      Additional Brokerage Split or Fee
                                    </option>
                                    <option value="externalReferral">
                                      External Referral
                                    </option>
                                    <option value="otherPayout">Other…</option>
                                  </select>
                                </div>
                                <div className="col-md-4 d-flex align-items-center justify-content-start">
                                  <h6 className="m-0">
                                    {" "}
                                    Total: $
                                    {totalAmountNew
                                      ? totalAmountNew.toFixed(2)
                                      : ""}
                                  </h6>
                                </div>
                              </div>
                            </>
                          )}
                        </>
                      ) : (
                        <>
                          {summary.represent === "both" ? (
                            <>
                              {summary.phase === "canceled" ? (
                                <div className="row mt-0">
                                  <div className="col-md-4 d-flex align-items-center justify-content-start">
                                    <p className="mb-0">Transaction Fee</p>
                                  </div>
                                  <div className="col-md-4">
                                    <input
                                      className="form-control"
                                      id="inline-form-input"
                                      type="text"
                                      name="transactionFee"
                                      value={transactionFesData.transactionFee}
                                      // onChange={handleChange}
                                    />
                                  </div>
                                  <div className="col-md-4 d-flex align-items-center justify-content-start">
                                    <p className="mb-0">In Depth Realty</p>
                                  </div>

                                  <div className="col-md-4  align-items-center justify-content-start">
                                    <h6 className="mb-0">
                                      Total: ${totalAmountNew}
                                    </h6>
                                  </div>
                                </div>
                              ) : (
                                <>
                                  {getCommission.property_interest &&
                                  getCommission.property_interest === "yes" ? (
                                    <>
                                      <div className="row mt-0">
                                        <div className="col-md-4 d-flex align-items-center justify-content-start">
                                          <p className="mb-0">
                                            Transaction Fee
                                          </p>
                                        </div>
                                        <div className="col-md-4">
                                          <input
                                            className="form-control"
                                            id="inline-form-input"
                                            type="text"
                                            name="transactionFee"
                                            value={
                                              transactionFesData.transactionFee
                                            }
                                            // onChange={handleChange}
                                          />
                                        </div>
                                        <div className="col-md-4 d-flex align-items-center justify-content-start">
                                          <p className="mb-0">
                                            In Depth Realty
                                          </p>
                                        </div>
                                      </div>

                                      <div className="row mt-2">
                                        <div className="col-md-4 d-flex align-items-center justify-content-start">
                                          <p className="mb-0"> Wire fee:</p>
                                        </div>
                                        <div className="col-md-4">
                                          <input
                                            className="form-control"
                                            id="inline-form-input"
                                            type="text"
                                            name="wireFee"
                                            value={transactionWire.wireFee}
                                            // onChange={handleChange}
                                          />
                                        </div>
                                        <div className="col-md-4 d-flex align-items-center justify-content-start">
                                          <p className="mb-0">
                                            In Depth Realty
                                          </p>
                                        </div>
                                      </div>

                                      <div className="row mt-2">
                                        <div className="col-md-4 d-flex align-items-center justify-content-start">
                                          <p className="mb-0">
                                            Personal Real Estate Risk Management
                                            Fee:
                                          </p>
                                        </div>
                                        <div className="col-md-4">
                                          <input
                                            className="form-control"
                                            id="inline-form-input"
                                            type="text"
                                            name="transactionPersonal"
                                            value={
                                              transactionFesPersonal.transactionPersonal
                                            }
                                            // onChange={handleChange}
                                          />
                                        </div>
                                        <div className="col-md-4 d-flex align-items-center justify-content-start">
                                          <p className="mb-0">
                                            In Depth Realty
                                          </p>
                                        </div>
                                      </div>

                                      {getCommission && (
                                        <div className="row mt-2">
                                          <div className="col-md-4 d-flex align-items-center justify-content-start">
                                            <p className="mb-0">
                                              High Value Transaction Fee:
                                            </p>
                                          </div>

                                          <div className="col-md-4">
                                            <input
                                              className="form-control"
                                              id="inline-form-input"
                                              type="text"
                                              name="transactionHighValueFee"
                                              value={transactionHighValueFee}
                                              readOnly
                                            />
                                          </div>
                                          <div className="col-md-4 d-flex align-items-center justify-content-start">
                                            <p className="mb-0">
                                              In Depth Realty
                                            </p>
                                          </div>
                                        </div>
                                      )}
                                    </>
                                  ) : (
                                    <>
                                      <div className="row mt-0">
                                        <div className="col-md-4 d-flex align-items-center justify-content-start">
                                          <p className="mb-0">
                                            Buyer Side Transaction fee
                                          </p>
                                        </div>
                                        <div className="col-md-4">
                                          <input
                                            className="form-control"
                                            id="inline-form-input"
                                            type="text"
                                            name="buyerFee"
                                            value={transactionBuyerFee.buyerFee}
                                            // onChange={handleChange}
                                          />
                                        </div>
                                        <div className="col-md-4 d-flex align-items-center justify-content-start">
                                          <p className="mb-0">
                                            In Depth Realty
                                          </p>
                                        </div>
                                      </div>

                                      <div className="row mt-2">
                                        <div className="col-md-4 d-flex align-items-center justify-content-start">
                                          <p className="mb-0">
                                            Seller Side Transaction fee:
                                          </p>
                                        </div>
                                        <div className="col-md-4">
                                          <input
                                            className="form-control"
                                            id="inline-form-input"
                                            type="text"
                                            name="sellerFee"
                                            value={
                                              transactionSellerFee.sellerFee
                                            }
                                            // onChange={handleChange}
                                          />
                                        </div>
                                        <div className="col-md-4 d-flex align-items-center justify-content-start">
                                          <p className="mb-0">
                                            In Depth Realty
                                          </p>
                                        </div>
                                      </div>

                                      <div className="row mt-2">
                                        <div className="col-md-4 d-flex align-items-center justify-content-start">
                                          <p className="mb-0">
                                            Dual Agency Risk Management Fee:
                                          </p>
                                        </div>
                                        <div className="col-md-4">
                                          <input
                                            className="form-control"
                                            id="inline-form-input"
                                            type="text"
                                            name="managementFee"
                                            value={
                                              transactionRisk.managementFee
                                            }
                                            // onChange={handleChange}
                                          />
                                        </div>
                                        <div className="col-md-4 d-flex align-items-center justify-content-start">
                                          <p className="mb-0">
                                            In Depth Realty
                                          </p>
                                        </div>
                                      </div>

                                      <div className="row mt-2">
                                        <div className="col-md-4 d-flex align-items-center justify-content-start">
                                          <p className="mb-0">Wire fee:</p>
                                        </div>
                                        <div className="col-md-4">
                                          <input
                                            className="form-control"
                                            id="inline-form-input"
                                            type="text"
                                            name="wireFee"
                                            value={transactionWire.wireFee}
                                            // onChange={handleChange}
                                          />
                                        </div>
                                        <div className="col-md-4 d-flex align-items-center justify-content-start">
                                          <p className="mb-0">
                                            In Depth Realty
                                          </p>
                                        </div>
                                      </div>
                                    </>
                                  )}

                                  {rowsPayout.map((row, index) => (
                                    <div key={index}>
                                      <div className="row mt-4">
                                        <div className="col-md-4">
                                          <input
                                            className="form-control"
                                            id={`type-${index}`}
                                            type="text"
                                            name="type"
                                            value={row.type}
                                            onChange={(e) =>
                                              handleChangeRowPayout(
                                                e,
                                                index,
                                                "type"
                                              )
                                            }
                                          />
                                        </div>

                                        <div className="col-md-4">
                                          <input
                                            className="form-control"
                                            id={`amount-${index}`}
                                            type="number"
                                            name="amount"
                                            value={row.amount}
                                            onChange={(e) =>
                                              handleChangePayout(
                                                e,
                                                index,
                                                "amount"
                                              )
                                            }
                                          />
                                        </div>

                                        {row.type === "brokerageFee" ? (
                                          <>
                                            <div className="col-md-4">
                                              <span className="ms-2">
                                                {
                                                  (row.payto =
                                                    row.payto === "brokerageFee"
                                                      ? "In Depth Realty"
                                                      : "In Depth Realty")
                                                }
                                              </span>
                                              <img
                                                className="ms-3"
                                                onClick={() =>
                                                  handleRemovePayout(index)
                                                }
                                                src="https://cdn.pboffice.net/shared/transaction/page_select_drop.png"
                                              />
                                            </div>
                                          </>
                                        ) : (
                                          <div className="col-md-4">
                                            <div className="d-flex">
                                              <div className="col-md-6">
                                                <input
                                                  className="form-control"
                                                  name="payto"
                                                  value={row.payto}
                                                  onChange={(e) =>
                                                    handleChangePayout(
                                                      e,
                                                      index,
                                                      "payto"
                                                    )
                                                  }
                                                  onBlur={() =>
                                                    setSelectedPayout("")
                                                  }
                                                />
                                              </div>
                                              <div className="col-md-6 d-flex align-items-center justify-content-start">
                                                <img
                                                  className="ms-3"
                                                  onClick={() =>
                                                    handleRemovePayout(index)
                                                  }
                                                  src="https://cdn.pboffice.net/shared/transaction/page_select_drop.png"
                                                />
                                              </div>
                                            </div>
                                          </div>
                                        )}
                                      </div>
                                    </div>
                                  ))}
                                  <div className="row mt-4">
                                    <div className="col-md-8">
                                      <select
                                        className="form-select"
                                        name="payoutItem"
                                        value={selectedPayout}
                                        onChange={handleInputChange}
                                        onBlur={() => setSelectedPayout("")}
                                      >
                                        <option>-- add a payout --</option>
                                        <option value="brokerageFee">
                                          Additional Brokerage Split or Fee
                                        </option>
                                        <option value="externalReferral">
                                          External Referral
                                        </option>
                                        <option value="otherPayout">
                                          Other…
                                        </option>
                                      </select>
                                    </div>
                                    <div className="col-md-4 d-flex align-items-center justify-content-start">
                                      <h6 className="mb-0">
                                        Total: $
                                        {totalAmountNew
                                          ? totalAmountNew.toFixed(2)
                                          : ""}
                                      </h6>
                                    </div>
                                  </div>
                                </>
                              )}
                            </>
                          ) : (
                            <>
                              {summary.phase === "canceled" ? (
                                <div className="row mt-0">
                                  <div className="col-md-4 d-flex align-items-center justify-content-start">
                                    <p className="mb-0">Transaction Fee</p>
                                  </div>
                                  <div className="col-md-4">
                                    <input
                                      className="form-control"
                                      id="inline-form-input"
                                      type="text"
                                      name="transactionFee"
                                      value={transactionFesData.transactionFee}
                                      // onChange={handleChange}
                                    />
                                  </div>
                                  <div className="col-md-4 d-flex align-items-center justify-content-start">
                                    <p className="mb-0">In Depth Realty</p>
                                  </div>

                                  <div className="col-md-4  align-items-center justify-content-start">
                                    <h6 className="mb-0">
                                      Total: ${totalAmountNew}
                                    </h6>
                                  </div>
                                </div>
                              ) : (
                                <>
                                  <div className="row mt-0">
                                    <div className="col-md-4 d-flex align-items-center justify-content-start">
                                      <p className="mb-0">Transaction Fee</p>
                                    </div>
                                    <div className="col-md-4">
                                      <input
                                        className="form-control"
                                        id="inline-form-input"
                                        type="text"
                                        name="transactionFee"
                                        value={
                                          transactionFesData.transactionFee
                                        }
                                        // onChange={handleChange}
                                      />
                                    </div>
                                    <div className="col-md-4 d-flex align-items-center justify-content-start">
                                      <p className="mb-0">In Depth Realty</p>
                                    </div>
                                  </div>

                                  <div className="row mt-2">
                                    <div className="col-md-4 d-flex align-items-center justify-content-start">
                                      <p className="mb-0"> Wire fee:</p>
                                    </div>
                                    <div className="col-md-4">
                                      <input
                                        className="form-control"
                                        id="inline-form-input"
                                        type="text"
                                        name="wireFee"
                                        value={transactionWire.wireFee}
                                        // onChange={handleChange}
                                      />
                                    </div>
                                    <div className="col-md-4 d-flex align-items-center justify-content-start">
                                      <p className="mb-0">In Depth Realty</p>
                                    </div>
                                  </div>

                                  {getCommission && (
                                    <div className="row mt-2">
                                      <div className="col-md-4 d-flex align-items-center justify-content-start">
                                        <p className="mb-0">
                                          High Value Transaction Fee:
                                        </p>
                                      </div>

                                      <div className="col-md-4">
                                        <input
                                          className="form-control"
                                          id="inline-form-input"
                                          type="text"
                                          name="transactionHighValueFee"
                                          value={transactionHighValueFee}
                                          readOnly
                                        />
                                      </div>
                                      <div className="col-md-4 d-flex align-items-center justify-content-start">
                                        <p className="mb-0">In Depth Realty</p>
                                      </div>
                                    </div>
                                  )}

                                  {getCommission.property_interest &&
                                  getCommission.property_interest === "yes" ? (
                                    <div className="row mt-2">
                                      <div className="col-md-4 d-flex align-items-center justify-content-start">
                                        <p className="mb-0">
                                          Personal Real Estate Risk Management
                                          Fee:
                                        </p>
                                      </div>
                                      <div className="col-md-4">
                                        <input
                                          className="form-control"
                                          id="inline-form-input"
                                          type="text"
                                          name="transactionPersonal"
                                          value={
                                            transactionFesPersonal.transactionPersonal
                                          }
                                          // onChange={handleChange}
                                        />
                                      </div>
                                      <div className="col-md-4 d-flex align-items-center justify-content-start">
                                        <p className="mb-0">In Depth Realty</p>
                                      </div>
                                    </div>
                                  ) : (
                                    ""
                                  )}

                                  {rowsPayout.map((row, index) => (
                                    <div key={index}>
                                      <div className="row mt-4">
                                        <div className="col-md-4">
                                          <input
                                            className="form-control"
                                            id={`type-${index}`}
                                            type="text"
                                            name="type"
                                            value={row.type}
                                            onChange={(e) =>
                                              handleChangeRowPayout(
                                                e,
                                                index,
                                                "type"
                                              )
                                            }
                                          />
                                        </div>

                                        <div className="col-md-4">
                                          <input
                                            className="form-control"
                                            id={`amount-${index}`}
                                            type="text"
                                            name="amount"
                                            value={row.amount}
                                            onChange={(e) =>
                                              handleChangePayout(
                                                e,
                                                index,
                                                "amount"
                                              )
                                            }
                                          />
                                        </div>

                                        {row.type === "brokerageFee" ? (
                                          <>
                                            <div className="col-md-4">
                                              <span className="ms-2">
                                                {
                                                  (row.payto =
                                                    row.payto === "brokerageFee"
                                                      ? "In Depth Realty"
                                                      : "In Depth Realty")
                                                }
                                              </span>
                                              <img
                                                className="ms-3"
                                                onClick={() =>
                                                  handleRemovePayout(index)
                                                }
                                                src="https://cdn.pboffice.net/shared/transaction/page_select_drop.png"
                                              />
                                            </div>
                                          </>
                                        ) : (
                                          <div className="col-md-4">
                                            <div className="d-flex">
                                              <div className="col-md-6">
                                                <input
                                                  className="form-control"
                                                  name="payto"
                                                  value={row.payto}
                                                  onChange={(e) =>
                                                    handleChangePayout(
                                                      e,
                                                      index,
                                                      "payto"
                                                    )
                                                  }
                                                  onBlur={() =>
                                                    setSelectedPayout("")
                                                  }
                                                />
                                              </div>
                                              <div className="col-md-6 d-flex align-items-center justify-content-start">
                                                <img
                                                  className="ms-3"
                                                  onClick={() =>
                                                    handleRemovePayout(index)
                                                  }
                                                  src="https://cdn.pboffice.net/shared/transaction/page_select_drop.png"
                                                />
                                              </div>
                                            </div>
                                          </div>
                                        )}
                                      </div>
                                    </div>
                                  ))}

                                  <div className="row mt-4">
                                    <div className="col-md-8">
                                      <select
                                        className="form-select"
                                        name="payoutItem"
                                        value={selectedPayout}
                                        onChange={handleInputChange}
                                        onBlur={() => setSelectedPayout("")}
                                      >
                                        <option>-- add a payout --</option>
                                        <option value="brokerageFee">
                                          Additional Brokerage Split or Fee
                                        </option>
                                        <option value="externalReferral">
                                          External Referral
                                        </option>
                                        <option value="otherPayout">
                                          Other…
                                        </option>
                                      </select>
                                    </div>
                                    <div className="col-md-4 d-flex align-items-center justify-content-start">
                                      <h6 className="m-0">
                                        {" "}
                                        Total: $
                                        {totalAmountNew
                                          ? totalAmountNew.toFixed(2)
                                          : ""}
                                      </h6>
                                    </div>
                                  </div>
                                </>
                              )}
                            </>
                          )}
                        </>
                      )}

                      <div className="row mt-3">
                        <div className="col-md-12 mt-2">
                          <p className="mb-2">
                            AGENT REMARKS - Enter any comments you have about
                            the funding of this transaction.
                          </p>
                        </div>

                        <div className="col-md-12">
                          <label className="col-form-label pt-0 mb-0 float-left w-100 mb-0">
                            Comments/Remarks/Instructions
                          </label>
                          <textarea
                            className="form-control mt-0"
                            id="textarea-input"
                            rows="5"
                            name="comments"
                            onChange={handleChangeNew}
                            value={
                              formValuesNew.comments ?? findingGet?.comments
                            }
                            autoComplete="on"
                          ></textarea>
                        </div>
                      </div>
                      <div className="pull-right mt-4">
                        <NavLink
                          to={`/transaction-review/${params.id}`}
                          className="btn btn-secondary pull-right ms-3"
                          type="button"
                        >
                          Cancel
                        </NavLink>
                        <button
                          className="btn btn-primary pull-right"
                          type="button"
                          disabled={isActive}
                          onClick={handleSubmit}
                        >
                          {isActive
                            ? "Please Wait..."
                            : "Create Funding Request"}
                        </button>
                      </div>
                    </div>

                    <div className="col-md-2 offset-md-1">
                      <div className="d-flex align-items-center justify-content-between mb-4">
                        <h5 className="mb-0">Transaction Details</h5>
                      </div>
                      <div className="border rounded-3 p-3 float-left w-100 bg-light">
                        <label className="col-form-label pt-0 mb-0 py-1">
                          Funding :
                        </label>
                        <p className="mb-3">Not Set</p>
                        <label className="col-form-label pt-0 mb-0 py-1">
                          {" "}
                          {summary.represent ? (
                            <>
                              {summary.represent === "buyer" ? "Buyer" : ""}

                              {summary.represent === "seller" ? "Seller" : ""}

                              {summary.represent === "both"
                                ? "Buyer & Seller"
                                : ""}

                              {summary.represent === "referral" ? "Seller" : ""}
                            </>
                          ) : (
                            ""
                          )}{" "}
                          (Client):
                        </label>
                        <p className="mb-3">
                          {" "}
                          {summary?.contact1?.[0]?.data?.firstName ??
                            "Not Set"}{" "}
                          {summary?.contact1?.[0]?.data?.lastName ?? "Not Set"}
                        </p>

                        <label className="col-form-label pt-0 mb-0 py-1">
                          {summary?.contact2?.[0]?.data?.type === "seller"
                            ? "Seller"
                            : summary?.contact2?.[0]?.data?.type === "buyer"
                            ? "Buyer"
                            : summary?.contact2?.[0]?.data?.type === "buyersale"
                            ? "Buyer & Seller"
                            : summary?.contact2?.[0]?.data?.type ===
                              "referral_sellerAgent"
                            ? "Buyer"
                            : "Default Label"}
                        </label>
                        {summary?.contact2?.[0]?.data?.type ===
                        "referral_sellerAgent" ? (
                          <p className="mb-3">Not Set</p>
                        ) : (
                          <p className="mb-3">
                            {summary?.contact2?.[0]?.data?.firstName ??
                              "Not Set"}{" "}
                            {summary?.contact2?.[0]?.data?.lastName ?? ""}
                          </p>
                        )}

                        <label className="col-form-label pt-0 mb-0 py-1">
                          {" "}
                          MLS #:
                        </label>
                        <p className="mb-3">
                          {" "}
                          {summary?.propertyDetail?.mlsNumber ?? "Not set"}
                        </p>
                        <label className="col-form-label pt-0 mb-0 py-1">
                          Sale Price:
                        </label>
                        <p className="mb-3">
                          {getCommission.purchase_price ?? "Not set"}
                        </p>
                        <label className="col-form-label pt-0 mb-0 py-1">
                          Legacy:
                        </label>
                        <p className="mb-3">
                          {getContact?.legacy_account === "yes" ? "Yes" : "No"}
                        </p>

                        <label className="col-form-label pt-0 mb-0 py-1">
                          Commission:
                        </label>
                        <p className="mb-3">Not Set</p>
                        <label className="col-form-label pt-0 mb-0 py-1">
                          Moved-In Date:
                        </label>
                        <p className="mb-3">Not Set</p>
                        <label className="col-form-label pt-0 mb-0 py-1">
                          Time Spent:
                        </label>
                        <p className="mb-3">{formattedDateTime ?? "Not set"}</p>

                        <label className="col-form-label pt-0 mb-0 py-1">
                          Agent's Comm. Plan:
                        </label>
                        <p className="mb-3">Not Set</p>
                      </div>
                    </div>
                  </div>
                ) : requestreview ? (
                  <>
                    {summary.trans_status === "submited" ? (
                      <div className="welcome_msg">
                        <ConfettiCanvas />
                        <div className="confetti-message">
                          <h2>Congratulations on your closing!!!</h2>
                          <p>
                            Your file will now be reviewed and submitted for
                            payment.
                          </p>
                        </div>
                      </div>
                    ) : (
                      <>
                        <div className="d-flex align-items-center justify-content-between mb-4">
                          <h3 className="mb-0">Submit the Transaction</h3>
                        </div>
                        <div className="row">
                          <div className="col-md-8">
                            <div className="border rounded-3 p-3 bg-light">
                              <div className="float-left w-100">
                                <p>
                                  The funding request for this transaction has
                                  been created.
                                </p>
                              </div>

                              {/* <p className="mb-2">You will return here after you've completed the form.</p> */}
                            </div>
                            {/* {
                        summary.trans_status === "submited" ?
                        <></>
                      : */}
                            <div className="pull-right mt-4">
                              <button
                                className="btn btn-primary btn-sm pull-right"
                                onClick={handleUpdatetransaction}
                              >
                                Continue to Submit
                              </button>
                            </div>

                            {/* } */}
                          </div>
                        </div>
                      </>
                    )}
                  </>
                ) : (
                  <>
                    <div className="d-flex align-items-center justify-content-between mb-4">
                      <h3 className="mb-0">Submit Transaction for Review</h3>
                    </div>
                    <div className="row">
                      <div className="col-md-8">
                        <div className="border rounded-3 p-3 bg-light">
                          <div className="float-left w-100">
                            <p className="mb-2">Create Funding Request</p>
                            <p>
                              To complete the Funding Request, you will need to
                              know:
                            </p>
                          </div>
                          <ul style={{ fontSize: 16, color: "black" }}>
                            <li>
                              The Tenant's Broker contact information
                              (companyName and Agent name, phone number, and
                              <br /> fax or email)
                            </li>
                            <li>
                              The settlement charges, inluding your commission,
                              relevant bonuses, and any other fees
                              <br /> you charge to clients.
                            </li>
                            <li>
                              Names and amounts for special payouts like
                              Contributions back to your client,
                              <br /> commission splits with your partners, etc.
                            </li>
                          </ul>
                          <p className="mb-2">
                            You will return here after you've completed the
                            form.
                          </p>
                        </div>

                        <div className="row mt-5">
                          <div className="col-md-12">
                            <p>
                              {" "}
                              <b>
                                Please select what option the title company will
                                be delivering commission funds to the brokerage
                                for this transaction.
                              </b>
                            </p>
                          </div>

                          <div className="col-md-12">
                            <div className="form-check ms-lg-3 ms-md-3">
                              <input
                                className="form-check-input"
                                id=""
                                type="radio"
                                name="funding_method"
                                value="wired_funds"
                                onChange={handleChange}
                                checked={
                                  formValues?.funding_method === "wired_funds"
                                }
                              />
                              <label className="form-label" id="">
                                Wired Funds
                              </label>
                              <p>
                                This is our preferred method and is the quickest
                                way for you to receive your commissions. Funds
                                are usually in your bank account within 1-2
                                Business days after the brokerage receives the
                                funds and your transaction is approved. Wire
                                instructions can be{" "}
                                <a
                                  href="https://brokeragentbase.s3.amazonaws.com/1697638834179-In%20Depth%20Commission%20Wire%20Insturctions.pdf"
                                  target="_blank"
                                >
                                  found here.{" "}
                                </a>{" "}
                                Please provide the wire instructions to your
                                title company.
                              </p>
                            </div>

                            <div className="form-check ms-lg-3 ms-md-3">
                              <input
                                className="form-check-input"
                                id=""
                                type="radio"
                                name="funding_method"
                                value="check"
                                onChange={handleChange}
                                checked={formValues?.funding_method === "check"}
                              />
                              <label className="form-label" id="">
                                Check
                              </label>
                              <p>
                                Checks being delivered, overnighted, mailed or
                                deposited will have a 7-10 business day hold.
                                This is a hold placed by the bank to clear
                                funds. Please have your title company send all
                                checks to In Depth Realty 10808 South River
                                Front Parkway, Suite 3065, South Jordan, Utah
                                84095. By selecting this option you agree that
                                your commission funds will not be available for
                                payment for the 7-10 business days after the
                                brokerage has received and deposited the funds.
                                Please choose the wire option for quicker
                                payments.
                              </p>
                            </div>

                            <div className="invalid-tooltip">
                              {formErrors.funding_method}
                            </div>
                          </div>
                        </div>

                        <div className="pull-right mt-4">
                          <button
                            className="btn btn-primary btn-sm pull-right"
                            onClick={handleRequest}
                          >
                            Submit Transaction for Review
                          </button>
                        </div>
                      </div>
                    </div>
                  </>
                )}
              </div>
            </div>
          </div>
        </div>
      </main>
    </div>
  );
};

export default FundingRequest;

import React, { useEffect, useState } from "react";
import Loader from "../../Pages/Loader/Loader.js";
import Toaster from "../../Pages/Toaster/Toaster.js";
import user_service from "../service/user_service";
import moment from "moment";
import ReactPaginate from "react-paginate";


const ListingUtility = () => {
  const [loader, setLoader] = useState({ isActive: null });
  const { isActive } = loader;
  const [toaster, setToaster] = useState({
    types: null,
    isShow: null,
    toasterBody: "",
    message: "",
  });
  const { types, isShow, toasterBody, message } = toaster;

  const [summary, setSummary] = useState([]);
  const [pageCount, setPageCount] = useState(0);

  const UtiltityAgentGetData = async () => {
    await user_service.UtiltityAgentGet(1).then((response) => {
      setLoader({ isActive: false });
      if (response) {
        setSummary(response.data.data);
        setPageCount(Math.ceil(response.data.totalRecords / 10));
      }
    });
  };

  useEffect(() => {
    UtiltityAgentGetData();
  }, []);


  const handlePageClick = async (data) => {
    try {
      const currentPage = data.selected + 1;
      setLoader({ isActive: true });
      const response = await user_service.UtiltityAgentGet(currentPage);
      setLoader({ isActive: false });
      if (response && response.data) {
        setSummary(response.data.data);
        setPageCount(Math.ceil(response.data.totalRecords / 10));
      }
    } catch (error) {
      console.error("Error in handlePageClick:", error);
      // Handle errors as needed
    }
  };


  const formatDateTime = (date, time) => {
    const formattedDate = moment.utc(date).format("MM/DD/YYYY"); // Adjusted format to "Day/Month/Year"
    // const formattedTime = moment.utc(time, 'HH:mm').format('hh:mm A'); // Adjusted format to 12-hour time with AM/PM
    return `${formattedDate}`;
  };

  const [query, setQuery] = useState("");
  const [pageCountNew, setPageCountNew] = useState(0);
  const [utilityData, setUtilityData] = useState([]);
  const [isLoading, setIsLoading] = useState(false);


  const UtilitySearch = async () => {
    setIsLoading(true);
    await user_service.UtilityDataAgentGet(1, query).then((response) => {
      setIsLoading(false);
      if (response) {
        setUtilityData(response.data.data);
        setPageCountNew(Math.ceil(response.data.totalRecords / 10));
      }
    });
  };

  useEffect(() => {
    if (query) {
      UtilitySearch();
    }
  }, [query]);

  const handlePageClickNew = async (data) => {
    let currentPage = data.selected + 1;
    setLoader({ isActive: true });

    try {
      const response = await user_service.UtilityDataAgentGet(
        currentPage,
        query
      );
      setLoader({ isActive: false });

      if (response) {
        setUtilityData(response.data.data);
        setPageCountNew(Math.ceil(response.data.totalRecords / 10));
      }
    } catch (error) {
      console.error("Error fetching data:", error);
      setLoader({ isActive: false });
    }
  };

  return (
    <div className="bg-secondary float-left w-100 pt-4">
      <Loader isActive={isActive} />
      {isShow && (
        <Toaster
          types={types}
          isShow={isShow}
          toasterBody={toasterBody}
          message={message}
        />
      )}
      <main className="page-wrapper">
        <div className="row">
          {/* <!-- Page container--> */}
          <div className="col-md-12">
            <div className="bg-light border rounded-3 mb-4 p-3">
              <h3 className="text-left mb-0">Utility Tool Listing</h3>
            </div>
            <div className="content-overlay">
              <div className="col-md-12 pb-md-3">
                <div className="bg-light border rounded-3 p-3 w-100">
                  <div className="float-none m-auto w-75">
                    <div className="row d-flex justify-content-center m-auto">
                      <h6 className="text-center">Find All Your Utilities</h6>
                      <p className="text-center mb-4">
                        Find the utility company for your internet,Tv,
                        <br />
                        security,electricity,gas,water and garbage.
                      </p>
                      <div className="col-md-6 mb-3">
                        <label className="form-label">Search by Zip code</label>
                        <input
                          className="form-control"
                          type="text"
                          id="search-input-1"
                          placeholder="Enter a zip code"
                          valvalue={query}
                          onChange={(event) => setQuery(event.target.value)}
                        />
                      </div>
                    </div>
                  </div>
                </div>

                {query ? (
                  <div className="col-md-12 bg-light border rounded-3 p-3 mt w-100 mt-3">
                    <table
                      id="DepositLedger"
                      className="table table-striped mb-0"
                      width="100%"
                      border="0"
                      cellSpacing="0"
                      cellPadding="0"
                    >
                      <tbody>
                        <table
                          id="DepositLedger"
                          className="table table-striped mb-0"
                          width="100%"
                          border="0"
                          cellSpacing="0"
                          cellPadding="0"
                        >
                          <thead>
                            <tr>
                              <th>NEARBY UTILITIES</th>
                              <th>PROVIDER</th>
                              <th>TYPE</th>
                              <th>CONTACT</th>
                            </tr>
                          </thead>
                          <tbody>
                            {Array.isArray(utilityData) &&
                            utilityData.length > 0 ? (
                              utilityData.map((item) => (
                                <tr key={item.id}>
                                  <td>
                                    {item.utilityLogo ? (
                                      <img
                                        style={{ width: "100px" }}
                                        src={item.utilityLogo}
                                      />
                                    ) : (
                                      ""
                                    )}
                                  </td>
                                  <td>
                                    <a
                                      href={item.providerLink}
                                      target="_blank"
                                      rel="noopener noreferrer"
                                    >
                                      {item.provider}
                                    </a>
                                  </td>
                                  <td>{item.type}</td>
                                  <td>{item.contact}</td>
                                </tr>
                              ))
                            ) : (
                              <tr>
                                <td>No data to display.</td>
                              </tr>
                            )}
                          </tbody>
                        </table>
                      </tbody>
                    </table>
                  </div>
                ) : (
                  <>
                    {summary.length > 0 ? (
                      summary.map((post, index) => (
                        <div key={index} className="card card-hover mb-2 mt-2">
                          <div className="card-body d-flex">
                            <ul className="list-unstyled col-md-4">
                              <li>
                                <b>Full Name: </b>
                                {post.firstName} {post.lastName}
                              </li>
                              <li>
                                <b>Email: </b>
                                {post.email}
                              </li>
                              <li>
                                <b>Estimate-Date: </b>
                                {formatDateTime(post.estimateDate)}
                              </li>

                              {post.inviteDate ? (
                                <li>
                                  <b>Invite-Date: </b>
                                  {formatDateTime(post.inviteDate)}
                                </li>
                              ) : (
                                ""
                              )}
                            </ul>
                            <ul className="list-unstyled col-md-4">
                              <li>
                                <b>Old Street Address: </b>
                                {post.oldStreetAddress}
                              </li>
                              <li>
                                <b>Old City: </b>
                                {post.oldCity}
                              </li>

                              <li>
                                <b>Old State: </b>
                                {post.oldState}
                              </li>

                              <li>
                                <b>Old Zip-code: </b>
                                {post.oldZipcode}
                              </li>

                              {post.inviteDate ? (
                                <li>
                                  <b>Invite-Date: </b>
                                  {post.inviteDate}
                                </li>
                              ) : (
                                ""
                              )}
                            </ul>

                            <ul className="list-unstyled col-md-4">
                              <li>
                                <b>New Street Address: </b>
                                {post.newStreetAddress}
                              </li>
                              <li>
                                <b>New City: </b>
                                {post.newCity}
                              </li>

                              <li>
                                <b>New State: </b>
                                {post.newState}
                              </li>

                              <li>
                                <b>New Zip-code: </b>
                                {post.newZipcode}
                              </li>

                              {post.inviteDate ? (
                                <li>
                                  <b>Invite-Date: </b>
                                  {post.inviteDate}
                                </li>
                              ) : (
                                ""
                              )}
                            </ul>
                          </div>
                        </div>
                      ))
                    ) : (
                          <p className="text-center text-white py-4">No open house data available.</p>
                    )}


               <div className="justify-content-end mb-1">
                  <ReactPaginate
                    previousLabel={"Previous"}
                    nextLabel={"Next"}
                    breakLabel={"..."}
                    pageCount={pageCount}
                    marginPagesDisplayed={1}
                    pageRangeDisplayed={2}
                    onPageChange={handlePageClick}
                    containerClassName={"pagination justify-content-center"}
                    pageClassName={"page-item"}
                    pageLinkClassName={"page-link"}
                    previousClassName={"page-item"}
                    previousLinkClassName={"page-link"}
                    nextClassName={"page-item"}
                    nextLinkClassName={"page-link"}
                    breakClassName={"page-item"}
                    breakLinkClassName={"page-link"}
                    activeClassName={"active"}
                  />
                </div>
                  </>
                )}

                {query ? (
                  <div className="d-flex align-items-center justify-content-end mb-0 mt-4">
                    <ReactPaginate
                      className=""
                      previousLabel={"Previous"}
                      nextLabel={"next"}
                      breakLabel={"..."}
                      pageCount={pageCountNew}
                      marginPagesDisplayed={1}
                      pageRangeDisplayed={2}
                      onPageChange={handlePageClickNew}
                      containerClassName={
                        "pagination justify-content-center mb-0"
                      }
                      pageClassName={"page-item"}
                      pageLinkClassName={"page-link"}
                      previousClassName={"page-item"}
                      previousLinkClassName={"page-link"}
                      nextClassName={"page-item"}
                      nextLinkClassName={"page-link"}
                      breakClassName={"page-item"}
                      breakLinkClassName={"page-link"}
                      activeClassName={"active"}
                    />
                  </div>
                ) : (
                  ""
                )}
              </div>
            </div>
          </div>
        </div>
      </main>
    </div>
  );
};

export default ListingUtility;

import React, { useEffect, useState } from "react";
import avtar from "../img/avtar.jpg";
import { NavLink, useNavigate } from "react-router-dom";
import user_service from "../service/user_service";
import ReactPaginate from "react-paginate";
import Loader from "../../Pages/Loader/Loader.js";
import Toaster from "../../Pages/Toaster/Toaster";
import defaultpropertyimage from "../../Components/img/defaultpropertyimage.jpeg";
import jwt from "jwt-decode";
import Swal from "sweetalert2";
import "sweetalert2/dist/sweetalert2.css";

function MyTransaction() {
  const [activeGet, setActiveGet] = useState([]);
  const [results, setResults] = useState([]);
  const [pageCount, setPageCount] = useState(0);
  const [loader, setLoader] = useState({ isActive: null });
  const { isActive } = loader;
  const [toaster, setToaster] = useState({
    types: null,
    isShow: null,
    toasterBody: "",
    message: "",
  });
  const { types, isShow, toasterBody, message } = toaster;
  const [represent, setRepresent] = useState("");
  const [type, setType] = useState("");
  const [phase, setPhase] = useState("");
  const [filing, setFiling] = useState("");
  const [profile, setProfile] = useState("");

  const [searchdata, setSearchdata] = useState("");
  const navigate = useNavigate();

  const AddTransaction = () => {
    navigate("/new-transaction");
  };

  const Catalog = (id, agentID) => {
    if (localStorage.getItem("auth")) {
      const decodedToken = jwt(localStorage.getItem("auth"));
      if (
        decodedToken.contactType === "admin" ||
        decodedToken.contactType === "staff" ||
        agentID === decodedToken.id
      ) {
        navigate(`/transaction-summary/${id}`);
      } else {
        navigate(`/transaction`);
      }
    }
  };

  const TransactionGetAll = async () => {
    try {
      setLoader({ isActive: true });

      let queryParams = `?page=1`;
      if (represent) queryParams += `&represent=${represent}`;
      if (type) queryParams += `&type=${type}`;
      if (phase) queryParams += `&phase=${phase}`;
      if (filing) queryParams += `&filing=${filing}`;

      if (
        localStorage.getItem("auth") &&
        jwt(localStorage.getItem("auth")).contactType == "admin"
      ) {
        // queryParams += `&agentId=${jwt(localStorage.getItem("auth")).id}`;
      } else if (
        localStorage.getItem("auth") &&
        jwt(localStorage.getItem("auth")).contactType == "staff"
      ) {
      } else {
        queryParams += `&agentId=${jwt(localStorage.getItem("auth")).id}`;
      }

      const response = await user_service.TransactionGet(queryParams);

      setLoader({ isActive: false });

      if (response && response.data) {
        setActiveGet(response.data.data);
        setPageCount(Math.ceil(response.data.totalRecords / 10));
      }
    } catch (error) {
      console.error("Error in TransactionGetAll:", error);
      // Handle errors as needed
    }
  };

  const handlePageClick = async (data) => {
    try {
      const currentPage = data.selected + 1;
      setLoader({ isActive: true });
      let queryParams = `?page=${currentPage}`;
      if (represent) queryParams += `&represent=${represent}`;
      if (type) queryParams += `&type=${type}`;
      if (phase) queryParams += `&phase=${phase}`;
      if (filing) queryParams += `&filing=${filing}`;

      if (
        localStorage.getItem("auth") &&
        jwt(localStorage.getItem("auth")).contactType == "admin"
      ) {
      } else if (
        localStorage.getItem("auth") &&
        jwt(localStorage.getItem("auth")).contactType == "staff"
      ) {
      } else {
        queryParams += `&agentId=${jwt(localStorage.getItem("auth")).id}`;
      }
      const response = await user_service.TransactionGet(queryParams);

      setLoader({ isActive: false });

      if (response && response.data) {
        setActiveGet(response.data.data);
        setPageCount(Math.ceil(response.data.totalRecords / 10));
      }
    } catch (error) {
      console.error("Error in handlePageClick:", error);
      // Handle errors as needed
    }
  };

  useEffect(() => {
    window.scrollTo(0, 0);
    TransactionGetAll();
  }, [represent, type, phase, filing]);

  {
    /* <!-- paginate Function Api call Start--> */
  }

  const handleResetClick = () => {
    setLoader({ isActive: true });
    setSearchdata("");
    TransactionGetAll();
  };

  const SearchGetAll = async () => {
    setLoader({ isActive: true });
    let queryParams = `?page=1`;
    if (
      localStorage.getItem("auth") &&
      jwt(localStorage.getItem("auth")).contactType == "admin"
    ) {
    } else if (
      localStorage.getItem("auth") &&
      jwt(localStorage.getItem("auth")).contactType == "staff"
    ) {
    } else {
      queryParams += `&agentId=${jwt(localStorage.getItem("auth")).id}`;
    }
    await user_service
      .TransactionGet(queryParams, searchdata)
      .then((response) => {
        if (response) {
          console.log(response);
          setActiveGet(response.data.data);
          setPageCount(Math.ceil(response.data.totalRecords / 10));
        }
        setLoader({ isActive: false });
      });
  };

  const propertypic = (e) => {
    e.target.src = defaultpropertyimage;
  };
  const handleChange = (e) => {
    setSearchdata(e.target.value);
  };

  const handleDeleteTransactionpermanent = async (id) => {
    try {
      const confirmation = await Swal.fire({
        title: "Are you sure?",
        text: "It will be permanently deleted!",
        icon: "warning",
        showCancelButton: true,
        confirmButtonColor: "#3085d6",
        cancelButtonColor: "#d33",
        confirmButtonText: "Yes, delete it!",
      });

      if (confirmation.isConfirmed) {
        const userData = {
          is_active: "0",
        };

        try {
          const response = await user_service.TransactionUpdate(id, userData);

          if (response) {
            setLoader({ isActive: false });

            if (response) {
              Swal.fire(
                "Deleted!",
                "Transaction is deleted permanently.",
                "success"
              );
            }

            setLoader({ isActive: true });
            TransactionGetAll();
          }
        } catch (error) {
          console.error(error);
          Swal.fire(
            "Error",
            "An error occurred while deleting the file.",
            "error"
          );
        }
      }
    } catch (error) {
      console.error(error);
      Swal.fire("Error", "An error occurred while deleting the file.", "error");
    }
  };

  useEffect(() => {
    window.scrollTo(0, 0);
    const profileGetAll = async () => {
      setLoader({ isActive: true });
      await user_service
        .profileGet(jwt(localStorage.getItem("auth")).id)
        .then((response) => {
          if (response) {
            setLoader({ isActive: false });
            setProfile(response.data);
          }
        });
    };
    profileGetAll();
  }, []);


  const handleSubmit = (id, contactType) => {
    if(id){
      if (contactType === "private") {
        navigate(`/private-contact/${id}`);
      } else {
        navigate(`/contact-profile/${id}`);
      }
    }
  };

  const handleCategory = (category)=>{
    setRepresent(category);
  }

  const handlePhase = (phase)=>{
    setPhase(phase);
  }
  
  return (
    <div className="bg-secondary float-left w-100 pt-4 mb-4">
      <Loader isActive={isActive} />
      {isShow && (
        <Toaster
          types={types}
          isShow={isShow}
          toasterBody={toasterBody}
          message={message}
        />
      )}
      <main className="page-wrapper">
        <div className="content-overlay">
          <div className="float-left w-100 mb-2 d-flex align-items-center justify-content-between">
            <h3 className="text-white mb-0">Transactions</h3>
          </div>
          <p className="text-white">
            {" "}
            Create, Manage, Search and View your Transactions here.
          </p>
          <hr className="mb-3" />
          <div className="form-outline d-lg-flex d-md-flex justify-content-between align-items-center w-100">
            <div className="float-start w-100 d-flex">
            <input
              id="search-input-1"
              type="search"
              className="form-control w-50"
              placeholder="Street address, contact name, or reference number."
              name="searcchdata"
              value={searchdata}
              onChange={handleChange}
              onKeyDown={(e) => {
                if (e.key === "Enter") {
                  SearchGetAll();
                }
              }}
            />
          
            <button
              type="button"
                className="btn btn-primary ms-lg-3 ms-md-3 ms-3"
              onClick={() => SearchGetAll()}
            >
              Search
            </button>
            <a
              href="#"
              className="ms-lg-3 ms-md-3 text-white"
              id="button-item"
              onClick={handleResetClick}
              type="button"
            >
              Reset
            </a>
            </div>
            <div className="d-lg-flex d-md-flex d-block mt-lg-0 mt-md-0 mt-sm-2 mt-2">
      
              {profile && profile.contactType === "staff" ? (
                <></>
              ) : (
                <a
                    className="btn btn-info ms-lg-3 ms-md-3 mt-lg-0 mt-md-0 mt-sm-2 mt-2"
                  onClick={AddTransaction}
                >
                  + New Transaction
                </a>
              )}
            </div>
          </div>
          {/* <!-- Content--> */}
     
          <div className="">
            <div class="table-responsive bg-light border rounded-3 p-3 mt-3">
              <table
                class="table table-striped align-middle mb-0 w-100"
                border="0"
                cellspacing="0"
                cellpadding="0"
              >
                <thead>
                  <tr>
                    <th>ADDRESS</th>
                    <th>
                      <div class="dropdown">
                        <button type="button" class="bg-light border-0 text-dark dropdown-toggle" data-bs-toggle="dropdown">
                          SIDE
                        </button>
                     
                        <ul class="dropdown-menu">
                          <label className="form-label ms-3 mb-0">By Category</label>
                          <li><a class="dropdown-item" onClick={() => handleCategory('buyer')}>Buyer</a></li>
                          <li><a class="dropdown-item" onClick={() => handleCategory('seller')}>Seller</a></li>
                          <li><a class="dropdown-item" onClick={() => handleCategory('both')}>Buyer & Seller</a></li>
                          <li><a class="dropdown-item" onClick={() => handleCategory('referral')}>Referral</a></li>

                        </ul>
                      </div>
                    </th>
                    <th>CLIENT</th>
                    <th>AGENT</th>
                    <th>
                      <div class="dropdown">
                        <button type="button" class="bg-light border-0 text-dark dropdown-toggle" data-bs-toggle="dropdown">
                          STATUS
                        </button>
                        <ul class="dropdown-menu">
                          <label className="form-label ms-3 mb-0">By Phase</label>
                          <li><a class="dropdown-item"onClick={() => handlePhase('pre-listed')}>Pre-Listed</a></li>
                          <li><a class="dropdown-item"onClick={() => handlePhase('active-listing')}>Active Listing</a></li>
                          <li><a class="dropdown-item"onClick={() => handlePhase('showing-homes')}>Showing homes</a></li>
                          <li><a class="dropdown-item"onClick={() => handlePhase('under-contract')}>Under Contract</a></li>
                          <li><a class="dropdown-item"onClick={() => handlePhase('closed')}>Closed</a></li>
                          <li><a class="dropdown-item"onClick={() => handlePhase('canceled')}>Canceled</a></li>
                        </ul>
                      </div>
                    </th>

                    <th>TRANSACTION REVIEW</th>
                    {localStorage.getItem("auth") &&
                    jwt(localStorage.getItem("auth")).contactType == "admin" ? (
                      <th>ACTION</th>
                    ) : (
                      ""
                    )}
                  </tr>
                </thead>
                <tbody>
                  {activeGet && activeGet.length > 0 ? (
                    activeGet.map((post) => (
                      <tr key={post._id}>
                        {post.propertyDetail ? (
                          <>
                            <td>
                              <div className="d-flex align-items-center justify-content-start" style={{ width: "250px"}}
                               onClick={(e) =>
                                Catalog(post._id, post.agentId)
                              }>
                                <img
                                  style={{ width: "65px", height: "65px" }}
                                  src={
                                    post.propertyDetail?.image &&
                                    post.propertyDetail.image !== "image"
                                      ? post.propertyDetail.image
                                      : defaultpropertyimage
                                  }
                                  alt="Property"
                                  onError={(e) => propertypic(e)}
                                />
                                <span className="ms-2">
                                  {post.propertyDetail.streetAddress}
                                </span>
                              </div>
                            </td>
                            <td>
                              {post.represent === "seller"
                                ? "Seller"
                                : post.represent === "buyer"
                                ? "Buyer"
                                : post.represent === "both"
                                ? "Buyer & Seller"
                                : post.represent === "referral"
                                ? "Referral"
                                : "N/A"}
                            </td>

                            <td>
                              {post.contact1 &&
                              post.contact1.length > 0 &&
                              post.contact1[0] ? (
                                <div
                                onClick={() =>
                                  handleSubmit(post.contact1[0].data._id, post.contact1[0].data.contactType)
                                }>
                                  {post.contact1[0].data.organization
                                    ? post.contact1[0].data.organization
                                    : post.contact1[0].data.firstName}
                                  &nbsp;{post.contact1[0].data.lastName}
                                </div>
                              ) : (
                                "N/A"
                              )}
                            
                            </td>

                            <td>
                              {post.contact3 &&
                              post.contact3.length > 0 &&
                              post.contact3[0] ? (
                                <>
                                  {post.contact3[0].data.organization
                                    ? post.contact3[0].data.organization
                                    : post.contact3[0].data.firstName}
                                  &nbsp;{post.contact3[0].data.lastName}
                                </>
                              ) : (
                                "N/A"
                              )}
                            </td>

                            <td>
                              {post.phase ? (
                                <span className="badge bg-success">
                                  {post.phase === "pre-listed"
                                    ? "Pre-Listed"
                                    : post.phase === "active-listing"
                                    ? "Active Listing"
                                    : post.phase === "showing-homes"
                                    ? "Showing Homes"
                                    : post.phase === "under-contract"
                                    ? "Under Contract"
                                    : post.phase === "closed"
                                    ? "Closed"
                                    : post.phase === "canceled"
                                    ? "Canceled"
                                    : ""}
                                </span>
                              ) : (
                                "N/A"
                              )}
                            </td>
                            
                            <td>
                              <div className="btn btn-secondary btn-sm">
                                {post.trans_status === "submitted"
                                  ? "Submitted"
                                  : post.trans_status === "approve"
                                  ? "In Review Approved"
                                  : post.trans_status === "confirmed"
                                  ? "In Review Confirmed"
                                  : post.trans_status === "funding_complete"
                                  ? "Funding Complete"
                                  : post.trans_status === "filed"
                                  ? "Filed"
                                  : "Active"}
                              </div>
                            </td>

                            <td>
                              {localStorage.getItem("auth") &&
                              jwt(localStorage.getItem("auth")).contactType ==
                                "admin" ? (
                                <div>
                                  <button
                                    onClick={(e) =>
                                      handleDeleteTransactionpermanent(post._id)
                                    }
                                    className="btn btn-secondary"
                                  >
                                    <i
                                      className="fa fa-trash-o me-2"
                                      aria-hidden="true"
                                    ></i>
                                    Delete
                                  </button>
                                </div>
                              ) : (
                                ""
                              )}
                            </td>
                          </>
                        ) : (
                          <>
                            <td>
                              <img
                                  style={{ width: "65px", height: "65px" }}
                                onClick={(e) => Catalog(post._id, post.agentId)}
                                src="https://integra.workspace.lwolf.com/common/handler/mls/photo_custom.ashx?s=1&t=5eada3e3-491e-46e2-938c-7c785a1f9c68&r=Seller"
                                alt="Property"
                                onError={(e) => propertypic(e)}
                              />
                              Property Not Set
                            </td>

                            <td>
                              {post.represent === "seller"
                                ? "Seller"
                                : post.represent === "buyer"
                                ? "Buyer"
                                : post.represent === "both"
                                ? "Buyer & Seller"
                                : post.represent === "referral"
                                ? "Referral"
                                : "N/A"}
                            </td>

                            <td>
                              <div 
                              onClick={() =>
                                handleSubmit(post._id, post.contactType)
                              }>
                              {post.contact1 &&
                              post.contact1.length > 0 &&
                              post.contact1[0] ? (
                                <>
                                  {post.contact1[0].data.organization
                                    ? post.contact1[0].data.organization
                                    : post.contact1[0].data.firstName}
                                  &nbsp;{post.contact1[0].data.lastName}
                                </>
                              ) : (
                                "N/A"
                              )}
                              </div>
                            </td>

                            <td>
                              {post.contact3 &&
                              post.contact3.length > 0 &&
                              post.contact3[0] ? (
                                <>
                                  {post.contact3[0].data.organization
                                    ? post.contact3[0].data.organization
                                    : post.contact3[0].data.firstName}
                                  &nbsp;{post.contact3[0].data.lastName}
                                </>
                              ) : (
                                "N/A"
                              )}
                            </td>

                            <td>
                              {post.phase ? (
                                <span className="badge bg-success">
                                  {post.phase === "pre-listed"
                                    ? "Pre-Listed"
                                    : post.phase === "active-listing"
                                    ? "Active Listing"
                                    : post.phase === "showing-homes"
                                    ? "Showing Homes"
                                    : post.phase === "under-contract"
                                    ? "Under Contract"
                                    : post.phase === "closed"
                                    ? "Closed"
                                    : post.phase === "canceled"
                                    ? "Canceled"
                                    : ""}
                                </span>
                              ) : (
                                "N/A"
                              )}
                            </td>

                            <td>
                              <div className="btn btn-secondary btn-sm">
                                {post.trans_status === "submitted"
                                  ? "Submitted"
                                  : post.trans_status === "approve"
                                  ? "In Review Approved"
                                  : post.trans_status === "confirmed"
                                  ? "In Review Confirmed"
                                  : post.trans_status === "funding_complete"
                                  ? "Funding Complete"
                                  : post.trans_status === "filed"
                                  ? "Filed"
                                  : "Active"}
                              </div>
                            </td>
                            <td>
                              {localStorage.getItem("auth") &&
                              jwt(localStorage.getItem("auth")).contactType ==
                                "admin" ? (
                                <div>
                                  <button
                                    onClick={(e) =>
                                      handleDeleteTransactionpermanent(post._id)
                                    }
                                    className="btn btn-secondary"
                                  >
                                    <i
                                      className="fa fa-trash-o me-2"
                                      aria-hidden="true"
                                    ></i>
                                    Delete
                                  </button>
                                </div>
                              ) : (
                                ""
                              )}
                            </td>
                          </>
                        )}
                      </tr>
                    ))
                  ) : (
                    <tr>
                      <td colSpan="7" className="text-center">
                        <div className="mt-5 float-start w-100">
                          <i
                            className="fa fa-calculator mb-5"
                            aria-hidden="true"
                            style={{ fontSize: "135px" }}
                          ></i>
                          <br />
                          <span className="text-dark">
                            No Active Transactions
                          </span>
                        </div>
                      </td>
                    </tr>
                  )}
                </tbody>
              </table>
            </div>
            <div className="justify-content-end mb-1 mt-4">
              <ReactPaginate
                previousLabel={"Previous"}
                nextLabel={"Next"}
                breakLabel={"..."}
                pageCount={pageCount}
                marginPagesDisplayed={1}
                pageRangeDisplayed={2}
                onPageChange={handlePageClick}
                containerClassName={"pagination justify-content-center"}
                pageClassName={"page-item"}
                pageLinkClassName={"page-link"}
                previousClassName={"page-item"}
                previousLinkClassName={"page-link"}
                nextClassName={"page-item"}
                nextLinkClassName={"page-link"}
                breakClassName={"page-item"}
                breakLinkClassName={"page-link"}
                activeClassName={"active"}
              />
            </div>

            {/* <div className="col-lg-4 col-md-4 col-xl-3 mt-lg-0 mt-md-0 mt-sm-4 mt-4 order-lg-2 order-md-2 order-sm-1 order-1">
              <div className="" id="filters-sidebar">
                <h6 className="text-white">Filter Transactions</h6>
                <div className="offcanvas-body">
                  <label className="text-white form-label fs-sm mt-1">
                    By Category
                  </label>
                  <select
                    className="form-select mb-2"
                    name="represent"
                    onChange={(event) => setRepresent(event.target.value)}
                    value={represent}
                  >
                    <option value="" disabled="">
                      Choose Category
                    </option>
                    <option value="buyer">Buyer</option>
                    <option value="seller">Seller</option>
                    <option value="both">Buyer & Seller</option>
                    <option value="referral">Referral</option>
                  </select>

                  <label className="text-white form-label fs-sm mt-1">
                    By Type
                  </label>
                  <select
                    className="form-select mb-2"
                    name="type"
                    onChange={(event) => setType(event.target.value)}
                    value={type}
                  >
                    <option value="" disabled="">
                      Choose Type
                    </option>
                    <option value="buy_residential">Buyer Residential</option>
                    <option value="buy_commercial">Buyer Commercial</option>
                    <option value="buy_investment">Buyer Investment</option>
                    <option value="buy_land"> Buyer Land</option>
                    <option value="buy_referral">Buyer Referral</option>
                    <option value="buy_lease">Buyer Lease</option>
                    <option value="sale_residential">Seller Residential</option>
                    <option value="sale_commercial">Seller Commercial</option>
                    <option value="sale_investment">Seller Investment</option>
                    <option value="sale_mobile">Seller Mobile</option>
                    <option value="sale_land">Seller Land</option>
                    <option value="sale_referral">Seller Referral</option>
                    <option value="sale_lease">Seller Lease</option>
                    <option value="both_residential">
                      Buyer & Seller Residential
                    </option>
                    <option value="both_commercial">
                      Buyer & Seller Commercial
                    </option>
                    <option value="both_investment">
                      Buyer & Seller Investment
                    </option>
                    <option value="both_land">Buyer & Seller Land</option>
                    <option value="both_referral">
                      Buyer & Seller Referral
                    </option>
                    <option value="both_lease">Buyer & Seller Lease</option>
                    <option value="referral_residential">
                      Residential Referral
                    </option>
                    <option value="referral_commercial">
                      Commercial Referral
                    </option>
                    <option value="referral_investment">
                      Investment Referral
                    </option>
                    <option value="referral_land">Land Referral</option>
                    <option value="referral_mobile">Mobile Referral</option>
                  </select>

                  <label className="text-white form-label fs-sm mt-1">
                    By Phase
                  </label>
                  <select
                    className="form-select mb-2"
                    name="phase"
                    onChange={(event) => setPhase(event.target.value)}
                    value={phase}
                  >
                    <option value="" disabled="">
                      Choose Phase{" "}
                    </option>
                    <option value="pre-listed">Pre-Listed</option>
                    <option value="active-listing">Active Listing</option>
                    <option value="showing-homes">Showing homes</option>
                    <option value="under-contract">Under Contract</option>
                    <option value="closed">Closed</option>
                    <option value="canceled">Canceled</option>
                  </select>

                  <label className="text-white form-label fs-sm mt-1">
                    By Review
                  </label>
                  <select
                    className="form-select mb-2"
                    name="filing"
                    onChange={(event) => setFiling(event.target.value)}
                    value={filing}
                  >
                    <option value="" disabled="">
                      Choose
                    </option>
                    <option value="filed_complted">Filing</option>
                  </select>
                </div>
              </div>
            </div>  */}
          </div>
        </div>
      </main>
    </div>
  );
}

export default MyTransaction;

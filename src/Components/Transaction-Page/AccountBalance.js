import React, { useState, useEffect } from "react";
import _ from 'lodash';
import Loader from "../../Pages/Loader/Loader.js";
import Toaster from "../../Pages/Toaster/Toaster.js";
import { NavLink, useParams } from 'react-router-dom';
import user_service from "../service/user_service";
import moment from "moment-timezone";


function AccountBalance() {
    const params = useParams();
    console.log(params.id);

    const [loader, setLoader] = useState({ isActive: null })
    const { isActive } = loader;
    const [toaster, setToaster] = useState({ types: null, isShow: null, toasterBody: "", message: "" });
    const { types, isShow, toasterBody, message } = toaster;
    const [balance, setBalance] = useState([])

    const [totalsaving, setTotalSaving] = useState(0);

    useEffect(() => { window.scrollTo(0, 0);
        AgentAccountingGet()
    }, [])




    const AgentAccountingGet = async () => {
        await user_service.agentAccountingGet().then((response) => {
            if (response) {
                setBalance(response.data)
                let creditTotal = 0;
                let feeTotal = 0;
                let fineTotal = 0;
                let refundTotal = 0;
                let purchaseTotal = 0;
                let savingTotal = 0;

                response.data.data.forEach((item) => {
                    if (item.entry_type === "Credit") {
                        creditTotal += parseInt(item.amount);
                        // savingTotal += parseInt(item.amount);
                    } else if (item.entry_type === "Fee") {
                        feeTotal += parseInt(item.amount);
                    } else if (item.entry_type === "Fine") {
                        fineTotal += parseInt(item.amount);
                    } else if (item.entry_type === "Refund") {
                        refundTotal += parseInt(item.amount);
                    } else if (item.entry_type === "Purchase") {
                        purchaseTotal += parseInt(item.amount);
                    }

                })
                setTotalSaving(creditTotal +refundTotal - feeTotal - fineTotal- purchaseTotal);
            }
        })

    }


    const handleRemove = async (id) => {
        console.log(id);
        setLoader({ isActive: true })
        await user_service.agentAccountingDelete(id).then((response) => {
            if (response) {
                console.log(response);
                setLoader({ isActive: false })
                const AgentAccountingGet = async () => {
                    await user_service.agentAccountingGet().then((response) => {
                        if (response) {
                            setBalance(response.data)
                        }
                    });
                }
                AgentAccountingGet()
            }
        });
    }
    return (
        <div className="bg-secondary float-left w-100 pt-4 mb-4">
            <Loader isActive={isActive} />
            {isShow && <Toaster types={types} isShow={isShow} toasterBody={toasterBody} message={message} />}
            <div className="">
                <div className="">
                    <div className="">
                        <div className="row">
                            <div className="col-md-8">
                                <div className="add_listing border rounded-3 w-100 p-3">
                                    <div className="">
                                        <p>The following fields are available to help keep track of accounting details for the associate account. Positive values reflect an account credit, negative values reflect a balance due.</p>
                                        <div className="table-responsive">
                                        {
                                            balance ?
                                                <table id="DepositLedger" className="table table-striped" width="100%" border="0" cellSpacing="0" cellPadding="0">
                                                    <thead>
                                                        <tr>
                                                            <th>Category</th>
                                                            <th>Memo</th>
                                                            <th className="hright">Amount</th>
                                                            <th className="hright">Date Entered</th>
                                                            <th className="">Action</th>
                                                            {/* <th className="d-none">Remove</th>
                                                            <th className="d-none">Edit</th> */}
                                                            
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        {
                                                            balance.data ?
                                                                balance.data.length > 0 ?
                                                                    balance.data.map((item) => {
                                                                        return (
                                                                            <tr>
                                                                                <td><span className="SuppressText">{item.entry_type}</span></td>
                                                                                <td><span className="SuppressText">{item.memo}</span></td>
                                                                                <td><span className="SuppressText">${item.amount}</span></td>
                                                                                <td><span className="SuppressText">{moment(item.entry_date).format('M/D/YYYY')}</span></td>
                                                                                <td><span className="pull-left SuppressText">
                                                                                    <img
                                                                                        onClick={(e) => handleRemove(item._id)}
                                                                                        className="pull-right"
                                                                                        src="https://cdn.pboffice.net/shared/transaction/page_select_drop.png"
                                                                                    /></span>
                                                                                    <NavLink className="pull-left ms-3" to={`/add-balance/${params.id}/${item._id}`}><i className="fa fa-pencil" aria-hidden="true"></i>
                                                                                    </NavLink>
                                                                                </td>
                                                                                {/* <td className="pull-left">
                                                                                    <NavLink className="pull-left" to={`/add-balance/${params.id}/${item._id}`}><i className="fa fa-pencil" aria-hidden="true"></i>
                                                                                    </NavLink>
                                                                                </td> */}
                                                                            </tr>
                                                                        )
                                                                    })
                                                                    :
                                                                    ""
                                                                :
                                                                ""
                                                        }
                                                    </tbody>
                                                </table>
                                                :
                                                <p>No billing entry found.</p>

                                        }
                                        </div>

                                        <div className="mt-3">
                                            <div className="d-flex justify-content-start align-items-center">
                                                <i className="fa fa-calendar me-2" aria-hidden="true"></i><label className="col-form-label mb-0">Balance Last Adjusted</label>&nbsp;&nbsp;<p className="mb-0">{balance.data ? balance.data.length > 0 ? moment(balance.data[balance.data.length - 1].entry_date).format('M/D/YYYY') : "" : ""}</p>
                                            </div>
                                            <div className="d-flex justify-content-start align-items-center">
                                                <i className="fa fa-usd me-2" aria-hidden="true"></i><label className="col-form-label mb-0">Account Balance</label>&nbsp;&nbsp;<p className="mb-0">(${totalsaving})</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="float-left w-100 pull-right mt-4">
                                <NavLink to={`/add-balance/${params.id}`} className="btn btn-primary pull-right" type="button">Add Balance</NavLink>
                                </div>
                            </div>
                            <div className="col-md-4"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}
export default AccountBalance;
import React, { useState, useMemo, useEffect } from "react";
import _, { startCase } from "lodash";
import user_service from "../service/user_service";
import jwt from "jwt-decode";
import Loader from "../../Pages/Loader/Loader.js";
import Toaster from "../../Pages/Toaster/Toaster.js";
import { useNavigate, NavLink, useParams } from "react-router-dom";
import avtar from "../img/avtar.jpg";
import ReactPaginate from "react-paginate";
import countryList from "country-list";
import defaultpropertyimage from "../img/defaultpropertyimage.jpeg";

function NewTransaction() {
  const initialValues = {
    firstName: "",
    lastName: "",
    company: "",
    email: "",
    phone: "",
    agentId: "",
  };
  const [formValues, setFormValues] = useState(initialValues);

  const initialValuesNew = {
    represent: "",
    type: "",
    ownership: "",
    phase: "",
    contact: "",
    agentId: "",
  };

  const [formValuesNew, setFormValuesNew] = useState(initialValuesNew);

  const [formErrors, setFormErrors] = useState({});
  const [isSubmitClick, setISSubmitClick] = useState(false);
  const [summary, setSummary] = useState([]);
  const [arrayData, setArrayData] = useState([]);
  const [select, setSelect] = useState("");
  const [clientId, setClientId] = useState("");
  const [represent, setRepresent] = useState("");
  const [transactionOptions, setTransactionOptions] = useState([]);
  const [ownerships, setOwnerShips] = useState("ownership");
  const [contact, setContact] = useState("");
  const [propertyId, setPropertyId] = useState("");

  const [checkBox, setSetCheckbox] = useState(false);
  const [results, setResults] = useState([]);
  const [pageCount, setPageCount] = useState(0);
  const [currentPage, setCurrentPage] = useState(1);
  const [isLoading, setIsLoading] = useState(false);
  const [showPopup, setShowPopup] = useState(false);
  const [clientdone, setClientdone] = useState(false);

  const [loader, setLoader] = useState({ isActive: null });
  const { isActive } = loader;
  const [toaster, setToaster] = useState({
    types: null,
    isShow: null,
    toasterBody: "",
    message: "",
  });
  const { types, isShow, toasterBody, message } = toaster;

  const navigate = useNavigate();
  const params = useParams();

  const handleCheck = () => {
    setSetCheckbox(checkBox ? true : false);
  };

  useEffect(() => {
    window.scrollTo(0, 0);
    // TransactionGetById(params.id);
    ContactGetById(jwt(localStorage.getItem("auth")).id);
  }, []);

  // onChange Function start
  const handleOwner = (e) => {
    const { name, value } = e.target;
    setOwnerShips({ ...ownerships, [name]: value });
  };

  const handleChanges = (e) => {
    const { name, value } = e.target;
    setFormValuesNew({ ...formValuesNew, [name]: value });
  };

  // onChange Function start
  const handleChange = (e) => {
    const { name, value } = e.target;
    setFormValues({ ...formValues, [name]: value });
  };

  // onChange Function end

  {
    /* <!-- Form Validation Start--> */
  }
  useEffect(() => {
    if (formValues && isSubmitClick) {
      validate();
    }
  }, [formValues]);

  const validate = () => {
    const values = formValues;
    const errors = {};
    if (!values.firstName) {
      errors.firstName = "name is required";
    }

    if (!values.lastName) {
      errors.lastName = "lastName is required";
    }

    if (!values.email) {
      errors.email = "Email is required";
    } else if (
      !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i.test(values.email)
    ) {
      errors.email = "Invalid email address";
    }

    // if (!values.email) {
    //   errors.email = "email is required";
    // } else if (
    //   !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.email)
    // ) {
    //   errors.email = "Invalid email address";
    // }

    setFormErrors(errors);
    return errors;
  };
  {
    /* <!-- Form Validation End--> */
  }

  {
    /* <!--Api call Form onSubmit Start--> */
  }
  const handleNewContact = async (e, id) => {
    e.preventDefault();
    setISSubmitClick(true);
    let checkValue = validate();
    if (_.isEmpty(checkValue)) {
      const userData = {
        firstName: formValues.firstName,
        lastName: formValues.lastName,
        company: formValues.company,
        email: formValues.email,
        phone: formValues.phone,
        agentId: jwt(localStorage.getItem("auth")).id,
      };
      // console.log(userData)
      try {
        setLoader({ isActive: true });
        const response = await user_service.contact(
          userData,
          formValues.firstName,
          id
        );
        const contactData = response.data;
        setSummary((prevSummary) => [...prevSummary, contactData]);

        setArrayData((prevArrayData) => [...prevArrayData, contactData._id]);

        setClientId(id);
        setSelect("");
        setLoader({ isActive: false });
        setToaster({
          type: "Contact Successfully",
          isShow: true,
          toasterBody: response.data.message,
          message: "Contact Successfully",
        });
        document.getElementById("closeModal").click();
      } catch (error) {
        setLoader({ isActive: false });
        setToaster({
          types: "error",
          isShow: true,
          toasterBody: error,
          message: "Error",
        });
      }
      setTimeout(() => {
        setToaster({
          types: "error",
          isShow: false,
          toasterBody: null,
          message: "Error",
        });
      }, 2000);
    }
  };
  {
    /* <!-- Api call Form onSubmit End--> */
  }

  const selectContact = async (id) => {
    try {
      const response = await user_service.contactGetById(id);
      const contactData = response.data;
      //console.log(contactData)
      setSummary((prevSummary) => [...prevSummary, contactData]);
      setArrayData((prevArrayData) => [...prevArrayData, contactData._id]);
      setClientId(id);
      setSelect("");
      setClientdone(true);
      document.getElementById("closeModal").click();
    } catch (error) {
      //console.log(error);
    }
  };

  {
    /* <!-- paginate Function Api call Start--> */
  }
  useEffect(() => {
    if (formValues) {
      SearchGetAll();
    }
  }, [formValues]);

  const SearchGetAll = async () => {
    await user_service
      .SearchContactGet(1, formValues.firstName)
      .then((response) => {
        if (response) {
          setResults(response.data.data);
          setPageCount(Math.ceil(response.data.totalRecords / 10));
        }
      });
  };

  const handlePageClick = async (data) => {
    let currentPage = data.selected + 1;
    let skip = currentPage - 1 + currentPage;
    setLoader({ isActive: true });
    await user_service
      .SearchContactGet(currentPage, formValues.firstName)
      .then((response) => {
        setLoader({ isActive: false });
        if (response) {
          // setSummary(response.data);
          setResults(response.data.data);
          setPageCount(Math.ceil(response.data.totalRecords / 10));
        }
      });
  };

  const handleSearchTransaction = async (id) => {
    await user_service.contactGetById(id).then((response) => {
      setClientId(id);
      setSelect(response.data);
    });
  };

  const handleSearch = (event) => {
    event.preventDefault();
    setCurrentPage(1);
  };

  const cancel = () => {
    setSelect("");
  };

  const handleCancel = (id) => {
    setShowPopup(false);
    setRepresent("");
  };

  const handleDelete = (e, id) => {
    if (represent) {
      console.log(`Deleting post with represent ID: ${represent}`);
      setShowPopup(false);
      setRepresent("");
      setSelect("");
      setSummary([]);
      setClientId("");
      setContact("");
      setClientdone(false);
    }
  };

  const AddTransaction = async (e) => {
    e.preventDefault();
    const contacts = arrayData.map((contactId) => ({
      type: formValuesNew.represent,
      contactId: contactId,
    }));
    console.log(contacts);
    const contacts3 = arrayData.map((contactId) => ({
      type: formValuesNew.represent + "_Agent",
      contactId: jwt(localStorage.getItem("auth")).id,
    }));

    const userData = {
      agentId: jwt(localStorage.getItem("auth")).id,
      office_id: localStorage.getItem("active_office_id"),
      office_name: localStorage.getItem("active_office"),
      type: formValuesNew.type,
      phase: "start",
      category: formValuesNew.category,
      contact1: contacts,
      ownership: formValuesNew.ownership,
      contact3: contacts3,
      represent: formValuesNew.represent,
      propertyDetail: propertyId,
    };

    console.log(userData);

    // try {
    //   setLoader({ isActive: true });
    //   const response = await user_service.TransactionCreate(userData);
    //   if (response) {
    //     // console.log(response.data._id)
    //     setLoader({ isActive: false });
    //     setToaster({
    //       type: "Transaction Successfully",
    //       isShow: true,
    //       toasterBody: response.data.message,
    //       message: "Transaction Successfully",
    //     });
    //     setTimeout(() => {
    //       navigate(`/transaction-summary/${response.data._id}`);
    //     }, 500);
    //   } else {
    //     setLoader({ isActive: false });
    //     setToaster({
    //       types: "error",
    //       isShow: true,
    //       toasterBody: response.data.message,
    //       message: "Error",
    //     });
    //   }
    // } catch (error) {
    //   setLoader({ isActive: false });
    //   setToaster({
    //     types: "error",
    //     isShow: true,
    //     toasterBody: error,
    //     message: "Error",
    //   });
    // }
    // setTimeout(() => {
    //   setToaster({
    //     types: "error",
    //     isShow: false,
    //     toasterBody: null,
    //     message: "Error",
    //   });
    // }, 2000);
  };

  const [selectedValuetype, setSelectedValuetype] = useState("");
  const handleChangeNew = (e) => {
    const selectedValue = e.target.value;
    setSelectedValuetype(selectedValue);
    const [type, category] = selectedValue.split("_");

    setFormValuesNew((prevValues) => ({
      ...prevValues,
      type,
      category,
    }));
  };

  const TransactionStep = (stepno) => {
    if (stepno) {
      console.log(stepno);
      setContact(stepno);
    }
  };

  // select propery section code start

  const initialValuesproperty = {
    listingsAgent: "",
    category: "",
    mlsId: "",
    propertyType: "",
    mlsNumber: "",
    agentId: "",
    streetDirection: "North",
    associateName: "",
    price: "",
    garage: "",
    bed: "",
    subDivision: "",
    streetAddress: "",
    city: "",
    state: "Utah",
    zipCode: "",
    schoolDistrict: "",
    country: "",
    areaLocation: "",
    status: "",
    listDate: "",
    image: "",
  };

  // const initialValues = {agentId:"", mlsNumber: "", areaLocation: "", streetAddress: "", state: "Utah", mlsId: "", city: "", zipCode: "", price: "", price: "", };
  const [formValuesproperty, setFormValuesproperty] = useState(
    initialValuesproperty
  );
  const [formErrorsproperty, setFormErrorsproperty] = useState({});
  const [isSubmitClickproperty, setISSubmitClickproperty] = useState(false);
  const [step1, setStep1] = useState("");
  const [category, setCategory] = useState("");
  const [step2, setStep2] = useState("");
  const [searchby, setSearchby] = useState("mls");
  const [membership, setMembership] = useState("");

  const options = useMemo(() => {
    const countries = countryList.getData();
    const utahState = {
      code: "UT",
      name: "Utah",
      districts: [
        "Alabama",
        "Alaska",
        "Arizona",
        "Arkansas",
        "California",
        "Colorado",
        "Connecticut",
        "Delaware",
        "District of Columbia",
        "Florida",
        "Georgia",
        "Hawaii",
        "Idaho",
        "Illinois",
        "Indiana",
        "Iowa",
        "Kansas",
        "Kentucky",
        "Louisiana",
        "Maine",
        "Maryland",
        "Massachusetts",
        "Michigan",
        "Minnesota",
        "Mississippi",
        "Missouri",
        "Montana",
        "Nebraska",
        "Nevada",
        "New Hampshire",
        "New Jersey",
        "New Mexico",
        "New York",
        "North Carolina",
        "North Dakota",
        "Ohio",
        "Oklahoma",
        "Oregon",
        "Pennsylvania",
        "Rhode Island",
        "South Carolina",
        "South Dakota",
        "Tennessee",
        "Texas",
        "Utah",
        "Vermont",
        "Virginia",
        "Washington",
        "West Virginia",
        "Wisconsin",
        "Wyoming",
      ],
    };
    return [...countries, utahState];
  }, []);

  // const [loader, setLoader] = useState({ isActive: null })
  // const { isActive } = loader;
  // const [toaster, setToaster] = useState({ types: null, isShow: null, toasterBody: "", message: "" });
  // const { types, isShow, toasterBody, message } = toaster;

  const [search_value, setSearchValue] = useState({ associateName: "" });
  const [searchmlsnumber, setSearchmlsnumber] = useState({ mlsnumber: "" });
  const [selectproperty, setSelectproperty] = useState("");
  const [transactionId, setTransactionId] = useState("");
  const [result, setResult] = useState([]);
  // const [isLoading, setIsLoading] = useState(false);
  const [pageCountproperty, setPageCountproperty] = useState(0);
  const [array, setArray] = useState("");

  // const params = useParams();
  const AddBasic = (stepno, value) => {
    if (stepno === "1") {
      setStep1(stepno);
      setCategory(value);
      //console.log(stepno)
      //console.log(value)
    }
    if (stepno === "2") {
      setStep2(stepno);
      setCategory(value);
      //console.log(stepno)
      //console.log(value)
    }
  };
  {
    /* <!-- Input onChange Start--> */
  }
  const handleChangeproperty = (e) => {
    const { name, value } = e.target;
    setFormValuesproperty({ ...formValuesproperty, [name]: value });
  };

  const handleChangesearch = (event) => {
    setSearchValue({ associateName: event.target.value });
  };

  const handleChangesearchmls = (event) => {
    setSearchmlsnumber({ mlsnumber: event.target.value });
  };

  {
    /* <!-- Input onChange End--> */
  }

  {
    /* <!-- Form Validation Start--> */
  }
  useEffect(() => {
    if (formValuesproperty && isSubmitClickproperty) {
      validate();
    }
  }, [formValuesproperty]);

  const validateproperty = () => {
    const values = formValuesproperty;
    const errorsproperty = {};
    if (!values.mlsNumber) {
      errorsproperty.mlsNumber = "streetNumber is required";
    }

    if (!values.streetDirection) {
      errorsproperty.streetDirection = "steeetDirection is required";
    }
    if (!values.streetAddress) {
      errorsproperty.streetAddress = "streetName is required";
    }

    if (!values.mlsId) {
      errorsproperty.mlsId = "unitNumber is required";
    }
    if (!values.city) {
      errorsproperty.city = "city is required";
    }
    if (!values.state) {
      errorsproperty.state = "state is required";
    }

    if (!values.zipCode) {
      errorsproperty.zipCode = "postalCode is required";
    }
    // if (!values.price) {
    //     errors.price = "PriceSale is required";
    // }

    if (!values.priceLease) {
      errorsproperty.priceLease = "PriceLease is required";
    }
    setFormErrorsproperty(errorsproperty);
    return errorsproperty;
  };
  {
    /* <!-- Form Validation End--> */
  }

  {
    /* <!-- Form onSubmit Start--> */
  }
  const handleSubmit = async (e) => {
    e.preventDefault();
    setISSubmitClickproperty(true);
    let checkValueproperty = validateproperty();
    if (_.isEmpty(checkValueproperty)) {
      const currentDate = new Date();
      const formattedDate = currentDate.toISOString();

      const userData = {
        agentId: jwt(localStorage.getItem("auth")).id,
        listingsAgent: "Shawn",
        category: "residential",
        mlsId: formValuesproperty.mlsId,
        propertyType: "Commercial",
        mlsNumber: formValuesproperty.mlsNumber,
        associateName: "Integra Reality",
        price: formValuesproperty.price,
        priceLease: formValuesproperty.priceLease,
        garage: "1",
        bed: "5",
        city: formValuesproperty.city,
        state: formValuesproperty.state,
        country: "Utah",
        subDivision: "Utah",
        streetAddress: formValuesproperty.streetAddress,
        zipCode: formValuesproperty.zipCode,
        schoolDistrict: "Salt lake Utah",
        areaLocation: "Utah",
        streetDirection: formValuesproperty.streetDirection,
        status: "Active",
        listDate: formattedDate,
        image: "image",
      };

      try {
        setLoader({ isActive: true });
        const response = await user_service.listingsCreate(userData);
        setLoader({ isActive: false });
        if (response) {
          setLoader({ isActive: false });
          setToaster({
            types: "SelectProperty",
            isShow: true,
            toasterBody: response.data.message,
            message: "Property Selected Successfully",
          });
          setPropertyId(response.data._id);
          setSelect(response.data);
          setTimeout(() => {
            setToaster((prevToaster) => ({
              ...prevToaster,
              isShow: false,
            }));
          }, 500);
        } else {
          setLoader({ isActive: false });
          setToaster({
            types: "error",
            isShow: true,
            toasterBody: response.data.message,
            message: "Error",
          });
        }
      } catch (error) {
        setLoader({ isActive: false });
        setToaster({
          types: "error",
          isShow: true,
          toasterBody: error,
          message: "Error",
        });
      }
      setTimeout(() => {
        setToaster({
          types: "error",
          isShow: false,
          toasterBody: null,
          message: "Error",
        });
      }, 1000);
    }
  };

  const propertySearch = async () => {
    if (search_value.associateName) {
      try {
        setIsLoading(true);

        const response = await user_service.listingSearch(
          1,
          search_value.associateName
        );
        if (response) {
          //console.log(response);
          setResult(response.data.data);
          setPageCount(Math.ceil(response.data.totalRecords / 10));
        }

        setIsLoading(false);
      } catch (error) {
        console.error(error);
        setIsLoading(false);
      }
    }
  };

  const MLSSearch = async () => {
    if (searchmlsnumber.mlsnumber) {
      try {
        setIsLoading(true);
        let response;
        if (membership === "UtahRealEstate.com") {
          console.log(membership);

          response = await user_service.listingSearchMLS(
            searchmlsnumber.mlsnumber
          );
        } else if (membership === "Park City") {
          response = await user_service.listingSearchMLSspark(
            searchmlsnumber.mlsnumber,
            "parkcity"
          );
        } else if (membership === "Washington County") {
          response = await user_service.listingSearchMLSspark(
            searchmlsnumber.mlsnumber,
            "washingtoncounty"
          );
        } else if (membership === "Iron County") {
          response = await user_service.listingSearchMLSspark(
            searchmlsnumber.mlsnumber,
            "ironcountry"
          );
        } else {
          response = await user_service.listingSearchMLS(
            searchmlsnumber.mlsnumber
          );
        }

        if (response) {
          console.log("response");
          console.log(response);
          setResult(response.data);
          setPageCount(1);
        }

        setIsLoading(false);
      } catch (error) {
        console.error(error);
        setIsLoading(false);
      }
    }
  };

  const handlePageClickproperty = async (data) => {
    let currentPage = data.selected + 1;
    let skip = currentPage - 1 + currentPage;
    setLoader({ isActive: true });
    await user_service
      .listingSearch(currentPage, search_value.associateName)
      .then((response) => {
        setLoader({ isActive: false });
        if (response) {
          setResult(response.data.data);
          setPageCount(Math.ceil(response.data.totalRecords / 10));
        }
      });
  };

  const handleSearchListing = async (id) => {
    try {
      if (searchby === "mls") {
        setSelect(id);
      } else {
        const response = await user_service.listingsGetById(id);
        //console.log(response);
        setSelect(response.data);
      }
    } catch (error) {
      console.error(error);
    }
  };

  const handleSearchListingMLS = async (mlsdata) => {
    if (mlsdata.value) {
      const mlspropdata = {
        agentId: jwt(localStorage.getItem("auth")).id,
        listingsAgent: mlsdata.value[0].ListAgentFullName ?? "N/A",
        category: mlsdata.value[0].PropertyType,
        mlsId: mlsdata.value[0].ListingId.toString(),
        propertyType: mlsdata.value[0].PropertyType,
        mlsNumber: mlsdata.value[0].ListingId.toString(),
        associateName: mlsdata.value[0]?.AssociationName
          ? mlsdata.value[0].AssociationName
          : "N/A",
        price: mlsdata.value[0].ListPrice.toString() ?? "N/A",
        priceLease: mlsdata.value[0]?.LeaseAmount
          ? mlsdata.value[0].LeaseAmount.toString()
          : "N/A",
        garage: mlsdata.value[0]?.GarageSpaces
          ? mlsdata.value[0].GarageSpaces.toString()
          : "N/A",
        bed: mlsdata.value[0]?.BedroomsTotal
          ? mlsdata.value[0].BedroomsTotal.toString()
          : "N/A",
        city: mlsdata.value[0]?.City ?? "N/A",
        state: mlsdata.value[0]?.StateOrProvince ?? "N/A",
        country: mlsdata.value[0]?.CountyOrParish ?? "N/A",
        subDivision: mlsdata.value[0]?.SubdivisionName ?? "N/A",
        streetAddress: mlsdata.value[0]?.UnparsedAddress ?? "N/A",
        zipCode: mlsdata.value[0]?.PostalCode
          ? mlsdata.value[0].PostalCode.toString()
          : "N/A",
        schoolDistrict: mlsdata.value[0]?.ElementarySchoolDistrict ?? "N/A",
        areaLocation: mlsdata.value[0]?.MLSAreaMajor ?? "N/A",
        streetDirection: "N/A",
        status: mlsdata.value[0]?.MlsStatus ?? "N/A",
        listDate: mlsdata.value[0]?.OriginalEntryTimestamp ?? new Date(),
        image:
          mlsdata?.media && mlsdata?.media[0]?.MediaURL
            ? mlsdata?.media[0]?.MediaURL
            : "image",
        listing_AgentID: mlsdata.value[0]?.ListAgentKey
          ? mlsdata.value[0].ListAgentKey
          : "",
        propertyFrom: "mls",
        additionaldataMLS: JSON.stringify(mlsdata),
      };

      console.log(mlspropdata);
      console.log("mlspropdata");

      try {
        setLoader({ isActive: true });
        const response = await user_service.listingsCreate(mlspropdata);
        setLoader({ isActive: false });

        if (response) {
          setToaster({
            types: "SelectProperty",
            isShow: true,
            toasterBody: response.data.message,
            message: "Property Selected Successfully",
          });

          setTimeout(() => {
            setToaster((prevToaster) => ({
              ...prevToaster,
              isShow: false,
            }));
          }, 500);
          return response.data; // You can modify this to return the specific data you need
        } else {
          setToaster({
            types: "error",
            isShow: true,
            toasterBody: response.data.message,
            message: "Error",
          });

          // Return an error or null if needed
          return null;
        }
      } catch (error) {
        setLoader({ isActive: false });
        setToaster({
          types: "error",
          isShow: true,
          toasterBody: error,
          message: "Error",
        });

        // Reject the promise with the error
        throw error;
      }
    }
  };

  const handlePropertyUpdate = async () => {
    let resp; // Define a variable to store the response from handleSearchListingMLS
    let propertyId;
    if (searchby === "mls") {
      // Call handleSearchListingMLS and wait for the response
      resp = await handleSearchListingMLS(select);
      setSelect(resp);
      propertyId = resp._id;
      console.log(resp);
      console.log("resp");
    } else {
      propertyId = select._id;
    }
    setPropertyId(propertyId);
    console.log(propertyId);
    // console.log(select);
    // console.log("propertyId");

    // const contacts = transactionId?.contact1.map((post) => ({
    //     type: post.data.type,
    //     contactId: post.data._id
    // }));
    // try {
    //     const userData = {
    //         agentId: jwt(localStorage.getItem('auth')).id,
    //         type: transactionId.type,
    //         phase: transactionId.phase,
    //         represent: transactionId.represent,
    //         contact1: contacts,
    //         contact2: [],
    //         contact3: [],
    //         contact4: [],
    //         contact5: [],
    //         contact6: [],
    //         propertyDetail: propertyId,
    //         category: transactionId.category,
    //         _id: transactionId._id,
    //     };
    //     setLoader({ isActive: true });
    //     await user_service.TransactionUpdate(params.id, userData).then((response) => {
    //         if (response.status === 200) {
    //             setArray(response.data);
    //             setLoader({ isActive: false });
    //             setToaster({
    //                 types: "Transaction_Updated",
    //                 isShow: true,
    //                 toasterBody: response.data.message,
    //                 message: "Transaction Updated Successfully",
    //             });
    //             navigate(`/t-property/${params.id}`);
    //         } else {
    //             setLoader({ isActive: false });
    //             setToaster({
    //                 types: "error",
    //                 isShow: true,
    //                 toasterBody: response.data.message,
    //                 message: "Error",
    //             });
    //         }
    //     })
    // } catch (error) {
    //     setLoader({ isActive: false });
    //     setToaster({
    //         types: "error",
    //         isShow: true,
    //         toasterBody: error.response ? error.response.data.message : error.message,
    //         message: "Error",
    //     });
    // }
  };

  const changeOption = () => {
    setStep1("");
  };
  const Cancel = () => {
    setStep2("");
    setResult("");
    setSelect("");
    setPropertyId("");
  };

  const CancelProperty = () => {
    setSelect("");
    setPropertyId("");
  };

  const clickHere = () => {
    setStep1("");
  };

  const Searchbyfunction = (e, type) => {
    e.preventDefault();
    setSearchby(type);
  };

  const ListingById = async () => {
    try {
      const response = await user_service.listingsGetById(params.pid);
      setLoader({ isActive: false });

      if (response) {
        setArray(response.data);
        // setAllContacts(response.data.contact1);
      }
    } catch (error) {
      console.error(error);
      setLoader({ isActive: false });
    }
  };

  const [getContact, setGetContact] = useState(initialValues);
  const [mls_membership, setMls_membership] = useState([]);
  const [
    commaSeparatedValuesmls_membership,
    setCommaSeparatedValuesmls_membership,
  ] = useState("");
  const ContactGetById = async () => {
    await user_service
      .contactGetById(jwt(localStorage.getItem("auth")).id)
      .then((response) => {
        if (response) {
          setGetContact(response.data);

          if (response.data && response.data.additionalActivateFields) {
            const mls_membershipString =
              response.data.additionalActivateFields?.mls_membership;
            if (mls_membershipString) {
              // Check if mls_membershipString is a string
              if (typeof mls_membershipString === "string") {
                try {
                  // Attempt to parse the string as JSON
                  const mls_membershipParsed = JSON.parse(mls_membershipString);
                  // Set the parsed value to your state or variable
                  if (mls_membershipParsed == "") {
                    setMls_membership(mls_membership);
                  } else {
                    setMls_membership(mls_membershipParsed);
                  }
                  if (mls_membershipParsed == "") {
                    setCommaSeparatedValuesmls_membership("");
                  } else {
                    const boardMembershipValues = mls_membershipParsed.map(
                      (item) => item.value
                    );
                    //   console.log("boardMembershipValues")
                    //   console.log(boardMembershipValues)
                    setCommaSeparatedValuesmls_membership(
                      // boardMembershipValues.join(", ")
                      boardMembershipValues
                    );
                  }
                } catch (error) {
                  // Handle the error if parsing fails
                  console.error("Error parsing mls_membership as JSON:", error);
                  // You might want to set a default value or handle the error in another way
                  // For debugging, you can log the JSON parse error message:
                  console.error("JSON Parse Error:", error.message);
                  // Set a default value if parsing fails
                  setCommaSeparatedValuesmls_membership(""); // Replace "defaultValue" with your default value
                }
              } else {
                //console.log("sdfdsfdsfsd");
                // Handle the case where mls_membershipString is not a string (e.g., it's already an object)
                // You can directly set it to your state or variable
                setCommaSeparatedValuesmls_membership("");
              }
            } else {
              // Handle the case where mls_membershipString is undefined or null
              // Set a default value or handle it as needed
              setCommaSeparatedValuesmls_membership(""); // Replace "defaultValue" with your default value
            }
          }
        }
      });
  };
  // select property section code end

  const propertypic = (e) => {
    e.target.src = defaultpropertyimage;
  };
  return (
    <div className="bg-secondary float-left w-100 pt-4 mb-4">
      <Loader isActive={isActive} />
      {isShow && (
        <Toaster
          types={types}
          isShow={isShow}
          toasterBody={toasterBody}
          message={message}
        />
      )}
      <main className="page-wrapper profile_page_wrap">
        <div className="content-overlay">
          <div className="">
            <div className="">
              <div className="row">
                <h3 className="text-white mb-4">Add a New Transaction</h3>
                <div className="col-lg-8">
                  <section
                    className="bg-light border rounded-3 p-3"
                    id="basic-info"
                  >
                    <div className="">
                      <div className="col-sm-12 mb-3">
                        <label className="col-form-label">
                          Who do you represent?
                          {/* <span className="text-danger">*</span> */}
                        </label>
                        <select
                          className="form-select"
                          name="represent"
                          value={formValuesNew.represent}
                          onChange={handleChanges}
                        >
                          <option value="">Choose a party</option>
                          <option value="buyer">Buyer</option>
                          <option value="seller">Seller</option>
                          <option value="buyerseller">Buyer & Seller</option>
                          <option value="referral">Referral</option>
                        </select>
                      </div>
                      {formValuesNew.represent ? (
                        <>
                          {formValuesNew.represent === "buyer" ? (
                            <>
                              <div className="col-sm-12 mb-3">
                                <label className="col-form-label">
                                  Select Transaction Type.
                                </label>
                                <select
                                  className="form-select"
                                  name="type"
                                  value={selectedValuetype}
                                  onChange={handleChangeNew}
                                >
                                  <option value="">
                                    Choose a Transaction Type
                                  </option>
                                  <option value="buy_residential">
                                    Buyer Residential
                                  </option>
                                  <option value="buy_commercial">
                                    Buyer Commercial
                                  </option>
                                  <option value="buy_investment">
                                    Buyer Investment
                                  </option>
                                  <option value="buy_land">Buyer Land</option>
                                  <option value="buy_referral">
                                    Buyer Referral
                                  </option>
                                  <option value="buy_lease">Buyer Lease</option>
                                </select>
                              </div>
                            </>
                          ) : (
                            ""
                          )}

                          {formValuesNew.represent === "seller" ? (
                            <>
                              <div className="col-sm-12 mb-3">
                                <label className="col-form-label">
                                  Select Transaction Type.
                                </label>
                                <select
                                  className="form-select"
                                  name="type"
                                  value={selectedValuetype}
                                  onChange={handleChangeNew}
                                >
                                  <option value="">
                                    Choose a Transaction Type
                                  </option>
                                  <option value="sale_residential">
                                    Seller Residential
                                  </option>
                                  <option value="sale_commercial">
                                    Seller Commercial{" "}
                                  </option>
                                  <option value="sale_investment">
                                    Seller Investment
                                  </option>
                                  <option value="sale_land">Seller Land</option>
                                  <option value="sale_mobile">
                                    Seller Mobile
                                  </option>
                                  <option value="sale_referral">
                                    Seller Referral
                                  </option>
                                  <option value="sale_lease">
                                    Seller Lease
                                  </option>
                                </select>
                              </div>
                            </>
                          ) : (
                            ""
                          )}

                          {formValuesNew.represent === "buyerseller" ? (
                            <>
                              <div className="col-sm-12 mb-3">
                                <label className="col-form-label">
                                  Select Transaction Type.
                                </label>
                                <select
                                  className="form-select"
                                  name="type"
                                  value={selectedValuetype}
                                  onChange={handleChangeNew}
                                >
                                  <option value="">
                                    Choose a Transaction Type
                                  </option>
                                  <option value="both_residential">
                                    Buyer & Seller Residential
                                  </option>
                                  <option value="both_commercial">
                                    Buyer & Seller Commercial
                                  </option>
                                  <option value="both_investment">
                                    Buyer & Seller Investment
                                  </option>
                                  <option value="both_land">
                                    Buyer & Seller Land
                                  </option>
                                  <option value="both_referral">
                                    Buyer & Seller Referral
                                  </option>
                                  <option value="both_lease">
                                    Buyer & Seller Lease
                                  </option>
                                </select>
                              </div>
                            </>
                          ) : (
                            ""
                          )}

                          {formValuesNew.represent === "referral" ? (
                            <>
                              <div className="col-sm-12 mb-3">
                                <label className="col-form-label">
                                  Select Transaction Type.
                                </label>
                                <select
                                  className="form-select"
                                  name="type"
                                  value={selectedValuetype}
                                  onChange={handleChangeNew}
                                >
                                  <option value="">
                                    Choose a Transaction Type
                                  </option>
                                  <option value="referral_residential">
                                    Residential Referral
                                  </option>
                                  <option value="referral_commercial">
                                    Commercial Referral
                                  </option>
                                  <option value="referral_investment">
                                    Investment Referral
                                  </option>
                                  <option value="referral_land">
                                    Land Referral
                                  </option>
                                  <option value="referral_mobile">
                                    Mobile Referral
                                  </option>
                                </select>
                              </div>
                            </>
                          ) : (
                            ""
                          )}
                        </>
                      ) : (
                        ""
                      )}

                      {selectedValuetype ? (
                        <>
                          {formValuesNew.represent === "buyer" ? (
                            <>
                              <div className="col-sm-12 mb-3">
                                <label className="col-form-label">
                                  Select Ownership Style.
                                </label>
                                <select
                                  className="form-select"
                                  name="ownership"
                                  value={formValuesNew.ownership}
                                  onChange={handleChanges}
                                >
                                  <option>Choose ownership</option>
                                  <option value="conventional">
                                    Conventional
                                  </option>
                                  <option value="corporation">
                                    Corporation
                                  </option>
                                  <option value="trust">Trust</option>
                                </select>
                              </div>
                            </>
                          ) : (
                            ""
                          )}

                          {formValuesNew.represent === "seller" ? (
                            <>
                              <div className="col-sm-12 mb-3">
                                <label className="col-form-label">
                                  Select Ownership Style.
                                </label>
                                <select
                                  className="form-select"
                                  name="ownership"
                                  value={formValuesNew.ownership}
                                  onChange={handleChanges}
                                >
                                  <option>Choose ownership</option>
                                  <option value="conventional">
                                    Conventional
                                  </option>
                                  <option value="newConstruction">
                                    New Construction
                                  </option>
                                  <option value="corporation">
                                    Corporation
                                  </option>
                                  <option value="trust">Trust</option>
                                  <option value="probate">Probate</option>
                                  <option value="foreclosure">
                                    Foreclosure/REO
                                  </option>
                                  <option value="hud">HUD</option>
                                  <option value="shortSale">Short Sale</option>
                                </select>
                              </div>
                            </>
                          ) : (
                            ""
                          )}

                          {formValuesNew.represent === "buyerseller" ? (
                            <>
                              <div className="col-sm-12 mb-3">
                                <label className="col-form-label">
                                  Select Ownership Style.
                                </label>
                                <select
                                  className="form-select"
                                  name="ownership"
                                  value={formValuesNew.ownership}
                                  onChange={handleChanges}
                                >
                                  <option>Choose ownership</option>
                                  <option value="conventional">
                                    Conventional
                                  </option>
                                  <option value="corporation">
                                    Corporation
                                  </option>
                                  <option value="trust">Trust</option>
                                </select>
                              </div>
                            </>
                          ) : (
                            ""
                          )}

                          {formValuesNew.represent === "referral" ? (
                            <>
                              <div className="col-sm-12 mb-3">
                                <label className="col-form-label">
                                  Select Ownership Style.
                                </label>
                                <select
                                  className="form-select"
                                  name="ownership"
                                  value={formValuesNew.ownership}
                                  onChange={handleChanges}
                                >
                                  <option>Choose ownership</option>
                                  <option value="conventional">
                                    Conventional
                                  </option>
                                  <option value="newConstruction">
                                    New Construction
                                  </option>
                                  <option value="corporation">
                                    Corporation
                                  </option>
                                  <option value="trust">Trust</option>
                                  <option value="probate">Probate</option>
                                  <option value="foreclosure">
                                    Foreclosure/REO
                                  </option>
                                  <option value="hud">HUD</option>
                                  <option value="shortSale">Short Sale</option>
                                </select>
                              </div>
                            </>
                          ) : (
                            ""
                          )}
                        </>
                      ) : (
                        ""
                      )}
                      {formValuesNew.ownership ? (
                        <>
                          {summary
                            ? summary.map((postt) => (
                                <div className="col-sm-12 mb-3">
                                  <label className="col-form-label">
                                    Selected Contact
                                  </label>
                                  <div className="card bg-secondary col-md-6 mt-0 mb-3">
                                    <div className="card-body">
                                      <div className="view_profile">
                                        <img
                                          onClick={() => {
                                            setShowPopup(true);
                                            setRepresent(postt._id);
                                          }}
                                          className="pull-right"
                                          src="https://cdn.pboffice.net/shared/transaction/page_select_drop.png"
                                        />
                                        <img
                                          className="pull-left"
                                          src={
                                            postt.image ||
                                            "https://integra.backagent.net/common/handler/pub/photo.ashx?V=1703c0890a0462fd68520b884a04f86f.0&C=people&S=2&P=%2f!Default%2fThumb%2fphoto_pending.jpg"
                                          }
                                          alt="Profile"
                                        />
                                        <span key={postt._id}>
                                          <a className="card-title mt-0">
                                            {postt.firstName}&nbsp;
                                            {postt.lastName}
                                          </a>
                                          <p>
                                            {postt.company}
                                            <br />
                                            {postt.phone}
                                            <br />
                                            <a href="#">Send Message</a>
                                          </p>
                                        </span>
                                      </div>
                                    </div>
                                    {showPopup && (
                                      <div className="ContactCardConfirms">
                                        <div className="ConfirmContent">
                                          <div className="ConfirmQuestion">
                                            Remove this contact?
                                          </div>

                                          <a
                                            onClick={(e) =>
                                              handleDelete(e, postt._id)
                                            }
                                          >
                                            Yes, remove this contact
                                          </a>
                                          <br />
                                          <a
                                            onClick={() =>
                                              handleCancel(represent)
                                            }
                                          >
                                            No, cancel
                                          </a>
                                        </div>
                                      </div>
                                    )}
                                  </div>
                                </div>
                              ))
                            : ""}

                          {clientdone ? (
                            ""
                          ) : (
                            <>
                              <label className="col-form-label">
                                Select Contacts
                              </label>
                              <div className="">
                                <button
                                  type="button"
                                  className="add_contacts_button"
                                  data-toggle="modal"
                                  data-target="#addcontact"
                                  onClick={() => TransactionStep("contact")}
                                >
                                  + Choose a Contact
                                </button>
                              </div>
                            </>
                          )}

                          {clientdone !== false ? (
                            <>
                              {/* select property section  */}

                              <div className="col-sm-12 mb-3">
                                <div className="align-items-center justify-content-between">
                                  {step1 === "" && step2 === "" ? (
                                    <>
                                      <label className="col-form-label">
                                        Select a Property.
                                      </label>
                                      <p className="mb-4">
                                        Your information can be automatically
                                        loaded with photos and descriptions.
                                      </p>
                                      <div className="col-lg-12 mb-3">
                                        <div className="border rounded-3 p-3">
                                          <div className="d-flex align-items-center justify-content-between">
                                            <div className="pe-2">
                                              <label className="col-form-label fw-bold">
                                                Yes, Find the MLS Listing
                                              </label>
                                              <div id="type-value-mls">
                                                Find a listing by street name or
                                                MLS number
                                              </div>
                                            </div>
                                            <a
                                              className="btn btn-primary btn-sm  ms-5 mt-3 order-lg-3"
                                              onClick={() =>
                                                AddBasic("2", "Enter MLS")
                                              }
                                            >
                                              Search MLS
                                            </a>
                                          </div>
                                        </div>
                                      </div>

                                      <div className="col-lg-12 mb-2">
                                        <div className="border rounded-3 p-3">
                                          <div className="d-flex align-items-center justify-content-between">
                                            <div className="pe-2">
                                              <label className="col-form-label fw-bold">
                                                No, Enter Basic Info
                                              </label>
                                              <div id="type-value">
                                                For property not in MLS, or
                                                cannot be found.
                                              </div>
                                            </div>
                                            <a
                                              className="btn btn-primary btn-sm  ms-5 mt-3 order-lg-3"
                                              onClick={() =>
                                                AddBasic("1", "Enter Basic")
                                              }
                                            >
                                              Add Basic
                                            </a>
                                          </div>
                                        </div>
                                      </div>
                                    </>
                                  ) : (
                                    ""
                                  )}

                                  {step1 && step2 === "" && propertyId == "" ? (
                                    <div className="">
                                      <div className=" p-3 mb-0">
                                        <div className="form-check">
                                          <input
                                            className="form-check-input"
                                            id="form-check-2"
                                            type="checkbox"
                                            onChange={handleChangeproperty}
                                            checked
                                          />
                                          <label id="form-check-label-one">
                                            You selected the <b>'Basic Info'</b>{" "}
                                            property entry option.
                                            <a onClick={changeOption}>
                                              <NavLink>
                                                &nbsp;&nbsp;Change Option
                                              </NavLink>
                                            </a>
                                          </label>
                                          <p className="my-2">
                                            What are the details for the
                                            property?
                                          </p>
                                          <p className="mb-2" id="">
                                            Please enter as much information as
                                            possible.
                                          </p>
                                        </div>
                                        <div className="row mt-4">
                                          <div className="col-md-6">
                                            <label
                                              className="col-col-form-label"
                                              id="col-form-label-one"
                                            >
                                              Street Number:
                                            </label>
                                            <input
                                              className="form-control"
                                              id="text-input-one"
                                              type="text"
                                              placeholder="Enter your street number"
                                              name="mlsNumber"
                                              onChange={handleChangeproperty}
                                              value={
                                                formValuesproperty.mlsNumber
                                              }
                                              style={{
                                                border:
                                                  formErrorsproperty?.mlsNumber
                                                    ? "1px solid red"
                                                    : "",
                                              }}
                                            />
                                            {formErrorsproperty.mlsNumber && (
                                              <div className="invalid-tooltip">
                                                {formErrorsproperty.mlsNumber}
                                              </div>
                                            )}
                                          </div>

                                          <div className="col-md-6">
                                            <label
                                              className="col-col-form-label"
                                              id="col-form-label-one"
                                            >
                                              Street Direction:
                                            </label>
                                            <select
                                              className="form-select"
                                              id="text-input-one"
                                              name="streetDirection"
                                              placeholder="steeet direction"
                                              value={
                                                formValuesproperty.streetDirection
                                              }
                                              onChange={handleChangeproperty}
                                              style={{
                                                border:
                                                  formErrorsproperty?.streetDirection
                                                    ? "1px solid red"
                                                    : "",
                                              }}
                                            >
                                              <option></option>
                                              <option>North</option>
                                              <option>Northeast</option>
                                              <option>East</option>
                                              <option>Southeast</option>
                                              <option>South</option>
                                              <option>Southwest</option>
                                              <option>West</option>
                                              <option>Northwest</option>
                                            </select>
                                            {formErrorsproperty.streetDirection && (
                                              <div className="invalid-tooltip">
                                                {
                                                  formErrorsproperty.streetDirection
                                                }
                                              </div>
                                            )}
                                          </div>

                                          <div className="col-md-6 mt-3">
                                            <label
                                              className="col-col-form-label"
                                              id="col-form-label-one"
                                            >
                                              Street Name:
                                            </label>
                                            <input
                                              className="form-control"
                                              id="text-input-one"
                                              type="text"
                                              name="streetAddress"
                                              placeholder="Enter your streetName"
                                              onChange={handleChangeproperty}
                                              value={
                                                formValuesproperty.streetAddress
                                              }
                                              style={{
                                                border:
                                                  formErrorsproperty?.streetAddress
                                                    ? "1px solid red"
                                                    : "",
                                              }}
                                            />

                                            {formErrorsproperty.streetAddress && (
                                              <div className="invalid-tooltip">
                                                {
                                                  formErrorsproperty.streetAddress
                                                }
                                              </div>
                                            )}
                                          </div>

                                          <div className="col-md-6 mt-3">
                                            <label
                                              className="col-col-form-label"
                                              id="col-form-label-one"
                                            >
                                              Unit Number:
                                            </label>
                                            <input
                                              className="form-control"
                                              id="text-input-one"
                                              name="mlsId"
                                              type="text"
                                              placeholder="Enter your unitNumber"
                                              onChange={handleChangeproperty}
                                              value={formValuesproperty.mlsId}
                                              style={{
                                                border:
                                                  formErrorsproperty?.mlsId
                                                    ? "1px solid red"
                                                    : "",
                                              }}
                                            />
                                            {formErrorsproperty.mlsId && (
                                              <div className="invalid-tooltip">
                                                {formErrorsproperty.mlsId}
                                              </div>
                                            )}
                                          </div>

                                          <div className="col-md-6 mt-3">
                                            <label
                                              className="col-col-form-label"
                                              id="col-form-label-one"
                                            >
                                              City:
                                            </label>
                                            <input
                                              className="form-control"
                                              id="text-input-one"
                                              name="city"
                                              type="text"
                                              placeholder="Enter your City"
                                              onChange={handleChangeproperty}
                                              autoComplete="on"
                                              value={formValuesproperty.city}
                                              style={{
                                                border: formErrorsproperty?.city
                                                  ? "1px solid red"
                                                  : "",
                                              }}
                                            />

                                            {formErrorsproperty.city && (
                                              <div className="invalid-tooltip">
                                                {formErrorsproperty.city}
                                              </div>
                                            )}
                                          </div>

                                          <div className="col-md-6 mt-3">
                                            <label
                                              className="col-col-form-label"
                                              id="col-form-label-one"
                                            >
                                              State/Province:
                                            </label>
                                            <select
                                              id="text-input-one"
                                              className="form-select"
                                              name="state"
                                              onChange={handleChangeproperty}
                                              value={formValuesproperty.state}
                                            >
                                              <option value="0">
                                                --Select--
                                              </option>
                                              {options
                                                .find(
                                                  (option) =>
                                                    option.code === "UT"
                                                )
                                                .districts.map(
                                                  (district, key) => (
                                                    <option
                                                      value={district}
                                                      key={key}
                                                    >
                                                      {district}
                                                    </option>
                                                  )
                                                )}
                                            </select>
                                          </div>

                                          <div className="col-md-6 mt-3">
                                            <label
                                              className="col-col-form-label"
                                              id="col-form-label-one"
                                            >
                                              Postal Code:
                                            </label>
                                            <input
                                              className="form-control"
                                              id="text-input-one"
                                              name="zipCode"
                                              type="text"
                                              placeholder="Enter your postal code"
                                              onChange={handleChangeproperty}
                                              value={formValuesproperty.zipCode}
                                              style={{
                                                border:
                                                  formErrorsproperty?.zipCode
                                                    ? "1px solid red"
                                                    : "",
                                              }}
                                            />

                                            {formErrorsproperty.zipCode && (
                                              <div className="invalid-tooltip">
                                                {formErrorsproperty.zipCode}
                                              </div>
                                            )}
                                          </div>

                                          <div className="col-md-6 mt-3">
                                            <label
                                              className="col-col-form-label"
                                              id="col-form-label-one"
                                            >
                                              List Price (Sale):
                                            </label>
                                            <input
                                              className="form-control"
                                              id="text-input-one"
                                              name="price"
                                              type="text"
                                              placeholder="Enter your list price sale"
                                              onChange={handleChangeproperty}
                                              value={formValuesproperty.price}
                                            />
                                          </div>

                                          <div className="col-md-6 mt-3">
                                            <label
                                              className="col-col-form-label"
                                              id="col-form-label-one"
                                            >
                                              List Price (Lease) /mo:
                                            </label>
                                            <input
                                              className="form-control"
                                              id="text-input-one"
                                              name="priceLease"
                                              type="text"
                                              placeholder="Enter your list price lease"
                                              onChange={handleChangeproperty}
                                              value={
                                                formValuesproperty.priceLease
                                              }
                                              style={{
                                                border:
                                                  formErrorsproperty?.priceLease
                                                    ? "1px solid red"
                                                    : "",
                                              }}
                                            />
                                            {formErrorsproperty.priceLease && (
                                              <div className="invalid-tooltip">
                                                {formErrorsproperty.priceLease}
                                              </div>
                                            )}
                                          </div>

                                          <div className="d-flex mt-4">
                                            <label className="col-col-form-label p-0">
                                              Want to add by MLS search?
                                            </label>
                                            &nbsp;&nbsp;
                                            <p className="form-check-label-two">
                                              To search MLS,
                                            </p>
                                            <NavLink
                                              className="form-check-label-two"
                                              onClick={clickHere}
                                            >
                                              Click Here.
                                            </NavLink>
                                          </div>
                                        </div>
                                      </div>
                                      <div className="pull-right">
                                        <a
                                          className="btn btn-primary btn-sm  ms-5 mt-3 order-lg-3"
                                          href="#"
                                          onClick={handleSubmit}
                                        >
                                          Add Property Now{" "}
                                        </a>
                                      </div>
                                    </div>
                                  ) : (
                                    ""
                                  )}
                                </div>
                                {select ? (
                                  <>
                                    <div className="bg-light rounded-3 p-4 p-md-5 mb-0">
                                      {console.log(select)}
                                      <div className="form-check">
                                        <input
                                          className="form-check-input"
                                          id="form-check-2"
                                          type="checkbox"
                                          checked
                                        />
                                        <label id="form-check-label-one">
                                          You selected the{" "}
                                          <b>
                                            {searchby === "mls"
                                              ? "MLS Listing"
                                              : "My Listing"}
                                          </b>{" "}
                                          property entry option.
                                          <NavLink onClick={Cancel}>
                                            &nbsp;&nbsp;Change Option
                                          </NavLink>
                                        </label>
                                      </div>
                                      <br />

                                      <div className="form-check">
                                        <input
                                          className="form-check-input"
                                          id="form-check-2"
                                          type="checkbox"
                                          checked
                                        />
                                        <label id="form-check-label-one">
                                          You selected the{" "}
                                          <b>
                                            {searchby === "mls"
                                              ? "MLS Listing"
                                              : "My Listing"}
                                          </b>{" "}
                                          property entry option.
                                          <NavLink onClick={CancelProperty}>
                                            &nbsp;&nbsp;Change Option
                                          </NavLink>
                                        </label>
                                      </div>
                                    </div>
                                    {propertyId ? (
                                      <div className="card card-hover card-horizontal transactions shadow-sm mb-0 mt-0 p-3">
                                        <a className="card-img-top">
                                          <img
                                            className=""
                                            src={
                                              select.image &&
                                              select.image !== "image"
                                                ? select.image
                                                : defaultpropertyimage
                                            }
                                            alt="Property"
                                            onError={(e) => propertypic(e)}
                                          />
                                        </a>
                                        <div className="card-body position-relative pb-3">
                                          <span className="mb-2">
                                            {select.state}_{select.mlsNumber}
                                          </span>
                                          <br />
                                          <span className="mb-2">
                                            {select.streetAddress}
                                          </span>
                                          <br />
                                          <span className="mb-2">
                                            {select.areaLocation}
                                          </span>
                                          <br />
                                          <span className="mb-2">
                                            {select.listingsAgent}
                                          </span>
                                        </div>

                                        {/* <div className="d-flex align-items-center justify-content-between"> */}
                                        <div className="">
                                          <p className="mb-2 badge bg-success">
                                            {select.price}
                                          </p>
                                          <br />
                                          <h6 className="mt-0">
                                            {select.status}
                                          </h6>
                                        </div>
                                      </div>
                                    ) : (
                                      ""
                                    )}
                                    {console.log(select)}
                                    {propertyId ? (
                                      ""
                                    ) : (
                                      <NavLink
                                        className="btn btn-primary btn-sm ms-5 mt-3 order-lg-3"
                                        href="#"
                                        onClick={handlePropertyUpdate}
                                      >
                                        Set Property Now
                                      </NavLink>
                                    )}
                                  </>
                                ) : (
                                  <div className="row">
                                    {step1 === "" && step2 ? (
                                      <div className=" mb-2 p-3">
                                        <div className="">
                                          <div className="form-check">
                                            <input
                                              className="form-check-input"
                                              id="form-check-2"
                                              type="checkbox"
                                              checked
                                            />
                                            <label id="form-check-label-one">
                                              You selected the{" "}
                                              <b>
                                                {searchby === "mls"
                                                  ? "MLS Listing"
                                                  : "My Listing"}
                                              </b>{" "}
                                              property entry option.
                                              <NavLink onClick={Cancel}>
                                                &nbsp;&nbsp;Change Option
                                              </NavLink>
                                            </label>
                                          </div>
                                          <div className="float-left w-100 my-3">
                                            <a
                                              className="btn btn-primary btn-sm order-lg-3"
                                              href="javascript:void(0)"
                                              onClick={(e) =>
                                                Searchbyfunction(e, "mls")
                                              }
                                            >
                                              Search By MLS Number
                                            </a>
                                            &nbsp;
                                            <a
                                              className="btn btn-primary btn-sm order-lg-3 ms-2"
                                              href="javascript:void(0)"
                                              onClick={(e) =>
                                                Searchbyfunction(e, "mylisting")
                                              }
                                            >
                                              Show My Listing
                                            </a>
                                          </div>
                                          {searchby === "mls" ? (
                                            <>
                                              <h6 className="mb-2">
                                                Search By MLS Number:
                                              </h6>
                                              {/* <p className="my-2">utahrealestate.com</p> */}
                                              <label className="col-form-label fs-sm mt-1">
                                                Select Membership{" "}
                                              </label>
                                              <select
                                                className="form-select mb-2"
                                                name="membership"
                                                onChange={(event) =>
                                                  setMembership(
                                                    event.target.value
                                                  )
                                                }
                                                value={membership}
                                              >
                                                <option value="">
                                                  Please Select Membership
                                                </option>
                                                {commaSeparatedValuesmls_membership &&
                                                commaSeparatedValuesmls_membership.length >
                                                  0
                                                  ? commaSeparatedValuesmls_membership.map(
                                                      (memberahip) => (
                                                        <option>
                                                          {memberahip}
                                                        </option>
                                                      )
                                                    )
                                                  : ""}
                                              </select>
                                              {membership ? (
                                                <>
                                                  <h6 className="mt-2 mb-2">
                                                    Enter a MLS Number:
                                                  </h6>

                                                  <div className="col-md-12 mt-2">
                                                    <div className="d-flex align-items-center justify-content-between">
                                                      <input
                                                        className="form-control w-100"
                                                        id="text-input-onee"
                                                        type="text"
                                                        name="mlsnumber"
                                                        placeholder="Enter MLS number"
                                                        onChange={
                                                          handleChangesearchmls
                                                        }
                                                        value={
                                                          searchmlsnumber.mlsnumber
                                                        }
                                                      />
                                                      <div className="pull-right">
                                                        <a
                                                          className="btn btn-primary btn-sm  mt-3 order-lg-3"
                                                          onClick={MLSSearch}
                                                          disabled={isLoading}
                                                        >
                                                          {isLoading
                                                            ? "Please wait"
                                                            : "Search"}
                                                        </a>
                                                      </div>
                                                    </div>
                                                  </div>
                                                </>
                                              ) : (
                                                ""
                                              )}
                                            </>
                                          ) : searchby === "mylisting" ? (
                                            <>
                                              <p className="mt-3 mb-3">
                                                <h6>
                                                  Search From Added Property
                                                  Listing:
                                                </h6>
                                              </p>
                                              <p className="mb-3">
                                                <h6>Enter a Associate Name:</h6>
                                              </p>

                                              <div className="col-md-12 mt-2">
                                                <div className="col-md-12">
                                                  <input
                                                    className="form-control w-100"
                                                    id="text-input-onee"
                                                    type="text"
                                                    name="associateName"
                                                    placeholder="Enter your associate name"
                                                    onChange={
                                                      handleChangesearch
                                                    }
                                                    value={
                                                      search_value.associateName
                                                    }
                                                  />
                                                </div>
                                                <div className="pull-right">
                                                  <a
                                                    className="btn btn-primary btn-sm  mt-3 order-lg-3"
                                                    onClick={propertySearch}
                                                    disabled={isLoading}
                                                  >
                                                    {isLoading
                                                      ? "Please wait"
                                                      : "Search"}
                                                  </a>
                                                </div>
                                              </div>
                                            </>
                                          ) : (
                                            ""
                                          )}
                                        </div>
                                      </div>
                                    ) : (
                                      ""
                                    )}

                                    <div>
                                      {isLoading ? (
                                        <div className="mb-4 mt-5 text-center">
                                          <a className="card-img-top">
                                            <i className="fa fa-spinner fa-spin"></i>
                                            &nbsp; Searching...
                                          </a>
                                        </div>
                                      ) : result ? (
                                        searchby === "mylisting" ? (
                                          <>
                                            {result.length > 0 ? (
                                              <>
                                                {result.map((post) => (
                                                  <div
                                                    className=" card card-hover card-horizontal transactions shadow-sm mb-0 mt-0 p-3"
                                                    onClick={(e) =>
                                                      handleSearchListing(
                                                        post._id
                                                      )
                                                    }
                                                  >
                                                    <a className="card-img-top">
                                                      {/* <img src={post.image} /> */}
                                                      <img
                                                        className=""
                                                        src={
                                                          post.image &&
                                                          post.image !== "image"
                                                            ? post.image
                                                            : defaultpropertyimage
                                                        }
                                                        alt="Property"
                                                        onError={(e) =>
                                                          propertypic(e)
                                                        }
                                                      />
                                                    </a>
                                                    <div className="card-body position-relative pb-3">
                                                      <span className="mb-2">
                                                        {post.state}_
                                                        {post.mlsNumber}
                                                      </span>
                                                      <br />
                                                      <span className="mb-2">
                                                        {post.streetAddress}
                                                      </span>
                                                      <br />
                                                      <span className="mb-2">
                                                        {post.areaLocation}
                                                      </span>
                                                      <br />
                                                      <span className="mb-2">
                                                        {post.listingsAgent}
                                                      </span>
                                                    </div>

                                                    {/* <div className="d-flex align-items-center justify-content-between"> */}
                                                    <div className="">
                                                      <p className="mb-2 badge bg-success">
                                                        {post.price}
                                                      </p>
                                                      <br />
                                                      <h6 className="mt-0">
                                                        {post.status}
                                                      </h6>
                                                      <button
                                                        className="btn btn-primary  px-3 px-sm-4"
                                                        id=""
                                                        type="button"
                                                      >
                                                        Select Listing
                                                      </button>
                                                    </div>
                                                  </div>
                                                ))}
                                                <div className="justify-content-end mb-1">
                                                  <ReactPaginate
                                                    previousLabel={"Previous"}
                                                    nextLabel={"next"}
                                                    breakLabel={"..."}
                                                    pageCount={
                                                      pageCountproperty
                                                    }
                                                    marginPagesDisplayed={1}
                                                    pageRangeDisplayed={2}
                                                    onPageChange={
                                                      handlePageClickproperty
                                                    }
                                                    containerClassName={
                                                      "pagination justify-content-center"
                                                    }
                                                    pageClassName={"page-item"}
                                                    pageLinkClassName={
                                                      "page-link"
                                                    }
                                                    previousClassName={
                                                      "page-item"
                                                    }
                                                    previousLinkClassName={
                                                      "page-link"
                                                    }
                                                    nextClassName={"page-item"}
                                                    nextLinkClassName={
                                                      "page-link"
                                                    }
                                                    breakClassName={"page-item"}
                                                    breakLinkClassName={
                                                      "page-link"
                                                    }
                                                    activeClassName={"active"}
                                                  />
                                                </div>
                                              </>
                                            ) : (
                                              ""
                                            )}
                                          </>
                                        ) : searchby === "mls" &&
                                          result.value &&
                                          result.value.length > 0 ? (
                                          <>
                                            {/* {console.log(result.value)}
                                                                          {console.log("resultvvv")} */}
                                            <div
                                              className="card card-hover card-horizontal transactions shadow-sm mb-0 mt-0 p-3"
                                              onClick={(e) =>
                                                handleSearchListing(result)
                                              }
                                            >
                                              <a className="card-img-top">
                                                {/* <img src={result?.media[0]?.MediaURL} /> */}
                                                <img
                                                  className=""
                                                  src={
                                                    result?.media[0]?.MediaURL
                                                      ? result?.media[0]
                                                          ?.MediaURL
                                                      : defaultpropertyimage
                                                  }
                                                  alt="Property"
                                                  onError={(e) =>
                                                    propertypic(e)
                                                  }
                                                />
                                              </a>
                                              <div className="card-body position-relative pb-3">
                                                <span className="mb-2">
                                                  {
                                                    result.value[0]
                                                      ?.CountyOrParish
                                                  }
                                                  _{result.value[0]?.ListingId}
                                                </span>
                                                <br />
                                                <span className="mb-2">
                                                  {
                                                    result.value[0]
                                                      ?.UnparsedAddress
                                                  }
                                                </span>
                                                <br />
                                                <span className="mb-2">
                                                  {result.value[0].City},{" "}
                                                  {
                                                    result.value[0]
                                                      .StateOrProvince
                                                  }{" "}
                                                  {result.value[0]?.PostalCode}
                                                </span>
                                                <br />
                                                <span className="mb-2">{`${result?.value[0]?.ListAgentFullName} / ${result?.value[0]?.ListOfficeName}`}</span>
                                              </div>

                                              {/* <div className="d-flex align-items-center justify-content-between"> */}
                                              <div className="">
                                                <p className="mb-2 badge bg-success">
                                                  {result.value[0].ListPrice}
                                                </p>
                                                <br />
                                                <h6 className="mt-0">
                                                  {result.value[0].MlsStatus}
                                                </h6>
                                                <button
                                                  className="btn btn-primary  px-3 px-sm-4"
                                                  id=""
                                                  type="button"
                                                >
                                                  Select Listing
                                                </button>
                                              </div>
                                            </div>
                                          </>
                                        ) : (
                                          ""
                                        )
                                      ) : (
                                        ""
                                      )}
                                    </div>
                                  </div>
                                )}
                              </div>

                              {/* select property section end */}
                              {propertyId ? (
                                <div className="col-sm-12 mb-3">
                                  <div className="selected_hover card bg-secondary col-md-6 mt-0 mb-3">
                                    <div className=" p-3 ">
                                      <div className="d-flex align-items-center justify-content-between">
                                        <div className="pe-2">
                                          <label className="col-form-label fw-bold">
                                            Confirm & Save
                                          </label>
                                          <p>
                                            if everything looks good, you are
                                            ready to
                                            <br /> create the new transaction.
                                          </p>
                                          <button
                                            id="type-value"
                                            className="m-2 btn btn-primary btn-sm"
                                            onClick={AddTransaction}
                                          >
                                            Create New Transaction
                                          </button>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              ) : (
                                ""
                              )}
                            </>
                          ) : (
                            ""
                          )}
                        </>
                      ) : (
                        ""
                      )}
                    </div>
                  </section>
                </div>

                <div className="col-md-4 mt-lg-0 mt-md-0 mt-sm-4 mt-4">
                  <div id="accordionCards" className="">
                    <div className="card bg-secondary mb-2 mt-0">
                      <div className="card-body">
                        <h3 className="h3 card-title pt-1 mb-0">
                          ✅ Transaction Status
                        </h3>
                      </div>
                      <div
                        className="collapse show"
                        id="cardCollapse1"
                        data-bs-parent="#accordionCards"
                      >
                        <div className="card-body mt-n1 pt-0">
                          <ul className="list-unstyled mb-2">
                            <li className="d-flex align-items-center mb-2">
                              {formValuesNew.represent ? (
                                <>
                                  <i className="fi-check text-success me-2"></i>
                                  <a
                                    className="nav-link fw-normal ps-1 p-0"
                                    href="#basic-info"
                                    data-scroll=""
                                    data-scroll-offset="20"
                                  >
                                    Category
                                  </a>
                                </>
                              ) : (
                                <>
                                  <img src="https://cdn.pboffice.net/shared/transaction/page_select_drop.png" />
                                  <a
                                    className="nav-link fw-normal ps-1 p-0"
                                    href="#basic-info"
                                    data-scroll=""
                                    data-scroll-offset="20"
                                  >
                                    Category
                                  </a>
                                </>
                              )}
                            </li>
                            <li className="d-flex align-items-center mb-2">
                              {formValuesNew.type ? (
                                <>
                                  <i className="fi-check text-success me-2"></i>
                                  <a
                                    className="nav-link fw-normal ps-1 p-0"
                                    href="#basic-info"
                                    data-scroll=""
                                    data-scroll-offset="20"
                                  >
                                    Type
                                  </a>
                                </>
                              ) : (
                                <>
                                  <img src="https://cdn.pboffice.net/shared/transaction/page_select_drop.png" />
                                  <a
                                    className="nav-link fw-normal ps-1 p-0"
                                    href="#basic-info"
                                    data-scroll=""
                                    data-scroll-offset="20"
                                  >
                                    Type
                                  </a>
                                </>
                              )}
                            </li>

                            <li className="d-flex align-items-center mb-2">
                              {formValuesNew.ownership ? (
                                <>
                                  <i className="fi-check text-success me-2"></i>
                                  <a
                                    className="nav-link fw-normal ps-1 p-0"
                                    href="#details"
                                    data-scroll=""
                                    data-scroll-offset="20"
                                  >
                                    Ownership
                                  </a>
                                </>
                              ) : (
                                <>
                                  <img
                                    src="https://cdn.pboffice.net/shared/transaction/page_select_drop.png"
                                    alt="Select Drop"
                                  />
                                  <a
                                    className="nav-link fw-normal ps-1 p-0"
                                    href="#basic-info"
                                    data-scroll=""
                                    data-scroll-offset="20"
                                  >
                                    Ownership
                                  </a>
                                </>
                              )}
                            </li>
                            <li className="d-flex align-items-center mb-2">
                              {contact ? (
                                <>
                                  <i className="fi-check text-success me-2"></i>
                                  <a
                                    className="nav-link fw-normal ps-1 p-0"
                                    href="#price"
                                    data-scroll=""
                                    data-scroll-offset="20"
                                  >
                                    Contacts
                                  </a>
                                </>
                              ) : (
                                <>
                                  <img
                                    src="https://cdn.pboffice.net/shared/transaction/page_select_drop.png"
                                    alt="Select Drop"
                                  />
                                  <a
                                    className="nav-link fw-normal ps-1 p-0"
                                    href="#basic-info"
                                    data-scroll=""
                                    data-scroll-offset="20"
                                  >
                                    Contacts
                                  </a>
                                </>
                              )}
                            </li>
                            <li className="d-flex align-items-center mb-2">
                              {propertyId ? (
                                <>
                                  <i className="fi-check text-success me-2"></i>
                                  <a
                                    className="nav-link fw-normal ps-1 p-0"
                                    href="#price"
                                    data-scroll=""
                                    data-scroll-offset="20"
                                  >
                                    Property
                                  </a>
                                </>
                              ) : (
                                <>
                                  <img
                                    src="https://cdn.pboffice.net/shared/transaction/page_select_drop.png"
                                    alt="Select Drop"
                                  />
                                  <a
                                    className="nav-link fw-normal ps-1 p-0"
                                    href="#basic-info"
                                    data-scroll=""
                                    data-scroll-offset="20"
                                  >
                                    Property
                                  </a>
                                </>
                              )}
                            </li>
                          </ul>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </main>

      <div className="modal in" role="dialog" id="addcontact">
        <div
          className="modal-dialog modal-lg modal-dialog-scrollable"
          role="document"
        >
          <div className="modal-content">
            <div className="modal-header">
              <h4 className="modal-title">Select Contact</h4>
              <button
                className="btn-close"
                type="button"
                data-dismiss="modal"
                aria-label="Close"
              ></button>
            </div>
            <div className="modal-body">
              <div className="row">
                <div className="col-md-6 mt-3">
                  {select === "" ? (
                    <div className="border rounded-3 p-3">
                      <form onSubmit={handleSearch}>
                        {/* <!-- First Name input --> */}
                        <div className="mb-3">
                          <label className="col-form-label">First Name</label>
                          <br />
                          <input
                            className="form-control"
                            type="text"
                            id="text-input"
                            name="firstName"
                            placeholder="Enter your first name"
                            onChange={(event) => handleChange(event)}
                            autoComplete="on"
                            style={{
                              border: formErrors?.firstName
                                ? "1px solid red"
                                : "1px solid #00000026",
                            }}
                            value={formValues.firstName}
                          />
                          <div className="invalid-tooltip">
                            {formErrors.firstName}
                          </div>
                        </div>

                        {/* <!-- Last Name input --> */}
                        <div className="mb-3">
                          <label className="col-form-label">Last Name</label>
                          <input
                            className="form-control"
                            type="text"
                            id="text-input"
                            name="lastName"
                            placeholder="Enter your last name"
                            onChange={(event) => handleChange(event)}
                            autoComplete="on"
                            style={{
                              border: formErrors?.lastName
                                ? "1px solid red"
                                : "1px solid #00000026",
                            }}
                            value={formValues.lastName}
                          />
                          <div className="invalid-tooltip">
                            {formErrors.lastName}
                          </div>
                        </div>

                        {/* <!-- Company Input --> */}
                        <div className="mb-3">
                          <label className="col-form-label">Company</label>
                          <input
                            className="form-control"
                            type="text"
                            id="text-input"
                            name="company"
                            placeholder="Enter your company"
                            onChange={(event) => handleChange(event)}
                            autoComplete="on"
                            value={formValues.company}
                          />
                        </div>

                        {/* <!-- Email input --> */}
                        <div className="mb-3">
                          <label className="col-form-label">Email</label>
                          <input
                            className="form-control"
                            id="email-input"
                            type="text"
                            name="email"
                            placeholder="Enter your email"
                            onChange={(event) => handleChange(event)}
                            autoComplete="on"
                            style={{
                              border: formErrors?.email
                                ? "1px solid red"
                                : "1px solid #00000026",
                            }}
                            value={formValues.email}
                          />
                          <div className="invalid-tooltip">
                            {formErrors.email}
                          </div>
                        </div>

                        {/* <!-- Phone Input --> */}
                        <div className="mb-3">
                          <label className="col-form-label">Phone</label>
                          <input
                            className="form-control"
                            type="tel"
                            id="tel-input"
                            name="phone"
                            placeholder="Enter your phone"
                            onChange={(event) => handleChange(event)}
                            autoComplete="on"
                            pattern="[0-9]+"
                            required
                            title="Please enter a valid phone number"
                            value={formValues.phone}
                          />
                        </div>

                        {/* <!-- Checkbox Input --> */}
                        <div>
                          <div className="form-check">
                            <input
                              className="form-check-input"
                              name="checkbox"
                              type="checkbox"
                              id="agree-to-terms"
                              onClick={handleCheck}
                              value={checkBox.checkBox}
                            />
                            <div className="invalid-tooltip">
                              {checkBox.checkBox}
                            </div>
                            <label className="form-check-label">
                              No email address available.
                            </label>
                          </div>
                        </div>
                      </form>
                      <div className="pull-right mt-4">
                        <button
                          type="button"
                          className="btn btn-primary btn-sm"
                          id="save-button"
                          onClick={handleNewContact}
                        >
                          Add New Contact
                        </button>
                        <button
                          type="button"
                          className="btn btn-secondary btn-sm ms-3"
                          id="closeModal"
                          data-dismiss="modal"
                        >
                          Cancel & Close
                        </button>
                      </div>
                    </div>
                  ) : (
                    <div className="col-md-6 w-100">
                      <div className="card bg-secondary mt-0">
                        <h6>{select.represent}</h6>
                        <div className="card-body">
                          <img
                            className="rounded-circle pull-right"
                            onClick={cancel}
                            src="https://cdn.pboffice.net/shared/transaction/page_select_drop.png"
                          />
                          <div className="view_profile">
                            <img
                              className="rounded-circle pull-left"
                              src={
                                select.image ||
                                "https://integra.backagent.net/common/handler/pub/photo.ashx?V=1703c0890a0462fd68520b884a04f86f.0&C=people&S=2&P=%2f!Default%2fThumb%2fphoto_pending.jpg"
                              }
                              alt="Profile"
                            />

                            <span>
                              <strong>
                                <a href="#" className="card-title mt-0">
                                  {select.firstName}&nbsp;
                                  {select.lastName}
                                </a>
                              </strong>
                              <p>
                                {select.company}
                                <br />
                                {select.phone}
                                <br />
                                <a href="#">Send Message</a>
                              </p>
                            </span>
                          </div>
                        </div>
                      </div>
                      <div className="pull-right mt-3">
                        <button
                          type="button"
                          className="btn btn-primary btn-sm"
                          onClick={(e) => selectContact(select._id)}
                        >
                          Select Contact
                        </button>
                        <button
                          type="button"
                          className="btn btn-secondary btn-sm ms-2"
                          id="closeModal"
                          data-dismiss="modal"
                          onClick={cancel}
                        >
                          Cancel & Close
                        </button>
                      </div>
                    </div>
                  )}
                </div>
                <div className="col-md-6 mt-3">
                  {isLoading ? (
                    <p>Loading...</p>
                  ) : (
                    results.map((post) => {
                      return (
                        <div>
                          {/* <!-- List group with icons and badges --> */}
                          <ul
                            className="list-group"
                            onClick={(e) => handleSearchTransaction(post._id)}
                          >
                            <li className="list-group-item d-flex justify-content-start align-items-center mt-0">
                              <img
                                className="rounded-circle avtar"
                                src={post.image || avtar}
                                alt="Profile"
                              />
                              <span className="float-left text-left w-100 ms-2">
                                {" "}
                                <strong>
                                  {post.firstName}
                                  {post.lastName}
                                </strong>{" "}
                                {post.company}
                                {post.phone}
                              </span>
                            </li>
                          </ul>
                        </div>
                      );
                    })
                  )}
                </div>
                <div className="pagination justify-content-center mb-0 mt-3">
                  <ReactPaginate
                    previousLabel={"Previous"}
                    nextLabel={"next"}
                    breakLabel={"..."}
                    pageCount={pageCount}
                    marginPagesDisplayed={1}
                    pageRangeDisplayed={2}
                    onPageChange={handlePageClick}
                    containerClassName={"pagination justify-content-center"}
                    pageClassName={"page-item"}
                    pageLinkClassName={"page-link"}
                    previousClassName={"page-item"}
                    previousLinkClassName={"page-link"}
                    nextClassName={"page-item"}
                    nextLinkClassName={"page-link"}
                    breakClassName={"page-item"}
                    breakLinkClassName={"page-link"}
                    activeClassName={"active"}
                  />
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
export default NewTransaction;

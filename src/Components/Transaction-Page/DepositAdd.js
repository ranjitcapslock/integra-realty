import React, { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import user_service from "../service/user_service";
import Loader from "../../Pages/Loader/Loader.js";
import Toaster from "../../Pages/Toaster/Toaster";
import { NavLink, useNavigate } from "react-router-dom";
import AllTab from "../../Pages/AllTab";
import jwt from "jwt-decode";
import moment from "moment-timezone";
import _ from "lodash";

const DepositAdd = () => {
  const params = useParams();
  const navigate = useNavigate();
  const [loader, setLoader] = useState({ isActive: null });
  const { isActive } = loader;
  const [toaster, setToaster] = useState({
    types: null,
    isShow: null,
    toasterBody: "",
    message: "",
  });
  const { types, isShow, toasterBody, message } = toaster;
  const [summary, setSummary] = useState("");

  const initialValues = {
    entry_type: "deposit",
    entry_date: "",
    date_earnest: "",
    payer :"",
    amount: "",
    check: "",
    memo: "",
    agentId: "",
    transactionId: "",
    ref_no: "",
  };
  const [formValues, setFormValues] = useState(initialValues);
  const [formErrors, setFormErrors] = useState({});
  const [isSubmitClick, setISSubmitClick] = useState(false);
  const [entryDate, setEntryDate] = useState("");


  useEffect(() => {
    window.scrollTo(0, 0);
    setLoader({ isActive: true });
    const TransactionGetById = async () => {
      await user_service.transactionGetById(params.id).then((response) => {
        if (response) {
          setLoader({ isActive: false });
          setSummary(response.data);
        }
      });
    };
    TransactionGetById(params.id);
  }, [params.id]);

  useEffect(() => {
    window.scrollTo(0, 0);
    if (params.depositId) {
      setLoader({ isActive: true });
      const TransactionDepositGetById = async () => {
        await user_service
          .transactionDepositGetId(params.depositId)
          .then((response) => {
            if (response) {
              setLoader({ isActive: false });
              console.log(response.data);
              setFormValues(response.data);
              // const EntryDate = response.data.entry_date
              // setEntryDate(EntryDate);
            }
          });
      };
      TransactionDepositGetById(params.depositId);
    }
  }, [params.depositId]);

  const handleChange = (e) => {
    const { name, value } = e.target;
    setFormValues({ ...formValues, [name]: value });
  };

  useEffect(() => {
    if (formValues && isSubmitClick) {
      validate();
    }
  }, [formValues]);

  const validate = () => {
    const values = formValues;
    const errors = {};

    if (!values.entry_date) {
      errors.entry_date = "Entry date is required";
    }
    if (!values.amount) {
      errors.amount = "Earnest Money Amount is required";
    }

    if (!values.date_earnest) {
      errors.date_earnest = "Earnest Money Date is required";
    }
    if (!values.payer) {
      errors.payer = "Payer is required";
    }

    setFormErrors(errors);
    return errors;
  };

  const handleSubmit = async (e) => {
    if (params.depositId) {
      e.preventDefault();
      setISSubmitClick(true);
      let checkValue = validate();
      if (_.isEmpty(checkValue)) {
        const userData = {
          agentId: jwt(localStorage.getItem("auth")).id,
          entry_type: formValues.entry_type,
          entry_date: formValues.entry_date,
          check: formValues.check,
          amount: formValues.amount,
          date_earnest: formValues.date_earnest,
          payer: formValues.payer,
          memo: formValues.memo,
          ref_no: formValues.ref_no,
          transactionId: summary._id,
          firstName:
            summary.contact1[0].data.firstName +
            " " +
            summary.contact1[0].data.lastName,
        };
        setLoader({ isActive: true });
        await user_service
          .transactionDepositUpdate(params.depositId, userData)
          .then((response) => {
            if (response) {
              const userDatan = {
                addedBy: jwt(localStorage.getItem("auth")).id,
                agentId: jwt(localStorage.getItem("auth")).id,
                transactionId: params.id,
                message: "Transaction Deposit Update ",
                // transaction_property_address: "ADDRESS 2",
                note_type: "Deposit",
              };
              const responsen = user_service.transactionNotes(userDatan);

              setLoader({ isActive: false });
              setToaster({
                type: "Transaction Deposit Successfully",
                isShow: true,
                toasterBody: response.data.message,
                message: "Transaction Deposit Successfully",
              });
              setTimeout(() => {
                setToaster((prevToaster) => ({
                  ...prevToaster,
                  isShow: false,
                }));
                navigate(`/update-deposit/${response.data.transactionId}`);
              }, 500);
            }
          });
      }
    } else {
      e.preventDefault();
      setISSubmitClick(true);
      let checkValue = validate();
      if (_.isEmpty(checkValue)) {
        const userData = {
          agentId: jwt(localStorage.getItem("auth")).id,
          entry_type: formValues.entry_type ?? "deposit",
          entry_date: formValues.entry_date,
          check: formValues.check,
          amount: formValues.amount,
          date_earnest: formValues.date_earnest,
          payer: formValues.payer,
          memo: formValues.memo,
          transactionId: summary._id,
          firstName:
            summary.contact1[0].data.firstName +
            " " +
            summary.contact1[0].data.lastName,
        };
        setLoader({ isActive: true });
        await user_service.transactionDepositAdd(userData).then((response) => {
          if (response) {
            const userDatan = {
              addedBy: jwt(localStorage.getItem("auth")).id,
              agentId: jwt(localStorage.getItem("auth")).id,
              transactionId: params.id,
              message: "Transaction Deposit Add ",
              note_type: "Deposit",
            };
            const responsen = user_service.transactionNotes(userDatan);

            setLoader({ isActive: false });
            setToaster({
              type: "Transaction Deposit Successfully",
              isShow: true,
              toasterBody: response.data.message,
              message: "Transaction Deposit Successfully",
            });
            setTimeout(() => {
              setToaster((prevToaster) => ({ ...prevToaster, isShow: false }));
              navigate(`/update-deposit/${response.data.transactionId}`);
            }, 500);
          }
        });
      }
    }
  };

  const CheckBox = (event) => {
    const select = event.target.value;
    setFormValues({
      ...formValues,
      entry_type: select,
    });
  };

  return (
    <div className="bg-secondary float-left w-100 pt-4">
      <Loader isActive={isActive} />
      {isShow && (
        <Toaster
          types={types}
          isShow={isShow}
          toasterBody={toasterBody}
          message={message}
        />
      )}
      <main className="page-wrapper">
        <div className="content-overlay transaction_tab_view mt-0">
        <div className="row">
          <AllTab />
          <div className="col-md-12">
            <div className="justify-content-center pb-sm-2">
              <div className="bg-light border rounded-3 p-3">
                <div className="add_listing">
                  <div className="row">
                    <h4>Add Deposit Ledger Entry</h4>
                    <p className="col-sm-12">
                      Log earnest money deposits and releases associated with
                      this transaction here.
                    </p>
                    <h5>What type of entry?</h5>
                    <div
                      className="col-sm-12 mb-3 mt-2"
                      onClick={(e) => CheckBox(e)}
                    >
                      <div className="form-check form-check-inline">
                        <input
                          className="form-check-input"
                          id="form-radio-5"
                          type="radio"
                          name="entryType"
                          value="deposit"
                          onClick={CheckBox}
                          checked={
                            formValues?.entry_type ===
                            "deposit"
                          }
                        />
                        <label className="form-check-label">
                          Deposit / Receipt of Check
                        </label>
                        <h6>
                          Record a deposit amount held on behalf of customer.
                        </h6>
                      </div>
                      <br />
                      <div className="form-check form-check-inline">
                        <input
                          className="form-check-input"
                          id="form-radio-5"
                          type="radio"
                          name="entryType"
                          value="release"
                          onClick={CheckBox}

                          checked={
                            formValues?.entry_type ===
                            "release" ||
                            !formValues?.entry_type
                          }
                        />
                        <label className="form-check-label">
                          Release / Return of Check
                        </label>
                        <h6 className="mb-0">
                          Record a deposit amount released back to the customer.
                        </h6>
                      </div>
                    </div>

                    <div className="col-sm-12">
                      <label className="col-form-label">
                          Entry Date &nbsp;&nbsp; <small><NavLink>REQUIRED</NavLink></small>
                      </label>
                      <input
                        className="form-control"
                        type="date"
                        id="inline-form-input"
                        placeholder="Choose date"
                        name="entry_date"
                        onChange={handleChange}
                       
                        value={formValues.entry_date}
                        style={{
                          border: formErrors?.entry_date
                            ? "1px solid red"
                            : "1px solid #00000026",
                        }}
                        data-datepicker-options='{"altInput": true, "altFormat": "F j, Y", "dateFormat": "Y-m-d"}'
                      />

                      {formErrors.entry_date && (
                        <div className="invalid-tooltip">
                          {formErrors.entry_date}
                        </div>
                      )}
                    </div>

                    <div className="col-sm-12">
                      <label className="col-form-label">
                          Date Earnest Money Received By Broker &nbsp;&nbsp; <small><NavLink>REQUIRED</NavLink></small>
                      </label>
                      <input
                        className="form-control"
                        type="date"
                        id="inline-form-input"
                        placeholder="Choose date"
                        name="date_earnest"
                        onChange={handleChange}
                        value={formValues.date_earnest}
                        style={{
                          border: formErrors?.date_earnest
                            ? "1px solid red"
                            : "1px solid #00000026",
                        }}
                        data-datepicker-options='{"altInput": true, "altFormat": "F j, Y", "dateFormat": "Y-m-d"}'
                      />

                      {formErrors.date_earnest && (
                        <div className="invalid-tooltip">
                          {formErrors.date_earnest}
                        </div>
                      )}
                    </div>

                    <div className="col-sm-12">
                      <label className="col-form-label">
                          Earnest Money Amount &nbsp;&nbsp; <small><NavLink>REQUIRED</NavLink></small>
                      </label>
                      <input
                        className="form-control"
                        id="inline-form-input"
                        type="text"
                        name="amount"
                        value={formValues.amount}
                        onChange={handleChange}
                        style={{
                          border: formErrors?.amount
                            ? "1px solid red"
                            : "1px solid #00000026",
                        }}
                      />
                      {formErrors.amount && (
                        <div className="invalid-tooltip">
                          {formErrors.amount}
                        </div>
                      )}
                    </div>
                      
                    <div className="col-sm-12">
                        <label className="col-form-label">Payer &nbsp;&nbsp; <small><NavLink>REQUIRED</NavLink></small></label>
                      <input
                        className="form-control"
                        id="inline-form-input"
                        type="text"
                        name="payer"
                        value={formValues.payer}
                        onChange={handleChange}
                        style={{
                          border: formErrors?.payer
                            ? "1px solid red"
                            : "1px solid #00000026",
                        }}
                      />
                      {formErrors.payer && (
                        <div className="invalid-tooltip">
                          {formErrors.payer}
                        </div>
                      )}
                    </div>

                    <div className="col-sm-12">
                      <label className="col-form-label">Check #</label>
                      <input
                        className="form-control"
                        id="inline-form-input"
                        type="text"
                        name="check"
                        value={formValues.check}
                        onChange={handleChange}
                      />
                    </div>

                    <div className="col-sm-12">
                      <label className="col-form-label">Notes</label>
                      <input
                        className="form-control"
                        id="inline-form-input"
                        type="text"
                        name="memo"
                        value={formValues.memo}
                        onChange={handleChange}
                      />
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className="pull-right my-4">
              {params.depositId ? (
                <button
                  className="btn btn-primary px-3 px-sm-4"
                  type="button"
                  onClick={handleSubmit}
                >
                  Update
                </button>
              ) : (
                <button
                  className="btn btn-primary px-3 px-sm-4"
                  type="button"
                  onClick={handleSubmit}
                >
                  Add Entry
                </button>
              )}
              <NavLink
                to={`/update-deposit/${params.id}`}
                className="btn btn-secondary px-3 px-sm-4 ms-3"
              >
                Cancel & Go Back
              </NavLink>
            </div>
          </div>
          </div>
        </div>
      </main>
    </div>
  );
};

export default DepositAdd;

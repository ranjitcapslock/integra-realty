import React, { useState, useEffect } from "react";
import _ from "lodash";
import Loader from "../../Pages/Loader/Loader.js";
import Toaster from "../../Pages/Toaster/Toaster.js";
import { NavLink, useNavigate, useParams } from "react-router-dom";
import jwt from "jwt-decode";
import user_service from "../service/user_service";

function AddBalance() {
  const navigate = useNavigate();

  const params = useParams();
  console.log(params.addBalance);
  const [loader, setLoader] = useState({ isActive: null });
  const { isActive } = loader;
  const [toaster, setToaster] = useState({
    types: null,
    isShow: null,
    toasterBody: "",
    message: "",
  });
  const { types, isShow, toasterBody, message } = toaster;

  const initialValues = {
    entry_type: "",
    amount: "",
    memo: "",
  };
  const [formValues, setFormValues] = useState(initialValues);
  const [formErrors, setFormErrors] = useState({});
  const [isSubmitClick, setISSubmitClick] = useState(false);

  const handleChange = (e) => {
    const { name, value } = e.target;
    setFormValues({ ...formValues, [name]: value });
  };

  useEffect(() => {
    if (formValues && isSubmitClick) {
      validate();
    }
  }, [formValues]);

  const validate = () => {
    const values = formValues;
    const errors = {};

    if (!values.amount) {
      errors.amount = "amount is required!";
    }

    if (!values.memo) {
      errors.memo = "memo is required!";
    }

    setFormErrors(errors);
    return errors;
  };
  {
    /* <!-- Form Validation End--> */
  }

  {
    /* <!-- Api call  Function Start--> */
  }
  const handleBilling = async (e) => {
    if (params.addBalance) {
      console.log(params.addBalance);
      e.preventDefault();
      setISSubmitClick(true);
      let checkValue = validate();
      if (_.isEmpty(checkValue)) {
        const userData = {
          agentId: jwt(localStorage.getItem("auth")).id,

          amount: formValues.amount,
          entry_type: formValues.entry_type ? formValues.entry_type : "Credit",
          memo: formValues.memo,
        };
        console.log(userData);
        try {
          setLoader({ isActive: true });
          await user_service
            .agentAccountingUpdate(params.addBalance, userData)
            .then((response) => {
              if (response) {
                console.log(response);
                setLoader({ isActive: false });
                setToaster({
                  type: "Add Balance Successfully",
                  isShow: true,
                  toasterBody: response.data.message,
                  message: "Add Balance Successfully",
                });
                setTimeout(() => {
                  setToaster((prevToaster) => ({
                    ...prevToaster,
                    isShow: false,
                  }));
                  navigate(`/account-balance/${params.id}`);
                }, 2000);
              }
            });
        } catch (err) {
          setToaster({
            type: "API Error",
            isShow: true,
            toasterBody: err.response.data.message,
            message: "API Error",
          });

          setTimeout(() => {
            setToaster((prevToaster) => ({ ...prevToaster, isShow: false }));
          }, 2000);
        }
      }
    } else {
      e.preventDefault();
      setISSubmitClick(true);
      let checkValue = validate();
      if (_.isEmpty(checkValue)) {
        const userData = {
          agentId: jwt(localStorage.getItem("auth")).id,

          amount: formValues.amount,
          entry_type: formValues.entry_type ? formValues.entry_type : "Credit",
          memo: formValues.memo,
        };
        console.log(userData);
        try {
          setLoader({ isActive: true });
          await user_service.agentAccounting(userData).then((response) => {
            if (response) {
              console.log(response);
              setLoader({ isActive: false });
              setToaster({
                type: "Add Balance Successfully",
                isShow: true,
                toasterBody: response.data.message,
                message: "Add Balance Successfully",
              });
              setISSubmitClick(false)
              setTimeout(() => {
                setToaster((prevToaster) => ({
                  ...prevToaster,
                  isShow: false,
                }));
                navigate(`/account-balance/${params.id}`);
              }, 2000);
            }
          });
        } catch (err) {
          setToaster({
            type: "API Error",
            isShow: true,
            toasterBody: err.response.data.message,
            message: "API Error",
          });

          setTimeout(() => {
            setToaster((prevToaster) => ({ ...prevToaster, isShow: false }));
          }, 2000);
        }
      }
    }
  };
  {
    /* <!-- Api call  Function End--> */
  }

  useEffect(() => {
    window.scrollTo(0, 0);
    if (params.addBalance) {
      AgentAccountingId();
    }
  }, []);

  const AgentAccountingId = async () => {
    await user_service
      .agentAccountingGetId(params.addBalance)
      .then((response) => {
        if (response) {
          setFormValues(response.data);
        }
      });
  };
  return (
    <div className="bg-secondary float-left w-100 pt-4 mb-4">
      <Loader isActive={isActive} />
      {isShow && (
        <Toaster
          types={types}
          isShow={isShow}
          toasterBody={toasterBody}
          message={message}
        />
      )}
      <div className="row">
        <div className="float-left w-100 d-flex align-items-center justify-content-between mb-3">
          <h3 className="text-white pull-left mb-0" id="getContact">
            Add Billing Entries
          </h3>
        </div>
        <div className="col-md-8">
          <div className="add_listing border rounded-3 w-100 p-3">
            <div className="add_listing w-100">
              <div className="row">
                <div className="col-md-12 mb-3">
                  <label className="form-label" for="pr-city">
                    Select Category
                  </label>
                  <select
                    className="form-select"
                    id="pr-city"
                    name="entry_type"
                    value={formValues.entry_type}
                    onChange={handleChange}
                  >
                    <option>Credit</option>
                    <option>Fee</option>
                    <option>Fine</option>
                    <option>Purchase</option>
                    <option>Refund</option>
                  </select>
                </div>
                <div className="col-md-12 mb-3">
                  <label className="form-label">Enter Memo</label>
                  <textarea
                    className="form-control"
                    id="textarea-input"
                    rows="5"
                    name="memo"
                    value={formValues.memo}
                    onChange={handleChange}
                  >
                    Hello World!
                  </textarea>
                  {formErrors.memo && (
                    <div className="invalid-tooltip">{formErrors.memo}</div>
                  )}
                </div>
                <div className="col-md-12 mb-3">
                  <label className="form-label">Enter Amount *</label>
                  <input
                    className="form-control"
                    id="inline-form-input"
                    type="number"
                    name="amount"
                    value={formValues.amount}
                    onChange={handleChange}
                  />
                  {formErrors.amount && (
                    <div className="invalid-tooltip">{formErrors.amount}</div>
                  )}
                </div>
              </div>
            </div>
          </div>
          <div className="float-left w-100 pull-right mt-4">
            <NavLink
              to={`/account-balance/${params.id}`}
              className="btn btn-secondary pull-right ms-5"
              type="button"
            >
              Cancel & Go Back
            </NavLink>
            <button
              onClick={handleBilling}
              className="btn btn-primary pull-right"
              type="button"
            >
              {params.addBalance ? "Update" : "Add Billing Entry"}
            </button>
          </div>
        </div>

        <div className="col-md-4"></div>
      </div>
    </div>
  );
}
export default AddBalance;

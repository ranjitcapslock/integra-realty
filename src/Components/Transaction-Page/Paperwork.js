import React, { useState, useEffect } from "react";
import _ from 'lodash';
import user_service from "../service/user_service";
import jwt from "jwt-decode";
import Loader from "../../Pages/Loader/Loader.js";
import Toaster from "../../Pages/Toaster/Toaster.js";
import axios from "axios";
import { logDOM } from "@testing-library/react";
import { NavLink } from "react-router-dom";


function Paperwork() {
    const [loader, setLoader] = useState({ isActive: null })
    const { isActive } = loader;
    const [toaster, setToaster] = useState({ types: null, isShow: null, toasterBody: "", message: "" });
    const { types, isShow, toasterBody, message } = toaster;

    const [paperGet, setPaperGet] = useState([])

    useEffect(() => { window.scrollTo(0, 0);
        setLoader({ isActive: true })
        const PaperworkAll = async () => {
            await user_service.paperworkGet().then((response) => {
                setLoader({ isActive: false })
                if (response) {
                    setPaperGet(response.data);
                }
            });
        }
        PaperworkAll()
    }, []);


    const handleRemove = async (id) => {
        setLoader({ isActive: true })
        await user_service.paperworkDelete(id).then((response) => {
            if (response) {
                setLoader({ isActive: false })
                const PaperworkAll = async () => {
                    await user_service.paperworkGet().then((response) => {
                        setLoader({ isActive: false })
                        if (response) {
                            setPaperGet(response.data);
                        }
                    });
                }
                PaperworkAll()
            }
        });
    }
    return (
        <div className="bg-secondary float-left w-100 pt-4">
            <Loader isActive={isActive} />
            {isShow && <Toaster types={types} isShow={isShow} toasterBody={toasterBody} message={message} />}
            <div className="container">
                <div className="justify-content-center pb-sm-2">
                    <div className="bg-light rounded-3 p-4 mb-3">
                        <NavLink to="/add-paperwork" className="btn btn-primary pull-right ms-5" type="button">Add a New Paperwork</NavLink>
                        <div className="add_listing shadow-sm w-100 m-auto p-4">
                            <h2>Available Paperwork</h2>

                            <div className="row mt-5">

                                <table id="DepositLedger" className="DepositTable" width="100%" border="0" cellSpacing="0" cellPadding="0">
                                    <colgroup>
                                        <col width="50%" />
                                        <col width="25%" />

                                        <col width="25%" />

                                    </colgroup>
                                    <thead>
                                        <tr>
                                            <th>Title</th>
                                            <th>Remove</th>
                                            <th className="hright">Modify</th>

                                        </tr>
                                    </thead>

                                    <tbody>
                                        {paperGet.data ?
                                            paperGet.data.length > 0 ?
                                                paperGet.data.map((items) => {
                                                    return (
                                                        <tr>
                                                            <td><span className="SuppressText">{items.paperwork_title}</span></td>
                                                            <td><span className="SuppressText">
                                                                {
                                                                items.paperwork_title === "Independent Contractor Agreement" || items.paperwork_title === "W9" ? 
                                                                ""
                                                                :
                                                                <img onClick={(e) => handleRemove(items._id)}
                                                                    className="pull-center"
                                                                    src="https://cdn.pboffice.net/shared/transaction/page_select_drop.png"
                                                                />
                                                                }
                                                                </span>
                                                            </td>
                                                            <td>
                                                            {
                                                                items.paperwork_title === "Independent Contractor Agreement" || items.paperwork_title === "W9"? 
                                                                ""
                                                                :
                                                                <NavLink to={`/add-paperwork/${items._id}`}><span className="SuppressText">Edit</span></NavLink>
                                                            }
                                                            </td>
                                                        </tr>
                                                    )
                                                })
                                                :
                                                ""
                                            :
                                            ""
                                        }
                                    </tbody>
                                </table>
                            </div>

                        </div>


                    </div>
                </div>
            </div>
        </div>
    )
}
export default Paperwork;
import React, { useState, useEffect } from "react";
import utility from "../img/utilities-hero.svg";
import _ from "lodash";
import user_service from "../service/user_service.js";
import jwt from "jwt-decode";
import Loader from "../../Pages/Loader/Loader.js";
import Toaster from "../../Pages/Toaster/Toaster.js";
import { NavLink } from "react-router-dom";
import ReactPaginate from "react-paginate";

function UtilityTool() {
  const initialValues = {
    firstName: "",
    lastName: "",
    email: "",
    oldStreetAddress: "",
    oldCity: "",
    oldState: "",
    oldZipcode: "",
    newStreetAddress: "",
    newCity: "",
    newState: "",
    newZipcode: "",
    estimateDate: "",
    utilityMessage: "",
    inviteClient: "",
    inviteDate: "",
  };
  const [formValues, setFormValues] = useState(initialValues);
  const [formErrors, setFormErrors] = useState({});
  const [isSubmitClick, setISSubmitClick] = useState(false);
  const [checkInvite, setCheckInvite] = useState("now");

  const [loader, setLoader] = useState({ isActive: null });
  const { isActive } = loader;
  const [toaster, setToaster] = useState({
    types: null,
    isShow: null,
    toasterBody: "",
    message: "",
  });
  const { types, isShow, toasterBody, message } = toaster;

  // onChange Function start
  const handleChange = (e) => {
    const { name, value } = e.target;
    setFormValues({ ...formValues, [name]: value });
  };
  // onChange Function end

  {
    /* <!-- Form Validation Start--> */
  }
  useEffect(() => {
    if (formValues && isSubmitClick) {
      validate();
    }
  }, [formValues]);

  const validate = () => {
    const values = formValues;
    const errors = {};
    if (!values.firstName) {
      errors.firstName = "First Name is required";
    }

    if (!values.lastName) {
      errors.lastName = "Last Name is required";
    }
    if (!values.email) {
      errors.email = "Email is required";
    }

    if (!values.oldStreetAddress) {
      errors.oldStreetAddress = "Old Address is required";
    }
    if (!values.oldCity) {
      errors.oldCity = "Old City is required";
    }

    if (!values.oldState) {
      errors.oldState = "old State is required";
    }

    if (!values.oldZipcode) {
      errors.oldZipcode = "Old Zip-code is required";
    }

    if (!values.newStreetAddress) {
      errors.newStreetAddress = "New Address is required";
    }
    if (!values.newCity) {
      errors.newCity = "New City is required";
    }

    if (!values.newState) {
      errors.newState = "New State is required";
    }
    if (!values.newZipcode) {
      errors.newZipcode = "New Zip-code is required";
    }

    if (!values.utilityMessage) {
      errors.utilityMessage = "Message is required";
    }

    if (!values.estimateDate) {
      errors.estimateDate = "Estimate-Date is required";
    }

    if (checkInvite === "later") {
      if (!values.inviteDate) {
        errors.inviteDate = "Invite-Date is required";
      }
    }

    setFormErrors(errors);
    return errors;
  };
  {
    /* <!-- Form Validation End--> */
  }

  {
    /* <!--Api call Form onSubmit Start--> */
  }

  const handleSubmit = async (e) => {
    e.preventDefault();
    setISSubmitClick(true);
    let checkValue = validate();
    if (_.isEmpty(checkValue)) {
      const userData = {
        agentId: jwt(localStorage.getItem("auth")).id,
        firstName: formValues.firstName,
        lastName: formValues.lastName,
        email: formValues.email,
        oldStreetAddress: formValues.oldStreetAddress,
        oldCity: formValues.oldCity,
        oldState: formValues.oldState,
        oldZipcode: formValues.oldZipcode,
        newStreetAddress: formValues.newStreetAddress,
        newCity: formValues.newCity,
        newState: formValues.newState,
        newZipcode: formValues.newZipcode,
        estimateDate: formValues.estimateDate,
        utilityMessage: formValues.utilityMessage,
        inviteClient: checkInvite,
        inviteDate: formValues.inviteDate,
      };

      try {
        setLoader({ isActive: true });
        const response = await user_service.UtiltityAgentPost(userData);
        if (response.status === 200) {
          console.log(response.data._id);
          const responseData = await user_service.UtilityEmail(
            response.data._id
          );
          setLoader({ isActive: false });
          setToaster({
            type: "Add Utility",
            isShow: true,
            toasterBody: response.data.message,
            message: "Utility Added Successfully",
          });
          setTimeout(() => {
            setToaster((prevToaster) => ({ ...prevToaster, isShow: false }));
            // navigate(`/question`);
          }, 1000);
        } else {
          setLoader({ isActive: false });
          setToaster({
            type: "error",
            isShow: true,
            toasterBody: response.data.message,
            message: "Error",
          });
        }
      } catch (error) {
        setLoader({ isActive: false });
        setToaster({
          type: "error",
          isShow: true,
          toasterBody: error.response
            ? error.response.data.message
            : error.message,
          message: "Error",
        });
        setTimeout(() => {
          setToaster((prevToaster) => ({ ...prevToaster, isShow: false }));
        }, 2000);
      }
    }
  };

  const handleChangeInvite = (e) => {
    setCheckInvite(e.target.name);
    setCheckInvite(e.target.value);
  };

  const handleNewClick = () => {
    setFormValues({
      ...formValues,
      newStreetAddress: "",
      newCity: "",
      newState: "",
      newZipcode: "",
    });
  };

  const handleOldClick = () => {
    setFormValues({
      ...formValues,
      oldStreetAddress: "",
      oldCity: "",
      oldState: "",
      oldZipcode: "",
    });
  };

  const [query, setQuery] = useState("");
  const [pageCountNew, setPageCountNew] = useState(0);
  const [utilityData, setUtilityData] = useState([]);
  const [isLoading, setIsLoading] = useState(false);

  const UtilitySearch = async () => {
    setIsLoading(true);
    await user_service.UtilityDataAgentGet(1, query).then((response) => {
      setIsLoading(false);
      if (response) {
        setUtilityData(response.data.data);
        setPageCountNew(Math.ceil(response.data.totalRecords / 10));
      }
    });
  };

  useEffect(() => {
    if (query) {
      UtilitySearch();
    }
  }, [query]);

  const handlePageClickNew = async (data) => {
    let currentPage = data.selected + 1;
    setLoader({ isActive: true });

    try {
      const response = await user_service.UtilityDataAgentGet(
        currentPage,
        query
      );
      setLoader({ isActive: false });

      if (response) {
        setUtilityData(response.data.data);
        setPageCountNew(Math.ceil(response.data.totalRecords / 10));
      }
    } catch (error) {
      console.error("Error fetching data:", error);
      setLoader({ isActive: false });
    }
  };

  useEffect(() => {
    window.scrollTo(0, 0);
    if (query) {
      UtilitySearch();
    }
  }, [query]);

  return (
    <>
      <div className="bg-secondary float-left w-100 pt-4 mb-4">
        <Loader isActive={isActive} />
        {isShow && (
          <Toaster
            types={types}
            isShow={isShow}
            toasterBody={toasterBody}
            message={message}
          />
        )}
        <main className="page-wrapper">
            <NavLink className="btn btn-primary pull-right" to={`/agent-utility-listing`}>Utility Tool Listing</NavLink>
          <div className="d-flex align-items-center justify-content-center mb-4">
            <h3 className="text-white mb-0">Utility Lookup Tool</h3>
          </div>
          <div className="bg-light border rounded-3 p-3 w-100">
            <div className="float-none m-auto w-75">
              <div className="row d-flex justify-content-center m-auto">
                <h6 className="text-center">Find All Your Utilities</h6>
                <p className="text-center mb-4">
                  Find the utility company for your internet,Tv,
                  <br />
                  security,electricity,gas,water and garbage.
                </p>
                <div className="col-md-6 mb-3">
                  <label className="form-label">Search by Zip code</label>
                  <input
                    className="form-control"
                    type="text"
                    id="search-input-1"
                    placeholder="Enter a zip code"
                    valvalue={query}
                    onChange={(event) => setQuery(event.target.value)}
                  />
                </div>
              </div>
            </div>
          </div>

          {query ? (
           <div className="col-md-12 bg-light border rounded-3 p-3 mt w-100 mt-3">
           <table
             id="DepositLedger"
             className="table table-striped mb-0"
             width="100%"
             border="0"
             cellSpacing="0"
             cellPadding="0"
           >
             <tbody>
               <table
                 id="DepositLedger"
                 className="table table-striped mb-0"
                 width="100%"
                 border="0"
                 cellSpacing="0"
                 cellPadding="0"
               >
                 <thead>
                   <tr>
                     <th>NEARBY UTILITIES</th>
                     <th>PROVIDER</th>
                     <th>TYPE</th>
                     <th>CONTACT</th>
                   </tr>
                 </thead>
                 <tbody>
                   {Array.isArray(utilityData) && utilityData.length > 0 ? (
                     utilityData.map((item) => (
                       <tr key={item.id}>
                         <td>
                           {item.utilityLogo ? (
                             <img
                               style={{ width: "100px" }}
                               src={item.utilityLogo}
                             />
                           ) : (
                             ""
                           )}
                         </td>
                         <td>
                           <a
                             href={item.providerLink}
                             target="_blank"
                             rel="noopener noreferrer"
                           >
                             {item.provider}
                           </a>
                         </td>
                         <td>{item.type}</td>
                         <td>{item.contact}</td>
                       </tr>
                     ))
                   ) : (
                     <tr>
                       <td>No data to display.</td>
                     </tr>
                   )}
                 </tbody>
               </table>
             </tbody>
           </table>
         </div>
          ) : (
            <>
              <div className="bg-light border rounded-3 p-3 w-100 mt-3">
                <div className="float-none m-auto w-75">
                  <div className="row">
                    <h6 className="text-center mb-4">
                      WHO ARE YOU SENDING THIS INVITE TO?
                    </h6>
                    <div className="col-md-6 mb-3">
                      <label className="form-label">First Name*</label>
                      <input
                        className="form-control"
                        id="inline-form-input"
                        type="text"
                        name="firstName"
                        value={formValues.firstName ?? ""}
                        onChange={handleChange}
                        style={{
                          border: formErrors?.firstName ? "1px solid red" : "",
                        }}
                      />
                      {formErrors.firstName && (
                        <div className="invalid-tooltip">
                          {formErrors.firstName}
                        </div>
                      )}
                    </div>
                    <div className="col-md-6 mb-3">
                      <label className="form-label">Last Name*</label>
                      <input
                        className="form-control"
                        id="inline-form-input"
                        type="text"
                        name="lastName"
                        value={formValues.lastName ?? ""}
                        onChange={handleChange}
                        style={{
                          border: formErrors?.lastName ? "1px solid red" : "",
                        }}
                      />
                      {formErrors.lastName && (
                        <div className="invalid-tooltip">
                          {formErrors.lastName}
                        </div>
                      )}
                    </div>

                    <div className="col-md-6 mb-3">
                      <label className="form-label">Email*</label>
                      <input
                        className="form-control"
                        id="inline-form-input"
                        type="text"
                        name="email"
                        value={formValues.email ?? ""}
                        onChange={handleChange}
                        style={{
                          border: formErrors?.email ? "1px solid red" : "",
                        }}
                      />
                      {formErrors.email && (
                        <div className="invalid-tooltip">
                          {formErrors.email}
                        </div>
                      )}
                    </div>
                  </div>
                </div>
              </div>

              <div className="bg-light border rounded-3 p-3 w-100 mt-3">
                <div className="float-none m-auto w-75">
                  <div className="row">
                    <h6 className="text-center">
                      WHERE ARE YOUR CLIENT(S) MOVING TO AND FROM?
                    </h6>
                    <p className="text-center">
                      One address to required to pre fill your client's forms,
                      but bith is better.
                    </p>

                    <div className="row">
                      <h4 className="text-center mb-4">
                        YOUR CLIENT(S) OLD ADDRESS?
                      </h4>

                      <div className="col-md-6 mb-3">
                        <label className="form-label">
                          Old Street Address*
                        </label>
                        <input
                          className="form-control"
                          id="inline-form-input"
                          type="text"
                          name="oldStreetAddress"
                          value={formValues.oldStreetAddress ?? ""}
                          onChange={handleChange}
                          style={{
                            border: formErrors?.oldStreetAddress
                              ? "1px solid red"
                              : "",
                          }}
                        />
                        {formErrors.oldStreetAddress && (
                          <div className="invalid-tooltip">
                            {formErrors.oldStreetAddress}
                          </div>
                        )}
                      </div>

                      <div className="col-md-6 md-3">
                        <label className="form-label">Old City*</label>
                        <input
                          className="form-control"
                          id="inline-form-input"
                          type="text"
                          name="oldCity"
                          value={formValues.oldCity ?? ""}
                          onChange={handleChange}
                          style={{
                            border: formErrors?.oldCity ? "1px solid red" : "",
                          }}
                        />
                        {formErrors.oldCity && (
                          <div className="invalid-tooltip">
                            {formErrors.oldCity}
                          </div>
                        )}
                      </div>

                      <div className="col-md-6 mb-3">
                        <label className="form-label">Old State*</label>
                        <input
                          className="form-control"
                          id="inline-form-input"
                          type="text"
                          name="oldState"
                          value={formValues.oldState ?? ""}
                          onChange={handleChange}
                          style={{
                            border: formErrors?.oldState ? "1px solid red" : "",
                          }}
                        />
                        {formErrors.oldState && (
                          <div className="invalid-tooltip">
                            {formErrors.oldState}
                          </div>
                        )}
                      </div>

                      <div className="col-md-6 mb-3">
                        <label className="form-label">Old Zip-code*</label>
                        <input
                          className="form-control"
                          id="inline-form-input"
                          type="number"
                          name="oldZipcode"
                          value={formValues.oldZipcode ?? ""}
                          onChange={handleChange}
                          style={{
                            border: formErrors?.oldZipcode
                              ? "1px solid red"
                              : "",
                          }}
                        />
                        {formErrors.oldZipcode && (
                          <div className="invalid-tooltip">
                            {formErrors.oldZipcode}
                          </div>
                        )}
                        <button
                          className="btn btn-primary mt-4 pull-right"
                          onClick={handleOldClick}
                        >
                          CLEAR
                        </button>
                      </div>
                    </div>

                    <div className="row mt-4">
                      <h4 className="text-center mb-4">
                        YOUR CLIENT(S) NEW ADDRESS?
                      </h4>
                      <div className="col-md-6 mb-3">
                        <label className="form-label">
                          New Street Address*
                        </label>
                        <input
                          className="form-control"
                          id="inline-form-input"
                          type="text"
                          name="newStreetAddress"
                          value={formValues.newStreetAddress ?? ""}
                          onChange={handleChange}
                          style={{
                            border: formErrors?.newStreetAddress
                              ? "1px solid red"
                              : "",
                          }}
                        />
                        {formErrors.newStreetAddress && (
                          <div className="invalid-tooltip">
                            {formErrors.newStreetAddress}
                          </div>
                        )}
                      </div>

                      <div className="col-md-6 mb-3">
                        <label className="form-label">New City*</label>
                        <input
                          className="form-control"
                          id="inline-form-input"
                          type="text"
                          name="newCity"
                          value={formValues.newCity ?? ""}
                          onChange={handleChange}
                          style={{
                            border: formErrors?.newCity ? "1px solid red" : "",
                          }}
                        />
                        {formErrors.newCity && (
                          <div className="invalid-tooltip">
                            {formErrors.newCity}
                          </div>
                        )}
                      </div>

                      <div className="col-md-6 mb-3">
                        <label className="form-label">New State*</label>
                        <input
                          className="form-control"
                          id="inline-form-input"
                          type="text"
                          name="newState"
                          value={formValues.newState ?? ""}
                          onChange={handleChange}
                          style={{
                            border: formErrors?.newState ? "1px solid red" : "",
                          }}
                        />
                        {formErrors.newState && (
                          <div className="invalid-tooltip">
                            {formErrors.newState}
                          </div>
                        )}
                      </div>

                      <div className="col-md-6 mb-3">
                        <label className="form-label">New Zip-code*</label>
                        <input
                          className="form-control"
                          id="inline-form-input"
                          type="number"
                          name="newZipcode"
                          value={formValues.newZipcode ?? ""}
                          onChange={handleChange}
                          style={{
                            border: formErrors?.newZipcode
                              ? "1px solid red"
                              : "",
                          }}
                        />
                        {formErrors.newZipcode && (
                          <div className="invalid-tooltip">
                            {formErrors.newZipcode}
                          </div>
                        )}

                        <button
                          className="btn btn-primary mt-4 pull-right"
                          onClick={handleNewClick}
                        >
                          CLEAR
                        </button>
                      </div>
                    </div>
                  </div>
                </div>
              </div>

              <div className="bg-light border rounded-3 p-3 w-100 mt-3">
                <div className="float-none m-auto w-75">
                  <div className="row d-flex justify-content-center m-auto">
                    <div className="col-md-6 mb-3">
                      <h6 className="text-center">WHEN ARE THEY MOVING?</h6>
                      <p className="text-center mb-4">
                        Not sure? Just give us your best guess. Your client will
                        always be able to update this date.
                      </p>

                      <label className="form-label">Estimate Move Date*</label>
                      <input
                        className="form-control"
                        id="inline-form-input"
                        type="date"
                        name="estimateDate"
                        value={formValues.estimateDate ?? ""}
                        onChange={handleChange}
                        style={{
                          border: formErrors?.estimateDate
                            ? "1px solid red"
                            : "",
                        }}
                      />
                      {formErrors.estimateDate && (
                        <div className="invalid-tooltip">
                          {formErrors.estimateDate}
                        </div>
                      )}
                    </div>
                  </div>

                  <div className="row d-flex justify-content-center m-auto">
                    <div className="col-md-6 mb-4 mt-4">
                      <h6 className="text-center">
                        WHEN DO YOU WANT TO INVITE YOUR CLIENT?
                      </h6>
                      <p className="text-center mb-4">
                        We recommend inviting your client 3-4 weeks before their
                        move.
                      </p>
                      <div
                        className="d-flex"
                        onClick={(e) => handleChangeInvite(e)}
                      >
                        <div className="col-md-6 form-check ps-3">
                          <input
                            className="form-check-input"
                            id="form-radio-4"
                            type="radio"
                            name="inviting"
                            value="now"
                            defaultChecked
                          />
                          <label
                            className="form-check-label ms-2"
                            id="radio-level1"
                          >
                            Now
                          </label>
                        </div>
                        <div className="col-md-6 form-check ms-2">
                          <input
                            className="form-check-input"
                            id="form-radio-4"
                            type="radio"
                            name="inviting"
                            value="later"
                          />
                          <label
                            className="form-check-label ms-2"
                            id="radio-level1"
                          >
                            Later
                          </label>
                        </div>
                      </div>
                    </div>
                  </div>

                  {checkInvite === "later" ? (
                    <div className="row d-flex justify-content-center m-auto">
                      <div className="col-md-6 mb-4 mt-4">
                        <label className="form-label">
                          SELECT AN INVITE SEND Date*
                        </label>
                        <input
                          className="form-control"
                          id="inline-form-input"
                          type="date"
                          name="inviteDate"
                          value={formValues.inviteDate ?? ""}
                          onChange={handleChange}
                          style={{
                            border: formErrors?.inviteDate
                              ? "1px solid red"
                              : "",
                          }}
                        />

                        {formErrors.inviteDate && (
                          <div className="invalid-tooltip">
                            {formErrors.inviteDate}
                          </div>
                        )}
                      </div>
                    </div>
                  ) : (
                    ""
                  )}

                  <div className="row d-flex justify-content-center m-auto">
                    <div className="col-md-6 mb-3 mb-4 mt-4">
                      <h6 className="text-center mb-4">
                        WHAT WOULD YOU LIKE TO SAY?
                      </h6>
                      <label className="form-label">Message*</label>
                      <textarea
                        className="form-control"
                        id="textarea-input"
                        rows="5"
                        name="utilityMessage"
                        value={formValues.utilityMessage}
                        onChange={handleChange}
                        style={{
                          border: formErrors?.utilityMessage
                            ? "1px solid red"
                            : "",
                        }}
                      >
                        Message
                      </textarea>
                      {formErrors.utilityMessage && (
                        <div className="invalid-tooltip">
                          {formErrors.utilityMessage}
                        </div>
                      )}
                    </div>
                  </div>
                  <div className="pull-right mt-4">
                    <NavLink
                      to={`/`}
                      className="btn btn-secondary"
                      type="button"
                    >
                      Cancel
                    </NavLink>

                    <button
                      className="btn btn-primary ms-2"
                      type="button"
                      onClick={handleSubmit}
                    >
                      Send
                    </button>
                  </div>
                </div>
              </div>
            </>
          )}

          {query ? (
            <div className="d-flex align-items-center justify-content-end mb-0 mt-4">
              <ReactPaginate
                className=""
                previousLabel={"Previous"}
                nextLabel={"next"}
                breakLabel={"..."}
                pageCount={pageCountNew}
                marginPagesDisplayed={1}
                pageRangeDisplayed={2}
                onPageChange={handlePageClickNew}
                containerClassName={"pagination justify-content-center mb-0"}
                pageClassName={"page-item"}
                pageLinkClassName={"page-link"}
                previousClassName={"page-item"}
                previousLinkClassName={"page-link"}
                nextClassName={"page-item"}
                nextLinkClassName={"page-link"}
                breakClassName={"page-item"}
                breakLinkClassName={"page-link"}
                activeClassName={"active"}
              />
            </div>
          ) : (
            ""
          )}
        </main>
      </div>
    </>
  );
}
export default UtilityTool;

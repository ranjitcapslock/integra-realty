import React, { useState, useEffect } from "react";
import Loader from "../../Pages/Loader/Loader.js";
import Toaster from "../../Pages/Toaster/Toaster";
import AllTab from "../../Pages/AllTab";
import jwt from "jwt-decode";
import user_service from "../../Components/service/user_service";
import { NavLink, useNavigate, useParams } from "react-router-dom";
import axios from "axios";
import _ from "lodash";
import Swal from "sweetalert2";
import "sweetalert2/dist/sweetalert2.css";
import { DragDropContext, Droppable, Draggable } from "react-beautiful-dnd";
import OverlayTrigger from 'react-bootstrap/OverlayTrigger'
import Tooltip from 'react-bootstrap/Tooltip'

// import Calculator from "../img/calculator.png";
// import StatusBar from "../../Pages/StatusBar.js";

const TransactionSummary = () => {
  const [loader, setLoader] = useState({ isActive: null });
  const { isActive } = loader;
  const [toaster, setToaster] = useState({
    types: null,
    isShow: null,
    toasterBody: "",
    message: "",
  });
  const { types, isShow, toasterBody, message } = toaster;
  const params = useParams();

  const navigate = useNavigate();
  const [getrequireddoucmentno, setGetrequireddoucmentno] = useState([]);

  const Getrequireddoucmentno = async () => {
    const agentId = jwt(localStorage.getItem("auth")).id;
    await user_service
      .Getrequireddoucmentno(agentId, params.id)
      .then((response) => {
        // setLoader({ isActive: false })
        if (response) {
          //  console.log(response.data)
          setGetrequireddoucmentno(response.data);
        }
      });
  };

  const [transactionId, setTransactionId] = useState([]);

  const [requireddetailsaddeddata, setRequiredDetailsaddeddata] = useState([]);

  const Detailsaddeddata = async () => {
    const agentId = jwt(localStorage.getItem("auth")).id;
    await user_service.Detailsaddeddata(agentId, params.id).then((response) => {
      setLoader({ isActive: false });
      if (response) {
        const requiredYesArray = response.data.data.filter(
          (item) => item.is_documentRequired === "yes"
        );
        setRequiredDetailsaddeddata(requiredYesArray);
      }
    });
  };

  const [orderedDeadlines, setOrderedDeadlines] = useState([]);

  const TransactionGetById = async () => {
    await user_service.transactionGetById(params.id).then((response) => {
      if (response) {
        setLoader({ isActive: false });
        const data = response.data;

        // Prepare dates array and sort them
        const deadlines = [
          { label: "Appraisal Deadline", date: data.appraisal_edeadline },
          { label: "Due Diligence Deadline", date: data.diligence_deadline },
          { label: "Financing Deadline", date: data.financing_deadline },
          { label: "Seller’s Disclosure Deadline", date: data.seller_deadline },
          { label: "Settlement Deadline", date: data.settlement_deadline },
        ]
          .filter((item) => item.date) // Remove null or undefined dates
          .sort((a, b) => new Date(a.date) - new Date(b.date)); // Sort by date

        setTransactionId(data);
        setOrderedDeadlines(deadlines);
      }
    });
  };
  // const TransactionGetById = async () => {
  //   await user_service.transactionGetById(params.id).then((response) => {
  //     if (response) {
  //       setLoader({ isActive: false });
  //       setTransactionId(response.data);
  //     }
  //   });
  // };

  const [transactionFundingRequest, setTransactionFundingRequest] =
    useState("");
  const TransactionFundingRequest = async () => {
    await user_service.transactionFundingRequest(params.id).then((response) => {
      if (response) {
        setLoader({ isActive: false });
        setTransactionFundingRequest(response.data);
      }
    });
  };

  const date = transactionId.timeStamp ? new Date(transactionId.timeStamp) : "";

  let formattedDate = "";
  let formattedTime = "";

  if (date) {
    const month = date.getUTCMonth() + 1;
    const day = date.getUTCDate();
    const year = date.getUTCFullYear();

    const hours = date.getUTCHours();
    const minutes = date.getUTCMinutes();
    const seconds = date.getUTCSeconds();

    formattedDate = `${month}/${day}/${year}`;
    formattedTime = `${hours}:${minutes}:${seconds}`;
  }

  useEffect(() => {
    Getrequireddoucmentno();
    Detailsaddeddata();
    TransactionGetById();
    TransactionFundingRequest(params.id);
  }, []);

  const [currentDateTime, setCurrentDateTime] = useState(null);

  useEffect(() => {
    const updateDateTime = () => {
      const options = {
        weekday: "long",
        year: "numeric",
        month: "long",
        day: "numeric",
        hour: "numeric",
        minute: "numeric",
        second: "numeric",
        timeZoneName: "short",
      };

      const localDateTime = new Date().toLocaleString("en-US", options);
      setCurrentDateTime(localDateTime);
    };

    const intervalId = setInterval(updateDateTime, 1000);

    return () => clearInterval(intervalId);
  }, []);

  const handleReview = () => {
    if (
      localStorage.getItem("auth") &&
      jwt(localStorage.getItem("auth")).contactType == "admin"
    ) {
      navigate(`/transaction-review/${params.id}`);
    }
  };

  const handleFunding = async (e) => {
    navigate(`/transaction-details-funding/${params.id}`);
  };

  const initialValues = {
    task_name: "",
    assign: "",
    description: "",
    task_document: "",
    task_date: "",
  };
  const [formValues, setFormValues] = useState(initialValues);
  const [formErrors, setFormErrors] = useState({});
  const [isSubmitClick, setISSubmitClick] = useState(false);

  const [file, setFile] = useState(null);
  const [data, setData] = useState("");

  const [taskVisible, settaskVisible] = useState(false);
  const [taskUpdateVisible, setTaskUpdateVisible] = useState(false);

  const handleTasks = () => {
    settaskVisible(true);
    setFormValues("");
    setData("");
  };

  const handleClose = () => {
    settaskVisible(false);
  };

  const handleChange = (e) => {
    const { name, value } = e.target;
    setFormValues({ ...formValues, [name]: value });
  };

  const handleFileUpload = async (e) => {
    const selectedFile = e.target.files[0];

    if (selectedFile) {
      setFile(selectedFile);

      const fileNameWithoutExtension = selectedFile.name.replace(
        /\.[^/.]+$/,
        ""
      );
      const formData = new FormData();
      formData.append("file", selectedFile);

      const config = {
        headers: {
          Accept: "application/json",
          "Content-Type": "multipart/form-data",
          Authorization: `Bearer ${localStorage.getItem("auth")}`,
        },
      };

      try {
        setLoader({ isActive: true });

        const uploadResponse = await axios.post(
          "https://api.brokeragentbase.com/upload",
          // "http://localhost:4000/upload",
          formData,
          config
        );
        const uploadedFileData = uploadResponse.data;

        setLoader({ isActive: false });
        setData(uploadedFileData);
        // setFormValues((prevValues) => ({
        //   ...prevValues,
        //   image: "",
        // }));
      } catch (error) {
        console.error("Error occurred during file upload:", error);
      }
    }
  };

  useEffect(() => {
    if (formValues && isSubmitClick) {
      validate();
    }
  }, [formValues]);

  const validate = () => {
    const values = formValues;
    const errors = {};
    if (!values.task_name) {
      errors.task_name = "Task Name is required";
    }
    setFormErrors(errors);
    return errors;
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
    setISSubmitClick(true);
    let checkValue = validate();
    if (_.isEmpty(checkValue)) {
      const userData = {
        task_name: formValues.task_name,
        assign: formValues.assign,
        description: formValues.description,
        task_date: formValues.task_date,
        task_document: data,
        transactionId: params.id,
      };
      try {
        setLoader({ isActive: true });
        const response = await user_service.TaskPost(userData);
        if (response && response.data) {
          setLoader({ isActive: false });
          const userDatan = {
            addedBy: jwt(localStorage.getItem("auth")).id,
            agentId: jwt(localStorage.getItem("auth")).id,
            transactionId: params.id,
            message: "Transaction Task Added",
            // transaction_property_address: "ADDRESS 2",
            note_type: "Task",
          };
          const responsen = await user_service.transactionNotes(userDatan);

          setToaster({
            types: "Task",
            isShow: true,
            message: "Task Added Successfully",
          });
          settaskVisible(false);
          TransactionTask();
          setTimeout(() => {
            setToaster((prevToaster) => ({ ...prevToaster, isShow: false }));
          }, 1000);
        } else {
          setLoader({ isActive: false });
          setToaster({
            types: "error",
            isShow: true,
            message: "Error",
          });
        }
      } catch (error) {
        setLoader({ isActive: false });
        setToaster({
          types: "error",
          isShow: true,
          message: "Error",
        });
      }
    }
  };

  const handleSubmitUpdate = async (e) => {
    if (formValues._id) {
      e.preventDefault();
      setISSubmitClick(true);
      let checkValue = validate();
      if (_.isEmpty(checkValue)) {
        const userData = {
          task_name: formValues.task_name,
          assign: formValues.assign,
          description: formValues.description,
          task_date: formValues.task_date,
          task_document: data ? data : formValues.task_document,
          transactionId: params.id,
        };
        try {
          setLoader({ isActive: true });
          const response = await user_service.transactionTaskUpdate(
            formValues._id,
            userData
          );
          if (response && response.data) {
            setLoader({ isActive: false });
            const userDatan = {
              addedBy: jwt(localStorage.getItem("auth")).id,
              agentId: jwt(localStorage.getItem("auth")).id,
              transactionId: params.id,
              message: "Transaction Task Update",
              // transaction_property_address: "ADDRESS 2",
              note_type: "Task",
            };
            const responsen = await user_service.transactionNotes(userDatan);

            setToaster({
              types: "Task",
              isShow: true,
              message: "Task Update Successfully",
            });
            setTaskUpdateVisible(false);
            TransactionTask();
            setTimeout(() => {
              setToaster((prevToaster) => ({ ...prevToaster, isShow: false }));
            }, 1000);
          } else {
            setLoader({ isActive: false });
            setToaster({
              types: "error",
              isShow: true,
              message: "Error",
            });
          }
        } catch (error) {
          setLoader({ isActive: false });
          setToaster({
            types: "error",
            isShow: true,
            message: "Error",
          });
        }
      }
    }
  };

  const [getTasks, setGetTasks] = useState([]);
  const [numPages, setNumPages] = useState(null);
  const onDocumentLoadSuccess = ({ numPages }) => {
    setNumPages(numPages);
  };

  const isImage = (fileUrl) => {
    const imageExtensions = ["jpg", "jpeg", "png", "gif", "bmp", "webp"];
    const fileExtension = fileUrl.split(".").pop().toLowerCase();
    return imageExtensions.includes(fileExtension);
  };

  const [filteredTasks, setFilteredTasks] = useState([]);
  const [filteredTaskNoDue, setFilteredTaskNoDue] = useState([]);

  const [filteredTasksOverDue, setFilteredTasksOverDue] = useState([]);

  const TransactionTask = async () => {
    try {
      const response = await user_service.transactionTask(params.id);
      if (response) {
        const tasks = response.data.data;
        setGetTasks(tasks);

        // Filter tasks for the next 7 days
        const currentDate = new Date();
        const next7DaysDate = new Date();
        next7DaysDate.setDate(currentDate.getDate() + 7);

        // new Date(dataItem.task_date) < new Date()

        const overdueTasks = tasks.filter((task) => {
          const taskDate = new Date(task.task_date);
          return taskDate < currentDate;
        });

        setFilteredTasksOverDue(overdueTasks);

        const filtered = tasks.filter((task) => {
          const taskDate = new Date(task.task_date);
          return (
            taskDate >= currentDate &&
            taskDate <= next7DaysDate &&
            task.task_date // Ensure task_date exists
          );
        });

        setFilteredTasks(filtered);

        currentDate.setHours(0, 0, 0, 0); // Normalize to start of the day

        const filteredNoDue = tasks.filter((task) => {
          const taskDate = new Date(task.task_date);
          return (
            task.task_date && // Ensure task_date exists
            taskDate >= currentDate // Task date is today or in the future
          );
        });
        setFilteredTaskNoDue(filteredNoDue);
      }
    } catch (error) {
      console.error("Error fetching tasks:", error);
    }
  };

  const handleEdit = async (id) => {
    setTaskUpdateVisible(true);
    await user_service.transactionTaskGetId(id).then((response) => {
      if (response) {
        setLoader({ isActive: false });
        setFormValues(response.data);
      }
    });
  };

  const handleDelete = async (id) => {
    Swal.fire({
      title: "Are you sure?",
      text: "It will be permanently deleted task!",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Yes, delete it!",
    }).then(async (result) => {
      if (result.isConfirmed) {
        try {
          const response = await user_service.transactionTaskDelete(id);
          if (response) {
            setLoader({ isActive: false });
            const userDatan = {
              addedBy: jwt(localStorage.getItem("auth")).id,
              agentId: jwt(localStorage.getItem("auth")).id,
              transactionId: params.id,
              message: "Transaction Task Delete",
              // transaction_property_address: "ADDRESS 2",
              note_type: "Task",
            };
            const responsen = await user_service.transactionNotes(userDatan);
            if (response) {
              Swal.fire("Deleted!", "Task is deleted permanently.", "success");
              //setFormValues(response.data);
            }
            TransactionTask();
          }
        } catch (error) {
          Swal.fire(
            "Error",
            "An error occurred while deleting the file.",
            "error"
          );
          console.error(error);
        }
      }
    });
  };

  const handleCloseUpdate = () => {
    setTaskUpdateVisible(false);
  };

  const handleChangeCheck = async (
    id,
    task_alldata,
    overDue_data,
    sevenday_data,
    noDue_data
  ) => {
    if (task_alldata) {
      console.log(task_alldata, "1111");
      const taskToUpdate = getTasks.find((task) => task._id === id);
      if (!taskToUpdate) return;
      const newCheckedValue = taskToUpdate.checktask === "yes" ? "no" : "yes";
      setGetTasks((prevTasks) =>
        prevTasks.map((task) =>
          task._id === id ? { ...task, checktask: newCheckedValue } : task
        )
      );
      await handleCheckboxChange(id, newCheckedValue);
    } else if (overDue_data) {
      const taskToUpdate = filteredTasksOverDue.find((task) => task._id === id);
      if (!taskToUpdate) return;
      const newCheckedValue = taskToUpdate.checktask === "yes" ? "no" : "yes";
      setFilteredTasksOverDue((prevTasks) =>
        prevTasks.map((task) =>
          task._id === id ? { ...task, checktask: newCheckedValue } : task
        )
      );
      await handleCheckboxChange(id, newCheckedValue);
    } else if (sevenday_data) {
      console.log(sevenday_data, "3333333");
      const taskToUpdate = filteredTasks.find((task) => task._id === id);
      if (!taskToUpdate) return;
      const newCheckedValue = taskToUpdate.checktask === "yes" ? "no" : "yes";
      setFilteredTasks((prevTasks) =>
        prevTasks.map((task) =>
          task._id === id ? { ...task, checktask: newCheckedValue } : task
        )
      );
      await handleCheckboxChange(id, newCheckedValue);
    } else if (noDue_data) {
      console.log(noDue_data, "44444444");
      const taskToUpdate = filteredTaskNoDue.find((task) => task._id === id);
      if (!taskToUpdate) return;
      const newCheckedValue = taskToUpdate.checktask === "yes" ? "no" : "yes";
      setFilteredTaskNoDue((prevTasks) =>
        prevTasks.map((task) =>
          task._id === id ? { ...task, checktask: newCheckedValue } : task
        )
      );
      await handleCheckboxChange(id, newCheckedValue);
    }
  };

  const handleCheckboxChange = async (id, checked) => {
    try {
      const userData = { checktask: checked };
      setLoader({ isActive: true });
      const response = await user_service.transactionTaskUpdate(id, userData);
      if (response && response.data) {
        console.log(`Task ${id} updated successfully!`);
        setLoader({ isActive: false });
        TransactionTask();
      } else {
        console.error(`Failed to update Task ${id}.`);
        setLoader({ isActive: false });
      }
    } catch (error) {
      console.error(`Error updating Task ${id}:`, error);
      setLoader({ isActive: false });
    }
  };

  useEffect(() => {
    TransactionTask();
  }, []);

  const handleDragEnd = async (result) => {
    if (!result.destination) return; // If dropped outside the list

    const reorderedTasks = Array.from(getTasks);
    const [movedTask] = reorderedTasks.splice(result.source.index, 1);
    reorderedTasks.splice(result.destination.index, 0, movedTask);
    setGetTasks(reorderedTasks);

    const updatedOrder = reorderedTasks.map((task, index) => ({
      _id: task._id,
      serial_no: index,
    }));

    try {
      setLoader({ isActive: true });
      const response = await user_service.taskUpdateSerial({
        tasks: updatedOrder,
      });
      if (response && response.data) {
        setLoader({ isActive: false });
        TransactionTask(); // Refresh the tasks list
      } else {
        console.log("error");
      }
    } catch (error) {
      console.error("Error updating task order:", error);
    }
  };


  const handleStausUpdate = async () => {
   if(params.id){
     try {
       const userData = {
        summaryStatus: "inActive",
       };
       await user_service
         .TransactionUpdate(params.id, userData)
         .then((response) => {
           if (response.status === 200) {
             const userDatan = {
               addedBy: jwt(localStorage.getItem("auth")).id,
               agentId: jwt(localStorage.getItem("auth")).id,
               transactionId: params.id,
               message: "Introduction to Transaction Summary",
               // transaction_property_address: "ADDRESS 2",
               note_type: "Transaction Summary",
             };
             const responsen = user_service.transactionNotes(userDatan);
             TransactionGetById(params.id);
             setToaster({
               type: "Add Transaction",
               isShow: true,
               message: "Transaction summary viewed successfully.",
             });
             setTimeout(() => {
               setToaster((prevToaster) => ({ ...prevToaster, isShow: false }));
             }, 3000);
           } else {
             setLoader({ isActive: false });
             setToaster({
               types: "error",
               isShow: true,
               message: "Error",
             });
           }
         });
     } catch (error) {
       setLoader({ isActive: false });
       setToaster({
         types: "error",
         isShow: true,
         message: "Error",
       });
     }
   }
  };

  return (
    <div className="bg-secondary float-left w-100 pt-4 mb-4">
      <Loader isActive={isActive} />
      {isShow && (
        <Toaster
          types={types}
          isShow={isShow}
          toasterBody={toasterBody}
          message={message}
        />
      )}
      <main className="page-wrapper">
        <div className="">
          <div className="content-overlay transaction_tab_view mt-0">
            <div className="row">
              <AllTab />

              {/* {console.log(transactionId.summaryStatus)} */}

              {transactionId.summaryStatus === "active" ? (
                <div
                  className="modal show"
                  role="dialog"
                  style={{ display: "block" }}
                >
                  <div
                    className="modal-dialog modal-lg modal-dialog-scrollable"
                    role="document"
                  >
                    <div className="modal-content mt-5">
                      <div className="modal-header">
                        <h4 className="modal-title">
                          The Importance of Creating Your Transaction Early
                        </h4>
                      </div>

                      <div className="modal-body fs-sm">
                        <span>
                          Creating your transaction as soon as you take on a new
                          listing or begin working with a new buyer is
                          essential. Early setup and regular updates allow you
                          to maximize the benefits of our software, streamline
                          your workflow, comply with state-mandated regulations,
                          and improve communication. This ensures a smoother,
                          more efficient transaction process for everyone
                          involved.
                        </span>
                        <br />
                        <span>
                          Our real estate transaction management software is
                          specifically designed to simplify and automate the
                          transaction process. Key benefits include:
                        </span>
                        <ul
                          className="mt-3 ps-5"
                          style={{ listStyle: "outside" }}
                        >
                          <li>
                            <h6 className="mb-1">
                              Centralized Data and Documents:
                            </h6>
                            <p>
                              Securely store all transaction information in one
                              location for easy access during the transaction
                              and for future reference.
                            </p>
                          </li>
                          <li>
                            <h6 className="mb-1">Task Automation:</h6>
                            <p>
                              Automate repetitive tasks like document generation
                              and deadline reminders to save time and reduce
                              errors.
                            </p>
                          </li>
                          <li>
                            <h6 className="mb-1">Improved Communication:</h6>
                            <p>
                              Utilize a single platform for real-time updates
                              and seamless collaboration with all transaction
                              parties.
                            </p>
                          </li>
                          <li>
                            <h6 className="mb-1">
                              Enhanced Client Experience:
                            </h6>
                            <p>
                              Provide clients with real-time access to documents
                              and transaction progress, keeping them informed
                              and engaged.
                            </p>
                          </li>
                          <li>
                            <h6 className="mb-1">MLS Data Integration:</h6>
                            <p>
                              Connect directly to property details, reducing
                              manual data input and improving accuracy.
                            </p>
                          </li>
                          <li>
                            <h6 className="mb-1">Compliance Management:</h6>
                            <p>
                              Track required disclosures and documents to ensure
                              adherence to legal and regulatory standards.
                            </p>
                          </li>

                          <li>
                            <h6 className="mb-1">Financial Insights:</h6>
                            <p>
                              Monitor commission calculations and gain
                              visibility into transaction profitability.
                            </p>
                          </li>

                          <li>
                            <h6 className="mb-1">Seamless Collaboration:</h6>
                            <p>
                              Work efficiently with agents, brokers, lenders,
                              title companies, and clients.
                            </p>
                          </li>

                          <li>
                            <h6 className="mb-1">Data Security:</h6>
                            <p>
                              Protect sensitive documents and information with
                              secure, centralized storage.
                            </p>
                          </li>
                        </ul>
                        <div className="float-end mb-4">
                          <OverlayTrigger
                            placement="left"
                            overlay={<Tooltip>Close Modal</Tooltip>}
                          >
                            <button
                              className="btn-close shadow border p-3"
                              id="closeModal"
                              type="button"
                              onClick={handleStausUpdate}
                            ></button>
                          </OverlayTrigger>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              ) : (
                <div className="col-md-12">
                  <div className="bg-light border rounded-3 mb-3 p-3">
                    <div className="row">
                      <div className="col-md-9">
                        <h3 className="text-left mb-2">Transaction Status</h3>
                        <p className="mb-0" id="text-document">
                          Here you can see your transaction details and can edit
                          them easily.
                        </p>
                      </div>
                      <div className="col-md-3 py-2 justify-content-end d-flex align-items-center">
                        {transactionFundingRequest?.data?.length > 0 ? (
                          <>
                            {transactionId.filing === "filed_complted" ? (
                              <span
                                className="btn btn-secondary btn-sm"
                                style={{
                                  cursor: "no-drop",
                                  color: "#ffffff",
                                  backgroundColor: "#445985",
                                }}
                              >
                                Filed Transaction
                              </span>
                            ) : (
                              <button
                                onClick={handleReview}
                                className="btn btn-secondary btn-sm"
                              >
                                In Review
                              </button>
                            )}
                          </>
                        ) : (
                          <>
                            {getrequireddoucmentno?.totalDetailsNoRequired ==
                            "0" ? (
                              getrequireddoucmentno?.totalContactNoRequired ==
                                "0" &&
                              getrequireddoucmentno?.totalpropertydetails ==
                                "0" &&
                              getrequireddoucmentno?.totalDocumentNoRequired ==
                                "0" &&
                              getrequireddoucmentno?.totalComissionDetails ==
                                "0" ? (
                                transactionId.phase === "canceled" ? (
                                  localStorage.getItem("auth") &&
                                  jwt(localStorage.getItem("auth"))
                                    .contactType === "admin" ? (
                                    <button
                                      className="btn btn-secondary btn-sm"
                                      onClick={handleFunding}
                                    >
                                      <span className="text-success">
                                        Submit Transaction
                                      </span>
                                    </button>
                                  ) : (
                                    <button className="btn btn-secondary btn-sm">
                                      <span className="text-success">
                                        Submit Transaction In Progress
                                      </span>
                                    </button>
                                  )
                                ) : (
                                  <button
                                    className="btn btn-secondary btn-sm"
                                    href={`/transaction-details-funding/${params.id}`}
                                    onClick={handleFunding}
                                  >
                                    <span className="text-success">
                                      Submit Your Transaction
                                    </span>
                                  </button>
                                )
                              ) : (
                                <button
                                  className="btn btn-secondary btn-sm"
                                  style={{
                                    cursor: "no-drop",
                                    color: "#cdc4c4",
                                    backgroundColor: "#dedef7",
                                  }}
                                  title="To submit your transaction for final review, please ensure all required items and documents are completed. Review each transaction page above—any missing information or documents will be marked with a red circle and a number. Make sure all required fields are filled out before submitting to the office."
                                >
                                  <span className="text-success">
                                    Submit Transaction In Progress
                                  </span>
                                </button>
                              )
                            ) : (
                              <button
                                className="btn btn-secondary btn-sm"
                                style={{
                                  cursor: "no-drop",
                                  color: "#cdc4c4",
                                  backgroundColor: "#dedef7",
                                }}
                                title="To submit your transaction for final review, please ensure all required items and documents are completed. Review each transaction page above—any missing information or documents will be marked with a red circle and a number. Make sure all required fields are filled out before submitting to the office."
                              >
                                <span className="text-success">
                                  Submit Transaction In Progress
                                </span>
                              </button>
                            )}
                          </>
                        )}
                      </div>
                    </div>
                  </div>
                  {/* <StatusBar /> */}
                  <div className="bg-light p-3 border-0 rounded-3 shadow-sm float-start w-100">
                    <div className="row mt-4">
                      <div className="">
                        <h4 className="pull-left">Tasks</h4>
                        <button
                          className="btn btn-primary pull-right"
                          data-toggle="modal"
                          data-target="#transaction_task"
                          onClick={handleTasks}
                        >
                          Add New
                        </button>
                      </div>

                      <div className="accordion" id="accordionExample">
                        <div className="notes-main task_lists bg-light border-top border-0 p-3 rounded-3">
                          <div>
                            {orderedDeadlines.length > 0 ? (
                              <h4 className="py-3">Property Date</h4>
                            ) : (
                              ""
                            )}
                            {orderedDeadlines.map((deadline, index) => (
                              <div className="row mb-3">
                                <div
                                  key={index}
                                  className="d-flex align-items-center justify-content-start float-start w-100"
                                >
                                  <div className="float-start w-100">
                                    <div className="d-flex align-items-center justify-content-between">
                                      <strong>{deadline.label}</strong>
                                      <div className="dropdown d-flex align-items-center justify-content-between">
                                        <p
                                          className="me-3 my-0"
                                          style={{
                                            fontSize: "12px",
                                            color: "#81321c",
                                          }}
                                        >
                                          {new Date(
                                            deadline.date
                                          ).toLocaleDateString("en-US", {
                                            month: "2-digit",
                                            day: "2-digit",
                                            year: "numeric",
                                          })}
                                        </p>
                                      </div>
                                    </div>
                                    <NavLink>
                                      {transactionId?.represent === "buyer"
                                        ? "Buyer"
                                        : transactionId?.represent === "seller"
                                        ? "Seller"
                                        : transactionId?.represent === "both"
                                        ? "Buyer & Seller"
                                        : transactionId?.represent ===
                                          "referral"
                                        ? "Referral"
                                        : ""}
                                    </NavLink>
                                  </div>
                                </div>
                              </div>
                            ))}

                            <div className="accordion-item mb-4">
                              <h2
                                className="accordion-header text-center "
                                id="task_transaction"
                              >
                                <button
                                  className="accordion-button collapsed "
                                  type="button"
                                  data-bs-toggle="collapse"
                                  data-bs-target="#collapseTwo"
                                  aria-expanded="false"
                                  aria-controls="collapseTwo"
                                ></button>
                              </h2>
                              <div
                                className="accordion-collapse collapse"
                                aria-labelledby="task_transaction"
                                data-bs-parent="#accordionExample"
                                id="collapseTwo"
                              >
                                <div className="accordion-body">
                                  <h4 className="text-center py-3">Tasks</h4>
                                  <DragDropContext onDragEnd={handleDragEnd}>
                                    <Droppable droppableId="tasks">
                                      {(provided) => (
                                        <div
                                          ref={provided.innerRef}
                                          {...provided.droppableProps}
                                          className="task-list"
                                        >
                                          {getTasks && getTasks.length > 0 ? (
                                            getTasks.map((dataItem, index) => (
                                              <Draggable
                                                key={dataItem._id}
                                                draggableId={dataItem._id}
                                                index={index}
                                              >
                                                {(provided) => (
                                                  <div
                                                    ref={provided.innerRef}
                                                    {...provided.draggableProps}
                                                    {...provided.dragHandleProps}
                                                    className="row mb-5"
                                                  >
                                                    <div className="">
                                                      <div className="d-flex align-items-center justify-content-start float-start w-100">
                                                        <div className="ms-0 float-start w-100">
                                                          <div className="d-flex align-items-center justify-content-between">
                                                            <strong>
                                                              <input
                                                                className="me-2"
                                                                type="checkbox"
                                                                id={`checkbox-${dataItem._id}`}
                                                                name="checktask"
                                                                checked={
                                                                  dataItem.checktask ===
                                                                  "yes"
                                                                }
                                                                onChange={() =>
                                                                  handleChangeCheck(
                                                                    dataItem._id,
                                                                    "task_alldata"
                                                                  )
                                                                }
                                                              />
                                                              {
                                                                dataItem.task_name
                                                              }
                                                            </strong>

                                                            <strong
                                                              style={{
                                                                color: "green",
                                                              }}
                                                            >
                                                              {dataItem.checktask ===
                                                              "yes"
                                                                ? "Completed"
                                                                : ""}
                                                            </strong>
                                                            <div className="dropdown d-flex align-items-center justify-content-between">
                                                              {dataItem.task_date ? (
                                                                <p
                                                                  className="me-3 my-0"
                                                                  style={{
                                                                    fontSize:
                                                                      "12px",
                                                                    color:
                                                                      "#81321c",
                                                                  }}
                                                                >
                                                                  {new Date(
                                                                    dataItem.task_date
                                                                  ).toLocaleDateString(
                                                                    "en-US",
                                                                    {
                                                                      month:
                                                                        "2-digit",
                                                                      day: "2-digit",
                                                                      year: "numeric",
                                                                    }
                                                                  )}
                                                                </p>
                                                              ) : (
                                                                ""
                                                              )}
                                                              <button
                                                                className="border-0 dropdown-toggle bg-transparent pe-0"
                                                                type="button"
                                                                id="dropdownMenuButton"
                                                                data-bs-toggle="dropdown"
                                                                aria-expanded="false"
                                                              >
                                                                <i
                                                                  className="fa fa-ellipsis-h opacity-80 mb-0"
                                                                  aria-hidden="true"
                                                                ></i>
                                                              </button>
                                                              <ul
                                                                className="dropdown-menu"
                                                                aria-labelledby="dropdownMenuButton"
                                                              >
                                                                <li>
                                                                  <a
                                                                    className="dropdown-item"
                                                                    data-toggle="modal"
                                                                    data-target="#task-update"
                                                                    onClick={() =>
                                                                      handleEdit(
                                                                        dataItem._id
                                                                      )
                                                                    }
                                                                  >
                                                                    <i
                                                                      className="h2 fi-edit opacity-80 mb-0 me-2"
                                                                      aria-hidden="true"
                                                                    ></i>
                                                                    Edit
                                                                  </a>
                                                                </li>
                                                                <li>
                                                                  <a
                                                                    className="dropdown-item"
                                                                    onClick={() =>
                                                                      handleDelete(
                                                                        dataItem._id
                                                                      )
                                                                    }
                                                                  >
                                                                    <i className="fas fa-trash me-2"></i>{" "}
                                                                    Delete
                                                                    document
                                                                  </a>
                                                                </li>
                                                              </ul>
                                                            </div>
                                                          </div>
                                                          <div className="d-flex">
                                                            <NavLink
                                                              style={{
                                                                color:
                                                                  "#46b0b0",
                                                                fontSize:
                                                                  "14px",
                                                              }}
                                                            >
                                                              {dataItem.assign}
                                                            </NavLink>
                                                            <p
                                                              className="ms-3"
                                                              style={{
                                                                backgroundColor:
                                                                  "#ffefd2",
                                                                fontSize:
                                                                  "14px",
                                                                padding:
                                                                  "2px 4px",
                                                              }}
                                                            >
                                                              {
                                                                transactionId
                                                                  ?.propertyDetail
                                                                  ?.streetAddress
                                                              }
                                                            </p>
                                                          </div>
                                                          <p
                                                            className="mb-2"
                                                            style={{
                                                              fontSize: "14px",
                                                              textAlign:
                                                                "justify",
                                                            }}
                                                          >
                                                            {
                                                              dataItem.description
                                                            }
                                                          </p>
                                                        </div>
                                                      </div>
                                                    </div>
                                                  </div>
                                                )}
                                              </Draggable>
                                            ))
                                          ) : (
                                            <div className="bg-light border rounded-3 p-3">
                                              <p className="text-center my-5">
                                                No tasks found.
                                              </p>
                                            </div>
                                          )}
                                          {provided.placeholder}
                                        </div>
                                      )}
                                    </Droppable>
                                  </DragDropContext>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>

                        <div className="accordion-item mb-4">
                          <h2 className="accordion-header" id="overdue_task">
                            <button
                              className="accordion-button collapsed"
                              type="button"
                              data-bs-toggle="collapse"
                              data-bs-target="#collapseThree"
                              aria-expanded="false"
                              aria-controls="collapseThree"
                            >
                              Overdue Tasks{" "}
                              <span className="badge_require_task">
                                {filteredTasksOverDue.length}
                              </span>
                            </button>
                          </h2>
                          <div
                            className="accordion-collapse collapse"
                            aria-labelledby="overdue_task"
                            data-bs-parent="#accordionExample"
                            id="collapseThree"
                          >
                            <div className="accordion-body">
                              {filteredTasksOverDue &&
                              filteredTasksOverDue.length > 0 ? (
                                filteredTasksOverDue
                                  .filter(
                                    (dataItem) =>
                                      new Date(dataItem.task_date) < new Date()
                                  ) // Filter overdue tasks
                                  .map((dataItem) => (
                                    <div
                                      className="row mb-5"
                                      key={dataItem._id}
                                    >
                                      <div className="">
                                        <div className="d-flex align-items-center justify-content-start float-start w-100">
                                          <div className="ms-0 float-start w-100">
                                            <div className="d-flex align-items-center justify-content-between">
                                              <strong className="d-flex">
                                                <input
                                                  className="me-2"
                                                  type="checkbox"
                                                  id={`checkbox-${dataItem._id}`}
                                                  name="checktask"
                                                  checked={
                                                    dataItem.checktask === "yes"
                                                  }
                                                  onChange={() =>
                                                    handleChangeCheck(
                                                      dataItem._id,
                                                      "overDue_data"
                                                    )
                                                  }
                                                />
                                                {dataItem.task_name}
                                              </strong>

                                              <strong
                                                style={{
                                                  color: "green",
                                                }}
                                              >
                                                {dataItem.checktask === "yes"
                                                  ? "Completed"
                                                  : ""}
                                              </strong>
                                              <div className="dropdown d-flex align-items-center justify-content-between">
                                                {dataItem.task_date ? (
                                                  <p
                                                    className="me-3 my-0"
                                                    style={{
                                                      fontSize: "12px",
                                                      color: "#81321c",
                                                    }}
                                                  >
                                                    {new Date(
                                                      dataItem.task_date
                                                    ).toLocaleDateString(
                                                      "en-US",
                                                      {
                                                        month: "2-digit",
                                                        day: "2-digit",
                                                        year: "numeric",
                                                      }
                                                    )}
                                                  </p>
                                                ) : (
                                                  ""
                                                )}
                                                <button
                                                  className="border-0 dropdown-toggle bg-transparent pe-0"
                                                  type="button"
                                                  id="dropdownMenuButton"
                                                  data-bs-toggle="dropdown"
                                                  aria-expanded="false"
                                                >
                                                  <i
                                                    className="fa fa-ellipsis-h opacity-80 mb-0"
                                                    aria-hidden="true"
                                                  ></i>
                                                </button>
                                                <ul
                                                  className="dropdown-menu"
                                                  aria-labelledby="dropdownMenuButton"
                                                >
                                                  <li>
                                                    <a
                                                      className="dropdown-item"
                                                      data-toggle="modal"
                                                      data-target="#task-update"
                                                      onClick={() =>
                                                        handleEdit(dataItem._id)
                                                      }
                                                    >
                                                      <i
                                                        className="h2 fi-edit opacity-80 mb-0 me-2"
                                                        aria-hidden="true"
                                                      ></i>
                                                      Edit
                                                    </a>
                                                  </li>
                                                  <li>
                                                    <a
                                                      className="dropdown-item"
                                                      onClick={() =>
                                                        handleDelete(
                                                          dataItem._id
                                                        )
                                                      }
                                                    >
                                                      <i className="fas fa-trash me-2"></i>{" "}
                                                      Delete document
                                                    </a>
                                                  </li>
                                                </ul>
                                              </div>
                                            </div>
                                            <div className="d-flex">
                                              <NavLink
                                                style={{
                                                  color: "#46b0b0",
                                                  fontSize: "14px",
                                                }}
                                              >
                                                {dataItem.assign}
                                              </NavLink>
                                              <p
                                                className="ms-3"
                                                style={{
                                                  backgroundColor: "#ffefd2",
                                                  fontSize: "14px",
                                                  padding: "2px 4px",
                                                }}
                                              >
                                                {
                                                  transactionId?.propertyDetail
                                                    ?.streetAddress
                                                }
                                              </p>
                                            </div>
                                            <p
                                              className="mb-2"
                                              style={{
                                                fontSize: "14px",
                                                textAlign: "justify",
                                              }}
                                            >
                                              {dataItem.description}
                                            </p>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                  ))
                              ) : (
                                <div className="bg-light border rounded-3 p-3">
                                  <p className="text-center my-5">
                                    No overdue tasks found.
                                  </p>
                                </div>
                              )}
                            </div>
                          </div>
                        </div>

                        <div className="accordion-item mb-4">
                          <h2 className="accordion-header" id="seven_days">
                            <button
                              className="accordion-button collapsed"
                              type="button"
                              data-bs-toggle="collapse"
                              data-bs-target="#collapseFour"
                              aria-expanded="false"
                              aria-controls="collapseFour"
                            >
                              Next 7 Days{" "}
                              <span className="badge_require_task">
                                {filteredTasks.length}
                              </span>
                            </button>
                          </h2>
                          <div
                            className="accordion-collapse collapse"
                            aria-labelledby="seven_days"
                            data-bs-parent="#accordionExample"
                            id="collapseFour"
                          >
                            <div className="accordion-body">
                              {filteredTasks && filteredTasks.length > 0 ? (
                                filteredTasks.map((dataItem) => (
                                  <div className="row mb-5" key={dataItem._id}>
                                    <div className="">
                                      <div className="d-flex align-items-center justify-content-start float-start w-100">
                                        <div className="ms-0 float-start w-100">
                                          <div className="d-flex align-items-center justify-content-between">
                                            <strong className="d-flex">
                                              <input
                                                className="me-2"
                                                type="checkbox"
                                                id={`checkbox-${dataItem._id}`}
                                                name="checktask"
                                                checked={
                                                  dataItem.checktask === "yes"
                                                }
                                                onChange={() =>
                                                  handleChangeCheck(
                                                    dataItem._id,
                                                    "sevenday_data"
                                                  )
                                                }
                                              />
                                              {dataItem.task_name}
                                            </strong>

                                            <strong
                                              style={{
                                                color: "green",
                                              }}
                                            >
                                              {dataItem.checktask === "yes"
                                                ? "Completed"
                                                : ""}
                                            </strong>
                                            <div className="dropdown d-flex align-items-center justify-content-between">
                                              {dataItem.task_date ? (
                                                <p
                                                  className="me-3 my-0"
                                                  style={{
                                                    fontSize: "12px",
                                                    color: "#81321c",
                                                  }}
                                                >
                                                  {new Date(
                                                    dataItem.task_date
                                                  ).toLocaleDateString(
                                                    "en-US",
                                                    {
                                                      month: "2-digit",
                                                      day: "2-digit",
                                                      year: "numeric",
                                                    }
                                                  )}
                                                </p>
                                              ) : (
                                                ""
                                              )}
                                              <button
                                                className="border-0 dropdown-toggle bg-transparent pe-0"
                                                type="button"
                                                id="dropdownMenuButton"
                                                data-bs-toggle="dropdown"
                                                aria-expanded="false"
                                              >
                                                <i
                                                  className="fa fa-ellipsis-h opacity-80 mb-0"
                                                  aria-hidden="true"
                                                ></i>
                                              </button>
                                              <ul
                                                className="dropdown-menu"
                                                aria-labelledby="dropdownMenuButton"
                                              >
                                                <li>
                                                  <a
                                                    className="dropdown-item"
                                                    data-toggle="modal"
                                                    data-target="#task-update"
                                                    onClick={() =>
                                                      handleEdit(dataItem._id)
                                                    }
                                                  >
                                                    <i
                                                      className="h2 fi-edit opacity-80 mb-0 me-2"
                                                      aria-hidden="true"
                                                    ></i>
                                                    Edit
                                                  </a>
                                                </li>
                                                <li>
                                                  <a
                                                    className="dropdown-item"
                                                    onClick={() =>
                                                      handleDelete(dataItem._id)
                                                    }
                                                  >
                                                    <i className="fas fa-trash me-2"></i>{" "}
                                                    Delete document
                                                  </a>
                                                </li>
                                              </ul>
                                            </div>
                                          </div>
                                          <div className="d-flex">
                                            <NavLink
                                              style={{
                                                color: "#46b0b0",
                                                fontSize: "14px",
                                              }}
                                            >
                                              {dataItem.assign}
                                            </NavLink>
                                            <p
                                              className="ms-3"
                                              style={{
                                                backgroundColor: "#ffefd2",
                                                fontSize: "14px",
                                                padding: "2px 4px",
                                              }}
                                            >
                                              {
                                                transactionId?.propertyDetail
                                                  ?.streetAddress
                                              }
                                            </p>
                                          </div>
                                          <p
                                            className="mb-2"
                                            style={{
                                              fontSize: "14px",
                                              textAlign: "justify",
                                            }}
                                          >
                                            {dataItem.description}
                                          </p>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                ))
                              ) : (
                                <div className="bg-light border rounded-3 p-3">
                                  <p className="text-center my-5">
                                    No tasks found for the next 7 days.
                                  </p>
                                </div>
                              )}
                            </div>
                          </div>
                        </div>

                        <div className="accordion-item mb-4">
                          <h2 className="accordion-header" id="no_dueDate">
                            <button
                              className="accordion-button collapsed"
                              type="button"
                              data-bs-toggle="collapse"
                              data-bs-target="#collapseFive"
                              aria-expanded="false"
                              aria-controls="collapseFive"
                            >
                              No Due Date{" "}
                              <span className="badge_require_task">
                                {filteredTaskNoDue.length}
                              </span>
                            </button>
                          </h2>
                          <div
                            className="accordion-collapse collapse"
                            aria-labelledby="no_dueDate"
                            data-bs-parent="#accordionExample"
                            id="collapseFive"
                          >
                            <div className="accordion-body">
                              {filteredTaskNoDue &&
                              filteredTaskNoDue.length > 0 ? (
                                filteredTaskNoDue.map((dataItem) => (
                                  <div className="row mb-5" key={dataItem._id}>
                                    <div className="d-flex align-items-center justify-content-start float-start w-100">
                                      <div className="ms-0 float-start w-100">
                                        <div className="d-flex align-items-center justify-content-between">
                                          <strong className="d-flex">
                                            <input
                                              className="me-2"
                                              type="checkbox"
                                              id={`checkbox-${dataItem._id}`}
                                              name="checktask"
                                              checked={
                                                dataItem.checktask === "yes"
                                              }
                                              onChange={() =>
                                                handleChangeCheck(
                                                  dataItem._id,
                                                  "noDue_data"
                                                )
                                              }
                                            />
                                            {dataItem.task_name}
                                          </strong>

                                          <strong
                                            style={{
                                              color: "green",
                                            }}
                                          >
                                            {dataItem.checktask === "yes"
                                              ? "Completed"
                                              : ""}
                                          </strong>
                                          <div className="dropdown d-flex align-items-center justify-content-between">
                                            {dataItem.task_date ? (
                                              <p
                                                className="me-3 my-0"
                                                style={{
                                                  fontSize: "12px",
                                                  color: "#81321c",
                                                }}
                                              >
                                                {new Date(
                                                  dataItem.task_date
                                                ).toLocaleDateString("en-US", {
                                                  month: "2-digit",
                                                  day: "2-digit",
                                                  year: "numeric",
                                                })}
                                              </p>
                                            ) : (
                                              ""
                                            )}
                                            <button
                                              className="border-0 dropdown-toggle bg-transparent pe-0"
                                              type="button"
                                              id="dropdownMenuButton"
                                              data-bs-toggle="dropdown"
                                              aria-expanded="false"
                                            >
                                              <i
                                                className="fa fa-ellipsis-h opacity-80 mb-0"
                                                aria-hidden="true"
                                              ></i>
                                            </button>
                                            <ul
                                              className="dropdown-menu"
                                              aria-labelledby="dropdownMenuButton"
                                            >
                                              <li>
                                                <a
                                                  className="dropdown-item"
                                                  data-toggle="modal"
                                                  data-target="#task-update"
                                                  onClick={() =>
                                                    handleEdit(dataItem._id)
                                                  }
                                                >
                                                  <i
                                                    className="h2 fi-edit opacity-80 mb-0 me-2"
                                                    aria-hidden="true"
                                                  ></i>
                                                  Edit
                                                </a>
                                              </li>
                                              <li>
                                                <a
                                                  className="dropdown-item"
                                                  onClick={() =>
                                                    handleDelete(dataItem._id)
                                                  }
                                                >
                                                  <i className="fas fa-trash me-2"></i>{" "}
                                                  Delete document
                                                </a>
                                              </li>
                                            </ul>
                                          </div>
                                        </div>
                                        <div className="d-flex">
                                          <NavLink
                                            style={{
                                              color: "#46b0b0",
                                              fontSize: "14px",
                                            }}
                                          >
                                            {dataItem.assign}
                                          </NavLink>
                                          <p
                                            className="ms-3"
                                            style={{
                                              backgroundColor: "#ffefd2",
                                              fontSize: "14px",
                                              padding: "2px 4px",
                                            }}
                                          >
                                            {
                                              transactionId?.propertyDetail
                                                ?.streetAddress
                                            }
                                          </p>
                                        </div>
                                        <p
                                          className="mb-2"
                                          style={{
                                            fontSize: "14px",
                                            textAlign: "justify",
                                          }}
                                        >
                                          {dataItem.description}
                                        </p>
                                      </div>
                                    </div>
                                  </div>
                                ))
                              ) : (
                                <div className="bg-light border rounded-3 p-3">
                                  <p className="text-center my-5">
                                    No Due Date tasks found.
                                  </p>
                                </div>
                              )}
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              )}
            </div>
          </div>

          {taskVisible && (
            <div className="modal" role="dialog" id="transaction_task">
              <div
                className="modal-dialog modal-md modal-dialog-scrollable"
                role="document"
              >
                <div className="modal-content">
                  <div className="modal-header">
                    <h4 className="modal-title">Add Task</h4>
                    <button
                      className="btn-close"
                      onClick={handleClose}
                    ></button>
                  </div>
                  <div className="modal-body fs-sm">
                    <div className="col-md-12">
                      <div className="mb-3">
                        <label className="form-label" for="pr-city">
                          Task Name<span className="text-danger">*</span>
                        </label>
                        <input
                          className="form-control"
                          name="task_name"
                          onChange={handleChange}
                          autoComplete="on"
                          value={formValues.task_name}
                          style={{
                            border: formErrors?.task_name
                              ? "1px solid red"
                              : "",
                          }}
                        />
                        <div className="invalid-tooltip">
                          {formErrors.task_name}
                        </div>
                      </div>

                      <div className="mb-3">
                        <label className="form-label" for="pr-city">
                          Assign to
                        </label>
                        <input
                          className="form-control"
                          name="assign"
                          onChange={handleChange}
                          value={formValues.assign}
                        />
                      </div>

                      <div className="mb-3">
                        <label className="form-label" for="pr-city">
                          Description
                        </label>
                        <textarea
                          className="form-control mt-0"
                          id="textarea-input"
                          rows="5"
                          name="description"
                          onChange={handleChange}
                          autoComplete="on"
                          value={formValues.description}
                        ></textarea>
                      </div>

                      <div className="mb-3">
                        <label className="form-label" for="pr-birth-date">
                          Set Due Data
                        </label>
                        <input
                          className="form-control"
                          type="date"
                          id="inline-form-input"
                          placeholder="Choose date"
                          name="task_date"
                          onChange={handleChange}
                          value={formValues.task_date}
                        />
                      </div>

                      <div className="mb-3">
                        <label className="form-label" for="pr-birth-date">
                          Attach Document
                        </label>
                        <input
                          className="file-uploader file-uploader-grid"
                          id="inline-form-input"
                          type="file"
                          name="task_document"
                          onChange={handleFileUpload}
                        />
                        {data ? <img src={data} /> : ""}
                      </div>

                      <div className="pull-right py-3">
                        <button
                          onClick={handleSubmit}
                          type="button"
                          className="btn btn-primary btn-sm "
                        >
                          Add Task
                        </button>

                        <button
                          type="button"
                          className="btn btn-secondary btn-sm ms-3"
                          onClick={handleClose}
                        >
                          Cancel & Close
                        </button>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          )}

          {taskUpdateVisible && (
            <div className="modal" role="dialog" id="task-update">
              <div
                className="modal-dialog modal-md modal-dialog-scrollable"
                role="document"
              >
                <div className="modal-content">
                  <div className="modal-header">
                    <h4 className="modal-title">Update Task</h4>
                    <button
                      className="btn-close"
                      onClick={handleCloseUpdate}
                    ></button>
                  </div>
                  <div className="modal-body fs-sm">
                    <div className="col-md-12">
                      <div className="mb-3">
                        <label className="form-label" for="pr-city">
                          Task Name<span className="text-danger">*</span>
                        </label>
                        <input
                          className="form-control"
                          name="task_name"
                          onChange={handleChange}
                          autoComplete="on"
                          value={formValues.task_name}
                          style={{
                            border: formErrors?.task_name
                              ? "1px solid red"
                              : "",
                          }}
                        />
                        <div className="invalid-tooltip">
                          {formErrors.task_name}
                        </div>
                      </div>

                      <div className="mb-3">
                        <label className="form-label" for="pr-city">
                          Assign to
                        </label>
                        <input
                          className="form-control"
                          name="assign"
                          onChange={handleChange}
                          value={formValues.assign}
                        />
                      </div>

                      <div className="mb-3">
                        <label className="form-label" for="pr-city">
                          Description
                        </label>
                        <textarea
                          className="form-control mt-0"
                          id="textarea-input"
                          rows="5"
                          name="description"
                          onChange={handleChange}
                          autoComplete="on"
                          value={formValues.description}
                        ></textarea>
                      </div>

                      <div className="mb-3">
                        <label className="form-label" for="pr-birth-date">
                          Set Due Data
                        </label>
                        <input
                          className="form-control"
                          type="date"
                          id="inline-form-input"
                          placeholder="Choose date"
                          name="task_date"
                          onChange={handleChange}
                          value={formValues.task_date}
                        />
                      </div>

                      <div className="mb-3">
                        <label className="form-label" for="pr-birth-date">
                          Attach Document
                        </label>
                        <input
                          className="file-uploader file-uploader-grid"
                          id="inline-form-input"
                          type="file"
                          name="task_document"
                          onChange={handleFileUpload}
                        />
                      </div>

                      <div className="pull-right mt-3">
                        <button
                          onClick={handleSubmitUpdate}
                          type="button"
                          className="btn btn-primary btn-sm "
                        >
                          Update Task
                        </button>

                        <button
                          type="button"
                          className="btn btn-secondary btn-sm ms-3"
                          onClick={handleCloseUpdate}
                        >
                          Cancel & Close
                        </button>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          )}
        </div>
      </main>
    </div>
  );
};

export default TransactionSummary;

import React, { useState, useEffect } from "react";
import avtar from "../img/avtar.jpg";
import ReactPaginate from "react-paginate";
import user_service from '../service/user_service';
import jwt from "jwt-decode";
import Loader from "../../Pages/Loader/Loader.js";
import Toaster from '../../Pages/Toaster/Toaster.js';
import { NavLink, useNavigate, useParams } from "react-router-dom";
import _ from "lodash";
function AddTransaction() {
    const [step1, setStep1] = useState("");
    const [category, setCategory] = useState("")
    const [step2, setStep2] = useState("");
    const [type, setType] = useState("")
    const [step3, setStep3] = useState("");
    const [phase, setPhase] = useState("")
    const [step4, setStep4] = useState("");
    const [represent, setRepresent] = useState("")
    const [step5, setStep5] = useState("");
    const [contact, setContact] = useState("")

    const [loader, setLoader] = useState({ isActive: null })
    const { isActive } = loader;
    const [toaster, setToaster] = useState({ types: null, isShow: null, toasterBody: "", message: "" });
    const { types, isShow, toasterBody, message } = toaster;
    const [summary, setSummary] = useState([]);
    const [arrayData, setArrayData] = useState([]);
    const [select, setSelect] = useState("");
    const [clientId, setClientId] = useState("")

    const initialValues = { firstName: "", lastName: "", company: "", email: "", phone: "", agentId: "" };
    const [formValues, setFormValues] = useState(initialValues);
    const [formErrors, setFormErrors] = useState({});
    const [isSubmitClick, setISSubmitClick] = useState(false)

    const [results, setResults] = useState([]);
    const [pageCount, setPageCount] = useState(0);
    const [currentPage, setCurrentPage] = useState(1);
    const [isLoading, setIsLoading] = useState(false);
    const [showPopup, setShowPopup] = useState(false);
    
     



    const navigate = useNavigate();


    const handleProgressbar = (stepno) => {
        if (stepno === "step active category") {
            setCategory(current => !current);
            setType(false);
            setPhase(false);
            setRepresent(false);
            setContact(false);
        } else if (stepno === "step active type") {
            setCategory(false);
            setType(current => !current);
            setPhase(false);
            setRepresent(false);
            setContact(false);
        } else if (stepno === "step active phase") {
            setCategory(false);
            setType(false);
            setPhase(current => !current);
            setRepresent(false);
            setContact(false);
        } else if (stepno === "step active represent") {
            setCategory(false);
            setType(false);
            setPhase(false);
            setRepresent(current => !current);
            setContact(false);
        } else if (stepno === "step active contact") {
            setCategory(false);
            setType(false);
            setPhase(false);
            setRepresent(false);
            setContact(current => !current);
        }
    };


    const TransactionStepBack = (step, value) => {
        if (step === "step2") {
            setStep1("");
            setStep2("");
            setStep3("");
            setStep4("");
            setStep5("");
        }
        if (step === "step3") {
            setStep2("");
            setStep3("");
            setStep4("");
            setStep5("");

        };

        if (step === "step4") {
            setStep3("");
            setStep4("");
            setStep5("");
        };
        if (step === "step5") {

            setStep4("");
        };

    }



    {/* <!-- Add value  Function Start--> */ }

    const TransactionStep = (stepno, value) => {
        if (stepno === "1") {
            setStep1(stepno);
            setCategory(value)
        }
        if (stepno === "2") {
            setStep2(stepno);
            setType(value)
        }
        if (stepno === "3") {
            setStep3(stepno);
            setPhase(value)
        }
        if (stepno === "4") {
            setStep4(stepno);
            setRepresent(value)
        }
        if (stepno === "5") {
            setStep5(stepno);
            setContact(value)
        }
        else {
            setStep1(stepno);

        }

    }
    {/* <!-- Add value  Function End--> */ }


    { /* <!-- Api call  Function Start--> */ }
    const AddTransaction = async (e) => {
        e.preventDefault();
        const contacts = arrayData.map((contactId) => ({
            type: represent,
            contactId: contactId
        }));
        const contacts3 = arrayData.map((contactId) => ({
            type: represent+"_Agent",
            contactId: jwt(localStorage.getItem("auth")).id
        }));
        const userData = {
            agentId: jwt(localStorage.getItem("auth")).id,
            office_id: localStorage.getItem("active_office_id"),
            office_name: localStorage.getItem("active_office"),
            type: type,
            phase: phase,
            represent: represent,
            contact1: contacts,
            contact3: contacts3,
            category: category,
        };

        try {
            setLoader({ isActive: true });
            const response = await user_service.TransactionCreate(userData);
            if (response) {
                // console.log(response.data._id)
                setLoader({ isActive: false });
                setToaster({
                    type: "Transaction Successfully",
                    isShow: true,
                    toasterBody: response.data.message,
                    message: "Transaction Successfully",
                });
                setTimeout(() => {
                    navigate(`/transaction-summary/${response.data._id}`);
                }, 500);
            } else {
                setLoader({ isActive: false });
                setToaster({
                    types: "error",
                    isShow: true,
                    toasterBody: response.data.message,
                    message: "Error",
                });
            }
        } catch (error) {
            setLoader({ isActive: false });
            setToaster({
                types: "error",
                isShow: true,
                toasterBody: error,
                message: "Error",
            });
        }
        setTimeout(() => {
            setToaster({ types: "error", isShow: false, toasterBody: null, message: "Error" });
        }, 2000);
    };

    const Addapartment = async (e) => {
        e.preventDefault();
        const contacts = arrayData.map((contactId) => ({
            type: "Tenant",
            contactId: contactId
        }));
        const userData = {
            agentId: jwt(localStorage.getItem("auth")).id,
            type: "sale",
            phase: "post-Move-In",
            represent: "Tenant",
            contact1: contacts,
            category: "Apartment",
        };

        try {
            setLoader({ isActive: true });
            const response = await user_service.TransactionCreate(userData);
            if (response) {
                //console.log(response.data._id)
                setLoader({ isActive: false });
                setToaster({
                    type: "Transaction Successfully",
                    isShow: true,
                    toasterBody: response.data.message,
                    message: "Transaction Successfully",
                });
                setTimeout(() => {
                    navigate(`/transaction-summary/${response.data._id}`);
                }, 500);
            } else {
                setLoader({ isActive: false });
                setToaster({
                    types: "error",
                    isShow: true,
                    toasterBody: response.data.message,
                    message: "Error",
                });
            }
        } catch (error) {
            setLoader({ isActive: false });
            setToaster({
                types: "error",
                isShow: true,
                toasterBody: error,
                message: "Error",
            });
        }
        setTimeout(() => {
            setToaster({ types: "error", isShow: false, toasterBody: null, message: "Error" });
        }, 2000);
    };
    {/* <!-- Api call  Function End--> */ }


    const [checkBox, setSetCheckbox] = useState(false);

    const handleCheck = () => {
        setSetCheckbox(checkBox ? true : false);
    };

    {/* <!-- Input onChange Start--> */ }
    const handleChange = (e) => {
        const { name, value } = e.target;
        setFormValues({ ...formValues, [name]: value });
    };
    {/* <!-- Input onChange End--> */ }


    {/* <!-- Form Validation Start--> */ }
    useEffect(() => { 
        if (formValues && isSubmitClick) {
            validate()
        }
    }, [formValues])

    const validate = () => {
        const values = formValues
        const errors = {};
        if (!values.firstName) {
            errors.firstName = "name is required";
        }

        if (!values.lastName) {
            errors.lastName = "lastName is required";
        }

        if (!values.email) {
            errors.email = "Email is required";
          } else if (
            !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i.test(values.email)
          ) {
            errors.email = "Invalid email address";
          }
      

        // if (!values.email) {
        //     errors.email = "email is required";
        // }
        // else if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.email)) {
        //     errors.email = 'Invalid email address'
        // }

        setFormErrors(errors)
        return errors;
    }
    {/* <!-- Form Validation End--> */ }




    {/* <!--Api call Form onSubmit Start--> */ }
    const handleNewContact = async (e, id) => {
        e.preventDefault();
        setISSubmitClick(true)
        let checkValue = validate()
        if (_.isEmpty(checkValue)) {
            const userData = {
                firstName: formValues.firstName,
                lastName: formValues.lastName,
                company: formValues.company,
                email: formValues.email,
                phone: formValues.phone,
                agentId: jwt(localStorage.getItem("auth")).id,
            };
            // console.log(userData)
            try {
                setLoader({ isActive: true })
                const response = await user_service.contact(userData, formValues.firstName, id);
                const contactData = response.data;
                setSummary(prevSummary => [...prevSummary, contactData]);

                setArrayData(prevArrayData => [...prevArrayData, contactData._id]);

                setClientId(id);
                setSelect("");
                setLoader({ isActive: false })
                setToaster({ type: "Contact Successfully", isShow: true, toasterBody: response.data.message, message: "Contact Successfully", });
                document.getElementById("closeModal").click();

            }
            catch (error) {
                setLoader({ isActive: false })
                setToaster({ types: "error", isShow: true, toasterBody: error, message: "Error", });
            }
            setTimeout(() => {
                setToaster({ types: "error", isShow: false, toasterBody: null, message: "Error", });
            }, 2000);
        }
    }
    {/* <!-- Api call Form onSubmit End--> */ }

    const selectContact = async (id) => {
        try {
            const response = await user_service.contactGetById(id);
            const contactData = response.data;
            //console.log(contactData)
            setSummary(prevSummary => [...prevSummary, contactData]);
            console.log(contactData);
            setArrayData(prevArrayData => [...prevArrayData, contactData._id]);
            setClientId(id);
            setSelect("");
            document.getElementById("closeModal").click();
        } catch (error) {
            //console.log(error);
        }
    };


    {/* <!-- paginate Function Api call Start--> */ }
    useEffect(() => { 
        if (formValues) {
            SearchGetAll()
        }
    }, [formValues]);


    const SearchGetAll = async () => {
        await user_service.SearchContactGet(1, formValues.firstName).then((response) => {
            if (response) {
                setResults(response.data.data);
                setPageCount(Math.ceil(response.data.totalRecords / 10));

            }
        });
    }

    const handlePageClick = async (data) => {
        let currentPage = data.selected + 1;
        let skip = (currentPage - 1) + currentPage;
        setLoader({ isActive: true })
        await user_service.SearchContactGet(currentPage, formValues.firstName).then((response) => {
            setLoader({ isActive: false })
            if (response) {
                // setSummary(response.data);
                setResults(response.data.data);
                setPageCount(Math.ceil(response.data.totalRecords / 10));
            }
        });

    }

    const handleSearchTransaction = async (id) => {
        await user_service.contactGetById(id).then((response) => {
            setClientId(id)
            setSelect(response.data);
        });
    }

    const handleSearch = (event) => {
        event.preventDefault();
        setCurrentPage(1);
    }

    const cancel = () => {
        setSelect("")
    }

    const handleDelete =  (e,id) => {
      
        if (category) {
            console.log(`Deleting post with category ID: ${category}`);
            setShowPopup(false);
            setCategory("");
            setSelect("")
            setSummary([]);
        }
    }

    const handleCancel = (id) => {
        setShowPopup(false);
        setCategory("");
    }

    return (
        <div className="bg-secondary float-left w-100 pt-4">
            <Loader isActive={isActive} />
            {isShow && <Toaster types={types}
                isShow={isShow}
                toasterBody={toasterBody}
                message={message} />}
            <main className="page-wrapper">
                {/* <!-- Page container--> */}
                <div className="container mt-5 mb-md-4 py-5">
                    {/* <!-- Breadcrumb--> */}
                    <nav className="mb-3 mb-md-4 pt-md-3" aria-label="Breadcrumb">
                        <ol className="breadcrumb">
                            <li className="breadcrumb-item"><a href="/new-transaction">Home</a></li>
                            <li className="breadcrumb-item active" aria-current="page">New Transaction</li>
                        </ol>
                    </nav>
                    {/* <!-- Page content--> */}
                    <div className="row justify-content-center pb-sm-2">
                        <div className="col-lg-11 col-xl-10">
                            <div className="bg-light rounded-3 py-4 px-md-4 mb-3">
                                <div className="steps pt-4 pb-1">
                                    <div className={category ? 'step active' : ''} onClick={handleProgressbar}>
                                        <div className="step-progress"><span className="step-progress-start"></span><span className="step-progress-end"></span><span className="step-number">1</span></div>
                                        <div className="step-label" >Category</div>
                                    </div>
                                    <div className={type ? 'step active' : ''} onClick={handleProgressbar}>
                                        <div className="step-progress"><span className="step-progress-start"></span><span className="step-progress-end"></span><span className="step-number">2</span></div>
                                        <div className="step-label">Type</div>
                                    </div>
                                    <div className={phase ? 'step active' : ''} onClick={handleProgressbar}>
                                        <div className="step-progress"><span className="step-progress-start"></span><span className="step-progress-end"></span><span className="step-number">3</span></div>
                                        <div className="step-label">Phase</div>
                                    </div>
                                    <div className={represent ? 'step active' : ''} onClick={handleProgressbar}>
                                        <div className="step-progress"><span className="step-progress-start"></span><span className="step-progress-end"></span><span className="step-number">4</span></div>
                                        <div className="step-label">Represent</div>
                                    </div>
                                    <div className={contact ? 'step active' : ''} onClick={handleProgressbar}>
                                        <div className="step-progress"><span className="step-progress-start"></span><span className="step-progress-end"></span><span className="step-number">5</span></div>
                                        <div className="step-label">Contact</div>
                                    </div>
                                </div>
                            </div>

                            {/* <!-- Step 1 start here--> */}
                            {
                                step1 === "" && step5 === "" ?
                                    <div className="bg-light rounded-3 p-4 p-md-5 mb-0 ">
                                        <h2 className="h4 mb-2"><i className="fi-info-circle text-primary fs-5 mt-n1 me-2"></i>Add a Transaction</h2>
                                        <p className="mb-4">What category of transaction are you creating?</p>
                                        <div className="col-lg-12 mb-2">
                                            <div className="border rounded-3 p-3 selected_hover" onClick={() => TransactionStep("1", "Residential")}>
                                                <div className="d-flex align-items-center justify-content-between">
                                                    <div className="pe-2">
                                                        <label className="form-label fw-bold">Residential</label>
                                                        <div id="type-value">As defined by your brokerage and REALTOR® association.
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="col-lg-12 mb-2">
                                            <div className="border rounded-3 p-3 selected_hover" onClick={() => TransactionStep("5", "Apartment")}>
                                                <div className="d-flex align-items-center justify-content-between">
                                                    <div className="pe-2">
                                                        <label className="form-label fw-bold">Apartment (Managed Building)</label>
                                                        <div id="type-value">Only used for standard property management controlled leasing.</div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="col-lg-12 mb-2">
                                            <div className="border rounded-3 p-3 selected_hover" onClick={() => TransactionStep("1", "Commercial")}>
                                                <div className="d-flex align-items-center justify-content-between">
                                                    <div className="pe-2">
                                                        <label className="form-label fw-bold">Commercial</label>
                                                        <div id="type-value">As defined by your brokerage and REALTOR® association.</div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="col-lg-12 mb-2">
                                            <div className="border rounded-3 p-3 selected_hover" onClick={() => TransactionStep("1", "Referral")}>
                                                <div className="d-flex align-items-center justify-content-between">
                                                    <div className="pe-2">
                                                        <label className="form-label fw-bold">Referral Only</label>
                                                        <div id="type-value">You referred a buyer or seller to another agent, for a fee.</div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    : ""
                            }
                            {/* <!-- Step 1 ends here--> */}


                            {/* <!-- Step 2 starts here--> */}
                            {
                                step1 && step2 === "" ?
                                    <div className="bg-light rounded-3 p-4 p-md-5 mb-0"  >

                                        <h2 className="h4 mb-2"><i className="fi-info-circle text-primary fs-5 mt-n1 me-2"></i>Add a Transaction</h2>
                                        <hr /><br />
                                        <p className="mb-4">What type of transaction is it?</p>
                                        <div className="col-lg-12 mb-2">
                                            <div className="border rounded-3 p-3 selected_hover" onClick={() => TransactionStep("2", "Sale")}>
                                                <div className="d-flex align-items-center justify-content-between">
                                                    <div className="pe-2">
                                                        <label className="form-label fw-bold">Sale or Sale Listing</label>
                                                        <div id="type-value">The property owner will change upon completion.
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div className="col-lg-12 mb-2" >
                                            <div className="border rounded-3 p-3 selected_hover" onClick={() => TransactionStep("2", "Lease")}>
                                                <div className="d-flex align-items-center justify-content-between">
                                                    <div className="pe-2">
                                                        <label className="form-label fw-bold">Lease or Lease Listing</label>
                                                        <div id="type-value">Tenant is changing, title remains the same.</div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    : ""
                            }

                            {/* <!-- Step 2 ends here--> */}

                            {/* <!-- Step 3 starts here--> */}
                            {
                                step2 && step3 === "" ?
                                    <div className="bg-light rounded-3 p-4 p-md-5 mb-0">
                                        <h2 className="h4 mb-2"><i className="fi-info-circle text-primary fs-5 mt-n1 me-2"></i>Add a Transaction</h2>
                                        <hr /><br />


                                        <h6><i className="fa fa-check" aria-hidden="true"></i>  You selected the '{category}'  category.  <a onClick={(e) => TransactionStepBack('step2')}><NavLink>Change</NavLink></a></h6>
                                        <hr /><br />
                                        <h6><i className="fa fa-check" aria-hidden="true"></i>  You selected the 'Sale' {type}.  <a onClick={(e) => TransactionStepBack('step3')}><NavLink>Change</NavLink></a></h6>
                                        <hr /><br />
                                        <p className="mb-4">Where would you like to start the transaction?</p>
                                        <div className="col-lg-12 mb-2">
                                            <div className="border rounded-3 p-3 selected_hover" onClick={() => TransactionStep("3", "start")}>
                                                <div className="d-flex align-items-center justify-content-between">
                                                    <div className="pe-2">
                                                        <label className="form-label fw-bold">Start</label>
                                                        <div id="type-value">You are in the early stages of representing your client. (Recommended)
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="col-lg-12 mb-2">
                                            <div className="border rounded-3 p-3 selected_hover" onClick={() => TransactionStep("3", "showing")}>
                                                <div className="d-flex align-items-center justify-content-between">
                                                    <div className="pe-2">
                                                        <label className="form-label fw-bold">Showing</label>
                                                        <div id="type-value">Client is actively searching for, or marketing, a property.
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                        <div className="col-lg-12 mb-2">
                                            <div className="border rounded-3 p-3 selected_hover" onClick={() => TransactionStep("3", "contract")}>
                                                <div className="d-flex align-items-center justify-content-between">
                                                    <div className="pe-2">
                                                        <label className="form-label fw-bold">Contract</label>
                                                        <div id="type-value">Your client's contract is written. You may still be negotiating final terms and due diligence.
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="col-lg-12 mb-2">
                                            <div className="border rounded-3 p-3 selected_hover" onClick={() => TransactionStep("3", "pro-closing")}>
                                                <div className="d-flex align-items-center justify-content-between">
                                                    <div className="pe-2">
                                                        <label className="form-label fw-bold">Pre-Closing</label>
                                                        <div id="type-value">All documents have been signed and executed. You are ready to submit for funding.
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="col-lg-12 mb-2">
                                            <div className="border rounded-3 p-3 selected_hover" onClick={() => TransactionStep("3", "post-closing")}>
                                                <div className="d-flex align-items-center justify-content-between">
                                                    <div className="pe-2">
                                                        <label className="form-label fw-bold">Post-Closing</label>
                                                        <div id="type-value">The transaction has already closed. You need to submit your paperwork and request a commission check from your broker.
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    : ""
                            }
                            {/* <!-- Step 3 ends here--> */}

                            {/* <!-- Step 4 starts here--> */}
                            {
                                step3 && step4 === "" ?

                                    <div className="bg-light rounded-3 p-4 p-md-5 mb-0"  >

                                        <h2 className="h4 mb-2"><i className="fi-info-circle text-primary fs-5 mt-n1 me-2"></i>Add a Transaction</h2>
                                        <hr /><br />
                                        <h6><i className="fa fa-check" aria-hidden="true"></i>  You selected the '{category}' category.   <a onClick={(e) => TransactionStepBack('step2')}><NavLink>Change</NavLink></a></h6>
                                        <hr /><br />
                                        <h6><i className="fa fa-check" aria-hidden="true"></i>  You selected the '{type}' type.    <a onClick={(e) => TransactionStepBack('step3')}><NavLink>Change</NavLink></a></h6>
                                        <hr /><br />
                                        <h6><i className="fa fa-check" aria-hidden="true"></i> You selected the 'Start' {phase}.  <a onClick={(e) => TransactionStepBack('step4')}><NavLink>Change</NavLink></a></h6>
                                        <hr /><br />

                                        <p className="mb-4">Which party do you represent?</p>
                                        <div className="col-lg-12 mb-2">
                                            <div className="border rounded-3 p-3 selected_hover" onClick={() => TransactionStep("4", type === "Lease" ? "Landlord" : "Seller")}>
                                                <div className="d-flex align-items-center justify-content-between">
                                                    <div className="pe-2">
                                                        <label className="form-label fw-bold"> {type === "Lease" ? "Landlord" : "Seller"}</label>
                                                        <div id="type-value">{type === "Lease" ? "The property owner or management company." : "The current title holder of a property."}
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        {
                                            phase !== "start" && phase !== "showing" ?
                                                <div className="col-lg-12 mb-2" >
                                                    <div className="border rounded-3 p-3 selected_hover" onClick={() => TransactionStep("4", type === "Lease" ? "Landlord & Tenant" : "Seller & Buyer")}>
                                                        <div className="d-flex align-items-center justify-content-between">
                                                            <div className="pe-2">
                                                                <label className="form-label fw-bold">{type === "Lease" ? "Landlord & Tenant" : "Seller & Buyer"}</label>
                                                                <div id="type-value">{type === "Lease" ? "Both sides of the transaction." : "Both sides of the transaction."}</div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                : ''
                                        }
                                        <div className="col-lg-12 mb-2" >
                                            <div className="border rounded-3 p-3 selected_hover" onClick={() => TransactionStep("4", type === "Lease" ? "Tenant" : "Buyer")}>
                                                <div className="d-flex align-items-center justify-content-between">
                                                    <div className="pe-2">
                                                        <label className="form-label fw-bold">{type === "Lease" ? "Tenant" : "Buyer"}</label>
                                                        <div id="type-value">{type === "Lease" ? "The lease or rent payer." : "The purchaser of a property."}</div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                    : ""
                            }

                            {
                                step4 ?
                                    <div className="bg-light rounded-3 p-4 p-md-5 mb-0">
                                        <h2 className="h4 mb-2"><i className="fi-info-circle text-primary fs-5 mt-n1 me-2"></i>Add a Transaction</h2>
                                        <hr /><br />
                                        <h6><i className="fa fa-check" aria-hidden="true"></i>  You selected the '{category}' category.  <a onClick={(e) => TransactionStepBack('step2')}><NavLink>Change</NavLink></a></h6>
                                        <hr /><br />
                                        <h6><i className="fa fa-check" aria-hidden="true"></i>  You selected the 'Sale' {type}.   <a onClick={(e) => TransactionStepBack('step3')}><NavLink>Change</NavLink></a></h6>
                                        <h6><i className="fa fa-check" aria-hidden="true"></i> You selected the 'Start' {phase}.  <a onClick={(e) => TransactionStepBack('step4')}><NavLink>Change</NavLink></a></h6>
                                        <h6><i className="fa fa-check" aria-hidden="true"></i> You represent the '{represent}'.  <a onClick={(e) => TransactionStepBack('step5')}><NavLink>Change</NavLink></a></h6>
                                        <p className="mb-4">Who is the {represent}?</p>
                                        {
                                            summary.map((postt) => (

                                                <div className="card bg-secondary col-md-6">
                                                    <div className="card-body">
                                                        <div className="view_profile">
                                                            <img
                                                                onClick={() => { setShowPopup(true); setCategory(postt._id); }}
                                                                className="pull-right"
                                                                src="https://cdn.pboffice.net/shared/transaction/page_select_drop.png"/>
                                                            <img className='pull-left' src={postt.image || "https://integra.backagent.net/common/handler/pub/photo.ashx?V=1703c0890a0462fd68520b884a04f86f.0&C=people&S=2&P=%2f!Default%2fThumb%2fphoto_pending.jpg"} alt="Profile" />
                                                            <span key={postt._id}>
                                                                <a className="card-title mt-0">{postt.firstName}&nbsp;{postt.lastName}</a>
                                                                <p>{postt.company}<br />{postt.phone}<br />
                                                                <a href='#'>Send Message</a></p>
                                                            </span>
                                                        </div>
                                                    </div>
                                                    {showPopup && (
                                                        <div className="ContactCardConfirms">
                                                            <div className="ConfirmContent">
                                                                <div className="ConfirmQuestion">Remove this contact?</div>
                                                                {console.log( postt._id)}
                                                                <div className="pull-right">
                                                                <a onClick={(e) => handleDelete(e, postt._id)}>
                                                                    Yes
                                                                </a>
                                                                <a className="ms-2" onClick={() => handleCancel(category)}
                                                                >No</a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    )}
                                                </div>
                                            ))
                                        }


                                        <div className="col-lg-12 mb-2">
                                            <div className="border rounded-3 p-3 selected_hover">
                                                <div className="d-flex align-items-center justify-content-between">
                                                    <div className="pe-2">
                                                        <button type="button" className="btn btn-info btn-lg" data-toggle="modal" data-target="#addcontact"
                                                            onClick={() => TransactionStep("5", "Select Client")}>Select Client</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="col-lg-12 mb-2">
                                            <div className="border rounded-3 p-3 selected_hover">
                                                <div className="d-flex align-items-center justify-content-between">
                                                    <div className="pe-2">
                                                        <label className="form-label fw-bold">Confirm & Save</label>
                                                        <p>if everything looks good, you are ready to<br /> create the new transaction.</p>
                                                        <button id="type-value" className="m-2 btn btn-primary btn-sm" onClick={AddTransaction}>Create New Transaction</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>


                                    </div>
                                    : ''
                            }
                            {/* <!-- Step 4 ends here--> */}


                            {/* <!-- Step 5 Start here--> */}
                            {
                                step4 === "" && step5 ?
                                    <div className="bg-light rounded-3 p-4 p-md-5 mb-0">
                                        <h2 className="h4 mb-2"><i className="fi-info-circle text-primary fs-5 mt-n1 me-2"></i>Add a Transaction</h2>
                                        <hr /><br />
                                        <h6><i className="fa fa-check" aria-hidden="true"></i>  You selected the 'Apartment' category.  <a onClick={(e) => TransactionStepBack('step2')}><NavLink>Change</NavLink></a></h6>
                                        <hr /><br />
                                        <h6><i className="fa fa-check" aria-hidden="true"></i>  You selected the 'Lease' type.   <a onClick={(e) => TransactionStepBack('step2')}><NavLink>Change</NavLink></a></h6>
                                        <h6><i className="fa fa-check" aria-hidden="true"></i> You selected the  'Post-Move-In'.  <a onClick={(e) => TransactionStepBack('step2')}><NavLink>Change</NavLink></a></h6>

                                        <h6><i className="fa fa-check" aria-hidden="true"></i> You represent the  'Tenant'.  <a onClick={(e) => TransactionStepBack('step2')}><NavLink>Change</NavLink></a></h6>
                                        <p className="mb-4">Who is the {represent === "" ? "Tenant" : "Tenant"}?</p>


                                        {
                                            summary.map((postt) => (

                                                <div className="card bg-secondary col-md-6">
                                                    <div className="card-body">
                                                        <div className="view_profile">
                                                            <img
                                                                onClick={() => { setShowPopup(true); setCategory(postt._id); }}
                                                                className="pull-right"
                                                                src="https://cdn.pboffice.net/shared/transaction/page_select_drop.png"
                                                            />
                                                            <img className='pull-left' src={postt.image || "https://integra.backagent.net/common/handler/pub/photo.ashx?V=1703c0890a0462fd68520b884a04f86f.0&C=people&S=2&P=%2f!Default%2fThumb%2fphoto_pending.jpg"} alt="Profile" />
                                                            <span key={postt._id}>
                                                                <a className="card-title mt-0">{postt.firstName}&nbsp;{postt.lastName}</a>
                                                                <p>{postt.company}<br />{postt.phone}<br />
                                                                    <a href='#'>Send Message</a></p>
                                                            </span>
                                                        </div>
                                                    </div>
                                                </div>
                                            ))
                                        }

                                        {showPopup && (
                                            <div className="ContactCardConfirms">
                                                <div className="ConfirmContent">
                                                    <div className="ConfirmQuestion">Remove this contact?</div>
                                                    <div className="pull-right">
                                                    <a onClick={(e) => handleDelete(e)}>
                                                        Yes
                                                    </a>
                                                    <a className="ms-2" onClick={() => handleCancel(category)}
                                                    >No</a>
                                                    </div>
                                                </div>
                                            </div>
                                        )}


                                        <div className="col-lg-12 mb-2">
                                            <div className="border rounded-3 p-3 selected_hover">
                                                <div className="d-flex align-items-center justify-content-between">
                                                    <div className="pe-2">
                                                        <button type="button" className="btn btn-info btn-lg" data-toggle="modal" data-target="#addcontact"
                                                            onClick={() => TransactionStep("5", "Select Client")}>Select Client</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="bg-light rounded-3 p-4 p-md-5 mb-0">
                                            <div className="col-lg-12 mb-2">
                                                <div className="border rounded-3 p-3 selected_hover">
                                                    <div className="d-flex align-items-center justify-content-between">
                                                        <div className="pe-2">
                                                            <label className="form-label fw-bold">Confirm & Save</label>
                                                            <p>if everything looks good, you are ready to<br /> create the new transaction.</p>
                                                            <button id="type-value" className="m-2 btn btn-primary btn-sm" onClick={Addapartment}>Create New Transaction</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    : ""



                            }

                            {/* <!-- Step 5 ends here--> */}


                            <form className="new_transaction d-none">
                                <input type="text" name="category" id="category" value={step1.category} /><br />
                                <input type="text" name="type" id="type" value={step2.type} /><br />
                                <input type="text" name="phase" id="phase" value={step3.phase} /><br />
                                <input type="text" name="represent" id="represent" value={step4.represent} /><br />
                                <input type="text" name="contact" id="contactid" value={step5.contact} /><br />
                            </form>

                        </div>
                    </div>
                </div>

                <div className="modal" role="dialog" id="addcontact">
                    <div className="modal-dialog" role="document">
                        <div className="modal-content">
                            <div className="modal-body row">
                                <div className=" col-md-6 mt-5" >

                                    {
                                        select === "" ?
                                            <div>
                                                <form onSubmit={handleSearch}>
                                                    {/* <!-- First Name input --> */}
                                                    <div className="mb-3">
                                                        <label className="form-label">First Name</label><br />
                                                        <input className="form-control"
                                                            type="text"
                                                            id="text-input"
                                                            name='firstName'
                                                            placeholder="Enter your first name"
                                                            onChange={(event) => handleChange(event)}
                                                            autoComplete="on"
                                                            style={{
                                                                border: formErrors?.firstName ? '1px solid red' : '1px solid #00000026'
                                                            }}
                                                            value={formValues.firstName} />
                                                        <div className="invalid-tooltip">{formErrors.firstName}</div>
                                                    </div>


                                                    {/* <!-- Last Name input --> */}
                                                    <div className="mb-3">

                                                        <label className="form-label">Last Name</label>
                                                        <input className="form-control"
                                                            type="text"
                                                            id="text-input"
                                                            name='lastName'
                                                            placeholder="Enter your last name"
                                                            onChange={(event) => handleChange(event)}
                                                            autoComplete="on"
                                                            style={{
                                                                border: formErrors?.lastName ? '1px solid red' : '1px solid #00000026'
                                                            }}
                                                            value={formValues.lastName} />
                                                        <div className="invalid-tooltip">{formErrors.lastName}</div>
                                                    </div>

                                                    {/* <!-- Company Input --> */}
                                                    <div className="mb-3">
                                                        <label className="form-label">Company</label>
                                                        <input className="form-control"
                                                            type="text"
                                                            id="text-input"
                                                            name='company'
                                                            placeholder="Enter your company"
                                                            onChange={(event) => handleChange(event)}
                                                            autoComplete="on"
                                                            value={formValues.company} />
                                                    </div>

                                                    {/* <!-- Email input --> */}
                                                    <div className="mb-3">
                                                        <label className="form-label">Email</label>
                                                        <input className="form-control"
                                                            id="email-input"
                                                            type="text"
                                                            name='email'
                                                            placeholder="Enter your email"
                                                            onChange={(event) => handleChange(event)}
                                                            autoComplete="on"
                                                            style={{
                                                                border: formErrors?.email ? '1px solid red' : '1px solid #00000026'
                                                            }}
                                                            value={formValues.email} />
                                                        <div className="invalid-tooltip">{formErrors.email}</div>
                                                    </div>

                                                    {/* <!-- Phone Input --> */}
                                                    <div className="mb-3">
                                                        <label className="form-label">Phone</label>
                                                        <input
                                                            className="form-control"
                                                            type="tel"
                                                            id="tel-input"
                                                            name="phone"
                                                            placeholder="Enter your phone"
                                                            onChange={(event) => handleChange(event)}
                                                            autoComplete="on"
                                                            pattern="[0-9]+"
                                                            required
                                                            title="Please enter a valid phone number"
                                                            value={formValues.phone}
                                                        />
                                                    </div>

                                                    {/* <!-- Checkbox Input --> */}
                                                    <div>
                                                        <div className="form-check">
                                                            <input className="form-check-input"
                                                                name="checkbox"
                                                                type="checkbox"
                                                                id="agree-to-terms"
                                                                onClick={handleCheck}
                                                                value={checkBox.checkBox}
                                                            />
                                                            <div className="invalid-tooltip">{checkBox.checkBox}</div>
                                                            <label className="form-check-label">No email address available.</label>
                                                        </div>
                                                    </div>
                                                </form>
                                                <div className="modal-footer">
                                                    <button type="button" className="btn btn-default btn btn-secondary btn-sm mt-2 ms-2" id="closeModal" data-dismiss="modal">Cancel & Close</button>
                                                    <button type="button" className="btn btn-primary btn-sm" id="save-button" onClick={handleNewContact}>Add New Contact</button>
                                                </div>
                                            </div>

                                            :
                                            <div className='col-md-6 mt-5 w-100'>
                                                <div className="card bg-secondary">
                                                    <h6>{select.represent}</h6>
                                                    <div className="card-body">
                                                        <img className='pull-right' onClick={cancel} src='https://cdn.pboffice.net/shared/transaction/page_select_drop.png' />
                                                        <div className="view_profile">
                                                            <img className="pull-left" src={select.image || "https://integra.backagent.net/common/handler/pub/photo.ashx?V=1703c0890a0462fd68520b884a04f86f.0&C=people&S=2&P=%2f!Default%2fThumb%2fphoto_pending.jpg"} alt="Profile" />

                                                            <span>
                                                                <a href='#' className="card-title mt-0">{select.firstName}&nbsp;{select.lastName}</a>
                                                                <p>{select.company}<br />{select.phone}<br />
                                                                    <a href='#'>Send Message</a>
                                                                </p>

                                                            </span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <button type="button" className="btn btn-default btn btn-secondary btn-sm mt-2 ms-2" id="closeModal" data-dismiss="modal" onClick={cancel}>Cancel & Close</button>
                                                <button type="button" className="btn btn-primary btn-sm t-2" onClick={(e) => selectContact(select._id)}>Select Contact</button>
                                            </div>
                                    }
                                </div>


                                <div className=" col-md-6 mt-5" >
                                    {isLoading ? (
                                        <p>Loading...</p>
                                    ) :
                                        (
                                            results.map((post) => {
                                                return (
                                                    <div>
                                                        {/* <!-- List group with icons and badges --> */}
                                                        <ul className="list-group" onClick={(e) => handleSearchTransaction(post._id)}>
                                                            <li className="list-group-item d-flex justify-content-between align-items-center">
                                                                <span>
                                                                    <img className="avtar" src={post.image || avtar} alt="Profile" />
                                                                    &nbsp; <strong>{post.firstName}{post.lastName}</strong><br />
                                                                    &nbsp;  &nbsp;  &nbsp;  &nbsp; {post.company}<br />
                                                                    &nbsp;   &nbsp;  &nbsp;  &nbsp;{post.phone}
                                                                </span>
                                                            </li>

                                                        </ul>
                                                    </div>
                                                )
                                            })
                                        )
                                    }
                                </div>

                                <div className="justify-content-end mb-1">
                                    <ReactPaginate
                                        previousLabel={"Previous"}
                                        nextLabel={"next"}
                                        breakLabel={"..."}
                                        pageCount={pageCount}
                                        marginPagesDisplayed={1}
                                        pageRangeDisplayed={2}
                                        onPageChange={handlePageClick}
                                        containerClassName={"pagination justify-content-center"}
                                        pageClassName={"page-item"}
                                        pageLinkClassName={"page-link"}
                                        previousClassName={"page-item"}
                                        previousLinkClassName={"page-link"}
                                        nextClassName={"page-item"}
                                        nextLinkClassName={"page-link"}
                                        breakClassName={"page-item"}
                                        breakLinkClassName={"page-link"}
                                        activeClassName={"active"}
                                    />
                                </div>

                            </div>


                        </div>
                    </div>
                </div>

            </main>


           

        </div>

    )
}
export default AddTransaction;
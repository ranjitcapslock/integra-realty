import React, { useEffect, useState } from 'react';
import { useParams } from 'react-router-dom';
import user_service from '../service/user_service';
import Loader from '../../Pages/Loader/Loader.js';
import Toaster from '../../Pages/Toaster/Toaster';
import { NavLink } from 'react-router-dom';
import AllTab from '../../Pages/AllTab';


const UpdateOffice = () => {
    const params = useParams();
    const [loader, setLoader] = useState({ isActive: null });
    const { isActive } = loader;
    const [toaster, setToaster] = useState({ types: null, isShow: null, toasterBody: '', message: '' });
    const { types, isShow, toasterBody, message } = toaster;
    const [summary, setSummary] = useState('');


    useEffect(() => { window.scrollTo(0, 0);
        setLoader({ isActive: true });
        const TransactionGetById = async () => {
            await user_service.transactionGetById(params.id).then((response) => {
                if (response) {
                    setLoader({ isActive: false });
                    setSummary(response.data);
                }
            });
        };
        TransactionGetById(params.id);
    }, [params.id]);





    return (
        <div className="bg-secondary float-left w-100 pt-4">
            <Loader isActive={isActive} />
            {isShow && <Toaster types={types} isShow={isShow} toasterBody={toasterBody} message={message} />}
            <main className="page-wrapper">
                {/* <AllTab transaction_data = {summary}/> */}
                <div className="content-overlay mb-md-4">
                    <div className="row">
                        <div className="col-md-8">
                            <div className="bg-light border rounded-3 p-3">
                                <h6 className="">Change Assigned Office for Transaction</h6>
                                <p className="mb-2">The office must be manually changed for each transaction when an agent changes office.</p>
                                {/* <p>Current Office: Corporate</p><br /> */}
                                <p className="mb-2">Current Office: &nbsp;&nbsp;<label className="form-label" for="pr-country">Corporate</label></p>

                                <div className="mb-3">
                                    <label className="col-form-label" for="pr-country">Assign Transaction to:</label>
                                    <select className="form-select" id="pr-country"
                                        name="corporate">
                                        <option>Corporate</option>
                                    </select>
                                </div>
                            </div>
                            <div className="pull-right mt-3">
                                <button className="btn btn-primary btn-sm" type="button">Assign Transaction to New Office</button>
                                <NavLink to={`/transaction-review/${params.id}`} className="btn btn-secondary btn-sm ms-3" type="button">Cancel & Return</NavLink>
                            </div>
                        </div>
                    <div className="col-md-4">

                    </div>
                    </div>
                </div>
            </main>
            
        </div>
    );
};

export default UpdateOffice;

import React, { useEffect, useState } from "react";
import Loader from "../../Pages/Loader/Loader.js";
import Toaster from "../../Pages/Toaster/Toaster.js";
import user_service from "../service/user_service";
import AllTab from "../../Pages/AllTab";
import jwt from "jwt-decode";
import { useParams, useNavigate } from "react-router-dom";

const HireTransaction = () => {
  const [loader, setLoader] = useState({ isActive: null });
  const { isActive } = loader;
  const [toaster, setToaster] = useState({
    types: null,
    isShow: null,
    toasterBody: "",
    message: "",
  });
  const { types, isShow, toasterBody, message } = toaster;
  const [formValues, setFormValues] = useState("");
  const params = useParams();
  const [contactDataNew, setContactDataNew] = useState("");
  const [summary, setSummary] = useState("");
  const navigate = useNavigate();

  const TransactionGetById = async () => {
    try {
      setLoader({ isActive: true });
      const response = await user_service.transactionGetById(params.id);
      if (response) {
        const responseData = response.data;
        setSummary(response.data);
        const contact3Data =
          responseData.contact3 &&
          responseData.contact3[0] &&
          responseData.contact3[0].data;
        const contactDatas = contact3Data || "";
        setContactDataNew(contactDatas);
      }

      setLoader({ isActive: false });
    } catch (error) {
      console.error("Error fetching transaction details:", error);
      setLoader({ isActive: false });
    }
  };

  const PlatformdetailsGet = async () => {
    await user_service.platformdetailsGet().then((response) => {
      if (response) {
        console.log(response);
        setFormValues(response.data.data);
      }
    });
  };

  useEffect(() => {
    window.scrollTo(0, 0);
    TransactionGetById();
    PlatformdetailsGet();
  }, []);

  const handleSubmit = async (e) => {
    const contactData = formValues.map((item) => ({
      email: item.email,
      firstName: "Shawn Kenney",
      is_view: "1",
      _id: item.agentId,
    }));
    e.preventDefault();
    const userData = {
      agentId: jwt(localStorage.getItem("auth")).id,
      office_id: localStorage.getItem("active_office_id"),
      office_name: localStorage.getItem("active_office"),
      category: "Agent Request to Hire Transaction Coordinator",
      message: summary?.propertyDetail?.streetAddress,
      transactionId: summary._id,
      contactType: contactData,
      isview: `${contactDataNew?.firstName} ${contactDataNew?.lastName}`,
    };

    const response = await user_service.noticeAdmin(userData);

    if (response) {
      const userDatan = {
        hire_coordinator: "yes",
      };
      await user_service.TransactionUpdate(params.id, userDatan);
      setLoader({ isActive: false });
      setToaster({
        type: "Success",
        isShow: true,
        message: "Transaction Coordinator hired successfully.",
      });

      // noticeGetAllData();
      setTimeout(() => {
        setToaster((prevToaster) => ({ ...prevToaster, isShow: false }));
        navigate(`/transaction-summary/${params.id}`);
      }, 2000);
    } else {
      setLoader({ isActive: false });
      setToaster({
        types: "error",
        isShow: true,
        toasterBody: response.data.message,
        message: "Error",
      });
      setTimeout(() => {
        setToaster((prevToaster) => ({ ...prevToaster, isShow: false }));
        // navigate("/control-panel/documents/");
      }, 2000);
    }
  };
  return (
    <div className="bg-secondary float-left w-100 pt-4">
      <Loader isActive={isActive} />
      {isShow && (
        <Toaster
          types={types}
          isShow={isShow}
          toasterBody={toasterBody}
          message={message}
        />
      )}
      <main className="page-wrapper">
        <div className="content-overlay transaction_tab_view mt-0">
        <div className="row">
          <AllTab />
          {/* <!-- Page container--> */}
          <div className="col-md-12">
            <div className="content-overlay">
              <div className="col-md-12 pb-md-3">
                <div className="bg-light border rounded-3 p-3 w-100">
                  <h6 className="text-center mb-2">
                    Transaction Management Services.
                  </h6>
                  <h3
                    className="text-center"
                    style={{
                      color: "#0000cb",
                      fontSize: "32px",
                    }}
                  >
                    Work Smarter... Not Harder!
                  </h3>
                    <div className="d-lg-flex d-md-flex d-sm-flex d-block justify-content-center">
                    <div className="" style={{ fontSize: "20px" }}>
                      <b>Will manage and coordinate</b>
                      <br />
                      <b>your real estate file from</b>
                      <br />
                      <b>Under Contract to Closing</b>
                    </div>
                    <div className="ms-lg-5 ms-md-5 ms-sm-5 ms-0"
                      style={{
                        backgroundColor: "#0000cb",
                        color: "white",
                        borderRadius: "16px",
                        fontSize: "16px",
                        padding: "10px",
                      }}
                    >
                      <li>Brokeragentbase expert.</li>
                      <li>Paid and closing.</li>
                    </div>
                  </div>
                </div>
                <div className="bg-light border rounded-3 p-3 mt-3 w-100 mb-3 hire_coordinator">
                  <p>
                    Listing or Buyer Side $499.00 paid at closing. Duel
                    Agent/Both Sides $699.00 paid at closing.
                  </p>
                  <a href="#">
                    The average agents can save up to 18 hours per transaction
                    using our services - that's a significant time saving for
                    each deal you close.
                  </a>
                  <p className="mt-2">
                    Start by creating a new transaction on Brokeragentbase.com.
                    You will need to complete the following steps in the
                    transaction so your transaction coordinator has the
                    information they need to be able to complete the work.
                  </p>

                  <p>*Complete the property address</p>

                  <p>*Upload any documents you currently have</p>

                  <p>*Enter your client's name(s).</p>
                  <p>*Enter the sellers agents info</p>
                  <p>*Enter the clients Lenders Info if you have a lender</p>
                  <p>*Enter your clients Title Company’s Info</p>
                  <p>
                    *Enter any other vendors you or your client will be using.
                  </p>
                  <p>
                    *Enter your commission amount in the commissions/Financial
                    page. If you're a buyer's agent any commission being paid by
                    a seller cannot exceed what you have on your buyer/agent
                    agreement. Enter this amount.
                  </p>
                  <p>*Enter the Earnest Money info.</p>
                  <p>
                    Your transaction coordinator will take over your file from
                    here and you can see the progress within Brokeragentbase.com
                    with any notes. If your transaction coordinator needs
                    anything from you they will reach out to you directly.{" "}
                  </p>
                  <p>
                    Work Smarter... Not Harder! Will manage and coordinate your
                    real estate file from Under Contract to Closing.
                  </p>
                  <p>*Brokeragentbase.com Experts</p>
                  <p>*Paid at Closing" </p>
                  <p>*Will manage your File(s) so you can sell Real Estate.</p>
                  <p>*Put our experienced team to work for you.</p>
                  <p>
                    A Good Transaction Manager = More Time and More Time = More
                    Money.
                  </p>
                  {summary.hire_coordinator === "yes" ? (
                    <button
                      className="btn btn-primary"
                      style={{
                        cursor: "pointer",
                        color: "black",
                        backgroundColor: "#dedef7",
                      }}
                      title="Transaction Coordinator has already been hired."
                    >
                      Ready To Start
                    </button>
                  ) : (
                    <button className="btn btn-primary" onClick={handleSubmit}>
                      Ready To Start
                    </button>
                  )}
                </div>
              </div>
            </div>
          </div>
        </div>
        </div>
      </main>
    </div>
  );
};

export default HireTransaction;

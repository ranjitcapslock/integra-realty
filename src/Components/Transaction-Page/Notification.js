import React, { useState, useEffect } from "react";
import { useParams } from "react-router-dom";
import user_service from "../service/user_service.js";
import Loader from "../../Pages/Loader/Loader.js";
import Toaster from "../../Pages/Toaster/Toaster.js";
import AllTab from "../../Pages/AllTab.js";
import jwt from "jwt-decode";

import BootstrapSwitchButton from "bootstrap-switch-button-react";

const Notification = () => {
  const [loader, setLoader] = useState({ isActive: null });
  const { isActive } = loader;
  const [toaster, setToaster] = useState({
    types: null,
    isShow: null,
    toasterBody: "",
    message: "",
  });

  const [notificationId, setNotificationId] = useState("");
  const { types, isShow, toasterBody, message } = toaster;
  const params = useParams();
  const [notificationSettings, setNotificationSettings] = useState({
    // matchLandingPage: false,
    listing_expiration_date: false,
    report_Bug: false,
    notice_Email: false,
    birthday_Wishes: false,
    star_Rating: false,
    licence_expiration_date: false,
    document_Mail: false,
    utility_Tool: false,
  });

  const handleSwitchChange = async (isChecked, notificationType) => {
    const updatedValue = isChecked ? "on" : "off";
    const userData = {
      agentId: jwt(localStorage.getItem("auth")).id,
      transactionId: params.id,
      [notificationType]: updatedValue,
    };

    try {
      setLoader({ isActive: true });
      let response;
      
      if (notificationId) {
        response = await user_service.notificationUpdate(notificationId, userData);
        console.log(response);
      } else {
        // First time, post new data
        response = await user_service.notificationPost(userData);
        if (response && response.data._id) {
          setNotificationId(response.data._id); // Store the ID for future updates
        }
      }

      if (response) {
        setLoader({ isActive: false });
        setToaster({
          type: "Notification",
          isShow: true,
          message: notificationId ? "Notification Updated Successfully" : "Notification Added Successfully",
        });

        setTimeout(() => {
          setToaster((prevToaster) => ({ ...prevToaster, isShow: false }));
        }, 2000);

        setNotificationSettings((prevSettings) => ({
          ...prevSettings,
          [notificationType]: isChecked,
        }));
      }
    } catch (err) {
      setLoader({ isActive: false });
      setToaster({
        type: "API Error",
        isShow: true,
        message: "API Error",
      });
    }
  };

  // const handleSwitchChange = async (isChecked, notificationType) => {
  //   const updatedValue = isChecked ? "on" : "off";
  //   const userData = {
  //     agentId: jwt(localStorage.getItem("auth")).id,
  //     transactionId: params.id,
  //     [notificationType]: updatedValue,
  //   };

  //   try {
  //     setLoader({ isActive: true });
  //     const response = await user_service.notificationPost(userData);

  //     if (response) {
  //       setLoader({ isActive: false });
  //       setToaster({
  //         type: "Notification Add",
  //         isShow: true,
  //         message: "Notification Added Successfully",
  //       });

  //       setTimeout(() => {
  //         setToaster((prevToaster) => ({ ...prevToaster, isShow: false }));
  //       }, 2000);

  //       setNotificationSettings((prevSettings) => ({
  //         ...prevSettings,
  //         [notificationType]: isChecked,
  //       }));
  //     }
  //   } catch (err) {
  //     setLoader({ isActive: false });
  //     setToaster({
  //       type: "API Error",
  //       isShow: true,
  //       message: "API Error",
  //     });
  //   }
  // };

  useEffect(() => {
    window.scrollTo(0, 0);
    setLoader({ isActive: true });
    notificationGetAllData();
  }, []);

  const notificationGetAllData = async () => {
    try {
      const response = await user_service.notificationGetAll(params.id);
      setLoader({ isActive: false });
      if (response) {
        const data = response.data.data;
        console.log(data[0]._id);
        setNotificationId(data[0]?._id); 
        setNotificationSettings({
          // matchLandingPage: data[0]?.matchLandingPage === "on",
          listing_expiration_date: data[0]?.listing_expiration_date === "on",
          report_Bug: data[0]?.report_Bug === "on",
          notice_Email: data[0]?.notice_Email === "on",
          birthday_Wishes: data[0]?.birthday_Wishes === "on",
          star_Rating: data[0]?.star_Rating === "on",
          licence_expiration_date: data[0]?.licence_expiration_date === "on",
          document_Mail: data[0]?.document_Mail === "on",
          utility_Tool: data[0]?.utility_Tool === "on",
        });
      }
    } catch (err) {
      setLoader({ isActive: false });
      // setToaster({
      //   type: "API Error",
      //   isShow: true,
      //   message: "Failed to fetch notification settings",
      // });
    }
  };

  return (
    <div className="bg-secondary float-left w-100 pt-4 mb-4">
      <Loader isActive={isActive} />
      {isShow && (
        <Toaster
          types={types}
          isShow={isShow}
          toasterBody={toasterBody}
          message={message}
        />
      )}

      <main className="">
        <div className="">
          <div className="content-overlay transaction_tab_view mt-0">
            <div className="row">
              <AllTab />
              <div className="col-md-12">
                <div className="bg-light border rounded-3 mb-2 p-3">
                  <div className="col-md-12 py-2">
                    <h3 className="text-left mb-0 justify-content-start d-flex align-items-center">
                      Notification
                    </h3>
                  </div>
                </div>
                <div className="bg-light border rounded-3 p-3 mt-3 float-start w-100">
                  {/* <h3 className="mb-3">Property Details:</h3>
                  <hr className="my-3"/> */}
                  <ul
                    className="nav nav-tabs float-start w-100 border-bottom pb-3 mb-2"
                    role="tablist"
                  >
                    <li className="nav-item">
                      <a
                        href="#home1"
                        className="nav-link active"
                        data-bs-toggle="tab"
                        role="tab"
                      >
                        {/* <i className="fi-home me-2"></i> */}
                        Agent Email Notifications
                      </a>
                    </li>
                    {/* <li className="nav-item">
                      <a
                        href="#profile1"
                        className="nav-link"
                        data-bs-toggle="tab"
                        role="tab"
                      >
                        Buyer Transaction Notifications
                      </a>
                    </li> */}

                    {/* <li className="nav-item">
                      <a
                        href="#profile2"
                        className="nav-link"
                        data-bs-toggle="tab"
                        role="tab"
                      >
                        Seller Transaction Notifications
                      </a>
                    </li> */}
                  </ul>

                  <div className="tab-content float-start w-100">
                    <div
                      className="tab-pane fade show active"
                      id="home1"
                      role="tabpanel"
                    >
                      <div className="agent_transaction_notifications table-responsive">
                        <table className="table">
                          <thead>
                            <tr>
                              {/* <th>#</th> */}
                              <th className="w-50">Activity</th>
                              <th className="w-50">Status</th>
                              {/* <th>As Email</th> */}
                            </tr>
                          </thead>
                          <tbody>
                            {/* <tr>
                              <td>Email me the Property Landing Page</td>
                              <td>
                                <BootstrapSwitchButton
                                  checked={
                                    notificationSettings.matchLandingPage
                                  }
                                  size="lg"
                                  onlabel="On"
                                  offlabel="Off"
                                  onChange={(isChecked) =>
                                    handleSwitchChange(
                                      isChecked,
                                      "matchLandingPage"
                                    )
                                  }
                                />
                              </td>
                            </tr> */}
                            <tr>
                              <td>Email me the Listing Expire Reminder</td>
                              <td>
                                <BootstrapSwitchButton
                                  checked={
                                    notificationSettings.listing_expiration_date
                                  }
                                  size="lg"
                                  onlabel="On"
                                  offlabel="Off"
                                  onChange={(isChecked) =>
                                    handleSwitchChange(
                                      isChecked,
                                      "listing_expiration_date"
                                    )
                                  }
                                />
                              </td>
                            </tr>
                            <tr>
                              <td>Email me the Report a Bug</td>
                              <td>
                                <BootstrapSwitchButton
                                  checked={notificationSettings.report_Bug}
                                  size="lg"
                                  onlabel="On"
                                  offlabel="Off"
                                  onChange={(isChecked) =>
                                    handleSwitchChange(isChecked, "report_Bug")
                                  }
                                />
                              </td>
                            </tr>
                            <tr>
                              <td>Email me the Notice Email</td>
                              <td>
                                <BootstrapSwitchButton
                                  checked={notificationSettings.notice_Email}
                                  size="lg"
                                  onlabel="On"
                                  offlabel="Off"
                                  onChange={(isChecked) =>
                                    handleSwitchChange(
                                      isChecked,
                                      "notice_Email"
                                    )
                                  }
                                />
                              </td>
                            </tr>
                            <tr>
                              <td>Email me the Birthday Wishes</td>
                              <td>
                                <BootstrapSwitchButton
                                  checked={notificationSettings.birthday_Wishes}
                                  size="lg"
                                  onlabel="On"
                                  offlabel="Off"
                                  onChange={(isChecked) =>
                                    handleSwitchChange(
                                      isChecked,
                                      "birthday_Wishes"
                                    )
                                  }
                                />
                              </td>
                            </tr>
                            <tr>
                              <td>Email me the Star Rating Review</td>
                              <td>
                                <BootstrapSwitchButton
                                  checked={notificationSettings.star_Rating}
                                  size="lg"
                                  onlabel="On"
                                  offlabel="Off"
                                  onChange={(isChecked) =>
                                    handleSwitchChange(isChecked, "star_Rating")
                                  }
                                />
                              </td>
                            </tr>
                            <tr>
                              <td>Email me the License Expire Reminder</td>
                              <td>
                                <BootstrapSwitchButton
                                  checked={
                                    notificationSettings.licence_expiration_date
                                  }
                                  size="lg"
                                  onlabel="On"
                                  offlabel="Off"
                                  onChange={(isChecked) =>
                                    handleSwitchChange(
                                      isChecked,
                                      "licence_expiration_date"
                                    )
                                  }
                                />
                              </td>
                            </tr>
                            {/* <tr>
                              <td>Email me the Document Mail</td>
                              <td>
                                <BootstrapSwitchButton
                                  checked={notificationSettings.document_Mail}
                                  size="lg"
                                  onlabel="On"
                                  offlabel="Off"
                                  onChange={(isChecked) =>
                                    handleSwitchChange(
                                      isChecked,
                                      "document_Mail"
                                    )
                                  }
                                />
                              </td>
                            </tr> */}
                            {/* <tr>
                              <td>Email me the Utility Tool</td>
                              <td>
                                <BootstrapSwitchButton
                                  checked={notificationSettings.utility_Tool}
                                  size="lg"
                                  onlabel="On"
                                  offlabel="Off"
                                  onChange={(isChecked) =>
                                    handleSwitchChange(
                                      isChecked,
                                      "utility_Tool"
                                    )
                                  }
                                />
                              </td>
                            </tr> */}
                          </tbody>
                        </table>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </main>
    </div>
  );
};

export default Notification;

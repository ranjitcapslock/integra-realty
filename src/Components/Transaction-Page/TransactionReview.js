import React, { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import { NavLink } from "react-router-dom";

import user_service from "../service/user_service";
import Loader from "../../Pages/Loader/Loader.js";
import Toaster from "../../Pages/Toaster/Toaster";
import AllTab from "../../Pages/AllTab";
import jwt from "jwt-decode";
import Swal from "sweetalert2";
import "sweetalert2/dist/sweetalert2.css";

const TransactionReview = () => {
  const [loader, setLoader] = useState({ isActive: null });
  const { isActive } = loader;
  const [toaster, setToaster] = useState({
    types: null,
    isShow: null,
    toasterBody: "",
    message: "",
  });
  const { types, isShow, toasterBody, message } = toaster;

  const [transactionsummary, setTransactionsummary] = useState("");
  const [transactionFundingRequest, setTransactionFundingRequest] =
    useState("");
  const params = useParams();
  const [documentValues, setDocumentValues] = useState([]);
  const [getDocument, setGetDocument] = useState([]);
  
  const [currentPage, setCurrentPage] = useState(0);
  const [totalRecords, setTotalRecords] = useState(0);
  const [newListing, setNewListing] = useState("");

  const TransactionGetById = async () => {
    if (params.id) {
      await user_service.transactionGetById(params.id).then((response) => {
        if (response) {
          setLoader({ isActive: false });
          setTransactionsummary(response.data);
        }
      });
    }
  };
  const TransactionFundingRequest = async () => {
    await user_service.transactionFundingRequest(params.id).then((response) => {
      if (response) {
        setLoader({ isActive: false });
        setTransactionFundingRequest(response.data);
      }
    });
  };

  useEffect(() => {
    window.scrollTo(0, 0);
    setLoader({ isActive: true });
    TransactionGetDeposit(params.id);
    TransactionGetById(params.id);
    TransactionFundingRequest(params.id);
    transactionDocumentAll();
    Getrequireddoucmentno();
    Detailsaddeddata();
  }, []);

  const transactionDocumentAll = async () => {
    await user_service.transactionDocumentGet(params.id).then((response) => {
      if (response) {
        setLoader({ isActive: false });
        const approvedItems = response.data.data.filter(
          (C) => C.approvalStatus === "Yes"
        );

        const approvedCount = approvedItems.length;
        setTotalRecords(approvedCount);
        setGetDocument(response.data.data);
        const documentValuess = [];
        response.data.data?.map((postt) => {
          documentValuess.push(postt.documentType);
        });
        setDocumentValues(documentValuess);
      }
    });
  };

  const numberPerPage = 10;

  const startingIndex = (currentPage - 1) * numberPerPage + 1;
  const endingIndex = Math.min(currentPage * numberPerPage, totalRecords);
  const dataCountOnPage = endingIndex - startingIndex + 1;

  const approvealldocuments = async (e) => {
    Swal.fire({
      title: "Are you sure?",
      text: "All documents will be  approved at on click!",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Yes, approve all!",
    }).then(async (result) => {
      if (result.isConfirmed) {
        try {
          e.preventDefault();
          const updateData = {
            approvalStatus: "Yes",
          };

          setLoader({ isActive: true });
          await user_service
            .transactionDocumentallUpdate(params.id, updateData)
            .then((response) => {
              if (response) {
                setLoader({ isActive: false });
                transactionDocumentAll();
                Getrequireddoucmentno();
              }
            });
        } catch (error) {
          Swal.fire(
            "Error",
            "An error occurred while approving the documents.",
            "error"
          );
          console.error(error);
        }
      }
    });
  };

  const resetalldocuments = async (e) => {
    Swal.fire({
      title: "Are you sure?",
      text: "All documents will be  reset at on click!",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Yes, reset all!",
    }).then(async (result) => {
      if (result.isConfirmed) {
        try {
          e.preventDefault();
          const updateData = {
            approvalStatus: "Reject",
          };

          setLoader({ isActive: true });
          await user_service
            .transactionDocumentallUpdate(params.id, updateData)
            .then((response) => {
              if (response) {
                setLoader({ isActive: false });
                transactionDocumentAll();
              }
            });
        } catch (error) {
          Swal.fire(
            "Error",
            "An error occurred while resetting the documents.",
            "error"
          );
          console.error(error);
        }
      }
    });
  };

  const [documentcheckapprove, setDocumentcheckapprove] = useState("");
  const handleCheckBoxdocuments = (e) => {
    const isChecked = e.target.checked;
    if (isChecked) {
      setDocumentcheckapprove(e.target.value);
    } else {
      setDocumentcheckapprove("");
    }
  };

  const handleUpdatetransaction = async () => {
    try {
      const userDataa = {
        trans_status: "submited",
        approval: "pending",
        confirmation: "pending",
      };
      await user_service
        .TransactionUpdate(params.id, userDataa)
        .then((response) => {
          if (response.status === 200) {
            TransactionGetById(params.id);
            // window.location.reload();
            setToaster({
              type: "Add Transaction",
              isShow: true,
              message: "Transaction Updated Successfully",
            });
            setTimeout(() => {
              setToaster((prevToaster) => ({ ...prevToaster, isShow: false }));
            }, 3000);
          } else {
            setLoader({ isActive: false });
            setToaster({
              types: "error",
              isShow: true,
              message: "Error",
            });
          }
        });
    } catch (error) {
      setLoader({ isActive: false });
      setToaster({
        types: "error",
        isShow: true,
        message: "Error",
      });
    }
  };

  const currentDate = new Date();
  const formattedDate = currentDate.toISOString();
  

  const updatetransaction = async (e) => {
    e.preventDefault();
    try {
      const updateData = {
        approval: "complete",
        confirmation: "inprogress",
        filing: "pending",
        funding: "pending",
        trans_status: "approve",
        approvalDate:formattedDate,
      };
      
      await user_service
        .TransactionUpdate(params.id, updateData)
        .then((response) => {
          if (response.status === 200) {
            const userDatan = {
              addedBy: jwt(localStorage.getItem("auth")).id,
              agentId: jwt(localStorage.getItem("auth")).id,
              transactionId: params.id,
              message: "Transaction has been approavl by management.",

              note_type: "Review",
            };
            const responsen = user_service.transactionNotes(userDatan);

            TransactionGetById(params.id);
          
            setToaster({
              type: "Add Transaction",
              isShow: true,
              toasterBody: response.data.message,
              message: "Transaction Phase Update Successfully",
            });
            setTimeout(() => {
              setToaster((prevToaster) => ({ ...prevToaster, isShow: false }));
            }, 3000);
          } else {
            setLoader({ isActive: false });
            setToaster({
              types: "error",
              isShow: true,
              toasterBody: response.data.message,
              message: "Error",
            });
          }
        });
    } catch (error) {
      setLoader({ isActive: false });
      setToaster({
        types: "error",
        isShow: true,
        toasterBody: error.response
          ? error.response.data.message
          : error.message,
        message: "Error",
      });
    }
  };

  const [handleCheckBoxfundc, setHandleCheckBoxfundc] = useState("");
  const handleCheckBoxfundcfunction = (e) => {
    const isChecked = e.target.checked;
    if (isChecked) {
      setHandleCheckBoxfundc(e.target.value);
    } else {
      setHandleCheckBoxfundc("");
    }
  };

  const updatetransactionconfirm = async (e) => {
    e.preventDefault();
    try {
      const updateData = {
        confirmation: "complete",
        funding: "inprogress",
        filing: "pending",
        trans_status: "confirmed",
        confirmationDate:formattedDate
      };
      await user_service
        .TransactionUpdate(params.id, updateData)
        .then((response) => {
          if (response.status === 200) {
            TransactionGetById(params.id);
            const userDatan = {
              addedBy: jwt(localStorage.getItem("auth")).id,
              agentId: jwt(localStorage.getItem("auth")).id,
              transactionId: params.id,
              message: "Transaction has been confirmation by management.",
              // transaction_property_address: "ADDRESS 2",
              note_type: "Review",
            };
            const responsen = user_service.transactionNotes(userDatan);

            setToaster({
              type: "Update Transaction",
              isShow: true,
              toasterBody: response.data.message,
              message: "Transaction Update Successfully",
            });
            setTimeout(() => {
              setToaster((prevToaster) => ({ ...prevToaster, isShow: false }));
            }, 3000);
          } else {
            setLoader({ isActive: false });
            setToaster({
              types: "error",
              isShow: true,
              toasterBody: response.data.message,
              message: "Error",
            });
          }
        });
    } catch (error) {
      setLoader({ isActive: false });
      setToaster({
        types: "error",
        isShow: true,
        toasterBody: error.response
          ? error.response.data.message
          : error.message,
        message: "Error",
      });
    }
  };

  const [handleCheckBoxfundpaidagent, setHandleCheckBoxfundpaidagent] =
    useState("");
  const handleCheckBoxfundpaidagentfunction = (e) => {
    const isChecked = e.target.checked;
    if (isChecked) {
      setHandleCheckBoxfundpaidagent(e.target.value);
    } else {
    
      setHandleCheckBoxfundpaidagent("");
    }
  };

  const updatetransactionpaidagent = async (e) => {
    e.preventDefault();
    try {
      const updateData = {
        funding: "complete",
        filing: "inprogress",
        trans_status: "funding_complete",
        fundingDate:formattedDate
      };
      await user_service
        .TransactionUpdate(params.id, updateData)
        .then((response) => {
          if (response.status === 200) {
            const userDatan = {
              addedBy: jwt(localStorage.getItem("auth")).id,
              agentId: jwt(localStorage.getItem("auth")).id,
              transactionId: params.id,
              message: "Transaction has been funded by management.",
              // transaction_property_address: "ADDRESS 2",
              note_type: "Review",
            };
            const responsen = user_service.transactionNotes(userDatan);

            TransactionGetById(params.id);
            // window.location.reload();
            setToaster({
              type: "Update Transaction",
              isShow: true,
              toasterBody: response.data.message,
              message: "Transaction Update Successfully",
            });
            setTimeout(() => {
              setToaster((prevToaster) => ({ ...prevToaster, isShow: false }));
            }, 3000);
          } else {
            setLoader({ isActive: false });
            setToaster({
              types: "error",
              isShow: true,
              toasterBody: response.data.message,
              message: "Error",
            });
          }
        });
    } catch (error) {
      setLoader({ isActive: false });
      setToaster({
        types: "error",
        isShow: true,
        toasterBody: error.response
          ? error.response.data.message
          : error.message,
        message: "Error",
      });
    }
  };

  const [handleCheckBoxfiling, setHandleCheckBoxfiling] = useState("");
  const handleCheckBoxfilingfunction = (e) => {
    const isChecked = e.target.checked;
    if (isChecked) {
      // Checkbox is checked
      setHandleCheckBoxfiling(e.target.value);
      //console.log("Checkbox is checked");
    } else {
      // Checkbox is not checked
      //console.log("Checkbox is not checked");
      setHandleCheckBoxfiling("");
    }
  };

  const updatetransactionfiling = async (e) => {
    e.preventDefault();
    try {
      // Update transaction
      const updateData = {
        filing: "filed_complted",
        trans_status: "filed",
        filingDate:formattedDate
      };
      const response = await user_service.TransactionUpdate(
        params.id,
        updateData
      );
      
      if (response.status === 200) {
        const responseData = await user_service.Testmoinals(params.id);
      //  console.log(responseData);
        const userDatan = {
          addedBy: jwt(localStorage.getItem("auth")).id,
          agentId: jwt(localStorage.getItem("auth")).id,
          transactionId: params.id,
          message: "Transaction has been filled by management.",
          // transaction_property_address: "ADDRESS 2",
          note_type: "Review",
        };
        const responsen = user_service.transactionNotes(userDatan);

        // Update listing
        const updateDataListing = {
          status: "Sold",
        };

        const response2 = await user_service.listingUpdate(
          transactionsummary?.propertyDetail._id,
          updateDataListing
        );
        setNewListing(response2.data);
        // Update UI or perform other actions
        TransactionGetById(params.id);

        setToaster({
          type: "Update Transaction",
          isShow: true,
          toasterBody: response.data.message,
          message: "Transaction Update Successfully",
        });

        setTimeout(() => {
          setToaster((prevToaster) => ({ ...prevToaster, isShow: false }));
        }, 3000);
      } else {
        setLoader({ isActive: false });
        setToaster({
          types: "error",
          isShow: true,
          toasterBody: response.data.message,
          message: "Error",
        });
      }
    } catch (error) {
      setLoader({ isActive: false });
      setToaster({
        types: "error",
        isShow: true,
        toasterBody: error.response
          ? error.response.data.message
          : error.message,
        message: "Error",
      });
    }
  };

  const getTimeDifference = (createdDate) => {
    const currentDate = new Date();
    const diffInMilliseconds = currentDate - new Date(createdDate);
    const diffInMinutes = Math.floor(diffInMilliseconds / 60000);
    const hours = Math.floor(diffInMinutes / 60);
    const days = Math.floor(hours / 24);
    const months = Math.floor(days / 30);
    const years = Math.floor(months / 12);

    if (years > 0) {
      return `${years} year${years > 1 ? "s" : ""}`;
    } else if (months > 0) {
      return `${months} month${months > 1 ? "s" : ""}`;
    } else if (days > 0) {
      return `${days} day${days > 1 ? "s" : ""}`;
    } else if (hours > 24) {
      const remainingHours = hours - Math.floor(hours / 24) * 24;
      return `1 day, ${remainingHours} hr${remainingHours > 1 ? "s" : ""}`;
    } else {
      return `${hours} hr${hours > 1 ? "s" : ""}, ${diffInMinutes % 60} min${
        diffInMinutes % 60 > 1 ? "s" : ""
      }`;
    }
  };

  const [getrequireddoucmentno, setGetrequireddoucmentno] = useState([]);
  const [requireddoucmentsdone, setRequireddoucmentsdone] = useState(false);

  const Getrequireddoucmentno = async () => {
    const agentId = jwt(localStorage.getItem("auth")).id;
    await user_service
      .Getrequireddoucmentno(agentId, params.id)
      .then((response) => {
        // setLoader({ isActive: false })
        if (response) {
          //console.log(response.data)
          setGetrequireddoucmentno(response.data);

          if (
            response.data.totalContactNoRequired == 0 &&
            response.data.totalDetailsNoRequired == 0 &&
            response.data.totalDocumentNoRequired == 0
          ) {
            setRequireddoucmentsdone(true);
          }
        }
      });
  };

  const [requireddetailsaddeddata, setRequiredDetailsaddeddata] = useState([]);
  const Detailsaddeddata = async () => {
    const agentId = jwt(localStorage.getItem("auth")).id;
    await user_service.Detailsaddeddata(agentId, params.id).then((response) => {
      setLoader({ isActive: false });
      if (response) {
        const requiredYesArray = response.data.data.filter(
          (item) => item.is_documentRequired === "yes"
        );
        // const groupedBycategory = response.data.data.reduce((acc, item) => {
        //     const category = item.category;
        //     if (!acc[category]) {
        //         acc[category] = [];
        //     }
        //     acc[category].push(item);
        //     return acc;
        // }, {});

        //   console.log(requiredYesArray);
        //   console.log("requiredYesArray");
        setRequiredDetailsaddeddata(requiredYesArray);
        // setDetailsaddeddata(groupedBycategory);
      }
    });
  };

  const [deposit, setDeposit] = useState([]);

  const [depositAdd, setDepositAdd] = useState(0);
  const [release, setRelease] = useState(0);
  const [lastMonthHolding, setLastMonthHolding] = useState(0);
  const [holdingNow, setHoldingNow] = useState(0);

  const TransactionGetDeposit = async () => {
    await user_service.transactionDepositGet(params.id).then((response) => {
      if (response && typeof response.data === "object") {
        setDeposit(response.data);

        let currentdate = new Date();
        let currentmonth = currentdate.getMonth();
        let currentyear = currentdate.getFullYear();

        let lastmonthsaving = 0;
        let lastmonthwithdraw = 0;
        let currentmonthsaving = 0;
        let totalsaving = 0;
        let totaldebit = 0;

        response.data.data.forEach((item) => {
          const itemDate = new Date(item.entry_date);
          const itemYear = itemDate.getFullYear();
          const itemMonth = itemDate.getMonth();

          if (currentyear === itemYear && currentmonth === itemMonth) {
            if (item.entry_type === "deposit") {
              currentmonthsaving += parseInt(item.amount);
              totalsaving += parseInt(item.amount);
            } else {
              currentmonthsaving -= parseInt(item.amount);
              totaldebit += parseInt(item.amount);
            }
          } else {
            if (item.entry_type === "deposit") {
              lastmonthsaving += parseInt(item.amount);
            } else if (item.entry_type === "release") {
              lastmonthwithdraw += parseInt(item.amount);
            }
          }
        });
        setLastMonthHolding(lastmonthsaving - lastmonthwithdraw);
        setHoldingNow(
          lastmonthsaving - lastmonthwithdraw + totalsaving - totaldebit
        );
        setDepositAdd(totalsaving);
        setRelease(totaldebit);
      }
    });
  };
  return (
    <div className="bg-secondary float-left w-100 pt-4 mb-4">
      <Loader isActive={isActive} />
      {isShow && (
        <Toaster
          types={types}
          isShow={isShow}
          toasterBody={toasterBody}
          message={message}
        />
      )}
      <main className="page-wrapper">
        <div className="">
          <div className="content-overlay transaction_tab_view mt-0">
            <div className="row">
              <AllTab />
              <div className="col-md-12 transaction_review">
                {/* <StatusBar /> */}
                  <div className="bg-light p-3 border-0 rounded-3 shadow-sm float-start w-100">
                    <div className="row">
                      <div className="col-lg-3 col-md-3 col-sm-6 col-6 text-center">
                        <h6 className="mb-3">Approval</h6>
                        <p
                          className="mb-0"
                          style={{
                            color:
                              transactionsummary?.approval === "complete"
                                ? "white"
                                : "" ||
                                  transactionsummary?.approval === "inprogress"
                                ? "white"
                                : "" ||
                                  transactionsummary?.approval === "pending"
                                ? "#000000"
                                : "",

                            background:
                              transactionsummary?.approval === "complete"
                                ? "green"
                                : "" ||
                                  transactionsummary?.approval === "inprogress"
                                ? "#a9846a"
                                : "" ||
                                  transactionsummary?.approval === "pending"
                                ? "#ececec"
                                : "",

                            padding:
                              transactionsummary?.approval === "complete"
                                ? "7px"
                                : "" ||
                                  transactionsummary?.approval === "inprogress"
                                ? "7px"
                                : "" ||
                                  transactionsummary?.approval === "pending"
                                ? "7px"
                                : "",
                          }}
                        >
                          <span className="review_span">
                            {transactionsummary?.approval
                              ? transactionsummary.approval
                                  .charAt(0)
                                  .toUpperCase() +
                                transactionsummary.approval.slice(1)
                              : "Pending"}
                          </span>
                        </p>
                      </div>
                      <div className="col-lg-3 col-md-3 col-sm-6 col-6 text-center">
                        <h6 className="mb-3">Confirmation</h6>
                        <p
                          style={{
                            color:
                              transactionsummary?.confirmation === "complete"
                                ? "white"
                                : "" ||
                                  transactionsummary?.confirmation ===
                                    "inprogress"
                                ? "white"
                                : "" ||
                                  transactionsummary?.confirmation === "pending"
                                ? "#000000"
                                : "",

                            background:
                              transactionsummary?.confirmation === "complete"
                                ? "green"
                                : "" ||
                                  transactionsummary?.confirmation ===
                                    "inprogress"
                                ? "#FFD700"
                                : "" ||
                                  transactionsummary?.confirmation === "pending"
                                ? "#ececec"
                                : "",

                            padding:
                              transactionsummary?.confirmation === "complete"
                                ? "7px"
                                : "" ||
                                  transactionsummary?.confirmation ===
                                    "inprogress"
                                ? "7px"
                                : "" ||
                                  transactionsummary?.confirmation === "pending"
                                ? "7px"
                                : "",
                          }}
                        >
                          <span className="review_span">
                            {transactionsummary?.confirmation
                              ? transactionsummary.confirmation
                                  .charAt(0)
                                  .toUpperCase() +
                                transactionsummary.confirmation.slice(1)
                              : "Pending"}
                          </span>
                        </p>
                      </div>
                      <div className="col-lg-3 col-md-3 col-sm-6 col-6 text-center">
                        <h6 className="mb-3">Funding</h6>
                        <p
                          style={{
                            color:
                              transactionsummary?.funding === "complete"
                                ? "white"
                                : "" ||
                                  transactionsummary?.funding === "inprogress"
                                ? "white"
                                : "" ||
                                  transactionsummary?.funding === "pending"
                                ? "#000000"
                                : "",

                            background:
                              transactionsummary?.funding === "complete"
                                ? "green"
                                : "" ||
                                  transactionsummary?.funding === "inprogress"
                                ? "#FFD700"
                                : "" ||
                                  transactionsummary?.funding === "pending"
                                ? "#ececec"
                                : "",

                            padding:
                              transactionsummary?.funding === "complete"
                                ? "7px"
                                : "" ||
                                  transactionsummary?.funding === "inprogress"
                                ? "7px"
                                : "" ||
                                  transactionsummary?.funding === "pending"
                                ? "7px"
                                : "",
                          }}
                        >
                          <span className="review_span">
                            {transactionsummary.funding
                              ? transactionsummary.funding
                                  .charAt(0)
                                  .toUpperCase() +
                                transactionsummary.funding.slice(1)
                              : "Pending"}
                          </span>
                        </p>
                      </div>
                      <div className="col-lg-3 col-md-3 col-sm-6 col-6 text-center">
                        <h6 className="mb-3">Filing</h6>
                        <p
                          style={{
                            color:
                              transactionsummary?.filing === "filed_complted"
                                ? "white"
                                : "" ||
                                  transactionsummary?.filing === "inprogress"
                                ? "white"
                                : "" || transactionsummary?.filing === "pending"
                                ? "#000000"
                                : "",

                            background:
                              transactionsummary?.filing === "filed_complted"
                                ? "green"
                                : "" ||
                                  transactionsummary?.filing === "inprogress"
                                ? "#FFD700"
                                : "" || transactionsummary?.filing === "pending"
                                ? "#ececec"
                                : "",

                            padding:
                              transactionsummary?.filing === "filed_complted"
                                ? "7px"
                                : "" ||
                                  transactionsummary?.filing === "inprogress"
                                ? "7px"
                                : "" || transactionsummary?.filing === "pending"
                                ? "7px"
                                : "",
                          }}
                        >
                          <span className="review_span">
                            <span className="review_span">
                              {transactionsummary?.filing
                                ? transactionsummary?.filing ===
                                  "filed_complted"
                                  ? "Completed"
                                  : transactionsummary?.filing
                                : ""}
                            </span>
                          </span>
                        </p>
                      </div>
                    </div>

                    <div className="float-left w-100">
                      <h3 className="mb-3 pb-3 border-bottom">Summary</h3>
                    </div>

                    <div className="row">
                      <div className="col-sm-6">
                        <h6 className="mb-2">Funding</h6>
                        <p>Standard Funding</p>
                      </div>

                      <div className="col-sm-6">
                        <h6 className="mb-2">Office</h6>
                        <p className="">
                          InDepth Realty Corporate{" "}
                          <NavLink
                            className="ms-2"
                            to={`/update-office/${params.id}`}
                          >
                            change
                          </NavLink>
                        </p>
                      </div>

                      <div className="col-sm-6">
                        <h6 className="mb-2">Sellers</h6>
                        <p>
                          {transactionsummary.contact1 &&
                          transactionsummary.contact1.length > 0 ? (
                            transactionsummary.contact1[0] !== null ? (
                              <>
                                <p
                                  className=""
                                  key={transactionsummary.contact1[0]._id}
                                >
                                  {transactionsummary.contact1[0].data
                                    .organization &&
                                  transactionsummary.contact1[0].data
                                    .organization !== ""
                                    ? transactionsummary.contact1[0].data
                                        .organization
                                    : transactionsummary.contact1[0].data
                                        .firstName}
                                  &nbsp;
                                  {transactionsummary.contact1[0].data.lastName}
                                </p>
                                {/* <p className="mb-2 fs-sm text-muted">{transactionsummary.contact1[0].data.firstName
                                                                    }&nbsp;{transactionsummary.contact1[0].data.lastName}</p> */}
                              </>
                            ) : null
                          ) : (
                            <p>---</p>
                          )}
                        </p>
                      </div>
                      {transactionsummary?.propertyDetail?.mlsNumber ? (
                        <>
                          <div className="col-sm-6">
                            <h6 className="mb-2">MLS #</h6>
                            <p>
                              {transactionsummary?.propertyDetail?.mlsNumber}
                            </p>
                          </div>
                        </>
                      ) : (
                        "---"
                      )}

                      <div className="col-sm-6">
                        <h6 className="mb-2">SalePrice</h6>
                        <p>
                          {transactionsummary?.propertyDetail?.price ? (
                            <>
                              <p>{transactionsummary?.propertyDetail?.price}</p>
                            </>
                          ) : (
                            "---"
                          )}
                        </p>
                      </div>

                      <div className="col-sm-6">
                        <h6 className="mb-2">Time Spent</h6>
                        <p>
                          {transactionsummary?.timeStamp
                            ? getTimeDifference(transactionsummary?.timeStamp)
                            : "--:--:--"}
                        </p>
                      </div>

                      <div className="col-sm-6">
                        <h6 className="mb-2">EM Held</h6>
                        <p className="">
                          {holdingNow ?? "None"}&nbsp;&nbsp;{" "}
                          <NavLink
                            className="btn btn-primary btn-sm"
                            to={`/update-deposit/${params.id}`}
                          >
                            Update
                          </NavLink>
                        </p>
                      </div>

                      <div className="col-md-6">
                        <h6 className="mb-2">Submitted</h6>
                        <p className="">
                          {" "}
                          {transactionsummary?.updated
                            ? new Date(
                                transactionsummary?.updated
                              ).toLocaleString("en-US", {
                                month: "short",
                                day: "numeric",
                                year: "numeric",
                                hour: "numeric",
                                minute: "numeric",
                              })
                            : ""}
                          {/* {getTimeDifference(transactionsummary?.updated)} */}
                        </p>
                        {/* <h6>Approval</h6> */}
                      </div>

                      {transactionsummary?.approval === "complete" ? (
                        ""
                      ) : (
                        <div className="">
                          <div className="col-md-12 d-flex">
                            <h6 className="mb-2 me-2">Approval</h6>
                            <p className="mb-2">
                              <NavLink>{totalRecords}</NavLink> &nbsp;Document
                            </p>
                          </div>
                          <hr className="mt-2" />
                          <div className="row mt-3">
                            <div className="col-md-3">
                              <label className="col-form-label p-0">
                                Bulk Approve:
                              </label>
                              {/* <p><span className="review_span">In Progress</span></p> */}
                            </div>

                            {/* <div className="col-md-3">
                                            <h6> <NavLink>{totalRecords}&nbsp; Document</NavLink></h6>
                                        </div> */}

                            <div className="col-md-5">
                              <NavLink onClick={approvealldocuments}>
                                Approve All ({totalRecords})
                              </NavLink>
                            </div>

                            <div className="col-md-4">
                              <NavLink onClick={resetalldocuments}>
                                {" "}
                                Reset All
                              </NavLink>
                            </div>
                          </div>

                          {getDocument.map((item, index) => (
                            <div className="row mt-3" key={index}>
                              <div className="col-md-3">
                                <label
                                  className="col-form-label p-0"
                                  style={{
                                    color:
                                      item.approvalStatus === "Yes"
                                        ? "#009900"
                                        : item.approvalStatus === "Reject"
                                        ? "#800"
                                        : "#000",
                                  }}
                                >
                                  {/* {item.approvalStatus === "Yes" ? "Approved" : "Not Reviewed"} */}
                                  {item.approvalStatus === "Yes"
                                    ? "Approved"
                                    : item.approvalStatus === "Reject"
                                    ? "Denied"
                                    : "Not Reviewed"}
                                </label>
                              </div>
                              <div className="col-md-5">
                                <object>
                                  <a
                                    className="review_span"
                                    href={`/transactionDoc/${params.id}/${item._id}`}
                                    target="_blank"
                                    rel="noreferrer"
                                  >
                                    {/* {console.log(item.documentType)} */}
                                    {item.documentName ?? item.documentType}
                                  </a>
                                </object>
                              </div>
                              {/* <div className="col-md-4">
                                                                <label className="col-form-label p-0">Contract</label>
                                                            </div> */}
                            </div>
                          ))}

                          {getrequireddoucmentno?.totalDocumentNoRequiredapprove ==
                          "0" ? (
                            <div className="mt-3">
                              {console.log(getrequireddoucmentno)}

                              {requireddoucmentsdone ? (
                                <>
                                  <input
                                    className="form-check-input me-2"
                                    id="form-check-1"
                                    onClick={(e) => handleCheckBoxdocuments(e)}
                                    type="checkbox"
                                    name="required"
                                    value="checkBox"
                                  />
                                  <label
                                    className="form-check-label"
                                    id="radio-level1"
                                  >
                                    All documents are correctly signed and
                                    executed
                                  </label>
                                  <br />
                                  {documentcheckapprove ? (
                                    <NavLink
                                      className="btn btn-primary btn-sm mb-2 mt-3 ms-3"
                                      style={{ background: "#fd5631" }}
                                      onClick={(e) => updatetransaction(e)}
                                    >
                                      Approve
                                    </NavLink>
                                  ) : (
                                    <NavLink
                                      className="btn btn-primary btn-sm mb-2 mt-3 ms-3"
                                      style={{ cursor: "no-drop" }}
                                    >
                                      Approve
                                    </NavLink>
                                  )}
                                </>
                              ) : (
                                <p className="mb-2">
                                  Please Upload all required documents.
                                </p>
                              )}
                            </div>
                          ) : null}

                          <hr className="my-4" />
                        </div>
                      )}

                      <div className="row">
                        <div className="col-md-8">
                          {transactionsummary.approval === "complete" ? (
                            <>
                              {!transactionsummary?.confirmation ||
                              transactionsummary?.confirmation ===
                                "complete" ? (
                                ""
                              ) : (
                                <>
                                  {/* <p className="">Forms:  Complete 2 forms, when needed.</p> */}
                                  <div className="row">
                                    <div className="col-md-4">
                                      <label className="col-form-label p-0">
                                        Funding Request:
                                      </label>
                                    </div>
                                    <div className="col-md-4">
                                      {/* <strong>Not Reviewed</strong> */}

                                      {transactionFundingRequest?.data?.length >
                                      0 ? (
                                        <p>
                                          <span className="review_span">
                                            Done{" "}
                                          </span>
                                          &nbsp; &nbsp;
                                          <a
                                            href={
                                              transactionFundingRequest?.data[0]
                                                ?.invoice_url
                                            }
                                            target="_blank"
                                          >
                                            <span className="review_span">
                                              View
                                            </span>
                                          </a>
                                        </p>
                                      ) : (
                                        <p>
                                          <span className="review_span">
                                            <NavLink
                                              to={`/transaction-details-funding/${params.id}`}
                                            >
                                              Create Funding Request
                                            </NavLink>
                                          </span>
                                        </p>
                                      )}
                                    </div>
                                    {transactionFundingRequest?.data?.length >
                                    0 ? (
                                        <div className="row">
                                          <div className="col-md-4">
                                            <label className="col-form-label p-0">
                                              Funding Confirmation:
                                            </label>
                                          </div>

                                          <div className="col-md-4">
                                            {transactionFundingRequest?.data[0]
                                              ?.confirmation_status ===
                                            "confirmed" ? (
                                              <>
                                                <p>
                                                  <span className="review_span">
                                                    Done{" "}
                                                  </span>
                                                  &nbsp; &nbsp;
                                                  <a
                                                    href={
                                                      transactionFundingRequest
                                                        ?.data[0]
                                                        ?.confirm_invoice_url
                                                    }
                                                    target="_blank"
                                                  >
                                                    <span className="review_span">
                                                      View
                                                    </span>
                                                  </a>
                                                  <NavLink
                                                    className="ms-2"
                                                    to={`/transaction-details-funding-confirmation/${params.id}`}
                                                  >
                                                    -Edit
                                                  </NavLink>
                                                </p>
                                              </>
                                            ) : (
                                              <NavLink
                                                to={`/transaction-details-funding-confirmation/${params.id}`}
                                              >
                                                {" "}
                                                {transactionFundingRequest
                                                  ?.data[0]
                                                  ?.confirmation_status ===
                                                "confirmed"
                                                  ? "Done"
                                                  : "Create Confirmation"}{" "}
                                              </NavLink>
                                            )}
                                          </div>
                                        </div>
                                    ) : (
                                      ""
                                    )}
                                  </div>
                                </>
                              )}
                            </>
                          ) : (
                            ""
                          )}

                          {requireddoucmentsdone ? (
                            <>
                              {getDocument &&
                              getDocument.length > 0 &&
                              getDocument[0].approvalStatus === "Yes" ? (
                                <>
                                  {transactionFundingRequest &&
                                  transactionFundingRequest?.data[0] &&
                                  transactionFundingRequest?.data[0]
                                    ?.confirmation_status === "confirmed" ? (
                                    <>
                                      {transactionFundingRequest?.data?.length >
                                      0 ? (
                                        transactionFundingRequest?.data[0]
                                          ?.confirmation_status ===
                                          "confirmed" &&
                                        transactionsummary?.confirmation !=
                                          "complete" ? (
                                          <>
                                            {transactionsummary?.approval ===
                                            "complete" ? (
                                              <div className=" mt-3">
                                                <input
                                                  className="form-check-input me-2"
                                                  id="form-check-1"
                                                  onClick={(e) =>
                                                    handleCheckBoxfundcfunction(
                                                      e
                                                    )
                                                  }
                                                  type="checkbox"
                                                  name="required"
                                                  value="handleCheckBoxfundc"
                                                />
                                                <label
                                                  className="form-check-label"
                                                  id="radio-level1"
                                                >
                                                  Funds to the broker have been
                                                  received.
                                                </label>
                                                <br />

                                                <NavLink
                                                  className="btn btn-secondary btn-sm mb-2 mt-3"
                                                  style={{
                                                    background: "#777777",
                                                    color: "white",
                                                  }}
                                                  onClick={(e) =>
                                                    handleUpdatetransaction(e)
                                                  }
                                                >
                                                  Send Back
                                                </NavLink>
                                                {handleCheckBoxfundc ? (
                                                  <NavLink
                                                    className="btn btn-primary btn-sm mb-2 mt-3 ms-3"
                                                    style={{
                                                      background: "#fd5631",
                                                    }}
                                                    onClick={(e) =>
                                                      updatetransactionconfirm(
                                                        e
                                                      )
                                                    }
                                                  >
                                                    Set to Confirmed
                                                  </NavLink>
                                                ) : (
                                                  <NavLink
                                                    className="btn btn-primary btn-sm mb-2 mt-3 ms-3"
                                                    style={{
                                                      cursor: "no-drop",
                                                    }}
                                                  >
                                                    Set to Confirmed
                                                  </NavLink>
                                                )}
                                              </div>
                                            ) : (
                                              ""
                                            )}
                                          </>
                                        ) : (
                                         <>
                                         
                                         </>
                                        )
                                      ) : (
                                        ""
                                      )}
                                      {transactionsummary.confirmation ===
                                      "complete" ? (
                                        <>
                                          {
                                            // !transactionsummary?.funding || transactionsummary?.funding === "complete" ?
                                            transactionsummary?.funding ===
                                            "complete" ? (
                                              ""
                                            ) : (
                                              <>
                                                <div className="mt-3">
                                                  <input
                                                    className="form-check-input me-2"
                                                    id="form-check-1"
                                                    onClick={(e) =>
                                                      handleCheckBoxfundpaidagentfunction(
                                                        e
                                                      )
                                                    }
                                                    type="checkbox"
                                                    name="required"
                                                    value="handleCheckBoxfundpaidagent"
                                                  />
                                                  <label
                                                    className="form-check-label"
                                                    id="radio-level1"
                                                  >
                                                    Funds to the agent have been
                                                    paid.
                                                  </label>
                                                </div>
                                                <div className="">
                                                  <NavLink
                                                    className="btn btn-secondary btn-sm mb-2 mt-3"
                                                    style={{
                                                      background: "#777777",
                                                      color: "white",
                                                    }}
                                                    onClick={(e) =>
                                                      updatetransaction(e)
                                                    }
                                                  >
                                                    Back To Approved
                                                  </NavLink>
                                                  {handleCheckBoxfundpaidagent ? (
                                                    <NavLink
                                                      className="btn btn-primary btn-sm mb-2 mt-3 ms-5"
                                                      style={{
                                                        background: "#fd5631",
                                                      }}
                                                      onClick={(e) =>
                                                        updatetransactionpaidagent(
                                                          e
                                                        )
                                                      }
                                                    >
                                                      Set to Funded
                                                    </NavLink>
                                                  ) : (
                                                    <NavLink
                                                      className="btn btn-primary btn-sm mb-2 mt-3 ms-5"
                                                      style={{
                                                        cursor: "no-drop",
                                                      }}
                                                    >
                                                      Set to Funded
                                                    </NavLink>
                                                  )}
                                                </div>
                                              </>
                                            )
                                          }
                                        </>
                                      ) : (
                                        ""
                                      )}

                                      {
                                        // !transactionsummary?.filing || transactionsummary?.funding === "inprogress" || transactionsummary?.funding === "" || transactionsummary?.filing === "complete" ?
                                        transactionsummary?.funding !=
                                        "complete" ? (
                                          ""
                                        ) : (
                                          <>
                                            <div className="mt-3">
                                              {transactionsummary?.filing ===
                                              "filed_complted" ? (
                                                <>
                                                  <h6>Finished</h6>
                                                  <p>
                                                    This transaction was
                                                    successfully filed.
                                                  </p>

                                                  <div className="">
                                                    <NavLink
                                                      className="btn btn-primary btn-sm mb-2 mt-3 ms-5"
                                                      style={{
                                                        background: "#777777",
                                                      }}
                                                      onClick={(e) =>
                                                        updatetransactionpaidagent(
                                                          e
                                                        )
                                                      }
                                                    >
                                                      Recall form Filed
                                                    </NavLink>
                                                  </div>
                                                </>
                                              ) : (
                                                <>
                                                  <input
                                                    className="form-check-input me-2"
                                                    id="form-check-1"
                                                    onClick={(e) =>
                                                      handleCheckBoxfilingfunction(
                                                        e
                                                      )
                                                    }
                                                    type="checkbox"
                                                    name="required"
                                                    value="handleCheckBoxfiling"
                                                  />
                                                  <label
                                                    className="form-check-label"
                                                    id="radio-level1"
                                                  >
                                                    Transaction is ready to be
                                                    filed.
                                                  </label>
                                                </>
                                              )}
                                            </div>
                                            <div className="">
                                              {handleCheckBoxfiling ? (
                                                <>
                                                  {transactionsummary?.filing ===
                                                  "filed_complted" ? (
                                                    <></>
                                                  ) : (
                                                    <>
                                                      <NavLink
                                                        className="btn btn-primary btn-sm mb-2 mt-3 ms-5"
                                                        style={{
                                                          background: "#777777",
                                                          color: "white",
                                                        }}
                                                        onClick={(e) =>
                                                          updatetransactionconfirm(
                                                            e
                                                          )
                                                        }
                                                      >
                                                        Back To Confirmed
                                                      </NavLink>
                                                      <NavLink
                                                        className="btn btn-primary btn-sm mb-2 mt-3 ms-5"
                                                        style={{
                                                          background: "#fd5631",
                                                        }}
                                                        onClick={(e) =>
                                                          updatetransactionfiling(
                                                            e
                                                          )
                                                        }
                                                      >
                                                        Set to Filed
                                                      </NavLink>
                                                    </>
                                                  )}
                                                </>
                                              ) : (
                                                ""
                                              )}
                                            </div>
                                          </>
                                        )
                                      }
                                      <p className="mt-3">
                                        The funding data for this transaction
                                        will be automatically
                                        <br /> synced to your QuickBooks Online
                                        when Set to Funded the first time.
                                      </p>
                                    </>
                                  ) : (
                                    ""
                                  )}
                                </>
                              ) : (
                               <>
                                {transactionFundingRequest &&
                                  transactionFundingRequest?.data[0] &&
                                  transactionFundingRequest?.data[0]
                                    ?.confirmation_status === "confirmed" ? (
                                    <>
                                      {transactionFundingRequest?.data?.length >
                                      0 ? (
                                        transactionFundingRequest?.data[0]
                                          ?.confirmation_status ===
                                          "confirmed" &&
                                        transactionsummary?.confirmation !=
                                          "complete" ? (
                                          <>
                                            {transactionsummary?.approval ===
                                            "complete" ? (
                                              <div className=" mt-3">
                                                <input
                                                  className="form-check-input me-2"
                                                  id="form-check-1"
                                                  onClick={(e) =>
                                                    handleCheckBoxfundcfunction(
                                                      e
                                                    )
                                                  }
                                                  type="checkbox"
                                                  name="required"
                                                  value="handleCheckBoxfundc"
                                                />
                                                <label
                                                  className="form-check-label"
                                                  id="radio-level1"
                                                >
                                                  Funds to the broker have been
                                                  received.
                                                </label>
                                                <br />

                                                <NavLink
                                                  className="btn btn-secondary btn-sm mb-2 mt-3"
                                                  style={{
                                                    background: "#777777",
                                                    color: "white",
                                                  }}
                                                  onClick={(e) =>
                                                    handleUpdatetransaction(e)
                                                  }
                                                >
                                                  Send Back
                                                </NavLink>
                                                {handleCheckBoxfundc ? (
                                                  <NavLink
                                                    className="btn btn-primary btn-sm mb-2 mt-3 ms-3"
                                                    style={{
                                                      background: "#fd5631",
                                                    }}
                                                    onClick={(e) =>
                                                      updatetransactionconfirm(
                                                        e
                                                      )
                                                    }
                                                  >
                                                    Set to Confirmed
                                                  </NavLink>
                                                ) : (
                                                  <NavLink
                                                    className="btn btn-primary btn-sm mb-2 mt-3 ms-3"
                                                    style={{
                                                      cursor: "no-drop",
                                                    }}
                                                  >
                                                    Set to Confirmed
                                                  </NavLink>
                                                )}
                                              </div>
                                            ) : (
                                              ""
                                            )}
                                          </>
                                        ) : (
                                         <>
                                         
                                         </>
                                        )
                                      ) : (
                                        ""
                                      )}
                                      {transactionsummary.confirmation ===
                                      "complete" ? (
                                        <>
                                          {
                                            // !transactionsummary?.funding || transactionsummary?.funding === "complete" ?
                                            transactionsummary?.funding ===
                                            "complete" ? (
                                              ""
                                            ) : (
                                              <>
                                                <div className="mt-3">
                                                  <input
                                                    className="form-check-input me-2"
                                                    id="form-check-1"
                                                    onClick={(e) =>
                                                      handleCheckBoxfundpaidagentfunction(
                                                        e
                                                      )
                                                    }
                                                    type="checkbox"
                                                    name="required"
                                                    value="handleCheckBoxfundpaidagent"
                                                  />
                                                  <label
                                                    className="form-check-label"
                                                    id="radio-level1"
                                                  >
                                                    Funds to the agent have been
                                                    paid.
                                                  </label>
                                                </div>
                                                <div className="">
                                                  <NavLink
                                                    className="btn btn-secondary btn-sm mb-2 mt-3"
                                                    style={{
                                                      background: "#777777",
                                                      color: "white",
                                                    }}
                                                    onClick={(e) =>
                                                      updatetransaction(e)
                                                    }
                                                  >
                                                    Back To Approved
                                                  </NavLink>
                                                  {handleCheckBoxfundpaidagent ? (
                                                    <NavLink
                                                      className="btn btn-primary btn-sm mb-2 mt-3 ms-5"
                                                      style={{
                                                        background: "#fd5631",
                                                      }}
                                                      onClick={(e) =>
                                                        updatetransactionpaidagent(
                                                          e
                                                        )
                                                      }
                                                    >
                                                      Set to Funded
                                                    </NavLink>
                                                  ) : (
                                                    <NavLink
                                                      className="btn btn-primary btn-sm mb-2 mt-3 ms-5"
                                                      style={{
                                                        cursor: "no-drop",
                                                      }}
                                                    >
                                                      Set to Funded
                                                    </NavLink>
                                                  )}
                                                </div>
                                              </>
                                            )
                                          }
                                        </>
                                      ) : (
                                        ""
                                      )}

                                      {
                                        // !transactionsummary?.filing || transactionsummary?.funding === "inprogress" || transactionsummary?.funding === "" || transactionsummary?.filing === "complete" ?
                                        transactionsummary?.funding !=
                                        "complete" ? (
                                          ""
                                        ) : (
                                          <>
                                            <div className="mt-3">
                                              {transactionsummary?.filing ===
                                              "filed_complted" ? (
                                                <>
                                                  <h6>Finished</h6>
                                                  <p>
                                                    This transaction was
                                                    successfully filed.
                                                  </p>

                                                  <div className="">
                                                    <NavLink
                                                      className="btn btn-primary btn-sm mb-2 mt-3 ms-5"
                                                      style={{
                                                        background: "#777777",
                                                      }}
                                                      onClick={(e) =>
                                                        updatetransactionpaidagent(
                                                          e
                                                        )
                                                      }
                                                    >
                                                      Recall form Filed
                                                    </NavLink>
                                                  </div>
                                                </>
                                              ) : (
                                                <>
                                                  <input
                                                    className="form-check-input me-2"
                                                    id="form-check-1"
                                                    onClick={(e) =>
                                                      handleCheckBoxfilingfunction(
                                                        e
                                                      )
                                                    }
                                                    type="checkbox"
                                                    name="required"
                                                    value="handleCheckBoxfiling"
                                                  />
                                                  <label
                                                    className="form-check-label"
                                                    id="radio-level1"
                                                  >
                                                    Transaction is ready to be
                                                    filed.
                                                  </label>
                                                </>
                                              )}
                                            </div>
                                            <div className="">
                                              {handleCheckBoxfiling ? (
                                                <>
                                                  {transactionsummary?.filing ===
                                                  "filed_complted" ? (
                                                    <></>
                                                  ) : (
                                                    <>
                                                      <NavLink
                                                        className="btn btn-primary btn-sm mb-2 mt-3 ms-5"
                                                        style={{
                                                          background: "#777777",
                                                          color: "white",
                                                        }}
                                                        onClick={(e) =>
                                                          updatetransactionconfirm(
                                                            e
                                                          )
                                                        }
                                                      >
                                                        Back To Confirmed
                                                      </NavLink>
                                                      <NavLink
                                                        className="btn btn-primary btn-sm mb-2 mt-3 ms-5"
                                                        style={{
                                                          background: "#fd5631",
                                                        }}
                                                        onClick={(e) =>
                                                          updatetransactionfiling(
                                                            e
                                                          )
                                                        }
                                                      >
                                                        Set to Filed
                                                      </NavLink>
                                                    </>
                                                  )}
                                                </>
                                              ) : (
                                                ""
                                              )}
                                            </div>
                                          </>
                                        )
                                      }
                                      <p className="mt-3">
                                        The funding data for this transaction
                                        will be automatically
                                        <br /> synced to your QuickBooks Online
                                        when Set to Funded the first time.
                                      </p>
                                    </>
                                  ) : (
                                    ""
                                  )}
                               </>
                              )}
                            </>
                          ) : (
                            ""
                          )}
                        </div>
                        {
                          transactionsummary.approval ==="complete" ?
                          <div className="col-md-4">
                          {transactionFundingRequest &&
                          transactionFundingRequest.data &&
                          transactionFundingRequest.data.length > 0 &&
                          transactionFundingRequest.data[0].comments ? (
                            <p className="pull-right">
                              <b>Notes:</b>{" "}
                              {transactionFundingRequest.data[0].comments}
                            </p>
                          ) : (
                            ""
                          )}
                        </div>
                        :""
                        }
                      
                      </div>
                    </div>
                 
                </div>
              </div>
              {/* <div className="col-md-4">
                                <NavLink className="btn btn-primary btn-sm mb-2" >View Snapshot</NavLink><br />
                                <hr />
                                <p className="mt-5">Sent Form History</p>
                                <span>No funding documents sent.</span>
                            </div> */}
            </div>
          </div>
        </div>
      </main>
    </div>
  );
};

export default TransactionReview;

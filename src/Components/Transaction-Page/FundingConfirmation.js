import React, { useEffect, useState } from "react";

import { useParams, NavLink, useNavigate } from "react-router-dom";
import user_service from "../service/user_service";
import Loader from "../../Pages/Loader/Loader.js";
import Toaster from "../../Pages/Toaster/Toaster";
import AllTab from "../../Pages/AllTab";
import jwt from "jwt-decode";
import axios from "axios";
import "jspdf-autotable";
import { jsPDF } from "jspdf";
import image from "../../Components/img/logoNew.png";
import _ from "lodash";

const FundingConfirmation = () => {
  const [loader, setLoader] = useState({ isActive: null });
  const { isActive } = loader;
  const [toaster, setToaster] = useState({
    types: null,
    isShow: null,
    toasterBody: "",
    message: "",
  });
  const { types, isShow, toasterBody, message } = toaster;

  const [summary, setSummary] = useState("");
  const [request, setRequest] = useState(false);
  const [getCommission, setGetCommission] = useState("");

  const initialRows = [];
  const initialPayoutItem = [];

  const initialValues = {
    companyName: "",
    email: "",
    contactName: "",
    lastName: "",
    phone: "",
    commission: "",
    fileNumber: "",
    comments: "",
    invoice_url: "",
  };

 
  const [totalAmountNew, setTotalAmountNew] = useState(0);

  
  const [formValues, setFormValues] = useState(initialValues);
  const [pdfPath, setPdfPath] = useState("");

  const [fundingGet, setFundingGet] = useState("");

  const [rows, setRows] = useState(initialRows);
  const [selectedOption, setSelectedOption] = useState("");
  const [totalAmount, setTotalAmount] = useState(0);
  const [netAmount, setNetAmount] = useState(0);

  const [rowsPayout, setRowsPayout] = useState(initialPayoutItem);
  const [selectedPayout, setSelectedPayout] = useState("");
  const [submitData, setSubmitData] = useState("");
  const navigate = useNavigate();
  const [getContact, setGetContact] = useState("");
  const [contactData, setContactData] = useState("");
  const [commissionChanged, setCommissionChanged] = useState(false);

  const params = useParams();

 

  const TransactionGetById = async () => {
    await user_service.transactionGetById(params.id).then((response) => {
      setLoader({ isActive: false });
      if (response) {
        const responseData = response.data;
        setSummary(responseData);

        const contact2Data =
          responseData.contact2 &&
          responseData.contact2[0] &&
          responseData.contact2[0].data;
        const contactDatas = contact2Data || "";
        setContactData(contactDatas);

        const contact3Data =
          responseData.contact3 &&
          responseData.contact3[0] &&
          responseData.contact3[0].data;
        const contactDatas3 = contact3Data || "";
        console.log(contactDatas3._id);
        ContactGetId(contactDatas3._id);
      }
    });
  };

  const ContactGetId = async (contactId) => {
    try {
      if (contactId) {
        const response = await user_service.contactGetById(contactId);
        if (response) {
          setGetContact(response.data);
        }
      }
    } catch (error) {
      console.error("Error fetching contact details:", error);
    }
  };

  const CommissionGet = async () => {
    setLoader({ isActive: true });
    await user_service.commissionGet(params.id).then((response) => {
      if (response) {
        setLoader({ isActive: false });

        setGetCommission(response.data.data[0] || {});
      }
    });
  };

  const FundingRequestGet = async () => {
    if (params.id) {
      try {
        const response = await user_service.transactionFundingRequest(
          params.id
        );
        setLoader({ isActive: false });

        if (
          response &&
          response.data &&
          response.data.data &&
          response.data.data[0]
        ) {
          setFundingGet(response.data.data[0]);
          setFormValues({
            companyName: response.data.data[0]?.companyName || "",
            phone: response.data.data[0]?.phone || "",
            contactName: response.data.data[0]?.contactName || "",
            email: response.data.data[0]?.email || "",
            fileNumber: response.data.data[0]?.fileNumber || "",
          });

          let bonusAmount = [];
          let payoutItems = [];
          let bonusData = 0;
          let payoutData = 0;
          response.data.data[0].incomeItem.map((item) => {
            bonusData = parseInt(bonusData) + parseInt(item.amount);
            if (item.type) {
              bonusAmount.push(item);
            }
          });

          response.data.data[0].payoutItem.map((item) => {
            payoutData = parseInt(payoutData) + parseInt(item.amount);

            if (item.type) {
              payoutItems.push(item);
            }
          });

          // const totalEstimate = Number(totalAmountss) - Number(transactionFesData.transactionFee) ||
          // 0;
          // console.log(totalEstimate);
          // setNetAmount(totalEstimate);
          // setNetAmount(estimateData);

          setRows(bonusAmount);
          setRowsPayout(payoutItems);
        }
      } catch (error) {
        console.error(error);
      }
    }
  };

  useEffect(() => {
    window.scrollTo(0, 0);
    setLoader({ isActive: true });
    TransactionGetById(params.id);
    FundingRequestGet();
    CommissionGet(params.id);
  }, []);

  const [transactionFesData, setTransactionData] = useState({
    transactionFee: fundingGet.transactionFee ?? "",
  });

  const [transactionWire, setTransactionWire] = useState({
    wireFee: fundingGet.wireFee ?? "",
  });

  const [transactionFesPersonal, setTransactionFeePersonal] = useState({
    transactionPersonal:  fundingGet.transactionPersonal ?? "",
  });

  const [transactionHighValueFee, setTransactionHighValueFee] = useState({
    highValueFee : fundingGet.highValueFee ?? "",
  });

  const [transactionBuyerFee, setTransactionBuyerFee] = useState({
    buyerFee: fundingGet.buyerFee ?? "",
  });


  const [transactionSellerFee, setTransactionSellerFee] = useState({
    sellerFee: fundingGet.sellerFee ?? "",
  });

  const [transactionRisk, setTransactionRisk] = useState({
    managementFee:fundingGet.managementFee ?? "",
  });


  const handleChangeTransactionFee = (e) => {
    const { name, value } = e.target;
    setTransactionData({ ...transactionFesData, [name]: value });
  };

  const handleChangeWireFee = (e) => {
    const { name, value } = e.target;
    setTransactionWire({ ...transactionWire, [name]: value });
  };

  const handleChangePransactionPersonal = (e) => {
    const { name, value } = e.target;
    setTransactionFeePersonal({ ...transactionFesPersonal, [name]: value });
  };

  const handleChangeHighValueFee = (e) => {
    const { name, value } = e.target;
    setTransactionHighValueFee({ ...transactionHighValueFee, [name]: value });
};

const handleChangeBuyerFee = (e) => {
  const { name, value } = e.target;
  setTransactionBuyerFee({ ...transactionBuyerFee, [name]: value });
};



const handleChangesSellerFee = (e) => {
  const { name, value } = e.target;
  setTransactionSellerFee({ ...transactionSellerFee, [name]: value });
};



const handleChangeManagementFee = (e) => {
  const { name, value } = e.target;
  setTransactionRisk({ ...transactionRisk, [name]: value });
};




  
  useEffect(() => {
    setTransactionData({ transactionFee: fundingGet.transactionFee ?? "" });
  }, [fundingGet.transactionFee]);

  useEffect(() => {
    setTransactionWire({ wireFee: fundingGet.wireFee ?? "" });
  }, [fundingGet.wireFee]);

  useEffect(() => {
    setTransactionFeePersonal({ transactionPersonal: fundingGet.transactionPersonal ?? "" });
  }, [fundingGet.transactionPersonal]);

  useEffect(() => {
    setTransactionHighValueFee({ highValueFee: fundingGet.highValueFee ?? "" });
  }, [fundingGet.highValueFee]);

  useEffect(() => {
    setTransactionBuyerFee({ buyerFee: fundingGet.buyerFee ?? "" });
  }, [fundingGet.buyerFee]);

  useEffect(() => {
    setTransactionSellerFee({ sellerFee: fundingGet.sellerFee ?? "" });
  }, [fundingGet.sellerFee]);


  useEffect(() => {
    setTransactionRisk({ managementFee: fundingGet.managementFee ?? "" });
  }, [fundingGet.managementFee]);


  useEffect(() => {
    if (summary.phase === "canceled") {
      let transactionFeeForCalculation =
        getContact?.legacy_account === "yes" ? "0" : "0";

      setTransactionData({
        ...transactionFesData,
        transactionFee: transactionFeeForCalculation,
      });
      setTransactionWire("");
      setTransactionHighValueFee("");
      setTransactionBuyerFee("");
      setTransactionFeePersonal("");
      setTransactionSellerFee("");
      setTransactionRisk("");
      setTotalAmountNew(transactionFeeForCalculation);
    }
     else {
      let totalFeesAmount = Number(formValues.commission) || 0;

      if (rows && rows.length > 0) {
        totalFeesAmount += rows.reduce(
          (acc, obj) => acc + Number(obj.amount) || 0,
          0
        );
      }

      if (
        fundingGet?.commission !== null &&
        fundingGet?.commission !== undefined
      ) {
        const totaldata =
          totalFeesAmount + parseFloat(fundingGet?.commission || 0);
        setTotalAmount(totaldata);
      }

      if (commissionChanged) {
        setTotalAmount(totalFeesAmount);
      }
    }
  }, [rows, formValues, getContact, totalAmount, fundingGet]);

  useEffect(() => {
    if (summary.phase === "canceled") {
      let transactionFeeForCalculation =
        getContact?.legacy_account === "yes" ? "0" : "0";

      setTransactionData({
        ...transactionFesData,
        transactionFee: transactionFeeForCalculation,
      });
      setTransactionWire("");
      setTransactionHighValueFee("");
      setTransactionBuyerFee("");
      setTransactionFeePersonal("");
      setTransactionSellerFee("");
      setTransactionRisk("");
      setTotalAmountNew(transactionFeeForCalculation);
    } else {
      if (
        summary.type === "buy_commercial" ||
        summary.type === "sale_commercial" ||
        summary.type === "both_commercial" ||
        summary.type === "referral_commercial"
      ) {
        let totalPayoutAmount = 0;
        let totalPayoutAmountWireFee = 0;
        
        if (transactionFesData && transactionFesData?.transactionFee) {
          totalPayoutAmount = Number(transactionFesData.transactionFee) || 0;
        }
    
        if (transactionWire && transactionWire.wireFee) {
            totalPayoutAmountWireFee = Number(transactionWire.wireFee) || 0;
        }

        if (rowsPayout && rowsPayout.length > 0) {
          let result = rowsPayout.reduce(
              (acc, obj) => acc + (Number(obj.amount) || 0),
              0
          );
          totalPayoutAmount = result + totalPayoutAmount;
          const total = fundingGet?.estimate - result;
          setNetAmount(Math.max(total, 0));
      }
      setTotalAmountNew(totalPayoutAmount);

        if (totalAmount) {
            const netPayAmount = 
                Number(totalAmount) - 
                Number(totalPayoutAmount) -
                Number(totalPayoutAmountWireFee)
                 || 0;
            setNetAmount(netPayAmount < 0 ? 0 : netPayAmount);
        }
    
        if (totalAmountNew) {
            const netPayAmountNew = 
                Number(netAmount) + 
                Number(totalPayoutAmount) + 
                Number(totalPayoutAmountWireFee)|| 0;
            setTotalAmountNew(netPayAmountNew < 0 ? 0 : netPayAmountNew);
        }
      }
       else {
        if (summary.represent === "both") {
          if (getCommission.property_interest === "yes") {
            let totalPayoutAmount = 0;
            let totalPayoutAmountWireFee = 0;
            let totalPayoutAmountTransactionPersonal = 0;
            let totalPayoutAmountHighValueFee = 0;
        
            if (transactionFesData && transactionFesData.transactionFee) {
              totalPayoutAmount = Number(transactionFesData.transactionFee) || 0;
            }
        
            if (transactionWire && transactionWire.wireFee) {
                totalPayoutAmountWireFee = Number(transactionWire.wireFee) || 0;
            }
        
            if (transactionFesPersonal && transactionFesPersonal.transactionPersonal) {
                totalPayoutAmountTransactionPersonal = Number(transactionFesPersonal.transactionPersonal) || 0;
            }
        
            if (transactionHighValueFee && transactionHighValueFee.highValueFee) {
                totalPayoutAmountHighValueFee = Number(transactionHighValueFee.highValueFee) || 0;
            }
        
            if (rowsPayout && rowsPayout.length > 0) {
              let result = rowsPayout.reduce(
                (acc, obj) => acc + Number(obj.amount) || 0,
                0
              );
              totalPayoutAmount = result + totalPayoutAmount;
              const total = fundingGet?.estimate - result;
              setNetAmount(Math.max(total, 0));
            }
  
            setTotalAmountNew(totalPayoutAmount);

            if (totalAmount) {
                const netPayAmount = 
                    Number(totalAmount) - 
                    Number(totalPayoutAmount) -
                    Number(totalPayoutAmountWireFee) -
                    Number(totalPayoutAmountTransactionPersonal) -
                    Number(totalPayoutAmountHighValueFee) || 0;
                setNetAmount(netPayAmount < 0 ? 0 : netPayAmount);
            }
        
            if (totalAmountNew) {
                const netPayAmountNew = 
                    Number(netAmount) + 
                    Number(totalPayoutAmountHighValueFee) + 
                    Number(totalPayoutAmount) + 
                    Number(totalPayoutAmountWireFee) + 
                    Number(totalPayoutAmountTransactionPersonal) || 0;
                setTotalAmountNew(netPayAmountNew < 0 ? 0 : netPayAmountNew);
            }
        }
          else {
              let totalPayoutAmounts = 0;
      
              if (transactionBuyerFee && transactionBuyerFee.buyerFee) {
                  totalPayoutAmounts += Number(transactionBuyerFee.buyerFee) || 0;
              }
      
              if (transactionSellerFee && transactionSellerFee.sellerFee) {
                  totalPayoutAmounts += Number(transactionSellerFee.sellerFee) || 0;
              }
      
              if (transactionRisk && transactionRisk.managementFee) {
                  totalPayoutAmounts += Number(transactionRisk.managementFee) || 0;
              }
      
              if (transactionWire && transactionWire.wireFee) {
                  totalPayoutAmounts += Number(transactionWire.wireFee) || 0;
              }
      
              if (rowsPayout && rowsPayout.length > 0) {
                  let result = rowsPayout.reduce(
                      (acc, obj) => acc + (Number(obj.amount) || 0),
                      0
                  );
                  totalPayoutAmounts = result + totalPayoutAmounts;
                  const total = fundingGet?.estimate - result;
                  setNetAmount(Math.max(total, 0));
              }
              setTotalAmountNew(totalPayoutAmounts);
      
              if (totalAmount) {
                  const netPayAmount = Number(totalAmount) - totalPayoutAmounts;
                  setNetAmount(netPayAmount < 0 ? 0 : netPayAmount);
              }
      
              if (totalAmountNew) {
                  const netPayAmountNews = Number(netAmount) + Number(totalPayoutAmounts);
                  setTotalAmountNew(netPayAmountNews < 0 ? 0 : netPayAmountNews);
              }
          }
      }
         else {
          if (getCommission.property_interest === "yes") {
            let totalPayoutAmount = 0;
            let totalPayoutAmountWireFee = 0;
            let totalPayoutAmountTransactionPersonal = 0;
            let totalPayoutAmountHighValueFee = 0;
        
            if (transactionFesData && transactionFesData.transactionFee) {
              totalPayoutAmount = Number(transactionFesData.transactionFee) || 0;
            }
        
            if (transactionWire && transactionWire.wireFee) {
                totalPayoutAmountWireFee = Number(transactionWire.wireFee) || 0;
            }
        
            if (transactionFesPersonal && transactionFesPersonal.transactionPersonal) {
                totalPayoutAmountTransactionPersonal = Number(transactionFesPersonal.transactionPersonal) || 0;
            }
        
            if (transactionHighValueFee && transactionHighValueFee.highValueFee) {
                totalPayoutAmountHighValueFee = Number(transactionHighValueFee.highValueFee) || 0;
            }
        
            if (rowsPayout && rowsPayout.length > 0) {
                let result = rowsPayout.reduce(
                    (acc, obj) => acc + (Number(obj.amount) || 0),
                    0
                );
                totalPayoutAmount = result + totalPayoutAmount;
                const total = fundingGet?.estimate - result;
                setNetAmount(Math.max(total, 0));
            }
        
            setTotalAmountNew(totalPayoutAmount);
        

            if (totalAmount) {
                const netPayAmount = 
                    Number(totalAmount) - 
                    Number(totalPayoutAmount) -
                    Number(totalPayoutAmountWireFee) -
                    Number(totalPayoutAmountTransactionPersonal) -
                    Number(totalPayoutAmountHighValueFee) || 0;
                setNetAmount(netPayAmount < 0 ? 0 : netPayAmount);
            }
        
            if (totalAmountNew) {
                const netPayAmountNew = 
                    Number(netAmount) + 
                    Number(totalPayoutAmountHighValueFee) + 
                    Number(totalPayoutAmount) + 
                    Number(totalPayoutAmountWireFee) + 
                    Number(totalPayoutAmountTransactionPersonal) || 0;
                setTotalAmountNew(netPayAmountNew < 0 ? 0 : netPayAmountNew);
            }
        }
        else {
          let totalPayoutAmount = 0;
          let totalPayoutAmountWireFee = 0;
          let totalPayoutAmountHighValueFee = 0;

          if (transactionFesData && transactionFesData.transactionFee) {
            totalPayoutAmount = Number(transactionFesData.transactionFee) || 0;
          }
      
          if (transactionWire && transactionWire.wireFee) {
              totalPayoutAmountWireFee = Number(transactionWire.wireFee) || 0;
          }

          if (transactionHighValueFee && transactionHighValueFee.highValueFee) {
              totalPayoutAmountHighValueFee = Number(transactionHighValueFee.highValueFee) || 0;
          }
      
          if (rowsPayout && rowsPayout.length > 0) {
              let result = rowsPayout.reduce(
                  (acc, obj) => acc + (Number(obj.amount) || 0),
                  0
              );
              totalPayoutAmount = result + totalPayoutAmount;
              const total = fundingGet?.estimate - result;
              setNetAmount(Math.max(total, 0));
          }
      
          setTotalAmountNew(totalPayoutAmount);
      

          if (totalAmount) {
              const netPayAmount = 
                  Number(totalAmount) - 
                  Number(totalPayoutAmount) -
                  Number(totalPayoutAmountWireFee) -
                  Number(totalPayoutAmountHighValueFee) || 0;
              setNetAmount(netPayAmount < 0 ? 0 : netPayAmount);
          }
      
          if (totalAmountNew) {
              const netPayAmountNew = 
                  Number(netAmount) + 
                  Number(totalPayoutAmount) + 
                  Number(totalPayoutAmountWireFee) + 
                  Number(totalPayoutAmountHighValueFee)  || 0;
              setTotalAmountNew(netPayAmountNew < 0 ? 0 : netPayAmountNew);
          }
          }
        }
      }
    }
  }, [
    totalAmount,
    totalAmountNew,
    netAmount,
    rowsPayout,
    formValues,
    getContact,
    transactionFesData,
    transactionBuyerFee,
    transactionSellerFee,
    transactionRisk,
    transactionWire,
    transactionHighValueFee,
    transactionFesPersonal,
    summary.represent,
    summary.type,
    fundingGet._id,
    fundingGet,
  ]);

  const handleChanges = (e) => {
    const { name, value } = e.target;
    const newValue = parseFloat(value) || "";
    if (name === "commission") {
      setCommissionChanged(true);
    }

    setFormValues({
      ...formValues,
      [name]: newValue,
    });
  };

  const handleChange = (e) => {
    const { name, value } = e.target;
    setFormValues({ ...formValues, [name]: value });
  };

  const handleOptionChange = (e) => {
    const newValue = e.target.value;

    let chargedToName = "";

    if (summary?.represent === "buyer") {
      chargedToName = `${summary?.contact2?.[0]?.data?.firstName} ${summary?.contact2?.[0]?.data?.lastName}`;
    } else {
      chargedToName = `${summary?.contact1?.[0]?.data?.firstName} ${summary?.contact1?.[0]?.data?.lastName}`;
    }

    const isTypeExists = rows.some((row) => row.value === newValue);

    if (newValue && !isTypeExists) {
      const newRow = {
        type: newValue,
        amount: "",
        chargedto: newValue === "other" ? chargedToName : chargedToName,
      };
      setRows([...rows, newRow]);
    }
  };

  const handleChangeRow = (e, index, fieldName) => {
    const updatedValue = e.target.value;
    const updatedRows = [...rows];
    updatedRows[index][fieldName] = updatedValue;
    setRows(updatedRows);
  };

  const handleChangeRowPayout = (e, index, fieldName) => {
    const updatedValue = e.target.value;
    const updatedRows = [...rowsPayout];
    updatedRows[index][fieldName] = updatedValue;
    setRowsPayout(updatedRows);
  };

  const handleRemove = (index) => {
    const updatedRows = [...rows];
    updatedRows.splice(index, 1);
    setRows(updatedRows);
  };

  const handleChangeBonus = (e, index, fieldType) => {
    e.preventDefault();
    const { value } = e.target;
    const updatedRows = [...rows];
    updatedRows[index][fieldType] = value;
    setRows(updatedRows);
  };

  const handleInputChange = (e) => {
    const newValue = e.target.value;
    let chargedToName = "";

    if (summary?.represent === "buyer") {
      chargedToName = `${summary?.contact2?.[0]?.data?.firstName} ${summary?.contact2?.[0]?.data?.lastName}`;
    } else {
      chargedToName = `${summary?.contact1?.[0]?.data?.firstName} ${summary?.contact1?.[0]?.data?.lastName}`;
    }

    const isTypeExists = rowsPayout.some((row) => row.value === newValue);
    if (newValue && !isTypeExists) {
      const newRow = {
        type: newValue,
        amount: "",
        payto:
          newValue === "externalReferral" || newValue === "otherPayout"
            ? chargedToName
            : chargedToName,
      };
      setRowsPayout([...rowsPayout, newRow]);
    }
  };

  const handleChangePayout = (e, index, payoutType) => {
    e.preventDefault();
    const { value } = e.target;

    const updatedRowsPayout = [...rowsPayout];
    updatedRowsPayout[index][payoutType] = value;
    setRowsPayout(updatedRowsPayout);
  };

  const handleRemovePayout = (index) => {
    const updatedRows = [...rowsPayout];
    updatedRows.splice(index, 1);
    setRowsPayout(updatedRows);
  };


  const handleSubmit = async (e) => {
    if (fundingGet?._id) {
      if (summary.phase === "canceled") {
        const userData = {
          email: formValues.email,
          phone: formValues.phone ?? fundingGet.phone,
          companyName: formValues.companyName ?? fundingGet.companyName,
          contactName: formValues.contactName ?? fundingGet.contactName,
          commission: JSON.stringify(formValues.commission)
            ? JSON.stringify(formValues.commission)
            : fundingGet?.commission,
          fileNumber: formValues.fileNumber ?? fundingGet.fileNumber,
          incomeItem: rows,
          payoutItem: rowsPayout,
          estimate: fundingGet.estimate,

          transactionFee: transactionFesData.transactionFee,
          comments: formValues.comments ?? fundingGet?.comments,
          transactionId: summary._id,
          invoice_url: fundingGet?.invoice_url,
          addressProperty: summary?.propertyDetail?.streetAddress ?? "",
          stateProperty: summary?.propertyDetail?.state ?? "",
          typeProperty: summary?.propertyDetail?.propertyType ?? "",
          represent_Property:
            summary?.represent === "buyer"
              ? "Buyer"
              : summary?.represent === "seller"
              ? "Seller"
              : summary?.represent === "both"
              ? "Buyer&Seller"
              : summary?.represent === "referral"
              ? "Referral"
              : "",
          zipcode_Property: summary?.propertyDetail?.zipCode ?? "",
        };
      
        try {
          setLoader({ isActive: true });
          const response = await user_service.fundingRequestUpdate(
            fundingGet?._id,
            userData
          );
          if (response) {
            const userDatan = {
              addedBy: jwt(localStorage.getItem("auth")).id,
              agentId: jwt(localStorage.getItem("auth")).id,
              transactionId: params.id,
              message: "Standard Funding Confirmation Update.",
              // transaction_property_address: "ADDRESS 2",
              note_type: "Funding Confirmation",
            };
            const responsen = await user_service.transactionNotes(userDatan);
            setSubmitData(response.data);

            setLoader({ isActive: false });
            generatePDF(response.data);
          } else {
            setLoader({ isActive: false });
            setToaster({
              type: "error",
              isShow: true,
              toasterBody: "Error: Funding Update Failed",
              message: "Error",
            });
          }
        } catch (error) {
          setToaster({
            type: "error",
            isShow: true,
            toasterBody: "Error: " + error.message,
            message: "Error",
          });
          setTimeout(() => {
            setToaster((prevToaster) => ({ ...prevToaster, isShow: false }));
          }, 2000);
        }
      } 
      else {
        if (totalAmount === totalAmountNew) {
        e.preventDefault();
        const userData = {
          email: formValues.email,
          phone: formValues.phone ?? fundingGet.phone,
          companyName: formValues.companyName ?? fundingGet.companyName,
          contactName: formValues.contactName ?? fundingGet.contactName,
          commission: JSON.stringify(formValues.commission)
            ? JSON.stringify(formValues.commission)
            : fundingGet?.commission,
          fileNumber: formValues.fileNumber ?? fundingGet.fileNumber,
          incomeItem: rows,
          payoutItem: rowsPayout,
          estimate: JSON.stringify(netAmount),

          transactionFee: transactionFesData.transactionFee,
          comments: formValues.comments ?? fundingGet?.comments,
          transactionId: summary._id,
          invoice_url: fundingGet?.invoice_url,
          wireFee: transactionWire?.wireFee ?? "",
          buyerFee: transactionBuyerFee?.buyerFee ?? "",
          sellerFee: transactionSellerFee?.sellerFee ?? "",
          highValueFee: transactionHighValueFee.highValueFee ?? "",
          transactionPersonal: transactionFesPersonal.transactionPersonal ?? "",
          managementFee: transactionRisk?.managementFee ?? "",
          addressProperty: summary?.propertyDetail?.streetAddress ?? "",
          stateProperty: summary?.propertyDetail?.state ?? "",
          typeProperty: summary?.propertyDetail?.propertyType ?? "",
          represent_Property:
            summary?.represent === "buyer"
              ? "Buyer"
              : summary?.represent === "seller"
              ? "Seller"
              : summary?.represent === "both"
              ? "Buyer&Seller"
              : summary?.represent === "referral"
              ? "Referral"
              : "",
          zipcode_Property: summary?.propertyDetail?.zipCode ?? "",
        };

        try {
          setLoader({ isActive: true });
          const response = await user_service.fundingRequestUpdate(
            fundingGet?._id,
            userData
          );
          if (response) {
            const userDatan = {
              addedBy: jwt(localStorage.getItem("auth")).id,
              agentId: jwt(localStorage.getItem("auth")).id,
              transactionId: params.id,
              message: "Standard Funding Confirmation Update.",
              // transaction_property_address: "ADDRESS 2",
              note_type: "Funding Confirmation",
            };
            const responsen = await user_service.transactionNotes(userDatan);

            setSubmitData(response.data);

            setLoader({ isActive: false });
            generatePDF(response.data);
          } else {
            setLoader({ isActive: false });
            setToaster({
              type: "error",
              isShow: true,
              message: "Error",
            });
          }
        } catch (error) {
          setToaster({
            type: "error",
            isShow: true,
            toasterBody: "Error: " + error.message,
            message: "Error",
          });
          setTimeout(() => {
            setToaster((prevToaster) => ({ ...prevToaster, isShow: false }));
          }, 2000);
        }
      }
      else {
        setToaster({
          type: "error",
          isShow: true,
          toasterBody:
            "Income Item total amount or Payout Item total amount do not match. Please review your entries.",
          message: "Validation Error",
        });

        setTimeout(() => {
          setToaster((prevToaster) => ({ ...prevToaster, isShow: false }));
        }, 5000);
      }

      }
    }
  };

  const generatePDF = async (submitData) => {
    try {
      const doc = new jsPDF();

      const styles = `
            <style>
              body {
                font-family: Arial, sans-serif;
                margin: 0;
                padding: 20px;
              }
              h1 {
                text-align: center;
                font-size: 24px;
              }
              table {
                width: 100%;
                border-collapse: collapse;
                margin-top: 20px;
              }
              th, td {
                border: 1px solid #ddd;
                padding: 8px;
                text-align: left;
              }
              th {
                background-color: #ccc;
              }
              tr:nth-child(even) {
                background-color: #f5f5f5;
              }
            </style>
          `;

      const imageWidth = 50;
      const imageHeight = 25;
      const leftMargin = 10;
      const topMargin = 10;
      doc.addImage(
        image,
        "JPEG",
        leftMargin,
        topMargin,
        imageWidth,
        imageHeight,
        "FAST"
      );

      const pageWidth = doc.internal.pageSize.getWidth();
      const pageHeight = doc.internal.pageSize.getHeight();

      if (
        summary.type === "buy_commercial" ||
        summary.type === "sale_commercial" ||
        summary.type === "both_commercial" ||
        summary.type === "referral_commercial"
      ) {
        doc.html(
          styles +
            '<h2 style="font-size: 4px; margin-top: 37px; margin-left: 15px;">Funding-Confirmation</h2>',
          {
            callback: async () => {
              if (summary) {
                const representName =
                  summary?.represent === "seller"
                    ? "Seller"
                    : summary?.represent === "buyer"
                    ? "Buyer"
                    : summary?.represent === "both"
                    ? "Buyer & Seller"
                    : summary?.represent === "referral"
                    ? "Referral"
                    : "";

                let contactTypeData = "";
                if (contactData.type === "seller") {
                  contactTypeData = "Seller";
                } else if (contactData.type === "buyer") {
                  contactTypeData = "Buyer";
                } else if (contactData.type === "buyersale") {
                  contactTypeData = "Buyer & Seller";
                } else if (contactData.type === "referral_sellerAgent") {
                  contactTypeData = "Buyer";
                }

                let contactnameData = "";
                if (contactData.type === "referral_sellerAgent") {
                  contactnameData = "";
                } else {
                  contactnameData = `${summary?.contact2[0]?.data.firstName} ${summary?.contact2[0]?.data.lastName}`;
                }

                const tableDatas = [
                  [
                    "Agent",
                    "Type",
                    representName,
                    "Sale Price",
                    contactTypeData,
                  ],

                  [
                    `${summary?.contact3[0]?.data.firstName} ${summary?.contact3[0]?.data.lastName}`,
                    `${
                      summary?.category ? summary?.category : ""
                    } ${representName}`,
                    `${summary?.contact1[0]?.data.firstName} ${summary?.contact1[0]?.data.lastName}`,
                    getCommission.purchase_price,
                    contactnameData,
                  ],
                ];

                const tableStylesNew = {
                  theme: "grid",
                  tableWidth: "auto",
                  styles: { fontSize: 10 },
                  margin: { top: 50 },
                };

                doc.autoTable({
                  head: [],
                  body: tableDatas,
                  ...tableStylesNew,
                });
              }

              const tableStylesNew = {
                theme: "grid",
                tableWidth: "auto",
                styles: { fontSize: 10 },
                margin: { top: 50 },
              };

              const headingText = "Settlement Charges";

              const fontSize = 11;
              const leftMargin = 14;
              const topMargin = 71;

              doc.setFontSize(fontSize);

              doc.text(headingText, leftMargin, topMargin);

              const tableHeaders = ["Charged To", "For Service", "Amount"];

              let contactName = "";
              let commissionType = "";

              if (summary?.represent === "buyer") {
                contactName =
                  `Seller - ${summary?.contact2[0]?.data.firstName} ${summary?.contact2[0]?.data.lastName}` ||
                  "";
                commissionType = "Buyer Commission";
              } else if (summary?.represent === "seller") {
                contactName =
                  `Seller - ${summary?.contact1[0]?.data.firstName} ${summary?.contact1[0]?.data.lastName}` ||
                  "";
                commissionType = "Seller Commission";
              } else if (summary?.represent === "both") {
                contactName =
                  `Seller - ${summary?.contact1[0]?.data.firstName} ${summary?.contact1[0]?.data.lastName}` ||
                  "";
                commissionType = "Buyer & Seller Commission";
              } else if (summary?.represent === "referral") {
                contactName =
                  `Seller - ${summary?.contact1[0]?.data.firstName} ${summary?.contact1[0]?.data.lastName}` ||
                  "";
                commissionType = "Referral Commission";
              }

              const commissionRow = [
                contactName,
                commissionType,
                `$${submitData?.commission || 0}`,
              ];
              if (summary.phase === "canceled") {
                const totalCharges = parseFloat(submitData?.commission);

                const totalChargesRow = [
                  "Total Charges",
                  "",

                  `$${totalCharges.toFixed(2)}`,
                ];

                const tableDataCommission = [
                  tableHeaders,
                  commissionRow,
                  totalChargesRow,
                ];
                doc.autoTable({ body: tableDataCommission, tableStylesNew });

                const tablePayoutHeader = ["Pay To", "For Service", "Amount"];
                const estimateRow = [
                  `${summary?.contact3[0]?.data.firstName} ${summary?.contact3[0]?.data.lastName}` ||
                    "",
                  summary?.category || "Net Pay Due to Agent (Estimate)",
                  `$${Number(submitData?.estimate || 0).toFixed(2)}`, // Assuming estimate is a numeric value
                ];
                const transactionRow = [
                  "In Depth Realty",
                  "Transaction Fee",
                  `$${submitData?.transactionFee || 0}`,
                ];

                const totalChargesPauout =
                  parseFloat(submitData?.estimate) +
                  parseFloat(submitData?.transactionFee);

                const totalChargesRowPayout = [
                  "Total Charges",
                  "",

                  `$${totalChargesPauout.toFixed(2)}`,
                ];

                const tableDataEstimate = [
                  tablePayoutHeader,
                  estimateRow,
                  transactionRow,
                  totalChargesRowPayout,
                ];
                doc.autoTable({ body: tableDataEstimate, tableStylesNew });
              } else {
                const incomeItemRows = submitData?.incomeItem.map((item) => [
                  item.chargedto,
                  item.type,
                  `$${item.amount}`,
                ]);

                const totalCharges =
                  parseFloat(submitData?.commission) +
                  submitData?.incomeItem.reduce(
                    (total, item) => total + parseFloat(item.amount),
                    0
                  );

                // Define the "Total Charges" row
                const totalChargesRow = [
                  "Total Charges",
                  "",

                  `$${totalCharges.toFixed(2)}`,
                ];
                const tableDataCommission = [
                  tableHeaders,
                  commissionRow,
                  ...incomeItemRows,
                  totalChargesRow,
                ];
                doc.autoTable({ body: tableDataCommission, tableStylesNew });

                const tablePayoutHeader = ["Pay To", "For Service", "Amount"];

                const estimateRow = [
                  `${summary?.contact3[0]?.data.firstName} ${summary?.contact3[0]?.data.lastName}` ||
                    "",
                  summary?.category || "Net Pay Due to Agent (Estimate)",
                  `$${Number(submitData?.estimate || 0).toFixed(2)}`, // Assuming estimate is a numeric value
                ];

                const transactionRow = [
                  "In Depth Realty",
                  "Transaction Fee",
                  `$${submitData?.transactionFee || 0}`,
                ];

                const transactionWireFee = [
                  "In Depth Realty",
                  "Wire Fee",
                  `$${submitData?.wireFee || 0}`,
                ];

                const pauoutItemRows = submitData?.payoutItem.map((item) => [
                  item.payto,
                  item.type,
                  `$${item.amount}`,
                ]);

                const totalChargesPauout =
                  parseFloat(submitData?.estimate) +
                  parseFloat(submitData?.transactionFee) +
                  parseFloat(submitData?.wireFee) +
                  submitData?.payoutItem.reduce(
                    (total, item) => total + parseFloat(item.amount),
                    0
                  );

                const totalChargesRowPayout = [
                  "Total Charges",
                  "",

                  `$${totalChargesPauout.toFixed(2)}`,
                ];

                const tableDataEstimate = [
                  tablePayoutHeader,
                  estimateRow,
                  transactionRow,
                  transactionWireFee,
                  ...pauoutItemRows,
                  totalChargesRowPayout,
                ];
                doc.autoTable({ body: tableDataEstimate, tableStylesNew });
              }

              const commentText = submitData?.comments || "";
              const splitTextIntoChunks = (text, chunkSize) => {
                const words = text.split(" ");
                const chunks = [];
                for (let i = 0; i < words.length; i += chunkSize) {
                  chunks.push(words.slice(i, i + chunkSize).join(" "));
                }
                return chunks;
              };
              const commentChunks = splitTextIntoChunks(commentText, 15);

              const commentsX = 10;
              let commentsY = 10 + 180;

              doc.setFontSize(10);
              doc.text("Notes:", commentsX, commentsY);
              commentsY += 10;

              commentChunks.forEach((chunk) => {
                doc.text(chunk, commentsX, commentsY);
                commentsY += 15;
              });

              const pdfBlob = doc.output("blob");
              const pdfFormData = new FormData();
              pdfFormData.append("file", pdfBlob, "generated.pdf");

              const apiUrl = "https://api.brokeragentbase.com/upload";
              const config = {
                headers: {
                  "Content-Type": "multipart/form-data",
                },
              };

              const response = await axios.post(apiUrl, pdfFormData, config);
              setPdfPath(response.data);

              const userData = {
                confirmation_status: "confirmed",
                confirm_invoice_url: response.data,
              };

              if (fundingGet?._id) {
                setLoader({ isActive: true });
                await user_service
                  .fundingRequestUpdate(fundingGet._id, userData)
                  .then((response) => {
                    if (response) {
                      setSubmitData(response.data);
                      setLoader({ isActive: false });
                      setToaster({
                        type: "success",
                        isShow: true,
                         message: "Funding Updated Successfully",
                      });
                      setTimeout(() => {
                        setToaster((prevToaster) => ({
                          ...prevToaster,
                          isShow: false,
                        }));
                        navigate(`/transaction-review/${params.id}`);
                      }, 2000);
                    } else {
                      setLoader({ isActive: false });
                      setToaster({
                        types: "error",
                        isShow: true,
                        toasterBody: response.data.message,
                        message: "Error",
                      });
                    }
                  });
              }
            },
          }
        );
      } else {
        if (summary?.represent === "both") {
          if (getCommission.property_interest === "yes") {
            doc.html(
              styles +
                '<h2 style="font-size: 4px; margin-top: 37px; margin-left: 15px;">Funding-Confirmation</h2>',
              {
                callback: async () => {
                  if (summary) {
                    const representName =
                      summary?.represent === "seller"
                        ? "Seller"
                        : summary?.represent === "buyer"
                        ? "Buyer"
                        : summary?.represent === "both"
                        ? "Buyer & Seller"
                        : summary?.represent === "referral"
                        ? "Referral"
                        : "";

                    let contactTypeData = "";
                    if (contactData.type === "seller") {
                      contactTypeData = "Seller";
                    } else if (contactData.type === "buyer") {
                      contactTypeData = "Buyer";
                    } else if (contactData.type === "buyersale") {
                      contactTypeData = "Buyer & Seller";
                    } else if (contactData.type === "referral_sellerAgent") {
                      contactTypeData = "Buyer";
                    }

                    let contactnameData = "";
                    if (contactData.type === "referral_sellerAgent") {
                      contactnameData = "";
                    } else {
                      contactnameData = `${summary?.contact2[0]?.data.firstName} ${summary?.contact2[0]?.data.lastName}`;
                    }

                    const tableDatas = [
                      [
                        "Agent",
                        "Type",
                        representName,
                        "Sale Price",
                        contactTypeData,
                      ],

                      [
                        `${summary?.contact3[0]?.data.firstName} ${summary?.contact3[0]?.data.lastName}`,
                        `${
                          summary?.category ? summary?.category : ""
                        } ${representName}`,
                        `${summary?.contact1[0]?.data.firstName} ${summary?.contact1[0]?.data.lastName}`,
                        getCommission.purchase_price,
                        contactnameData,
                      ],
                    ];

                    const tableStylesNew = {
                      theme: "grid",
                      tableWidth: "auto",
                      styles: { fontSize: 10 },
                      margin: { top: 50 },
                    };

                    doc.autoTable({
                      head: [],
                      body: tableDatas,
                      ...tableStylesNew,
                    });
                  }

                  const tableStylesNew = {
                    theme: "grid",
                    tableWidth: "auto",
                    styles: { fontSize: 10 },
                    margin: { top: 50 },
                  };

                  const headingText = "Settlement Charges";

                  const fontSize = 11;
                  const leftMargin = 14;
                  const topMargin = 71;

                  doc.setFontSize(fontSize);

                  doc.text(headingText, leftMargin, topMargin);

                  const tableHeaders = ["Charged To", "For Service", "Amount"];

                  let contactName = "";
                  let commissionType = "";

                  if (summary?.represent === "buyer") {
                    contactName =
                      `Seller - ${summary?.contact2[0]?.data.firstName} ${summary?.contact2[0]?.data.lastName}` ||
                      "";
                    commissionType = "Buyer Commission";
                  } else if (summary?.represent === "seller") {
                    contactName =
                      `Seller - ${summary?.contact1[0]?.data.firstName} ${summary?.contact1[0]?.data.lastName}` ||
                      "";
                    commissionType = "Seller Commission";
                  } else if (summary?.represent === "both") {
                    contactName =
                      `Seller - ${summary?.contact1[0]?.data.firstName} ${summary?.contact1[0]?.data.lastName}` ||
                      "";
                    commissionType = "Buyer & Seller Commission";
                  } else if (summary?.represent === "referral") {
                    contactName =
                      `Seller - ${summary?.contact1[0]?.data.firstName} ${summary?.contact1[0]?.data.lastName}` ||
                      "";
                    commissionType = "Referral Commission";
                  }

                  const commissionRow = [
                    contactName,
                    commissionType,
                    `$${submitData?.commission || 0}`,
                  ];
                  if (summary.phase === "canceled") {
                    const totalCharges = parseFloat(submitData?.commission);

                    const totalChargesRow = [
                      "Total Charges",
                      "",

                      `$${totalCharges.toFixed(2)}`,
                    ];
                    const tableDataCommission = [
                      tableHeaders,
                      commissionRow,
                      totalChargesRow,
                    ];
                    doc.autoTable({
                      body: tableDataCommission,
                      tableStylesNew,
                    });

                    const tablePayoutHeader = [
                      "Pay To",
                      "For Service",
                      "Amount",
                    ];
                    const estimateRow = [
                      `${summary?.contact3[0]?.data.firstName} ${summary?.contact3[0]?.data.lastName}` ||
                        "",
                      summary?.category || "Net Pay Due to Agent (Estimate)",
                      `$${Number(submitData?.estimate || 0).toFixed(2)}`, // Assuming estimate is a numeric value
                    ];
                    const transactionRow = [
                      "In Depth Realty",
                      "Transaction Fee",
                      `$${submitData?.transactionFee || 0}`,
                    ];

                    const totalChargesPauout =
                      parseFloat(submitData?.estimate) +
                      parseFloat(submitData?.transactionFee);

                    const totalChargesRowPayout = [
                      "Total Charges",
                      "",

                      `$${totalChargesPauout.toFixed(2)}`,
                    ];

                    const tableDataEstimate = [
                      tablePayoutHeader,
                      estimateRow,
                      transactionRow,
                      totalChargesRowPayout,
                    ];
                    doc.autoTable({ body: tableDataEstimate, tableStylesNew });
                  } else {
                    const incomeItemRows = submitData?.incomeItem.map(
                      (item) => [item.chargedto, item.type, `$${item.amount}`]
                    );

                    const totalCharges =
                      parseFloat(submitData?.commission) +
                      submitData?.incomeItem.reduce(
                        (total, item) => total + parseFloat(item.amount),
                        0
                      );

                    const totalChargesRow = [
                      "Total Charges",
                      "",

                      `$${totalCharges.toFixed(2)}`,
                    ];
                    const tableDataCommission = [
                      tableHeaders,
                      commissionRow,
                      ...incomeItemRows,
                      totalChargesRow,
                    ];
                    doc.autoTable({
                      body: tableDataCommission,
                      tableStylesNew,
                    });

                    const tablePayoutHeader = [
                      "Pay To",
                      "For Service",
                      "Amount",
                    ];

                    const estimateRow = [
                      `${summary?.contact3[0]?.data.firstName} ${summary?.contact3[0]?.data.lastName}` ||
                        "",
                      summary?.category || "Net Pay Due to Agent (Estimate)",
                      `$${Number(submitData?.estimate || 0).toFixed(2)}`, // Assuming estimate is a numeric value
                    ];

                    const transactionRow = [
                      "In Depth Realty",
                      "Transaction Fee",
                      `$${submitData?.transactionFee || 0}`,
                    ];

                    const transactionWireFee = [
                      "In Depth Realty",
                      "Wire Fee",
                      `$${submitData?.wireFee || 0}`,
                    ];

                    const transactionHighvalueFee = [
                      "In Depth Realty",
                      "High Value Transaction Fee",
                      `$${submitData?.highValueFee || 0}`,
                    ];

                    const transactionPropertyvalueFee = [
                      "In Depth Realty",
                      "Personal Real Estate Risk Management Fee",
                      `$${submitData?.transactionPersonal || 0}`,
                    ];

                    const pauoutItemRows = submitData?.payoutItem.map(
                      (item) => [item.payto, item.type, `$${item.amount}`]
                    );

                    const totalChargesPauout =
                      parseFloat(submitData?.estimate) +
                      parseFloat(submitData?.transactionFee) +
                      parseFloat(submitData?.wireFee) +
                      parseFloat(submitData?.highValueFee) +
                      parseFloat(submitData?.transactionPersonal) +
                      submitData?.payoutItem.reduce(
                        (total, item) => total + parseFloat(item.amount),
                        0
                      );

                    const totalChargesRowPayout = [
                      "Total Charges",
                      "",

                      `$${totalChargesPauout.toFixed(2)}`,
                    ];

                    const tableDataEstimate = [
                      tablePayoutHeader,
                      estimateRow,
                      transactionRow,
                      transactionWireFee,
                      transactionPropertyvalueFee,
                      transactionHighvalueFee,
                      ...pauoutItemRows,
                      totalChargesRowPayout,
                    ];
                    doc.autoTable({ body: tableDataEstimate, tableStylesNew });
                  }

                  const commentText = submitData?.comments || "";
                  const splitTextIntoChunks = (text, chunkSize) => {
                    const words = text.split(" ");
                    const chunks = [];
                    for (let i = 0; i < words.length; i += chunkSize) {
                      chunks.push(words.slice(i, i + chunkSize).join(" "));
                    }
                    return chunks;
                  };
                  const commentChunks = splitTextIntoChunks(commentText, 15);

                  const commentsX = 10;
                  let commentsY = 10 + 180;

                  doc.setFontSize(10);
                  doc.text("Notes:", commentsX, commentsY);
                  commentsY += 10;

                  commentChunks.forEach((chunk) => {
                    doc.text(chunk, commentsX, commentsY);
                    commentsY += 15; // Move to the next line
                  });

                  const pdfBlob = doc.output("blob");
                  const pdfFormData = new FormData();
                  pdfFormData.append("file", pdfBlob, "generated.pdf");

                  const apiUrl = "https://api.brokeragentbase.com/upload";
                  const config = {
                    headers: {
                      "Content-Type": "multipart/form-data",
                    },
                  };

                  const response = await axios.post(
                    apiUrl,
                    pdfFormData,
                    config
                  );
                  setPdfPath(response.data);

                  const userData = {
                    confirmation_status: "confirmed",
                    confirm_invoice_url: response.data,
                  };

                  if (fundingGet?._id) {
                    setLoader({ isActive: true });
                    await user_service
                      .fundingRequestUpdate(fundingGet._id, userData)
                      .then((response) => {
                        if (response) {
                          setSubmitData(response.data);
                          setLoader({ isActive: false });
                          setToaster({
                            type: "success",
                            isShow: true,
                             message: "Funding Updated Successfully",
                          });
                          setTimeout(() => {
                            setToaster((prevToaster) => ({
                              ...prevToaster,
                              isShow: false,
                            }));
                            navigate(`/transaction-review/${params.id}`);
                          }, 2000);
                        } else {
                          setLoader({ isActive: false });
                          setToaster({
                            types: "error",
                            isShow: true,
                            toasterBody: response.data.message,
                            message: "Error",
                          });
                        }
                      });
                  }
                },
              }
            );
          } else {
            doc.html(
              styles +
                '<h2 style="font-size: 4px; margin-top: 37px; margin-left: 15px;">Funding-Confirmation</h2>',
              {
                callback: async () => {
                  if (summary) {
                    const representName =
                      summary?.represent === "both" ? "Buyer & Seller" : "";

                    let contactTypeData = "";
                    if (contactData.type === "seller") {
                      contactTypeData = "Seller";
                    } else if (contactData.type === "buyer") {
                      contactTypeData = "Buyer";
                    } else if (contactData.type === "buyersale") {
                      contactTypeData = "Buyer & Seller";
                    } else if (contactData.type === "referral_sellerAgent") {
                      contactTypeData = "Referral";
                    }

                    const tableDatas = [
                      [
                        "Agent",
                        "Type",
                        representName,
                        "Sale Price",
                        contactTypeData,
                      ],
                      [
                        `${summary?.contact3[0]?.data.firstName} ${summary?.contact3[0]?.data.lastName}`,
                        `${
                          summary?.category ? summary?.category : ""
                        } ${representName}`,
                        `${summary?.contact1[0]?.data.firstName} ${summary?.contact1[0]?.data.lastName}`,
                        getCommission.purchase_price,
                        `${summary?.contact2[0]?.data.firstName} ${summary?.contact2[0]?.data.lastName}`,
                      ],
                    ];

                    const tableStylesNew = {
                      theme: "grid",
                      tableWidth: "auto",
                      styles: { fontSize: 10 },
                      margin: { top: 50 },
                    };

                    doc.autoTable({
                      head: [],
                      body: tableDatas,
                      ...tableStylesNew,
                    });
                  }

                  const tableStylesNew = {
                    theme: "grid",
                    tableWidth: "auto",
                    styles: { fontSize: 10 },
                    margin: { top: 50 },
                  };

                  const headingText = "Settlement Charges";

                  const fontSize = 11;
                  const leftMargin = 14;
                  const topMargin = 71;

                  doc.setFontSize(fontSize);

                  doc.text(headingText, leftMargin, topMargin);

                  const tableHeaders = ["Charged To", "For Service", "Amount"];

                  let commissionType = "";
                  if (summary?.represent === "buyer") {
                    commissionType = "Buyer Commission";
                  } else if (summary?.represent === "seller") {
                    commissionType = "Seller Commission";
                  } else if (summary?.represent === "both") {
                    commissionType = "Buyer & Seller Commission";
                  } else if (summary?.represent === "referral") {
                    commissionType = "Referral Commission";
                  }

                  const commissionRow = [
                    `Seller - ${summary?.contact1[0]?.data.firstName} ${summary?.contact1[0]?.data.lastName}` ||
                      "",
                    commissionType,
                    `$${submitData?.commission || 0}`,
                  ];

                  if (summary.phase === "canceled") {
                    const totalCharges = parseFloat(submitData?.commission);

                    const totalChargesRow = [
                      "Total Charges",
                      "",

                      `$${totalCharges.toFixed(2)}`,
                    ];
                    const tableDataCommission = [
                      tableHeaders,
                      commissionRow,
                      totalChargesRow,
                    ];
                    doc.autoTable({
                      body: tableDataCommission,
                      tableStylesNew,
                    });

                    const tablePayoutHeader = [
                      "Pay To",
                      "For Service",
                      "Amount",
                    ];
                    const estimateRow = [
                      `${summary?.contact3[0]?.data.firstName} ${summary?.contact3[0]?.data.lastName}` ||
                        "",
                      summary?.category || "Net Pay Due to Agent (Estimate)",
                      `$${Number(submitData?.estimate || 0).toFixed(2)}`, // Assuming estimate is a numeric value
                    ];
                    const transactionRow = [
                      "In Depth Realty",
                      "Transaction Fee",
                      `$${submitData?.transactionFee || 0}`,
                    ];

                    const totalChargesPauout =
                      parseFloat(submitData?.estimate) +
                      parseFloat(submitData?.transactionFee);

                    const totalChargesRowPayout = [
                      "Total Charges",
                      "",

                      `$${totalChargesPauout.toFixed(2)}`,
                    ];

                    const tableDataEstimate = [
                      tablePayoutHeader,
                      estimateRow,
                      transactionRow,
                      totalChargesRowPayout,
                    ];
                    doc.autoTable({ body: tableDataEstimate, tableStylesNew });
                  } else {
                    const incomeItemRows = submitData?.incomeItem.map(
                      (item) => [item.chargedto, item.type, `$${item.amount}`]
                    );

                    const totalCharges =
                      parseFloat(submitData?.commission) +
                      submitData?.incomeItem.reduce(
                        (total, item) => total + parseFloat(item.amount),
                        0
                      );

                    // Define the "Total Charges" row
                    const totalChargesRow = [
                      "Total Charges",
                      "",

                      `$${totalCharges.toFixed(2)}`,
                    ];
                    const tableDataCommission = [
                      tableHeaders,
                      commissionRow,
                      ...incomeItemRows,
                      totalChargesRow,
                    ];
                    doc.autoTable({
                      body: tableDataCommission,
                      tableStylesNew,
                    });

                    const tablePayoutHeader = [
                      "Pay To",
                      "For Service",
                      "Amount",
                    ];

                    const estimateRow = [
                      `${summary?.contact3[0]?.data.firstName} ${summary?.contact3[0]?.data.lastName}` ||
                        "",
                      summary?.category || "Net Pay Due to Agent (Estimate)",
                      `$${Number(submitData?.estimate || 0).toFixed(2)}`,
                    ];

                    const transactionRowBuyer = [
                      "In Depth Realty",
                      "Buyer Transaction fee",
                      `$${submitData?.buyerFee || 0}`,
                    ];

                    const transactionRowSeller = [
                      "In Depth Realty",
                      "Seller Transaction fee",
                      `$${submitData?.sellerFee || 0}`,
                    ];

                    const transactionRowRisk = [
                      "In Depth Realty",
                      "Dual Agency Risk Management Fee",
                      `$${submitData?.managementFee || 0}`,
                    ];

                    const transactionWireFee = [
                      "In Depth Realty",
                      "Wire fee:",
                      `$${submitData?.wireFee || 0}`,
                    ];

                    const pauoutItemRows = submitData?.payoutItem.map(
                      (item) => [item.payto, item.type, `$${item.amount}`]
                    );

                    const totalChargesPauout =
                      parseFloat(submitData?.estimate) +
                      parseFloat(submitData?.buyerFee) +
                      parseFloat(submitData?.sellerFee) +
                      parseFloat(submitData?.managementFee) +
                      parseFloat(submitData?.wireFee) +
                      submitData?.payoutItem.reduce(
                        (total, item) => total + parseFloat(item.amount),
                        0
                      );

                    const totalChargesRowPayout = [
                      "Total Charges",
                      "",

                      `$${totalChargesPauout.toFixed(2)}`,
                    ];

                    const tableDataEstimate = [
                      tablePayoutHeader,
                      estimateRow,
                      transactionRowBuyer,
                      transactionRowSeller,
                      transactionRowRisk,
                      transactionWireFee,
                      ...pauoutItemRows,
                      totalChargesRowPayout,
                    ];
                    doc.autoTable({ body: tableDataEstimate, tableStylesNew });
                  }

                  const commentText = submitData?.comments || "";
                  const splitTextIntoChunks = (text, chunkSize) => {
                    const words = text.split(" ");
                    const chunks = [];
                    for (let i = 0; i < words.length; i += chunkSize) {
                      chunks.push(words.slice(i, i + chunkSize).join(" "));
                    }
                    return chunks;
                  };
                  const commentChunks = splitTextIntoChunks(commentText, 15);

                  const commentsX = 10;
                  let commentsY = 10 + 180;

                  doc.setFontSize(10);
                  doc.text("Notes:", commentsX, commentsY);
                  commentsY += 10;

                  commentChunks.forEach((chunk) => {
                    doc.text(chunk, commentsX, commentsY);
                    commentsY += 15;
                  });

                  const pdfBlob = doc.output("blob");
                  const pdfFormData = new FormData();
                  pdfFormData.append("file", pdfBlob, "generated.pdf");

                  const apiUrl = "https://api.brokeragentbase.com/upload";
                  const config = {
                    headers: {
                      "Content-Type": "multipart/form-data",
                    },
                  };

                  const response = await axios.post(
                    apiUrl,
                    pdfFormData,
                    config
                  );
                  setPdfPath(response.data);

                  const userData = {
                    confirmation_status: "confirmed",
                    confirm_invoice_url: response.data,
                  };

                  if (fundingGet?._id) {
                    setLoader({ isActive: true });
                    await user_service
                      .fundingRequestUpdate(fundingGet._id, userData)
                      .then((response) => {
                        if (response) {
                          setSubmitData(response.data);
                          setLoader({ isActive: false });
                          setToaster({
                            type: "success",
                            isShow: true,
                             message: "Funding Updated Successfully",
                          });
                          setTimeout(() => {
                            setToaster((prevToaster) => ({
                              ...prevToaster,
                              isShow: false,
                            }));
                            navigate(`/transaction-review/${params.id}`);
                          }, 2000);
                        } else {
                          setLoader({ isActive: false });
                          setToaster({
                            types: "error",
                            isShow: true,
                            toasterBody: response.data.message,
                            message: "Error",
                          });
                        }
                      });
                  }
                },
              }
            );
          }
        } else {
          if (getCommission.property_interest === "yes") {
            doc.html(
              styles +
                '<h2 style="font-size: 4px; margin-top: 37px; margin-left: 15px;">Funding-Confirmation</h2>',
              {
                callback: async () => {
                  if (summary) {
                    const representName =
                      summary?.represent === "seller"
                        ? "Seller"
                        : summary?.represent === "buyer"
                        ? "Buyer"
                        : summary?.represent === "both"
                        ? "Buyer & Seller"
                        : summary?.represent === "referral"
                        ? "Referral"
                        : "";

                    let contactTypeData = "";
                    if (contactData.type === "seller") {
                      contactTypeData = "Seller";
                    } else if (contactData.type === "buyer") {
                      contactTypeData = "Buyer";
                    } else if (contactData.type === "buyersale") {
                      contactTypeData = "Buyer & Seller";
                    } else if (contactData.type === "referral_sellerAgent") {
                      contactTypeData = "Buyer";
                    }

                    let contactnameData = "";
                    if (contactData.type === "referral_sellerAgent") {
                      contactnameData = "";
                    } else {
                      contactnameData = `${summary?.contact2[0]?.data.firstName} ${summary?.contact2[0]?.data.lastName}`;
                    }

                    const tableDatas = [
                      [
                        "Agent",
                        "Type",
                        representName,
                        "Sale Price",
                        contactTypeData,
                      ],

                      [
                        `${summary?.contact3[0]?.data.firstName} ${summary?.contact3[0]?.data.lastName}`,
                        `${
                          summary?.category ? summary?.category : ""
                        } ${representName}`,
                        `${summary?.contact1[0]?.data.firstName} ${summary?.contact1[0]?.data.lastName}`,
                        getCommission.purchase_price,
                        contactnameData,
                      ],
                    ];

                    const tableStylesNew = {
                      theme: "grid",
                      tableWidth: "auto",
                      styles: { fontSize: 10 },
                      margin: { top: 50 },
                    };

                    doc.autoTable({
                      head: [],
                      body: tableDatas,
                      ...tableStylesNew,
                    });
                  }

                  const tableStylesNew = {
                    theme: "grid",
                    tableWidth: "auto",
                    styles: { fontSize: 10 },
                    margin: { top: 50 },
                  };

                  const headingText = "Settlement Charges";

                  const fontSize = 11;
                  const leftMargin = 14;
                  const topMargin = 71;

                  doc.setFontSize(fontSize);

                  doc.text(headingText, leftMargin, topMargin);

                  const tableHeaders = ["Charged To", "For Service", "Amount"];

                  let contactName = "";
                  let commissionType = "";

                  if (summary?.represent === "buyer") {
                    contactName =
                      `Seller - ${summary?.contact2[0]?.data.firstName} ${summary?.contact2[0]?.data.lastName}` ||
                      "";
                    commissionType = "Buyer Commission";
                  } else if (summary?.represent === "seller") {
                    contactName =
                      `Seller - ${summary?.contact1[0]?.data.firstName} ${summary?.contact1[0]?.data.lastName}` ||
                      "";
                    commissionType = "Seller Commission";
                  } else if (summary?.represent === "both") {
                    contactName =
                      `Seller - ${summary?.contact1[0]?.data.firstName} ${summary?.contact1[0]?.data.lastName}` ||
                      "";
                    commissionType = "Buyer & Seller Commission";
                  } else if (summary?.represent === "referral") {
                    contactName =
                      `Seller - ${summary?.contact1[0]?.data.firstName} ${summary?.contact1[0]?.data.lastName}` ||
                      "";
                    commissionType = "Referral Commission";
                  }

                  const commissionRow = [
                    contactName,
                    commissionType,
                    `$${submitData?.commission || 0}`,
                  ];
                  if (summary.phase === "canceled") {
                    const totalCharges = parseFloat(submitData?.commission);

                    const totalChargesRow = [
                      "Total Charges",
                      "",

                      `$${totalCharges.toFixed(2)}`,
                    ];
                    const tableDataCommission = [
                      tableHeaders,
                      commissionRow,
                      totalChargesRow,
                    ];
                    doc.autoTable({
                      body: tableDataCommission,
                      tableStylesNew,
                    });

                    const tablePayoutHeader = [
                      "Pay To",
                      "For Service",
                      "Amount",
                    ];
                    const estimateRow = [
                      `${summary?.contact3[0]?.data.firstName} ${summary?.contact3[0]?.data.lastName}` ||
                        "",
                      summary?.category || "Net Pay Due to Agent (Estimate)",
                      `$${Number(submitData?.estimate || 0).toFixed(2)}`, // Assuming estimate is a numeric value
                    ];
                    const transactionRow = [
                      "In Depth Realty",
                      "Transaction Fee",
                      `$${submitData?.transactionFee || 0}`,
                    ];

                    const totalChargesPauout =
                      parseFloat(submitData?.estimate) +
                      parseFloat(submitData?.transactionFee);

                    const totalChargesRowPayout = [
                      "Total Charges",
                      "",

                      `$${totalChargesPauout.toFixed(2)}`,
                    ];

                    const tableDataEstimate = [
                      tablePayoutHeader,
                      estimateRow,
                      transactionRow,
                      totalChargesRowPayout,
                    ];
                    doc.autoTable({ body: tableDataEstimate, tableStylesNew });
                  } else {
                    const incomeItemRows = submitData?.incomeItem.map(
                      (item) => [item.chargedto, item.type, `$${item.amount}`]
                    );

                    const totalCharges =
                      parseFloat(submitData?.commission) +
                      submitData?.incomeItem.reduce(
                        (total, item) => total + parseFloat(item.amount),
                        0
                      );

                    // Define the "Total Charges" row
                    const totalChargesRow = [
                      "Total Charges",
                      "",

                      `$${totalCharges.toFixed(2)}`,
                    ];
                    const tableDataCommission = [
                      tableHeaders,
                      commissionRow,
                      ...incomeItemRows,
                      totalChargesRow,
                    ];
                    doc.autoTable({
                      body: tableDataCommission,
                      tableStylesNew,
                    });

                    const tablePayoutHeader = [
                      "Pay To",
                      "For Service",
                      "Amount",
                    ];

                    const estimateRow = [
                      `${summary?.contact3[0]?.data.firstName} ${summary?.contact3[0]?.data.lastName}` ||
                        "",
                      summary?.category || "Net Pay Due to Agent (Estimate)",
                      `$${Number(submitData?.estimate || 0).toFixed(2)}`, // Assuming estimate is a numeric value
                    ];

                    const transactionRow = [
                      "In Depth Realty",
                      "Transaction Fee",
                      `$${submitData?.transactionFee || 0}`,
                    ];

                    const transactionWireFee = [
                      "In Depth Realty",
                      "Wire Fee",
                      `$${submitData?.wireFee || 0}`,
                    ];

                    const transactionHighvalueFee = [
                      "In Depth Realty",
                      "High Value Transaction Fee",
                      `$${submitData?.highValueFee || 0}`,
                    ];

                    const transactionPropertyvalueFee = [
                      "In Depth Realty",
                      "Personal Real Estate Risk Management Fee",
                      `$${submitData?.transactionPersonal || 0}`,
                    ];

                    const pauoutItemRows = submitData?.payoutItem.map(
                      (item) => [item.payto, item.type, `$${item.amount}`]
                    );

                    const totalChargesPauout =
                      parseFloat(submitData?.estimate) +
                      parseFloat(submitData?.transactionFee) +
                      parseFloat(submitData?.wireFee) +
                      parseFloat(submitData?.highValueFee) +
                      parseFloat(submitData?.transactionPersonal) +
                      submitData?.payoutItem.reduce(
                        (total, item) => total + parseFloat(item.amount),
                        0
                      );

                    const totalChargesRowPayout = [
                      "Total Charges",
                      "",

                      `$${totalChargesPauout.toFixed(2)}`,
                    ];

                    const tableDataEstimate = [
                      tablePayoutHeader,
                      estimateRow,
                      transactionRow,
                      transactionWireFee,
                      transactionPropertyvalueFee,
                      transactionHighvalueFee,
                      ...pauoutItemRows,
                      totalChargesRowPayout,
                    ];
                    doc.autoTable({ body: tableDataEstimate, tableStylesNew });
                  }

                  const commentText = submitData?.comments || "";
                  const splitTextIntoChunks = (text, chunkSize) => {
                    const words = text.split(" ");
                    const chunks = [];
                    for (let i = 0; i < words.length; i += chunkSize) {
                      chunks.push(words.slice(i, i + chunkSize).join(" "));
                    }
                    return chunks;
                  };
                  const commentChunks = splitTextIntoChunks(commentText, 15);

                  const commentsX = 10;
                  let commentsY = 10 + 180;

                  doc.setFontSize(10);
                  doc.text("Notes:", commentsX, commentsY);
                  commentsY += 10;

                  commentChunks.forEach((chunk) => {
                    doc.text(chunk, commentsX, commentsY);
                    commentsY += 15; // Move to the next line
                  });

                  const pdfBlob = doc.output("blob");
                  const pdfFormData = new FormData();
                  pdfFormData.append("file", pdfBlob, "generated.pdf");

                  const apiUrl = "https://api.brokeragentbase.com/upload";
                  const config = {
                    headers: {
                      "Content-Type": "multipart/form-data",
                    },
                  };

                  const response = await axios.post(
                    apiUrl,
                    pdfFormData,
                    config
                  );
                  setPdfPath(response.data);

                  const userData = {
                    confirmation_status: "confirmed",
                    confirm_invoice_url: response.data,
                  };

                  if (fundingGet?._id) {
                    setLoader({ isActive: true });
                    await user_service
                      .fundingRequestUpdate(fundingGet._id, userData)
                      .then((response) => {
                        if (response) {
                          setSubmitData(response.data);
                          setLoader({ isActive: false });
                          setToaster({
                            type: "success",
                            isShow: true,
                             message: "Funding Updated Successfully",
                          });
                          setTimeout(() => {
                            setToaster((prevToaster) => ({
                              ...prevToaster,
                              isShow: false,
                            }));
                            navigate(`/transaction-review/${params.id}`);
                          }, 2000);
                        } else {
                          setLoader({ isActive: false });
                          setToaster({
                            types: "error",
                            isShow: true,
                            toasterBody: response.data.message,
                            message: "Error",
                          });
                        }
                      });
                  }
                },
              }
            );
          } else {
            doc.html(
              styles +
                '<h2 style="font-size: 4px; margin-top: 37px; margin-left: 15px;">Funding-Confirmation</h2>',
              {
                callback: async () => {
                  if (summary) {
                    const representName =
                      summary?.represent === "seller"
                        ? "Seller"
                        : summary?.represent === "buyer"
                        ? "Buyer"
                        : summary?.represent === "both"
                        ? "Buyer & Seller"
                        : summary?.represent === "referral"
                        ? "Referral"
                        : "";

                    let contactTypeData = "";
                    if (contactData.type === "seller") {
                      contactTypeData = "Seller";
                    } else if (contactData.type === "buyer") {
                      contactTypeData = "Buyer";
                    } else if (contactData.type === "buyersale") {
                      contactTypeData = "Buyer & Seller";
                    } else if (contactData.type === "referral_sellerAgent") {
                      contactTypeData = "Buyer";
                    }

                    let contactnameData = "";
                    if (contactData.type === "referral_sellerAgent") {
                      contactnameData = "";
                    } else {
                      contactnameData = `${summary?.contact2[0]?.data.firstName} ${summary?.contact2[0]?.data.lastName}`;
                    }

                    const tableDatas = [
                      [
                        "Agent",
                        "Type",
                        representName,
                        "Sale Price",
                        contactTypeData,
                      ],

                      [
                        `${summary?.contact3[0]?.data.firstName} ${summary?.contact3[0]?.data.lastName}`,
                        `${
                          summary?.category ? summary?.category : ""
                        } ${representName}`,
                        `${summary?.contact1[0]?.data.firstName} ${summary?.contact1[0]?.data.lastName}`,
                        getCommission.purchase_price,
                        contactnameData,
                      ],
                    ];

                    const tableStylesNew = {
                      theme: "grid",
                      tableWidth: "auto",
                      styles: { fontSize: 10 },
                      margin: { top: 50 },
                    };

                    doc.autoTable({
                      head: [],
                      body: tableDatas,
                      ...tableStylesNew,
                    });
                  }

                  const tableStylesNew = {
                    theme: "grid",
                    tableWidth: "auto",
                    styles: { fontSize: 10 },
                    margin: { top: 50 },
                  };

                  const headingText = "Settlement Charges";

                  const fontSize = 11;
                  const leftMargin = 14;
                  const topMargin = 71;

                  doc.setFontSize(fontSize);

                  doc.text(headingText, leftMargin, topMargin);

                  const tableHeaders = ["Charged To", "For Service", "Amount"];

                  let contactName = "";
                  let commissionType = "";

                  if (summary?.represent === "buyer") {
                    contactName =
                      `Seller - ${summary?.contact2[0]?.data.firstName} ${summary?.contact2[0]?.data.lastName}` ||
                      "";
                    commissionType = "Buyer Commission";
                  } else if (summary?.represent === "seller") {
                    contactName =
                      `Seller - ${summary?.contact1[0]?.data.firstName} ${summary?.contact1[0]?.data.lastName}` ||
                      "";
                    commissionType = "Seller Commission";
                  } else if (summary?.represent === "both") {
                    contactName =
                      `Seller - ${summary?.contact1[0]?.data.firstName} ${summary?.contact1[0]?.data.lastName}` ||
                      "";
                    commissionType = "Buyer & Seller Commission";
                  } else if (summary?.represent === "referral") {
                    contactName =
                      `Seller - ${summary?.contact1[0]?.data.firstName} ${summary?.contact1[0]?.data.lastName}` ||
                      "";
                    commissionType = "Referral Commission";
                  }

                  const commissionRow = [
                    contactName,
                    commissionType,
                    `$${submitData?.commission || 0}`,
                  ];

                  if (summary.phase === "canceled") {
                    const totalCharges = parseFloat(submitData?.commission);

                    const totalChargesRow = [
                      "Total Charges",
                      "",

                      `$${totalCharges.toFixed(2)}`,
                    ];
                    const tableDataCommission = [
                      tableHeaders,
                      commissionRow,
                      totalChargesRow,
                    ];
                    doc.autoTable({
                      body: tableDataCommission,
                      tableStylesNew,
                    });

                    const tablePayoutHeader = [
                      "Pay To",
                      "For Service",
                      "Amount",
                    ];
                    const estimateRow = [
                      `${summary?.contact3[0]?.data.firstName} ${summary?.contact3[0]?.data.lastName}` ||
                        "",
                      summary?.category || "Net Pay Due to Agent (Estimate)",
                      `$${Number(submitData?.estimate || 0).toFixed(2)}`, // Assuming estimate is a numeric value
                    ];
                    const transactionRow = [
                      "In Depth Realty",
                      "Transaction Fee",
                      `$${submitData?.transactionFee || 0}`,
                    ];

                    const totalChargesPauout =
                      parseFloat(submitData?.estimate) +
                      parseFloat(submitData?.transactionFee);

                    const totalChargesRowPayout = [
                      "Total Charges",
                      "",

                      `$${totalChargesPauout.toFixed(2)}`,
                    ];

                    const tableDataEstimate = [
                      tablePayoutHeader,
                      estimateRow,
                      transactionRow,
                      totalChargesRowPayout,
                    ];
                    doc.autoTable({ body: tableDataEstimate, tableStylesNew });
                  } else {
                    const incomeItemRows = submitData?.incomeItem.map(
                      (item) => [item.chargedto, item.type, `$${item.amount}`]
                    );

                    const totalCharges =
                      parseFloat(submitData?.commission) +
                      submitData?.incomeItem.reduce(
                        (total, item) => total + parseFloat(item.amount),
                        0
                      );

                    // Define the "Total Charges" row
                    const totalChargesRow = [
                      "Total Charges",
                      "",

                      `$${totalCharges.toFixed(2)}`,
                    ];
                    const tableDataCommission = [
                      tableHeaders,
                      commissionRow,
                      ...incomeItemRows,
                      totalChargesRow,
                    ];
                    doc.autoTable({
                      body: tableDataCommission,
                      tableStylesNew,
                    });

                    const tablePayoutHeader = [
                      "Pay To",
                      "For Service",
                      "Amount",
                    ];

                    const estimateRow = [
                      `${summary?.contact3[0]?.data.firstName} ${summary?.contact3[0]?.data.lastName}` ||
                        "",
                      summary?.category || "Net Pay Due to Agent (Estimate)",
                      `$${Number(submitData?.estimate || 0).toFixed(2)}`, // Assuming estimate is a numeric value
                    ];

                    const transactionRow = [
                      "In Depth Realty",
                      "Transaction Fee",
                      `$${submitData?.transactionFee || 0}`,
                    ];

                    const transactionWireFee = [
                      "In Depth Realty",
                      "Wire Fee",
                      `$${submitData?.wireFee || 0}`,
                    ];

                    const transactionHighvalueFee = [
                      "In Depth Realty",
                      "High Value Transaction Fee",
                      `$${submitData?.highValueFee || 0}`,
                    ];

                    const pauoutItemRows = submitData?.payoutItem.map(
                      (item) => [item.payto, item.type, `$${item.amount}`]
                    );

                    const totalChargesPauout =
                      parseFloat(submitData?.estimate) +
                      parseFloat(submitData?.transactionFee) +
                      parseFloat(submitData?.wireFee) +
                      parseFloat(submitData?.highValueFee) +
                      submitData?.payoutItem.reduce(
                        (total, item) => total + parseFloat(item.amount),
                        0
                      );

                    const totalChargesRowPayout = [
                      "Total Charges",
                      "",

                      `$${totalChargesPauout.toFixed(2)}`,
                    ];

                    const tableDataEstimate = [
                      tablePayoutHeader,
                      estimateRow,
                      transactionRow,
                      transactionWireFee,
                      transactionHighvalueFee,
                      ...pauoutItemRows,
                      totalChargesRowPayout,
                    ];
                    doc.autoTable({ body: tableDataEstimate, tableStylesNew });
                  }

                  const commentText = submitData?.comments || "";
                  const splitTextIntoChunks = (text, chunkSize) => {
                    const words = text.split(" ");
                    const chunks = [];
                    for (let i = 0; i < words.length; i += chunkSize) {
                      chunks.push(words.slice(i, i + chunkSize).join(" "));
                    }
                    return chunks;
                  };
                  const commentChunks = splitTextIntoChunks(commentText, 15);

                  const commentsX = 10;
                  let commentsY = 10 + 180;

                  doc.setFontSize(10);
                  doc.text("Notes:", commentsX, commentsY);
                  commentsY += 10;

                  commentChunks.forEach((chunk) => {
                    doc.text(chunk, commentsX, commentsY);
                    commentsY += 15; // Move to the next line
                  });

                  const pdfBlob = doc.output("blob");
                  const pdfFormData = new FormData();
                  pdfFormData.append("file", pdfBlob, "generated.pdf");

                  const apiUrl = "https://api.brokeragentbase.com/upload";
                  const config = {
                    headers: {
                      "Content-Type": "multipart/form-data",
                    },
                  };

                  const response = await axios.post(
                    apiUrl,
                    pdfFormData,
                    config
                  );
                  setPdfPath(response.data);

                  const userData = {
                    confirmation_status: "confirmed",
                    confirm_invoice_url: response.data,
                  };

                  if (fundingGet?._id) {
                    setLoader({ isActive: true });
                    await user_service
                      .fundingRequestUpdate(fundingGet._id, userData)
                      .then((response) => {
                        if (response) {
                          setSubmitData(response.data);
                          setLoader({ isActive: false });
                          setToaster({
                            type: "success",
                            isShow: true,
                             message: "Funding Updated Successfully",
                          });
                          setTimeout(() => {
                            setToaster((prevToaster) => ({
                              ...prevToaster,
                              isShow: false,
                            }));
                            navigate(`/transaction-review/${params.id}`);
                          }, 2000);
                        } else {
                          setLoader({ isActive: false });
                          setToaster({
                            types: "error",
                            isShow: true,
                            toasterBody: response.data.message,
                            message: "Error",
                          });
                        }
                      });
                  }
                },
              }
            );
          }
        }
      }
    } catch (error) {
      console.error("Error generating PDF:", error);
    }
  };

  const timestamp = summary.propertyDetail?.timeStamp;
  const date = new Date(timestamp);
  const formattedDateTime = date.toLocaleString();

  return (
    <div className="bg-secondary float-left w-100 pt-4 mb-4">
      <Loader isActive={isActive} />
      {isShow && (
        <Toaster
          types={types}
          isShow={isShow}
          toasterBody={toasterBody}
          message={message}
        />
      )}
      <main className="page-wrapper">
        {/* <AllTab/> */}
        {/* <!-- Page content--> */}
        {/* <!-- Page container--> */}
        <div className="content-overlay transaction_tab_view mt-0">
        <div className="row">
          <AllTab />
          <div className="col-md-12">
            <div className="add_listing border rounded-3 p-3 bg-light">
              <div className="row">
                <div className="col-md-12">
                  <div className="d-flex align-items-center justify-content-between border-bottom pb-2 mb-4">
                    <h3 className="mb-0">Funding Confirmation</h3>
                  </div>
                </div>

                <div className="col-md-9">
                  <div className="add_listing rounded-3">
                    <p className="">
                      TENANT'S BROKER - Verify tenant's broker contact info.
                    </p>
                    <div className="row mt-4">
                      <div className="col-sm-4 mb-3">
                        <label className="col-form-label pt-0 mb-0">
                          Company Name
                        </label>
                        <input
                          className="form-control"
                          id="inline-form-input"
                          type="text"
                          name="companyName"
                          value={formValues.companyName}
                          onChange={handleChange}
                        />
                      </div>

                      <div className="col-sm-4 mb-3">
                        <label className="col-form-label pt-0 mb-0">
                          Phone
                        </label>
                        <input
                          className="form-control"
                          id="inline-form-input"
                          type="text"
                          name="phone"
                          value={formValues.phone}
                          onChange={handleChange}
                        />
                      </div>
                      <div className="col-sm-4 mb-3">
                        <label className="col-form-label pt-0 mb-0">
                          File Number (GF#)
                        </label>
                        <input
                          className="form-control"
                          id="inline-form-input"
                          type="text"
                          name="fileNumber"
                          value={formValues.fileNumber}
                          onChange={handleChange}
                        />
                        {/* <div className="invalid-tooltip">{formErrors.fileNumber}</div> */}
                      </div>
                    </div>

                    <div className="row mt-0">
                      <div className="col-sm-4 mb-3">
                        <label className="col-form-label pt-0 mb-0">
                          Contact Name
                        </label>
                        <input
                          className="form-control"
                          id="inline-form-input"
                          type="text"
                          name="contactName"
                          value={formValues.contactName}
                          onChange={handleChange}
                        />
                      </div>

                      <div className="col-sm-8">
                        <label className="col-form-label pt-0 mb-0">
                          Email
                        </label>
                        <input
                          className="form-control"
                          id="inline-form-input"
                          type="text"
                          name="email"
                          value={formValues.email}
                          onChange={handleChange}
                        />
                      </div>
                    </div>

                    <div className="row mt-3">
                      <div className="col-md-12">
                        <p>
                          SETTLEMENT CHARGES - Itemize commissions and fees due
                          for your side of this transaction.
                        </p>
                      </div>
                      <div className="col-md-4">
                        <label className="col-form-label pt-0 mb-0">
                          Income Item
                        </label>
                      </div>
                      <div className="col-md-4">
                        <label className="col-form-label pt-0 mb-0">
                          Amount
                        </label>
                      </div>
                      <div className="col-md-4">
                        <label className="col-form-label pt-0 mb-0">
                          Charged To
                        </label>
                      </div>
                    </div>

                    {summary.phase === "canceled" ? (
                      <div className="row mt-0">
                        <div className="col-md-4 d-flex align-items-center justify-content-start">
                          <p className="mb-0">Agent Commission</p>
                        </div>
                        <div className="col-md-4">
                          <input
                            className="form-control"
                            id="inline-form-input"
                            type="number"
                            name="commission"
                            value={fundingGet?.commission}
                            readOnly
                          />
                        </div>
                        <div className="col-md-4 d-flex align-items-center justify-content-start">
                          <i
                            className="fa fa-calculator"
                            aria-hidden="true"
                          ></i>
                          <span className="ms-2">Seller</span>
                        </div>
                      </div>
                    ) : (
                      <>
                        <div className="row mt-0">
                          <div className="col-md-4 d-flex align-items-center justify-content-start">
                            <p className="mb-0">Agent Commission</p>
                          </div>
                          <div className="col-md-4">
                            <input
                              className="form-control"
                              id="inline-form-input"
                              type="number"
                              name="commission"
                              value={
                                formValues.commission ?? fundingGet?.commission
                              }
                              onChange={handleChanges}
                            />
                          </div>
                          <div className="col-md-4 d-flex align-items-center justify-content-start">
                            <i
                              className="fa fa-calculator"
                              aria-hidden="true"
                            ></i>
                            <span className="ms-2">Seller</span>
                          </div>
                        </div>
                        {rows.map((row, index) => (
                          <div key={index}>
                            <div className="row mt-3">
                              <div className="col-md-4 d-flex align-items-center justify-content-start">
                                {row.type === "bonus" ? (
                                  <p className="mb-0">{row.type}</p>
                                ) : (
                                  <input
                                    className="form-control"
                                    id={`type-${index}`}
                                    type="text"
                                    name="type"
                                    value={row.type}
                                    onChange={(e) =>
                                      handleChangeRow(e, index, "type")
                                    }
                                  />
                                )}
                              </div>

                              <div className="col-md-4">
                                <input
                                  className="form-control"
                                  id={`amount-${index}`}
                                  type="number"
                                  name="amount"
                                  value={row.amount}
                                  onChange={(e) =>
                                    handleChangeBonus(e, index, "amount")
                                  }
                                />
                              </div>

                              <div className="col-md-4 d-flex align-items-center justify-content-start">
                                <span className="ms-2">{row.chargedto}</span>
                                <img
                                  className="ms-3"
                                  onClick={() => handleRemove(index)}
                                  src="https://cdn.pboffice.net/shared/transaction/page_select_drop.png"
                                />
                              </div>
                            </div>
                          </div>
                        ))}
                      </>
                    )}

                    {summary.phase === "canceled" ? (
                      <div className="col-md-4  align-items-center justify-content-start">
                        <h6 className="mb-0">Total: ${totalAmount}</h6>
                      </div>
                    ) : (
                      <div className="row mt-4">
                        <div className="col-md-8 mt-0">
                          <select
                            className="form-select"
                            name="incomeItem"
                            value={selectedOption}
                            onChange={handleOptionChange}
                            onBlur={() => setSelectedOption("")}
                          >
                            <option value="">-- Add an income --</option>
                            <option value="bonus">Bonus</option>
                            <option value="transactionFee">
                              Transaction Fee
                            </option>
                            <option value="other">Other…</option>
                          </select>
                        </div>
                        <div className="col-md-4 d-flex align-items-center justify-content-start">
                          <h6 className="mb-0">Total: ${totalAmount}</h6>
                        </div>
                      </div>
                    )}

                    <div className="row mt-5">
                      <div className="col-md-12">
                        <p>
                          PAYOUTS - Itemize all payouts from the total
                          settlement charges.
                        </p>
                      </div>
                      <div className="col-md-4">
                        <h6>Payout Item</h6>
                      </div>
                      <div className="col-md-4">
                        <h6>Amount</h6>
                      </div>
                      <div className="col-md-4">
                        <h6>Pay To</h6>
                      </div>
                    </div>

                    {summary.phase === "canceled" ? (
                      <div className="row">
                        <div className="col-md-4">
                          <p>Net Pay Due to Agent (Estimate)</p>
                        </div>
                        <div className="col-md-4">
                          {fundingGet.estimate ? (
                            <p>${fundingGet.estimate ?? ""}</p>
                          ) : (
                            ""
                          )}
                        </div>
                        <div className="col-md-4 d-flex align-items-center justify-content-start">
                          <p className="mb-0">
                            {summary?.contact3?.[0]?.data?.firstName +
                              " " +
                              summary?.contact3?.[0]?.data?.lastName}
                          </p>
                        </div>
                      </div>
                    ) : (
                      <div className="row">
                        <div className="col-md-4">
                          <p>Net Pay Due to Agent (Estimate)</p>
                        </div>
                        <div className="col-md-4">
                          {netAmount ? <p>${netAmount.toFixed(2)}</p> : ""}
                        </div>
                        <div className="col-md-4 d-flex align-items-center justify-content-start">
                          <p className="mb-0">
                            {summary?.contact3?.[0]?.data?.firstName +
                              " " +
                              summary?.contact3?.[0]?.data?.lastName}
                          </p>
                        </div>
                      </div>
                    )}

                    {summary.type === "buy_commercial" ||
                    summary.type === "sale_commercial" ||
                    summary.type === "both_commercial" ||
                    summary.type === "referral_commercial" ? (
                      <>
                        {summary.phase === "canceled" ? (
                          <div className="row mt-0">
                            <div className="col-md-4 d-flex align-items-center justify-content-start">
                              <p className="mb-0">Transaction Fee</p>
                            </div>
                            <div className="col-md-4">
                              <input
                                className="form-control"
                                id="inline-form-input"
                                type="text"
                                name="transactionFee"
                                value="0"
                                // onChange={handleChange}
                              />
                            </div>
                            <div className="col-md-4 d-flex align-items-center justify-content-start">
                              <p className="mb-0">In Depth Realty</p>
                            </div>

                            <div className="col-md-4  align-items-center justify-content-start">
                              <h6 className="mb-0">
                                Total: ${totalAmountNew ? "0" : "0"}
                              </h6>
                            </div>
                          </div>
                        ) : (
                          <>
                            <div className="row mt-0">
                              <div className="col-md-4 d-flex align-items-center justify-content-start">
                                <p className="mb-0">
                                  Commercial Transaction Fee
                                </p>
                              </div>
                              <div className="col-md-4">
                                <input
                                  className="form-control"
                                  id="inline-form-input"
                                  type="text"
                                  name="transactionFee"
                                  value={transactionFesData.transactionFee}
                                  onChange={handleChangeTransactionFee}
                                />
                              </div>
                              <div className="col-md-4 d-flex align-items-center justify-content-start">
                                <p className="mb-0">In Depth Realty</p>
                              </div>
                            </div>

                            <div className="row mt-2">
                              <div className="col-md-4 d-flex align-items-center justify-content-start">
                                <p className="mb-0"> Wire fee:</p>
                              </div>
                              <div className="col-md-4">
                                <input
                                  className="form-control"
                                  id="inline-form-input"
                                  type="text"
                                  name="wireFee"
                                  value={transactionWire.wireFee}
                                  onChange={handleChangeWireFee}
                                />
                              </div>
                              <div className="col-md-4 d-flex align-items-center justify-content-start">
                                <p className="mb-0">In Depth Realty</p>
                              </div>
                            </div>

                            {rowsPayout.map((row, index) => (
                              <div key={index}>
                                <div className="row mt-4">
                                  <div className="col-md-4">
                                    <input
                                      className="form-control"
                                      id={`type-${index}`}
                                      type="text"
                                      name="type"
                                      value={row.type}
                                      onChange={(e) =>
                                        handleChangeRowPayout(e, index, "type")
                                      }
                                    />
                                  </div>

                                  <div className="col-md-4">
                                    <input
                                      className="form-control"
                                      id={`amount-${index}`}
                                      type="text"
                                      name="amount"
                                      value={row.amount}
                                      onChange={(e) =>
                                        handleChangePayout(e, index, "amount")
                                      }
                                    />
                                  </div>

                                  {row.type === "brokerageFee" ? (
                                    <>
                                      <div className="col-md-4">
                                        <span className="ms-2">
                                          {
                                            (row.payto =
                                              row.payto === "brokerageFee"
                                                ? "In Depth Realty"
                                                : "In Depth Realty")
                                          }
                                        </span>
                                        <img
                                          className="ms-3"
                                          onClick={() =>
                                            handleRemovePayout(index)
                                          }
                                          src="https://cdn.pboffice.net/shared/transaction/page_select_drop.png"
                                        />
                                      </div>
                                    </>
                                  ) : (
                                    <div className="col-md-4">
                                      <div className="d-flex">
                                        <div className="col-md-6">
                                          <input
                                            className="form-control"
                                            name="payto"
                                            value={row.payto}
                                            onChange={(e) =>
                                              handleChangePayout(
                                                e,
                                                index,
                                                "payto"
                                              )
                                            }
                                            onBlur={() => setSelectedPayout("")}
                                          />
                                        </div>
                                        <div className="col-md-6 d-flex align-items-center justify-content-start">
                                          <img
                                            className="ms-3"
                                            onClick={() =>
                                              handleRemovePayout(index)
                                            }
                                            src="https://cdn.pboffice.net/shared/transaction/page_select_drop.png"
                                          />
                                        </div>
                                      </div>
                                    </div>
                                  )}
                                </div>
                              </div>
                            ))}
                            <div className="row mt-4">
                              <div className="col-md-8">
                                <select
                                  className="form-select"
                                  name="payoutItem"
                                  value={selectedPayout}
                                  onChange={handleInputChange}
                                  onBlur={() => setSelectedPayout("")}
                                >
                                  <option>-- add a payout --</option>
                                  <option value="brokerageFee">
                                    Additional Brokerage Split or Fee
                                  </option>
                                  <option value="externalReferral">
                                    External Referral
                                  </option>
                                  <option value="otherPayout">Other…</option>
                                </select>
                              </div>
                              <div className="col-md-4 d-flex align-items-center justify-content-start">
                                <h6 className="m-0">
                                  {console.log(totalAmountNew)}
                                  {" "}
                                  Total: $
                                  {totalAmountNew
                                    ? totalAmountNew.toFixed(2)
                                    : ""}
                                </h6>
                              </div>
                            </div>
                          </>
                        )}
                      </>
                    ) : (
                      <>
                        {summary.represent === "both" ? (
                          <>
                            {summary.phase === "canceled" ? (
                              <div className="row mt-0">
                                <div className="col-md-4 d-flex align-items-center justify-content-start">
                                  <p className="mb-0">Transaction Fee</p>
                                </div>
                                <div className="col-md-4">
                                <input
                                className="form-control"
                                id="inline-form-input"
                                type="text"
                                name="transactionFee"
                                value="0"
                                // onChange={handleChange}
                              />
                                </div>
                                <div className="col-md-4 d-flex align-items-center justify-content-start">
                                  <p className="mb-0">In Depth Realty</p>
                                </div>

                                <div className="col-md-4  align-items-center justify-content-start">
                                  <h6 className="mb-0">
                                    Total: ${totalAmountNew}
                                  </h6>
                                </div>
                              </div>
                            ) : (
                              <>
                                {getCommission.property_interest &&
                                getCommission.property_interest === "yes" ? (
                                  <>
                                    <div className="row mt-0">
                                      <div className="col-md-4 d-flex align-items-center justify-content-start">
                                        <p className="mb-0">Transaction Fee</p>
                                      </div>
                                      <div className="col-md-4">
                                        <input
                                          className="form-control"
                                          id="inline-form-input"
                                          type="text"
                                          name="transactionFee"
                                          value={
                                            transactionFesData.transactionFee
                                          }
                                          onChange={handleChangeTransactionFee}
                                        />
                                      </div>
                                      <div className="col-md-4 d-flex align-items-center justify-content-start">
                                        <p className="mb-0">In Depth Realty</p>
                                      </div>
                                    </div>

                                    <div className="row mt-2">
                                      <div className="col-md-4 d-flex align-items-center justify-content-start">
                                        <p className="mb-0"> Wire fee:</p>
                                      </div>
                                      <div className="col-md-4">
                                        <input
                                          className="form-control"
                                          id="inline-form-input"
                                          type="text"
                                          name="wireFee"
                                          value={transactionWire.wireFee}
                                          onChange={handleChangeWireFee}
                                        />
                                      </div>
                                      <div className="col-md-4 d-flex align-items-center justify-content-start">
                                        <p className="mb-0">In Depth Realty</p>
                                      </div>
                                    </div>

                                    <div className="row mt-2">
                                      <div className="col-md-4 d-flex align-items-center justify-content-start">
                                        <p className="mb-0">
                                          Personal Real Estate Risk Management
                                          Fee:
                                        </p>
                                      </div>
                                      <div className="col-md-4">
                                        <input
                                          className="form-control"
                                          id="inline-form-input"
                                          type="text"
                                          name="transactionPersonal"
                                          value={
                                            transactionFesPersonal.transactionPersonal
                                          }
                                          onChange={handleChangePransactionPersonal}

                                        />
                                      </div>
                                      <div className="col-md-4 d-flex align-items-center justify-content-start">
                                        <p className="mb-0">In Depth Realty</p>
                                      </div>
                                    </div>

                                    {getCommission && (
                                      <div className="row mt-2">
                                        <div className="col-md-4 d-flex align-items-center justify-content-start">
                                          <p className="mb-0">
                                            High Value Transaction Fee:
                                          </p>
                                        </div>
                                        <div className="col-md-4">
                                        
                                          <input
                                            className="form-control"
                                            id="inline-form-input"
                                            type="text"
                                            name="highValueFee"
                                            value={transactionHighValueFee.highValueFee}
                                             onChange={handleChangeHighValueFee}
                                          />
                                        </div>
                                        <div className="col-md-4 d-flex align-items-center justify-content-start">
                                          <p className="mb-0">
                                            In Depth Realty
                                          </p>
                                        </div>
                                      </div>
                                    )}
                                  </>
                                ) : (
                                  <>
                                    <div className="row mt-0">
                                      <div className="col-md-4 d-flex align-items-center justify-content-start">
                                        <p className="mb-0">
                                          Buyer Side Transaction fee
                                        </p>
                                      </div>
                                      <div className="col-md-4">
                                        <input
                                          className="form-control"
                                          id="inline-form-input"
                                          type="text"
                                          name="buyerFee"
                                          value={transactionBuyerFee.buyerFee}
                                          onChange={handleChangeBuyerFee}
                                        />
                                      </div>
                                      <div className="col-md-4 d-flex align-items-center justify-content-start">
                                        <p className="mb-0">In Depth Realty</p>
                                      </div>
                                    </div>

                                    <div className="row mt-2">
                                      <div className="col-md-4 d-flex align-items-center justify-content-start">
                                        <p className="mb-0">
                                          Seller Side Transaction fee:
                                        </p>
                                      </div>
                                      <div className="col-md-4">
                                        <input
                                          className="form-control"
                                          id="inline-form-input"
                                          type="text"
                                          name="sellerFee"
                                          value={transactionSellerFee.sellerFee}
                                          onChange={handleChangesSellerFee}
                                        />
                                      </div>
                                      <div className="col-md-4 d-flex align-items-center justify-content-start">
                                        <p className="mb-0">In Depth Realty</p>
                                      </div>
                                    </div>

                                    <div className="row mt-2">
                                      <div className="col-md-4 d-flex align-items-center justify-content-start">
                                        <p className="mb-0">
                                          Dual Agency Risk Management Fee:
                                        </p>
                                      </div>
                                      <div className="col-md-4">
                                        <input
                                          className="form-control"
                                          id="inline-form-input"
                                          type="text"
                                          name="managementFee"
                                          value={transactionRisk.managementFee}
                                          onChange={handleChangeManagementFee}
                                        />
                                      </div>
                                      <div className="col-md-4 d-flex align-items-center justify-content-start">
                                        <p className="mb-0">In Depth Realty</p>
                                      </div>
                                    </div>

                                    <div className="row mt-2">
                                      <div className="col-md-4 d-flex align-items-center justify-content-start">
                                        <p className="mb-0">Wire fee:</p>
                                      </div>
                                      <div className="col-md-4">
                                        <input
                                          className="form-control"
                                          id="inline-form-input"
                                          type="text"
                                          name="wireFee"
                                          value={transactionWire.wireFee}
                                          onChange={handleChangeWireFee}
                                        />
                                      </div>
                                      <div className="col-md-4 d-flex align-items-center justify-content-start">
                                        <p className="mb-0">In Depth Realty</p>
                                      </div>
                                    </div>
                                  </>
                                )}

                                {rowsPayout.map((row, index) => (
                                  <div key={index}>
                                    <div className="row mt-4">
                                      <div className="col-md-4">
                                        <input
                                          className="form-control"
                                          id={`type-${index}`}
                                          type="text"
                                          name="type"
                                          value={row.type}
                                          onChange={(e) =>
                                            handleChangeRowPayout(
                                              e,
                                              index,
                                              "type"
                                            )
                                          }
                                        />
                                      </div>

                                      <div className="col-md-4">
                                        <input
                                          className="form-control"
                                          id={`amount-${index}`}
                                          type="number"
                                          name="amount"
                                          value={row.amount}
                                          onChange={(e) =>
                                            handleChangePayout(
                                              e,
                                              index,
                                              "amount"
                                            )
                                          }
                                        />
                                      </div>

                                      {row.type === "brokerageFee" ? (
                                        <>
                                          <div className="col-md-4">
                                            <span className="ms-2">
                                              {
                                                (row.payto =
                                                  row.payto === "brokerageFee"
                                                    ? "In Depth Realty"
                                                    : "In Depth Realty")
                                              }
                                            </span>
                                            <img
                                              className="ms-3"
                                              onClick={() =>
                                                handleRemovePayout(index)
                                              }
                                              src="https://cdn.pboffice.net/shared/transaction/page_select_drop.png"
                                            />
                                          </div>
                                        </>
                                      ) : (
                                        <div className="col-md-4">
                                          <div className="d-flex">
                                            <div className="col-md-6">
                                              <input
                                                className="form-control"
                                                name="payto"
                                                value={row.payto}
                                                onChange={(e) =>
                                                  handleChangePayout(
                                                    e,
                                                    index,
                                                    "payto"
                                                  )
                                                }
                                                onBlur={() =>
                                                  setSelectedPayout("")
                                                }
                                              />
                                            </div>
                                            <div className="col-md-6 d-flex align-items-center justify-content-start">
                                              <img
                                                className="ms-3"
                                                onClick={() =>
                                                  handleRemovePayout(index)
                                                }
                                                src="https://cdn.pboffice.net/shared/transaction/page_select_drop.png"
                                              />
                                            </div>
                                          </div>
                                        </div>
                                      )}
                                    </div>
                                  </div>
                                ))}
                                <div className="row mt-4">
                                  <div className="col-md-8">
                                    <select
                                      className="form-select"
                                      name="payoutItem"
                                      value={selectedPayout}
                                      onChange={handleInputChange}
                                      onBlur={() => setSelectedPayout("")}
                                    >
                                      <option>-- add a payout --</option>
                                      <option value="brokerageFee">
                                        Additional Brokerage Split or Fee
                                      </option>
                                      <option value="externalReferral">
                                        External Referral
                                      </option>
                                      <option value="otherPayout">
                                        Other…
                                      </option>
                                    </select>
                                  </div>
                                  <div className="col-md-4 d-flex align-items-center justify-content-start">
                                    <h6 className="mb-0">
                                      Total: $
                                      {totalAmountNew
                                        ? totalAmountNew.toFixed(2)
                                        : ""}
                                    </h6>
                                  </div>
                                </div>
                              </>
                            )}
                          </>
                        ) : (
                          <>
                            {summary.phase === "canceled" ? (
                              <div className="row mt-0">
                                <div className="col-md-4 d-flex align-items-center justify-content-start">
                                  <p className="mb-0">Transaction Fee</p>
                                </div>
                                <div className="col-md-4">
                                  <input
                                    className="form-control"
                                    id="inline-form-input"
                                    type="text"
                                    name="transactionFee"
                                    value={transactionFesData.transactionFee}
                                    onChange={handleChangeTransactionFee}
                                  />
                                </div>
                                <div className="col-md-4 d-flex align-items-center justify-content-start">
                                  <p className="mb-0">In Depth Realty</p>
                                </div>

                                <div className="col-md-4  align-items-center justify-content-start">
                                  <h6 className="mb-0">
                                    Total: ${totalAmountNew}
                                  </h6>
                                </div>
                              </div>
                            ) : (
                              <>
                                <div className="row mt-0">
                                  <div className="col-md-4 d-flex align-items-center justify-content-start">
                                    <p className="mb-0">Transaction Fee</p>
                                  </div>
                                  <div className="col-md-4">
                                    <input
                                      className="form-control"
                                      id="inline-form-input"
                                      type="text"
                                      name="transactionFee"
                                      value={transactionFesData.transactionFee}
                                      onChange={handleChangeTransactionFee}
                                    />
                                  </div>
                                  <div className="col-md-4 d-flex align-items-center justify-content-start">
                                    <p className="mb-0">In Depth Realty</p>
                                  </div>
                                </div>

                                <div className="row mt-2">
                                  <div className="col-md-4 d-flex align-items-center justify-content-start">
                                    <p className="mb-0"> Wire fee:</p>
                                  </div>
                                  <div className="col-md-4">
                                    <input
                                      className="form-control"
                                      id="inline-form-input"
                                      type="text"
                                      name="wireFee"
                                      value={transactionWire.wireFee}
                                      onChange={handleChangeWireFee}
                                    />
                                  </div>
                                  <div className="col-md-4 d-flex align-items-center justify-content-start">
                                    <p className="mb-0">In Depth Realty</p>
                                  </div>
                                </div>

                                {getCommission && (
                                  <div className="row mt-2">
                                    <div className="col-md-4 d-flex align-items-center justify-content-start">
                                      <p className="mb-0">
                                        High Value Transaction Fee:
                                      </p>
                                    </div>

                                    <div className="col-md-4">
                                      <input
                                        className="form-control"
                                        id="inline-form-input"
                                        type="text"
                                        name="highValueFee"
                                        value={transactionHighValueFee.highValueFee}
                                        onChange={handleChangeHighValueFee}
                                      />
                                    </div>
                                    <div className="col-md-4 d-flex align-items-center justify-content-start">
                                      <p className="mb-0">In Depth Realty</p>
                                    </div>
                                  </div>
                                )}

                                {getCommission.property_interest &&
                                getCommission.property_interest === "yes" ? (
                                  <div className="row mt-2">
                                    <div className="col-md-4 d-flex align-items-center justify-content-start">
                                      <p className="mb-0">
                                        Personal Real Estate Risk Management
                                        Fee:
                                      </p>
                                    </div>
                                    <div className="col-md-4">
                                      <input
                                        className="form-control"
                                        id="inline-form-input"
                                        type="text"
                                        name="transactionPersonal"
                                        value={
                                          transactionFesPersonal.transactionPersonal
                                        }
                                        onChange={handleChangePransactionPersonal}
                                      />
                                    </div>
                                    <div className="col-md-4 d-flex align-items-center justify-content-start">
                                      <p className="mb-0">In Depth Realty</p>
                                    </div>
                                  </div>
                                ) : (
                                  ""
                                )}

                                {rowsPayout.map((row, index) => (
                                  <div key={index}>
                                    <div className="row mt-4">
                                      <div className="col-md-4">
                                        <input
                                          className="form-control"
                                          id={`type-${index}`}
                                          type="text"
                                          name="type"
                                          value={row.type}
                                          onChange={(e) =>
                                            handleChangeRowPayout(
                                              e,
                                              index,
                                              "type"
                                            )
                                          }
                                        />
                                      </div>

                                      <div className="col-md-4">
                                        <input
                                          className="form-control"
                                          id={`amount-${index}`}
                                          type="text"
                                          name="amount"
                                          value={row.amount}
                                          onChange={(e) =>
                                            handleChangePayout(
                                              e,
                                              index,
                                              "amount"
                                            )
                                          }
                                        />
                                      </div>

                                      {row.type === "brokerageFee" ? (
                                        <>
                                          <div className="col-md-4">
                                            <span className="ms-2">
                                              {
                                                (row.payto =
                                                  row.payto === "brokerageFee"
                                                    ? "In Depth Realty"
                                                    : "In Depth Realty")
                                              }
                                            </span>
                                            <img
                                              className="ms-3"
                                              onClick={() =>
                                                handleRemovePayout(index)
                                              }
                                              src="https://cdn.pboffice.net/shared/transaction/page_select_drop.png"
                                            />
                                          </div>
                                        </>
                                      ) : (
                                        <div className="col-md-4">
                                          <div className="d-flex">
                                            <div className="col-md-6">
                                              <input
                                                className="form-control"
                                                name="payto"
                                                value={row.payto}
                                                onChange={(e) =>
                                                  handleChangePayout(
                                                    e,
                                                    index,
                                                    "payto"
                                                  )
                                                }
                                                onBlur={() =>
                                                  setSelectedPayout("")
                                                }
                                              />
                                            </div>
                                            <div className="col-md-6 d-flex align-items-center justify-content-start">
                                              <img
                                                className="ms-3"
                                                onClick={() =>
                                                  handleRemovePayout(index)
                                                }
                                                src="https://cdn.pboffice.net/shared/transaction/page_select_drop.png"
                                              />
                                            </div>
                                          </div>
                                        </div>
                                      )}
                                    </div>
                                  </div>
                                ))}

                                <div className="row mt-4">
                                  <div className="col-md-8">
                                    <select
                                      className="form-select"
                                      name="payoutItem"
                                      value={selectedPayout}
                                      onChange={handleInputChange}
                                      onBlur={() => setSelectedPayout("")}
                                    >
                                      <option>-- add a payout --</option>
                                      <option value="brokerageFee">
                                        Additional Brokerage Split or Fee
                                      </option>
                                      <option value="externalReferral">
                                        External Referral
                                      </option>
                                      <option value="otherPayout">
                                        Other…
                                      </option>
                                    </select>
                                  </div>
                                  <div className="col-md-4 d-flex align-items-center justify-content-start">
                                    <h6 className="m-0">
                                      {" "}
                                      Total: $
                                      {totalAmountNew
                                        ? totalAmountNew.toFixed(2)
                                        : ""}
                                    </h6>
                                  </div>
                                </div>
                              </>
                            )}
                          </>
                        )}
                      </>
                    )}

                    {/* <h6 className="mt-2">Total: ${totalAmount? totalAmount:incomeResponce}</h6> */}
                    <div className="row mt-5">
                      <div className="col-md-12">
                        <p>
                          AGENT REMARKS - Enter any comments you have about the
                          funding of this transaction.
                        </p>
                      </div>

                      <div className="col-md-12">
                        <label className="col-form-label pt-0 mb-0 float-left w-100 mb-0">
                          Comments/Remarks/Instructions
                        </label>
                        <textarea
                          className="form-control mt-0"
                          id="textarea-input"
                          rows="5"
                          name="comments"
                          onChange={handleChange}
                          autoComplete="on"
                          value={formValues.comments ?? fundingGet?.comments}
                        >
                          Hello World!
                        </textarea>
                      </div>
                    </div>

                    {/* <div className="row mt-5">
                      <div className="col-md-12">
                        <p>
                          Funding Method - how will you be paid at closing.
                        </p>
                      </div>

                      <div className="col-md-12">
                            <div className="form-check ms-lg-3 ms-md-3">
                              <input
                                className="form-check-input"
                                id=""
                                type="radio"
                                name="funding_method"
                                value="wired_funds"
                                onChange={handleChange}
                                checked={formValues?.funding_method === "wired_funds"}
                              />
                              <label className="form-label" id="">
                                Wired Funds
                              </label>
                            </div>

                            <div className="form-check ms-lg-3 ms-md-3">
                              <input
                                className="form-check-input"
                                id=""
                                type="radio"
                                name="funding_method"
                                value="check"
                                onChange={handleChange}
                                checked={formValues?.funding_method === "check"}
                              />
                              <label className="form-label" id="">
                                Check
                              </label>
                            </div>

                            <div className="invalid-tooltip">{formErrors.funding_method}</div>
                      </div>
                   </div> */}
                  </div>
                  <div className="pull-right mt-4">
                    <button
                      className="btn btn-secondary pull-right ms-3"
                      type="button"
                    >
                      Cancel
                    </button>
                    <button
                      className="btn btn-primary pull-right"
                      type="button"
                      disabled={isActive}
                      onClick={handleSubmit}
                    >
                      {isActive ? "Please Wait..." : "Confirm Funding Request"}
                    </button>
                  </div>
                </div>

                <div className="col-md-2 offset-md-1">
                  <div className="d-flex align-items-center justify-content-between mb-4">
                    <h5 className="mb-0">Transaction Details</h5>
                  </div>
                  <div className="border rounded-3 p-3 float-left w-100">
                    <label className="col-form-label pt-0 mb-0 py-1">
                      Funding:
                    </label>
                    <p className="mb-2">Not Set</p>
                    <label className="col-form-label pt-0 mb-0 py-1">
                      {" "}
                      {summary.represent ? (
                        <>
                          {summary.represent === "buyer" ? "Buyer" : ""}

                          {summary.represent === "seller" ? "Seller" : ""}

                          {summary.represent === "both" ? "Buyer & Seller" : ""}

                          {summary.represent === "referral" ? "Seller" : ""}
                        </>
                      ) : (
                        ""
                      )}{" "}
                      (Client):
                    </label>

                    <p className="mb-3">
                      {" "}
                      {summary?.contact1?.[0]?.data?.firstName ??
                        "Not Set"}{" "}
                      {summary?.contact1?.[0]?.data?.lastName ?? "Not Set"}
                    </p>

                    <label className="col-form-label pt-0 mb-0 py-1">
                      {summary?.contact2?.[0]?.data?.type === "seller"
                        ? "Seller"
                        : summary?.contact2?.[0]?.data?.type === "buyer"
                        ? "Buyer"
                        : summary?.contact2?.[0]?.data?.type === "buyersale"
                        ? "Buyer & Seller"
                        : summary?.contact2?.[0]?.data?.type ===
                          "referral_sellerAgent"
                        ? "Buyer"
                        : "Default Label"}
                    </label>
                    {summary?.contact2?.[0]?.data?.type ===
                    "referral_sellerAgent" ? (
                      <p className="mb-3">Not Set</p>
                    ) : (
                      <p className="mb-3">
                        {summary?.contact2?.[0]?.data?.firstName ?? "Not Set"}{" "}
                        {summary?.contact2?.[0]?.data?.lastName ?? ""}
                      </p>
                    )}

                    <label className="col-form-label pt-0 mb-0 py-1">
                      {" "}
                      MLS #:
                    </label>
                    <p className="mb-2">
                      {" "}
                      {summary?.propertyDetail?.mlsNumber ?? "Not set"}
                    </p>
                    <label className="col-form-label pt-0 mb-0 py-1">
                      Sale Price:
                    </label>
                    <p className="mb-3">
                      {getCommission.purchase_price ?? "Not set"}
                    </p>
                    <label className="col-form-label pt-0 mb-0 py-1">
                      Legacy:
                    </label>
                    <p className="mb-3">
                      {getContact?.legacy_account === "yes" ? "Yes" : "No"}
                    </p>

                    <label className="col-form-label pt-0 mb-0 py-1">
                      Commission:
                    </label>
                    <p className="mb-2">Not Set</p>
                    <label className="col-form-label pt-0 mb-0 py-1">
                      Moved-In Date:
                    </label>
                    <p className="mb-2">Not Set</p>
                    <label className="col-form-label pt-0 mb-0 py-1">
                      Time Spent:
                    </label>
                    <p className="mb-2">{formattedDateTime ?? "Not set"}</p>

                    <label className="col-form-label pt-0 mb-0 py-1">
                      Agent's Comm. Plan:
                    </label>
                    <p className="mb-2">Not Set</p>
                  </div>
                </div>
              </div>
            </div>
          </div>
          </div>
        </div>
      </main>
    </div>
  );
};

export default FundingConfirmation;

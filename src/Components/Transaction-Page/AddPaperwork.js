import React, { useState, useEffect } from "react";
import _ from "lodash";
import user_service from "../service/user_service";
import jwt from "jwt-decode";
import Loader from "../../Pages/Loader/Loader.js";
import Toaster from "../../Pages/Toaster/Toaster.js";

import { NavLink, useNavigate, useParams } from "react-router-dom";

function AddPaperwork() {
  const params = useParams();
  const navigate = useNavigate();

  const initialValues = {
    paperwork_location: "",
    paperwork_title: "",
    paperwork_formurl: "",
    required: "",
    can_agentupload: "",
    paperwork_expire: "",
    custom_expire_year: "",
    is_active: "",
  };
  const [formValues, setFormValues] = useState(initialValues);
  const [formErrors, setFormErrors] = useState({});
  const [isSubmitClick, setISSubmitClick] = useState(false);
  const [loader, setLoader] = useState({ isActive: null });
  const { isActive } = loader;
  const [toaster, setToaster] = useState({
    types: null,
    isShow: null,
    toasterBody: "",
    message: "",
  });
  const { types, isShow, toasterBody, message } = toaster;

  const [check, setCheck] = useState("Yes");
  const [requiredCheck, setRequiredCheck] = useState("Yes");

  const [checkRadio, setCheckRadio] = useState("Yes");

  const [showPopup, setShowPopup] = useState(false);
  const [selectedItem, setSelectedItem] = useState("Corporate");

  const handlePopupToggle = () => {
    setShowPopup(!showPopup);
  };

  const handleSelectItem = (item) => {
    setSelectedItem(item);
    setShowPopup(false);
  };

  const CheckList = (e) => {
    setCheck(e.target.name);
    setCheck(e.target.value);
  };

  const checkRequired = (e) => {
    setRequiredCheck(e.target.name);
    setRequiredCheck(e.target.value);
  };

  const CheckAgent = (e) => {
    setCheckRadio(e.target.name);
    setCheckRadio(e.target.value);
  };

  const handleChange = (e) => {
    const { name, value } = e.target;
    setFormValues({ ...formValues, [name]: value });
  };
  const cancel = () => {
    setShowPopup(false);
  };

  {
    /* <!-- Form Validation Start--> */
  }
  useEffect(() => {
    if (formValues && isSubmitClick) {
      validate();
    }
  }, [formValues]);

  const validate = () => {
    const values = formValues;
    const errors = {};

    if (!values.paperwork_title) {
      errors.paperwork_title = "paperwork_title is required";
    }

    setFormErrors(errors);
    return errors;
  };
  {
    /* <!-- Form Validation End--> */
  }

  const handleSubmit = async (e) => {
    if (params.id) {
      e.preventDefault();
      setISSubmitClick(true);
      let checkValue = validate();
      if (_.isEmpty(checkValue)) {
        const userData = {
          paperwork_location: selectedItem,
          paperwork_title: formValues.paperwork_title,
          paperwork_formurl: formValues.paperwork_formurl,
          required: requiredCheck,
          can_agentupload: checkRadio,
          paperwork_expire: check,
          custom_expire_year: formValues.custom_expire_year,
          is_active: "Yes",
          agentId: jwt(localStorage.getItem("auth")).id,
        };
        console.log(userData);
        setLoader({ isActive: true });
        const response = await user_service.paperworkUpdate(
          params.id,
          userData
        );
        if (response) {
          console.log(response);
          setLoader({ isActive: false });
          setToaster({
            type: "UpdatePaperwork  Successfully",
            isShow: true,
            toasterBody: response.data.message,
            message: "UpdatePaperwork Successfully",
          });
          setTimeout(() => {
            setToaster((prevToaster) => ({ ...prevToaster, isShow: false }));
            navigate(`/paperwork`);
          }, 500);
        }
      }
    } else {
      e.preventDefault();
      setISSubmitClick(true);
      let checkValue = validate();
      if (_.isEmpty(checkValue)) {
        const userData = {
          paperwork_location: selectedItem,
          paperwork_title: formValues.paperwork_title,
          paperwork_formurl: formValues.paperwork_formurl,
          required: requiredCheck,
          can_agentupload: checkRadio,
          paperwork_expire: check,
          custom_expire_year: formValues.custom_expire_year,
          is_active: "Yes",
          agentId: jwt(localStorage.getItem("auth")).id,
        };
        console.log(userData);
        setLoader({ isActive: true });
        const response = await user_service.paperworkAdd(userData);
        if (response) {
          console.log(response);
          setLoader({ isActive: false });
          setToaster({
            type: "AddPaperwork Successfully",
            isShow: true,
            toasterBody: response.data.message,
            message: "AddPaperwork Successfully",
          });
          setTimeout(() => {
            setToaster((prevToaster) => ({ ...prevToaster, isShow: false }));
            navigate(`/paperwork`);
          }, 500);
        }
      }
    }
  };

  useEffect(() => {
    window.scrollTo(0, 0);
    if (params.id) {
      setLoader({ isActive: true });
      const PaperworkAll = async () => {
        await user_service.paperworkGetId(params.id).then((response) => {
          setLoader({ isActive: false });
          if (response) {
            console.log(response);
            setFormValues(response.data);
          }
        });
      };
      PaperworkAll(params.id);
    }
  }, [params.id]);

  return (
    <div className="bg-secondary float-left w-100 pt-4">
      <Loader isActive={isActive} />
      {isShow && (
        <Toaster
          types={types}
          isShow={isShow}
          toasterBody={toasterBody}
          message={message}
        />
      )}
      <div className="container">
        <div className="justify-content-center pb-sm-2">
          <div className="bg-light rounded-3 p-4 mb-3">
            <div className="add_listing shadow-sm w-100 m-auto p-4">
              <button
                className="btn btn-primary pull-right ms-5"
                type="button"
                onClick={handleSubmit}
              >
                {params.id ? "Update" : "Add Paperwork"}
              </button>
              <NavLink
                to="/paperwork"
                className="btn btn-secondary pull-right"
                type="button"
              >
                Cancel
              </NavLink>

              <div className="row">
                <h6>At what location lavel this paperwork should be added</h6>

                <div className="col-sm-10">
                  <p className="d-flex">
                    {selectedItem} &nbsp; &nbsp;
                    <NavLink>
                      <p onClick={handlePopupToggle}>change</p>
                    </NavLink>
                  </p>

                  {showPopup && (
                    <div className="popup">
                      <img
                        onClick={cancel}
                        className="pull-right"
                        src="https://cdn.pboffice.net/shared/transaction/page_select_drop.png"
                      />
                      <span>Choose a Location</span>
                      <br />
                      <span>Choose the location from the tree.</span>
                      <hr />
                      <NavLink>InDepth Realty</NavLink>
                      <div>
                        <li>
                          <NavLink
                            onClick={() => handleSelectItem("Corporate")}
                          >
                            Corporate
                          </NavLink>
                        </li>
                        <li>
                          {" "}
                          <NavLink onClick={() => handleSelectItem("Elite")}>
                            Elite
                          </NavLink>
                        </li>
                        <li>
                          <NavLink onClick={() => handleSelectItem("Arizona")}>
                            Arizona
                          </NavLink>
                        </li>
                        <li>
                          <NavLink onClick={() => handleSelectItem("Arizona")}>
                            Colorado
                          </NavLink>
                        </li>
                        <li>
                          <NavLink onClick={() => handleSelectItem("Arizona")}>
                            Idaho
                          </NavLink>
                        </li>
                      </div>
                    </div>
                  )}
                </div>

                <div className="col-md-8">
                  <label className="form-label">
                    Paperwork Title or Form Name
                  </label>
                  <input
                    className="form-control"
                    id="inline-form-input"
                    type="text"
                    name="paperwork_title"
                    value={formValues.paperwork_title}
                    onChange={handleChange}
                    style={{
                      border: formErrors?.paperwork_title
                        ? "1px solid red"
                        : "1px solid #00000026",
                    }}
                  />
                  {formErrors.paperwork_title && (
                    <div className="invalid-tooltip">
                      {formErrors.paperwork_title}
                    </div>
                  )}
                </div>

                <div className="col-md-8 mt-5">
                  <label className="form-label">
                    URL to Download Blank Form (optional){" "}
                  </label>
                  <input
                    className="form-control"
                    id="inline-form-input"
                    type="text"
                    name="paperwork_formurl"
                    value={formValues.paperwork_formurl}
                    onChange={handleChange}
                  />
                </div>

                <div
                  className="col-md-8 mt-5"
                  onClick={(e) => checkRequired(e)}
                >
                  <h6>Is this paperwork required?</h6>
                  <div className="d-flex">
                    <div className="form-check">
                      <input
                        className="form-check-input"
                        id="form-check-1"
                        type="radio"
                        name="required"
                        value="Yes"
                        checked
                      />
                      <label className="form-check-label" id="radio-level1">
                        Yes
                      </label>
                    </div>{" "}
                    &nbsp;&nbsp;&nbsp;
                    <div className="form-check">
                      <input
                        className="form-check-input"
                        id="form-check-2"
                        type="radio"
                        name="required"
                        value="No"
                      />
                      <label className="form-check-label" id="radio-level1">
                        No
                      </label>
                    </div>
                  </div>
                </div>

                <div className="col-md-8 mt-5" onClick={(e) => CheckAgent(e)}>
                  <h6>Can you agent uplaod this paperwork?</h6>
                  <div className="d-flex">
                    <div className="form-check">
                      <input
                        className="form-check-input"
                        id="form-check-3"
                        type="radio"
                        name="can_agentupload"
                        value="Yes"
                        checked
                      />
                      <label className="form-check-label" id="radio-level1">
                        Yes
                      </label>
                    </div>{" "}
                    &nbsp;&nbsp;&nbsp;
                    <div className="form-check">
                      <input
                        className="form-check-input"
                        id="form-check-4"
                        type="radio"
                        name="can_agentupload"
                        value="No"
                      />
                      <label className="form-check-label" id="radio-level1">
                        No
                      </label>
                    </div>
                  </div>
                </div>

                <div className="col-md-8 mt-5" onClick={(e) => CheckList(e)}>
                  <h6>When does this paperwork expire?</h6>
                  <div className="form-check">
                    <input
                      className="form-check-input"
                      id="form-check-5"
                      type="radio"
                      name="paperwork_expire"
                      value="never"
                      checked
                    />
                    <label className="form-check-label" id="radio-level1">
                      Never (Does not have expiration date)
                    </label>
                  </div>{" "}
                  &nbsp;&nbsp;&nbsp;
                  <div className="form-check">
                    <input
                      className="form-check-input"
                      id="form-check-6"
                      type="radio"
                      name="paperwork_expire"
                      value="yearly"
                    />
                    <label className="form-check-label" id="radio-level1">
                      Yearly (Expire first day of each year)
                    </label>
                  </div>
                  <div className="form-check mt-5">
                    <input
                      className="form-check-input"
                      id="form-check-7"
                      type="radio"
                      name="paperwork_expire"
                      value="plan_activated"
                    />
                    <label className="form-check-label" id="radio-level1">
                      Anniversary (Expire after the selected years form the date
                      memeber plan activated)
                    </label>
                  </div>
                  {check === "plan_activated" ? (
                    <div>
                      <select
                        className="form-select"
                        id="pr-city"
                        name="custom_expire_year"
                        onChange={handleChange}
                        value={formValues.custom_expire_year}
                      >
                        <option>1</option>
                        <option>2</option>
                        <option>3</option>
                        <option>4</option>
                        <option>5</option>
                      </select>
                    </div>
                  ) : (
                    ""
                  )}
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
export default AddPaperwork;

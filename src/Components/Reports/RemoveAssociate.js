import React, { useState, useEffect } from "react";
import Pic from "../img/pic.png";
import Modal from "../../Modal";
import user_service from "../service/user_service";
import ReactPaginate from "react-paginate";
import Loader from "../../Pages/Loader/Loader.js";
import Toaster from "../../Pages/Toaster/Toaster.js";
import { NavLink, useNavigate } from "react-router-dom";
import Birthday from "../img/birthday.jpg";
import moment from "moment-timezone";


function RemoveAssociate() {
  const [getContact, setGetContact] = useState([]);
  const [pageCount, setPageCount] = useState(0);
  const [totalRecords, setTotalRecords] = useState("");
  const [loader, setLoader] = useState({ isActive: null });
  const { isActive } = loader;
  const [toaster, setToaster] = useState({
    types: null,
    isShow: null,
    toasterBody: "",
    message: "",
  });
  const { types, isShow, toasterBody, message } = toaster;

  useEffect(() => {
    window.scrollTo(0, 0);
    const ContactGetDeleted = async () => {
        setLoader({ isActive: true })
        await user_service.contactGetassocDeleted(1,"deleted").then((response) => {
            setLoader({ isActive: false })
            if (response) {
                console.log((response));
                setGetContact(response.data.data);
                setPageCount(Math.ceil(response.data.totalRecords / 10));
            }
        });
    }
    ContactGetDeleted()
  }, []);
  

  const handlePageClick = async (data) => {
    let currentPage = data.selected + 1;
    let skip = currentPage - 1 + currentPage;
    setLoader({ isActive: true });
    console.log(currentPage);
    await user_service
      .contactGetassocDeleted(currentPage, "deleted")
      .then((response) => {
        setLoader({ isActive: false });
        if (response) {
          // setResults(response.data.data);
          setGetContact(response.data.data);
          setPageCount(Math.ceil(response.data.totalRecords / 10));
        }
      });
  };

  return (
    <div className="bg-secondary float-left w-100 pt-4 mb-4">
      <Loader isActive={isActive} />
      {isShow && (
        <Toaster
          types={types}
          isShow={isShow}
          toasterBody={toasterBody}
          message={message}
        />
      )}
      <main className="page-wrapper">
        <Modal />
     
        <div className="content-overlay">
        
          <div className="">
            <div className="row">
              
              <div className="col-md-12">
                <div className="float-left w-100 d-flex align-items-center justify-content-between mb-4">
                  <h3 className="text-white mb-0">Removed Contact</h3>
                </div>
             

                {/* <!-- Item--> */}
                {getContact && getContact.length > 0 ? (
                  getContact.map((post) => (
                    <>
                      {post.contact_status === "deleted" ? (
                        <div className="card card-hover mb-2">
                          <div className="card-body">
                            <div className="float-left w-100">
                              <div className="float-left w-100 d-lg-flex d-md-flex d-inline-block align-items-center justify-content-between">
                                {/* <img className="d-none d-sm-block" src={Pic} width="100" alt="Resume picture" /> */}
                                <div
                                  className="float-left w-100 d-flex align-items-center justify-content-start"
                                  key={post._id}
                                >
                                  <img
                                    height="70"
                                    width="70"
                                    className="rounded-circlee profile_picture"
                                    src={post.image || Pic}
                                    alt="Profile"
                                  />
                                  {/* <div className="ms-3" onClick={()=>handleSubmit(post._id)}> */}
                                  <div className="ms-3">
                                    <p className="mb-1">
                                      <strong>{post.firstName}</strong>&nbsp;
                                      <strong>{post.lastName}</strong>
                                    </p>
                                    {post?.phone ? (
                                      <>
                                        <p className="mb-1">
                                          Phone: {post?.phone ?? ""}
                                        </p>
                                      </>
                                    ) : (
                                      ""
                                    )}
                                    {post?.email ? (
                                      <>
                                        <p className="mb-1">
                                          Email: {post?.email ?? ""}
                                        </p>
                                      </>
                                    ) : (
                                      ""
                                    )}
                                    {post.additionalActivateFields
                                      .admin_note ? (
                                      <>
                                        <p className="mb-1">
                                          Transfer From:{" "}
                                          {post.additionalActivateFields
                                            .admin_note
                                            ? post.additionalActivateFields
                                                .admin_note
                                            : ""}
                                        </p>
                                      </>
                                    ) : (
                                      ""
                                    )}
                                    {post.additionalActivateFields
                                      .office_affiliation ? (
                                      <>
                                        <p className="mb-1">
                                          Office Affiliation:{" "}
                                          {post.additionalActivateFields
                                            .office_affiliation
                                            ? post.additionalActivateFields
                                                .office_affiliation
                                            : ""}
                                        </p>
                                      </>
                                    ) : (
                                      ""
                                    )}
                                  </div>
                                </div>
                                <div className="float-left w-100 d-flex align-items-center justify-content-between my-lg-0 my-md-0 my-sm-3 my-3">
                                <badge
                              className="mx-4"
                              style={{ fontSize: "14px" }}
                            >
                              {post.contactType
                                ? post.contactType.charAt(0).toUpperCase() +
                                  post.contactType.slice(1)
                                : "Not Assigned"}
                            </badge>

                                  <span className="">
                                    <NavLink
                                      to={`/contact-profile/${post._id}`}
                                      className=""
                                    >
                                      <i className="fi-info-circle fs-5 mt-n1 me-2"></i>
                                    </NavLink>
                                  </span>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      ) : (
                        ""
                      )}
                    </>
                  ))
                ) : (
                  <div className="text-center mb-3">
                    <img className="" src={Pic} />
                    <br />
                    <p className="mt-3 text-white">Not Any Remove Associate.</p>
                  </div>
                )}
              </div>
              {pageCount && pageCount > 1 ? (
                <div className="d-flex align-items-center justify-content-end mb-0 mt-4">
                  <ReactPaginate
                    className=""
                    previousLabel={"Previous"}
                    nextLabel={"Next"}
                    breakLabel={"..."}
                    pageCount={pageCount}
                    marginPagesDisplayed={1}
                    pageRangeDisplayed={2}
                    onPageChange={handlePageClick}
                    containerClassName={
                      "pagination justify-content-center mb-0"
                    }
                    pageClassName={"page-item"}
                    pageLinkClassName={"page-link"}
                    previousClassName={"page-item"}
                    previousLinkClassName={"page-link"}
                    nextClassName={"page-item"}
                    nextLinkClassName={"page-link"}
                    breakClassName={"page-item"}
                    breakLinkClassName={"page-link"}
                    activeClassName={"active"}
                  />
                </div>
              ) : (
                ""
              )}
            </div>
          </div>
        </div>
      </main>
    </div>
  );
}
export default RemoveAssociate;

import React, { useState, useEffect } from "react";
import Pic from "../img/pic.png";
// import Modal from "../../Modal";
import user_service from "../service/user_service";
import ReactPaginate from "react-paginate";
import Loader from "../../Pages/Loader/Loader.js";
import PagesNavigationUI from "../../Pages/PagesNavigationUI.js";
import Toaster from "../../Pages/Toaster/Toaster.js";
import { NavLink, useNavigate } from "react-router-dom";
import jwt from "jwt-decode";
import moment from "moment-timezone";
import Swal from "sweetalert2";
import "sweetalert2/dist/sweetalert2.css";

function OnBoardingInfo() {
  const [getContact, setGetContact] = useState([]);
  const [pageCount, setPageCount] = useState(0);
  const [totalRecords, setTotalRecords] = useState("");
  const [loader, setLoader] = useState({ isActive: null });
  const { isActive } = loader;
  const [toaster, setToaster] = useState({
    types: null,
    isShow: null,
    toasterBody: "",
    message: "",
  });
  const { types, isShow, toasterBody, message } = toaster;
  const [query, setQuery] = useState("");
  const [contactType, setContactType] = useState("private");
  const [contactDeleted, setContactDeleted] = useState("");
  const [results, setResults] = useState([]);
  const [currentPage, setCurrentPage] = useState(1);
  const [isLoading, setIsLoading] = useState(false);
  const [getBanner, setGetBanner] = useState("");

  useEffect(() => {
    window.scrollTo(0, 0);
    SearchGetAll(1);
  }, []);

  const SearchGetAll = async () => {
    setIsLoading(true);
    await user_service.SearchNotcompleteassociates(1).then((response) => {
      setIsLoading(false);
      if (response) {
        // setResults(response.data.data);
        console.log(response.data);
        setGetContact(response.data.data);
        setPageCount(Math.ceil(response.data.totalRecords / 10));
        setTotalRecords(response.data.totalRecords);
      }
    });
  };

  const handlePageClick = async (data) => {
    let currentPage = data.selected + 1;
    let skip = currentPage - 1 + currentPage;
    setLoader({ isActive: true });
    await user_service
      .SearchNotcompleteassociates(currentPage)
      .then((response) => {
        setLoader({ isActive: false });
        if (response) {
          // setResults(response.data.data);
          setGetContact(response.data.data);
          setPageCount(Math.ceil(response.data.totalRecords / 10));
          setTotalRecords(response.data.totalRecords);
        }
      });
  };

  {
    /* <!-- paginate Function Search Api call Start--> */
  }

  const getTimeDifference = (createdDate) => {
    const currentDate = new Date();
    const diffInMilliseconds = currentDate - new Date(createdDate);
    const diffInMinutes = Math.floor(diffInMilliseconds / 60000);
    const hours = Math.floor(diffInMinutes / 60);
    const days = Math.floor(hours / 24);
    const months = Math.floor(days / 30);
    const years = Math.floor(months / 12);

    if (years > 0) {
      return `${years} year${years > 1 ? "s" : ""} ago`;
    } else if (months > 0) {
      return `${months} month${months > 1 ? "s" : ""} ago`;
    } else if (days > 0) {
      return `${days} day${days > 1 ? "s" : ""} ago`;
    } else if (hours > 24) {
      const remainingHours = hours - Math.floor(hours / 24) * 24;
      return `1 day, ${remainingHours} hr${remainingHours > 1 ? "s" : ""} ago`;
    } else {
      return `${hours} hr${hours > 1 ? "s" : ""}, ${diffInMinutes % 60} min${
        diffInMinutes % 60 > 1 ? "s" : ""
      } ago`;
    }
  };


  const handlesetuppassword = async (id) => {
      console.log(id);

    const userData = {
      _id: id,
      agentId: jwt(localStorage.getItem("auth")).id,
    };
    console.log("userData", userData)
    try {
      setLoader({ isActive: true });
      const response = await user_service.onboardlink(userData);
      if (response) {
        setLoader({ isActive: false });
        setToaster({
          type: "Onboarding Link Sent Successfully",
          isShow: true,
          toasterBody: response.data.message,
          message: "Onboarding Link Sent Successfully",
        });
        setTimeout(() => {
          setToaster((prevToaster) => ({ ...prevToaster, isShow: false }));
          //  navigate(`/onboarding/${response.data.intranet_confirmationcode}/${response.data.intranet_uniqueid}`);
        }, 2000);
      } else {
        setLoader({ isActive: false });
        setToaster({
          types: "error",
          isShow: true,
          toasterBody: response.data.message,
          message: "Error",
        });
      }
    } catch (error) {
      setLoader({ isActive: false });
      setToaster({
        types: "error",
        isShow: true,
        toasterBody: error,
        message: "Error",
      });
    }
    setTimeout(() => {
      setToaster({
        types: "error",
        isShow: false,
        toasterBody: null,
        message: "Error",
      });
    }, 2000);
  };

  return (
    <div className="bg-secondary float-left w-100 pt-4 mb-4">
      <Loader isActive={isActive} />
      {isShow && (
        <Toaster
          types={types}
          isShow={isShow}
          toasterBody={toasterBody}
          message={message}
        />
      )}
      <div className="">
        <main className="page-wrapper homepage_layout">
          <div className="row mt-2">
            <div className="float-left w-100 mb-4">
              <h3 className="text-white pull-left mb-0">
                On Boarding Not Complete contact
              </h3>
            </div>
            <div className="col-md-12">
              {getContact && getContact.length > 0 ? (
                getContact.map((post) => (
                  <div className="card card-hover mb-2">
                    <div className="card-body">
                      <div className="float-left w-100">
                        <div className="float-left w-100 d-lg-flex d-md-flex d-inline-block align-items-center justify-content-between">
                          {/* <img className="d-none d-sm-block" src={Pic} width="100" alt="Resume picture" /> */}
                          <div
                            className="float-left w-100 d-flex align-items-center justify-content-start"
                            key={post._id}>
                            <img
                              height="70"
                              width="70"
                              className="rounded-circlee profile_picture"
                              src={post.image || Pic}
                              alt="Profile"
                            />
                            {/* <div className="ms-3" onClick={()=>handleSubmit(post._id)}> */}
                            <div className="ms-3">
                              <p className="mb-1">
                                <strong>{post.firstName}</strong>&nbsp;
                                <strong>{post.lastName}</strong>
                              </p>
                              {post?.phone ? (
                                <>
                                  <p className="mb-1">
                                    Phone: {post?.phone ?? ""}
                                  </p>
                                </>
                              ) : (
                                ""
                              )}
                              {post?.email ? (
                                <>
                                  <p className="mb-1">
                                    Email: {post?.email ?? ""}
                                  </p>
                                </>
                              ) : (
                                ""
                              )}
                            </div>
                          </div>
                          <div className="float-left w-100 d-flex align-items-center justify-content-between my-lg-0 my-md-0 my-sm-3 my-3">
                            <badge
                              className="ms-sm-0 ms-0"
                              style={{ fontSize: "14px" }}
                            >
                              {post.contactType
                                ? post.contactType.charAt(0).toUpperCase() +
                                  post.contactType.slice(1)
                                : "Not Assigned"}
                            </badge>

                            {localStorage.getItem("auth") &&
                            jwt(localStorage.getItem("auth")).contactType ==
                              "admin" ? (
                              <>
                                {(post.onboardfinal != "done" ||
                                  !post.onboard_approve) && (
                                  <button
                                      className="btn btn-primary px-lg-3 px-md-2 px-sm-2 px-sm-0 mx-2"
                                    onClick={()=>handlesetuppassword(post._id)}
                                  >
                                    Send Onboarding Request
                                  </button>
                                )}
                              </>
                            ) : (
                              ""
                            )}

                            <span className="">
                              <NavLink
                                to={`/contact-profile/${post._id}`}
                                className=""
                              >
                                <i className="fi-info-circle fs-5 mt-n1 me-2"></i>
                              </NavLink>
                            </span>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                ))
              ) : (
                <div className="text-center mb-3">
                  <img className="" src={Pic} />
                  <br />
                  <p className="mt-3 text-white">Not Any Associate.</p>
                </div>
              )}

              {pageCount > 1 ? (
                <div className="d-flex align-items-center justify-content-end mb-0 mt-4">
                  <span className="float-left w-100 text-white">
                    <b>Total Matches : {totalRecords}</b>
                  </span>
                  <ReactPaginate
                    className=""
                    previousLabel={"Previous"}
                    nextLabel={"next"}
                    breakLabel={"..."}
                    pageCount={pageCount}
                    marginPagesDisplayed={1}
                    pageRangeDisplayed={2}
                    onPageChange={handlePageClick}
                    containerClassName={
                      "pagination justify-content-center mb-0"
                    }
                    pageClassName={"page-item"}
                    pageLinkClassName={"page-link"}
                    previousClassName={"page-item"}
                    previousLinkClassName={"page-link"}
                    nextClassName={"page-item"}
                    nextLinkClassName={"page-link"}
                    breakClassName={"page-item"}
                    breakLinkClassName={"page-link"}
                    activeClassName={"active"}
                  />
                </div>
              ) : (
                ""
              )}

              {/* <!-- Item--> */}

              {/* <!-- Item--> */}
            </div>
          </div>
        </main>
      </div>
    </div>
  );
}
export default OnBoardingInfo;

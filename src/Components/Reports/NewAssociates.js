import React, { useState, useEffect } from "react";
import Pic from "../img/pic.png";
// import Modal from "../../Modal";
import user_service from "../service/user_service";
import ReactPaginate from "react-paginate";
import Loader from "../../Pages/Loader/Loader.js";
import Toaster from "../../Pages/Toaster/Toaster.js";
import { NavLink, useNavigate } from "react-router-dom";
import Birthday from "../img/birthday.jpg";
import moment from "moment-timezone";


function NewAssociates() {
  const [getContact, setGetContact] = useState([]);
  const [pageCount, setPageCount] = useState(0);
  const [totalRecords, setTotalRecords] = useState("");
  const [loader, setLoader] = useState({ isActive: null });
  const { isActive } = loader;
  const [toaster, setToaster] = useState({
    types: null,
    isShow: null,
    toasterBody: "",
    message: "",
  });
  const { types, isShow, toasterBody, message } = toaster;

  const [query, setQuery] = useState("");
  const [select, setSelect] = useState("");
  const [results, setResults] = useState([]);
  const [currentPage, setCurrentPage] = useState(1);
  const [isLoading, setIsLoading] = useState(false);

  // paginate Function
  const [newAssociates, setNewAssociates] = useState("");
  useEffect(() => {
    window.scrollTo(0, 0);
    // const ContactGetAll = async () => {
    //     setLoader({ isActive: true })
    //     await user_service.contactGetassoc(1,"associate").then((response) => {
    //         setLoader({ isActive: false })
    //         if (response) {
    //             setGetContact(response.data.data);
    //             setPageCount(Math.ceil(response.data.totalRecords / 10));
    //         }
    //     });
    // }
    // ContactGetAll()

    const homesidenewassociates = async () => {
      setLoader({ isActive: true });
      await user_service.newassociates(1).then((response) => {
        setLoader({ isActive: false });
        if (response) {
          // console.log(response.data);
          setNewAssociates(response.data.data);
          setPageCount(Math.ceil(response.data.totalRecords / 10));
        }
      });
    };

    homesidenewassociates();
  }, []);

  const handlePageClick = async (data) => {
    let currentPage = data.selected + 1;
    let skip = currentPage - 1 + currentPage;
    setLoader({ isActive: true });
    console.log(currentPage);
    await user_service.newassociates(currentPage).then((response) => {
      setLoader({ isActive: false });
      if (response) {
        // setResults(response.data.data);
        setNewAssociates(response.data.data);
        setPageCount(Math.ceil(response.data.totalRecords / 10));
      }
    });
  };

  {
    /* <!-- paginate Function Search Api call Start--> */
  }
  useEffect(() => {
    window.scrollTo(0, 0);
    if (query) {
      SearchGetAll(1);
    }
  }, [query]);

  const SearchGetAll = async () => {
    setIsLoading(true);
    await user_service
      .SearchContactGet(1, query, query, query)
      .then((response) => {
        setIsLoading(false);
        if (response) {
          // setResults(response.data.data);
          setGetContact(response.data.data);
          setPageCount(Math.ceil(response.data.totalRecords / 10));
        }
      });
  };

  const navigate = useNavigate();
  const handleSubmit = (id) => {
    console.log(id);
    navigate(`/contact-profile/${id}`);
  };

  {
    /* <!-- paginate Function Search Api call End--> */
  }

  const handleSearch = (event) => {
    event.preventDefault();
    setCurrentPage(1);
    SearchGetAll(1);
  };

  // Helper function to format date as "Day, Month Day" (e.g., "Friday, September 21")
  function getFormattedDate(dateString) {
    const options = { weekday: "long", month: "long", day: "numeric" };
    const date = new Date(dateString);
    return date.toLocaleDateString("en-US", options);
  }

  function getFormattedDateFormat(dateString) {
    const options = { year: "numeric", month: "2-digit", day: "2-digit" };
    const date = new Date(dateString);
    return date.toLocaleDateString("en-US", options);
  }
  return (
    <div className="bg-secondary float-left w-100 pt-4 mb-4">
      <Loader isActive={isActive} />
      {isShow && (
        <Toaster
          types={types}
          isShow={isShow}
          toasterBody={toasterBody}
          message={message}
        />
      )}
      <main className="page-wrapper">
        {/* <!-- Page container--> */}
        <div className="content-overlay">
          {/* <!-- Breadcrumb--> */}
          {/* <nav className="mb-3 mb-md-4 pt-md-3" aria-label="Breadcrumb">
                        <ol className="breadcrumb breadcrumb-dark">
                            <li className="breadcrumb-item"><a href="/">Home</a></li>
                            <li className="breadcrumb-item active" aria-current="page">New Associates </li>
                        </ol>
                    </nav> */}

          {/* <!-- Page card like wrapper--> */}
          <div className="">
            <div className="row">
              {/* List of resumes--> */}
              <div className="col-md-12">
                <div className="float-left w-100 d-flex align-items-center justify-content-between mb-4">
                  <h3 className="text-white mb-0">New Associates</h3>
                </div>
                {/* <!-- Normal search form group --> */}
                {/* <form onSubmit={handleSearch}>
                                    <p className="mt-5">Quick Search: <a href="">Show All</a></p>
                                    <div className="d-flex justify-content-start align-items-center">
                                        <div className="form-outline">
                                            <input className="form-control" type="search" id="search-input-1" placeholder="Search All"
                                                valvalue={query} onChange={(event) => setQuery(event.target.value)} />
                                        </div>
                                        <span className="ms-4">
                                            <button type="button" className="btn btn-translucent-primary">Find</button>
                                            <a href="#" id="button-item" type="button">Reset</a>
                                        </span>

                                    </div>
                                </form> */}

                {/* <!-- Item--> */}
                {newAssociates && newAssociates.length > 0 ? (
                  newAssociates.map((post) => (
                    <>
                      {post.contact_status === "active" ? (
                        <div className="card card-hover mb-2">
                          <div className="card-body">
                            <div className="float-left w-100">
                              <div className="float-left w-100 d-lg-flex d-md-flex d-inline-block align-items-center justify-content-between">
                                {/* <img className="d-none d-sm-block" src={Pic} width="100" alt="Resume picture" /> */}
                                <div
                                  className="float-left w-100 d-flex align-items-center justify-content-start"
                                  key={post._id}
                                >
                                  <img
                                    height="70"
                                    width="70"
                                    className="rounded-circlee profile_picture"
                                    src={post.image || Pic}
                                    alt="Profile"
                                  />
                                  {/* <div className="ms-3" onClick={()=>handleSubmit(post._id)}> */}
                                  <div className="ms-3">
                                    <p className="mb-1">
                                      <strong>{post.firstName}</strong>&nbsp;
                                      <strong>{post.lastName}</strong>
                                    </p>
                                    {post?.phone ? (
                                      <>
                                        <p className="mb-1">
                                          Phone: {post?.phone ?? ""}
                                        </p>
                                      </>
                                    ) : (
                                      ""
                                    )}
                                    {post?.email ? (
                                      <>
                                        <p className="mb-1">
                                          Email: {post?.email ?? ""}
                                        </p>
                                      </>
                                    ) : (
                                      ""
                                    )}
                                    {post.additionalActivateFields
                                      .admin_note ? (
                                      <>
                                        <p className="mb-1">
                                          Transfer From:{" "}
                                          {post.additionalActivateFields
                                            .admin_note
                                            ? post.additionalActivateFields
                                                .admin_note
                                            : ""}
                                        </p>
                                      </>
                                    ) : (
                                      ""
                                    )}
                                    {post.additionalActivateFields
                                      .office_affiliation ? (
                                      <>
                                        <p className="mb-1">
                                          Office Affiliation:{" "}
                                          {post.additionalActivateFields
                                            .office_affiliation
                                            ? post.additionalActivateFields
                                                .office_affiliation
                                            : ""}
                                        </p>
                                      </>
                                    ) : (
                                      ""
                                    )}
                                  </div>
                                </div>
                                <div className="float-left w-100 d-flex align-items-center justify-content-between my-lg-0 my-md-0 my-sm-3 my-3">
                                  <p className="mb-0">
                                    Associate Since:{" "}
                                    {post.additionalActivateFields
                                      .associate_since
                                      ? getFormattedDateFormat(
                                          post.additionalActivateFields
                                            .associate_since
                                        )
                                      : getFormattedDateFormat(
                                        post.timeStamp
                                      )}
                                  </p>

                                  <span className="">
                                    <NavLink
                                      to={`/contact-profile/${post._id}`}
                                      className=""
                                    >
                                      <i className="fi-info-circle fs-5 mt-n1 me-2"></i>
                                    </NavLink>
                                  </span>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      ) : (
                        ""
                      )}
                    </>
                  ))
                ) : (
                  <div className="text-center mb-3">
                    <img className="" src={Pic} />
                    <br />
                    <p className="mt-3 text-white">Not Any New Associate.</p>
                  </div>
                )}
              </div>
              {pageCount && pageCount > 1 ? (
                <div className="d-flex align-items-center justify-content-end mb-0 mt-4">
                  <ReactPaginate
                    className=""
                    previousLabel={"Previous"}
                    nextLabel={"Next"}
                    breakLabel={"..."}
                    pageCount={pageCount}
                    marginPagesDisplayed={1}
                    pageRangeDisplayed={2}
                    onPageChange={handlePageClick}
                    containerClassName={
                      "pagination justify-content-center mb-0"
                    }
                    pageClassName={"page-item"}
                    pageLinkClassName={"page-link"}
                    previousClassName={"page-item"}
                    previousLinkClassName={"page-link"}
                    nextClassName={"page-item"}
                    nextLinkClassName={"page-link"}
                    breakClassName={"page-item"}
                    breakLinkClassName={"page-link"}
                    activeClassName={"active"}
                  />
                </div>
              ) : (
                ""
              )}
            </div>
          </div>
        </div>
      </main>
    </div>
  );
}
export default NewAssociates;

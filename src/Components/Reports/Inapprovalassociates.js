import React, { useState, useEffect } from "react";
import Pic from "../img/pic.png";
// import Modal from "../../Modal";
import user_service from "../service/user_service";
import ReactPaginate from "react-paginate";
import Loader from "../../Pages/Loader/Loader.js";
import PagesNavigationUI from "../../Pages/PagesNavigationUI.js";
import Toaster from "../../Pages/Toaster/Toaster.js";
import { NavLink, useNavigate } from "react-router-dom";
import jwt from "jwt-decode";
import moment from "moment-timezone";
import Swal from "sweetalert2";
import "sweetalert2/dist/sweetalert2.css";

function Inapprovalassociates() {
  const [getContact, setGetContact] = useState([]);
  const [pageCount, setPageCount] = useState(0);
  const [totalRecords, setTotalRecords] = useState("");
  const [loader, setLoader] = useState({ isActive: null });
  const { isActive } = loader;
  const [toaster, setToaster] = useState({
    types: null,
    isShow: null,
    toasterBody: "",
    message: "",
  });
  const { types, isShow, toasterBody, message } = toaster;
  const [query, setQuery] = useState("");
  const [contactType, setContactType] = useState("private");
  const [contactDeleted, setContactDeleted] = useState("");
  const [results, setResults] = useState([]);
  const [currentPage, setCurrentPage] = useState(1);
  const [isLoading, setIsLoading] = useState(false);
  const [getBanner, setGetBanner] = useState("");

  useEffect(() => {
    window.scrollTo(0, 0);
    SearchGetAll(1);
  }, []);

  const handlePageClick = async (data) => {
    let currentPage = data.selected + 1;
    let skip = currentPage - 1 + currentPage;
    setLoader({ isActive: true });
    await user_service
      .SearchInapprovalassociates(currentPage)
      .then((response) => {
        setLoader({ isActive: false });
        if (response) {
          // setResults(response.data.data);
          setGetContact(response.data.data);
          setPageCount(Math.ceil(response.data.totalRecords / 10));
          setTotalRecords(response.data.totalRecords);
        }
      });
  };

  {
    /* <!-- paginate Function Search Api call Start--> */
  }
  useEffect(() => {
    window.scrollTo(0, 0);
    if (query) {
      SearchGetAll(1);
      SearchGetAllNew();
    }
  }, [query]);

  const SearchGetAll = async () => {
    setIsLoading(true);
    await user_service.SearchInapprovalassociates(0).then((response) => {
      setIsLoading(false);
      if (response) {
        // setResults(response.data.data);
        console.log(response.data);
        setGetContact(response.data.data);
        setPageCount(Math.ceil(response.data.totalRecords / 10));
        setTotalRecords(response.data.totalRecords);
      }
    });
  };

  const SearchGetAllNew = async () => {
    setIsLoading(true);
    await user_service.SearchInapprovalassociates(0, query).then((response) => {
      setIsLoading(false);
      if (response) {
        setResults(response.data.data);
        setPageCount(Math.ceil(response.data.totalRecords / 10));
      }
    });
  };

  const navigate = useNavigate();
  const handleSubmit = (id, contactType) => {
    console.log(contactType);
    if (contactType === "private") {
      navigate(`/private-contact/${id}`);
    } else {
      navigate(`/contact-profile/${id}`);
    }
  };

  const handleSearch = (event) => {
    event.preventDefault();
    setCurrentPage(1);
    SearchGetAll(1);
  };

  const handleDeleteContact = async (id, status) => {
    console.log(id);
    console.log(status);

    if (id) {
      const userData = {
        contact_status: status ? "deleted" : status,
      };
      console.log(userData);
      setLoader({ isActive: true });
      await user_service.contactUpdate(id, userData).then((response) => {
        if (response) {
          setLoader({ isActive: false });
          setContactDeleted(response.data);
          SearchGetAll();
        } else {
          setLoader({ isActive: false });
          setToaster({
            type: "error",
            isShow: true,
            toasterBody: response.data.message,
            message: "Error",
          });

          setTimeout(() => {
            setToaster((prevToaster) => ({
              ...prevToaster,
              isShow: false,
            }));
          }, 3000);
        }
      });
    }
  };

  const handleApproveContact = async (id, status) => {
    Swal.fire({
      title: "Are you sure?",
      text:
        status == "active" ? "You want to Approved." : "You want to Reject.",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText:
        status == "active" ? "Yes, Approve it!" : "Yes, Reject it!",
    }).then(async (result) => {
      if (result.isConfirmed) {
        try {
          if (id) {
            const userData = {
              contact_status: status,
              onboard_approve: status == "active" ? "yes" : "no",
            };

            var msg;
            if (status == "active") {
              msg = "Contact Approved Successfully.";
            } else {
              msg = "Contact Rejected Successfully.";
            }
            console.log(userData);
            setLoader({ isActive: true });
            await user_service.contactUpdate(id, userData).then((response) => {
              if (response) {
                setLoader({ isActive: false });

                // setToaster({
                //   type: "Approve",
                //   isShow: true,
                //   //toasterBody: msg,
                //   message: msg,
                // });
                // setTimeout(() => {
                //   setToaster((prevToaster) => ({
                //     ...prevToaster,
                //     isShow: false,
                //   }));
                // }, 3000);

                Swal.fire("Done!", msg, "success");

                SearchGetAll();
              } else {
                // setLoader({ isActive: false });
                // setToaster({
                //   type: "error",
                //   isShow: true,
                //   toasterBody: response.data.message,
                //   message: "Error",
                // });

                // setTimeout(() => {
                //   setToaster((prevToaster) => ({
                //     ...prevToaster,
                //     isShow: false,
                //   }));
                // }, 3000);

                Swal.fire("Error!", response.data.messagesg, "error");
              }
            });
          }
        } catch (error) {
          Swal.fire("Error", "An error occurred.", "error");

          console.error(error);
        }
      }
    });
  };

  const handleApproveContactagainonboard = async (id, status) => {
    Swal.fire({
      title:
        status == "active"
          ? "Are you sure? You want to Approved."
          : "Are you sure? You want to Reject.",
      // text: status == "active" ? "You want to Approved." : "You want to Reject.",
      text:
        status == "active"
          ? ""
          : "After reject it , contact can now fill onboard details again.",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText:
        status == "active" ? "Yes, Approve it!" : "Yes, Reject it!",
    }).then(async (result) => {
      if (result.isConfirmed) {
        try {
          if (id) {
            // const userData = {
            //   contact_status: status,
            //   onboard_approve: status == "active" ? "yes" : "no",
            // };

            let userData = {
              contact_status: status,
              onboard_approve: status == "active" ? "yes" : "no",
            };

            if (status !== "active") {
              userData.onboardfinal = "";
            }

            var msg;
            if (status == "active") {
              msg = "Contact Approved Successfully.";
            } else {
              msg = "Contact Rejected Successfully.";
            }
            console.log(userData);
            setLoader({ isActive: true });
            await user_service.contactUpdate(id, userData).then((response) => {
              if (response) {
                setLoader({ isActive: false });

                // setToaster({
                //   type: "Approve",
                //   isShow: true,
                //   //toasterBody: msg,
                //   message: msg,
                // });
                // setTimeout(() => {
                //   setToaster((prevToaster) => ({
                //     ...prevToaster,
                //     isShow: false,
                //   }));
                // }, 3000);

                Swal.fire("Done!", msg, "success");

                SearchGetAll();
              } else {
                // setLoader({ isActive: false });
                // setToaster({
                //   type: "error",
                //   isShow: true,
                //   toasterBody: response.data.message,
                //   message: "Error",
                // });

                // setTimeout(() => {
                //   setToaster((prevToaster) => ({
                //     ...prevToaster,
                //     isShow: false,
                //   }));
                // }, 3000);

                Swal.fire("Error!", response.data.messagesg, "error");
              }
            });
          }
        } catch (error) {
          Swal.fire("Error", "An error occurred.", "error");

          console.error(error);
        }
      }
    });
  };

  const getTimeDifference = (createdDate) => {
    const currentDate = new Date();
    const diffInMilliseconds = currentDate - new Date(createdDate);
    const diffInMinutes = Math.floor(diffInMilliseconds / 60000);
    const hours = Math.floor(diffInMinutes / 60);
    const days = Math.floor(hours / 24);
    const months = Math.floor(days / 30);
    const years = Math.floor(months / 12);

    if (years > 0) {
      return `${years} year${years > 1 ? "s" : ""} ago`;
    } else if (months > 0) {
      return `${months} month${months > 1 ? "s" : ""} ago`;
    } else if (days > 0) {
      return `${days} day${days > 1 ? "s" : ""} ago`;
    } else if (hours > 24) {
      const remainingHours = hours - Math.floor(hours / 24) * 24;
      return `1 day, ${remainingHours} hr${remainingHours > 1 ? "s" : ""} ago`;
    } else {
      return `${hours} hr${hours > 1 ? "s" : ""}, ${diffInMinutes % 60} min${
        diffInMinutes % 60 > 1 ? "s" : ""
      } ago`;
    }
  };

 
  const handleApprovePaperwork = async (pid, pstatus) => {
    try {
      const userData = {
        approvalStatus: pstatus,
      };

      setLoader({ isActive: true });
      await user_service
        .agentpaperworkDocumentUpdate(pid, userData)
        .then((response) => {
          if (response) {
            // ContactGetById(params.id);
            SearchGetAll(1);
            setLoader({ isActive: false });

            setToaster({
              type: "Document Updated Successfully",
              isShow: true,
              toasterBody: response.data.message,
              message: "Document Updated Successfully",
            });
            setTimeout(() => {
              setToaster((prevToaster) => ({ ...prevToaster, isShow: false }));
            }, 3000);
          } else {
            setLoader({ isActive: false });
            setToaster({
              type: "error",
              isShow: true,
              toasterBody: response.data.message,
              message: "Error",
            });

            setTimeout(() => {
              setToaster((prevToaster) => ({ ...prevToaster, isShow: false }));
            }, 3000);
          }
        });
    } catch (error) {
      setLoader({ isActive: false });
      setToaster({
        types: "error",
        isShow: true,
        toasterBody: error.response
          ? error.response.data.message
          : error.message,
        message: "Error",
      });

      setTimeout(() => {
        setToaster((prevToaster) => ({ ...prevToaster, isShow: false }));
      }, 2000);
    }
  };

  const handlesignagain = async (pid, uid) => {
    const userData = {
      _id: uid,
      paperworkid: pid,
      //agentId: jwt(localStorage.getItem("auth")).id,
    };
    console.log("userData", userData);
    try {
      setLoader({ isActive: true });
      const response = await user_service.onboardlinkresubmit(userData);
      if (response) {
        SearchGetAll(1);
        setLoader({ isActive: false });
        setToaster({
          type: "Onboarding Link Sent Successfully",
          isShow: true,
          toasterBody: response.data.message,
          message: "Onboarding Link Sent Successfully",
        });
        setTimeout(() => {
          setToaster((prevToaster) => ({ ...prevToaster, isShow: false }));
        }, 2000);
      } else {
        setLoader({ isActive: false });
        setToaster({
          types: "error",
          isShow: true,
          toasterBody: response.data.message,
          message: "Error",
        });
      }
    } catch (error) {
      setLoader({ isActive: false });
      setToaster({
        types: "error",
        isShow: true,
        toasterBody: error,
        message: "Error",
      });
    }
    setTimeout(() => {
      setToaster({
        types: "error",
        isShow: false,
        toasterBody: null,
        message: "Error",
      });
    }, 2000);
  };

  const profilepic = (e) => {
    e.target.src = Pic;
  };
  return (
    <div className="bg-secondary float-left w-100 pt-4 mb-4">
      <Loader isActive={isActive} />
      {isShow && (
        <Toaster
          types={types}
          isShow={isShow}
          toasterBody={toasterBody}
          message={message}
        />
      )}
      <div className="">
        <main className="page-wrapper homepage_layout">
          {/* <Modal /> */}
          {/* <!-- Page container--> */}
          {/* <!-- Breadcrumb--> */}
          {/* <nav className="mb-3 mb-md-4 pt-md-3" aria-label="Breadcrumb">
            <ol className="breadcrumb breadcrumb-dark">
              <li className="breadcrumb-item">
                <a href="/">Home</a>
              </li>
              <li className="breadcrumb-item">
                <a href="/">Account</a>
              </li>
              <li className="breadcrumb-item active" aria-current="page">
                My Contact
              </li>
            </ol>
          </nav> */}

          <div className="row mt-2">
            {/* <!-- List of resumes--> */}
            {/* <PagesNavigationUI /> */}
            <div className="col-md-12">
              <div className="float-left w-100 mb-4">
                <h3 className="text-white pull-left mb-0">
                  On Boarding Associates Under Approval
                </h3>
                <NavLink
                  to="/add-contact"
                  type="button"
                  className="btn btn-info mb-2 pull-right"
                >
                  Add a Contact
                </NavLink>

                <hr className="mb-3 w-100" />
              </div>

              {/* <!-- Normal search form group --> */}
              {/* <form onSubmit={handleSearch}>

                <div className="d-flex justify-content-start align-items-center">
                  <div className="form-outline">
                    <input
                      className="form-control"
                      type="text"
                      id="search-input-1"
                      placeholder="Search All"
                      valvalue={query}
                      onChange={(event) => setQuery(event.target.value)}
                    />
                  </div>
                </div>
              </form> */}

              {pageCount > 1 ? (
                <div className="d-flex align-items-center justify-content-end mb-0 mt-4">
                  <span className="float-left w-100 text-white">
                    <b>Total Matches : {totalRecords}</b>
                  </span>
                  <ReactPaginate
                    className=""
                    previousLabel={"Previous"}
                    nextLabel={"next"}
                    breakLabel={"..."}
                    pageCount={pageCount}
                    marginPagesDisplayed={1}
                    pageRangeDisplayed={2}
                    onPageChange={handlePageClick}
                    containerClassName={
                      "pagination justify-content-center mb-0"
                    }
                    pageClassName={"page-item"}
                    pageLinkClassName={"page-link"}
                    previousClassName={"page-item"}
                    previousLinkClassName={"page-link"}
                    nextClassName={"page-item"}
                    nextLinkClassName={"page-link"}
                    breakClassName={"page-item"}
                    breakLinkClassName={"page-link"}
                    activeClassName={"active"}
                  />
                </div>
              ) : (
                ""
              )}

              {/* <!-- Item--> */}
              {getContact && getContact.length > 0 ? (
                getContact.map((post) => (
                  <div className="accordion mt-4" id={`a_${post._id}`}>
                    <div className="accordion-item border rounded-3 p-3">
                      <h2
                        className="accordion-header"
                        id={`headingOne${post._id}`}
                      >
                        <button
                          className="accordion-button"
                          type="button"
                          data-bs-toggle="collapse"
                          data-bs-target={`#collapse${post._id}`}
                          aria-expanded="true"
                          aria-controls={`collapse${post._id}`}
                        >
                          <div className="card border-0 w-100">
                            <div className="card-body w-100 p-0">
                              <div className="float-left w-100">
                                <div className="inapprovalassociates float-left w-100 d-flex align-items-center justify-content-between">
                                  {/* <img className="d-none d-sm-block" src={Pic} width="100" alt="Resume picture" /> */}
                                  <div className="float-left w-100 d-flex align-items-center justify-content-start">
                                    <img
                                      className="rounded-circle profile_picture"
                                      onClick={() =>
                                        handleSubmit(post._id, post.contactType)
                                      }
                                      src={post.image || Pic}
                                      alt="Profile"
                                      onError={(e) => profilepic(e)}
                                    />
                                    {console.log(post.contactType)}
                                    <div
                                      className="ms-3 float-left w-100"
                                      //onClick={() => handleSubmit(post._id, post.contactType)}
                                    >
                                      <strong>{post.firstName}</strong> &nbsp;
                                      <strong>{post.lastName}</strong>
                                      <p className="my-1">{post.company}</p>
                                      <p className="mb-1">{post.phone}</p>
                                      <p className="my-1">
                                        <NavLink>
                                          <i
                                            className="fa fa-envelope"
                                            aria-hidden="true"
                                          >
                                            &nbsp; {post.email}
                                          </i>
                                        </NavLink>
                                      </p>
                                      <p className="mb-1">
                                        Tax Certificate:&nbsp;
                                        {post.onboard_taxcertification ? (
                                          <a
                                            href={post.onboard_taxcertification}
                                            target="_blank"
                                          >
                                            <span className=" badge bg-success">
                                              View
                                            </span>
                                          </a>
                                        ) : (
                                          "Not Set"
                                        )}
                                      </p>
                                      <p className="mb-1">
                                        Agreement:&nbsp;
                                        {post.onboard_signature ? (
                                          <a
                                            href={post.onboard_signature}
                                            target="_blank"
                                          >
                                            <span className=" badge bg-success">
                                              View
                                            </span>
                                          </a>
                                        ) : (
                                          "Not Set"
                                        )}
                                      </p>
                                    </div>
                                  </div>

                                  <div className="pull-right tags-badge d-flex align-items-center justify-content-end w-100 me-4">
                                    {/* <p className="mb-0">
                                        <badge
                                          // className="mt-3"
                                          style={{ fontSize: "14px" }}
                                        >
                                          {post.contactType
                                            ? post.contactType.charAt(0).toUpperCase() +
                                              post.contactType.slice(1)
                                            : "Not Assigned"}
                                        </badge>
                                      </p> */}

                                    {localStorage.getItem("auth") &&
                                    jwt(localStorage.getItem("auth"))
                                      .contactType == "admin" &&
                                    post.onboardfinal === "done" ? (
                                      <>
                                        <span className="ms-4">
                                          <button
                                            type="button"
                                            onClick={() =>
                                              handleApproveContact(
                                                post._id,
                                                "active"
                                              )
                                            }
                                            className="btn btn-translucent-primary"
                                          >
                                            Approve
                                          </button>
                                        </span>

                                        <span className="ms-4">
                                          <button
                                            type="button"
                                            onClick={() =>
                                              handleApproveContact(
                                                post._id,
                                                "inapproval"
                                              )
                                            }
                                            className="btn btn-translucent-primary"
                                          >
                                            Reject
                                          </button>
                                        </span>
                                      </>
                                    ) : (
                                      ""
                                    )}
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </button>
                      </h2>
                      <div
                        className="accordion-collapse collapse"
                        aria-labelledby={`headingOne${post._id}`}
                        data-bs-parent={`#a_${post._id}`}
                        id={`collapse${post._id}`}
                      >
                        <div className="accordion-body">
                          <div className="bg-light rounded-3 mb-3">
                            <h2 className="h4 mb-4">Contact</h2>
                            <div className="row">
                              <div className="col-sm-6 mb-3 d-flex align-items-center justify-content-start">
                                <label
                                  className="col-form-label p-0 me-3"
                                  for="pr-fn"
                                >
                                  Name :
                                </label>
                                <p className="mb-0">
                                  {post.firstName ? post.firstName : ""}
                                  {post.firstName ? (
                                    <i className="fi-check text-success ms-2"></i>
                                  ) : (
                                    <img
                                      className="ms-2"
                                      src="https://cdn.pboffice.net/shared/transaction/page_select_drop.png"
                                    ></img>
                                  )}
                                </p>
                              </div>

                              <div className="col-sm-6 mb-3 d-flex align-items-center justify-content-start">
                                <label
                                  className="col-form-label p-0 me-3"
                                  for="pr-fn"
                                >
                                  Mobile :
                                </label>
                                <p className="mb-0">
                                  {post.phone ? post.phone : ""}
                                  {post.phone ? (
                                    <i className="fi-check text-success ms-2"></i>
                                  ) : (
                                    <img
                                      className="ms-2"
                                      src="https://cdn.pboffice.net/shared/transaction/page_select_drop.png"
                                    ></img>
                                  )}
                                </p>
                              </div>

                              <div className="col-sm-6 mb-3 d-flex align-items-center justify-content-start">
                                <label
                                  className="col-form-label p-0 me-3"
                                  for="pr-fn"
                                >
                                  Date of Birth :
                                </label>
                                <p className="mb-0">
                                  {post.birthday
                                    ? moment
                                        .tz(post.birthday, "US/Mountain")
                                        .format("M/D/YYYY")
                                    : ""}
                                  {post.birthday ? (
                                    <i className="fi-check text-success ms-2"></i>
                                  ) : (
                                    <img
                                      className="ms-2"
                                      src="https://cdn.pboffice.net/shared/transaction/page_select_drop.png"
                                    ></img>
                                  )}
                                </p>
                              </div>

                              <div className="col-sm-6 mb-3 d-flex align-items-center justify-content-start">
                                <label
                                  className="col-form-label p-0 me-3"
                                  for="pr-fn"
                                >
                                  Recruited By :
                                </label>
                                <p className="mb-0">
                                  {post?.additionalActivateFields
                                    ?.staff_recruiter
                                    ? post?.additionalActivateFields
                                        ?.staff_recruiter
                                    : ""}
                                  {post?.additionalActivateFields
                                    ?.staff_recruiter ? (
                                    <i className="fi-check text-success ms-2"></i>
                                  ) : (
                                    <img
                                      className="ms-2"
                                      src="https://cdn.pboffice.net/shared/transaction/page_select_drop.png"
                                    ></img>
                                  )}
                                </p>
                              </div>
                            </div>
                          </div>

                          <div className="bg-light rounded-3 mb-3">
                            <h2 className="h4 mb-4">License</h2>
                            <div className="row">
                              <div className="col-sm-6 mb-3 d-flex align-items-center justify-content-start">
                                <label
                                  className="col-form-label p-0 me-3"
                                  for="pr-fn"
                                >
                                  Licensed Type :
                                </label>
                                <p className="mb-0">
                                  {post?.additionalActivateFields?.license_type
                                    ? post?.additionalActivateFields
                                        ?.license_type
                                    : ""}
                                  {post?.additionalActivateFields
                                    ?.license_type ? (
                                    <i className="fi-check text-success ms-2"></i>
                                  ) : (
                                    <img
                                      className="ms-2"
                                      src="https://cdn.pboffice.net/shared/transaction/page_select_drop.png"
                                    ></img>
                                  )}
                                </p>
                              </div>

                              <div className="col-sm-6 mb-3 d-flex align-items-center justify-content-start">
                                <label
                                  className="col-form-label p-0 me-3"
                                  for="pr-fn"
                                >
                                  Licensed Number :
                                </label>
                                <p className="mb-0">
                                  {post?.additionalActivateFields
                                    ?.license_number
                                    ? post?.additionalActivateFields
                                        ?.license_number
                                    : ""}
                                  {post?.additionalActivateFields
                                    ?.license_number ? (
                                    <i className="fi-check text-success ms-2"></i>
                                  ) : (
                                    <img
                                      className="ms-2"
                                      src="https://cdn.pboffice.net/shared/transaction/page_select_drop.png"
                                    ></img>
                                  )}
                                </p>
                              </div>

                              <div className="col-sm-6 mb-3 d-flex align-items-center justify-content-start">
                                <label
                                  className="col-form-label p-0 me-3"
                                  for="pr-fn"
                                >
                                  Licensed Expires :
                                </label>
                                <p className="mb-0">
                                  {post?.additionalActivateFields?.date_expire
                                    ? post?.additionalActivateFields
                                        ?.date_expire
                                    : ""}
                                  {post?.additionalActivateFields
                                    ?.date_expire ? (
                                    <i className="fi-check text-success ms-2"></i>
                                  ) : (
                                    <img
                                      className="ms-2"
                                      src="https://cdn.pboffice.net/shared/transaction/page_select_drop.png"
                                    ></img>
                                  )}
                                </p>
                              </div>

                              <div className="col-sm-6 mb-3 d-flex align-items-center justify-content-start">
                                <label
                                  className="col-form-label p-0 me-3"
                                  for="pr-fn"
                                >
                                  Licensed Issue State :
                                </label>
                                <p className="mb-0">
                                  {post?.additionalActivateFields?.license_state
                                    ? post?.additionalActivateFields
                                        ?.license_state
                                    : ""}
                                  {post?.additionalActivateFields
                                    ?.license_state ? (
                                    <i className="fi-check text-success ms-2"></i>
                                  ) : (
                                    <img
                                      className="ms-2"
                                      src="https://cdn.pboffice.net/shared/transaction/page_select_drop.png"
                                    ></img>
                                  )}
                                </p>
                              </div>
                            </div>
                          </div>

                          <div className="bg-light rounded-3 mb-3">
                            <h2 className="h4 mb-4">Signed Documents</h2>
                            <div className="row">
                              {/* <label className="col-form-label p-0 me-3" for="pr-fn">Licensed Type :</label>
                                      <p className="mb-0">{post?.additionalActivateFields?.license_type ? post?.additionalActivateFields?.license_type :""} 
                                      {
                                        post?.additionalActivateFields?.license_type ?
                                        <i className="fi-check text-success ms-2"></i>
                                        :
                                        <img className="ms-2" src="https://cdn.pboffice.net/shared/transaction/page_select_drop.png"></img>
                                      }
                                      </p> */}

                              {post.contact_paperwork &&
                              post.contact_paperwork.length > 0 ? (
                                post.contact_paperwork.map((item) => (
                                  <div className="col-sm-12 mb-3 d-flex align-items-center justify-content-start">
                                    <p key={item._id}>
                                      {/* {item
                                                          .contact_paperworkdocument[0]
                                                          .approvalStatus ===
                                                        "yes" ? (
                                                          <i
                                                            className="fa fa-check"
                                                            aria-hidden="true"
                                                          ></i>
                                                        ) : (
                                                          <i
                                                            className="fa fa-times"
                                                            aria-hidden="true"
                                                          ></i>
                                                        )}
                                                        &nbsp; */}
                                      <b>{item.paperwork_title}</b> -{" "}
                                      <a
                                        href={
                                          item.contact_paperworkdocument[0]
                                            .documenturl
                                        }
                                        target="_blank"
                                        rel="noopener noreferrer"
                                        className="btn btn-translucent-primary"
                                      >
                                        View File
                                      </a>
                                      {localStorage.getItem("auth") &&
                                      jwt(localStorage.getItem("auth"))
                                        .contactType == "admin" ? (
                                        <span className="ms-4">
                                          {item.contact_paperworkdocument[0]
                                            .approvalStatus === "yes" ? (
                                            <button
                                              type="button"
                                              onClick={() =>
                                                handleApprovePaperwork(
                                                  item
                                                    .contact_paperworkdocument[0]
                                                    ._id,
                                                  "no"
                                                )
                                              }
                                              className="btn btn-translucent-primary"
                                            >
                                              Reject
                                            </button>
                                          ) : (
                                            <button
                                              type="button"
                                              onClick={() =>
                                                handleApprovePaperwork(
                                                  item
                                                    .contact_paperworkdocument[0]
                                                    ._id,
                                                  "yes"
                                                )
                                              }
                                              className="btn btn-translucent-primary"
                                            >
                                              Approve
                                            </button>
                                          )}
                                        </span>
                                      ) : (
                                        ""
                                      )}
                                      {localStorage.getItem("auth") &&
                                      jwt(localStorage.getItem("auth"))
                                        .contactType == "admin" &&
                                      post.onboardfinal === "done" ? (
                                        <span className="ms-4">
                                          <button
                                            type="button"
                                            onClick={() => {
                                              // Call the first function: handleApprovePaperwork
                                              handleApprovePaperwork(
                                                item
                                                  .contact_paperworkdocument[0]
                                                  ._id,
                                                "no"
                                              );

                                              // Call the second function: handlesignagain
                                              handlesignagain(
                                                item._id,
                                                post._id
                                              );
                                            }}
                                            className="btn btn-translucent-primary"
                                          >
                                            Reject & Sign this document again
                                          </button>
                                        </span>
                                      ) : (
                                        ""
                                      )}
                                      <br />
                                      <span>
                                        {new Date(
                                          item.contact_paperworkdocument[0].created
                                        ).toLocaleString("en-US", {
                                          month: "short",
                                          day: "numeric",
                                          year: "numeric",
                                          hour: "numeric",
                                          minute: "numeric",
                                        })}
                                        <small>
                                          {" "}
                                          {getTimeDifference(
                                            item.contact_paperworkdocument[0]
                                              .created
                                          )}
                                        </small>
                                      </span>
                                    </p>
                                  </div>
                                ))
                              ) : (
                                <p>Not Any Signed Doucment</p>
                              )}
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                ))
              ) : (
                <p className="text-center mt-5">
                  <i
                    className="fa fa-search text-white mt-5 mb-3"
                    aria-hidden="true"
                    style={{ fontSize: "100px" }}
                  ></i>
                  <br />
                  <span className="text-white">No pending Applications.</span>
                </p>
              )}
              {/* <!-- Item--> */}
            </div>
          </div>
        </main>
      </div>
    </div>
  );
}
export default Inapprovalassociates;

import React, { useState, useEffect, useRef } from "react";
import Loader from "../../Pages/Loader/Loader.js";
import Toaster from "../../Pages/Toaster/Toaster.js";
import { NavLink, useNavigate, useParams } from "react-router-dom";
import DateTimePicker from "react-datetime-picker";
import "react-datetime-picker/dist/DateTimePicker.css";
import Pic from "../img/pic.png";
import jwt from "jwt-decode";
import user_service from "../service/user_service";
import _ from "lodash";

function AddAssocTasks() {
  const initialValues = {
    taskType: "toAssociate",
    recipients: "",
    // task_recipients:"",
    selectGroup: "marketing",
    selectLevel: "corporate",
    selectDepartment: "",
    taskTitle: "",
    question: "",
    responseFormat: "",
    answer: [],
    taskDate: "",
    taskTime: "",
  };

  const navigate = useNavigate();

  const [formValues, setFormValues] = useState(initialValues);
  const [formErrors, setFormErrors] = useState({});
  const [isSubmitClick, setISSubmitClick] = useState(false);

  const [getContact, setGetContact] = useState([]);
  const [pageCount, setPageCount] = useState(0);
  const [totalRecords, setTotalRecords] = useState("");
  const [loader, setLoader] = useState({ isActive: null });
  const { isActive } = loader;
  const [toaster, setToaster] = useState({
    types: null,
    isShow: null,
    toasterBody: "",
    message: "",
  });
  const { types, isShow, toasterBody, message } = toaster;

  const [contactStatus, setContactStatus] = useState("active");
  const [contactName, setContactName] = useState("");
  const [contactType, setContactType] = useState("associate");
  const [currentPage, setCurrentPage] = useState(1);
  const [isLoading, setIsLoading] = useState(false);

  const [checkFormat, setCheckFormat] = useState("");

  const [notesmessage, setNotesmessage] = useState("");
  const editorRef = useRef(null);
  const log = () => {
    if (editorRef.current) {
      setNotesmessage(editorRef.current.getContent());
    }
  };

  const params = useParams();

  const [profile, setProfile] = useState("");

  const handleChange = (e) => {
    const { name, value } = e.target;
    setFormValues({ ...formValues, [name]: value });
  };
  const handleDateChange = (value) => {
    setFormValues({
      ...formValues,
      taskDate: value
    });
  };

  // const ContactGetAll = async () => {
  //   //if (params.type == "post") {
  //   setLoader({ isActive: true });
  //   await user_service.contactGet(1).then((response) => {
  //     setLoader({ isActive: false });
  //     if (response) {
  //       console.log(response.data.data);
  //       setGetContact(response.data.data);
  //       setPageCount(Math.ceil(response.data.totalRecords / 10));
  //       setTotalRecords(response.data.totalRecords);
  //     }
  //   });
  //   //}
  // };

  const profileGetAll = async () => {
    if (localStorage.getItem("auth")) {
      await user_service
        .profileGet(jwt(localStorage.getItem("auth")).id)
        .then((response) => {
          if (response) {
            // console.log(response);
            setProfile(response.data);
          }
        });
    }
  };
  useEffect(() => {
    window.scrollTo(0, 0);
    profileGetAll();
  }, []);

  // useEffect(() => {
  //   window.scrollTo(0, 0);
  //   ContactGetAll();
  // }, []);

  // console.log(useridd);
  // const groupValue = () => {
  //   setUseridd($("#useridd").val());
  //   var val_id = $("#useridd").val();
  //   // if (val_id) {
  //   //     this.props.dispatch(getUserdetailshub(val_id));
  //   // }
  // };

  // const removeGroupValue = () => {};
  // useEffect(() => {
  //   window.scrollTo(0, 0);
  //   if (contactType) {
  //     SearchGetAll(1);
  //   }
  // }, [contactType]);

  // const SearchGetAll = async () => {
  //   setIsLoading(true);
  //   await user_service
  //     .SearchContactfilter(1, contactType)
  //     .then((response) => {
  //       setIsLoading(false);
  //       if (response) {
  //         // setResults(response.data.data);
  //         setGetContact(response.data.data);
  //         setPageCount(Math.ceil(response.data.totalRecords / 100));
  //         setTotalRecords(response.data.totalRecords);
  //       }
  //     });
  // };

  const SearchGetAll = async () => {
    try {
      const response = await user_service.SearchContactfilterAssociate(
        1,
        contactType,
        contactStatus,
        contactName
      );
      if (response) {
        setGetContact(response.data.data);
        // setPageCount(Math.ceil(response.data.totalRecords / 10));
        // setTotalRecords(response.data.totalRecords);
      }
    } catch (error) {
      console.error("Error fetching data:", error);
    }
  };

  useEffect(() => {
    window.scrollTo(0, 0);
    if (contactName) {
      SearchGetAll(1);
    }
  }, [contactType, contactName]);

  const [contactsAssociate, setContactsAssociate] = useState("");
  const handleContactAssociate = (contactAssociate) => {
    setContactsAssociate(contactAssociate);
    setGetContact([]);
    setContactName("");
  };

  const CheckBoxFormat = (e) => {
    setCheckFormat(e.target.name);
    setCheckFormat(e.target.value);
  };

  {
    /* <!-- Form Validation Start--> */
  }
  useEffect(() => {
    if (formValues && isSubmitClick) {
      validate();
    }
  }, [formValues]);

  const validate = () => {
    const values = formValues;
    const errors = {};
    if (!values.taskTitle) {
      errors.taskTitle = "TaskTitle is required";
    }

    if (!values.question) {
      errors.question = "Question is required";
    }

    setFormErrors(errors);
    return errors;
  };
  {
    /* <!-- Form Validation End--> */
  }

  const handleChangeRow = (index, event) => {
    const newAnswers = [...formValues.answer];
    newAnswers[index].answers = event.target.value;
    setFormValues({ ...formValues, answer: newAnswers });
  };

  const addAnswerInput = () => {
    setFormValues({
      ...formValues,
      answer: [...formValues.answer, { answers: "" }],
    });
  };

  const removeAnswerInput = (index) => {
    const newAnswers = [...formValues.answer];
    newAnswers.splice(index, 1);
    setFormValues({ ...formValues, answer: newAnswers });
  };

  const handleSubmit = async (e, id) => {
    e.preventDefault();
    setISSubmitClick(true);
    let checkValue = validate();
    if (_.isEmpty(checkValue)) {
      if (formValues.taskType === "toAssociate") {
        const currentDate = new Date();
        const formattedDate = currentDate.toISOString();

        const userData = {
          agentId: jwt(localStorage.getItem("auth")).id,
          office_id: localStorage.getItem("active_office_id"),
          office_name: localStorage.getItem("active_office"),
          taskType: formValues.taskType,
          recipients: contactsAssociate._id ? contactsAssociate._id.toString() : "",
          selectGroup: formValues.selectGroup,
          selectLevel: formValues.selectLevel,
          selectDepartment: formValues.selectDepartment,
          taskTitle: formValues.taskTitle,
          question: formValues.question,
          responseFormat: checkFormat,
          answer: formValues.answer,
          taskDate: formValues.taskDate || formattedDate,
          from : `${profile.firstName} ${profile.lastName}`,
          fromImage : profile.image || ""
          // taskTime: formValues.taskTime,
        };
        try {
          setLoader({ isActive: true });
          const response = await user_service.assocTaskPost(userData);
          if (response) {
            setFormValues(response.data);
            setLoader({ isActive: false });
            setToaster({
              type: "Task Successfully",
              isShow: true,
              message: "Task Successfully",
            });
            setTimeout(() => {
              setToaster((prevToaster) => ({ ...prevToaster, isShow: false }));
              navigate(`/task`);
            }, 1000);
          } else {
            setLoader({ isActive: false });
            setToaster({
              types: "error",
              isShow: true,
              message: "Error",
            });
          }
        } catch (error) {
          setLoader({ isActive: false });
          setToaster({
            types: "error",
            isShow: true,
            message: "Error",
          });
        }
      }
       else {
        const currentDate = new Date();
        const formattedDate = currentDate.toISOString();

        const userData = {
          agentId: jwt(localStorage.getItem("auth")).id,
          office_id: localStorage.getItem("active_office_id"),
          office_name: localStorage.getItem("active_office"),
          taskType: formValues.taskType,
          recipients: jwt(localStorage.getItem("auth")).id,
          // selectGroup: formValues.selectGroup,
          // selectLevel: formValues.selectLevel,
          // selectDepartment: formValues.selectDepartment,
          taskTitle: formValues.taskTitle,
          question: formValues.question,
          responseFormat: checkFormat,
          answer: formValues.answer,
          taskDate: formValues.taskDate || formattedDate,
          taskTime: formValues.taskTime,
          from : `${profile.firstName} ${profile.lastName}`,
        };
        try {
          setLoader({ isActive: true });
          const response = await user_service.assocTaskPost(userData);
          if (response) {
            setFormValues(response);
            setLoader({ isActive: false });
            setToaster({
              type: "Task Successfully",
              isShow: true,
              message: "Task Successfully",
            });
            setTimeout(() => {
              setToaster((prevToaster) => ({ ...prevToaster, isShow: false }));
              navigate(`/task`);
            }, 1000);
            console.log(response.data);
          } else {
            setLoader({ isActive: false });
            setToaster({
              types: "error",
              isShow: true,
              message: "Error",
            });
          }
        } catch (error) {
          setLoader({ isActive: false });
          setToaster({
            types: "error",
            isShow: true,
            message: "Error",
          });
          setTimeout(() => {
            setToaster({
              types: "error",
              isShow: false,
              message: "Error",
            });
          }, 2000);
        }
      }
    }
  };
  {
    /* <!-- Api call Form onSubmit End--> */
  }

  return (
    <div className="bg-secondary float-left w-100 pt-4 mb-4">
      <Loader isActive={isActive} />
      {isShow && (
        <Toaster
          types={types}
          isShow={isShow}
          toasterBody={toasterBody}
          message={message}
        />
      )}
      <div className="content-overlay mb-md-4">
        {/* <!-- Page container--> */}
        {/* <!-- Breadcrumb--> */}
        <main className="page-wrapper profile_page_wrap">
          {/* <!-- Page card like wrapper--> */}
          <div className="d-flex align-items-center justify-content-between">
            <h3 className="text-white mb-0">Send a Task</h3>
          </div>
          {/* <hr className="mt-2" /> */}
          <div className="row">
            <div className="col-md-8">
              <div className="border bg-light rounded-3 mt-3">
                <div className="p-3 mb-3">
                  <div className="">
                    <h5>Connect with departments and associates.</h5>
                    <p>
                      Tasks are the most reliable way to complete a request.
                    </p>
                  </div>
                  <div className="row mt-0">
                    <div className="presenting_banners">
                      <div className="col-sm-12">
                        <h6>Select Task Type</h6>
                        <select
                          className="form-select"
                          name="taskType"
                          value={formValues.taskType}
                          onChange={handleChange}
                        >
                          <option value="toAssociate">
                            Associate to Associate
                          </option>
                          {/* <option value="toGroup">Associate to Group</option> */}
                          {/* <option value="toDepartment">
                            Associate to Department
                          </option> */}
                          <option value="personal">Personal</option>
                        </select>
                        <br />
                      </div>

                      {formValues.taskType === "toAssociate" ? (
                        <>
                          <div
                            className="col-sm-12"
                            // onClick={(e) => CheckBox(e)}
                          >
                            <h5>Add Recipients</h5>
                            <div className="form-check form-check-inline">
                              <input
                                className="form-check-input"
                                id="form-radio-5"
                                type="radio"
                                name="recipients"
                                value="associate"
                                checked="checked"
                              />
                              <label className="form-label">Associate</label>
                            </div>
                          </div>

                          <div className="col-sm-12 mb-0">
                            <h5 className="float-left w-100">
                              Search by name -
                            </h5>

                            <input
                              className="form-control"
                              id="inline-form-input"
                              type="text"
                              name="contactName"
                              onChange={(event) =>
                                setContactName(event.target.value)
                              }
                            />

                            {/* {
                              <Select2
                                multiple
                                className="form-control"
                                name="useridd"
                                value={useridd}
                                data={
                                  getContact &&
                                  getContact.map(
                                    ({ _id, firstName, lastName }) => ({
                                      text: firstName + " " + lastName,
                                      id: _id,
                                    })
                                  )
                                }
                                options={{
                                  maximumSelectionLength: 1000,
                                  placeholder: "Choose Contact",
                                }}
                                onSelect={groupValue}
                                onUnselect={removeGroupValue}
                                id="useridd"
                                required={true}
                              />
                            } */}
                          </div>
                        </>
                      ) : (
                        ""
                      )}

                      {formValues.taskType === "personal" ? (
                        <>
                          <h5>Task Recipient</h5>
                          <div className="float-left w-100">
                            <p className="pull-left mb-0">
                              <img
                                className="rounded-circle profile_task mb-2"
                                src={profile.image ||  Pic}
                                alt="profile picture"
                              />
                            </p>
                            <span className="ms-2 pull-left">
                              {profile.firstName}&nbsp;{profile.lastName}
                              <p className="mb-0">
                                {profile?.active_office
                                  ? profile?.active_office
                                  : "Corporate"}
                              </p>
                            </span>
                          </div>
                        </>
                      ) : (
                        ""
                      )}
                    </div>

                    {contactName ? (
                      <>
                        {getContact.length > 0 ? (
                          getContact.map((contact, index) => (
                            <div
                              className="col-md-12"
                              key={index}
                              onClick={() => handleContactAssociate(contact)}
                            >
                              <div
                                className="float-left w-100"
                                key={contact._id}
                              >
                                <p className="pull-left mb-0">
                                  <img
                                    className="rounded-circle profile_task mb-2"
                                    src={
                                      contact.image && contact.image !== "image"
                                        ? contact.image
                                        : Pic
                                    }
                                  />
                                </p>

                                <span className="ms-2 pull-left">
                                  {contact.firstName} {contact.lastName}
                                  <p className="mb-0">
                                    {contact.active_office}
                                  </p>
                                </span>
                              </div>
                            </div>
                          ))
                        ) : (
                          <p></p>
                        )}
                      </>
                    ) : (
                      <></>
                    )}

                      {contactsAssociate
                         ? 
                          <div className="member_Associate float-left w-100 mt-3" key={contactsAssociate._id}>
                            <p className="pull-left mb-0">
                              <img
                                className="rounded-circle profile_task mb-2"
                                src={
                                  contactsAssociate.image && contactsAssociate.image !== "image"
                                    ? contactsAssociate.image
                                    : Pic
                                }
                              />
                            </p>

                            <span className="ms-2 pull-left">
                              {contactsAssociate.firstName} {contactsAssociate.lastName}
                              <p className="mb-0">{contactsAssociate.active_office}</p>
                            </span>
                          </div>
                        // ))
                      : ""}

                    <div className="col-md-12">
                      <div className="col-md-12 mt-5">
                        <h6>Select Task Title</h6>
                        <input
                          className="form-control"
                          id="inline-form-input"
                          type="text"
                          name="taskTitle"
                          value={formValues.taskTitle}
                          onChange={handleChange}
                          style={{
                            border: formErrors?.taskTitle
                              ? "1px solid red"
                              : "1px solid #00000026",
                          }}
                        />
                        {formErrors.taskTitle && (
                          <div className="invalid-tooltip">
                            {formErrors.taskTitle}
                          </div>
                        )}
                      </div>

                      <div className="col-md-12 mt-3">
                        <h6>Enter Your Request or Question</h6>
                        <textarea
                          className="form-control mt-0"
                          id="textarea-input"
                          rows="5"
                          name="question"
                          onChange={handleChange}
                          autoComplete="on"
                          value={formValues.question}
                          style={{
                            border: formErrors?.question
                              ? "1px solid red"
                              : "1px solid #00000026",
                          }}
                        />
                        {formErrors.question && (
                          <div className="invalid-tooltip">
                            {formErrors.question}
                          </div>
                        )}
                      </div>

                      {formValues.taskType === "personal" ? (
                        <div
                          className="col-md-12 mt-3"
                          onClick={(e) => CheckBoxFormat(e)}
                        >
                          <h6>Select Response Format</h6>
                          <div className="form-check form-check-inline">
                            <input
                              className="form-check-input"
                              id="form-radio-5"
                              type="radio"
                              name="format"
                              value="basicTask"
                            />
                            <label className="form-label">Basic Task</label>
                          </div>
                        </div>
                      ) : (
                        <div
                          className="col-md-12 mt-3"
                          onClick={(e) => CheckBoxFormat(e)}
                        >
                          <h6>Select Response Format</h6>
                          <div className="form-check form-check-inline">
                            <input
                              className="form-check-input"
                              id="form-radio-5"
                              type="radio"
                              name="format"
                              value="basicTask"
                            />
                            <label className="form-label">Basic Task</label>
                          </div>
                          <br />

                          <div className="form-check form-check-inline">
                            <input
                              className="form-check-input"
                              id="form-radio-5"
                              type="radio"
                              name="format"
                              value="standardTask "
                            />
                            <label className="form-label">Standard Task</label>
                          </div>
                          <br />

                          <div className="form-check form-check-inline">
                            <input
                              className="form-check-input"
                              id="form-radio-5"
                              type="radio"
                              name="format"
                              value="question"
                            />
                            <label className="form-label">
                              Yes/No Question
                            </label>
                          </div>
                          <br />

                          <div className="form-check form-check-inline">
                            <input
                              className="form-check-input"
                              id="form-radio-5"
                              type="radio"
                              name="format"
                              value="singleAnswer"
                            />
                            <label className="form-label">
                              Single Line Answer
                            </label>
                          </div>
                          <br />

                          <div className="form-check form-check-inline">
                            <input
                              className="form-check-input"
                              id="form-radio-5"
                              type="radio"
                              name="format"
                              value="multilineAnswer"
                            />
                            <label className="form-label">
                              Multiline Answer
                            </label>
                          </div>
                          <br />

                          <div className="form-check form-check-inline">
                            <input
                              className="form-check-input"
                              id="form-radio-5"
                              type="radio"
                              name="format"
                              value="multiChoiceAnswer"
                            />
                            <label className="form-label">
                              Multiple Choice Answer
                            </label>
                          </div>
                        </div>
                      )}

                      {checkFormat === "multiChoiceAnswer" && (
                        <div className="col-md-12 mt-2">
                          <h6>
                            Enter the answers the recipient will choose from:
                          </h6>
                          <div className="col-md-12 mb-3">
                            {formValues.answer.map((item, index) => (
                              <>
                                <input
                                  key={index}
                                  className="form-control mt-2"
                                  type="text"
                                  name={`answers${index}`}
                                  onChange={(e) => handleChangeRow(index, e)}
                                  value={item.answers}
                                  style={{
                                    border: formErrors.answer
                                      ? "1px solid red"
                                      : "1px solid #00000026",
                                  }}
                                />
                                {formErrors.answer && (
                                  <div className="invalid-tooltip">
                                    {formErrors.answer}
                                  </div>
                                )}
                              </>
                            ))}

                            <button
                              className="btn btn-primary mt-2"
                              onClick={addAnswerInput}
                            >
                              Add Answer
                            </button>
                            <button
                              className="btn btn-primary mt-2 ms-3"
                              onClick={() => removeAnswerInput()}
                            >
                              Remove Answer
                            </button>
                          </div>
                        </div>
                      )}

                      <div className="col-md-12 mt-2">
                        <p className="">
                          All tasks but the 'Basic Task' require the task sender
                          to review and approve each answer. Two-way comments
                          are available to aid in the completion of all tasks.
                        </p>
                        <div className="row">
                          <div className="col-md-6">
                            <h6>Select Due Date-Time</h6>
                              <DateTimePicker
                              onChange={handleDateChange}
                              value={formValues.taskDate}
                              format="y-MM-dd h:mm a" // Format date-time in 'YYYY-MM-DD HH:MM AM/PM'
                              className="form-control"
                              id="inline-form-input"
                              placeholder="y-MM-dd h:mm ae" 
                              disableClock={true} // Disable the clock for cleaner UI
                            />
                            {/* <input
                              className="form-control"
                              id="inline-form-input"
                              type="date"
                              name="taskDate"
                              value={formValues.taskDate}
                              onChange={handleChange}
                            /> */}
                          </div>
                          {/* <div className="col-md-6">
                            <h6>Time</h6>
                            <input
                              className="form-control"
                              id="inline-form-input"
                              type="text"
                              name="taskTime"
                              value={formValues.taskTime}
                              onChange={handleChange}
                            />
                          </div> */}
                        </div>
                      </div>
                      <div className="pull-right mt-4">
                        <button
                          className="btn btn-primary btn-sm"
                          onClick={handleSubmit}
                        >
                          Send Task
                        </button>
                        <NavLink
                          to={`/task`}
                          className="btn btn-secondary btn-sm ms-2 ms-3"
                        >
                          Cancel
                        </NavLink>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className="col-md-4"></div>
          </div>
        </main>
      </div>
    </div>
  );
}
export default AddAssocTasks;

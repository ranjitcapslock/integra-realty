import React, { useState, useEffect } from "react";
import Pic from "../img/pic.png";
import Modal from "../../Modal";
import user_service from "../service/user_service";
import ReactPaginate from "react-paginate";
import Loader from "../../Pages/Loader/Loader.js";
import Toaster from "../../Pages/Toaster/Toaster.js";
import { NavLink, useNavigate, useParams } from "react-router-dom";
import Task from "../img/task.png";
import PagesNavigationUI from "../../Pages/PagesNavigationUI.js";
import jwt from "jwt-decode";

function AssocTasks() {
  const [getContact, setGetContact] = useState([]);
  const [pageCount, setPageCount] = useState(0);
  const [totalRecords, setTotalRecords] = useState("");
  const [loader, setLoader] = useState({ isActive: null });
  const { isActive } = loader;
  const [toaster, setToaster] = useState({
    types: null,
    isShow: null,
    toasterBody: "",
    message: "",
  });
  const { types, isShow, toasterBody, message } = toaster;

  const [query, setQuery] = useState("");
  const [currentPage, setCurrentPage] = useState(1);
  const [isLoading, setIsLoading] = useState(false);

  const params = useParams();
 
  useEffect(() => {
    window.scrollTo(0, 0);
    setLoader({ isActive: true });
    FundingRequestGet();
  }, []);

  
  const [fundingGet, setFundingGet] = useState("");
  const [fundingGetId, setFundingGetId] = useState("");


  console.log(params.id);
  const FundingRequestGet = async () => {
    try {
      setLoader({ isActive: true });
  
      let response;
  
      if (params.id) {
        response = await user_service.transactionFundingId(params.id);
        setLoader({ isActive: false });
        setFundingGetId(response.data);
      } else {
        response = await user_service.transactionFundingAll();
        setLoader({ isActive: false });
        console.log(response.data.data);
        setFundingGet(response.data.data);
      }
  
      
    } catch (error) {
      setLoader({ isActive: false });
      console.error("Error fetching funding data:", error);
    }
  };
  
  const handlePageClick = async (data) => {
    let currentPage = data.selected + 1;
    let skip = currentPage - 1 + currentPage;
    setLoader({ isActive: true });
    console.log(currentPage);
    await user_service
      .contactGetassoc(currentPage, "associate")
      .then((response) => {
        setLoader({ isActive: false });
        if (response) {
          // setResults(response.data.data);
          setGetContact(response.data.data);
          setPageCount(Math.ceil(response.data.totalRecords / 10));
        }
      });
  };


  const SearchGetAll = async () => {
    setIsLoading(true);
    await user_service
      .SearchContactGet(1, query, query, query)
      .then((response) => {
        setIsLoading(false);
        if (response) {
          // setResults(response.data.data);
          setGetContact(response.data.data);
          setPageCount(Math.ceil(response.data.totalRecords / 10));
        }
      });
  };

  const navigate = useNavigate();

  const handleSubmit = (id) => {
    console.log(id);
    navigate(`/contact-profile/${id}`);
  };

  

  const handleSubmitReview = async(id) => {
    try {
      const userDataa = {
      funding_status: "review",
      };
      setLoader({ isActive: true });
      await user_service
        .fundingRequestUpdate(id, userDataa)
        .then((response) => {
          if(response){
            setLoader({ isActive: false });
            setToaster({
              type: "success",
              isShow: true,
              message: "Funding Updated Successfully",
            });
  
            setTimeout(() => {
              setToaster((prevToaster) => ({
                ...prevToaster,
                isShow: false,
              }));
            }, 2000);
            FundingRequestGet()
          }
        });
    } catch (error) {
      setLoader({ isActive: false });
      setToaster({
        types: "error",
        isShow: true,
        message: "Error",
      });
    }
  };
  const handleSubmitFunding = async(id) => {
    try {
      const userDataa = {
      funding_status: "complete",
      };
      setLoader({ isActive: true });
      await user_service
        .fundingRequestUpdate(id, userDataa)
        .then((response) => {
          if(response){
            setLoader({ isActive: false });
            setToaster({
              type: "success",
              isShow: true,
              message: "Funding Updated Successfully",
            });
            setTimeout(() => {
              setToaster((prevToaster) => ({
                ...prevToaster,
                isShow: false,
              }));
            }, 2000);
            FundingRequestGet()
          }
        });
    } catch (error) {
      setLoader({ isActive: false });
      setToaster({
        types: "error",
        isShow: true,
        message: "Error",
      });
    }
  };
  

 
  const handleSearch = (event) => {
    event.preventDefault();
    setCurrentPage(1);
    SearchGetAll(1);
  };

  const getTimeDifference = (createdDate) => {
    const currentDate = new Date();
    const diffInMilliseconds = currentDate - new Date(createdDate);
    const diffInMinutes = Math.floor(diffInMilliseconds / 60000);
    const hours = Math.floor(diffInMinutes / 60);
    const days = Math.floor(hours / 24);
    const months = Math.floor(days / 30);
    const years = Math.floor(months / 12);

    if (years > 0) {
      return `${years} year${years > 1 ? "s" : ""} ago`;
    } else if (months > 0) {
      return `${months} month${months > 1 ? "s" : ""} ago`;
    } else if (days > 0) {
      return `${days} day${days > 1 ? "s" : ""} ago`;
    } else if (hours > 24) {
      const remainingHours = hours - Math.floor(hours / 24) * 24;
      return `1 day, ${remainingHours} hr${remainingHours > 1 ? "s" : ""} ago`;
    } else {
      return `${hours} hr${hours > 1 ? "s" : ""}, ${diffInMinutes % 60} min${
        diffInMinutes % 60 > 1 ? "s" : ""
      } ago`;
    }
  };
  return (
    <div className="">
      <Loader isActive={isActive} />
      {isShow && (
        <Toaster
          types={types}
          isShow={isShow}
          toasterBody={toasterBody}
          message={message}
        />
      )}
      <div className="">
        <Modal />
        {/* <!-- Page container--> */}
        {/* <!-- Breadcrumb--> */}
        <main className="page-wrapper homepage_layout">
          {/* <!-- Page card like wrapper--> */}

          <div className="float-left w-100 d-flex align-items-center justify-content-between">
            <h3 className="text-white w-100 mb-0">My Tasks</h3>
            <span className="pull-right">
              {/* <NavLink
                to="/add-tasks/"
                type="button"
                className="btn btn-info btn-lg pull-right">
                Add a Task
              </NavLink> */}
            </span>
          </div>
          <p className="text-white w-100">
            Easily manage your to-do list, set priorities, and track your
            progress in one convenient place.
          </p>
          <hr className="mb-3" />
          <div className="row">
            <div className="col-md-8 mt-0">
              {localStorage.getItem("auth") &&
              jwt(localStorage.getItem("auth")).contactType == "admin" ? (
                <div className="btn-group pull-right"></div>
              ) : (
                ""
              )}
              {/* <!-- Item--> */}

              {params.id ? (
                <div className="card bg-secondary card-hover mb-2 mt-2">
                  <div className="card-body">
                    <div className="float-left w-100">
                      <div className="float-left w-100 d-flex align-items-center justify-content-between">
                        {/* <img className="d-none d-sm-block" src={Pic} width="100" alt="Resume picture" /> */}
                        <div
                          className="float-left w-100 d-flex align-items-center justify-content-start"
                          key={fundingGetId?._id}>
                         
                          {/* <img className="rounded-circlee profile_picture" height="70" width="70" src={post.image || Pic} alt="Profile" /> */}
                          {/* <div className="ms-3" onClick={()=>handleSubmit(post._id)}> */}
                          {/* <div className="ms-3">
                            <strong>{fundingGetId?.contactName}</strong>&nbsp;
                            <p className="mb-1">{fundingGetId?.companyName}</p>
                          </div> */}

                          <div className="ms-0">
                            <strong>Standard Funding - {fundingGetId?.addressProperty}</strong>&nbsp;
                            <p className="mb-1">From : {fundingGetId?.contactName}<br/>  To: Funding</p>
                          </div>
                        </div>
                        <span className="pull-right tags-badge d-flex align-items-center justify-content-end w-100 ms-5">
                          <badge className="" style={{ fontSize: "14px" }}>
                            Due :
                            {new Date(fundingGetId?.created).toLocaleString("en-US", {
                              month: "short",
                              day: "numeric",
                              year: "numeric",
                              hour: "numeric",
                              minute: "numeric",
                            })}
                            <br />
                          </badge>
                        </span>
                        <span className="pull-right tags-badge d-flex align-items-center justify-content-end w-100">
                          <badge className="" style={{ fontSize: "14px" }}>
                            {getTimeDifference(fundingGetId?.created)}
                          </badge>
                        </span>
                        <span className="pull-right tags-badge d-flex align-items-center justify-content-end w-100">
                          <NavLink
                            to="/transaction"
                            className=""
                          >
                            View
                          </NavLink>
                        </span>
                      </div>
                    </div>
                  </div>
                </div>
              ) : (
                <>
                  {fundingGet && fundingGet.length > 0 ? (
                    fundingGet.map((post) => (
                      <div className="card bg-secondary card-hover mb-2 mt-2">
                         {console.log(post.transactionData)}
                        <div className="card-body">
                          <div className="float-left w-100">
                            <div className="float-left w-100 d-lg-flex d-sm-flex d-m-block d-block align-items-center justify-content-between">
                              {/* <img className="d-none d-sm-block" src={Pic} width="100" alt="Resume picture" /> */}
                              <div
                                className="float-left w-100 d-lg-flex d-sm-flex d-m-block d-block align-items-center justify-content-start"
                                key={post._id}
                              >
                                {/* <img className="rounded-circlee profile_picture" height="70" width="70" src={post.image || Pic} alt="Profile" /> */}

                                {/* <div className="ms-3" onClick={()=>handleSubmit(post._id)}> */}
                                <div className="ms-0">
                                  <strong>{post.transactionData?.contact1[0]?.data?.firstName} {post.transactionData?.contact1[0]?.data?.lastName}</strong>
                                  {/* <p className="mb-1">{post?.companyName}</p> */}
                                  <p className="mb-1"><strong>Agent:</strong> {post.transactionData?.contact3[0]?.data?.firstName} {post.transactionData?.contact3[0]?.data?.lastName}</p>

                                </div>
                              </div>
                              <span className="pull-right tags-badge d-lg-flex d-sm-flex d-m-block d-block align-items-center justify-content-end w-100">
                                <badge
                                  className="ms-lg-auto ms-md-auto ms-sm-0 ms-0"
                                  style={{ fontSize: "14px" }}
                                >
                                  Due :
                                  {new Date(post.created).toLocaleString(
                                    "en-US",
                                    {
                                      month: "short",
                                      day: "numeric",
                                      year: "numeric",
                                      hour: "numeric",
                                      minute: "numeric",
                                    }
                                  )}
                                  <br />
                                </badge>
                              </span>
                              <span className="pull-right tags-badge d-lg-flex d-sm-flex d-m-block d-block align-items-center justify-content-end w-100">
                                <badge
                                  className="ms-lg-auto ms-md-auto ms-sm-0 ms-0"
                                  style={{ fontSize: "14px" }}
                                >
                                  {getTimeDifference(post.created)}
                                </badge>
                              </span>
                            
                                {/* <span className="pull-right tags-badge d-flex align-items-center justify-content-end w-50">

                                <NavLink className="btn btn-primary"
                                >
                                Mark as Completed
                                </NavLink>
                             </span> */}
                            </div>
                            
                            <div className="float-start w-100 mt-4 d-flex align-items-center justify-content-between">
                              <div className="">
                                <NavLink className="btn btn-primary"
                                  onClick={() => handleSubmitReview(post._id)}
                                >In Review</NavLink>
                                <NavLink className="btn btn-primary ms-lg-3 ms-md-3 ms-sm-3 ms-0 mt-lg-0 mt-md-0 mt-sm-0 mt-3"
                                  onClick={() => handleSubmitFunding(post._id)}
                                >Mark as Completed</NavLink>
                              </div>
                                <div className="">
                                  <NavLink className="btn btn-secondary ms-3"
                                    to={`/transaction-review/${post.transactionId}`}>View</NavLink>
                                </div>
                              </div>
                         
                          </div>
                        </div>
                      </div>
                    ))
                  ) : (
                    <p className="text-center">
                      <img style={{width: "90px", height: "90px", }}  className="mb-3" src={Task} /> <br />
                      <span className="mt-2 text-white">
                        Oops! It seems there are no tasks to display at the
                        moment. <br />
                        Don't worry, you can always add new tasks or check back
                        later.
                      </span>
                    </p>
                  )}
                </>
              )}
            </div>
            <div className="col-md-4">
              <div id="accordionCards">
                <div
                  className="card bg-secondary mb-2 mt-2"
                  data-bs-toggle="collapse"
                  data-bs-target="#cardCollapse1"
                >
                  <div className="card-body">
                    <h3 className="h3 card-title pt-1 mb-0">💡 Tips</h3>
                  </div>
                  <div
                    className="collapse show"
                    id="cardCollapse1"
                    data-bs-parent="#accordionCards"
                  >
                    <div className="card-body mt-n1 pt-0">
                      <p className="fs-sm">
                        Effectively managing your tasks not only ensures you
                        stay organized but also reflects positively on your
                        reliability and efficiency.
                      </p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </main>
      </div>
    </div>
  );
}
export default AssocTasks;

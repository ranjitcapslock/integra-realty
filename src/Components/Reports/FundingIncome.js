import React, { useState, useEffect } from "react";
import user_service from "../service/user_service";
import Loader from "../../Pages/Loader/Loader.js";
import Toaster from "../../Pages/Toaster/Toaster.js";
import { useNavigate } from "react-router-dom";

const FundingIncome = () => {
  const [loader, setLoader] = useState({ isActive: null });
  const { isActive } = loader;
  const [toaster, setToaster] = useState({
    types: null,
    isShow: null,
    toasterBody: "",
    message: "",
  });
  const { types, isShow, toasterBody, message } = toaster;

  const navigate = useNavigate();
  const [formValues, setFormValues] = useState([]);
  const [monthTransactionRootData, setMonthTransactionRootData] = useState([]);
  const [totalCountCommission, setTotalCountCommission] = useState([]);
  const fetchTransactionData = async () => {
    try {
      setLoader({ isActive: true });
      const response = await user_service.Getfilledtransactionsmonthwise();
      if (response && response.data && response.data.monthlyResultearning) {
        console.log(response);
        setLoader({ isActive: false });
        setFormValues(response.data.monthlyResultearning);
      } else {
        console.error("Invalid response data structure:", response);
        setLoader({ isActive: false });
      }
    } catch (error) {
      console.error("Error fetching filled transactions monthwise:", error);
      setLoader({ isActive: false });
    }
  };

  useEffect(() => {
    fetchTransactionData();
  }, []);

  useEffect(() => {
    const mappedTransactionRootData = formValues.flatMap((item) =>
      item.transactionRootData.map((trans) => ({
        trans_status: trans.trans_status,
        _id: trans._id,
      }))
    );
    setMonthTransactionRootData(mappedTransactionRootData);
  }, [formValues]);

  useEffect(() => {
    // Extract transaction data from formValues
    const monthlyIncome = formValues
    .map((item) => item.transactiondata) // Get transaction data arrays
    .flat() // Flatten the nested arrays
    .map((item) => item.details) // Extract the 'details' field from each item
    .filter((data) => data); 

    // Flatten the transaction data array
    const flattenedIncome = [].concat(...monthlyIncome);

    // Filter out any falsy values from the flattenedIncome
    const itemdata = flattenedIncome.filter((data) => {
      return data;
    });

    // Map the filtered income data and set it to state
    const newData = itemdata.map((data) => {
      return data;
    });
    setTotalCountCommission(newData);
  }, [formValues]);

 
  const [selectedMonth, setSelectedMonth] = useState(new Date().getMonth() + 1);

  const handleChangeMonth = (event) => {
    setSelectedMonth(parseInt(event.target.value));
  };

  const currentYear = new Date().getFullYear();
  const currentMonth = new Date().getMonth() + 1; // Month is zero-based

  const generateSelectOptions = () => {
    const options = [];
    for (let i = currentMonth; i >= 1; i--) {
      options.push(
        <option key={i} value={i}>
          {new Date(currentYear, i - 1, 1).toLocaleDateString("default", {
            month: "long",
          })}{" "}
          {currentYear}
        </option>
      );
    }
    return options;
  };

  const handleClickAddress = (id) => {
    navigate(`/transaction-summary/${id}`);
  };
  return (
    <div className="bg-secondary float-left w-100 pt-4">
      <Loader isActive={isActive} />
      {isShow && (
        <Toaster
          types={types}
          isShow={isShow}
          toasterBody={toasterBody}
          message={message}
        />
      )}
      <main className="page-wrapper getContact_page_wrap">
        <div className="container content-overlay mt-0">
          <div className="row mt-3">
            <div className="col-md-12">
              <div className="row">
                <div className="border rounded-3 p-3 bg-light">
                  <p className="h6 text-center">Transaction Funding Income</p>
                  <div className="table-responsive">
                    <div className="col-md-3 mb-3">
                      <select
                        className="form-select"
                        value={selectedMonth}
                        onChange={handleChangeMonth}
                      >
                        {generateSelectOptions()}
                      </select>
                    </div>
                    <table
                      id="DepositLedger"
                      className="table table-striped mb-0"
                      width="100%"
                      border="0"
                      cellSpacing="0"
                      cellPadding="0"
                    >
                      <thead>
                        <tr>
                          <th>Transaction Address</th>
                          <th>Transaction Income</th>
                        </tr>
                      </thead>
                      <tbody>
                        {totalCountCommission.length > 0 ? (
                          (() => {
                            let monthlyTotalAmount = 0;
                            const filteredItems = totalCountCommission
                              .filter((item) => {
                                const transactionDate = new Date(item.created);
                                const transactionMonth =
                                  transactionDate.getMonth() + 1;
                                return transactionMonth === selectedMonth;
                              })
                              .map((item, index) => {
                                const foundItems = item.payoutItem.filter(
                                  (payoutItem) =>
                                    payoutItem.payto === "In Depth Realty"
                                );

                                const amounts = foundItems.map((payoutItem) =>
                                  parseInt(payoutItem.amount)
                                );
                                const totalAmount =
                                  (parseInt(item.transactionFee) || 0) +
                                  (parseInt(item.wireFee) || 0) +
                                  (parseInt(item.transactionPersonal) || 0) +
                                  (parseInt(item.managementFee) || 0) +
                                  (parseInt(item.highValueFee) || 0) +
                                  (parseInt(item.sellerFee) || 0) +
                                  (parseInt(item.buyerFee) || 0) +
                                  (amounts.reduce(
                                    (acc, curr) => acc + curr,
                                    0
                                  ) || 0); // Summing amounts

                                monthlyTotalAmount += totalAmount;

                                // Find the trans_status for the current transactionId and assign label
                                const transactionStatus =
                                  monthTransactionRootData.find(
                                    (data) => data._id === item.transactionId
                                  )?.trans_status;

                                const transactionStatusLabel =
                                   transactionStatus === "filed"
                                    ? "Filed"
                                    : "N/A";

                                return (
                                  <tr key={index}>
                                    <td
                                      onClick={() =>
                                        handleClickAddress(item.transactionId)
                                      }
                                    >
                                      {item.addressProperty ?? "NaN"}
                                    </td>
                                    <td>{totalAmount}</td>
                                    <td>{transactionStatusLabel}</td>{" "}
                                  </tr>
                                );
                              });

                            // Return the filtered items along with the total row
                            return (
                              <>
                                {filteredItems}
                                <tr>
                                  <th>Total Amount:</th>
                                  <td className="h5">{monthlyTotalAmount}</td>
                                </tr>
                              </>
                            );
                          })()
                        ) : (
                          <tr>
                            <td colSpan="3">No data to display.</td>
                          </tr>
                        )}

                        {/* {totalCountCommission.length > 0 ? (
                          (() => {
                            let monthlyTotalAmount = 0;
                            const filteredItems = totalCountCommission
                              .filter((item) => {
                                const transactionDate = new Date(item.created);
                             
                                const transactionMonth =
                                  transactionDate.getMonth() + 1;
                                return transactionMonth === selectedMonth;
                              })
                              .map((item, index) => {
                                const foundItems = item.payoutItem.filter(
                                  (item) => item.payto === "In Depth Realty"
                                );
                                const amounts = foundItems.map((item) =>
                                  parseInt(item.amount)
                                );
                                const totalAmount =
                                  (parseInt(item.transactionFee) || 0) +
                                  (parseInt(item.wireFee) || 0) +
                                  (parseInt(item.transactionPersonal) || 0) +
                                  (parseInt(item.managementFee) || 0) +
                                  (parseInt(item.highValueFee) || 0) +
                                  (parseInt(item.sellerFee) || 0) +
                                  (parseInt(item.buyerFee) || 0) +
                                  (amounts.reduce(
                                    (acc, curr) => acc + curr,
                                    0
                                  ) || 0);

                                monthlyTotalAmount += totalAmount;

                                return (
                                  <tr key={index}>
                                    <td onClick={()=>handleClickAddress(item.transactionId)}>{item.addressProperty ?? "NaN"}</td>
                                    <td>{totalAmount}</td>
                                  </tr>
                                );
                              });

                          
                            return (
                              <>
                                {filteredItems}
                                <tr>
                                  <th>Total Amount:</th>
                                  <td className="h5">{monthlyTotalAmount}</td>
                                </tr>
                              </>
                            );
                          })()
                        ) : (
                          <tr>
                            <td colSpan="3">No data to display.</td>
                          </tr>
                        )} */}
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </main>
    </div>
  );
};

export default FundingIncome;

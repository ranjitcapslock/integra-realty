import React, { useState, useEffect } from "react";
import Pic from "../img/pic.png";
import Modal from "../../Modal";
import user_service from "../service/user_service";
import ReactPaginate from "react-paginate";
import Loader from "../../Pages/Loader/Loader.js";
import Toaster from "../../Pages/Toaster/Toaster.js";
import { NavLink, useNavigate } from "react-router-dom";

function AssocLicensesReport() {
  const [getContact, setGetContact] = useState([]);
  const [pageCount, setPageCount] = useState(0);
  const [totalRecords, setTotalRecords] = useState("");
  const [loader, setLoader] = useState({ isActive: null });
  const { isActive } = loader;
  const [toaster, setToaster] = useState({
    types: null,
    isShow: null,
    toasterBody: "",
    message: "",
  });
  const { types, isShow, toasterBody, message } = toaster;

  const [query, setQuery] = useState("");
  const [select, setSelect] = useState("");
  const [results, setResults] = useState([]);
  const [currentPage, setCurrentPage] = useState(1);
  const [isLoading, setIsLoading] = useState(false);
 
  useEffect(() => {
    window.scrollTo(0, 0);

    const ContactGetAll = async () => {
      setLoader({ isActive: true });
      try {
        const response = await user_service.contactGetassoc(1, "associate");
        setLoader({ isActive: false });

        if (response && response.data.data.length > 0) {
          setGetContact(response.data.data);
          setPageCount(Math.ceil(response.data.totalRecords / 10));
        } else {
          setGetContact([]);
          setPageCount(0);
        }
      } catch (error) {
        console.error("Error fetching contacts:", error);
        setLoader({ isActive: false });
      }
    };

    ContactGetAll();
  }, []);

  
  const handlePageClick = async (data) => {
    let currentPage = data.selected + 1;
    let skip = currentPage - 1 + currentPage;
    setLoader({ isActive: true });
    console.log(currentPage);
    await user_service
      .contactGetassoc(currentPage, "associate")
      .then((response) => {
        setLoader({ isActive: false });
        if (response) {
          // setResults(response.data.data);
          setGetContact(response.data.data);
          setPageCount(Math.ceil(response.data.totalRecords / 10));
        }
      });
  };

  {
    /* <!-- paginate Function Search Api call Start--> */
  }
  useEffect(() => {
    window.scrollTo(0, 0);
    if (query) {
      SearchGetAll(1);
    }
  }, [query]);

  const SearchGetAll = async () => {
    setIsLoading(true);
    await user_service
      .SearchContactGet(1, query, query, query)
      .then((response) => {
        setIsLoading(false);
        if (response) {
          // setResults(response.data.data);
          setGetContact(response.data.data);
          setPageCount(Math.ceil(response.data.totalRecords / 10));
        }
      });
  };

  const navigate = useNavigate();

  const handleSubmit = (id) => {
    console.log(id);
    navigate(`/contact-profile/${id}`);
  };

  {
    /* <!-- paginate Function Search Api call End--> */
  }

  const handleSearch = (event) => {
    event.preventDefault();
    setCurrentPage(1);
    SearchGetAll(1);
  };

  const isExpired = (expirationDate) => {
    const currentDate = new Date();
    const expireDate = new Date(expirationDate);
    return expireDate < currentDate;
  }
  return (
    <div className="bg-secondary float-left w-100 pt-4">
      <Loader isActive={isActive} />
      {isShow && (
        <Toaster
          types={types}
          isShow={isShow}
          toasterBody={toasterBody}
          message={message}
        />
      )}
      <main className="page-wrapper">
        <Modal />
        {/* <!-- Page container--> */}
        <div className="container content-overlay">
          <div className="row">
            {/* <!-- List of resumes--> */}
            <div className="col-md-12">
              <div className="bg-light p-3 border-0 rounded-3 shadow-sm float-start w-100">
                <h5>Associate Licenses Report</h5>
                {/* <!-- Normal search form group --> */}
                <form onSubmit={handleSearch}>
                  <p className="mt-5">
                    Quick Search: <a href="">Show All</a>
                  </p>
                  <div className="d-lg-flex d-md-flex d-sm-block d-block justify-content-start align-items-between">
                    <div className="form-outline mb-lg-0 mb-md-0 mb-sm-4 mb-4">
                      <input
                        className="form-control"
                        type="search"
                        id="search-input-1"
                        placeholder="Search All"
                        valvalue={query}
                        onChange={(event) => setQuery(event.target.value)}
                      />
                    </div>
                    <span className="ms-lg-4 ms-md-4 ms-sm-0 ms-0 mt-lg-0 mt-md-0 mt-sm-4 mt-4">
                      <button
                        type="button"
                        className="btn btn-translucent-primary"
                      >
                        Find
                      </button>
                      <a href="#" id="button-item" type="button">
                        Reset
                      </a>
                    </span>
                  </div>
                  <div>
                    {isLoading ? (
                      <p>Loading...</p>
                    ) : (
                      results.map((post) => {
                        return (
                          <div>
                            {/* <!-- List group with icons and badges --> */}
                            <ul className="list-group">
                              <li className="list-group-item d-flex justify-content-between align-items-center">
                                <div>
                                  <strong>{post.firstName}</strong>&nbsp;
                                  <br />
                                  <strong>{post.lastName}</strong>
                                  <br />
                                </div>
                              </li>
                            </ul>
                          </div>
                        );
                      })
                    )}
                  </div>
                </form>

                {/* <!-- Item--> */}
                {getContact.map((post) => (
                  <div className="card bg-secondary card-hover mb-2 mt-4">
                    <div className="card-body">
                      <div className="float-left w-100">
                        <div className="float-left w-100 d-lg-flex d-md-flex d-sm-block d-block align-items-center justify-content-between">
                          <div
                            className="mb-lg-0 mb-md-0 mb-sm-4 mb-4 float-left w-100 d-flex align-items-center justify-content-start"
                            key={post._id}
                          >
                            <img
                              className="rounded-circlee profile_picture"
                              height="70"
                              width="70"
                              src={post.image || Pic}
                              alt="Profile"
                            />

                            {/* <div className="ms-3" onClick={()=>handleSubmit(post._id)}> */}
                            <div className="ms-3">
                              <strong>{post.firstName}</strong>&nbsp;
                              <strong>{post.lastName}</strong>
                              <p className="mb-1">
                                {post.additionalActivateFields.office
                                  ? post.additionalActivateFields.office
                                  : post.contactType}
                              </p>
                            </div>
                          </div>
                          <span className="pull-left tags-badge d-lg-flex d-md-flex d-sm-block d-block align-items-center justify-content-end w-100">
                            <badge className="ms-lg-auto ms-md-auto ms-sm-0 ms-0" style={{ fontSize: "14px" }}>
                              License:{" "}
                              {post.additionalActivateFields.license_number
                                ? post.additionalActivateFields.license_number
                                : "Missing"}
                            </badge>
                          </span>
                          <span className="pull-left tags-badge d-lg-flex d-md-flex d-sm-block d-block align-items-center justify-content-end w-100">
                            <badge className="ms-lg-auto ms-md-auto ms-sm-0 ms-0" style={{ fontSize: "14px" }}>
                              Expiration:{" "}
                              {post.additionalActivateFields.date_expire
                                ? post.additionalActivateFields.date_expire
                                : "Missing"}
                              {post.additionalActivateFields?.date_expire ? (
                                <span className={`d-table badge ${isExpired(post.additionalActivateFields.date_expire) ? 'bg-danger' : 'bg-success'}`}>
                                  {isExpired(post.additionalActivateFields.date_expire) ? "License Expired" : "License Active"}
                                </span>
                              ) : (
                                ""
                              )}
                            </badge>
                          </span>
                          <span className="pull-right tags-badge d-flex align-items-center justify-content-end w-100">
                            <NavLink
                              to={`/contact-profile/${post._id}`}
                              className=""
                              title="Please click on the edit icon to open the contact page, then click to the account details tab and  update your license"
                            >
                              <i className="fi-edit"></i>
                            </NavLink>
                          </span>
                        </div>
                      </div>
                    </div>
                  </div>
                ))}
              </div>
            </div>
            <div className="d-flex align-items-center justify-content-end mb-0 mt-4">
              <ReactPaginate
                className=""
                previousLabel={"Previous"}
                nextLabel={"Next"}
                breakLabel={"..."}
                pageCount={pageCount}
                marginPagesDisplayed={1}
                pageRangeDisplayed={2}
                onPageChange={handlePageClick}
                containerClassName={"pagination justify-content-center mb-0"}
                pageClassName={"page-item"}
                pageLinkClassName={"page-link"}
                previousClassName={"page-item"}
                previousLinkClassName={"page-link"}
                nextClassName={"page-item"}
                nextLinkClassName={"page-link"}
                breakClassName={"page-item"}
                breakLinkClassName={"page-link"}
                activeClassName={"active"}
              />
            </div>
          </div>
        </div>
      </main>
    </div>
  );
}
export default AssocLicensesReport;


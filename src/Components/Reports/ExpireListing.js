import React, { useEffect, useState } from "react";
import { NavLink, useNavigate } from "react-router-dom";
import user_service from "../service/user_service.js";
import ReactPaginate from "react-paginate";
import Loader from "../../Pages/Loader/Loader.js";
import Toaster from "../../Pages/Toaster/Toaster.js";
import jwt from "jwt-decode";
import moment from "moment-timezone";
import defaultpropertyimage from "../../Components/img/defaultpropertyimage.jpeg";


function ExpireListing() {
  const [activeGet, setActiveGet] = useState([]);
  const [pageCount, setPageCount] = useState(0);
  const [loader, setLoader] = useState({ isActive: null });
  const { isActive } = loader;
  const [toaster, setToaster] = useState({
    types: null,
    isShow: null,
    toasterBody: "",
    message: "",
  });
  const { types, isShow, toasterBody, message } = toaster;
  const navigate = useNavigate();

  useEffect(() => {
    window.scrollTo(0, 0);
    const ExpireListingGet = async () => {
      setLoader({ isActive: true });
      await user_service.expireListingGet(1).then((response) => {
        setLoader({ isActive: false });
        if (response) {
          setActiveGet(response.data.data);
          setPageCount(Math.ceil(response.data.totalCount / 10));
        }
      });
    };
    ExpireListingGet();
  }, []);

  const handlePageClick = async (data) => {
    try {
      const currentPage = data.selected + 1;

      const response = await user_service.expireListingGet(currentPage);

      setLoader({ isActive: false });

      if (response && response.data) {
        setActiveGet(response.data.data);
        setPageCount(Math.ceil(response.data.totalCount / 10));
      }
    } catch (error) {
      console.error("Error in handlePageClick:", error);
      // Handle errors as needed
    }
  };

  const Catalog = (id, agentID) => {
    if (localStorage.getItem("auth")) {
      const decodedToken = jwt(localStorage.getItem("auth"));
      if (decodedToken.contactType === "admin" || agentID === decodedToken.id) {
        navigate(`/transaction-summary/${id}`);
      } else {
        navigate(`/transaction`);
      }
    }
  };

  const propertypic = (e) => {
    e.target.src = defaultpropertyimage;
  };

  return (
    <div className="bg-secondary float-left w-100 pt-4 mb-4">
      <Loader isActive={isActive} />
      {isShow && (
        <Toaster
          types={types}
          isShow={isShow}
          toasterBody={toasterBody}
          message={message}
        />
      )}
      <main className="page-wrapper">
        <div className="content-overlay">
          <div className="row">
            <div className="col-lg-12  bg-light p-3 border-0 rounded-3 shadow-sm float-start w-100">
              <h6>Expired Listing Report</h6>
              {activeGet && activeGet.length > 0 ? (
                activeGet.map((post) => (
                  <React.Fragment key={post._id}>
                    <div
                      className="card card-hover card-horizontal border-0 shadow-sm mb-4 p-3"
                      onClick={(e) =>
                        Catalog(
                          post.transaction?._id,
                          post.transaction?.agentId
                        )
                      }
                    >
                      <a className="card-img-top w-25">
                        <img
                          className="img-fluid w-100"
                          src={
                            post.image && post.image !== "image"
                              ? post.image
                              : defaultpropertyimage
                          }
                          alt="Property"
                          onError={(e) => propertypic(e)}
                        />
                      </a>
                      <div className="w-75 card-body position-relative pb-3 pt-0 mt-lg-0 mt-md-0 mt-sm-4 mt-4 px-lg-4 px-md-4 px-sm-0 px-0">
                        {post.mlsNumber ? (
                          <p className="mb-2">
                            <b>MLS Number:</b> #{post.mlsNumber}&nbsp;
                          </p>
                        ) : (
                          ""
                        )}

                        {post.streetAddress ? (
                          <p className="mb-2">
                            <b>Street-Address:</b>&nbsp;
                            {post.streetAddress}, {post.city},{post.state}
                          </p>
                        ) : (
                          ""
                        )}

                        {post.propertyType ? (
                          <p className="mb-2">
                            <b>Type:</b> {post.propertyType}&nbsp;
                          </p>
                        ) : (
                          ""
                        )}

                        {post.contact3 && post.contact3.length > 0 ? (
                          post.contact3[0] !== null ? (
                            <p className="mb-2" key={post.contact3[0]._id}>
                              <b>Agent:</b>&nbsp;
                              {post.contact3[0].organization &&
                              post.contact3[0].organization !== ""
                                ? post.contact3[0].organization
                                : post.contact3[0].firstName}
                              &nbsp;{post.contact3[0].lastName}
                            </p>
                          ) : null
                        ) : (
                          <p></p>
                        )}

                        {post.contact1 && post.contact1.length > 0 ? (
                          post.contact1[0] !== null ? (
                            <p className="mb-2" key={post.contact1[0]._id}>
                              <b> Client:</b>&nbsp;
                              {post.contact1[0].organization &&
                              post.contact1[0].organization !== ""
                                ? post.contact1[0].organization
                                : post.contact1[0].firstName}
                              &nbsp;{post.contact1[0].lastName}
                            </p>
                          ) : null
                        ) : (
                          <p></p>
                        )}

                        {post.listingsAgent ? (
                          <p className="mb-2">
                            <strong>ListingsAgent:</strong> {post.listingsAgent}
                          </p>
                        ) : (
                          ""
                        )}
                        {post.status ? (
                          <p className="mb-2 badge bg-success">
                            <strong>Status:</strong> {post.status}
                          </p>
                        ) : (
                          ""
                        )}
                        <br />

                        {post.contact3 &&
                        post.contact3.length> 0 &&
                        post.contact3[0].additionalActivateFields.office ? (
                          <span>
                            <strong>Office:</strong>{" "}
                            {post.contact3[0].additionalActivateFields.office}
                          </span>
                        ) : (
                          ""
                        )}
                        <br />

                        {post.expiration_date ? (
                          <span>
                            <strong>Expire Date:</strong>{" "}
                            {moment(post.expiration_date).format("MM/DD/YYYY")}
                          </span>
                        ) : (
                          ""
                        )}
                      </div>
                    </div>
                  </React.Fragment>
                ))
              ) : (
                <p className="text-center mt-5">
                  <i
                    className="fa fa-calculator mb-5"
                    aria-hidden="true"
                    style={{ fontSize: "135px" }}
                  ></i>
                  <br />
                  <span className="text-white">No Active Listing</span>
                </p>
              )}
            </div>
              <div className="justify-content-end mb-1 mt-4">
                <ReactPaginate
                  previousLabel={"Previous"}
                  nextLabel={"Next"}
                  breakLabel={"..."}
                  pageCount={pageCount}
                  marginPagesDisplayed={1}
                  pageRangeDisplayed={2}
                  onPageChange={handlePageClick}
                  containerClassName={"pagination justify-content-center"}
                  pageClassName={"page-item"}
                  pageLinkClassName={"page-link"}
                  previousClassName={"page-item"}
                  previousLinkClassName={"page-link"}
                  nextClassName={"page-item"}
                  nextLinkClassName={"page-link"}
                  breakClassName={"page-item"}
                  breakLinkClassName={"page-link"}
                  activeClassName={"active"}
                />
              </div>
          </div>
        </div>
      </main>
    </div>
  );
}

export default ExpireListing;

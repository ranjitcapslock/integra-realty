import React, { useEffect, useState } from "react";
import avtar from "../img/avtar.jpg";
import { NavLink, useNavigate, useParams } from "react-router-dom";
import user_service from "../service/user_service";
import ReactPaginate from "react-paginate";
import Loader from "../../Pages/Loader/Loader.js";
import Toaster from "../../Pages/Toaster/Toaster";
import defaultpropertyimage from "../../Components/img/defaultpropertyimage.jpeg";
import { logDOM } from "@testing-library/react";
import jwt from "jwt-decode";
function TransactFundingReport() {
  const [activeGet, setActiveGet] = useState([]);
  const params = useParams();
  const [results, setResults] = useState([]);
  const [pageCount, setPageCount] = useState(0);
  const [loader, setLoader] = useState({ isActive: null });
  const { isActive } = loader;
  const [toaster, setToaster] = useState({
    types: null,
    isShow: null,
    toasterBody: "",
    message: "",
  });
  const { types, isShow, toasterBody, message } = toaster;

  const navigate = useNavigate();

  const AddTransaction = () => {
    navigate("/add-transaction");
  };

  const Catalog = (id) => {
    navigate(`/transaction-summary/${id}`);
  };

  {
    /* <!-- paginate Function Api call Start--> */
  }
  const TransactionGetAll = async () => {
    //await user_service.TransactionGet(1).then((response) => {
    //const agentId = params.id
    await user_service.TransactionGetReport(1).then((response) => {
      setLoader({ isActive: false });
      if (response) {
        // console.log(response.data)
        setActiveGet(response.data.data);
        setPageCount(Math.ceil(response.data.totalRecords / 10));
      }
    });
  };
  useEffect(() => {
    window.scrollTo(0, 0);
    setLoader({ isActive: true });
    TransactionGetAll();
  }, []);

  const handleResetClick = () => {
    setLoader({ isActive: true });
    setSearchdata("");
    TransactionGetAll();
  };

  const handlePageClick = async (data) => {
    let currentPage = data.selected + 1;
    let skip = currentPage - 1 + currentPage;
    setLoader({ isActive: true });
    if (
      localStorage.getItem("auth") &&
      (jwt(localStorage.getItem("auth")).contactType == "associate" ||
        jwt(localStorage.getItem("auth")).contactType == "agent")
    ) {
      var query = `?page=${currentPage}&agentId=${
        jwt(localStorage.getItem("auth")).id
      }`;
    } else {
      var query = `?page=${currentPage}`;
    }
    await user_service.TransactionGet(query).then((response) => {
      setLoader({ isActive: false });
      if (response) {
        setActiveGet(response.data.data);
        setPageCount(Math.ceil(response.data.totalRecords / 10));
      }
    });
  };
  {
    /* <!-- paginate Function Api call End--> */
  }

  // const SearchGetAll = async () => {

  //     await user_service.SearchTransaction(1, searchdata).then((response) => {
  //         if (response) {

  //             setActiveGet(response.data.data);
  //             setPageCount(Math.ceil(response.data.totalRecords / 10));

  //         }
  //     });
  // }

  const SearchGetAll = async () => {
    let queryParams = `?page=1`;
    if (
      localStorage.getItem("auth") &&
      jwt(localStorage.getItem("auth")).contactType == "admin"
    ) {
      // queryParams += `&agentId=${jwt(localStorage.getItem("auth")).id}`;
    } else {
      queryParams += `&agentId=${jwt(localStorage.getItem("auth")).id}`;
    }
    await user_service
      .TransactionGet(queryParams, searchdata)
      .then((response) => {
        if (response) {
          console.log(response);
          setActiveGet(response.data.data);
          setPageCount(Math.ceil(response.data.totalRecords / 10));
        }
      });
  };

  const propertypic = (e) => {
    e.target.src = defaultpropertyimage;
  };
  const [searchdata, setSearchdata] = useState("");
  const handleChange = (e) => {
    setSearchdata(e.target.value);
  };

  return (
    <div className="bg-secondary float-left w-100 pt-4">
      <Loader isActive={isActive} />
      {isShow && (
        <Toaster
          types={types}
          isShow={isShow}
          toasterBody={toasterBody}
          message={message}
        />
      )}
      <main className="page-wrapper">
        {/* <!-- Page container--> */}
        <div className="container content-overlay">
          {/* <!-- Page card like wrapper--> */}

          {/* <nav className="mb-3 mb-md-4 pt-md-3" aria-label="Breadcrumb">
                        <ol className="breadcrumb breadcrumb-dark">
                            <li className="breadcrumb-item"><a href="#">Home</a></li>
                            <li className="breadcrumb-item"><a href="#">Account</a></li>
                            <li className="breadcrumb-item active" aria-current="page">Reports</li>
                        </ol>
                    </nav> */}
          <div className="">
            <div className="float-left w-100 d-flex align-items-center justify-content-start">
              <h3 className="mb-3 text-white">
                Funding and Disbursement Report
              </h3>
            </div>

            {/* <!-- Content--> */}
            <div className="row">
              <div className="col-md-12 mb-5">
                <div className="d-flex justify-content-start align-items-center mb-4">
                  <div className="form-outline col-lg-8 col-md-6 col-sm-6 col-6">
                    <input
                      id="search-input-1"
                      type="search"
                      className="form-control"
                      placeholder="Street address, contact name, or reference number."
                      name="searchdata"
                      onChange={handleChange}
                    />
                  </div>
                  <span className="ms-3 d-flex">
                    <button
                      type="button"
                      className="btn btn-translucent-primary btn-sm text-white"
                      onClick={() => SearchGetAll()}
                    >
                      Find
                    </button>
                    <a
                      href="#"
                      id="button-item"
                      className="btn btn-translucent-primary btn-sm text-white"
                      onClick={handleResetClick}
                      type="button"
                    >
                      Reset
                    </a>
                  </span>
                </div>

                {/* <div className="mt-2" id="from-check-inline">
                                    <span className="ms-0" id="match-only">Show Only</span>
                                    <span className="ms-2">
                                        <input className="form-check-input" id="form-radio-1" type="radio" name="radios-inline" checked />
                                        <label className="form-check-label">My Transactions</label>
                                    </span>
                                    <span className="ms-2">
                                        <input className="form-check-input" id="form-radio-2" type="radio" name="radios-inline" checked />
                                        <label className="form-check-label">Managed Transactions</label>
                                    </span>
                                </div> */}
             

                {/* <!-- Item--> */}
                {activeGet ? (
                  activeGet.map((post) => (
                    <React.Fragment key={post._id}>
                      {post.propertyDetail ? (
                        <div className="card card-hover card-horizontal transactions border-0 shadow-sm mb-4 p-3">
                          <div className="col-md-4 text-left mb-lg-0 mb-md-0 mb-sm-2 mb-2">
                            {/* <a className="card-img-top" onClick={(e) => Catalog(post._id)}>
                                    <img className="img-fluid w-100" src={post.propertyDetail.image &&  post.propertyDetail.image !== "image" ? post.propertyDetail.image :defaultpropertyimage} alt="Property"  onError={e => propertypic(e)} />
                                </a> */}
                            <div className="card-body position-relative p-0">
                              {/* <NavLink>{post.propertyDetail.state}</NavLink> */}
                              <p className="mb-1 fs-sm text-muted">
                                <h6 className="mb-0">
                                  <a className="active">
                                    {post.propertyDetail.streetAddress},{" "}
                                    {post.propertyDetail.city},
                                    {post.propertyDetail.state}
                                  </a>
                                </h6>
                              </p>
                              {post.contact1 && post.contact1.length > 0 ? (
                                post.contact1[0] !== null ? (
                                  <p
                                    className="mb-1 fs-sm text-muted"
                                    key={post.contact1[0]._id}
                                  >
                                    {post.contact1[0].data.organization &&
                                    post.contact1[0].data.organization !== ""
                                      ? post.contact1[0].data.organization
                                      : post.contact1[0].data.firstName}
                                    &nbsp;{post.contact1[0].data.lastName}
                                  </p>
                                ) : null
                              ) : (
                                <p></p>
                              )}
                              <p className="mb-1 fs-sm text-muted">
                                {post.propertyDetail.listingsAgent}
                              </p>
                              {/* <p className="mb-1 fs-sm text-muted"><i className="fa fa-home" aria-hidden="true"></i> {post.represent}</p> */}
                              <p className="mb-1 fs-sm text-muted">
                                Started :{" "}
                                {new Date(post.timeStamp).toLocaleString(
                                  "en-US",
                                  {
                                    month: "short",
                                    day: "numeric",
                                    year: "numeric",
                                    // hour: 'numeric',
                                    // minute: 'numeric',
                                  }
                                )}
                              </p>

                              <p className="mb-1 fs-sm text-muted">
                                Updated :{" "}
                                {new Date(post.updated).toLocaleString(
                                  "en-US",
                                  {
                                    month: "short",
                                    day: "numeric",
                                    year: "numeric",
                                    // hour: 'numeric',
                                    // minute: 'numeric',
                                  }
                                )}
                              </p>

                              {/* <p className="mb-2 fs-sm text-muted">Closing : {new Date(post.updated).toLocaleString('en-US', {
                                                                month: 'short',
                                                                day: 'numeric',
                                                                year: 'numeric',
                                                                // hour: 'numeric',
                                                                // minute: 'numeric',
                                                            })}</p> */}

                              {/* <span className="d-table badge bg-success">{post.propertyDetail.category} 
                                                        </span> */}
                              {/* <p className="mb-2 fs-sm text-muted">{post.propertyDetail.category}</p> */}
                            </div>
                          </div>
                          <div className="col-md-4 d-lg-flex d-md-flex d-sm-block d-block align-items-center justify-content-center mb-lg-0 mb-md-0 mb-sm-2 mb-2">
                            <div className="RefNum">
                              Transaction ID#{post.reference_no}
                              <br />
                              <span className="ReviewNums text-left">
                                <span className="ReviewNum stat">Request</span>
                              </span>
                            </div>
                            
                          </div>
                          <div className="col-md-4 d-lg-flex d-md-flex d-sm-block d-block align-items-center justify-content-end">
                            <div className="text-left">
                              <div className="Types text-start">
                                                        
                                                              {post.represent ===
                                                                "buyer" ? (
                                                                  <>
                                                                 <p className="mb-1">
                                                                    <b>
                                                                      Category
                                                                    </b>
                                                                    : &nbsp;
                                                                    {post.represent ===
                                                                    "buyer"
                                                                      ? "Buyer"
                                                                      : ""}
                                                                      </p>
                                                                  </>
                                                                ) : (
                                                                  ""
                                                                )}

                                                                {post.represent ===
                                                                "seller" ? (
                                                                  <>
                                                               <p className="mb-1">
                                                                    <b>
                                                                      Category
                                                                    </b>
                                                                    : &nbsp;
                                                                    {post.represent ===
                                                                    "seller"
                                                                      ? "Seller"
                                                                      : ""}
                                                                      </p>
                                                                  </>
                                                                ) : (
                                                                  ""
                                                                )}

                                                                {post.represent ===
                                                                "both" ? (
                                                                  <>
                                                                  <p className="mb-1">
                                                                    <b>
                                                                      Category
                                                                    </b>
                                                                    : &nbsp;
                                                                    {post.represent ===
                                                                    "both"
                                                                      ? "Buyer & Seller"
                                                                      : ""}
                                                                      </p>
                                                                  </>
                                                                ) : (
                                                                  ""
                                                                )}
                                                                {post.represent ===
                                                                "referral" ? (
                                                                  <>
                                                               <p className="mb-1">

                                                                    <b>
                                                                      Category
                                                                    </b>
                                                                    : &nbsp;
                                                                    {post.represent ===
                                                                    "referral"
                                                                      ? "Referral"
                                                                      : ""}
                                                                      </p>
                                                                  </>
                                                                ) : (
                                                                  ""
                                                                )}

                                                 
                                                            
                                {post.represent === "buyer" ? (
                                  <>
                                    <p className="mb-1">
                                    <b>Type</b>: &nbsp;
                                    {post.type === "buy_residential"
                                      ? "Buyer Residential"
                                      : ""}
                                    {post.type === "buy_commercial"
                                      ? "Buyer Commercial"
                                      : ""}
                                    {post.type === "buy_investment"
                                      ? "Buyer Investment"
                                      : ""}
                                    {post.type === "buy_land" ? "Buyer Land" : ""}
                                    {post.type === "buy_referral"
                                      ? "Buyer Referral"
                                      : ""}
                                    {post.type === "buy_lease"
                                      ? "Buyer Lease"
                                      : ""}
                                      </p>
                                  </>
                                ) : (
                                  ""
                                )}

                                {post.represent === "seller" ? (
                                  <>
                                    <p className="mb-1">
                                    <b>Type</b>: &nbsp;
                                    {post.type === "sale_residential"
                                      ? "Seller Residential"
                                      : ""}
                                    {post.type === "sale_commercial"
                                      ? "Seller Commercial"
                                      : ""}
                                    {post.type === "sale_investment"
                                      ? "Seller Investment"
                                      : ""}
                                    {post.type === "sale_referral"
                                      ? "Seller Referral"
                                      : ""}
                                    {post.type === "sale_lease"
                                      ? "Seller Lease"
                                      : ""}
                                      </p>
                                  </>
                                ) : (
                                  ""
                                )}

                                {post.represent === "both" ? (
                                  <>
                                    <p className="mb-1">
                                    <b>Type</b>: &nbsp;
                                    {post.type === "both_residential"
                                      ? "Buyer & Seller Residential"
                                      : ""}
                                    {post.type === "both_commercial"
                                      ? "Buyer & Seller Commercial"
                                      : ""}
                                    {post.type === "both_investment"
                                      ? "Buyer & Seller Investment"
                                      : ""}
                                    {post.type === "both_land"
                                      ? "Buyer & Seller Land"
                                      : ""}
                                    {post.type === "both_referral"
                                      ? "Buyer & Seller Referral"
                                      : ""}
                                    {post.type === "both_lease"
                                      ? "Buyer & Seller Lease"
                                      : ""}
                                      </p>
                                  </>
                                ) : (
                                  ""
                                )}

                                {post.represent === "referral" ? (
                                  <>
                                    <p className="mb-1">
                                    <b>Type</b>: &nbsp;
                                    {post.type === "referral_residential"
                                      ? "Residential Referral"
                                      : ""}
                                    {post.type === "referral_commercial"
                                      ? "Commercial Referral"
                                      : ""}
                                    {post.type === "referral_investment"
                                      ? "Investment Referral"
                                      : ""}
                                    {post.type === "referral_land"
                                      ? "Land Referral"
                                      : ""}
                                    {post.type === "referral_lease"
                                      ? "Lease Referral"
                                      : ""}
                                    </p>
                                  </>
                                ) : (
                                  ""
                                )}

                                {post.phase ? (
                                  <p className="mb-1">
                                    <b>Phase</b>: &nbsp;
                                    {post.phase === "pre-listed"
                                      ? "Pre-Listed"
                                      : ""}
                                    {post.phase === "active-listing"
                                      ? "Active Listing"
                                      : ""}
                                    {post.phase === "showing-homes"
                                      ? "Showing homes"
                                      : ""}
                                    {post.phase === "under-contract"
                                      ? "Under Contract"
                                      : ""}
                                    {post.phase === "closed" ? "Closed" : ""}
                                    {post.phase === "canceled" ? "Canceled" : ""}
                                  </p>
                                ) : (
                                  "Not Set"
                                )}

                                {/* <p className="mb-1 fs-sm text-muted">
                                  Phase: {post.phase ?? "Not Set"}
                                </p> */}
                                <p className="mb-1">
                                <b>Funding</b>: &nbsp;
                                  {post.funding != "" ? post.funding : "Not Set"}
                                </p>
                              </div>
                              <br/>
                              <NavLink
                                to={`/transaction-review/${post._id}`}
                                type="button"
                                className="btn btn-primary btn-sm"
                                id="save-button">
                                Review
                              </NavLink>
                            </div>
                          </div>
                        </div>
                      ) : (
                        <>
                          {/* <div className="card card-hover card-horizontal transactions border-0 shadow-sm mb-4 p-3">
                                                    <a className="card-img-top" onClick={(e) => Catalog(post._id)}>
                                                        <img className="img-fluid w-100" src="https://integra.workspace.lwolf.com/common/handler/mls/photo_custom.ashx?s=1&t=5eada3e3-491e-46e2-938c-7c785a1f9c68&r=Seller" alt="Property"  onError={e => propertypic(e)} />
                                                    </a>
                                                    <div className="card-body position-relative pb-0 pt-0">
                                                        <p className="mb-2 fs-sm text-muted">Property Not Set</p>
                                                        {
                                                        post.contact1 && post.contact1.length > 0 ? (
                                                                post.contact1[0] !== null ? (
                                                                    <>
                                                                    <p className="mb-2 fs-sm text-muted" key={post.contact1[0]._id}>  
                                                                        {
                                                                            post.contact1[0].data.organization && post.contact1[0].data.organization !== "" ?
                                                                                post.contact1[0].data.organization
                                                                            :
                                                                                post.contact1[0].data.firstName
                                                                        }&nbsp;{post.contact1[0].data.lastName}
                                                                    </p>
                                                                    <p className="mb-2 fs-sm text-muted">{post.contact1[0].data.firstName
                                                                        }&nbsp;{post.contact1[0].data.lastName}</p>
                                                                    </>
                                                                ) : null
                                                        ) : (
                                                            <p></p>
                                                        )}
                                                        <p className="mb-2 fs-sm text-muted"><i className="fa fa-home" aria-hidden="true"></i> {post.represent}</p>
                                                    </div>
                                                </div> */}
                        </>
                      )}
                    </React.Fragment>
                  ))
                ) : (
                  <p className="text-center mt-5">No Any Active Transacion</p>
                )}

                {pageCount > 1 ? (
                  <div className="justify-content-end mb-1">
                    <ReactPaginate
                      previousLabel={"Previous"}
                      nextLabel={"next"}
                      breakLabel={"..."}
                      pageCount={pageCount}
                      marginPagesDisplayed={1}
                      pageRangeDisplayed={2}
                      onPageChange={handlePageClick}
                      containerClassName={"pagination justify-content-center"}
                      pageClassName={"page-item"}
                      pageLinkClassName={"page-link"}
                      previousClassName={"page-item"}
                      previousLinkClassName={"page-link"}
                      nextClassName={"page-item"}
                      nextLinkClassName={"page-link"}
                      breakClassName={"page-item"}
                      breakLinkClassName={"page-link"}
                      activeClassName={"active"}
                    />
                  </div>
                ) : (
                  ""
                )}
              </div>
            </div>
          </div>
        </div>
      </main>
    </div>
  );
}

export default TransactFundingReport;

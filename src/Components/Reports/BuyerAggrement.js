import React, { useState, useEffect } from "react";
import Pic from "../img/pic.png";
import user_service from "../service/user_service";
import ReactPaginate from "react-paginate";
import Loader from "../../Pages/Loader/Loader.js";
import Toaster from "../../Pages/Toaster/Toaster.js";
import { NavLink} from "react-router-dom";

import { Document, Page } from "react-pdf";
import jwt from "jwt-decode";
import Swal from "sweetalert2";
import "sweetalert2/dist/sweetalert2.css";
function BuyerAggrement() {
    const [contacts, setContacts] = useState([]);
  const [pageCount, setPageCount] = useState(0);
  const [lastLogin, setLastLogin] = useState(""); // Default lastLogin filter
  const [activeTab, setActiveTab] = useState("all");
  const [loader, setLoader] = useState({ isActive: null });
  const { isActive } = loader;
  const [toaster, setToaster] = useState({
    types: null,
    isShow: null,
    toasterBody: "",
    message: "",
  });
  const { types, isShow, toasterBody, message } = toaster;
  useEffect(() => {
    window.scrollTo(0, 0);
    ContactGetAll()
  }, [activeTab]);
// const ContactGetAll = async () => {
//     setLoader({ isActive: true })
//     await user_service.contactBuyerAggrement(1).then((response) => {
//         setLoader({ isActive: false })
//         if (response) {
//             setContacts(response.data.data);
//             setPageCount(Math.ceil(response.data.totalRecords / 10));
//         }
//     });
// }

const ContactGetAll = async () => {
  setLoader({ isActive: true });

  await user_service.contactBuyerAggrement(1).then((response) => {
    setLoader({ isActive: false });

    if (response) {
      let filteredContacts = response.data.data;

      // Filter based on activeTab
      if (activeTab === "approved") {
        filteredContacts = filteredContacts.filter(contact => contact.buyer_document_status === "yes");
      } else if (activeTab === "rejected") {
        filteredContacts = filteredContacts.filter(contact => contact.buyer_document_status === "no");
      } else {
        filteredContacts = filteredContacts.filter(contact => contact.buyer_document_status !== "yes" && contact.buyer_document_status !== "no");
      }

      setContacts(filteredContacts);
      setPageCount(Math.ceil(response.data.totalRecords / 10));
    }
  });
};

// Handle tab click
const handleTabChange = (tab) => {
  setActiveTab(tab);
};

const handlePageClick = async (data) => {
  let currentPage = data.selected + 1;
  setLoader({ isActive: true });

  await user_service.contactBuyerAggrement(currentPage).then((response) => {
    setLoader({ isActive: false });

    if (response) {
      let filteredContacts = response.data.data;

      // Apply filtering based on active tab
      if (activeTab === "approved") {
        filteredContacts = filteredContacts.filter(contact => contact.buyer_document_status === "yes");
      } else if (activeTab === "rejected") {
        filteredContacts = filteredContacts.filter(contact => contact.buyer_document_status === "no");
      } else {
        filteredContacts = filteredContacts.filter(contact => contact.buyer_document_status !== "yes" && contact.buyer_document_status !== "no");
      }

      setContacts(filteredContacts);
      setPageCount(Math.ceil(response.data.totalRecords / 10));
    }
  });
};


  const handleApproveContact = async (id, status) => {
    Swal.fire({
      title: "Are you sure?",
      text:
        status == "yes" ? "You want to Approved." : "You want to Reject.",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText:
        status == "yes" ? "Yes, Approve it!" : "Yes, Reject it!",
    }).then(async (result) => {
      if (result.isConfirmed) {
        try {
          if (id) {
            const userData = {
            buyer_document_status: status,
            };

            var msg;
            if (status == "yes") {
              msg = "Contact Approved Successfully.";
            } else {
              msg = "Contact Rejected Successfully.";
            }
            setLoader({ isActive: true });
            await user_service.contactUpdate(id, userData).then((response) => {
              if (response) {
                setLoader({ isActive: false })
                Swal.fire("Done!", msg, "success");

                ContactGetAll();
              } else {
                Swal.fire("Error!", response.data.messagesg, "error");
              }
            });
          }
        } catch (error) {
          Swal.fire("Error", "An error occurred.", "error");
          console.error(error);
        }
      }
    });
  };
 
  const formatDate = (dateString) => {
    const date = new Date(dateString);
    const options = {
      year: 'numeric',
      month: '2-digit',
      day: '2-digit',
      hour: '2-digit',
      minute: '2-digit',
      second: '2-digit',
    };
    return new Intl.DateTimeFormat('en-US', options).format(date);
  };
  return (
    <div className="bg-secondary float-left w-100 pt-4 mb-4">
      <Loader isActive={isActive} />
      {isShow && (
        <Toaster
          types={types}
          isShow={isShow}
          toasterBody={toasterBody}
          message={message}
        />
      )}
      <main className="page-wrapper">
        <div className="content-overlay">
          <div className="">
            <div className="row">
              
              <div className="col-md-12">
                <div className="float-left w-100 d-flex align-items-center justify-content-between mb-4">
                  <h3 className="text-white mb-0">Buyer-Broker Agreements Document</h3>
                </div>
                <div className="d-flex mb-3">
                  <button 
                      className={`btn ${activeTab === "all" ? "btn-primary" : "btn-secondary"}`} 
                      onClick={() => handleTabChange("all")}
                  >
                      Other Document Listing
                  </button>
                  <button 
                      className={`btn ms-5 ${activeTab === "approved" ? "btn-primary" : "btn-secondary"}`} 
                      onClick={() => handleTabChange("approved")}
                  >
                      Approved Document Listing
                  </button>

                  <button 
                      className={`btn ms-5 ${activeTab === "rejected" ? "btn-primary" : "btn-secondary"}`} 
                      onClick={() => handleTabChange("rejected")}
                  >
                      Reject Document Listing
                  </button>
                </div>

                {/* <!-- Item--> */}
                {contacts && contacts.length > 0 ? (
                  contacts.map((post) => (
                        <div className="card card-hover mb-2">
                          <div className="card-body">
                            <div className="float-left w-100">
                              <div className="float-left w-100 d-lg-flex d-md-flex d-inline-block align-items-center justify-content-between">
                                <div
                                  className="float-left w-100 d-flex align-items-center justify-content-start"
                                  key={post._id}
                                >
                                  <div className="ms-3">
                                    {
                                        post.buyers_name ?
                                    <p className="mb-1">
                                      <strong>Buyers Name’s:</strong> {post.buyers_name}   
                                    </p>
                                    :""
                                    }

                                   {
                                    post.buyer_dateTime ? 
                                    <p className="mb-1">
                                      <strong>Expires Date and time:</strong> {formatDate(post.buyer_dateTime)}   
                                    </p>
                                    :""
                                   }

                                   {
                                    post.buyer_broker_fee ?
                                    <p className="mb-1">
                                      <strong>Buyer Brokerage Fee:</strong> {post.brokerage_Fee==="doller-percentage"? "$":""}{post.buyer_broker_fee} {post.brokerage_Fee==="fee-percentage"? "%":""}
                                    </p>
                                    :""
                                   }

                              <badge
                              className="mx-4"
                              style={{ fontSize: "18px" }}
                            >
                              {post.contactType
                                ? post.contactType.charAt(0).toUpperCase() +
                                  post.contactType.slice(1)
                                : "Not Assigned"}
                            </badge>
                                  </div>
                                </div>
                                <div className="float-left w-100 d-flex align-items-center justify-content-between my-lg-0 my-md-0 my-sm-3 my-3">
                            {post.buyer_document ? (
                            <>
                                {/\.(png|jpg|jpeg)$/i.test(post.buyer_document) ? (
                                    <a
                                      className="pull-left mr-3"
                                      style={{ width: "100px" }}
                                      href={post.buyer_document}
                                      target="_blank"
                                      rel="noopener noreferrer"
                                      download=""
                                    >
                                      {/* Display image preview */}
                                      <img
                                        src={post.buyer_document}
                                        alt="Uploaded File"
                                        style={{ width: "100%", height: "auto", objectFit: "cover" }}
                                      />
                                    </a>
                                 ) : (
                                    <a
                                    className="pull-left mr-3"
                                    style={{ width: "100px" }}
                                      href={post.buyer_document}
                                      target="_blank"
                                      rel="noopener noreferrer"
                                      download=""
                                    >
                                      {/* Render PDF file */}
                                      <Document
                                        className="document_filePersonal"
                                        file={post.buyer_document}
                                        renderMode="canvas"
                                      >
                                        <Page pageNumber={1} />
                                      </Document>
                                    </a>
                                  )}
                       
                            </>
                          ) : (
                            ""
                            )}
                               {localStorage.getItem("auth") &&
                                    jwt(localStorage.getItem("auth"))
                                      .contactType == "admin"  ? (
                                      <>
                                      {
                                        post.buyer_document_status ==="yes" ?
                                        <span style={{fontSize:"20px", color:"green"}}>Document Approved<i class="fi-check text-success ms-2"></i></span>
                                        :
                                        <span className="ms-4">
                                          <button
                                            type="button"
                                            onClick={() =>
                                              handleApproveContact(
                                                post._id,
                                                "yes"
                                              )
                                            }
                                            className="btn btn-translucent-primary"
                                          >
                                            Approve
                                          </button>
                                        </span>
                                      }

                                        <span className="ms-4">
                                          <button
                                            type="button"
                                            onClick={() =>
                                              handleApproveContact(
                                                post._id,
                                                "no"
                                              )
                                            }
                                            className="btn btn-translucent-primary"
                                          >
                                            Reject
                                          </button>
                                        </span>
                                      </>
                                    ) : (
                                      ""
                                    )}
                                  <span className="">
                                    <NavLink
                                      to={`/private-contact/${post._id}`}
                                      className=""
                                    >
                                      <i className="fi-info-circle fs-5 mt-n1 me-2"></i>
                                    </NavLink>
                                  </span>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                  ))
                ) : (
                  <div className="text-center mb-3">
                    <p className="mt-3 text-white">Data not Available</p>
                  </div>
                )}
              </div>
              {pageCount && pageCount > 1 ? (
                <div className="d-flex align-items-center justify-content-end mb-0 mt-4">
                  <ReactPaginate
                    className=""
                    previousLabel={"Previous"}
                    nextLabel={"Next"}
                    breakLabel={"..."}
                    pageCount={pageCount}
                    marginPagesDisplayed={1}
                    pageRangeDisplayed={2}
                    onPageChange={handlePageClick}
                    containerClassName={
                      "pagination justify-content-center mb-0"
                    }
                    pageClassName={"page-item"}
                    pageLinkClassName={"page-link"}
                    previousClassName={"page-item"}
                    previousLinkClassName={"page-link"}
                    nextClassName={"page-item"}
                    nextLinkClassName={"page-link"}
                    breakClassName={"page-item"}
                    breakLinkClassName={"page-link"}
                    activeClassName={"active"}
                  />
                </div>
              ) : (
                ""
              )}
            </div>
          </div>
        </div>
      </main>
    </div>
  );
}
export default BuyerAggrement;

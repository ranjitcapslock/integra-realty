import React, { useState, useEffect } from "react";
import Pic from "../img/pic.png";
import Modal from "../../Modal";
import user_service from "../service/user_service";
import ReactPaginate from "react-paginate";
import Loader from "../../Pages/Loader/Loader.js";
import Toaster from "../../Pages/Toaster/Toaster.js";
import { NavLink, useNavigate, useParams } from "react-router-dom";
import Tasks from "../img/task.png";
import PagesNavigationUI from "../../Pages/PagesNavigationUI.js";
import jwt from "jwt-decode";
import Draggable from "react-draggable";
import AssocTasks from "./AssocTasks.js";
import DateTimePicker from "react-datetime-picker";
import "react-datetime-picker/dist/DateTimePicker.css";

function Task() {
  const [loader, setLoader] = useState({ isActive: null });
  const { isActive } = loader;
  const [toaster, setToaster] = useState({
    types: null,
    isShow: null,
    toasterBody: "",
    message: "",
  });
  const { types, isShow, toasterBody, message } = toaster;

  const initialValues = {
    your_answer: "",
    issue: "",
    comments: [
      {
        comment: "",
      },
    ],
    taskTime: "",
    taskDate: "",
  };

  const navigate = useNavigate();

  const [taskData, setTaskData] = useState("");
  const [taskDataAssociate, setTaskDataAssociate] = useState("");

  const [taskAssociate, setTaskAssociate] = useState(false);
  

  const [taskIssue, setTaskIssue] = useState(false);
  const [taskAdjust, setTaskAdjust] = useState(false);
  const [checkRadio, setCheckRadio] = useState("");
  const [isOpen, setIsOpen] = useState(false);
  const [reOpen, setReOpen] = useState(false);

  const [reCallTask, setReCallTask] = useState(false);
  const [closeTask, setCloseTask] = useState(false);

  const [profile, setProfile] = useState("");

  // const [timeDifference, setTimeDifference] = useState(null);
  // const params = useParams();

  const [formValues, setFormValues] = useState(initialValues);
  const [noticereceived, setNoticereceived] = useState([]);
  const [noticesend, setNoticesend] = useState([]);
  const [tabSelected, setTabSelected] = useState("personal");
  const [pageCount, setPageCount] = useState(0);

  const handleChange = (e) => {
    const { name, value } = e.target;
    setFormValues({ ...formValues, [name]: value });
  };

  const handleDateChange = (value) => {
    setFormValues({
      ...formValues,
      taskDate: value
    });
  };

  const handleChangeComment = (event) => {
    const { name, value } = event.target;

    if (name.startsWith("comments[")) {
      const index = parseInt(name.match(/\[(\d+)\]/)[1], 10);
      const updatedComments = [...formValues.comments];
      updatedComments[index].comment = value;

      setFormValues({
        ...formValues,
        comments: updatedComments,
      });
    } else {
      setFormValues({
        ...formValues,
        [name]: value,
      });
    }
  };

  const handleSend = (send) => {
    if (send === "toAssociate") {
      setTabSelected(send);
      assocTaskGetAll();
    }
    if (send === "AssocTasks") {
      setTabSelected(send);
    }
  };

  const itemsPerPage = 10;
  const assocTaskGetAll = async (page = 1) => {
    if (localStorage.getItem("auth")) {
      setLoader({ isActive: true });

      const userid = jwt(localStorage.getItem("auth")).id;
      const query = `?page=${page}&recipients=${userid}`;

      try {
        const response = await user_service.assocTaskGet(query);

        if (response && response.data && response.data.data) {
          const taskData = response.data.data;

          // Filter data based on tab selection
          const filteredTasks =
            tabSelected === "personal"
              ? taskData.filter((task) => task.taskType === "personal")
              : taskData.filter((task) => task.taskType === "toAssociate");

          if (tabSelected === "personal") {
            setNoticereceived(filteredTasks);
          } else {
            setNoticesend(filteredTasks);
          }

          setPageCount(Math.ceil(response.data.totalRecords / itemsPerPage));
          setLoader({ isActive: false });
        }
      } catch (error) {
        setLoader({ isActive: false });
        console.error("Error fetching tasks:", error);
      }
    }
  };

  const handlePageClick = async (data) => {
    const selectedPage = data.selected + 1;
    await assocTaskGetAll(selectedPage); // Call assocTaskGetAll with the selected page
  };

  useEffect(() => {
    assocTaskGetAll(); // Fetch tasks when the component mounts or tabSelected changes
  }, [tabSelected]);

  //  const assocTaskGetAll = async () => {
  //       if (localStorage.getItem("auth")) {
  //         setLoader({ isActive: true });
  //         const userid = jwt(localStorage.getItem("auth")).id;
  //         var query = `?recipients=${userid}`;
  //         await user_service.assocTaskGet(query).then((response) => {
  //           if (response) {
  //             setLoader({ isActive: false });
  //             // persoal this useState data add
  //             setNoticereceived(response.data.data);

  //             // Associate  this useState data add
  //             setNoticesend()
  //           }
  //         });
  //       }
  //     };

  // const assocTaskGetAll = async () => {
  //   if (localStorage.getItem("auth")) {
  //     setLoader({ isActive: true });

  //     const userid = jwt(localStorage.getItem("auth")).id;
  //     const query = `?page=1&recipients=${userid}`;

  //     try {
  //       const response = await user_service.assocTaskGet(query);

  //       if (response && response.data && response.data.data) {
  //         setLoader({ isActive: false });

  //         const taskData = response.data.data;

  //         // Filter data by taskType
  //         const personalTasks = taskData.filter(
  //           (task) => task.taskType === "personal"
  //         );
  //         const associateTasks = taskData.filter(
  //           (task) => task.taskType === "toAssociate"
  //         );

         
  //         setNoticereceived(personalTasks);
  //         setNoticesend(associateTasks);
  //       setPageCount(Math.ceil(response.data.totalRecords / 10));
  //       }
  //     } catch (error) {
  //       setLoader({ isActive: false });
  //       console.error("Error fetching tasks:", error);
  //     }
  //   }
  // };

  // const handlePageClick = async (data) => {
  //   try {
  //     const userid = jwt(localStorage.getItem("auth")).id;
  //     const currentPage = data.selected + 1;
  //     setLoader({ isActive: true });
  //     const queryParams = `?page=${currentPage}&recipients=${userid}`;
  //     const response = await user_service.assocTaskGet(queryParams);

  //     setLoader({ isActive: false });

  //     if (response && response.data) {
  //       setNoticereceived(response.data.data);
  //       setPageCount(Math.ceil(response.data.totalRecords / 10));
  //     }
  //   } catch (error) {
  //     console.error("Error in handlePageClick:", error);
  //     // Handle errors as needed
  //   }
  // };

  // useEffect(() => {
  //   assocTaskGetAll();
  // }, [tabSelected]);

  const handlePersonal = (personals) => {
    if (personals === "personal") {
      setTabSelected(personals);
      assocTaskGetAll();
    }
  };



  useEffect(() => {
    window.scrollTo(0, 0);
    const profileGetAll = async () => {
      setLoader({ isActive: true });
      await user_service
        .profileGet(jwt(localStorage.getItem("auth")).id)
        .then((response) => {
          if (response) {
            setLoader({ isActive: false });
            // console.log(response.data);
            setProfile(response.data);
          }
        });
    };
    profileGetAll();
  }, []);

  // const getTimeDifference = (createdDate) => {
  //   try {
  //     const currentDate = new Date();
  //     const taskDate = new Date(createdDate);

  //     // Check if taskDate is a valid date
  //     if (isNaN(taskDate)) {
  //       return "Invalid date";
  //     }

  //     const diffInMilliseconds = currentDate - taskDate;
  //     const diffInMinutes = Math.floor(diffInMilliseconds / 60000);
  //     const hours = Math.floor(diffInMinutes / 60);
  //     const days = Math.floor(hours / 24);
  //     const months = Math.floor(days / 30);
  //     const years = Math.floor(months / 12);

  //     if (years > 0) {
  //       return `${years} year${years > 1 ? "s" : ""} ago`;
  //     } else if (months > 0) {
  //       return `${months} month${months > 1 ? "s" : ""} ago`;
  //     } else if (days > 0) {
  //       return `${days} day${days > 1 ? "s" : ""} ago`;
  //     } else if (hours > 24) {
  //       const remainingHours = hours - Math.floor(hours / 24) * 24;
  //       return `1 day, ${remainingHours} hr${remainingHours > 1 ? "s" : ""} ago`;
  //     } else {
  //       return `${hours} hr${hours > 1 ? "s" : ""}, ${diffInMinutes % 60} min${
  //         diffInMinutes % 60 > 1 ? "s" : ""
  //       } ago`;
  //     }
  //   } catch (error) {
  //     console.error("Error in getTimeDifference:", error);
  //     return "Error calculating time difference";
  //   }
  // };

  const getTimeDifference = (createdDate) => {
    const currentDate = new Date();
    const diffInMilliseconds = currentDate - new Date(createdDate);
    const diffInMinutes = Math.floor(diffInMilliseconds / 60000);
    const hours = Math.floor(diffInMinutes / 60);
    const days = Math.floor(hours / 24);
    const months = Math.floor(days / 30);
    const years = Math.floor(months / 12);

    if (years > 0) {
      return `${years} year${years > 1 ? "s" : ""}`;
    } else if (months > 0) {
      return `${months} month${months > 1 ? "s" : ""}`;
    } else if (days > 0) {
      return `${days} day${days > 1 ? "s" : ""}`;
    } else if (hours > 24) {
      const remainingHours = hours - Math.floor(hours / 24) * 24;
      return `1 day, ${remainingHours} hr${remainingHours > 1 ? "s" : ""}`;
    } else {
      return `${hours} hr${hours > 1 ? "s" : ""}, ${diffInMinutes % 60} min${
        diffInMinutes % 60 > 1 ? "s" : ""
      }`;
    }
  };

  const handleClick = async (id) => {
    try {
      if (id) {
        const response = await user_service.assocTaskGetId(id);

        if (response && response.data) {
          setTaskData(response.data);
        }
      }
    } catch (error) {
      console.error("Error fetching data:", error);
      // Handle errors as needed
    }
  };

  const handleClickAssociate = async (id) => {
    try {
      if (id) {
        const response = await user_service.assocTaskGetId(id);

        if (response && response.data) {
          setTaskDataAssociate(response.data);
        }
      }
    } catch (error) {
      console.error("Error fetching data:", error);
      // Handle errors as needed
    }
  };

  const handleClose = () => {
    setTaskData("");
    setTaskDataAssociate("")
  };

  const handleRecallTask = () => {
    setReCallTask(true);
  };

  const handleCloseTask = () => {
    setTaskAssociate(true);
    setCloseTask(false);
  };

  const handleClosedate = () => {
    setTaskAdjust("");
  };
  const handleCommentsModal = () => {
    setIsOpen(true);
  };
  const handleCloseComment = () => {
    setIsOpen("");
  };

  const handleIssue = () => {
    setTaskIssue(true);
  };

  const handleCancelIssu = () => {
    setTaskIssue("");
  };

  const handleAdjust = () => {
    setTaskAdjust(true);
  };

  const handleReOpen = () => {
    setReOpen(true);
  };

  const handleIssu = async (id) => {
    if (id) {
      const userData = {
        issue: formValues.issue,
      };
      try {
        setLoader({ isActive: true });
        const response = await user_service.assocTaskGetUpdate(id, userData);
        setTaskData("") 
        setTaskDataAssociate("")  
        setFormValues(response.data);
        setTaskIssue("");
        setLoader({ isActive: false });
        setToaster({
          type: "Task Updated Successfully",
          isShow: true,
          message: "Task Updated Successfully",
        });
        setTimeout(() => {
          setToaster((prevToaster) => ({ ...prevToaster, isShow: false }));
          // navigate(`/contact-profile/${params.id}`);
        }, 3000);

        assocTaskGetAll();
      } catch (error) {
        setLoader({ isActive: false });
        setToaster({
          types: "error",
          isShow: true,
          message: "Error",
        });
        setTimeout(() => {
          setToaster((prevToaster) => ({ ...prevToaster, isShow: false }));
        }, 3000);
      }
    }
  };

  const handleComment = async (id) => {
    if (id) {
      const userData = {
        comments: formValues.comments,
      };
      console.log(userData);
      // try {
      //   setLoader({ isActive: true });
      //   const response = await user_service.assocTaskGetUpdate(id, userData);
      //   console.log(response);
      //   setFormValues(response.data);
      //   setIsOpen("");
      //   setLoader({ isActive: false });
      //   setToaster({
      //     type: "Task Updated Successfully",
      //     isShow: true,
      //     toasterBody: response.data.message,
      //     message: "Task Updated Successfully",
      //   });
      //   setTimeout(() => {
      //     setToaster((prevToaster) => ({ ...prevToaster, isShow: false }));
      //     // navigate(`/contact-profile/${params.id}`);
      //   }, 3000);

      //   assocTaskGetAll();
      // } catch (error) {
      //   setLoader({ isActive: false });
      //   setToaster({
      //     types: "error",
      //     isShow: true,
      //     toasterBody: error.response
      //       ? error.response.data.message
      //       : error.message,
      //     message: "Error",
      //   });
      //   setTimeout(() => {
      //     setToaster((prevToaster) => ({ ...prevToaster, isShow: false }));
      //   }, 3000);
      // }
    }
  };

  const handleAdjustDate = async (id) => {
    if (id) {
      const userData = {
        taskDate: formValues.taskDate,
        // taskTime: formValues.taskTime,
      };
      console.log(userData);
      try {
        setLoader({ isActive: true });
        const response = await user_service.assocTaskGetUpdate(id, userData);
        setFormValues(response.data);
        setTaskData("")  
       setTaskDataAssociate("")
        setLoader({ isActive: false });
        setToaster({
          type: "Task Updated Successfully",
          isShow: true,
          message: "Task Updated Successfully",
        });
        setTimeout(() => {
          setToaster((prevToaster) => ({ ...prevToaster, isShow: false }));
          // navigate(`/contact-profile/${params.id}`);
        }, 3000);

        assocTaskGetAll();
      } catch (error) {
        setLoader({ isActive: false });
        setToaster({
          types: "error",
          isShow: true,
          message: "Error",
        });
        setTimeout(() => {
          setToaster((prevToaster) => ({ ...prevToaster, isShow: false }));
        }, 3000);
      }
    }
  };

  const handleCheck = (e) => {

    setCheckRadio(e.target.name);
    setCheckRadio(e.target.value);
  };

  const handleResponce = async (id) => {
    if (id) {
      const userData = {
        your_answer: checkRadio ? "Done" : "",
      };
      try {
        setLoader({ isActive: true });
        const response = await user_service.assocTaskGetUpdate(id, userData);
        setTaskData("") 
        setTaskDataAssociate("")
        setFormValues(response.data);
        setLoader({ isActive: false });
        setToaster({
          type: "Task Updated Successfully",
          isShow: true,
          message: "Task Updated Successfully",
        });
        setTimeout(() => {
          setToaster((prevToaster) => ({ ...prevToaster, isShow: false }));
          // navigate(`/contact-profile/${params.id}`);
        }, 3000);

        assocTaskGetAll();
      } catch (error) {
        setLoader({ isActive: false });
        setToaster({
          types: "error",
          isShow: true,
          message: "Error",
        });
        setTimeout(() => {
          setToaster((prevToaster) => ({ ...prevToaster, isShow: false }));
        }, 3000);
      }
    }
  };
  return (
    <div className="bg-secondary float-left w-100 pt-4 mb-4">
      <Loader isActive={isActive} />
      {isShow && (
        <Toaster
          types={types}
          isShow={isShow}
          toasterBody={toasterBody}
          message={message}
        />
      )}
      <div className="">
        <Modal />
        {/* <!-- Page container--> */}
        {/* <!-- Breadcrumb--> */}
        <main className="page-wrapper homepage_layout">
          {/* <!-- Page card like wrapper--> */}

          <div className="float-left w-100 d-flex align-items-center justify-content-between">
            <h3 className="text-white w-100 mb-0">My Tasks</h3>
          </div>
          <ul
            className="nav nav-tabs d-flex align-items-center 
          align-items-sm-center justify-content-center mb-2"
            role="tablist"
          >
            <li className="nav-item me-sm-3 mb-3" role="presentation">
              <a
                className={`nav-link text-center ${
                  tabSelected === "personal" ? "active" : ""
                }`}
                onClick={() => handlePersonal("personal")}
              >
                Personal
              </a>
            </li>

            {localStorage.getItem("auth") &&
              jwt(localStorage.getItem("auth")).contactType === "admin" && (
                <>
                  <li className="nav-item mb-3" role="presentation">
                    <a
                      className={`nav-link text-center`}
                      onClick={() => handleSend("AssocTasks")}
                    >
                      Transaction Review Requests
                    </a>
                  </li>
                </>
              )}

            <li className="nav-item mb-3" role="presentation">
              <a
                className={`nav-link text-center ${
                  tabSelected === "toAssociate" ? "active" : ""
                }`}
                onClick={() => handleSend("toAssociate")}
              >
                Send Tasks
              </a>
            </li>
          </ul>
          {tabSelected === "personal" && (
            <div className="row">
              <div className="col-md-12">
                <h6 className="text-white">{noticereceived.length} Task</h6>
              </div>
              <div className="col-md-8">
                {noticereceived && noticereceived.length > 0 ? (
                  noticereceived.map((dataItem, index) => (
                    <div
                      className="notes-main bg-light border-top border-0 p-3 rounded-3"
                      key={index}
                      onClick={() => handleClick(dataItem._id)}
                    >
                      <div className="row">
                        
                        <div className="col-md-6">
                          <div className="d-flex align-items-center justify-content-start">
                            <img
                              className="rounded-circle image-note mt-3 mb-3"
                              src={dataItem.fromImage || profile.image  || Pic }
                              alt="image"
                              width="75"
                              height="75"
                            />
                            <div className="ms-3">
                              <strong>{dataItem.taskTitle}</strong>
                              <p id="" className="mb-0">
                                From:{" "}
                                {dataItem.from
                                  ? dataItem.from
                                  : `${profile.firstName} ${profile.lastName}`}
                                {/* From: {profile.firstName} {profile.lastName} */}
                              </p>
                            </div>
                          </div>
                        </div>
                        <div className="col-md-6 d-flex align-items-center justify-content-start">
                          <div className="">
                            <p className="mb-0 w-100">
                                  Due:{" "}
                                  {dataItem?.taskDate ? (
                                    <>
                                      {new Date(
                                        dataItem.taskDate
                                      ).toLocaleDateString("en-US", {
                                        month: "2-digit", // Ensure month is two digits
                                        day: "2-digit", // Ensure day is two digits
                                        year: "numeric",
                                      })}{" "}
                                      {new Date(
                                        dataItem.taskDate
                                      ).toLocaleTimeString("en-US", {
                                        hour: "numeric",
                                        minute: "2-digit",
                                        hour12: true, // Use 12-hour clock
                                      })}
                                      {/* {new Date(
                                          dataItem.taskDate
                                        ).toLocaleString("en-US", {
                                          month: "short",
                                          day: "numeric",
                                          year: "numeric",
                                          hour: "numeric",
                                          minute: "numeric",
                                        })} */}
                                      <br />
                                      <strong>
                                        Sent:{" "}
                                        {dataItem?.taskDate
                                          ? getTimeDifference(
                                              dataItem?.taskDate
                                            )
                                          : "--:--:--"}{" "}
                                        ago
                                      </strong>
                                    </>
                                  ) : (
                                    "No date available"
                                  )}
                            </p>
                          </div>
                        </div>
                       
                      </div>
                    </div>
                  ))
                ) : (
                  <div className="bg-light border rounded-3 p-3">
                    <p className="text-center my-5">No tasks found.</p>
                  </div>
                )}

                <div className="pull-right mt-4">
                  <NavLink
                    to="/add-tasks/"
                    type="button"
                    className="btn btn-info btn-lg pull-right"
                  >
                    Add a Task
                  </NavLink>
                </div>
              </div>

              {taskData ? (
                <div className="col-md-4">
                  <div className="bg-light border rounded-3 p-3 float-left d-inline-block w-100">
                    <div className="pull-right">
                      <button
                        onClick={handleClose}
                        className="btn-close ms-5"
                        type="button"
                        data-dismiss="modal"
                        aria-label="Close"
                      ></button>
                    </div>
                    <div className="notes-main bg-light m-0 border-0">
                      <div className="row">
                        <div className="col-md-12">
                          <div className="d-flex align-items-center justify-content-start">
                            <img
                              className="rounded-circle image-note  mb-3"
                              // src={profile.image}
                              src={taskData.fromImage || profile.image  || Pic }
                              alt="image"
                              width="75"
                              height="75"
                            />

                            <div className="ms-3">
                              <strong className="">
                                From:{" "}
                                {taskData.from
                                  ? taskData.from
                                  : `${profile.firstName} ${profile.lastName}`}
                              </strong>
                              <p id="" className="mb-0">
                                To: {profile.firstName} {profile.lastName}
                              </p>
                            </div>
                          </div>
                        </div>

                        <hr />

                        <div className="col-md-12 d-flex align-items-center justify-content-start my-2">
                          <div className="">
                            
                            <p className="mb-0 w-100">
                              <small>
                                Due:
                                {new Date(
                                  taskData?.taskDate
                                ).toLocaleDateString("en-US", {
                                  month: "2-digit", // Ensure month is two digits
                                  day: "2-digit", // Ensure day is two digits
                                  year: "numeric",
                                })}{" "}
                                {new Date(
                                  taskData?.taskDate
                                ).toLocaleTimeString("en-US", {
                                  hour: "numeric",
                                  minute: "2-digit",
                                  hour12: true, // Use 12-hour clock
                                })}
                                <br />
                                <strong>
                                  Sent:{" "}
                                  {taskData?.taskDate
                                    ? getTimeDifference(taskData?.taskDate)
                                    : "--:--:--"}{" "}
                                  ago
                                </strong>
                                <NavLink
                                  onClick={handleAdjust}
                                  className="pull-right ms-5"
                                >
                                  Adjust
                                </NavLink>
                                {taskAdjust && (
                                  <Draggable>
                                    <div
                                      className="bg-light border rounded-3 p-3 float-left"
                                      style={{
                                        position: "absolute",
                                        background: "#fff",
                                        zIndex: 1000,
                                      }}
                                    >
                                      <div className="d-flex align-items-center justify-content-between">
                                        <h6 className="modal-title pt-0">
                                          Adjust Date
                                        </h6>
                                        <button
                                          onClick={handleClosedate}
                                          className="btn-close"
                                          type="button"
                                          data-dismiss="modal"
                                          aria-label="Close"
                                        ></button>
                                      </div>

                                      <div className="float-left w-100 mt-3">
                                        <p className="mb-0 w-100">
                                          Originally due:
                                          {new Date(
                                            taskData?.taskDate
                                          ).toLocaleDateString("en-US", {
                                            month: "2-digit", // Ensure month is two digits
                                            day: "2-digit", // Ensure day is two digits
                                            year: "numeric",
                                          })}{" "}
                                          {new Date(
                                            taskData?.taskDate
                                          ).toLocaleTimeString("en-US", {
                                            hour: "numeric",
                                            minute: "2-digit",
                                            hour12: true, // Use 12-hour clock
                                          })}
                                          <br />
                                          <br />
                                        </p>

                                        <div className="col-sm-12 mt-3">
                                          <label className="form-label">
                                            Change to: Date
                                          </label>
                                          <DateTimePicker
                                            onChange={handleDateChange}
                                            value={formValues.taskDate}
                                            format="y-MM-dd h:mm a" // Format date-time in 'YYYY-MM-DD HH:MM AM/PM'
                                            className="form-control"
                                            id="inline-form-input"
                                            placeholder="y-MM-dd h:mm ae"
                                            disableClock={true} // Disable the clock for cleaner UI
                                          />
                                          {/* <input
                                         className="form-control"
                                         id="inline-form-input"
                                         type="date"
                                         name="taskDate"
                                         value={formValues.taskDate}
                                         onChange={handleChange}
                                       /> */}
                                        </div>
                                        {/* <div className="col-sm-12 mt-3">
                                       <label className="form-label">
                                         Time
                                       </label>
                                       <input
                                         className="form-control"
                                         id="inline-form-input"
                                         type="text"
                                         name="taskTime"
                                         value={formValues.taskTime}
                                         onChange={handleChange}
                                       />
                                     </div> */}
                                        {/* {console.log(taskData)} */}
                                        <div className="col-sm-12 mt-3">
                                          <button
                                            onClick={() =>
                                              handleAdjustDate(taskData?._id)
                                            }
                                            className="btn btn-info btn-sm pull-right mt-2"
                                          >
                                            Adjust Date
                                          </button>
                                        </div>
                                        {/* <button onClick={handleClose}>Close</button> */}
                                      </div>
                                    </div>
                                  </Draggable>
                                )}
                              </small>
                            </p>
                            {/* {timeDifference && (
                           <p className="mb-0">
                             Sent: {timeDifference.days} days,{" "}
                             {timeDifference.hours} hours,{" "}
                             {timeDifference.seconds} seconds ago
                           </p>
                         )} */}
                          </div>
                        </div>
                        <hr />

                        <div className="col-md-12 align-items-center justify-content-start my-3">
                          <p className="mb-0 w-100">
                            <small>{taskData?.taskTitle}</small>
                          </p>
                          <p className="mb-0 mt-1">
                            <small>{taskData?.question}</small>
                          </p>

                          <label className="form-check-label">
                            Your answer:
                          </label>
                          {taskData?.your_answer === "Done" ? (
                            <>
                              <h6>Done</h6>
                            </>
                          ) : (
                            <>
                              {taskData?.issue ? (
                                <>
                                  <h6>No Answer</h6>
                                </>
                              ) : (
                                <div className="mt-1">
                                  <div className="form-check">
                                    <input
                                      className="form-check-input"
                                      name="your_answer"
                                      type="checkbox"
                                      id="agree-to-terms"
                                      onClick={(e) => handleCheck(e)}
                                      // value={formValues.answer}
                                    />
                                    <label className="form-check-label">
                                      Task Complete
                                    </label>
                                  </div>
                                  <button
                                    onClick={handleIssue}
                                    className="btn btn-info btn-sm pull-right ms-2"
                                  >
                                    Issued?
                                  </button>
                                  <button
                                    onClick={() =>
                                      handleResponce(taskData?._id)
                                    }
                                    className="btn btn-info btn-sm pull-right"
                                  >
                                    Send Responce
                                  </button>
                                </div>
                              )}
                            </>
                          )}
                        </div>
                        <hr />

                        {taskData?.issue ? (
                          <>
                            <div className="d-flex align-items-center justify-content-start">
                              <img
                                className="rounded-circle image-note  mb-3"
                                src={profile.image}
                                alt="image"
                                width="75"
                                height="75"
                              />

                              <div className="ms-3">
                                <strong className="">
                                  {" "}
                                  From:{" "}
                                  {taskData.from
                                    ? taskData.from
                                    : `${profile.firstName} ${profile.lastName}`}
                                  {/* From: {profile.firstName} {profile.lastName} */}
                                </strong>
                                <p className="mb-0">
                                  {new Date(
                                    taskData?.taskDate
                                  ).toLocaleDateString("en-US", {
                                    weekday: "short",
                                    month: "short",
                                    day: "numeric",
                                    year: "numeric",
                                  })}{" "}
                                  {taskData?.taskTime}
                                </p>
                              </div>
                            </div>
                            <p className="ms-5">{taskData.issue}</p>
                          </>
                        ) : (
                          <div className="col-md-12  align-items-center justify-content-start mt-3">
                            {taskIssue ? (
                              <>
                                <p className="float-left w-100 mb-2">
                                  <small>
                                    Having an issue fulfilling this task?
                                    Describe why.
                                  </small>
                                </p>
                                <textarea
                                  className="form-control float-left w-100"
                                  id="textarea-input"
                                  rows="5"
                                  name="issue"
                                  onChange={handleChange}
                                  autoComplete="on"
                                  value={formValues.issue}
                                ></textarea>
                                <div className="mt-3">
                                  <button
                                    onClick={handleCancelIssu}
                                    className="btn btn-info btn-sm pull-right ms-2"
                                  >
                                    Cancel
                                  </button>
                                  <button
                                    className="btn btn-info btn-sm pull-right"
                                    onClick={() => handleIssu(taskData?._id)}
                                  >
                                    Send Issue
                                  </button>
                                </div>
                              </>
                            ) : (
                              ""
                            )}
                          </div>
                        )}
                        {/* <hr /> */}

                        {/* <div className="col-md-12 d-flex align-items-center justify-content-start mt-5">
                       <h6>COMMENTS</h6>
                       <button
                         className="btn btn-info pull-right ms-5"
                         onClick={handleCommentsModal}
                       >
                         + Add
                       </button>

                       {isOpen ? (
                         <Draggable>
                           <div
                             style={{
                               border: "1px solid #ccc",
                               padding: "16px",
                               position: "absolute",
                               background: "#fff",
                               zIndex: 1000,
                             }}
                           >
                             <div className="modal-header">
                               <h4 className="modal-title">Add Comment</h4>
                               <button
                                 onClick={handleCloseComment}
                                 className="btn-close"
                                 type="button"
                                 data-dismiss="modal"
                                 aria-label="Close"
                               ></button>
                             </div>

                             <div className="mt-3">
                               <p className="mb-0 w-100">
                                 <label className="form-check-label">
                                   Enter your comment
                                 </label>
                                 {formValues.comments.map((comment, index) => (
                                   <textarea
                                     key={comment.commentId} // Use a unique identifier here
                                     className="form-control float-left w-100"
                                     id={`textarea-input-${index}`}
                                     rows="5"
                                     name={`comments[${index}].comment`}
                                     onChange={handleChangeComment}
                                     autoComplete="on"
                                     value={comment.comment}
                                   />
                                 ))}

                                 <button
                                   onClick={() => handleComment(taskData?._id)}
                                   className="btn btn-info pull-right"
                                 >
                                   Add Comment
                                 </button>

                               </p>
                             </div>
                           </div>
                         </Draggable>
                       ) : (
                         ""
                       )}
                     </div> */}

                        {taskData?.comments
                          ? taskData.comments.map((item) => (
                              <div
                                key={item._id}
                                className="d-flex align-items-center justify-content-start mt-3"
                              >
                                <img
                                  className="rounded-circle image-note  mb-3"
                                  src={profile.image}
                                  alt="image"
                                  width="75"
                                  height="75"
                                />

                                <div className="ms-3">
                                  <strong className="">
                                    {" "}
                                    From:{" "}
                                    {taskData.from
                                      ? taskData.from
                                      : `${profile.firstName} ${profile.lastName}`}
                                    {/* From: {profile.firstName} {profile.lastName} */}
                                  </strong>
                                  <p className="mb-0">
                                    {new Date(
                                      taskData.taskDate
                                    ).toLocaleDateString("en-US", {
                                      weekday: "short",
                                      month: "short",
                                      day: "numeric",
                                      year: "numeric",
                                    })}{" "}
                                    {taskData.taskTime}
                                  </p>
                                </div>
                                <p className="ms-5">{item.comment}</p>
                              </div>
                            ))
                          : ""}
                      </div>
                    </div>
                  </div>
                </div>
              ) : (
                <div className="col-md-4">
                    <div className="bg-light border rounded-3 p-3 float-left d-inline-block w-100 mt-lg-0 mt-md-0 mt-sm-4 mt-4">
                    <h6>TASK SUMMARY</h6>
                    <p>{noticereceived.length} active</p>
                    <p>{noticereceived.length} personal</p>
                  </div>
                </div>
              )}
            </div>
          )}

          {tabSelected === "toAssociate" && (
            <div className="row">
              <div className="col-md-12">
                <h6 className="text-white">{noticesend.length} Task</h6>
              </div>
              <div className="col-md-8">
                {noticesend && noticesend.length > 0 ? (
                  noticesend.map((dataItem, index) => (
                    <div
                      className="notes-main bg-light border-top border-0 rounded-3"
                      key={index}
                      onClick={() => handleClickAssociate(dataItem._id)}
                    >
                      <div className="row">
                        <div className="col-md-6">
                          <div className="d-flex align-items-center justify-content-start">
                            <img
                              className="rounded-circle image-note mt-3 mb-3"
                              // src={profile.image}
                              src={dataItem.fromImage || profile.image  || Pic }
                              alt="image"
                              width="75"
                              height="75"
                            />
                            <div className="ms-3">
                              <strong>{dataItem.taskTitle}</strong>
                              <p id="" className="mb-0">
                                From:{" "}
                                {dataItem.from
                                  ? dataItem.from
                                  : `${profile.firstName} ${profile.lastName}`}
                                {/* From: {profile.firstName} {profile.lastName} */}
                              </p>
                            </div>
                          </div>
                        </div>
                        <div className="col-md-6 d-flex align-items-center justify-content-start">
                          <div className="">
                           <p className="mb-0 w-100">
                                  Due:{" "}
                                  {dataItem?.taskDate ? (
                                    <>
                                      {new Date(
                                        dataItem.taskDate
                                      ).toLocaleDateString("en-US", {
                                        month: "2-digit", // Ensure month is two digits
                                        day: "2-digit", // Ensure day is two digits
                                        year: "numeric",
                                      })}{" "}
                                      {new Date(
                                        dataItem.taskDate
                                      ).toLocaleTimeString("en-US", {
                                        hour: "numeric",
                                        minute: "2-digit",
                                        hour12: true, // Use 12-hour clock
                                      })}
                                      {/* {new Date(
                                          dataItem.taskDate
                                        ).toLocaleString("en-US", {
                                          month: "short",
                                          day: "numeric",
                                          year: "numeric",
                                          hour: "numeric",
                                          minute: "numeric",
                                        })} */}
                                      <br />
                                      <strong>
                                        Sent:{" "}
                                        {dataItem?.taskDate
                                          ? getTimeDifference(
                                              dataItem?.taskDate
                                            )
                                          : "--:--:--"}{" "}
                                        ago
                                      </strong>
                                    </>
                                  ) : (
                                    "No date available"
                                  )}
                               
                             
                            </p>
                          </div>
                        </div>
                      </div>
                    </div>
                  ))
                ) : (
                  <div className="bg-light border rounded-3 p-3">
                    <p className="text-center my-5">No tasks found.</p>
                  </div>
                )}
              </div>

              {taskDataAssociate ? (
                <div className="col-md-4">
                  <div className="bg-light border rounded-3 p-3 float-left d-inline-block w-100">
                    <div className="pull-right">
                      <button
                        onClick={handleClose}
                        className="btn-close ms-5"
                        type="button"
                        data-dismiss="modal"
                        aria-label="Close"
                      ></button>
                    </div>
                    <div className="notes-main bg-light m-0 border-0">
                      <div className="row">
                        <div className="col-md-12">
                          <div className="d-flex align-items-center justify-content-start">
                            <img
                              className="rounded-circle image-note  mb-3"
                              // src={profile.image}
                              src={taskDataAssociate.fromImage || profile.image  || Pic }
                              alt="image"
                              width="75"
                              height="75"
                            />

                            <div className="ms-3">
                              <strong className="">
                                From:{" "}
                                {taskDataAssociate.from
                                  ? taskDataAssociate.from
                                  : `${profile.firstName} ${profile.lastName}`}
                              </strong>
                              <p id="" className="mb-0">
                                To: {profile.firstName} {profile.lastName}
                              </p>
                            </div>
                          </div>
                        </div>

                        <hr />

                        <div className="col-md-12 d-flex align-items-center justify-content-start my-2">
                          <div className="">
                            <p className="mb-0 w-100">
                              <small>
                                Due:
                                {new Date(
                                  taskDataAssociate?.taskDate
                                ).toLocaleDateString("en-US", {
                                  month: "2-digit", // Ensure month is two digits
                                  day: "2-digit", // Ensure day is two digits
                                  year: "numeric",
                                })}{" "}
                                {new Date(
                                  taskDataAssociate?.taskDate
                                ).toLocaleTimeString("en-US", {
                                  hour: "numeric",
                                  minute: "2-digit",
                                  hour12: true, // Use 12-hour clock
                                })}
                                <br />
                                <strong>
                                  Sent:{" "}
                                  {taskDataAssociate?.taskDate
                                    ? getTimeDifference(taskDataAssociate?.taskDate)
                                    : "--:--:--"}{" "}
                                  ago
                                </strong>
                                <NavLink
                                  onClick={handleAdjust}
                                  className="pull-right ms-5"
                                >
                                  Adjust
                                </NavLink>
                                {taskAdjust && (
                                  <Draggable>
                                    <div
                                      className="bg-light border rounded-3 p-3 float-left"
                                      style={{
                                        position: "absolute",
                                        background: "#fff",
                                        zIndex: 1000,
                                      }}
                                    >
                                      <div className="d-flex align-items-center justify-content-between">
                                        <h6 className="modal-title pt-0">
                                          Adjust Date
                                        </h6>
                                        <button
                                          onClick={handleClosedate}
                                          className="btn-close"
                                          type="button"
                                          data-dismiss="modal"
                                          aria-label="Close"
                                        ></button>
                                      </div>

                                      <div className="float-left w-100 mt-3">
                                        <p className="mb-0 w-100">
                                          Originally due:
                                          {new Date(
                                            taskDataAssociate?.taskDate
                                          ).toLocaleDateString("en-US", {
                                            month: "2-digit", // Ensure month is two digits
                                            day: "2-digit", // Ensure day is two digits
                                            year: "numeric",
                                          })}{" "}
                                          {new Date(
                                            taskDataAssociate?.taskDate
                                          ).toLocaleTimeString("en-US", {
                                            hour: "numeric",
                                            minute: "2-digit",
                                            hour12: true, // Use 12-hour clock
                                          })}
                                          <br />
                                          <br />
                                        </p>

                                        <div className="col-sm-12 mt-3">
                                          <label className="form-label">
                                            Change to: Date
                                          </label>
                                          <DateTimePicker
                                            onChange={handleDateChange}
                                            value={formValues.taskDate}
                                            format="y-MM-dd h:mm a" // Format date-time in 'YYYY-MM-DD HH:MM AM/PM'
                                            className="form-control"
                                            id="inline-form-input"
                                            placeholder="y-MM-dd h:mm ae"
                                            disableClock={true} // Disable the clock for cleaner UI
                                          />
                                          {/* <input
                                         className="form-control"
                                         id="inline-form-input"
                                         type="date"
                                         name="taskDate"
                                         value={formValues.taskDate}
                                         onChange={handleChange}
                                       /> */}
                                        </div>
                                        {/* <div className="col-sm-12 mt-3">
                                       <label className="form-label">
                                         Time
                                       </label>
                                       <input
                                         className="form-control"
                                         id="inline-form-input"
                                         type="text"
                                         name="taskTime"
                                         value={formValues.taskTime}
                                         onChange={handleChange}
                                       />
                                     </div> */}
                                        {/* {console.log(taskDataAssociate)} */}
                                        <div className="col-sm-12 mt-3">
                                          <button
                                            onClick={() =>
                                              handleAdjustDate(taskDataAssociate?._id)
                                            }
                                            className="btn btn-info btn-sm pull-right mt-2"
                                          >
                                            Adjust Date
                                          </button>
                                        </div>
                                        {/* <button onClick={handleClose}>Close</button> */}
                                      </div>
                                    </div>
                                  </Draggable>
                                )}
                              </small>
                            </p>
                            {/* {timeDifference && (
                           <p className="mb-0">
                             Sent: {timeDifference.days} days,{" "}
                             {timeDifference.hours} hours,{" "}
                             {timeDifference.seconds} seconds ago
                           </p>
                         )} */}
                          </div>
                        </div>
                        <hr />

                        <div className="col-md-12 align-items-center justify-content-start my-3">
                          <p className="mb-0 w-100">
                            <small>{taskDataAssociate?.taskTitle}</small>
                          </p>
                          <p className="mb-0 mt-1">
                            <small>{taskDataAssociate?.question}</small>
                          </p>

                          <label className="form-check-label">
                            Your answer:
                          </label>
                          {taskDataAssociate?.your_answer === "Done" ? (
                            <>
                              <h6>Done</h6>
                            </>
                          ) : (
                            <>
                              {taskDataAssociate?.issue ? (
                                <>
                                  <h6>No Answer</h6>
                                </>
                              ) : (
                                <div className="mt-1">
                                  <div className="form-check">
                                    <input
                                      className="form-check-input"
                                      name="your_answer"
                                      type="checkbox"
                                      id="agree-to-terms"
                                      onClick={(e) => handleCheck(e)}
                                      // value={formValues.answer}
                                    />
                                    <label className="form-check-label">
                                      Task Complete
                                    </label>
                                  </div>
                                  <button
                                    onClick={handleIssue}
                                    className="btn btn-info btn-sm pull-right ms-2"
                                  >
                                    Issued?
                                  </button>
                                  <button
                                    onClick={() =>
                                      handleResponce(taskDataAssociate?._id)
                                    }
                                    className="btn btn-info btn-sm pull-right"
                                  >
                                    Send Responce
                                  </button>
                                </div>
                              )}
                            </>
                          )}
                        </div>
                        <hr />

                        {taskDataAssociate?.issue ? (
                          <>
                            <div className="d-flex align-items-center justify-content-start">
                              <img
                                className="rounded-circle image-note  mb-3"
                                src={profile.image}
                                alt="image"
                                width="75"
                                height="75"
                              />

                              <div className="ms-3">
                                <strong className="">
                                  {" "}
                                  From:{" "}
                                  {taskDataAssociate.from
                                    ? taskDataAssociate.from
                                    : `${profile.firstName} ${profile.lastName}`}
                                  {/* From: {profile.firstName} {profile.lastName} */}
                                </strong>
                                <p className="mb-0">
                                  {new Date(
                                    taskDataAssociate?.taskDate
                                  ).toLocaleDateString("en-US", {
                                    weekday: "short",
                                    month: "short",
                                    day: "numeric",
                                    year: "numeric",
                                  })}{" "}
                                  {taskDataAssociate?.taskTime}
                                </p>
                              </div>
                            </div>
                            <p className="ms-5">{taskDataAssociate.issue}</p>
                          </>
                        ) : (
                          <div className="col-md-12  align-items-center justify-content-start mt-3">
                            {taskIssue ? (
                              <>
                                <p className="float-left w-100 mb-2">
                                  <small>
                                    Having an issue fulfilling this task?
                                    Describe why.
                                  </small>
                                </p>
                                <textarea
                                  className="form-control float-left w-100"
                                  id="textarea-input"
                                  rows="5"
                                  name="issue"
                                  onChange={handleChange}
                                  autoComplete="on"
                                  value={formValues.issue}
                                ></textarea>
                                <div className="mt-3">
                                  <button
                                    onClick={handleCancelIssu}
                                    className="btn btn-info btn-sm pull-right ms-2"
                                  >
                                    Cancel
                                  </button>
                                  <button
                                    className="btn btn-info btn-sm pull-right"
                                    onClick={() => handleIssu(taskDataAssociate?._id)}
                                  >
                                    Send Issue
                                  </button>
                                </div>
                              </>
                            ) : (
                              ""
                            )}
                          </div>
                        )}
                        {/* <hr /> */}

                        {/* <div className="col-md-12 d-flex align-items-center justify-content-start mt-5">
                       <h6>COMMENTS</h6>
                       <button
                         className="btn btn-info pull-right ms-5"
                         onClick={handleCommentsModal}
                       >
                         + Add
                       </button>

                       {isOpen ? (
                         <Draggable>
                           <div
                             style={{
                               border: "1px solid #ccc",
                               padding: "16px",
                               position: "absolute",
                               background: "#fff",
                               zIndex: 1000,
                             }}
                           >
                             <div className="modal-header">
                               <h4 className="modal-title">Add Comment</h4>
                               <button
                                 onClick={handleCloseComment}
                                 className="btn-close"
                                 type="button"
                                 data-dismiss="modal"
                                 aria-label="Close"
                               ></button>
                             </div>

                             <div className="mt-3">
                               <p className="mb-0 w-100">
                                 <label className="form-check-label">
                                   Enter your comment
                                 </label>
                                 {formValues.comments.map((comment, index) => (
                                   <textarea
                                     key={comment.commentId} // Use a unique identifier here
                                     className="form-control float-left w-100"
                                     id={`textarea-input-${index}`}
                                     rows="5"
                                     name={`comments[${index}].comment`}
                                     onChange={handleChangeComment}
                                     autoComplete="on"
                                     value={comment.comment}
                                   />
                                 ))}

                                 <button
                                   onClick={() => handleComment(taskDataAssociate?._id)}
                                   className="btn btn-info pull-right"
                                 >
                                   Add Comment
                                 </button>

                               </p>
                             </div>
                           </div>
                         </Draggable>
                       ) : (
                         ""
                       )}
                     </div> */}

                        {taskDataAssociate?.comments
                          ? taskDataAssociate.comments.map((item) => (
                              <div
                                key={item._id}
                                className="d-flex align-items-center justify-content-start mt-3"
                              >
                                <img
                                  className="rounded-circle image-note  mb-3"
                                  src={profile.image}
                                  alt="image"
                                  width="75"
                                  height="75"
                                />

                                <div className="ms-3">
                                  <strong className="">
                                    {" "}
                                    From:{" "}
                                    {taskDataAssociate.from
                                      ? taskDataAssociate.from
                                      : `${profile.firstName} ${profile.lastName}`}
                                    {/* From: {profile.firstName} {profile.lastName} */}
                                  </strong>
                                  <p className="mb-0">
                                    {new Date(
                                      taskDataAssociate.taskDate
                                    ).toLocaleDateString("en-US", {
                                      weekday: "short",
                                      month: "short",
                                      day: "numeric",
                                      year: "numeric",
                                    })}{" "}
                                    {taskDataAssociate.taskTime}
                                  </p>
                                </div>
                                <p className="ms-5">{item.comment}</p>
                              </div>
                            ))
                          : ""}
                      </div>
                    </div>
                  </div>
                </div>
              ) : (
                <div className="col-md-4">
                    <div className="bg-light border rounded-3 p-3 float-left d-inline-block w-100 mt-lg-0 mt-md-0 mt-sm-4 mt-4">
                  <h6>TASK SUMMARY</h6>
                  <p>{noticesend.length} active</p>
                  <p>{noticesend.length} personal</p>
                </div>
              </div>
              )}
            </div>
          )}

          {tabSelected === "AssocTasks" && <AssocTasks />}

          <div className="float-start w-100 justify-content-end mb-1 mt-4">
                        <ReactPaginate
                            previousLabel={"Previous"}
                            nextLabel={"Next"}
                            breakLabel={"..."}
                            pageCount={pageCount}
                            marginPagesDisplayed={1}
                            pageRangeDisplayed={2}
                            onPageChange={handlePageClick}
                            containerClassName={"pagination justify-content-center"}
                            pageClassName={"page-item"}
                            pageLinkClassName={"page-link"}
                            previousClassName={"page-item"}
                            previousLinkClassName={"page-link"}
                            nextClassName={"page-item"}
                            nextLinkClassName={"page-link"}
                            breakClassName={"page-item"}
                            breakLinkClassName={"page-link"}
                            activeClassName={"active"}
                        />
                        </div>
        </main>
      </div>
    </div>
  );
}
export default Task;

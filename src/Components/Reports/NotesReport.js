import React, { useState, useEffect } from 'react'
import Pic from "../img/pic.png";
import { NavLink, useNavigate, useParams } from 'react-router-dom';
import user_service from '../service/user_service';
import axios from "axios";
import Loader from "../../Pages/Loader/Loader.js";
import Toaster from '../../Pages/Toaster/Toaster.js';
import jwt from "jwt-decode";
import Modalresetpassword from "../../Modalresetpassword";
import { now, set } from 'lodash';
import { CSVLink } from "react-csv";
const NotesReport = () => {
    const [loader, setLoader] = useState({ isActive: null });
    const { isActive } = loader;
    const [toaster, setToaster] = useState({ types: null, isShow: null, toasterBody: "", message: "" });
    const { types, isShow, toasterBody, message } = toaster;

    
    const [notesResults, setNotesResults] = useState(0);
    const [entrytype, setEntrytype] = useState("");
    

    const NotesAll = async (entrytype) => {
        try {
          const response = await user_service.notesAllGet(entrytype);
          setLoader({ isActive: false });
      
          if (response.data && response.data.data) {
            setNotesResults(response.data.data);
          }
        } catch (error) {
          console.error("Error fetching expense ledger:", error);
        }
      };
      
      
    useEffect(() => { window.scrollTo(0, 0);
        setLoader({ isActive: true })
        NotesAll(entrytype)
    }, []);

    const changetype = (e) => {
        const { name, value } = e.target;
        setEntrytype(value)
        NotesAll(value)
    };

    return (
        <>
        {/* {console.log("expenseAdd")}
        {console.log(expenseAdd)}
        {console.log(release)}
        {console.log(lastMonthHolding)}
        {console.log(holdingNow)}
        {console.log("holdingNow")} */}

            <div className="bg-secondary float-left w-100 pt-4 mb-4">
                <Loader isActive={isActive} />
                {isShow && <Toaster types={types}
                    isShow={isShow}
                    toasterBody={toasterBody}
                    message={message} />}
                <main className="page-wrapper getContact_page_wrap">
                    <div className="content-overlay mt-0">
                        
                        <div className="">
                            {/* <h4 className="nav-item mb-md-0 me-md-2 pe-md-1 mb-5" id="getContact"><a className="nav-link active" href="#" aria-current="page">Profile</a></h4> */}
                            <div className="">
                                <div className="">
                                    <div className="row">
                                        <div className="border bg-light rounded-3 p-3" id="auth-info">
                                            <div className="">
                                                <div className="mb-2">
                                                    <h6 className="mb-4">Search Criteria</h6>
                                                    <div className="col-md-6">
                                                        {/* <label className="form-label" >Where-</label> */}
                                                        <select className="form-select form-select-dark" name="entrytype"
                                                            value={entrytype} onChange={changetype}>
                                                            <option value="">All</option>
                                                            <option value="Staff Notes">Staff Only</option>
                                                            <option value="Agent Notes">Agent Only</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>

                                            <div>Summary of {entrytype} held in trust.</div>
                                                <div className="row">
                                                    <div className="col-md-12 mt-4">
                                                        {console.log(notesResults)}
                                                        {
                                                        notesResults && notesResults.length > 0 ?
                                                        notesResults.map((note) => (
                                                            <>
                                                                <div key={note._id} className="card card-hover mb-2">
                                                                    <div className="card-body">
                                                                        <div className="float-left w-100 d-lg-flex d-md-flex d-inline-block align-items-center justify-content-between">
                                                                            <div className="float-left w-100 d-flex align-items-center justify-content-start">
                                                                                <img className="rounded-circle profile_picture" src={note.contact_image} alt="image" width="50" height="50" />
                                                                                <span className="ms-3">
                                                                                    <p className="mb-2"><strong className="">{note.contact_name}</strong></p>
                                                                                    <p className="mb-0">{new Date(note.created).toLocaleString()}</p>
                                                                                </span>
                                                                            </div>
                                                                            <div className="col-lg-6 col-md-6 col-sm-12 col-12 mt-lg-0 mt-md-0 mt-sm-4 mt-4">
                                                                                <h6 className="mb-2">{note.note_type.charAt(0).toUpperCase() + note.note_type.slice(1)}</h6>
                                                                                    {/* <h6 id='note-text-two'>{note.message}</h6> */}
                                                                                <p className="mb-0" dangerouslySetInnerHTML={{ __html: note.message }} />
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </>
                                                            ))
                                                            :""
                                                        }
                                                        {/* <>
                                                            <div className='notes-main'>
                                                                <img className='image-note mt-3' src='https://integra.backagent.net/common/handler/pub/photo.ashx?V=3d122128e33967055f739bcfd8c03729.0&C=people&S=0&P=%2f!Default%2fTag%2fphoto_pending.jpg' alt="image" width="28" height="25" />
                                                                <strong className=''>&nbsp;Abhi Bhatt Bhatt Verse</strong>
                                                                <br />
                                                                <p id='note-text'>Wed, 04/12/2023 12:59 AM</p>
                                                            </div>
                                                            <div>
                                                                <p id='note-text-one'>checklist Item Completed</p>
                                                                <h6 id='note-text-two'>Add the listing to the MLS</h6>
                                                            </div>
                                                        </> */}
                                                    </div>
                                                </div>
                                       </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </main>
            </div>
        </>

    )
}

export default NotesReport
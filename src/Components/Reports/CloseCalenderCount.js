import React, { useState, useEffect } from "react";
import user_service from "../service/user_service";
import Loader from "../../Pages/Loader/Loader.js";
import Toaster from "../../Pages/Toaster/Toaster.js";
import { useNavigate } from "react-router-dom";
import moment from "moment-timezone";
import jwt from "jwt-decode";

const CloseCalenderCount = () => {
  const [loader, setLoader] = useState({ isActive: null });
  const { isActive } = loader;
  const [toaster, setToaster] = useState({
    types: null,
    isShow: null,
    toasterBody: "",
    message: "",
  });
  const { types, isShow, toasterBody, message } = toaster;

  const navigate = useNavigate();
  const [selectedMonth, setSelectedMonth] = useState(new Date().getMonth() + 1);
  const [currentMonthData, setCurrentMonthData] = useState([]);
  const [calenderGet, setCalenderGet] = useState([]);

  const currentYear = new Date().getFullYear();
  const currentMonth = new Date().getMonth();


  const TransactionCalenderGet = async () => {
    try {
      setLoader({ isActive: true });

      let queryParams;
      if (
        localStorage.getItem("auth") &&
        jwt(localStorage.getItem("auth")).contactType == "admin"
      ) {
      } else {
        queryParams = `?page=1&agentId=` + jwt(localStorage.getItem("auth")).id;
      }

      const response = await user_service.TransactionCalenderGet(queryParams);

      setLoader({ isActive: false });

      if (response && response.data) {
        console.log(response);
        setCalenderGet(response.data.data);
      }
    } catch (error) {
      console.error("Error in fetchCalendar:", error);
      // Handle errors as needed, e.g., show a user-friendly error message
    }
  };

  useEffect(() => {
    TransactionCalenderGet();
  }, []);

  const handleChangeMonth = (event) => {
    setSelectedMonth(parseInt(event.target.value));
  };

 

  useEffect(() => {
    // Filter calenderGet to only include items from the current month
    const filteredData = calenderGet.filter((post) => {
      const postDate = new Date(post.settlement_deadline); // Assuming 'ettlement_deadline' is the date field
      return (
       
        postDate.getFullYear() === currentYear &&
        postDate.getMonth() + 1 === selectedMonth
      );
    });
    setCurrentMonthData(filteredData);
  }, [selectedMonth, calenderGet]);

  const generateSelectOptions = () => {
    const options = [];
    for (let i = 12; i >= 1; i--) {
      options.push(
        <option key={i} value={i}>
          {new Date(currentYear, i - 1, 1).toLocaleDateString("default", {
            month: "long",
          })}{" "}
          {currentYear}
        </option>
      );
    }
    return options;
  };


  const handleClickAddress = (id) => {
    navigate(`/transaction-summary/${id}`);
  };
  return (
    <div className="bg-secondary float-left w-100 pt-4">
      <Loader isActive={isActive} />
      {isShow && (
        <Toaster
          types={types}
          isShow={isShow}
          toasterBody={toasterBody}
          message={message}
        />
      )}
      <main className="page-wrapper getContact_page_wrap">
        <div className="container content-overlay mt-0">
          <div className="row mt-3">
            <div className="col-md-12">
              <div className="row">
                <div className="border rounded-3 p-3 bg-light">
                  <p className="h6 text-center">Transaction Count Address</p>
                  <div className="table-responsive">
                  <div className="col-md-3 mb-3">
                      <select
                        className="form-select "
                        value={selectedMonth}
                        onChange={handleChangeMonth}
                      >
                        {generateSelectOptions()}
                      </select>
                    </div>
                    <table
                      id="DepositLedger"
                      className="table table-striped mb-0"
                      width="100%"
                      border="0"
                      cellSpacing="0"
                      cellPadding="0"
                    >
                      <thead>
                        <tr>
                          <th>Transaction</th>

                          <th>Transaction Address</th>
                        </tr>
                      </thead>
                      <tbody>
                        {currentMonthData.length > 0 ? (
                          currentMonthData.map((post, index) => (
                            <tr key={index}>
                              <td>
                                {" "}
                                {post.represent === "seller"
                                  ? "Seller"
                                  : post.represent === "buyer"
                                  ? "Buyer"
                                  : post.represent === "both"
                                  ? "Buyer & Seller"
                                  : post.represent === "referral"
                                  ? "Referral"
                                  : ""}
                              </td>
                              <td onClick={()=>handleClickAddress(post._id)}>{post.propertyDetail?.streetAddress}</td>
                            </tr>
                          ))
                        ) : (
                          <tr>
                            <td colSpan="15">
                              No transactions found for the current month
                            </td>
                          </tr>
                        )}
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </main>
    </div>
  );
};

export default CloseCalenderCount;

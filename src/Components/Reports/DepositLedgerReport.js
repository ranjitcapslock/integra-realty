import React, { useState, useEffect } from 'react'
import Pic from "../img/pic.png";
import { NavLink, useNavigate, useParams } from 'react-router-dom';
import user_service from '../service/user_service';
import axios from "axios";
import Loader from "../../Pages/Loader/Loader.js";
import Toaster from '../../Pages/Toaster/Toaster.js';

const DepositLedgerReport = () => {
    const [loader, setLoader] = useState({ isActive: null });
    const { isActive } = loader;
    const [toaster, setToaster] = useState({ types: null, isShow: null, toasterBody: "", message: "" });
    const { types, isShow, toasterBody, message } = toaster;
    const [transactionResults, setTransactionResults] = useState(0);
    const [overallResults, setOverallResults] = useState(0);
    const [deposit, setDeposit] = useState([]);

    const DepositLedgerAll = async () => {
        try {
            const response = await user_service.depositLedgerAllGet();
            if (response.data && response.data.data) {
                setDeposit(response.data);
                const groupedBytransactionId = response.data.data.reduce((acc, item) => {
                    const transactionId = item.transactionId;
                    if (!acc[transactionId]) {
                        acc[transactionId] = [];
                    }
                    acc[transactionId].push(item);
                    return acc;
                }, {});

                let currentdate = new Date();
                let currentmonth = currentdate.getMonth();
                let currentyear = currentdate.getFullYear();

                let lastmonthsavingOverall = 0;
                let lastmonthwithdrawOverall = 0;
                let currentmonthsavingOverall = 0;
                let totalsavingOverall = 0;
                let totaldebitOverall = 0;

                const transactionResults = {};

                for (const transactionId in groupedBytransactionId) {
                    if (transactionId !== "1") {

                        const items = groupedBytransactionId[transactionId];
                        let firstEntryDate = null;
                        let transactionData = null;

                        let lastmonthsaving = 0;
                        let lastmonthwithdraw = 0;
                        let currentmonthsaving = 0;
                        let totalsaving = 0;
                        let totaldebit = 0;
                        items.forEach((item) => {
                            const itemDate = new Date(item.entry_date);
                            const itemYear = itemDate.getFullYear();
                            const itemMonth = itemDate.getMonth();

                            if (!firstEntryDate) {
                                firstEntryDate = item.entry_date;
                            }
                            if (!transactionData) {
                                transactionData = item.transactionData;
                            }

                            if (currentyear === itemYear && currentmonth === itemMonth) {
                                if (item.entry_type === "deposit") {
                                    currentmonthsaving += parseInt(item.amount);
                                    totalsaving += parseInt(item.amount);
                                    currentmonthsavingOverall += parseInt(item.amount);
                                    totalsavingOverall += parseInt(item.amount);
                                } else {
                                    currentmonthsaving -= parseInt(item.amount);
                                    totaldebit += parseInt(item.amount);
                                    currentmonthsavingOverall -= parseInt(item.amount);
                                    totaldebitOverall += parseInt(item.amount);
                                }
                            } else {
                                if (item.entry_type === "deposit") {
                                    lastmonthsaving += parseInt(item.amount);
                                    lastmonthsavingOverall += parseInt(item.amount);
                                } else if (item.entry_type === "release") {
                                    lastmonthwithdraw += parseInt(item.amount);
                                    lastmonthwithdrawOverall += parseInt(item.amount);
                                }
                            }
                        });

                        transactionResults[transactionId] = {
                            firstEntryDate,
                            transactionData,
                            lastMonthHolding: lastmonthsaving - lastmonthwithdraw,
                            holdingNow: lastmonthsaving - lastmonthwithdraw + totalsaving - totaldebit,
                            depositThisMonth: currentmonthsaving,
                            releaseThisMonth: totaldebit,
                        };
                    }
                }

                const overallResults = {
                    lastMonthHolding: lastmonthsavingOverall - lastmonthwithdrawOverall,
                    holdingNow: lastmonthsavingOverall - lastmonthwithdrawOverall + totalsavingOverall - totaldebitOverall,
                    depositThisMonth: currentmonthsavingOverall,
                    releaseThisMonth: totaldebitOverall,
                };

                setTransactionResults(transactionResults);
                setOverallResults(overallResults);
                setLoader({ isActive: false })
            }
        } catch (error) {
            console.error("Error fetching deposit ledger:", error);
        }
    };


    const [currentMonth, setCurrentMonth] = useState("");
    const [backMonth, setbackMonth] = useState("");
    const [monthsToShow, setmonthsToShow] = useState([]);



    const handleMonthClick = (monthValue) => {
        setCurrentMonth(monthValue);
        // console.log(monthValue)
        // console.log("monthValue")
        const currentDate = new Date();
        const backMonthDate = new Date(currentDate);
        backMonthDate.setMonth(currentDate.getMonth() - 5);
        setbackMonth(backMonthDate);
    
        // const selectedYear = currentDate.getFullYear();
        // const selectedMonth = monthValue;
         
      
        
        // const firstMonthToShow = selectedMonth -currentDate.getFullYear();
        // const firstYearToShow = selectedYear - (firstMonthToShow < 0 ? 1 : 0);
        // const lastMonthToShow = selectedMonth;
         
        const groupedBytransactionId = deposit.data.reduce((acc, item) => {
            const transactionId = item.transactionId;
            if (!acc[transactionId]) {
                acc[transactionId] = [];
            }
            acc[transactionId].push(item);
            return acc;
        }, {});

        let currentdate = new Date();
       // let currentmonth = currentdate.getMonth();
        let currentyear = currentdate.getFullYear();
       // console.log(groupedBytransactionId);
      

        let lastmonthsavingOverall = 0;
        let lastmonthwithdrawOverall = 0;
        let currentmonthsavingOverall = 0;
        let totalsavingOverall = 0;
        let totaldebitOverall = 0;

        const transactionResults = {};
      

        for (const transactionId in groupedBytransactionId) {

            if (transactionId) {

                const items = groupedBytransactionId[transactionId];

                let firstEntryDate = null;
                let transactionData = null;

                let lastmonthsaving = 0;
                let lastmonthwithdraw = 0;
                let currentmonthsaving = 0;
                let totalsaving = 0;
                let totaldebit = 0;
                items.forEach((item) => {
                    const itemDate = new Date(item.entry_date);
                    const itemYear = itemDate.getFullYear();
                    const itemMonth = itemDate.getMonth();

                    if (!firstEntryDate) {
                        firstEntryDate = item.entry_date;
                    }
                    if (!transactionData) {
                        transactionData = item.transactionData;
                    }
                       
                    if(item.entry_date && itemMonth <= monthValue - 1){
                        // console.log("okkkk")
                        // //console.log("entry_date"+ item.entry_date);
                        // console.log("monthValue :  "+ monthValue);
                        // console.log("itemMonth  :  "+ itemMonth);
                    if (currentyear === itemYear && monthValue === itemMonth + 1){
                       
                        // console.log("entry_date".item.entry_date);
                        // console.log("monthValue".item.monthValue);
                        // console.log("itemMonth".item.itemMonth);
                    
                        if (item.entry_type === "deposit") {
                            currentmonthsaving += parseInt(item.amount);
                            totalsaving += parseInt(item.amount);
                            currentmonthsavingOverall += parseInt(item.amount);
                            totalsavingOverall += parseInt(item.amount);
                        } else {
                            currentmonthsaving -= parseInt(item.amount);
                            totaldebit += parseInt(item.amount);
                            currentmonthsavingOverall -= parseInt(item.amount);
                            totaldebitOverall += parseInt(item.amount);
                        }
                    
                    
                    }else {
                                if (item.entry_type === "deposit") {
                                    lastmonthsaving += parseInt(item.amount);
                                    lastmonthsavingOverall += parseInt(item.amount);
                                } else if (item.entry_type === "release") {
                                    lastmonthwithdraw += parseInt(item.amount);
                                    lastmonthwithdrawOverall += parseInt(item.amount);
                                }
                        
                    }
                }
            
                
                });
                transactionResults[transactionId] = {
                    firstEntryDate,
                    transactionData,
                    lastMonthHolding: lastmonthsaving - lastmonthwithdraw,
                    depositThisMonth: currentmonthsaving,
                    releaseThisMonth: totaldebit,
                    holdingNow: lastmonthsaving - lastmonthwithdraw + totalsaving - totaldebit,
                };
               //console.log(transactionResults);
            }
        }

        const overallResults = {
            lastMonthHolding: lastmonthsavingOverall - lastmonthwithdrawOverall,
            depositThisMonth: currentmonthsavingOverall,
            releaseThisMonth: totaldebitOverall,
            holdingNow: lastmonthsavingOverall - lastmonthwithdrawOverall + totalsavingOverall - totaldebitOverall,
        };
             
        setTransactionResults(transactionResults);
        setOverallResults(overallResults);
    };
    
    
    

    const getMonthName = (monthValue) => {
        const months = [
            'January', 'February', 'March', 'April', 'May', 'June',
            'July', 'August', 'September', 'October', 'November', 'December',
        ];
        return months[monthValue - 1] || '';
    };

    const monthdetail = () => {
        const currentDate = new Date();
        const currentMonthValue = currentDate.getMonth() + 1;

        setCurrentMonth(currentMonthValue)

        let monthData = [];
        for (let i = currentMonthValue - 5; i <= currentMonthValue; i++) {
            const monthValue = (i <= 0) ? (12 + i) : i;
            monthData.push(monthValue);
            // setCurrentMonth(monthValue)
        }
        setmonthsToShow(monthData)
    }




    useEffect(() => { window.scrollTo(0, 0);
        setLoader({ isActive: true })
        DepositLedgerAll()
        monthdetail()
    }, []);

    return (
        <>
            <div className="bg-secondary float-left w-100 pt-4">
                <Loader isActive={isActive} />
                {isShow && <Toaster types={types}
                    isShow={isShow}
                    toasterBody={toasterBody}
                    message={message} />}
                <main className="page-wrapper getContact_page_wrap">
                    <div className="container content-overlay mt-0">

                        <div className="">
                            <div className="row">
                                <div className="col-md-12">
                                    <div className="row">
                                        <div className="border rounded-3 p-3 bg-light" id="auth-info">

                                            <div className="callout accent page">
                                                <div style={{ padding: "0px 10px 20px;" }}>
                                                    <div style={{ margin: "20px 20px 20px 20px;" }} >
                                                        <div style={{ width: "600px;" }}>
                                                            <h4>Monthly summary of earnest money held in trust.</h4>
                                                        </div>
                                                    </div>
                                                    <div>
                                                        <div id="ReportMonthChoice" className="mt-3">
                                                            {monthsToShow.map((monthValue) => (
                                                                <NavLink
                                                                    key={monthValue}
                                                                    to={`./?m=${monthValue}`}
                                                                    className={`p-2${currentMonth === monthValue ? ' selected' : ''}`}
                                                                    onClick={() => handleMonthClick(monthValue)}>
                                                                    {getMonthName(monthValue)}
                                                                </NavLink>
                                                            ))}
                                                        </div>

                                                        <div className="table-responsive bg-light border rounded-3 p-3 mt-3">
                                                            <table id="DepositReport" className="table table-striped align-middle mb-0 w-100" width="100%" border="0" cellSpacing="0" cellPadding="0">
                                                                {/* <colgroup>
                                                                    <col width="20%" />
                                                                    <col width="40%" />
                                                                    <col width="10%" />
                                                                    <col width="10%" />
                                                                    <col width="10%" />
                                                                    <col width="10%" />
                                                                </colgroup> */}

                                                                <tbody>
                                                                    <tr>
                                                                        <th colspan="2"><em>Totals</em></th>
                                                                        <th className="hright">Held<br />
                                                                            in {getMonthName(currentMonth - 1)}</th>
                                                                        <th className="hright">Deposits<br />
                                                                            in {getMonthName(currentMonth)}</th>
                                                                        <th className="hright">Releases<br />
                                                                            in {getMonthName(currentMonth)}</th>
                                                                        <th className="hright">Holding<br />
                                                                            in {getMonthName(currentMonth)}</th>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>&nbsp;</td>
                                                                        <td>&nbsp;</td>
                                                                        <td className="hright">${overallResults.lastMonthHolding}</td>
                                                                        <td className="hright">${overallResults.depositThisMonth}</td>
                                                                        <td className="hright">(${overallResults.releaseThisMonth})</td>
                                                                        <td className="hright">${overallResults.holdingNow}</td>
                                                                    </tr>
                                                                    <tr>
                                                                        <th colspan="6"><em>
                                                                            <br />
                                                                            Transaction Breakdown</em></th>
                                                                    </tr>
                                                                    <tr>
                                                                        <th>First Deposit</th>
                                                                        <th>Transaction</th>
                                                                        <th className="hright">Held<br />
                                                                            in {getMonthName(currentMonth - 1)}</th>
                                                                        <th className="hright">Deposits<br />
                                                                            in {getMonthName(currentMonth)}</th>
                                                                        <th className="hright">Releases<br />
                                                                            in {getMonthName(currentMonth)}</th>
                                                                        <th className="hright">Holding<br />
                                                                            in {getMonthName(currentMonth)}</th>
                                                                    </tr>

                                                                
                                                                    {
                                                                        Object.keys(transactionResults).length > 0 ? (
                                                                            Object.keys(transactionResults).map((transactionId) => {
                                                                                const entry = transactionResults[transactionId];

                                                                                return (
                                                                                    <tr key={transactionId}>

                                                                                        <td>{new Date(entry.firstEntryDate).toLocaleString('en-US', {
                                                                                            weekday: 'short',
                                                                                            month: 'short',
                                                                                            day: 'numeric',
                                                                                            year: 'numeric',
                                                                                        })}</td>
                                                                                        <td>
                                                                                            <a href={`/update-deposit/${transactionId}`}>
                                                                                                {entry.transactionData && entry.transactionData.propertyDetail && entry.transactionData.propertyDetail.streetAddress
                                                                                                    ? entry.transactionData.propertyDetail.streetAddress
                                                                                                    : ""}
                                                                                                {" / "}
                                                                                                {entry.transactionData &&
                                                                                                    entry.transactionData.contact1 &&
                                                                                                    entry.transactionData.contact1.length > 0 ? (
                                                                                                    entry.transactionData.contact1[0] !== null ? (
                                                                                                        <>
                                                                                                            {entry.transactionData.contact1[0].data &&
                                                                                                                entry.transactionData.contact1[0].data.organization &&
                                                                                                                entry.transactionData.contact1[0].data.organization !== ""
                                                                                                                ? entry.transactionData.contact1[0].data.organization
                                                                                                                : entry.transactionData.contact1[0].data.firstName}{" "}
                                                                                                            {entry.transactionData.contact1[0].data.lastName}
                                                                                                        </>
                                                                                                    ) : null
                                                                                                ) : (
                                                                                                    ""
                                                                                                )}
                                                                                            </a>
                                                                                        </td>

                                                                                        <td className="hright">{entry.lastMonthHolding}</td>
                                                                                        <td className="hright">{entry.depositThisMonth}</td>
                                                                                        <td className="hright">{entry.releaseThisMonth}</td>
                                                                                        <td className="hright">{entry.holdingNow}</td>
                                                                                    </tr>
                                                                                );
                                                                            })
                                                                        ) : (
                                                                            ""
                                                                        )
                                                                    }
                                                                </tbody>

                                                            </table>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>




                        </div>
                    </div>
                </main>

            </div >


        </>

    )
}

export default DepositLedgerReport
import React, { useState, useEffect } from "react";
import Modal from "../../Modal";
import user_service from "../service/user_service";
import Loader from "../../Pages/Loader/Loader.js";
import Toaster from "../../Pages/Toaster/Toaster.js";
import jwt from "jwt-decode";
import Pic from "../img/pic.png";
import defaultpropertyimage from "../../Components/img/defaultpropertyimage.jpeg";
import { NavLink } from "react-router-dom";
import ReactPaginate from "react-paginate";

function AgentTestimonials() {
  const [loader, setLoader] = useState({ isActive: null });
  const { isActive } = loader;
  const [toaster, setToaster] = useState({
    types: null,
    isShow: null,
    toasterBody: "",
    message: "",
  });
  const { types, isShow, toasterBody, message } = toaster;

  const [agentData, setAgentData] = useState("");
  const [pageCount, setPageCount] = useState(0);

  useEffect(() => {
    window.scrollTo(0, 0);
    const TestimonialsAllAgent = async () => {
      setLoader({ isActive: true });
      await user_service.TestimonialsGetAll(1).then((response) => {
        setLoader({ isActive: false });
        if (response) {
          setAgentData(response.data.data);
          setPageCount(Math.ceil(response.data.totalRecords / 10));
        }
      });
    };
    TestimonialsAllAgent();
  }, []);

  const handlePageClick = async (data) => {
    try {
      const currentPage = data.selected + 1;
      setLoader({ isActive: true });
      const response = await user_service.TestimonialsGetAll(currentPage);
      setLoader({ isActive: false });
      if (response && response.data) {
        setAgentData(response.data.data);
        setPageCount(Math.ceil(response.data.totalRecords / 10));
      }
    } catch (error) {
      console.error("Error in handlePageClick:", error);
      // Handle errors as needed
    }
  };

  const [contactTestimonials, setContactTestimonials] = useState("");

  const handleTestimonialAgent = async (id) => {
    await user_service.TestimonialsGetId(id).then((response) => {
      if (response) {
        setLoader({ isActive: false });
        setContactTestimonials(response.data);
      }
    });
  };

  const propertypic = (e) => {
    e.target.src = defaultpropertyimage;
  };
  return (
    <div className="bg-secondary float-left w-100 pt-4 mb-4">
      <Loader isActive={isActive} />
      {isShow && (
        <Toaster
          types={types}
          isShow={isShow}
          toasterBody={toasterBody}
          message={message}
        />
      )}
      <main className="page-wrapper profile_page_wrap">
        <div className="">
          <div className="">
          <div className="justify-content-end mb-1">
              <ReactPaginate
                previousLabel={"Previous"}
                nextLabel={"Next"}
                breakLabel={"..."}
                pageCount={pageCount}
                marginPagesDisplayed={1}
                pageRangeDisplayed={2}
                onPageChange={handlePageClick}
                containerClassName={"pagination justify-content-end"}
                pageClassName={"page-item"}
                pageLinkClassName={"page-link"}
                previousClassName={"page-item"}
                previousLinkClassName={"page-link"}
                nextClassName={"page-item"}
                nextLinkClassName={"page-link"}
                breakClassName={"page-item"}
                breakLinkClassName={"page-link"}
                activeClassName={"active"}
              />
            </div>
            <div className="promoted_product float-left w-100">
              <div className="bg-light p-3 border-0 rounded-3 shadow-sm float-start w-100 float-left mb-3 w-100">
                <h4>Agent Testimonials Office Report</h4>
              </div>

              {agentData && agentData.length > 0 ? (
                agentData.map((post) => (
                  <div
                    className="bg-light p-3 border-0 rounded-3 shadow-sm float-start w-100 mb-5"
                    key={post._id}
                  >
                    <div className="">
                      <div className="float-left w-100">
                        <div className="float-left w-100 d-flex align-items-center justify-content-between">
                          <NavLink
                            to={`/contact-profile/${post._id}`}
                            className="float-left w-100 d-flex align-items-center justify-content-start pb-3 border-bottom mb-5"
                          >
                            <img
                              className="rounded-circle profile_picture"
                              height="70"
                              width="70"
                              src={post.clientData[0]?.image || Pic}
                              alt="Profile"
                            />

                            <div className="ms-3">
                              <strong>
                                {post.clientData[0]?.firstName ?? ""}
                              </strong>
                              &nbsp;
                              <strong>
                                {post.clientData[0]?.lastName ?? ""}
                              </strong>
                              <p className="mb-1">
                                {post.clientData[0]?.email
                                  ? post.clientData[0]?.email
                                  : ""}
                              </p>
                            </div>
                          </NavLink>
                        </div>
                        <div className="row">
                          {post.userData && post.userData.length > 0 ? (
                            post.userData.map((testimonial, index) => (
                              <div
                                key={testimonial.id}
                                className="col-lg-3 col-md-3 col-sm-6 col-12 mb-4"
                              >
                                <div className="card shadow-sm cursor-pointer card-hover border-0 h-100">
                                  <div className="card-img-top card-img-hover">
                                    <img
                                      className="img-fluid w-100"
                                      src={
                                        testimonial.image &&
                                        testimonial.image !== "image"
                                          ? testimonial.image
                                          : defaultpropertyimage
                                      }
                                      alt="Property"
                                      onError={(e) => propertypic(e)}
                                    />
                                  </div>
                                  <div className="card-body position-relative p-2 pb-0">
                                    <p className="mb-1">
                                      {Array(parseInt(testimonial.star))
                                        .fill()
                                        .map((_, index) => (
                                          <i
                                            key={index}
                                            style={{
                                              color: "#FFBC0B",
                                              fontSize: "20px",
                                              margin: "2px",
                                            }}
                                            className="fa fa-star"
                                            aria-hidden="true"
                                          ></i>
                                        ))}
                                    </p>

                                    {testimonial.displayName ? (
                                      <p className="mb-2">
                                        <b>Name:</b> {testimonial.displayName}
                                      </p>
                                    ) : (
                                      ""
                                    )}
                                    {testimonial.headlineReview ? (
                                      <p className="mb-2">
                                        <b>{testimonial.headlineReview}</b>
                                      </p>
                                    ) : (
                                      ""
                                    )}
                                    {testimonial.description ? (
                                      <p className="review_description mb-2">
                                        <b>Description</b> :{" "}
                                        {testimonial.description}
                                      </p>
                                    ) : (
                                      ""
                                    )}
                                    <button
                                      className="btn btn-primary"
                                      data-toggle="modal"
                                      data-target="#testimonialUser"
                                      onClick={() =>
                                        handleTestimonialAgent(testimonial._id)
                                      }
                                    >
                                      Read View
                                    </button>
                                  </div>
                                  <hr className="w-100 mt-2 mb-0" />
                                </div>
                              </div>
                            ))
                          ) : (
                            <p>No testimonials available</p>
                          )}
                        </div>
                      </div>
                    </div>
                  </div>
                ))
              ) : (
                <div className="text-center mb-3">
                  <img className="" src={Pic} />
                  <br />
                  <p className="mt-3 text-white">Not Any Agent Testimonials</p>
                </div>
              )}
            </div>
       
          </div>
        </div>

        <div className="modal IN" role="dialog" id="testimonialUser">
          <div
            className="modal-dialog modal-md modal-dialog-scrollable"
            role="document"
          >
            <div className="modal-content">
              <div className="modal-header">
                <h4 className="modal-title" id="myModalLabel">
                  Testimonials
                </h4>
                <button
                  className="btn-close"
                  id="closeModalAttach"
                  type="button"
                  data-dismiss="modal"
                  aria-label="Close"
                ></button>
              </div>

              <div className="modal-body fs-sm">
                <div className="col-md-12">
                  <img
                    src={
                      contactTestimonials.image &&
                      contactTestimonials.image !== "image"
                        ? contactTestimonials.image
                        : defaultpropertyimage
                    }
                    alt="Property"
                    onError={(e) => propertypic(e)}
                  />
                  {contactTestimonials.star &&
                  !isNaN(contactTestimonials.star) &&
                  parseInt(contactTestimonials.star) > 0 ? (
                    <p className="mt-2">
                      {[...Array(parseInt(contactTestimonials.star))].map(
                        (_, index) => (
                          <i
                            key={index}
                            style={{ color: "#FFBC0B", fontSize: "30px" }}
                            className="fa fa-star"
                            aria-hidden="true"
                          ></i>
                        )
                      )}
                    </p>
                  ) : (
                    <p>No star rating available</p>
                  )}

                  {contactTestimonials.displayName ? (
                    <p>
                      <b>Display Name:</b> {contactTestimonials.displayName}
                    </p>
                  ) : (
                    ""
                  )}

                  {contactTestimonials.headlineReview ? (
                    <p>
                      <b>Headline Review:</b>{" "}
                      {contactTestimonials.headlineReview}
                    </p>
                  ) : (
                    ""
                  )}

                  {contactTestimonials.description ? (
                    <p>
                      <b>Description:</b> {contactTestimonials.description}
                    </p>
                  ) : (
                    ""
                  )}
                </div>
              </div>
            </div>
          </div>
        </div>
      </main>
    </div>
  );
}
export default AgentTestimonials;

import React, { useEffect, useState } from "react";
import avtar from "../img/avtar.jpg";
import { NavLink, useNavigate } from "react-router-dom";
import user_service from "../service/user_service.js";
import ReactPaginate from "react-paginate";
import Loader from "../../Pages/Loader/Loader.js";
import Toaster from "../../Pages/Toaster/Toaster.js";
import jwt from "jwt-decode";
import defaultpropertyimage from "../../Components/img/defaultpropertyimage.jpeg";


function DocumentReview() {
  const [activeGet, setActiveGet] = useState([]);
  const [pageCount, setPageCount] = useState(0);
  const [loader, setLoader] = useState({ isActive: null });
  const { isActive } = loader;
  const [toaster, setToaster] = useState({
    types: null,
    isShow: null,
    toasterBody: "",
    message: "",
  });
  const { types, isShow, toasterBody, message } = toaster;
 
  const navigate = useNavigate();

 
  const Catalog = (id, agentID) => {
    if (localStorage.getItem("auth")) {
      const decodedToken = jwt(localStorage.getItem("auth"));
      if (decodedToken.contactType === "admin" || agentID === decodedToken.id) {
        navigate(`/transaction-summary/${id}`);
      } else {
        navigate(`/transaction`);
      }
    }
  };


  useEffect(() => {
    window.scrollTo(0, 0);
    const DocumentReview = async () => {
        setLoader({ isActive: true })
        await user_service.documentReviewGet(1).then((response) => {
            setLoader({ isActive: false })
            if (response) {
                setActiveGet(response.data.data);
                setPageCount(Math.ceil(response.data.totalCount / 10));
            }
        });
    }
    DocumentReview()
  }, []);

  
  const handlePageClick = async (data) => {
    try {
      const currentPage = data.selected + 1;
      setLoader({ isActive: true });
      const response = await user_service.documentReviewGet(currentPage);
      setLoader({ isActive: false });
      if (response && response.data) {
        setActiveGet(response.data.data);
        setPageCount(Math.ceil(response.data.totalCount / 10));
      }
    } catch (error) {
      console.error("Error in handlePageClick:", error);
    }
  };

 
  const propertypic = (e) => {
    e.target.src = defaultpropertyimage;
  };

  return (
    <div className="bg-secondary float-left w-100 pt-4 mb-4">
      <Loader isActive={isActive} />
      {isShow && (
        <Toaster
          types={types}
          isShow={isShow}
          toasterBody={toasterBody}
          message={message}
        />
      )}
      <main className="page-wrapper">
        <div className="content-overlay">
          <div className="row">
            <div className="col-lg-12  bg-light p-3 border-0 rounded-3 shadow-sm float-start w-100">
              <h6>Document Review Report</h6>

              {activeGet && activeGet.length > 0 ? (
                activeGet.map((post) => (
                  <React.Fragment key={post._id}>
                    {post.propertyDetail ? (
                      <div className="card card-hover card-horizontal transactions border cursor-pointer mb-4 p-3 transactionImage">
                        <a
                          className="card-img-top"
                          onClick={(e) => Catalog(post._id,post.agentId)}
                        >
                          <img
                            className="img-fluid w-100"
                            src={
                              post.propertyDetail.image &&
                              post.propertyDetail.image !== "image"
                                ? post.propertyDetail.image
                                : defaultpropertyimage
                            }
                            alt="Property"
                            onError={(e) => propertypic(e)}
                          />
                        </a>
                        <div className="card-body position-relative pb-0 pt-0">
                          <div>
                          <object>
                                  <a
                                    className="btn btn-primary btn-sm pull-right mb-"
                                    href={`/transactionDoc/${post._id}/${post.documents[0]?._id}`}
                                    target="_blank"
                                    rel="noreferrer"
                                  >
                                   Review Now
                                  </a>
                                </object>
                            <h6 className="mb-2 w-75">
                              {post.propertyDetail.streetAddress},{" "}
                              {post.propertyDetail.city},
                              {post.propertyDetail.state}
                            </h6>
                            {post.contact1 && post.contact1.length > 0 ? (
                              post.contact1[0] !== null ? (
                                <p
                                  className="mb-2 fs-sm text-muted"
                                  key={post.contact1[0]._id}
                                >
                                  {post.contact1[0].organization &&
                                  post.contact1[0].organization !== ""
                                    ? post.contact1[0].organization
                                    : post.contact1[0].firstName}
                                  &nbsp;{post.contact1[0].lastName}
                                </p>
                              ) : null
                            ) : (
                              <p></p>
                            )}
                            <p className="mb-2 fs-sm text-muted">
                              {post.propertyDetail.listingsAgent}
                            </p>

                            {localStorage.getItem("auth") &&
                            jwt(localStorage.getItem("auth")).contactType ==
                              "admin" ? (
                              <>
                                <p className="mb-2 fs-sm text-muted">
                                  <i
                                    className="fa fa-home me-1"
                                    aria-hidden="true"
                                  ></i>
                                  {post.represent === "seller"
                                    ? "Seller"
                                    : post.represent === "buyer"
                                    ? "Buyer"
                                    : post.represent === "both"
                                    ? "Buyer & Seller"
                                    : post.represent === "referral"
                                    ? "Referral"
                                    : ""}
                                </p>
                              </>
                            ) : (
                              <>
                                <p className="mb-2 fs-sm text-muted">
                                  <i
                                    className="fa fa-home me-1"
                                    aria-hidden="true"
                                  ></i>
                                  {post.represent === "seller"
                                    ? "Seller"
                                    : post.represent === "buyer"
                                    ? "Buyer"
                                    : post.represent === "both"
                                    ? "Buyer & Seller"
                                    : post.represent === "referral"
                                    ? "Referral"
                                    : ""}
                                </p>
                              </>
                            )}

                                {post.phase ? (
                                  <span className="d-table badge bg-success">
                                    {post.phase === "pre-listed"
                                      ? "Pre-Listed"
                                      : post.phase === "active-listing"
                                      ? "Active Listing"
                                      : post.phase === "showing-homes"
                                      ? "Showing Homes"
                                      : post.phase === "under-contract"
                                      ? "Under Contract"
                                      : post.phase === "closed"
                                      ? "Closed"
                                      : post.phase === "canceled"
                                      ? "Canceled"
                                      : ""}
                                  </span>
                                ) : (
                                  ""
                                )}
                           
                                {post.contact3 && post.contact3.length > 0 ? (
                                  post.contact3[0] !== null ? (
                                    <div className="d-flex mt-2">
                                      <b className="">Agent:</b>&nbsp;
                                      <p
                                        className="mb-2 mt-1 fs-sm text-muted"
                                        key={post.contact3[0]._id}
                                      >
                                        {post.contact3[0].organization &&
                                        post.contact3[0].organization !==
                                          ""
                                          ? post.contac31[0].organization
                                          : post.contact3[0].firstName}
                                        &nbsp;{post.contact3[0].lastName}
                                      </p>
                                    </div>
                                  ) : null
                                ) : (
                                  <p></p>
                                )}
                               {
                                post.reference_no ?
                                <p><b>Ref# :</b> {post.reference_no}</p>
                                :""
                               }
                            
                          </div>
                         
                        </div>
                      </div>
                    ) : (
                      <>
                        <div className="card card-hover card-horizontal transactions border cursor-pointer mb-4 p-3">
                          <a
                            className="card-img-top"
                            onClick={(e) => Catalog(post._id)}
                          >
                            <img
                              className="img-fluid w-100"
                              src="https://integra.workspace.lwolf.com/common/handler/mls/photo_custom.ashx?s=1&t=5eada3e3-491e-46e2-938c-7c785a1f9c68&r=Seller"
                              alt="Property"
                              onError={(e) => propertypic(e)}
                            />
                          </a>
                          <div className="card-body position-relative pb-0 pt-0">
                            <h6 className="mb-2">Property Not Set</h6>
                            {post.contact1 && post.contact1.length > 0 ? (
                              // post.contact1[0].map((item) => (
                              post.contact1[0] !== null ? (
                                <>
                                  <p
                                    className="mb-2 fs-sm text-muted"
                                    key={post.contact1[0]._id}
                                  >
                                    {post.contact1[0].organization &&
                                    post.contact1[0].organization !== ""
                                      ? post.contact1[0].organization
                                      : post.contact1[0].firstName}
                                    &nbsp;{post.contact1[0].lastName}
                                  </p>
                                  <p className="mb-2 fs-sm text-muted">
                                    {post.contact1[0].firstName}&nbsp;
                                    {post.contact1[0].lastName}
                                  </p>
                                </>
                              ) : null
                            ) : (
                              ""
                            )}
                            <p className="mb-2 fs-sm text-muted">
                              <i className="fa fa-home me-1" aria-hidden="true"></i>
                              {post.represent === "seller"
                                ? "Seller"
                                : post.represent === "buyer"
                                ? "Buyer"
                                : post.represent === "both"
                                ? "Buyer & Seller"
                                : post.represent === "referral"
                                ? "Referral"
                                : ""}
                            </p>
                          </div>
                        </div>
                      </>
                    )}
                  </React.Fragment>
                ))
              ) : (
                <p className="text-center mt-5">
                  <i
                    className="fa fa-calculator mb-5"
                    aria-hidden="true"
                    style={{ fontSize: "135px" }}
                  ></i>
                  <br />
                  <span className="text-white">No Active Transactions</span>
                </p>
              )}
           
            </div>

            <div className="justify-content-end mb-1">
              <ReactPaginate
                previousLabel={"Previous"}
                nextLabel={"Next"}
                breakLabel={"..."}
                pageCount={pageCount}
                marginPagesDisplayed={1}
                pageRangeDisplayed={2}
                onPageChange={handlePageClick}
                containerClassName={"pagination justify-content-center"}
                pageClassName={"page-item"}
                pageLinkClassName={"page-link"}
                previousClassName={"page-item"}
                previousLinkClassName={"page-link"}
                nextClassName={"page-item"}
                nextLinkClassName={"page-link"}
                breakClassName={"page-item"}
                breakLinkClassName={"page-link"}
                activeClassName={"active"}
              />
            </div>
          </div>
        </div>
      </main>
    </div>
  );
}

export default DocumentReview;

import React, { useState, useEffect } from "react";
import { Calendar, momentLocalizer } from "react-big-calendar";
import moment from "moment-timezone";
import "react-big-calendar/lib/css/react-big-calendar.css";
import Loader from "../../Pages/Loader/Loader.js";
import Toaster from "../../Pages/Toaster/Toaster.js";
import { NavLink, useNavigate } from "react-router-dom";
import user_service from "../service/user_service";
import jwt from "jwt-decode";
import _ from "lodash";

const localizer = momentLocalizer(moment);

const eventColors = {
  meeting: "#FF5733",
  duty: "#3366FF",
};

const CloseCalendar = () => {
  const CustomToolbar = () => {
    return (
      <div className="rbc-toolbar mt-3">
        <span className="rbc-toolbar-label"></span>
        <span className="rbc-toolbar-label"></span>
      </div>
    );
  };

  const [calenderGet, setCalenderGet] = useState([]);
  
  const [countData, setCountData]= useState("")
  const [totalCountCommission, setTotalCountCommission]= useState("")

  const [events, setEvents] = useState([]);
  const [selectedDate, setSelectedDate] = useState(null);
  const [loader, setLoader] = useState({ isActive: null });
  const { isActive } = loader;
  const [toaster, setToaster] = useState({
    types: null,
    isShow: null,
    toasterBody: "",
    message: "",
  });
  const { types, isShow, toasterBody, message } = toaster;

  const navigate = useNavigate();

  useEffect(() => {
    TransactionCalenderGet();
    
}, []);

const eventStyleGetter = (event, start, end, isSelected) => {
  const backgroundColor = event.color;
  return {
    style: {
      backgroundColor,
      borderRadius: '0px',
      opacity: 0.8,
      color: 'white',
      border: '0px',
      display: 'block',
    },
  };
};



  const handleSelectSlot = (slotInfo) => {
    const today = moment().startOf("day");
    const selectedDate = moment(slotInfo.start).startOf("day");

    if (
      localStorage.getItem("auth") &&
      jwt(localStorage.getItem("auth")).contactType === "admin"
    ) {
      if (selectedDate.isSameOrAfter(today)) {
        setSelectedDate(slotInfo);
        window.$("#modal-show-calender").modal("show");
      }
    }
  };

  const handleEventClick = (event) => {
    event.preventDefault();
    const eventId = event.currentTarget.getAttribute("data-eventid");
    navigate(`/transaction-summary/${eventId}`);
  };


  const TransactionCalenderGet = async () => {
    try {
        setLoader({ isActive: true });
  
        let queryParams;
        if(localStorage.getItem("auth") && jwt(localStorage.getItem("auth")).contactType == "admin" ){
           queryParams = `?page=1`;
        }else{
           queryParams = `?page=1&agentId=`+jwt(localStorage.getItem("auth")).id;
        }
      
        const response = await user_service.TransactionCalenderGet(queryParams);
  
        setLoader({ isActive: false });
  
        if (response && response.data) {
          console.log(response);
          setCalenderGet(response.data);
        }
    } catch (error) {
        console.error('Error in fetchCalendar:', error);
        // Handle errors as needed, e.g., show a user-friendly error message
    }
  };
  

useEffect(() => {
  fetchTransactionData();
}, []);

const [selectedMonth, setSelectedMonth] = useState(moment());


const generateEvents = (selectedMonth) => {
  const generatedEvents = [];

  if (calenderGet && calenderGet.data) {
    calenderGet.data.forEach((date) => {
      let startDate;
      let endDate;

      if (date.settlement_deadline && date.settlement_deadline.includes("/")) {
        startDate = moment.tz(date.settlement_deadline, "M/D/YYYY", "America/Denver").toDate();
        endDate = moment.tz(date.settlement_deadline, "M/D/YYYY", "America/Denver").toDate();
      } else if (date.settlement_deadline) {
        startDate = moment.tz(date.settlement_deadline, "America/Denver").toDate();
        endDate = moment.tz(date.settlement_deadline, "America/Denver").toDate();
      } else {
        // console.warn("startDate or endDate is undefined for:", date);
        return;
      }

      if (!startDate || !endDate) {
        console.warn("startDate or endDate is undefined for:", date);
        startDate = new Date();
        endDate = new Date(); 
      }

      generatedEvents.push({
        title: "Scheduled Settlement",
        start: startDate,
        end: endDate,
        type: "meeting",
        id: date._id,
        color: date.filing === "filed_complted" ? 'red' : '#151e43',
      });
    });
  }

  const selectedMonthEvents = generatedEvents.filter(event => {
    return moment(event.start).isSame(selectedMonth, 'month') && moment(event.start).isSame(selectedMonth, 'year');
  });

  setCountData(selectedMonthEvents);

  return generatedEvents;
};

useEffect(() => {
  const generatedEvents = generateEvents(selectedMonth);
  setEvents(generatedEvents);
}, [calenderGet, selectedMonth]);

  const handleNavigate = (date, view, action) => {
    switch (action) {
      case "TODAY":
        const today = moment(); 
        setSelectedMonth(today); 
        const todayEvents = generateEvents(today);
        setEvents(todayEvents); 
        break;
      case "PREV":
        const previousMonth = moment(selectedMonth).subtract(1, 'month');
        setSelectedMonth(previousMonth);
        const previousMonthEvents = generateEvents(previousMonth);
        setEvents(previousMonthEvents);
        break;
      case "NEXT":
        const nextMonth = moment(selectedMonth).add(1, 'month');
        setSelectedMonth(nextMonth);
        const nextMonthEvents = generateEvents(nextMonth);
        setEvents(nextMonthEvents);
        break;
        default:
        break;
    }
  };
  
  


 const [formValues, setFormValues] = useState([])

  const fetchTransactionData = async () => {
    try {
      setLoader({ isActive: true });
      const response = await user_service.Getfilledtransactionsmonthwise();
      if (response && response.data && response.data.monthlyResultearning) {
        setLoader({ isActive: false });
  
        setFormValues(response.data.monthlyResultearning);
      } else {
        console.error('Invalid response data structure:', response);
        setLoader({ isActive: false });
      }
    } catch (error) {
      console.error('Error fetching filled transactions monthwise:', error);
      setLoader({ isActive: false });
    }
  };

  useEffect(() => {
    const selectedYear = selectedMonth.year();
    const selectedMonthNumber = selectedMonth.month() + 1;
  
    // Filter formValues to match the selected year and month
    const selectedMonthData = formValues.filter(item => {
      return item._id.year === selectedYear && item._id.month === selectedMonthNumber;
    });
  
    console.log(selectedMonthData, "selectedMonthData");
  
    // Calculate the total commission
    const totalCommission = selectedMonthData
      .map(item => item.transactiondata) // Extract transaction data arrays
      .flat() // Flatten nested arrays
      .map(item => item.details) // Extract the `details` field
      .filter(details => details) // Remove undefined or null values
      .reduce((accumulator, details) => {
        // Filter payout items for "In Depth Realty"
        const payoutAmounts = (details.payoutItem || []) // Ensure `payoutItem` exists
          .filter(payoutItem => payoutItem.payto === "In Depth Realty") // Match specific payto
          .map(payoutItem => parseFloat(payoutItem.amount) || 0); // Parse amounts as floats and default to 0
  
        // Calculate the sum of payout amounts
        const payoutTotal = payoutAmounts.reduce((acc, curr) => acc + curr, 0);
  
        // Add all fees and payout totals to the accumulator
        return (
          accumulator +
          (parseFloat(details.transactionFee) || 0) +
          (parseFloat(details.wireFee) || 0) +
          (parseFloat(details.transactionPersonal) || 0) +
          (parseFloat(details.managementFee) || 0) +
          (parseFloat(details.highValueFee) || 0) +
          (parseFloat(details.sellerFee) || 0) +
          (parseFloat(details.buyerFee) || 0) +
          payoutTotal
        );
      }, 0);
  
    // Ensure the totalCommission is a fixed-point number for better readability
    setTotalCountCommission(totalCommission.toFixed(2));
  }, [selectedMonth, formValues]);
  
  
const handleClickCount= ()=>{
  navigate(`/close-calendar-count`);
 }

   const handlePage= ()=>{
    navigate(`/funding-income`);
   }
  

  return (
    <div className="bg-secondary float-left w-100 pt-4 mb-4">
      <Loader isActive={isActive} />
      {isShow && (
        <Toaster
          types={types}
          isShow={isShow}
          toasterBody={toasterBody}
          message={message}
        />
      )}
      <main className="closing_calender">
        <div className="row">
          <div className="col-md-12">
            <h3 className="text-white mb-4">Closing Calendar</h3>
            <div className="bg-light border rounded-3 p-3">
              <div className="">
                <div className="float-left w-100">
                <span style={{ cursor: "pointer" }} className="ms-4" onClick={handleClickCount}><b>Count:</b> {countData.length}</span>
                {console.log(totalCountCommission, "totalCountCommission")}
                  <span style={{ cursor: "pointer" }}  onClick={handlePage} className="ms-5"><b>Income:</b> ${totalCountCommission}</span>
                </div>
                <div className="row mt-3 pt-0">
               
                  <div style={{ height: "500px", marginTop:"10px" }}>
                    <Calendar
                      localizer={localizer}
                      events={events}
                      startAccessor="start"
                      endAccessor="end"
                      eventPropGetter={eventStyleGetter}
                      style={{ flex: 1 }}
                      onSelectSlot={handleSelectSlot}
                      selectable={true}
                      onNavigate={handleNavigate}
                      components={{
                        event: ({ event }) => (
                          <div
                            className="rbc-event-content"
                            title={event.title}
                            onClick={handleEventClick}
                            data-eventid={event.id}
                          >
                            {event.title}
                          </div>
                        ),
                      }}
                      views={{
                        month: true,
                        day: true,
                        week: false,
                        agenda: false,
                      }}
                      component={{
                        toolbar: CustomToolbar,
                       
                      }}
                    />
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </main>
    </div>
  );
};

export default CloseCalendar;

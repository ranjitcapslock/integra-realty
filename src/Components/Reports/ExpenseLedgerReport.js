import React, { useState, useEffect } from "react";
import user_service from "../service/user_service";
import Loader from "../../Pages/Loader/Loader.js";
import Toaster from "../../Pages/Toaster/Toaster.js";
import { CSVLink } from "react-csv";
import ReactPaginate from "react-paginate";
import { NavLink, useNavigate } from "react-router-dom";

const ExpenseLedgerReport = () => {
  const [loader, setLoader] = useState({ isActive: null });
  const { isActive } = loader;
  const [toaster, setToaster] = useState({
    types: null,
    isShow: null,
    toasterBody: "",
    message: "",
  });
  const { types, isShow, toasterBody, message } = toaster;
  const [expenseResults, setExpenseResults] = useState([]);
  const [entrytype, setEntrytype] = useState("expenses");
  const [pageCount, setPageCount] = useState(0);
  const navigate = useNavigate();

  const ExpenseLedgerAll = async (entrytype) => {
    try {
      let queryParams = `?page=1&type=${entrytype}`;
      const response = await user_service.expenseLedgerAllGet(queryParams);
      setLoader({ isActive: false });

      if (response.data && response.data.data) {
        setExpenseResults(response.data.data);
        setPageCount(Math.ceil(response.data.totalRecords / 10));
      }
    } catch (error) {
      console.error("Error fetching expense ledger:", error);
    }
  };

  const handlePageClick = async (data) => {
    try {
      const currentPage = data.selected + 1;
      setLoader({ isActive: true });
      const queryParams = `?page=${currentPage}&type=${entrytype}`;
      const response = await user_service.expenseLedgerAllGet(queryParams);

      setLoader({ isActive: false });

      if (response && response.data) {
        setExpenseResults(response.data.data);
        setPageCount(Math.ceil(response.data.totalRecords / 10));
      }
    } catch (error) {
      console.error("Error in handlePageClick:", error);
      // Handle errors as needed
    }
  };

  useEffect(() => {
    window.scrollTo(0, 0);
    setLoader({ isActive: true });
    ExpenseLedgerAll(entrytype);
  }, []);

  const changetype = (e) => {
    const { name, value } = e.target;
    setEntrytype(value);
    ExpenseLedgerAll(value);
  };

  const handleNavigate = (id)=>{
    navigate(`/transaction-summary/${id}`);
  }
  return (
    <div className="bg-secondary float-left w-100 pt-4 mb-4">
      <Loader isActive={isActive} />
      {isShow && (
        <Toaster
          types={types}
          isShow={isShow}
          toasterBody={toasterBody}
          message={message}
        />
      )}
      <main className="page-wrapper getContact_page_wrap">
        <div className="content-overlay mt-0">
          <div className="">
            <div className="row">
              <div className="col-md-12">
                <div className="row">
                  <div className="table-responsive bg-light border rounded-3 p-3 mt-3">
                    <div className="mb-3">
                      <h6 className="">Search Criteria</h6>
                      <div className="col-md-6">
                        <div className="d-flex align-items-center justify-content-start">
                          <select
                            className="form-select form-select-dark"
                            name="entrytype"
                            value={entrytype}
                            onChange={changetype}
                          >
                            <option value="expenses">Expenses</option>
                            <option value="mileage">Mileage</option>
                          </select>
                          {expenseResults ? (
                            <CSVLink
                              data={expenseResults}
                              className="learn-more pull-right ms-4"
                              filename={`Integra-users-data.csv`}
                            >
                              Export
                            </CSVLink>
                          ) : (
                            ""
                          )}
                        </div>
                      </div>
                      <div className="mt-3">
                        <div style={{ width: "600px;" }}>
                          Summary of {entrytype} held in trust.
                        </div>
                      </div>
                    </div>

                    <table
                      id="ExpenseReport"
                      className="table table-striped align-middle mb-0 w-100"
                      width="100%"
                      border="0"
                      cellSpacing="0"
                      cellPadding="0"
                    >
                      <colgroup>
                        <col width="20%" />
                        <col width="20%" />
                        <col width="10%" />
                        <col width="20%" />
                        <col width="20%" />
                        <col width="10%" />
                      </colgroup>

                      <thead>
                        <tr className="p-3">
                          <th className="p-3">Date</th>
                          <th className="p-3">Transaction</th>
                          <th className="p-3">Paid For</th>
                          <th className="p-3">Paid To</th>
                          <th className="p-3">Description</th>
                          <th className="p-3">Amount</th>
                        </tr>
                      </thead>

                      <tbody>
                        {expenseResults.length > 0 ? (
                          <>
                            {expenseResults.map((entry) => (
                              //entry.type === "expenses" ?
                              <tr className="p-3" key={entry._id} onClick={()=>handleNavigate(entry.transactionId)}>
                                <td className="p-3">
                                  <div style={{ width: "120px" }}>
                                    {new Date(entry.entryDate).toLocaleString(
                                      "en-US",
                                      {
                                        weekday: "short",
                                        month: "short",
                                        day: "numeric",
                                        year: "numeric",
                                      }
                                    )}
                                  </div>
                                </td>
                                <td>{entry.transactionId}</td>
                                <td className="p-3">{entry.PaidFor}</td>
                                <td className="p-3">{entry.PaidTo}</td>
                                <td className="p-3">{entry.description}</td>
                                <td className="p-3">{entry.amountPaid}</td>
                              </tr>
                              //  :""
                            ))}
                            <tr>
                              <td colSpan="5" style={{ textAlign: "right" }}>
                                <b>Total:</b>
                              </td>
                              <td className="p-3">
                                {expenseResults
                                  .filter((entry) => entry.type === entrytype) // Filter entries of type "expenses"
                                  .reduce(
                                    (total, entry) => total + entry.amountPaid,
                                    0
                                  )}
                              </td>
                            </tr>
                          </>
                        ) : (
                          <tr>
                            <td colSpan="6">No expense entries found.</td>
                          </tr>
                        )}
                      </tbody>
                    </table>
                  </div>

                  <div className="d-flex align-items-center justify-content-end mb-0 mt-4">
                    <ReactPaginate
                      previousLabel={"Previous"}
                      nextLabel={"Next"}
                      breakLabel={"..."}
                      pageCount={pageCount}
                      marginPagesDisplayed={1}
                      pageRangeDisplayed={2}
                      onPageChange={handlePageClick}
                      containerClassName={"pagination justify-content-center"}
                      pageClassName={"page-item"}
                      pageLinkClassName={"page-link"}
                      previousClassName={"page-item"}
                      previousLinkClassName={"page-link"}
                      nextClassName={"page-item"}
                      nextLinkClassName={"page-link"}
                      breakClassName={"page-item"}
                      breakLinkClassName={"page-link"}
                      activeClassName={"active"}
                    />
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </main>
    </div>
  );
};

export default ExpenseLedgerReport;

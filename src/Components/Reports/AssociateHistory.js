import React, { useState, useEffect } from "react";
import Pic from "../img/pic.png";
import user_service from "../service/user_service";
import ReactPaginate from "react-paginate";
import Loader from "../../Pages/Loader/Loader.js";
import Toaster from "../../Pages/Toaster/Toaster.js";
import { Line } from 'react-chartjs-2';


function AssociateHistory() {
    const [chartData, setChartData] = useState({
        labels: [],
        datasets: [
          {
            label: 'Active Agents',
            data: [],
            borderColor: 'rgba(75,192,192,1)',
            fill: false,
          },
          {
            label: 'Deleted Agents',
            data: [],
            borderColor: 'rgba(255,99,132,1)',
            fill: false,
          },
        ],
      });

  const [loader, setLoader] = useState({ isActive: null });
  const { isActive } = loader;
  const [toaster, setToaster] = useState({
    types: null,
    isShow: null,
    toasterBody: "",
    message: "",
  });
  const { types, isShow, toasterBody, message } = toaster;

 
  useEffect(() => {
    window.scrollTo(0, 0);
    const ContactGetAllsss = async () => {
        setLoader({ isActive: true })
        await user_service.associateReportContact().then((response) => {
            setLoader({ isActive: false })
            if (response) {
                const data = response.data.data;
                const labels = data.map(item => item.month);
                const activeData = data.map(item => item.active);
                const deletedData = data.map(item => item.deleted);
        
                setChartData({
                  labels,
                  datasets: [
                    {
                      label: 'Active',
                      data: activeData,
                      borderColor: 'rgba(75,192,192,1)',
                      fill: false,
                    },
                    {
                      label: 'Left',
                      data: deletedData,
                      borderColor: 'rgba(255,99,132,1)',
                      fill: false,
                    },
                  ],
                });
            }
        });
    }
    ContactGetAllsss()
  }, []);


  return (
    <div className="bg-secondary float-left w-100 pt-4 mb-4">
      <Loader isActive={isActive} />
      {isShow && (
        <Toaster
          types={types}
          isShow={isShow}
          toasterBody={toasterBody}
          message={message}
        />
      )}
      <main className="page-wrapper">
        <div className="content-overlay">
          <div className="">
            <div className="row">
              <div className="col-md-12">
              <div className="bg-light p-3 border-0 rounded-3 shadow-sm float-start w-100">
                <div className="float-left w-100 d-flex align-items-center justify-content-between mb-4">
                  <h3 className="mb-0">Active Associate History</h3>
                </div>
                <Line data={chartData} />
                </div>

              </div>
           
            </div>
          </div>
        </div>
      </main>
    </div>
  );
}
export default AssociateHistory;

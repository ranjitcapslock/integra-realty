import React, { useState, useEffect } from "react";
import Pic from "../img/pic.png";
import user_service from "../service/user_service";
import ReactPaginate from "react-paginate";
import Loader from "../../Pages/Loader/Loader.js";
import Toaster from "../../Pages/Toaster/Toaster.js";
import { NavLink} from "react-router-dom";

function SponsorAssociate() {
  const [getContact, setGetContact] = useState([]);
  const [pageCount, setPageCount] = useState(0);
  const [loader, setLoader] = useState({ isActive: null });
  const { isActive } = loader;
  const [toaster, setToaster] = useState({
    types: null,
    isShow: null,
    toasterBody: "",
    message: "",
  });
  const { types, isShow, toasterBody, message } = toaster;


 
  useEffect(() => {
    window.scrollTo(0, 0);
    const ContactGetSponsor = async () => {
        setLoader({ isActive: true })
        await user_service.sponsorReportContact(1).then((response) => {
            setLoader({ isActive: false })
            if (response) {
              setGetContact(response.data.data);
          setPageCount(Math.ceil(response.data.totalCount / 10));

            }
        });
    }
    ContactGetSponsor()
  }, []);

  const handlePageClick = async (data) => {
    let currentPage = data.selected + 1;
    let skip = currentPage - 1 + currentPage;
    setLoader({ isActive: true });
    console.log(currentPage);
    await user_service
      .sponsorReportContact(currentPage)
      .then((response) => {
        setLoader({ isActive: false });
        if (response) {
          setGetContact(response.data.data);
          setPageCount(Math.ceil(response.data.totalCount / 10));
        }
      });
  };

 
  return (
    <div className="bg-secondary float-left w-100 pt-4">
      <Loader isActive={isActive} />
      {isShow && (
        <Toaster
          types={types}
          isShow={isShow}
          toasterBody={toasterBody}
          message={message}
        />
      )}
      <main className="page-wrapper">
        {/* <!-- Page container--> */}
        <div className="container content-overlay">
          <div className="row">
            {/* <!-- List of resumes--> */}
            <div className="col-md-12">
              <div className="bg-light p-3 border-0 rounded-3 shadow-sm float-start w-100">
                <h5>Associate Sponsorship Report</h5>
             
                {getContact && getContact.length > 0 ? (
                  getContact.map((post) => (
                  <div className="card bg-secondary card-hover mb-2 mt-4">
                    <div className="card-body">
                      <div className="float-left w-100">
                          <div className="float-left w-100 d-lg-flex d-md-flex d-sm-flex d-block align-items-center justify-content-between">
                          <div
                            className="float-left w-100 d-flex align-items-center justify-content-start"
                            key={post._id}
                          >
                            <img
                              className="rounded-circlee profile_picture"
                              height="70"
                              width="70"
                              src={post.image || Pic}
                              alt="Profile"
                            />

                            {/* <div className="ms-3" onClick={()=>handleSubmit(post._id)}> */}
                            <div className="ms-3">
                              <strong>{post.firstName}</strong>&nbsp;
                              <strong>{post.lastName}</strong>
                              <p className="mb-1">
                                {post.additionalActivateFields.office
                                  ? post.additionalActivateFields.office
                                  : post.contactType}
                              </p>
                            </div>
                          </div>
                        
                            <span className="pull-left tags-badge d-lg-flex d-md-flex d-sm-block d-block align-items-center justify-content-end w-100 mt-lg-0 mt-md-0 mt-sm-3 mt-3">
                           {post.additionalActivateFields?.sponsor_associate ?
                            <p className="mb-0"> <b>Sponsor</b> : {post.additionalActivateFields?.sponsor_associate}</p>
                            :""
                            }
                          </span>

                            <span className="pull-right tags-badge d-flex align-items-center justify-content-end w-100">
                            <NavLink
                              to={`/contact-profile/${post._id}`}
                              className=""
                            >
                              <i className="fi-edit"></i>
                            </NavLink>
                          </span>
                        </div>
                      </div>
                    </div>
                  </div>
                ))
                ) : (
                  <div className="text-center mb-3">
                    <img className="" src={Pic} />
                    <br />
                    <p className="mt-3 text-white">Not Any Inactive Associate.</p>
                  </div>
                )}
              </div>
            </div>
            <div className="d-flex align-items-center justify-content-end mb-0 mt-4">
              <ReactPaginate
                className=""
                previousLabel={"Previous"}
                nextLabel={"Next"}
                breakLabel={"..."}
                pageCount={pageCount}
                marginPagesDisplayed={1}
                pageRangeDisplayed={2}
                onPageChange={handlePageClick}
                containerClassName={"pagination justify-content-center mb-0"}
                pageClassName={"page-item"}
                pageLinkClassName={"page-link"}
                previousClassName={"page-item"}
                previousLinkClassName={"page-link"}
                nextClassName={"page-item"}
                nextLinkClassName={"page-link"}
                breakClassName={"page-item"}
                breakLinkClassName={"page-link"}
                activeClassName={"active"}
              />
            </div>
          </div>
        </div>
      </main>
    </div>
  );
}
export default SponsorAssociate;

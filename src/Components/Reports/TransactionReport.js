import React, { useState,useEffect } from "react";
import user_service from "../service/user_service";

import Loader from "../../Pages/Loader/Loader.js";
import Toaster from "../../Pages/Toaster/Toaster";
  import ChartDataLabels from "chartjs-plugin-datalabels";

import { Bar } from 'react-chartjs-2';
import  Chart  from 'chart.js/auto';
import { Line } from 'react-chartjs-2';

const TransactionReport = () => {
    const [loader, setLoader] = useState({ isActive: null });
    const [toaster, setToaster] = useState({
      types: null,
      isShow: null,
      toasterBody: "",
      message: "",
    });

    const [formValues, setFormValues] = useState([])
    const [formValuesNew, setFormValuesNew] = useState("")

    const [formValuesVolume, setFormValuesVolume] = useState([])


   
 
    useEffect(() => {
        const fetchTransactionData = async () => {
          try {
            setLoader({ isActive: true });
            const response = await user_service.Getfilledtransactionsmonthwise();
            if (response && response.data && response.data.monthlyResultearning) {
              setFormValues(response.data.monthlyResultearning);
              setLoader({ isActive: false });

              const newResponce = response.data
              setFormValuesNew(newResponce);

              const newDataRespoce = response.data.monthlyTransactionCount
              setFormValuesVolume(newDataRespoce);
            } else {
              console.error('Invalid response data structure:', response);
              setLoader({ isActive: false });
            }
          } catch (error) {
            console.error('Error fetching filled transactions monthwise:', error);
            setLoader({ isActive: false });
          }
        };
    
        fetchTransactionData();
      }, []);

     

    Chart.register(ChartDataLabels);

    const monthNames = [
        'January', 'February', 'March', 'April', 'May', 'June',
        'July', 'August', 'September', 'October', 'November', 'December'
      ];
      const data = {
        labels: formValuesVolume.map(item => {
          const monthIndex = item._id.month - 1;
          const monthName = monthNames[monthIndex];
          const year = item._id.year;
          return `${monthName} ${year}`;
        }),
        datasets: [
          {
            label: 'Trans:', 
            backgroundColor: 'rgba(202, 199, 201, 0.8)',
            data: formValuesVolume.map(item => item.count),
            fill: false,
          },
        ],
      };

   
    const totalCount = formValuesVolume.reduce((accumulator, item) => accumulator + item.count, 0);

    const options = {
        plugins: {
          datalabels: {
            color: "black",
            labels: {
              title: {
                font: {
                  weight: "bold"
                }
              }
            },
            anchor: "end",
            align: "-90"
          },
          legend: {
            position: "top",
            labels: {
              boxHeight: 10,
              boxWidth: 2
            }
          }
        }
      };

  

      const monthNamesVolume = [
        'January', 'February', 'March', 'April', 'May', 'June',
        'July', 'August', 'September', 'October', 'November', 'December'
      ];
      
      const uniqueMonthYears = new Set();
      
      formValuesVolume.forEach(item => {
        const monthIndex = item._id.month - 1;
        const monthName = monthNamesVolume[monthIndex];
        const year = item._id.year;
        const monthYear = `${monthName} ${year}`;
        uniqueMonthYears.add(monthYear);
      });
      
      const uniqueMonthYearArray = [...uniqueMonthYears];
      
      const datanew = {
        labels: uniqueMonthYearArray,
        datasets: [
          {
            data: formValuesVolume.map(item => item.purchase_price),
            label: '',
            backgroundColor: 'rgba(202, 199, 201, 0.8)',
            hoverBackgroundColor: 'blue',
            lineTension: 0,
            fill: true,
            cubicInterpolationMode: 'monotone',
            tension: 0.5,
            borderColor: 'rgba(202, 199, 201, 0.8)',
            pointRadius: 10,
            pointHoverRadius: 15,
          },
        ],
      };
      
    
    
      const optionsData = {
        responsive: true,
        plugins: {
          legend: { display: false },
          datalabels: {
            color: 'gray',
          },
          tooltip: {
            titleColor: 'gray',
            backgroundColor: 'rgba(0, 0, 0, 0.9)',
            bodyColor: 'green',
            displayColors: false,
            yAlign: 'bottom',
          },
        },
        scales: {
          xAxes: {
            grid: {
              lineWidth: 0.5,
              display: false,
              borderColor: 'transparent',
              tickColor: 'transparent',
            },
            ticks: {
              color: 'green',
              font: {
                size: 18,
              },
            },
          },
          yAxis: {
            beginAtZero: true,
            grid: {
              color: 'black',
              lineWidth: 0.5,
              tickColor: 'transparent',
              borderColor: 'transparent',
            },
            ticks: {
              color: 'black',
              font: {
                size: 16,
              },
            },
          },
        },
      };

    const totalCountVolume = formValuesVolume.reduce((accumulator, item) => accumulator +item.purchase_price, 0);

    const monthNamesCommission = [
        'January', 'February', 'March', 'April', 'May', 'June',
        'July', 'August', 'September', 'October', 'November', 'December'
      ];
      
   const uniqueMonthYearsComm = new Set();
      
      formValues.forEach(item => {
        const monthIndex = item._id.month - 1;
        const monthName = monthNamesCommission[monthIndex];
        const year = item._id.year;
        const monthYear = `${monthName} ${year}`;
        uniqueMonthYearsComm.add(monthYear);
      });
      
      const uniqueMonthYearArrayComm = [...uniqueMonthYearsComm];
      
      const dataCommission = {
        labels: uniqueMonthYearArrayComm,
        datasets: [
          {
            data: formValues.map(item => {
              // Calculate the total amount for each item
              const totalAmount = item.totalBuyerFee + item.totalHighValueFee + item.totalManagementFee + item.totalTransactionFee + item.totalWireFee + item.totalPersonalFee + item.totalSellerFee;
              return totalAmount;
            }),
            label: '',
            backgroundColor: 'rgba(202, 199, 201, 0.8)',
            hoverBackgroundColor: 'blue',
            lineTension: 0,
            fill: true,
            cubicInterpolationMode: 'monotone',
            tension: 0.5,
            borderColor: 'rgba(202, 199, 201, 0.8)',
            pointRadius: 10,
            pointHoverRadius: 15,
          },
        ],
      };
      
    
    
      const optionsDataComm = {
        responsive: true,
        plugins: {
          legend: { display: false },
          datalabels: {
            color: 'gray',
          },
          tooltip: {
            titleColor: 'gray',
            backgroundColor: 'rgba(0, 0, 0, 0.9)',
            bodyColor: 'green',
            displayColors: false,
            yAlign: 'bottom',
          },
        },
        scales: {
          xAxes: {
            grid: {
              lineWidth: 0.5,
              display: false,
              borderColor: 'transparent',
              tickColor: 'transparent',
            },
            ticks: {
              color: 'green',
              font: {
                size: 18,
              },
            },
          },
          yAxis: {
            beginAtZero: true,
            grid: {
              color: 'black',
              lineWidth: 0.5,
              tickColor: 'transparent',
              borderColor: 'transparent',
            },
            ticks: {
              color: 'black',
              font: {
                size: 16,
              },
            },
          },
        },
      };

      const totalCountCommission = formValues.reduce((accumulator, item) => accumulator + item.totalBuyerFee + item.totalHighValueFee + item.totalManagementFee + item.totalTransactionFee + item.totalWireFee + item.totalPersonalFee + item.totalSellerFee, 0);
        console.log(totalCountCommission);

        // const totalCountFor = formValues.reduce((accumulator, item) => accumulator + item.count, 0);
       

  return (
    <div className="bg-secondary float-left w-100 pt-4 mb-4">
      <Loader isActive={loader.isActive} />
      {toaster.isShow && (
        <Toaster
          types={toaster.types}
          isShow={toaster.isShow}
          toasterBody={toaster.toasterBody}
          message={toaster.message}
        />
      )}
      <main className="page-wrapper">
        <div className="">
          <div className="row">
            <div className="col-md-12">
              <div className="bg-light border rounded-3  mb-4 p-3">
                <div className="">
                  <h3 className="text-left mb-0 justify-content-start d-flex align-items-center">
                    Office Transactions Report
                  </h3>
                </div>
              </div>
              <div className="bg-light p-3 border-0 rounded-3 shadow-sm float-start w-100">
                <div className="row">

                <div className="col-md-8">
                    <h4>Count</h4>
                <Bar data={data} plugins={[ChartDataLabels]} options={options} />
                </div>
                    <div className="col-md-4 pull-right">
                    <p><b>Created:</b>{totalCount}</p>
                     {
                        formValuesNew.totalRecordsTransactionCompleted ? 
                        <p><b>Filed:</b> {formValuesNew.totalRecordsTransactionCompleted}</p>
                        :""
                     }
                    </div>
                </div>
              <hr className="mt-4"/>
              <div className="row">
              <div className="col-md-8">
              <h4 className="mt-3">Volume</h4>
              <Line options={optionsData} data={datanew} />
              </div>
              <div className="col-md-4 mt-4">
              <p><b>Total:</b> ${totalCountVolume ? totalCountVolume.toLocaleString('en-US') : "0"}</p>
              </div>
                </div>
           

              <hr className="mt-4"/>
              <div className="row mt-4">
              <div className="col-md-8">
              <h4 className="mt-3">Commissions</h4>
              <Line options={optionsDataComm} data={dataCommission} />
              </div>
              <div className="col-md-4 mt-4">
              <p><b>Total:</b> ${totalCountCommission ? totalCountCommission.toLocaleString('en-US') : "0"}</p>
              </div>
                </div>
            
              </div>
            </div>
          </div>
        </div>
      </main>
    </div>
  );
};

export default TransactionReport;

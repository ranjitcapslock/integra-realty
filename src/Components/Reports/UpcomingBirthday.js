import React, { useState, useEffect } from "react";
import Pic from "../img/pic.png";
import user_service from "../service/user_service";
import ReactPaginate from "react-paginate";
import Loader from "../../Pages/Loader/Loader.js";
import Toaster from "../../Pages/Toaster/Toaster.js";
import { NavLink, useNavigate } from "react-router-dom";
import Birthday from "../img/birthday.jpg";
import jwt from "jwt-decode";

function UpcomingBirthday() {
  const [getContact, setGetContact] = useState([]);
  const [pageCount, setPageCount] = useState(0);
  const [totalRecords, setTotalRecords] = useState("");
  const [loader, setLoader] = useState({ isActive: null });
  const { isActive } = loader;
  const [toaster, setToaster] = useState({
    types: null,
    isShow: null,
    toasterBody: "",
    message: "",
  });
  const { types, isShow, toasterBody, message } = toaster;

  const [query, setQuery] = useState("");
  const [select, setSelect] = useState("");
  const [results, setResults] = useState([]);
  const [currentPage, setCurrentPage] = useState(1);
  const [isLoading, setIsLoading] = useState(false);

  // paginate Function
  const [upcomingbirthday, setUpcomingbirthday] = useState("");
  useEffect(() => {
    const homeside = async () => {
      setLoader({ isActive: true });
      try {
        const response = await user_service.homeside(1);
        setLoader({ isActive: false });
        if (response) {
          setUpcomingbirthday(response.data.data);
          setPageCount(Math.ceil(response.data.totalRecords / 10));
        }
      } catch (error) {
        console.error("Error fetching homeside data:", error);
        setLoader({ isActive: false });
      }
    };

    homeside();
  }, []);

  const handlePageClick = async (data) => {
    const currentPage = data.selected + 1;
    const skip = (currentPage - 1) * 10; // Correcting the skip calculation
    setLoader({ isActive: true });
    try {
      const response = await user_service.homeside(currentPage, "associate");
      setLoader({ isActive: false });
      if (response) {
        setUpcomingbirthday(response.data.data);
        setPageCount(Math.ceil(response.data.totalRecords / 10));
      }
    } catch (error) {
      console.error("Error fetching homeside data:", error);
      setLoader({ isActive: false });
    }
  };

  {
    /* <!-- paginate Function Search Api call Start--> */
  }
  useEffect(() => {
    window.scrollTo(0, 0);
    if (query) {
      SearchGetAll(1);
    }
  }, [query]);

  const SearchGetAll = async () => {
    setIsLoading(true);
    await user_service
      .SearchContactGet(1, query, query, query)
      .then((response) => {
        setIsLoading(false);
        if (response) {
          // setResults(response.data.data);
          setGetContact(response.data.data);
          setPageCount(Math.ceil(response.data.totalRecords / 10));
        }
      });
  };

  const navigate = useNavigate();

  const handleSubmit = (id) => {
    console.log(id);
    navigate(`/contact-profile/${id}`);
  };

  {
    /* <!-- paginate Function Search Api call End--> */
  }

  const handleSearch = (event) => {
    event.preventDefault();
    setCurrentPage(1);
    SearchGetAll(1);
  };

  // Helper function to format date as "Day, Month Day" (e.g., "Friday, September 21")
  function getFormattedDate(dateString) {
    const options = { weekday: "long", month: "long", day: "numeric" };
    const date = new Date(dateString);
    return date.toLocaleDateString("en-US", options);
  }

  const getTimeDifference = (createdDate) => {
    const currentDate = new Date();
    const diffInMilliseconds = currentDate - new Date(createdDate);
    const diffInMinutes = Math.floor(diffInMilliseconds / 60000);
    const hours = Math.floor(diffInMinutes / 60);
    const days = Math.floor(hours / 24);
    const months = Math.floor(days / 30);
    const years = Math.floor(months / 12);

    if (years > 0) {
      return `${years} year${years > 1 ? "s" : ""}  with In Depth Realty`;
    } else if (months > 0) {
      return `${months} month${months > 1 ? "s" : ""}  with In Depth Realty`;
    } else if (days > 0) {
      return `${days} day${days > 1 ? "s" : ""}  with In Depth Realty`;
    } else if (hours > 24) {
      const remainingHours = hours - Math.floor(hours / 24) * 24;
      return `1 day, ${remainingHours} hr${
        remainingHours > 1 ? "s" : ""
      }  with In Depth Realty`;
    } else {
      return `${hours} hr${hours > 1 ? "s" : ""}, ${diffInMinutes % 60} min${
        diffInMinutes % 60 > 1 ? "s" : ""
      }  with In Depth Realty`;
    }
  };

  return (
    <div className="bg-secondary float-left w-100 pt-4 mb-4">
      <Loader isActive={isActive} />
      {isShow && (
        <Toaster
          types={types}
          isShow={isShow}
          toasterBody={toasterBody}
          message={message}
        />
      )}
      <main className="page-wrapper">
        {/* <!-- Page container--> */}
        <div className="content-overlay">
          {/* <!-- Breadcrumb--> */}
          {/* <nav className="mb-3 mb-md-4 pt-md-3" aria-label="Breadcrumb">
                        <ol className="breadcrumb breadcrumb-dark">
                            <li className="breadcrumb-item"><a href="/">Home</a></li>
                            <li className="breadcrumb-item active" aria-current="page">Upcoming Birthdays </li>
                        </ol>
                    </nav> */}

          {/* <!-- Page card like wrapper--> */}
          <div className="">
            <div className="row">
              {/* <!-- List of resumes--> */}
              <div className="col-md-12">
                <h3 className="mb-4 text-white">Upcoming Birthdays</h3>
                {/* <!-- Normal search form group --> */}
                {/* <form onSubmit={handleSearch}>
                                    <p className="mt-5">Quick Search: <a href="">Show All</a></p>
                                    <div className="d-flex justify-content-start align-items-center">
                                        <div className="form-outline">
                                            <input className="form-control" type="search" id="search-input-1" placeholder="Search All"
                                                valvalue={query} onChange={(event) => setQuery(event.target.value)} />
                                        </div>
                                        <span className="ms-4">
                                            <button type="button" className="btn btn-translucent-primary">Find</button>
                                            <a href="#" id="button-item" type="button">Reset</a>
                                        </span>

                                    </div>
                                </form> */}

                {/* <!-- Item--> */}
                {upcomingbirthday && upcomingbirthday.length > 0 ? (
                  upcomingbirthday.map((post) => (
                    <>
                      {post.contact_status === "active" ? 
                        <div
                          className="card bg-secondary card-hover mb-3 mt-0"
                          key={post._id}
                        >
                          <div className="card-body">
                            <div className="float-left w-100">
                              <div className="float-left w-100 d-lg-flex d-md-flex d-inline-block align-items-center justify-content-between">
                                <div className="float-left w-100 d-flex align-items-center justify-content-start">
                                  <img
                                    className="rounded-circle profile_picture"
                                    height="70"
                                    width="70"
                                    src={post.image || Pic}
                                    alt="Profile"
                                  />
                                  <div className="ms-3">
                                    <p className="mb-1">
                                      <strong>{post.firstName}</strong>&nbsp;
                                      <strong>{post.lastName}</strong>
                                    </p>
                                    <p className="mb-1">
                                      Phone: {post?.phone ?? ""}
                                    </p>
                                    <p className="mb-1">
                                      Email: {post?.email ?? ""}
                                    </p>
                                    <p className="mb-1">
                                      Transfer From:{" "}
                                      {post?.additionalActivateFields
                                        ?.admin_note ?? ""}
                                    </p>
                                    <p className="mb-1">
                                      Office Affiliation:{" "}
                                      {post?.additionalActivateFields
                                        ?.office_affiliation ??
                                        post?.additionalActivateFields
                                          ?.office ??
                                        ""}
                                    </p>
                                  </div>
                                </div>
                                <div className="float-left w-100 d-flex align-items-center justify-content-between my-lg-0 my-md-0 my-sm-3 my-3">
                                  <div>
                                    <p className="mb-1">
                                      Birthday:{" "}
                                      {getFormattedDate(post.birthday)}
                                    </p>
                                    <p className="mb-0">
                                      Work Anniversaries:{" "}
                                      {post?.additionalActivateFields
                                        ?.associate_since
                                        ? getTimeDifference(
                                            post.additionalActivateFields
                                              .associate_since
                                          )
                                        : getTimeDifference(new Date())}
                                    </p>
                                  </div>
                                  <span className="">
                                    <NavLink
                                      to={`/contact-profile/${post._id}`}
                                      className=""
                                    >
                                      <i className="fi-info-circle fs-5 mt-n1 me-2"></i>
                                    </NavLink>
                                  </span>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                        :
                       ""
                      }
                    </>
                  ))
                ) : (
                  <div className="float-start w-100 text-center mb-3 mt-5">
                    <img className="" src={Birthday} alt="Birthday" />
                    <br />
                    <p className="float-start w-100 mt-3 text-white">
                      No upcoming birthdays.
                    </p>
                  </div>
                )}

                {pageCount && pageCount > 1 ? (
                  <div className="float-left w-100 d-flex align-items-center justify-content-end mb-0 mt-4">
                    <ReactPaginate
                      className=""
                      previousLabel={"Previous"}
                      nextLabel={"Next"}
                      breakLabel={"..."}
                      pageCount={pageCount}
                      marginPagesDisplayed={1}
                      pageRangeDisplayed={2}
                      onPageChange={handlePageClick}
                      containerClassName={
                        "pagination justify-content-center mb-0"
                      }
                      pageClassName={"page-item"}
                      pageLinkClassName={"page-link"}
                      previousClassName={"page-item"}
                      previousLinkClassName={"page-link"}
                      nextClassName={"page-item"}
                      nextLinkClassName={"page-link"}
                      breakClassName={"page-item"}
                      breakLinkClassName={"page-link"}
                      activeClassName={"active"}
                    />
                  </div>
                ) : (
                  ""
                )}
              </div>
            </div>
          </div>
        </div>
      </main>
    </div>
  );
}
export default UpcomingBirthday;

import React, { useState, useEffect } from "react";
import { NavLink, useNavigate, useParams } from "react-router-dom";
import Loader from "../../Pages/Loader/Loader.js";
import Toaster from "../../Pages/Toaster/Toaster.js";
const Reports = () => {
  const [loader, setLoader] = useState({ isActive: null });
  const { isActive } = loader;
  const [toaster, setToaster] = useState({
    types: null,
    isShow: null,
    toasterBody: "",
    message: "",
  });
  const { types, isShow, toasterBody, message } = toaster;

  return (
    <>
      <div className="bg-secondary float-left w-100 pt-4 mb-4">
        <Loader isActive={isActive} />
        {isShow && (
          <Toaster
            types={types}
            isShow={isShow}
            toasterBody={toasterBody}
            message={message}
          />
        )}
        <main className="page-wrapper getContact_page_wrap">
          <div className="content-overlay mt-0">
            <div className="">
              {/* <h4 className="nav-item mb-md-0 me-md-2 pe-md-1 mb-5" id="getContact"><a className="nav-link active" href="#" aria-current="page">Profile</NavLink></h4> */}
              <div className="row">
                <div className="col-md-4">
                  <div className="">
                    <div
                      className="border bg-light rounded-3 p-3"
                      id="auth-info"
                    >
                      <div className="callout accent">
                        <h5>Personal Reports</h5>
                        <hr />
                        <ul className="list-group mt-3">
                          <li className="list-group-item list-group-item-secondary">
                            <NavLink style={{color:"#3278BD"}} to="/reports/personal/production">
                              Transaction Production History
                            </NavLink>
                            <br />
                            <span className="dd">
                              See your closing and income trends over time, and
                              download a spreadsheet for additional details
                            </span>
                          </li>
                          <li className="list-group-item list-group-item-secondary">
                            <NavLink style={{color:"#3278BD"}} to="/reports/personal/listing-issue">
                              Listing Issues
                            </NavLink>
                            <br />
                            <span className="dd">
                              Review your listings that are missing a matching
                              transaction
                            </span>
                          </li>
                          <li className="list-group-item list-group-item-secondary">
                            <NavLink style={{color:"#3278BD"}} to="/reports/personal/expenses">
                              Expenses
                            </NavLink>
                            <br />
                            <span className="dd">
                              See all your transaction expenses
                            </span>
                          </li>
                        </ul>
                      </div>
                    </div>
                  </div>
                </div>

                <div className="col-md-4">
                  <div className="border bg-light rounded-3 p-3" id="auth-info">
                    <div className="callout accent">
                      <h5>Office Reports</h5>
                      <hr />
                      <ul className="list-group mt-3">
                        <li className="list-group-item list-group-item-secondary">
                          <NavLink style={{color:"#3278BD"}} to="/reports/office/assoc-snapshot/">
                            Associate Snapshot
                          </NavLink>
                          <br />
                          <span>
                            See current active associate counts by office
                          </span>
                        </li>
                        <li className="list-group-item list-group-item-secondary">
                          <NavLink style={{color:"#3278BD"}} to="/reports/office/associates">
                            Associate History
                          </NavLink>
                          <br />
                          <span className="dd">
                            See your associate roster trend over time
                          </span>
                        </li>
                        <li className="list-group-item list-group-item-secondary">
                          <NavLink style={{color:"#3278BD"}} to="/reports/office/assoc-added">
                            Added Associates
                          </NavLink>
                          <br />
                          <span className="dd">
                            See all associates recently added to the system
                          </span>
                        </li>
                        <li className="list-group-item list-group-item-secondary">
                          <NavLink style={{color:"#3278BD"}} to="/reports/office/assoc-removed">
                            Removed Associates
                          </NavLink>
                          <br />
                          <span className="dd">
                            See all associates recently removed from the system
                          </span>
                        </li>
                        <li className="list-group-item list-group-item-secondary">
                          <NavLink style={{color:"#3278BD"}} to="/reports/office/assoc-inactive">
                            Inactive Associates
                          </NavLink>
                          <br />
                          <span className="dd">
                            List associates who have not logged in for a while
                          </span>
                        </li>
                  
                        <li className="list-group-item list-group-item-secondary">
                          <NavLink style={{color:"#3278BD"}} to="/reports/office/assoc-photos">
                           Associate Photos
                          </NavLink>
                          <br />
                          <span className="dd">
                            Review associate photos to verify brand standards
                          </span>
                        </li>
                        <li className="list-group-item list-group-item-secondary">
                          <NavLink style={{color:"#3278BD"}} to="/reports/office/assoc-licenses">
                            Associate Licenses
                          </NavLink>
                          <br />
                          <span className="dd">
                            See all licenses and status
                          </span>
                        </li>
                        <li className="list-group-item list-group-item-secondary">
                          <NavLink style={{color:"#3278BD"}} to="/reports/office/assoc-mls/">
                            Associate MLS IDs
                          </NavLink>
                          <br />
                          <span className="dd">
                            See all Agent IDs assigned or missing
                          </span>
                        </li>
                        <li className="list-group-item list-group-item-secondary">
                          <NavLink style={{color:"#3278BD"}} to="/reports/office/assoc-plans/">
                            Associate Member Plans
                          </NavLink>
                          <br />
                          <span className="dd">
                            See details of who is on each plan
                          </span>
                        </li>
                        <li className="list-group-item list-group-item-secondary">
                          <NavLink style={{color:"#3278BD"}} to="/reports/office/assoc-logins/">
                            Associate Logins
                          </NavLink>
                          <br />
                          <span className="dd">
                            See all recent associate login sessions
                          </span>
                        </li>
                        <li className="list-group-item list-group-item-secondary">
                          <NavLink style={{color:"#3278BD"}} to="/reports/office/assoc-activity/">
                            Associate Activity
                          </NavLink>
                          <br />
                          <span className="dd">
                            See usage metrics for each associate
                          </span>
                        </li>
                        <li className="list-group-item list-group-item-secondary">
                          <NavLink style={{color:"#3278BD"}} to="/reports/office/blocked-email/">
                            Blocked Emails
                          </NavLink>
                          <br />
                          <span className="dd">
                            See all associates with blocked email addresses
                          </span>
                        </li>
                        <li className="list-group-item list-group-item-secondary">
                          <NavLink style={{color:"#3278BD"}} to="/reports/office/recruitment/">
                            Recruitment
                          </NavLink>
                          <br />
                          <span className="dd">
                            See those associates who have recruited other
                            associates, and how many they recruited
                          </span>
                        </li>
                        <li className="list-group-item list-group-item-secondary">
                          <NavLink style={{color:"#3278BD"}} to="/reports/office/sponsorship">
                            Sponsorship
                          </NavLink>
                          <br />
                          <span className="dd">
                            See those associates who have sponsored other
                            associates, and how many they sponsor
                          </span>
                        </li>

                        <li className="list-group-item list-group-item-secondary">
                          <NavLink style={{color:"#3278BD"}} to="/reports/office/security-access/">
                            Office Security Access
                          </NavLink>
                          <br />
                          <span className="dd">
                            Assign or remove security access cards per associate
                          </span>
                        </li>

                        <li className="list-group-item list-group-item-secondary">
                          <NavLink style={{color:"#3278BD"}} to="/reports/office/inapprovalassociates">
                            Onboarding Applications
                          </NavLink>
                          <br />
                          <span className="dd">
                            Onboarding Applications for New Associates
                          </span>
                        </li>

                        <li className="list-group-item list-group-item-secondary">
                          <NavLink style={{color:"#3278BD"}} to="/reports/office/onboarding-info">
                            Onboarding Info
                          </NavLink>
                          <br />
                          <span className="dd">
                            Review associate Onboarding Info
                          </span>
                        </li>

                        <li className="list-group-item list-group-item-secondary">
                          <NavLink style={{color:"#3278BD"}} to="/reports/office/agent-testimonials">
                            {" "}
                            Agent Testimonials
                          </NavLink>
                          <br />
                          <span className="dd">
                            {" "}
                            Agent testimonials office report
                          </span>
                        </li>
                      </ul>
                    </div>
                  </div>
                </div>

                <div className="col-md-4">
                  <div className="border bg-light rounded-3 p-3" id="auth-info">
                    <div className="callout accent">
                      <h5>Office Transaction Reports</h5>
                      <hr />
                      <ul className="list-group mt-3">
                        <li className="list-group-item list-group-item-secondary">
                          <NavLink style={{color:"#3278BD"}} to="/reports/office/production/">
                            Office Production History
                          </NavLink>
                          <br />
                          <span className="dd">
                            See office closing and income trends over time, and
                            download a spreadsheet for additional details
                          </span>
                        </li>
                        <li className="list-group-item list-group-item-secondary">
                          <NavLink style={{color:"#3278BD"}} to="/report/close-calendar">
                            Closing Calendar
                          </NavLink>
                          <br />
                          <span className="dd">
                            See all future or completed transactions in a
                            standard calendar view
                          </span>
                        </li>
                        <li className="list-group-item list-group-item-secondary">
                          <NavLink style={{color:"#3278BD"}} to="/reports/office/buyer-broker-aggrement/">
                          Buyer-Broker Agreements
                          </NavLink>
                          <br />
                          <span className="dd">
                          Uploded Buyer-Broker Agreements Document
                          </span>
                        </li>

                        <li className="list-group-item list-group-item-secondary">
                          <NavLink style={{color:"#3278BD"}} to="/reports/transact/documents/">
                            Document Review
                          </NavLink>
                          <br />
                          <span className="dd">
                            Review documents as they are added
                          </span>
                        </li>

                        <li className="list-group-item list-group-item-secondary">
                          <NavLink style={{color:"#3278BD"}} to="/reports/transact/funding">
                            Funding and Disbursement
                          </NavLink>
                          <br />
                          <span className="dd">
                            Review incomplete funding and unsent disbursements
                          </span>
                        </li>
                        <li className="list-group-item list-group-item-secondary">
                          <NavLink style={{color:"#3278BD"}} to="/reports/transact/notes">
                            Transaction Notes
                          </NavLink>
                          <br />
                          <span className="dd">
                            See the notes added to transactions by agents or
                            staff
                          </span>
                        </li>
                        <li className="list-group-item list-group-item-secondary">
                          <NavLink style={{color:"#3278BD"}} to="/reports/transact/vendors/">
                            Vendors
                          </NavLink>
                          <br />
                          <span className="dd">
                            See the list of vendors and co-op agents and the
                            transactions where they were used
                          </span>
                        </li>
                        <li className="list-group-item list-group-item-secondary">
                          <NavLink style={{color:"#3278BD"}} to="/reports/transact/deposits">
                            Trust Account Ledger
                          </NavLink>
                          <br />
                          <span className="dd">
                            See your offices' earnest money deposit history for
                            monthly and total funds held in trust
                          </span>
                        </li>
                        <li className="list-group-item list-group-item-secondary">
                          <NavLink style={{color:"#3278BD"}} to="/reports/transact/listing-issue/">
                            Listing Issues
                          </NavLink>
                          <br />
                          <span className="dd">
                            Review IDX listings that are missing a matching
                            transaction
                          </span>
                        </li>
                        <li className="list-group-item list-group-item-secondary">
                          <NavLink style={{color:"#3278BD"}} to="/reports/transact/expired-listings/">
                            Expired Listings
                          </NavLink>
                          <br />
                          <span className="dd">
                            See listing transactions with expired and expiring
                            listing agreements
                          </span>
                        </li>
                        <li className="list-group-item list-group-item-secondary">
                          <NavLink style={{color:"#3278BD"}} to="/reports/transact/first-transaction">
                            First Transaction
                          </NavLink>
                          <br />
                          <span className="dd">
                            Highlight each agent's first transaction
                          </span>
                        </li>
                      </ul>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </main>
      </div>
    </>
  );
};

export default Reports;

import React, { useState, useEffect } from "react";
// import avtar from "./Components/img/avtar.jpg";
import _ from "lodash";
import user_service from "../service/user_service";
import jwt from "jwt-decode";
import Loader from "../../Pages/Loader/Loader.js";
import Toaster from "../../Pages/Toaster/Toaster.js";
import { NavLink, useNavigate, useParams } from "react-router-dom";
import axios from "axios";

function AddQuestion() {
  const initialValues = {
    question: "",
    answer: "",
    attach_type: "",
    link_title: "",
    link_url: "",
    file_url: "",
  };
  const [formValues, setFormValues] = useState(initialValues);
  const [formErrors, setFormErrors] = useState({});
  const [isSubmitClick, setISSubmitClick] = useState(false);
  const [loader, setLoader] = useState({ isActive: null });
  const { isActive } = loader;
  const [toaster, setToaster] = useState({
    types: null,
    isShow: null,
    toasterBody: "",
    message: "",
  });
  const { types, isShow, toasterBody, message } = toaster;
  const [file, setFile] = useState(null);
  const [data, setData] = useState("");
  const [fileExtension, setFileExtension] = useState("");
  const [acceptedFileTypes, setAcceptedFileTypes] = useState([]);
  const [showSubmitButton, setShowSubmitButton] = useState(false);
  const navigate = useNavigate();
  const [single, setSingle] = useState("");
  const [formData, setFormData] = useState("");

  const params = useParams();
  console.log(params.id);

  // onChange Function start
  const handleChange = (e) => {
    const { name, value } = e.target;
    setFormValues({ ...formValues, [name]: value });
  };
  // onChange Function end

  {
    /* <!-- Form Validation Start--> */
  }
    useEffect(() => { 
      if (formValues && isSubmitClick) {
        validate();
      }
    }, [formValues]);

    const validate = () => {
      const values = formValues;
      const errors = {};
      if (!values.question) {
        errors.question = "Question is required";
      }
      if (!values.answer) {
        errors.answer = "Answer is required";
      }
      setFormErrors(errors);
      return errors;
    };
  {
    /* <!-- Form Validation End--> */
  }

  const handleFileUpload = async (e) => {
    const selectedFile = e.target.files[0];

    if (selectedFile) {
      setFile(selectedFile);
      setFileExtension(selectedFile.name.split(".").pop());
      setShowSubmitButton(true);

      const formData = new FormData();
      formData.append("file", selectedFile);
      console.log(selectedFile);
      const config = {
        headers: {
          Accept: "application/json",
          "Content-Type": "multipart/form-data",
          Authorization: `Bearer ${localStorage.getItem("auth")}`,
        },
      };
      try {
        setLoader({ isActive: true });
        const uploadResponse = await axios.post(
          "https://api.brokeragentbase.com/upload",
          formData,
          config
        );
        const uploadedFileData = uploadResponse.data;
        setLoader({ isActive: false });
        setData(uploadedFileData);
      } catch (error) {
        console.error("Error occurred during file upload:", error);
      }
    }
  };

  {
    /* <!--Api call Form onSubmit Start--> */
  }

  const handleSubmit = async (e) => {
    if(params.id){
      e.preventDefault();
      const userData = {
        agentId: jwt(localStorage.getItem("auth")).id,
        question: formValues.question,
        answer: formValues.answer,
        attach_type: single,
        link_title: formValues.link_title,
        link_url: formValues.link_url,
        file_url: data,
       // pin:"yes"
      };
      console.log(userData);
      try{
          setLoader({ isActive: true });
          await user_service.questionsUpdate(params.id,userData).then((response) => {
            if (response) {
              console.log(response.data);
              setLoader({ isActive: false });
              setToaster({ type: "Update Question", isShow: true, toasterBody: response.data.message, message: "Question Update Successfully", });
              setTimeout(() => {
                  setToaster((prevToaster) => ({ ...prevToaster, isShow: false }))
                  navigate(`/question`);
                }, 1000);
            } else {
              setLoader({ isActive: false });
              setToaster({
                types: "error",
                isShow: true,
                toasterBody: response.data.message,
                message: "Error",
              });
            }
          });
      }
      catch (error) {
          setLoader({ isActive: false });
          setToaster({
            types: "error",
            isShow: true,
            toasterBody: error.response ? error.response.data.message : error.message,
            message: "Error",
          });
          setTimeout(() => {
            setToaster((prevToaster) => ({ ...prevToaster, isShow: false }));
          }, 2000);
        }
      }
  else{
    e.preventDefault();
    setISSubmitClick(true);
    let checkValue = validate();
    if (_.isEmpty(checkValue)) {
    const userData = {
      agentId: jwt(localStorage.getItem("auth")).id,
      question: formValues.question,
      answer: formValues.answer,
      attach_type: single,
      link_title: formValues.link_title,
      link_url: formValues.link_url,
      file_url: data,
    };
    console.log(userData);
    try{
        setLoader({ isActive: true });
        await user_service.questionsPost(userData).then((response) => {
          if (response) {
            console.log(response.data);
            setLoader({ isActive: false });
            setToaster({ type: "Add Question", isShow: true, toasterBody: response.data.message, message: "Add Question Successfully", });
            setTimeout(() => {
                setToaster((prevToaster) => ({ ...prevToaster, isShow: false }))
                navigate(`/question`);
            }, 1000);
          } else {
            setLoader({ isActive: false });
            setToaster({
              types: "error",
              isShow: true,
              toasterBody: response.data.message,
              message: "Error",
            });
          }
        });
    }
    catch (error) {
        setLoader({ isActive: false });
        setToaster({
          types: "error",
          isShow: true,
          toasterBody: error.response ? error.response.data.message : error.message,
          message: "Error",
        });
        setTimeout(() => {
          setToaster((prevToaster) => ({ ...prevToaster, isShow: false }));
        }, 2000);
      }
}
  }
    
  };

 

  const QuestionGetId = async () => {
    if(params.id){
      await user_service.questionsGetId(params.id).then((response) => {
        if (response) {
          setFormData(response.data);
        }
      });
    }
  };

  useEffect(() => { window.scrollTo(0, 0);
    QuestionGetId();
  }, []);


  const CheckList = (e) => {
    setSingle(e.target.name);
    setSingle(e.target.value);
  };

  return (
    <>
    <div className="bg-secondary float-left w-100 pt-4 mb-4">
      <Loader isActive={isActive} />
      {isShow && (
        <Toaster
          types={types}
          isShow={isShow}
          toasterBody={toasterBody}
          message={message}
        />
      )}
      <main className="page-wrapper">
        <div className="">
          <div className="">
            <div className="">
              <div className="d-flex align-items-center justify-content-start mb-4">
                  <h3 className="text-white mb-0">
                    Add Question
                  </h3>
                 
              </div>   
                <div className="row">
                  <div className="col-md-8">
                  <div className="bg-light border rounded-3 p-3">

                  <div className="col-sm-12 mb-3">
                    <label className="form-label">*Question</label>
                    <input
                      className="form-control"
                      id="inline-form-input"
                      type="text"
                      name="question"
                      value={formValues.question? formValues.question : formData.question}
                      onChange={handleChange}
                      style={{
                        border: formErrors?.question ? "1px solid red" : "",
                      }}
                    />
                    {formErrors.question && (
                      <div className="invalid-tooltip">
                        {formErrors.question}
                      </div>
                    )}
                  </div>

                  <div className="col-sm-12 mb-3">
                    <label className="form-label">Answer</label>
                    <input
                      className="form-control"
                      id="inline-form-input"
                      type="text"
                      name="answer"
                      value={formValues.answer? formValues.answer : formData.answer}

                      onChange={handleChange}
                      style={{
                        border: formErrors?.answer ? "1px solid red" : "",
                      }}
                    />
                    {formErrors.answer && (
                      <div className="invalid-tooltip">{formErrors.answer}</div>
                    )}
                  </div>

                  <div className="col-sm-12 mb-3">
                    <label className="form-label">
                      * Is this a link or file?
                    </label>
                    <div className="form-check d-flex" onClick={(e) => CheckList(e)}>
                      <div className="">
                        <input
                          className="form-check-input"
                          id="form-radio-four"
                          type="radio"
                          name="check"
                          value="link"
                        />
                        <label className="form-check-label" id="radio-level1">
                          Link
                        </label>
                      </div>
                      <div className="ms-5">
                        <input
                          className="form-check-input"
                          id="form-radio-4"
                          type="radio"
                          name="check"
                          value="file"
                        />
                        <label className="form-check-label ms-2" id="radio-level1">
                          File
                        </label>
                      </div>
                    </div>
                  </div>
                 
                 { 
                     single === "link" ?
                     <>
                  <div className="col-sm-12 mb-3">
                    <label className="form-label">
                      What do you want to name your link?
                    </label>
                    <input
                      className="form-control"
                      id="inline-form-input"
                      type="text"
                      name="link_title"
                      value={formValues.link_title? formValues.link_title : formData.link_title}
                      onChange={handleChange}
                      style={{
                        border: formErrors?.link_title ? "1px solid red" : "",
                      }}
                    />
                    {formErrors.link_title && (
                      <div className="invalid-tooltip">
                        {formErrors.link_title}
                      </div>
                    )}
                  </div>


                  <div className="col-sm-12 mb-3">
                    <label className="form-label">*Link Url</label>
                    <input
                      className="form-control"
                      id="inline-form-input"
                      type="text"
                      name="link_url"
                      value={formValues.link_url? formValues.link_url : formData.link_url}
                      onChange={handleChange}
                    />
                 
                  </div>
                     </>
                     :""
                 }
                  
                  {
                     single === "file" ?
                     <>
                  <div className="col-sm-12 mb-3">
                    <label className="form-label">
                  *File
                    </label>
                    <input
                      className="file-uploader file-uploader-grid"
                      id="inline-form-input"
                      type="file"
                      name="image"
                      onChange={handleFileUpload}
                    />
                  </div>

                  <div className="col-sm-12 mb-3">
                    <label className="form-label">
                      What do you want to name your file?
                    </label>
                    <input
                      className="form-control"
                      id="inline-form-input"
                      type="text"
                      name="link_title"
                      value={formValues.link_title? formValues.link_title : formData.link_title}
                      onChange={handleChange}
                    />
                  </div>
                     </>


                  :""
                  }
          
                </div>
                <div className="pull-right mt-3">
                <NavLink type="button" className="btn btn-secondary pull-right ms-3" to="/question">Cancel</NavLink>
                  <button
                    className="btn btn-primary"
                    type="button"
                    onClick={handleSubmit}>
                    Submit
                  </button>
                </div>
              </div>
             
                </div>
                <div className="col-md-4">
               </div>
              </div>
            </div>
          </div>
      </main>
      
    </div>
    
    </>
  );
}
export default AddQuestion;

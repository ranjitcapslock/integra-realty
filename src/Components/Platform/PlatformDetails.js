import React, { useState, useEffect } from "react";
// import avtar from "./Components/img/avtar.jpg";
import _ from "lodash";
import user_service from "../service/user_service";
import jwt from "jwt-decode";
import Loader from "../../Pages/Loader/Loader.js";
import Toaster from "../../Pages/Toaster/Toaster.js";
import { useNavigate } from "react-router-dom";
import axios from "axios";

function PlatformDetails() {
  const initialValues = {
    email: "",
    addresline1: "",
    addresline2: "",
    addresline3: "",
    phone: "",
    facebook: "",
    twitter: "",
    whatsapp: "",
    telegram: "",
    instagram: "",
    platform_logo: "",
  };
  const [formValues, setFormValues] = useState(initialValues);
  const [formData, setFormData] = useState("");

  const [formErrors, setFormErrors] = useState({});
  const [isSubmitClick, setISSubmitClick] = useState(false);
  const [loader, setLoader] = useState({ isActive: null });
  const { isActive } = loader;
  const [toaster, setToaster] = useState({
    types: null,
    isShow: null,
    toasterBody: "",
    message: "",
  });
  const { types, isShow, toasterBody, message } = toaster;

  const [file, setFile] = useState(null);
  const [data, setData] = useState("");
  const [fileExtension, setFileExtension] = useState("");
  const [acceptedFileTypes, setAcceptedFileTypes] = useState([]);
  const [showSubmitButton, setShowSubmitButton] = useState(false);
  const navigate = useNavigate();


  const PlatformdetailsGet = async () => {
    await user_service.platformdetailsGet().then((response) => {
      if (response) {
        setFormData(response.data.data[0]);
      }
    });
};

useEffect(() => { window.scrollTo(0, 0);
  PlatformdetailsGet();
}, []);

  // onChange Function start
  const handleChange = (e) => {
    const { name, value } = e.target;
    setFormValues({ ...formValues, [name]: value });
  };
  // onChange Function end

  {
    /* <!-- Form Validation Start--> */
  }
  useEffect(() => { 
    if (formValues && isSubmitClick) {
      validate();
    }
  }, [formValues]);

  const validate = () => {
    const values = formValues;
    const errors = {};

    if (!values.email) {
      errors.email = "Email is required";
    } else if (
      !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i.test(values.email)
    ) {
      errors.email = "Invalid email address";
    }

    // if (!values.email) {
    //   errors.email = "Email is required";
    // }
    
    if (!values.addresline1) {
      errors.addresline1 = "Addresline1 is required";
    }
    if (!values.addresline2) {
      errors.addresline2 = "Addresline2 is required";
    }
    setFormErrors(errors);
    return errors;
  };
  {
    /* <!-- Form Validation End--> */
  }

  const handleFileUpload = async (e) => {
    const selectedFile = e.target.files[0];

    if (selectedFile) {
      setFile(selectedFile);
      setFileExtension(selectedFile.name.split(".").pop());
      setShowSubmitButton(true);

      const formData = new FormData();
      formData.append("file", selectedFile);
      console.log(selectedFile);
      const config = {
        headers: {
          Accept: "application/json",
          "Content-Type": "multipart/form-data",
          Authorization: `Bearer ${localStorage.getItem("auth")}`,
        },
      };
      try {
        const uploadResponse = await axios.post(
          "https://api.brokeragentbase.com/upload",
          formData,
          config
        );
        const uploadedFileData = uploadResponse.data;
        setData(uploadedFileData);
      } catch (error) {
        console.error("Error occurred during file upload:", error);
      }
    }
  };

  {
    /* <!--Api call Form onSubmit Start--> */
  }
 

  const handleSubmit = async (e) => {
   if(formData?._id){
    e.preventDefault();
      const userData = {
        agentId: jwt(localStorage.getItem("auth")).id,
        email: formValues.email,
        addresline1: formValues.addresline1,
        addresline2: formValues.addresline2,
        addresline3: formValues.addresline3,
        phone: formValues.phone,
        facebook: formValues.facebook,
        twitter: formValues.twitter,
        whatsapp: formValues.whatsapp,
        telegram: formValues.telegram,
        instagram: formValues.instagram,
        platform_logo: data,
      };
      setLoader({ isActive: true });
      await user_service.platformdetailsUpdate(formData?._id, userData).then((response) => {
        if (response) {
          setFormValues(response.data);
          setLoader({ isActive: false });
          setToaster({ type: "Update PlatformDetails", isShow: true, toasterBody: response.data.message, message: "Update PlatformDetails Successfully", });
          setTimeout(() => {
              setToaster((prevToaster) => ({ ...prevToaster, isShow: false }))
          }, 1000);
          PlatformdetailsGet()
        } else {
          setLoader({ isActive: false });
          setToaster({
            types: "error",
            isShow: true,
            toasterBody: response.data.message,
            message: "Error",
          });
        }
      });
    
  }
    else{
      e.preventDefault();
      setISSubmitClick(true);
      let checkValue = validate();
      if (_.isEmpty(checkValue)) {
        const userData = {
          agentId: jwt(localStorage.getItem("auth")).id,
          email: formValues.email,
          addresline1: formValues.addresline1,
          addresline2: formValues.addresline2,
          addresline3: formValues.addresline3,
          phone: formValues.phone,
          facebook: formValues.facebook,
          twitter: formValues.twitter,
          whatsapp: formValues.whatsapp,
          telegram: formValues.telegram,
          instagram: formValues.instagram,
          platform_logo: data,
        };
        try{

          setLoader({ isActive: true });
  
          await user_service.platformdetailsPost(userData).then((response) => {
            if (response) {
              setFormValues(response.data);
              setLoader({ isActive: false });
              setToaster({ type: "Add PlatformDetails", isShow: true, toasterBody: response.data.message, message: "Add PlatformDetails Successfully", });
              setTimeout(() => {
                  setToaster((prevToaster) => ({ ...prevToaster, isShow: false }))
              }, 1000);
              PlatformdetailsGet()
            } else {
              setLoader({ isActive: false });
              setToaster({
                types: "error",
                isShow: true,
                toasterBody: response.data.message,
                message: "Error",
              });
            }
          });
        }
        catch (error) {
          setLoader({ isActive: false });
          setToaster({
            types: "error",
            isShow: true,
            toasterBody: error.response ? error.response.data.message : error.message,
            message: "Error",
          });
          setTimeout(() => {
            setToaster((prevToaster) => ({ ...prevToaster, isShow: false }));
          }, 2000);
        }
      }
    }
   
  };


 

  return (
    <div className="bg-secondary float-left w-100 pt-4 mb-4">
      <Loader isActive={isActive} />
      {isShow && (
        <Toaster
          types={types}
          isShow={isShow}
          toasterBody={toasterBody}
          message={message}
        />
      )}
      <main className="page-wrapper">
        <div className="container">
          <div className="">
            <div className="mb-4">
              <h3 className="text-white mb-0">
                Platform Details
              </h3>
            </div>
            <div className="bg-light rounded-3 p-3 border mb-3">
            <div className="row">
                  <div className="col-sm-6 mb-3">
                    <label className="form-label">Email</label>
                    <input
                      className="form-control"
                      id="inline-form-input"
                      type="text"
                      name="email"
                      value={formValues?.email? formValues?.email :formData?.email }
                      onChange={handleChange}
                      style={{
                        border: formErrors?.email ? "1px solid red" : "",
                      }}
                    />
                    {formErrors.email && (
                      <div className="invalid-tooltip">{formErrors.email}</div>
                    )}
                  </div>
                  <div className="col-sm-6 mb-3">
                    <label className="form-label">Addresline1</label>
                    <input
                      className="form-control"
                      id="inline-form-input"
                      type="text"
                      name="addresline1"
                      value={formValues?.addresline1?formValues?.addresline1 : formData?.addresline1 }
                      onChange={handleChange}
                      style={{
                        border: formErrors?.addresline1 ? "1px solid red" : "",
                      }}
                    />
                    {formErrors.addresline1 && (
                      <div className="invalid-tooltip">
                        {formErrors.addresline1}
                      </div>
                    )}
                  </div>
                  <div className="col-sm-6 mb-3">
                    <label className="form-label">Addresline2</label>
                    <input
                      className="form-control"
                      id="inline-form-input"
                      type="text"
                      name="addresline2"
                      value={formValues?.addresline2 ?formValues?.addresline2 : formData?.addresline2 }

                      onChange={handleChange}
                      style={{
                        border: formErrors?.addresline2 ? "1px solid red" : "",
                      }}
                    />
                    {formErrors.addresline2 && (
                      <div className="invalid-tooltip">
                        {formErrors.addresline2}
                      </div>
                    )}
                  </div>
                  <div className="col-sm-6 mb-3">
                    <label className="form-label">Addresline3</label>
                    <input
                      className="form-control"
                      id="inline-form-input"
                      type="text"
                      name="addresline3"
                      value={formValues?.addresline3 ?formValues?.addresline3 : formData?.addresline3 }

                      onChange={handleChange}
                    />
                  </div>
                  <div className="col-sm-6 mb-3">
                    <label className="form-label">Phone</label>
                    <input
                      className="form-control"
                      id="inline-form-input"
                      type="text"
                      name="phone"
                      value={formValues?.phone ?formValues?.phone : formData?.phone }
                      onChange={handleChange}
                    />
                  </div>
                  <div className="col-sm-6 mb-3">
                    <label className="form-label">Facebook</label>
                    <input
                      className="form-control"
                      id="inline-form-input"
                      type="text"
                      name="facebook"
                      value={formValues?.facebook ?formValues?.facebook : formData?.facebook }
                      onChange={handleChange}
                    />
                  </div>
                  <div className="col-sm-6 mb-3">
                    <label className="form-label">Twitter</label>
                    <input
                      className="form-control"
                      id="inline-form-input"
                      type="text"
                      name="twitter"
                      value={formValues?.twitter ?formValues?.twitter : formData?.twitter }
                      onChange={handleChange}
                    />
                  </div>
                  <div className="col-sm-6 mb-3">
                    <label className="form-label">Whatsapp</label>
                    <input
                      className="form-control"
                      id="inline-form-input"
                      type="text"
                      name="whatsapp"
                      value={formValues?.whatsapp ?formValues?.whatsapp : formData?.whatsapp }
                      onChange={handleChange}
                    />
                  </div>
                  <div className="col-sm-6 mb-3">
                    <label className="form-label">Telegram</label>
                    <input
                      className="form-control"
                      id="inline-form-input"
                      type="text"
                      name="telegram"
                      value={formValues?.telegram ?formValues?.telegram : formData?.telegram }
                      onChange={handleChange}
                    />
                  </div>
                  <div className="col-sm-6 mb-3">
                    <label className="form-label">Instagram</label>
                    <input
                      className="form-control"
                      id="inline-form-input"
                      type="text"
                      name="instagram"
                      value={formValues?.instagram ?formValues?.instagram : formData?.instagram }
                      onChange={handleChange}
                    />
                  </div>
                  {
                      formData.platform_logo ?
                      <>
                      <img className="platform_Logo" src={formData.platform_logo}/>
                      <div className="col-sm-6 mb-3">
                      <label className="form-label">
                      Platform Logo Update:
                     </label>
                    <input
                      className="file-uploader file-uploader-grid"
                      id="inline-form-input"
                      type="file"
                      name="image"
                      onChange={handleFileUpload}
                    />
                  
                  </div>
                      </>

                      :
                  <div className="col-sm-6 mb-3">
                    <label className="form-label">
                    Platform Logo Upload:
                    </label>

                    <input
                      className="file-uploader file-uploader-grid"
                      id="inline-form-input"
                      type="file"
                      name="image"
                      onChange={handleFileUpload}
                    />
                  
                  </div>
}
                </div>
               </div>
                <div className="pull-right mt-3">
                  <button
                    className="btn btn-primary btn-lg"
                    type="button"
                    onClick={handleSubmit}>
                    Submit
                  </button>
              </div>
            </div>
          </div>
      </main>
    </div>
  );
}
export default PlatformDetails;

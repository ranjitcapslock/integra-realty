import React, { useState, useEffect } from "react";
// import avtar from "./Components/img/avtar.jpg";
import _ from "lodash";
import user_service from "../service/user_service";
import jwt from "jwt-decode";
import Loader from "../../Pages/Loader/Loader.js";
import Toaster from "../../Pages/Toaster/Toaster.js";
import { NavLink, useNavigate } from "react-router-dom";
import axios from "axios";
import { asyncScheduler } from "rxjs";

function Question() {
  const [loader, setLoader] = useState({ isActive: null });
  const { isActive } = loader;
  const [toaster, setToaster] = useState({
    types: null,
    isShow: null,
    toasterBody: "",
    message: "",
  });
  const { types, isShow, toasterBody, message } = toaster;

  const [formData, setFormData] = useState([]);

  const QuestionGet = async () => {
    await user_service.questionsGet().then((response) => {
      if (response) {
        console.log(response);
        setFormData(response.data);
      }
    });
  };

  useEffect(() => {
    window.scrollTo(0, 0);
    QuestionGet();
  }, []);

  const [isSubmitClick, setISSubmitClick] = useState(false);
  const pinquetion = async (e, id, pintype) => {
    e.preventDefault();
    const userData = {
      pin: pintype,
    };
    try {
      setLoader({ isActive: true });
      await user_service.questionsUpdate(id, userData).then((response) => {
        if (response) {
          var msg = "Question pinned to dashboard.";
          if (pintype == "yes") {
            msg = "Question pinned to dashboard.";
          } else {
            msg = "Question Unpinned from dashboard.";
          }

          setLoader({ isActive: false });
          setToaster({
            type: msg,
            isShow: true,
            toasterBody: response.data.message,
            message: msg,
          });
          setTimeout(() => {
            setToaster((prevToaster) => ({ ...prevToaster, isShow: false }));
            QuestionGet();
          }, 1000);
        } else {
          setLoader({ isActive: false });
          setToaster({
            types: "error",
            isShow: true,
            toasterBody: response.data.message,
            message: "Error",
          });
        }
      });
    } catch (error) {
      setLoader({ isActive: false });
      setToaster({
        types: "error",
        isShow: true,
        toasterBody: error.response
          ? error.response.data.message
          : error.message,
        message: "Error",
      });
      setTimeout(() => {
        setToaster((prevToaster) => ({ ...prevToaster, isShow: false }));
      }, 2000);
    }
  };

  const deletequetion = async (e, id) => {
    e.preventDefault();
    setLoader({ isActive: true });
    await user_service.questionDelete(id).then((response) => {
      if (response) {
        setLoader({ isActive: false });
        setToaster({
          types: "Delete",
          isShow: true,
          toasterBody: response.data.message,
          // message: "",
        });
        setTimeout(() => {
          QuestionGet();
        }, 1000);
      }
    });
  };

  return (
    <>
      <div className="bg-secondary float-left w-100 pt-4 mb-4">
        <Loader isActive={isActive} />
        {isShow && (
          <Toaster
            types={types}
            isShow={isShow}
            toasterBody={toasterBody}
            message={message}
          />
        )}
        <main className="page-wrapper">
          <div className="">
            <div className="">
              <div className="d-flex align-items-center justify-content-start mb-4">
                <h3 className="text-white mb-0 pull-left">Question Listing</h3>
              </div>
              <div className="row">
                <div className="col-md-8">
                  <div className="bg-light border rounded-3 p-4">
                    {formData.data && formData.data.length > 0
                      ? formData.data.map((items) => (
                          <div className="question_listing d-flex align-items-center justify-content-start">
                            <div
                              className="accordion mt-3"
                              id={`accordionExample-${items._id}`}
                              key={items._id}
                            >
                              <div className="accordion-item">
                                <h2
                                  className="accordion-header"
                                  id={`heading-${items._id}`}
                                >
                                  <button
                                    className="accordion-button"
                                    type="button"
                                    data-bs-toggle="collapse"
                                    data-bs-target={`#collapse-${items._id}`}
                                    aria-expanded="true"
                                    aria-controls={`collapse-${items._id}`}
                                  >
                                    {items.question}
                                  </button>
                                </h2>
                                <div
                                  className="accordion-collapse collapse"
                                  aria-labelledby={`heading-${items._id}`}
                                  data-bs-parent={`#accordionExample-${items._id}`}
                                  id={`collapse-${items._id}`}
                                >
                                  <div className="accordion-body">
                                    {items.answer}
                                  </div>
                                  <hr />
                                  {items.link_url.length > 0 ? (
                                    <div className="accordion-body mt-3">
                                      <a
                                        href={items.link_url}
                                        target="_blank"
                                        rel="noopener noreferrer"
                                      >
                                        {items.link_title}
                                      </a>
                                    </div>
                                  ) : (
                                    ""
                                  )}
                                  {items.file_url.length > 0 ? (
                                    <div className="accordion-body mt-3">
                                      <a
                                        href={items.file_url}
                                        target="_blank"
                                        rel="noopener noreferrer"
                                      >
                                        {items.link_title}
                                      </a>
                                    </div>
                                  ) : (
                                    ""
                                  )}
                                </div>
                              </div>
                            </div>
                            {localStorage.getItem("auth") &&
                            jwt(localStorage.getItem("auth")).contactType ==
                              "admin" ? (
                              <>
                                <div className="pull-right mt-2">
                                  <NavLink to={`/add-question/${items._id}`}>
                                    <i className="fi-edit ms-2"></i>
                                  </NavLink>
                                  <NavLink
                                    onClick={(e) =>
                                      pinquetion(
                                        e,
                                        items._id,
                                        items.pin == "yes" ? "no" : "yes"
                                      )
                                    }
                                    to={`#`}
                                  >
                                    <i
                                      className="fa fa-thumb-tack ms-2"
                                      aria-hidden="true"
                                    ></i>
                                  </NavLink>
                                  <NavLink
                                    onClick={(e) => deletequetion(e, items._id)}
                                    to={`#`}
                                  >
                                    <i
                                      className="fa fa-trash ms-2"
                                      aria-hidden="true"
                                    ></i>
                                  </NavLink>
                                </div>
                              </>
                            ) : (
                              ""
                            )}
                          </div>
                        ))
                      : "Apologies, but nothing was found; please check back later."}
                  </div>
                  {localStorage.getItem("auth") &&
                  jwt(localStorage.getItem("auth")).contactType == "admin" ? (
                    <div className="mt-4 pull-right">
                      <NavLink
                        type="button"
                        className="btn btn-secondary pull-right"
                        to="/add-question"
                      >
                        Add Question
                      </NavLink>
                    </div>
                  ) : (
                    ""
                  )}
                </div>
                <div className="col-md-4"></div>
              </div>
            </div>
          </div>
        </main>
      </div>
    </>
  );
}
export default Question;

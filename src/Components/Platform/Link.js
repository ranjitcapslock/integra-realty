import React, { useState, useEffect } from "react";
// import avtar from "./Components/img/avtar.jpg";
import _ from "lodash";
import user_service from "../service/user_service";
import jwt from "jwt-decode";
import Loader from "../../Pages/Loader/Loader.js";
import Toaster from "../../Pages/Toaster/Toaster.js";
import { NavLink, useNavigate } from "react-router-dom";
import axios from "axios";
import { asyncScheduler } from "rxjs";

function Links() {
  const [loader, setLoader] = useState({ isActive: null });
  const { isActive } = loader;
  const [toaster, setToaster] = useState({
    types: null,
    isShow: null,
    toasterBody: "",
    message: "",
  });
  const { types, isShow, toasterBody, message } = toaster;

  const [formData, setFormData] = useState([]);

  const LinkGet = async () => {
    const userid = jwt(localStorage.getItem("auth")).id;
    var pin = `?agentId=${userid}`;
    await user_service.linksGet(pin).then((response) => {
      if (response) {
        // console.log(response);
        setFormData(response.data);
      }
    });
  };

  useEffect(() => { window.scrollTo(0, 0);
    LinkGet();
  }, []);


  const [isSubmitClick, setISSubmitClick] = useState(false);
  const pinlink = async (e, id, pintype) =>{
      e.preventDefault();
      const userData = {
       pin: pintype
      };
      try{
          setLoader({ isActive: true });
          await user_service.linksUpdate(id,userData).then((response) => {
            if (response) {
              var msg = "Link pinned to dashboard.";
              if(pintype == "yes"){
                  msg = "Link pinned to dashboard."
              }else{
                  msg = "Link Unpinned from dashboard."
              }

              setLoader({ isActive: false });
              setToaster({ type: msg, isShow: true, toasterBody: response.data.message, message: msg, });
              setTimeout(() => {
                  setToaster((prevToaster) => ({ ...prevToaster, isShow: false }))
                  LinkGet();
                }, 1000);
            } else {
              setLoader({ isActive: false });
              setToaster({
                types: "error",
                isShow: true,
                toasterBody: response.data.message,
                message: "Error",
              });
            }
          });
      }
      catch (error) {
          setLoader({ isActive: false });
          setToaster({
            types: "error",
            isShow: true,
            toasterBody: error.response ? error.response.data.message : error.message,
            message: "Error",
          });
          setTimeout(() => {
            setToaster((prevToaster) => ({ ...prevToaster, isShow: false }));
          }, 2000);
        }
  }

  const deletelink = async (e, id) =>{ 
    e.preventDefault();
    setLoader({ isActive: true })
    await user_service.linkDelete(id).then((response) => {
        if (response) {
            setLoader({ isActive: false })
            setToaster({
                types: "Delete",
                isShow: true,
                toasterBody: response.data.message,
                // message: "",
              });
              setTimeout(() => {
                LinkGet();
              }, 1000);
        }
    });
  } 

  return (
    <>
    <div className="bg-secondary float-left w-100 pt-4 mb-4">
      <Loader isActive={isActive} />
      {isShow && (
        <Toaster
          types={types}
          isShow={isShow}
          toasterBody={toasterBody}
          message={message}
        />
      )}
      <main className="page-wrapper">
        <div className="">
          <div className="justify-content-center pb-sm-2">
            <div className="bg-light rounded-3 p-3 mb-3">
              <div className="d-flex align-items-center justify-content-between mb-4">
                  <h2 className="h4 mb-0 pull-left">
                    Links Listing
                  </h2>
                  { 
                    localStorage.getItem("auth") && (jwt(localStorage.getItem("auth")).contactType == "admin" || jwt(localStorage.getItem("auth")).contactType == "associate" ) ?
                    <div className="pull-right">
                      <NavLink type="button" className="btn btn-secondary pull-right" to="/add-linkfiles">Add Link</NavLink>
                    </div>
                    :""
                  }
                  
              </div>

              <div className="row">
                {formData.data && formData.data.length > 0
                  ? formData.data.map((items) => (
                      <div className="question_listing">
                      <div
                        className="accordion"
                        id={`accordionExample-${items._id}`}
                        key={items._id}
                      >
                        <div className="accordion-item">
                          <h2
                            className="accordion-header"
                            id={`heading-${items._id}`}
                          >
                            <button
                              className="accordion-button"
                              type="button"
                              data-bs-toggle="collapse"
                              data-bs-target={`#collapse-${items._id}`}
                              aria-expanded="true"
                              aria-controls={`collapse-${items._id}`}
                            >
                              {items.title}
                            </button>
                          </h2>
                          <div
                            className="accordion-collapse collapse"
                            aria-labelledby={`heading-${items._id}`}
                            data-bs-parent={`#accordionExample-${items._id}`}
                            id={`collapse-${items._id}`}
                          >
                            <div className="accordion-body">{items.desctiption}</div>
                            <hr />
                            {items.link_url.length>0 ? 
                              <div className="accordion-body mt-3">
                                <a
                                  href={items.link_url}
                                  target="_blank"
                                  rel="noopener noreferrer"
                                >
                                  {items.link_title}
                                </a>
                              </div>
                               :""
                            }
                               {items.file_url.length>0 ? 
                              <div className="accordion-body mt-3">
                                <a
                                  href={items.file_url}
                                  target="_blank"
                                  rel="noopener noreferrer"
                                >
                                  {items.link_title}
                                </a>
                              </div>
                              :""
                            }
                          </div>
                        </div>
                      </div>
                      { 
                        localStorage.getItem("auth") && (jwt(localStorage.getItem("auth")).contactType == "admin" || jwt(localStorage.getItem("auth")).id == items.agentId)  ?
                          <>
                              <div className="pull-right">
                                <NavLink to={`/add-linkfiles/${items._id}`}><i className="fi-edit ms-2"></i></NavLink>
                                <NavLink onClick={(e)=>pinlink(e,items._id, items.pin== "yes" ? "no" :"yes")} to={`#`}><i className="fa fa-thumb-tack ms-2" aria-hidden="true"></i></NavLink>
                                <NavLink onClick={(e)=>deletelink(e,items._id)} to={`#`}><i className="fa fa-trash ms-2" aria-hidden="true"></i></NavLink>
                                </div> 
                              </>
                          :""
                      }
                    
                      </div>
                    
                    ))
                  : "Apologies, but nothing was found; please check back later."}
              </div>
            </div>
          </div>
        </div>
      </main>
      
    </div>
    
</>
  );
}
export default Links;

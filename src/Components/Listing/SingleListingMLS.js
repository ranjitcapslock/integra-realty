import React, { useState, useEffect } from "react";
import { NavLink, useParams } from "react-router-dom";
import user_service from "../service/user_service";
import Loader from "../../Pages/Loader/Loader.js";
import Toaster from "../../Pages/Toaster/Toaster";
import avtar from "../img/photo2.jpg";
import defaultpropertyimage from "../../Components/img/defaultpropertyimage.jpeg";
import moment from "moment-timezone";

const SingleListingMLS = () => {
  const [loader, setLoader] = useState({ isActive: null });
  const { isActive } = loader;
  const [toaster, setToaster] = useState({
    types: null,
    isShow: null,
    toasterBody: "",
    message: "",
  });
  const { types, isShow, toasterBody, message } = toaster;
  const [summary, setSummary] = useState([]);
  const params = useParams();

  const MLSSearch = async () => {
    if (params.id) {
      try {
        setLoader({ isActive: true });
        let response;
        // if (membership === "UtahRealEstate.com") {
        //     console.log(membership)

        response = await user_service.listingSearchMLS(params.id);
        // } else if (membership === "Park City") {
        //   response = await user_service.listingSearchMLSspark(params.id);
        // } else if (membership === "Washington County") {
        //   response = await user_service.listingSearchMLSspark(params.id);
        // } else if (membership === "Iron County") {
        //   response = await user_service.listingSearchMLSspark(params.id);
        // } else {
        //   response = await user_service.listingSearchMLS(params.id);
        // }

        if (response) {
          //   console.log("response");
          //   console.log(response);
          setSummary(response.data);
        }

        setLoader({ isActive: false });
      } catch (error) {
        console.error(error);
        setLoader({ isActive: false });
      }
    }
  };

  const SingleListingById = async () => {
    await user_service.listingsGetById(params.id).then((response) => {
      setLoader({ isActive: false });
      if (response) {
        setSummary(response.data);
      }
    });
  };

  useEffect(() => {
    window.scrollTo(0, 0);
    // setLoader({ isActive: true })
    // SingleListingById(params.id)
    MLSSearch(params.id);
  }, []);
  const propertypic = (e) => {
    e.target.src = defaultpropertyimage;
  };
  return (
    <div className="bg-secondary float-left w-100 pt-4 mb-4">
      <Loader isActive={isActive} />
      {isShow && (
        <Toaster
          types={types}
          isShow={isShow}
          toasterBody={toasterBody}
          message={message}
        />
      )}
      <main className="page-wrapper">
        <div className="">
          {/* <div className="float-left w-100 d-flex align-items-center justify-content-between mb-4">
                <div className="d-flex align-items-center justify-content-between">
                    <h3 className="pull-left mb-0"></h3>
                    <NavLink className="btn btn-info btn-sm mb-2 pull-right" to="/mylisting">Back</NavLink>
                </div>
            </div> */}

          <div className="">
            {summary && summary.value ? (
              <>
                <div className="float-left w-100 d-flex align-items-center justify-content-between mb-4">
                  <section className="">
                    {/* <nav className="mb-3 pt-md-3" aria-label="breadcrumb">
                                                <ol className="breadcrumb">
                                                    <li className="breadcrumb-item"><a href="#">Home</a></li>
                                                    <li className="breadcrumb-item active" aria-current="page">Property</li>
                                                </ol>
                                            </nav> */}
                    <h3 className="text-white mb-2">
                      {summary.value[0].UnparsedAddress}
                    </h3>
                    <p className="text-white mb-0 fs-lg">
                      {summary.value[0].StateOrProvince}
                    </p>
                  </section>
                  <NavLink
                    className="btn btn-info btn-sm pull-right"
                    to="/mylisting"
                  >
                    <i className="fa fa-angle-left me-2"></i>Back
                  </NavLink>
                </div>

                <section className="mb-0 pb-3" data-simplebar>
                  <div
                    className="row g-2 g-md-3 gallery"
                    data-thumbnails="true"
                  >
                    <div className="col-md-8 singleListing">
                      <div className="bg-light border rounded-3 p-3">
                        <img
                          className="img-fluid"
                          src={
                            summary.media && summary.media[0]
                              ? summary.media[0].MediaURL
                              : defaultpropertyimage
                          }
                          alt="Property"
                          onError={(e) => propertypic(e)}
                        />
                        <div className="col-md-12 imageProperty">
                          {/* {console.log(JSON.parse(summary.additionaldataMLS).value)} */}
                          {summary.media
                            ? summary.media.length > 0
                              ? summary.media.map((item) => {
                                  return (
                                    <>
                                      <img
                                        src={item.MediaURL}
                                        alt="Gallery thumbnail"
                                      />
                                    </>
                                  );
                                })
                              : ""
                            : ""}
                        </div>

                        <section className="mt-3">
                          <div className="row">
                            <div className="col-md-12 mb-md-0">
                              {/* {console.log(JSON.parse(summary.additionaldataMLS))} */}
                              <h2 className="h3 border-bottom pb-2">
                                {summary.value[0].ListPrice
                                  ? parseFloat(
                                      summary.value[0].ListPrice
                                    ).toLocaleString("en-US")
                                  : ""}
                                <span className="d-inline-block ms-1 fs-base fw-normal text-body">
                                  USD
                                </span>
                              </h2>
                              <h3 className="h4">Property Details</h3>
                              <div className="row pb-md-3">
                                <ul className="list-unstyled col-lg-6 col-md-6 col-sm-12 col-12">
                                  <li className="mb-3 d-flex">
                                    <h6 className="mb-0 me-2">Category: </h6>
                                    {summary.value[0].PropertyType}
                                  </li>
                                  <li className="mb-3 d-flex">
                                    <h6 className="mb-0 me-2">
                                      ListingsAgent:{" "}
                                    </h6>
                                    {summary.value[0].ListAgentFullName}
                                  </li>
                                  <li className="mb-3 d-flex">
                                    <h6 className="mb-0 me-2">
                                      Listing Agent ID::{" "}
                                    </h6>
                                    {summary.value[0].ListAgentKey}
                                  </li>
                                  <li className="mb-3 d-flex">
                                    <h6 className="mb-0 me-2">
                                      Area Location:{" "}
                                    </h6>
                                    {summary.value[0].MLSAreaMajor}
                                  </li>
                                  <li className="mb-3 d-flex">
                                    <h6 className="mb-0 me-2">County: </h6>
                                    {summary.value[0].Country}
                                  </li>
                                  <li className="mb-3 d-flex">
                                    <h6 className="mb-0 me-2">Subdivision: </h6>
                                    {summary.value[0].Suh6divisionName}
                                  </li>
                                  <li className="mb-3 d-flex">
                                    <h6 className="mb-0 me-2">List Date: </h6>
                                    {summary?.value[0].OriginalEntryTimestamp
                                      ? moment
                                          .tz(
                                            summary?.value[0]
                                              .OriginalEntryTimestamp,
                                            "US/Mountain"
                                          )
                                          .format("M/D/YYYY")
                                      : ""}
                                  </li>
                                </ul>
                                <ul className="list-unstyled col-lg-6 col-md-6 col-sm-12 col-12">
                                  <li className="mb-3 d-flex">
                                    <h6 className="mb-0 me-2">MLS Number: </h6>#
                                    {summary.value[0].ListingId}
                                  </li>
                                  <li className="mb-3 d-flex">
                                    <h6 className="mb-0 me-2">
                                      AssociateName:{" "}
                                    </h6>
                                    {summary.value[0].AssociationName}
                                  </li>
                                  <li className="mb-3 d-flex">
                                    <h6 className="mb-0 me-2">
                                      MLS Office ID:{" "}
                                    </h6>
                                    {summary.value[0].ListOfficeKey}
                                  </li>
                                  <li className="mb-3 d-flex">
                                    <h6 className="mb-0 me-2">Status: </h6>
                                    {summary.value[0].MlsStatus}
                                  </li>
                                  <li className="me-3 pe-3 border-end mb-3 d-flex">
                                    <h6 className="mb-0 me-2">Bedrooms:</h6>
                                    {summary.value[0].BedroomsTotal}
                                  </li>
                                  <li className="me-3 pe-3 border-end mb-3 d-flex">
                                    <h6 className="mb-0 me-2">Bathrooms:</h6>{" "}
                                    {summary.value[0].BathroomsFull ?? 0} /{" "}
                                    {summary.value[0].BathroomsHalf ?? 0}
                                  </li>
                                  <li className="me-3 pe-3 border-end mb-3 d-flex">
                                    <h6 className="mb-0 me-2">Garage:</h6>{" "}
                                    {summary.GarageSpaces}
                                  </li>
                                  <li className="me-3 pe-3 border-end mb-3 d-flex">
                                    <h6 className="mb-0 me-2"> Square Ft:</h6>{" "}
                                    {summary.value[0].BuildingAreaTotal}
                                  </li>
                                  <li className="me-3 pe-3 border-end mb-3 d-flex">
                                    <h6 className="mb-0 me-2">Lot Size:</h6>{" "}
                                    {summary.value[0].LotSizeAcres} Acres
                                  </li>
                                </ul>
                              </div>
                              <div className="mb-4 pb-md-3">
                                <h3 className="h4">Overview</h3>
                                <p className="mb-1">
                                  {summary.value[0].PublicRemarks}
                                </p>
                              </div>

                              <div className="pb-lg-2 py-4 border-top">
                                <ul className="d-flex list-unstyled fs-sm mb-0">
                                  <li className="me-3 pe-3 border-end mb-3">
                                    Published by:{" "}
                                    <h6 className="mb-2 me-2">
                                      {summary.value[0].SourceSystemName}
                                    </h6>
                                  </li>
                                  <li className="me-3 pe-3 border-end mb-3">
                                    Published date:
                                    <h6 className="mb-2 me-2">
                                      {summary.value[0].OnMarketDate
                                        ? moment
                                            .tz(
                                              summary.value[0].OnMarketDate,
                                              "US/Mountain"
                                            )
                                            .format("M/D/YYYY")
                                        : ""}
                                    </h6>
                                  </li>
                                </ul>
                              </div>
                            </div>
                          </div>
                        </section>
                      </div>
                    </div>

                    <aside className="col-lg-4 col-md-5 ms-lg-auto pb-1">
                      <div className="card shadow-sm">
                        <div className="card-body">
                          <div className="d-flex align-items-start justify-content-between">
                            <a
                              className="text-decoration-none"
                              href="real-estate-vendor-properties.html"
                            >
                              {/* <img className="rounded-circle mb-2" src={avtar} width="60" alt="Avatar" /> */}
                              <h5 className="mb-1">
                                {summary.value[0].ListAgentFullName}
                              </h5>
                              <div className="mb-1">
                                <span className="star-rating">
                                  <i className="star-rating-icon fi-star-filled active"></i>
                                  <i className="star-rating-icon fi-star-filled active"></i>
                                  <i className="star-rating-icon fi-star-filled active"></i>
                                  <i className="star-rating-icon fi-star-filled active"></i>
                                  <i className="star-rating-icon fi-star-filled active"></i>
                                </span>
                                <span className="ms-1 fs-sm text-muted">
                                  (45 reviews)
                                </span>
                              </div>
                              <p className="text-body">
                                {summary.value[0].ListOfficeName}
                              </p>
                            </a>
                          </div>
                          <ul className="list-unstyled mb-0">
                            <li>
                              <a
                                className="nav-link fw-normal p-0"
                                href="tel:3025550107"
                              >
                                <i className="fi-phone mt-n1 me-2 align-middle opacity-60"></i>
                                {summary.value[0].ListAgentOfficePhone}
                              </a>
                            </li>
                          </ul>
                        </div>
                      </div>
                    </aside>
                  </div>
                </section>
              </>
            ) : (
              <p className="test-center">Information not available .</p>
            )}
          </div>
        </div>
      </main>
    </div>
  );
};

export default SingleListingMLS;

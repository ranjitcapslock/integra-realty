import React, { useState, useEffect } from "react";
import user_service from "../service/user_service";
import Loader from "../../Pages/Loader/Loader.js";
import Toaster from "../../Pages/Toaster/Toaster";
import { NavLink, useNavigate } from "react-router-dom";
import defaultpropertyimage from "../../Components/img/defaultpropertyimage.jpeg";

const MarketListing = () => {
  const [loader, setLoader] = useState({ isActive: null });
  const { isActive } = loader;
  const [toaster, setToaster] = useState({
    types: null,
    isShow: null,
    toasterBody: "",
    message: "",
  });
  const { types, isShow, toasterBody, message } = toaster;

  const [getListing, setGetListing] = useState([]);
  const navigate = useNavigate();

  const SingleListing = (id) => {
    navigate(`/single-listing/${id}`);
  };

  const propertypic = (e) => {
    e.target.src = defaultpropertyimage;
  };

  useEffect(() => {
    window.scrollTo(0, 0);
    setLoader({ isActive: true });
    const ListingGetAll = async () => {
      await user_service.listingsGet().then((response) => {
        setLoader({ isActive: false });
        if (response) {
          setGetListing(response.data.data);
        }
      });
    };
    ListingGetAll();
  }, []);

  return (
    <div className="bg-secondary float-left w-100 pt-4">
      <Loader isActive={isActive} />
      {isShow && (
        <Toaster
          types={types}
          isShow={isShow}
          toasterBody={toasterBody}
          message={message}
        />
      )}
      <main className="page-wrapper">
        <div className="container mb-md-4 py-5 pt-0">
          {/* <!-- Breadcrumb--> */}
          <nav className="mb-3 mb-md-3 pt-md-3" aria-label="Breadcrumb">
            <ol className="breadcrumb breadcrumb-dark">
              <li className="breadcrumb-item">
                <a href="#">Home</a>
              </li>
              <li className="breadcrumb-item">
                <a href="#">Account</a>
              </li>
              <li className="breadcrumb-item active" aria-current="page">
                My Property
              </li>
            </ol>
          </nav>

          <div className="tns-carousel-wrapper tns-controls-outside-xxl tns-nav-outside tns-nav-outside-flush mx-n2">
            <div className="tns-outer" id="tns2-ow">
              <div
                className="tns-liveregion tns-visually-hidden"
                aria-live="polite"
                aria-atomic="true"
              >
                slide <span className="current">9 to 12</span> of 5
              </div>
              <div id="tns2-mw" className="tns-ovh">
                <div className="tns-inner" id="tns2-iw">
                  <div
                    className="tns-carousel-inner row gx-4 mx-0 pt-3 pb-4  tns-slider tns-carousel tns-subpixel tns-calc tns-horizontal"
                    data-carousel-options='{"items": 4, "responsive": {"0":{"items":1},"500":{"items":2},"768":{"items":3},"992":{"items":4}}}'
                    id="tns2"
                  >
                    <div className="col tns-item tns-slide-cloned"
                      aria-hidden="true"
                      tabindex="-1"
                    >
                      <div className="card shadow-sm card-hover border-0 h-100">

                        <div className="card-img-top card-img-hover">
                          <img
                            src=""
                            alt="Image"
                          />
                        </div>
                        <div className="card-body position-relative pb-3">
                       
                          <h3 className="h6 mb-2 fs-base">
                            <a
                              className="nav-link stretched-link"
                              href="real-estate-single-v1.html"
                            >
                              Greenpoint Rentals | 85 sq.m
                            </a>
                          </h3>
                          <p className="mb-2 fs-sm text-muted">
                            1510 Castle Hill Ave Bronx, NY 10462
                          </p>
                          <div className="fw-bold">
                            <i className="fi-cash mt-n1 me-2 lead align-middle opacity-70"></i>
                            $1,330
                          </div>
                        </div>


                        <div className="card-footer d-flex align-items-center justify-content-center mx-3 pt-3 text-nowrap">
                          <span className="d-inline-block mx-1 px-2 fs-sm">
                            1<i className="fi-bed ms-1 mt-n1 fs-lg text-muted"></i>
                          </span>
                          <span className="d-inline-block mx-1 px-2 fs-sm">
                            1<i className="fi-bath ms-1 mt-n1 fs-lg text-muted"></i>
                          </span>
                          <span className="d-inline-block mx-1 px-2 fs-sm">
                            1<i className="fi-car ms-1 mt-n1 fs-lg text-muted"></i>
                          </span>
                        </div>
                      </div>
                    </div>
                 
                  </div>
                </div>
              </div>
            </div>
          </div>

          {/* <div className="row g-4 py-4">
                         
                            <div className="col-lg-8 col-md-7 mb-5">
                                <div className="d-flex align-items-center">
                                    <h1 className="h3 mb-0">Property Listings</h1><br />
                                </div>
                                <br />

                              
                                {
                                    getListing.map((post) => (

                                        <div className="card card-hover card-horizontal border-0 shadow-sm mb-4 p-3" onClick={(e) => SingleListing(post._id)}>
                                            <a className="card-img-top">
                                                <img className="img-fluid w-100" src={post.image &&  post.image !== "image" ? post.image : defaultpropertyimage}  alt="Property"  onError={e => propertypic(e)} />
                                            </a>
                                            <div className="card-body position-relative pb-3 pt-0">
                                                <p className="mb-2 fs-sm"><strong>MLS Number:</strong> #{post.mlsNumber}</p>
                                                <p className="mb-2 fs-sm"><strong>Street-Address:</strong> {post.streetAddress}</p>
                                                <p className="mb-2 fs-sm"><strong>Type:</strong> {post.propertyType}</p>
                                                <p className="mb-2 fs-sm"><strong>State:</strong> {post.areaLocation}</p>
                                                <p className="mb-2 fs-sm"><strong>List Price:</strong>$ {parseFloat(post.price).toLocaleString('en-US')}</p>
                                                <p className="mb-2 fs-sm"><strong>ListingsAgent:</strong> {post.listingsAgent}</p>
                                            </div>
                                        </div>
                                    ))
                                }

                            
                         
                           
                        </div>
                    </div> */}
        </div>
      </main>
    </div>
  );
};

export default MarketListing;

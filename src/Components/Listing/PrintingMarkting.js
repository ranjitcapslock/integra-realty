import React, { useState, useEffect } from "react";
import _ from "lodash";
import Loader from "../../Pages/Loader/Loader.js";
import Toaster from "../../Pages/Toaster/Toaster.js";
import { NavLink, useNavigate } from "react-router-dom";
import TypeOne from "../img/type1.png";
import TypeTwo from "../img/BrchureMls.png";
import TypeThree from "../img/postCards.png";
import TypeFour from "../img/PropertyCard.png";
import TypeFive from "../img/PropertyLabel.png";



function PrintingMarkting() {
  const [loader, setLoader] = useState({ isActive: null });
  const { isActive } = loader;
  const [toaster, setToaster] = useState({
    types: null,
    isShow: null,
    toasterBody: "",
    message: "",
  });
  const { types, isShow, toasterBody, message } = toaster;
  const navigate = useNavigate();

  return (
    <div className="bg-secondary float-left w-100 pt-4 mb-4">
      <Loader isActive={isActive} />
      {isShow && (
        <Toaster
          types={types}
          isShow={isShow}
          toasterBody={toasterBody}
          message={message}
        />
      )}
      <main className="page-wrapper">
        <div className="content-overlay">
          <div className="d-flex align-items-center justify-content-start mb-4">
            <h3 className="mb-0 text-white">Design Printing Marketing</h3>
          </div>
          <div className="row bg-light p-3 border-0 rounded-3">
            <div className="select_template_print float-start w-100 mt-3 mb-5">
              <div className="d-lg-flex d-md-flex d-sm-block d-block">
                <div>
                  <div className="form-check mb-0">
                    <img
                      // className={templateType === "type1" ? "active" : ""}
                      src={TypeOne}
                      alt="Type One"
                      style={{ cursor: "pointer" }}
                      width="100px"
                      height="100px"
                    />
                  </div>
                  <NavLink
                    to={`/real-estate-print`}
                    className="btn btn-primary mt-2"
                  >
                    Flyer Design
                  </NavLink>
                </div>
                <div className="ms-lg-5 ms-md-5 ms-sm-5 ms-0 mt-lg-0 mt-md-0 mt-sm-4 mt-4">
                  <div className="form-check mb-0">
                    <img
                      // className={templateType === "type2" ? "active" : ""}
                      src={TypeTwo}
                      alt="Type Two"
                      style={{ cursor: "pointer" }}
                      width="100px"
                      height="100px"
                      // onClick={() => handleType("type2")}
                    />
                  </div>
                  <NavLink
                    to={`/brochure-mls-print`}
                    className="btn btn-primary mt-2"
                  >
                    Brochure Design
                  </NavLink>
                </div>

                <div className="ms-lg-5 ms-md-5 ms-sm-5 ms-0 mt-lg-0 mt-md-0 mt-sm-4 mt-4">
                  <div className="form-check mb-0">
                    <img
                      // className={templateType === "type2" ? "active" : ""}
                      src={TypeThree}
                      alt="Type Two"
                      style={{ cursor: "pointer" }}
                      width="100px"
                      height="100px"
                      // onClick={() => handleType("type2")}
                    />
                  </div>
                  <NavLink
                    to={`/postcard-mls-print`}
                    className="btn btn-primary mt-2"
                  >
                    Post Cards Design
                  </NavLink>
                </div>

                <div className="ms-lg-5 ms-md-5 ms-sm-5 ms-0 mt-lg-0 mt-md-0 mt-sm-4 mt-4">
                  <div className="form-check mb-0">
                    <img
                      src={TypeFour}
                      alt="Type Two"
                      style={{ cursor: "pointer" }}
                      width="100px"
                      height="100px"
                      // onClick={() => handleType("type2")}
                    />
                  </div>
                  <NavLink
                    to={`/propertycard-mls-print`}
                    className="btn btn-primary mt-2"
                  >
                    Property Cards Design
                  </NavLink>
                </div>

                <div className="ms-lg-5 ms-md-5 ms-sm-5 ms-0 mt-lg-0 mt-md-0 mt-sm-4 mt-4">
                  <div className="form-check mb-0">
                    <img
                      src={TypeFive}
                      alt="Type Five"
                      style={{ cursor: "pointer" }}
                      width="100px"
                      height="100px"
                      // onClick={() => handleType("type2")}
                    />
                  </div>
                  <NavLink
                    to={`/propertylabel-mls-print`}
                    className="btn btn-primary mt-2"
                  >
                    Property Labels Design
                  </NavLink>
                </div>
              </div>
            </div>
          </div>
        </div>
      </main>
    </div>
  );
}
export default PrintingMarkting;

import React, { useState, useRef, useEffect } from "react";
import { NavLink, useNavigate, useParams } from "react-router-dom";
import user_service from "../service/user_service";
import jwt from "jwt-decode";
import _ from "lodash";
import Loader from "../../Pages/Loader/Loader.js";
import Toaster from "../../Pages/Toaster/Toaster.js";
import TypeOne from "../img/postCards.png";

import defaultpropertyimage from "../../Components/img/defaultpropertyimage.jpeg";

import Logo from "../img/logo.png";
import {
  Document,
  Page,
  Text,
  Image,
  StyleSheet,
  PDFDownloadLink,
  View,
} from "@react-pdf/renderer";

const PostCards = () => {
  const [membership, setMembership] = useState("");
  const componentRef = useRef();

  const [loader, setLoader] = useState({ isActive: null });
  const { isActive } = loader;
  const [toaster, setToaster] = useState({
    types: null,
    isShow: null,
    toasterBody: "",
    message: "",
  });
  const { types, isShow, toasterBody, message } = toaster;

  const [searchmlsnumber, setSearchmlsnumber] = useState({ mlsNumber: "" });

  const [select, setSelect] = useState("");

  useEffect(() => {
    window.scrollTo(0, 0);
    ContactGetById(jwt(localStorage.getItem("auth")).id);
    PlatformdetailsGet();
  }, []);

  const handleChangesearchmls = (event) => {
    setSearchmlsnumber({ mlsnumber: event.target.value });
  };

  const [templateType, setTemplateType] = useState();
  const [isLoading, setIsLoading] = useState(false);

  const handleType = (input) => {
    const templateType = input.target ? input.target.value : input;
    setTemplateType(templateType);
  };

  const API_BASE_URL = "https://api.brokeragentbase.com";
  const MLSSearch = async () => {
    if (searchmlsnumber.mlsnumber) {
      try {
        setIsLoading(true);
        let response;

        if (membership === "UtahRealEstate.com") {
          response = await user_service.listingSearchMLS(
            searchmlsnumber.mlsnumber
          );
        } else if (membership === "Park City") {
          response = await user_service.listingSearchMLSspark(
            searchmlsnumber.mlsnumber,
            "parkcity"
          );
        } else if (membership === "Washington County") {
          response = await user_service.listingSearchMLSspark(
            searchmlsnumber.mlsnumber,
            "washingtoncounty"
          );
        } else if (membership === "Iron County") {
          response = await user_service.listingSearchMLSspark(
            searchmlsnumber.mlsnumber,
            "ironcounty"
          );
        } else {
          response = await user_service.listingSearchMLS(
            searchmlsnumber.mlsnumber
          );
        }

        if (response && response.data) {
          const propertyData = response.data;

          propertyData.media = (propertyData.media || []).map((image) => ({
            MediaURL: image?.MediaURL
              ? `${API_BASE_URL}/proxy-image?url=${encodeURIComponent(
                  image.MediaURL
                )}`
              : defaultpropertyimage,
            description: image?.description || "Click to add a description",
          }));
          setSelect(propertyData);
          setToaster({
            types: "Success",
            isShow: true,
            message: "MLS search successful!",
          });
        } else {
          setToaster({
            types: "Error",
            isShow: true,
            message: "No data found for the provided MLS number.",
          });
        }

        setIsLoading(false);
      } catch (error) {
        console.error("Error in MLSSearch:", error);
        setToaster({
          types: "Error",
          isShow: true,
          message: "Please provide a valid MLS number.",
        });
      } finally {
        setIsLoading(false);
        setTimeout(() => {
          setToaster((prevToaster) => ({ ...prevToaster, isShow: false }));
        }, 2000);
      }
    } else {
      setToaster({
        types: "Error",
        isShow: true,
        message: "Please provide a valid MLS number.",
      });
      setTimeout(() => {
        setToaster((prevToaster) => ({ ...prevToaster, isShow: false }));
      }, 2000);
    }
  };

  const [getContact, setGetContact] = useState("");
  const [mls_membership, setMls_membership] = useState([]);
  const [
    commaSeparatedValuesmls_membership,
    setCommaSeparatedValuesmls_membership,
  ] = useState("");

  const ContactGetById = async () => {
    await user_service
      .contactGetById(jwt(localStorage.getItem("auth")).id)
      .then((response) => {
        if (response) {
          setGetContact(response.data);
          if (response.data && response.data.additionalActivateFields) {
            const mls_membershipString =
              response.data.additionalActivateFields?.mls_membership;
            if (mls_membershipString) {
              if (typeof mls_membershipString === "string") {
                try {
                  const mls_membershipParsed = JSON.parse(mls_membershipString);
                  if (mls_membershipParsed == "") {
                    setMls_membership(mls_membership);
                  } else {
                    setMls_membership(mls_membershipParsed);
                  }
                  if (mls_membershipParsed == "") {
                    setCommaSeparatedValuesmls_membership("");
                  } else {
                    const boardMembershipValues = mls_membershipParsed.map(
                      (item) => item.value
                    );

                    setCommaSeparatedValuesmls_membership(
                      boardMembershipValues
                    );
                  }
                } catch (error) {
                  console.error("Error parsing mls_membership as JSON:", error);
                  setCommaSeparatedValuesmls_membership("");
                }
              } else {
                setCommaSeparatedValuesmls_membership("");
              }
            } else {
              setCommaSeparatedValuesmls_membership("");
            }
          }
        }
      });
  };

  const [editingField, setEditingField] = useState(null);
  const [editingText, setEditingText] = useState(null);
  const [editedText, setEditedText] = useState("");
  const [selectedImage, setSelectedImage] = useState(null);
  const [uploadedImages, setUploadedImages] = useState({});

  const handleImageClick = (imageIndex) => {
    setSelectedImage(imageIndex);
  };

  // Handle image upload
  const handleImageUpload = (e, imageIndex) => {
    const file = e.target.files[0];
    if (file) {
      const reader = new FileReader();
      reader.onload = (upload) => {
        // Update the uploaded image state
        setUploadedImages((prev) => ({
          ...prev,
          [imageIndex]: upload.target.result,
        }));

        // Ensure that `media` is defined before updating it
        setSelect((prevSummary) => {
          const updatedImages = [...(prevSummary.media || [])]; // Handle case where media is undefined
          updatedImages[imageIndex] = { MediaURL: upload.target.result }; // Update the image at the specific index
          return { ...prevSummary, media: updatedImages };
        });
      };
      reader.readAsDataURL(file);
    }
    setSelectedImage(null);
  };

  const handleTextClick = (field, currentValue) => {
    setEditingField(field); // Set the field being edited
    setEditedText(currentValue); // Pre-fill input with current value (address)
  };

  // Function to handle saving the edited text locally
  const handleTextSave = (field) => {
    // Update the selected value locally after editing
    setSelect((prevSelect) => ({
      ...prevSelect,
      value: [
        {
          ...prevSelect.value[0],
          UnparsedAddress: editedText, // Update the address with the new value
        },
      ],
    }));

    setEditingField(null); // Close the input field after saving
  };

  
  const handleTextPrice = (field, currentValue) => {
    setEditingField(field); // Set the field being edited
    setEditedText(currentValue); // Pre-fill input with current value (address)
  };

  const handleTextSavePrice = (field) => {
    // Update the selected value locally after editing
    setSelect((prevSelect) => ({
      ...prevSelect,
      value: [
        {
          ...prevSelect.value[0],
          ListPrice: editedText, // Update the address with the new value
        },
      ],
    }));

    setEditingField(null); // Close the input field after saving
  };

  const handleTextPublicRemarks = (field, currentValue) => {
    setEditingField(field); // Set the field being edited
    setEditedText(currentValue); // Pre-fill input with current value (address)
  };

  const handleTextSavePublicRemarks = (field) => {
    setSelect((prevSelect) => ({
      ...prevSelect,
      value: [
        {
          ...prevSelect.value[0],
          PublicRemarks: editedText, // Update the address with the new value
        },
      ],
    }));

    setEditingField(null); // Close the input field after saving
  };

  


  const handleTextListAgent = (field, currentValue) => {
    setEditingField(field); // Set the field being edited
    setEditedText(currentValue); // Set the current text value in the input
  };

  // Handle saving the edited text for the list agent
  const handleTextSaveListAgent = (field) => {
    setProfile((prevProfile) => ({
      ...prevProfile,
      [field]: editedText, // Dynamically update the appropriate field (firstName or lastName)
    }));
    setEditingField(null); // Close the input field after saving
  };

  const handleTextListOffice = (field, currentValue) => {
    setEditingField(field); // Set the field being edited
    setEditedText(currentValue); // Pre-fill input with current value (address)
  };

  const handleTextSaveListOffice = (field) => {
    // Update the selected value locally after editing
    setProfile((prevProfile) => ({
      ...prevProfile,
      active_office: editedText, // Dynamically update the appropriate field (firstName or lastName)
    }));

    setEditingField(null); // Close the input field after saving
  };

  const handleTextOfficePhone = (field, currentValue) => {
    setEditingField(field); // Set the field being edited
    setEditedText(currentValue); // Pre-fill input with current value (address)
  };

  const handleTextSaveOfficePhone = (field) => {
    // Update the selected value locally after editing
    setProfile((prevProfile) => ({
      ...prevProfile,
      phone: editedText, // Dynamically update the appropriate field (firstName or lastName)
    }));

    setEditingField(null); // Close the input field after saving
  };

  const handleTextPublished = (field, currentValue) => {
    setEditingField(field); // Set the field being edited
    setEditedText(currentValue); // Pre-fill input with current value (address)
  };

  const handleTextSavePublished = (field) => {
    // Update the selected value locally after editing
    setSelect((prevSelect) => ({
      ...prevSelect,
      value: [
        {
          ...prevSelect.value[0],
          SourceSystemName: editedText, // Update the address with the new value
        },
      ],
    }));

    setEditingField(null); // Close the input field after saving
  };

  const handleTextCompanyCity = (field, currentValue) => {
    setEditingField(field); // Set the field being edited
    setEditedText(currentValue); // Pre-fill input with current value (address)
  };

  const handleTextSaveCompanyCity = (field) => {
    // Update the selected value locally after editing
    setSelect((prevSelect) => ({
      ...prevSelect,
      value: [
        {
          ...prevSelect.value[0],
          City: editedText, // Update the address with the new value
        },
      ],
    }));

    setEditingField(null); // Close the input field after saving
  };

  const handleTextCompanyState = (field, currentValue) => {
    setEditingField(field); // Set the field being edited
    setEditedText(currentValue); // Pre-fill input with current value (address)
  };

  const handleTextSaveCompanyState = (field) => {
    // Update the selected value locally after editing
    setSelect((prevSelect) => ({
      ...prevSelect,
      value: [
        {
          ...prevSelect.value[0],
          StateOrProvince: editedText, // Update the address with the new value
        },
      ],
    }));

    setEditingField(null); // Close the input field after saving
  };

  const handleTextCompanyPostCode = (field, currentValue) => {
    setEditingField(field); // Set the field being edited
    setEditedText(currentValue); // Pre-fill input with current value (address)
  };

  const handleTextSaveCompanyPostCode = (field) => {
    // Update the selected value locally after editing
    setSelect((prevSelect) => ({
      ...prevSelect,
      value: [
        {
          ...prevSelect.value[0],
          PostalCode: editedText, // Update the address with the new value
        },
      ],
    }));

    setEditingField(null); // Close the input field after saving
  };

  const [platformData, setPlatFormData] = useState({});
  const [uploadedLogo, setUploadedLogo] = useState(null);
  const [showLogoInput, setShowLogoInput] = useState(false); // State to show or hide input field
  const [profile, setProfile] = useState({});

  const [uploadedContact, setUploadedContact] = useState(null);
  const [showContact, setShowContact] = useState(false);

  const PlatformdetailsGet = async () => {
    await user_service.platformdetailsGet().then((response) => {
      if (response) {
        setPlatFormData(response.data.data[0]);
      }
    });
  };

  const profileGetAll = async () => {
    if (localStorage.getItem("auth")) {
      try {
        const response = await user_service.profileGet(
          jwt(localStorage.getItem("auth")).id
        );
        if (response) {
          const profileData = response.data;
          if (profileData && profileData.image) {
            const proxyImageUrl = `${API_BASE_URL}/proxy-image?url=${encodeURIComponent(
              profileData.image
            )}`;

            // Assuming 'setProfile' is a function that updates the state with the modified profile data
            setProfile({
              ...profileData, // Copy existing profile data
              image: proxyImageUrl, // Replace original image with the proxy URL
            });
          } else {
            setProfile(profileData); // No image to proxy, just set the profile as is
          }
        }
      } catch (error) {
        console.error("Error fetching profile data:", error);
      }
    }
  };

  useEffect(() => {
    window.scrollTo(0, 0);
    PlatformdetailsGet();
    profileGetAll();
  }, []);

  // Handle logo upload

  const handleLogoUploadContact = (e) => {
    const file = e.target.files[0];
    if (file) {
      const reader = new FileReader();
      reader.onload = (upload) => {
        setUploadedContact(upload.target.result); // Store the uploaded logo
        setShowContact(false); // Hide the input after upload
        // Update platformData with the new logo while preserving the rest of the data
        setProfile((prevData) => ({
          ...prevData,
          image: upload.target.result || prevData.image, // Update the logo in platformData
        }));
      };
      reader.readAsDataURL(file); // Read the file as a data URL for image preview
    }
  };

  const handleLogoUpload = (e) => {
    const file = e.target.files[0];
    if (file) {
      const reader = new FileReader();
      reader.onload = (upload) => {
        setUploadedLogo(upload.target.result); // Store the uploaded logo
        setShowLogoInput(false); // Hide the input after upload
        // Update platformData with the new logo while preserving the rest of the data
        setPlatFormData((prevData) => ({
          ...prevData,
          platform_logo: upload.target.result, // Update the logo in platformData
        }));
      };
      reader.readAsDataURL(file); // Read the file as a data URL for image preview
    }
  };

  // Handle logo click to show the input
  const handleLogoClick = () => {
    setShowLogoInput(true); // Show the file input when the logo is clicked
  };

  const handleLogoClickContact = () => {
    setShowContact(true); // Show the file input when the logo is clicked
  };

  const styles = StyleSheet.create({
    page: {
      backgroundColor: "#b4a3a359",
      padding: 10,
    },

    pageThird: {
      backgroundColor: "#b4a3a359",
      padding: 10,
    },

    rowContainer: {
      display: "flex",
      flexDirection: "row",
      justifyContent: "space-between",
      width: "100%",
    },

    rowContainerSecond: {
      display: "flex",
      flexDirection: "row",
      justifyContent: "space-between",
      width: "100%",
      marginBottom: 5,
    },

    rowContainerThird: {
      flexDirection: "row",
      justifyContent: "space-between",
      width: "100%",
      height: 200,
    },
    colMd8: {
      width: "67%", // Equivalent to col-md-8
      height: "100%",
      paddingRight: 10,
    },
    colMd4: {
      width: "33%", // Equivalent to col-md-4
      height: "100%",
      paddingLeft: 10,
    },
    mainImage: {
      width: "100%",
      height: "100%",
      objectFit: "cover",
      // border: "5px solid #00000",
    },
    sideImage: {
      width: "100%",
      height: "100%",
      objectFit: "cover",
    },

    mainpic: {
      width: "100%",
      height: 400,
      objectFit: "cover",
      marginBottom: 2,
      marginTop: 40,
    },

    mainpicSecond: {
      width: "100%",
      height: 300,
      objectFit: "cover",
      marginBottom: 2,
      marginTop: 40,
    },


    rowContainerFourth: {
      flexDirection: "row",
      justifyContent: "space-between",
      width: "100%",
      height: 200,
      backgroundColor: "#000000",
      marginTop:5
    },

    descriptionText: {
      marginTop: 5,
      padding:5,
      color: "#fff",
      fontSize: 10,
      textAlign: "justify",
    },

    // contact section
    addressBox: {
      width: "100%",
      backgroundColor: getComputedStyle(
        document.documentElement
      ).getPropertyValue("--branding-footer-color"),
      padding: 10,
      // marginTop: 40,
      // borderRadius: 5,
      display: "flex",
      flexDirection: "row",
      justifyContent: "space-between",
      height: 150,
      // marginBottom:0,
    },

    officeSection: {
      display: "flex",
      // justifyContent : "flex-start",
      marginBottom: 5,
      position: "relative",
    },

    officeSectionBorder: {
      borderWidth: 5,
      border: "5px solid #00000",
      // borderColor: "#000000",
    },

    officeContactImage: {
      width: 70,
      height: 100,
      marginLeft: 10,
      bottom: 2,
      position: "absolute",
      borderRadius: 0,
      borderWidth: 5,
      backgroundColor:"white",
      border: "5px solid #00000",
    },

    officeAddress: {
      width: "50%",
      alignItems: "flex-start",
      marginBottom: 5,
    },

    officeAddressSecond: {
      alignItems: "flex-end",
      marginTop:10,
      marginRight: 10,
    },

    officeDetails: {
      marginLeft: 10,
      // marginTop:10
    },
    officeText: {
      fontSize: 10,
      color: "#ffffff",
      // textTransform: "uppercase",
      marginBottom: 2,
    },

    officeLogo: {
      width: 80,
      objectFit: "contain",
      marginBottom: 10,
    },
  
  });

  const Flyer = ({ select, platformData, profile }) => {
    return (
      <Document>
        <Page size="A4" style={styles.page}>
          <View style={styles.rowContainer}>
            {select?.media[0]?.MediaURL && (
              <Image style={styles.mainpic} src={select.media[0].MediaURL} />
            )}
          </View>

          {/* contact section */}
          <View style={styles.addressBox}>
            <View style={styles.officeAddress}>
              <View>
                <Image style={styles.officeLogo} src={platformData || Logo} />
              </View>
              <Text style={styles.officeText}>
                Price $ {select?.value[0]?.ListPrice || "X00,00"}
              </Text>
              <Text style={styles.officeText}>
                {select?.value[0]?.SourceSystemName || "UtahRealEstate.com"}
              </Text>
              <Text style={styles.officeText}>
                {select?.value[0]?.UnparsedAddress || "2024 Demo St"}
              </Text>
              <Text style={styles.officeText}>
                {select?.value[0]?.City || "American Fork"},&nbsp;
                {select?.value[0]?.StateOrProvince || "UT"}&nbsp;
                {select?.value[0]?.PostalCode || "84003"}
              </Text>
            </View>
            <View style={styles.officeSection}>
              <View style={styles.officeSectionBorder}>
                <Image style={styles.officeContactImage} src={profile.image} />
              </View>
              <View style={styles.officeDetails}>
                <Text style={styles.officeText}>
                  Agent: {profile?.firstName} {profile?.lastName}
                </Text>
                <Text style={styles.officeText}>
                  Office: {profile?.active_office}
                </Text>
                <Text style={styles.officeText}>Phone: {profile?.phone}</Text>
              </View>
            </View>
          </View>
        </Page>

        <Page size="A4" style={styles.pageThird}>
          <View style={styles.rowContainerSecond}>
            {select?.media[1]?.MediaURL && (
              <Image
                style={styles.mainpicSecond}
                src={select.media[1].MediaURL}
              />
            )}
          </View>

          <View style={styles.rowContainerThird}>
            {/* Left Section: col-md-8 */}
            <View style={styles.colMd8}>
              {select?.media[2]?.MediaURL && (
                <Image
                  source={{ uri: select.media[2].MediaURL }}
                  style={styles.mainImage}
                />
              )}
            </View>

            {/* Right Section: col-md-4 */}
            <View style={styles.colMd4}>
              {select?.media[3]?.MediaURL && (
                <Image
                  source={{ uri: select.media[3].MediaURL }}
                  style={styles.sideImage}
                />
              )}
            </View>
          </View>

          <View style={styles.rowContainerFourth}>
            {/* Left Section: col-md-8 */}
            <View style={styles.colMd8}>
              <Text  style={styles.descriptionText}>
                {select?.value[0]?.PublicRemarks}
              </Text>
            </View>

            {/* Right Section: col-md-4 */}
            <View style={styles.colMd4}>
            <View style={styles.officeAddressSecond}>
              <View>
                <Image style={styles.officeLogo} src={platformData || Logo} />
              </View>
              <Text style={styles.officeText}>
                {select?.value[0]?.SourceSystemName || "UtahRealEstate.com"}
              </Text>
              <Text style={styles.officeText}>
                {select?.value[0]?.UnparsedAddress || "2024 Demo St"}
              </Text>
              <Text style={styles.officeText}>
                {select?.value[0]?.City || "American Fork"},&nbsp;
                {select?.value[0]?.StateOrProvince || "UT"}&nbsp;
                {select?.value[0]?.PostalCode || "84003"}
              </Text>
            </View>
            </View>
          </View>
        </Page>
      </Document>
    );
  };

  const handleCancel = () => {
    setSelect("");
  };

  const navigate = useNavigate();

  const handleNonListed = () => {
    navigate("/real-estate-non-listed-print");
  };

  const [nextStep, setNextStep] = useState("1");
  const handleNext = (newStep) => {
    setNextStep(newStep);
  };

  const wordlimit = (e) => {
    const wordCount = e.target.value.length;
    console.log(wordCount, "wordCount");
    if (wordCount < 800) {
      setEditedText(e.target.value);
    }
  };

  const wordlimitSecond = (e) => {
    const wordCount = e.target.value.length;
    console.log(wordCount, "wordCount");
    if (wordCount < 150) {
      setEditedText(e.target.value);
    }
  };

 

  return (
    <div className="bg-secondary float-left w-100 pt-4 mb-4">
      <Loader isActive={isActive} />

      {isShow && (
        <Toaster
          types={types}
          isShow={isShow}
          toasterBody={toasterBody}
          message={message}
        />
      )}
      <main className="page-wrapper">
        <div className="row event-calender">
          {select ? (
            <div className="">
              {templateType === "type1" ? (
                <div className="">
                  {select && select.value && select.value.length > 0 && (
                    <section className="mb-5 pb-3 w-100" data-simplebar>
                      <div
                        className="bg-light border rounded-3 p-3"
                        data-thumbnails="true"
                      >
                        <div className="postCardsTemplate">
                          {/* Step 1 */}
                          {nextStep === "1" && (
                            <div className="row" data-thumbnails="true">
                              <div className="col-md-12 singleListing">
                                <div className="card-img-top">
                                  <img
                                    className="img-fluid"
                                    style={{
                                      minHeight: "400px",
                                      borderRadius: "0px",
                                    }}
                                    src={
                                      uploadedImages[0] ||
                                      select.media[0].MediaURL ||
                                      defaultpropertyimage
                                    }
                                    alt="Property"
                                    onClick={() => handleImageClick(0)}
                                  />
                                  {selectedImage === 0 && (
                                    <label className="documentlabelNew">
                                      <input
                                        id="REPC_real_estate_purchase_contract"
                                        type="file"
                                        onChange={(e) =>
                                          handleImageUpload(e, 0)
                                        }
                                      />
                                      <i className="h2 fi-edit opacity-80"></i>
                                    </label>
                                  )}
                                </div>
                              </div>
                              <div className="col-md-12 w-100 mt-1">
                                <div className="row contact_postCards">
                                  <div className="col-md-6">
                                    <div className="imageLogo w-100  float-start w-100">
                                      <div>
                                        <img
                                          className="mb-2 rounded-0"
                                          src={uploadedLogo || Logo}
                                          style={{
                                            width: "100px",
                                            cursor: "pointer",
                                          }}
                                          onClick={handleLogoClick}
                                        />
                                        {showLogoInput && (
                                          <label className="documentlabelAllLogo">
                                            <input
                                              id="REPC_real_estate_purchase_contract"
                                              type="file"
                                              onChange={handleLogoUpload}
                                            />
                                            <i class="h2 fi-edit opacity-80"></i>
                                          </label>
                                        )}
                                      </div>

                                      {editingField === "price" ? (
                                        <div className="text-center">
                                          <input
                                            type="text"
                                            value={editedText}
                                            onChange={(e) =>
                                              setEditedText(e.target.value)
                                            }
                                            onBlur={() =>
                                              handleTextSavePrice("price")
                                            }
                                            autoFocus
                                          />
                                        </div>
                                      ) : (
                                        <span
                                          className="text-white"
                                          onClick={() =>
                                            handleTextPrice(
                                              "price",
                                              select?.value[0]?.ListPrice
                                            )
                                          }
                                        >
                                          $ {select?.value[0]?.ListPrice ?? "0"}
                                        </span>
                                      )}

                                      {editingField === "published" ? (
                                        <input
                                          className="w-100"
                                          type="text"
                                          value={editedText}
                                          onChange={(e) =>
                                            setEditedText(e.target.value)
                                          }
                                          onBlur={() =>
                                            handleTextSavePublished("published")
                                          }
                                          autoFocus
                                        />
                                      ) : (
                                        <p
                                          className="mb-0 text-white"
                                          style={{ fontSize: "12px" }}
                                          onClick={() =>
                                            handleTextPublished(
                                              "published",
                                              select.value[0]?.SourceSystemName
                                            )
                                          }
                                        >
                                          {select.value[0]?.SourceSystemName ||
                                            "UtahRealEstate.com"}
                                        </p>
                                      )}

                                      {editingField === "address" ? (
                                        <input
                                          className="w-100"
                                          type="text"
                                          value={editedText} // Controlled input with edited text
                                          onChange={(e) =>
                                            setEditedText(e.target.value)
                                          } // Update text as the user types
                                          onBlur={() =>
                                            handleTextSave("address")
                                          }
                                          autoFocus
                                        />
                                      ) : (
                                        <span
                                          className="mb-0 text-white"
                                          style={{ fontSize: "12px" }}
                                          onClick={() =>
                                            handleTextClick(
                                              "address",
                                              select?.value[0]?.UnparsedAddress
                                            )
                                          }
                                        >
                                          {select?.value[0]?.UnparsedAddress ||
                                            "2024 Demo St"}
                                        </span>
                                      )}

                                      <br />

                                      {editingField === "companyCity" ? (
                                        <input
                                          className="w-100"
                                          type="text"
                                          value={editedText} // Controlled input with edited text
                                          onChange={(e) =>
                                            setEditedText(e.target.value)
                                          } // Update text as the user types
                                          onBlur={() =>
                                            handleTextSaveCompanyCity(
                                              "companyCity"
                                            )
                                          }
                                          autoFocus
                                        />
                                      ) : (
                                        <span
                                          className="mb-0 text-white"
                                          style={{ fontSize: "12px" }}
                                          onClick={() =>
                                            handleTextCompanyCity(
                                              "companyCity",
                                              select?.value[0]?.City
                                            )
                                          }
                                        >
                                          {select?.value[0]?.City ||
                                            "American Fork"}
                                          ,
                                        </span>
                                      )}

                                      {editingField === "companyState" ? (
                                        <input
                                          type="text"
                                          value={editedText} // Controlled input with edited text
                                          onChange={(e) =>
                                            setEditedText(e.target.value)
                                          } // Update text as the user types
                                          onBlur={() =>
                                            handleTextSaveCompanyState(
                                              "companyState"
                                            )
                                          }
                                          autoFocus
                                        />
                                      ) : (
                                        <span
                                          className="mb-0 text-white"
                                          style={{ fontSize: "12px" }}
                                          onClick={() =>
                                            handleTextCompanyState(
                                              "companyState",
                                              select?.value[0]?.StateOrProvince
                                            )
                                          }
                                        >
                                          {select?.value[0]?.StateOrProvince ||
                                            "UT"}
                                          ,
                                        </span>
                                      )}

                                      {editingField === "companyPostCode" ? (
                                        <input
                                          className="w-100"
                                          type="text"
                                          value={editedText} // Controlled input with edited text
                                          onChange={(e) =>
                                            setEditedText(e.target.value)
                                          } // Update text as the user types
                                          onBlur={() =>
                                            handleTextSaveCompanyPostCode(
                                              "companyPostCode"
                                            )
                                          }
                                          autoFocus
                                        />
                                      ) : (
                                        <span
                                          className="mb-0 text-white"
                                          style={{ fontSize: "12px" }}
                                          onClick={() =>
                                            handleTextCompanyPostCode(
                                              "companyPostCode",
                                              select?.value[0]?.PostalCode
                                            )
                                          }
                                        >
                                          {select?.value[0]?.PostalCode ||
                                            "84003"}
                                        </span>
                                      )}
                                    </div>
                                  </div>

                                  <div className="col-md-6">
                                    <div className="pull-right text-end">
                                      <img
                                        className="print_Profile_PostCard"
                                        src={
                                          uploadedContact ||
                                          (profile.image ? profile.image : "")
                                        } // Show uploaded logo or default avatar
                                        style={{
                                          // width: "100px",
                                          cursor: "pointer",
                                        }} // Pointer cursor for better UX
                                        onClick={handleLogoClickContact} // Show file input on click
                                      />
                                      {/* Conditionally render file input on logo click */}
                                      {showContact && (
                                        <label className="documentlabelPostCards">
                                          <input
                                            id="REPC_real_estate_purchase_contract"
                                            type="file"
                                            onChange={handleLogoUploadContact}
                                          />
                                          <i class="h2 fi-edit opacity-80"></i>
                                        </label>
                                      )}

                                      <div className="d-flex mt-3">
                                        {editingField === "firstName" ? (
                                          <input
                                            type="text"
                                            value={editedText} // Controlled input with edited text
                                            onChange={(e) =>
                                              setEditedText(e.target.value)
                                            } // Update text as the user types
                                            onBlur={() =>
                                              handleTextSaveListAgent(
                                                "firstName"
                                              )
                                            } // Save the text when the user leaves the input field
                                            autoFocus
                                          />
                                        ) : (
                                          <span
                                            style={{ fontSize: "12px" }}
                                            onClick={() =>
                                              handleTextListAgent(
                                                "firstName",
                                                profile.firstName,
                                                profile.lastName
                                              )
                                            }
                                          >
                                            {profile && (
                                              <p className="mb-0 text-white">
                                                <b>Agent: </b>
                                                {profile.firstName}&nbsp;
                                              </p>
                                            )}
                                          </span>
                                        )}

                                        {editingField === "lastName" ? (
                                          <input
                                            type="text"
                                            value={editedText} // Controlled input with edited text
                                            onChange={(e) =>
                                              setEditedText(e.target.value)
                                            } // Update text as the user types
                                            onBlur={() =>
                                              handleTextSaveListAgent(
                                                "lastName"
                                              )
                                            } // Save the text when the user leaves the input field
                                            autoFocus
                                          />
                                        ) : (
                                          <span
                                            style={{ fontSize: "12px" }}
                                            onClick={() =>
                                              handleTextListAgent(
                                                "lastName",
                                                profile.lastName
                                              )
                                            }
                                          >
                                            {profile && (
                                              <p className="mb-0 text-white">
                                                {" "}
                                                {profile.lastName}&nbsp;
                                              </p>
                                            )}
                                          </span>
                                        )}
                                      </div>

                                      {editingField === "listOffice" ? (
                                        <input
                                          type="text"
                                          value={editedText} // Controlled input with edited text
                                          onChange={(e) =>
                                            setEditedText(e.target.value)
                                          } // Update text as the user types
                                          onBlur={() =>
                                            handleTextSaveListOffice(
                                              "listOffice"
                                            )
                                          } // Save the text when the user leaves the input field
                                          autoFocus
                                        />
                                      ) : (
                                        <span
                                          style={{ fontSize: "12px" }}
                                          onClick={() =>
                                            handleTextListOffice(
                                              "listOffice",
                                              profile.active_office
                                            )
                                          }
                                        >
                                          {profile && (
                                            <p className="mb-0 text-white">
                                              <b>Office: </b>
                                              {profile.active_office}
                                            </p>
                                          )}
                                        </span>
                                      )}

                                      {editingField === "officePhone" ? (
                                        <input
                                          type="text"
                                          value={editedText} // Controlled input with edited text
                                          onChange={(e) =>
                                            setEditedText(e.target.value)
                                          } // Update text as the user types
                                          onBlur={() =>
                                            handleTextSaveOfficePhone(
                                              "officePhone"
                                            )
                                          } // Save the text when the user leaves the input field
                                          autoFocus
                                        />
                                      ) : (
                                        <span
                                          style={{ fontSize: "12px" }}
                                          onClick={() =>
                                            handleTextOfficePhone(
                                              "officePhone",
                                              profile.phone
                                            )
                                          }
                                        >
                                          {profile && (
                                            <p className="mb-0 text-white">
                                              <b>Phone: </b>
                                              {profile.phone}
                                            </p>
                                          )}
                                        </span>
                                      )}
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                          )}

                          {/* Step 2 */}
                          {nextStep === "2" && (
                            <>
                              <div className="row">
                                <div className="col-md-12 singleListing pb-3">
                                  <div className="card-img-top">
                                    <img
                                      className="img-fluid"
                                      style={{
                                        minHeight: "400px",
                                        borderRadius: "0px",
                                      }}
                                      src={
                                        select?.media[1]?.MediaURL ||
                                        defaultpropertyimage
                                      }
                                      alt="Property"
                                      onClick={() => handleImageClick(1)}
                                    />
                                    {selectedImage === 1 && (
                                      <label className="imagePostCardSecond">
                                        <input
                                          id="REPC_real_estate_purchase_contract"
                                          type="file"
                                          onChange={(e) =>
                                            handleImageUpload(e, 1)
                                          }
                                        />
                                        <i className="h2 fi-edit opacity-80"></i>
                                      </label>
                                    )}
                                  </div>
                                </div>

                                <div className="col-md-8 pb-3">
                                  {/* <div className="card-img-top"> */}
                                  <img
                                    className="w-100"
                                    style={{
                                      borderRadius: "0px",
                                    }}
                                    src={
                                      select?.media[2]?.MediaURL ||
                                      defaultpropertyimage
                                    }
                                    alt="Property"
                                    onClick={() => handleImageClick(2)}
                                  />
                                  {selectedImage === 2 && (
                                    <label className="imagePostCardThird">
                                      <input
                                        id="REPC_real_estate_purchase_contract"
                                        type="file"
                                        onChange={(e) =>
                                          handleImageUpload(e, 2)
                                        }
                                      />
                                      <i className="h2 fi-edit opacity-80"></i>
                                    </label>
                                  )}
                                  {/* </div> */}
                                </div>
                                <div className="col-md-4 pb-3">
                                  {/* <div className="card-img-top"> */}
                                  <img
                                    className="w-100"
                                    style={{
                                      borderRadius: "0px",
                                      objectFit: "cover",
                                      height: "100%",
                                    }}
                                    src={
                                      select?.media[3]?.MediaURL ||
                                      defaultpropertyimage
                                    }
                                    alt="Property"
                                    onClick={() => handleImageClick(3)}
                                  />
                                  {selectedImage === 3 && (
                                    <label className="imagePostCardFour">
                                      <input
                                        id="REPC_real_estate_purchase_contract"
                                        type="file"
                                        onChange={(e) =>
                                          handleImageUpload(e, 3)
                                        }
                                      />
                                      <i className="h2 fi-edit opacity-80"></i>
                                    </label>
                                  )}
                                  {/* </div> */}
                                </div>
                              </div>
                              <div className="postCards_description">
                                <div className="row">
                                  <div className="col-md-8">
                                    {editingField === "publicRemarks" ? (
                                      <textarea
                                        className="form-control mt-0 w-100"
                                        id="textarea-input"
                                        rows="5"
                                        type="text"
                                        value={editedText} // Controlled input with edited text
                                        onChange={(e) => {
                                          const wordCount = e.target.value
                                            .trim()
                                            .split(/\s+/).length;
                                          if (wordCount <= 150) {
                                            setEditedText(e.target.value); // Update text only if word count is <= 150
                                          }
                                        }} // Update text as the user types
                                        onBlur={() =>
                                          handleTextSavePublicRemarks(
                                            "publicRemarks"
                                          )
                                        } // Save the text when the user leaves the input field
                                        autoFocus
                                      />
                                    ) : (
                                      <p
                                        className="description_PrintPage"
                                        style={{ fontSize: "12px" }}
                                        onClick={() =>
                                          handleTextPublicRemarks(
                                            "publicRemarks",
                                            select?.value[0]?.PublicRemarks ||
                                              "testing"
                                          )
                                        }
                                      >
                                        {select.value[0]?.PublicRemarks}
                                      </p>
                                    )}
                                  </div>
                                  <div className="col-md-4 pull-right text-end">
                                    <div className="imageLogo w-100  float-start w-100">
                                      <div>
                                        <img
                                          className="mb-2 rounded-0"
                                          src={uploadedLogo || Logo}
                                          style={{
                                            width: "100px",
                                            cursor: "pointer",
                                          }}
                                          onClick={handleLogoClick}
                                        />
                                        {showLogoInput && (
                                          <label className="documentPostCardsSecond">
                                            <input
                                              id="REPC_real_estate_purchase_contract"
                                              type="file"
                                              onChange={handleLogoUpload}
                                            />
                                            <i class="h2 fi-edit opacity-80"></i>
                                          </label>
                                        )}
                                      </div>

                                      {editingField === "published" ? (
                                        <input
                                          className="w-100"
                                          type="text"
                                          value={editedText}
                                          onChange={(e) =>
                                            setEditedText(e.target.value)
                                          }
                                          onBlur={() =>
                                            handleTextSavePublished("published")
                                          }
                                          autoFocus
                                        />
                                      ) : (
                                        <p
                                          className="mb-0"
                                          style={{ fontSize: "12px" }}
                                          onClick={() =>
                                            handleTextPublished(
                                              "published",
                                              select.value[0]?.SourceSystemName
                                            )
                                          }
                                        >
                                          {select.value[0]?.SourceSystemName ||
                                            "UtahRealEstate.com"}
                                        </p>
                                      )}

                                      {editingField === "address" ? (
                                        <input
                                          className="w-100"
                                          type="text"
                                          value={editedText} // Controlled input with edited text
                                          onChange={(e) =>
                                            setEditedText(e.target.value)
                                          } // Update text as the user types
                                          onBlur={() =>
                                            handleTextSave("address")
                                          }
                                          autoFocus
                                        />
                                      ) : (
                                        <span
                                          className="mb-0"
                                          style={{ fontSize: "12px" }}
                                          onClick={() =>
                                            handleTextClick(
                                              "address",
                                              select?.value[0]?.UnparsedAddress
                                            )
                                          }
                                        >
                                          {select?.value[0]?.UnparsedAddress ||
                                            "2024 Demo St"}
                                        </span>
                                      )}

                                      <br />

                                      {editingField === "companyCity" ? (
                                        <input
                                          className="w-100"
                                          type="text"
                                          value={editedText} // Controlled input with edited text
                                          onChange={(e) =>
                                            setEditedText(e.target.value)
                                          } // Update text as the user types
                                          onBlur={() =>
                                            handleTextSaveCompanyCity(
                                              "companyCity"
                                            )
                                          }
                                          autoFocus
                                        />
                                      ) : (
                                        <span
                                          className="mb-0"
                                          style={{ fontSize: "12px" }}
                                          onClick={() =>
                                            handleTextCompanyCity(
                                              "companyCity",
                                              select?.value[0]?.City
                                            )
                                          }
                                        >
                                          {select?.value[0]?.City ||
                                            "American Fork"}
                                          ,
                                        </span>
                                      )}

                                      {editingField === "companyState" ? (
                                        <input
                                          type="text"
                                          value={editedText} // Controlled input with edited text
                                          onChange={(e) =>
                                            setEditedText(e.target.value)
                                          } // Update text as the user types
                                          onBlur={() =>
                                            handleTextSaveCompanyState(
                                              "companyState"
                                            )
                                          }
                                          autoFocus
                                        />
                                      ) : (
                                        <span
                                          className="mb-0"
                                          style={{ fontSize: "12px" }}
                                          onClick={() =>
                                            handleTextCompanyState(
                                              "companyState",
                                              select?.value[0]?.StateOrProvince
                                            )
                                          }
                                        >
                                          {select?.value[0]?.StateOrProvince ||
                                            "UT"}
                                          ,
                                        </span>
                                      )}

                                      {editingField === "companyPostCode" ? (
                                        <input
                                          className="w-100"
                                          type="text"
                                          value={editedText} // Controlled input with edited text
                                          onChange={(e) =>
                                            setEditedText(e.target.value)
                                          } // Update text as the user types
                                          onBlur={() =>
                                            handleTextSaveCompanyPostCode(
                                              "companyPostCode"
                                            )
                                          }
                                          autoFocus
                                        />
                                      ) : (
                                        <span
                                          className="mb-0"
                                          style={{ fontSize: "12px" }}
                                          onClick={() =>
                                            handleTextCompanyPostCode(
                                              "companyPostCode",
                                              select?.value[0]?.PostalCode
                                            )
                                          }
                                        >
                                          {select?.value[0]?.PostalCode ||
                                            "84003"}
                                        </span>
                                      )}
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </>
                          )}
                          {/* Add more steps here if needed */}
                        </div>

                        <div className="template_second">
                          <button
                            className="btn btn-secondary pull-right  ms-3"
                            onClick={() => handleNext("2")}
                          >
                            Inside Left Page
                          </button>
                          <button
                            className="btn btn-secondary pull-right  ms-3"
                            onClick={() => handleNext("1")}
                          >
                            Front Page
                          </button>

                          <div className="ms-5">
                            <PDFDownloadLink
                              document={
                                <Flyer
                                  select={select}
                                  platformData={uploadedLogo}
                                  profile={profile}
                                />
                              }
                              fileName={select.address || "Flyer.pdf"}
                            >
                              Download Pdf
                            </PDFDownloadLink>
                          </div>
                        </div>
                      </div>
                    </section>
                  )}
                </div>
              ) : (
                ""
              )}
            </div>
          ) : (
            <div className="col-md-8">
              <h3 className="text-white mb-4">Design your Post Cards.</h3>
              <div className="bg-light float-start w-100 rounded-3 mb-2 p-3">
                <p
                  id=""
                  dangerouslySetInnerHTML={{
                    __html: platformData?.flyerpagecontent,
                  }}
                />
                <hr className="mb-3" />
                <div className="mb-5">
                  <div className="select_template float-start w-100 mt-3 mb-5">
                    <label className="col-form-label" id="radio-level1">
                      Select Template:
                    </label>
                    <div className="d-flex">
                      <div className="form-check mb-0">
                        {/* Image 1 */}
                        <img
                          className={templateType === "type1" ? "active" : ""}
                          src={TypeOne}
                          alt="Type One"
                          style={{ cursor: "pointer" }}
                          onClick={() => handleType("type1")}
                        />
                      </div>
                    </div>
                  </div>

                  {templateType ? (
                    <div className="float-start w-100 mt-3">
                      <label className="col-form-label ">
                        Select Membership{" "}
                      </label>
                      <select
                        className="form-select mb-2"
                        name="membership"
                        onChange={(event) => setMembership(event.target.value)}
                        value={membership}
                      >
                        <option value="">Please Select Membership</option>
                        {commaSeparatedValuesmls_membership &&
                        commaSeparatedValuesmls_membership.length > 0
                          ? commaSeparatedValuesmls_membership.map(
                              (memberahip) => <option>{memberahip}</option>
                            )
                          : ""}
                      </select>
                      {membership ? (
                        <>
                          <label className="col-form-label">
                            Enter MLS Number:
                          </label>
                          {/* <h6 className="mt-2 mb-2"></h6> */}
                          <div className="col-md-12">
                            <input
                              className="form-control w-100"
                              id="text-input-onee"
                              type="text"
                              name="mlsnumber"
                              placeholder="Enter MLS number"
                              onChange={handleChangesearchmls}
                              value={searchmlsnumber.mlsnumber}
                            />
                          </div>

                          <div className="pull-left mt-4">
                            <button
                              className="btn btn-primary btn-sm order-lg-3"
                              onClick={MLSSearch}
                              disabled={isLoading}
                            >
                              {isLoading ? "Please wait" : "Search Print"}
                            </button>
                          </div>
                        </>
                      ) : (
                        ""
                      )}
                    </div>
                  ) : (
                    ""
                  )}
                </div>
                {/* <div className="col-md-4">
                    <button
                      className="btn btn-primary mt-4"
                      onClick={handleNonListed}
                    >
                      Non-Listed Property
                    </button>
                  </div> */}
              </div>
            </div>
          )}
        </div>
      </main>
    </div>
  );
};

export default PostCards;

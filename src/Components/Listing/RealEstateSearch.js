import React, { useState, useMemo, useEffect } from "react";
import countryList from "country-list";
import { NavLink, useNavigate, useParams } from "react-router-dom";
import user_service from "../service/user_service";
import jwt from "jwt-decode";
import _ from "lodash";
import Loader from "../../Pages/Loader/Loader.js";
import Toaster from "../../Pages/Toaster/Toaster.js";
import axios from "axios";
import moment from "moment-timezone";

const RealEstateSearch = () => {
  const navigate = useNavigate();
  const initialValues = {
    listingsAgent: "",
    category: "",
    unitNumber: "",
    propertyType: "",
    mlsNumber: "",
    agentId: "",
    streetDirection: "North",
    associateName: "",
    price: "",
    garage: "",
    bed: "",
    subDivision: "",
    streetAddress: "",
    city: "",
    state: "Utah",
    zipCode: "",
    schoolDistrict: "",
    country: "",
    areaLocation: "",
    status: "",
    listDate: "",
    image: "",
  };

  const [formValues, setFormValues] = useState(initialValues);

 
 
  const [membership, setMembership] = useState("");

  const [result, setResult] = useState([]);
  
  const [loader, setLoader] = useState({ isActive: null });
  const { isActive } = loader;
  const [toaster, setToaster] = useState({
    types: null,
    isShow: null,
    toasterBody: "",
    message: "",
  });
  const { types, isShow, toasterBody, message } = toaster;

  const [searchmlsnumber, setSearchmlsnumber] = useState({ mlsNumber: "" });
 
  const [select, setSelect] = useState("");
 
  const [isLoading, setIsLoading] = useState(false);
  

  const params = useParams();

  const options = useMemo(() => {
    const countries = countryList.getData();
    const utahState = {
      code: "UT",
      name: "Utah",
      districts: [
        "Alabama",
        "Alaska",
        "Arizona",
        "Arkansas",
        "California",
        "Colorado",
        "Connecticut",
        "Delaware",
        "District of Columbia",
        "Florida",
        "Georgia",
        "Hawaii",
        "Idaho",
        "Illinois",
        "Indiana",
        "Iowa",
        "Kansas",
        "Kentucky",
        "Louisiana",
        "Maine",
        "Maryland",
        "Massachusetts",
        "Michigan",
        "Minnesota",
        "Mississippi",
        "Missouri",
        "Montana",
        "Nebraska",
        "Nevada",
        "New Hampshire",
        "New Jersey",
        "New Mexico",
        "New York",
        "North Carolina",
        "North Dakota",
        "Ohio",
        "Oklahoma",
        "Oregon",
        "Pennsylvania",
        "Rhode Island",
        "South Carolina",
        "South Dakota",
        "Tennessee",
        "Texas",
        "Utah",
        "Vermont",
        "Virginia",
        "Washington",
        "West Virginia",
        "Wisconsin",
        "Wyoming",
      ],
    };
    return [...countries, utahState];
  }, []);



  useEffect(() => {
    window.scrollTo(0, 0);
    ContactGetById(jwt(localStorage.getItem("auth")).id);
  }, []);

  const handleChangesearchmls = (event) => {
    setSearchmlsnumber({ mlsnumber: event.target.value });
  };




  const MLSSearch = async () => {
    if (searchmlsnumber.mlsnumber) {
      try {
        setIsLoading(true);
        let response;
        if (membership === "UtahRealEstate.com") {
          console.log(membership);

          response = await user_service.listingSearchMLS(
            searchmlsnumber.mlsnumber
          );
        } else if (membership === "Park City") {
          response = await user_service.listingSearchMLSspark(
            searchmlsnumber.mlsnumber,
            "parkcity"
          );
        } else if (membership === "Washington County") {
          response = await user_service.listingSearchMLSspark(
            searchmlsnumber.mlsnumber,
            "washingtoncounty"
          );
        } else if (membership === "Iron County") {
          response = await user_service.listingSearchMLSspark(
            searchmlsnumber.mlsnumber,
            "ironcountry"
          );
        } else {
          response = await user_service.listingSearchMLS(
            searchmlsnumber.mlsnumber
          );
        }

        if (response) {
          setResult(response.data);
        }

        setIsLoading(false);
      } catch (error) {
        console.error(error);
        setIsLoading(false);
      }
    }
  };

  const handleSearchListing = async (result) => {
    setSelect(result);
  };

  const [getContact, setGetContact] = useState(initialValues);
  const [mls_membership, setMls_membership] = useState([]);
  const [
    commaSeparatedValuesmls_membership,
    setCommaSeparatedValuesmls_membership,
  ] = useState("");

  const ContactGetById = async () => {
    await user_service
      .contactGetById(jwt(localStorage.getItem("auth")).id)
      .then((response) => {
        if (response) {
          setGetContact(response.data);

          if (response.data && response.data.additionalActivateFields) {
            const mls_membershipString =
              response.data.additionalActivateFields?.mls_membership;
            if (mls_membershipString) {
              // Check if mls_membershipString is a string
              if (typeof mls_membershipString === "string") {
                try {
                  // Attempt to parse the string as JSON
                  const mls_membershipParsed = JSON.parse(mls_membershipString);
                  // Set the parsed value to your state or variable
                  if (mls_membershipParsed == "") {
                    setMls_membership(mls_membership);
                  } else {
                    setMls_membership(mls_membershipParsed);
                  }
                  if (mls_membershipParsed == "") {
                    setCommaSeparatedValuesmls_membership("");
                  } else {
                    const boardMembershipValues = mls_membershipParsed.map(
                      (item) => item.value
                    );
                    //   console.log("boardMembershipValues")
                    //   console.log(boardMembershipValues)
                    setCommaSeparatedValuesmls_membership(
                      // boardMembershipValues.join(", ")
                      boardMembershipValues
                    );
                  }
                } catch (error) {
                  // Handle the error if parsing fails
                  console.error("Error parsing mls_membership as JSON:", error);
                  // You might want to set a default value or handle the error in another way
                  // For debugging, you can log the JSON parse error message:
                  console.error("JSON Parse Error:", error.message);
                  // Set a default value if parsing fails
                  setCommaSeparatedValuesmls_membership(""); // Replace "defaultValue" with your default value
                }
              } else {
                //console.log("sdfdsfdsfsd");
                // Handle the case where mls_membershipString is not a string (e.g., it's already an object)
                // You can directly set it to your state or variable
                setCommaSeparatedValuesmls_membership("");
              }
            } else {
              // Handle the case where mls_membershipString is undefined or null
              // Set a default value or handle it as needed
              setCommaSeparatedValuesmls_membership(""); // Replace "defaultValue" with your default value
            }
          }
        }
      });
  };

  return (
    <div className="bg-secondary float-left w-100 pt-4 mb-4">
      <Loader isActive={isActive} />

      {isShow && (
        <Toaster
          types={types}
          isShow={isShow}
          toasterBody={toasterBody}
          message={message}
        />
      )}
      <main className="page-wrapper">
        {/* <!-- Page content--> */}
        {/* <!-- Page container--> */}
        <div className="row">
          {/* <AllTab /> */}
          <div className="col-md-10">
            <div className="">
              <div className="bg-light float-start w-100 rounded-3 mb-2 p-3">
                <div className="">
                  <>
                    <h6 className="">Search By Mls Number</h6>
                    <label className="col-form-label">Select Membership </label>
                    <select
                      className="form-select mb-2"
                      name="membership"
                      onChange={(event) => setMembership(event.target.value)}
                      value={membership}
                    >
                      <option value="">Please Select Membership</option>
                      {commaSeparatedValuesmls_membership &&
                      commaSeparatedValuesmls_membership.length > 0
                        ? commaSeparatedValuesmls_membership.map(
                            (memberahip) => <option>{memberahip}</option>
                          )
                        : ""}
                    </select>
                    {membership ? (
                      <>
                        <h6 className="mt-2 mb-2">Enter a MLS Number:</h6>

                        <div className="col-md-12 mt-2">
                          <input
                            className="form-control w-100"
                            id="text-input-onee"
                            type="text"
                            name="mlsnumber"
                            placeholder="Enter MLS number"
                            onChange={handleChangesearchmls}
                            value={searchmlsnumber.mlsnumber}
                          />
                        </div>
                        <div className="pull-right mt-4">
                          <a
                            className="btn btn-primary btn-sm order-lg-3"
                            onClick={MLSSearch}
                            disabled={isLoading}
                          >
                            {isLoading ? "Please wait" : "Search"}
                          </a>
                        </div>
                      </>
                    ) : (
                      ""
                    )}

                    {result && result.value && result.value.length > 0 && (
                      <div className="card card-hover card-horizontal transactions border-0 shadow-sm mb-4 p-3 mt-3 float-start w-100">
                        <div className="media_images">
                          {result?.media[0]?.MediaURL && (
                            <div className="card-img-top">
                              <img
                                className="img-fluid"
                                src={result.media[0].MediaURL}
                              />
                            </div>
                          )}

                          {/* <div className="property_thumb">
                            {result.media.map((mediaItem, mediaIndex) => (
                              <img
                                key={mediaIndex}
                                className="media-image"
                                src={mediaItem.MediaURL}
                                onClick={() =>
                                  handleImageUrl(mediaItem.MediaURL)
                                }
                              />
                            ))}
                          </div> */}
                        </div>

                        <div className="card-body position-relative pb-3">
                          <span className="mb-2">
                            {result.value[0]?.CountyOrParish}_
                            {result.value[0]?.ListingId}
                          </span>
                          <br />
                          <span className="mb-2">
                            {result.value[0]?.UnparsedAddress}
                          </span>
                          <br />
                          <span className="mb-2">
                            {result.value[0].City},{" "}
                            {result.value[0].StateOrProvince}{" "}
                            {result.value[0]?.PostalCode}
                          </span>
                          <br />
                          <span className="mb-2">
                            {`${result?.value[0]?.ListAgentFullName} / ${
                              result?.value[0]?.ListOfficeName ?? ""
                            }`}
                          </span>
                        </div>

                        <div className="float-left text-left">
                          <button
                            className="btn btn-primary me-0"
                            id="selectButton"
                            type="button"
                            data-toggle="modal"
                            data-target="#mls_listing"
                            onClick={(e) => handleSearchListing(result)}
                          >
                            Select
                          </button>

                          <p className="my-2 badge bg-success">
                            {result.value[0]?.ListPrice}
                          </p>
                          <br />
                          <h6 className="mt-0 pull-left">
                            {result.value[0]?.MlsStatus}
                          </h6>
                        </div>
                      </div>
                    )}
                  </>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="modal in" role="dialog" id="mls_listing">
          <div
            className="modal-dialog modal-lg modal-dialog-scrollable"
            role="document"
          >
            <div className="modal-content">
              <div className="modal-header">
                <h4 className="modal-title">Mls Single Listing</h4>
                <button
                  className="btn-close"
                  id="closeModalAttach"
                  type="button"
                  data-dismiss="modal"
                  // onClick={handleBack}
                  aria-label="Close"
                ></button>
              </div>

              <div className="modal-body fs-sm">
                {select && select.value && select.value.length > 0 && (
                  <>
                    <section className="mb-2">
                      <h3 className="mb-0">
                        {" "}
                        {select?.value[0]?.UnparsedAddress ?? ""}
                      </h3>
                      <p className="mb-2 pb-1 fs-lg">
                        {" "}
                        {select?.value[0].City},{" "}
                        {select.value[0].StateOrProvince}{" "}
                        {select.value[0]?.PostalCode}
                      </p>
                    </section>

                    <section className="mb-0 pb-3" data-simplebar>
                      <div
                        className="row g-2 g-md-3 gallery"
                        data-thumbnails="true"
                      >
                        <div className="col-md-8 singleListing">
                          <div className="bg-light border rounded-3 p-3">
                            {select?.media[0]?.MediaURL && (
                              <div className="card-img-top">
                                <img
                                  className="img-fluid"
                                  src={select.media[0].MediaURL}
                                  alt="Property"
                                />
                              </div>
                            )}
                            <div className="col-md-12 imageProperty">
                              {select.media.map((mediaItem, mediaIndex) => (
                                <img
                                  key={mediaIndex}
                                  className="media-image"
                                  src={mediaItem.MediaURL}
                                />
                              ))}
                            </div>

                            <section className="">
                              <div className="">
                                <div className="col-md-12 mb-md-0 mt-3">
                                  <h2 className="h3 border-bottom">
                                    {select.value[0]?.ListPrice}
                                    <span className="d-inline-block ms-1 fs-base fw-normal text-body">
                                      USD
                                    </span>
                                  </h2>
                                  <h3 className="h4">Property Details</h3>
                                  <div className="col-md-12 pb-md-3 d-flex">
                                    <ul className="list-unstyled col-md-6">
                                      {select.value[0]?.PropertyType ? (
                                        <li>
                                          <b>Category: </b>
                                          {select.value[0]?.PropertyType}
                                        </li>
                                      ) : (
                                        ""
                                      )}

                                      {select.value[0]?.ListAgentFullName ? (
                                        <li>
                                          <b>ListingsAgent: </b>
                                          {select.value[0]?.ListAgentFullName}
                                        </li>
                                      ) : (
                                        ""
                                      )}

                                      {select.value[0]?.ListAgentFullName ? (
                                        <li>
                                          <b>Listing Agent ID:: </b>
                                          {select.value[0]?.ListAgentKey}
                                        </li>
                                      ) : (
                                        ""
                                      )}

                                      {select.value[0]?.MLSAreaMajor ? (
                                        <li>
                                          <b>Area Location: </b>
                                          {select.value[0]?.MLSAreaMajor}
                                        </li>
                                      ) : (
                                        ""
                                      )}

                                      {select.value[0]?.CountyOrParish ? (
                                        <li>
                                          <b>County: </b>
                                          {select.value[0]?.CountyOrParish}
                                        </li>
                                      ) : (
                                        ""
                                      )}

                                      {select.value[0]?.SubdivisionName ? (
                                        <li>
                                          <b>Subdivision: </b>
                                          {select.value[0]?.SubdivisionName}
                                        </li>
                                      ) : (
                                        ""
                                      )}

                                      {select.value[0]
                                        ?.OriginalEntryTimestamp ? (
                                        <li>
                                          <b>List Date: </b>
                                          {select.value[0]
                                            ?.OriginalEntryTimestamp
                                            ? moment
                                                .tz(
                                                  select.value[0]
                                                    ?.OriginalEntryTimestamp,
                                                  "US/Mountain"
                                                )
                                                .format("M/D/YYYY")
                                            : ""}
                                        </li>
                                      ) : (
                                        ""
                                      )}
                                    </ul>

                                    <ul className="list-unstyled col-md-6">
                                      {select.value[0]?.ListingId ? (
                                        <li>
                                          <b>MLS Number: </b>#
                                          {select.value[0]?.ListingId}
                                        </li>
                                      ) : (
                                        ""
                                      )}

                                      {select.value[0]?.AssociationName ? (
                                        <li>
                                          <b>AssociateName: </b>
                                          {select.value[0]?.AssociationName}
                                        </li>
                                      ) : (
                                        ""
                                      )}

                                      {select.value[0]?.ListOfficeKey ? (
                                        <li>
                                          <b>MLS Office ID: </b>
                                          {select.value[0]?.ListOfficeKey}
                                        </li>
                                      ) : (
                                        ""
                                      )}

                                      {select.value[0]?.MlsStatus ? (
                                        <li>
                                          <b>Status: </b>
                                          {select.value[0]?.MlsStatus}
                                        </li>
                                      ) : (
                                        ""
                                      )}

                                      {select.value[0]?.BedroomsTotal ? (
                                        <li className="me-3 pe-3 border-end">
                                          <b className="me-1">Bedrooms:</b>
                                          {select.value[0]?.BedroomsTotal}
                                        </li>
                                      ) : (
                                        ""
                                      )}

                                      {select.value[0]?.BathroomsFull ? (
                                        <li className="me-3 pe-3 border-end">
                                          <b className="me-1">
                                            Bathrooms:{" "}
                                            {select.value[0]?.BathroomsFull ??
                                              0}
                                            /{" "}
                                            {select.value[0]?.BathroomsHalf ??
                                              0}
                                          </b>
                                        </li>
                                      ) : (
                                        ""
                                      )}

                                      {select.value[0]?.GarageSpaces ? (
                                        <li className="me-3 pe-3 border-end">
                                          <b className="me-1">
                                            Garage:{" "}
                                            {select.value[0]?.GarageSpaces}
                                          </b>
                                        </li>
                                      ) : (
                                        ""
                                      )}

                                      {select.value[0]?.BuildingAreaTotal ? (
                                        <li className="me-3 pe-3 border-end">
                                          <b className="me-1">
                                            {" "}
                                            Square Ft:{" "}
                                            {select.value[0]?.BuildingAreaTotal}
                                          </b>
                                        </li>
                                      ) : (
                                        ""
                                      )}

                                      {select.value[0]?.LotSizeAcres ? (
                                        <li className="me-3 pe-3 border-end">
                                          <b className="me-1">
                                            Lot Size:{" "}
                                            {select.value[0]?.LotSizeAcres}
                                            Acres
                                          </b>
                                        </li>
                                      ) : (
                                        ""
                                      )}
                                    </ul>
                                  </div>

                                  {select.value[0]?.PublicRemarks ? (
                                    <div className="mb-4 pb-md-3">
                                      <h3 className="h4">Overview</h3>
                                      <p className="mb-1">
                                        {select.value[0]?.PublicRemarks}
                                      </p>
                                    </div>
                                  ) : (
                                    ""
                                  )}

                                  <div className="pb-lg-2 py-4 border-top">
                                    <ul className="d-flex list-unstyled fs-sm">
                                      {select.value[0]?.SourceSystemName ? (
                                        <li className="me-3 pe-3 border-end">
                                          Published by:{" "}
                                          <b>
                                            {select.value[0]?.SourceSystemName}
                                          </b>
                                        </li>
                                      ) : (
                                        ""
                                      )}

                                      {select.value[0]?.OnMarketDate ? (
                                        <li className="me-3 pe-3 border-end">
                                          Published date:
                                          <b>
                                            {select.value[0]?.OnMarketDate
                                              ? moment
                                                  .tz(
                                                    select.value[0]
                                                      ?.OnMarketDate,
                                                    "US/Mountain"
                                                  )
                                                  .format("M/D/YYYY")
                                              : ""}
                                          </b>
                                        </li>
                                      ) : (
                                        ""
                                      )}
                                    </ul>
                                  </div>
                                </div>
                              </div>
                            </section>
                          </div>
                        </div>
                        <aside className="col-lg-4 col-md-5 ms-lg-auto pb-1">
                          <div className="card shadow-sm">
                            <div className="card-body">
                              <div className="d-flex align-items-start justify-content-between">
                                <a
                                  className="text-decoration-none"
                                  href="real-estate-vendor-properties.html"
                                >
                                  <h5 className="mb-1">
                                    {select.value[0]?.ListAgentFullName}
                                  </h5>

                                  <p className="text-body">
                                    {select.value[0]?.ListOfficeName}
                                  </p>
                                </a>
                              </div>
                              <ul className="list-unstyled border-bottom mb-4 pb-4 p-0">
                                <li>
                                  <a
                                    className="nav-link fw-normal p-0"
                                    href="tel:3025550107"
                                  >
                                    <i className="fi-phone mt-n1 me-2 align-middle opacity-60"></i>
                                    {select.value[0]?.ListAgentOfficePhone}
                                  </a>
                                </li>
                              </ul>
                            </div>
                          </div>
                        </aside>
                      </div>
                    </section>
                  </>
                )}
              </div>
            </div>
          </div>
        </div>
      </main>
    </div>
  );
};

export default RealEstateSearch;

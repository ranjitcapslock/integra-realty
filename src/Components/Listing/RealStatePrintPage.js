import React, { useState, useRef, useEffect } from "react";
import { NavLink, useNavigate, useParams } from "react-router-dom";
import user_service from "../service/user_service.js";
import jwt from "jwt-decode";
import _ from "lodash";
import Loader from "../../Pages/Loader/Loader.js";
import Toaster from "../../Pages/Toaster/Toaster.js";
import defaultpropertyimage from "../../Components/img/defaultpropertyimage.jpeg";
import NonFlyer1 from "../img/NonFlyer1.png";
import NonFlyer2 from "../img/NonFlyer2.png";
import NonFlyer3 from "../img/NonFlyer3.png";
import Logo from "../img/logo.png";
import {
  Document,
  Page,
  Text,
  Image,
  StyleSheet,
  PDFDownloadLink,
  View,
} from "@react-pdf/renderer";

import { Font } from "@react-pdf/renderer";

Font.register({
  family: "Times-Roman",
});

const RealStatePrintPage = () => {
  const [loader, setLoader] = useState({ isActive: null });
  const { isActive } = loader;
  const [toaster, setToaster] = useState({
    types: null,
    isShow: null,
    toasterBody: "",
    message: "",
  });
  const { types, isShow, toasterBody, message } = toaster;

  const [searchmlsnumber, setSearchmlsnumber] = useState({ mlsNumber: "" });
  const componentRef = useRef();

  useEffect(() => {
    window.scrollTo(0, 0);
    ContactGetById(jwt(localStorage.getItem("auth")).id);
  }, []);

  const [templateType, setTemplateType] = useState("1");
  const [template, setTemplate] = useState(false);

  const [isLoading, setIsLoading] = useState(false);

  const handleType = (input) => {
    const templateType = input.target ? input.target.value : input;
    setTemplateType(templateType);
    setTemplate(true);
  };

  const [getContact, setGetContact] = useState("");

  const ContactGetById = async () => {
    await user_service
      .contactGetById(jwt(localStorage.getItem("auth")).id)
      .then((response) => {
        if (response) {
          setGetContact(response.data);
        }
      });
  };

  const [selectedImage, setSelectedImage] = useState(null);
  const [uploadedImages, setUploadedImages] = useState([]);

  // Handle image click for uploading
  const handleImageClick = (imageIndex) => {
    setSelectedImage(imageIndex);
  };

  const handleImageUpload = (e, imageIndex = 0) => {
    const file = e.target.files[0];
    if (file) {
      const reader = new FileReader();
      reader.onload = (upload) => {
        setSelect((prevSelect) => ({
          ...prevSelect,
          images: prevSelect.images.map((image, index) =>
            index === imageIndex
              ? { ...image, src: upload.target.result }
              : image
          ),
        }));
      };
      reader.readAsDataURL(file);
    }
  };

  // const handleImageUpload = (e, imageIndex = 0) => {
  //   const file = e.target.files[0];
  //   if (file) {
  //     const reader = new FileReader();
  //     reader.onload = (upload) => {
  //       setSelect((prev) => {
  //         const newImages = [...prev];
  //         newImages[imageIndex] = upload.target.result;
  //         return newImages;
  //       });
  //     };
  //     reader.readAsDataURL(file);
  //   }
  //   setSelectedImage(null);
  // };

  const imageDescriptions = [
    {
      src: uploadedImages[0] || defaultpropertyimage,
    },
    {
      src: uploadedImages[1] || defaultpropertyimage,
      description: "Enter a brief description for Image 1",
    },
    {
      src: uploadedImages[2] || defaultpropertyimage,
      description: "Enter a brief description for Image 2",
    },
    {
      src: uploadedImages[3] || defaultpropertyimage,
      description: "Enter a brief description for Image 3",
    },
    {
      src: uploadedImages[4] || defaultpropertyimage,
      description: "Enter a brief description for Image 4",
    },
    {
      src: uploadedImages[5] || defaultpropertyimage,
      description: "Enter a brief description for Image 5",
    },
    {
      src: uploadedImages[6] || defaultpropertyimage,
      description: "Enter a brief description for Image 6",
    },

    {
      src: uploadedImages[7] || defaultpropertyimage,
      description: "Enter a brief description for Image 7",
    },

    {
      src: uploadedImages[8] || defaultpropertyimage,
      description: "Enter a brief description for Image 8",
    },
  ];

  const [select, setSelect] = useState({
    price: "X00,00",
    address: "2024 Demo St",
    bed: "0",
    bath: "0",
    sqft: "X,000 Sq.Ft.",
    lotSize: "0",
    yearBuilt: "2024",
    publicRemarks: "Designer Charm",
    published: "UtahRealEstate.com",
    companyCity: "American Fork",
    companyState: "UT",
    companyPostCode: "84003",
    exterior:"Traditional House",
    roof:"Tile",
    cooling:"Central AC",
    heating:"Gas",
    garage:"X car",

    images: [
      {
        src: uploadedImages[0] || defaultpropertyimage,
        description: "Enter a brief description for Image",
      },
      {
        src: uploadedImages[1] || defaultpropertyimage,
        description: "Enter a brief description for Image 1",
      },
      {
        src: uploadedImages[2] || defaultpropertyimage,
        description: "Enter a brief description for Image 2",
      },
      {
        src: uploadedImages[3] || defaultpropertyimage,
        description: "Enter a brief description for Image 3",
      },
      {
        src: uploadedImages[4] || defaultpropertyimage,
        description: "Enter a brief description for Image 4",
      },
      {
        src: uploadedImages[5] || defaultpropertyimage,
        description: "Enter a brief description for Image 5",
      },
      {
        src: uploadedImages[6] || defaultpropertyimage,
        description: "Enter a brief description for Image 6",
      },
      {
        src: uploadedImages[7] || defaultpropertyimage,
        description: "Enter a brief description for Image 7",
      },

      {
        src: uploadedImages[8] || defaultpropertyimage,
        description: "Enter a brief description for Image 8",
      },
    ],
    companyAddress: "",
  });

  const [editingField, setEditingField] = useState(null);
  const [editedText, setEditedText] = useState("");

  const handleTextClick = (field, currentValue) => {
    setEditingField(field);
    setEditedText(currentValue);
  };

  const handleTextSaveBrochure = (index) => {
    setSelect((prevSelect) => ({
      ...prevSelect,
      images: prevSelect.images.map((image, i) =>
        i === index ? { ...image, description: editedText } : image
      ),
    }));
    setEditingField(null);
  };

  const handleTextSave = () => {
    setSelect((prevSelect) => ({
      ...prevSelect,
      [editingField]: editedText,
    }));
    setEditingField(null);
  };
  const handleTextSaveSecond = () => {
    setSelect((prevSelect) => ({
      ...prevSelect,
      [editingField]: editedText,
    }));
    setEditingField(null);
  };

  const handleTextListAgent = (field, currentValue) => {
    setEditingField(field); // Set the field being edited
    setEditedText(currentValue); // Set the current text value in the input
  };

  // Handle saving the edited text for the list agent
  const handleTextSaveListAgent = (field) => {
    setProfile((prevProfile) => ({
      ...prevProfile,
      [field]: editedText, // Dynamically update the appropriate field (firstName or lastName)
    }));
    setEditingField(null); // Close the input field after saving
  };

  const handleTextListOffice = (field, currentValue) => {
    setEditingField(field); // Set the field being edited
    setEditedText(currentValue); // Pre-fill input with current value (address)
  };

  const handleTextSaveListOffice = (field) => {
    // Update the selected value locally after editing
    setProfile((prevProfile) => ({
      ...prevProfile,
      active_office: editedText, // Dynamically update the appropriate field (firstName or lastName)
    }));

    setEditingField(null); // Close the input field after saving
  };

  const handleTextOfficePhone = (field, currentValue) => {
    setEditingField(field); // Set the field being edited
    setEditedText(currentValue); // Pre-fill input with current value (address)
  };

  const handleTextSaveOfficePhone = (field) => {
    // Update the selected value locally after editing
    setProfile((prevProfile) => ({
      ...prevProfile,
      phone: editedText, // Dynamically update the appropriate field (firstName or lastName)
    }));

    setEditingField(null); // Close the input field after saving
  };

  const [platformData, setPlatFormData] = useState({});
  const [uploadedLogo, setUploadedLogo] = useState(null);
  const [showLogoInput, setShowLogoInput] = useState(false); // State to show or hide input field
  const [profile, setProfile] = useState({});

  const [uploadedContact, setUploadedContact] = useState(null);
  const [showContact, setShowContact] = useState(false);

  const PlatformdetailsGet = async () => {
    await user_service.platformdetailsGet().then((response) => {
      if (response) {
        setPlatFormData(response.data.data[0]);
      }
    });
  };

  const API_BASE_URL = "https://api.brokeragentbase.com";
  const profileGetAll = async () => {
    if (localStorage.getItem("auth")) {
      try {
        const response = await user_service.profileGet(
          jwt(localStorage.getItem("auth")).id
        );

        if (response && response.data) {
          const profileData = response.data;

          // Modify image URL to use proxy for the profile image
          if (profileData.image) {
            profileData.image = `${API_BASE_URL}/proxy-image?url=${encodeURIComponent(
              profileData.image
            )}`;
          }

          // Set the modified profile data with the proxied image URL
          setProfile(profileData);
        }
      } catch (error) {
        console.error("Error fetching profile data:", error);
      }
    }
  };

  useEffect(() => {
    window.scrollTo(0, 0);
    PlatformdetailsGet();
    profileGetAll();
  }, []);

  // Handle logo upload

  const handleLogoUploadContact = (e) => {
    const file = e.target.files[0];
    if (file) {
      const reader = new FileReader();
      reader.onload = (upload) => {
        setUploadedContact(upload.target.result); // Store the uploaded logo
        setShowContact(false); // Hide the input after upload
        // Update platformData with the new logo while preserving the rest of the data
        setProfile((prevData) => ({
          ...prevData,
          image: upload.target.result || prevData.image, // Update the logo in platformData
        }));
      };
      reader.readAsDataURL(file); // Read the file as a data URL for image preview
    }
  };

  const handleLogoUpload = (e) => {
    const file = e.target.files[0];
    if (file) {
      const reader = new FileReader();
      reader.onload = (upload) => {
        setUploadedLogo(upload.target.result); // Store the uploaded logo
        setShowLogoInput(false); // Hide the input after upload
        // // Update platformData with the new logo while preserving the rest of the data
        // setPlatFormData((prevData) => ({
        //   ...prevData,
        //   platform_logo: upload.target.result, // Update the logo in platformData
        // }));
      };
      reader.readAsDataURL(file); // Read the file as a data URL for image preview
    }
  };

  // Handle logo click to show the input
  const handleLogoClick = () => {
    setShowLogoInput(true); // Show the file input when the logo is clicked
  };

  const handleLogoClickContact = () => {
    setShowContact(true); // Show the file input when the logo is clicked
  };

  const styles = StyleSheet.create({
    page: {
      backgroundColor: "#b4a3a359",
      padding: 10,
    },

    pageThird: {
      backgroundColor: "#b4a3a359",
      padding: 10,
      display: 'flex',
      flexDirection: 'column',
      height: 200,
      boxSizing: 'border-box',
    },

    
    rowContainer: {
      display: "flex",
      flexDirection: "row",
      justifyContent: "space-between",
      width: "100%",
    },

    mainpic: {
      width: "100%",
      height: 400,
      objectFit: "cover",
      marginBottom: 10,
      marginTop:30
    },
    newBox: {
      width: "100%",
      position: "relative",
      height: 300,
      backgroundColor: "#2f251a",
    },

    detailFirstPage: {
      position: "absolute",
      bottom: 0,
    },

    detailOverview: {
      fontSize: 12,
      color: "#917462",
      textAlign: "justify",
      marginTop: 10,
    },

    longboxSubHeaderNew: {
      fontSize: 14,
      color: "#fff",
      margin: 10,
    },

    priceBrochure: {
      fontSize: 12,
      color: "#fff",
      marginLeft: 10,
    },

    flyerContainer: {
      display: "flex",
      flexWrap: "wrap",
      flexDirection: "row",
      justifyContent: "space-between",
    },
    imageWrapper: {
      width: "48%",
      marginBottom: 20,
      border: "1px solid #ddd",
      padding: 5,
    },
    flyerImage: {
      width: "100%",
      height: 200,
      objectFit: "cover",
    },
    description: {
      marginTop: 8,
      fontSize: 10,
      color: "#333",
    
    },

    row: {
      flexDirection: "row",
      flexWrap: "wrap",
      width: "100%",
      marginTop:40,
    },
  
    col: {
      width: "50%", // This creates a col-md-6 structure
      padding: 10,
    },
  
    mainImage: {
      width: "100%",
      height:400,
      objectFit: "cover",
      borderRadius: 10,
      border: "1px solid #ddd",
    },
  
    propertyDetailsContainer: {
      backgroundColor: "#fff",
      borderRadius: 10,
      padding:10,
      height:400,
      border: "1px solid #ddd",
    },
  
    propertyHeading: {
      fontSize: 14,
      fontWeight: "bold",
      marginBottom: 10,
      marginTop:5,
      textAlign: "center",
    },
  
    propertyDetailsBackground: {
      backgroundColor: "#2e3339",
      borderRadius: 10,
      padding: 10,
    },
  
    detailItem: {
      flexDirection: "row",
      justifyContent: "space-between",
      marginBottom: 5
    },
  
    detailLabel: {
      color: "#ffffff",
      fontSize: 12,
      fontWeight: "bold",
    },
  
    detailValue: {
      color: "#ffffff",
      fontSize: 12,
    },
  
    descriptionSection: {
      marginTop: 5,
    },
  
    descriptionText: {
      marginTop:10,
      fontSize: 12,
      textAlign:"justify"
    },

    // contact section
    addressBox: {
      width: "100%",
      backgroundColor: getComputedStyle(
        document.documentElement
      ).getPropertyValue("--branding-footer-color"), // Dynamically get CSS variable value
      padding: 10,
      marginTop:40,
      borderRadius: 5,
      display: "flex",
      flexDirection: "row",
      justifyContent: "space-between",
      // marginBottom:0,
    },

    officeSection: {
      display: "flex",
      // justifyContent : "flex-start",
      marginBottom: 5,
    },

    officeContactImage: {
      width: 60,
      height: 60,
      borderRadius: 50,
      marginLeft: 10,
    },
    officeAddress: {
      width: "50%",
      alignItems: "flex-end",
      marginBottom: 5,
    },

    officeDetails: {
      marginLeft: 10 /* Add space between image and text */,
    },
    officeText: {
      fontSize: 10,
      color: "#ffffff",
      // textTransform: "uppercase",
      marginBottom: 2,
    },

    officeAddressTest: {
      fontSize: 12,
      color: "#000000",
      marginBottom: 2,
    },

    contactHeading: {
      fontSize: 18,
      fontWeight: "bold",
      color: "#00000",
      textAlign: "center",
      marginBottom: 10,
    },

    propertyHeadingFirst: {
      marginTop: 5,
      fontSize: 18,
      fontWeight: "bold",
      color: "#00000",
      textAlign: "center",
      marginBottom: 10,
    },

    newBoxFourthPage: {
      width: "100%",
      position: "relative",
      height: 300,
      backgroundColor: "#f85e00",
    },
    officeLogo: {
      width: 100,
      objectFit: "contain",
      marginBottom: 10,
    },
       logoCol: {
      width: "48%", // Slightly smaller width for the logo column
    },
  });


  const Flyer = ({ select, platformData, profile }) => {
    const placeholderImage = defaultpropertyimage;
    return (
      <Document>
        <Page size="A4" style={styles.page}>
          <View style={styles.rowContainer}>
            <Image
              style={styles.mainpic}
              src={select.images[0].src || placeholderImage}
            />
          </View>

          <View style={styles.rowContainer}>
            <View style={styles.newBox}>
              <View style={styles.detailOverview}>
                <Text style={styles.longboxSubHeaderNew}>
                  {select.images[0].description}
                </Text>
              </View>
              <View style={styles.detailFirstPage}>
                <Text style={styles.priceBrochure}>
                  Listed at ${select.price || "X00,00"}
                </Text>
                <Text style={styles.longboxSubHeaderNew}>
                  {select.address || "2024 Demo St"}
                </Text>
              </View>
            </View>
          </View>
        </Page>

        <Page size="A4" style={styles.page}>
          <View style={styles.flyerContainer}>
            {(select.images && select.images.length > 0
              ? select.images.slice(1, 7)
              : Array(6).fill({ src: "", description: "" })
            ).map((imageObj, index) => {
              return (
                <View key={index} style={styles.imageWrapper}>
                  <Image
                    src={imageObj.src || placeholderImage}
                    style={styles.flyerImage}
                  />
                  <Text style={styles.description}>
                    {imageObj.description}
                  </Text>
                </View>
              );
            })}
          </View>
        </Page>

        <Page size="A4" style={styles.pageThird}>
        <View style={styles.row}>
          {/* Left Column: Image */}
          <View style={styles.col}>
            <Image
              style={styles.mainImage}
              src={select.images?.[7]?.src || placeholderImage}
            />
          </View>

          {/* Right Column: Property Details */}
          <View style={styles.col}>
            <View style={styles.propertyDetailsContainer}>
              <Text style={styles.propertyHeading}>Property Details</Text>
              <View style={styles.propertyDetailsBackground}>
                <View style={styles.detailItem}>
                  <Text style={styles.detailLabel}>Bedrooms</Text>
                  <Text style={styles.detailValue}>{select.bed || "0"}</Text>
                </View>
                <View style={styles.detailItem}>
                  <Text style={styles.detailLabel}>Bathrooms</Text>
                  <Text style={styles.detailValue}>{select.bath || "0"}</Text>
                </View>
                <View style={styles.detailItem}>
                  <Text style={styles.detailLabel}>Sqft</Text>
                  <Text style={styles.detailValue}>
                    {select.sqft || "X,000 Sq.Ft."}
                  </Text>
                </View>
                <View style={styles.detailItem}>
                  <Text style={styles.detailLabel}>Lot Size</Text>
                  <Text style={styles.detailValue}>{select.lotSize || "0"}</Text>
                </View>
                <View style={styles.detailItem}>
                  <Text style={styles.detailLabel}>Year Built</Text>
                  <Text style={styles.detailValue}>{select.yearBuilt || "2024"}</Text>
                </View>

                <View style={styles.detailItem}>
                  <Text style={styles.detailLabel}>Exterior</Text>
                  <Text style={styles.detailValue}>
                    {select.exterior || "Traditional House"}
                  </Text>
                </View>
                <View style={styles.detailItem}>
                  <Text style={styles.detailLabel}>Roof</Text>
                  <Text style={styles.detailValue}>{select.roof || "Tile"}</Text>
                </View>
                <View style={styles.detailItem}>
                  <Text style={styles.detailLabel}>Cooling</Text>
                  <Text style={styles.detailValue}>{select.cooling || "Central AC"}</Text>
                </View>
                <View style={styles.detailItem}>
                  <Text style={styles.detailLabel}>Heating</Text>
                  <Text style={styles.detailValue}>{select.heating || "Gas"}</Text>
                </View>
                <View style={styles.detailItem}>
                  <Text style={styles.detailLabel}>Garage</Text>
                  <Text style={styles.detailValue}>{select.garage || "X car"}</Text>
                </View>

             </View>
                <View style={styles.descriptionSection}>
                 <Text style={styles.descriptionText}>  
                   {select.images[7].description}
               </Text>
              </View>
            </View>
          </View>

             {/* contact section */}
            <View style={styles.addressBox}>
            <View style={styles.officeSection}>
              <View>
                <Image
                  style={styles.officeContactImage}
                  src={profile.image} // Compressed profile image
                />
              </View>
              <View style={styles.officeDetails}>
                <Text style={styles.officeText}>
                  Agent: {profile?.firstName} {profile?.lastName}
                </Text>
                <Text style={styles.officeText}>
                  Office: {profile?.active_office}
                </Text>
                <Text style={styles.officeText}>Phone: {profile?.phone}</Text>
              </View>
            </View>
            <View style={styles.officeAddress}>
            <View style={styles.logoCol}>
              <Image
                style={styles.officeLogo}
                src={platformData || Logo}
              />
            </View>
              <Text style={styles.officeText}>
                {select.published || "UtahRealEstate.com"}
              </Text>
              <Text style={styles.officeText}>
                {select.address || "2024 Demo St"}
              </Text>
              <Text style={styles.officeText}>
                {select.companyCity || "American Fork"},&nbsp;
                {select.companyState || "UT"}&nbsp;
                {select.companyPostCode || "84003"}
              </Text>
            </View>
             </View>
        </View>
        </Page>

        <Page size="A4" style={styles.page}>
          <View style={styles.rowContainer}>
            <Image
              style={styles.mainpic}
              src={select.images[8].src || placeholderImage}
            />
          </View>

          <View style={styles.rowContathinerFourth}>
            <View style={styles.newBoxFourthPage}>
              <View style={styles.detailOverview}>
                <Text style={styles.longboxSubHeaderNew}>
                  {select.images[8].description}
                </Text>
              </View>
            </View>
          </View>
        </Page>
      </Document>
    );
  };

  const stylesSecond = StyleSheet.create({
    page: {
      backgroundColor: "#b4a3a359",
      padding: 10,
    },

    brownbox: {
      padding: 5,
      marginBottom: 1,
      borderRadius: 5,
      position: "relative",
      overflow: "hidden",
      zIndex: 1,
    },

    rowContainer: {
      display: "flex",
      flexDirection: "row",
      justifyContent: "space-between",
      width: "100%",
    },
    logoCol: {
      width: "48%", // Slightly smaller width for the logo column
    },
    textCol: {
      width: "48%",
      marginTop: 0,
      padding: 0,
      display: "flex",
      justifyContent: "flex-end",
      textAlign: "right",
    },
    officeLogo: {
      width: 100,
      objectFit: "contain",
      marginBottom: 10,
    },
    priceText: {
      fontSize: 14,
      fontWeight: 700,
      color: "#000",
      marginBottom: 10,
    },
    // addressText: {
    //   fontSize: 14,
    //   marginTop: 5,
    //   marginBottom: 5,
    //   fontWeight: "bold",
    // },
    overviewHeader: {
      fontSize: 22,
      fontWeight: "bold",
      color: "#000",
      textAlign: "center",
      marginBottom: 15,
      transform: [{ rotate: "2deg" }],
      zIndex: 10,
    },
    contactInfo: {
      flexDirection: "row",
      justifyContent: "space-between",
      marginBottom: 20,
    },
    contactText: {
      fontSize: 12,
      color: "#fff",
    },
    mainpic: {
      width: "100%",
      height: 200,
      objectFit: "cover",

      transform: [{ rotate: "1deg" }],
      zIndex: 2,
    },

    price: {
      fontSize: 24,
      fontWeight: "bold",
      textAlign: "center",
      color: "#f5a623",
      marginBottom: 5,
      transform: [{ rotate: "-1deg" }],
    },
    longbox: {
      width: "100%",
      backgroundColor: "#1a1d1f",
      opacity: 0.95,
      padding: 15,
      borderRadius: 10,
      marginBottom: 5,
      flexDirection: "row",
      justifyContent: "space-between",
      zIndex: 5,
      paddingTop: 10,
    },
    description: {
      flex: 3, // 75% width (3 out of 4 parts)
      paddingRight: 10,
    },
    propertyData: {
      flex: 1, // 25% width (1 out of 4 parts)
      paddingLeft: 10,
    },
    contactHeading: {
      fontSize: 18,
      fontWeight: "bold",
      color: "#dc936b",
      marginBottom: 10,
    },
    longboxSubHeader: {
      fontSize: 12,
      color: "#e0e0e0",
      marginBottom: 5,
    },
    longboxSubHeaderNew: {
      fontSize: 10,
      color: "#fff",
      marginBottom: 5,
    },
    propertyDetails: {
      flexDirection: "column", // Ensure items are stacked vertically
      fontSize: 10,
      color: "#fff",
      textAlign: "justify",
      marginTop: 10,
    },
    detailItem: {
      fontSize: 10,
      textAlign: "center",
      marginBottom: 10,
      color: "#fff",
    },
    detailOverview: {
      fontSize: 10,
      color: "#fff",
      textAlign: "justify",
      marginTop: 10,
    },
    imageGrid: {
      flexDirection: "row",
      flexWrap: "wrap",
      justifyContent: "space-between",
      marginBottom: 5,
      position: "relative",
      marginTop: -30,
      paddingLeft: 20,
      paddingRight: 20,
    },
    imageItem: {
      width: "32%",
      height: 150,
      objectFit: "cover",
      marginBottom: 10,
      zIndex: 1,
    },
    addressBox: {
      width: "100%",
      backgroundColor: getComputedStyle(
        document.documentElement
      ).getPropertyValue("--branding-footer-color"), // Dynamically get CSS variable value
      marginTop: 10,
      padding: 20,
      borderRadius: 16,
      flexDirection: "row",
      justifyContent: "space-between",
      position: "relative",
      zIndex: 5,
      transform: [{ scaleY: 1.05 }],
    },
    addressText: {
      fontSize: 16,
      color: "#fff",
      textAlign: "center",
      transform: [{ rotate: "-2deg" }],
    },
    officeSection: {
      width: "50%",
      alignItems: "flex-start",
      marginBottom: 5,
    },
    officeAddress: {
      width: "50%",
      alignItems: "flex-end",
      marginBottom: 5,
    },
    officeLogo: {
      width: 100,
      height: 50,
      marginBottom: 10,
    },
    officeDetails: {
      width: "100%",
    },
    officeText: {
      fontSize: 14,
      color: "#fff",
      marginBottom: 2,
      zIndex: 10,
    },
    officeAddressTest: {
      fontSize: 14,
      color: "#fff",
      marginBottom: 2,
    },
    propertyHeading: {
      padding: 10,
      fontSize: 20,
      fontWeight: "bold",
      color: "#fff",
      textAlign: "center",
      marginBottom: 15,
      transform: [{ scaleY: 1.2 }],
    },
    contactHeading: {
      fontSize: 16,
      fontWeight: "bold",
      color: "#fff",
      textAlign: "center",
      marginBottom: 15,
      marginTop: 15,
      transform: [{ rotate: "2deg" }],
    },
    contactSecond: {
      fontSize: 16,
      fontWeight: "bold",
      color: "#cca345",
      textAlign: "center",
      marginBottom: 15,
      transform: [{ rotate: "2deg" }],
    },
  });

  const FlyerAlternative = ({
    select,
    uploadedImages,
    platformData,
    profile,
  }) => {
    const placeholderImage = defaultpropertyimage;

    return (
      <Document>
        <Page size="A4" style={stylesSecond.page}>
          {/* Header Section */}
          <View style={stylesSecond.rowContainer}>
            <View style={stylesSecond.logoCol}>
              <Image
                style={stylesSecond.officeLogo}
                src={platformData || Logo}
              />
            </View>
            <View style={stylesSecond.textCol}>
              <Text style={stylesSecond.priceText}>
                PRICE AT ${select?.price || "X00,000"}
              </Text>
            </View>
          </View>

          {/* Main Image and Grid Section */}
          <View style={stylesSecond.brownbox}>
            <Image
              style={stylesSecond.mainpic}
              src={uploadedImages[0] || placeholderImage}
            />
            <View style={stylesSecond.imageGrid}>
              {(Array.isArray(uploadedImages) && uploadedImages.length > 1
                ? uploadedImages.slice(1, 4)
                : Array(3).fill(placeholderImage)
              ).map((image, index) => (
                <Image key={index} style={stylesSecond.imageItem} src={image} />
              ))}
            </View>
          </View>

          {/* Property Description */}
          <View style={stylesSecond.longbox}>
            <View style={stylesSecond.description}>
              <Text style={stylesSecond.contactHeading}>Description</Text>
              <Text style={stylesSecond.longboxSubHeaderNew}>
                {select?.publicRemarks || "Designer Charm"}
              </Text>
            </View>
            <View style={stylesSecond.propertyData}>
              <Text style={stylesSecond.contactHeading}>Property Details</Text>
              <Text style={stylesSecond.detailItem}>
                Bedrooms: {select?.bed || "0"}
              </Text>
              <Text style={stylesSecond.detailItem}>
                Bathrooms: {select?.bath || "0"}
              </Text>
              <Text style={stylesSecond.detailItem}>
                Lot Size: {select?.lotSize || "N/A"}
              </Text>
              <Text style={stylesSecond.detailItem}>
                Sqft: {select?.sqft || "X,000 Sq.Ft."}
              </Text>
              <Text style={stylesSecond.detailItem}>
                Year Built: {select?.yearBuilt || "N/A"}
              </Text>
            </View>
          </View>

          {/* Contact and Address Section */}
          <View style={stylesSecond.addressBox}>
            <View style={stylesSecond.officeSection}>
              <Text style={stylesSecond.officeText}>
                {select?.published || "UtahRealEstate.com"}
              </Text>
              <Text style={stylesSecond.officeText}>
                {select?.address || "2024 Demo St"}
              </Text>
              <Text style={stylesSecond.officeText}>
                {select?.companyCity || "American Fork"},&nbsp;
                {select?.companyState || "UT"}&nbsp;
                {select?.companyPostCode || "84003"}
              </Text>
            </View>

            <View style={stylesSecond.officeAddress}>
              <Text style={stylesSecond.contactSecond}>CONTACT US NOW</Text>
              <Text style={stylesSecond.officeText}>
                Agent: {profile?.firstName} {profile?.lastName}
              </Text>
              <Text style={stylesSecond.officeText}>
                Office: {profile?.active_office || "N/A"}
              </Text>
              <Text style={stylesSecond.officeText}>
                Phone: {profile?.phone || "N/A"}
              </Text>
            </View>
          </View>
        </Page>
      </Document>
    );
  };

  const stylesSecondThird = StyleSheet.create({
    page: {
      backgroundColor: "#26221e",
      padding: 10,
    },

    rowContainer: {
      display: "flex",
      flexDirection: "row",
      justifyContent: "space-between",
      width: "100%",
    },
    logoCol: {
      width: "48%", // Slightly smaller width for the logo column
    },
    textCol: {
      width: "48%",
      justifyContent: "flex-end",
      textAlign: "right",
      marginRight: 10,
    },
    officeLogo: {
      width: 100,
      objectFit: "contain",
      marginBottom: 10,
    },
    priceText: {
      fontSize: 14,
      fontWeight: "bold",
      color: "red",
      marginBottom: 5,
    },

    // brownThirdPrice: {
    //   color: "red",
    //   marginBottom: 10,
    //   marginLeft: 10,
    // },
    mainLogo: {
      display: "flex",
      flexDirection: "row",
      padding: 5,
      marginBottom: 10,
      alignItems: "center",
      justifyContent: "space-between",
      width: "100%",
    },
    brownbox: {
      padding: 5,
      marginBottom: 1,
      borderRadius: 5,
      position: "relative",
      overflow: "hidden",
      zIndex: 1,
    },
    brownboxHeaderPrice: {
      fontSize: 18,
      fontWeight: "bold",
      color: "#dc936b",
      marginBottom: 5,
      transform: [{ scaleY: 1.2 }],
    },
    brownboxHeader: {
      fontSize: 18,
      fontWeight: "bold",
      color: "#fff",
      textAlign: "center",
      marginBottom: 5,
      transform: [{ scaleY: 1.2 }],
    },
    overviewHeader: {
      fontSize: 22,
      fontWeight: "bold",
      color: "#000",
      textAlign: "center",
      marginBottom: 15,
      transform: [{ rotate: "2deg" }],
      zIndex: 10,
    },
    contactInfo: {
      flexDirection: "row",
      justifyContent: "space-between",
      marginBottom: 20,
    },
    contactText: {
      fontSize: 12,
      color: "#fff",
    },

    mainpic: {
      width: "100%",
      flexDirection: "row",
      justifyContent: "space-between",
      marginBottom: 100,
    },
    imageThirdType: {
      flex: 2,
      height: 400,
      padding: 10,
    },
    imageItemThird: {
      flex: 2,
      padding: 10,
      height: 400,
    },

    imageThirdNew: {
      padding: 10,
      borderRadius: 16,
    },
    detailItem: {
      fontSize: 10,
      textAlign: "center",
      marginBottom: 10,
      color: "#fff",
    },
    detailOverview: {
      fontSize: 10,
      color: "#fff",
    },

    contactHeading: {
      fontSize: 16,
      fontWeight: "bold",
      color: "#fff",
      textAlign: "center",
      marginBottom: 15,
      marginTop: 15,
      transform: [{ rotate: "2deg" }],
    },

    longboxSubHeaderNew: {
      fontSize: 10,
      color: "#e0e0e0",
      marginBottom: 5,
    },
    price: {
      fontSize: 24,
      fontWeight: "bold",
      textAlign: "center",
      color: "#f5a623",
      marginBottom: 5,
      transform: [{ rotate: "-1deg" }],
    },
    longbox: {
      width: "100%",
      backgroundColor: "#1a1d1f",
      opacity: 0.95,
      padding: 15,
      borderRadius: 10,
      marginBottom: 5,
      flexDirection: "row",
      // justifyContent: "space-between",
      zIndex: 5,
      marginTop: 100,
    },
    description: {
      flex: 3,
      paddingRight: 10,
    },
    propertyData: {
      flex: 2,
      paddingLeft: 10,
    },

    longboxSubHeader: {
      fontSize: 12,
      color: "#e0e0e0",
      marginBottom: 5,
    },

    propertyDetails: {
      flexDirection: "column",
      fontSize: 10,
      color: "#fff",
      textAlign: "justify",
      marginTop: 10,
    },

    imageItem: {
      width: "32%",
      height: 150,
      objectFit: "cover",
      marginBottom: 10,
      zIndex: 1,
    },
    addressBox: {
      width: "100%",
      backgroundColor: getComputedStyle(
        document.documentElement
      ).getPropertyValue("--branding-footer-color"), // Dynamically get CSS variable value
      marginTop: 10,
      padding: 20,
      borderRadius: 16,
      flexDirection: "row",
      justifyContent: "space-between",
      position: "relative",
      zIndex: 5,
      transform: [{ scaleY: 1.05 }],
    },
    addressText: {
      fontSize: 16,
      color: "#fff",
      textAlign: "center",
      transform: [{ rotate: "-2deg" }],
    },
    officeSection: {
      width: "50%",
      alignItems: "flex-start",
      marginBottom: 5,
    },
    officeAddress: {
      marginBottom: 20,
      width: "50%",
      alignItems: "flex-end",
      marginBottom: 5,
    },
    officeLogo: {
      width: 120,
      height: 60,
      transform: [{ scale: 1.1 }],
    },
    officeDetails: {
      width: "100%",
    },
    officeText: {
      fontSize: 14,
      color: "#fff",
      textTransform: "uppercase",
      marginBottom: 2,
      zIndex: 10,
    },
    officeAddressTest: {
      fontSize: 14,
      color: "#fff",
      textTransform: "lowercase",
      marginBottom: 2,
    },
    propertyHeading: {
      padding: 10,
      fontSize: 20,
      fontWeight: "bold",
      color: "#fff",
      textAlign: "center",
      marginBottom: 15,
      transform: [{ scaleY: 1.2 }],
    },

    contactSecond: {
      fontSize: 16,
      fontWeight: "bold",
      color: "#cca345",
      textAlign: "center",
      marginBottom: 15,
      transform: [{ rotate: "2deg" }],
    },
  });

  const FlyerAlternativeThird = ({
    select,
    uploadedImages,
    platformData,
    profile,
  }) => {
    return (
      <Document>
        <Page size="A4" style={stylesSecondThird.page}>
          <View style={stylesSecondThird.rowContainer}>
            {/* Office Logo */}
            <View style={stylesSecondThird.logoCol}>
              <Image
                style={stylesSecondThird.officeLogo}
                src={platformData || Logo || ""}
                alt="Office Logo"
              />
            </View>
            {/* Price and Address */}
            <View style={stylesSecondThird.textCol}>
              <Text style={stylesSecondThird.priceText}>
                PRICE AT ${select.price || "X00,00"}
              </Text>
            </View>
          </View>

          <View style={stylesSecondThird.mainpic}>
            <View style={stylesSecondThird.imageThirdType}>
              <Image
                style={stylesSecond.imageThirdNew}
                src={uploadedImages[0] || defaultpropertyimage || ""}
                alt="Main Property Image"
              />
              <View style={stylesSecondThird.imageGrid}>
                <Text style={stylesSecondThird.contactHeading}>
                  Description
                </Text>
                <View style={stylesSecondThird.detailOverview}>
                  <Text style={stylesSecondThird.longboxSubHeaderNew}>
                    {select.publicRemarks || "Designer Charm"}
                  </Text>
                </View>
              </View>
            </View>
            {/* Other Images */}
            <View style={stylesSecondThird.imageItemThird}>
              {(Array.isArray(uploadedImages) && uploadedImages.length > 1
                ? uploadedImages.slice(1, 4)
                : Array(3).fill(defaultpropertyimage)
              ).map((mediaItem, mediaIndex) => (
                <Image
                  style={stylesSecondThird.imageThirdNew}
                  src={mediaItem || defaultpropertyimage || ""}
                  key={mediaIndex}
                  alt={`Media ${mediaIndex + 1}`}
                />
              ))}
            </View>
          </View>
          <View style={styles.longbox}>
            <View style={styles.propertyDetails}>
              <View style={styles.detailItem}>
                <Text style={styles.longboxSubHeader}>Bedrooms</Text>
                <Text style={styles.longboxSubHeaderNew}>
                  {select.bed || "0"}
                </Text>
              </View>
              <View style={styles.detailItem}>
                <Text style={styles.longboxSubHeader}>Bathrooms</Text>
                <Text style={styles.longboxSubHeaderNew}>
                  {select.bath || "0"}
                </Text>
              </View>

              <View style={styles.detailItem}>
                <Text style={styles.longboxSubHeader}>Sqft</Text>
                <Text style={styles.longboxSubHeaderNew}>
                  {select.sqft || "X,000 Sq.Ft."}
                </Text>
              </View>

              <View style={styles.detailItem}>
                <Text style={styles.longboxSubHeader}>Lot Size</Text>
                <Text style={styles.longboxSubHeaderNew}>
                  {select.lotSize || ""}
                </Text>
              </View>

              <View style={styles.detailItem}>
                <Text style={styles.longboxSubHeader}>Year Built</Text>
                <Text style={styles.longboxSubHeaderNew}>
                  {select.yearBuilt || "2024"}
                </Text>
              </View>
            </View>
          </View>
          <View style={stylesSecond.addressBox}>
            <View style={stylesSecond.officeSection}>
              <View style={stylesSecond.officeDetails}>
                <Text style={stylesSecond.officeText}>
                  {select.published || "UtahRealEstate.com"}
                </Text>
                <Text style={stylesSecond.officeText}>
                  {select.address || "2024 Demo St"},
                </Text>
                <Text style={stylesSecond.officeText}>
                  {select.companyCity || "American Fork"},&nbsp;
                  {select.companyState || "UT"}&nbsp;
                  {select.companyPostCode || "84003"}
                </Text>
              </View>
            </View>
            <View style={stylesSecond.officeAddress}>
              <Image
                style={styles.officeContactImage}
                src={profile.image} // Compressed profile image
              />
              <Text style={stylesSecond.officeText}>
                Agent: {profile?.firstName} {profile?.lastName}
              </Text>
              <Text style={stylesSecond.officeText}>
                Office: {profile?.active_office}
              </Text>
              <Text style={stylesSecond.officeText}>
                Phone: {profile?.phone}
              </Text>
            </View>
          </View>
        </Page>
      </Document>
    );
  };

  const [nextStep, setNextStep] = useState("1");
  const handleNext = (newStep) => {
    setNextStep(newStep);
  };

  const handleCancel = () => {
    setTemplate("");
  };
  const wordlimit = (e) => {
    const wordCount = e.target.value.length;
    console.log(wordCount,"wordCount");
    if (wordCount < 800) {
      setEditedText(e.target.value);
    }
  };

  const wordlimitSecond = (e) => {
    const wordCount = e.target.value.length;
    console.log(wordCount,"wordCount");
    if (wordCount < 150) {
      setEditedText(e.target.value);
    }
  };

  return (
    <div className="bg-secondary float-left">
      <Loader isActive={isActive} />
      {isShow && (
        <Toaster
          types={types}
          isShow={isShow}
          toasterBody={toasterBody}
          message={message}
        />
      )}
      <main className="page-wrapper">
        <div className="row" data-thumbnails="true">
          {/* {template ? (
            <> */}
              {/* {templateType === "type1" ? ( */}
                <section className="mb-5 pb-3 w-100" data-simplebar>
                  <div
                    className="bg-light border rounded-3 p-3"
                    data-thumbnails="true"
                  >
                    <div className="firstTemplate">
                      {/* Step 1 */}
                      {nextStep === "1" && (
                        <div className="row" data-thumbnails="true">
                          <div className="col-md-12">
                            <div className="col-md-12 singleListing">
                              <div className="card-img-top brochure">
                                <img
                                  className="w-100"
                                  style={{
                                    borderRadius: "0px",
                                    height: "500px",
                                    objectFit: "cover",
                                  }}
                                  src={
                                    select.images[0].src || defaultpropertyimage
                                  }
                                  alt="Property"
                                  onClick={() => handleImageClick(0)}
                                />
                                {selectedImage === 0 && (
                                  <label className="documentlabelNew">
                                    <input
                                      id="REPC_real_estate_purchase_contract"
                                      type="file"
                                      onChange={(e) => handleImageUpload(e, 0)} // Upload for the single image
                                    />
                                    <i className="h2 fi-edit opacity-80"></i>
                                  </label>
                                )}
                              </div>
                            </div>
                            <div className="col-md-12 w-100 mt-1">
                              <div className="card-img-top brochureListing p-3">
                                {editingField === `description-0` ? ( // Target description for index 8
                                  <textarea
                                    className="form-control mt-0 w-100"
                                    id="textarea-input-8" // Unique ID for index 8
                                    rows="3"
                                    value={editedText}
                                    onChange={(e) => wordlimit(e)}
                                    // onChange={(e) => {
                                    //   const wordCount = e.target.value.length;
                                    //   if (wordCount < 100) {
                                    //     setEditedText(e.target.value); // Update edited text
                                    //   }
                                    // }}
                                    onBlur={() => handleTextSaveBrochure(0)} // Save description for index 1
                                    autoFocus
                                  />
                                ) : (
                                  <h2
                                    className="text-white description_PrintPage"
                                    style={{
                                      color: "white",
                                      // fontFamily: "circular",
                                      // fontStyle: "italic",
                                      fontSize: "16px",
                                    }}
                                    onClick={() =>
                                      handleTextClick(
                                        `description-0`,
                                        select.images[0]?.description || ""
                                      )
                                    }
                                  >
                                    {select.images[0]?.description ||
                                      "Click to add a description"}{" "}
                                  </h2>
                                )}

                                <div className="brochure_FirstPage">
                                  {/* Price Field */}
                                  {editingField === "price" ? (
                                    <div className="text-center">
                                      <input
                                        type="text"
                                        value={editedText}
                                        onChange={(e) =>
                                          setEditedText(e.target.value)
                                        }
                                        onBlur={handleTextSave}
                                        autoFocus
                                      />
                                    </div>
                                  ) : (
                                    <span
                                      className="text-white"
                                      onClick={() =>
                                        handleTextClick("price", "X00,00")
                                      }
                                    >
                                      Listed at ${select.price || "X00,00"}
                                    </span>
                                  )}

                                  {/* Address Field */}
                                  {editingField === "address" ? (
                                    <div className="text-center">
                                      <input
                                        type="text"
                                        value={editedText}
                                        onChange={(e) =>
                                          setEditedText(e.target.value)
                                        }
                                        onBlur={handleTextSave}
                                        autoFocus
                                      />
                                    </div>
                                  ) : (
                                    <h6
                                      className="text-white"
                                      onClick={() =>
                                        handleTextClick(
                                          "address",
                                          "2024 Demo St"
                                        )
                                      }
                                    >
                                      {select.address || "2024 Demo St"}
                                    </h6>
                                  )}
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      )}

                      {/* Step 2 */}
                      {nextStep === "2" && (
                        <div className="row">
                          {console.log(select)}

                          {select.images.slice(1, 7).map((imageObj, index) => (
                            <div className="col-md-6 mt-2" key={index + 1}>
                              <div className="proprty_mediaImage">
                                <img
                                  className="w-100"
                                  style={{
                                    borderRadius: "0px",
                                    objectFit: "cover",
                                  }}
                                  src={imageObj.src}
                                  alt={`Property media ${index + 1}`}
                                  onClick={() => handleImageClick(index + 1)}
                                />
                                {selectedImage === index + 1 && (
                                  <label className="documentlabelAllImage">
                                    <input
                                      id={`image-upload-${index + 1}`}
                                      type="file"
                                      onChange={(e) =>
                                        handleImageUpload(e, index + 1)
                                      }
                                    />
                                    <i className="h2 fi-edit opacity-80"></i>
                                  </label>
                                )}
                              </div>

                              {/* Editable description */}
                              {editingField ===
                              `description-${index}` /* Adjusting field identifier */ ? (
                                <textarea
                                  className="form-control mt-0 w-100"
                                  id={`textarea-input-${index}`} /* Adjusting id for sliced index */
                                  rows="3"
                                  value={editedText}
                                  onChange={(e) => wordlimitSecond(e)}
                                  // onChange={(e) => {
                                  //   const wordCount = e.target.value.length;
                                  //   if (wordCount < 100) {
                                  //     setEditedText(e.target.value); // Update edited text
                                  //   }
                                  // }}
                                  onBlur={() =>
                                    handleTextSaveBrochure(index + 1)
                                  } /* Adjusting function parameter */
                                  autoFocus
                                />
                              ) : (
                                <p
                                  className="description_PrintPageSecond mt-2"
                                  style={{ fontSize: "12px" }}
                                  onClick={() =>
                                    handleTextClick(
                                      `description-${index}`,
                                      imageObj.description
                                    )
                                  }
                                >
                                  {imageObj.description ||
                                    "Click to add a description"}
                                </p>
                              )}
                            </div>
                          ))}
                        </div>
                      )}

                      {nextStep === "3" && (
                        <>
                          <div className="row mb-4" style={{margin:"10px"}}>
                            <div className="col-md-6">
                              <div className="card-img-top brochure">
                                <img
                                  className="w-100"
                                  style={{
                                    borderRadius: "10px",
                                    height: "500px",
                                    objectFit: "cover",
                                  }}
                                  src={
                                    select.images[7].src || defaultpropertyimage
                                  }
                                  alt="Property"
                                  onClick={() => handleImageClick(7)}
                                />
                                {selectedImage === 7 && (
                                  <label className="documentlabelAllImage">
                                    <input
                                      id="REPC_real_estate_purchase_contract"
                                      type="file"
                                      onChange={(e) => handleImageUpload(e, 7)} // Upload for the single image
                                    />
                                    <i className="h2 fi-edit opacity-80"></i>
                                  </label>
                                )}
                              </div>
                            </div>
                            <div className="col-md-6" style={{backgroundColor:"white", borderRadius:"10px"}}>
                              <h6 className="text-center mt-3">Property Details</h6>
                              <div className="row proprty_background m-0">
                                <ul className="list-unstyled text-center">
                                  <div className="d-flex align-items-center justify-content-between">
                                    <p className="text-white  mb-2">
                                      Bedrooms:
                                    </p>
                                    {editingField === "bed" ? (
                                      <input
                                        className="w-100"
                                        type="text"
                                        value={editedText} // Controlled input with edited text
                                        onChange={(e) =>
                                          setEditedText(e.target.value)
                                        } // Update text as the user types
                                        onBlur={handleTextSave} // Save the text when the user leaves the input field
                                        autoFocus
                                      />
                                    ) : (
                                      <p
                                        className="text-white ms-2 mb-2"
                                        onClick={() =>
                                          handleTextClick("bed", "0")
                                        }
                                      >
                                        {select.bed || "0"}
                                      </p>
                                    )}
                                  </div>

                                  <div className="d-flex align-items-center justify-content-between">
                                    <p className="text-white  mb-2">
                                      Bathrooms:
                                    </p>
                                    {editingField === "bath" ? (
                                      <input
                                        className="w-100"
                                        type="text"
                                        value={editedText} // Controlled input with edited text
                                        onChange={(e) =>
                                          setEditedText(e.target.value)
                                        } // Update text as the user types
                                        onBlur={handleTextSave} // Save the text when the user leaves the input field
                                        autoFocus
                                      />
                                    ) : (
                                      <p
                                        className="text-white ms-2 mb-2"
                                        onClick={() =>
                                          handleTextClick("bath", "0")
                                        }
                                      >
                                        {select.bath || "0"}
                                      </p>
                                    )}
                                  </div>

                                  <div className="d-flex align-items-center justify-content-between">
                                    <p className="text-white mb-2">Sqft:</p>
                                    {editingField === "sqft" ? (
                                      <input
                                        className="w-100"
                                        type="text"
                                        value={editedText} // Controlled input with edited text
                                        onChange={(e) =>
                                          setEditedText(e.target.value)
                                        } // Update text as the user types
                                        onBlur={handleTextSave} // Save the text when the user leaves the input field
                                        autoFocus
                                      />
                                    ) : (
                                      <p
                                        className="text-white ms-2 mb-2"
                                        onClick={() =>
                                          handleTextClick(
                                            "sqft",
                                            "X,000 Sq.Ft."
                                          )
                                        }
                                      >
                                        {select.sqft || "X,000 Sq.Ft."}
                                      </p>
                                    )}
                                  </div>

                                  <div className="d-flex align-items-center justify-content-between">
                                    <p className="text-white mb-2">Lot Size:</p>
                                    {editingField === "lotSize" ? (
                                      <input
                                        className="w-100"
                                        type="text"
                                        value={editedText} // Controlled input with edited text
                                        onChange={(e) =>
                                          setEditedText(e.target.value)
                                        } // Update text as the user types
                                        onBlur={handleTextSave} // Save the text when the user leaves the input field
                                        autoFocus
                                      />
                                    ) : (
                                      <p
                                        className="text-white ms-2 mb-2"
                                        onClick={() =>
                                          handleTextClick("lotSize", "0")
                                        }
                                      >
                                        {select.lotSize || "0"}
                                      </p>
                                    )}
                                  </div>

                                  <div className="d-flex align-items-center justify-content-between">
                                    <div className="d-flex">
                                      <p className="text-white mb-2">
                                        Year Built:
                                      </p>
                                    </div>
                                    {editingField === "yearBuilt" ? (
                                      <input
                                        className="w-100"
                                        type="text"
                                        value={editedText} // Controlled input with edited text
                                        onChange={(e) =>
                                          setEditedText(e.target.value)
                                        } // Update text as the user types
                                        onBlur={handleTextSave} // Save the text when the user leaves the input field
                                        autoFocus
                                      />
                                    ) : (
                                      <p
                                        className="text-white ms-2 mb-2"
                                        onClick={() =>
                                          handleTextClick("yearBuilt", "2024")
                                        }
                                      >
                                        {select.yearBuilt || "2024"}
                                      </p>
                                    )}
                                  </div>

                                  <div className="d-flex align-items-center justify-content-between">
                                    <div className="d-flex">
                                      <p className="text-white mb-2">
                                      Exterior:
                                      </p>
                                    </div>
                                    {editingField === "exterior" ? (
                                      <input
                                        className="w-100"
                                        type="text"
                                        value={editedText} // Controlled input with edited text
                                        onChange={(e) =>
                                          setEditedText(e.target.value)
                                        } // Update text as the user types
                                        onBlur={handleTextSave}
                                        autoFocus
                                      />
                                    ) : (
                                      <p
                                        className="text-white ms-2 mb-2"
                                        onClick={() =>
                                          handleTextClick(
                                            "exterior",
                                            "Traditional House"
                                          )
                                        }
                                      >
                                        {select.exterior || "Traditional House"}
                                      </p>
                                    )}
                                  </div>

                                  <div className="d-flex align-items-center justify-content-between">
                                    <div className="d-flex">
                                      <p className="text-white mb-2">Roof:</p>
                                    </div>
                                    {editingField === "roof" ? (
                                      <input
                                        className="w-100"
                                        type="text"
                                        value={editedText} // Controlled input with edited text
                                        onChange={(e) =>
                                          setEditedText(e.target.value)
                                        } // Update text as the user types
                                        onBlur={handleTextSave}
                                        autoFocus
                                      />
                                    ) : (
                                      <p
                                        className="text-white ms-2 mb-2"
                                        onClick={() =>
                                          handleTextClick(
                                            "roof",
                                            "Tile"
                                          )
                                        }
                                      >
                                        {select.roof || "Tile"},
                                      </p>
                                    )}
                                  </div>

                                  <div className="d-flex align-items-center justify-content-between">
                                    <div className="d-flex">
                                      <p className="text-white mb-2">Cooling:</p>
                                    </div>
                                    {editingField === "cooling" ? (
                                      <input
                                        type="text"
                                        value={editedText} // Controlled input with edited text
                                        onChange={(e) =>
                                          setEditedText(e.target.value)
                                        } // Update text as the user types
                                        onBlur={handleTextSave}
                                        autoFocus
                                      />
                                    ) : (
                                      <p
                                        className="text-white ms-2 mb-2"
                                        onClick={() =>
                                          handleTextClick("cooling", "Central AC")
                                        }
                                      >
                                        {select.cooling || "Central AC"},
                                      </p>
                                    )}
                                  </div>

                                  <div className="d-flex align-items-center justify-content-between">
                                    <div className="d-flex">
                                      <p className="text-white mb-2">
                                      Heating
                                      </p>
                                    </div>
                                    {editingField === "heating" ? (
                                      <input
                                        className="w-100"
                                        type="text"
                                        value={editedText} // Controlled input with edited text
                                        onChange={(e) =>
                                          setEditedText(e.target.value)
                                        } // Update text as the user types
                                        onBlur={handleTextSave}
                                        autoFocus
                                      />
                                    ) : (
                                      <p
                                        className="text-white ms-2 mb-2"
                                        onClick={() =>
                                          handleTextClick(
                                            "heating",
                                            "Gas"
                                          )
                                        }
                                      >
                                        {select.heating || "Gas"}
                                      </p>
                                    )}
                                  </div>

                                  <div className="d-flex align-items-center justify-content-between">
                                    <div className="d-flex">
                                      <p className="text-white mb-2">
                                      Garage
                                      </p>
                                    </div>
                                    {editingField === "garage" ? (
                                      <input
                                        className="w-100"
                                        type="text"
                                        value={editedText} // Controlled input with edited text
                                        onChange={(e) =>
                                          setEditedText(e.target.value)
                                        } // Update text as the user types
                                        onBlur={handleTextSave}
                                        autoFocus
                                      />
                                    ) : (
                                      <p
                                        className="text-white ms-2 mb-2"
                                        onClick={() =>
                                          handleTextClick(
                                            "garage",
                                            "X car"
                                          )
                                        }
                                      >
                                        {select.garage || "X car"}
                                      </p>
                                    )}
                                  </div>
                                </ul>
                              </div>

                              <div className="col-md-12 mt-3">
                                {editingField === `description-7` ? ( // Target description for index 8
                                  <textarea
                                    className="form-control mt-0 w-100"
                                    id="textarea-input-8" // Unique ID for index 8
                                    rows="3"
                                    value={editedText}
                                    onChange={(e) => wordlimitSecond(e)}
                                    // onChange={(e) => {
                                    //   const wordCount = e.target.value.length;
                                    //   if (wordCount < 150) {
                                    //     setEditedText(e.target.value); 
                                    //   }
                                    // }}
                                    onBlur={() => handleTextSaveBrochure(7)} // Save description for index 1
                                    autoFocus
                                  />
                                ) : (
                                  <p
                                    className="description_PrintPageSecond"
                                    style={{ fontSize: "14px" }}
                                    onClick={() =>
                                      handleTextClick(
                                        `description-7`,
                                        select.images[7]?.description || ""
                                      )
                                    }
                                  >
                                    {select.images[7]?.description ||
                                      "Click to add a description"}{" "}
                                  </p>
                                )}
                              </div>
                            </div>
                          </div>

                          <div className="row mt-5 contact_background">
                            <div className="col-md-6">
                              <div className="col-md-6">
                                <div className="float-start w-100">
                                  <img
                                    className="print_Profile"
                                    src={
                                      uploadedContact ||
                                      (profile.image ? profile.image : "")
                                    } // Show uploaded logo or default avatar
                                    style={{
                                      width: "100px",
                                      cursor: "pointer",
                                    }} // Pointer cursor for better UX
                                    onClick={handleLogoClickContact} // Show file input on click
                                  />
                                  {/* Conditionally render file input on logo click */}
                                  {showContact && (
                                    <label className="documentlabelContact">
                                      <input
                                        id="REPC_real_estate_purchase_contract"
                                        type="file"
                                        onChange={handleLogoUploadContact}
                                      />
                                      <i class="h2 fi-edit opacity-80"></i>
                                    </label>
                                  )}

                                  <div className="d-flex">
                                    {editingField === "firstName" ? (
                                      <input
                                        type="text"
                                        value={editedText} // Controlled input with edited text
                                        onChange={(e) =>
                                          setEditedText(e.target.value)
                                        } // Update text as the user types
                                        onBlur={() =>
                                          handleTextSaveListAgent("firstName")
                                        } // Save the text when the user leaves the input field
                                        autoFocus
                                      />
                                    ) : (
                                      <span
                                        style={{ fontSize: "12px" }}
                                        onClick={() =>
                                          handleTextListAgent(
                                            "firstName",
                                            profile.firstName,
                                            profile.lastName
                                          )
                                        }
                                      >
                                        {profile && (
                                          <p className="mb-0 text-white">
                                            <b>Agent: </b>
                                            {profile.firstName}&nbsp;
                                          </p>
                                        )}
                                      </span>
                                    )}

                                    {editingField === "lastName" ? (
                                      <input
                                        type="text"
                                        value={editedText} // Controlled input with edited text
                                        onChange={(e) =>
                                          setEditedText(e.target.value)
                                        } // Update text as the user types
                                        onBlur={() =>
                                          handleTextSaveListAgent("lastName")
                                        } // Save the text when the user leaves the input field
                                        autoFocus
                                      />
                                    ) : (
                                      <span
                                        style={{ fontSize: "12px" }}
                                        onClick={() =>
                                          handleTextListAgent(
                                            "lastName",
                                            profile.lastName
                                          )
                                        }
                                      >
                                        {profile && (
                                          <p className="mb-0 text-white">
                                            {" "}
                                            {profile.lastName}&nbsp;
                                          </p>
                                        )}
                                      </span>
                                    )}
                                  </div>

                                  {editingField === "listOffice" ? (
                                    <input
                                      type="text"
                                      value={editedText} // Controlled input with edited text
                                      onChange={(e) =>
                                        setEditedText(e.target.value)
                                      } // Update text as the user types
                                      onBlur={() =>
                                        handleTextSaveListOffice("listOffice")
                                      } // Save the text when the user leaves the input field
                                      autoFocus
                                    />
                                  ) : (
                                    <span
                                      style={{ fontSize: "12px" }}
                                      onClick={() =>
                                        handleTextListOffice(
                                          "listOffice",
                                          profile.active_office
                                        )
                                      }
                                    >
                                      {profile && (
                                        <p className="mb-0 text-white">
                                          <b>Office: </b>
                                          {profile.active_office}
                                        </p>
                                      )}
                                    </span>
                                  )}

                                  {editingField === "officePhone" ? (
                                    <input
                                      type="text"
                                      value={editedText} // Controlled input with edited text
                                      onChange={(e) =>
                                        setEditedText(e.target.value)
                                      } // Update text as the user types
                                      onBlur={() =>
                                        handleTextSaveOfficePhone("officePhone")
                                      } // Save the text when the user leaves the input field
                                      autoFocus
                                    />
                                  ) : (
                                    <span
                                      style={{ fontSize: "12px" }}
                                      onClick={() =>
                                        handleTextOfficePhone(
                                          "officePhone",
                                          profile.phone
                                        )
                                      }
                                    >
                                      {profile && (
                                        <p className="mb-0 text-white">
                                          <b>Phone: </b>
                                          {profile.phone}
                                        </p>
                                      )}
                                    </span>
                                  )}
                                </div>
                              </div>
                            </div>
                            <div className="col-md-6">
                              <div className="pull-right text-end imageLogo w-100">
                                <div>
                                  <img
                                    className="mb-2 rounded-0"
                                    src={uploadedLogo || Logo}
                                    style={{
                                      width: "100px",
                                      cursor: "pointer",
                                    }}
                                    onClick={handleLogoClick}
                                  />
                                  {showLogoInput && (
                                    <label className="documentlabelAllLogo">
                                      <input
                                        id="REPC_real_estate_purchase_contract"
                                        type="file"
                                        onChange={handleLogoUpload}
                                      />
                                      <i class="h2 fi-edit opacity-80"></i>
                                    </label>
                                  )}
                                </div>

                                {editingField === "published" ? (
                                  <input
                                    className="w-100"
                                    type="text"
                                    value={editedText} // Controlled input with edited text
                                    onChange={(e) =>
                                      setEditedText(e.target.value)
                                    } // Update text as the user types
                                    onBlur={handleTextSave} // Save the text when the user leaves the input field
                                    autoFocus
                                  />
                                ) : (
                                  <p
                                    className="mb-0 text-white"
                                    style={{ fontSize: "12px" }}
                                    onClick={() =>
                                      handleTextClick(
                                        "published",
                                        "UtahRealEstate.com"
                                      )
                                    }
                                  >
                                    {select.published || "UtahRealEstate.com"}
                                  </p>
                                )}

                                {editingField === "address" ? (
                                  <input
                                    className="w-100"
                                    type="text"
                                    value={editedText} // Controlled input with edited text
                                    onChange={(e) =>
                                      setEditedText(e.target.value)
                                    } // Update text as the user types
                                    onBlur={handleTextSave}
                                    autoFocus
                                  />
                                ) : (
                                  <span
                                    className="mb-0 text-white"
                                    style={{ fontSize: "12px" }}
                                    onClick={() =>
                                      handleTextClick("address", "2024 Demo St")
                                    }
                                  >
                                    {select.address || "2024 Demo St"}
                                  </span>
                                )}

                                <br />

                                {editingField === "companyCity" ? (
                                  <input
                                    className="w-100"
                                    type="text"
                                    value={editedText} // Controlled input with edited text
                                    onChange={(e) =>
                                      setEditedText(e.target.value)
                                    } // Update text as the user types
                                    onBlur={handleTextSave}
                                    autoFocus
                                  />
                                ) : (
                                  <span
                                    className="mb-0 text-white"
                                    style={{ fontSize: "12px" }}
                                    onClick={() =>
                                      handleTextClick(
                                        "companyCity",
                                        "American Fork"
                                      )
                                    }
                                  >
                                    {select.companyCity || "American Fork"},
                                  </span>
                                )}

                                {editingField === "companyState" ? (
                                  <input
                                    type="text"
                                    value={editedText} // Controlled input with edited text
                                    onChange={(e) =>
                                      setEditedText(e.target.value)
                                    } // Update text as the user types
                                    onBlur={handleTextSave}
                                    autoFocus
                                  />
                                ) : (
                                  <span
                                    className="mb-0 text-white"
                                    style={{ fontSize: "12px" }}
                                    onClick={() =>
                                      handleTextClick("companyState", "UT")
                                    }
                                  >
                                    {select.companyState || "UT"},
                                  </span>
                                )}

                                {editingField === "companyPostCode" ? (
                                  <input
                                    className="w-100"
                                    type="text"
                                    value={editedText} // Controlled input with edited text
                                    onChange={(e) =>
                                      setEditedText(e.target.value)
                                    } // Update text as the user types
                                    onBlur={handleTextSave}
                                    autoFocus
                                  />
                                ) : (
                                  <span
                                    className="mb-0 text-white"
                                    style={{ fontSize: "12px" }}
                                    onClick={() =>
                                      handleTextClick(
                                        "companyPostCode",
                                        "84003"
                                      )
                                    }
                                  >
                                    {select.companyPostCode || "84003"}
                                  </span>
                                )}
                              </div>
                            </div>
                          </div>
                        </>
                      )}

                      {nextStep === "4" && (
                        <div className="row" data-thumbnails="true">
                          <div className="col-md-12">
                            <div className="col-md-12 singleListing">
                              <div className="card-img-top brochure">
                                <img
                                  className="w-100"
                                  style={{
                                    borderRadius: "0px",
                                    height: "500px",
                                    objectFit: "cover",
                                  }}
                                  src={
                                    select.images[8]?.src ||
                                    defaultpropertyimage
                                  } // Ensure index 8 exists
                                  alt="Property"
                                  onClick={() => handleImageClick(8)} // Handle click for index 8
                                />
                                {selectedImage === 8 && (
                                  <label className="documentlabelNew">
                                    <input
                                      id="REPC_real_estate_purchase_contract"
                                      type="file"
                                      onChange={(e) => handleImageUpload(e, 8)} // Upload for index 8
                                    />
                                    <i className="h2 fi-edit opacity-80"></i>
                                  </label>
                                )}
                              </div>
                            </div>
                            <div className="col-md-12 w-100 mt-1">
                              <div className="card-img-top brochureListingFourth p-3">
                                {editingField === `description-8` ? ( // Target description for index 8
                                  <textarea
                                    className="form-control mt-0 w-100"
                                    id="textarea-input-8" // Unique ID for index 8
                                    rows="3"
                                    value={editedText}
                                    onChange={(e) => wordlimit(e)}
                                    // onChange={(e) => {
                                    //   const wordCount = e.target.value.length;
                                    //   if (wordCount < 100) {
                                    //     setEditedText(e.target.value); // Update edited text
                                    //   }
                                    // }}
                                    onBlur={() => handleTextSaveBrochure(8)} // Save description for index 8
                                    autoFocus
                                  />
                                ) : (
                                  <h2
                                    className="text-white description_PrintPage mt-2"
                                    style={{
                                      color: "white",
                                      // fontFamily: "circular",
                                      // fontStyle: "italic",
                                      fontSize: "16px",
                                    }}
                                    onClick={() =>
                                      handleTextClick(
                                        `description-8`,
                                        select.images[8]?.description || ""
                                      )
                                    }
                                  >
                                    {select.images[8]?.description ||
                                      "Click to add a description"}{" "}
                                  </h2>
                                )}
                              </div>
                            </div>
                          </div>
                        </div>
                      )}

                      {/* Add more steps here if needed */}
                    </div>

                    <div className="template_second">
                 

                      <button
                        className="btn btn-secondary pull-right ms-3"
                        onClick={() => handleNext("4")}
                      >
                        Back Page
                      </button>
                      <button
                        className="btn btn-secondary pull-right  ms-3"
                        onClick={() => handleNext("3")}
                      >
                        Inside Right Page
                      </button>
                      <button
                        className="btn btn-secondary pull-right  ms-3"
                        onClick={() => handleNext("2")}
                      >
                        Inside Left Page
                      </button>
                      <button
                        className="btn btn-secondary pull-right  ms-3"
                        onClick={() => handleNext("1")}
                      >
                        Front Page
                      </button>

                      <div className="ms-5">
                      <PDFDownloadLink
                        document={
                          <Flyer
                            select={select}
                            platformData={uploadedLogo}
                            profile={profile}
                          />
                        }
                        fileName={select.address || "Flyer.pdf"}
                      >
                        Download Pdf
                      </PDFDownloadLink>
                      </div>


                
                    </div>
                  </div>
                </section>
               {/* ) : (
                ""
               )} */}

              {templateType === "type2" ? (
                <div ref={componentRef}>
                  <section className="mb-5 pb-3 w-100" data-simplebar>
                    <div
                      className="bg-light border rounded-3 p-3"
                      data-thumbnails="true"
                    >
                      <div className="firstTemplate" data-thumbnails="true">
                        <div className="row" data-thumbnails="true">
                          <div className="col-md-12 singleListing">
                            <div className="row">
                              <div className="col-md-6 mt-3">
                                <img
                                  className="mb-4 rounded-0 "
                                  src={uploadedLogo || Logo}
                                  style={{
                                    width: "100px",
                                    cursor: "pointer",
                                  }} // Add cursor pointer for better UX
                                  onClick={handleLogoClick} // Click to show file input
                                />
                                {showLogoInput && (
                                  <label className="documentlabelAllLogo">
                                    <input
                                      id="REPC_real_estate_purchase_contract"
                                      type="file"
                                      onChange={handleLogoUpload}
                                    />
                                    <i class="h2 fi-edit opacity-80"></i>
                                  </label>
                                )}
                              </div>
                              <div className="col-md-6 mt-3">
                                {editingField === "price" ? (
                                  <div className="text-center">
                                    <input
                                      type="text"
                                      value={editedText}
                                      onChange={(e) =>
                                        setEditedText(e.target.value)
                                      }
                                      onBlur={handleTextSaveSecond}
                                      autoFocus
                                    />
                                  </div>
                                ) : (
                                  <h6
                                    className="d-flex justify-content-end mt-4"
                                    onClick={() =>
                                      handleTextClick("price", "X00,00")
                                    }
                                  >
                                    PRICE AT $ {select.price || "X00,00"}
                                  </h6>
                                )}
                              </div>
                            </div>

                            <div className="card-img-top">
                              <img
                                className="img-fluid rounded-0"
                                src={uploadedImages[0] || defaultpropertyimage}
                                alt="Property"
                                onClick={() => handleImageClick(0)}
                              />
                              {selectedImage === 0 && (
                                <label className="documentlabelNew">
                                  <input
                                    id="REPC_real_estate_purchase_contract"
                                    type="file"
                                    onChange={(e) => handleImageUpload(e, 0)} // Upload for the single image
                                  />
                                  <i className="h2 fi-edit opacity-80"></i>
                                </label>
                              )}
                            </div>
                          </div>

                          <div className="row">
                            {[1, 2, 3].map((_, defaultIndex) => (
                              <div className="col-md-4 mt-2" key={defaultIndex}>
                                <div className="second_mediaImage">
                                  <img
                                    className="img-fluid w-100 rounded-0"
                                    src={
                                      uploadedImages[defaultIndex + 1] ||
                                      defaultpropertyimage
                                    }
                                    alt={`Default Property media ${
                                      defaultIndex + 1
                                    }`}
                                    onClick={() =>
                                      handleImageClick(defaultIndex + 1)
                                    } // Allow clicking to upload a new image
                                  />
                                  {selectedImage === defaultIndex + 1 && (
                                    <label className="documentlabelAllImage">
                                      <input
                                        id="REPC_real_estate_purchase_contract"
                                        type="file"
                                        onChange={(e) =>
                                          handleImageUpload(e, defaultIndex + 1)
                                        } // Allow image upload
                                      />
                                      <i className="h2 fi-edit opacity-80"></i>
                                    </label>
                                  )}
                                </div>
                              </div>
                            ))}
                          </div>
                          <div className="propertySecond_details float-left w-100">
                            <div className="row">
                              <div className="col-md-8">
                                <h6 className="text-center text-white">
                                  Description
                                </h6>
                                {/* Edit PublicRemarks */}
                                {editingField === "publicRemarks" ? (
                                  <textarea
                                    className="form-control mt-0"
                                    id="textarea-input"
                                    rows="5"
                                    type="text"
                                    value={editedText} // Controlled input with edited text
                                    onChange={(e) => {
                                      const wordCount = e.target.value
                                        .trim()
                                        .split(/\s+/).length;
                                      if (wordCount <= 150) {
                                        setEditedText(e.target.value); // Update text only if word count is <= 150
                                      }
                                    }} // Update text as the user types
                                    onBlur={handleTextSaveSecond}
                                    autoFocus
                                  />
                                ) : (
                                  <p
                                    className="mb-1 description_PrintPage p-2"
                                    onClick={() =>
                                      handleTextClick(
                                        "publicRemarks",
                                        "Designer Charm"
                                      )
                                    }
                                  >
                                    <small>
                                      {select.publicRemarks || "Designer Charm"}
                                    </small>
                                  </p>
                                )}
                              </div>
                              <div className="col-md-4">
                                <h6 className="text-center text-white">
                                  Property Details
                                </h6>
                                <ul className="list-unstyled text-center">
                                  {editingField === "bed" ? (
                                    <input
                                      className="w-100"
                                      type="text"
                                      value={editedText} // Controlled input with edited text
                                      onChange={(e) =>
                                        setEditedText(e.target.value)
                                      } // Update text as the user types
                                      onBlur={handleTextSaveSecond} // Save the text when the user leaves the input field
                                      autoFocus
                                    />
                                  ) : (
                                    <p
                                      className="text-white mb-2"
                                      onClick={() =>
                                        handleTextClick("bed", "0")
                                      }
                                    >
                                      <small>
                                        Bedrooms:
                                        {select.bed || "0"}
                                      </small>
                                    </p>
                                  )}

                                  {editingField === "bath" ? (
                                    <input
                                      className="w-100"
                                      type="text"
                                      value={editedText} // Controlled input with edited text
                                      onChange={(e) =>
                                        setEditedText(e.target.value)
                                      } // Update text as the user types
                                      onBlur={handleTextSaveSecond} // Save the text when the user leaves the input field
                                      autoFocus
                                    />
                                  ) : (
                                    <p
                                      className="text-white mb-2"
                                      onClick={() =>
                                        handleTextClick("bath", "0")
                                      }
                                    >
                                      <small>
                                        Bedrooms:
                                        {select.bath || "0"}
                                      </small>
                                    </p>
                                  )}

                                  {editingField === "sqft" ? (
                                    <input
                                      className="w-100"
                                      type="text"
                                      value={editedText} // Controlled input with edited text
                                      onChange={(e) =>
                                        setEditedText(e.target.value)
                                      } // Update text as the user types
                                      onBlur={handleTextSaveSecond} // Save the text when the user leaves the input field
                                      autoFocus
                                    />
                                  ) : (
                                    <p
                                      className="text-white mb-2"
                                      onClick={() =>
                                        handleTextClick("sqft", "X,000 Sq.Ft.")
                                      }
                                    >
                                      <small>
                                        Sqft:
                                        {select.sqft || "X,000 Sq.Ft."}
                                      </small>
                                    </p>
                                  )}

                                  {editingField === "lotSize" ? (
                                    <input
                                      className="w-100"
                                      type="text"
                                      value={editedText} // Controlled input with edited text
                                      onChange={(e) =>
                                        setEditedText(e.target.value)
                                      } // Update text as the user types
                                      onBlur={handleTextSaveSecond} // Save the text when the user leaves the input field
                                      autoFocus
                                    />
                                  ) : (
                                    <p
                                      className="text-white mb-2"
                                      onClick={() =>
                                        handleTextClick("lotSize", "0")
                                      }
                                    >
                                      <small>
                                        Lot size : {select.lotSize || "0"}
                                      </small>
                                    </p>
                                  )}

                                  {editingField === "yearBuilt" ? (
                                    <input
                                      className="w-100"
                                      type="text"
                                      value={editedText} // Controlled input with edited text
                                      onChange={(e) =>
                                        setEditedText(e.target.value)
                                      } // Update text as the user types
                                      onBlur={handleTextSaveSecond} // Save the text when the user leaves the input field
                                      autoFocus
                                    />
                                  ) : (
                                    <p
                                      className="text-white mb-2"
                                      onClick={() =>
                                        handleTextClick("yearBuilt", "2024")
                                      }
                                    >
                                      <small>
                                        Year Built :{" "}
                                        {select.yearBuilt || "2024"}
                                      </small>
                                    </p>
                                  )}
                                </ul>
                              </div>
                            </div>
                          </div>

                          <div className="row mt-2 contactSecond_background">
                            <div className="col-md-6 float-left">
                              {editingField === "published" ? (
                                <input
                                  className="w-100"
                                  type="text"
                                  value={editedText} // Controlled input with edited text
                                  onChange={(e) =>
                                    setEditedText(e.target.value)
                                  } // Update text as the user types
                                  onBlur={handleTextSaveSecond} // Save the text when the user leaves the input field
                                  autoFocus
                                />
                              ) : (
                                <span
                                  className="text-white mb-0"
                                  onClick={() =>
                                    handleTextClick(
                                      "published",
                                      "UtahRealEstate.com"
                                    )
                                  }
                                >
                                  <smal>
                                    {select.published || "UtahRealEstate.com"}
                                  </smal>
                                </span>
                              )}
                              <br />

                              {editingField === "address" ? (
                                <input
                                  className="w-100"
                                  type="text"
                                  value={editedText} // Controlled input with edited text
                                  onChange={(e) =>
                                    setEditedText(e.target.value)
                                  } // Update text as the user types
                                  onBlur={handleTextSaveSecond}
                                  autoFocus
                                />
                              ) : (
                                <span
                                  className="text-white mb-0"
                                  onClick={() =>
                                    handleTextClick("address", "2024 Demo St")
                                  }
                                >
                                  {select.address || "2024 Demo St"}
                                </span>
                              )}
                              <br />
                              <div className="d-flex">
                                {editingField === "companyCity" ? (
                                  <input
                                    className="w-100"
                                    type="text"
                                    value={editedText} // Controlled input with edited text
                                    onChange={(e) =>
                                      setEditedText(e.target.value)
                                    } // Update text as the user types
                                    onBlur={handleTextSaveSecond}
                                    autoFocus
                                  />
                                ) : (
                                  <span
                                    className="text-white mb-0"
                                    onClick={() =>
                                      handleTextClick(
                                        "companyCity",
                                        "American Fork"
                                      )
                                    }
                                  >
                                    {select.companyCity || "American Fork"},
                                  </span>
                                )}

                                {editingField === "companyState" ? (
                                  <input
                                    type="text"
                                    value={editedText} // Controlled input with edited text
                                    onChange={(e) =>
                                      setEditedText(e.target.value)
                                    } // Update text as the user types
                                    onBlur={handleTextSaveSecond}
                                    autoFocus
                                  />
                                ) : (
                                  <span
                                    className="text-white mb-0"
                                    onClick={() =>
                                      handleTextClick("companyState", "UT")
                                    }
                                  >
                                    {select.companyState || "UT"},
                                  </span>
                                )}

                                {editingField === "companyPostCode" ? (
                                  <input
                                    className="w-100"
                                    type="text"
                                    value={editedText} // Controlled input with edited text
                                    onChange={(e) =>
                                      setEditedText(e.target.value)
                                    } // Update text as the user types
                                    onBlur={handleTextSaveSecond}
                                    autoFocus
                                  />
                                ) : (
                                  <span
                                    className="text-white mb-0"
                                    onClick={() =>
                                      handleTextClick(
                                        "companyPostCode",
                                        "84003"
                                      )
                                    }
                                  >
                                    {select.companyPostCode || "84003"}
                                  </span>
                                )}
                              </div>
                            </div>

                            <div className="col-md-6">
                              <ul className="list-unstyled text-end">
                                <img
                                  className="print_Profile"
                                  src={
                                    uploadedContact ||
                                    (profile.image ? profile.image : "")
                                  } // Show uploaded logo or default avatar
                                  style={{
                                    width: "100px",
                                    cursor: "pointer",
                                  }} // Pointer cursor for better UX
                                  onClick={handleLogoClickContact} // Show file input on click
                                />
                                {/* Conditionally render file input on logo click */}
                                {showContact && (
                                  <label className="documentlabelContact">
                                    <input
                                      id="REPC_real_estate_purchase_contract"
                                      type="file"
                                      onChange={handleLogoUploadContact}
                                    />
                                    <i class="h2 fi-edit opacity-80"></i>
                                  </label>
                                )}

                                <div className="d-flex justify-content-end">
                                  {editingField === "firstName" ? (
                                    <input
                                      type="text"
                                      value={editedText} // Controlled input with edited text
                                      onChange={(e) =>
                                        setEditedText(e.target.value)
                                      } // Update text as the user types
                                      onBlur={() =>
                                        handleTextSaveListAgent("firstName")
                                      } // Save the text when the user leaves the input field
                                      autoFocus
                                    />
                                  ) : (
                                    <p
                                      className="text-white mb-0"
                                      onClick={() =>
                                        handleTextListAgent(
                                          "firstName",
                                          profile.firstName,
                                          profile.lastName
                                        )
                                      }
                                    >
                                      {profile && (
                                        <p className="mb-0">
                                          <b>Agent: </b>
                                          {profile.firstName}&nbsp;
                                        </p>
                                      )}
                                    </p>
                                  )}

                                  {editingField === "lastName" ? (
                                    <input
                                      type="text"
                                      value={editedText} // Controlled input with edited text
                                      onChange={(e) =>
                                        setEditedText(e.target.value)
                                      } // Update text as the user types
                                      onBlur={() =>
                                        handleTextSaveListAgent("lastName")
                                      } // Save the text when the user leaves the input field
                                      autoFocus
                                    />
                                  ) : (
                                    <p
                                      className="text-white mb-0"
                                      onClick={() =>
                                        handleTextListAgent(
                                          "lastName",
                                          profile.lastName
                                        )
                                      }
                                    >
                                      {profile && (
                                        <p className="mb-0">
                                          {" "}
                                          {profile.lastName}&nbsp;
                                        </p>
                                      )}
                                    </p>
                                  )}
                                </div>

                                {editingField === "listOffice" ? (
                                  <input
                                    type="text"
                                    value={editedText} // Controlled input with edited text
                                    onChange={(e) =>
                                      setEditedText(e.target.value)
                                    } // Update text as the user types
                                    onBlur={() =>
                                      handleTextSaveListOffice("listOffice")
                                    } // Save the text when the user leaves the input field
                                    autoFocus
                                  />
                                ) : (
                                  <p
                                    className="text-white mb-0"
                                    onClick={() =>
                                      handleTextListOffice(
                                        "listOffice",
                                        profile.active_office
                                      )
                                    }
                                  >
                                    {profile && (
                                      <p className="mb-0">
                                        <b>Office: </b>
                                        {profile.active_office}
                                      </p>
                                    )}
                                  </p>
                                )}

                                {editingField === "officePhone" ? (
                                  <input
                                    type="text"
                                    value={editedText} // Controlled input with edited text
                                    onChange={(e) =>
                                      setEditedText(e.target.value)
                                    } // Update text as the user types
                                    onBlur={() =>
                                      handleTextSaveOfficePhone("officePhone")
                                    } // Save the text when the user leaves the input field
                                    autoFocus
                                  />
                                ) : (
                                  <p
                                    className="text-white mb-0"
                                    onClick={() =>
                                      handleTextOfficePhone(
                                        "officePhone",
                                        profile.phone
                                      )
                                    }
                                  >
                                    {profile && (
                                      <p className="mb-0">
                                        <b>Phone: </b>
                                        {profile.phone}
                                      </p>
                                    )}
                                  </p>
                                )}
                              </ul>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div className="template_second">
                        <button
                          className="btn btn-secondary pull-right"
                          onClick={() => handleCancel()}
                        >
                          Cancel
                        </button>
                        <PDFDownloadLink
                          document={
                            <FlyerAlternative
                              select={select}
                              uploadedImages={uploadedImages}
                              platformData={uploadedLogo}
                              profile={profile}
                            />
                          }
                          fileName={select.address || "Flyer2.pdf"}
                        >
                          Download Pdf
                        </PDFDownloadLink>
                      </div>
                    </div>
                  </section>
                </div>
              ) : (
                ""
              )}

              {templateType === "type3" ? (
                <section
                  className="mb-0 pb-3 bg-light border rounded-3 p-3"
                  data-simplebar
                >
                  <div className="thirdTemplate">
                    <div className="row">
                      <div className="col-md-6 mt-3">
                        <img
                          className="mb-4 rounded-0 "
                          src={uploadedLogo || Logo}
                          style={{
                            width: "100px",
                            cursor: "pointer",
                          }} // Add cursor pointer for better UX
                          onClick={handleLogoClick} // Click to show file input
                        />
                        {showLogoInput && (
                          <label className="documentlabelAllLogo">
                            <input
                              id="REPC_real_estate_purchase_contract"
                              type="file"
                              onChange={handleLogoUpload}
                            />
                            <i class="h2 fi-edit opacity-80"></i>
                          </label>
                        )}
                      </div>

                      <div className="col-md-6 mt-3">
                        {editingField === "price" ? (
                          <div className="text-center">
                            <input
                              type="text"
                              value={editedText} // Controlled input with edited text
                              onChange={(e) => setEditedText(e.target.value)}
                              onBlur={handleTextSave} // Save the text when the user leaves the input field
                              autoFocus
                            />
                          </div>
                        ) : (
                          <h6
                            className="d-flex justify-content-end mt-4"
                            style={{ color: "red" }}
                            onClick={() => handleTextClick("price", "X00,00")}
                          >
                            PRICE AT $ {select.price || "X00,00"}
                          </h6>
                        )}
                      </div>
                    </div>
                    <div className="row" data-thumbnails="true">
                      <div className="col-md-6 singleListing mb-3">
                        <div className="card-img-top">
                          <img
                            className="img-fluid rounded-0"
                            src={uploadedImages[0] || defaultpropertyimage}
                            alt="Property"
                            onClick={() => handleImageClick(0)}
                          />

                          {selectedImage === 0 && (
                            <label className="documentlabelNew">
                              <input
                                id="REPC_real_estate_purchase_contract"
                                type="file"
                                onChange={(e) => handleImageUpload(e, 0)} // Upload for the single image
                              />
                              <i className="h2 fi-edit opacity-80"></i>
                            </label>
                          )}
                        </div>

                        {/* Edit PublicRemarks */}
                        <div className="mt-2">
                          <h6 className="text-center text-white">
                            Description
                          </h6>
                          {editingField === "publicRemarks" ? (
                            <textarea
                              className="form-control mt-0"
                              id="textarea-input"
                              rows="5"
                              type="text"
                              value={editedText} // Controlled input with edited text
                              onChange={(e) => {
                                const wordCount = e.target.value
                                  .trim()
                                  .split(/\s+/).length;
                                if (wordCount <= 150) {
                                  setEditedText(e.target.value); // Update text only if word count is <= 150
                                }
                              }} // Update text as the user types
                              onBlur={handleTextSave}
                              autoFocus
                            />
                          ) : (
                            <span
                              onClick={() =>
                                handleTextClick(
                                  "publicRemarks",
                                  "Designer Charm"
                                )
                              }
                            >
                              <div className="mb-4">
                                <p className="mb-1 description_PrintPage text-white">
                                  {select.publicRemarks || "Designer Charm"}
                                </p>
                              </div>
                            </span>
                          )}
                        </div>
                      </div>
                      <div className="col-md-6">
                        {[1, 2, 3].map((_, defaultIndex) => (
                          <div
                            className="col-md-12 mt-2 p-3 mb-2"
                            key={defaultIndex}
                          >
                            <div className="second_mediaImage">
                              <img
                                className="img-fluid w-100 rounded-0"
                                src={
                                  uploadedImages[defaultIndex + 1] ||
                                  defaultpropertyimage
                                }
                                alt={`Default Property media ${
                                  defaultIndex + 1
                                }`}
                                onClick={() =>
                                  handleImageClick(defaultIndex + 1)
                                } // Allow clicking to upload a new image
                              />
                              {selectedImage === defaultIndex + 1 && (
                                <label className="documentlabelAllImage">
                                  <input
                                    id="REPC_real_estate_purchase_contract"
                                    type="file"
                                    onChange={(e) =>
                                      handleImageUpload(e, defaultIndex + 1)
                                    } // Allow image upload
                                  />
                                  <i className="h2 fi-edit opacity-80"></i>
                                </label>
                              )}
                            </div>
                          </div>
                        ))}
                      </div>
                    </div>

                    <div className="row thirdDetails_Property">
                      <div className="col-md-2">
                        <h6 className="text-white">Bedrooms</h6>

                        {editingField === "bed" ? (
                          <input
                            className="w-100"
                            type="text"
                            value={editedText} // Controlled input with edited text
                            onChange={(e) => setEditedText(e.target.value)} // Update text as the user types
                            onBlur={handleTextSave} // Save the text when the user leaves the input field
                            autoFocus
                          />
                        ) : (
                          <p
                            className="text-white"
                            onClick={() => handleTextClick("bed", "0")}
                          >
                            <small>{select.bed || "0"}</small>
                          </p>
                        )}
                      </div>

                      <div className="col-md-2 ms-3">
                        <h6 className="text-white">Bathrooms</h6>
                        {editingField === "bath" ? (
                          <input
                            className="w-100"
                            type="text"
                            value={editedText} // Controlled input with edited text
                            onChange={(e) => setEditedText(e.target.value)} // Update text as the user types
                            onBlur={handleTextSave} // Save the text when the user leaves the input field
                            autoFocus
                          />
                        ) : (
                          <p
                            className="text-white"
                            onClick={() => handleTextClick("bath", "0")}
                          >
                            <small>{select.bath || "0"}</small>
                          </p>
                        )}
                      </div>

                      <div className="col-md-2 ms-4">
                        <h6 className="text-white">Sqft</h6>

                        {editingField === "sqft" ? (
                          <input
                            className="w-100"
                            type="text"
                            value={editedText} // Controlled input with edited text
                            onChange={(e) => setEditedText(e.target.value)} // Update text as the user types
                            onBlur={handleTextSave} // Save the text when the user leaves the input field
                            autoFocus
                          />
                        ) : (
                          <p
                            className="text-white"
                            onClick={() =>
                              handleTextClick("sqft", "X,000 Sq.Ft.")
                            }
                          >
                            <small>{select.sqft || "X,000 Sq.Ft."}</small>
                          </p>
                        )}
                      </div>

                      <div className="col-md-2 ms-3">
                        <h6 className="text-white">Lot Size</h6>
                        {editingField === "lotSize" ? (
                          <input
                            className="w-100"
                            type="text"
                            value={editedText} // Controlled input with edited text
                            onChange={(e) => setEditedText(e.target.value)} // Update text as the user types
                            onBlur={handleTextSave} // Save the text when the user leaves the input field
                            autoFocus
                          />
                        ) : (
                          <p
                            className="text-white"
                            onClick={() => handleTextClick("lotSize", "0")}
                          >
                            <small>{select.lotSize || "0"}</small>
                          </p>
                        )}
                      </div>

                      <div className="col-md-2 ms-5">
                        <h6 className="text-white">Year Built</h6>
                        {editingField === "yearBuilt" ? (
                          <input
                            className="w-100"
                            type="text"
                            value={editedText} // Controlled input with edited text
                            onChange={(e) => setEditedText(e.target.value)} // Update text as the user types
                            onBlur={handleTextSave} // Save the text when the user leaves the input field
                            autoFocus
                          />
                        ) : (
                          <p
                            className="text-white"
                            onClick={() => handleTextClick("yearBuilt", "2024")}
                          >
                            <small>{select.yearBuilt || "2024"}</small>
                          </p>
                        )}
                      </div>
                    </div>

                    <div className="row mt-2 contactSecond_background">
                      <div className="col-md-6 float-left">
                        <div className="imageLogo w-100">
                          {editingField === "published" ? (
                            <input
                              className="w-100"
                              type="text"
                              value={editedText} // Controlled input with edited text
                              onChange={(e) => setEditedText(e.target.value)} // Update text as the user types
                              onBlur={handleTextSave} // Save the text when the user leaves the input field
                              autoFocus
                            />
                          ) : (
                            <p
                              className="text-white mb-0"
                              onClick={() =>
                                handleTextClick(
                                  "published",
                                  "UtahRealEstate.com"
                                )
                              }
                            >
                              <smal>
                                {select.published || "UtahRealEstate.com"}
                              </smal>
                            </p>
                          )}

                          {editingField === "companyAddress" ? (
                            <input
                              className="w-100"
                              type="text"
                              value={editedText} // Controlled input with edited text
                              onChange={(e) => setEditedText(e.target.value)} // Update text as the user types
                              onBlur={handleTextSave}
                              autoFocus
                            />
                          ) : (
                            <span
                              className="text-white mb-0"
                              onClick={() =>
                                handleTextClick("address", "2024 Demo St")
                              }
                            >
                              {select.address || "2024 Demo St"}
                            </span>
                          )}

                          <br />

                          <div className="d-flex">
                            {editingField === "companyCity" ? (
                              <input
                                className="w-100"
                                type="text"
                                value={editedText} // Controlled input with edited text
                                onChange={(e) => setEditedText(e.target.value)} // Update text as the user types
                                onBlur={handleTextSave}
                                autoFocus
                              />
                            ) : (
                              <span
                                className="text-white mb-0"
                                onClick={() =>
                                  handleTextClick(
                                    "companyCity",
                                    "American Fork"
                                  )
                                }
                              >
                                {select.companyCity || "American Fork"},
                              </span>
                            )}

                            {editingField === "companyState" ? (
                              <input
                                type="text"
                                value={editedText} // Controlled input with edited text
                                onChange={(e) => setEditedText(e.target.value)} // Update text as the user types
                                onBlur={handleTextSave}
                                autoFocus
                              />
                            ) : (
                              <span
                                className="text-white mb-0"
                                onClick={() =>
                                  handleTextClick("companyState", "UT")
                                }
                              >
                                {select.companyState || "UT"},
                              </span>
                            )}

                            {editingField === "companyPostCode" ? (
                              <input
                                className="w-100"
                                type="text"
                                value={editedText} // Controlled input with edited text
                                onChange={(e) => setEditedText(e.target.value)} // Update text as the user types
                                onBlur={handleTextSave}
                                autoFocus
                              />
                            ) : (
                              <span
                                className="text-white mb-0"
                                onClick={() =>
                                  handleTextClick("companyPostCode", "84003")
                                }
                              >
                                {select.companyPostCode || "84003"}
                              </span>
                            )}
                          </div>
                        </div>
                      </div>

                      <div className="col-md-6">
                        <ul className="list-unstyled text-end">
                          <img
                            className="print_Profile"
                            src={
                              uploadedContact ||
                              (profile.image ? profile.image : "")
                            } // Show uploaded logo or default avatar
                            style={{
                              width: "100px",
                              cursor: "pointer",
                            }} // Pointer cursor for better UX
                            onClick={handleLogoClickContact} // Show file input on click
                          />
                          {/* Conditionally render file input on logo click */}
                          {showContact && (
                            <label className="documentlabelContact">
                              <input
                                id="REPC_real_estate_purchase_contract"
                                type="file"
                                onChange={handleLogoUploadContact}
                              />
                              <i class="h2 fi-edit opacity-80"></i>
                            </label>
                          )}

                          <div className="d-flex justify-content-end">
                            {editingField === "firstName" ? (
                              <input
                                type="text"
                                value={editedText} // Controlled input with edited text
                                onChange={(e) => setEditedText(e.target.value)} // Update text as the user types
                                onBlur={() =>
                                  handleTextSaveListAgent("firstName")
                                } // Save the text when the user leaves the input field
                                autoFocus
                              />
                            ) : (
                              <p
                                className="text-white mb-0"
                                onClick={() =>
                                  handleTextListAgent(
                                    "firstName",
                                    profile.firstName,
                                    profile.lastName
                                  )
                                }
                              >
                                {profile && (
                                  <p className="mb-0">
                                    <b>Agent: </b>
                                    {profile.firstName}&nbsp;
                                  </p>
                                )}
                              </p>
                            )}

                            {editingField === "lastName" ? (
                              <input
                                type="text"
                                value={editedText} // Controlled input with edited text
                                onChange={(e) => setEditedText(e.target.value)} // Update text as the user types
                                onBlur={() =>
                                  handleTextSaveListAgent("lastName")
                                } // Save the text when the user leaves the input field
                                autoFocus
                              />
                            ) : (
                              <p
                                className="text-white mb-0"
                                onClick={() =>
                                  handleTextListAgent(
                                    "lastName",
                                    profile.lastName
                                  )
                                }
                              >
                                {profile && (
                                  <p className="mb-0">
                                    {" "}
                                    {profile.lastName}&nbsp;
                                  </p>
                                )}
                              </p>
                            )}
                          </div>

                          {editingField === "listOffice" ? (
                            <input
                              type="text"
                              value={editedText} // Controlled input with edited text
                              onChange={(e) => setEditedText(e.target.value)} // Update text as the user types
                              onBlur={() =>
                                handleTextSaveListOffice("listOffice")
                              } // Save the text when the user leaves the input field
                              autoFocus
                            />
                          ) : (
                            <p
                              className="text-white mb-0"
                              onClick={() =>
                                handleTextListOffice(
                                  "listOffice",
                                  profile.active_office
                                )
                              }
                            >
                              {profile && (
                                <p className="mb-0">
                                  <b>Office: </b>
                                  {profile.active_office}
                                </p>
                              )}
                            </p>
                          )}

                          {editingField === "officePhone" ? (
                            <input
                              type="text"
                              value={editedText} // Controlled input with edited text
                              onChange={(e) => setEditedText(e.target.value)} // Update text as the user types
                              onBlur={() =>
                                handleTextSaveOfficePhone("officePhone")
                              } // Save the text when the user leaves the input field
                              autoFocus
                            />
                          ) : (
                            <p
                              className="text-white mb-0"
                              onClick={() =>
                                handleTextOfficePhone(
                                  "officePhone",
                                  profile.phone
                                )
                              }
                            >
                              {profile && (
                                <p className="mb-0">
                                  <b>Phone: </b>
                                  {profile.phone}
                                </p>
                              )}
                            </p>
                          )}
                        </ul>
                      </div>
                    </div>
                  </div>

                  <div className="template_second">
                    <button
                      className="btn btn-secondary pull-right"
                      onClick={() => handleCancel()}
                    >
                      Cancel
                    </button>
                    <PDFDownloadLink
                      document={
                        <FlyerAlternativeThird
                          select={select}
                          uploadedImages={uploadedImages}
                          platformData={uploadedLogo}
                          profile={profile}
                        />
                      }
                      fileName={select.address || "Flyer3.pdf"}
                    >
                      Download Pdf
                    </PDFDownloadLink>
                  </div>
                </section>
              ) : (
                ""
              )}
            {/* </>
          ) : (
            ""
          )} */}
        </div>
      </main>
    </div>
  );
};

export default RealStatePrintPage;

import React, { useState, useEffect } from "react";
import user_service from "../service/user_service";
import { NavLink, useNavigate, useParams } from "react-router-dom";
import Loader from "../../Pages/Loader/Loader.js";
import Toaster from "../../Pages/Toaster/Toaster";
import ReactPaginate from "react-paginate";
import defaultpropertyimage from "../../Components/img/defaultpropertyimage.jpeg";
import jwt from "jwt-decode";

const MyMLSlisting = () => {
  const [loader, setLoader] = useState({ isActive: null });
  const params = useParams();
  const { isActive } = loader;
  const [toaster, setToaster] = useState({
    types: null,
    isShow: null,
    toasterBody: "",
    message: "",
  });
  const { types, isShow, toasterBody, message } = toaster;

  const [getListing, setGetListing] = useState([]);
  const [pageCount, setpageCount] = useState(0);
  const initialValues = {
    listingsAgent: "",
    category: "",
    mlsId: "",
    propertyType: "",
    mlsNumber: "",
    agentId: "",
    associateName: "",
    price: "",
    garage: "",
    bed: "",
    subDivision: "",
    streetAddress: "",
    city: "",
    state: "",
    zipCode: "",
    schoolDistrtict: "",
    country: "",
    areaLocation: "",
    status: "",
    listDate: "",
    image: "",
  };
  const [formValues, setFormValues] = useState(initialValues);
  const [formErrors, setFormErrors] = useState({});
  const [listingtype, setListingtype] = useState({});

  const navigate = useNavigate();

  const SingleListingMLS = (id) => {
    navigate(`/single-listing-mls/${id}`);
  };

  const propertypic = (e) => {
    e.target.src = defaultpropertyimage;
  };

  const ListingGetAll = async (type) => {
    try {
      const agentId = jwt(localStorage.getItem("auth")).id;
  
      if (type === "Active") {
        setListingtype("Active");
      } else {
        setListingtype("Closed");
      }
  
      setLoader({ isActive: true });
  
      const response = await user_service.listingSearchMLSagentid(agentId, type);
      if (response && response.data) {
        setLoader({ isActive: false });
        console.log(response.data);
        setGetListing(response.data.value);
        setpageCount(Math.ceil(response.data.value.length / 10));
      } else {
        console.error("Empty or invalid response from the server");
      }
    } catch (error) {
      setLoader({ isActive: false });
      
      console.error("An error occurred while fetching listings:", error);
      // Handle the error, e.g., display an error message to the user
    }
  };
  
  useEffect(() => {
    window.scrollTo(0, 0);
    // setLoader({ isActive: true });
    ListingGetAll("Active");
  }, []);
  
  return (
    <div className="bg-secondary float-left w-100 pt-4 mb-4">
      <Loader isActive={isActive} />
      {isShow && (
        <Toaster
          types={types}
          isShow={isShow}
          toasterBody={toasterBody}
          message={message}
        />
      )}
      <main className="page-wrapper">
        {/* <!-- Page content--> */}
        {/* <!-- Page container--> */}
        <div className="">
          {/* <!-- Breadcrumb--> */}
          {/* <nav className="mb-3 mb-md-3 pt-md-3" aria-label="Breadcrumb">
            <ol className="breadcrumb breadcrumb-dark">
              <li className="breadcrumb-item">
                <a href="/">Home</a>
              </li>
              <li className="breadcrumb-item">
                <a href="/">Account</a>
              </li>
              <li className="breadcrumb-item active" aria-current="page">
                Property
              </li>
            </ol>
          </nav> */}
          <div className="float-left w-100 d-flex align-items-center justify-content-between mb-4">
            <h3 className="text-white mb-0" id="">
              My Listings
            </h3>
          </div>
          <ul
            className="nav nav-tabs d-flex align-items-center align-items-sm-center justify-content-center mb-4 pb-2"
            role="tablist"
          >
            <li className="nav-item me-sm-3 mb-3" role="presentation">
              <a
                className="nav-link text-center active"
                href="#tab"
                data-bs-toggle="tab"
                role="tab"
                aria-controls="reviews-about-you"
                aria-selected="true"
                onClick={() => ListingGetAll('Active')}
              >
                Active Listings
              </a>
            </li>
            <li className="nav-item mb-3" role="presentation">
              <a
                className="nav-link text-center"
                href="#tab2"
                data-bs-toggle="tab"
                role="tab"
                aria-controls="reviews-by-you"
                aria-selected="false"
                tabindex="-1"
                onClick={() => ListingGetAll('Closed')}
              >
                Recently Sold Listings
              </a>
            </li>
          </ul>
          
          
          {/* <div className="float-left w-100 d-flex align-items-center justify-content-between mb-4">
            <div className="">
                    <a className={listingtype == "Active" ? "btn btn-info btn-lg mb-2 activelistingtype" :"btn btn-info btn-lg mb-2"}  onClick={() => ListingGetAll('Active')}>Active Listing</a>
                    <a className={listingtype == "Closed" ? "btn btn-info btn-lg mb-2 activelistingtype" :"btn btn-info btn-lg mb-2"}  onClick={() => ListingGetAll('Closed')}>Sold Listing</a>
            </div>
          </div> */}


          <div className="">
          <div className="tns-carousel-wrapper tns-controls-outside-xxl tns-nav-outside tns-nav-outside-flush mx-n2">
            <div className="tns-outer" id="tns2-ow">
              <div
                className="tns-liveregion tns-visually-hidden"
                aria-live="polite"
                aria-atomic="true"
              >
                slide <span className="current">9 to 12</span> of 5
              </div>
              <div id="tns2-mw" className="tns-ovh">
                <div className="tns-inner" id="tns2-iw">
                  <div
                    className="tns-carousel-inner row gx-4 mx-0 pt-3 pb-4  tns-slider tns-carousel tns-subpixel tns-calc tns-horizontal"
                    data-carousel-options='{"items": 4, "responsive": {"0":{"items":1},"500":{"items":2},"768":{"items":3},"992":{"items":4}}}'
                    id="tns2">
                    
                    {
                      getListing && getListing.length > 0 ?
                    getListing.map((post) => (
                      <>
                        <div
                          className="col-lg-3 col-md-6 col-sm-6 col-12 tns-item tns-slide-cloned tns-slide-active p-3"
                          onClick={() => SingleListingMLS(post.ListingKeyNumeric)}
                        >
                          <div className="card shadow-sm card-hover cursor-pointer border-0 h-100">
                            <div className="card-img-top card-img-hover">
                              {/* <div className="position-absolute start-0 top-0 pt-3 ps-3"><span className="d-table badge bg-success mb-1">Verified</span><span className="d-table badge bg-info">New</span></div> */}
                              <div className="content-overlay end-0 top-0 pt-3 pe-3">
                                {/* <button className="btn btn-icon btn-light btn-xs text-primary rounded-circle"
                                   type="button" data-bs-toggle="tooltip" data-bs-placement="left" 
                                   aria-label="Add to Wishlist" data-bs-original-title="Add to Wishlist"><i className="fi-heart"></i></button> */}
                              </div>
                              <img
                                className="img-fluid w-100 rounded-0"
                                src={
                                  post?.Media && post?.Media
                                    ? post?.Media[0]?.MediaURL
                                    : defaultpropertyimage
                                }
                                alt="Property"
                                onError={(e) => propertypic(e)}
                              />
                            </div>
                            <div className="card-body position-relative pb-3">
                              <h4 className="mb-2 fs-xs fw-normal text-uppercase text-primary">
                                {post.MlsStatus}
                              </h4>

                              <h3 className="h6 mb-0 fs-base">
                                <i className="fi-cash mt-n1 me-2 lead align-middle opacity-70"></i>
                                $
                                {post.ListPrice ? parseFloat(post.ListPrice).toLocaleString("en-US") : ""}
                              </h3>
                              <p className="mb-1">
                                <small>
                                  <span>
                                    {
                                      post.BedroomsTotal
                                    }
                                    &nbsp;beds
                                  </span>
                                  &nbsp;*&nbsp;
                                  <span>
                                    {post.BathroomsFull ?? 0 }{" "}
                                    &nbsp;bath
                                  </span>
                                  &nbsp;*&nbsp;
                                  <span>
                                    {post.BuildingAreaTotal }
                                    &nbsp;Sqft
                                  </span>
                                </small>
                              </p>
                              <p className="mb-1 fs-sm text-muted">
                                {post.UnparsedAddress}
                              </p>

                              <p className="mb-0 fs-sm text-muted">
                                MLS#: {post.ListingKeyNumeric}
                              </p>
                            </div>
                          </div>
                        </div>
                      </>
                    ))
                    :<p className="text-center text-white">Not any listing available.</p>
                    }
                  </div>
                </div>
              </div>
            </div>
          </div>
          </div>
        </div>
      </main>
    </div>
  );
};

export default MyMLSlisting;

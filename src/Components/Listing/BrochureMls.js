import React, { useState, useRef, useEffect } from "react";
import { NavLink, useNavigate, useParams } from "react-router-dom";
import user_service from "../service/user_service";
import jwt from "jwt-decode";
import _ from "lodash";
import Loader from "../../Pages/Loader/Loader.js";
import Toaster from "../../Pages/Toaster/Toaster.js";
import TypeOne from "../img/BrchureMls.png";
import TypeTwo from "../img/type2.png";
import TypeThree from "../img/type3.png";
import defaultpropertyimage from "../../Components/img/defaultpropertyimage.jpeg";

import Logo from "../img/logo.png";
import {
  Document,
  Page,
  Text,
  Image,
  StyleSheet,
  PDFDownloadLink,
  View,
} from "@react-pdf/renderer";

const BrochureMls = () => {
  const [membership, setMembership] = useState("");
  const componentRef = useRef();

  const [loader, setLoader] = useState({ isActive: null });
  const { isActive } = loader;
  const [toaster, setToaster] = useState({
    types: null,
    isShow: null,
    toasterBody: "",
    message: "",
  });
  const { types, isShow, toasterBody, message } = toaster;

  const [searchmlsnumber, setSearchmlsnumber] = useState({ mlsNumber: "" });

  const [select, setSelect] = useState("");

  useEffect(() => {
    window.scrollTo(0, 0);
    ContactGetById(jwt(localStorage.getItem("auth")).id);
    PlatformdetailsGet();
  }, []);

  const handleChangesearchmls = (event) => {
    setSearchmlsnumber({ mlsnumber: event.target.value });
  };

  const [templateType, setTemplateType] = useState();
  const [isLoading, setIsLoading] = useState(false);

  const handleType = (input) => {
    const templateType = input.target ? input.target.value : input;
    setTemplateType(templateType);
  };

  const API_BASE_URL = "https://api.brokeragentbase.com";
  const MLSSearch = async () => {
    if (searchmlsnumber.mlsnumber) {
      try {
        setIsLoading(true);
        let response;

        if (membership === "UtahRealEstate.com") {
          response = await user_service.listingSearchMLS(
            searchmlsnumber.mlsnumber
          );
        } else if (membership === "Park City") {
          response = await user_service.listingSearchMLSspark(
            searchmlsnumber.mlsnumber,
            "parkcity"
          );
        } else if (membership === "Washington County") {
          response = await user_service.listingSearchMLSspark(
            searchmlsnumber.mlsnumber,
            "washingtoncounty"
          );
        } else if (membership === "Iron County") {
          response = await user_service.listingSearchMLSspark(
            searchmlsnumber.mlsnumber,
            "ironcounty"
          );
        } else {
          response = await user_service.listingSearchMLS(
            searchmlsnumber.mlsnumber
          );
        }

        if (response && response.data) {
          const propertyData = response.data;

          propertyData.media = (propertyData.media || []).map((image) => ({
            MediaURL: image?.MediaURL
              ? `${API_BASE_URL}/proxy-image?url=${encodeURIComponent(
                  image.MediaURL
                )}`
              : defaultpropertyimage,
            description: image?.description || "Click to add a description",
          }));
          setSelect(propertyData);
          setToaster({
            types: "Success",
            isShow: true,
            message: "MLS search successful!",
          });
        } else {
          setToaster({
            types: "Error",
            isShow: true,
            message: "No data found for the provided MLS number.",
          });
        }

        setIsLoading(false);
      } catch (error) {
        console.error("Error in MLSSearch:", error);
        setToaster({
          types: "Error",
          isShow: true,
          message: "Please provide a valid MLS number.",
        });
      } finally {
        setIsLoading(false);
        setTimeout(() => {
          setToaster((prevToaster) => ({ ...prevToaster, isShow: false }));
        }, 2000);
      }
    } else {
      setToaster({
        types: "Error",
        isShow: true,
        message: "Please provide a valid MLS number.",
      });
      setTimeout(() => {
        setToaster((prevToaster) => ({ ...prevToaster, isShow: false }));
      }, 2000);
    }
  };

  const [getContact, setGetContact] = useState("");
  const [mls_membership, setMls_membership] = useState([]);
  const [
    commaSeparatedValuesmls_membership,
    setCommaSeparatedValuesmls_membership,
  ] = useState("");

  const ContactGetById = async () => {
    await user_service
      .contactGetById(jwt(localStorage.getItem("auth")).id)
      .then((response) => {
        if (response) {
          setGetContact(response.data);
          if (response.data && response.data.additionalActivateFields) {
            const mls_membershipString =
              response.data.additionalActivateFields?.mls_membership;
            if (mls_membershipString) {
              if (typeof mls_membershipString === "string") {
                try {
                  const mls_membershipParsed = JSON.parse(mls_membershipString);
                  if (mls_membershipParsed == "") {
                    setMls_membership(mls_membership);
                  } else {
                    setMls_membership(mls_membershipParsed);
                  }
                  if (mls_membershipParsed == "") {
                    setCommaSeparatedValuesmls_membership("");
                  } else {
                    const boardMembershipValues = mls_membershipParsed.map(
                      (item) => item.value
                    );

                    setCommaSeparatedValuesmls_membership(
                      boardMembershipValues
                    );
                  }
                } catch (error) {
                  console.error("Error parsing mls_membership as JSON:", error);
                  setCommaSeparatedValuesmls_membership("");
                }
              } else {
                setCommaSeparatedValuesmls_membership("");
              }
            } else {
              setCommaSeparatedValuesmls_membership("");
            }
          }
        }
      });
  };

  const [editingField, setEditingField] = useState(null);
  const [editingText, setEditingText] = useState(null);
  const [editedText, setEditedText] = useState("");
  const [selectedImage, setSelectedImage] = useState(null);
  const [uploadedImages, setUploadedImages] = useState({});

  const handleImageClick = (imageIndex) => {
    setSelectedImage(imageIndex);
  };

  // Handle image upload
  const handleImageUpload = (e, imageIndex) => {
    const file = e.target.files[0];
    if (file) {
      const reader = new FileReader();
      reader.onload = (upload) => {
        // Update the uploaded image state
        setUploadedImages((prev) => ({
          ...prev,
          [imageIndex]: upload.target.result,
        }));

        // Ensure that `media` is defined before updating it
        setSelect((prevSummary) => {
          const updatedImages = [...(prevSummary.media || [])]; // Handle case where media is undefined
          updatedImages[imageIndex] = { MediaURL: upload.target.result }; // Update the image at the specific index
          return { ...prevSummary, media: updatedImages };
        });
      };
      reader.readAsDataURL(file);
    }
    setSelectedImage(null);
  };

  const handleTextClick = (field, currentValue) => {
    setEditingField(field); // Set the field being edited
    setEditedText(currentValue); // Pre-fill input with current value (address)
  };

  // Function to handle saving the edited text locally
  const handleTextSave = (field) => {
    // Update the selected value locally after editing
    setSelect((prevSelect) => ({
      ...prevSelect,
      value: [
        {
          ...prevSelect.value[0],
          UnparsedAddress: editedText, // Update the address with the new value
        },
      ],
    }));

    setEditingField(null); // Close the input field after saving
  };

  
 
  const handleTextPrice = (field, currentValue) => {
    setEditingField(field); // Set the field being edited
    setEditedText(currentValue); // Pre-fill input with current value (address)
  };

  const handleTextSavePrice = (field) => {
    // Update the selected value locally after editing
    setSelect((prevSelect) => ({
      ...prevSelect,
      value: [
        {
          ...prevSelect.value[0],
          ListPrice: editedText, // Update the address with the new value
        },
      ],
    }));

    setEditingField(null); // Close the input field after saving
  };

  const handleTextPublicRemarks = (field, currentValue) => {
    setEditingField(field); // Set the field being edited
    setEditedText(currentValue); // Pre-fill input with current value (address)
  };

  const handleTextSavePublicRemarks = (field) => {
    setSelect((prevSelect) => ({
      ...prevSelect,
      value: [
        {
          ...prevSelect.value[0],
          PublicRemarks: editedText, // Update the address with the new value
        },
      ],
    }));

    setEditingField(null); // Close the input field after saving
  };

  const handleTextBathroom = (field, currentValue) => {
    setEditingField(field); // Set the field being edited
    setEditedText(currentValue); // Pre-fill input with current value (address)
  };

  const handleTextSaveBathroom = (field) => {
    // Update the selected value locally after editing
    setSelect((prevSelect) => ({
      ...prevSelect,
      value: [
        {
          ...prevSelect.value[0],
          BathroomsFull: editedText, // Update the address with the new value
        },
      ],
    }));

    setEditingField(null); // Close the input field after saving
  };

  const handleTextBedrooms = (field, currentValue) => {
    setEditingField(field); // Set the field being edited
    setEditedText(currentValue); // Pre-fill input with current value (address)
  };

  const handleTextSaveBedrooms = (field) => {
    // Update the selected value locally after editing
    setSelect((prevSelect) => ({
      ...prevSelect,
      value: [
        {
          ...prevSelect.value[0],
          BedroomsTotal: editedText, // Update the address with the new value
        },
      ],
    }));

    setEditingField(null); // Close the input field after saving
  };

  const handleTextSquareFeet = (field, currentValue) => {
    setEditingField(field); // Set the field being edited
    setEditedText(currentValue); // Pre-fill input with current value (address)
  };

  const handleTextSaveSquareFeet = (field) => {
    // Update the selected value locally after editing
    setSelect((prevSelect) => ({
      ...prevSelect,
      value: [
        {
          ...prevSelect.value[0],
          BuildingAreaTotal: editedText, // Update the address with the new value
        },
      ],
    }));

    setEditingField(null); // Close the input field after saving
  };

  const handleTextLotSize = (field, currentValue) => {
    setEditingField(field); // Set the field being edited
    setEditedText(currentValue); // Pre-fill input with current value (address)
  };

  const handleTextSaveLotSize = (field) => {
    // Update the selected value locally after editing
    setSelect((prevSelect) => ({
      ...prevSelect,
      value: [
        {
          ...prevSelect.value[0],
          LotSizeAcres: editedText, // Update the address with the new value
        },
      ],
    }));

    setEditingField(null); // Close the input field after saving
  };

  const handleTextYearBuilt = (field, currentValue) => {
    setEditingField(field); // Set the field being edited
    setEditedText(currentValue); // Pre-fill input with current value (address)
  };

  const handleTextSaveYearBuilt = (field) => {
    // Update the selected value locally after editing
    setSelect((prevSelect) => ({
      ...prevSelect,
      value: [
        {
          ...prevSelect.value[0],
          YearBuilt: editedText, // Update the address with the new value
        },
      ],
    }));

    setEditingField(null); // Close the input field after saving
  };

  const handleTextHeating = (field, currentValue) => {
    setEditingField(field); // Set the field being edited
    setEditedText(currentValue); // Pre-fill input with current value (address)
  };

  const handleTextSaveHeating = (field) => {
    setSelect((prevSelect) => ({
      ...prevSelect,
      value: [
        {
          ...prevSelect.value[0],
          Heating: editedText, // Update the address with the new value
        },
      ],
    }));
    setEditingField(null); // Close the input field after saving
  };

  const handleTextColing = (field, currentValue) => {
    setEditingField(field); // Set the field being edited
    setEditedText(currentValue); // Pre-fill input with current value (address)
  };

  const handleTextSaveCooling = (field) => {
    setSelect((prevSelect) => ({
      ...prevSelect,
      value: [
        {
          ...prevSelect.value[0],
          Cooling: editedText,
        },
      ],
    }));
    setEditingField(null); // Close the input field after saving
  };

  const handleTextGarageSpaces = (field, currentValue) => {
    setEditingField(field); // Set the field being edited
    setEditedText(currentValue); // Pre-fill input with current value (address)
  };

  const handleTextSaveGarageSpaces = (field) => {
    setSelect((prevSelect) => ({
      ...prevSelect,
      value: [
        {
          ...prevSelect.value[0],
          GarageSpaces: editedText,
        },
      ],
    }));
    setEditingField(null); // Close the input field after saving
  };

  const handleTextRoof = (field, currentValue) => {
    setEditingField(field); // Set the field being edited
    setEditedText(currentValue); // Pre-fill input with current value (address)
  };

  const handleTextSaveRoof = (field) => {
    setSelect((prevSelect) => ({
      ...prevSelect,
      value: [
        {
          ...prevSelect.value[0],
          Roof: editedText,
        },
      ],
    }));
    setEditingField(null); // Close the input field after saving
  };

  const handleTextListAgent = (field, currentValue) => {
    setEditingField(field); // Set the field being edited
    setEditedText(currentValue); // Set the current text value in the input
  };

  // Handle saving the edited text for the list agent
  const handleTextSaveListAgent = (field) => {
    setProfile((prevProfile) => ({
      ...prevProfile,
      [field]: editedText, // Dynamically update the appropriate field (firstName or lastName)
    }));
    setEditingField(null); // Close the input field after saving
  };

  const handleTextListOffice = (field, currentValue) => {
    setEditingField(field); // Set the field being edited
    setEditedText(currentValue); // Pre-fill input with current value (address)
  };

  const handleTextSaveListOffice = (field) => {
    // Update the selected value locally after editing
    setProfile((prevProfile) => ({
      ...prevProfile,
      active_office: editedText, // Dynamically update the appropriate field (firstName or lastName)
    }));

    setEditingField(null); // Close the input field after saving
  };

  const handleTextOfficePhone = (field, currentValue) => {
    setEditingField(field); // Set the field being edited
    setEditedText(currentValue); // Pre-fill input with current value (address)
  };

  const handleTextSaveOfficePhone = (field) => {
    // Update the selected value locally after editing
    setProfile((prevProfile) => ({
      ...prevProfile,
      phone: editedText, // Dynamically update the appropriate field (firstName or lastName)
    }));

    setEditingField(null); // Close the input field after saving
  };

  const handleTextPublished = (field, currentValue) => {
    setEditingField(field); // Set the field being edited
    setEditedText(currentValue); // Pre-fill input with current value (address)
  };

  const handleTextSavePublished = (field) => {
    // Update the selected value locally after editing
    setSelect((prevSelect) => ({
      ...prevSelect,
      value: [
        {
          ...prevSelect.value[0],
          SourceSystemName: editedText, // Update the address with the new value
        },
      ],
    }));

    setEditingField(null); // Close the input field after saving
  };

  const handleTextCompanyCity = (field, currentValue) => {
    setEditingField(field); // Set the field being edited
    setEditedText(currentValue); // Pre-fill input with current value (address)
  };

  const handleTextSaveCompanyCity = (field) => {
    // Update the selected value locally after editing
    setSelect((prevSelect) => ({
      ...prevSelect,
      value: [
        {
          ...prevSelect.value[0],
          City: editedText, // Update the address with the new value
        },
      ],
    }));

    setEditingField(null); // Close the input field after saving
  };

  const handleTextCompanyState = (field, currentValue) => {
    setEditingField(field); // Set the field being edited
    setEditedText(currentValue); // Pre-fill input with current value (address)
  };

  const handleTextSaveCompanyState = (field) => {
    // Update the selected value locally after editing
    setSelect((prevSelect) => ({
      ...prevSelect,
      value: [
        {
          ...prevSelect.value[0],
          StateOrProvince: editedText, // Update the address with the new value
        },
      ],
    }));

    setEditingField(null); // Close the input field after saving
  };

  const handleTextCompanyPostCode = (field, currentValue) => {
    setEditingField(field); // Set the field being edited
    setEditedText(currentValue); // Pre-fill input with current value (address)
  };

  const handleTextSaveCompanyPostCode = (field) => {
    // Update the selected value locally after editing
    setSelect((prevSelect) => ({
      ...prevSelect,
      value: [
        {
          ...prevSelect.value[0],
          PostalCode: editedText, // Update the address with the new value
        },
      ],
    }));

    setEditingField(null); // Close the input field after saving
  };

  const [platformData, setPlatFormData] = useState({});
  const [uploadedLogo, setUploadedLogo] = useState(null);
  const [showLogoInput, setShowLogoInput] = useState(false); // State to show or hide input field
  const [profile, setProfile] = useState({});

  const [uploadedContact, setUploadedContact] = useState(null);
  const [showContact, setShowContact] = useState(false);

  const PlatformdetailsGet = async () => {
    await user_service.platformdetailsGet().then((response) => {
      if (response) {
        setPlatFormData(response.data.data[0]);
      }
    });
  };

  const profileGetAll = async () => {
    if (localStorage.getItem("auth")) {
      try {
        const response = await user_service.profileGet(
          jwt(localStorage.getItem("auth")).id
        );
        if (response) {
          const profileData = response.data;
          if (profileData && profileData.image) {
            const proxyImageUrl = `${API_BASE_URL}/proxy-image?url=${encodeURIComponent(
              profileData.image
            )}`;

            // Assuming 'setProfile' is a function that updates the state with the modified profile data
            setProfile({
              ...profileData, // Copy existing profile data
              image: proxyImageUrl, // Replace original image with the proxy URL
            });
          } else {
            setProfile(profileData); // No image to proxy, just set the profile as is
          }
        }
      } catch (error) {
        console.error("Error fetching profile data:", error);
      }
    }
  };

  useEffect(() => {
    window.scrollTo(0, 0);
    PlatformdetailsGet();
    profileGetAll();
  }, []);

  // Handle logo upload

  const handleLogoUploadContact = (e) => {
    const file = e.target.files[0];
    if (file) {
      const reader = new FileReader();
      reader.onload = (upload) => {
        setUploadedContact(upload.target.result); // Store the uploaded logo
        setShowContact(false); // Hide the input after upload
        // Update platformData with the new logo while preserving the rest of the data
        setProfile((prevData) => ({
          ...prevData,
          image: upload.target.result || prevData.image, // Update the logo in platformData
        }));
      };
      reader.readAsDataURL(file); // Read the file as a data URL for image preview
    }
  };

  const handleLogoUpload = (e) => {
    const file = e.target.files[0];
    if (file) {
      const reader = new FileReader();
      reader.onload = (upload) => {
        setUploadedLogo(upload.target.result); // Store the uploaded logo
        setShowLogoInput(false); // Hide the input after upload
        // Update platformData with the new logo while preserving the rest of the data
        setPlatFormData((prevData) => ({
          ...prevData,
          platform_logo: upload.target.result, // Update the logo in platformData
        }));
      };
      reader.readAsDataURL(file); // Read the file as a data URL for image preview
    }
  };

  // Handle logo click to show the input
  const handleLogoClick = () => {
    setShowLogoInput(true); // Show the file input when the logo is clicked
  };

  const handleLogoClickContact = () => {
    setShowContact(true); // Show the file input when the logo is clicked
  };

  const styles = StyleSheet.create({
    page: {
      backgroundColor: "#b4a3a359",
      padding: 10,
    },

    pageThird: {
      backgroundColor: "#b4a3a359",
      padding: 10,
      display: "flex",
      flexDirection: "column",
      height: 200,
      boxSizing: "border-box",
    },

    rowContainer: {
      display: "flex",
      flexDirection: "row",
      justifyContent: "space-between",
      width: "100%",
    },

    mainpic: {
      width: "100%",
      height: 400,
      objectFit: "cover",
      marginBottom: 10,
      marginTop: 30,
    },
    newBox: {
      width: "100%",
      position: "relative",
      height: 300,
      backgroundColor: "#2f251a",
    },

    detailFirstPage: {
      position: "absolute",
      bottom: 0,
    },

    detailOverview: {
      fontSize: 12,
      color: "#917462",
      textAlign: "justify",
      marginTop: 10,
    },

    longboxSubHeaderNew: {
      fontSize: 14,
      color: "#fff",
      margin: 10,
    },

    priceBrochure: {
      fontSize: 12,
      color: "#fff",
      marginLeft: 10,
    },

    flyerContainer: {
      display: "flex",
      flexWrap: "wrap",
      flexDirection: "row",
      justifyContent: "space-between",
    },
    imageWrapper: {
      width: "48%",
      marginBottom: 20,
      border: "1px solid #ddd",
      padding: 5,
    },
    flyerImage: {
      width: "100%",
      height: 200,
      objectFit: "cover",
    },
    description: {
      marginTop: 8,
      fontSize: 10,
      color: "#333",
    },

    row: {
      flexDirection: "row",
      flexWrap: "wrap",
      width: "100%",
      marginTop: 40,
    },

    col: {
      width: "50%", // This creates a col-md-6 structure
      padding: 10,
    },

    mainImage: {
      width: "100%",
      height: 400,
      objectFit: "cover",
      borderRadius: 10,
      border: "1px solid #ddd",
    },

    propertyDetailsContainer: {
      backgroundColor: "#fff",
      borderRadius: 10,
      padding: 10,
      height: 400,
      border: "1px solid #ddd",
    },

    propertyHeading: {
      fontSize: 14,
      fontWeight: "bold",
      marginBottom: 10,
      marginTop: 5,
      textAlign: "center",
    },

    propertyDetailsBackground: {
      backgroundColor: "#2e3339",
      borderRadius: 10,
      padding: 10,
    },

    detailItem: {
      flexDirection: "row",
      justifyContent: "space-between",
      marginBottom: 5,
    },

    detailLabel: {
      color: "#ffffff",
      fontSize: 12,
      fontWeight: "bold",
    },

    detailValue: {
      color: "#ffffff",
      fontSize: 12,
    },

    descriptionSection: {
      marginTop: 5,
    },

    descriptionText: {
      marginTop: 10,
      fontSize: 12,
      textAlign: "justify",
    },

    // contact section
    addressBox: {
      width: "100%",
      backgroundColor: getComputedStyle(
        document.documentElement
      ).getPropertyValue("--branding-footer-color"), // Dynamically get CSS variable value
      padding: 10,
      marginTop: 40,
      borderRadius: 5,
      display: "flex",
      flexDirection: "row",
      justifyContent: "space-between",
      // marginBottom:0,
    },

    officeSection: {
      display: "flex",
      // justifyContent : "flex-start",
      marginBottom: 5,
    },

    officeContactImage: {
      width: 60,
      height: 60,
      borderRadius: 50,
      marginLeft: 10,
    },
    officeAddress: {
      width: "50%",
      alignItems: "flex-end",
      marginBottom: 5,
    },

    officeDetails: {
      marginLeft: 10 /* Add space between image and text */,
    },
    officeText: {
      fontSize: 10,
      color: "#ffffff",
      // textTransform: "uppercase",
      marginBottom: 2,
    },

    officeAddressTest: {
      fontSize: 12,
      color: "#000000",
      marginBottom: 2,
    },

    contactHeading: {
      fontSize: 18,
      fontWeight: "bold",
      color: "#00000",
      textAlign: "center",
      marginBottom: 10,
    },

    propertyHeadingFirst: {
      marginTop: 5,
      fontSize: 18,
      fontWeight: "bold",
      color: "#00000",
      textAlign: "center",
      marginBottom: 10,
    },

    newBoxFourthPage: {
      width: "100%",
      position: "relative",
      height: 300,
      backgroundColor: "#f85e00",
    },

    officeLogo: {
      width: 100,
      objectFit: "contain",
      marginBottom: 10,
    },
    logoCol: {
      width: "48%", // Slightly smaller width for the logo column
    },
  });

  const Flyer = ({ select, platformData, profile }) => {
    const placeholderImage = defaultpropertyimage;
    return (
      <Document>
        <Page size="A4" style={styles.page}>
          <View style={styles.rowContainer}>
            {select?.media[0]?.MediaURL && (
              <Image style={styles.mainpic} src={select.media[0].MediaURL} />
            )}
          </View>

          <View style={styles.rowContainer}>
            <View style={styles.newBox}>
              <View style={styles.detailOverview}>
                <Text style={styles.longboxSubHeaderNew}>
                  {select?.media[0]?.description ||
                    "Click to add a description"}
                </Text>
              </View>
              <View style={styles.detailFirstPage}>
                <Text style={styles.priceBrochure}>
                  Listed at ${select?.value[0]?.ListPrice || "X00,00"}
                </Text>
                <Text style={styles.longboxSubHeaderNew}>
                  {select?.value[0]?.UnparsedAddress || "2024 Demo St"}
                </Text>
              </View>
            </View>
          </View>
        </Page>

        <Page size="A4" style={styles.page}>
          <View style={styles.flyerContainer}>
            {select?.media && select.media.length > 1
              ? [
                  ...select.media.slice(1, 7),
                  ...Array(
                    Math.max(
                      0,
                      6 - select.media.slice(1, 7).length // Calculate remaining slots for default images
                    )
                  ).fill({ MediaURL: defaultpropertyimage, description: "" }), // Fill with default images
                ].map((mediaItem, mediaIndex) => (
                  <View key={mediaIndex} style={styles.imageWrapper}>
                    <Image
                      source={{
                        uri: mediaItem?.MediaURL || defaultpropertyimage,
                      }} // Default to placeholder if MediaURL is empty
                      style={styles.flyerImage}
                      key={mediaIndex}
                    />
                    <Text style={styles.description}>
                      {mediaItem?.description || "Click to add a description"}{" "}
                      {/* Default description */}
                    </Text>
                  </View>
                ))
              : // Default fallback for when no media exists or condition is not met
                Array(6)
                  .fill()
                  .map((_, defaultIndex) => (
                    <View key={defaultIndex} style={styles.imageWrapper}>
                      <Image
                        source={{ uri: defaultpropertyimage }} // Default to placeholder if MediaURL is empty
                        style={styles.flyerImage}
                        key={defaultIndex}
                      />
                    </View>
                  ))}
          </View>
        </Page>

        <Page size="A4" style={styles.pageThird}>
          <View style={styles.row}>
            {/* Left Column: Image */}
            <View style={styles.col}>
              <Image
                style={styles.mainImage}
                src={select?.media[7]?.MediaURL || placeholderImage}
              />
            </View>

            {/* Right Column: Property Details */}
            <View style={styles.col}>
              <View style={styles.propertyDetailsContainer}>
                <Text style={styles.propertyHeading}>Property Details</Text>
                <View style={styles.propertyDetailsBackground}>
                  <View style={styles.detailItem}>
                    <Text style={styles.detailLabel}>Bedrooms</Text>
                    <Text style={styles.detailValue}>
                      {select?.value[0]?.BedroomsTotal || "0"}
                    </Text>
                  </View>
                  <View style={styles.detailItem}>
                    <Text style={styles.detailLabel}>Bathrooms</Text>
                    <Text style={styles.detailValue}>
                      {select?.value[0]?.BathroomsFull || "0"}
                    </Text>
                  </View>
                  <View style={styles.detailItem}>
                    <Text style={styles.detailLabel}>Sqft</Text>
                    <Text style={styles.detailValue}>
                      {select?.value[0]?.BuildingAreaTotal || "X,000 Sq.Ft."}
                    </Text>
                  </View>
                  <View style={styles.detailItem}>
                    <Text style={styles.detailLabel}>Lot Size</Text>
                    <Text style={styles.detailValue}>
                      {select?.value[0]?.LotSizeAcres || "0"}
                    </Text>
                  </View>
                  <View style={styles.detailItem}>
                    <Text style={styles.detailLabel}>Year Built</Text>
                    <Text style={styles.detailValue}>
                      {select?.value[0]?.YearBuilt || "2024"}
                    </Text>
                  </View>

                  <View style={styles.detailItem}>
                    <Text style={styles.detailLabel}>Heating</Text>
                    <Text style={styles.detailValue}>
                      {select?.value[0]?.Heating || "Gas"}
                    </Text>
                  </View>

                  <View style={styles.detailItem}>
                    <Text style={styles.detailLabel}>Cooling</Text>
                    <Text style={styles.detailValue}>
                      {select?.value[0]?.Cooling || "Central AC"}
                    </Text>
                  </View>

                  <View style={styles.detailItem}>
                    <Text style={styles.detailLabel}>Garage</Text>
                    <Text style={styles.detailValue}>
                      {select?.value[0]?.GarageSpaces || "X car"}
                    </Text>
                  </View>

                  <View style={styles.detailItem}>
                    <Text style={styles.detailLabel}>Roof</Text>
                    <Text style={styles.detailValue}>
                      {select?.value[0]?.Roof || "Tile"}
                    </Text>
                  </View>
                </View>
                <View style={styles.descriptionSection}>
                  <Text style={styles.descriptionText}>
                    {select?.media[7]?.description ||
                      "Click to add a description"}
                  </Text>
                </View>
              </View>
            </View>

            {/* contact section */}
            <View style={styles.addressBox}>
              <View style={styles.officeSection}>
                <View>
                  <Image
                    style={styles.officeContactImage}
                    src={profile.image} // Compressed profile image
                  />
                </View>
                <View style={styles.officeDetails}>
                  <Text style={styles.officeText}>
                    Agent: {profile?.firstName} {profile?.lastName}
                  </Text>
                  <Text style={styles.officeText}>
                    Office: {profile?.active_office}
                  </Text>
                  <Text style={styles.officeText}>Phone: {profile?.phone}</Text>
                </View>
              </View>
              <View style={styles.officeAddress}>
                <View style={styles.logoCol}>
                  <Image style={styles.officeLogo} src={platformData || Logo} />
                </View>
                <Text style={styles.officeText}>
                  {select?.value[0]?.SourceSystemName || "UtahRealEstate.com"}
                </Text>
                <Text style={styles.officeText}>
                  {select?.value[0]?.UnparsedAddress || "2024 Demo St"}
                </Text>
                <Text style={styles.officeText}>
                  {select?.value[0]?.City || "American Fork"},&nbsp;
                  {select?.value[0]?.StateOrProvince || "UT"}&nbsp;
                  {select?.value[0]?.PostalCode || "84003"}
                </Text>
              </View>
            </View>
          </View>
        </Page>

        <Page size="A4" style={styles.page}>
          <View style={styles.rowContainer}>
            <Image
              style={styles.mainpic}
              src={select?.media[8]?.MediaURL || placeholderImage}
            />
          </View>
          <View style={styles.rowContathinerFourth}>
            <View style={styles.newBoxFourthPage}>
              <View style={styles.detailOverview}>
                <Text style={styles.longboxSubHeaderNew}>
                  {select?.media[8]?.description ||
                    "Click to add a description"}
                </Text>
              </View>
            </View>
          </View>
        </Page>
      </Document>
    );
  };

  const handleCancel = () => {
    setSelect("");
  };

  const navigate = useNavigate();

  const handleNonListed = () => {
    navigate("/real-estate-non-listed-print");
  };

  const [nextStep, setNextStep] = useState("1");
  const handleNext = (newStep) => {
    setNextStep(newStep);
  };

  const wordlimit = (e) => {
    const wordCount = e.target.value.length;
    console.log(wordCount, "wordCount");
    if (wordCount < 800) {
      setEditedText(e.target.value);
    }
  };

  const wordlimitSecond = (e) => {
    const wordCount = e.target.value.length;
    console.log(wordCount, "wordCount");
    if (wordCount < 150) {
      setEditedText(e.target.value);
    }
  };

  const handleTextSaveBrochure = (index) => {
    setSelect((prevSelect) => ({
      ...prevSelect,
      media: prevSelect.media.map((image, i) =>
        i === index ? { ...image, description: editedText } : image
      ),
    }));
    setEditingField(null);
  };

  return (
    <div className="bg-secondary float-left w-100 pt-4 mb-4">
      <Loader isActive={isActive} />

      {isShow && (
        <Toaster
          types={types}
          isShow={isShow}
          toasterBody={toasterBody}
          message={message}
        />
      )}
      <main className="page-wrapper">
        <div className="row event-calender">
          {select ? (
            <div className="">
              {templateType === "type1" ? (
                <div className="">
                  {select && select.value && select.value.length > 0 && (
                    <section className="mb-5 pb-3 w-100" data-simplebar>
                      <div
                        className="bg-light border rounded-3 p-3"
                        data-thumbnails="true"
                      >
                        <div className="firstTemplate">
                          {/* Step 1 */}
                          {nextStep === "1" && (
                            <div className="row" data-thumbnails="true">
                              <div className="col-md-12">
                                <div className="col-md-12 singleListing">
                                  <div className="card-img-top brochure">
                                    <img
                                      className="w-100"
                                      style={{
                                        borderRadius: "0px",
                                        height: "500px",
                                        objectFit: "cover",
                                      }}
                                      src={
                                        uploadedImages[0] ||
                                        select.media[0].MediaURL ||
                                        defaultpropertyimage
                                      }
                                      alt="Property"
                                      onClick={() => handleImageClick(0)}
                                    />
                                    {selectedImage === 0 && (
                                      <label className="documentlabelNew">
                                        <input
                                          id="REPC_real_estate_purchase_contract"
                                          type="file"
                                          onChange={(e) =>
                                            handleImageUpload(e, 0)
                                          }
                                        />
                                        <i className="h2 fi-edit opacity-80"></i>
                                      </label>
                                    )}
                                  </div>
                                </div>
                                <div className="col-md-12 w-100 mt-1">
                                  <div className="card-img-top brochureListing p-3">
                                    {editingField === `description-0` ? ( // Target description for index 8
                                      <textarea
                                        className="form-control mt-0 w-100"
                                        id="textarea-input-8" // Unique ID for index 8
                                        rows="3"
                                        value={editedText}
                                        onChange={(e) => wordlimit(e)}
                                        onBlur={() => handleTextSaveBrochure(0)} // Save description for index 1
                                        autoFocus
                                      />
                                    ) : (
                                      <h2
                                        className="text-white description_PrintPage"
                                        style={{
                                          color: "white",
                                          // fontFamily: "circular",
                                          // fontStyle: "italic",
                                          fontSize: "16px",
                                        }}
                                        onClick={() =>
                                          handleTextClick(
                                            `description-0`,
                                            select?.media[0]?.description || ""
                                          )
                                        }
                                      >
                                        {select?.media[0]?.description ||
                                          "Click to add a description"}{" "}
                                      </h2>
                                    )}

                                    <div className="brochure_FirstPage">
                                      {/* Price Field */}
                                      {editingField === "price" ? (
                                        <div className="text-center">
                                          <input
                                            type="text"
                                            value={editedText}
                                            onChange={(e) =>
                                              setEditedText(e.target.value)
                                            }
                                            onBlur={() =>
                                              handleTextSavePrice("price")
                                            }
                                            autoFocus
                                          />
                                        </div>
                                      ) : (
                                        <span
                                          className="text-white"
                                          onClick={() =>
                                            handleTextPrice(
                                              "price",
                                              select?.value[0]?.ListPrice
                                            )
                                          }
                                        >
                                          Listed at ${" "}
                                          {select?.value[0]?.ListPrice ?? ""}
                                        </span>
                                      )}

                                      {/* Address Field */}
                                      {editingField === "address" ? (
                                        <div className="text-center">
                                          <input
                                            type="text"
                                            value={editedText}
                                            onChange={(e) =>
                                              setEditedText(e.target.value)
                                            }
                                            onBlur={() =>
                                              handleTextSave("address")
                                            }
                                            autoFocus
                                          />
                                        </div>
                                      ) : (
                                        <h6
                                          className="text-white"
                                          onClick={() =>
                                            handleTextClick(
                                              "address",
                                              select?.value[0]?.UnparsedAddress
                                            )
                                          }
                                        >
                                          {select?.value[0]?.UnparsedAddress ??
                                            ""}
                                        </h6>
                                      )}
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                          )}

                          {/* Step 2 */}
                          {nextStep === "2" && (
                            <div className="row">
                              {select?.media && select.media.length > 1
                                ? [
                                    ...select.media.slice(1, 7),
                                    ...Array(
                                      Math.max(
                                        0,
                                        6 - select.media.slice(1, 7).length
                                      )
                                    ).fill(null),
                                  ].map((mediaItem, mediaIndex) => (
                                    <div
                                      className="col-md-6 mt-2"
                                      key={mediaIndex + 1}
                                    >
                                      <div className="proprty_mediaImage">
                                        <img
                                          className="img-fluid w-100 rounded-0 brochure_Image"
                                          src={
                                            mediaItem
                                              ? uploadedImages[
                                                  mediaIndex + 1
                                                ] ||
                                                mediaItem.MediaURL ||
                                                defaultpropertyimage
                                              : defaultpropertyimage
                                          }
                                          alt={`Property media ${
                                            mediaIndex + 1
                                          }`}
                                          onClick={() =>
                                            handleImageClick(mediaIndex + 1)
                                          }
                                        />
                                        {selectedImage === mediaIndex + 1 && (
                                          <label className="documentlabelAllImage">
                                            <input
                                              id={`REPC_real_estate_purchase_contract_${mediaIndex}`}
                                              type="file"
                                              onChange={(e) =>
                                                handleImageUpload(
                                                  e,
                                                  mediaIndex + 1
                                                )
                                              }
                                            />
                                            <i className="h2 fi-edit opacity-80"></i>
                                          </label>
                                        )}
                                      </div>
                                      {/* Editable description */}
                                      {editingField ===
                                      `description-${mediaIndex}` ? (
                                        <textarea
                                          className="form-control mt-0 w-100"
                                          id={`textarea-input-${mediaIndex}`}
                                          rows="3"
                                          value={editedText}
                                          onChange={(e) => wordlimitSecond(e)}
                                          onBlur={() =>
                                            handleTextSaveBrochure(
                                              mediaIndex + 1
                                            )
                                          }
                                          autoFocus
                                        />
                                      ) : (
                                        <p
                                          className="description_PrintPageSecond mt-2"
                                          style={{ fontSize: "12px" }}
                                          onClick={() =>
                                            handleTextClick(
                                              `description-${mediaIndex}`,
                                              mediaItem?.description || ""
                                            )
                                          }
                                        >
                                          {mediaItem?.description ||
                                            "Click to add a description"}
                                        </p>
                                      )}
                                    </div>
                                  ))
                                : [1, 2, 3, 4, 5, 6].map((_, defaultIndex) => (
                                    <div
                                      className="col-md-4 mt-2"
                                      key={defaultIndex}
                                    >
                                      <div className="proprty_mediaImage">
                                        <img
                                          className="img-fluid w-100 rounded-0 brochure_Image"
                                          src={
                                            uploadedImages[defaultIndex + 1] ||
                                            defaultpropertyimage
                                          }
                                          alt={`Default Property media ${
                                            defaultIndex + 1
                                          }`}
                                          onClick={() =>
                                            handleImageClick(defaultIndex + 1)
                                          }
                                        />
                                        {selectedImage === defaultIndex + 1 && (
                                          <label className="documentlabelAllImage">
                                            <input
                                              id={`REPC_real_estate_purchase_contract_${defaultIndex}`}
                                              type="file"
                                              onChange={(e) =>
                                                handleImageUpload(
                                                  e,
                                                  defaultIndex + 1
                                                )
                                              }
                                            />
                                            <i className="h2 fi-edit opacity-80"></i>
                                          </label>
                                        )}
                                      </div>
                                    </div>
                                  ))}
                            </div>
                          )}

                          {nextStep === "3" && (
                            <>
                              <div
                                className="row mb-4"
                                style={{ margin: "10px" }}
                              >
                                <div className="col-md-6">
                                  <div className="card-img-top brochure">
                                    <img
                                      className="w-100"
                                      style={{
                                        borderRadius: "10px",
                                        height: "500px",
                                        objectFit: "cover",
                                      }}
                                      src={
                                        select?.media[7]?.MediaURL ||
                                        defaultpropertyimage
                                      }
                                      alt="Property"
                                      onClick={() => handleImageClick(7)}
                                    />
                                    {selectedImage === 7 && (
                                      <label className="documentImageBrochureSeven">
                                        <input
                                          id="REPC_real_estate_purchase_contract"
                                          type="file"
                                          onChange={(e) =>
                                            handleImageUpload(e, 7)
                                          } // Upload for the single image
                                        />
                                        <i className="h2 fi-edit opacity-80"></i>
                                      </label>
                                    )}
                                  </div>
                                </div>
                                <div
                                  className="col-md-6"
                                  style={{
                                    backgroundColor: "white",
                                    borderRadius: "10px",
                                  }}
                                >
                                  <h6 className="text-center mt-3">
                                    Property Details
                                  </h6>
                                  <div className="row proprty_background m-0">
                                    <ul className="list-unstyled text-center">
                                      <div className="d-flex align-items-center justify-content-between">
                                        <p className="text-white  mb-2">
                                          Bedrooms:
                                        </p>
                                        {editingField === "bedrooms" ? (
                                          <input
                                            className="w-100"
                                            type="text"
                                            value={editedText} // Controlled input with edited text
                                            onChange={(e) =>
                                              setEditedText(e.target.value)
                                            } // Update text as the user types
                                            onBlur={() =>
                                              handleTextSaveBedrooms("bedrooms")
                                            } // Save the text when the user leaves the input field
                                            autoFocus
                                          />
                                        ) : (
                                          <p
                                            className="text-white ms-2 mb-2"
                                            onClick={() =>
                                              handleTextBedrooms(
                                                "bedrooms",
                                                select?.value[0]?.BedroomsTotal
                                              )
                                            }
                                          >
                                            {select.value[0]?.BedroomsTotal ||
                                              "0"}
                                          </p>
                                        )}
                                      </div>

                                      <div className="d-flex align-items-center justify-content-between">
                                        <p className="text-white  mb-2">
                                          Bathrooms:
                                        </p>
                                        {editingField === "bathroom" ? (
                                          <input
                                            className="w-100"
                                            type="text"
                                            value={editedText} // Controlled input with edited text
                                            onChange={(e) =>
                                              setEditedText(e.target.value)
                                            } // Update text as the user types
                                            onBlur={() =>
                                              handleTextSaveBathroom("bathroom")
                                            } // Save the text when the user leaves the input field
                                            autoFocus
                                          />
                                        ) : (
                                          <p
                                            className="text-white ms-2 mb-2"
                                            onClick={() =>
                                              handleTextBathroom(
                                                "bathroom",
                                                select?.value[0]?.BathroomsFull
                                              )
                                            }
                                          >
                                            {select?.value[0]?.BathroomsFull ||
                                              "0"}
                                          </p>
                                        )}
                                      </div>

                                      <div className="d-flex align-items-center justify-content-between">
                                        <p className="text-white mb-2">Sqft:</p>
                                        {editingField === "squareFeet" ? (
                                          <input
                                            className="w-100"
                                            type="text"
                                            value={editedText} // Controlled input with edited text
                                            onChange={(e) =>
                                              setEditedText(e.target.value)
                                            } // Update text as the user types
                                            onBlur={() =>
                                              handleTextSaveSquareFeet(
                                                "squareFeet"
                                              )
                                            } // Save the text when the user leaves the input field
                                            autoFocus
                                          />
                                        ) : (
                                          <p
                                            className="text-white ms-2 mb-2"
                                            onClick={() =>
                                              handleTextSquareFeet(
                                                "squareFeet",
                                                select?.value[0]
                                                  ?.BuildingAreaTotal
                                              )
                                            }
                                          >
                                            {select?.value[0]
                                              ?.BuildingAreaTotal ||
                                              "X,000 Sq.Ft."}
                                          </p>
                                        )}
                                      </div>

                                      <div className="d-flex align-items-center justify-content-between">
                                        <p className="text-white mb-2">
                                          Lot Size:
                                        </p>
                                        {editingField === "lotSize" ? (
                                          <input
                                            className="w-100"
                                            type="text"
                                            value={editedText} // Controlled input with edited text
                                            onChange={(e) =>
                                              setEditedText(e.target.value)
                                            } // Update text as the user types
                                            onBlur={() =>
                                              handleTextSaveLotSize("lotSize")
                                            } // Save the text when the user leaves the input field
                                            autoFocus
                                          />
                                        ) : (
                                          <p
                                            className="text-white ms-2 mb-2"
                                            onClick={() =>
                                              handleTextLotSize(
                                                "lotSize",
                                                select?.value[0]?.LotSizeAcres
                                              )
                                            }
                                          >
                                            {select?.value[0]?.LotSizeAcres ||
                                              "0"}
                                          </p>
                                        )}
                                      </div>

                                      <div className="d-flex align-items-center justify-content-between">
                                        <div className="d-flex">
                                          <p className="text-white mb-2">
                                            Year Built:
                                          </p>
                                        </div>
                                        {editingField === "yearBuilt" ? (
                                          <input
                                            className="w-100"
                                            type="text"
                                            value={editedText} // Controlled input with edited text
                                            onChange={(e) =>
                                              setEditedText(e.target.value)
                                            } // Update text as the user types
                                            onBlur={() =>
                                              handleTextSaveYearBuilt(
                                                "yearBuilt"
                                              )
                                            } // Save the text when the user leaves the input field
                                            autoFocus
                                          />
                                        ) : (
                                          <p
                                            className="text-white ms-2 mb-2"
                                            onClick={() =>
                                              handleTextYearBuilt(
                                                "yearBuilt",
                                                select?.value[0]?.YearBuilt
                                              )
                                            }
                                          >
                                            {select?.value[0]?.YearBuilt ||
                                              "2024"}
                                          </p>
                                        )}
                                      </div>

                                      <div className="d-flex align-items-center justify-content-between">
                                        <div className="d-flex">
                                          <p className="text-white mb-2">
                                            Heating:
                                          </p>
                                        </div>
                                        {editingField === "heating" ? (
                                          <input
                                            className="w-100"
                                            type="text"
                                            value={editedText} // Controlled input with edited text
                                            onChange={(e) =>
                                              setEditedText(e.target.value)
                                            } // Update text as the user types
                                            onBlur={() =>
                                              handleTextSaveHeating("heating")
                                            }
                                            autoFocus
                                          />
                                        ) : (
                                          <p
                                            className="text-white ms-2 mb-2"
                                            onClick={() =>
                                              handleTextHeating(
                                                "heating",
                                                select?.value[0]?.Heating
                                              )
                                            }
                                          >
                                            {select?.value[0]?.Heating || "Gas"}
                                          </p>
                                        )}
                                      </div>

                                      <div className="d-flex align-items-center justify-content-between">
                                        <div className="d-flex">
                                          <p className="text-white mb-2">
                                            Cooling:
                                          </p>
                                        </div>
                                        {editingField === "cooling" ? (
                                          <input
                                            type="text"
                                            value={editedText} // Controlled input with edited text
                                            onChange={(e) =>
                                              setEditedText(e.target.value)
                                            } // Update text as the user types
                                            onBlur={() =>
                                              handleTextSaveCooling("cooling")
                                            }
                                            autoFocus
                                          />
                                        ) : (
                                          <p
                                            className="text-white ms-2 mb-2"
                                            onClick={() =>
                                              handleTextColing(
                                                "cooling",
                                                select?.value[0]?.Cooling
                                              )
                                            }
                                          >
                                            {select?.value[0]?.Cooling ||
                                              "Central AC"}
                                            ,
                                          </p>
                                        )}
                                      </div>

                                      <div className="d-flex align-items-center justify-content-between">
                                        <div className="d-flex">
                                          <p className="text-white mb-2">
                                            Garage
                                          </p>
                                        </div>
                                        {editingField === "garage" ? (
                                          <input
                                            className="w-100"
                                            type="text"
                                            value={editedText} // Controlled input with edited text
                                            onChange={(e) =>
                                              setEditedText(e.target.value)
                                            } // Update text as the user types
                                            onBlur={() =>
                                              handleTextSaveGarageSpaces(
                                                "garage"
                                              )
                                            }
                                            autoFocus
                                          />
                                        ) : (
                                          <p
                                            className="text-white ms-2 mb-2"
                                            onClick={() =>
                                              handleTextGarageSpaces(
                                                "garage",
                                                select?.value[0]?.GarageSpaces
                                              )
                                            }
                                          >
                                            {select?.value[0]?.GarageSpaces ||
                                              "X car"}
                                          </p>
                                        )}
                                      </div>

                                      <div className="d-flex align-items-center justify-content-between">
                                        <div className="d-flex">
                                          <p className="text-white mb-2">
                                            Roof
                                          </p>
                                        </div>
                                        {editingField === "roof" ? (
                                          <input
                                            className="w-100"
                                            type="text"
                                            value={editedText} // Controlled input with edited text
                                            onChange={(e) =>
                                              setEditedText(e.target.value)
                                            } // Update text as the user types
                                            onBlur={() =>
                                              handleTextSaveRoof("roof")
                                            }
                                            autoFocus
                                          />
                                        ) : (
                                          <p
                                            className="text-white ms-2 mb-2"
                                            onClick={() =>
                                              handleTextRoof(
                                                "roof",
                                                select?.value[0]?.Roof
                                              )
                                            }
                                          >
                                            {select?.value[0]?.Roof || "Tile,"}
                                          </p>
                                        )}
                                      </div>
                                    </ul>
                                  </div>

                                  <div className="col-md-12 mt-3">
                                    {editingField === `description-7` ? ( // Target description for index 8
                                      <textarea
                                        className="form-control mt-0 w-100"
                                        id="textarea-input-8" // Unique ID for index 8
                                        rows="3"
                                        value={editedText}
                                        onChange={(e) => wordlimitSecond(e)}
                                        onBlur={() => handleTextSaveBrochure(7)} // Save description for index 1
                                        autoFocus
                                      />
                                    ) : (
                                      <p
                                        className="description_PrintPageSecond"
                                        style={{ fontSize: "14px" }}
                                        onClick={() =>
                                          handleTextClick(
                                            `description-7`,
                                            select?.media[7]?.description || ""
                                          )
                                        }
                                      >
                                        {select?.media[7]?.description ||
                                          "Click to add a description"}{" "}
                                      </p>
                                    )}
                                  </div>
                                </div>
                              </div>

                              <div className="row mt-5 contact_background">
                                <div className="col-md-6">
                                  <div className="col-md-6">
                                    <div className="float-start w-100">
                                      <img
                                        className="print_Profile"
                                        src={
                                          uploadedContact ||
                                          (profile.image ? profile.image : "")
                                        } // Show uploaded logo or default avatar
                                        style={{
                                          width: "100px",
                                          cursor: "pointer",
                                        }} // Pointer cursor for better UX
                                        onClick={handleLogoClickContact} // Show file input on click
                                      />
                                      {/* Conditionally render file input on logo click */}
                                      {showContact && (
                                        <label className="documentlabelContact">
                                          <input
                                            id="REPC_real_estate_purchase_contract"
                                            type="file"
                                            onChange={handleLogoUploadContact}
                                          />
                                          <i class="h2 fi-edit opacity-80"></i>
                                        </label>
                                      )}

                                      <div className="d-flex">
                                        {editingField === "firstName" ? (
                                          <input
                                            type="text"
                                            value={editedText} // Controlled input with edited text
                                            onChange={(e) =>
                                              setEditedText(e.target.value)
                                            } // Update text as the user types
                                            onBlur={() =>
                                              handleTextSaveListAgent(
                                                "firstName"
                                              )
                                            } // Save the text when the user leaves the input field
                                            autoFocus
                                          />
                                        ) : (
                                          <span
                                            style={{ fontSize: "12px" }}
                                            onClick={() =>
                                              handleTextListAgent(
                                                "firstName",
                                                profile.firstName,
                                                profile.lastName
                                              )
                                            }
                                          >
                                            {profile && (
                                              <p className="mb-0 text-white">
                                                <b>Agent: </b>
                                                {profile.firstName}&nbsp;
                                              </p>
                                            )}
                                          </span>
                                        )}

                                        {editingField === "lastName" ? (
                                          <input
                                            type="text"
                                            value={editedText} // Controlled input with edited text
                                            onChange={(e) =>
                                              setEditedText(e.target.value)
                                            } // Update text as the user types
                                            onBlur={() =>
                                              handleTextSaveListAgent(
                                                "lastName"
                                              )
                                            } // Save the text when the user leaves the input field
                                            autoFocus
                                          />
                                        ) : (
                                          <span
                                            style={{ fontSize: "12px" }}
                                            onClick={() =>
                                              handleTextListAgent(
                                                "lastName",
                                                profile.lastName
                                              )
                                            }
                                          >
                                            {profile && (
                                              <p className="mb-0 text-white">
                                                {" "}
                                                {profile.lastName}&nbsp;
                                              </p>
                                            )}
                                          </span>
                                        )}
                                      </div>

                                      {editingField === "listOffice" ? (
                                        <input
                                          type="text"
                                          value={editedText} // Controlled input with edited text
                                          onChange={(e) =>
                                            setEditedText(e.target.value)
                                          } // Update text as the user types
                                          onBlur={() =>
                                            handleTextSaveListOffice(
                                              "listOffice"
                                            )
                                          } // Save the text when the user leaves the input field
                                          autoFocus
                                        />
                                      ) : (
                                        <span
                                          style={{ fontSize: "12px" }}
                                          onClick={() =>
                                            handleTextListOffice(
                                              "listOffice",
                                              profile.active_office
                                            )
                                          }
                                        >
                                          {profile && (
                                            <p className="mb-0 text-white">
                                              <b>Office: </b>
                                              {profile.active_office}
                                            </p>
                                          )}
                                        </span>
                                      )}

                                      {editingField === "officePhone" ? (
                                        <input
                                          type="text"
                                          value={editedText} // Controlled input with edited text
                                          onChange={(e) =>
                                            setEditedText(e.target.value)
                                          } // Update text as the user types
                                          onBlur={() =>
                                            handleTextSaveOfficePhone(
                                              "officePhone"
                                            )
                                          } // Save the text when the user leaves the input field
                                          autoFocus
                                        />
                                      ) : (
                                        <span
                                          style={{ fontSize: "12px" }}
                                          onClick={() =>
                                            handleTextOfficePhone(
                                              "officePhone",
                                              profile.phone
                                            )
                                          }
                                        >
                                          {profile && (
                                            <p className="mb-0 text-white">
                                              <b>Phone: </b>
                                              {profile.phone}
                                            </p>
                                          )}
                                        </span>
                                      )}
                                    </div>
                                  </div>
                                </div>
                                <div className="col-md-6">
                                  <div className="pull-right text-end imageLogo w-100">
                                    <div>
                                      <img
                                        className="mb-2 rounded-0"
                                        src={uploadedLogo || Logo}
                                        style={{
                                          width: "100px",
                                          cursor: "pointer",
                                        }}
                                        onClick={handleLogoClick}
                                      />
                                      {showLogoInput && (
                                        <label className="documentlabelAllLogo">
                                          <input
                                            id="REPC_real_estate_purchase_contract"
                                            type="file"
                                            onChange={handleLogoUpload}
                                          />
                                          <i class="h2 fi-edit opacity-80"></i>
                                        </label>
                                      )}
                                    </div>

                                    {editingField === "published" ? (
                                      <input
                                        className="w-100"
                                        type="text"
                                        value={editedText}
                                        onChange={(e) =>
                                          setEditedText(e.target.value)
                                        }
                                        onBlur={() =>
                                          handleTextSavePublished("published")
                                        }
                                        autoFocus
                                      />
                                    ) : (
                                      <p
                                        className="mb-0 text-white"
                                        style={{ fontSize: "12px" }}
                                        onClick={() =>
                                          handleTextPublished(
                                            "published",
                                            select.value[0]?.SourceSystemName
                                          )
                                        }
                                      >
                                        {select.value[0]?.SourceSystemName ||
                                          "UtahRealEstate.com"}
                                      </p>
                                    )}

                                    {editingField === "address" ? (
                                      <input
                                        className="w-100"
                                        type="text"
                                        value={editedText} // Controlled input with edited text
                                        onChange={(e) =>
                                          setEditedText(e.target.value)
                                        } // Update text as the user types
                                        onBlur={() => handleTextSave("address")}
                                        autoFocus
                                      />
                                    ) : (
                                      <span
                                        className="mb-0 text-white"
                                        style={{ fontSize: "12px" }}
                                        onClick={() =>
                                          handleTextClick(
                                            "address",
                                            select?.value[0]?.UnparsedAddress
                                          )
                                        }
                                      >
                                        {select?.value[0]?.UnparsedAddress ||
                                          "2024 Demo St"}
                                      </span>
                                    )}

                                    <br />

                                    {editingField === "companyCity" ? (
                                      <input
                                        className="w-100"
                                        type="text"
                                        value={editedText} // Controlled input with edited text
                                        onChange={(e) =>
                                          setEditedText(e.target.value)
                                        } // Update text as the user types
                                        onBlur={() =>
                                          handleTextSaveCompanyCity(
                                            "companyCity"
                                          )
                                        }
                                        autoFocus
                                      />
                                    ) : (
                                      <span
                                        className="mb-0 text-white"
                                        style={{ fontSize: "12px" }}
                                        onClick={() =>
                                          handleTextCompanyCity(
                                            "companyCity",
                                            select?.value[0]?.City
                                          )
                                        }
                                      >
                                        {select?.value[0]?.City ||
                                          "American Fork"}
                                        ,
                                      </span>
                                    )}

                                    {editingField === "companyState" ? (
                                      <input
                                        type="text"
                                        value={editedText} // Controlled input with edited text
                                        onChange={(e) =>
                                          setEditedText(e.target.value)
                                        } // Update text as the user types
                                        onBlur={() =>
                                          handleTextSaveCompanyState(
                                            "companyState"
                                          )
                                        }
                                        autoFocus
                                      />
                                    ) : (
                                      <span
                                        className="mb-0 text-white"
                                        style={{ fontSize: "12px" }}
                                        onClick={() =>
                                          handleTextCompanyState(
                                            "companyState",
                                            select?.value[0]?.StateOrProvince
                                          )
                                        }
                                      >
                                        {select?.value[0]?.StateOrProvince ||
                                          "UT"}
                                        ,
                                      </span>
                                    )}

                                    {editingField === "companyPostCode" ? (
                                      <input
                                        className="w-100"
                                        type="text"
                                        value={editedText} // Controlled input with edited text
                                        onChange={(e) =>
                                          setEditedText(e.target.value)
                                        } // Update text as the user types
                                        onBlur={() =>
                                          handleTextSaveCompanyPostCode(
                                            "companyPostCode"
                                          )
                                        }
                                        autoFocus
                                      />
                                    ) : (
                                      <span
                                        className="mb-0 text-white"
                                        style={{ fontSize: "12px" }}
                                        onClick={() =>
                                          handleTextCompanyPostCode(
                                            "companyPostCode",
                                            select?.value[0]?.PostalCode
                                          )
                                        }
                                      >
                                        {select?.value[0]?.PostalCode ||
                                          "84003"}
                                      </span>
                                    )}
                                  </div>
                                </div>
                              </div>
                            </>
                          )}

                          {nextStep === "4" && (
                            <div className="row" data-thumbnails="true">
                              <div className="col-md-12">
                                <div className="col-md-12 singleListing">
                                  <div className="card-img-top brochure">
                                    <img
                                      className="w-100"
                                      style={{
                                        borderRadius: "0px",
                                        height: "500px",
                                        objectFit: "cover",
                                      }}
                                      src={
                                        select?.media[8]?.MediaURL ||
                                        defaultpropertyimage
                                      } // Ensure index 8 exists
                                      alt="Property"
                                      onClick={() => handleImageClick(8)} // Handle click for index 8
                                    />
                                    {selectedImage === 8 && (
                                      <label className="documentlabelNew">
                                        <input
                                          id="REPC_real_estate_purchase_contract"
                                          type="file"
                                          onChange={(e) =>
                                            handleImageUpload(e, 8)
                                          } // Upload for index 8
                                        />
                                        <i className="h2 fi-edit opacity-80"></i>
                                      </label>
                                    )}
                                  </div>
                                </div>
                                <div className="col-md-12 w-100 mt-1">
                                  <div className="card-img-top brochureListingFourth p-3">
                                    {editingField === `description-8` ? ( // Target description for index 8
                                      <textarea
                                        className="form-control mt-0 w-100"
                                        id="textarea-input-8" // Unique ID for index 8
                                        rows="3"
                                        value={editedText}
                                        onChange={(e) => wordlimit(e)}
                                        onBlur={() => handleTextSaveBrochure(8)} // Save description for index 8
                                        autoFocus
                                      />
                                    ) : (
                                      <h2
                                        className="text-white description_PrintPage mt-2"
                                        style={{
                                          color: "white",
                                          // fontFamily: "circular",
                                          // fontStyle: "italic",
                                          fontSize: "16px",
                                        }}
                                        onClick={() =>
                                          handleTextClick(
                                            `description-8`,
                                            select?.media[8]?.description || ""
                                          )
                                        }
                                      >
                                        {select?.media[8]?.description ||
                                          "Click to add a description"}{" "}
                                      </h2>
                                    )}
                                  </div>
                                </div>
                              </div>
                            </div>
                          )}

                          {/* Add more steps here if needed */}
                        </div>

                        <div className="template_second">
                          <button
                            className="btn btn-secondary pull-right ms-3"
                            onClick={() => handleNext("4")}
                          >
                            Back Page
                          </button>
                          <button
                            className="btn btn-secondary pull-right  ms-3"
                            onClick={() => handleNext("3")}
                          >
                            Inside Right Page
                          </button>
                          <button
                            className="btn btn-secondary pull-right  ms-3"
                            onClick={() => handleNext("2")}
                          >
                            Inside Left Page
                          </button>
                          <button
                            className="btn btn-secondary pull-right  ms-3"
                            onClick={() => handleNext("1")}
                          >
                            Front Page
                          </button>

                          <div className="ms-5">
                            <PDFDownloadLink
                              document={
                                <Flyer
                                  select={select}
                                  platformData={uploadedLogo}
                                  profile={profile}
                                />
                              }
                              fileName={select.address || "Flyer.pdf"}
                            >
                              Download Pdf
                            </PDFDownloadLink>
                          </div>
                        </div>
                      </div>
                    </section>
                  )}
                </div>
              ) : (
                ""
              )}
            </div>
          ) : (
            <>
              <div className="col-md-8">
                <h3 className="text-white mb-4">Design your Brochure.</h3>
                <div className="bg-light float-start w-100 rounded-3 mb-2 p-3">
                  <p
                    id=""
                    dangerouslySetInnerHTML={{
                      __html: platformData?.flyerpagecontent,
                    }}
                  />
                  <hr className="mb-3" />
                  <div className="mb-5">
                    <div className="select_template float-start w-100 mt-3 mb-5">
                      <label className="col-form-label" id="radio-level1">
                        Select Template:
                      </label>
                      <div className="d-flex">
                        <div className="form-check mb-0">
                          {/* Image 1 */}
                          <img
                            className={templateType === "type1" ? "active" : ""}
                            src={TypeOne}
                            alt="Type One"
                            style={{ cursor: "pointer" }}
                            onClick={() => handleType("type1")}
                          />
                        </div>
                        {/* <div className="form-check mb-0 ms-3">
                          <img
                            className={templateType === "type2" ? "active" : ""}
                            src={TypeTwo}
                            alt="Type Two"
                            style={{ cursor: "pointer" }}
                            onClick={() => handleType("type2")}
                          />
                        </div>
                        <div className="form-check mb-0 ms-3">
                          <img
                            className={templateType === "type3" ? "active" : ""}
                            src={TypeThree}
                            alt="Type Three"
                            style={{ cursor: "pointer" }}
                            onClick={() => handleType("type3")}
                          />
                        </div> */}
                      </div>
                    </div>

                    {templateType ? (
                      <div className="float-start w-100 mt-3">
                        <label className="col-form-label ">
                          Select Membership{" "}
                        </label>
                        <select
                          className="form-select mb-2"
                          name="membership"
                          onChange={(event) =>
                            setMembership(event.target.value)
                          }
                          value={membership}
                        >
                          <option value="">Please Select Membership</option>
                          {commaSeparatedValuesmls_membership &&
                          commaSeparatedValuesmls_membership.length > 0
                            ? commaSeparatedValuesmls_membership.map(
                                (memberahip) => <option>{memberahip}</option>
                              )
                            : ""}
                        </select>
                        {membership ? (
                          <>
                            <label className="col-form-label">
                              Enter MLS Number:
                            </label>
                            {/* <h6 className="mt-2 mb-2"></h6> */}
                            <div className="col-md-12">
                              <input
                                className="form-control w-100"
                                id="text-input-onee"
                                type="text"
                                name="mlsnumber"
                                placeholder="Enter MLS number"
                                onChange={handleChangesearchmls}
                                value={searchmlsnumber.mlsnumber}
                              />
                            </div>

                            <div className="pull-left mt-4">
                              <button
                                className="btn btn-primary btn-sm order-lg-3"
                                onClick={MLSSearch}
                                disabled={isLoading}
                              >
                                {isLoading ? "Please wait" : "Search Print"}
                              </button>
                            </div>
                          </>
                        ) : (
                          ""
                        )}
                      </div>
                    ) : (
                      ""
                    )}
                  </div>
                  {/* <div className="col-md-4">
                    <button
                      className="btn btn-primary mt-4"
                      onClick={handleNonListed}
                    >
                      Non-Listed Property
                    </button>
                  </div> */}
                </div>
              </div>
            </>
          )}
        </div>
      </main>
    </div>
  );
};

export default BrochureMls;

import React, { useState, useRef, useEffect } from "react";
import { NavLink, useNavigate, useParams } from "react-router-dom";
import user_service from "../service/user_service";
import jwt from "jwt-decode";
import _ from "lodash";
import Loader from "../../Pages/Loader/Loader.js";
import Toaster from "../../Pages/Toaster/Toaster.js";
import TypeOne from "../img/type1.png";
import TypeTwo from "../img/type2.png";
import TypeThree from "../img/type3.png";
import defaultpropertyimage from "../../Components/img/defaultpropertyimage.jpeg";

import Logo from "../img/logo.png";
import {
  Document,
  Page,
  Text,
  Image,
  StyleSheet,
  PDFDownloadLink,
  View,
} from "@react-pdf/renderer";

const PrintEditor = () => {
  const [membership, setMembership] = useState("");
  const componentRef = useRef();

  const [loader, setLoader] = useState({ isActive: null });
  const { isActive } = loader;
  const [toaster, setToaster] = useState({
    types: null,
    isShow: null,
    toasterBody: "",
    message: "",
  });
  const { types, isShow, toasterBody, message } = toaster;

  const [searchmlsnumber, setSearchmlsnumber] = useState({ mlsNumber: "" });

  const [select, setSelect] = useState("");

  useEffect(() => {
    window.scrollTo(0, 0);
    ContactGetById(jwt(localStorage.getItem("auth")).id);
    PlatformdetailsGet();
  }, []);

  const handleChangesearchmls = (event) => {
    setSearchmlsnumber({ mlsnumber: event.target.value });
  };

  const [templateType, setTemplateType] = useState();
  const [isLoading, setIsLoading] = useState(false);

  const handleType = (input) => {
    const templateType = input.target ? input.target.value : input;
    setTemplateType(templateType);
  };

  const API_BASE_URL = "https://api.brokeragentbase.com";
  const MLSSearch = async () => {
    if (searchmlsnumber.mlsnumber) {
      try {
        setIsLoading(true);
        let response;

        if (membership === "UtahRealEstate.com") {
          response = await user_service.listingSearchMLS(
            searchmlsnumber.mlsnumber
          );
        } else if (membership === "Park City") {
          response = await user_service.listingSearchMLSspark(
            searchmlsnumber.mlsnumber,
            "parkcity"
          );
        } else if (membership === "Washington County") {
          response = await user_service.listingSearchMLSspark(
            searchmlsnumber.mlsnumber,
            "washingtoncounty"
          );
        } else if (membership === "Iron County") {
          response = await user_service.listingSearchMLSspark(
            searchmlsnumber.mlsnumber,
            "ironcounty"
          );
        } else {
          response = await user_service.listingSearchMLS(
            searchmlsnumber.mlsnumber
          );
        }

        if (response && response.data) {
          const propertyData = response.data;

          // Check if media or value arrays are empty
          if (
            (propertyData.media && propertyData.media.length === 0) ||
            (propertyData.value && propertyData.value.length === 0)
          ) {
            setToaster({
              types: "Error",
              isShow: true,
              message: "Please Add a Valid MLS Number.",
            });
          } else {
            // Modify image URLs to route through proxy if media is available
            if (propertyData.media && propertyData.media.length > 0) {
              propertyData.media = propertyData.media.map((image) => {
                return {
                  ...image,
                  MediaURL: `${API_BASE_URL}/proxy-image?url=${encodeURIComponent(
                    image.MediaURL
                  )}`,
                };
              });
            }

            setSelect(propertyData);
            setToaster({
              types: "Success",
              isShow: true,
              message: "MLS search successful!",
            });
          }
        } else {
          setToaster({
            types: "Error",
            isShow: true,
            message: "No data found for the provided MLS number.",
          });
        }

        setIsLoading(false);
      } catch (error) {
        console.error("Error in MLSSearch:", error);
        setToaster({
          types: "Error",
          isShow: true,
          message: "Please provide a valid MLS number.",
        });
      } finally {
        setIsLoading(false);
        setTimeout(() => {
          setToaster((prevToaster) => ({ ...prevToaster, isShow: false }));
        }, 2000);
      }
    } else {
      setToaster({
        types: "Error",
        isShow: true,
        message: "Please provide a valid MLS number.",
      });
      setTimeout(() => {
        setToaster((prevToaster) => ({ ...prevToaster, isShow: false }));
      }, 2000);
    }
  };

  const [getContact, setGetContact] = useState("");
  const [mls_membership, setMls_membership] = useState([]);
  const [
    commaSeparatedValuesmls_membership,
    setCommaSeparatedValuesmls_membership,
  ] = useState("");

  const ContactGetById = async () => {
    await user_service
      .contactGetById(jwt(localStorage.getItem("auth")).id)
      .then((response) => {
        if (response) {
          setGetContact(response.data);
          if (response.data && response.data.additionalActivateFields) {
            const mls_membershipString =
              response.data.additionalActivateFields?.mls_membership;
            if (mls_membershipString) {
              if (typeof mls_membershipString === "string") {
                try {
                  const mls_membershipParsed = JSON.parse(mls_membershipString);
                  if (mls_membershipParsed == "") {
                    setMls_membership(mls_membership);
                  } else {
                    setMls_membership(mls_membershipParsed);
                  }
                  if (mls_membershipParsed == "") {
                    setCommaSeparatedValuesmls_membership("");
                  } else {
                    const boardMembershipValues = mls_membershipParsed.map(
                      (item) => item.value
                    );

                    setCommaSeparatedValuesmls_membership(
                      boardMembershipValues
                    );
                  }
                } catch (error) {
                  console.error("Error parsing mls_membership as JSON:", error);
                  setCommaSeparatedValuesmls_membership("");
                }
              } else {
                setCommaSeparatedValuesmls_membership("");
              }
            } else {
              setCommaSeparatedValuesmls_membership("");
            }
          }
        }
      });
  };

  const [editingField, setEditingField] = useState(null);
  const [editingText, setEditingText] = useState(null);
  const [editedText, setEditedText] = useState("");
  const [selectedImage, setSelectedImage] = useState(null);
  const [uploadedImages, setUploadedImages] = useState({});

  const handleImageClick = (imageIndex) => {
    setSelectedImage(imageIndex);
  };

  // Handle image upload
  const handleImageUpload = (e, imageIndex) => {
    const file = e.target.files[0];
    if (file) {
      const reader = new FileReader();
      reader.onload = (upload) => {
        // Update the uploaded image state
        setUploadedImages((prev) => ({
          ...prev,
          [imageIndex]: upload.target.result,
        }));

        // Ensure that `media` is defined before updating it
        setSelect((prevSummary) => {
          const updatedImages = [...(prevSummary.media || [])]; // Handle case where media is undefined
          updatedImages[imageIndex] = { MediaURL: upload.target.result }; // Update the image at the specific index
          return { ...prevSummary, media: updatedImages };
        });
      };
      reader.readAsDataURL(file);
    }
    setSelectedImage(null);
  };

  const handleTextClick = (field, currentValue) => {
    setEditingField(field); // Set the field being edited
    setEditedText(currentValue); // Pre-fill input with current value (address)
  };

  // Function to handle saving the edited text locally
  const handleTextSave = (field) => {
    // Update the selected value locally after editing
    setSelect((prevSelect) => ({
      ...prevSelect,
      value: [
        {
          ...prevSelect.value[0],
          UnparsedAddress: editedText, // Update the address with the new value
        },
      ],
    }));

    setEditingField(null); // Close the input field after saving
  };

  const handleTextCity = (field, currentValue) => {
    setEditingField(field); // Set the field being edited
    setEditedText(currentValue); // Pre-fill input with current value (address)
  };

  const handleTextSaveCity = (field) => {
    // Update the selected value locally after editing
    setSelect((prevSelect) => ({
      ...prevSelect,
      value: [
        {
          ...prevSelect.value[0],
          City: editedText, // Update the address with the new value
        },
      ],
    }));

    setEditingField(null); // Close the input field after saving
  };

  const handleTextState = (field, currentValue) => {
    setEditingField(field); // Set the field being edited
    setEditedText(currentValue); // Pre-fill input with current value (address)
  };

  const handleTextSaveState = (field) => {
    // Update the selected value locally after editing
    setSelect((prevSelect) => ({
      ...prevSelect,
      value: [
        {
          ...prevSelect.value[0],
          StateOrProvince: editedText, // Update the address with the new value
        },
      ],
    }));

    setEditingField(null); // Close the input field after saving
  };

  const handleTextPostCode = (field, currentValue) => {
    setEditingField(field); // Set the field being edited
    setEditedText(currentValue); // Pre-fill input with current value (address)
  };

  const handleTextSavePostCode = (field) => {
    // Update the selected value locally after editing
    setSelect((prevSelect) => ({
      ...prevSelect,
      value: [
        {
          ...prevSelect.value[0],
          PostalCode: editedText, // Update the address with the new value
        },
      ],
    }));

    setEditingField(null); // Close the input field after saving
  };

  const handleTextPrice = (field, currentValue) => {
    setEditingField(field); // Set the field being edited
    setEditedText(currentValue); // Pre-fill input with current value (address)
  };

  const handleTextSavePrice = (field) => {
    // Update the selected value locally after editing
    setSelect((prevSelect) => ({
      ...prevSelect,
      value: [
        {
          ...prevSelect.value[0],
          ListPrice: editedText, // Update the address with the new value
        },
      ],
    }));

    setEditingField(null); // Close the input field after saving
  };

  const handleTextPublicRemarks = (field, currentValue) => {
    setEditingField(field); // Set the field being edited
    setEditedText(currentValue); // Pre-fill input with current value (address)
  };

  const handleTextSavePublicRemarks = (field) => {
    setSelect((prevSelect) => ({
      ...prevSelect,
      value: [
        {
          ...prevSelect.value[0],
          PublicRemarks: editedText, // Update the address with the new value
        },
      ],
    }));

    setEditingField(null); // Close the input field after saving
  };

  const handleTextBathroom = (field, currentValue) => {
    setEditingField(field); // Set the field being edited
    setEditedText(currentValue); // Pre-fill input with current value (address)
  };

  const handleTextSaveBathroom = (field) => {
    // Update the selected value locally after editing
    setSelect((prevSelect) => ({
      ...prevSelect,
      value: [
        {
          ...prevSelect.value[0],
          BathroomsFull: editedText, // Update the address with the new value
        },
      ],
    }));

    setEditingField(null); // Close the input field after saving
  };

  const handleTextBedrooms = (field, currentValue) => {
    setEditingField(field); // Set the field being edited
    setEditedText(currentValue); // Pre-fill input with current value (address)
  };

  const handleTextSaveBedrooms = (field) => {
    // Update the selected value locally after editing
    setSelect((prevSelect) => ({
      ...prevSelect,
      value: [
        {
          ...prevSelect.value[0],
          BedroomsTotal: editedText, // Update the address with the new value
        },
      ],
    }));

    setEditingField(null); // Close the input field after saving
  };

  const handleTextSquareFeet = (field, currentValue) => {
    setEditingField(field); // Set the field being edited
    setEditedText(currentValue); // Pre-fill input with current value (address)
  };

  const handleTextSaveSquareFeet = (field) => {
    // Update the selected value locally after editing
    setSelect((prevSelect) => ({
      ...prevSelect,
      value: [
        {
          ...prevSelect.value[0],
          BuildingAreaTotal: editedText, // Update the address with the new value
        },
      ],
    }));

    setEditingField(null); // Close the input field after saving
  };

  const handleTextLotSize = (field, currentValue) => {
    setEditingField(field); // Set the field being edited
    setEditedText(currentValue); // Pre-fill input with current value (address)
  };

  const handleTextSaveLotSize = (field) => {
    // Update the selected value locally after editing
    setSelect((prevSelect) => ({
      ...prevSelect,
      value: [
        {
          ...prevSelect.value[0],
          LotSizeAcres: editedText, // Update the address with the new value
        },
      ],
    }));

    setEditingField(null); // Close the input field after saving
  };

  const handleTextLocation = (field, currentValue) => {
    setEditingField(field); // Set the field being edited
    setEditedText(currentValue); // Pre-fill input with current value (address)
  };

  const handleTextSaveLocation = (field) => {
    // Update the selected value locally after editing
    setSelect((prevSelect) => ({
      ...prevSelect,
      value: [
        {
          ...prevSelect.value[0],
          MLSAreaMajor: editedText, // Update the address with the new value
        },
      ],
    }));

    setEditingField(null); // Close the input field after saving
  };

  const handleTextYearBuilt = (field, currentValue) => {
    setEditingField(field); // Set the field being edited
    setEditedText(currentValue); // Pre-fill input with current value (address)
  };

  const handleTextSaveYearBuilt = (field) => {
    // Update the selected value locally after editing
    setSelect((prevSelect) => ({
      ...prevSelect,
      value: [
        {
          ...prevSelect.value[0],
          YearBuilt: editedText, // Update the address with the new value
        },
      ],
    }));

    setEditingField(null); // Close the input field after saving
  };
  const handleTextListAgent = (field, currentValue) => {
    setEditingField(field); // Set the field being edited
    setEditedText(currentValue); // Set the current text value in the input
  };

  // Handle saving the edited text for the list agent
  const handleTextSaveListAgent = (field) => {
    setProfile((prevProfile) => ({
      ...prevProfile,
      [field]: editedText, // Dynamically update the appropriate field (firstName or lastName)
    }));
    setEditingField(null); // Close the input field after saving
  };

  const handleTextListOffice = (field, currentValue) => {
    setEditingField(field); // Set the field being edited
    setEditedText(currentValue); // Pre-fill input with current value (address)
  };

  const handleTextSaveListOffice = (field) => {
    // Update the selected value locally after editing
    setProfile((prevProfile) => ({
      ...prevProfile,
      active_office: editedText, // Dynamically update the appropriate field (firstName or lastName)
    }));

    setEditingField(null); // Close the input field after saving
  };

  const handleTextOfficePhone = (field, currentValue) => {
    setEditingField(field); // Set the field being edited
    setEditedText(currentValue); // Pre-fill input with current value (address)
  };

  const handleTextSaveOfficePhone = (field) => {
    // Update the selected value locally after editing
    setProfile((prevProfile) => ({
      ...prevProfile,
      phone: editedText, // Dynamically update the appropriate field (firstName or lastName)
    }));

    setEditingField(null); // Close the input field after saving
  };

  const handleTextPublished = (field, currentValue) => {
    setEditingField(field); // Set the field being edited
    setEditedText(currentValue); // Pre-fill input with current value (address)
  };

  const handleTextSavePublished = (field) => {
    // Update the selected value locally after editing
    setSelect((prevSelect) => ({
      ...prevSelect,
      value: [
        {
          ...prevSelect.value[0],
          SourceSystemName: editedText, // Update the address with the new value
        },
      ],
    }));

    setEditingField(null); // Close the input field after saving
  };

  const handleTextClickCompanyAddress = (field, currentValue) => {
    setEditingField(field); // Set the field being edited
    setEditedText(currentValue); // Pre-fill input with current value (address)
  };

  const handleTextSaveCompanyAddress = (field) => {
    // Update the selected value locally after editing
    setSelect((prevSelect) => ({
      ...prevSelect,
      value: [
        {
          ...prevSelect.value[0],
          UnparsedAddress: editedText, // Update the address with the new value
        },
      ],
    }));

    setEditingField(null); // Close the input field after saving
  };

  const handleTextCompanyCity = (field, currentValue) => {
    setEditingField(field); // Set the field being edited
    setEditedText(currentValue); // Pre-fill input with current value (address)
  };

  const handleTextSaveCompanyCity = (field) => {
    // Update the selected value locally after editing
    setSelect((prevSelect) => ({
      ...prevSelect,
      value: [
        {
          ...prevSelect.value[0],
          City: editedText, // Update the address with the new value
        },
      ],
    }));

    setEditingField(null); // Close the input field after saving
  };

  const handleTextCompanyState = (field, currentValue) => {
    setEditingField(field); // Set the field being edited
    setEditedText(currentValue); // Pre-fill input with current value (address)
  };

  const handleTextSaveCompanyState = (field) => {
    // Update the selected value locally after editing
    setSelect((prevSelect) => ({
      ...prevSelect,
      value: [
        {
          ...prevSelect.value[0],
          StateOrProvince: editedText, // Update the address with the new value
        },
      ],
    }));

    setEditingField(null); // Close the input field after saving
  };

  const handleTextCompanyPostCode = (field, currentValue) => {
    setEditingField(field); // Set the field being edited
    setEditedText(currentValue); // Pre-fill input with current value (address)
  };

  const handleTextSaveCompanyPostCode = (field) => {
    // Update the selected value locally after editing
    setSelect((prevSelect) => ({
      ...prevSelect,
      value: [
        {
          ...prevSelect.value[0],
          PostalCode: editedText, // Update the address with the new value
        },
      ],
    }));

    setEditingField(null); // Close the input field after saving
  };

  const [platformData, setPlatFormData] = useState({});
  const [uploadedLogo, setUploadedLogo] = useState(null);
  const [showLogoInput, setShowLogoInput] = useState(false); // State to show or hide input field
  const [profile, setProfile] = useState({});

  const [uploadedContact, setUploadedContact] = useState(null);
  const [showContact, setShowContact] = useState(false);

  const PlatformdetailsGet = async () => {
    await user_service.platformdetailsGet().then((response) => {
      if (response) {
        setPlatFormData(response.data.data[0]);
      }
    });
  };

  const profileGetAll = async () => {
    if (localStorage.getItem("auth")) {
      try {
        const response = await user_service.profileGet(
          jwt(localStorage.getItem("auth")).id
        );
        if (response) {
          const profileData = response.data;
          if (profileData && profileData.image) {
            const proxyImageUrl = `${API_BASE_URL}/proxy-image?url=${encodeURIComponent(
              profileData.image
            )}`;

            // Assuming 'setProfile' is a function that updates the state with the modified profile data
            setProfile({
              ...profileData, // Copy existing profile data
              image: proxyImageUrl, // Replace original image with the proxy URL
            });
          } else {
            setProfile(profileData); // No image to proxy, just set the profile as is
          }
        }
      } catch (error) {
        console.error("Error fetching profile data:", error);
      }
    }
  };

  useEffect(() => {
    window.scrollTo(0, 0);
    PlatformdetailsGet();
    profileGetAll();
  }, []);

  // Handle logo upload

  const handleLogoUploadContact = (e) => {
    const file = e.target.files[0];
    if (file) {
      const reader = new FileReader();
      reader.onload = (upload) => {
        setUploadedContact(upload.target.result); // Store the uploaded logo
        setShowContact(false); // Hide the input after upload
        // Update platformData with the new logo while preserving the rest of the data
        setProfile((prevData) => ({
          ...prevData,
          image: upload.target.result || prevData.image, // Update the logo in platformData
        }));
      };
      reader.readAsDataURL(file); // Read the file as a data URL for image preview
    }
  };

  const handleLogoUpload = (e) => {
    const file = e.target.files[0];
    if (file) {
      const reader = new FileReader();
      reader.onload = (upload) => {
        setUploadedLogo(upload.target.result); // Store the uploaded logo
        setShowLogoInput(false); // Hide the input after upload
        // Update platformData with the new logo while preserving the rest of the data
        setPlatFormData((prevData) => ({
          ...prevData,
          platform_logo: upload.target.result, // Update the logo in platformData
        }));
      };
      reader.readAsDataURL(file); // Read the file as a data URL for image preview
    }
  };

  // Handle logo click to show the input
  const handleLogoClick = () => {
    setShowLogoInput(true); // Show the file input when the logo is clicked
  };

  const handleLogoClickContact = () => {
    setShowContact(true); // Show the file input when the logo is clicked
  };

  const styles = StyleSheet.create({
    page: {
      backgroundColor: "#b4a3a359",
      padding: 10,
    },
    rowContainer: {
      display: "flex",
      flexDirection: "row",
      justifyContent: "space-between",
      width: "100%",
    },
    logoCol: {
      width: "48%", // Slightly smaller width for the logo column
    },
    textCol: {
      width: "48%",
      display: "flex",
      justifyContent: "flex-end",
      textAlign: "right",
    },
    officeLogo: {
      width: 100,
      objectFit: "contain",
      marginBottom: 10,
    },
    priceText: {
      fontSize: 14,
      fontWeight: "bold",
      marginBottom: 5,
    },
    addressText: {
      fontSize: 14,
      marginTop: 5,
      marginBottom: 5,
      fontWeight: "bold",
    },

    overviewHeader: {
      fontSize: 20,
      fontWeight: "bold",
      color: "#000",
      textAlign: "center",
      marginBottom: 10,
    },
    contactInfo: {
      flexDirection: "row",
      justifyContent: "space-between",
      marginBottom: 20,
    },
    contactText: {
      fontSize: 12,
      color: "#fff",
    },
    mainpic: {
      width: "100%",
      height: 200,
      objectFit: "cover",
      marginBottom: 10,
    },
    longbox: {
      width: "100%",
      backgroundColor: "#2e3339",
      opacity: 0.9,
      padding: 10,
      borderRadius: 5,
      marginBottom: 5,
    },

    longboxSubHeader: {
      fontSize: 12,
      textAlign: "center",
      color: "#fff",
      marginBottom: 5,
    },

    longboxSubHeaderNew: {
      fontSize: 10,
      textAlign: "center",
      color: "#fff",
      marginBottom: 5,
    },
    price: {
      fontSize: 24,
      fontWeight: "bold",
      textAlign: "center",
      color: "#917462",
      marginBottom: 5,
    },

    propertyDetails: {
      flexDirection: "row",
      justifyContent: "space-between",
      marginBottom: 5,
    },
    propertyDetailsSecondRow: {
      padding: 5,
      flexDirection: "row",
      justifyContent: "space-between",
      marginBottom: 5,
    },
    detailItem: {
      fontSize: 12,
      color: "#917462",
      textAlign: "center",
    },

    detailOverview: {
      fontSize: 12,
      color: "#917462",
      textAlign: "justify",
      marginTop: 10,
    },
    imageGrid: {
      flexDirection: "row",
      flexWrap: "wrap",
      justifyContent: "space-between",
      marginBottom: 5,
    },
    imageItem: {
      width: "32%",
      height: 150,
      objectFit: "cover",
      marginBottom: 10,
    },

    addressBox: {
      width: "100%",
      backgroundColor: getComputedStyle(
        document.documentElement
      ).getPropertyValue("--branding-footer-color"), // Dynamically get CSS variable value
      padding: 10,
      borderRadius: 5,
      display: "flex",
      flexDirection: "row",
      justifyContent: "space-between",
    },

    officeSection: {
      display: "flex",
      // justifyContent : "flex-start",
      marginBottom: 5,
    },

    officeContactImage: {
      width: 60,
      height: 60,
      borderRadius: 50,
      marginLeft: 10,
    },
    officeAddress: {
      width: "50%",
      alignItems: "flex-end",
      marginBottom: 5,
    },

    officeDetails: {
      marginLeft: 10 /* Add space between image and text */,
    },
    officeText: {
      fontSize: 10,
      color: "#ffffff",
      // textTransform: "uppercase",
      marginBottom: 2,
    },

    officeAddressTest: {
      fontSize: 12,
      color: "#000000",
      marginBottom: 2,
    },

    contactHeading: {
      fontSize: 18,
      fontWeight: "bold",
      color: "#00000",
      textAlign: "center",
      marginBottom: 10,
    },

    propertyHeadingFirst: {
      marginTop: 5,
      fontSize: 18,
      fontWeight: "bold",
      color: "#00000",
      textAlign: "center",
      marginBottom: 10,
    },
  });
  const Flyer = ({ select, platformData, profile }) => {
    return (
      <Document>
        <Page size="A4" style={styles.page}>
          <View style={styles.rowContainer}>
            {/* First Column: Platform Logo */}
            <View style={styles.logoCol}>
              <Image
                style={styles.officeLogo}
                src={platformData ?? Logo} // Compressed platform logo
              />
            </View>

            {/* Second Column: Price and Address */}
            <View style={styles.textCol}>
              <Text style={styles.priceText}>
                Listed at ${select?.value[0]?.ListPrice ?? ""}
              </Text>
              <Text style={styles.addressText}>
                {select?.value[0]?.UnparsedAddress ?? ""}
              </Text>
            </View>
          </View>

          {select?.media[0]?.MediaURL && (
            <Image style={styles.mainpic} src={select.media[0].MediaURL} />
          )}

          <Text style={styles.propertyHeadingFirst}>PROPERTY DETAILS</Text>
          <View style={styles.longbox}>
            <View style={styles.propertyDetails}>
              <View style={styles.detailItem}>
                <Text style={styles.longboxSubHeader}>Bedrooms</Text>
                <Text style={styles.longboxSubHeaderNew}>
                  {select?.value[0]?.BedroomsTotal}
                </Text>
              </View>
              <View style={styles.detailItem}>
                <Text style={styles.longboxSubHeader}>Bathrooms</Text>
                <Text style={styles.longboxSubHeaderNew}>
                  {select?.value[0]?.BathroomsFull}
                </Text>
              </View>
              <View style={styles.detailItem}>
                <Text style={styles.longboxSubHeader}>Sqft</Text>
                <Text style={styles.longboxSubHeaderNew}>
                  {select?.value[0]?.BuildingAreaTotal}
                </Text>
              </View>
              <View style={styles.detailItem}>
                <Text style={styles.longboxSubHeader}>Lot Size</Text>
                <Text style={styles.longboxSubHeaderNew}>
                  {select?.value[0]?.LotSizeAcres}
                </Text>
              </View>
              <View style={styles.detailItem}>
                <Text style={styles.longboxSubHeader}>Year Built</Text>
                <Text style={styles.longboxSubHeaderNew}>
                  {select?.value[0]?.YearBuilt}
                </Text>
              </View>
            </View>

            <View style={styles.detailOverview}>
              <Text style={styles.longboxSubHeaderNew}>
                {select?.value[0]?.PublicRemarks}
              </Text>
            </View>
          </View>

          <View style={styles.imageGrid}>
          {select?.media && select.media.length > 1
          ? // Process the media array and ensure we have exactly 3 images
            [
              ...select.media.slice(1, 4), // Slice the media array from index 1 to 4
              ...Array(
                Math.max(
                  0,
                  3 - select.media.slice(1, 4).length // Calculate remaining slots for default images
                )
              ).fill({ MediaURL: defaultpropertyimage }), // Fill with default images
            ].map((mediaItem, mediaIndex) => (
              <Image
              style={styles.imageItem}
                source={{ uri: mediaItem?.MediaURL || defaultpropertyimage }} // Use default image if mediaItem is null
                key={mediaIndex}
              />
            ))
          : // Default fallback for when no media exists or condition is not met
            Array(3).fill().map((_, defaultIndex) => (
              <Image
                style={styles.imageItem}
                source={{ uri: defaultpropertyimage }}
                key={defaultIndex}
              />
            ))
        }
            {/* {select?.media.slice(1, 4).map((mediaItem, mediaIndex) => (
              <Image
                style={styles.imageItem}
                src={mediaItem.MediaURL}
                key={mediaIndex}
              />
            ))} */}
          </View>

          {/* contact section */}
          <View style={styles.addressBox}>
            <View style={styles.officeSection}>
              <Image
                style={styles.officeContactImage}
                src={profile.image} // Compressed profile image
              />
              <View style={styles.officeDetails}>
                <Text style={styles.officeText}>
                  Agent: {profile?.firstName} {profile?.lastName}
                </Text>
                <Text style={styles.officeText}>
                  Office: {profile?.active_office}
                </Text>
                <Text style={styles.officeText}>Phone: {profile?.phone}</Text>
              </View>
            </View>

            <View style={styles.officeAddress}>
              <Text style={styles.officeText}>
                {select?.value[0]?.SourceSystemName}
              </Text>
              <Text style={styles.officeText}>
                {select?.value[0]?.UnparsedAddress},
              </Text>
              <Text style={styles.officeText}>
                {select?.value[0]?.City},&nbsp;
                {select?.value[0]?.StateOrProvince}&nbsp;
                {select?.value[0]?.PostalCode}
              </Text>
            </View>
          </View>
        </Page>
      </Document>
    );
  };

  const stylesSecond = StyleSheet.create({
    page: {
      backgroundColor: "#b4a3a359",
      padding: 10,
    },

    brownbox: {
      padding: 5,
      marginBottom: 1,
      borderRadius: 5,
      position: "relative",
      overflow: "hidden",
      zIndex: 1,
    },

    rowContainer: {
      display: "flex",
      flexDirection: "row",
      justifyContent: "space-between",
      width: "100%",
    },
    logoCol: {
      width: "48%", // Slightly smaller width for the logo column
    },
    textCol: {
      width: "48%",
      marginTop: 0,
      padding: 0,
      display: "flex",
      justifyContent: "flex-end",
      textAlign: "right",
    },
    officeLogo: {
      width: 100,
      objectFit: "contain",
      marginBottom: 10,
    },
    priceText: {
      fontSize: 14,
      fontWeight: 700,
      color: "#000",
      marginBottom: 10,
    },
   
    overviewHeader: {
      fontSize: 22,
      fontWeight: "bold",
      color: "#000",
      textAlign: "center",
      marginBottom: 15,
      transform: [{ rotate: "2deg" }],
      zIndex: 10,
    },
    contactInfo: {
      flexDirection: "row",
      justifyContent: "space-between",
      marginBottom: 20,
    },
    contactText: {
      fontSize: 12,
      color: "#fff",
    },
    mainpic: {
      width: "100%",
      height: 200,
      objectFit: "cover",

      transform: [{ rotate: "1deg" }],
      zIndex: 2,
    },

    price: {
      fontSize: 24,
      fontWeight: "bold",
      textAlign: "center",
      color: "#f5a623",
      marginBottom: 5,
      transform: [{ rotate: "-1deg" }],
    },
    longbox: {
      width: "100%",
      backgroundColor: "#1a1d1f",
      // opacity: 0.95,
      padding: 10,
      borderRadius: 10,
      marginBottom: 5,
      flexDirection: "row",
      justifyContent: "space-between",
      zIndex: 5,
      // paddingTop: 5,
    },
    description: {
      flex: 3, // 75% width (3 out of 4 parts)
      paddingRight: 10,
    },
    propertyData: {
      flex: 1, // 25% width (1 out of 4 parts)
      paddingLeft: 10,
    },
    contactHeading: {
      fontSize: 18,
      fontWeight: "bold",
      color: "#dc936b",
      marginBottom: 10,
    },
    longboxSubHeader: {
      fontSize: 12,
      color: "#e0e0e0",
      marginBottom: 5,
    },
    longboxSubHeaderNew: {
      fontSize: 10,
      color: "#fff",
      marginBottom: 5,
    },
    propertyDetails: {
      flexDirection: "column", // Ensure items are stacked vertically
      fontSize: 10,
      color: "#fff",
      textAlign: "justify",
      marginTop: 5,
    },
    detailItem: {
      fontSize: 10,
      textAlign: "center",
      marginBottom: 10,
      color: "#fff",
    },
    detailOverview: {
      fontSize: 10,
      color: "#fff",
      textAlign: "justify",
      marginTop: 5,
    },
    imageGrid: {
      flexDirection: "row",
      flexWrap: "wrap",
      justifyContent: "space-between",
      marginBottom: 5,
      position: "relative",
      marginTop: -30,
      paddingLeft: 20,
      paddingRight: 20,
    },
    imageItem: {
      width: "32%",
      height: 150,
      objectFit: "cover",
      marginBottom: 10,
      zIndex: 1,
    },
    addressBox: {
      width: "100%",
      backgroundColor: getComputedStyle(
        document.documentElement
      ).getPropertyValue("--branding-footer-color"), // Dynamically get CSS variable value
      marginTop: 5,
      padding: 20,
      borderRadius: 16,
      flexDirection: "row",
      justifyContent: "space-between",
      position: "relative",
      zIndex: 5,
      transform: [{ scaleY: 1.05 }],
    },
    addressText: {
      fontSize: 16,
      color: "#fff",
      textAlign: "center",
      transform: [{ rotate: "-2deg" }],
    },
    officeSection: {
      width: "50%",
      alignItems: "flex-start",
      marginBottom: 5,
    },
    officeAddress: {
      width: "50%",
      alignItems: "flex-end",
      marginBottom: 5,
    },
    officeLogo: {
      width: 100,
      height: 50,
      marginBottom: 10,
    },
    officeDetails: {
      width: "100%",
    },
    officeText: {
      fontSize: 10,
      color: "#fff",
      marginBottom: 2,
      zIndex: 10,
    },
    officeAddressTest: {
      fontSize: 10,
      color: "#fff",
      marginBottom: 2,
    },
    propertyHeading: {
      padding: 10,
      fontSize: 20,
      fontWeight: "bold",
      color: "#fff",
      textAlign: "center",
      marginBottom: 15,
      transform: [{ scaleY: 1.2 }],
    },
    contactHeading: {
      fontSize: 16,
      fontWeight: "bold",
      color: "#fff",
      textAlign: "center",
      marginBottom: 15,
      marginTop: 10,
      transform: [{ rotate: "2deg" }],
    },
    contactSecond: {
      fontSize: 16,
      fontWeight: "bold",
      color: "#cca345",
      textAlign: "center",
      marginBottom: 15,
      transform: [{ rotate: "2deg" }],
    },

    officeContactImageSecond: {
      width: 60,
      height: 60,
      borderRadius: 50,
      marginLeft: 10,
      marginTop: 0,
    },
  });

  const FlyerAlternative = ({ select, platformData, profile }) => (
    <Document>
      <Page size="A4" style={stylesSecond.page}>
        <View style={stylesSecond.rowContainer}>
          {/* First Column: Platform Logo */}
          <View style={stylesSecond.logoCol}>
            <Image
              style={stylesSecond.officeLogo}
              src={platformData ?? Logo} // Compressed platform logo
            />
          </View>

          {/* Second Column: Price and Address */}
          <View style={stylesSecond.textCol}>
            <Text style={stylesSecond.priceText}>
              PRICE AT ${select?.value[0]?.ListPrice ?? ""}
            </Text>
          </View>
        </View>
        <View style={stylesSecond.brownbox}>
          {select?.media[0]?.MediaURL && (
            <Image
              style={stylesSecond.mainpic}
              src={select.media[0].MediaURL}
            />
          )}

         <View style={stylesSecond.imageGrid}>
          {select?.media && select.media.length > 1
          ? // Process the media array and ensure we have exactly 3 images
            [
              ...select.media.slice(1, 4), // Slice the media array from index 1 to 4
              ...Array(
                Math.max(
                  0,
                  3 - select.media.slice(1, 4).length // Calculate remaining slots for default images
                )
              ).fill({ MediaURL: defaultpropertyimage }), // Fill with default images
            ].map((mediaItem, mediaIndex) => (
              <Image
              style={stylesSecond.imageItem}
                source={{ uri: mediaItem?.MediaURL || defaultpropertyimage }} // Use default image if mediaItem is null
                key={mediaIndex}
              />
            ))
          : // Default fallback for when no media exists or condition is not met
            Array(3).fill().map((_, defaultIndex) => (
              <Image
              style={stylesSecond.imageItem}
                source={{ uri: defaultpropertyimage }}
                key={defaultIndex}
              />
            ))
        }
          </View>
        </View>

        <View style={stylesSecond.longbox}>
          <View style={stylesSecond.description}>
            <Text style={stylesSecond.contactHeading}>Description</Text>
            <View style={stylesSecond.detailOverview}>
              <Text style={stylesSecond.longboxSubHeaderNew}>
                {select?.value[0]?.PublicRemarks}
              </Text>
            </View>
          </View>

          <View style={stylesSecond.propertyData}>
            <Text style={stylesSecond.contactHeading}>Property Details</Text>
            <View style={stylesSecond.propertyDetails}>
              <View style={stylesSecond.detailItem}>
                <Text style={stylesSecond.detailLabel}>
                  Bedrooms: {select?.value[0]?.BedroomsTotal}
                </Text>
              </View>
              <View style={stylesSecond.detailItem}>
                <Text style={stylesSecond.detailLabel}>
                  Bathrooms: {select?.value[0]?.BathroomsFull}
                </Text>
              </View>
              <View style={stylesSecond.detailItem}>
                <Text style={stylesSecond.detailLabel}>
                  Lot Size: {select?.value[0]?.LotSizeAcres}
                </Text>
              </View>
              <View style={stylesSecond.detailItem}>
                <Text style={stylesSecond.detailLabel}>
                  Sqft: {select?.value[0]?.BuildingAreaTotal}
                </Text>
              </View>
              <View style={stylesSecond.detailItem}>
                <Text style={stylesSecond.detailLabel}>
                  Year Built: {select?.value[0]?.YearBuilt}
                </Text>
              </View>
            </View>
          </View>
        </View>

        <View style={stylesSecond.addressBox}>
          <View style={stylesSecond.officeSection}>
            <View style={stylesSecond.officeDetails}>
              <Text style={stylesSecond.officeText}>
                {select?.value[0]?.SourceSystemName}
              </Text>
              <Text style={stylesSecond.officeText}>
                {select?.value[0]?.UnparsedAddress},
              </Text>
              <Text style={stylesSecond.officeText}>
                {select?.value[0].City},&nbsp;
                {select.value[0].StateOrProvince}&nbsp;
                {select.value[0]?.PostalCode}
              </Text>
            </View>
          </View>

          <View style={stylesSecond.officeAddress}>
            {/* <Text style={stylesSecond.contactSecond}>CONTACT US NOW</Text> */}
            <Image
              style={styles.officeContactImage}
              src={profile.image} // Compressed profile image
            />
            <Text style={stylesSecond.officeText}>
              Agent: {profile?.firstName} {profile?.lastName}
            </Text>
            <Text style={stylesSecond.officeText}>
              Office: {profile?.active_office}
            </Text>
            <Text style={stylesSecond.officeText}>Phone: {profile?.phone}</Text>
          </View>
        </View>
      </Page>
    </Document>
  );

  const stylesSecondThird = StyleSheet.create({
    page: {
      backgroundColor: "#26221e",
      padding: 10,
    },

    rowContainer: {
      display: "flex",
      flexDirection: "row",
      justifyContent: "space-between",
      width: "100%",
    },
    logoCol: {
      width: "48%", // Slightly smaller width for the logo column
    },

    textCol: {
      width: "48%",
      justifyContent: "flex-end",
      textAlign: "right",
      marginRight: 10,
    },
    officeLogo: {
      width: 100,
      objectFit: "contain",
      marginBottom: 10,
    },
    priceText: {
      fontSize: 14,
      fontWeight: "bold",
      color: "red",
      marginBottom: 5,
    },

  
    mainLogo: {
      display: "flex",
      flexDirection: "row",
      padding: 5,
      marginBottom: 10,
      alignItems: "center",
      justifyContent: "space-between",
      width: "100%",
    },
    brownbox: {
      padding: 5,
      marginBottom: 1,
      borderRadius: 5,
      position: "relative",
      overflow: "hidden",
      zIndex: 1,
    },
    brownboxHeaderPrice: {
      fontSize: 18,
      fontWeight: "bold",
      color: "#dc936b",
      marginBottom: 5,
      transform: [{ scaleY: 1.2 }],
    },
    brownboxHeader: {
      fontSize: 18,
      fontWeight: "bold",
      color: "#fff",
      textAlign: "center",
      marginBottom: 5,
      transform: [{ scaleY: 1.2 }],
    },
    overviewHeader: {
      fontSize: 22,
      fontWeight: "bold",
      color: "#000",
      textAlign: "center",
      marginBottom: 15,
      transform: [{ rotate: "2deg" }],
      zIndex: 10,
    },
    contactInfo: {
      flexDirection: "row",
      justifyContent: "space-between",
      marginBottom: 20,
    },
    contactText: {
      fontSize: 12,
      color: "#fff",
    },

    mainpic: {
      width: "100%",
      flexDirection: "row",
      justifyContent: "space-between",
      marginBottom: 10,
    },
    imageThirdType: {
      flex: 2,
      height: 400,
      padding: 10,
    },

    imageItemThird: {
      flex: 2,
      padding: 10,
    },

    imageThirdNew: {
      padding: 10,
      borderRadius: 16,
    },

    imageThirdNewAll: {
      padding: 10,
      height:170,
      borderRadius: 16,
    },
    detailItem: {
      fontSize: 10,
      textAlign: "center",
      marginBottom: 10,
      color: "#fff",
    },
    detailOverview: {
      fontSize: 10,
      color: "#fff",
    },

    contactHeading: {
      fontSize: 16,
      fontWeight: "bold",
      color: "#fff",
      textAlign: "center",
      marginBottom: 15,
      marginTop: 15,
      transform: [{ rotate: "2deg" }],
    },

    longboxSubHeaderNew: {
      fontSize: 10,
      color: "#e0e0e0",
      textAlign: "justify",
      marginBottom: 5,
    },
    price: {
      fontSize: 24,
      fontWeight: "bold",
      textAlign: "center",
      color: "#f5a623",
      marginBottom: 5,
      transform: [{ rotate: "-1deg" }],
    },
    longbox: {
      width: "100%",
      backgroundColor: "#1a1d1f",
      opacity: 0.95,
      padding: 15,
      borderRadius: 10,
      marginBottom: 5,
      flexDirection: "row",
      // justifyContent: "space-between",
      zIndex: 5,
      marginTop: 100,
    },

    description: {
      flex: 3,
      paddingRight: 10,
    },
    propertyData: {
      flex: 2,
      paddingLeft: 10,
    },

    longboxSubHeader: {
      fontSize: 12,
      color: "#e0e0e0",
      marginBottom: 5,
    },

    propertyDetails: {
      flexDirection: "column",
      padding: 10,
      fontSize: 10,
      color: "#fff",
      textAlign: "justify",
      marginTop: 10,
    },

    imageItem: {
      width: "32%",
      height: 150,
      objectFit: "cover",
      marginBottom: 10,
      zIndex: 1,
    },

    addressBox: {
      width: "100%",
      backgroundColor: getComputedStyle(
        document.documentElement
      ).getPropertyValue("--branding-footer-color"), // Dynamically get CSS variable value
      marginTop: 10,
      padding: 20,
      borderRadius: 16,
      flexDirection: "row",
      justifyContent: "space-between",
      position: "relative",
      zIndex: 5,
      transform: [{ scaleY: 1.05 }],
    },

    addressText: {
      fontSize: 16,
      color: "#fff",
      textAlign: "center",
      transform: [{ rotate: "-2deg" }],
    },

    officeSection: {
      width: "50%",
      alignItems: "flex-start",
      marginBottom: 5,
    },
    officeAddress: {
      marginBottom: 20,
      width: "50%",
      alignItems: "flex-end",
      marginBottom: 5,
    },
    officeLogo: {
      width: 80,
      transform: [{ scale: 1.1 }],
    },
    officeDetails: {
      width: "100%",
    },
    officeText: {
      fontSize: 10,
      color: "#fff",
      textTransform: "uppercase",
      marginBottom: 2,
      zIndex: 10,
    },
    officeAddressTest: {
      fontSize: 10,
      color: "#fff",
      textTransform: "lowercase",
      marginBottom: 2,
    },
    propertyHeading: {
      padding: 10,
      fontSize: 20,
      fontWeight: "bold",
      color: "#fff",
      textAlign: "center",
      marginBottom: 15,
      transform: [{ scaleY: 1.2 }],
    },

    contactSecond: {
      fontSize: 16,
      fontWeight: "bold",
      color: "#cca345",
      textAlign: "center",
      marginBottom: 15,
      transform: [{ rotate: "2deg" }],
    },
  });


  const FlyerAlternativeThird = ({ select, platformData, profile }) => (
    <Document>
      <Page size="A4" style={stylesSecondThird.page}>
        <View style={stylesSecondThird.rowContainer}>
          {/* First Column: Platform Logo */}
          <View style={stylesSecondThird.logoCol}>
            <Image
              style={stylesSecondThird.officeLogo}
              src={platformData ?? Logo} // Compressed platform logo
            />
          </View>

          {/* Second Column: Price and Address */}
          <View style={stylesSecondThird.textCol}>
            <Text style={stylesSecondThird.priceText}>
              PRICE AT ${select?.value[0]?.ListPrice ?? ""}
            </Text>
          </View>
        </View>
          <View style={stylesSecondThird.mainpic}>
          <View style={stylesSecondThird.imageThirdType}>
            {select?.media[0]?.MediaURL && (
              <Image
                style={stylesSecond.imageThirdNew}
                src={select.media[0].MediaURL}
              />
            )}

            <View style={stylesSecondThird.imageGrid}>
              <Text style={stylesSecondThird.contactHeading}>Description</Text>
              <View style={stylesSecondThird.detailOverview}>
                <Text style={stylesSecondThird.longboxSubHeaderNew}>
                  {select?.value[0]?.PublicRemarks}
                </Text>
              </View>
            </View>
          </View>

         <View style={stylesSecondThird.imageItemThird}>
           {select?.media && select.media.length > 1
          ?
            [
              ...select.media.slice(1, 4), // Slice the media array from index 1 to 4
              ...Array(
                Math.max(
                  0,
                  3 - select.media.slice(1, 4).length // Calculate remaining slots for default images
                )
              ).fill({ MediaURL: defaultpropertyimage }), // Fill with default images
            ].map((mediaItem, mediaIndex) => (
              <Image
                style={stylesSecondThird.imageThirdNewAll}
                source={{ uri: mediaItem?.MediaURL || defaultpropertyimage }} // Use default image if mediaItem is null
                key={mediaIndex}
              />
            ))
          : 
            Array(3).fill().map((_, defaultIndex) => (
              <Image
                style={stylesSecondThird.imageThirdNewAll}
                source={{ uri: defaultpropertyimage }}
                key={defaultIndex}
              />
            ))
        }
          </View>
          </View>

        <View style={styles.longbox}>
          <View style={styles.propertyDetails}>
            <View style={styles.detailItem}>
              <Text style={styles.longboxSubHeader}>Bedrooms</Text>
              <Text style={styles.longboxSubHeaderNew}>
                {select?.value[0]?.BedroomsTotal}
              </Text>
            </View>
            <View style={styles.detailItem}>
              <Text style={styles.longboxSubHeader}>Bathrooms</Text>
              <Text style={styles.longboxSubHeaderNew}>
                {select?.value[0]?.BathroomsFull}
              </Text>
            </View>

            <View style={styles.detailItem}>
              <Text style={styles.longboxSubHeader}>Sqft</Text>
              <Text style={styles.longboxSubHeaderNew}>
                {select?.value[0]?.BuildingAreaTotal}
              </Text>
            </View>

            <View style={styles.detailItem}>
              <Text style={styles.longboxSubHeader}>Lot Size</Text>
              <Text style={styles.longboxSubHeaderNew}>
                {select?.value[0]?.LotSizeAcres}
              </Text>
            </View>

            <View style={styles.detailItem}>
              <Text style={styles.longboxSubHeader}>Year Built</Text>
              <Text style={styles.longboxSubHeaderNew}>
                {select?.value[0]?.YearBuilt}
              </Text>
            </View>
          </View>
        </View>
        <View style={stylesSecond.addressBox}>
          <View style={stylesSecond.officeSection}>
            <View style={stylesSecond.officeDetails}>
              {/* <Image
                style={stylesSecond.officeLogo}
                src={platformData ?? Logo}
              /> */}
              <Text style={stylesSecond.officeText}>
                {select?.value[0]?.SourceSystemName}
              </Text>
              <Text style={stylesSecond.officeText}>
                {select?.value[0]?.UnparsedAddress},
              </Text>
              <Text style={stylesSecond.officeText}>
                {select?.value[0].City},&nbsp;
                {select.value[0].StateOrProvince}&nbsp;
                {select.value[0]?.PostalCode}
              </Text>
            </View>
          </View>

          <View style={stylesSecond.officeAddress}>
            {/* <Text style={stylesSecond.contactSecond}>CONTACT US NOW</Text> */}
            <Image
              style={styles.officeContactImage}
              src={profile.image} // Compressed profile image
            />
            <Text style={stylesSecond.officeText}>
              Agent: {profile?.firstName} {profile?.lastName}
            </Text>
            <Text style={stylesSecond.officeText}>
              Office: {profile?.active_office}
            </Text>
            <Text style={stylesSecond.officeText}>Phone: {profile?.phone}</Text>
          </View>
          {/* <View style={stylesSecond.officeAddress}>
            <Text style={stylesSecond.contactSecond}>CONTACT US NOW</Text>
            <Text style={stylesSecond.officeText}>
              {select?.value[0]?.ListOfficeName}
            </Text>
            <Text style={stylesSecond.officeText}>
              Agent: {select?.value[0]?.ListAgentFullName}
            </Text>
            <Text style={stylesSecond.officeText}>
              Phone: {select?.value[0]?.ListAgentOfficePhone}
            </Text>
          </View> */}
        </View>
      </Page>
    </Document>
  );



  const handleCancel = () => {
    setSelect("");
  };

  const navigate = useNavigate();

  const handleNonListed = () => {
    navigate("/real-estate-non-listed-print");
  };
  return (
    <div className="bg-secondary float-left w-100 pt-4 mb-4">
      <Loader isActive={isActive} />

      {isShow && (
        <Toaster
          types={types}
          isShow={isShow}
          toasterBody={toasterBody}
          message={message}
        />
      )}
      <main className="page-wrapper">
        <div className="row event-calender">
          {select ? (
            <div className="">
              {templateType === "type1" ? (
                <div className="">
                  {select && select.value && select.value.length > 0 && (
                    <section className="mb-5 pb-3 w-100" data-simplebar>
                      <div
                        className="bg-light border rounded-3 p-3"
                        data-thumbnails="true"
                      >
                        <div className="firstTemplate">
                          <div className="row" data-thumbnails="true">
                            <div className="col-md-12">
                              <div className="row">
                                <div className="col-md-6 mt-3">
                                  <img
                                    className="mb-2 rounded-0"
                                    src={
                                      uploadedLogo || Logo
                                      // platformData.platform_logo
                                    } // Show uploaded logo or default logo
                                    style={{
                                      width: "100px",
                                      cursor: "pointer",
                                    }} // Add cursor pointer for better UX
                                    onClick={handleLogoClick} // Click to show file input
                                  />
                                  {showLogoInput && (
                                    <label className="documentlabelAllLogo">
                                      <input
                                        id="REPC_real_estate_purchase_contract"
                                        type="file"
                                        onChange={handleLogoUpload}
                                      />
                                      <i class="h2 fi-edit opacity-80"></i>
                                    </label>
                                  )}
                                </div>
                                <div className="col-md-6 mt-3">
                                  {editingField === "price" ? (
                                    <div className="text-center">
                                      <input
                                        type="text"
                                        value={editedText} // Controlled input with edited text
                                        onChange={(e) =>
                                          setEditedText(e.target.value)
                                        } // Update text as the user types
                                        onBlur={() =>
                                          handleTextSavePrice("price")
                                        } // Save the text when the user leaves the input field
                                        autoFocus
                                      />
                                    </div>
                                  ) : (
                                    <h6
                                      className="d-flex justify-content-end"
                                      onClick={() =>
                                        handleTextPrice(
                                          "price",
                                          select?.value[0]?.ListPrice
                                        )
                                      }
                                    >
                                      Listed at $
                                      {select?.value[0]?.ListPrice ?? ""}
                                    </h6>
                                  )}

                                  {editingField === "address" ? (
                                    <div className="text-center">
                                      <input
                                        type="text"
                                        value={editedText} // Controlled input with edited text
                                        onChange={(e) =>
                                          setEditedText(e.target.value)
                                        } // Update text as the user types
                                        onBlur={() => handleTextSave("address")} // Save the text when the user leaves the input field
                                        autoFocus
                                      />
                                    </div>
                                  ) : (
                                    <h6
                                      className="d-flex justify-content-end"
                                      onClick={() =>
                                        handleTextClick(
                                          "address",
                                          select?.value[0]?.UnparsedAddress
                                        )
                                      }
                                    >
                                      {select?.value[0]?.UnparsedAddress ?? ""}
                                    </h6>
                                  )}
                                </div>
                              </div>

                              <div className="col-md-12 singleListing">
                                {select?.media && select.media.length > 0 ? (
                                  <div className="card-img-top">
                                    <img
                                      className="img-fluid rounded-0"
                                      src={
                                        uploadedImages[0] ||
                                        select.media[0].MediaURL ||
                                        defaultpropertyimage
                                      }
                                      alt="Property"
                                      onClick={() => handleImageClick(0)}
                                    />
                                    {selectedImage === 0 && (
                                      <label className="documentlabelNew">
                                        <input
                                          id="REPC_real_estate_purchase_contract"
                                          type="file"
                                          onChange={(e) =>
                                            handleImageUpload(e, 0)
                                          } // Call image upload function on change
                                        />
                                        <i className="h2 fi-edit opacity-80"></i>
                                      </label>
                                    )}
                                  </div>
                                ) : (
                                  <div className="card-img-top">
                                    <img
                                      className="img-fluid rounded-0"
                                      src={
                                        uploadedImages[0] ||
                                        defaultpropertyimage
                                      } // Show default image
                                      alt="Default Property"
                                      onClick={() => handleImageClick(0)} // Allow clicking to upload a new image
                                    />
                                    {selectedImage === 0 && (
                                      <label className="documentlabelNew">
                                        <input
                                          id="REPC_real_estate_purchase_contract"
                                          type="file"
                                          onChange={(e) =>
                                            handleImageUpload(e, 0)
                                          } // Call image upload function on change
                                        />
                                        <i className="h2 fi-edit opacity-80"></i>
                                      </label>
                                    )}
                                  </div>
                                )}
                              </div>

                              <div className="property_details">
                                <div className="col-md-12">
                                  <h4 className="mt-2 text-center">
                                    Property Details
                                  </h4>
                                </div>
                                <div className="row proprty_background">
                                  <div className="col-md-2">
                                    <h6 className="text-white">Bedrooms</h6>

                                    {editingField === "bedrooms" ? (
                                      <input
                                        className="w-100"
                                        type="text"
                                        value={editedText} // Controlled input with edited text
                                        onChange={(e) =>
                                          setEditedText(e.target.value)
                                        } // Update text as the user types
                                        onBlur={() =>
                                          handleTextSaveBedrooms("bedrooms")
                                        } // Save the text when the user leaves the input field
                                        autoFocus
                                      />
                                    ) : (
                                      <p
                                        className="text-white"
                                        onClick={() =>
                                          handleTextBedrooms(
                                            "bedrooms",
                                            select?.value[0]?.BedroomsTotal
                                          )
                                        }
                                      >
                                        {select.value[0]?.BedroomsTotal ? (
                                          <small>
                                            {select.value[0]?.BedroomsTotal}
                                          </small>
                                        ) : (
                                          ""
                                        )}
                                      </p>
                                    )}
                                  </div>

                                  <div className="col-md-2 ms-3">
                                    <h6 className="text-white">Bathrooms</h6>

                                    {editingField === "bathroom" ? (
                                      <input
                                        className="w-100"
                                        type="text"
                                        value={editedText} // Controlled input with edited text
                                        onChange={(e) =>
                                          setEditedText(e.target.value)
                                        } // Update text as the user types
                                        onBlur={() =>
                                          handleTextSaveBathroom("bathroom")
                                        } // Save the text when the user leaves the input field
                                        autoFocus
                                      />
                                    ) : (
                                      <p
                                        className="text-white"
                                        onClick={() =>
                                          handleTextBathroom(
                                            "bathroom",
                                            select?.value[0]?.BathroomsFull
                                          )
                                        }
                                      >
                                        {select.value[0]?.BathroomsFull ? (
                                          <small>
                                            {select.value[0]?.BathroomsFull ??
                                              0}
                                            /{" "}
                                            {select.value[0]?.BathroomsHalf ??
                                              0}
                                          </small>
                                        ) : (
                                          ""
                                        )}
                                      </p>
                                    )}
                                  </div>

                                  <div className="col-md-2 ms-3">
                                    <h6 className="text-white">Sqft</h6>
                                    {editingField === "squareFeet" ? (
                                      <input
                                        className="w-100"
                                        type="text"
                                        value={editedText} // Controlled input with edited text
                                        onChange={(e) =>
                                          setEditedText(e.target.value)
                                        } // Update text as the user types
                                        onBlur={() =>
                                          handleTextSaveSquareFeet("squareFeet")
                                        } // Save the text when the user leaves the input field
                                        autoFocus
                                      />
                                    ) : (
                                      <p
                                        className="text-white"
                                        onClick={() =>
                                          handleTextSquareFeet(
                                            "squareFeet",
                                            select?.value[0]?.BuildingAreaTotal
                                          )
                                        }
                                      >
                                        {select.value[0]?.BuildingAreaTotal ? (
                                          <small>
                                            {select.value[0]?.BuildingAreaTotal}
                                          </small>
                                        ) : (
                                          ""
                                        )}
                                      </p>
                                    )}
                                  </div>

                                  <div className="col-md-2 ms-3">
                                    <h6 className="text-white">Lot Size</h6>
                                    {editingField === "lotSize" ? (
                                      <input
                                        className="w-100"
                                        type="text"
                                        value={editedText} // Controlled input with edited text
                                        onChange={(e) =>
                                          setEditedText(e.target.value)
                                        } // Update text as the user types
                                        onBlur={() =>
                                          handleTextSaveLotSize("lotSize")
                                        } // Save the text when the user leaves the input field
                                        autoFocus
                                      />
                                    ) : (
                                      <p
                                        className="text-white mb-2"
                                        onClick={() =>
                                          handleTextLotSize(
                                            "lotSize",
                                            select?.value[0]?.LotSizeAcres
                                          )
                                        }
                                      >
                                        {select.value[0]?.LotSizeAcres ? (
                                          <small>
                                            {select.value[0]?.LotSizeAcres}
                                          </small>
                                        ) : (
                                          ""
                                        )}
                                      </p>
                                    )}
                                  </div>

                                  <div className="col-md-2 ms-3">
                                    <div className="d-flex">
                                      <h6 className="text-white">Year Built</h6>
                                    </div>
                                    {editingField === "yearBuilt" ? (
                                      <input
                                        className="w-100"
                                        type="text"
                                        value={editedText} // Controlled input with edited text
                                        onChange={(e) =>
                                          setEditedText(e.target.value)
                                        } // Update text as the user types
                                        onBlur={() =>
                                          handleTextSaveYearBuilt("yearBuilt")
                                        } // Save the text when the user leaves the input field
                                        autoFocus
                                      />
                                    ) : (
                                      <p
                                        className="text-white"
                                        onClick={() =>
                                          handleTextYearBuilt(
                                            "yearBuilt",
                                            select?.value[0]?.YearBuilt
                                          )
                                        }
                                      >
                                        {select.value[0]?.YearBuilt ? (
                                          <small>
                                            {select.value[0]?.YearBuilt}
                                          </small>
                                        ) : (
                                          ""
                                        )}
                                      </p>
                                    )}
                                  </div>

                                  <div className="col-md-12">
                                    {editingField === "publicRemarks" ? (
                                      <textarea
                                        className="form-control mt-0 w-100"
                                        id="textarea-input"
                                        rows="5"
                                        type="text"
                                        value={editedText} // Controlled input with edited text
                                        onChange={(e) => {
                                          const wordCount = e.target.value
                                            .trim()
                                            .split(/\s+/).length;
                                          if (wordCount <= 150) {
                                            setEditedText(e.target.value); // Update text only if word count is <= 150
                                          }
                                        }} // Update text as the user types
                                        onBlur={() =>
                                          handleTextSavePublicRemarks(
                                            "publicRemarks"
                                          )
                                        } // Save the text when the user leaves the input field
                                        autoFocus
                                      />
                                    ) : (
                                      <p
                                        className="text-white description_PrintPage"
                                        style={{ fontSize: "12px" }}
                                        onClick={() =>
                                          handleTextPublicRemarks(
                                            "publicRemarks",
                                            select?.value[0]?.PublicRemarks
                                          )
                                        }
                                      >
                                        {select.value[0]?.PublicRemarks}
                                      </p>
                                    )}
                                  </div>
                                </div>

                                <div className="row">
                                  {select?.media && select.media.length > 1
                                    ?
                                      [
                                        ...select.media.slice(1, 4),
                                        ...Array(
                                          Math.max(
                                            0,
                                            3 - select.media.slice(1, 4).length
                                          )
                                        ).fill(null),
                                      ].map((mediaItem, mediaIndex) => (
                                        <div
                                          className="col-md-4 mt-2"
                                          key={mediaIndex}
                                        >
                                          <div className="proprty_mediaImage">
                                            <img
                                              className="img-fluid w-100 rounded-0"
                                              src={
                                                mediaItem
                                                  ? uploadedImages[
                                                      mediaIndex + 1
                                                    ] ||
                                                    mediaItem.MediaURL ||
                                                    defaultpropertyimage
                                                  : defaultpropertyimage
                                              }
                                              alt={`Property media ${
                                                mediaIndex + 1
                                              }`}
                                              onClick={() =>
                                                handleImageClick(mediaIndex + 1)
                                              }
                                            />
                                            {selectedImage ===
                                              mediaIndex + 1 && (
                                              <label className="documentlabelAllImage">
                                                <input
                                                  id={`REPC_real_estate_purchase_contract_${mediaIndex}`}
                                                  type="file"
                                                  onChange={(e) =>
                                                    handleImageUpload(
                                                      e,
                                                      mediaIndex + 1
                                                    )
                                                  }
                                                />
                                                <i className="h2 fi-edit opacity-80"></i>
                                              </label>
                                            )}
                                          </div>
                                        </div>
                                      ))
                                    : // Default images if no media exists
                                      [0, 1, 2].map((_, defaultIndex) => (
                                        <div
                                          className="col-md-4 mt-2"
                                          key={defaultIndex}
                                        >
                                          <div className="proprty_mediaImage">
                                            <img
                                              className="img-fluid w-100 rounded-0"
                                              src={
                                                uploadedImages[
                                                  defaultIndex + 1
                                                ] || defaultpropertyimage
                                              }
                                              alt={`Default Property media ${
                                                defaultIndex + 1
                                              }`}
                                              onClick={() =>
                                                handleImageClick(
                                                  defaultIndex + 1
                                                )
                                              }
                                            />
                                            {selectedImage ===
                                              defaultIndex + 1 && (
                                              <label className="documentlabelAllImage">
                                                <input
                                                  id={`REPC_real_estate_purchase_contract_${defaultIndex}`}
                                                  type="file"
                                                  onChange={(e) =>
                                                    handleImageUpload(
                                                      e,
                                                      defaultIndex + 1
                                                    )
                                                  }
                                                />
                                                <i className="h2 fi-edit opacity-80"></i>
                                              </label>
                                            )}
                                          </div>
                                        </div>
                                      ))}
                                </div>

                                {/* Contact Details */}

                                <div className="row mt-2 contact_background">
                                  <div className="col-md-6">
                                    <div className="col-md-6">
                                      <div className="float-start w-100">
                                        <img
                                          className="print_Profile"
                                          src={
                                            uploadedContact ||
                                            (profile.image ? profile.image : "")
                                          } // Show uploaded logo or default avatar
                                          style={{
                                            width: "100px",
                                            cursor: "pointer",
                                          }} // Pointer cursor for better UX
                                          onClick={handleLogoClickContact} // Show file input on click
                                        />
                                        {/* Conditionally render file input on logo click */}
                                        {showContact && (
                                          <label className="documentlabelContact">
                                            <input
                                              id="REPC_real_estate_purchase_contract"
                                              type="file"
                                              onChange={handleLogoUploadContact}
                                            />
                                            <i class="h2 fi-edit opacity-80"></i>
                                          </label>
                                        )}

                                        <div className="d-flex">
                                          {editingField === "firstName" ? (
                                            <input
                                              type="text"
                                              value={editedText} // Controlled input with edited text
                                              onChange={(e) =>
                                                setEditedText(e.target.value)
                                              } // Update text as the user types
                                              onBlur={() =>
                                                handleTextSaveListAgent(
                                                  "firstName"
                                                )
                                              } // Save the text when the user leaves the input field
                                              autoFocus
                                            />
                                          ) : (
                                            <span
                                              style={{ fontSize: "12px" }}
                                              onClick={() =>
                                                handleTextListAgent(
                                                  "firstName",
                                                  profile.firstName,
                                                  profile.lastName
                                                )
                                              }
                                            >
                                              {profile && (
                                                <p className="mb-0 text-white">
                                                  <b>Agent: </b>
                                                  {profile.firstName}&nbsp;
                                                </p>
                                              )}
                                            </span>
                                          )}

                                          {editingField === "lastName" ? (
                                            <input
                                              type="text"
                                              value={editedText} // Controlled input with edited text
                                              onChange={(e) =>
                                                setEditedText(e.target.value)
                                              } // Update text as the user types
                                              onBlur={() =>
                                                handleTextSaveListAgent(
                                                  "lastName"
                                                )
                                              } // Save the text when the user leaves the input field
                                              autoFocus
                                            />
                                          ) : (
                                            <span
                                              style={{ fontSize: "12px" }}
                                              onClick={() =>
                                                handleTextListAgent(
                                                  "lastName",
                                                  profile.lastName
                                                )
                                              }
                                            >
                                              {profile && (
                                                <p className="mb-0 text-white">
                                                  {" "}
                                                  {profile.lastName}&nbsp;
                                                </p>
                                              )}
                                            </span>
                                          )}
                                        </div>

                                        {editingField === "listOffice" ? (
                                          <input
                                            type="text"
                                            value={editedText} // Controlled input with edited text
                                            onChange={(e) =>
                                              setEditedText(e.target.value)
                                            } // Update text as the user types
                                            onBlur={() =>
                                              handleTextSaveListOffice(
                                                "listOffice"
                                              )
                                            } // Save the text when the user leaves the input field
                                            autoFocus
                                          />
                                        ) : (
                                          <span
                                            style={{ fontSize: "12px" }}
                                            onClick={() =>
                                              handleTextListOffice(
                                                "listOffice",
                                                profile.active_office
                                              )
                                            }
                                          >
                                            {profile && (
                                              <p className="mb-0 text-white">
                                                <b>Office: </b>
                                                {profile.active_office}
                                              </p>
                                            )}
                                          </span>
                                        )}

                                        {editingField === "officePhone" ? (
                                          <input
                                            type="text"
                                            value={editedText} // Controlled input with edited text
                                            onChange={(e) =>
                                              setEditedText(e.target.value)
                                            } // Update text as the user types
                                            onBlur={() =>
                                              handleTextSaveOfficePhone(
                                                "officePhone"
                                              )
                                            } // Save the text when the user leaves the input field
                                            autoFocus
                                          />
                                        ) : (
                                          <span
                                            style={{ fontSize: "12px" }}
                                            onClick={() =>
                                              handleTextOfficePhone(
                                                "officePhone",
                                                profile.phone
                                              )
                                            }
                                          >
                                            {profile && (
                                              <p className="mb-0 text-white">
                                                <b>Phone: </b>
                                                {profile.phone}
                                              </p>
                                            )}
                                          </span>
                                        )}
                                      </div>
                                    </div>
                                  </div>
                                  <div className="col-md-6">
                                    <div className="pull-right text-end imageLogo w-100">
                                      {editingField === "published" ? (
                                        <input
                                          className="w-100"
                                          type="text"
                                          value={editedText} // Controlled input with edited text
                                          onChange={(e) =>
                                            setEditedText(e.target.value)
                                          } // Update text as the user types
                                          onBlur={() =>
                                            handleTextSavePublished("published")
                                          } // Save the text when the user leaves the input field
                                          autoFocus
                                        />
                                      ) : (
                                        <p
                                          className="mb-0 text-white"
                                          style={{ fontSize: "12px" }}
                                          onClick={() =>
                                            handleTextPublished(
                                              "published",
                                              select.value[0]?.SourceSystemName
                                            )
                                          }
                                        >
                                          {select?.value[0]
                                            ?.SourceSystemName && (
                                            <smal>
                                              {
                                                select?.value[0]
                                                  ?.SourceSystemName
                                              }
                                            </smal>
                                          )}
                                        </p>
                                      )}

                                      {editingField === "companyAddress" ? (
                                        <input
                                          className="w-100"
                                          type="text"
                                          value={editedText} // Controlled input with edited text
                                          onChange={(e) =>
                                            setEditedText(e.target.value)
                                          } // Update text as the user types
                                          onBlur={() =>
                                            handleTextSaveCompanyAddress(
                                              "companyAddress"
                                            )
                                          }
                                          autoFocus
                                        />
                                      ) : (
                                        <span
                                          className="mb-0 text-white"
                                          style={{ fontSize: "12px" }}
                                          onClick={() =>
                                            handleTextClickCompanyAddress(
                                              "companyAddress",
                                              select?.value[0]?.UnparsedAddress
                                            )
                                          }
                                        >
                                          {select?.value[0]?.UnparsedAddress ??
                                            ""}
                                        </span>
                                      )}
                                      <br />

                                      {editingField === "companyCity" ? (
                                        <input
                                          className="w-100"
                                          type="text"
                                          value={editedText} // Controlled input with edited text
                                          onChange={(e) =>
                                            setEditedText(e.target.value)
                                          } // Update text as the user types
                                          onBlur={() =>
                                            handleTextSaveCompanyCity(
                                              "companyCity"
                                            )
                                          }
                                          autoFocus
                                        />
                                      ) : (
                                        <span
                                          className="mb-0 text-white"
                                          style={{ fontSize: "12px" }}
                                          onClick={() =>
                                            handleTextCompanyCity(
                                              "companyCity",
                                              select?.value[0]?.City
                                            )
                                          }
                                        >
                                          {select?.value[0]?.City ?? ""},{" "}
                                        </span>
                                      )}

                                      {editingField === "companyState" ? (
                                        <input
                                          type="text"
                                          value={editedText} // Controlled input with edited text
                                          onChange={(e) =>
                                            setEditedText(e.target.value)
                                          } // Update text as the user types
                                          onBlur={() =>
                                            handleTextSaveCompanyState(
                                              "companyState"
                                            )
                                          }
                                          autoFocus
                                        />
                                      ) : (
                                        <span
                                          className="mb-0 text-white"
                                          style={{ fontSize: "12px" }}
                                          onClick={() =>
                                            handleTextCompanyState(
                                              "companyState",
                                              select?.value[0]?.StateOrProvince
                                            )
                                          }
                                        >
                                          {select?.value[0]?.StateOrProvince ??
                                            ""}{" "}
                                        </span>
                                      )}

                                      {editingField === "companyPostCode" ? (
                                        <input
                                          className="w-100"
                                          type="text"
                                          value={editedText} // Controlled input with edited text
                                          onChange={(e) =>
                                            setEditedText(e.target.value)
                                          } // Update text as the user types
                                          onBlur={() =>
                                            handleTextSaveCompanyPostCode(
                                              "companyPostCode"
                                            )
                                          } // Save the text when the user leaves the input field
                                          autoFocus
                                        />
                                      ) : (
                                        <span
                                          className="mb-0 text-white"
                                          style={{ fontSize: "12px" }}
                                          onClick={() =>
                                            handleTextCompanyPostCode(
                                              "companyPostCode",
                                              select?.value[0]?.PostalCode
                                            )
                                          }
                                        >
                                          {select?.value[0]?.PostalCode ?? ""}
                                        </span>
                                      )}
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div className="template_second">
                          <button
                            className="btn btn-secondary pull-right"
                            onClick={() => handleCancel()}
                          >
                            Cancel
                          </button>
                          <PDFDownloadLink
                            document={
                              <Flyer
                                select={select}
                                platformData={uploadedLogo}
                                profile={profile}
                                // profileImage={uploadedContact}
                              />
                            }
                            fileName={
                              select?.value[0]?.UnparsedAddress ?? "Flyer.pdf"
                            }
                          >
                            Download Pdf
                          </PDFDownloadLink>
                        </div>
                      </div>
                    </section>
                  )}
                </div>
              ) : (
                ""
              )}

              {templateType === "type2" ? (
                <>
                  {select && select.value && select.value.length > 0 && (
                    <div ref={componentRef}>
                      <section
                        className="mb-0 pb-3 bg-light border rounded-3 p-3"
                        data-simplebar
                      >
                        <div className="firstTemplate">
                          <div className="row" data-thumbnails="true">
                            <div className="col-md-12 singleListing">
                              <div className="row">
                                <div className="col-md-6 mt-3">
                                  <img
                                    className="mb-4 rounded-0 "
                                    src={
                                      uploadedLogo || Logo
                                      // platformData.platform_logo
                                    } // Show uploaded logo or default logo
                                    style={{
                                      width: "100px",
                                      cursor: "pointer",
                                    }} // Add cursor pointer for better UX
                                    onClick={handleLogoClick} // Click to show file input
                                  />
                                  {showLogoInput && (
                                    <label className="documentlabelAllLogo">
                                      <input
                                        id="REPC_real_estate_purchase_contract"
                                        type="file"
                                        onChange={handleLogoUpload}
                                      />
                                      <i class="h2 fi-edit opacity-80"></i>
                                    </label>
                                  )}
                                </div>
                                <div className="col-md-6 mt-3">
                                  {editingField === "price" ? (
                                    <div className="text-center">
                                      <input
                                        type="text"
                                        value={editedText} // Controlled input with edited text
                                        onChange={(e) =>
                                          setEditedText(e.target.value)
                                        } // Update text as the user types
                                        onBlur={() =>
                                          handleTextSavePrice("price")
                                        } // Save the text when the user leaves the input field
                                        autoFocus
                                      />
                                    </div>
                                  ) : (
                                    <h6
                                      className="d-flex justify-content-end mt-4"
                                      onClick={() =>
                                        handleTextPrice(
                                          "price",
                                          select?.value[0]?.ListPrice
                                        )
                                      }
                                    >
                                      PRICE AT $
                                      {select?.value[0]?.ListPrice ?? ""}
                                    </h6>
                                  )}
                                </div>
                              </div>

                              {select?.media[0]?.MediaURL && (
                                <div className="card-img-top">
                                  <img
                                    className="img-fluid rounded-0"
                                    src={
                                      uploadedImages[0] ||
                                      select.media[0].MediaURL ||
                                      defaultpropertyimage
                                    }
                                    alt="Property"
                                    onClick={() => handleImageClick(0)}
                                  />
                                  {selectedImage === 0 && (
                                    <label className="documentlabelSecond">
                                      <input
                                        id="REPC_real_estate_purchase_contract"
                                        type="file"
                                        onChange={(e) =>
                                          handleImageUpload(e, 0)
                                        }
                                      />
                                      <i class="h2 fi-edit opacity-80"></i>
                                    </label>
                                  )}
                                </div>
                              )}
                            </div>

                            <div className="row">
                              {select?.media && select.media.length > 1
                                ? // Process the media array and ensure we have exactly 3 images
                                  [
                                    ...select.media.slice(1, 4),
                                    ...Array(
                                      Math.max(
                                        0,
                                        3 - select.media.slice(1, 4).length
                                      )
                                    ).fill(null),
                                  ].map((mediaItem, mediaIndex) => (
                                    <div
                                      className="col-md-4 mt-2"
                                      key={mediaIndex}
                                    >
                                      <div className="second_mediaImage">
                                        <img
                                          className="img-fluid w-100 rounded-0"
                                          src={
                                            mediaItem
                                              ? uploadedImages[
                                                  mediaIndex + 1
                                                ] ||
                                                mediaItem.MediaURL ||
                                                defaultpropertyimage
                                              : defaultpropertyimage
                                          }
                                          alt={`Property media ${
                                            mediaIndex + 1
                                          }`}
                                          onClick={() =>
                                            handleImageClick(mediaIndex + 1)
                                          }
                                        />
                                        {selectedImage === mediaIndex + 1 && (
                                          <label className="documentPropertyL">
                                            <input
                                              id={`REPC_real_estate_purchase_contract_${mediaIndex}`}
                                              type="file"
                                              onChange={(e) =>
                                                handleImageUpload(
                                                  e,
                                                  mediaIndex + 1
                                                )
                                              }
                                            />
                                            <i className="h2 fi-edit opacity-80"></i>
                                          </label>
                                        )}
                                      </div>
                                    </div>
                                  ))
                                : // Default images if no media exists
                                  [0, 1, 2].map((_, defaultIndex) => (
                                    <div
                                      className="col-md-4 mt-2"
                                      key={defaultIndex}
                                    >
                                      <div className="proprty_mediaImage">
                                        <img
                                          className="img-fluid w-100 rounded-0"
                                          src={
                                            uploadedImages[defaultIndex + 1] ||
                                            defaultpropertyimage
                                          }
                                          alt={`Default Property media ${
                                            defaultIndex + 1
                                          }`}
                                          onClick={() =>
                                            handleImageClick(defaultIndex + 1)
                                          }
                                        />
                                        {selectedImage === defaultIndex + 1 && (
                                          <label className="documentlabelAllImage">
                                            <input
                                              id={`REPC_real_estate_purchase_contract_${defaultIndex}`}
                                              type="file"
                                              onChange={(e) =>
                                                handleImageUpload(
                                                  e,
                                                  defaultIndex + 1
                                                )
                                              }
                                            />
                                            <i className="h2 fi-edit opacity-80"></i>
                                          </label>
                                        )}
                                      </div>
                                    </div>
                                  ))}
                            </div>
                            <div className="propertySecond_details float-left w-100">
                              <div className="row">
                                <div className="col-md-8">
                                  <h6 className="text-center text-white">
                                    Description
                                  </h6>
                                  {/* Edit PublicRemarks */}
                                  {editingField === "publicRemarks" ? (
                                    <textarea
                                      className="form-control mt-0"
                                      id="textarea-input"
                                      rows="5"
                                      type="text"
                                      value={editedText} // Controlled input with edited text
                                      onChange={(e) => {
                                        const wordCount = e.target.value
                                          .trim()
                                          .split(/\s+/).length;
                                        if (wordCount <= 150) {
                                          setEditedText(e.target.value); // Update text only if word count is <= 150
                                        }
                                      }} // Update text as the user types
                                      onBlur={() =>
                                        handleTextSavePublicRemarks(
                                          "publicRemarks"
                                        )
                                      } // Save the text when the user leaves the input field
                                      autoFocus
                                    />
                                  ) : (
                                    <p
                                      className="mb-1 description_PrintPage text-white p-2"
                                      onClick={() =>
                                        handleTextPublicRemarks(
                                          "publicRemarks",
                                          select?.value[0]?.PublicRemarks
                                        )
                                      }
                                    >
                                      {select.value[0]?.PublicRemarks ? (
                                        <small>
                                          {select.value[0]?.PublicRemarks}
                                        </small>
                                      ) : (
                                        ""
                                      )}
                                    </p>
                                  )}
                                </div>
                                <div className="col-md-4">
                                  <h6 className="text-center text-white">
                                    Property Details
                                  </h6>
                                  <ul className="list-unstyled text-center">
                                    {editingField === "bedrooms" ? (
                                      <input
                                        className="w-100"
                                        type="text"
                                        value={editedText} // Controlled input with edited text
                                        onChange={(e) =>
                                          setEditedText(e.target.value)
                                        } // Update text as the user types
                                        onBlur={() =>
                                          handleTextSaveBedrooms("bedrooms")
                                        } // Save the text when the user leaves the input field
                                        autoFocus
                                      />
                                    ) : (
                                      <p
                                        className="text-white mb-2"
                                        onClick={() =>
                                          handleTextBedrooms(
                                            "bedrooms",
                                            select?.value[0]?.BedroomsTotal
                                          )
                                        }
                                      >
                                        {select.value[0]?.BedroomsTotal ? (
                                          <small>
                                            Bedrooms:{" "}
                                            {select.value[0]?.BedroomsTotal}
                                          </small>
                                        ) : (
                                          ""
                                        )}
                                      </p>
                                    )}

                                    {editingField === "bathroom" ? (
                                      <input
                                        className="w-100"
                                        type="text"
                                        value={editedText} // Controlled input with edited text
                                        onChange={(e) =>
                                          setEditedText(e.target.value)
                                        } // Update text as the user types
                                        onBlur={() =>
                                          handleTextSaveBathroom("bathroom")
                                        } // Save the text when the user leaves the input field
                                        autoFocus
                                      />
                                    ) : (
                                      <p
                                        className="text-white mb-2"
                                        onClick={() =>
                                          handleTextBathroom(
                                            "bathroom",
                                            select?.value[0]?.BathroomsFull
                                          )
                                        }
                                      >
                                        {select.value[0]?.BathroomsFull ? (
                                          <small>
                                            Bathrooms:{" "}
                                            {select.value[0]?.BathroomsFull ??
                                              0}
                                            /{" "}
                                            {select.value[0]?.BathroomsHalf ??
                                              0}
                                          </small>
                                        ) : (
                                          ""
                                        )}
                                      </p>
                                    )}

                                    {editingField === "squareFeet" ? (
                                      <input
                                        className="w-100"
                                        type="text"
                                        value={editedText} // Controlled input with edited text
                                        onChange={(e) =>
                                          setEditedText(e.target.value)
                                        } // Update text as the user types
                                        onBlur={() =>
                                          handleTextSaveSquareFeet("squareFeet")
                                        } // Save the text when the user leaves the input field
                                        autoFocus
                                      />
                                    ) : (
                                      <p
                                        className="text-white mb-2"
                                        onClick={() =>
                                          handleTextSquareFeet(
                                            "squareFeet",
                                            select?.value[0]?.BuildingAreaTotal
                                          )
                                        }
                                      >
                                        {select.value[0]?.BuildingAreaTotal ? (
                                          <small>
                                            Sqft:{" "}
                                            {select.value[0]?.BuildingAreaTotal}
                                          </small>
                                        ) : (
                                          ""
                                        )}
                                      </p>
                                    )}

                                    {editingField === "lotSize" ? (
                                      <input
                                        className="w-100"
                                        type="text"
                                        value={editedText} // Controlled input with edited text
                                        onChange={(e) =>
                                          setEditedText(e.target.value)
                                        } // Update text as the user types
                                        onBlur={() =>
                                          handleTextSaveLotSize("lotSize")
                                        } // Save the text when the user leaves the input field
                                        autoFocus
                                      />
                                    ) : (
                                      <p
                                        className="text-white mb-2"
                                        onClick={() =>
                                          handleTextLotSize(
                                            "lotSize",
                                            select?.value[0]?.LotSizeAcres
                                          )
                                        }
                                      >
                                        {select.value[0]?.LotSizeAcres ? (
                                          <small>
                                            Lot size :{" "}
                                            {select.value[0]?.LotSizeAcres}
                                          </small>
                                        ) : (
                                          ""
                                        )}
                                      </p>
                                    )}

                                    {editingField === "yearBuilt" ? (
                                      <input
                                        className="w-100"
                                        type="text"
                                        value={editedText} // Controlled input with edited text
                                        onChange={(e) =>
                                          setEditedText(e.target.value)
                                        } // Update text as the user types
                                        onBlur={() =>
                                          handleTextSaveYearBuilt("yearBuilt")
                                        } // Save the text when the user leaves the input field
                                        autoFocus
                                      />
                                    ) : (
                                      <p
                                        className="text-white mb-2"
                                        onClick={() =>
                                          handleTextYearBuilt(
                                            "yearBuilt",
                                            select?.value[0]?.YearBuilt
                                          )
                                        }
                                      >
                                        {select.value[0]?.YearBuilt ? (
                                          <small>
                                            Year Built :{" "}
                                            {select.value[0]?.YearBuilt}
                                          </small>
                                        ) : (
                                          ""
                                        )}
                                      </p>
                                    )}
                                  </ul>
                                </div>
                              </div>
                            </div>

                            <div className="row mt-2 contactSecond_background">
                              <div className="col-md-6 float-left">
                                {editingField === "published" ? (
                                  <input
                                    className="w-100"
                                    type="text"
                                    value={editedText} // Controlled input with edited text
                                    onChange={(e) =>
                                      setEditedText(e.target.value)
                                    } // Update text as the user types
                                    onBlur={() =>
                                      handleTextSavePublished("published")
                                    } // Save the text when the user leaves the input field
                                    autoFocus
                                  />
                                ) : (
                                  <p
                                    className="text-white mb-0"
                                    onClick={() =>
                                      handleTextPublished(
                                        "published",
                                        select.value[0]?.SourceSystemName
                                      )
                                    }
                                  >
                                    {select?.value[0]?.SourceSystemName && (
                                      <smal>
                                        {select?.value[0]?.SourceSystemName}
                                      </smal>
                                    )}
                                  </p>
                                )}

                                {editingField === "companyAddress" ? (
                                  <input
                                    className="w-100"
                                    type="text"
                                    value={editedText} // Controlled input with edited text
                                    onChange={(e) =>
                                      setEditedText(e.target.value)
                                    } // Update text as the user types
                                    onBlur={() =>
                                      handleTextSaveCompanyAddress(
                                        "companyAddress"
                                      )
                                    }
                                    autoFocus
                                  />
                                ) : (
                                  <span
                                    className="text-white mb-0"
                                    onClick={() =>
                                      handleTextClickCompanyAddress(
                                        "companyAddress",
                                        select?.value[0]?.UnparsedAddress
                                      )
                                    }
                                  >
                                    {select?.value[0]?.UnparsedAddress ?? ""}
                                  </span>
                                )}
                                <div className="d-flex">
                                  {editingField === "companyCity" ? (
                                    <input
                                      className="w-100"
                                      type="text"
                                      value={editedText} // Controlled input with edited text
                                      onChange={(e) =>
                                        setEditedText(e.target.value)
                                      } // Update text as the user types
                                      onBlur={() =>
                                        handleTextSaveCompanyCity("companyCity")
                                      }
                                      autoFocus
                                    />
                                  ) : (
                                    <span
                                      className="text-white mb-0"
                                      onClick={() =>
                                        handleTextCompanyCity(
                                          "companyCity",
                                          select?.value[0]?.City
                                        )
                                      }
                                    >
                                      {select?.value[0]?.City ?? ""}, &nbsp;{" "}
                                    </span>
                                  )}

                                  {editingField === "companyState" ? (
                                    <input
                                      type="text"
                                      value={editedText} // Controlled input with edited text
                                      onChange={(e) =>
                                        setEditedText(e.target.value)
                                      } // Update text as the user types
                                      onBlur={() =>
                                        handleTextSaveCompanyState(
                                          "companyState"
                                        )
                                      }
                                      autoFocus
                                    />
                                  ) : (
                                    <p
                                      className="text-white mb-0"
                                      onClick={() =>
                                        handleTextCompanyState(
                                          "companyState",
                                          select?.value[0]?.StateOrProvince
                                        )
                                      }
                                    >
                                      {select?.value[0]?.StateOrProvince ?? ""}{" "}
                                      &nbsp;{" "}
                                    </p>
                                  )}

                                  {editingField === "companyPostCode" ? (
                                    <input
                                      className="w-100"
                                      type="text"
                                      value={editedText} // Controlled input with edited text
                                      onChange={(e) =>
                                        setEditedText(e.target.value)
                                      } // Update text as the user types
                                      onBlur={() =>
                                        handleTextSaveCompanyPostCode(
                                          "companyPostCode"
                                        )
                                      } // Save the text when the user leaves the input field
                                      autoFocus
                                    />
                                  ) : (
                                    <p
                                      className="text-white mb-0"
                                      onClick={() =>
                                        handleTextCompanyPostCode(
                                          "companyPostCode",
                                          select?.value[0]?.PostalCode
                                        )
                                      }
                                    >
                                      {select?.value[0]?.PostalCode ?? ""}
                                    </p>
                                  )}
                                </div>
                              </div>

                              <div className="col-md-6">
                                <ul className="list-unstyled text-end">
                                  <img
                                    className="print_Profile"
                                    src={
                                      uploadedContact ||
                                      (profile.image ? profile.image : "")
                                    } // Show uploaded logo or default avatar
                                    style={{
                                      width: "100px",
                                      cursor: "pointer",
                                    }} // Pointer cursor for better UX
                                    onClick={handleLogoClickContact} // Show file input on click
                                  />
                                  {/* Conditionally render file input on logo click */}
                                  {showContact && (
                                    <label className="documentlabelContact">
                                      <input
                                        id="REPC_real_estate_purchase_contract"
                                        type="file"
                                        onChange={handleLogoUploadContact}
                                      />
                                      <i class="h2 fi-edit opacity-80"></i>
                                    </label>
                                  )}
                                 
                                  <div className="d-flex justify-content-end">
                                    {editingField === "firstName" ? (
                                      <input
                                        type="text"
                                        value={editedText} // Controlled input with edited text
                                        onChange={(e) =>
                                          setEditedText(e.target.value)
                                        } // Update text as the user types
                                        onBlur={() =>
                                          handleTextSaveListAgent("firstName")
                                        } // Save the text when the user leaves the input field
                                        autoFocus
                                      />
                                    ) : (
                                      <p
                                        className="text-white mb-0"
                                        onClick={() =>
                                          handleTextListAgent(
                                            "firstName",
                                            profile.firstName,
                                            profile.lastName
                                          )
                                        }
                                      >
                                        {profile && (
                                          <p className="mb-0">
                                            <b>Agent: </b>
                                            {profile.firstName}&nbsp;
                                          </p>
                                        )}
                                      </p>
                                    )}

                                    {editingField === "lastName" ? (
                                      <input
                                        type="text"
                                        value={editedText} // Controlled input with edited text
                                        onChange={(e) =>
                                          setEditedText(e.target.value)
                                        } // Update text as the user types
                                        onBlur={() =>
                                          handleTextSaveListAgent("lastName")
                                        } // Save the text when the user leaves the input field
                                        autoFocus
                                      />
                                    ) : (
                                      <p
                                        className="text-white mb-0"
                                        onClick={() =>
                                          handleTextListAgent(
                                            "lastName",
                                            profile.lastName
                                          )
                                        }
                                      >
                                        {profile && (
                                          <p className="mb-0">
                                            {" "}
                                            {profile.lastName}&nbsp;
                                          </p>
                                        )}
                                      </p>
                                    )}
                                  </div>


                                  {editingField === "listOffice" ? (
                                    <input
                                      type="text"
                                      value={editedText} // Controlled input with edited text
                                      onChange={(e) =>
                                        setEditedText(e.target.value)
                                      } // Update text as the user types
                                      onBlur={() =>
                                        handleTextSaveListOffice("listOffice")
                                      } // Save the text when the user leaves the input field
                                      autoFocus
                                    />
                                  ) : (
                                    <p
                                      className="text-white mb-0"
                                      onClick={() =>
                                        handleTextListOffice(
                                          "listOffice",
                                          profile.active_office
                                        )
                                      }
                                    >
                                      {profile && (
                                        <p className="mb-0">
                                          <b>Office: </b>
                                          {profile.active_office}
                                        </p>
                                      )}
                                    </p>
                                  )}

                                  {editingField === "officePhone" ? (
                                    <input
                                      type="text"
                                      value={editedText} // Controlled input with edited text
                                      onChange={(e) =>
                                        setEditedText(e.target.value)
                                      } // Update text as the user types
                                      onBlur={() =>
                                        handleTextSaveOfficePhone("officePhone")
                                      } // Save the text when the user leaves the input field
                                      autoFocus
                                    />
                                  ) : (
                                    <p
                                      className="text-white mb-0"
                                      onClick={() =>
                                        handleTextOfficePhone(
                                          "officePhone",
                                          profile.phone
                                        )
                                      }
                                    >
                                      {profile && (
                                        <p className="mb-0">
                                          <b>Phone: </b>
                                          {profile.phone}
                                        </p>
                                      )}
                                    </p>
                                  )}
                                </ul>
                              </div>
                            </div>
                          </div>
                        </div>

                        <div className="template_second">
                          <button
                            className="btn btn-secondary pull-right"
                            onClick={() => handleCancel()}
                          >
                            Cancel
                          </button>

                          <PDFDownloadLink
                            document={
                              <FlyerAlternative
                                select={select}
                                platformData={uploadedLogo}
                                profile={profile}
                              />
                            }
                            fileName={
                              select?.value[0]?.UnparsedAddress ?? "Flyer.pdf"
                            }
                          >
                            Download Pdf
                          </PDFDownloadLink>
                        </div>
                      </section>
                    </div>
                  )}
                </>
              ) : (
                ""
              )}

              {templateType === "type3" ? (
                <>
                  {select && select.value && select.value.length > 0 && (
                    <>
                      <section
                        className="mb-0 pb-3 bg-light border rounded-3 p-3"
                        data-simplebar
                      >
                        <div className="thirdTemplate">
                          <div className="row">
                            <div className="col-md-6 mt-3">
                              <img
                                className="mb-4 rounded-0 "
                                src={
                                  uploadedLogo || Logo
                                  // platformData.platform_logo
                                } // Show uploaded logo or default logo
                                style={{
                                  width: "100px",
                                  cursor: "pointer",
                                }} // Add cursor pointer for better UX
                                onClick={handleLogoClick} // Click to show file input
                              />
                              {showLogoInput && (
                                <label className="documentlabelAllLogo">
                                  <input
                                    id="REPC_real_estate_purchase_contract"
                                    type="file"
                                    onChange={handleLogoUpload}
                                  />
                                  <i class="h2 fi-edit opacity-80"></i>
                                </label>
                              )}
                            </div>
                            <div className="col-md-6 mt-3">
                              {editingField === "price" ? (
                                <div className="text-center">
                                  <input
                                    type="text"
                                    value={editedText} // Controlled input with edited text
                                    onChange={(e) =>
                                      setEditedText(e.target.value)
                                    } // Update text as the user types
                                    onBlur={() => handleTextSavePrice("price")} // Save the text when the user leaves the input field
                                    autoFocus
                                  />
                                </div>
                              ) : (
                                <h6
                                  className="d-flex justify-content-end mt-4"
                                  style={{ color: "red" }}
                                  onClick={() =>
                                    handleTextPrice(
                                      "price",
                                      select?.value[0]?.ListPrice
                                    )
                                  }
                                >
                                  PRICE AT ${select?.value[0]?.ListPrice ?? ""}
                                </h6>
                              )}
                            </div>
                          </div>

                          <div className="row" data-thumbnails="true">
                            <div className="col-md-6 singleListing mb-3">
                              {select?.media[0]?.MediaURL && (
                                <div className="card-img-top">
                                  <img
                                    className="img-fluid rounded-0"
                                    src={
                                      uploadedImages[0] ||
                                      select.media[0].MediaURL ||
                                      defaultpropertyimage
                                    }
                                    alt="Property"
                                    onClick={() => handleImageClick(0)}
                                  />
                                  {selectedImage === 0 && (
                                    <label className="documentlabelThird">
                                      <input
                                        id="REPC_real_estate_purchase_contract"
                                        type="file"
                                        onChange={(e) =>
                                          handleImageUpload(e, 0)
                                        }
                                      />
                                      <i class="h2 fi-edit opacity-80"></i>
                                    </label>
                                  )}
                                </div>
                              )}

                              {/* Edit PublicRemarks */}
                              <div className="mt-2">
                                <h6 className="text-center text-white">
                                  Description
                                </h6>
                                {editingField === "publicRemarks" ? (
                                  <textarea
                                    className="form-control mt-0"
                                    id="textarea-input"
                                    rows="5"
                                    type="text"
                                    value={editedText} // Controlled input with edited text
                                    onChange={(e) => {
                                      const wordCount = e.target.value
                                        .trim()
                                        .split(/\s+/).length;
                                      if (wordCount <= 150) {
                                        setEditedText(e.target.value); // Update text only if word count is <= 150
                                      }
                                    }} // Update text as the user types
                                    onBlur={() =>
                                      handleTextSavePublicRemarks(
                                        "publicRemarks"
                                      )
                                    } // Save the text when the user leaves the input field
                                    autoFocus
                                  />
                                ) : (
                                  <span
                                    onClick={() =>
                                      handleTextPublicRemarks(
                                        "publicRemarks",
                                        select?.value[0]?.PublicRemarks
                                      )
                                    }
                                  >
                                    {select.value[0]?.PublicRemarks ? (
                                      <div className="mb-4">
                                        <p className="mb-1 description_PrintPage text-white">
                                          {select.value[0]?.PublicRemarks}
                                        </p>
                                      </div>
                                    ) : (
                                      ""
                                    )}
                                  </span>
                                )}
                              </div>
                            </div>
                            <div className="col-md-6">
                              {select?.media && select.media.length > 1
                                ? 
                                  [
                                    ...select.media.slice(1, 4),
                                    ...Array(
                                      Math.max(
                                        0,
                                        3 - select.media.slice(1, 4).length
                                      )
                                    ).fill(null),
                                  ].map((mediaItem, mediaIndex) => (
                                    <div
                                      className="col-md-12 mt-2 p-3 mb-2"
                                      key={mediaIndex}
                                    >
                                      <div className="second_mediaImage">
                                        <img
                                          className="img-fluid w-100 rounded-0"
                                          src={
                                            mediaItem
                                              ? uploadedImages[
                                                  mediaIndex + 1
                                                ] ||
                                                mediaItem.MediaURL ||
                                                defaultpropertyimage
                                              : defaultpropertyimage
                                          }
                                          alt={`Property media ${
                                            mediaIndex + 1
                                          }`}
                                          onClick={() =>
                                            handleImageClick(mediaIndex + 1)
                                          }
                                        />
                                        {selectedImage === mediaIndex + 1 && (
                                          <label className="documentlabelAllImage">
                                            <input
                                              id={`REPC_real_estate_purchase_contract_${mediaIndex}`}
                                              type="file"
                                              onChange={(e) =>
                                                handleImageUpload(
                                                  e,
                                                  mediaIndex + 1
                                                )
                                              }
                                            />
                                            <i className="h2 fi-edit opacity-80"></i>
                                          </label>
                                        )}
                                      </div>
                                    </div>
                                  ))
                                : // Default images if no media exists
                                  [0, 1, 2].map((_, defaultIndex) => (
                                    <div
                                      className="col-md-4 mt-2"
                                      key={defaultIndex}
                                    >
                                      <div className="proprty_mediaImage">
                                        <img
                                          className="img-fluid w-100 rounded-0"
                                          src={
                                            uploadedImages[defaultIndex + 1] ||
                                            defaultpropertyimage
                                          }
                                          alt={`Default Property media ${
                                            defaultIndex + 1
                                          }`}
                                          onClick={() =>
                                            handleImageClick(defaultIndex + 1)
                                          }
                                        />
                                        {selectedImage === defaultIndex + 1 && (
                                          <label className="documentlabelAllImage">
                                            <input
                                              id={`REPC_real_estate_purchase_contract_${defaultIndex}`}
                                              type="file"
                                              onChange={(e) =>
                                                handleImageUpload(
                                                  e,
                                                  defaultIndex + 1
                                                )
                                              }
                                            />
                                            <i className="h2 fi-edit opacity-80"></i>
                                          </label>
                                        )}
                                      </div>
                                    </div>
                                  ))}
                            </div>

                            {/* <div className="col-md-6">
                              {select.media
                                .slice(1, 4)
                                .map((mediaItem, mediaIndex) => (
                                  <div
                                    className="col-md-12 mt-2 p-3 mb-2"
                                    key={mediaIndex}
                                  >
                                    <div className="second_mediaImage">
                                      <img
                                        className="img-fluid w-100 rounded-0"
                                        src={
                                          uploadedImages[mediaIndex + 1] ||
                                          mediaItem.MediaURL
                                        }
                                        alt={`Property media ${mediaIndex + 1}`}
                                        onClick={() =>
                                          handleImageClick(mediaIndex + 1)
                                        }
                                      />

                                      {selectedImage === mediaIndex + 1 && (
                                        <label className="documentlabelAllImage">
                                          <input
                                            id="REPC_real_estate_purchase_contract"
                                            type="file"
                                            onChange={(e) =>
                                              handleImageUpload(
                                                e,
                                                mediaIndex + 1
                                              )
                                            }
                                          />
                                          <i class="h2 fi-edit opacity-80"></i>
                                        </label>
                                      )}
                                    </div>
                                  </div>
                                ))}
                            </div> */}
                          </div>

                          <div className="row thirdDetails_Property">
                            <div className="col-md-2">
                              <h6 className="text-white">Bedrooms</h6>

                              {editingField === "bedrooms" ? (
                                <input
                                  className="w-100"
                                  type="text"
                                  value={editedText} // Controlled input with edited text
                                  onChange={(e) =>
                                    setEditedText(e.target.value)
                                  } // Update text as the user types
                                  onBlur={() =>
                                    handleTextSaveBedrooms("bedrooms")
                                  } // Save the text when the user leaves the input field
                                  autoFocus
                                />
                              ) : (
                                <p
                                  className="text-white"
                                  onClick={() =>
                                    handleTextBedrooms(
                                      "bedrooms",
                                      select?.value[0]?.BedroomsTotal
                                    )
                                  }
                                >
                                  {select.value[0]?.BedroomsTotal ? (
                                    <small>
                                      {select.value[0]?.BedroomsTotal}
                                    </small>
                                  ) : (
                                    ""
                                  )}
                                </p>
                              )}
                            </div>

                            <div className="col-md-2 ms-3">
                              <h6 className="text-white">Bathrooms</h6>

                              {editingField === "bathroom" ? (
                                <input
                                  className="w-100"
                                  type="text"
                                  value={editedText} // Controlled input with edited text
                                  onChange={(e) =>
                                    setEditedText(e.target.value)
                                  } // Update text as the user types
                                  onBlur={() =>
                                    handleTextSaveBathroom("bathroom")
                                  } // Save the text when the user leaves the input field
                                  autoFocus
                                />
                              ) : (
                                <p
                                  className="text-white"
                                  onClick={() =>
                                    handleTextBathroom(
                                      "bathroom",
                                      select?.value[0]?.BathroomsFull
                                    )
                                  }
                                >
                                  {select.value[0]?.BathroomsFull ? (
                                    <small>
                                      {select.value[0]?.BathroomsFull ?? 0}/{" "}
                                      {select.value[0]?.BathroomsHalf ?? 0}
                                    </small>
                                  ) : (
                                    ""
                                  )}
                                </p>
                              )}
                            </div>

                            <div className="col-md-2 ms-4">
                              <h6 className="text-white">Sqft</h6>
                              {editingField === "squareFeet" ? (
                                <input
                                  className="w-100"
                                  type="text"
                                  value={editedText} // Controlled input with edited text
                                  onChange={(e) =>
                                    setEditedText(e.target.value)
                                  } // Update text as the user types
                                  onBlur={() =>
                                    handleTextSaveSquareFeet("squareFeet")
                                  } // Save the text when the user leaves the input field
                                  autoFocus
                                />
                              ) : (
                                <p
                                  className="text-white"
                                  onClick={() =>
                                    handleTextSquareFeet(
                                      "squareFeet",
                                      select?.value[0]?.BuildingAreaTotal
                                    )
                                  }
                                >
                                  {select.value[0]?.BuildingAreaTotal ? (
                                    <small>
                                      {select.value[0]?.BuildingAreaTotal}
                                    </small>
                                  ) : (
                                    ""
                                  )}
                                </p>
                              )}
                            </div>

                            <div className="col-md-2 ms-3">
                              <h6 className="text-white">Lot Size</h6>
                              {editingField === "lotSize" ? (
                                <input
                                  className="w-100"
                                  type="text"
                                  value={editedText} // Controlled input with edited text
                                  onChange={(e) =>
                                    setEditedText(e.target.value)
                                  } // Update text as the user types
                                  onBlur={() =>
                                    handleTextSaveLotSize("lotSize")
                                  } // Save the text when the user leaves the input field
                                  autoFocus
                                />
                              ) : (
                                <p
                                  className="text-white mb-2"
                                  style={{ fontSize: "12px" }}
                                  onClick={() =>
                                    handleTextLotSize(
                                      "lotSize",
                                      select?.value[0]?.LotSizeAcres
                                    )
                                  }
                                >
                                  {select.value[0]?.LotSizeAcres ? (
                                    <small>
                                      {select.value[0]?.LotSizeAcres}
                                    </small>
                                  ) : (
                                    ""
                                  )}
                                </p>
                              )}
                            </div>

                            <div className="col-md-2 ms-5">
                              <h6 className="text-white">Year Built</h6>
                              {editingField === "yearBuilt" ? (
                                <input
                                  className="w-100"
                                  type="text"
                                  value={editedText} // Controlled input with edited text
                                  onChange={(e) =>
                                    setEditedText(e.target.value)
                                  } // Update text as the user types
                                  onBlur={() =>
                                    handleTextSaveYearBuilt("yearBuilt")
                                  } // Save the text when the user leaves the input field
                                  autoFocus
                                />
                              ) : (
                                <p
                                  className="text-white"
                                  style={{ fontSize: "12px" }}
                                  onClick={() =>
                                    handleTextYearBuilt(
                                      "yearBuilt",
                                      select?.value[0]?.YearBuilt
                                    )
                                  }
                                >
                                  {select.value[0]?.YearBuilt ? (
                                    <small>{select.value[0]?.YearBuilt}</small>
                                  ) : (
                                    ""
                                  )}
                                </p>
                              )}
                            </div>
                          </div>

                          <div className="row mt-2 contactSecond_background">
                            <div className="col-md-6 float-left">
                              <div className="imageLogo w-100">
                                {editingField === "published" ? (
                                  <input
                                    className="w-100"
                                    type="text"
                                    value={editedText} // Controlled input with edited text
                                    onChange={(e) =>
                                      setEditedText(e.target.value)
                                    } // Update text as the user types
                                    onBlur={() =>
                                      handleTextSavePublished("published")
                                    } // Save the text when the user leaves the input field
                                    autoFocus
                                  />
                                ) : (
                                  <p
                                    className="text-white mb-0"
                                    onClick={() =>
                                      handleTextPublished(
                                        "published",
                                        select.value[0]?.SourceSystemName
                                      )
                                    }
                                  >
                                    {select?.value[0]?.SourceSystemName && (
                                      <smal>
                                        {select?.value[0]?.SourceSystemName}
                                      </smal>
                                    )}
                                  </p>
                                )}

                                {editingField === "companyAddress" ? (
                                  <input
                                    className="w-100"
                                    type="text"
                                    value={editedText} // Controlled input with edited text
                                    onChange={(e) =>
                                      setEditedText(e.target.value)
                                    } // Update text as the user types
                                    onBlur={() =>
                                      handleTextSaveCompanyAddress(
                                        "companyAddress"
                                      )
                                    }
                                    autoFocus
                                  />
                                ) : (
                                  <span
                                    className="text-white mb-0"
                                    onClick={() =>
                                      handleTextClickCompanyAddress(
                                        "companyAddress",
                                        select?.value[0]?.UnparsedAddress
                                      )
                                    }
                                  >
                                    {select?.value[0]?.UnparsedAddress ?? ""}
                                  </span>
                                )}
                                <br />

                                <div className="d-flex">
                                  {editingField === "companyCity" ? (
                                    <input
                                      className="w-100"
                                      type="text"
                                      value={editedText} // Controlled input with edited text
                                      onChange={(e) =>
                                        setEditedText(e.target.value)
                                      } // Update text as the user types
                                      onBlur={() =>
                                        handleTextSaveCompanyCity("companyCity")
                                      }
                                      autoFocus
                                    />
                                  ) : (
                                    <span
                                      className="text-white mb-0"
                                      onClick={() =>
                                        handleTextCompanyCity(
                                          "companyCity",
                                          select?.value[0]?.City
                                        )
                                      }
                                    >
                                      {select?.value[0]?.City ?? ""},{" "}
                                    </span>
                                  )}

                                  {editingField === "companyState" ? (
                                    <input
                                      type="text"
                                      value={editedText} // Controlled input with edited text
                                      onChange={(e) =>
                                        setEditedText(e.target.value)
                                      } // Update text as the user types
                                      onBlur={() =>
                                        handleTextSaveCompanyState(
                                          "companyState"
                                        )
                                      }
                                      autoFocus
                                    />
                                  ) : (
                                    <p
                                      className="text-white mb-0"
                                      onClick={() =>
                                        handleTextCompanyState(
                                          "companyState",
                                          select?.value[0]?.StateOrProvince
                                        )
                                      }
                                    >
                                      {select?.value[0]?.StateOrProvince ?? ""}{" "}
                                      &nbsp;{" "}
                                    </p>
                                  )}

                                  {editingField === "companyPostCode" ? (
                                    <input
                                      className="w-100"
                                      type="text"
                                      value={editedText} // Controlled input with edited text
                                      onChange={(e) =>
                                        setEditedText(e.target.value)
                                      } // Update text as the user types
                                      onBlur={() =>
                                        handleTextSaveCompanyPostCode(
                                          "companyPostCode"
                                        )
                                      } // Save the text when the user leaves the input field
                                      autoFocus
                                    />
                                  ) : (
                                    <p
                                      className="text-white mb-0"
                                      onClick={() =>
                                        handleTextCompanyPostCode(
                                          "companyPostCode",
                                          select?.value[0]?.PostalCode
                                        )
                                      }
                                    >
                                      {select?.value[0]?.PostalCode ?? ""}
                                    </p>
                                  )}
                                </div>
                              </div>
                            </div>

                            <div className="col-md-6">
                              <ul className="list-unstyled text-end">
                                <img
                                  className="print_Profile"
                                  src={
                                    uploadedContact ||
                                    (profile.image ? profile.image : "")
                                  } // Show uploaded logo or default avatar
                                  style={{
                                    width: "100px",
                                    cursor: "pointer",
                                  }} // Pointer cursor for better UX
                                  onClick={handleLogoClickContact} // Show file input on click
                                />
                                {/* Conditionally render file input on logo click */}
                                {showContact && (
                                  <label className="documentlabelContact">
                                    <input
                                      id="REPC_real_estate_purchase_contract"
                                      type="file"
                                      onChange={handleLogoUploadContact}
                                    />
                                    <i class="h2 fi-edit opacity-80"></i>
                                  </label>
                                )}
                                <div className="d-flex justify-content-end">
                                  {editingField === "firstName" ? (
                                    <input
                                      type="text"
                                      value={editedText} // Controlled input with edited text
                                      onChange={(e) =>
                                        setEditedText(e.target.value)
                                      } // Update text as the user types
                                      onBlur={() =>
                                        handleTextSaveListAgent("firstName")
                                      } // Save the text when the user leaves the input field
                                      autoFocus
                                    />
                                  ) : (
                                    <p
                                      className="text-white mb-0"
                                      onClick={() =>
                                        handleTextListAgent(
                                          "firstName",
                                          profile.firstName,
                                          profile.lastName
                                        )
                                      }
                                    >
                                      {profile && (
                                        <p className="mb-0">
                                          <b>Agent: </b>
                                          {profile.firstName}&nbsp;
                                        </p>
                                      )}
                                    </p>
                                  )}

                                  {editingField === "lastName" ? (
                                    <input
                                      type="text"
                                      value={editedText} // Controlled input with edited text
                                      onChange={(e) =>
                                        setEditedText(e.target.value)
                                      } // Update text as the user types
                                      onBlur={() =>
                                        handleTextSaveListAgent("lastName")
                                      } // Save the text when the user leaves the input field
                                      autoFocus
                                    />
                                  ) : (
                                    <p
                                      className="text-white mb-0"
                                      onClick={() =>
                                        handleTextListAgent(
                                          "lastName",
                                          profile.lastName
                                        )
                                      }
                                    >
                                      {profile && (
                                        <p className="mb-0">
                                          {" "}
                                          {profile.lastName}&nbsp;
                                        </p>
                                      )}
                                    </p>
                                  )}
                                </div>

                                {editingField === "listOffice" ? (
                                  <input
                                    type="text"
                                    value={editedText} // Controlled input with edited text
                                    onChange={(e) =>
                                      setEditedText(e.target.value)
                                    } // Update text as the user types
                                    onBlur={() =>
                                      handleTextSaveListOffice("listOffice")
                                    } // Save the text when the user leaves the input field
                                    autoFocus
                                  />
                                ) : (
                                  <p
                                    className="text-white mb-0"
                                    onClick={() =>
                                      handleTextListOffice(
                                        "listOffice",
                                        profile.active_office
                                      )
                                    }
                                  >
                                    {profile && (
                                      <p className="mb-0">
                                        <b>Office: </b>
                                        {profile.active_office}
                                      </p>
                                    )}
                                  </p>
                                )}

                                {editingField === "officePhone" ? (
                                  <input
                                    type="text"
                                    value={editedText} // Controlled input with edited text
                                    onChange={(e) =>
                                      setEditedText(e.target.value)
                                    } // Update text as the user types
                                    onBlur={() =>
                                      handleTextSaveOfficePhone("officePhone")
                                    } // Save the text when the user leaves the input field
                                    autoFocus
                                  />
                                ) : (
                                  <p
                                    className="text-white mb-0"
                                    onClick={() =>
                                      handleTextOfficePhone(
                                        "officePhone",
                                        profile.phone
                                      )
                                    }
                                  >
                                    {profile && (
                                      <p className="mb-0">
                                        <b>Phone: </b>
                                        {profile.phone}
                                      </p>
                                    )}
                                  </p>
                                )}
                              </ul>
                            </div>
                          </div>
                        </div>

                        <div className="template_second">
                          <button
                            className="btn btn-secondary pull-right"
                            onClick={() => handleCancel()}
                          >
                            Cancel
                          </button>
                          <PDFDownloadLink
                            document={
                              <FlyerAlternativeThird
                                select={select}
                                platformData={uploadedLogo}
                                profile={profile}
                              />
                            }
                            fileName={
                              select?.value[0]?.UnparsedAddress ?? "Flyer.pdf"
                            }
                          >
                            Download Pdf
                          </PDFDownloadLink>
                        </div>
                      </section>
                    </>
                  )}
                </>
              ) : (
                ""
              )}

              {/* {templateType === "type4" ? (
                <>
                  {select && select.value && select.value.length > 0 && (
                    <>
                      <section className="mb-0 pb-3" data-simplebar>
                        <div
                          className="row bg-light border rounded-3 p-3"
                          data-thumbnails="true"
                        >
                          <div className="col-md-12 singleListing">
                            {select?.media[0]?.MediaURL && (
                              <div className="card-img-top">
                                <img
                                  className="img-fluid"
                                  src={select.media[0].MediaURL}
                                  alt="Property"
                                />
                              </div>
                            )}
                          </div>

                          <div className="col-md-12 mt-3">
                            <div className="printTempalte_one">
                              <div className="row">
                                {select.media
                                  .slice(0, 4)
                                  .map((mediaItem, mediaIndex) => (
                                    <div className="col-md-3">
                                      <img
                                        className=""
                                        key={mediaIndex}
                                        src={mediaItem.MediaURL}
                                      />
                                    </div>
                                  ))}
                              </div>
                            </div>
                          </div>

                          <section className="row mt-3">
                            <div className="col-md-6">
                              <h3 className="h4">PROPERTY DETAILS</h3>
                              <div className="col-md-12 pb-md-3 d-flex">
                             

                                <ul className="list-unstyled col-md-6">
                                  {select.value[0]?.ListingId ? (
                                    <li>
                                      <b>MLS Number: </b>#
                                      {select.value[0]?.ListingId}
                                    </li>
                                  ) : (
                                    ""
                                  )}

                                  {select.value[0]?.AssociationName ? (
                                    <li>
                                      <b>AssociateName: </b>
                                      {select.value[0]?.AssociationName}
                                    </li>
                                  ) : (
                                    ""
                                  )}

                                  {select.value[0]?.ListOfficeKey ? (
                                    <li>
                                      <b>MLS Office ID: </b>
                                      {select.value[0]?.ListOfficeKey}
                                    </li>
                                  ) : (
                                    ""
                                  )}

                                  {select.value[0]?.MlsStatus ? (
                                    <li>
                                      <b>Status: </b>
                                      {select.value[0]?.MlsStatus}
                                    </li>
                                  ) : (
                                    ""
                                  )}

                                  {select.value[0]?.BedroomsTotal ? (
                                    <li className="me-3 pe-3">
                                      <b className="me-1">Bedrooms:</b>
                                      {select.value[0]?.BedroomsTotal}
                                    </li>
                                  ) : (
                                    ""
                                  )}

                                  {select.value[0]?.BathroomsFull ? (
                                    <li className="me-3 pe-3">
                                      <b className="me-1">
                                        Bathrooms:{" "}
                                        {select.value[0]?.BathroomsFull ?? 0}/{" "}
                                        {select.value[0]?.BathroomsHalf ?? 0}
                                      </b>
                                    </li>
                                  ) : (
                                    ""
                                  )}

                                  {select.value[0]?.GarageSpaces ? (
                                    <li className="me-3 pe-3">
                                      <b className="me-1">
                                        Garage: {select.value[0]?.GarageSpaces}
                                      </b>
                                    </li>
                                  ) : (
                                    ""
                                  )}

                                  {select.value[0]?.BuildingAreaTotal ? (
                                    <li className="me-3 pe-3">
                                      <b className="me-1">
                                        {" "}
                                        Square Ft:{" "}
                                        {select.value[0]?.BuildingAreaTotal}
                                      </b>
                                    </li>
                                  ) : (
                                    ""
                                  )}

                                  {select.value[0]?.LotSizeAcres ? (
                                    <li className="me-3 pe-3">
                                      <b className="me-1">
                                        Lot Size:{" "}
                                        {select.value[0]?.LotSizeAcres}
                                        Acres
                                      </b>
                                    </li>
                                  ) : (
                                    ""
                                  )}
                                </ul>
                              </div>

                              <div className="pb-lg-2">
                                <ul className="d-flex list-unstyled fs-sm">
                                  {select.value[0]?.SourceSystemName ? (
                                    <li className="me-3 pe-3">
                                      Published by:{" "}
                                      <b>{select.value[0]?.SourceSystemName}</b>
                                    </li>
                                  ) : (
                                    ""
                                  )}

                                  {select.value[0]?.OnMarketDate ? (
                                    <li className="me-3 pe-3">
                                      Published date:
                                      <b>
                                        {select.value[0]?.OnMarketDate
                                          ? moment
                                              .tz(
                                                select.value[0]?.OnMarketDate,
                                                "US/Mountain"
                                              )
                                              .format("M/D/YYYY")
                                          : ""}
                                      </b>
                                    </li>
                                  ) : (
                                    ""
                                  )}
                                </ul>
                              </div>
                            </div>
                            <section className="col-md-6">
                              <h2 className="h3">
                                Asking ${select.value[0]?.ListPrice}
                              </h2>
                              <h3 className="mb-0">
                                {" "}
                                {select?.value[0]?.UnparsedAddress ?? ""}
                              </h3>
                              <p className="mb-2 pb-1 fs-lg">
                                {" "}
                                {select?.value[0].City},{" "}
                                {select.value[0].StateOrProvince}{" "}
                                {select.value[0]?.PostalCode}
                              </p>
                            </section>
                          </section>

                          <section className="row border-top">
                            <div className="col-md-4">
                              <h6>CONTACT INFORMATION</h6>
                              <div className="d-flex align-items-start justify-content-between">
                                <a
                                  className="text-decoration-none"
                                  href="real-estate-vendor-properties.html"
                                >
                                  <h5 className="mb-1">
                                    {select.value[0]?.ListAgentFullName}
                                  </h5>

                                  <p className="text-body">
                                    {select.value[0]?.ListOfficeName}
                                  </p>
                                </a>
                              </div>
                              <ul className="list-unstyled mb-4 pb-4 p-0">
                                <li>
                                  <a
                                    className="nav-link fw-normal p-0"
                                    href="tel:3025550107"
                                  >
                                    <i className="fi-phone mt-n1 me-2 align-middle opacity-60"></i>
                                    {select.value[0]?.ListAgentOfficePhone}
                                  </a>
                                </li>
                              </ul>
                            </div>
                            {select.value[0]?.PublicRemarks ? (
                              <div className="col-md-8">
                                <h3 className="h4">Overview</h3>
                                <p className="mb-1">
                                  {select.value[0]?.PublicRemarks}
                                </p>
                              </div>
                            ) : (
                              ""
                            )}
                          </section>
                        </div>
                      </section>
                    </>
                  )}
                </>
              ) : (
                ""
              )} */}

              {/* {templateType === "type5" ? (
                <>
                  {select && select.value && select.value.length > 0 && (
                    <>
                      <section
                        className="mb-0 pb-3 bg-light border rounded-3 p-3"
                        data-simplebar
                      >
                        <div className="row" data-thumbnails="true">
                          <div>
                            <h2 className="pull-right">
                              Asking ${select.value[0]?.ListPrice}
                            </h2>
                          </div>
                          <div className="col-md-12 singleListing">
                            {select?.media[0]?.MediaURL && (
                              <div className="card-img-top">
                                <img
                                  className="img-fluid"
                                  src={select.media[0].MediaURL}
                                  alt="Property"
                                />
                              </div>
                            )}
                          </div>

                          <div className="col-md-12 mt-3">
                            <div className="printTempalte_one">
                              <div className="row">
                                {select.media
                                  .slice(0, 3)
                                  .map((mediaItem, mediaIndex) => (
                                    <div className="col-md-4">
                                      <img
                                        className=""
                                        key={mediaIndex}
                                        src={mediaItem.MediaURL}
                                      />
                                    </div>
                                  ))}
                              </div>
                            </div>
                          </div>

                          <section className="row mt-3">
                            <div className="col-md-8 border-end">
                              <h3 className="mb-0">
                                {" "}
                                {select?.value[0]?.UnparsedAddress ?? ""}
                              </h3>
                              <p className="mb-2 pb-1 fs-lg">
                                {" "}
                                {select?.value[0].City},{" "}
                                {select.value[0].StateOrProvince}{" "}
                                {select.value[0]?.PostalCode}
                              </p>

                              {select.value[0]?.PublicRemarks ? (
                                <div className="col-md-8">
                                  <h3 className="h4">Overview</h3>
                                  <p className="mb-1">
                                    {select.value[0]?.PublicRemarks}
                                  </p>
                                </div>
                              ) : (
                                ""
                              )}
                            </div>

                            <div className="col-md-4">
                              <h3 className="h4">PROPERTY DETAILS</h3>
                              <div className="col-md-12 pb-md-3 d-flex">
                                <ul className="list-unstyled col-md-6">
                                  {select.value[0]?.ListingId ? (
                                    <li>
                                      <b>MLS Number: </b>#
                                      {select.value[0]?.ListingId}
                                    </li>
                                  ) : (
                                    ""
                                  )}

                                  {select.value[0]?.AssociationName ? (
                                    <li>
                                      <b>AssociateName: </b>
                                      {select.value[0]?.AssociationName}
                                    </li>
                                  ) : (
                                    ""
                                  )}

                                  {select.value[0]?.ListOfficeKey ? (
                                    <li>
                                      <b>MLS Office ID: </b>
                                      {select.value[0]?.ListOfficeKey}
                                    </li>
                                  ) : (
                                    ""
                                  )}

                                  {select.value[0]?.MlsStatus ? (
                                    <li>
                                      <b>Status: </b>
                                      {select.value[0]?.MlsStatus}
                                    </li>
                                  ) : (
                                    ""
                                  )}

                                  {select.value[0]?.BedroomsTotal ? (
                                    <li className="me-3 pe-3">
                                      <b className="me-1">Bedrooms:</b>
                                      {select.value[0]?.BedroomsTotal}
                                    </li>
                                  ) : (
                                    ""
                                  )}

                                  {select.value[0]?.BathroomsFull ? (
                                    <li className="me-3 pe-3">
                                      <b className="me-1">
                                        Bathrooms:{" "}
                                        {select.value[0]?.BathroomsFull ?? 0}/{" "}
                                        {select.value[0]?.BathroomsHalf ?? 0}
                                      </b>
                                    </li>
                                  ) : (
                                    ""
                                  )}

                                  {select.value[0]?.GarageSpaces ? (
                                    <li className="me-3 pe-3">
                                      <b className="me-1">
                                        Garage: {select.value[0]?.GarageSpaces}
                                      </b>
                                    </li>
                                  ) : (
                                    ""
                                  )}

                                  {select.value[0]?.BuildingAreaTotal ? (
                                    <li className="me-3 pe-3">
                                      <b className="me-1">
                                        {" "}
                                        Square Ft:{" "}
                                        {select.value[0]?.BuildingAreaTotal}
                                      </b>
                                    </li>
                                  ) : (
                                    ""
                                  )}

                                  {select.value[0]?.LotSizeAcres ? (
                                    <li className="me-3 pe-3">
                                      <b className="me-1">
                                        Lot Size:{" "}
                                        {select.value[0]?.LotSizeAcres}
                                        Acres
                                      </b>
                                    </li>
                                  ) : (
                                    ""
                                  )}
                                </ul>
                              </div>

                              <div className="pb-lg-2">
                                <ul className="d-flex list-unstyled fs-sm">
                                  {select.value[0]?.SourceSystemName ? (
                                    <li className="me-3 pe-3">
                                      Published by:{" "}
                                      <b>{select.value[0]?.SourceSystemName}</b>
                                    </li>
                                  ) : (
                                    ""
                                  )}

                                  {select.value[0]?.OnMarketDate ? (
                                    <li className="me-3 pe-3">
                                      Published date:
                                      <b>
                                        {select.value[0]?.OnMarketDate
                                          ? moment
                                              .tz(
                                                select.value[0]?.OnMarketDate,
                                                "US/Mountain"
                                              )
                                              .format("M/D/YYYY")
                                          : ""}
                                      </b>
                                    </li>
                                  ) : (
                                    ""
                                  )}
                                </ul>
                              </div>
                            </div>
                          </section>

                          <section className="row border-top mt-3">
                            <div className="col-md-4">
                              <h6>CONTACT INFORMATION</h6>
                              <div className="d-flex align-items-start justify-content-between">
                                <a
                                  className="text-decoration-none"
                                  href="real-estate-vendor-properties.html"
                                >
                                  <h5 className="mb-1">
                                    {select.value[0]?.ListAgentFullName}
                                  </h5>

                                  <p className="text-body">
                                    {select.value[0]?.ListOfficeName}
                                  </p>
                                </a>
                              </div>
                              <ul className="list-unstyled mb-4 pb-4 p-0">
                                <li>
                                  <a
                                    className="nav-link fw-normal p-0"
                                    href="tel:3025550107"
                                  >
                                    <i className="fi-phone mt-n1 me-2 align-middle opacity-60"></i>
                                    {select.value[0]?.ListAgentOfficePhone}
                                  </a>
                                </li>
                              </ul>
                            </div>
                          </section>
                        </div>
                      </section>
                    </>
                  )}
                </>
              ) : (
                ""
              )} */}

              {/* {templateType === "type6" ? (
                <>
                  {select && select.value && select.value.length > 0 && (
                    <>
                      <section
                        className="mb-0 pb-3 bg-light border rounded-3 p-3"
                        data-simplebar
                      >
                        <div className="row" data-thumbnails="true">
                          <div className="col-md-12 singleListing">
                            {select?.media[0]?.MediaURL && (
                              <div className="card-img-top">
                                <img
                                  className="img-fluid"
                                  src={select.media[0].MediaURL}
                                  alt="Property"
                                />
                              </div>
                            )}
                          </div>

                          <div className="text-center">
                            <h2>Asking ${select.value[0]?.ListPrice}</h2>

                            <h3 className="mb-0">
                              {" "}
                              {select?.value[0]?.UnparsedAddress ?? ""}
                            </h3>
                          </div>

                          <section className="row mt-3">
                            {select.value[0]?.PublicRemarks ? (
                              <div className="col-md-4">
                                <h3 className="h4">Overview</h3>
                                <p className="mb-1">
                                  {select.value[0]?.PublicRemarks}
                                </p>
                              </div>
                            ) : (
                              ""
                            )}
                            <div className="col-md-8 mt-3">
                              <div className="printTempalte_one">
                                <div className="row">
                                  {select.media
                                    .slice(0, 4)
                                    .map((mediaItem, mediaIndex) => (
                                      <div className="col-md-6 mt-2">
                                        <img
                                          className=""
                                          key={mediaIndex}
                                          src={mediaItem.MediaURL}
                                        />
                                      </div>
                                    ))}
                                </div>
                              </div>
                            </div>
                          </section>

                          <section className="row border-top mt-3">
                            <div className="col-md-4">
                              <h3 className="h4">PROPERTY DETAILS</h3>
                              <div className="col-md-12 pb-md-3 d-flex">
                                <ul className="list-unstyled col-md-6">
                                  {select.value[0]?.ListingId ? (
                                    <li>
                                      <b>MLS Number: </b>#
                                      {select.value[0]?.ListingId}
                                    </li>
                                  ) : (
                                    ""
                                  )}

                                  {select.value[0]?.AssociationName ? (
                                    <li>
                                      <b>AssociateName: </b>
                                      {select.value[0]?.AssociationName}
                                    </li>
                                  ) : (
                                    ""
                                  )}

                                  {select.value[0]?.ListOfficeKey ? (
                                    <li>
                                      <b>MLS Office ID: </b>
                                      {select.value[0]?.ListOfficeKey}
                                    </li>
                                  ) : (
                                    ""
                                  )}

                                  {select.value[0]?.MlsStatus ? (
                                    <li>
                                      <b>Status: </b>
                                      {select.value[0]?.MlsStatus}
                                    </li>
                                  ) : (
                                    ""
                                  )}

                                  {select.value[0]?.BedroomsTotal ? (
                                    <li className="me-3 pe-3">
                                      <b className="me-1">Bedrooms:</b>
                                      {select.value[0]?.BedroomsTotal}
                                    </li>
                                  ) : (
                                    ""
                                  )}

                                  {select.value[0]?.BathroomsFull ? (
                                    <li className="me-3 pe-3">
                                      <b className="me-1">
                                        Bathrooms:{" "}
                                        {select.value[0]?.BathroomsFull ?? 0}/{" "}
                                        {select.value[0]?.BathroomsHalf ?? 0}
                                      </b>
                                    </li>
                                  ) : (
                                    ""
                                  )}

                                  {select.value[0]?.GarageSpaces ? (
                                    <li className="me-3 pe-3">
                                      <b className="me-1">
                                        Garage: {select.value[0]?.GarageSpaces}
                                      </b>
                                    </li>
                                  ) : (
                                    ""
                                  )}

                                  {select.value[0]?.BuildingAreaTotal ? (
                                    <li className="me-3 pe-3">
                                      <b className="me-1">
                                        {" "}
                                        Square Ft:{" "}
                                        {select.value[0]?.BuildingAreaTotal}
                                      </b>
                                    </li>
                                  ) : (
                                    ""
                                  )}

                                  {select.value[0]?.LotSizeAcres ? (
                                    <li className="me-3 pe-3">
                                      <b className="me-1">
                                        Lot Size:{" "}
                                        {select.value[0]?.LotSizeAcres}
                                        Acres
                                      </b>
                                    </li>
                                  ) : (
                                    ""
                                  )}
                                </ul>
                              </div>
                            </div>

                            <div className="col-md-4">
                              <h6>CONTACT INFORMATION</h6>
                              <div className="d-flex align-items-start justify-content-between">
                                <a
                                  className="text-decoration-none"
                                  href="real-estate-vendor-properties.html"
                                >
                                  <h5 className="mb-1">
                                    {select.value[0]?.ListAgentFullName}
                                  </h5>

                                  <p className="text-body">
                                    {select.value[0]?.ListOfficeName}
                                  </p>
                                </a>
                              </div>
                              <ul className="list-unstyled mb-4 pb-4 p-0">
                                <li>
                                  <a
                                    className="nav-link fw-normal p-0"
                                    href="tel:3025550107"
                                  >
                                    <i className="fi-phone mt-n1 me-2 align-middle opacity-60"></i>
                                    {select.value[0]?.ListAgentOfficePhone}
                                  </a>
                                </li>
                              </ul>
                            </div>
                          </section>
                        </div>
                      </section>
                    </>
                  )}
                </>
              ) : (
                ""
              )} */}
            </div>
          ) : (
            <>
              <div className="col-md-8">
                <h3 className="text-white mb-4">Design your Flyer.</h3>
                <div className="bg-light float-start w-100 rounded-3 mb-2 p-3">
                  <p
                    id=""
                    dangerouslySetInnerHTML={{
                      __html: platformData?.flyerpagecontent,
                    }}
                  />
                  <hr className="mb-3" />
                  <div className="mb-5">
                    <div
                      className="select_template float-start w-100 mt-3 mb-5"
                      // onClick={(e) => handleType(e)}
                    >
                      <label className="col-form-label" id="radio-level1">
                        Select Template:
                      </label>
                      <div className="d-flex">
                        <div className="form-check mb-0">
                          {/* Image 1 */}
                          <img
                            className={templateType === "type1" ? "active" : ""}
                            src={TypeOne}
                            alt="Type One"
                            style={{ cursor: "pointer" }}
                            onClick={() => handleType("type1")} // handle the image click
                          />
                        </div>
                        <div className="form-check mb-0 ms-3">
                          <img
                            className={templateType === "type2" ? "active" : ""}
                            src={TypeTwo}
                            alt="Type Two"
                            style={{ cursor: "pointer" }}
                            onClick={() => handleType("type2")}
                          />
                        </div>
                        <div className="form-check mb-0 ms-3">
                          {/* Image 3 */}
                          <img
                            className={templateType === "type3" ? "active" : ""}
                            src={TypeThree}
                            alt="Type Three"
                            style={{ cursor: "pointer" }}
                            onClick={() => handleType("type3")}
                          />
                        </div>
                      </div>
                      {/* <div className="form-check mb-0 ms-2">
                      <input
                        className="form-check-input"
                        id="form-check-1"
                        type="radio"
                        name="property"
                        value="type4"
                      />

                      <label className="form-label" id="radio-level1">
                        Type 4
                      </label>
                    </div>

                    <div className="form-check mb-0 ms-2">
                      <input
                        className="form-check-input"
                        id="form-check-1"
                        type="radio"
                        name="property"
                        value="type5"
                      />

                      <label className="form-label" id="radio-level1">
                        Type 5
                      </label>
                    </div>

                    <div className="form-check mb-0 ms-2">
                      <input
                        className="form-check-input"
                        id="form-check-1"
                        type="radio"
                        name="property"
                        value="type6"
                      />

                      <label className="form-label" id="radio-level1">
                        Type 6
                      </label>
                    </div> */}
                    </div>

                    {templateType ? (
                      <div className="float-start w-100 mt-3">
                        <label className="col-form-label ">
                          Select Membership{" "}
                        </label>
                        <select
                          className="form-select mb-2"
                          name="membership"
                          onChange={(event) =>
                            setMembership(event.target.value)
                          }
                          value={membership}
                        >
                          <option value="">Please Select Membership</option>
                          {commaSeparatedValuesmls_membership &&
                          commaSeparatedValuesmls_membership.length > 0
                            ? commaSeparatedValuesmls_membership.map(
                                (memberahip) => <option>{memberahip}</option>
                              )
                            : ""}
                        </select>
                        {membership ? (
                          <>
                            <label className="col-form-label">
                              Enter MLS Number:
                            </label>
                            {/* <h6 className="mt-2 mb-2"></h6> */}
                            <div className="col-md-12">
                              <input
                                className="form-control w-100"
                                id="text-input-onee"
                                type="text"
                                name="mlsnumber"
                                placeholder="Enter MLS number"
                                onChange={handleChangesearchmls}
                                value={searchmlsnumber.mlsnumber}
                              />
                            </div>

                            <div className="pull-left mt-4">
                              <button
                                className="btn btn-primary btn-sm order-lg-3"
                                onClick={MLSSearch}
                                disabled={isLoading}
                              >
                                {isLoading ? "Please wait" : "Search Print"}
                              </button>
                            </div>
                          </>
                        ) : (
                          ""
                        )}
                      </div>
                    ) : (
                      ""
                    )}
                  </div>
                  <div className="col-md-4">
                    <button
                      className="btn btn-primary mt-4"
                      onClick={handleNonListed}
                    >
                      Non-Listed Property
                    </button>
                  </div>
                </div>
              </div>
            </>
          )}
        </div>
      </main>
    </div>
  );
};

export default PrintEditor;

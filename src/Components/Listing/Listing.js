import React, { useState, useEffect } from "react";
import user_service from "../service/user_service";
import Loader from "../../Pages/Loader/Loader.js";
import Toaster from "../../Pages/Toaster/Toaster";
import { NavLink, useNavigate } from "react-router-dom";
import ReactPaginate from "react-paginate";
import defaultpropertyimage from "../../Components/img/defaultpropertyimage.jpeg";
import jwt from "jwt-decode";

const Listing = () => {
  const [loader, setLoader] = useState({ isActive: null });
  const { isActive } = loader;
  const [toaster, setToaster] = useState({
    types: null,
    isShow: null,
    toasterBody: "",
    message: "",
  });
  const { types, isShow, toasterBody, message } = toaster;
  const [getListing, setGetListing] = useState([]);
  const [pageCount, setpageCount] = useState(0);
  const initialValues = {
    listingsAgent: "",
    category: "",
    mlsId: "",
    propertyType: "",
    mlsNumber: "",
    agentId: "",
    associateName: "",
    price: "",
    garage: "",
    bed: "",
    subDivision: "",
    streetAddress: "",
    city: "",
    state: "",
    zipCode: "",
    schoolDistrtict: "",
    country: "",
    areaLocation: "",
    status: "",
    listDate: "",
    image: "",
  };
  const [formValues, setFormValues] = useState(initialValues);
  const [formErrors, setFormErrors] = useState({});
  const navigate = useNavigate();
  const SingleListing = (id) => {
    navigate(`/single-listing/${id}`);
  };
  const propertypic = (e) => {
    e.target.src = defaultpropertyimage;
  };

  useEffect(() => {
    window.scrollTo(0, 0);
    setLoader({ isActive: true });
    ListingGetAll();
  }, []);

  const ListingGetAll = async () => {
    let queryParams = `?page=1`;
  
    const auth = localStorage.getItem("auth");
    if (auth) {
      const decodedToken = jwt(auth);
      const contactType = decodedToken.contactType;
      const agentId = decodedToken.id;
  
      if (contactType === "admin") {
        // Admin can see all listings, so no agentId is added
      } else if (contactType === "staff") {
        // Staff can also see all listings, so no agentId is added
      } else {
        // Agent can see only their own listings
        queryParams += `&agentId=${agentId}`;
      }
    }
  
    setLoader({ isActive: true });
    
    await user_service.listingsGet(queryParams).then((response) => {
      setLoader({ isActive: false });
      if (response) {
        setGetListing(response.data.data);
        setpageCount(Math.ceil(response.data.totalRecords / 10));
      }
    });
  };

  const handlePageClick = async (data) => {
    let currentPage = data.selected + 1;
    let skip = (currentPage - 1) * currentPage;
    let queryParams = `?page=${currentPage}`;
    const auth = localStorage.getItem("auth");
    if (auth) {
      const decodedToken = jwt(auth);
      const contactType = decodedToken.contactType;
      const agentId = decodedToken.id;
  
      if (contactType === "admin") {
      } else if (contactType === "staff") {
      } else {
        queryParams += `&agentId=${agentId}`;
      }
    }

    await user_service.listingsGet(queryParams).then((response) => {
      if (response) {
        setGetListing(response.data.data);
        setpageCount(Math.ceil(response.data.totalRecords / 10));
      }
    });
  };

  // onChange Function start
  const handleChange = (e) => {
    const { name, value } = e.target;
    setFormValues({ ...formValues, [name]: value });
  };
  // onChange Function end
  return (
    <div className="bg-secondary float-left w-100 pt-4">
      <Loader isActive={isActive} />
      {isShow && (
        <Toaster
          types={types}
          isShow={isShow}
          toasterBody={toasterBody}
          message={message}
        />
      )}
      <main className="page-wrapper">
        <div className="container mb-md-4 py-5 pt-0">
          <div className="">
            <div className="row">
              {/* <!-- Content--> */}
              <div className="col-lg-8 col-md-7 mb-5">
                <div className="d-flex align-items-center justify-content-between">
                  <h3 className="text-white mb-0">Property Listings</h3>
                  <br />
                </div>
                <br />

                {getListing.map((post) => (
                  <div
                    className="card card-hover card-horizontal border-0 shadow-sm mb-4 p-3"
                    onClick={(e) => SingleListing(post._id)}
                  >
                    <a className="card-img-top">
                      <img
                        className="img-fluid w-100"
                        src={
                          post.image && post.image !== "image"
                            ? post.image
                            : defaultpropertyimage
                        }
                        alt="Property"
                        onError={(e) => propertypic(e)}
                      />
                    </a>
                    <div className="card-body position-relative pb-3 pt-0">
                      {post.mlsNumber ? (
                        <p className="mb-2 fs-sm">
                          <strong>MLS Number:</strong> #{post.mlsNumber}
                        </p>
                      ) : (
                        ""
                      )}

                      {post.streetAddress ? (
                        <p className="mb-2 fs-sm">
                          <strong>Street-Address:</strong> {post.streetAddress}
                        </p>
                      ) : (
                        ""
                      )}

                      {post.propertyType ? (
                        <p className="mb-2 fs-sm">
                          <strong>Type:</strong> {post.propertyType}
                        </p>
                      ) : (
                        ""
                      )}

                      {post.areaLocation ? (
                        <p className="mb-2 fs-sm">
                          <strong>State:</strong> {post.areaLocation}
                        </p>
                      ) : (
                        ""
                      )}

                      {post.price ? (
                        <p className="mb-2 fs-sm">
                          <strong>List Price:</strong>
                          {/* ${parseFloat(post.price).toLocaleString("en-US")} */}
                          ${post.price.toLocaleString("en-US")}
                        </p>
                      ) : (
                        ""
                      )}

                      {post.listingsAgent ? (
                        <p className="mb-2 fs-sm">
                          <strong>ListingsAgent:</strong> {post.listingsAgent}
                        </p>
                      ) : (
                        ""
                      )}
                      {post.status ? (
                        <p className="mb-2 fs-sm badge bg-success">
                          <strong>Status:</strong> {post.status}
                        </p>
                      ) : (
                        ""
                      )}
                    </div>
                  </div>
                ))}

                <div className="justify-content-end mb-1">
                  <ReactPaginate
                    previousLabel={"Previous"}
                    nextLabel={"next"}
                    breakLabel={"..."}
                    pageCount={pageCount}
                    marginPagesDisplayed={1}
                    pageRangeDisplayed={2}
                    onPageChange={handlePageClick}
                    containerClassName={"pagination justify-content-center"}
                    pageClassName={"page-item"}
                    pageLinkClassName={"page-link"}
                    previousClassName={"page-item"}
                    previousLinkClassName={"page-link"}
                    nextClassName={"page-item"}
                    nextLinkClassName={"page-link"}
                    breakClassName={"page-item"}
                    breakLinkClassName={"page-link"}
                    activeClassName={"active"}
                  />
                </div>
              </div>
              {/* <!-- Sidebar--> */}
              <aside className="col-lg-4 col-md-5 pe-xl-4 mb-5">
                <div className="card card-body border-0 shadow-sm pb-1 me-lg-1">
                  <p className="mb-2">IDX Provider</p>
                  <p className="mb-2">UtahRealEstate (Utah)</p>
                  <label className="form-label">Reset</label>
                  <NavLink
                    to="/add-listing"
                    className="btn btn-primary btn-lg mb-2"
                  >
                    Add Property
                  </NavLink>
                  <div className="collapse d-md-block mt-3" id="account-nav">
                    <div className="pb-4 mb-2">
                      <div
                        className="overflow-auto"
                        data-simplebar
                        data-simplebar-auto-hide="false"
                      >
                        <div className="float-left w-100 mb-3 col-12">
                          <label className="form-label">MLS Office ID</label>
                          <select
                            className="form-select form-select-dark"
                            id="select-input-dark"
                            name="mlsId"
                            value={formValues.mlsId}
                            onChange={handleChange}
                          >
                            <option>(Any Office)</option>
                            <option>68134</option>
                            <option>70344</option>
                            <option>87677</option>
                          </select>
                        </div>

                        <div className="float-left w-100 mb-3 col-12">
                          <label className="form-label">Property Type</label>
                          <select
                            className="form-select form-select-dark"
                            id="select-input-dark"
                            name="matchCommType"
                            value={formValues.propertyType}
                            onChange={handleChange}
                          >
                            <option>(Any Type)</option>
                            <option>Land</option>
                            <option>Residential</option>
                            <option>Commercial Lease</option>
                            <option>Commercial Sale</option>
                            <option>Commercial/Industrial</option>
                            <option>Residential/Lease</option>
                            <option>Farm</option>
                          </select>
                        </div>

                        <div className="float-left w-100 mb-3 col-12">
                          <label className="form-label">MLS Number</label>
                          <input
                            className="form-control"
                            id="inline-form-input"
                            type="number"
                            placeholder="MLS Number"
                            name="mlsNumber"
                            value={formValues.mlsNumber}
                            onChange={handleChange}
                            style={{
                              border: formErrors?.mlsNumber
                                ? "1px solid red"
                                : "",
                            }}
                          />
                          {/* <div className="invalid-tooltip">{formErrors.mlsNumber}</div> */}
                        </div>

                        <div className="float-left w-100 mb-3 col-12">
                          <label className="form-label">Associate Name</label>
                          <input
                            className="form-control"
                            id="inline-form-input"
                            type="number"
                            placeholder="Associate name"
                            name="associateName"
                            value={formValues.associateName}
                            onChange={handleChange}
                            style={{
                              border: formErrors?.associateName
                                ? "1px solid red"
                                : "",
                            }}
                          />
                          {/* <div className="invalid-tooltip">{formErrors.mlsNumber}</div> */}
                        </div>

                        <label className="form-label">Price Range</label>
                        <br />
                        <div className=" d-flex float-left w-100 mb-3 col-12">
                          <select
                            className="form-select"
                            id="inline-form-select"
                            name="price"
                            value={formValues.price}
                            onChange={handleChange}
                          >
                            <option></option>
                            <option>$500</option>
                            <option>$1000</option>
                            <option>$1500</option>
                            <option>$2000</option>
                            <option>$2500</option>
                            <option>$3000</option>
                            <option>$3500</option>
                            <option>$4000</option>
                            <option>$4500</option>
                            <option>$5000</option>
                          </select>
                          <span className="d-flex align-items-center justify-content-center px-2">
                            to
                          </span>

                          <select
                            className="form-select"
                            id="inline-form-select"
                            name="price"
                            value={formValues.price}
                            onChange={handleChange}
                          >
                            <option></option>
                            <option>$500</option>
                            <option>$1000</option>
                            <option>$1500</option>
                            <option>$2000</option>
                            <option>$2500</option>
                            <option>$3000</option>
                            <option>$3500</option>
                            <option>$4000</option>
                            <option>$4500</option>
                            <option>$5000</option>
                          </select>
                        </div>

                        <label className="form-label">Garage</label>
                        <div className="d-flex float-left w-100 mb-3 col-12">
                          <select
                            className="form-select"
                            id="inline-form-select"
                            name="garage"
                            value={formValues.garage}
                            onChange={handleChange}
                          >
                            <option></option>
                            <option>+1</option>
                            <option>+2</option>
                            <option>+3</option>
                            <option>+4</option>
                            <option>+5</option>
                            <option>+6</option>
                          </select>
                          <span className="d-flex align-items-center justify-content-center px-2">
                            Bed
                          </span>

                          <select
                            className="form-select"
                            id="inline-form-select"
                            name="bed"
                            value={formValues.bed}
                            onChange={handleChange}
                          >
                            <option></option>
                            <option>+1</option>
                            <option>+2</option>
                            <option>+3</option>
                            <option>+4</option>
                            <option>+5</option>
                            <option>+6</option>
                          </select>
                        </div>

                        <div className="float-left w-100 mb-3 col-12">
                          <label className="form-label">Subdivision</label>
                          <input
                            className="form-control"
                            id="inline-form-input"
                            type="number"
                            placeholder="Subdivision"
                            name="subDivision"
                            value={formValues.subDivision}
                            onChange={handleChange}
                            style={{
                              border: formErrors?.subDivision
                                ? "1px solid red"
                                : "",
                            }}
                          />
                          {/* <div className="invalid-tooltip">{formErrors.subDivision}</div> */}
                        </div>

                        <div className="float-left w-100 mb-3 col-12">
                          <label className="form-label">Street Address</label>
                          <input
                            className="form-control"
                            id="inline-form-input"
                            type="number"
                            placeholder="Street-Address"
                            name="streetAddress"
                            value={formValues.streetAddress}
                            onChange={handleChange}
                            style={{
                              border: formErrors?.streetAddress
                                ? "1px solid red"
                                : "",
                            }}
                          />
                          {/* <div className="invalid-tooltip">{formErrors.streetAddress}</div> */}
                        </div>

                        <div className="float-left w-100 mb-3">
                          <div className="row">
                            <div className="col-md-6">
                              <label className="form-label">City</label>
                              <input
                                className="form-control"
                                id="inline-form-input"
                                type="number"
                                placeholder="city"
                                name="city"
                                value={formValues.city}
                                onChange={handleChange}
                                style={{
                                  border: formErrors?.city
                                    ? "1px solid red"
                                    : "",
                                }}
                              />
                              {/* <div className="invalid-tooltip">{formErrors.city}</div> */}
                            </div>

                            <div className="col-md-6">
                              <label className="form-label">State</label>
                              <input
                                className="form-control"
                                id="inline-form-input"
                                type="number"
                                placeholder="State"
                                name="state"
                                value={formValues.state}
                                onChange={handleChange}
                                style={{
                                  border: formErrors?.state
                                    ? "1px solid red"
                                    : "",
                                }}
                              />
                              {/* <div className="invalid-tooltip">{formErrors.state}</div> */}
                            </div>
                          </div>
                        </div>

                        <div className="float-left w-100 mb-3 col-12">
                          <label className="form-label">Zip Code</label>
                          <input
                            className="form-control"
                            id="inline-form-input"
                            type="number"
                            placeholder="zip-code"
                            name="zipCode"
                            value={formValues.zipCode}
                            onChange={handleChange}
                            style={{
                              border: formErrors?.zipCode
                                ? "1px solid red"
                                : "",
                            }}
                          />
                          {/* <div className="invalid-tooltip">{formErrors.mlsNumber}</div> */}
                        </div>

                        <div className="float-left w-100 mb-3 col-12">
                          <label className="form-label">School District</label>
                          <input
                            className="form-control"
                            id="inline-form-input"
                            type="number"
                            placeholder="School"
                            name="schoolDistrtict"
                            value={formValues.schoolDistrtict}
                            onChange={handleChange}
                            style={{
                              border: formErrors?.schoolDistrtict
                                ? "1px solid red"
                                : "",
                            }}
                          />
                          {/* <div className="invalid-tooltip">{formErrors.mlsNumber}</div> */}
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </aside>
            </div>
          </div>
        </div>
      </main>
    </div>
  );
};

export default Listing;

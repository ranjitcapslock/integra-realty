  import React, { useState, useEffect } from "react";
  import { useParams } from "react-router-dom";
  import user_service from "../service/user_service";
  import Loader from "../../Pages/Loader/Loader.js";
  import Toaster from "../../Pages/Toaster/Toaster";
  // import Image1 from "../img/bedroom.jpg";
  //  import Helmet from "react-helmet"
  import Picture from "../img/pic.png";

  import Slider from "react-slick";
  import "slick-carousel/slick/slick-theme.css";
  import "slick-carousel/slick/slick.css";
  import defaultpropertyimage from "../../Components/img/defaultpropertyimage.jpeg";
  import moment from "moment-timezone";

function LandingPage() {
    const [loader, setLoader] = useState({ isActive: null });
    const { isActive } = loader;
    const [toaster, setToaster] = useState({
      types: null,
      isShow: null,
      toasterBody: "",
      message: "",
    });

    const settings = {
      // dots: true,
      // infinite: true,
      // speed: 500,
      // slidesToShow: 1,
      // slidesToScroll: 1,
      // autoplay: true,
      // autoplaySpeed: 3000,
      // arrows: false,
      slidesToShow: 1,
      slidesToScroll: 1,
      dots: true,
      infinite: true,
      fade: true,
      speed: 500, // ms
      autoplay: true,
      lazyLoad: true,
      autoplaySpeed: 3000,
      arrows: false,
    };

    const settingsNew = {
      dots: true,

      speed: 500,
      slidesToShow: 4,
      slidesToScroll: 1,
      autoplay: true,
      autoplaySpeed: 3000,
      arrows: false,

      responsive: [
        {
          breakpoint: 1024,
          settings: {
            slidesToShow: 3,
            slidesToScroll: 3,
            infinite: true,
            dots: true,
          },
        },
        {
          breakpoint: 600,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 2,
            initialSlide: 2,
          },
        },
        {
          breakpoint: 480,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1,
          },
        },
      ],
    };

    const { types, isShow, toasterBody, message } = toaster;
    const [summary, setSummary] = useState([]);
    const [profile, setProfile] = useState("");
    const params = useParams();

    // const {
    //   title = summary?.streetAddress || "" ,
    //   metaDescription = "default description",
    //   imageUrl = summary?.image || "" ,
    // } = props;
  

    const SingleListingById = async () => {
      await user_service.listingsGetById(params.id).then((response) => {
        setLoader({ isActive: false });
        if (response) {
          setSummary(response.data);

          const data = response.data.agentId;
          profileGetAll(data);
        }
      });
    };
    useEffect(() => {
      window.scrollTo(0, 0);
      // setLoader({ isActive: true })
      SingleListingById(params.id);
      // profileGetAll();
    }, []);

    const profileGetAll = async (agentId) => {
      setLoader({ isActive: true });
      await user_service.contactGetById(agentId).then((response) => {
        if (response) {
          setLoader({ isActive: false });
          setProfile(response.data);
        }
      });
    };

    const propertypic = (e) => {
      e.target.src = defaultpropertyimage;
    };

    const additionalDataMLS = summary.additionaldataMLS
      ? JSON.parse(summary.additionaldataMLS)
      : null;

      const profilepic = (e) => {
        e.target.src = Picture;
      };
      
    return (
      <div className="bg-secondary float-left w-100 mb-4">
        <Loader isActive={isActive} />
        {isShow && (
          <Toaster
            types={types}
            isShow={isShow}
            toasterBody={toasterBody}
            message={message}
          />
        )}
        {/* <Helmet>
          <title>{title}</title>
          <meta property="og:description" content={metaDescription} />
          <meta property="og:image" content={imageUrl} />
        </Helmet> */}
      {/* <Helmet>
        <title>{summary?.streetAddress || ""} </title>
        <meta property="og:url" content={`https://indepthrealty.brokeragentbase.com/landing-page/${params.id}`} />
        <meta property="og:title" content={summary?.streetAddress || ""} />
        <meta property="og:description" content="Description of your page goes here" />
        <meta property="og:image" content={summary?.image || ""} />
      </Helmet> */}
      <main className="page-wrapper">
        <section className="">
          <div className="">
            <div className="">
              <div className="">
                {summary.propertyFrom === "mls" ? (
                  <>
                    <section className="mb-0 pb-3">
                      <div className="">
                        <div className="col-md-12">
                          <div className="property_banner">
                            <div>
                              <Slider {...settings}>
                                {summary.additionaldataMLS &&
                                  JSON.parse(summary.additionaldataMLS).media &&
                                  JSON.parse(summary.additionaldataMLS)
                                    .media.slice(0, 5) // Limit to 5 images
                                    .map((item, index) => (
                                      <div
                                        className="col-md-12 landingImage"
                                        key={index}
                                        // style={{
                                        //   backgroundImage: `url(${item.MediaURL})`, // Corrected here
                                        //   backgroundSize: 'cover', // Optionally adjust background size
                                        //   backgroundRepeat: 'no-repeat', // Optionally adjust background repeat
                                        // }}
                                      >
                                        <img
                                          className=""
                                          src={item.MediaURL}
                                          alt={`Gallery thumbnail ${index}`}
                                        />
                                      </div>
                                    ))}
                              </Slider>

                              <div className="single-property-details">
                                <h2 className="mb-2">
                                  ${" "}
                                  {parseFloat(summary.price).toLocaleString(
                                    "en-US"
                                  )}
                                </h2>

                                <h3 className="mb-0">
                                  {summary.streetAddress}
                                </h3>
                                {/* <p className="mb-2 pb-1 fs-lg">
                                        {summary.state}
                                      </p>
                                 */}
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </section>
                    <div className="container">
                      <section className="mt-4">
                        <div className="property_overview bg-light border rounded-3 p-3">
                          <div className="mb-md-0 mt-3">
                            <div className="pb-3 d-flex align-align-center justify-content-start">
                              <div className="col-md-2">
                                <h3 className="h4 mb-1">
                                  {
                                    JSON.parse(summary.additionaldataMLS)
                                      .value[0].BedroomsTotal
                                  }
                                </h3>

                                <b className="me-1">Bed</b>
                              </div>

                              <div className="col-md-2">
                                <h3 className="h4 mb-1">
                                  {JSON.parse(summary.additionaldataMLS)
                                    .value[0].BathroomsFull ?? 0}{" "}
                                  /{" "}
                                  {JSON.parse(summary.additionaldataMLS)
                                    .value[0].BathroomsHalf ?? 0}
                                </h3>
                                <b className="me-1">Baths: </b>
                              </div>

                              <div className="col-md-2">
                                <h3 className="h4 mb-1">
                                  {
                                    JSON.parse(summary.additionaldataMLS)
                                      .value[0].BuildingAreaTotal
                                  }
                                </h3>

                                <b className="me-1"> SqFt </b>
                              </div>

                              <div className="col-md-2">
                                <h3 className="h4 mb-1">
                                  {
                                    JSON.parse(summary.additionaldataMLS)
                                      .value[0].LotSizeAcres
                                  }{" "}
                                </h3>
                                <b className="me-1">Lot Size </b>
                              </div>

                              <div className="col-md-2">
                                <h3 className="h4 mb-1">
                                  {
                                    JSON.parse(summary.additionaldataMLS)
                                      .value[0].YearBuilt
                                  }
                                </h3>
                                <b className="me-1">Year Built </b>
                              </div>
                            </div>
                            <div className="">
                              <h3 className="h4">Overview</h3>
                              <p className="mb-1">
                                {
                                  JSON.parse(summary.additionaldataMLS).value[0]
                                    .PublicRemarks
                                }
                              </p>
                              <h6 className="mt-2">
                                Listed by In Depth Realty
                              </h6>
                            </div>
                          </div>
                        </div>
                      </section>
                    </div>

                    <div className="container">
                      <section className="property_gallary bg-light border rounded-3 p-4 mt-4">
                        <h3 className="h4">Gallary</h3>
                        <Slider {...settingsNew}>
                          {summary.additionaldataMLS &&
                            JSON.parse(summary.additionaldataMLS).media &&
                            JSON.parse(summary.additionaldataMLS)
                              .media
                              .map((item, index) => (
                                <div className="" key={index}>
                                  <img
                                    className="img-fluid"
                                    src={item.MediaURL}
                                    alt={`Gallery thumbnail ${index}`}
                                  />
                                </div>
                              ))}
                        </Slider>
                      </section>

                      <section className="bg-light border rounded-3 p-4 mt-3">
                        <div className="">
                          <div className="">
                            <h3 className="h4">Property Details</h3>
                            <div className="col-md-12 pb-md-3 d-flex">
                              <ul className="list-unstyled col-md-6">
                                <li>
                                  <b>Category: </b>
                                  {summary.propertyType}
                                </li>
                                <li>
                                  <b>ListingsAgent: </b>
                                  {summary.listingsAgent}
                                </li>
                                <li>
                                  <b>Listing Agent ID:: </b>
                                  {additionalDataMLS?.value[0]?.ListAgentKey}
                                </li>
                                <li>
                                  <b>Area Location: </b>
                                  {summary.areaLocation}
                                </li>
                                <li>
                                  <b>County: </b>
                                  {summary.country}
                                </li>
                                <li>
                                  <b>Subdivision: </b>
                                  {summary.subDivision}
                                </li>
                                <li>
                                  <b>List Date: </b>
                                  {summary?.listDate
                                    ? moment
                                        .tz(summary?.listDate, "US/Mountain")
                                        .format("M/D/YYYY")
                                    : ""}
                                </li>
                              </ul>
                              <ul className="list-unstyled col-md-6">
                                <li>
                                  <b>MLS Number: </b>#{summary.mlsNumber}
                                </li>
                                <li>
                                  <b>AssociateName: </b>
                                  {summary.associateName}
                                </li>
                                <li>
                                  <b>MLS Office ID: </b>
                                  {additionalDataMLS?.value[0]?.ListOfficeKey}
                                </li>
                                <li>
                                  <b>Status: </b>
                                  {summary.status}
                                </li>
                                <li className="me-3 pe-3 border-end">
                                  <b className="me-1">Bedrooms:</b>
                                  {additionalDataMLS?.value[0]?.BedroomsTotal}
                                </li>
                                <li className="me-3 pe-3 border-end">
                                  <b className="me-1">
                                    Bathrooms:{" "}
                                    {additionalDataMLS?.value[0]
                                      ?.BathroomsFull ?? 0}{" "}
                                    /{" "}
                                    {additionalDataMLS?.value[0]
                                      ?.BathroomsHalf ?? 0}
                                  </b>
                                </li>
                                <li className="me-3 pe-3 border-end">
                                  <b className="me-1">
                                    Garage: {summary.garage}
                                  </b>
                                </li>
                                <li className="me-3 pe-3 border-end">
                                  <b className="me-1">
                                    {" "}
                                    Square Ft:{" "}
                                    {
                                      additionalDataMLS?.value[0]
                                        ?.BuildingAreaTotal
                                    }
                                  </b>
                                </li>
                                <li className="me-3 pe-3 border-end">
                                  <b className="me-1">
                                    Lot Size:{" "}
                                    {additionalDataMLS?.value[0]?.LotSizeAcres}{" "}
                                    Acres
                                  </b>
                                </li>
                              </ul>
                            </div>

                            <div className="pb-lg-2 py-4 border-top pb-0">
                              <ul className="d-flex list-unstyled fs-sm mb-0">
                                <li className="me-3 pe-3 border-end">
                                  Published by:{" "}
                                  <b>
                                    {
                                      additionalDataMLS?.value[0]
                                        ?.SourceSystemName
                                    }
                                  </b>
                                </li>
                                <li className="me-3 pe-3 border-end">
                                  Published date:{" "}
                                  <b>
                                    {additionalDataMLS?.value[0]?.OnMarketDate
                                      ? moment
                                          .tz(
                                            additionalDataMLS?.value[0]
                                              ?.OnMarketDate,
                                            "US/Mountain"
                                          )
                                          .format("M/D/YYYY")
                                      : ""}
                                  </b>
                                </li>
                              </ul>
                            </div>
                          </div>
                        </div>
                      </section>

                      <section className="contact_info bg-light border rounded-3 p-4 mt-3">
                        <h3 className="h4">Contact</h3>
                        <div class="card card-horizontal border-0">
                          <div class="">
                            <img 
                                style={{ width: "150px", height: "150px" }}
                              className="img-fluid mt-3"
                              src={profile.image ?? Picture}
                              alt="profile picture"
                              onError={(e) => profilepic(e)}
                            />
                          </div>
                          <blockquote class="card-body p-md-4 p-sm-4 p-1">
                            <div className="">
                              <strong>{profile.firstName}</strong> &nbsp;
                              <strong>{profile.lastName}</strong>
                              <p className="mb-1">{profile.company}</p>
                              <p className="mb-1">{profile.phone}</p>
                              <p className="mb-0">
                                <a
                                  aria-current="page"
                                  className="active"
                                  href="/contact"
                                >
                                  <i
                                    className="fa fa-envelope"
                                    aria-hidden="true"
                                  >
                                    &nbsp; {profile.email}
                                  </i>
                                </a>
                              </p>
                            </div>
                          </blockquote>
                        </div>
                        {/* <div className="float-left w-100 d-flex align-items-center justify-content-start">
                          <img className="rounded-circle profile_picture w-50" src={profile.image} alt="Profile" />
                          <div className="ms-3">
                            <strong>{profile.firstName}</strong> &nbsp;<strong>{profile.lastName}</strong>
                            <p className="mb-1">{profile.company}</p>
                            <p className="mb-1">{profile.phone}</p>
                            <p className="mb-0">
                              <a aria-current="page" className="active" href="/contact">
                                <i className="fa fa-envelope" aria-hidden="true">
                                  &nbsp; {profile.email}
                                </i>
                              </a>
                            </p>
                          </div>
                        </div> */}
                      </section>
                    </div>
                  </>
                ) : (
                 ""
                )}
              </div>
            </div>
          </div>
        </section>
      </main>
      
      </div>
    );
  };

  export default LandingPage;


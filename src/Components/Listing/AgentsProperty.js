import React, { useState, useRef, useEffect } from "react";
import { NavLink, useNavigate, useParams } from "react-router-dom";
import user_service from "../service/user_service.js";
import jwt from "jwt-decode";
import _ from "lodash";
import Loader from "../../Pages/Loader/Loader.js";
import Toaster from "../../Pages/Toaster/Toaster.js";


const AgentsProperty = () => {
  const [membership, setMembership] = useState("");
 

  const [loader, setLoader] = useState({ isActive: null });
  const { isActive } = loader;
  const [toaster, setToaster] = useState({
    types: null,
    isShow: null,
    toasterBody: "",
    message: "",
  });
  const { types, isShow, toasterBody, message } = toaster;

  const [searchmlsnumber, setSearchmlsnumber] = useState({ mlsNumber: "" });

  const [select, setSelect] = useState("");

  useEffect(() => {
    window.scrollTo(0, 0);
    ContactGetById(jwt(localStorage.getItem("auth")).id);
  }, []);

  const handleChangesearchmls = (event) => {
    setSearchmlsnumber({ mlsnumber: event.target.value });
  };

  const [templateType, setTemplateType] = useState();
  const [isLoading, setIsLoading] = useState(false);

  const handleType = (input) => {
    const templateType = input.target ? input.target.value : input;
    setTemplateType(templateType);
  };

  const MLSSearch = async () => {
    if (searchmlsnumber.mlsnumber) {
      try {
        setIsLoading(true);
        let response;
        if (membership === "UtahRealEstate.com") {
          console.log(membership);

          response = await user_service.listingSearchMLS(
            searchmlsnumber.mlsnumber
          );
        } else if (membership === "Park City") {
          response = await user_service.listingSearchMLSspark(
            searchmlsnumber.mlsnumber,
            "parkcity"
          );
        } else if (membership === "Washington County") {
          response = await user_service.listingSearchMLSspark(
            searchmlsnumber.mlsnumber,
            "washingtoncounty"
          );
        } else if (membership === "Iron County") {
          response = await user_service.listingSearchMLSspark(
            searchmlsnumber.mlsnumber,
            "ironcountry"
          );
        } else {
          response = await user_service.listingSearchMLS(
            searchmlsnumber.mlsnumber
          );
        }

        if (response) {
        setSelect(response.data);
        }

        setIsLoading(false);
      } catch (error) {
        console.error(error);
        setIsLoading(false);
      }
    }
    else {
        setToaster({
          types: "Error",
          isShow: true,
          message: "Please provide a valid MLS number.",
        });
        setTimeout(() => {
          setToaster((prevToaster) => ({ ...prevToaster, isShow: false }));
        }, 2000);
      }
  };


  const [getContact, setGetContact] = useState("");
  const [mls_membership, setMls_membership] = useState([]);
  const [
    commaSeparatedValuesmls_membership,
    setCommaSeparatedValuesmls_membership,
  ] = useState("");

  const ContactGetById = async () => {
    await user_service
      .contactGetById(jwt(localStorage.getItem("auth")).id)
      .then((response) => {
        if (response) {
          setGetContact(response.data);
          if (response.data && response.data.additionalActivateFields) {
            const mls_membershipString =
              response.data.additionalActivateFields?.mls_membership;
            if (mls_membershipString) {
              if (typeof mls_membershipString === "string") {
                try {
                  const mls_membershipParsed = JSON.parse(mls_membershipString);
                  if (mls_membershipParsed == "") {
                    setMls_membership(mls_membership);
                  } else {
                    setMls_membership(mls_membershipParsed);
                  }
                  if (mls_membershipParsed == "") {
                    setCommaSeparatedValuesmls_membership("");
                  } else {
                    const boardMembershipValues = mls_membershipParsed.map(
                      (item) => item.value
                    );

                    setCommaSeparatedValuesmls_membership(
                      boardMembershipValues
                    );
                  }
                } catch (error) {
                  console.error("Error parsing mls_membership as JSON:", error);
                  setCommaSeparatedValuesmls_membership("");
                }
              } else {
                setCommaSeparatedValuesmls_membership("");
              }
            } else {
              setCommaSeparatedValuesmls_membership("");
            }
          }
        }
      });
  };



 
  
  const navigate = useNavigate();

 

//   const [nextStep, setNextStep] = useState("1");
//   const handleNext = (newStep) => {
//     setNextStep(newStep);
//   };


  return (
    <div className="bg-secondary float-left w-100 pt-4 mb-4">
      <Loader isActive={isActive} />

      {isShow && (
        <Toaster
          types={types}
          isShow={isShow}
          toasterBody={toasterBody}
          message={message}
        />
      )}
      <main className="page-wrapper">
        <div className="row event-calender">
          {select ? (
            <div className="">
                  {select && select.value && select.value.length > 0 && (
                        <section className="mb-5 pb-3 w-100" data-simplebar>
                        <div
                          className="bg-light border rounded-3 p-3"
                          data-thumbnails="true"
                        >
                    <h4>test</h4>

                    <h4>test</h4>   <h4>test</h4>   <h4>test</h4>   <h4>test</h4>   <h4>test</h4>   <h4>test</h4>
                  </div>
                  </section>
                  )}
            </div>
          ) : (
            <div className="col-md-8">
              <h3 className="text-white mb-4">MLS Offers</h3>
              <div className="bg-light float-start w-100 rounded-3 mb-2 p-3">
                <hr className="mb-3" />
                <div className="mb-5">
                    <div className="float-start w-100 mt-3">
                      <label className="col-form-label ">
                        Select Membership{" "}
                      </label>
                      <select
                        className="form-select mb-2"
                        name="membership"
                        onChange={(event) => setMembership(event.target.value)}
                        value={membership}
                      >
                        <option value="">Please Select Membership</option>
                        {commaSeparatedValuesmls_membership &&
                        commaSeparatedValuesmls_membership.length > 0
                          ? commaSeparatedValuesmls_membership.map(
                              (memberahip) => <option>{memberahip}</option>
                            )
                          : ""}
                      </select>
                      {membership ? (
                        <>
                          <label className="col-form-label">
                            Enter MLS Number:
                          </label>
                          {/* <h6 className="mt-2 mb-2"></h6> */}
                          <div className="col-md-12">
                            <input
                              className="form-control w-100"
                              id="text-input-onee"
                              type="text"
                              name="mlsnumber"
                              placeholder="Enter MLS number"
                              onChange={handleChangesearchmls}
                              value={searchmlsnumber.mlsnumber}
                            />
                          </div>

                          <div className="pull-left mt-4">
                            <button
                              className="btn btn-primary btn-sm order-lg-3"
                              onClick={MLSSearch}
                              disabled={isLoading}
                            >
                              {isLoading ? "Please wait" : "Search"}
                            </button>
                          </div>
                        </>
                      ) : (
                        ""
                      )}
                    </div>            
                </div>
               
              </div>
            </div>
          )}
        </div>
      </main>
    </div>
  );
};

export default AgentsProperty;

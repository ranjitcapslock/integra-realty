import React, { useState, useEffect } from "react";
// import avtar from "./Components/img/avtar.jpg";
import _ from "lodash";
import user_service from "../service/user_service";
import jwt from "jwt-decode";
import Loader from "../../Pages/Loader/Loader.js";
import Toaster from "../../Pages/Toaster/Toaster.js";
import { useNavigate } from "react-router-dom";
import axios from "axios";

function AddListing() {
  const initialValues = {
    listingsAgent: "",
    category: "",
    mlsId: "",
    propertyType: "",
    mlsNumber: "",
    agentId: "",
    associateName: "",
    price: "",
    garage: "",
    bed: "",
    subDivision: "",
    streetAddress: "",
    city: "",
    state: "",
    zipCode: "",
    schoolDistrict: "",
    country: "",
    areaLocation: "",
    status: "",
    listDate: "",
    image: "",
  };
  const [formValues, setFormValues] = useState(initialValues);
  const [formErrors, setFormErrors] = useState({});
  const [isSubmitClick, setISSubmitClick] = useState(false);
  const [loader, setLoader] = useState({ isActive: null });
  const { isActive } = loader;
  const [toaster, setToaster] = useState({
    types: null,
    isShow: null,
    toasterBody: "",
    message: "",
  });
  const { types, isShow, toasterBody, message } = toaster;

  const [file, setFile] = useState(null);
  const [data, setData] = useState("");
  const [fileExtension, setFileExtension] = useState("");
  const [acceptedFileTypes, setAcceptedFileTypes] = useState([]);
  const [showSubmitButton, setShowSubmitButton] = useState(false);
  const navigate = useNavigate();

  // onChange Function start
  const handleChange = (e) => {
    const { name, value } = e.target;
    setFormValues({ ...formValues, [name]: value });
  };
  // onChange Function end

  {
    /* <!-- Form Validation Start--> */
  }
  useEffect(() => { 
    if (formValues && isSubmitClick) {
      validate();
    }
  }, [formValues]);

  const validate = () => {
    const values = formValues;
    const errors = {};
    if (!values.mlsNumber) {
      errors.mlsNumber = "mlsNumber is required";
    }

    if (!values.associateName) {
      errors.associateName = "associateName is required";
    }

    if (!values.listingsAgent) {
      errors.listingsAgent = "listingsAgent is required";
    }

    if (!values.category) {
      errors.category = "category is required";
    }

    if (!values.mlsId) {
      errors.mlsId = "mlsId is required";
    }
    if (!values.propertyType) {
      errors.propertyType = "propertyType is required";
    }

    // if (!values.price) {
    //   errors.price = "price is required";
    // }

    if (!values.garage) {
      errors.garage = "garage is required";
    }

    if (!values.bed) {
      errors.bed = "bed is required";
    }

    if (!values.subDivision) {
      errors.subDivision = "subDivision is required";
    }

    if (!values.city) {
      errors.city = "city is required";
    }

    if (!values.state) {
      errors.state = "state is required";
    }
    if (!values.country) {
      errors.country = "country is required";
    }
    if (!values.areaLocation) {
      errors.areaLocation = "areaLocation is required";
    }

    if (!values.streetAddress) {
      errors.streetAddress = "streetAddress is required";
    }

    if (!values.status) {
      errors.status = "status is required";
    }

    if (!values.schoolDistrict) {
      errors.schoolDistrict = "schoolDistrict is required";
    }

    if (!values.listDate) {
      errors.listDate = "listDate is required";
    }

    if (!values.zipCode) {
      errors.zipCode = "zipCode is required";
    }

    setFormErrors(errors);
    return errors;
  };
  {
    /* <!-- Form Validation End--> */
  }

  {
    /* <!--Api call Form onSubmit Start--> */
  }
  const handleFileUpload = async (e) => {
    const selectedFile = e.target.files[0];

    if (selectedFile) {
      setFile(selectedFile);
      setFileExtension(selectedFile.name.split(".").pop());
      setShowSubmitButton(true);

      const formData = new FormData();
      formData.append("file", selectedFile);
      console.log(selectedFile);
      const config = {
        headers: {
          Accept: "application/json",
          "Content-Type": "multipart/form-data",
          Authorization: `Bearer ${localStorage.getItem("auth")}`,
        },
      };
      try {
        setLoader({ isActive: true });
        const uploadResponse = await axios.post(
          "https://api.brokeragentbase.com/upload",
          formData,
          config
        );
        const uploadedFileData = uploadResponse.data;
        setLoader({ isActive: false });
        setData(uploadedFileData);
      } catch (error) {
        console.error("Error occurred during file upload:", error);
      }
    }
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
    setISSubmitClick(true);
    let checkValue = validate();
    if (_.isEmpty(checkValue)) {
      const userData = {
        agentId: jwt(localStorage.getItem("auth")).id,
        listingsAgent: formValues.listingsAgent,
        category: formValues.category,
        mlsId: formValues.mlsId,
        propertyType: formValues.propertyType,
        mlsNumber: formValues.mlsNumber,
        associateName: formValues.associateName,
        price: formValues.price,
        garage: formValues.garage,
        bed: formValues.bed,
        city: formValues.city,
        state: formValues.state,
        country: formValues.country,
        subDivision: formValues.subDivision,
        streetAddress: formValues.streetAddress,
        zipCode: formValues.zipCode,
        schoolDistrict: formValues.schoolDistrict,
        areaLocation: formValues.areaLocation,
        status: formValues.status,
        listDate: formValues.listDate,
        image: data,
      };
      console.log(userData);
      setLoader({ isActive: true });
      await user_service.listingsCreate(userData).then((response) => {
        if (response) {
          setData(response.data);
          setLoader({ isActive: false });
          setToaster({
            type: "Property Listing",
            isShow: true,
            toasterBody: response.data.message,
            message: "Property Successfully",
          });
          setTimeout(() => {
            navigate("/listing");
          }, 1000);
        } else {
          setLoader({ isActive: false });
          setToaster({
            types: "error",
            isShow: true,
            toasterBody: response.data.message,
            message: "Error",
          });
        }
      });
    }
  };
  {
    /* <!-- Api call Form onSubmit End--> */
  }

  return (
    <div className="bg-secondary float-left w-100 pt-4">
      <Loader isActive={isActive} />
      {isShow && (
        <Toaster
          types={types}
          isShow={isShow}
          toasterBody={toasterBody}
          message={message}
        />
      )}
      <main className="page-wrapper">
        <div className="container">
          <div className="justify-content-center pb-sm-2">
            <div className="bg-light rounded-3 p-4 mb-3">
              <h2 className="h4 mb-4">
                <i className="fi-info-circle text-primary fs-5 mt-n1 me-2"></i>
                Basic info
              </h2>
              <div className="add_listing col-lg-6 col-md-8 col-sm-12 col-12 m-auto">
                <div className="row">
                  <div className="col-sm-6 mb-3">
                    <label className="form-label" for="pr-country">
                      MLS Office ID<span className="text-danger">*</span>
                    </label>
                    <select
                      className="form-select"
                      id="pr-country"
                      name="mlsId"
                      value={formValues.mlsId}
                      onChange={handleChange}
                    >
                      <option></option>
                      <option>68134</option>
                      <option>70344</option>
                      <option>87677</option>
                    </select>
                    {formErrors.mlsId && (
                      <div className="invalid-tooltip">{formErrors.mlsId}</div>
                    )}
                  </div>

                  <div className="col-sm-6 mb-4">
                    <label className="form-label" for="pr-city">
                      Property Type<span className="text-danger">*</span>
                    </label>
                    <select
                      className="form-select"
                      id="pr-city"
                      name="propertyType"
                      value={formValues.propertyType}
                      onChange={handleChange}
                    >
                      <option></option>
                      <option>Land</option>
                      <option>Residential</option>
                      <option>Commercial Lease</option>
                      <option>Commercial Sale</option>
                      <option>Commercial/Industrial</option>
                      <option>Residential/Lease</option>
                      <option>Farm</option>
                    </select>
                    {formErrors.propertyType && (
                      <div className="invalid-tooltip">
                        {formErrors.propertyType}
                      </div>
                    )}
                  </div>

                  <div className="col-sm-6 mb-3">
                    <label className="form-label" for="pr-city">
                      Category<span className="text-danger">*</span>
                    </label>
                    <select
                      className="form-select"
                      id="pr-city"
                      name="category"
                      value={formValues.category}
                      onChange={handleChange}
                    >
                      <option></option>
                      <option>Single Family</option>
                      <option>Single Family 2-Story</option>
                      <option>Single Family (PUD) 2-Story</option>
                      <option>Single Family Rambler/Ranch</option>
                      <option>Single Family Split-Entry/Bi-Level</option>
                      <option>Townhouse Townhouse; Row-end</option>
                      <option>Townhouse (PUD) Townhouse; Row-mid</option>
                    </select>
                    {formErrors.category && (
                      <div className="invalid-tooltip">
                        {formErrors.category}
                      </div>
                    )}
                  </div>

                  <div className="col-sm-6 mb-3">
                    <label className="form-label">MLS Number</label>
                    <input
                      className="form-control"
                      id="inline-form-input"
                      type="number"
                      placeholder="MLS Number"
                      name="mlsNumber"
                      value={formValues.mlsNumber}
                      onChange={handleChange}
                      style={{
                        border: formErrors?.mlsNumber ? "1px solid red" : "",
                      }}
                    />
                    {formErrors.mlsNumber && (
                      <div className="invalid-tooltip">
                        {formErrors.mlsNumber}
                      </div>
                    )}
                  </div>

                  <div className="col-sm-6 mb-3">
                    <label className="form-label">Associate Name</label>
                    <input
                      className="form-control"
                      id="inline-form-input"
                      type="text"
                      placeholder="Associate Name"
                      name="associateName"
                      value={formValues.associateName}
                      onChange={handleChange}
                      style={{
                        border: formErrors?.associateName
                          ? "1px solid red"
                          : "",
                      }}
                    />
                    {formErrors.associateName && (
                      <div className="invalid-tooltip">
                        {formErrors.associateName}
                      </div>
                    )}
                  </div>

                  <div className="col-sm-6 mb-3">
                    <label className="form-label">Price Range</label>
                    <br />
                    <select
                      className="form-select"
                      id="pr-city"
                      name="price"
                      value={formValues.price}
                      onChange={handleChange}
                    >
                      <option></option>
                      <option>$500</option>
                      <option>$1000</option>
                      <option>$1500</option>
                      <option>$2000</option>
                      <option>$2500</option>
                      <option>$3000</option>
                      <option>$3500</option>
                      <option>$4000</option>
                      <option>$4500</option>
                      <option>$5000</option>
                    </select>
                    {/* {formErrors.price && (
                      <div className="invalid-tooltip">{formErrors.price}</div>
                    )} */}
                  </div>

                  <div className="col-sm-6 mb-3">
                    <label className="form-label">Garage</label>
                    <br />
                    <select
                      className="form-select"
                      id="pr-city"
                      placeholder="garage"
                      name="garage"
                      value={formValues.garage}
                      onChange={handleChange}
                    >
                      <option></option>
                      <option>+1</option>
                      <option>+2</option>
                      <option>+3</option>
                      <option>+4</option>
                      <option>+5</option>
                      <option>+6</option>
                    </select>
                    {formErrors.garage && (
                      <div className="invalid-tooltip">{formErrors.garage}</div>
                    )}
                  </div>

                  <div className="col-sm-6 mb-3">
                    <label className="form-label">Bed</label>
                    <select
                      className="form-select"
                      id="pr-city"
                      placeholder="bed"
                      name="bed"
                      value={formValues.bed}
                      onChange={handleChange}
                    >
                      <option></option>
                      <option>+1</option>
                      <option>+2</option>
                      <option>+3</option>
                      <option>+4</option>
                      <option>+5</option>
                      <option>+6</option>
                    </select>
                    {formErrors.bed && (
                      <div className="invalid-tooltip">{formErrors.bed}</div>
                    )}
                  </div>

                  <div className="col-sm-6 mb-3">
                    <label className="form-label">Subdivision</label>
                    <input
                      className="form-control"
                      id="inline-form-input"
                      type="text"
                      placeholder="Subdivision"
                      name="subDivision"
                      value={formValues.subDivision}
                      onChange={handleChange}
                      style={{
                        border: formErrors?.subDivision ? "1px solid red" : "",
                      }}
                    />

                    {formErrors.subDivision && (
                      <div className="invalid-tooltip">
                        {formErrors.subDivision}
                      </div>
                    )}
                  </div>

                  <div className="col-sm-6 mb-3">
                    <label className="form-label">Street Address</label>
                    <input
                      className="form-control"
                      id="inline-form-input"
                      type="text"
                      placeholder="Street-Address"
                      name="streetAddress"
                      value={formValues.streetAddress}
                      onChange={handleChange}
                      style={{
                        border: formErrors?.streetAddress
                          ? "1px solid red"
                          : "",
                      }}
                    />
                    {formErrors.streetAddress && (
                      <div className="invalid-tooltip">
                        {formErrors.streetAddress}
                      </div>
                    )}
                  </div>

                  <div className="col-sm-6 mb-3">
                    <label className="form-label">City</label>
                    <input
                      className="form-control"
                      id="inline-form-input"
                      type="text"
                      placeholder="city"
                      name="city"
                      value={formValues.city}
                      onChange={handleChange}
                      style={{
                        border: formErrors?.city ? "1px solid red" : "",
                      }}
                    />
                    {formErrors.city && (
                      <div className="invalid-tooltip">{formErrors.city}</div>
                    )}
                  </div>

                  <div className="col-sm-6 mb-3">
                    <label className="form-label">State</label>
                    <input
                      className="form-control"
                      id="inline-form-input"
                      type="text"
                      placeholder="State"
                      name="state"
                      value={formValues.state}
                      onChange={handleChange}
                      style={{
                        border: formErrors?.state ? "1px solid red" : "",
                      }}
                    />
                    {formErrors.state && (
                      <div className="invalid-tooltip">{formErrors.state}</div>
                    )}
                  </div>

                  <div className="col-sm-6 mb-3">
                    <label className="form-label">Country</label>
                    <input
                      className="form-control"
                      id="inline-form-input"
                      type="text"
                      placeholder="School"
                      name="country"
                      value={formValues.country}
                      onChange={handleChange}
                      style={{
                        border: formErrors?.country ? "1px solid red" : "",
                      }}
                    />
                    {formErrors.country && (
                      <div className="invalid-tooltip">
                        {formErrors.country}
                      </div>
                    )}
                  </div>

                  <div className="col-sm-6 mb-3">
                    <label className="form-label">Area-Location</label>
                    <input
                      className="form-control"
                      id="inline-form-input"
                      type="text"
                      placeholder="areaLocation"
                      name="areaLocation"
                      value={formValues.areaLocation}
                      onChange={handleChange}
                      style={{
                        border: formErrors?.areaLocation ? "1px solid red" : "",
                      }}
                    />
                    {formErrors.areaLocation && (
                      <div className="invalid-tooltip">
                        {formErrors.areaLocation}
                      </div>
                    )}
                  </div>

                  <div className="col-sm-6 mb-3">
                    <label className="form-label">Status</label>
                    <input
                      className="form-control"
                      id="inline-form-input"
                      type="text"
                      placeholder="status"
                      name="status"
                      value={formValues.status}
                      onChange={handleChange}
                      style={{
                        border: formErrors?.status ? "1px solid red" : "",
                      }}
                    />

                    {formErrors.status && (
                      <div className="invalid-tooltip">{formErrors.status}</div>
                    )}
                  </div>

                  <div className="col-sm-6 mb-3">
                    <label className="form-label" for="pr-birth-date">
                      ListDate<span className="text-danger">*</span>
                    </label>
                    <input
                      className="form-control"
                      type="date"
                      id="inline-form-input"
                      placeholder="Choose date"
                      name="listDate"
                      value={formValues.listDate}
                      onChange={handleChange}
                      style={{
                        border: formErrors?.listDate ? "1px solid red" : "",
                      }}
                      data-datepicker-options='{"altInput": true, "altFormat": "F j, Y", "dateFormat": "Y-m-d"}'
                    />
                    {formErrors.listDate && (
                      <div className="invalid-tooltip">
                        {formErrors.listDate}
                      </div>
                    )}
                  </div>

                  <div className="col-sm-6 mb-3">
                    <label className="form-label">ListingsAgent</label>
                    <input
                      className="form-control"
                      id="inline-form-input"
                      type="text"
                      placeholder="listingsAgent"
                      name="listingsAgent"
                      value={formValues.listingsAgent}
                      onChange={handleChange}
                      style={{
                        border: formErrors?.listingsAgent
                          ? "1px solid red"
                          : "",
                      }}
                    />
                    {formErrors.listingsAgent && (
                      <div className="invalid-tooltip">
                        {formErrors.listingsAgent}
                      </div>
                    )}
                  </div>

                  <div className="col-sm-6 mb-3">
                    <label className="form-label">Zip Code</label>
                    <input
                      className="form-control"
                      id="inline-form-input"
                      type="number"
                      placeholder="zip-code"
                      name="zipCode"
                      value={formValues.zipCode}
                      onChange={handleChange}
                      style={{
                        border: formErrors?.zipCode ? "1px solid red" : "",
                      }}
                    />
                    {formErrors.zipCode && (
                      <div className="invalid-tooltip">
                        {formErrors.zipCode}
                      </div>
                    )}
                  </div>

                  <div className="col-sm-6 mb-3">
                    <label className="form-label">School District</label>
                    <input
                      className="form-control"
                      id="inline-form-input"
                      type="text"
                      placeholder="School"
                      name="schoolDistrict"
                      value={formValues.schoolDistrict}
                      onChange={handleChange}
                      style={{
                        border: formErrors?.schoolDistrict
                          ? "1px solid red"
                          : "",
                      }}
                    />

                    {formErrors.schoolDistrict && (
                      <div className="invalid-tooltip">
                        {formErrors.schoolDistrict}
                      </div>
                    )}
                  </div>

                  <div className="col-sm-6 mb-3 d-flex align-items-center justify-content-start">
                    <label className="documentlabel pull-right mt-lg-4 mt-md-4 mt-sm-2 mt-2">
                      {" "}
                      Upload File
                      <input
                        className="form-control"
                        id="inline-form-input"
                        type="file"
                        accept={acceptedFileTypes
                        .map((type) => `.${type}`)
                        .join(",")}
                        name="image"
                        onChange={handleFileUpload}
                        value={formValues.file}
                      />
                    </label>
                  </div>
                </div>

                <div className="d-flex align-items-center justify-content-between mt-3">
                  <button
                    className="btn btn-primary px-3 px-sm-4"
                    type="button"
                    onClick={handleSubmit}>
                    Submit
                  </button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </main>
    </div>
  );
}
export default AddListing;

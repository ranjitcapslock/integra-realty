import React, { useState, useRef, useEffect } from "react";
import user_service from "../service/user_service.js";
import jwt from "jwt-decode";
import _ from "lodash";
import Loader from "../../Pages/Loader/Loader.js";
import Toaster from "../../Pages/Toaster/Toaster.js";
import TypeOne from "../img/PropertyLabel.png";
// import backgroundImages from "../img/background2.jpg";

import defaultpropertyimage from "../../Components/img/defaultpropertyimage.jpeg";

import Logo from "../img/logo.png";
import {
  Document,
  Page,
  Text,
  Image,
  StyleSheet,
  PDFDownloadLink,
  View,
  ImageBackground,
} from "@react-pdf/renderer";

const PropertyLebel = () => {
  const [membership, setMembership] = useState("");
  const componentRef = useRef();

  const [loader, setLoader] = useState({ isActive: null });
  const { isActive } = loader;
  const [toaster, setToaster] = useState({
    types: null,
    isShow: null,
    toasterBody: "",
    message: "",
  });
  const { types, isShow, toasterBody, message } = toaster;

  const [searchmlsnumber, setSearchmlsnumber] = useState({ mlsNumber: "" });

  const [select, setSelect] = useState("");

  useEffect(() => {
    window.scrollTo(0, 0);
    ContactGetById(jwt(localStorage.getItem("auth")).id);
    PlatformdetailsGet();
  }, []);

  const handleChangesearchmls = (event) => {
    setSearchmlsnumber({ mlsnumber: event.target.value });
  };

  const [templateType, setTemplateType] = useState();
  const [isLoading, setIsLoading] = useState(false);

  const handleType = (input) => {
    const templateType = input.target ? input.target.value : input;
    setTemplateType(templateType);
  };

  const API_BASE_URL = "https://api.brokeragentbase.com";
  const MLSSearch = async () => {
    if (searchmlsnumber.mlsnumber) {
      try {
        setIsLoading(true);
        let response;

        if (membership === "UtahRealEstate.com") {
          response = await user_service.listingSearchMLS(
            searchmlsnumber.mlsnumber
          );
        } else if (membership === "Park City") {
          response = await user_service.listingSearchMLSspark(
            searchmlsnumber.mlsnumber,
            "parkcity"
          );
        } else if (membership === "Washington County") {
          response = await user_service.listingSearchMLSspark(
            searchmlsnumber.mlsnumber,
            "washingtoncounty"
          );
        } else if (membership === "Iron County") {
          response = await user_service.listingSearchMLSspark(
            searchmlsnumber.mlsnumber,
            "ironcounty"
          );
        } else {
          response = await user_service.listingSearchMLS(
            searchmlsnumber.mlsnumber
          );
        }

        if (response && response.data) {
          const propertyData = response.data;

          propertyData.media = (propertyData.media || []).map((image) => ({
            MediaURL: image?.MediaURL
              ? `${API_BASE_URL}/proxy-image?url=${encodeURIComponent(
                  image.MediaURL
                )}`
              : defaultpropertyimage,
            description: image?.description || "Click to add a description",
          }));
          setSelect(propertyData);
          setToaster({
            types: "Success",
            isShow: true,
            message: "MLS search successful!",
          });
        } else {
          setToaster({
            types: "Error",
            isShow: true,
            message: "No data found for the provided MLS number.",
          });
        }

        setIsLoading(false);
      } catch (error) {
        console.error("Error in MLSSearch:", error);
        setToaster({
          types: "Error",
          isShow: true,
          message: "Please provide a valid MLS number.",
        });
      } finally {
        setIsLoading(false);
        setTimeout(() => {
          setToaster((prevToaster) => ({ ...prevToaster, isShow: false }));
        }, 2000);
      }
    } else {
      setToaster({
        types: "Error",
        isShow: true,
        message: "Please provide a valid MLS number.",
      });
      setTimeout(() => {
        setToaster((prevToaster) => ({ ...prevToaster, isShow: false }));
      }, 2000);
    }
  };

  //   const API_BASE_URL = "https://api.brokeragentbase.com";
  //   const MLSSearch = async () => {
  //     if (searchmlsnumber.mlsnumber) {
  //       try {
  //         setIsLoading(true);
  //         let response;

  //         if (membership === "UtahRealEstate.com") {
  //           response = await user_service.listingSearchMLS(
  //             searchmlsnumber.mlsnumber
  //           );
  //         } else if (membership === "Park City") {
  //           response = await user_service.listingSearchMLSspark(
  //             searchmlsnumber.mlsnumber,
  //             "parkcity"
  //           );
  //         } else if (membership === "Washington County") {
  //           response = await user_service.listingSearchMLSspark(
  //             searchmlsnumber.mlsnumber,
  //             "washingtoncounty"
  //           );
  //         } else if (membership === "Iron County") {
  //           response = await user_service.listingSearchMLSspark(
  //             searchmlsnumber.mlsnumber,
  //             "ironcounty"
  //           );
  //         } else {
  //           response = await user_service.listingSearchMLS(
  //             searchmlsnumber.mlsnumber
  //           );
  //         }

  //         if (response && response.data) {
  //           const propertyData = response.data;

  //           // Check if media or value arrays are empty
  //           if (
  //             (propertyData.media && propertyData.media.length === 0) ||
  //             (propertyData.value && propertyData.value.length === 0)
  //           ) {
  //             setToaster({
  //               types: "Error",
  //               isShow: true,
  //               message: "Please Add a Valid MLS Number.",
  //             });
  //           } else {
  //             // Modify image URLs to route through proxy if media is available
  //             if (propertyData.media && propertyData.media.length > 0) {
  //               propertyData.media = propertyData.media.map((image) => {
  //                 return {
  //                   ...image,
  //                   MediaURL: `${API_BASE_URL}/proxy-image?url=${encodeURIComponent(
  //                     image.MediaURL
  //                   )}`,
  //                 };
  //               });
  //             }

  //             setSelect(propertyData);
  //             setToaster({
  //               types: "Success",
  //               isShow: true,
  //               message: "MLS search successful!",
  //             });
  //           }
  //         } else {
  //           setToaster({
  //             types: "Error",
  //             isShow: true,
  //             message: "No data found for the provided MLS number.",
  //           });
  //         }

  //         setIsLoading(false);
  //       } catch (error) {
  //         console.error("Error in MLSSearch:", error);
  //         setToaster({
  //           types: "Error",
  //           isShow: true,
  //           message: "Please provide a valid MLS number.",
  //         });
  //       } finally {
  //         setIsLoading(false);
  //         setTimeout(() => {
  //           setToaster((prevToaster) => ({ ...prevToaster, isShow: false }));
  //         }, 2000);
  //       }
  //     } else {
  //       setToaster({
  //         types: "Error",
  //         isShow: true,
  //         message: "Please provide a valid MLS number.",
  //       });
  //       setTimeout(() => {
  //         setToaster((prevToaster) => ({ ...prevToaster, isShow: false }));
  //       }, 2000);
  //     }
  //   };

  const [getContact, setGetContact] = useState("");
  const [mls_membership, setMls_membership] = useState([]);
  const [
    commaSeparatedValuesmls_membership,
    setCommaSeparatedValuesmls_membership,
  ] = useState("");

  const ContactGetById = async () => {
    await user_service
      .contactGetById(jwt(localStorage.getItem("auth")).id)
      .then((response) => {
        if (response) {
          setGetContact(response.data);
          if (response.data && response.data.additionalActivateFields) {
            const mls_membershipString =
              response.data.additionalActivateFields?.mls_membership;
            if (mls_membershipString) {
              if (typeof mls_membershipString === "string") {
                try {
                  const mls_membershipParsed = JSON.parse(mls_membershipString);
                  if (mls_membershipParsed == "") {
                    setMls_membership(mls_membership);
                  } else {
                    setMls_membership(mls_membershipParsed);
                  }
                  if (mls_membershipParsed == "") {
                    setCommaSeparatedValuesmls_membership("");
                  } else {
                    const boardMembershipValues = mls_membershipParsed.map(
                      (item) => item.value
                    );

                    setCommaSeparatedValuesmls_membership(
                      boardMembershipValues
                    );
                  }
                } catch (error) {
                  console.error("Error parsing mls_membership as JSON:", error);
                  setCommaSeparatedValuesmls_membership("");
                }
              } else {
                setCommaSeparatedValuesmls_membership("");
              }
            } else {
              setCommaSeparatedValuesmls_membership("");
            }
          }
        }
      });
  };

  const [editingField, setEditingField] = useState(null);
  const [editingText, setEditingText] = useState(null);
  const [editedText, setEditedText] = useState("");
  const [selectedImage, setSelectedImage] = useState(null);
  const [uploadedImages, setUploadedImages] = useState({});

  const handleImageClick = (imageIndex) => {
    setSelectedImage(imageIndex);
  };

  // Handle image upload
  const handleImageUpload = (e, imageIndex) => {
    const file = e.target.files[0];
    if (file) {
      const reader = new FileReader();
      reader.onload = (upload) => {
        // Update the uploaded image state
        setUploadedImages((prev) => ({
          ...prev,
          [imageIndex]: upload.target.result,
        }));

        // Ensure that `media` is defined before updating it
        setSelect((prevSummary) => {
          const updatedImages = [...(prevSummary.media || [])]; // Handle case where media is undefined
          updatedImages[imageIndex] = { MediaURL: upload.target.result }; // Update the image at the specific index
          return { ...prevSummary, media: updatedImages };
        });
      };
      reader.readAsDataURL(file);
    }
    setSelectedImage(null);
  };

  const handleTextClick = (field, currentValue) => {
    setEditingField(field); // Set the field being edited
    setEditedText(currentValue); // Pre-fill input with current value (address)
  };

  // Function to handle saving the edited text locally
  const handleTextSave = (field) => {
    // Update the selected value locally after editing
    setSelect((prevSelect) => ({
      ...prevSelect,
      value: [
        {
          ...prevSelect.value[0],
          UnparsedAddress: editedText, // Update the address with the new value
        },
      ],
    }));

    setEditingField(null); // Close the input field after saving
  };

  const handleTextPrice = (field, currentValue) => {
    setEditingField(field); // Set the field being edited
    setEditedText(currentValue); // Pre-fill input with current value (address)
  };

  const handleTextSavePrice = (field) => {
    // Update the selected value locally after editing
    setSelect((prevSelect) => ({
      ...prevSelect,
      value: [
        {
          ...prevSelect.value[0],
          ListPrice: editedText, // Update the address with the new value
        },
      ],
    }));

    setEditingField(null); // Close the input field after saving
  };

  const handleTextPublicRemarks = (field, currentValue) => {
    setEditingField(field); // Set the field being edited
    setEditedText(currentValue); // Pre-fill input with current value (address)
  };

  const handleTextSavePublicRemarks = (field) => {
    setSelect((prevSelect) => ({
      ...prevSelect,
      value: [
        {
          ...prevSelect.value[0],
          PublicRemarks: editedText, // Update the address with the new value
        },
      ],
    }));

    setEditingField(null); // Close the input field after saving
  };

  const handleTextBathroom = (field, currentValue) => {
    setEditingField(field); // Set the field being edited
    setEditedText(currentValue); // Pre-fill input with current value (address)
  };

  const handleTextSaveBathroom = (field) => {
    // Update the selected value locally after editing
    setSelect((prevSelect) => ({
      ...prevSelect,
      value: [
        {
          ...prevSelect.value[0],
          BathroomsFull: editedText, // Update the address with the new value
        },
      ],
    }));

    setEditingField(null); // Close the input field after saving
  };

  const handleTextBedrooms = (field, currentValue) => {
    setEditingField(field); // Set the field being edited
    setEditedText(currentValue); // Pre-fill input with current value (address)
  };

  const handleTextSaveBedrooms = (field) => {
    // Update the selected value locally after editing
    setSelect((prevSelect) => ({
      ...prevSelect,
      value: [
        {
          ...prevSelect.value[0],
          BedroomsTotal: editedText, // Update the address with the new value
        },
      ],
    }));

    setEditingField(null); // Close the input field after saving
  };

  const handleTextSquareFeet = (field, currentValue) => {
    setEditingField(field); // Set the field being edited
    setEditedText(currentValue); // Pre-fill input with current value (address)
  };

  const handleTextSaveSquareFeet = (field) => {
    // Update the selected value locally after editing
    setSelect((prevSelect) => ({
      ...prevSelect,
      value: [
        {
          ...prevSelect.value[0],
          BuildingAreaTotal: editedText, // Update the address with the new value
        },
      ],
    }));

    setEditingField(null); // Close the input field after saving
  };

  const handleTextLotSize = (field, currentValue) => {
    setEditingField(field); // Set the field being edited
    setEditedText(currentValue); // Pre-fill input with current value (address)
  };

  const handleTextSaveLotSize = (field) => {
    // Update the selected value locally after editing
    setSelect((prevSelect) => ({
      ...prevSelect,
      value: [
        {
          ...prevSelect.value[0],
          LotSizeAcres: editedText, // Update the address with the new value
        },
      ],
    }));

    setEditingField(null); // Close the input field after saving
  };

  const handleTextYearBuilt = (field, currentValue) => {
    setEditingField(field); // Set the field being edited
    setEditedText(currentValue); // Pre-fill input with current value (address)
  };

  const handleTextSaveYearBuilt = (field) => {
    // Update the selected value locally after editing
    setSelect((prevSelect) => ({
      ...prevSelect,
      value: [
        {
          ...prevSelect.value[0],
          YearBuilt: editedText, // Update the address with the new value
        },
      ],
    }));

    setEditingField(null); // Close the input field after saving
  };

  const handleTextHeating = (field, currentValue) => {
    setEditingField(field); // Set the field being edited
    setEditedText(currentValue); // Pre-fill input with current value (address)
  };

  const handleTextSaveHeating = (field) => {
    setSelect((prevSelect) => ({
      ...prevSelect,
      value: [
        {
          ...prevSelect.value[0],
          Heating: editedText, // Update the address with the new value
        },
      ],
    }));
    setEditingField(null); // Close the input field after saving
  };

  const handleTextColing = (field, currentValue) => {
    setEditingField(field); // Set the field being edited
    setEditedText(currentValue); // Pre-fill input with current value (address)
  };

  const handleTextSaveCooling = (field) => {
    setSelect((prevSelect) => ({
      ...prevSelect,
      value: [
        {
          ...prevSelect.value[0],
          Cooling: editedText,
        },
      ],
    }));
    setEditingField(null); // Close the input field after saving
  };

  const handleTextGarageSpaces = (field, currentValue) => {
    setEditingField(field); // Set the field being edited
    setEditedText(currentValue); // Pre-fill input with current value (address)
  };

  const handleTextSaveGarageSpaces = (field) => {
    setSelect((prevSelect) => ({
      ...prevSelect,
      value: [
        {
          ...prevSelect.value[0],
          GarageSpaces: editedText,
        },
      ],
    }));
    setEditingField(null); // Close the input field after saving
  };

  const handleTextRoof = (field, currentValue) => {
    setEditingField(field); // Set the field being edited
    setEditedText(currentValue); // Pre-fill input with current value (address)
  };

  const handleTextSaveRoof = (field) => {
    setSelect((prevSelect) => ({
      ...prevSelect,
      value: [
        {
          ...prevSelect.value[0],
          Roof: editedText,
        },
      ],
    }));
    setEditingField(null); // Close the input field after saving
  };

  const handleTextListAgent = (field, currentValue) => {
    setEditingField(field); // Set the field being edited
    setEditedText(currentValue); // Set the current text value in the input
  };

  // Handle saving the edited text for the list agent
  const handleTextSaveListAgent = (field) => {
    setProfile((prevProfile) => ({
      ...prevProfile,
      [field]: editedText, // Dynamically update the appropriate field (firstName or lastName)
    }));
    setEditingField(null); // Close the input field after saving
  };

  const handleTextListOffice = (field, currentValue) => {
    setEditingField(field); // Set the field being edited
    setEditedText(currentValue); // Pre-fill input with current value (address)
  };

  const handleTextSaveListOffice = (field) => {
    // Update the selected value locally after editing
    setProfile((prevProfile) => ({
      ...prevProfile,
      active_office: editedText, // Dynamically update the appropriate field (firstName or lastName)
    }));

    setEditingField(null); // Close the input field after saving
  };

  const handleTextOfficePhone = (field, currentValue) => {
    setEditingField(field); // Set the field being edited
    setEditedText(currentValue); // Pre-fill input with current value (address)
  };

  const handleTextSaveOfficePhone = (field) => {
    // Update the selected value locally after editing
    setProfile((prevProfile) => ({
      ...prevProfile,
      phone: editedText, // Dynamically update the appropriate field (firstName or lastName)
    }));

    setEditingField(null); // Close the input field after saving
  };

  const handleTextPublished = (field, currentValue) => {
    setEditingField(field); // Set the field being edited
    setEditedText(currentValue); // Pre-fill input with current value (address)
  };

  const handleTextSavePublished = (field) => {
    // Update the selected value locally after editing
    setSelect((prevSelect) => ({
      ...prevSelect,
      value: [
        {
          ...prevSelect.value[0],
          SourceSystemName: editedText || "defaultText",
        },
      ],
    }));

    setEditingField(null); // Close the input field after saving
  };

  const handleTextCompanyCity = (field, currentValue) => {
    setEditingField(field); // Set the field being edited
    setEditedText(currentValue); // Pre-fill input with current value (address)
  };

  const handleTextSaveCompanyCity = (field) => {
    // Update the selected value locally after editing
    setSelect((prevSelect) => ({
      ...prevSelect,
      value: [
        {
          ...prevSelect.value[0],
          City: editedText, // Update the address with the new value
        },
      ],
    }));

    setEditingField(null); // Close the input field after saving
  };

  const handleTextCompanyState = (field, currentValue) => {
    setEditingField(field); // Set the field being edited
    setEditedText(currentValue); // Pre-fill input with current value (address)
  };

  const handleTextSaveCompanyState = (field) => {
    // Update the selected value locally after editing
    setSelect((prevSelect) => ({
      ...prevSelect,
      value: [
        {
          ...prevSelect.value[0],
          StateOrProvince: editedText, // Update the address with the new value
        },
      ],
    }));

    setEditingField(null); // Close the input field after saving
  };

  const handleTextCompanyPostCode = (field, currentValue) => {
    setEditingField(field); // Set the field being edited
    setEditedText(currentValue); // Pre-fill input with current value (address)
  };

  const handleTextSaveCompanyPostCode = (field) => {
    // Update the selected value locally after editing
    setSelect((prevSelect) => ({
      ...prevSelect,
      value: [
        {
          ...prevSelect.value[0],
          PostalCode: editedText, // Update the address with the new value
        },
      ],
    }));

    setEditingField(null); // Close the input field after saving
  };

  const [platformData, setPlatFormData] = useState({});
  const [uploadedLogo, setUploadedLogo] = useState(null);
  const [showLogoInput, setShowLogoInput] = useState(false); // State to show or hide input field
  const [profile, setProfile] = useState({});

  const [uploadedContact, setUploadedContact] = useState(null);
  const [showContact, setShowContact] = useState(false);

  const PlatformdetailsGet = async () => {
    await user_service.platformdetailsGet().then((response) => {
      if (response) {
        setPlatFormData(response.data.data[0]);
      }
    });
  };

  const profileGetAll = async () => {
    if (localStorage.getItem("auth")) {
      try {
        const response = await user_service.profileGet(
          jwt(localStorage.getItem("auth")).id
        );
        if (response) {
          const profileData = response.data;
          if (profileData && profileData.image) {
            const proxyImageUrl = `${API_BASE_URL}/proxy-image?url=${encodeURIComponent(
              profileData.image
            )}`;

            // Assuming 'setProfile' is a function that updates the state with the modified profile data
            setProfile({
              ...profileData, // Copy existing profile data
              image: proxyImageUrl, // Replace original image with the proxy URL
            });
          } else {
            setProfile(profileData); // No image to proxy, just set the profile as is
          }
        }
      } catch (error) {
        console.error("Error fetching profile data:", error);
      }
    }
  };

  useEffect(() => {
    window.scrollTo(0, 0);
    PlatformdetailsGet();
    profileGetAll();
  }, []);

  // Handle logo upload

  const handleLogoUploadContact = (e) => {
    const file = e.target.files[0];
    if (file) {
      const reader = new FileReader();
      reader.onload = (upload) => {
        setUploadedContact(upload.target.result); // Store the uploaded logo
        setShowContact(false); // Hide the input after upload
        // Update platformData with the new logo while preserving the rest of the data
        setProfile((prevData) => ({
          ...prevData,
          image: upload.target.result || prevData.image, // Update the logo in platformData
        }));
      };
      reader.readAsDataURL(file); // Read the file as a data URL for image preview
    }
  };

  const handleLogoUpload = (e) => {
    const file = e.target.files[0];
    if (file) {
      const reader = new FileReader();
      reader.onload = (upload) => {
        setUploadedLogo(upload.target.result); // Store the uploaded logo
        setShowLogoInput(false); // Hide the input after upload
        // Update platformData with the new logo while preserving the rest of the data
        setPlatFormData((prevData) => ({
          ...prevData,
          platform_logo: upload.target.result, // Update the logo in platformData
        }));
      };
      reader.readAsDataURL(file); // Read the file as a data URL for image preview
    }
  };

  // Handle logo click to show the input
  const handleLogoClick = () => {
    setShowLogoInput(true); // Show the file input when the logo is clicked
  };

  const handleLogoClickContact = () => {
    setShowContact(true); // Show the file input when the logo is clicked
  };

  const styles = StyleSheet.create({
    page: {
      backgroundColor: "#7f7676",
      padding: 10,
    },

    rowContainer: {
      display: "flex",
      flexDirection: "row",
      justifyContent: "space-between",
      width: "100%",
    },

    mainpic: {
      width: "100%",
      height: 400,
      objectFit: "cover",
      marginBottom: 2,
      marginTop: 40,
    },

    containerDescription: {
      width: "100%",
    },

    imageGrid: {
      display: "flex",
      alignItems: "center",
      flexDirection: "row",
      flexWrap: "wrap",
      justifyContent: "flex-end",
      position: "relative",
      marginTop: -70,
      paddingLeft: 20,
      paddingRight: 20,
    },
    imageItem: {
      width: 110,
      height: 110,
      objectFit: "cover",
      marginBottom: 10,
      padding:10,
      marginLeft:10,
      // right:100,
      // position:"relative",
      // bottom: 10
    },

    descriptionText: {
      marginTop: 5,
      // padding: 5,
      color: "white",
      fontSize: 14,
      textAlign: "justify",
    },

 
    pageSecond: {
      backgroundColor: "#7f7676", // Background color matching the flyer design
      padding: 20,
      flexDirection: "column",
    },
    propertyData: {
      marginBottom: 20,
      padding: 10,
    },
    officePrice: {
      fontSize: 28,
      fontWeight: "bold",
      color: "#ffffff",
      textAlign: "center",
      marginBottom: 20,
    },
    addressBox: {
      flexDirection: "row",
      justifyContent: "space-between",
      alignItems: "center",
      marginTop: 20,
      padding: 10,
      borderRadius: 5,
    },
  
    logoCol: {
      marginBottom: 8,
    },
    officeLogo: {
      width: 170,
      height: 170,
      objectFit: "contain",
      marginBottom: 10,
    },
    officeText: {
      fontSize: 12,
      color: "#ffffff",
      marginBottom: 4,
    },
    officeSection: {
      flexDirection: "row",
      alignItems: "center",
      width: "100%",
    },
    officeContactImage: {
      width: 180,
      height: 220,
      // objectFit: "cover",
      borderRadius:10,
      marginRight: 10,
    },
    officeDetails: {
      marginLeft:30
    },

    officeDetailsSecond :{
      marginLeft:40
    },

    officeAddress: {
      flexDirection: "row",
      alignItems: "center",
      width: "100%",
    },
  
  });

  const Flyer = ({ select, platformData, profile }) => {
    return (
      <Document>
        <Page size="A4" style={styles.page}>
          <View style={styles.rowContainer}>
            {select?.media[0]?.MediaURL && (
              <Image style={styles.mainpic} src={select.media[0].MediaURL} />
            )}
          </View>

          <View style={styles.imageGrid}>
            {select?.media && select.media.length > 1
              ? [
                  ...select.media.slice(1, 4),
                  ...Array(
                    Math.max(0, 3 - select.media.slice(1, 4).length)
                  ).fill({ MediaURL: defaultpropertyimage }),
                ].map((mediaItem, mediaIndex) => (
                  <Image
                    style={styles.imageItem}
                    source={{
                      uri: mediaItem?.MediaURL || defaultpropertyimage,
                    }} // Use default image if mediaItem is null
                    key={mediaIndex}
                  />
                ))
              : // Default fallback for when no media exists or condition is not met
                Array(3)
                  .fill()
                  .map((_, defaultIndex) => (
                    <Image
                      style={styles.imageItem}
                      source={{ uri: defaultpropertyimage }}
                      key={defaultIndex}
                    />
                  ))}
          </View>
          <View style={styles.containerDescription}>
            <Text style={styles.descriptionText}>
              {select?.value[0]?.PublicRemarks}
            </Text>
          </View>
        </Page>

        <Page size="A4" style={styles.pageSecond}>
        <View style={styles.propertyData}>
          <View style={styles.addressBox}>
               {/* Agent Contact Section */}
             <View style={styles.officeSection}>
              <View>
                <Image
                  style={styles.officeContactImage}
                  src={profile?.image} // Fallback profile image
                />
              </View>
              <View style={styles.officeDetails}>
                <Text style={styles.officeText}>
                  Agent: {profile?.firstName || "Shawn"} {profile?.lastName || "Kenney"}
                </Text>
                <Text style={styles.officeText}>
                  Office: {profile?.active_office || "Corporate"}
                </Text>
                <Text style={styles.officeText}>Phone: {profile?.phone || "9856589892"}</Text>
              </View>
            </View>
          </View>

          <View style={styles.addressBox}>
               {/* Agent Contact Section */}
             <View style={styles.officeSection}>
              <View>
              <Image style={styles.officeLogo}  src={platformData || Logo}/>
                {/* <Image
                  style={styles.officeContactImage}
                  src={profile?.image} // Fallback profile image
                /> */}
              </View>
              <View style={styles.officeDetailsSecond}>
              <Text style={styles.officeText}>
              $ {select?.value[0]?.ListPrice || "X00,00"}
             </Text>
              <Text style={styles.officeText}>
                {select?.value[0]?.SourceSystemName || "UtahRealEstate.com"}
              </Text>
              <Text style={styles.officeText}>
                {select?.value[0]?.UnparsedAddress || "2024 Demo St"}
              </Text>
              <Text style={styles.officeText}>
                {select?.value[0]?.City || "American Fork"},&nbsp;
                {select?.value[0]?.StateOrProvince || "UT"}&nbsp;
                {select?.value[0]?.PostalCode || "84003"}
              </Text>
              </View>
            </View>
          </View>
        </View>


        </Page>
      </Document>
    );
  };

  const [nextStep, setNextStep] = useState("1");
  const handleNext = (newStep) => {
    setNextStep(newStep);
  };

  return (
    <div className="bg-secondary float-left w-100 pt-4 mb-4">
      <Loader isActive={isActive} />

      {isShow && (
        <Toaster
          types={types}
          isShow={isShow}
          toasterBody={toasterBody}
          message={message}
        />
      )}
      <main className="page-wrapper">
        <div className="row event-calender">
          {select ? (
            <div className="">
              {templateType === "type1" ? (
                <div className="">
                  {select && select.value && select.value.length > 0 && (
                    <section className="mb-5 pb-3 w-100" data-simplebar>
                      <div
                        className="bg-light border rounded-3 p-3"
                        data-thumbnails="true"
                      >
                        <div className="labelCardsTemplate">
                          {/* Step 1 */}
                          {nextStep === "1" && (
                            <div className="row" data-thumbnails="true">
                              <div className="col-md-12 singleListing">
                                <div className="card-img-top">
                                  <img
                                    className="img-fluid"
                                    style={{
                                      minHeight: "400px",
                                      borderRadius: "0px",
                                    }}
                                    src={
                                      uploadedImages[0] ||
                                      select.media[0].MediaURL ||
                                      defaultpropertyimage
                                    }
                                    alt="Property"
                                    onClick={() => handleImageClick(0)}
                                  />
                                  {selectedImage === 0 && (
                                    <label className="documentlabelNew">
                                      <input
                                        id="REPC_real_estate_purchase_contract"
                                        type="file"
                                        onChange={(e) =>
                                          handleImageUpload(e, 0)
                                        }
                                      />
                                      <i className="h2 fi-edit opacity-80"></i>
                                    </label>
                                  )}
                                </div>
                              </div>

                              <div className="second_labelImage">
                                {select?.media && select.media.length > 1
                                  ? // Process the media array and ensure we have exactly 3 images
                                    [
                                      ...select.media.slice(1, 4),
                                      ...Array(
                                        Math.max(
                                          0,
                                          3 - select.media.slice(1, 4).length
                                        )
                                      ).fill(null),
                                    ].map((mediaItem, mediaIndex) => (
                                      <div className="proprty_mediaImage">
                                        <img
                                          className="img-fluid rounded-0"
                                          src={
                                            mediaItem
                                              ? uploadedImages[
                                                  mediaIndex + 1
                                                ] ||
                                                mediaItem.MediaURL ||
                                                defaultpropertyimage
                                              : defaultpropertyimage
                                          }
                                          alt={`Property media ${
                                            mediaIndex + 1
                                          }`}
                                          onClick={() =>
                                            handleImageClick(mediaIndex + 1)
                                          }
                                        />
                                        {selectedImage === mediaIndex + 1 && (
                                          <label className="documentPropertlabel">
                                            <input
                                              id={`REPC_real_estate_purchase_contract_${mediaIndex}`}
                                              type="file"
                                              onChange={(e) =>
                                                handleImageUpload(
                                                  e,
                                                  mediaIndex + 1
                                                )
                                              }
                                            />
                                            <i className="h2 fi-edit opacity-80"></i>
                                          </label>
                                        )}
                                      </div>
                                    ))
                                  : // Default images if no media exists
                                    [0, 1, 2].map((_, defaultIndex) => (
                                      <div className="proprty_mediaImage">
                                        <img
                                          className="img-fluid rounded-0"
                                          src={
                                            uploadedImages[defaultIndex + 1] ||
                                            defaultpropertyimage
                                          }
                                          alt={`Default Property media ${
                                            defaultIndex + 1
                                          }`}
                                          onClick={() =>
                                            handleImageClick(defaultIndex + 1)
                                          }
                                        />
                                        {selectedImage === defaultIndex + 1 && (
                                          <label className="documentPropertlabel">
                                            <input
                                              id={`REPC_real_estate_purchase_contract_${defaultIndex}`}
                                              type="file"
                                              onChange={(e) =>
                                                handleImageUpload(
                                                  e,
                                                  defaultIndex + 1
                                                )
                                              }
                                            />
                                            <i className="h2 fi-edit opacity-80"></i>
                                          </label>
                                        )}
                                      </div>
                                    ))}
                              </div>

                              <div className="col-md-12 mt-3">
                                {editingField === "publicRemarks" ? (
                                  <textarea
                                    className="form-control mt-0 w-100"
                                    id="textarea-input"
                                    rows="5"
                                    type="text"
                                    value={editedText} // Controlled input with edited text
                                    onChange={(e) => {
                                      const wordCount = e.target.value
                                        .trim()
                                        .split(/\s+/).length;
                                      if (wordCount <= 150) {
                                        setEditedText(e.target.value); // Update text only if word count is <= 150
                                      }
                                    }} // Update text as the user types
                                    onBlur={() =>
                                      handleTextSavePublicRemarks(
                                        "publicRemarks"
                                      )
                                    } // Save the text when the user leaves the input field
                                    autoFocus
                                  />
                                ) : (
                                  <p
                                    className="description_PrintPage text-white"
                                    style={{
                                      fontSize: "16px",
                                    }}
                                    onClick={() =>
                                      handleTextPublicRemarks(
                                        "publicRemarks",
                                        select?.value[0]?.PublicRemarks ||
                                          "testing"
                                      )
                                    }
                                  >
                                    {select.value[0]?.PublicRemarks}
                                  </p>
                                )}
                              </div>
                            </div>
                          )}

                          {/* Step 2 */}
                          {nextStep === "2" && (
                            <div className="row">
                              {/* <div className="col-md-12 text-center">
                                <h6 className="mb-0">ASKING PRICE</h6>

                                {editingField === "price" ? (
                                  <div className="text-center">
                                    <input
                                      type="text"
                                      value={editedText}
                                      onChange={(e) =>
                                        setEditedText(e.target.value)
                                      }
                                      onBlur={() =>
                                        handleTextSavePrice("price")
                                      }
                                      autoFocus
                                    />
                                  </div>
                                ) : (
                                  <span
                                    className=""
                                    style={{
                                      fontSize: "50px",
                                      color: "#E86712",
                                    }}
                                    onClick={() =>
                                      handleTextPrice(
                                        "price",
                                        select?.value[0]?.ListPrice
                                      )
                                    }
                                  >
                                    $ {select?.value[0]?.ListPrice ?? "0"}
                                  </span>
                                )}
                              </div>

                              <div className="col-md-12 text-center">
                                <h2 className="text-center">
                                  Property Details
                                </h2>
                                <ul className="list-unstyled text-center">
                                  {editingField === "bedrooms" ? (
                                    <input
                                      className="w-100"
                                      type="text"
                                      value={editedText} // Controlled input with edited text
                                      onChange={(e) =>
                                        setEditedText(e.target.value)
                                      } // Update text as the user types
                                      onBlur={() =>
                                        handleTextSaveBedrooms("bedrooms")
                                      } // Save the text when the user leaves the input field
                                      autoFocus
                                    />
                                  ) : (
                                    <p
                                      className="mb-2"
                                      style={{
                                        fontSize: "24px",
                                        color: "black",
                                      }}
                                      onClick={() =>
                                        handleTextBedrooms(
                                          "bedrooms",
                                          select?.value[0]?.BedroomsTotal
                                        )
                                      }
                                    >
                                      {select.value[0]?.BedroomsTotal ? (
                                        <small>
                                          Bedrooms:{" "}
                                          {select.value[0]?.BedroomsTotal}
                                        </small>
                                      ) : (
                                        ""
                                      )}
                                    </p>
                                  )}

                                  {editingField === "bathroom" ? (
                                    <input
                                      className="w-100"
                                      type="text"
                                      value={editedText} // Controlled input with edited text
                                      onChange={(e) =>
                                        setEditedText(e.target.value)
                                      } // Update text as the user types
                                      onBlur={() =>
                                        handleTextSaveBathroom("bathroom")
                                      } // Save the text when the user leaves the input field
                                      autoFocus
                                    />
                                  ) : (
                                    <p
                                      className="mb-2"
                                      style={{
                                        fontSize: "24px",
                                        color: "black",
                                      }}
                                      onClick={() =>
                                        handleTextBathroom(
                                          "bathroom",
                                          select?.value[0]?.BathroomsFull
                                        )
                                      }
                                    >
                                      {select.value[0]?.BathroomsFull ? (
                                        <small>
                                          Bathrooms:{" "}
                                          {select.value[0]?.BathroomsFull ?? 0}/{" "}
                                          {select.value[0]?.BathroomsHalf ?? 0}
                                        </small>
                                      ) : (
                                        ""
                                      )}
                                    </p>
                                  )}

                                  {editingField === "squareFeet" ? (
                                    <input
                                      className="w-100"
                                      type="text"
                                      value={editedText} // Controlled input with edited text
                                      onChange={(e) =>
                                        setEditedText(e.target.value)
                                      } // Update text as the user types
                                      onBlur={() =>
                                        handleTextSaveSquareFeet("squareFeet")
                                      } // Save the text when the user leaves the input field
                                      autoFocus
                                    />
                                  ) : (
                                    <p
                                      className="mb-2"
                                      style={{
                                        fontSize: "24px",
                                        color: "black",
                                      }}
                                      onClick={() =>
                                        handleTextSquareFeet(
                                          "squareFeet",
                                          select?.value[0]?.BuildingAreaTotal
                                        )
                                      }
                                    >
                                      {select.value[0]?.BuildingAreaTotal ? (
                                        <small>
                                          Sqft:{" "}
                                          {select.value[0]?.BuildingAreaTotal}
                                        </small>
                                      ) : (
                                        ""
                                      )}
                                    </p>
                                  )}

                                  {editingField === "lotSize" ? (
                                    <input
                                      className="w-100"
                                      type="text"
                                      value={editedText} // Controlled input with edited text
                                      onChange={(e) =>
                                        setEditedText(e.target.value)
                                      } // Update text as the user types
                                      onBlur={() =>
                                        handleTextSaveLotSize("lotSize")
                                      } // Save the text when the user leaves the input field
                                      autoFocus
                                    />
                                  ) : (
                                    <p
                                      className="mb-2"
                                      style={{
                                        fontSize: "24px",
                                        color: "black",
                                      }}
                                      onClick={() =>
                                        handleTextLotSize(
                                          "lotSize",
                                          select?.value[0]?.LotSizeAcres
                                        )
                                      }
                                    >
                                      {select.value[0]?.LotSizeAcres ? (
                                        <small>
                                          Lot size :{" "}
                                          {select.value[0]?.LotSizeAcres}
                                        </small>
                                      ) : (
                                        ""
                                      )}
                                    </p>
                                  )}

                                  {editingField === "yearBuilt" ? (
                                    <input
                                      className="w-100"
                                      type="text"
                                      value={editedText} // Controlled input with edited text
                                      onChange={(e) =>
                                        setEditedText(e.target.value)
                                      } // Update text as the user types
                                      onBlur={() =>
                                        handleTextSaveYearBuilt("yearBuilt")
                                      } // Save the text when the user leaves the input field
                                      autoFocus
                                    />
                                  ) : (
                                    <p
                                      className="mb-2"
                                      style={{
                                        fontSize: "24px",
                                        color: "black",
                                      }}
                                      onClick={() =>
                                        handleTextYearBuilt(
                                          "yearBuilt",
                                          select?.value[0]?.YearBuilt
                                        )
                                      }
                                    >
                                      {select.value[0]?.YearBuilt ? (
                                        <small>
                                          Year Built :{" "}
                                          {select.value[0]?.YearBuilt}
                                        </small>
                                      ) : (
                                        ""
                                      )}
                                    </p>
                                  )}
                                </ul>
                              </div> */}

                              <div className="row mt-2">
                                <div className="col-md-3">
                                  <img
                                    src={
                                      uploadedContact ||
                                      (profile.image ? profile.image : "")
                                    } // Show uploaded logo or default avatar
                                    style={{
                                      width: "198px",
                                      cursor: "pointer",
                                    }} // Pointer cursor for better UX
                                    onClick={handleLogoClickContact} // Show file input on click
                                  />
                                  {/* Conditionally render file input on logo click */}
                                  {showContact && (
                                    <label className="documentlabelContact">
                                      <input
                                        id="REPC_real_estate_purchase_contract"
                                        type="file"
                                        onChange={handleLogoUploadContact}
                                      />
                                      <i class="h2 fi-edit opacity-80"></i>
                                    </label>
                                  )}
                                </div>
                                <div className="col-md-4 mt-4">
                                  <ul className="list-unstyled text-end">
                                    <div className="d-flex justify-content-end">
                                      {editingField === "firstName" ? (
                                        <input
                                          type="text"
                                          value={editedText} // Controlled input with edited text
                                          onChange={(e) =>
                                            setEditedText(e.target.value)
                                          } // Update text as the user types
                                          onBlur={() =>
                                            handleTextSaveListAgent("firstName")
                                          } // Save the text when the user leaves the input field
                                          autoFocus
                                        />
                                      ) : (
                                        <p
                                          className="text-white mb-0"
                                          onClick={() =>
                                            handleTextListAgent(
                                              "firstName",
                                              profile.firstName,
                                              profile.lastName
                                            )
                                          }
                                        >
                                          {profile && (
                                            <p className="mb-0">
                                              <b>Agent: </b>
                                              {profile.firstName}&nbsp;
                                            </p>
                                          )}
                                        </p>
                                      )}

                                      {editingField === "lastName" ? (
                                        <input
                                          type="text"
                                          value={editedText} // Controlled input with edited text
                                          onChange={(e) =>
                                            setEditedText(e.target.value)
                                          } // Update text as the user types
                                          onBlur={() =>
                                            handleTextSaveListAgent("lastName")
                                          } // Save the text when the user leaves the input field
                                          autoFocus
                                        />
                                      ) : (
                                        <p
                                          className="text-white mb-0"
                                          onClick={() =>
                                            handleTextListAgent(
                                              "lastName",
                                              profile.lastName
                                            )
                                          }
                                        >
                                          {profile && (
                                            <p className="mb-0">
                                              {" "}
                                              {profile.lastName}&nbsp;
                                            </p>
                                          )}
                                        </p>
                                      )}
                                    </div>

                                    {editingField === "listOffice" ? (
                                      <input
                                        type="text"
                                        value={editedText} // Controlled input with edited text
                                        onChange={(e) =>
                                          setEditedText(e.target.value)
                                        } // Update text as the user types
                                        onBlur={() =>
                                          handleTextSaveListOffice("listOffice")
                                        } // Save the text when the user leaves the input field
                                        autoFocus
                                      />
                                    ) : (
                                      <p
                                        className="text-white mb-0"
                                        onClick={() =>
                                          handleTextListOffice(
                                            "listOffice",
                                            profile.active_office
                                          )
                                        }
                                      >
                                        {profile && (
                                          <p className="mb-0">
                                            <b>Office: </b>
                                            {profile.active_office}
                                          </p>
                                        )}
                                      </p>
                                    )}

                                    {editingField === "officePhone" ? (
                                      <input
                                        type="text"
                                        value={editedText} // Controlled input with edited text
                                        onChange={(e) =>
                                          setEditedText(e.target.value)
                                        } // Update text as the user types
                                        onBlur={() =>
                                          handleTextSaveOfficePhone(
                                            "officePhone"
                                          )
                                        } // Save the text when the user leaves the input field
                                        autoFocus
                                      />
                                    ) : (
                                      <p
                                        className="text-white mb-0"
                                        onClick={() =>
                                          handleTextOfficePhone(
                                            "officePhone",
                                            profile.phone
                                          )
                                        }
                                      >
                                        {profile && (
                                          <p className="mb-0">
                                            <b>Phone: </b>
                                            {profile.phone}
                                          </p>
                                        )}
                                      </p>
                                    )}
                                  </ul>
                                </div>
                              </div>
                              <div className="row mt-5">
                                <div className="col-md-4">
                                  <img
                                    className="mb-2 rounded-0"
                                    src={uploadedLogo || Logo}
                                    style={{
                                      width: "200px",
                                      cursor: "pointer",
                                    }}
                                    onClick={handleLogoClick}
                                  />
                                  {showLogoInput && (
                                    <label className="documentPropertyLogo">
                                      <input
                                        id="REPC_real_estate_purchase_contract"
                                        type="file"
                                        onChange={handleLogoUpload}
                                      />
                                      <i class="h2 fi-edit opacity-80"></i>
                                    </label>
                                  )}
                                </div>

                                <div className="col-md-6">
                                  {editingField === "price" ? (
                                    <div className="text-center">
                                      <input
                                        type="text"
                                        value={editedText}
                                        onChange={(e) =>
                                          setEditedText(e.target.value)
                                        }
                                        onBlur={() =>
                                          handleTextSavePrice("price")
                                        }
                                        autoFocus
                                      />
                                    </div>
                                  ) : (
                                    <span
                                      className="text-white"
                                      style={{
                                        fontSize: "30px",
                                      }}
                                      onClick={() =>
                                        handleTextPrice(
                                          "price",
                                          select?.value[0]?.ListPrice
                                        )
                                      }
                                    >
                                      $ {select?.value[0]?.ListPrice ?? "0"}
                                    </span>
                                  )}

                                  {editingField === "published" ? (
                                    <input
                                      className="w-100"
                                      type="text"
                                      value={editedText} // Controlled input with edited text
                                      onChange={(e) =>
                                        setEditedText(e.target.value)
                                      } // Update text as the user types
                                      onBlur={() =>
                                        handleTextSavePublished("published")
                                      } // Save the text when the user leaves the input field
                                      autoFocus
                                    />
                                  ) : (
                                    <p
                                      className="text-white mb-0"
                                      onClick={() =>
                                        handleTextPublished(
                                          "published",
                                          select.value[0]?.SourceSystemName
                                        )
                                      }
                                    >
                                      {select?.value[0]?.SourceSystemName ? (
                                        <small>
                                          {select?.value[0]?.SourceSystemName}
                                        </small>
                                      ) : (
                                        <small>Default Text</small>
                                      )}
                                    </p>
                                  )}

                                  {editingField === "address" ? (
                                    <input
                                      className="w-100"
                                      type="text"
                                      value={editedText} // Controlled input with edited text
                                      onChange={(e) =>
                                        setEditedText(e.target.value)
                                      } // Update text as the user types
                                      onBlur={() => handleTextSave("address")}
                                      autoFocus
                                    />
                                  ) : (
                                    <span
                                      className="text-white mb-0"
                                      onClick={() =>
                                        handleTextClick(
                                          "address",
                                          select?.value[0]?.UnparsedAddress
                                        )
                                      }
                                    >
                                      {select?.value[0]?.UnparsedAddress ?? ""}
                                    </span>
                                  )}
                                  <div className="d-flex">
                                    {editingField === "companyCity" ? (
                                      <input
                                        className="w-100"
                                        type="text"
                                        value={editedText} // Controlled input with edited text
                                        onChange={(e) =>
                                          setEditedText(e.target.value)
                                        } // Update text as the user types
                                        onBlur={() =>
                                          handleTextSaveCompanyCity(
                                            "companyCity"
                                          )
                                        }
                                        autoFocus
                                      />
                                    ) : (
                                      <span
                                        className="text-white mb-0"
                                        onClick={() =>
                                          handleTextCompanyCity(
                                            "companyCity",
                                            select?.value[0]?.City
                                          )
                                        }
                                      >
                                        {select?.value[0]?.City ?? ""}, &nbsp;{" "}
                                      </span>
                                    )}

                                    {editingField === "companyState" ? (
                                      <input
                                        type="text"
                                        value={editedText} // Controlled input with edited text
                                        onChange={(e) =>
                                          setEditedText(e.target.value)
                                        } // Update text as the user types
                                        onBlur={() =>
                                          handleTextSaveCompanyState(
                                            "companyState"
                                          )
                                        }
                                        autoFocus
                                      />
                                    ) : (
                                      <p
                                        className="text-white mb-0"
                                        onClick={() =>
                                          handleTextCompanyState(
                                            "companyState",
                                            select?.value[0]?.StateOrProvince
                                          )
                                        }
                                      >
                                        {select?.value[0]?.StateOrProvince ??
                                          ""}{" "}
                                        &nbsp;{" "}
                                      </p>
                                    )}

                                    {editingField === "companyPostCode" ? (
                                      <input
                                        className="w-100"
                                        type="text"
                                        value={editedText} // Controlled input with edited text
                                        onChange={(e) =>
                                          setEditedText(e.target.value)
                                        } // Update text as the user types
                                        onBlur={() =>
                                          handleTextSaveCompanyPostCode(
                                            "companyPostCode"
                                          )
                                        } // Save the text when the user leaves the input field
                                        autoFocus
                                      />
                                    ) : (
                                      <p
                                        className="text-white mb-0"
                                        onClick={() =>
                                          handleTextCompanyPostCode(
                                            "companyPostCode",
                                            select?.value[0]?.PostalCode
                                          )
                                        }
                                      >
                                        {select?.value[0]?.PostalCode ?? ""}
                                      </p>
                                    )}
                                  </div>
                                </div>
                              </div>
                            </div>
                          )}
                        </div>

                        <div className="template_second">
                          <button
                            className="btn btn-secondary pull-right  ms-3"
                            onClick={() => handleNext("2")}
                          >
                            Inside Left Page
                          </button>
                          <button
                            className="btn btn-secondary pull-right  ms-3"
                            onClick={() => handleNext("1")}
                          >
                            Front Page
                          </button>

                          <div className="ms-5">
                            <PDFDownloadLink
                              document={
                                <Flyer
                                  select={select}
                                  platformData={uploadedLogo}
                                  profile={profile}
                                />
                              }
                              fileName={select.address || "Flyer.pdf"}
                            >
                              Download Pdf
                            </PDFDownloadLink>
                          </div>
                        </div>
                      </div>
                    </section>
                  )}
                </div>
              ) : (
                ""
              )}
            </div>
          ) : (
            <div className="col-md-8">
              <h3 className="text-white mb-4">Design your Property Labels.</h3>
              <div className="bg-light float-start w-100 rounded-3 mb-2 p-3">
                <p
                  id=""
                  dangerouslySetInnerHTML={{
                    __html: platformData?.flyerpagecontent,
                  }}
                />
                <hr className="mb-3" />
                <div className="mb-5">
                  <div className="select_template float-start w-100 mt-3 mb-5">
                    <label className="col-form-label" id="radio-level1">
                      Select Template:
                    </label>
                    <div className="d-flex">
                      <div className="form-check mb-0">
                        {/* Image 1 */}
                        <img
                          className={templateType === "type1" ? "active" : ""}
                          src={TypeOne}
                          alt="Type One"
                          style={{ cursor: "pointer" }}
                          onClick={() => handleType("type1")}
                        />
                      </div>
                    </div>
                  </div>

                  {templateType ? (
                    <div className="float-start w-100 mt-3">
                      <label className="col-form-label ">
                        Select Membership{" "}
                      </label>
                      <select
                        className="form-select mb-2"
                        name="membership"
                        onChange={(event) => setMembership(event.target.value)}
                        value={membership}
                      >
                        <option value="">Please Select Membership</option>
                        {commaSeparatedValuesmls_membership &&
                        commaSeparatedValuesmls_membership.length > 0
                          ? commaSeparatedValuesmls_membership.map(
                              (memberahip) => <option>{memberahip}</option>
                            )
                          : ""}
                      </select>
                      {membership ? (
                        <>
                          <label className="col-form-label">
                            Enter MLS Number:
                          </label>
                          {/* <h6 className="mt-2 mb-2"></h6> */}
                          <div className="col-md-12">
                            <input
                              className="form-control w-100"
                              id="text-input-onee"
                              type="text"
                              name="mlsnumber"
                              placeholder="Enter MLS number"
                              onChange={handleChangesearchmls}
                              value={searchmlsnumber.mlsnumber}
                            />
                          </div>

                          <div className="pull-left mt-4">
                            <button
                              className="btn btn-primary btn-sm order-lg-3"
                              onClick={MLSSearch}
                              disabled={isLoading}
                            >
                              {isLoading ? "Please wait" : "Search Print"}
                            </button>
                          </div>
                        </>
                      ) : (
                        ""
                      )}
                    </div>
                  ) : (
                    ""
                  )}
                </div>
                {/* <div className="col-md-4">
                    <button
                      className="btn btn-primary mt-4"
                      onClick={handleNonListed}
                    >
                      Non-Listed Property
                    </button>
                  </div> */}
              </div>
            </div>
          )}
        </div>
      </main>
    </div>
  );
};

export default PropertyLebel;

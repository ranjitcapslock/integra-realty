import React, { useState, useEffect } from "react";
import { NavLink, useParams } from "react-router-dom";
import user_service from "../service/user_service";
import Loader from "../../Pages/Loader/Loader.js";
import Toaster from "../../Pages/Toaster/Toaster";
import avtar from "../img/photo2.jpg";
import defaultpropertyimage from "../../Components/img/defaultpropertyimage.jpeg";
import moment from "moment-timezone";

const SingleListing = () => {
  const [loader, setLoader] = useState({ isActive: null });
  const { isActive } = loader;
  const [toaster, setToaster] = useState({
    types: null,
    isShow: null,
    toasterBody: "",
    message: "",
  });
  const { types, isShow, toasterBody, message } = toaster;
  const [summary, setSummary] = useState([]);
  const params = useParams();

  const SingleListingById = async () => {
    await user_service.listingsGetById(params.id).then((response) => {
      setLoader({ isActive: false });
      if (response) {
        setSummary(response.data);
      }
    });
  };
  useEffect(() => {
    window.scrollTo(0, 0);
    // setLoader({ isActive: true })
    SingleListingById(params.id);
  }, []);
  const propertypic = (e) => {
    e.target.src = defaultpropertyimage;
  };


  return (
    <div className="bg-secondary float-left w-100 pt-4 mb-4">
      <Loader isActive={isActive} />
      {isShow && (
        <Toaster
          types={types}
          isShow={isShow}
          toasterBody={toasterBody}
          message={message}
        />
      )}
      <main className="">
        {summary.propertyFrom === "mls" ? (
          <>
      
            <section className="mb-2">
              {/* <nav className="mb-3 pt-md-3" aria-label="breadcrumb">
                                <ol className="breadcrumb">
                                    <li className="breadcrumb-item"><a href="#">Home</a></li>
                                    <li className="breadcrumb-item active" aria-current="page">Property</li>
                                </ol>
                            </nav> */}
              <h3 className="text-white mb-0">{summary.streetAddress}</h3>
              <p className="text-white mb-2 pb-1 fs-lg">{summary.state}</p>
            </section>

            <section className="mb-0 pb-3" data-simplebar>
              <div className="row g-2 g-md-3 gallery" data-thumbnails="true">
                <div className="col-md-8 singleListing">
                  <div className="bg-light border rounded-3 p-3">
                    <img
                      className="img-fluid"
                      src={
                        summary.image && summary.image !== "image"
                          ? summary.image
                          : defaultpropertyimage
                      }
                      alt="Property"
                      onError={(e) => propertypic(e)}
                    />
                    <div className="col-md-12 imageProperty">
                      {/* {console.log(JSON.parse(summary.additionaldataMLS).value)} */}
                      {JSON.parse(summary.additionaldataMLS).media
                        ? JSON.parse(summary.additionaldataMLS).media.length > 0
                          ? JSON.parse(summary.additionaldataMLS).media.map(
                              (item) => {
                                return (
                                  <>
                                    <img
                                      src={item.MediaURL}
                                      alt="Gallery thumbnail"
                                    />
                                  </>
                                );
                              }
                            )
                          : ""
                        : ""}
                    </div>

                    <section className="">
                      <div className="">
                        <div className="col-md-12 mb-md-0 mt-3">
                          {/* {console.log(JSON.parse(summary.additionaldataMLS))} */}
                          <h2 className="h3 border-bottom">
                            {parseFloat(summary.price).toLocaleString("en-US")}
                            <span className="d-inline-block ms-1 fs-base fw-normal text-body">
                              USD
                            </span>
                          </h2>
                          <h3 className="h4">Property Details</h3>
                          <div className="col-md-12 pb-md-3 d-flex">
                            <ul className="list-unstyled col-md-6">
                              <li>
                                <b>Category: </b>
                                {summary.propertyType}
                              </li>
                              <li>
                                <b>ListingsAgent: </b>
                                {summary.listingsAgent}
                              </li>
                              <li>
                                <b>Listing Agent ID:: </b>
                                {
                                  JSON.parse(summary.additionaldataMLS).value[0]
                                    .ListAgentKey
                                }
                              </li>
                              <li>
                                <b>Area Location: </b>
                                {summary.areaLocation}
                              </li>
                              <li>
                                <b>County: </b>
                                {summary.country}
                              </li>
                              <li>
                                <b>Subdivision: </b>
                                {summary.subDivision}
                              </li>
                              <li>
                                <b>List Date: </b>
                                {summary?.listDate
                                  ? moment
                                      .tz(summary?.listDate, "US/Mountain")
                                      .format("M/D/YYYY")
                                  : ""}
                              </li>
                            </ul>
                            <ul className="list-unstyled col-md-6">
                              <li>
                                <b>MLS Number: </b>#{summary.mlsNumber}
                              </li>
                              <li>
                                <b>AssociateName: </b>
                                {summary.associateName}
                              </li>
                              <li>
                                <b>MLS Office ID: </b>
                                {
                                  JSON.parse(summary.additionaldataMLS).value[0]
                                    .ListOfficeKey
                                }
                              </li>
                              <li>
                                <b>Status: </b>
                                {summary.status}
                              </li>
                              <li className="me-3 pe-3 border-end">
                                <b className="me-1">Bedrooms:</b>
                                {
                                  JSON.parse(summary.additionaldataMLS).value[0]
                                    .BedroomsTotal
                                }
                              </li>
                              <li className="me-3 pe-3 border-end">
                                <b className="me-1">
                                  Bathrooms:{" "}
                                  {JSON.parse(summary.additionaldataMLS)
                                    .value[0].BathroomsFull ?? 0}{" "}
                                  /{" "}
                                  {JSON.parse(summary.additionaldataMLS)
                                    .value[0].BathroomsHalf ?? 0}
                                </b>
                              </li>

                              <li className="me-3 pe-3 border-end">
                                <b className="me-1">Garage: {summary.garage}</b>
                              </li>
                              <li className="me-3 pe-3 border-end">
                                <b className="me-1">
                                  {" "}
                                  Square Ft:{" "}
                                  {
                                    JSON.parse(summary.additionaldataMLS)
                                      .value[0].BuildingAreaTotal
                                  }
                                </b>
                              </li>
                              <li className="me-3 pe-3 border-end">
                                <b className="me-1">
                                  Lot Size:{" "}
                                  {
                                    JSON.parse(summary.additionaldataMLS)
                                      .value[0].LotSizeAcres
                                  }{" "}
                                  Acres
                                </b>
                              </li>
                            </ul>
                          </div>
                          <div className="mb-4 pb-md-3">
                            <h3 className="h4">Overview</h3>
                            <p className="mb-1">
                              {
                                JSON.parse(summary.additionaldataMLS).value[0]
                                  .PublicRemarks
                              }
                            </p>
                          </div>

                          <div className="pb-lg-2 py-4 border-top">
                            <ul className="d-flex list-unstyled fs-sm">
                              <li className="me-3 pe-3 border-end">
                                Published by:{" "}
                                <b>
                                  {
                                    JSON.parse(summary.additionaldataMLS)
                                      .value[0].SourceSystemName
                                  }
                                </b>
                              </li>
                              <li className="me-3 pe-3 border-end">
                                Published date:
                                <b>
                                  {JSON.parse(summary.additionaldataMLS)
                                    .value[0].OnMarketDate
                                    ? moment
                                        .tz(
                                          JSON.parse(summary.additionaldataMLS)
                                            .value[0].OnMarketDate,
                                          "US/Mountain"
                                        )
                                        .format("M/D/YYYY")
                                    : ""}
                                </b>
                              </li>
                            </ul>
                          </div>
                        </div>
                      </div>
                    </section>
                  </div>
                </div>
                <aside className="col-lg-4 col-md-5 ms-lg-auto pb-1">
                  <div className="card shadow-sm">
                    <div className="card-body">
                      <div className="d-flex align-items-start justify-content-between">
                        <a
                          className="text-decoration-none"
                          href="real-estate-vendor-properties.html"
                        >
                          {/* <img className="rounded-circle mb-2" src={avtar} width="60" alt="Avatar" /> */}
                          <h5 className="mb-1">{summary.listingsAgent}</h5>
                          {/* <div className="mb-1">
                            <span className="star-rating">
                              <i className="star-rating-icon fi-star-filled active"></i>
                              <i className="-rating-icon fi-star-filled active"></i>
                              <i className="star-rating-icon fi-star-filled active"></i>
                              <i className="star-rating-icon fi-star-filled active"></i>
                              <i className="star-rating-icon fi-star-filled active"></i>
                            </span>
                            <span className="ms-1 fs-sm text-muted">
                              (45 reviews)
                            </span>
                          </div> */}
                          <p className="text-body">
                            {
                              JSON.parse(summary.additionaldataMLS).value[0]
                                .ListOfficeName
                            }
                          </p>
                        </a>
                      </div>
                      <ul className="list-unstyled border-bottom mb-4 pb-4 p-0">
                        <li>
                          <a
                            className="nav-link fw-normal p-0"
                            href="tel:3025550107"
                          >
                            <i className="fi-phone mt-n1 me-2 align-middle opacity-60"></i>
                            {
                              JSON.parse(summary.additionaldataMLS).value[0]
                                .ListAgentOfficePhone
                            }
                          </a>
                        </li>
                      </ul>
                    </div>
                  </div>
                </aside>
              </div>
            </section>
          </>
        ) : (
          <div className="col-md-10 bg-light border rounded-3 p-3">
            <section className="mt-2">

              <h3 className="mb-0">{summary.streetAddress}</h3>
              <p className="mb-2 pb-1 fs-lg">{summary.state}</p>
            </section>

            <section className="overflow-auto mb-0 pb-3" data-simplebar>
              <div className="row g-2 g-md-3 gallery" data-thumbnails="true">
                <div className="col-8 singleListing">
                  <a className="gallery-item rounded rounded-md-3" href="#">
                    <img
                      className="img-fluid"
                      src={
                        summary.image && summary.image !== "image"
                          ? summary.image
                          : defaultpropertyimage
                      }
                      alt="Property"
                      onError={(e) => propertypic(e)}
                    />
                  </a>
                </div>
              </div>
            </section>
            <section className="pb-1">
              <div className="row">
                <div className="col-md-12 mb-md-0">
                  {
                   summary.price ? 
                  <h2 className="h3 border-bottom">
                    {/* {parseFloat(summary.price).toLocaleString("en-US")} */}
                    {
                      summary.price
                    }
                    <span className="d-inline-block ms-1 fs-base fw-normal text-body">
                      USD
                    </span>
                  </h2>
                  :""
                  }

                  <h3 className="h4">Property Details</h3>
                  <div className="col-md-12 pb-md-3 d-flex">
                    <ul className="list-unstyled col-md-6">
                      {
                        summary.propertyType ?
                      <li>
                        <b>Category: </b>
                        {summary.propertyType}
                      </li>
                     :""
                      }
                      {
                        summary.areaLocation ?
                      <li>
                        <b>Area Location: </b>
                        {summary.areaLocation}
                      </li>
                      :""
                      }

                      {
                        summary.country ?
                      <li>
                        <b>County: </b>
                        {summary.country}
                      </li>
                      :""
                      }

                      {
                      summary.subDivision ? 
                      <li>
                        <b>Subdivision: </b>
                        {summary.subDivision}
                      </li>
                        :""
                      }

                      {

                      }

                      {
                        summary?.listDate ?
                      <li>
                        <b>List Date: </b>
                        {moment
                              .tz(summary?.listDate, "US/Mountain")
                              .format("M/D/YYYY")
                         }
                      </li>
                      :""
                      }
                    </ul>


                    <ul className="list-unstyled col-md-6">
                      {
                        summary.associateName ? 
                      <li>
                        <b>AssociateName: </b>
                        {summary.associateName}
                      </li>
                      :""
                      }

                      {
                        summary.status ?
                      <li>
                        <b>Status: </b>
                        {summary.status}
                      </li>
                      :""

                      }

                      {
                        summary.garage ?
                      <li>
                        <b>Garage: </b>
                        {summary.garage}
                      </li>
                      :""

                      }

                      {
                        summary.bed ?
                      <li>
                        <b>Bedrooms: </b>
                        {summary.bed}
                      </li>
                      :""
                      }
                    </ul>
                  </div>
                </div>
              </div>
            </section>
          </div>
        )}
      </main>
    </div>
  );
};

export default SingleListing;

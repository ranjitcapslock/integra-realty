import React, { useState, useRef, useEffect } from "react";
import { NavLink, useNavigate, useParams } from "react-router-dom";
import user_service from "../service/user_service.js";
import jwt from "jwt-decode";
import _ from "lodash";
import Loader from "../../Pages/Loader/Loader.js";
import Toaster from "../../Pages/Toaster/Toaster.js";
import defaultpropertyimage from "../../Components/img/defaultpropertyimage.jpeg";
import NonFlyer1 from "../img/NonFlyer1.png";
import NonFlyer2 from "../img/NonFlyer2.png";
import NonFlyer3 from "../img/NonFlyer3.png";
import Logo from "../img/logo.png";
import {
  Document,
  Page,
  Text,
  Image,
  StyleSheet,
  PDFDownloadLink,
  View,
} from "@react-pdf/renderer";

const NonListedPrint = () => {
  const [loader, setLoader] = useState({ isActive: null });
  const { isActive } = loader;
  const [toaster, setToaster] = useState({
    types: null,
    isShow: null,
    toasterBody: "",
    message: "",
  });
  const { types, isShow, toasterBody, message } = toaster;

  const [searchmlsnumber, setSearchmlsnumber] = useState({ mlsNumber: "" });
  const componentRef = useRef();

  useEffect(() => {
    window.scrollTo(0, 0);
    ContactGetById(jwt(localStorage.getItem("auth")).id);
  }, []);

  const [templateType, setTemplateType] = useState("");
  const [template, setTemplate] = useState(false);

  const [isLoading, setIsLoading] = useState(false);

  const handleType = (input) => {
    const templateType = input.target ? input.target.value : input;
    setTemplateType(templateType);
    setTemplate(true);
  };

  const [getContact, setGetContact] = useState("");

  const ContactGetById = async () => {
    await user_service
      .contactGetById(jwt(localStorage.getItem("auth")).id)
      .then((response) => {
        if (response) {
          setGetContact(response.data);
        }
      });
  };

  const [selectedImage, setSelectedImage] = useState(null);
  const [uploadedImages, setUploadedImages] = useState([]);

  // Handle image click for uploading
  const handleImageClick = (imageIndex) => {
    setSelectedImage(imageIndex);
  };

  const handleImageUpload = (e, imageIndex = 0) => {
    const file = e.target.files[0];
    if (file) {
      const reader = new FileReader();
      reader.onload = (upload) => {
        setUploadedImages((prev) => {
          const newImages = [...prev];
          newImages[imageIndex] = upload.target.result;
          return newImages;
        });
      };
      reader.readAsDataURL(file);
    }
    setSelectedImage(null);
  };

  const [select, setSelect] = useState({
    price: "X00,00",
    address: "2024 Demo St",
    bed: "0",
    bath: "0",
    sqft: "X,000 Sq.Ft.",
    lotSize: "0",
    yearBuilt: "2024",
    publicRemarks:
      "Enter a brief description of this property for prospective buyers",
    published: "UtahRealEstate.com",
    companyCity: "American Fork",
    companyState: "UT",
    companyPostCode: "84003",
    companyAddress: "",
  });

  const [editingField, setEditingField] = useState(null);
  const [editedText, setEditedText] = useState("");

  const handleTextClick = (field, currentValue) => {
    setEditingField(field);
    setEditedText(currentValue);
  };

  const handleTextSave = () => {
    setSelect((prevSelect) => ({
      ...prevSelect,
      [editingField]: editedText,
    }));
    setEditingField(null);
  };
  const handleTextSaveSecond = () => {
    setSelect((prevSelect) => ({
      ...prevSelect,
      [editingField]: editedText,
    }));
    setEditingField(null);
  };

  const handleTextListAgent = (field, currentValue) => {
    setEditingField(field); // Set the field being edited
    setEditedText(currentValue); // Set the current text value in the input
  };

  // Handle saving the edited text for the list agent
  const handleTextSaveListAgent = (field) => {
    setProfile((prevProfile) => ({
      ...prevProfile,
      [field]: editedText, // Dynamically update the appropriate field (firstName or lastName)
    }));
    setEditingField(null); // Close the input field after saving
  };

  const handleTextListOffice = (field, currentValue) => {
    setEditingField(field); // Set the field being edited
    setEditedText(currentValue); // Pre-fill input with current value (address)
  };

  const handleTextSaveListOffice = (field) => {
    // Update the selected value locally after editing
    setProfile((prevProfile) => ({
      ...prevProfile,
      active_office: editedText, // Dynamically update the appropriate field (firstName or lastName)
    }));

    setEditingField(null); // Close the input field after saving
  };

  const handleTextOfficePhone = (field, currentValue) => {
    setEditingField(field); // Set the field being edited
    setEditedText(currentValue); // Pre-fill input with current value (address)
  };

  const handleTextSaveOfficePhone = (field) => {
    // Update the selected value locally after editing
    setProfile((prevProfile) => ({
      ...prevProfile,
      phone: editedText, // Dynamically update the appropriate field (firstName or lastName)
    }));

    setEditingField(null); // Close the input field after saving
  };

  const [platformData, setPlatFormData] = useState({});
  const [uploadedLogo, setUploadedLogo] = useState(null);
  const [showLogoInput, setShowLogoInput] = useState(false); // State to show or hide input field
  const [profile, setProfile] = useState({});

  const [uploadedContact, setUploadedContact] = useState(null);
  const [showContact, setShowContact] = useState(false);

  const PlatformdetailsGet = async () => {
    await user_service.platformdetailsGet().then((response) => {
      if (response) {
        setPlatFormData(response.data.data[0]);
      }
    });
  };

  const API_BASE_URL = "https://api.brokeragentbase.com";
  const profileGetAll = async () => {
    if (localStorage.getItem("auth")) {
      try {
        const response = await user_service.profileGet(
          jwt(localStorage.getItem("auth")).id
        );

        if (response && response.data) {
          const profileData = response.data;

          // Modify image URL to use proxy for the profile image
          if (profileData.image) {
            profileData.image = `${API_BASE_URL}/proxy-image?url=${encodeURIComponent(
              profileData.image
            )}`;
          }

          // Set the modified profile data with the proxied image URL
          setProfile(profileData);
        }
      } catch (error) {
        console.error("Error fetching profile data:", error);
      }
    }
  };

  useEffect(() => {
    window.scrollTo(0, 0);
    PlatformdetailsGet();
    profileGetAll();
  }, []);

  // Handle logo upload

  const handleLogoUploadContact = (e) => {
    const file = e.target.files[0];
    if (file) {
      const reader = new FileReader();
      reader.onload = (upload) => {
        setUploadedContact(upload.target.result); // Store the uploaded logo
        setShowContact(false); // Hide the input after upload
        // Update platformData with the new logo while preserving the rest of the data
        setProfile((prevData) => ({
          ...prevData,
          image: upload.target.result || prevData.image, // Update the logo in platformData
        }));
      };
      reader.readAsDataURL(file); // Read the file as a data URL for image preview
    }
  };

  const handleLogoUpload = (e) => {
    const file = e.target.files[0];
    if (file) {
      const reader = new FileReader();
      reader.onload = (upload) => {
        setUploadedLogo(upload.target.result); // Store the uploaded logo
        setShowLogoInput(false); // Hide the input after upload
        // // Update platformData with the new logo while preserving the rest of the data
        // setPlatFormData((prevData) => ({
        //   ...prevData,
        //   platform_logo: upload.target.result, // Update the logo in platformData
        // }));
      };
      reader.readAsDataURL(file); // Read the file as a data URL for image preview
    }
  };

  // Handle logo click to show the input
  const handleLogoClick = () => {
    setShowLogoInput(true); // Show the file input when the logo is clicked
  };

  const handleLogoClickContact = () => {
    setShowContact(true); // Show the file input when the logo is clicked
  };

  const styles = StyleSheet.create({
    page: {
      backgroundColor: "#b4a3a359",
      padding: 10,
    },
    rowContainer: {
      display: "flex",
      flexDirection: "row",
      justifyContent: "space-between",
      width: "100%",
    },
    logoCol: {
      width: "48%", // Slightly smaller width for the logo column
    },
    textCol: {
      width: "48%",
      display: "flex",
      justifyContent: "flex-end",
      textAlign: "right",
    },
    officeLogo: {
      width: 100,
      objectFit: "contain",
      marginBottom: 10,
    },
    priceText: {
      fontSize: 14,
      fontWeight: "bold",
      marginBottom: 5,
    },
    addressText: {
      fontSize: 14,
      marginTop: 5,
      marginBottom: 5,
      fontWeight: "bold",
    },

    overviewHeader: {
      fontSize: 20,
      fontWeight: "bold",
      color: "#000",
      textAlign: "center",
      marginBottom: 10,
    },
    contactInfo: {
      flexDirection: "row",
      justifyContent: "space-between",
      marginBottom: 20,
    },
    contactText: {
      fontSize: 12,
      color: "#fff",
    },
    mainpic: {
      width: "100%",
      height: 200,
      objectFit: "cover",
      marginBottom: 10,
    },
    longbox: {
      width: "100%",
      backgroundColor: "#2e3339",
      opacity: 0.9,
      padding: 10,
      borderRadius: 5,
      marginBottom: 5,
    },

    longboxSubHeader: {
      fontSize: 12,
      textAlign: "center",
      color: "#fff",
      marginBottom: 5,
    },

    longboxSubHeaderNew: {
      fontSize: 10,
      textAlign: "center",
      color: "#fff",
      marginBottom: 5,
    },
    price: {
      fontSize: 24,
      fontWeight: "bold",
      textAlign: "center",
      color: "#917462",
      marginBottom: 5,
    },

    propertyDetails: {
      flexDirection: "row",
      justifyContent: "space-between",
      marginBottom: 5,
    },
    propertyDetailsSecondRow: {
      padding: 5,
      flexDirection: "row",
      justifyContent: "space-between",
      marginBottom: 5,
    },
    detailItem: {
      fontSize: 12,
      color: "#917462",
      textAlign: "center",
    },

    detailOverview: {
      fontSize: 12,
      color: "#917462",
      textAlign: "justify",
      marginTop: 10,
    },
    imageGrid: {
      flexDirection: "row",
      flexWrap: "wrap",
      justifyContent: "space-between",
      marginBottom: 5,
    },
    imageItem: {
      width: "32%",
      height: 150,
      objectFit: "cover",
      marginBottom: 10,
    },

    addressBox: {
      width: "100%",
      backgroundColor: getComputedStyle(
        document.documentElement
      ).getPropertyValue("--branding-footer-color"), // Dynamically get CSS variable value
      padding: 10,
      borderRadius: 5,
      display: "flex",
      flexDirection: "row",
      justifyContent: "space-between",
    },

    // addressBox: {
    //   width: "100%",
    //   backgroundColor: "#917462",
    //   padding: 10,
    //   borderRadius: 5,
    //   // marginBottom: 5,
    // },

    officeSection: {
      width: "50%",
      alignItems: "flex-start",
      marginBottom: 5,
    },
    officeAddress: {
      width: "50%",
      alignItems: "flex-end",
      marginBottom: 5,
    },

    officeContactImage: {
      width: 60,
      height: 60,
      borderRadius: 50,
      marginLeft: 10,
    },
    // officeContactImage: {
    //   width: 100,
    //   height: 50,
    // },
    officeDetails: {
      width: "100%",
    },
    officeText: {
      fontSize: 12,
      color: "#ffffff",
      // textTransform: "uppercase",
      marginBottom: 2,
    },

    officeAddressTest: {
      fontSize: 12,
      color: "#000000",
      textTransform: "lowercase",
      marginBottom: 2,
    },

    contactHeading: {
      fontSize: 18,
      fontWeight: "bold",
      color: "#00000",
      textAlign: "center",
      marginBottom: 10,
    },

    propertyHeadingFirst: {
      marginTop: 5,
      fontSize: 18,
      fontWeight: "bold",
      color: "#00000",
      textAlign: "center",
      marginBottom: 10,
    },
  });

  const Flyer = ({ select, uploadedImages, platformData, profile }) => {
    const placeholderImage = defaultpropertyimage;
    return (
      <Document>
        <Page size="A4" style={styles.page}>
          <View style={styles.rowContainer}>
            {/* First Column: Platform Logo */}
            <View style={styles.logoCol}>
              <Image
                style={styles.officeLogo}
                src={platformData ?? Logo} // Compressed platform logo
              />
            </View>

            {/* Second Column: Price and Address */}
            <View style={styles.textCol}>
              <Text style={styles.priceText}>
                Listed at ${select.price || "X00,00"}
              </Text>
              <Text style={styles.addressText}>
                {select.address || "2024 Demo St"}
              </Text>
            </View>
          </View>

          <Image
            style={styles.mainpic}
            src={uploadedImages[0] || placeholderImage}
          />

          <Text style={styles.propertyHeadingFirst}>PROPERTY DETAILS</Text>
          <View style={styles.longbox}>
            <View style={styles.propertyDetails}>
              <View style={styles.detailItem}>
                <Text style={styles.longboxSubHeader}>Bedrooms</Text>
                <Text style={styles.longboxSubHeaderNew}>
                  {select.bed || "0"}
                </Text>
              </View>
              <View style={styles.detailItem}>
                <Text style={styles.longboxSubHeader}>Bathrooms</Text>
                <Text style={styles.longboxSubHeaderNew}>
                  {select.bath || "0"}
                </Text>
              </View>
              <View style={styles.detailItem}>
                <Text style={styles.longboxSubHeader}>Sqft</Text>
                <Text style={styles.longboxSubHeaderNew}>
                  {select.sqft || "X,000 Sq.Ft."}
                </Text>
              </View>
              <View style={styles.detailItem}>
                <Text style={styles.longboxSubHeader}>Lot Size</Text>
                <Text style={styles.longboxSubHeaderNew}>
                  {select.lotSize || ""}
                </Text>
              </View>
              <View style={styles.detailItem}>
                <Text style={styles.longboxSubHeader}>Year Built</Text>
                <Text style={styles.longboxSubHeaderNew}>
                  {select.yearBuilt || "2024"}
                </Text>
              </View>
            </View>

            <View style={styles.detailOverview}>
              <Text style={styles.longboxSubHeaderNew}>
                {select.publicRemarks ||
                  "Enter a brief description of this property for prospective buyers."}
              </Text>
            </View>
          </View>

          <View style={styles.imageGrid}>
            {(Array.isArray(uploadedImages) && uploadedImages.length > 1
              ? uploadedImages.slice(1, 4)
              : Array(3).fill(placeholderImage)
            ).map((mediaItem, mediaIndex) => (
              <Image
                style={styles.imageItem}
                src={mediaItem}
                key={mediaIndex}
              />
            ))}
          </View>

          <Text style={styles.contactHeading}>CONTACT US NOW</Text>
          <View style={styles.addressBox}>
            <View style={styles.officeSection}>
              <View>
                <Image
                  style={styles.officeContactImage}
                  src={profile.image} // Compressed profile image
                />
              </View>
              <View style={styles.officeDetails}>
                <Text style={styles.officeText}>
                  Agent: {profile?.firstName} {profile?.lastName}
                </Text>
                <Text style={styles.officeText}>
                  Office: {profile?.active_office}
                </Text>
                <Text style={styles.officeText}>Phone: {profile?.phone}</Text>
              </View>
            </View>
            <View style={styles.officeAddress}>
              <Text style={styles.officeText}>
                {select.published || "UtahRealEstate.com"}
              </Text>
              <Text style={styles.officeText}>
                {select.address || "2024 Demo St"}
              </Text>
              <Text style={styles.officeText}>
                {select.companyCity || "American Fork"},&nbsp;
                {select.companyState || "UT"}&nbsp;
                {select.companyPostCode || "84003"}
              </Text>
            </View>
          </View>
        </Page>
      </Document>
    );
  };

  const stylesSecond = StyleSheet.create({
    page: {
      backgroundColor: "#b4a3a359",
      padding: 10,
    },

    brownbox: {
      padding: 5,
      marginBottom: 1,
      borderRadius: 5,
      position: "relative",
      overflow: "hidden",
      zIndex: 1,
    },

    rowContainer: {
      display: "flex",
      flexDirection: "row",
      justifyContent: "space-between",
      width: "100%",
    },
    logoCol: {
      width: "48%", // Slightly smaller width for the logo column
    },
    textCol: {
      width: "48%",
      marginTop: 0,
      padding: 0,
      display: "flex",
      justifyContent: "flex-end",
      textAlign: "right",
    },
    officeLogo: {
      width: 100,
      objectFit: "contain",
      marginBottom: 10,
    },
    priceText: {
      fontSize: 14,
      fontWeight: 700,
      color: "#000",
      marginBottom: 10,
    },
    // addressText: {
    //   fontSize: 14,
    //   marginTop: 5,
    //   marginBottom: 5,
    //   fontWeight: "bold",
    // },
    overviewHeader: {
      fontSize: 22,
      fontWeight: "bold",
      color: "#000",
      textAlign: "center",
      marginBottom: 15,
      transform: [{ rotate: "2deg" }],
      zIndex: 10,
    },
    contactInfo: {
      flexDirection: "row",
      justifyContent: "space-between",
      marginBottom: 20,
    },
    contactText: {
      fontSize: 12,
      color: "#fff",
    },
    mainpic: {
      width: "100%",
      height: 200,
      objectFit: "cover",

      transform: [{ rotate: "1deg" }],
      zIndex: 2,
    },

    price: {
      fontSize: 24,
      fontWeight: "bold",
      textAlign: "center",
      color: "#f5a623",
      marginBottom: 5,
      transform: [{ rotate: "-1deg" }],
    },
    longbox: {
      width: "100%",
      backgroundColor: "#1a1d1f",
      opacity: 0.95,
      padding: 15,
      borderRadius: 10,
      marginBottom: 5,
      flexDirection: "row",
      justifyContent: "space-between",
      zIndex: 5,
      paddingTop: 10,
    },
    description: {
      flex: 3, // 75% width (3 out of 4 parts)
      paddingRight: 10,
    },
    propertyData: {
      flex: 1, // 25% width (1 out of 4 parts)
      paddingLeft: 10,
    },
    contactHeading: {
      fontSize: 18,
      fontWeight: "bold",
      color: "#dc936b",
      marginBottom: 10,
    },
    longboxSubHeader: {
      fontSize: 12,
      color: "#e0e0e0",
      marginBottom: 5,
    },
    longboxSubHeaderNew: {
      fontSize: 10,
      color: "#fff",
      marginBottom: 5,
    },
    propertyDetails: {
      flexDirection: "column", // Ensure items are stacked vertically
      fontSize: 10,
      color: "#fff",
      textAlign: "justify",
      marginTop: 10,
    },
    detailItem: {
      fontSize: 10,
      textAlign: "center",
      marginBottom: 10,
      color: "#fff",
    },
    detailOverview: {
      fontSize: 10,
      color: "#fff",
      textAlign: "justify",
      marginTop: 10,
    },
    imageGrid: {
      flexDirection: "row",
      flexWrap: "wrap",
      justifyContent: "space-between",
      marginBottom: 5,
      position: "relative",
      marginTop: -30,
      paddingLeft: 20,
      paddingRight: 20,
    },
    imageItem: {
      width: "32%",
      height: 150,
      objectFit: "cover",
      marginBottom: 10,
      zIndex: 1,
    },
    addressBox: {
      width: "100%",
      backgroundColor: getComputedStyle(
        document.documentElement
      ).getPropertyValue("--branding-footer-color"), // Dynamically get CSS variable value
      marginTop: 10,
      padding: 20,
      borderRadius: 16,
      flexDirection: "row",
      justifyContent: "space-between",
      position: "relative",
      zIndex: 5,
      transform: [{ scaleY: 1.05 }],
    },
    addressText: {
      fontSize: 16,
      color: "#fff",
      textAlign: "center",
      transform: [{ rotate: "-2deg" }],
    },
    officeSection: {
      width: "50%",
      alignItems: "flex-start",
      marginBottom: 5,
    },
    officeAddress: {
      width: "50%",
      alignItems: "flex-end",
      marginBottom: 5,
    },
    officeLogo: {
      width: 100,
      height: 50,
      marginBottom: 10,
    },
    officeDetails: {
      width: "100%",
    },
    officeText: {
      fontSize: 14,
      color: "#fff",
      marginBottom: 2,
      zIndex: 10,
    },
    officeAddressTest: {
      fontSize: 14,
      color: "#fff",
      marginBottom: 2,
    },
    propertyHeading: {
      padding: 10,
      fontSize: 20,
      fontWeight: "bold",
      color: "#fff",
      textAlign: "center",
      marginBottom: 15,
      transform: [{ scaleY: 1.2 }],
    },
    contactHeading: {
      fontSize: 16,
      fontWeight: "bold",
      color: "#fff",
      textAlign: "center",
      marginBottom: 15,
      marginTop: 15,
      transform: [{ rotate: "2deg" }],
    },
    contactSecond: {
      fontSize: 16,
      fontWeight: "bold",
      color: "#cca345",
      textAlign: "center",
      marginBottom: 15,
      transform: [{ rotate: "2deg" }],
    },
  });

  const FlyerAlternative = ({
    select,
    uploadedImages,
    platformData,
    profile,
  }) => {
    const placeholderImage = defaultpropertyimage;

    return (
      <Document>
        <Page size="A4" style={stylesSecond.page}>
          {/* Header Section */}
          <View style={stylesSecond.rowContainer}>
            <View style={stylesSecond.logoCol}>
              <Image
                style={stylesSecond.officeLogo}
                src={platformData || Logo}
              />
            </View>
            <View style={stylesSecond.textCol}>
              <Text style={stylesSecond.priceText}>
                PRICE AT ${select?.price || "X00,000"}
              </Text>
            </View>
          </View>

          {/* Main Image and Grid Section */}
          <View style={stylesSecond.brownbox}>
            <Image
              style={stylesSecond.mainpic}
              src={uploadedImages[0] || placeholderImage}
            />
            <View style={stylesSecond.imageGrid}>
              {(Array.isArray(uploadedImages) && uploadedImages.length > 1
                ? uploadedImages.slice(1, 4)
                : Array(3).fill(placeholderImage)
              ).map((image, index) => (
                <Image key={index} style={stylesSecond.imageItem} src={image} />
              ))}
            </View>
          </View>

          {/* Property Description */}
          <View style={stylesSecond.longbox}>
            <View style={stylesSecond.description}>
              <Text style={stylesSecond.contactHeading}>Description</Text>
              <Text style={stylesSecond.longboxSubHeaderNew}>
                {select?.publicRemarks ||
                  "Enter a brief description of this property for prospective buyers."}
              </Text>
            </View>
            <View style={stylesSecond.propertyData}>
              <Text style={stylesSecond.contactHeading}>Property Details</Text>
              <Text style={stylesSecond.detailItem}>
                Bedrooms: {select?.bed || "0"}
              </Text>
              <Text style={stylesSecond.detailItem}>
                Bathrooms: {select?.bath || "0"}
              </Text>
              <Text style={stylesSecond.detailItem}>
                Lot Size: {select?.lotSize || "N/A"}
              </Text>
              <Text style={stylesSecond.detailItem}>
                Sqft: {select?.sqft || "X,000 Sq.Ft."}
              </Text>
              <Text style={stylesSecond.detailItem}>
                Year Built: {select?.yearBuilt || "N/A"}
              </Text>
            </View>
          </View>

          {/* Contact and Address Section */}
          <View style={stylesSecond.addressBox}>
            <View style={stylesSecond.officeSection}>
              <Text style={stylesSecond.officeText}>
                {select?.published || "UtahRealEstate.com"}
              </Text>
              <Text style={stylesSecond.officeText}>
                {select?.address || "2024 Demo St"}
              </Text>
              <Text style={stylesSecond.officeText}>
                {select?.companyCity || "American Fork"},&nbsp;
                {select?.companyState || "UT"}&nbsp;
                {select?.companyPostCode || "84003"}
              </Text>
            </View>

            <View style={stylesSecond.officeAddress}>
              <Text style={stylesSecond.contactSecond}>CONTACT US NOW</Text>
              <Text style={stylesSecond.officeText}>
                Agent: {profile?.firstName} {profile?.lastName}
              </Text>
              <Text style={stylesSecond.officeText}>
                Office: {profile?.active_office || "N/A"}
              </Text>
              <Text style={stylesSecond.officeText}>
                Phone: {profile?.phone || "N/A"}
              </Text>
            </View>
          </View>
        </Page>
      </Document>
    );
  };

  const stylesSecondThird = StyleSheet.create({
    page: {
      backgroundColor: "#26221e",
      padding: 10,
    },

    rowContainer: {
      display: "flex",
      flexDirection: "row",
      justifyContent: "space-between",
      width: "100%",
    },
    logoCol: {
      width: "48%", // Slightly smaller width for the logo column
    },
    textCol: {
      width: "48%",
      justifyContent: "flex-end",
      textAlign: "right",
      marginRight: 10,
    },
    officeLogo: {
      width: 100,
      objectFit: "contain",
      marginBottom: 10,
    },
    priceText: {
      fontSize: 14,
      fontWeight: "bold",
      color: "red",
      marginBottom: 5,
    },

    // brownThirdPrice: {
    //   color: "red",
    //   marginBottom: 10,
    //   marginLeft: 10,
    // },
    mainLogo: {
      display: "flex",
      flexDirection: "row",
      padding: 5,
      marginBottom: 10,
      alignItems: "center",
      justifyContent: "space-between",
      width: "100%",
    },
    brownbox: {
      padding: 5,
      marginBottom: 1,
      borderRadius: 5,
      position: "relative",
      overflow: "hidden",
      zIndex: 1,
    },
    brownboxHeaderPrice: {
      fontSize: 18,
      fontWeight: "bold",
      color: "#dc936b",
      marginBottom: 5,
      transform: [{ scaleY: 1.2 }],
    },
    brownboxHeader: {
      fontSize: 18,
      fontWeight: "bold",
      color: "#fff",
      textAlign: "center",
      marginBottom: 5,
      transform: [{ scaleY: 1.2 }],
    },
    overviewHeader: {
      fontSize: 22,
      fontWeight: "bold",
      color: "#000",
      textAlign: "center",
      marginBottom: 15,
      transform: [{ rotate: "2deg" }],
      zIndex: 10,
    },
    contactInfo: {
      flexDirection: "row",
      justifyContent: "space-between",
      marginBottom: 20,
    },
    contactText: {
      fontSize: 12,
      color: "#fff",
    },

    mainpic: {
      width: "100%",
      flexDirection: "row",
      justifyContent: "space-between",
      marginBottom: 100,
    },
    imageThirdType: {
      flex: 2,
      height: 400,
      padding: 10,
    },
    imageItemThird: {
      flex: 2,
      padding: 10,
      height: 400,
    },

    imageThirdNew: {
      padding: 10,
      borderRadius: 16,
    },
    detailItem: {
      fontSize: 10,
      textAlign: "center",
      marginBottom: 10,
      color: "#fff",
    },
    detailOverview: {
      fontSize: 10,
      color: "#fff",
    },

    contactHeading: {
      fontSize: 16,
      fontWeight: "bold",
      color: "#fff",
      textAlign: "center",
      marginBottom: 15,
      marginTop: 15,
      transform: [{ rotate: "2deg" }],
    },

    longboxSubHeaderNew: {
      fontSize: 10,
      color: "#e0e0e0",
      marginBottom: 5,
    },
    price: {
      fontSize: 24,
      fontWeight: "bold",
      textAlign: "center",
      color: "#f5a623",
      marginBottom: 5,
      transform: [{ rotate: "-1deg" }],
    },
    longbox: {
      width: "100%",
      backgroundColor: "#1a1d1f",
      opacity: 0.95,
      padding: 15,
      borderRadius: 10,
      marginBottom: 5,
      flexDirection: "row",
      // justifyContent: "space-between",
      zIndex: 5,
      marginTop: 100,
    },
    description: {
      flex: 3,
      paddingRight: 10,
    },
    propertyData: {
      flex: 2,
      paddingLeft: 10,
    },

    longboxSubHeader: {
      fontSize: 12,
      color: "#e0e0e0",
      marginBottom: 5,
    },

    propertyDetails: {
      flexDirection: "column",
      fontSize: 10,
      color: "#fff",
      textAlign: "justify",
      marginTop: 10,
    },

    imageItem: {
      width: "32%",
      height: 150,
      objectFit: "cover",
      marginBottom: 10,
      zIndex: 1,
    },
    addressBox: {
      width: "100%",
      backgroundColor: getComputedStyle(
        document.documentElement
      ).getPropertyValue("--branding-footer-color"), // Dynamically get CSS variable value
      marginTop: 10,
      padding: 20,
      borderRadius: 16,
      flexDirection: "row",
      justifyContent: "space-between",
      position: "relative",
      zIndex: 5,
      transform: [{ scaleY: 1.05 }],
    },
    addressText: {
      fontSize: 16,
      color: "#fff",
      textAlign: "center",
      transform: [{ rotate: "-2deg" }],
    },
    officeSection: {
      width: "50%",
      alignItems: "flex-start",
      marginBottom: 5,
    },
    officeAddress: {
      marginBottom: 20,
      width: "50%",
      alignItems: "flex-end",
      marginBottom: 5,
    },
    officeLogo: {
      width: 120,
      height: 60,
      transform: [{ scale: 1.1 }],
    },
    officeDetails: {
      width: "100%",
    },
    officeText: {
      fontSize: 14,
      color: "#fff",
      textTransform: "uppercase",
      marginBottom: 2,
      zIndex: 10,
    },
    officeAddressTest: {
      fontSize: 14,
      color: "#fff",
      textTransform: "lowercase",
      marginBottom: 2,
    },
    propertyHeading: {
      padding: 10,
      fontSize: 20,
      fontWeight: "bold",
      color: "#fff",
      textAlign: "center",
      marginBottom: 15,
      transform: [{ scaleY: 1.2 }],
    },

    contactSecond: {
      fontSize: 16,
      fontWeight: "bold",
      color: "#cca345",
      textAlign: "center",
      marginBottom: 15,
      transform: [{ rotate: "2deg" }],
    },
  });

  const FlyerAlternativeThird = ({
    select,
    uploadedImages,
    platformData,
    profile,
  }) => {
    return (
      <Document>
        <Page size="A4" style={stylesSecondThird.page}>
          <View style={stylesSecondThird.rowContainer}>
            {/* Office Logo */}
            <View style={stylesSecondThird.logoCol}>
              <Image
                style={stylesSecondThird.officeLogo}
                src={platformData || Logo || ""}
                alt="Office Logo"
              />
            </View>
            {/* Price and Address */}
            <View style={stylesSecondThird.textCol}>
              <Text style={stylesSecondThird.priceText}>
                PRICE AT ${select.price || "X00,00"}
              </Text>
            </View>
          </View>

          <View style={stylesSecondThird.mainpic}>
            <View style={stylesSecondThird.imageThirdType}>
              <Image
                style={stylesSecond.imageThirdNew}
                src={uploadedImages[0] || defaultpropertyimage || ""}
                alt="Main Property Image"
              />
              <View style={stylesSecondThird.imageGrid}>
                <Text style={stylesSecondThird.contactHeading}>
                  Description
                </Text>
                <View style={stylesSecondThird.detailOverview}>
                  <Text style={stylesSecondThird.longboxSubHeaderNew}>
                    {select.publicRemarks ||
                      "Enter a brief description of this property for prospective buyers."}
                  </Text>
                </View>
              </View>
            </View>
            {/* Other Images */}
            <View style={stylesSecondThird.imageItemThird}>
              {(Array.isArray(uploadedImages) && uploadedImages.length > 1
                ? uploadedImages.slice(1, 4)
                : Array(3).fill(defaultpropertyimage)
              ).map((mediaItem, mediaIndex) => (
                <Image
                  style={stylesSecondThird.imageThirdNew}
                  src={mediaItem || defaultpropertyimage || ""}
                  key={mediaIndex}
                  alt={`Media ${mediaIndex + 1}`}
                />
              ))}
            </View>
          </View>
          <View style={styles.longbox}>
            <View style={styles.propertyDetails}>
              <View style={styles.detailItem}>
                <Text style={styles.longboxSubHeader}>Bedrooms</Text>
                <Text style={styles.longboxSubHeaderNew}>
                  {select.bed || "0"}
                </Text>
              </View>
              <View style={styles.detailItem}>
                <Text style={styles.longboxSubHeader}>Bathrooms</Text>
                <Text style={styles.longboxSubHeaderNew}>
                  {select.bath || "0"}
                </Text>
              </View>

              <View style={styles.detailItem}>
                <Text style={styles.longboxSubHeader}>Sqft</Text>
                <Text style={styles.longboxSubHeaderNew}>
                  {select.sqft || "X,000 Sq.Ft."}
                </Text>
              </View>

              <View style={styles.detailItem}>
                <Text style={styles.longboxSubHeader}>Lot Size</Text>
                <Text style={styles.longboxSubHeaderNew}>
                  {select.lotSize || ""}
                </Text>
              </View>

              <View style={styles.detailItem}>
                <Text style={styles.longboxSubHeader}>Year Built</Text>
                <Text style={styles.longboxSubHeaderNew}>
                  {select.yearBuilt || "2024"}
                </Text>
              </View>
            </View>
          </View>
          <View style={stylesSecond.addressBox}>
            <View style={stylesSecond.officeSection}>
              <View style={stylesSecond.officeDetails}>
                <Text style={stylesSecond.officeText}>
                  {select.published || "UtahRealEstate.com"}
                </Text>
                <Text style={stylesSecond.officeText}>
                  {select.address || "2024 Demo St"},
                </Text>
                <Text style={stylesSecond.officeText}>
                  {select.companyCity || "American Fork"},&nbsp;
                  {select.companyState || "UT"}&nbsp;
                  {select.companyPostCode || "84003"}
                </Text>
              </View>
            </View>
            <View style={stylesSecond.officeAddress}>
              <Image
                style={styles.officeContactImage}
                src={profile.image} // Compressed profile image
              />
              <Text style={stylesSecond.officeText}>
                Agent: {profile?.firstName} {profile?.lastName}
              </Text>
              <Text style={stylesSecond.officeText}>
                Office: {profile?.active_office}
              </Text>
              <Text style={stylesSecond.officeText}>
                Phone: {profile?.phone}
              </Text>
            </View>
          </View>
        </Page>
      </Document>
    );
  };

  const handleCancel = () => {
    setTemplate("");
  };

  return (
    <div className="bg-secondary float-left">
      <Loader isActive={isActive} />
      {isShow && (
        <Toaster
          types={types}
          isShow={isShow}
          toasterBody={toasterBody}
          message={message}
        />
      )}
      <main className="page-wrapper">
        <div className="row" data-thumbnails="true">
          {template ? (
            <>
              {templateType === "type1" ? (
                <section className="mb-5 pb-3 w-100" data-simplebar>
                  <div
                    className="bg-light border rounded-3 p-3"
                    data-thumbnails="true"
                  >
                    <div className="firstTemplate">
                      <div className="row" data-thumbnails="true">
                        <div className="col-md-12">
                          <div className="row">
                            <div className="col-md-6 mt-3">
                              <img
                                className="mb-2 rounded-0"
                                src={uploadedLogo || Logo}
                                style={{
                                  width: "100px",
                                  cursor: "pointer",
                                }} // Add cursor pointer for better UX
                                onClick={handleLogoClick} // Click to show file input
                              />
                              {showLogoInput && (
                                <label className="documentlabelAllLogo">
                                  <input
                                    id="REPC_real_estate_purchase_contract"
                                    type="file"
                                    onChange={handleLogoUpload}
                                  />
                                  <i class="h2 fi-edit opacity-80"></i>
                                </label>
                              )}
                            </div>
                            <div className="col-md-6 mt-3">
                              {editingField === "price" ? (
                                <div className="text-center">
                                  <input
                                    type="text"
                                    value={editedText} // Controlled input with edited text
                                    onChange={(e) =>
                                      setEditedText(e.target.value)
                                    } // Update text as the user types
                                    onBlur={handleTextSave} // Save the text when the user leaves the input field
                                    autoFocus
                                  />
                                </div>
                              ) : (
                                <h6
                                  className="d-flex justify-content-end"
                                  onClick={() =>
                                    handleTextClick("price", "X00,00")
                                  }
                                >
                                  Listed at $ {select.price || "X00,00"}
                                  {/* Dynamically show the current address or default */}
                                </h6>
                              )}

                              {/* Address Field */}
                              {editingField === "address" ? (
                                <div className="text-center">
                                  <input
                                    type="text"
                                    value={editedText} // Controlled input with edited text
                                    onChange={(e) =>
                                      setEditedText(e.target.value)
                                    } // Update text as the user types
                                    onBlur={handleTextSave} // Save the text when the user leaves the input field
                                    autoFocus
                                  />
                                </div>
                              ) : (
                                <h6
                                  className="d-flex justify-content-end"
                                  onClick={() =>
                                    handleTextClick("address", "2024 Demo St")
                                  }
                                >
                                  {select.address || "2024 Demo St"}
                                  {/* Dynamically show the current address or default */}
                                </h6>
                              )}
                            </div>
                          </div>

                          <div className="col-md-12 singleListing">
                            <div className="card-img-top">
                              <img
                                className="img-fluid rounded-0"
                                src={uploadedImages[0] || defaultpropertyimage}
                                alt="Property"
                                onClick={() => handleImageClick(0)}
                              />
                              {selectedImage === 0 && (
                                <label className="documentlabelNew">
                                  <input
                                    id="REPC_real_estate_purchase_contract"
                                    type="file"
                                    onChange={(e) => handleImageUpload(e, 0)} // Upload for the single image
                                  />
                                  <i className="h2 fi-edit opacity-80"></i>
                                </label>
                              )}
                            </div>
                          </div>

                          <div className="property_details">
                            <div className="col-md-12">
                              <h4 className="mt-2 text-center">
                                Property Details
                              </h4>
                            </div>
                            <div className="row proprty_background">
                              <div className="col-md-2">
                                <h6 className="text-white">Bedrooms</h6>
                                {editingField === "bed" ? (
                                  <input
                                    className="w-100"
                                    type="text"
                                    value={editedText} // Controlled input with edited text
                                    onChange={(e) =>
                                      setEditedText(e.target.value)
                                    } // Update text as the user types
                                    onBlur={handleTextSave} // Save the text when the user leaves the input field
                                    autoFocus
                                  />
                                ) : (
                                  <p
                                    className="text-white"
                                    onClick={() => handleTextClick("bed", "0")}
                                  >
                                    {select.bed || "0"}
                                  </p>
                                )}
                              </div>

                              <div className="col-md-2 ms-3">
                                <h6 className="text-white">Bathrooms</h6>
                                {editingField === "bath" ? (
                                  <input
                                    className="w-100"
                                    type="text"
                                    value={editedText} // Controlled input with edited text
                                    onChange={(e) =>
                                      setEditedText(e.target.value)
                                    } // Update text as the user types
                                    onBlur={handleTextSave} // Save the text when the user leaves the input field
                                    autoFocus
                                  />
                                ) : (
                                  <p
                                    className="text-white"
                                    onClick={() => handleTextClick("bath", "0")}
                                  >
                                    {select.bath || "0"}
                                  </p>
                                )}
                              </div>

                              <div className="col-md-2 ms-3">
                                <h6 className="text-white">Sqft</h6>
                                {editingField === "sqft" ? (
                                  <input
                                    className="w-100"
                                    type="text"
                                    value={editedText} // Controlled input with edited text
                                    onChange={(e) =>
                                      setEditedText(e.target.value)
                                    } // Update text as the user types
                                    onBlur={handleTextSave} // Save the text when the user leaves the input field
                                    autoFocus
                                  />
                                ) : (
                                  <p
                                    className="text-white"
                                    onClick={() =>
                                      handleTextClick("sqft", "X,000 Sq.Ft.")
                                    }
                                  >
                                    {select.sqft || "X,000 Sq.Ft."}
                                  </p>
                                )}
                              </div>

                              <div className="col-md-2 ms-3">
                                <h6 className="text-white">Lot Size</h6>
                                {editingField === "lotSize" ? (
                                  <input
                                    className="w-100"
                                    type="text"
                                    value={editedText} // Controlled input with edited text
                                    onChange={(e) =>
                                      setEditedText(e.target.value)
                                    } // Update text as the user types
                                    onBlur={handleTextSave} // Save the text when the user leaves the input field
                                    autoFocus
                                  />
                                ) : (
                                  <p
                                    className="text-white"
                                    onClick={() =>
                                      handleTextClick("lotSize", "0")
                                    }
                                  >
                                    {select.lotSize || "0"}
                                  </p>
                                )}
                              </div>

                              <div className="col-md-2 ms-3">
                                <div className="d-flex">
                                  <h6 className="text-white">Year Built</h6>
                                </div>
                                {editingField === "yearBuilt" ? (
                                  <input
                                    className="w-100"
                                    type="text"
                                    value={editedText} // Controlled input with edited text
                                    onChange={(e) =>
                                      setEditedText(e.target.value)
                                    } // Update text as the user types
                                    onBlur={handleTextSave} // Save the text when the user leaves the input field
                                    autoFocus
                                  />
                                ) : (
                                  <p
                                    className="text-white"
                                    onClick={() =>
                                      handleTextClick("yearBuilt", "2024")
                                    }
                                  >
                                    {select.yearBuilt || "2024"}
                                  </p>
                                )}
                              </div>

                              <div className="col-md-12">
                                {editingField === "publicRemarks" ? (
                                  <textarea
                                    className="form-control mt-0 w-100"
                                    id="textarea-input"
                                    rows="5"
                                    type="text"
                                    value={editedText} // Controlled input with edited text
                                    onChange={(e) => {
                                      const wordCount = e.target.value
                                        .trim()
                                        .split(/\s+/).length;
                                      if (wordCount <= 150) {
                                        setEditedText(e.target.value); // Update text only if word count is <= 150
                                      }
                                    }} // Update text as the user types
                                    onBlur={handleTextSave}
                                    autoFocus
                                  />
                                ) : (
                                  <p
                                    className="text-white description_PrintPage"
                                    style={{ fontSize: "12px" }}
                                    onClick={() =>
                                      handleTextClick(
                                        "publicRemarks",
                                        "Enter a brief description of this property for prospective buyers."
                                      )
                                    }
                                  >
                                    {select.publicRemarks ||
                                      "Enter a brief description of this property for prospective buyers."}
                                  </p>
                                )}
                              </div>
                            </div>

                            <div className="row">
                              {[1, 2, 3].map((_, defaultIndex) => (
                                <div
                                  className="col-md-4 mt-2"
                                  key={defaultIndex}
                                >
                                  <div className="proprty_mediaImage">
                                    <img
                                      className="img-fluid w-100 rounded-0"
                                      src={
                                        uploadedImages[defaultIndex + 1] ||
                                        defaultpropertyimage
                                      }
                                      alt={`Default Property media ${
                                        defaultIndex + 1
                                      }`}
                                      onClick={() =>
                                        handleImageClick(defaultIndex + 1)
                                      } // Allow clicking to upload a new image
                                    />
                                    {selectedImage === defaultIndex + 1 && (
                                      <label className="documentlabelAllImage">
                                        <input
                                          id="REPC_real_estate_purchase_contract"
                                          type="file"
                                          onChange={(e) =>
                                            handleImageUpload(
                                              e,
                                              defaultIndex + 1
                                            )
                                          } // Allow image upload
                                        />
                                        <i className="h2 fi-edit opacity-80"></i>
                                      </label>
                                    )}
                                  </div>
                                </div>
                              ))}
                            </div>

                            {/* Contact Details */}

                            <div className="row mt-2 contact_background">
                              <div className="col-md-6">
                                <div className="col-md-6">
                                  <div className="float-start w-100">
                                    <img
                                      className="print_Profile"
                                      src={
                                        uploadedContact ||
                                        (profile.image ? profile.image : "")
                                      } // Show uploaded logo or default avatar
                                      style={{
                                        width: "100px",
                                        cursor: "pointer",
                                      }} // Pointer cursor for better UX
                                      onClick={handleLogoClickContact} // Show file input on click
                                    />
                                    {/* Conditionally render file input on logo click */}
                                    {showContact && (
                                      <label className="documentlabelContact">
                                        <input
                                          id="REPC_real_estate_purchase_contract"
                                          type="file"
                                          onChange={handleLogoUploadContact}
                                        />
                                        <i class="h2 fi-edit opacity-80"></i>
                                      </label>
                                    )}

                                    <div className="d-flex">
                                      {editingField === "firstName" ? (
                                        <input
                                          type="text"
                                          value={editedText} // Controlled input with edited text
                                          onChange={(e) =>
                                            setEditedText(e.target.value)
                                          } // Update text as the user types
                                          onBlur={() =>
                                            handleTextSaveListAgent("firstName")
                                          } // Save the text when the user leaves the input field
                                          autoFocus
                                        />
                                      ) : (
                                        <span
                                          style={{ fontSize: "12px" }}
                                          onClick={() =>
                                            handleTextListAgent(
                                              "firstName",
                                              profile.firstName,
                                              profile.lastName
                                            )
                                          }
                                        >
                                          {profile && (
                                            <p className="mb-0 text-white">
                                              <b>Agent: </b>
                                              {profile.firstName}&nbsp;
                                            </p>
                                          )}
                                        </span>
                                      )}

                                      {editingField === "lastName" ? (
                                        <input
                                          type="text"
                                          value={editedText} // Controlled input with edited text
                                          onChange={(e) =>
                                            setEditedText(e.target.value)
                                          } // Update text as the user types
                                          onBlur={() =>
                                            handleTextSaveListAgent("lastName")
                                          } // Save the text when the user leaves the input field
                                          autoFocus
                                        />
                                      ) : (
                                        <span
                                          style={{ fontSize: "12px" }}
                                          onClick={() =>
                                            handleTextListAgent(
                                              "lastName",
                                              profile.lastName
                                            )
                                          }
                                        >
                                          {profile && (
                                            <p className="mb-0 text-white">
                                              {" "}
                                              {profile.lastName}&nbsp;
                                            </p>
                                          )}
                                        </span>
                                      )}
                                    </div>

                                    {editingField === "listOffice" ? (
                                      <input
                                        type="text"
                                        value={editedText} // Controlled input with edited text
                                        onChange={(e) =>
                                          setEditedText(e.target.value)
                                        } // Update text as the user types
                                        onBlur={() =>
                                          handleTextSaveListOffice("listOffice")
                                        } // Save the text when the user leaves the input field
                                        autoFocus
                                      />
                                    ) : (
                                      <span
                                        style={{ fontSize: "12px" }}
                                        onClick={() =>
                                          handleTextListOffice(
                                            "listOffice",
                                            profile.active_office
                                          )
                                        }
                                      >
                                        {profile && (
                                          <p className="mb-0 text-white">
                                            <b>Office: </b>
                                            {profile.active_office}
                                          </p>
                                        )}
                                      </span>
                                    )}

                                    {editingField === "officePhone" ? (
                                      <input
                                        type="text"
                                        value={editedText} // Controlled input with edited text
                                        onChange={(e) =>
                                          setEditedText(e.target.value)
                                        } // Update text as the user types
                                        onBlur={() =>
                                          handleTextSaveOfficePhone(
                                            "officePhone"
                                          )
                                        } // Save the text when the user leaves the input field
                                        autoFocus
                                      />
                                    ) : (
                                      <span
                                        style={{ fontSize: "12px" }}
                                        onClick={() =>
                                          handleTextOfficePhone(
                                            "officePhone",
                                            profile.phone
                                          )
                                        }
                                      >
                                        {profile && (
                                          <p className="mb-0 text-white">
                                            <b>Phone: </b>
                                            {profile.phone}
                                          </p>
                                        )}
                                      </span>
                                    )}
                                  </div>
                                </div>
                              </div>
                              <div className="col-md-6">
                                <div className="pull-right text-end imageLogo w-100">
                                  {editingField === "published" ? (
                                    <input
                                      className="w-100"
                                      type="text"
                                      value={editedText} // Controlled input with edited text
                                      onChange={(e) =>
                                        setEditedText(e.target.value)
                                      } // Update text as the user types
                                      onBlur={handleTextSave} // Save the text when the user leaves the input field
                                      autoFocus
                                    />
                                  ) : (
                                    <p
                                      className="mb-0 text-white"
                                      style={{ fontSize: "12px" }}
                                      onClick={() =>
                                        handleTextClick(
                                          "published",
                                          "UtahRealEstate.com"
                                        )
                                      }
                                    >
                                      {select.published || "UtahRealEstate.com"}
                                    </p>
                                  )}

                                  {editingField === "companyAddress" ? (
                                    <input
                                      className="w-100"
                                      type="text"
                                      value={editedText} // Controlled input with edited text
                                      onChange={(e) =>
                                        setEditedText(e.target.value)
                                      } // Update text as the user types
                                      onBlur={handleTextSave}
                                      autoFocus
                                    />
                                  ) : (
                                    <span
                                      className="mb-0 text-white"
                                      style={{ fontSize: "12px" }}
                                      onClick={() =>
                                        handleTextClick(
                                          "address",
                                          "2024 Demo St"
                                        )
                                      }
                                    >
                                      {select.address || "2024 Demo St"}
                                    </span>
                                  )}

                                  <br />

                                  {editingField === "companyCity" ? (
                                    <input
                                      className="w-100"
                                      type="text"
                                      value={editedText} // Controlled input with edited text
                                      onChange={(e) =>
                                        setEditedText(e.target.value)
                                      } // Update text as the user types
                                      onBlur={handleTextSave}
                                      autoFocus
                                    />
                                  ) : (
                                    <span
                                      className="mb-0 text-white"
                                      style={{ fontSize: "12px" }}
                                      onClick={() =>
                                        handleTextClick(
                                          "companyCity",
                                          "American Fork"
                                        )
                                      }
                                    >
                                      {select.companyCity || "American Fork"},
                                    </span>
                                  )}

                                  {editingField === "companyState" ? (
                                    <input
                                      type="text"
                                      value={editedText} // Controlled input with edited text
                                      onChange={(e) =>
                                        setEditedText(e.target.value)
                                      } // Update text as the user types
                                      onBlur={handleTextSave}
                                      autoFocus
                                    />
                                  ) : (
                                    <span
                                      className="mb-0 text-white"
                                      style={{ fontSize: "12px" }}
                                      onClick={() =>
                                        handleTextClick("companyState", "UT")
                                      }
                                    >
                                      {select.companyState || "UT"},
                                    </span>
                                  )}

                                  {editingField === "companyPostCode" ? (
                                    <input
                                      className="w-100"
                                      type="text"
                                      value={editedText} // Controlled input with edited text
                                      onChange={(e) =>
                                        setEditedText(e.target.value)
                                      } // Update text as the user types
                                      onBlur={handleTextSave}
                                      autoFocus
                                    />
                                  ) : (
                                    <span
                                      className="mb-0 text-white"
                                      style={{ fontSize: "12px" }}
                                      onClick={() =>
                                        handleTextClick(
                                          "companyPostCode",
                                          "84003"
                                        )
                                      }
                                    >
                                      {select.companyPostCode || "84003"}
                                    </span>
                                  )}
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div className="template_second">
                      <button
                        className="btn btn-secondary pull-right"
                        onClick={() => handleCancel()}
                      >
                        Cancel
                      </button>
                      <PDFDownloadLink
                        document={
                          <Flyer
                            select={select}
                            uploadedImages={uploadedImages}
                            platformData={uploadedLogo}
                            profile={profile}
                            // profileImage={uploadedContact}
                          />
                        }
                        fileName={select.address || "Flyer.pdf"}
                      >
                        Download Pdf
                      </PDFDownloadLink>
                    </div>
                  </div>
                </section>
              ) : (
                ""
              )}

              {templateType === "type2" ? (
                <div ref={componentRef}>
                  <section className="mb-5 pb-3 w-100" data-simplebar>
                    <div
                      className="bg-light border rounded-3 p-3"
                      data-thumbnails="true"
                    >
                      <div className="firstTemplate" data-thumbnails="true">
                        <div className="row" data-thumbnails="true">
                          <div className="col-md-12 singleListing">
                            <div className="row">
                              <div className="col-md-6 mt-3">
                                <img
                                  className="mb-4 rounded-0 "
                                  src={uploadedLogo || Logo}
                                  style={{
                                    width: "100px",
                                    cursor: "pointer",
                                  }} // Add cursor pointer for better UX
                                  onClick={handleLogoClick} // Click to show file input
                                />
                                {showLogoInput && (
                                  <label className="documentlabelAllLogo">
                                    <input
                                      id="REPC_real_estate_purchase_contract"
                                      type="file"
                                      onChange={handleLogoUpload}
                                    />
                                    <i class="h2 fi-edit opacity-80"></i>
                                  </label>
                                )}
                              </div>
                              <div className="col-md-6 mt-3">
                                {editingField === "price" ? (
                                  <div className="text-center">
                                    <input
                                      type="text"
                                      value={editedText}
                                      onChange={(e) =>
                                        setEditedText(e.target.value)
                                      }
                                      onBlur={handleTextSaveSecond}
                                      autoFocus
                                    />
                                  </div>
                                ) : (
                                  <h6
                                    className="d-flex justify-content-end mt-4"
                                    onClick={() =>
                                      handleTextClick("price", "X00,00")
                                    }
                                  >
                                    PRICE AT $ {select.price || "X00,00"}
                                  </h6>
                                )}
                              </div>
                            </div>

                            <div className="card-img-top">
                              <img
                                className="img-fluid rounded-0"
                                src={uploadedImages[0] || defaultpropertyimage}
                                alt="Property"
                                onClick={() => handleImageClick(0)}
                              />
                              {selectedImage === 0 && (
                                <label className="documentlabelNew">
                                  <input
                                    id="REPC_real_estate_purchase_contract"
                                    type="file"
                                    onChange={(e) => handleImageUpload(e, 0)} // Upload for the single image
                                  />
                                  <i className="h2 fi-edit opacity-80"></i>
                                </label>
                              )}
                            </div>
                          </div>

                          <div className="row">
                            {[1, 2, 3].map((_, defaultIndex) => (
                              <div className="col-md-4 mt-2" key={defaultIndex}>
                                <div className="second_mediaImage">
                                  <img
                                    className="img-fluid w-100 rounded-0"
                                    src={
                                      uploadedImages[defaultIndex + 1] ||
                                      defaultpropertyimage
                                    }
                                    alt={`Default Property media ${
                                      defaultIndex + 1
                                    }`}
                                    onClick={() =>
                                      handleImageClick(defaultIndex + 1)
                                    } // Allow clicking to upload a new image
                                  />
                                  {selectedImage === defaultIndex + 1 && (
                                    <label className="documentlabelAllImage">
                                      <input
                                        id="REPC_real_estate_purchase_contract"
                                        type="file"
                                        onChange={(e) =>
                                          handleImageUpload(e, defaultIndex + 1)
                                        } // Allow image upload
                                      />
                                      <i className="h2 fi-edit opacity-80"></i>
                                    </label>
                                  )}
                                </div>
                              </div>
                            ))}
                          </div>
                          <div className="propertySecond_details float-left w-100">
                            <div className="row">
                              <div className="col-md-8">
                                <h6 className="text-center text-white">
                                  Description
                                </h6>
                                {/* Edit PublicRemarks */}
                                {editingField === "publicRemarks" ? (
                                  <textarea
                                    className="form-control mt-0"
                                    id="textarea-input"
                                    rows="5"
                                    type="text"
                                    value={editedText} // Controlled input with edited text
                                    onChange={(e) => {
                                      const wordCount = e.target.value
                                        .trim()
                                        .split(/\s+/).length;
                                      if (wordCount <= 150) {
                                        setEditedText(e.target.value); // Update text only if word count is <= 150
                                      }
                                    }} // Update text as the user types
                                    onBlur={handleTextSaveSecond}
                                    autoFocus
                                  />
                                ) : (
                                  <p
                                    className="mb-1 description_PrintPage text-white p-2"
                                    onClick={() =>
                                      handleTextClick(
                                        "publicRemarks",
                                        "Enter a brief description of this property for prospective buyers."
                                      )
                                    }
                                  >
                                    <small>
                                      {select.publicRemarks ||
                                        "Enter a brief description of this property for prospective buyers."}
                                    </small>
                                  </p>
                                )}
                              </div>
                              <div className="col-md-4">
                                <h6 className="text-center text-white">
                                  Property Details
                                </h6>
                                <ul className="list-unstyled text-center">
                                  {editingField === "bed" ? (
                                    <input
                                      className="w-100"
                                      type="text"
                                      value={editedText} // Controlled input with edited text
                                      onChange={(e) =>
                                        setEditedText(e.target.value)
                                      } // Update text as the user types
                                      onBlur={handleTextSaveSecond} // Save the text when the user leaves the input field
                                      autoFocus
                                    />
                                  ) : (
                                    <p
                                      className="text-white mb-2"
                                      onClick={() =>
                                        handleTextClick("bed", "0")
                                      }
                                    >
                                      <small>
                                        Bedrooms:
                                        {select.bed || "0"}
                                      </small>
                                    </p>
                                  )}

                                  {editingField === "bath" ? (
                                    <input
                                      className="w-100"
                                      type="text"
                                      value={editedText} // Controlled input with edited text
                                      onChange={(e) =>
                                        setEditedText(e.target.value)
                                      } // Update text as the user types
                                      onBlur={handleTextSaveSecond} // Save the text when the user leaves the input field
                                      autoFocus
                                    />
                                  ) : (
                                    <p
                                      className="text-white mb-2"
                                      onClick={() =>
                                        handleTextClick("bath", "0")
                                      }
                                    >
                                      <small>
                                        Bedrooms:
                                        {select.bath || "0"}
                                      </small>
                                    </p>
                                  )}

                                  {editingField === "sqft" ? (
                                    <input
                                      className="w-100"
                                      type="text"
                                      value={editedText} // Controlled input with edited text
                                      onChange={(e) =>
                                        setEditedText(e.target.value)
                                      } // Update text as the user types
                                      onBlur={handleTextSaveSecond} // Save the text when the user leaves the input field
                                      autoFocus
                                    />
                                  ) : (
                                    <p
                                      className="text-white mb-2"
                                      onClick={() =>
                                        handleTextClick("sqft", "X,000 Sq.Ft.")
                                      }
                                    >
                                      <small>
                                        Sqft:
                                        {select.sqft || "X,000 Sq.Ft."}
                                      </small>
                                    </p>
                                  )}

                                  {editingField === "lotSize" ? (
                                    <input
                                      className="w-100"
                                      type="text"
                                      value={editedText} // Controlled input with edited text
                                      onChange={(e) =>
                                        setEditedText(e.target.value)
                                      } // Update text as the user types
                                      onBlur={handleTextSaveSecond} // Save the text when the user leaves the input field
                                      autoFocus
                                    />
                                  ) : (
                                    <p
                                      className="text-white mb-2"
                                      onClick={() =>
                                        handleTextClick("lotSize", "0")
                                      }
                                    >
                                      <small>
                                        Lot size : {select.lotSize || "0"}
                                      </small>
                                    </p>
                                  )}

                                  {editingField === "yearBuilt" ? (
                                    <input
                                      className="w-100"
                                      type="text"
                                      value={editedText} // Controlled input with edited text
                                      onChange={(e) =>
                                        setEditedText(e.target.value)
                                      } // Update text as the user types
                                      onBlur={handleTextSaveSecond} // Save the text when the user leaves the input field
                                      autoFocus
                                    />
                                  ) : (
                                    <p
                                      className="text-white mb-2"
                                      onClick={() =>
                                        handleTextClick("yearBuilt", "2024")
                                      }
                                    >
                                      <small>
                                        Year Built :{" "}
                                        {select.yearBuilt || "2024"}
                                      </small>
                                    </p>
                                  )}
                                </ul>
                              </div>
                            </div>
                          </div>

                          <div className="row mt-2 contactSecond_background">
                            <div className="col-md-6 float-left">
                              {editingField === "published" ? (
                                <input
                                  className="w-100"
                                  type="text"
                                  value={editedText} // Controlled input with edited text
                                  onChange={(e) =>
                                    setEditedText(e.target.value)
                                  } // Update text as the user types
                                  onBlur={handleTextSaveSecond} // Save the text when the user leaves the input field
                                  autoFocus
                                />
                              ) : (
                                <span
                                  className="text-white mb-0"
                                  onClick={() =>
                                    handleTextClick(
                                      "published",
                                      "UtahRealEstate.com"
                                    )
                                  }
                                >
                                  <smal>
                                    {select.published || "UtahRealEstate.com"}
                                  </smal>
                                </span>
                              )}
                              <br />

                              {editingField === "address" ? (
                                <input
                                  className="w-100"
                                  type="text"
                                  value={editedText} // Controlled input with edited text
                                  onChange={(e) =>
                                    setEditedText(e.target.value)
                                  } // Update text as the user types
                                  onBlur={handleTextSaveSecond}
                                  autoFocus
                                />
                              ) : (
                                <span
                                  className="text-white mb-0"
                                  onClick={() =>
                                    handleTextClick("address", "2024 Demo St")
                                  }
                                >
                                  {select.address || "2024 Demo St"}
                                </span>
                              )}
                              <br />
                              <div className="d-flex">
                                {editingField === "companyCity" ? (
                                  <input
                                    className="w-100"
                                    type="text"
                                    value={editedText} // Controlled input with edited text
                                    onChange={(e) =>
                                      setEditedText(e.target.value)
                                    } // Update text as the user types
                                    onBlur={handleTextSaveSecond}
                                    autoFocus
                                  />
                                ) : (
                                  <span
                                    className="text-white mb-0"
                                    onClick={() =>
                                      handleTextClick(
                                        "companyCity",
                                        "American Fork"
                                      )
                                    }
                                  >
                                    {select.companyCity || "American Fork"},
                                  </span>
                                )}

                                {editingField === "companyState" ? (
                                  <input
                                    type="text"
                                    value={editedText} // Controlled input with edited text
                                    onChange={(e) =>
                                      setEditedText(e.target.value)
                                    } // Update text as the user types
                                    onBlur={handleTextSaveSecond}
                                    autoFocus
                                  />
                                ) : (
                                  <span
                                    className="text-white mb-0"
                                    onClick={() =>
                                      handleTextClick("companyState", "UT")
                                    }
                                  >
                                    {select.companyState || "UT"},
                                  </span>
                                )}

                                {editingField === "companyPostCode" ? (
                                  <input
                                    className="w-100"
                                    type="text"
                                    value={editedText} // Controlled input with edited text
                                    onChange={(e) =>
                                      setEditedText(e.target.value)
                                    } // Update text as the user types
                                    onBlur={handleTextSaveSecond}
                                    autoFocus
                                  />
                                ) : (
                                  <span
                                    className="text-white mb-0"
                                    onClick={() =>
                                      handleTextClick(
                                        "companyPostCode",
                                        "84003"
                                      )
                                    }
                                  >
                                    {select.companyPostCode || "84003"}
                                  </span>
                                )}
                              </div>
                            </div>

                            <div className="col-md-6">
                              <ul className="list-unstyled text-end">
                                <img
                                  className="print_Profile"
                                  src={
                                    uploadedContact ||
                                    (profile.image ? profile.image : "")
                                  } // Show uploaded logo or default avatar
                                  style={{
                                    width: "100px",
                                    cursor: "pointer",
                                  }} // Pointer cursor for better UX
                                  onClick={handleLogoClickContact} // Show file input on click
                                />
                                {/* Conditionally render file input on logo click */}
                                {showContact && (
                                  <label className="documentlabelContact">
                                    <input
                                      id="REPC_real_estate_purchase_contract"
                                      type="file"
                                      onChange={handleLogoUploadContact}
                                    />
                                    <i class="h2 fi-edit opacity-80"></i>
                                  </label>
                                )}

                                <div className="d-flex justify-content-end">
                                  {editingField === "firstName" ? (
                                    <input
                                      type="text"
                                      value={editedText} // Controlled input with edited text
                                      onChange={(e) =>
                                        setEditedText(e.target.value)
                                      } // Update text as the user types
                                      onBlur={() =>
                                        handleTextSaveListAgent("firstName")
                                      } // Save the text when the user leaves the input field
                                      autoFocus
                                    />
                                  ) : (
                                    <p
                                      className="text-white mb-0"
                                      onClick={() =>
                                        handleTextListAgent(
                                          "firstName",
                                          profile.firstName,
                                          profile.lastName
                                        )
                                      }
                                    >
                                      {profile && (
                                        <p className="mb-0">
                                          <b>Agent: </b>
                                          {profile.firstName}&nbsp;
                                        </p>
                                      )}
                                    </p>
                                  )}

                                  {editingField === "lastName" ? (
                                    <input
                                      type="text"
                                      value={editedText} // Controlled input with edited text
                                      onChange={(e) =>
                                        setEditedText(e.target.value)
                                      } // Update text as the user types
                                      onBlur={() =>
                                        handleTextSaveListAgent("lastName")
                                      } // Save the text when the user leaves the input field
                                      autoFocus
                                    />
                                  ) : (
                                    <p
                                      className="text-white mb-0"
                                      onClick={() =>
                                        handleTextListAgent(
                                          "lastName",
                                          profile.lastName
                                        )
                                      }
                                    >
                                      {profile && (
                                        <p className="mb-0">
                                          {" "}
                                          {profile.lastName}&nbsp;
                                        </p>
                                      )}
                                    </p>
                                  )}
                                </div>

                                {editingField === "listOffice" ? (
                                  <input
                                    type="text"
                                    value={editedText} // Controlled input with edited text
                                    onChange={(e) =>
                                      setEditedText(e.target.value)
                                    } // Update text as the user types
                                    onBlur={() =>
                                      handleTextSaveListOffice("listOffice")
                                    } // Save the text when the user leaves the input field
                                    autoFocus
                                  />
                                ) : (
                                  <p
                                    className="text-white mb-0"
                                    onClick={() =>
                                      handleTextListOffice(
                                        "listOffice",
                                        profile.active_office
                                      )
                                    }
                                  >
                                    {profile && (
                                      <p className="mb-0">
                                        <b>Office: </b>
                                        {profile.active_office}
                                      </p>
                                    )}
                                  </p>
                                )}

                                {editingField === "officePhone" ? (
                                  <input
                                    type="text"
                                    value={editedText} // Controlled input with edited text
                                    onChange={(e) =>
                                      setEditedText(e.target.value)
                                    } // Update text as the user types
                                    onBlur={() =>
                                      handleTextSaveOfficePhone("officePhone")
                                    } // Save the text when the user leaves the input field
                                    autoFocus
                                  />
                                ) : (
                                  <p
                                    className="text-white mb-0"
                                    onClick={() =>
                                      handleTextOfficePhone(
                                        "officePhone",
                                        profile.phone
                                      )
                                    }
                                  >
                                    {profile && (
                                      <p className="mb-0">
                                        <b>Phone: </b>
                                        {profile.phone}
                                      </p>
                                    )}
                                  </p>
                                )}
                              </ul>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div className="template_second">
                        <button
                          className="btn btn-secondary pull-right"
                          onClick={() => handleCancel()}
                        >
                          Cancel
                        </button>
                        <PDFDownloadLink
                          document={
                            <FlyerAlternative
                              select={select}
                              uploadedImages={uploadedImages}
                              platformData={uploadedLogo}
                              profile={profile}
                            />
                          }
                          fileName={select.address || "Flyer2.pdf"}
                        >
                          Download Pdf
                        </PDFDownloadLink>
                      </div>
                    </div>
                  </section>
                </div>
              ) : (
                ""
              )}

              {templateType === "type3" ? (
                <section
                  className="mb-0 pb-3 bg-light border rounded-3 p-3"
                  data-simplebar
                >
                  <div className="thirdTemplate">
                    <div className="row">
                      <div className="col-md-6 mt-3">
                        <img
                          className="mb-4 rounded-0 "
                          src={uploadedLogo || Logo}
                          style={{
                            width: "100px",
                            cursor: "pointer",
                          }} // Add cursor pointer for better UX
                          onClick={handleLogoClick} // Click to show file input
                        />
                        {showLogoInput && (
                          <label className="documentlabelAllLogo">
                            <input
                              id="REPC_real_estate_purchase_contract"
                              type="file"
                              onChange={handleLogoUpload}
                            />
                            <i class="h2 fi-edit opacity-80"></i>
                          </label>
                        )}
                      </div>

                      <div className="col-md-6 mt-3">
                        {editingField === "price" ? (
                          <div className="text-center">
                            <input
                              type="text"
                              value={editedText} // Controlled input with edited text
                              onChange={(e) => setEditedText(e.target.value)}
                              onBlur={handleTextSave} // Save the text when the user leaves the input field
                              autoFocus
                            />
                          </div>
                        ) : (
                          <h6
                            className="d-flex justify-content-end mt-4"
                            style={{ color: "red" }}
                            onClick={() => handleTextClick("price", "X00,00")}
                          >
                            PRICE AT $ {select.price || "X00,00"}
                          </h6>
                        )}
                      </div>
                    </div>
                    <div className="row" data-thumbnails="true">
                      <div className="col-md-6 singleListing mb-3">
                        <div className="card-img-top">
                          <img
                            className="img-fluid rounded-0"
                            src={uploadedImages[0] || defaultpropertyimage}
                            alt="Property"
                            onClick={() => handleImageClick(0)}
                          />

                          {selectedImage === 0 && (
                            <label className="documentNonListedThird">
                              <input
                                id="REPC_real_estate_purchase_contract"
                                type="file"
                                onChange={(e) => handleImageUpload(e, 0)} // Upload for the single image
                              />
                              <i className="h2 fi-edit opacity-80"></i>
                            </label>
                          )}
                        </div>

                        {/* Edit PublicRemarks */}
                        <div className="mt-2">
                          <h6 className="text-center text-white">
                            Description
                          </h6>
                          {editingField === "publicRemarks" ? (
                            <textarea
                              className="form-control mt-0"
                              id="textarea-input"
                              rows="5"
                              type="text"
                              value={editedText} // Controlled input with edited text
                              onChange={(e) => {
                                const wordCount = e.target.value
                                  .trim()
                                  .split(/\s+/).length;
                                if (wordCount <= 150) {
                                  setEditedText(e.target.value); // Update text only if word count is <= 150
                                }
                              }} // Update text as the user types
                              onBlur={handleTextSave}
                              autoFocus
                            />
                          ) : (
                            <span
                              onClick={() =>
                                handleTextClick(
                                  "publicRemarks",
                                  "Enter a brief description of this property for prospective buyers."
                                )
                              }
                            >
                              <div className="mb-4">
                                <p className="mb-1 description_PrintPage text-white">
                                  {select.publicRemarks ||
                                    "Enter a brief description of this property for prospective buyers."}
                                </p>
                              </div>
                            </span>
                          )}
                        </div>
                      </div>
                      <div className="col-md-6">
                        {[1, 2, 3].map((_, defaultIndex) => (
                          <div
                            className="col-md-12 mt-2 p-3 mb-2"
                            key={defaultIndex}
                          >
                            <div className="second_mediaImage">
                              <img
                                className="img-fluid w-100 rounded-0"
                                src={
                                  uploadedImages[defaultIndex + 1] ||
                                  defaultpropertyimage
                                }
                                alt={`Default Property media ${
                                  defaultIndex + 1
                                }`}
                                onClick={() =>
                                  handleImageClick(defaultIndex + 1)
                                } // Allow clicking to upload a new image
                              />
                              {selectedImage === defaultIndex + 1 && (
                                <label className="documentlabelAllImage">
                                  <input
                                    id="REPC_real_estate_purchase_contract"
                                    type="file"
                                    onChange={(e) =>
                                      handleImageUpload(e, defaultIndex + 1)
                                    } // Allow image upload
                                  />
                                  <i className="h2 fi-edit opacity-80"></i>
                                </label>
                              )}
                            </div>
                          </div>
                        ))}
                      </div>
                    </div>

                    <div className="row thirdDetails_Property">
                      <div className="col-md-2">
                        <h6 className="text-white">Bedrooms</h6>

                        {editingField === "bed" ? (
                          <input
                            className="w-100"
                            type="text"
                            value={editedText} // Controlled input with edited text
                            onChange={(e) => setEditedText(e.target.value)} // Update text as the user types
                            onBlur={handleTextSave} // Save the text when the user leaves the input field
                            autoFocus
                          />
                        ) : (
                          <p
                            className="text-white"
                            onClick={() => handleTextClick("bed", "0")}
                          >
                            <small>{select.bed || "0"}</small>
                          </p>
                        )}
                      </div>

                      <div className="col-md-2 ms-3">
                        <h6 className="text-white">Bathrooms</h6>
                        {editingField === "bath" ? (
                          <input
                            className="w-100"
                            type="text"
                            value={editedText} // Controlled input with edited text
                            onChange={(e) => setEditedText(e.target.value)} // Update text as the user types
                            onBlur={handleTextSave} // Save the text when the user leaves the input field
                            autoFocus
                          />
                        ) : (
                          <p
                            className="text-white"
                            onClick={() => handleTextClick("bath", "0")}
                          >
                            <small>{select.bath || "0"}</small>
                          </p>
                        )}
                      </div>

                      <div className="col-md-2 ms-4">
                        <h6 className="text-white">Sqft</h6>

                        {editingField === "sqft" ? (
                          <input
                            className="w-100"
                            type="text"
                            value={editedText} // Controlled input with edited text
                            onChange={(e) => setEditedText(e.target.value)} // Update text as the user types
                            onBlur={handleTextSave} // Save the text when the user leaves the input field
                            autoFocus
                          />
                        ) : (
                          <p
                            className="text-white"
                            onClick={() =>
                              handleTextClick("sqft", "X,000 Sq.Ft.")
                            }
                          >
                            <small>{select.sqft || "X,000 Sq.Ft."}</small>
                          </p>
                        )}
                      </div>

                      <div className="col-md-2 ms-3">
                        <h6 className="text-white">Lot Size</h6>
                        {editingField === "lotSize" ? (
                          <input
                            className="w-100"
                            type="text"
                            value={editedText} // Controlled input with edited text
                            onChange={(e) => setEditedText(e.target.value)} // Update text as the user types
                            onBlur={handleTextSave} // Save the text when the user leaves the input field
                            autoFocus
                          />
                        ) : (
                          <p
                            className="text-white"
                            onClick={() => handleTextClick("lotSize", "0")}
                          >
                            <small>{select.lotSize || "0"}</small>
                          </p>
                        )}
                      </div>

                      <div className="col-md-2 ms-5">
                        <h6 className="text-white">Year Built</h6>
                        {editingField === "yearBuilt" ? (
                          <input
                            className="w-100"
                            type="text"
                            value={editedText} // Controlled input with edited text
                            onChange={(e) => setEditedText(e.target.value)} // Update text as the user types
                            onBlur={handleTextSave} // Save the text when the user leaves the input field
                            autoFocus
                          />
                        ) : (
                          <p
                            className="text-white"
                            onClick={() => handleTextClick("yearBuilt", "2024")}
                          >
                            <small>{select.yearBuilt || "2024"}</small>
                          </p>
                        )}
                      </div>
                    </div>

                    <div className="row mt-2 contactSecond_background">
                      <div className="col-md-6 float-left">
                        <div className="imageLogo w-100">
                          {editingField === "published" ? (
                            <input
                              className="w-100"
                              type="text"
                              value={editedText} // Controlled input with edited text
                              onChange={(e) => setEditedText(e.target.value)} // Update text as the user types
                              onBlur={handleTextSave} // Save the text when the user leaves the input field
                              autoFocus
                            />
                          ) : (
                            <p
                              className="text-white mb-0"
                              onClick={() =>
                                handleTextClick(
                                  "published",
                                  "UtahRealEstate.com"
                                )
                              }
                            >
                              <smal>
                                {select.published || "UtahRealEstate.com"}
                              </smal>
                            </p>
                          )}

                          {editingField === "address" ? (
                            <input
                              className="w-100"
                              type="text"
                              value={editedText} // Controlled input with edited text
                              onChange={(e) => setEditedText(e.target.value)} // Update text as the user types
                              onBlur={handleTextSave}
                              autoFocus
                            />
                          ) : (
                            <span
                              className="text-white mb-0"
                              onClick={() =>
                                handleTextClick("address", "2024 Demo St")
                              }
                            >
                              {select.address || "2024 Demo St"}
                            </span>
                          )}

                          <br />

                          <div className="d-flex">
                            {editingField === "companyCity" ? (
                              <input
                                className="w-100"
                                type="text"
                                value={editedText} // Controlled input with edited text
                                onChange={(e) => setEditedText(e.target.value)} // Update text as the user types
                                onBlur={handleTextSave}
                                autoFocus
                              />
                            ) : (
                              <span
                                className="text-white mb-0"
                                onClick={() =>
                                  handleTextClick(
                                    "companyCity",
                                    "American Fork"
                                  )
                                }
                              >
                                {select.companyCity || "American Fork"},
                              </span>
                            )}

                            {editingField === "companyState" ? (
                              <input
                                type="text"
                                value={editedText} // Controlled input with edited text
                                onChange={(e) => setEditedText(e.target.value)} // Update text as the user types
                                onBlur={handleTextSave}
                                autoFocus
                              />
                            ) : (
                              <span
                                className="text-white mb-0"
                                onClick={() =>
                                  handleTextClick("companyState", "UT")
                                }
                              >
                                {select.companyState || "UT"},
                              </span>
                            )}

                            {editingField === "companyPostCode" ? (
                              <input
                                className="w-100"
                                type="text"
                                value={editedText} // Controlled input with edited text
                                onChange={(e) => setEditedText(e.target.value)} // Update text as the user types
                                onBlur={handleTextSave}
                                autoFocus
                              />
                            ) : (
                              <span
                                className="text-white mb-0"
                                onClick={() =>
                                  handleTextClick("companyPostCode", "84003")
                                }
                              >
                                {select.companyPostCode || "84003"}
                              </span>
                            )}
                          </div>
                        </div>
                      </div>

                      <div className="col-md-6">
                        <ul className="list-unstyled text-end">
                          <img
                            className="print_Profile"
                            src={
                              uploadedContact ||
                              (profile.image ? profile.image : "")
                            } // Show uploaded logo or default avatar
                            style={{
                              width: "100px",
                              cursor: "pointer",
                            }} // Pointer cursor for better UX
                            onClick={handleLogoClickContact} // Show file input on click
                          />
                          {/* Conditionally render file input on logo click */}
                          {showContact && (
                            <label className="documentlabelContact">
                              <input
                                id="REPC_real_estate_purchase_contract"
                                type="file"
                                onChange={handleLogoUploadContact}
                              />
                              <i class="h2 fi-edit opacity-80"></i>
                            </label>
                          )}

                          <div className="d-flex justify-content-end">
                            {editingField === "firstName" ? (
                              <input
                                type="text"
                                value={editedText} // Controlled input with edited text
                                onChange={(e) => setEditedText(e.target.value)} // Update text as the user types
                                onBlur={() =>
                                  handleTextSaveListAgent("firstName")
                                } // Save the text when the user leaves the input field
                                autoFocus
                              />
                            ) : (
                              <p
                                className="text-white mb-0"
                                onClick={() =>
                                  handleTextListAgent(
                                    "firstName",
                                    profile.firstName,
                                    profile.lastName
                                  )
                                }
                              >
                                {profile && (
                                  <p className="mb-0">
                                    <b>Agent: </b>
                                    {profile.firstName}&nbsp;
                                  </p>
                                )}
                              </p>
                            )}

                            {editingField === "lastName" ? (
                              <input
                                type="text"
                                value={editedText} // Controlled input with edited text
                                onChange={(e) => setEditedText(e.target.value)} // Update text as the user types
                                onBlur={() =>
                                  handleTextSaveListAgent("lastName")
                                } // Save the text when the user leaves the input field
                                autoFocus
                              />
                            ) : (
                              <p
                                className="text-white mb-0"
                                onClick={() =>
                                  handleTextListAgent(
                                    "lastName",
                                    profile.lastName
                                  )
                                }
                              >
                                {profile && (
                                  <p className="mb-0">
                                    {" "}
                                    {profile.lastName}&nbsp;
                                  </p>
                                )}
                              </p>
                            )}
                          </div>

                          {editingField === "listOffice" ? (
                            <input
                              type="text"
                              value={editedText} // Controlled input with edited text
                              onChange={(e) => setEditedText(e.target.value)} // Update text as the user types
                              onBlur={() =>
                                handleTextSaveListOffice("listOffice")
                              } // Save the text when the user leaves the input field
                              autoFocus
                            />
                          ) : (
                            <p
                              className="text-white mb-0"
                              onClick={() =>
                                handleTextListOffice(
                                  "listOffice",
                                  profile.active_office
                                )
                              }
                            >
                              {profile && (
                                <p className="mb-0">
                                  <b>Office: </b>
                                  {profile.active_office}
                                </p>
                              )}
                            </p>
                          )}

                          {editingField === "officePhone" ? (
                            <input
                              type="text"
                              value={editedText} // Controlled input with edited text
                              onChange={(e) => setEditedText(e.target.value)} // Update text as the user types
                              onBlur={() =>
                                handleTextSaveOfficePhone("officePhone")
                              } // Save the text when the user leaves the input field
                              autoFocus
                            />
                          ) : (
                            <p
                              className="text-white mb-0"
                              onClick={() =>
                                handleTextOfficePhone(
                                  "officePhone",
                                  profile.phone
                                )
                              }
                            >
                              {profile && (
                                <p className="mb-0">
                                  <b>Phone: </b>
                                  {profile.phone}
                                </p>
                              )}
                            </p>
                          )}
                        </ul>
                      </div>
                    </div>
                  </div>

                  <div className="template_second">
                    <button
                      className="btn btn-secondary pull-right"
                      onClick={() => handleCancel()}
                    >
                      Cancel
                    </button>
                    <PDFDownloadLink
                      document={
                        <FlyerAlternativeThird
                          select={select}
                          uploadedImages={uploadedImages}
                          platformData={uploadedLogo}
                          profile={profile}
                        />
                      }
                      fileName={select.address || "Flyer3.pdf"}
                    >
                      Download Pdf
                    </PDFDownloadLink>
                  </div>
                </section>
              ) : (
                ""
              )}
            </>
          ) : (
            <div className="select_template float-start w-100 mt-3">
              <label className="col-form-label text-white" id="radio-level1">
                Select Template:
              </label>
              <div className="d-flex">
                <div className="form-check mb-0">
                  <img
                    className={templateType === "type1" ? "active" : ""}
                    src={NonFlyer1}
                    alt="Type One"
                    style={{ cursor: "pointer" }}
                    onClick={() => handleType("type1")} // handle the image click
                  />
                </div>
                <div className="form-check mb-0 ms-3">
                  <img
                    className={templateType === "type2" ? "active" : ""}
                    src={NonFlyer2}
                    alt="Type Two"
                    style={{ cursor: "pointer" }}
                    onClick={() => handleType("type2")}
                  />
                </div>
                <div className="form-check mb-0 ms-3">
                  <img
                    className={templateType === "type3" ? "active" : ""}
                    src={NonFlyer3}
                    alt="Type Three"
                    style={{ cursor: "pointer" }}
                    onClick={() => handleType("type3")}
                  />
                </div>
              </div>
            </div>
          )}
        </div>
      </main>
    </div>
  );
};

export default NonListedPrint;

import React, { useState, useEffect } from "react";
import user_service from "../service/user_service";

import Loader from "../../Pages/Loader/Loader.js";
import Toaster from "../../Pages/Toaster/Toaster";
import jwt from "jwt-decode";
import { NavLink } from "react-router-dom";

const BussinessIncome = () => {
  const [loader, setLoader] = useState({ isActive: null });
  const [toaster, setToaster] = useState({
    types: null,
    isShow: null,
    toasterBody: "",
    message: "",
  });

  const [statsReport, setStatsReport] = useState("");
  const statsReportGet = async () => {
    let queryParams = `?page=1`;
    queryParams += `&agentId=${jwt(localStorage.getItem("auth")).id}`;
    await user_service
      .TransactionBussinessReport(queryParams)
      .then((response) => {
        if (response) {
          setStatsReport(response.data);
        }
      });
  };

 
  useEffect(() => {
    statsReportGet();
  }, []);

  const [earnings, setEarnings] = useState(true);

  const handleEarnings = () => {
    setEarnings((prevBack) => !prevBack);
  };

  return (
    <div className="bg-secondary float-left w-100 pt-4 mb-4">
      <Loader isActive={loader.isActive} />
      {toaster.isShow && (
        <Toaster
          types={toaster.types}
          isShow={toaster.isShow}
          toasterBody={toaster.toasterBody}
          message={toaster.message}
        />
      )}
      <main className="page-wrapper">
        <div className="">
          <div className="row">
            <div className="col-md-12">
              <div className="bg-light border rounded-3  mb-4 p-3">
                <div className="">
                  <h3 className="text-left mb-0 justify-content-start d-flex align-items-center">
                    Income
                  </h3>
                </div>
              </div>
              <div className="bg-light p-3 border-0 rounded-3 shadow-sm float-start w-100">
                <div className="row">
                  <div className="table-responsive bg-light border rounded-3 p-3">
                    <table
                      id="DepositLedger"
                      className="table table-striped align-middle mb-0 w-100"
                      border="0"
                      cellspacing="0"
                      cellpadding="0"
                    >
                      <thead>
                        <tr>
                          <th>Transaction Address</th>
                          <th>Income</th>
                        </tr>
                      </thead>
                      <tbody>
                        {Array.isArray(statsReport?.fundingData) &&
                        statsReport?.fundingData.length > 0 ? (
                          <>
                            {statsReport?.fundingData.map((item, index) => (
                              <tr key={index}>
                                <td>
                                  <NavLink
                                    to={`/transaction-summary/${item.transactionId}`}
                                  >
                                    {item.addressProperty}
                                  </NavLink>
                                </td>
                                <td>{item.estimate}</td>
                              </tr>
                            ))}
                            {/* Add the Total Amount Row */}
                            <tr>
                              <td>
                                <strong>Total Amount</strong>
                              </td>
                              <td>
                                <strong>
                                  {statsReport.fundingData
                                    .reduce(
                                      (acc, item) =>
                                        acc + (parseFloat(item.estimate) || 0),
                                      0
                                    )
                                    .toFixed(2)}
                                </strong>
                              </td>
                            </tr>
                          </>
                        ) : (
                          <tr>
                            <td colSpan="2">
                              No Data Found
                            </td>
                          </tr>
                        )}
                      </tbody>
                    </table>
                    <div>
                        <NavLink to={`/bussiness-tab`} className="btn btn-secondary mt-3 pull-right">Back</NavLink>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </main>
    </div>
  );
};

export default BussinessIncome;

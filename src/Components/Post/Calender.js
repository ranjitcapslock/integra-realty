import React, { useState, useEffect } from "react";
import { Calendar as BigCalendar, momentLocalizer } from "react-big-calendar";
import moment from "moment-timezone";
import "react-big-calendar/lib/css/react-big-calendar.css";
import Loader from "../../Pages/Loader/Loader.js";
import Toaster from "../../Pages/Toaster/Toaster.js";
import { NavLink, useNavigate } from "react-router-dom";
import user_service from "../service/user_service";
import jwt from "jwt-decode";
import _ from "lodash";
import BootstrapSwitchButton from "bootstrap-switch-button-react";

import Slider from "react-slick";
import "slick-carousel/slick/slick-theme.css";
import "slick-carousel/slick/slick.css";

const localizer = momentLocalizer(moment);
const settings = {
  dots: true,
  infinite: true,
  speed: 500,
  slidesToShow: 1,
  slidesToScroll: 1,
  autoplay: true,
  autoplaySpeed: 7000,
  arrows: false,
};

const eventColors = {
  meeting: "#FF5733",
  duty: "#3366FF",
};

const Calendar = () => {
  const CustomToolbar = () => {
    return (
      <div className="rbc-toolbar mt-3">
        <span className="rbc-toolbar-label"></span>
        <span className="rbc-btn-group mt-3">
          {/* <button type="button" className="rbc-active">Month</button> */}
          {/* <button type="button">Week</button> */}
          {/* <button type="button">Day</button> */}
          {/* <button type="button">Agenda</button> */}
        </span>
      </div>
    );
  };

  const navigate = useNavigate();

  const initialValues = {
    allDayEvent: "yes",
    category: "Office",
    draft: "",
    endDate: "",
    guestSeats: "",
    information: "",
    publicEvent: "",
    registration: "",
    startDate: "",
    title: "",
  };
  const [loader, setLoader] = useState({ isActive: null });
  const { isActive } = loader;
  const [toaster, setToaster] = useState({
    types: null,
    isShow: null,
    toasterBody: "",
    message: "",
  });
  const { types, isShow, toasterBody, message } = toaster;

  const [calenderGet, setCalenderGet] = useState([]);
  const [events, setEvents] = useState([]);
  const [selectedDate, setSelectedDate] = useState(null);
  const [formValues, setFormValues] = useState(initialValues);
  const [formErrors, setFormErrors] = useState({});
  const [isSubmitClick, setISSubmitClick] = useState(false);
  //  const [calenderId, setCalenderId] = useState([]);
  const [isModalVisible, setIsModalVisible] = useState(false); // State to control modal visibility
  const [currentDate, setCurrentDate] = useState("");
  // const [eventFilter, setEventFilter] = useState("Office");
  const [eventFilter, setEventFilter] = useState(["Office"]); // Store multiple filters
  const [eventFilterSingle, setEventFilterSingle] = useState(""); // Store multiple filters

  useEffect(() => {
    const interval = setInterval(() => {
      setCurrentDate(
        // moment().tz("America/Denver").format("dddd, D MMMM YYYY, h:mm:ss a")
        moment().tz("America/Denver").format("dddd, D MMMM YYYY")
      );
    }, 1000);

    return () => {
      clearInterval(interval);
    };
  }, []);

  const [defaultContent, setDefaultContent] = useState([]);
  const PageContentMarketingGet = async (month) => {
    try {
      const response = await user_service.pageContentMarketingGet();
      if (response) {
        // Filter the content for the selected month
        const filteredContent = response.data.data.filter(
          (item) => item.month === month && item.page === "events"
        );
        setDefaultContent(filteredContent);
      }
    } catch (error) {
      console.error("Error fetching content:", error);
    }
  };

  useEffect(() => {
    const currentMonth = moment().format("MMMM");
    PageContentMarketingGet(currentMonth); // Fetch content for the current month by default
  }, []);

  const handleSelectSlot = (slotInfo) => {
    const today = moment().startOf("day");
    const selectedDate = moment(slotInfo.start).startOf("day");

    if (
      localStorage.getItem("auth") &&
      jwt(localStorage.getItem("auth")).contactType === "admin"
    ) {
      if (selectedDate.isSameOrAfter(today)) {
        setSelectedDate(slotInfo);
        setIsModalVisible(true);
        window.$("#modal-show-calender").modal("show");
      }
    } else if (
      localStorage.getItem("auth") &&
      jwt(localStorage.getItem("auth")).contactType === "staff"
    ) {
      if (selectedDate.isSameOrAfter(today)) {
        setSelectedDate(slotInfo);
        setIsModalVisible(true);
        window.$("#modal-show-calender").modal("show");
      }
    }
  };

  const handleEventClick = (event) => {
    event.preventDefault();
    const eventId = event.currentTarget.getAttribute("data-eventid");
    navigate(`/event-detail/${eventId}`);
  };

  const [selectedMonth, setSelectedMonth] = useState(moment());

  const handleNavigate = (date, view, action) => {
    switch (action) {
      case "TODAY":
        const today = moment();
        setSelectedMonth(today);
        const todayEvents = generateEvents(today);
        setEvents(todayEvents);
        PageContentMarketingGet(today.format("MMMM")); // Fetch content for the current month
        break;

      case "PREV":
        const previousMonth = moment(selectedMonth).subtract(1, "month");
        setSelectedMonth(previousMonth);
        const previousMonthEvents = generateEvents(previousMonth);
        setEvents(previousMonthEvents);
        PageContentMarketingGet(previousMonth.format("MMMM")); // Fetch content for the previous month
        break;

      case "NEXT":
        const nextMonth = moment(selectedMonth).add(1, "month");
        setSelectedMonth(nextMonth);
        const nextMonthEvents = generateEvents(nextMonth);
        setEvents(nextMonthEvents);
        PageContentMarketingGet(nextMonth.format("MMMM")); // Fetch content for the next month
        break;

      default:
        break;
    }
  };

  const [filterData, setFilterData] = useState(false);
  const handleFilter = () => {
    setFilterData(true);
    // setEventFilter(["Office"])
  };

  const handleFilterHide = () => {
    setFilterData(false);
    // setEventFilter(["Office"])
  };

  // const fetchCalendar = async () => {
  //   try {
  //     setLoader({ isActive: true });
  //     let queryParams = ''; // Initialize with pagination

  //     const token = localStorage.getItem("auth");
  //     let userId = null;

  //     // Decode JWT to get the agent's ID
  //     if (token) {
  //       const decodedToken = jwt(token);
  //       userId = decodedToken.id; // Extract the logged-in agent's ID
  //     }

  //     // Add selected filters to query params
  //     if (eventFilter.length > 0) {
  //       const isPersonalSelected = eventFilter.includes("Personal");

  //       // Filter for "Personal" events: only show events created by the logged-in agent
  //       if (isPersonalSelected && userId) {
  //         queryParams += `&category=Personal&agentId=${userId}`;
  //       }

  //       const otherCategories = eventFilter.filter((filter) => filter !== "Personal");
  //       if (otherCategories.length > 0) {
  //         const filterQuery = otherCategories.map((filter) => `category=${filter}`).join('&');
  //         queryParams += `&${filterQuery}`;
  //       }
  //     }

  //     console.log(queryParams);

  //     // Fetch data from the API with the constructed query params
  //     const response = await user_service.CalenderGet(queryParams);
  //     setLoader({ isActive: false });

  //     if (response) {
  //       setCalenderGet(response.data.data); // Set the calendar data
  //     }
  //   } catch (error) {
  //     console.error("Error in fetchCalendar:", error);
  //   }
  // };

  // const toggleFilter = (category) => {
  //   setEventFilter((prevFilters) =>
  //     prevFilters.includes(category)
  //       ? prevFilters.filter((filter) => filter !== category) // Remove filter if already selected
  //       : [...prevFilters, category] // Add filter if not selected
  //   );
  // };

  // Fetch the profile data and set filters

  const profileGetAll = async () => {
    if (localStorage.getItem("auth")) {
      try {
        const response = await user_service.profileGet(
          jwt(localStorage.getItem("auth")).id
        );

        if (response && response.data) {
          const savedFilters = response.data.savedFilters || ["Office"];
          console.log("Saved Filters: ", savedFilters);
          setEventFilter(savedFilters); // Set event filter based on saved filters
        } else {
          console.warn("Profile data not found. Defaulting to 'Office'.");
          setEventFilter(["Office"]); // Default to Office
        }
      } catch (error) {
        console.error("Error fetching profile data:", error);
        setEventFilter(["Office"]); // Fallback to Office filter in case of error
      }
    }
  };

  // Fetch the calendar data based on the filters
  const fetchCalendar = async (currentFilters) => {
    try {
      setLoader({ isActive: true });
      let queryParams = "";

      const token = localStorage.getItem("auth");
      let userId = null;

      if (token) {
        const decodedToken = jwt(token);
        userId = decodedToken.id;
      }

      // Add selected filters to query params
      if (currentFilters && currentFilters.length > 0) {
        const isPersonalSelected = currentFilters.includes("Personal");

        if (isPersonalSelected && userId) {
          queryParams += `&category=Personal&agentId=${userId}`;
        }

        const otherCategories = currentFilters.filter(
          (filter) => filter !== "Personal"
        );
        if (otherCategories.length > 0) {
          const filterQuery = otherCategories
            .map((filter) => `category=${filter}`)
            .join("&");
          queryParams += `&${filterQuery}`;
        }
      }

      // Fetch calendar events based on the query parameters
      const response = await user_service.CalenderGet(queryParams);
      setLoader({ isActive: false });

      if (response) {
        setCalenderGet(response.data.data); // Set calendar events
      }
    } catch (error) {
      console.error("Error in fetchCalendar:", error);
    }
  };

  // Fetch calendar only after eventFilter is updated
  useEffect(() => {
    if (eventFilter.length > 0) {
      fetchCalendar(eventFilter); // Fetch calendar events with current filters
    }
  }, [eventFilter]);

  useEffect(() => {
    profileGetAll();
  }, []);

  // Toggle individual filters
  const toggleFilter = (category) => {
    const updatedFilters = eventFilter.includes(category)
      ? eventFilter.filter((filter) => filter !== category) // Remove if selected
      : [...eventFilter, category]; // Add if not selected

    setEventFilter(updatedFilters); // Update local state
    saveUserFilters(updatedFilters); // Save to backend
  };

  const saveUserFilters = async (newFilters) => {
    try {
      const token = localStorage.getItem("auth");
      const decodedToken = jwt(token);
      const userId = decodedToken.id; // Extract user ID from JWT

      const updatedUserContact = {
        savedFilters: newFilters, // Set saved filters
      };

      setLoader({ isActive: true });

      // Update user profile with new filter preferences
      await user_service.contactUpdate(userId, updatedUserContact);
      setLoader({ isActive: false });
    } catch (error) {
      console.error("Error saving user filters:", error);
    }
  };

  const generateEvents = () => {
    const generatedEvents = [];

    if (calenderGet) {
      calenderGet.forEach((date) => {
        let startDate, endDate;

        // Check if date.startDate is defined before accessing its properties
        if (date.startDate && date.startDate.includes("/")) {
          startDate = moment
            .tz(date.startDate, "M/D/YYYY", "America/Denver")
            .toDate();
          endDate = moment
            .tz(date.endDate, "M/D/YYYY", "America/Denver")
            .toDate();
        } else if (date.startDate) {
          startDate = moment.tz(date.startDate, "America/Denver").toDate();
          endDate = moment.tz(date.endDate, "America/Denver").toDate();
        } else {
          console.warn("startDate is undefined for:", date);
          return; // Skip this iteration
        }

        const eventId = date._id;
        const eventTitle = date.title;
        const eventType = date.eventType || "none"; // Default to 'none' if no eventType is set

        // Push the original event
        generatedEvents.push({
          title: eventTitle,
          start: startDate,
          end: endDate,
          type: "meeting",
          id: eventId,
        });

        // Handle recurring events based on eventType
        const repeatEvent = (interval, unit) => {
          const repeatedEvents = [];
          for (let i = 1; i <= interval; i++) {
            const newStartDate = moment(startDate).add(i, unit).toDate();
            const newEndDate = moment(endDate).add(i, unit).toDate();
            repeatedEvents.push({
              title: eventTitle,
              start: newStartDate,
              end: newEndDate,
              type: "meeting",
              id: `${eventId}`, // Unique ID for each repeated event
            });
          }
          return repeatedEvents;
        };

        // Generate recurring events based on eventType
        switch (eventType) {
          case "weekly":
            generatedEvents.push(...repeatEvent(52, "weeks")); // Repeat weekly for a year
            break;
          case "monthly":
            generatedEvents.push(...repeatEvent(12, "months")); // Repeat monthly for a year
            break;
          case "yearly":
            generatedEvents.push(...repeatEvent(10, "years")); // Repeat yearly for 10 years
            break;
          default:
            // No recurring event
            break;
        }
      });
    }
    return generatedEvents;
  };

  useEffect(() => {
    const generatedEvents = generateEvents();
    setEvents(generatedEvents);
  }, [calenderGet]);

  const eventStyleGetter = (event, start, end, isSelected) => {
    const isToday = moment(start).isSame(moment(), "day");

    const backgroundColor = isToday ? "#151e43" : eventColors[event.type];
    const borderColor = isToday ? "black" : "transparent";

    return {
      className: isToday ? "rbc-today-row" : "",
      style: {
        backgroundColor,
        borderColor,
      },
    };
  };

  useEffect(() => {
    if (formValues && isSubmitClick) {
      validate();
    }
  }, [formValues]);

  const handleChange = (e) => {
    const { name, value } = e.target;
    setFormValues({ ...formValues, [name]: value });
  };

  const validate = () => {
    const values = formValues;
    const errors = {};
    if (!values.category) {
      errors.category = "category is required";
    }

    if (!values.title) {
      errors.title = "title is required";
    }
    setFormErrors(errors);
    return errors;
  };

  const handleSubmit = async (e, slotInfo) => {
    e.preventDefault();

    if (slotInfo) {
      const startDate = moment(slotInfo.start).format("YYYY-MM-DD");
      const endDate = moment(slotInfo.start).format("YYYY-MM-DD");

      const userData = {
        office_id: localStorage.getItem("active_office_id"),
        office_name: localStorage.getItem("active_office"),
        category: formValues.category,
        allDayEvent: "yes",
        title: formValues.title,
        information: "information",
        registration: "yes",
        seatCount: "24",
        draft: "draft",
        agentId: jwt(localStorage.getItem("auth")).id,
        startDate: startDate,
        endDate: endDate,
        // startTime: "9:00 AM",
        // endTime: "10:00 AM",
        guestSeats: "guestSeats",
        publicEvent: "publicEvent",
      };

      try {
        setLoader({ isActive: true });
        const response = await user_service.CalenderPost(userData);
        if (response && response.data) {
          setLoader({ isActive: false });
          setToaster({
            types: "Calender",
            isShow: true,
            toasterBody: response.data.message,
            message: "Event Post Successfully",
          });
          setTimeout(() => {
            setToaster((prevToaster) => ({ ...prevToaster, isShow: false }));
          }, 1000);

          fetchCalendar();
          document.getElementById("close").click();
        } else {
          setLoader({ isActive: false });
          setToaster({
            types: "error",
            isShow: true,
            toasterBody: response.data.message,
            message: "Error",
          });
        }
      } catch (error) {
        setLoader({ isActive: false });
        setToaster({
          types: "error",
          isShow: true,
          toasterBody: error.message,
          message: "Error",
        });
      }
      setTimeout(() => {
        setToaster({
          types: "error",
          isShow: false,
          toasterBody: null,
          message: "Error",
        });
      }, 1000);
    }
  };

  const [formData, setFormData] = useState([]);

  const StaffRoleGetData = async () => {
    await user_service.StaffRoleGet().then((response) => {
      if (response) {
        console.log(response);
        setFormData(response.data.data);
      }
    });
  };
  useEffect(() => {
    if (formData) {
      StaffRoleGetData();
    }
  }, []);

  const eventColorsFilter = {
    Personal: "var(--calendar-personal-color",
    Office: "#06a773",
    Training: "#fdb10e",
    Staff: "var(--associate-staff-color",
    Birthday: "#445985",
    Anniversary: "#100e17",
    Community: "#43add5",
    Courses: "#ACA3C7",
  };

  return (
    <div className="bg-secondary float-left w-100 pt-4 mb-4">
      <Loader isActive={isActive} />
      {isShow && (
        <Toaster
          types={types}
          isShow={isShow}
          toasterBody={toasterBody}
          message={message}
        />
      )}
      <main className="page-wrapper">
        <div className="row event-calender">
          <div className="col-md-8">
            <div className="d-flex align-items-center justify-content-between">
              <h3 className="text-white mb-0">Calendar</h3>
              {localStorage.getItem("auth") &&
              jwt(localStorage.getItem("auth")).contactType == "admin" ? (
                <>
                  {defaultContent && defaultContent.length > 0 ? (
                    <NavLink
                      to={`/marketing-content/${defaultContent[0]._id}`}
                      className="btn btn-primary"
                    >
                      Update Calendar Content
                    </NavLink>
                  ) : (
                    <NavLink
                      to={`/marketing-content`}
                      className="btn btn-primary"
                    >
                      Add Calendar Content
                    </NavLink>
                  )}
                </>
              ) : (
                ""
              )}
            </div>

            <div className="featured_images mb-4">
              <Slider {...settings}>
                {defaultContent &&
                  defaultContent.length > 0 &&
                  defaultContent.map((post) => {
                    if (post.marketingbanner) {
                      return (
                        <div
                          key={post.marketingbanner}
                          className="carousel-item"
                        >
                          <span className="pull-left align-items-center justify-content-left d-flex w-100">
                            <img
                              alt=""
                              className="person-img w-100"
                              src={
                                post.marketingbanner
                                  ? post.marketingbanner.includes("http")
                                    ? post.marketingbanner
                                    : post.marketingbanner
                                  : ""
                              }
                            ></img>
                          </span>
                        </div>
                      );
                    }
                  })}
              </Slider>
            </div>
            <div className="bg-light border rounded-3 p-3">
              <div className="mb-3">
                {defaultContent && defaultContent.length > 0 ? (
                  defaultContent.map((item) => (
                    <div key={item._id}>
                      <span
                        dangerouslySetInnerHTML={{ __html: item.content }}
                      ></span>
                    </div>
                  ))
                ) : (
                  <div className="">
                    <span>
                      From workshops and webinars to tutorials and courses, our
                      calendar offers a diverse range of learning opportunities
                      to help you grow personally and professionally.
                    </span>
                  </div>
                )}

                <hr className="w-100" />
              </div>
              <div className="input-group d-flex align-items-center justify-content-between">
                {/* <div className="col-lg-6 col-md-6 col-sm-12 col-12 form-outline d-flex">
                  <input
                    id="search-input-1"
                    type="search"
                    className="form-control"
                    placeholder="Find an event."
                  />
                  <NavLink
                    // to="/search-Event"
                    type="button"
                    className="btn btn-primary ms-3"
                  >
                    Search
                  </NavLink>
                </div> */}

                <div className="col-lg-10 col-md-10 col-sm-12 col-12 mt-lg-0 mt-md-0 mt-sm-4 mt-3 d-flex">
                  <select
                    className="form-select ms-lg-0 ms-md-0 ms-sm-0 ms-0"
                    id="pr-city"
                    name="eventFilter"
                    // onChange={(event) => setEventFilter(event.target.value)}
                    // value={eventFilter}
                    onChange={(event) => setEventFilter([event.target.value])} // Set single category
                    value={eventFilter[0]}
                  >
                    <option value="" disabled="">
                      Choose Category
                    </option>
                    <option value="Office">Office</option>
                    <option value="Personal">Personal</option>
                    <option value="Staff">Staff</option>
                    <option value="Training">Training</option>
                    <option value="Birthday">Birthday</option>
                    <option value="Anniversary">Anniversary</option>
                    <option value="Community">Community</option>
                    <option value="Courses">Courses</option>
                  </select>
                  {/* {localStorage.getItem("auth") &&
                  jwt(localStorage.getItem("auth")).contactType == "admin" ? (
                    <NavLink
                      to="/add-Event"
                      className="btn btn-primary btn-sm  ms-3 pull-right"
                    >
                      Add Event
                    </NavLink>
                  ) : (
                    <>
                      {localStorage.getItem("auth") &&
                        jwt(localStorage.getItem("auth")).contactType ==
                          "staff" &&
                        (Array.isArray(formData) && formData.length > 0
                          ? formData.map((item) =>
                              item.roleStaff === "calender" ? (
                                <NavLink
                                  key={item.id}
                                  to="/add-Event"
                                  className="btn btn-primary btn-sm ms-3 pull-right"
                                >
                                  Add Event
                                </NavLink>
                              ) : (
                                ""
                              )
                            )
                          : "")}
                    </>
                  )} */}

                  <NavLink
                    to="/add-Event"
                    className="btn btn-primary btn-sm  ms-3 pull-right"
                  >
                    Add Event
                  </NavLink>

                  {filterData ? (
                    <button
                      className="btn btn-primary btn-sm  ms-3 pull-right"
                      onClick={handleFilterHide}
                    >
                      Hide Filter
                    </button>
                  ) : (
                    <>
                      <button
                        className="btn btn-primary btn-sm  ms-3 pull-right"
                        onClick={handleFilter}
                      >
                        Show Filter
                      </button>
                      {/* {localStorage.getItem("auth") &&
                      jwt(localStorage.getItem("auth")).contactType ==
                        "admin" ? (
                        <button
                          className="btn btn-primary btn-sm  ms-3 pull-right"
                          onClick={handleFilter}
                        >
                          Show Filter
                        </button>
                      ) : (
                        <>
                          {localStorage.getItem("auth") &&
                            jwt(localStorage.getItem("auth")).contactType ==
                              "staff" &&
                            (Array.isArray(formData) && formData.length > 0
                              ? formData.map((item) =>
                                  item.roleStaff === "calender" ? (
                                    <button
                                      className="btn btn-primary btn-sm ms-3 pull-right"
                                      onClick={handleFilter}
                                    >
                                      Show Filter
                                    </button>
                                  ) : (
                                    ""
                                  )
                                )
                              : "")}
                        </>
                      )} */}
                    </>
                  )}
                </div>
              </div>
              <div className="">
                <div className="row mt-3 pt-0">
                  <div style={{ height: "500px" }}>
                    <BigCalendar
                      localizer={localizer}
                      events={events}
                      startAccessor="start"
                      endAccessor="end"
                      eventPropGetter={eventStyleGetter}
                      style={{ flex: 1 }}
                      onSelectSlot={handleSelectSlot}
                      onNavigate={handleNavigate}
                      selectable={true}
                      components={{
                        event: ({ event }) => (
                          <div
                            className="rbc-event-content"
                            title={event.title}
                            onClick={handleEventClick}
                            data-eventid={event.id}
                          >
                            {event.title}
                          </div>
                        ),
                      }}
                      views={{
                        month: true,
                        week: false,
                        day: true,
                        agenda: false,
                      }}
                      component={{
                        toolbar: CustomToolbar,
                      }}
                    />
                  </div>
                </div>
              </div>
            </div>
          </div>

          {filterData ? (
            <div className="col-md-4 mt-4">
              <ul className="nav nav-tabs filterCalender mb-2 rounded-3 p-3 w-100">
                <li
                  className="nav-item me-sm-3 mb-4"
                  style={{
                    color: eventFilter.includes("Office")
                      ? "#06a773"
                      : "#f01b2a",
                  }}
                >
                  Office
                  <BootstrapSwitchButton
                    checked={eventFilter.includes("Office")} // Switch is on if "Office" is in eventFilter
                    onlabel="ON"
                    offlabel="OFF"
                    onstyle="success"
                    offstyle="danger"
                    size="lg"
                    onChange={() => toggleFilter("Office")} // Toggle "Office" filter on change
                  />
                </li>

                {/* <li
                  className="nav-item me-sm-3 mb-4"
                  style={{
                    color: eventFilter.includes("Personal") ? "var(--calendar-personal-color" : "#999",
                  }}
                  onClick={() => toggleFilter("Personal")}
                >
                  Personal

                       <BootstrapSwitchButton
                      checked={eventFilter.includes("Personal")}
                      onlabel="ON"
                      offlabel="OFF"
                      onstyle="primary"
                      offstyle="danger"
                      size="lg"
                      onChange={() => toggleFilter("Personal")}
                    />
                </li> */}

                <li
                  className="nav-item me-sm-3 mb-4"
                  style={{
                    color: eventFilter.includes("Staff")
                      ? "var(--associate-staff-color"
                      : "#f01b2a",
                  }}
                  // onClick={() => toggleFilter("Staff")}
                >
                  Staff
                  <BootstrapSwitchButton
                    checked={eventFilter.includes("Staff")}
                    onlabel="ON"
                    offlabel="OFF"
                    onstyle="danger"
                    offstyle="danger"
                    size="lg"
                    onChange={() => toggleFilter("Staff")}
                  />
                </li>

                <li
                  className="nav-item me-sm-3 mb-4"
                  style={{
                    color: eventFilter.includes("Training")
                      ? "#fdb10e"
                      : "#f01b2a",
                  }}
                  // onClick={() => toggleFilter("Training")}
                >
                  Training
                  <BootstrapSwitchButton
                    checked={eventFilter.includes("Training")}
                    onlabel="ON"
                    offlabel="OFF"
                    onstyle="warning"
                    offstyle="danger"
                    size="lg"
                    onChange={() => toggleFilter("Training")}
                  />
                </li>

                <li
                  className="nav-item me-sm-3 mb-4"
                  style={{
                    color: eventFilter.includes("Birthday")
                      ? "#445985"
                      : "#f01b2a",
                  }}
                  // onClick={() => toggleFilter("Birthday")}
                >
                  Birthday
                  <BootstrapSwitchButton
                    checked={eventFilter.includes("Birthday")}
                    onlabel="ON"
                    offlabel="OFF"
                    onstyle="info"
                    offstyle="danger"
                    size="lg"
                    onChange={() => toggleFilter("Birthday")}
                  />
                </li>

                <li
                  className="nav-item me-sm-3 mb-4"
                  style={{
                    color: eventFilter.includes("Anniversary")
                      ? "#100e17"
                      : "#f01b2a",
                  }}
                  // onClick={() => toggleFilter("Anniversary")}
                >
                  Anniversary
                  <BootstrapSwitchButton
                    checked={eventFilter.includes("Anniversary")}
                    onlabel="ON"
                    offlabel="OFF"
                    onstyle="dark"
                    offstyle="danger"
                    size="lg"
                    onChange={() => toggleFilter("Anniversary")}
                  />
                </li>

                <li
                  className="nav-item me-sm-3 mb-4"
                  style={{
                    color: eventFilter.includes("Community")
                      ? "#43add5"
                      : "#f01b2a",
                  }}
                  // onClick={() => toggleFilter("Community")}
                >
                  Community
                  <BootstrapSwitchButton
                    checked={eventFilter.includes("Community")}
                    onlabel="ON"
                    offlabel="OFF"
                    onstyle="light"
                    offstyle="danger"
                    size="lg"
                    onChange={() => toggleFilter("Community")}
                  />
                </li>

                <li
                  className="nav-item me-sm-3 mb-4"
                  style={{
                    color: eventFilter.includes("Courses")
                      ? "#ACA3C7"
                      : "#f01b2a",
                  }}
                  // onClick={() => toggleFilter("Courses")}
                >
                  Courses
                  <BootstrapSwitchButton
                    checked={eventFilter.includes("Courses")}
                    onlabel="ON"
                    offlabel="OFF"
                    onstyle="secondary"
                    offstyle="danger"
                    size="lg"
                    onChange={() => toggleFilter("Courses")}
                  />
                </li>
              </ul>
            </div>
          ) : (
            // <div className="col-md-4 mt-lg-0 mt-md-0 mt-sm-4 mt-4">
            //   <h3 className="text-white mb-4">Upcoming Events</h3>
            //   <div className="upcoming_calendar_events bg-light border rounded-3 p-3">
            //     {calenderGet && calenderGet.length > 0 ? (
            //       calenderGet
            //         // Generate recurring events and filter by date range
            //         .flatMap((post) => {
            //           const eventStartDate = moment.utc(post.startDate).local();
            //           const today = moment().startOf("day");
            //           const endOfNextMonth = moment()
            //             .add(1, "month")
            //             .endOf("month");

            //           let events = [];

            //           // Check if the event occurs between today and the end of next month
            //           if (
            //             eventStartDate.isSameOrAfter(today) &&
            //             eventStartDate.isSameOrBefore(endOfNextMonth)
            //           ) {
            //             events.push(post); // Add the event itself
            //           }

            //           // Handle recurring events
            //           if (
            //             post.eventType === "weekly" ||
            //             post.eventType === "monthly" ||
            //             post.eventType === "yearly"
            //           ) {
            //             let interval, unit;
            //             switch (post.eventType) {
            //               case "weekly":
            //                 interval = 52;
            //                 unit = "weeks";
            //                 break;
            //               case "monthly":
            //                 interval = 12;
            //                 unit = "months";
            //                 break;
            //               case "yearly":
            //                 interval = 10;
            //                 unit = "years";
            //                 break;
            //               default:
            //                 interval = 0;
            //                 unit = "";
            //             }

            //             // Generate future occurrences of recurring events
            //             for (let i = 1; i <= interval; i++) {
            //               const newStartDate = moment(eventStartDate)
            //                 .add(i, unit)
            //                 .local();

            //               // Filter based on the same date range
            //               if (
            //                 newStartDate.isSameOrAfter(today) &&
            //                 newStartDate.isSameOrBefore(endOfNextMonth)
            //               ) {
            //                 events.push({
            //                   ...post, // Copy the existing event data
            //                   startDate: newStartDate.format(), // Set the new start date for each occurrence
            //                 });
            //               }
            //             }
            //           }

            //           return events;
            //         })
            //         // Sort events by date in ascending order
            //         .sort((a, b) =>
            //           moment(a.startDate).diff(moment(b.startDate))
            //         )
            //         .map((post) => {
            //           const eventColor =
            //             eventColorsFilter[post.category] || "#000"; // Default color if none found

            //           return (
            //             <ul
            //               key={post._id}
            //               style={{ borderRight: `4px solid ${eventColor}` }}
            //               className="list-unstyled bg-light shadow rounded-3 mb-3 p-3"
            //             >
            //               <li className="d-flex align-items-center mb-0">
            //                 {/* Icon with color-coded background */}
            //                 <span
            //                   className="me-3 p-2 rounded-circle"
            //                   style={{
            //                     backgroundColor: eventColor,
            //                     color: "#fff",
            //                   }}
            //                 ></span>

            //                 {/* Event clickable link */}
            //                 <NavLink
            //                   to={`/event-detail/${post._id}`}
            //                   className="d-block w-100"
            //                   style={{ color: eventColor }}
            //                 >
            //                   <strong>
            //                     {moment(post.startDate).format("MM/DD/YYYY")}:
            //                   </strong>{" "}
            //                   {post.title}
            //                 </NavLink>

            //                 <NavLink
            //                   className="calendar_title"
            //                   to={`/event-detail/${post._id}`}
            //                   style={{ color: eventColor }}
            //                 >
            //                   {post.category}
            //                 </NavLink>
            //               </li>
            //             </ul>
            //           );
            //         })
            //     ) : (
            //       <p>No Events Scheduled</p>
            //     )}
            //   </div>
            // </div>
            <div className="col-md-4 mt-lg-0 mt-md-0 mt-sm-4 mt-4">
              <h3 className="text-white mb-4">Upcoming Events</h3>
              <div className="upcoming_calendar_events bg-light border rounded-3 p-3">
                <p>Today: {currentDate}</p>
                {calenderGet && calenderGet.length > 0 ? (
                  calenderGet
                    // Sort events by start date in ascending order (oldest to most recent)
                    .sort((a, b) =>
                      moment(a.startDate).diff(moment(b.startDate))
                    )
                    // Filter to show only events from today onward
                    .filter((post) => {
                      const eventStartDate = moment.utc(post.startDate).local(); // Event start date
                      const today = moment().startOf("day"); // Today's date

                      // Check if the event is today or in the future
                      return eventStartDate.isSameOrAfter(today, "day");
                    })
                    .map((post) => {
                      const eventColor =
                        eventColorsFilter[post.category] || "#000"; // Fallback color

                      return (
                        <ul
                          key={post._id}
                          style={{ borderRight: `4px solid ${eventColor}` }}
                          className="list-unstyled bg-light shadow rounded-3 mb-3 p-3"
                        >
                          <li className="d-flex align-items-center mb-0">
                            {/* Icon with color-coded background */}
                            <span
                              className="me-3 p-2 rounded-circle"
                              style={{
                                backgroundColor: eventColor,
                                color: "#fff",
                              }}
                            ></span>

                            {/* Make the event clickable, redirecting to event detail */}
                            <NavLink
                              to={`/event-detail/${post._id}`} // Event page path, ensure your routes match
                              className="d-block w-100"
                              style={{ color: eventColor }}
                            >
                              <strong>
                                {moment(post.startDate).format("MM/DD/YYYY")}:
                              </strong>{" "}
                              {post.title}
                            </NavLink>

                            <NavLink
                              className="calendar_title"
                              to={`/event-detail/${post._id}`} // Additional link for event category
                              style={{ color: eventColor }}
                            >
                              {post.category}
                            </NavLink>
                          </li>
                        </ul>
                      );
                    })
                ) : (
                  <p>No Events Scheduled</p>
                )}
              </div>
            </div>
          )}
        </div>

        {isModalVisible && (
          <>
            <div className="modal" role="dialog" id="modal-show-calender">
              <div
                className="modal-dialog modal-lg modal-dialog-scrollable"
                role="document"
              >
                <div className="modal-content">
                  <div className="modal-header">
                    <h4 className="modal-title">Add an Event</h4>
                    <button
                      className="btn-close"
                      type="button"
                      data-bs-dismiss="modal"
                      aria-label="Close"
                    ></button>
                  </div>
                  <div className="modal-body fs-sm">
                    <div className="col-md-12">
                      <div className="mb-3">
                        <label className="form-label" for="pr-city">
                          Event<span className="text-danger">*</span>
                        </label>
                        <input
                          className="form-control"
                          id="inline-form-input"
                          type="text"
                          placeholder="Title"
                          name="title"
                          onChange={handleChange}
                          value={formValues.title}
                          style={{
                            border: formErrors?.title ? "1px solid red" : "",
                          }}
                        />
                        <div className="invalid-tooltip">
                          {formErrors.title}
                        </div>
                      </div>
                      <div className="mb-3">
                        <label className="form-label" for="pr-city">
                          Category
                        </label>
                        <select
                          className="form-select"
                          id="pr-city"
                          name="category"
                          onChange={handleChange}
                          value={formValues.category}
                          style={{
                            border: formErrors?.category ? "1px solid red" : "",
                          }}
                        >
                          <option value="Office">Office</option>
                          <option value="Personal">Personal</option>
                          <option value="Staff">Staff</option>
                          <option value="Training">Training</option>
                          <option value="Community">Community</option>
                          <option value="Courses">Courses</option>
                        </select>
                        <div className="invalid-tooltip">
                          {formErrors.category}
                        </div>
                      </div>
                      <div className="pull-right mt-3">
                        {localStorage.getItem("auth") &&
                        jwt(localStorage.getItem("auth")).contactType ==
                          "admin" ? (
                          <button
                            onClick={(e) => handleSubmit(e, selectedDate)}
                            type="button"
                            className="btn btn-primary btn-sm "
                            id="save-button"
                          >
                            Add Event
                          </button>
                        ) : (
                          <>
                            {localStorage.getItem("auth") &&
                              jwt(localStorage.getItem("auth")).contactType ==
                                "staff" &&
                              (Array.isArray(formData) && formData.length > 0
                                ? formData.map((item) =>
                                    item.roleStaff === "calender" ? (
                                      <button
                                        onClick={(e) =>
                                          handleSubmit(e, selectedDate)
                                        }
                                        type="button"
                                        className="btn btn-primary btn-sm "
                                        id="save-button"
                                      >
                                        Add Event
                                      </button>
                                    ) : (
                                      ""
                                    )
                                  )
                                : "")}
                          </>
                        )}
                        <button
                          type="button"
                          className="btn btn-secondary btn-sm ms-3"
                          id="close"
                          data-bs-dismiss="modal"
                        >
                          Cancel & Close
                        </button>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </>
        )}
      </main>
    </div>
  );
};

export default Calendar;

import React, { useState, useEffect, useRef } from "react";
import { useNavigate, NavLink, useParams } from "react-router-dom";
import user_service from "../service/user_service";
import Loader from "../../Pages/Loader/Loader.js";
import Toaster from "../../Pages/Toaster/Toaster.js";
import _ from "lodash";
import axios from "axios";
import jwt from "jwt-decode";
import { Editor } from "react-draft-wysiwyg";
import { EditorState, ContentState, convertToRaw } from "draft-js";
import draftToHtml from "draftjs-to-html";
import htmlToDraft from "html-to-draftjs";
import "react-draft-wysiwyg/dist/react-draft-wysiwyg.css";

function UpdatePost() {
  const [postData, setPostdData] = useState(true);
  const [step1, setStep1] = useState("");
  const [category, setCategory] = useState("");
  const [step2, setStep2] = useState("");
  const [channel, setChannel] = useState("");
  const [step3, setStep3] = useState("");
  const [step4, setStep4] = useState("");

  const [loader, setLoader] = useState({ isActive: null });
  const { isActive } = loader;
  const [toaster, setToaster] = useState({
    types: null,
    isShow: null,
    toasterBody: "",
    message: "",
  });
  const { types, isShow, toasterBody, message } = toaster;

  const [file, setFile] = useState(null);
  const [data, setData] = useState("");

  const [editData, setEditData] = useState(false);
  const [pinArticle, setPinArticle] = useState("");
  const [checkPublication, setCheckPublication] = useState("");
  const [apiResponse, setApiResponse] = useState("");
  const [postResponce, setPostResponce] = useState("");
  const [fullMessageResponce, setFullMessageResponce] = useState("");

  const params = useParams();

  const initialValues = {
    category: "",
    channel: "",
    fullMessage: "",
    postBlurb: "",
    postTitle: "",
    author: "",
    articleComments: "1",
    linkURL: "",
    publicationLevel: "1",
    publishDate: "",
    file: "",
    pin_date: "",
    specific: "Corporate",
    pin_article: "",
  };
  const [formValues, setFormValues] = useState(initialValues);
  const [formErrors, setFormErrors] = useState({});

  const navigate = useNavigate();

  const editorRef = useRef(null);
  const log = () => {
    if (editorRef.current) {
      console.log(editorRef.current.getContent());
    }
  };

  const handleEdit = () => {
    setEditData(true);
  };

  const handlePinArticle = (e) => {
    setPinArticle(e.target.name);
    setPinArticle(e.target.value);
  };

  {
    /* <!-- Input onChange Start--> */
  }
  const handleChange = (e) => {
    console.log(e.target.value);
    const { name, value } = e.target;
    setFormValues({ ...formValues, [name]: value });
  };

  const CheckList = (e) => {
    setCheckPublication(e.target.name);
    setCheckPublication(e.target.value);
  };

  const [editorState, setEditorState] = useState(EditorState.createEmpty());

  const handleChanges = (editorState) => {
    setEditorState(editorState);
    const htmlContent = draftToHtml(
      convertToRaw(editorState.getCurrentContent())
    );
    setFormValues({ ...formValues, eventDescription: htmlContent });
  };

  const handleFileUpload = async (e) => {
    const selectedFile = e.target.files[0];

    if (selectedFile) {
      setFile(selectedFile);

      const formData = new FormData();
      formData.append("file", selectedFile);

      const config = {
        headers: {
          Accept: "application/json",
          "Content-Type": "multipart/form-data",
          Authorization: `Bearer ${localStorage.getItem("auth")}`,
        },
      };

      try {
        const uploadResponse = await axios.post(
          "https://api.brokeragentbase.com/upload",
          formData,
          config
        );
        const uploadedFileData = uploadResponse.data;
        setData(uploadedFileData);

        const updatedProfile = {
          // ...formValues,
          file: uploadedFileData,
        };

        console.log(updatedProfile);

        setLoader({ isActive: true });
        await user_service
          .postUpdate(params.id, updatedProfile)
          .then((response) => {
            if (response) {
              const PostGetById = async () => {
                await user_service.postDetail(params.id).then((response) => {
                  setLoader({ isActive: false });
                  if (response) {
                    setFormValues(response.data);
                    const PostCategory = response.data.category;
                    setCategory(PostCategory);
                    const PostChannel = response.data.channel;
                    setChannel(PostChannel);
                  }
                });
              };
              PostGetById();

              setLoader({ isActive: false });
              setToaster({
                type: "Profile Update",
                isShow: true,
                toasterBody: response.data.message,
                message: "Profile Update Successfully",
              });

              setTimeout(() => {
                setToaster((prevToaster) => ({
                  ...prevToaster,
                  isShow: false,
                }));
              }, 500);
            } else {
              setLoader({ isActive: false });
              setToaster({
                type: "error",
                isShow: true,
                toasterBody: response.data.message,
                message: "Error",
              });

              setTimeout(() => {
                setToaster((prevToaster) => ({
                  ...prevToaster,
                  isShow: false,
                }));
              }, 500);
            }
          });
      } catch (error) {
        console.error("Error occurred during file upload:", error);
        setLoader({ isActive: false });
      }
    }
  };

  {
    /* <!-- Input onChange End--> */
  }

  {
    /* <!-- Api call  Function Start--> */
  }
  const PostAll = async (e) => {
    e.preventDefault();

    const userData = {
      channel: channel,
      category: category,
      fullMessage: formValues.fullMessage,
      postBlurb: formValues.postBlurb,
      postTitle: formValues.postTitle,
      file: data,
      author: jwt(localStorage.getItem("auth")).id,
      articleComments: formValues.articleComments,
      linkURL: formValues.linkURL,
      publicationLevel: checkPublication,
      publishDate: formValues.publishDate
        ? formValues.publishDate
        : defaultValue
        ? defaultValue
        : apiResponse,
      pin_article: pinArticle,
      specific: formValues.specific,
      pin_date: formValues.pin_date ? formValues.pin_date : "",
    };
    console.log(userData);
    setLoader({ isActive: true });
    await user_service.postUpdate(params.id, userData).then((response) => {
      if (response) {
        setFormValues(response.data);
        setLoader({ isActive: false });
        setToaster({
          type: "Posted Successfully",
          isShow: true,
          toasterBody: response.data.message,
          message: "Posted Successfully",
        });
        setTimeout(() => {
          navigate("/ ");
        }, 500);
      } else {
        setLoader({ isActive: false });
        setToaster({
          types: "error",
          isShow: true,
          toasterBody: response.data.message,
          message: "Error",
        });
      }
    });
  };
  {
    /* <!-- Api call  Function End--> */
  }

  {
    /* <!-- Add value  Function Start--> */
  }
  const AssociatePost = (stepno, value) => {
    if (stepno === "1") {
      setStep1(stepno);
      setCategory(value);
      setPostdData(false);
      //console.log(value)
    }
    if (stepno === "2") {
      setStep2(stepno);
      setChannel(value);
      setPostdData(false);

      //console.log(value)
    }
    if (stepno === "3") {
      setStep3(stepno);
      setCategory(value);
      setPostdData(false);

      //console.log(value)
    }
    if (stepno === "4") {
      setStep4(stepno);
      setChannel(value);
      setPostdData(false);

      //console.log(value)
    } else {
      setStep1(stepno);
      setPostdData(false);
    }
  };
  {
    /* <!-- Add value  Function End--> */
  }

  const TransactionStepBack = (step, value) => {
    if (step === "step2") {
      setPostdData(false);

      setStep1("");
      setStep2("");
      setStep3("");
      setStep4("");
    }
    if (step === "step3") {
      setPostdData(false);

      setStep2("");
      setStep3("");
      setStep4("");
    }

    if (step === "step4") {
      setPostdData(false);
      setStep1("");
      setStep2("");
      setStep3("");
      setStep4("");
    }
  };

  const currentDate = new Date();
  const day = currentDate.getDate().toString().padStart(2, "0");
  const month = (currentDate.getMonth() + 1).toString().padStart(2, "0");
  const year = currentDate.getFullYear();

  const hours = currentDate.getHours().toString().padStart(2, "0");
  const minutes = currentDate.getMinutes().toString().padStart(2, "0");

  const defaultValue = formValues.publishDate
    ? ""
    : `${year}-${month}-${day}T${hours}:${minutes}`;

  const PostGetById = async () => {
    await user_service.postDetail(params.id).then((response) => {
      setLoader({ isActive: false });
      if (response) {
        setFormValues(response.data);
        const contentBlock = htmlToDraft(response.data.fullMessage);
        if (contentBlock) {
          const contentState = ContentState.createFromBlockArray(
            contentBlock.contentBlocks
          );
          setEditorState(EditorState.createWithContent(contentState));
        }
        const PostCategory = response.data.category;
        setCategory(PostCategory);
        const PostChannel = response.data.channel;
        setChannel(PostChannel);

        const apiPublishDate = new Date(response.data.publishDate);
        apiPublishDate.setHours(apiPublishDate.getHours() + 24);
        const formattedPublishDate = apiPublishDate.toISOString().slice(0, 16);
        setApiResponse(formattedPublishDate);
        // const fullMessageData = response.data.fullMessage;
        // setFormValues(fullMessageData);
        const imageData = response.data.file;
        setPostResponce(imageData);
      }
    });
  };

  useEffect(() => {
    window.scrollTo(0, 0);
    if (params.id) {
      PostGetById(params.id);
      setLoader({ isActive: true });
    }
  }, [params.id]);

  const handleCloseData = () => {
    navigate("/");
  };
  return (
    <div className="bg-secondary float-left w-100 pt-4 mb-4">
      <Loader isActive={isActive} />
      {isShow && (
        <Toaster
          types={types}
          isShow={isShow}
          toasterBody={toasterBody}
          message={message}
        />
      )}
      <main className="page-wrapper profile_page_wrap">
        {/* <!-- Page container--> */}
        <div className="content-overlay">
          {postData ? (
            <>
              <div className="row">
                <div className="col-md-8">
                  <div className="bg-light border rounded-3 p-3">
                    <p>
                      <i className="fa fa-check" aria-hidden="true"></i> You
                      selected the '{category}' category.&nbsp;
                      <a onClick={(e) => TransactionStepBack("step2")}>
                        <NavLink>Change</NavLink>
                      </a>
                    </p>
                    <p>
                      <i className="fa fa-check" aria-hidden="true"></i> You
                      selected the '{channel}' .&nbsp;
                      <a onClick={(e) => TransactionStepBack("step3")}>
                        <NavLink>Change</NavLink>
                      </a>
                    </p>
                    <hr />
                    <h5 className="h4 mb-2 mt-3">Add details for this post:</h5>
                    <form className="needs-validation">
                      <div className="mb-3">
                        <h6>Attached Media</h6>
                        <img
                          className="blogImage"
                          src={postResponce}
                          alt="image"
                        />

                        <div className="mt-3">
                          <label className="form-label btn btn-sm btn-primary">
                            Upload Media
                            <input
                              className="btn btn-primary pull-right ms-5 mt-2 d-none"
                              id="inline-form-input"
                              type="file"
                              name="image"
                              onChange={handleFileUpload}
                            />
                          </label>
                        </div>

                        {/* <button className="btn btn-primary pull-right ms-5 mt-2" type="button">Upload Media</button> */}
                      </div>
                      <div className="mb-3">
                        <label className="col-form-label">
                          Post Title &nbsp;
                          <span className="required">*</span>
                        </label>
                        <input
                          className="form-control"
                          type="text"
                          id="text-input"
                          name="postTitle"
                          placeholder="Enter your post title"
                          onChange={handleChange}
                          autoComplete="on"
                          style={{
                            border: formErrors?.postTitle
                              ? "1px solid red"
                              : "1px solid #00000026",
                          }}
                          value={formValues.postTitle}
                        />
                        {formErrors.postTitle && (
                          <div className="invalid-tooltip">
                            {formErrors.postTitle}
                          </div>
                        )}
                      </div>
                      {console.log(apiResponse)}
                      <div className="mb-3">
                        <label className="col-form-label">Publish Date</label>
                        <input
                          className="form-control"
                          type="datetime-local"
                          id="text-input"
                          name="publishDate"
                          onChange={handleChange}
                          autoComplete="on"
                          value={
                            formValues.publishDate
                              ? formValues.publishDate
                              : apiResponse
                          }
                        />
                      </div>

                      <div className="mb-3">
                        <label className="col-form-label">
                          Link URL &nbsp;<span className="option">*</span>
                        </label>
                        <br />
                        <input
                          className="form-control"
                          type="text"
                          id="text-input"
                          name="linkURL"
                          onChange={handleChange}
                          autoComplete="on"
                          value={formValues.linkURL}
                        />
                      </div>

                      <div className="mb-3">
                        <label className="col-form-label float-left w-100 mb-0">
                          Post Blurb &nbsp;
                          <span className="required">*</span>
                          <span>
                            <small className="pull-right">190chr</small>
                          </span>
                        </label>
                        <textarea
                          className="form-control mt-0"
                          id="textarea-input"
                          rows="5"
                          name="postBlurb"
                          onChange={handleChange}
                          autoComplete="on"
                          value={formValues.postBlurb}
                          style={{
                            border: formErrors?.postBlurb
                              ? "1px solid red"
                              : "1px solid #00000026",
                          }}
                        >
                          Hello World!
                        </textarea>
                        {formErrors.postBlurb && (
                          <div className="invalid-tooltip">
                            {formErrors.postBlurb}
                          </div>
                        )}
                      </div>

                      <div className="mb-3">
                        <label className="col-form-label">Full Message</label>
                        <Editor
                          editorState={editorState}
                          onEditorStateChange={handleChanges}
                          value={formValues.fullMessage}
                          toolbar={{
                            options: [
                              "inline",
                              "blockType",
                              "fontSize",
                              "list",
                              "textAlign",
                              "history",
                              "link", // Add link option here
                            ],
                            inline: {
                              options: [
                                "bold",
                                "italic",
                                "underline",
                                "strikethrough",
                              ],
                            },
                            list: { options: ["unordered", "ordered"] },
                            textAlign: {
                              options: ["left", "center", "right"],
                            },
                            history: { options: ["undo", "redo"] },
                            link: {
                              // Configure link options
                              options: ["link", "unlink"],
                            },
                          }}
                          wrapperClassName="demo-wrapper"
                          editorClassName="demo-editor"
                        />
                      </div>

                      {/* <!-- Inline radios --> */}
                      <div className="mb-3">
                        <label className="col-form-label w-100">
                          Publication Level
                        </label>
                        <p className="float-left w-100 mb-2">
                          Where should your post be shown?
                        </p>
                        <div
                          className="form-check d-flex "
                          onClick={(e) => CheckList(e)}
                        >
                          <div className="">
                            <input
                              className="form-check-input "
                              id="form-radio-four"
                              type="radio"
                              name="publicationLevel"
                              value="office"
                            />
                            <label className="form-check-label">
                              My Office Only
                            </label>
                          </div>
                          <div className="ms-5">
                            <input
                              className="form-check-input"
                              id="form-radio-4"
                              type="radio"
                              name="publicationLevel"
                              value="regional"
                            />
                            <label className="form-check-label ms-2">
                              Regional Offices
                            </label>
                          </div>

                          <div className="ms-5">
                            <input
                              className="form-check-input"
                              id="form-radio-4"
                              type="radio"
                              name="publicationLevel"
                              value="choose"
                            />
                            <label className="form-check-label ms-2">
                              Let Me Choose
                            </label>
                          </div>
                        </div>
                      </div>

                      {checkPublication === "choose" ? (
                        <>
                          <div className="mb-3">
                            <label className="col-form-label">
                              Specific Office
                            </label>
                            <select
                              className="form-select mt-2"
                              id="pr-city"
                              name="specific"
                              onChange={handleChange}
                              value={formValues.specific}
                            >
                              <option></option>
                              <option>Corporate</option>
                            </select>
                          </div>
                        </>
                      ) : (
                        ""
                      )}

                      <label className="col-form-label w-100">
                        Article Comments
                      </label>
                      <div className="mb-3">
                        <div className="form-check">
                          <input
                            className="form-check-input"
                            type="checkbox"
                            id="invalidCheck"
                            name="articleComments"
                            onChange={handleChange}
                            value="Yes"
                            required
                          />
                          <label className="form-check-label">
                            Yes, Allow everyone in the office to comment on this
                            post.
                          </label>
                        </div>
                      </div>

                      <div className="d-flex">
                        <label className="col-form-label p-0">
                          Pin Article
                        </label>
                        &nbsp;&nbsp;<span>STAFF ONLY OPTION</span>
                      </div>
                      <div className="my-2">
                        <div className="form-check">
                          <input
                            className="form-check-input"
                            type="checkbox"
                            id="invalidCheck"
                            name="pin_article"
                            onClick={(e) => handlePinArticle(e)}
                            value="Yes"
                            required
                          />
                          <label className="form-check-label">
                            Yes, Pin this article above standard posts.
                          </label>
                        </div>
                      </div>

                      {pinArticle === "Yes" ? (
                        <>
                          <div className="mb-3">
                            <label className="col-form-label">
                              Pin Removal Date
                            </label>
                            <input
                              className="form-control"
                              type="date"
                              id="text-input"
                              name="pin_date"
                              onChange={handleChange}
                              autoComplete="on"
                              value={formValues.pin_date}
                            />
                          </div>
                        </>
                      ) : (
                        ""
                      )}
                    </form>
                  </div>
                  <div className="pull-right mt-4">
                    <button
                      className="btn btn-secondary btn-sm"
                      onClick={handleCloseData}
                    >
                      Cancel & Go Back
                    </button>
                    <button
                      type="button"
                      className="btn btn-primary btn-sm ms-3"
                      id="save-button"
                      onClick={PostAll}
                    >
                      Publish Post
                    </button>
                  </div>
                </div>
                <div className="col-md-4 mt-lg-0 mt-md-0 mt-sm-5 mt-5">
                  <div id="accordionCards" className="">
                    <div className="card bg-secondary mb-2 mt-0">
                      <div className="card-body">
                        <h3 className="h3 card-title pt-1 mb-0">
                          ✅ Post Status
                        </h3>
                      </div>
                      <div
                        className="collapse show"
                        id="cardCollapse1"
                        data-bs-parent="#accordionCards"
                      >
                        <div className="card-body mt-n1 pt-0">
                          <ul className="list-unstyled mb-2">
                            <li className="d-flex align-items-center mb-2">
                              {category ? (
                                <>
                                  <i className="fi-check text-primary me-2"></i>
                                  <a
                                    className="nav-link fw-normal ps-1 p-0"
                                    href="#basic-info"
                                    data-scroll=""
                                    data-scroll-offset="20"
                                  >
                                    Category
                                  </a>
                                </>
                              ) : (
                                <>
                                  <img src="https://cdn.pboffice.net/shared/transaction/page_select_drop.png" />
                                  <a
                                    className="nav-link fw-normal ps-1 p-0"
                                    href="#basic-info"
                                    data-scroll=""
                                    data-scroll-offset="20"
                                  >
                                    Category
                                  </a>
                                </>
                              )}
                            </li>

                            <li className="d-flex align-items-center mb-2">
                              {channel ? (
                                <>
                                  <i className="fi-check text-primary me-2"></i>
                                  <a
                                    className="nav-link fw-normal ps-1 p-0"
                                    href="#basic-info"
                                    data-scroll=""
                                    data-scroll-offset="20"
                                  >
                                    Channel
                                  </a>
                                </>
                              ) : (
                                <>
                                  <img src="https://cdn.pboffice.net/shared/transaction/page_select_drop.png" />
                                  <a
                                    className="nav-link fw-normal ps-1 p-0"
                                    href="#basic-info"
                                    data-scroll=""
                                    data-scroll-offset="20"
                                  >
                                    Channel
                                  </a>
                                </>
                              )}
                            </li>
                          </ul>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </>
          ) : (
            <>
              <h3 className="text-white">Post an Entry</h3>

              {/* <!-- Step 1 start here--> */}
              {step1 === "" && step3 === "" ? (
                <div className="bg-light rounded-3 border p-3 mb-0">
                  <p>
                    With your help, the office can stay connected with the real
                    estate industry.
                    <br />
                    Be sure to review the Office Article Policy before posting a
                    new article.
                  </p>
                  <h5 className="mb-4">
                    Let's get started, select a category:
                  </h5>
                  <div className="col-lg-12 mb-2">
                    <div
                      className="border rounded-3 p-3 selected_hover"
                      onClick={() => AssociatePost("1", "OfficePost")}
                    >
                      <div className="d-flex align-items-center justify-content-between">
                        <div className="pe-2">
                          <label className="form-label fw-bold">
                            Office Post
                          </label>
                          <div id="type-value">
                            Promote key events and topics. Available to office
                            staff only.
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="col-lg-12 mb-2">
                    <div
                      className="border rounded-3 p-3 selected_hover"
                      onClick={() => AssociatePost("1", "groupPost")}
                    >
                      <div className="d-flex align-items-center justify-content-between">
                        <div className="pe-2">
                          <label className="form-label fw-bold">
                            Group Post
                          </label>
                          <div id="type-value">
                            Share ideas and announcements to fellow group
                            members.
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="col-lg-12 mb-2">
                    <div
                      className="border rounded-3 p-3 selected_hover"
                      onClick={() => AssociatePost("1", "associate")}
                    >
                      <div className="d-flex align-items-center justify-content-between">
                        <div className="pe-2">
                          <label className="form-label fw-bold">
                            Associate Post
                          </label>
                          <div id="type-value">
                            Add a Have/Want, Open House, or an announcement.
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="col-lg-12 mb-2">
                    <div
                      className="border rounded-3 p-3 selected_hover"
                      onClick={() => AssociatePost("3", "Community")}
                    >
                      <div className="d-flex align-items-center justify-content-between">
                        <div className="pe-3">
                          <label className="form-label fw-bold">
                            Community News
                          </label>
                          <div id="type-value">
                            Link to industry news and promote a mixer.
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              ) : (
                ""
              )}
              {/* <!-- Step 1 ends here--> */}

              {/* <!-- Step 2 starts here--> */}
              {step1 && step3 === "" && step2 === "" ? (
                <div className="bg-light rounded-3 p-3 mb-0">
                  <p>
                    <i className="fa fa-check" aria-hidden="true"></i> You
                    selected the '{category}' category. &nbsp;
                    <a onClick={(e) => TransactionStepBack("step2")}>
                      <NavLink>Change</NavLink>
                    </a>
                  </p>

                  <h2 className="h4 mb-2">Select a channel:</h2>
                  <div className="col-lg-12 mb-2">
                    <div
                      className="border rounded-3 p-3 selected_hover"
                      onClick={() => AssociatePost("2", "Agent Announcement")}
                    >
                      <div className="d-flex align-items-center justify-content-between">
                        <div className="pe-2">
                          <label className="form-label fw-bold">
                            Agent Announcement
                          </label>
                          <div id="type-value">
                            Publish a message to fellow associates.
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>

                  <div className="col-lg-12 mb-2">
                    <div
                      className="border rounded-3 p-3 selected_hover"
                      onClick={() => AssociatePost("2", "Have Item/Listing")}
                    >
                      <div className="d-flex align-items-center justify-content-between">
                        <div className="pe-2">
                          <label className="form-label fw-bold">
                            Have Item/Listing
                          </label>
                          <div id="type-value">
                            Offer a listing or service you have.
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="col-lg-12 mb-2">
                    <div
                      className="border rounded-3 p-3 selected_hover"
                      onClick={() => AssociatePost("2", "Want Buyer/Vendor")}
                    >
                      <div className="d-flex align-items-center justify-content-between">
                        <div className="pe-2">
                          <label className="form-label fw-bold">
                            Want Buyer/Vendor
                          </label>
                          <div id="type-value">
                            Identify a type of buyer or vendor you are looking
                            for.
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="col-lg-12 mb-2">
                    <div
                      className="border rounded-3 p-3 selected_hover"
                      onClick={() => AssociatePost("2", "Open House")}
                    >
                      <div className="d-flex align-items-center justify-content-between">
                        <div className="pe-2">
                          <label className="form-label fw-bold">
                            Open House
                          </label>
                          <div id="type-value">
                            Publish an upcoming open house event.
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              ) : (
                ""
              )}
              {/* <!-- Step 2 ends here--> */}

              {/* <!-- Step 3 starts here--> */}
              {step2 && step3 === "" ? (
                <div className="row">
                  <div className="col-md-8">
                    <div className="bg-light rounded-3 border p-3 mb-0">
                      <p>
                        <i className="fa fa-check" aria-hidden="true"></i> You
                        selected the '{category}' category.&nbsp;
                        <a onClick={(e) => TransactionStepBack("step2")}>
                          <NavLink>Change</NavLink>
                        </a>
                      </p>

                      <p>
                        <i className="fa fa-check" aria-hidden="true"></i> You
                        selected the '{channel}' .&nbsp;
                        <a onClick={(e) => TransactionStepBack("step3")}>
                          <NavLink>Change</NavLink>
                        </a>
                      </p>
                      <hr />

                      <h5 className="h4 mb-2 mt-3">
                        Add details for this post:
                      </h5>

                      <form className="needs-validation">
                        <div className="mb-3">
                          <h6>Attached Media</h6>
                          <img
                            className="blogImage"
                            src={postResponce}
                            alt="image"
                          />
                          <div className="mt-3">
                            <label className="form-label btn btm-sm btn-primary">
                              Upload Media
                              <input
                                className="btn btn-primary pull-right ms-5 mt-2 d-none"
                                id="inline-form-input"
                                type="file"
                                name="image"
                                onChange={handleFileUpload}
                              />
                            </label>
                          </div>
                        </div>
                        <div className="mb-3">
                          <label className="col-form-label">
                            Post Title &nbsp;
                            <span className="required">*</span>
                          </label>
                          <input
                            className="form-control"
                            type="text"
                            id="text-input"
                            name="postTitle"
                            placeholder="Enter your post title"
                            onChange={handleChange}
                            autoComplete="on"
                            style={{
                              border: formErrors?.postTitle
                                ? "1px solid red"
                                : "1px solid #00000026",
                            }}
                            value={formValues.postTitle}
                          />
                          {formErrors.postTitle && (
                            <div className="invalid-tooltip">
                              {formErrors.postTitle}
                            </div>
                          )}
                        </div>
                        {console.log(apiResponse)}
                        <div className="mb-3">
                          <label className="col-form-label">Publish Date</label>
                          <input
                            className="form-control"
                            type="datetime-local"
                            id="text-input"
                            name="publishDate"
                            onChange={handleChange}
                            autoComplete="on"
                            value={formValues.publishDate}
                          />
                        </div>

                        <div className="mb-3">
                          <label className="col-form-label">
                            Link URL &nbsp;
                            <span className="option">*</span>
                          </label>
                          <br />
                          <input
                            className="form-control"
                            type="text"
                            id="text-input"
                            name="linkURL"
                            onChange={handleChange}
                            autoComplete="on"
                            value={formValues.linkURL}
                          />
                        </div>

                        <div className="mb-3">
                          <label className="col-form-label float-left w-100 mb-0">
                            Post Blurb &nbsp;
                            <span className="required">Required</span>
                            <span>
                              <small className="pull-right">190chr</small>
                            </span>
                          </label>
                          <textarea
                            className="form-control mt-0"
                            id="textarea-input"
                            rows="5"
                            name="postBlurb"
                            onChange={handleChange}
                            autoComplete="on"
                            value={formValues.postBlurb}
                            style={{
                              border: formErrors?.postBlurb
                                ? "1px solid red"
                                : "1px solid #00000026",
                            }}
                          >
                            Hello World!
                          </textarea>
                          {formErrors.postBlurb && (
                            <div className="invalid-tooltip">
                              {formErrors.postBlurb}
                            </div>
                          )}
                        </div>

                        <div className="mb-3">
                          <label className="col-form-label">Full Message</label>
                          <Editor
                            editorState={editorState}
                            onEditorStateChange={handleChanges}
                            value={formValues.fullMessage}
                            toolbar={{
                              options: [
                                "inline",
                                "blockType",
                                "fontSize",
                                "list",
                                "textAlign",
                                "history",
                                "link", // Add link option here
                              ],
                              inline: {
                                options: [
                                  "bold",
                                  "italic",
                                  "underline",
                                  "strikethrough",
                                ],
                              },
                              list: { options: ["unordered", "ordered"] },
                              textAlign: {
                                options: ["left", "center", "right"],
                              },
                              history: { options: ["undo", "redo"] },
                              link: {
                                // Configure link options
                                options: ["link", "unlink"],
                              },
                            }}
                            wrapperClassName="demo-wrapper"
                            editorClassName="demo-editor"
                          />
                        </div>

                        <br />

                        {/* <!-- Inline radios --> */}
                        <div className="mb-3">
                          <label className="col-form-label w-100">
                            Publication Level
                          </label>
                          <p className="float-left w-100 mb-2">
                            Where should your post be shown?
                          </p>
                          <div
                            className="form-check d-flex"
                            onClick={(e) => CheckList(e)}
                          >
                            <div className="">
                              <input
                                className="form-check-input "
                                id="form-radio-four"
                                type="radio"
                                name="publicationLevel"
                                value="office"
                              />
                              <label className="form-check-label">
                                My Office Only
                              </label>
                            </div>
                            <div className="ms-5">
                              <input
                                className="form-check-input"
                                id="form-radio-4"
                                type="radio"
                                name="publicationLevel"
                                value="regional"
                              />
                              <label className="form-check-label ms-2">
                                Regional Offices
                              </label>
                            </div>

                            <div className="ms-5">
                              <input
                                className="form-check-input"
                                id="form-radio-4"
                                type="radio"
                                name="publicationLevel"
                                value="choose"
                              />
                              <label className="form-check-label ms-2">
                                Let Me Choose
                              </label>
                            </div>
                          </div>
                        </div>

                        {checkPublication === "choose" ? (
                          <>
                            <div className="mb-3">
                              <label className="col-form-label">
                                Specific Office
                              </label>
                              <select
                                className="form-select mt-2"
                                id="pr-city"
                                name="specific"
                                onChange={handleChange}
                                value={formValues.specific}
                              >
                                <option></option>
                                <option>Corporate</option>
                              </select>
                            </div>
                          </>
                        ) : (
                          ""
                        )}

                        <label className="col-form-label w-100">
                          Article Comments
                        </label>
                        <div className="mb-3">
                          <div className="form-check">
                            <input
                              className="form-check-input"
                              type="checkbox"
                              id="invalidCheck"
                              name="articleComments"
                              onChange={handleChange}
                              value="Yes"
                              required
                            />
                            <label className="form-check-label">
                              Yes, Allow everyone in the office to comment on
                              this post.
                            </label>
                          </div>
                        </div>

                        <div className="d-flex">
                          <label className="col-form-label p-0">
                            Pin Article
                          </label>
                          &nbsp;&nbsp;<span>STAFF ONLY OPTION</span>
                        </div>
                        <div className="my-2">
                          <div className="form-check">
                            <input
                              className="form-check-input"
                              type="checkbox"
                              id="invalidCheck"
                              name="pin_article"
                              onClick={(e) => handlePinArticle(e)}
                              value="Yes"
                              required
                            />
                            <label className="form-check-label">
                              Yes, Pin this article above standard posts.
                            </label>
                          </div>
                        </div>

                        {pinArticle === "Yes" ? (
                          <>
                            <div className="mb-3">
                              <label className="col-form-label">
                                Pin Removal Date
                              </label>
                              <input
                                className="form-control"
                                type="date"
                                id="text-input"
                                name="pin_date"
                                onChange={handleChange}
                                autoComplete="on"
                                value={formValues.pin_date}
                              />
                            </div>
                          </>
                        ) : (
                          ""
                        )}
                      </form>
                    </div>
                    <div className="pull-right mt-4">
                      <button
                        className="btn btn-secondary btn-sm"
                        onClick={handleCloseData}
                      >
                        Cancel & Go Back
                      </button>
                      <button
                        type="button"
                        className="btn btn-primary btn-sm ms-3"
                        id="save-button"
                        onClick={PostAll}
                      >
                        Publish Post
                      </button>
                    </div>
                  </div>
                  <div className="col-md-4">
                    <div id="accordionCards" className="">
                      <div className="card bg-secondary mb-2 mt-0">
                        <div className="card-body">
                          <h3 className="h3 card-title pt-1 mb-0">
                            ✅ Post Status
                          </h3>
                        </div>
                        <div
                          className="collapse show"
                          id="cardCollapse1"
                          data-bs-parent="#accordionCards"
                        >
                          <div className="card-body mt-n1 pt-0">
                            <ul className="list-unstyled mb-2">
                              <li className="d-flex align-items-center mb-2">
                                {category ? (
                                  <>
                                    <i className="fi-check text-primary me-2"></i>
                                    <a
                                      className="nav-link fw-normal ps-1 p-0"
                                      href="#basic-info"
                                      data-scroll=""
                                      data-scroll-offset="20"
                                    >
                                      Category
                                    </a>
                                  </>
                                ) : (
                                  <>
                                    <img src="https://cdn.pboffice.net/shared/transaction/page_select_drop.png" />
                                    <a
                                      className="nav-link fw-normal ps-1 p-0"
                                      href="#basic-info"
                                      data-scroll=""
                                      data-scroll-offset="20"
                                    >
                                      Category
                                    </a>
                                  </>
                                )}
                              </li>

                              <li className="d-flex align-items-center mb-2">
                                {channel ? (
                                  <>
                                    <i className="fi-check text-primary me-2"></i>
                                    <a
                                      className="nav-link fw-normal ps-1 p-0"
                                      href="#basic-info"
                                      data-scroll=""
                                      data-scroll-offset="20"
                                    >
                                      Channel
                                    </a>
                                  </>
                                ) : (
                                  <>
                                    <img src="https://cdn.pboffice.net/shared/transaction/page_select_drop.png" />
                                    <a
                                      className="nav-link fw-normal ps-1 p-0"
                                      href="#basic-info"
                                      data-scroll=""
                                      data-scroll-offset="20"
                                    >
                                      Channel
                                    </a>
                                  </>
                                )}
                              </li>
                            </ul>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              ) : (
                ""
              )}
              {/* <!-- Step 3 ends here--> */}

              {/* <!-- Step 4 starts here--> */}
              {step3 && step4 === "" ? (
                <div className="bg-light rounded-3 p-3 mb-0">
                  <p>
                    <i className="fa fa-check" aria-hidden="true"></i> You
                    selected the '{category}' category.&nbsp;
                    <a onClick={(e) => TransactionStepBack("step2")}>
                      <NavLink>Change</NavLink>
                    </a>
                  </p>

                  <h2 className="h4 mb-2">Select a channel:</h2>
                  <div className="col-lg-12 mb-2">
                    <div
                      className="border rounded-3 p-3 selected_hover"
                      onClick={() => AssociatePost("4", "Industry News")}
                    >
                      <div className="d-flex align-items-center justify-content-between">
                        <div className="pe-2">
                          <label className="form-label fw-bold">
                            Industry News
                          </label>
                          <div id="type-value">
                            Link to a resource in the real estate industry.
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>

                  <div className="col-lg-12 mb-2">
                    <div
                      className="border rounded-3 p-3 selected_hover"
                      onClick={() => AssociatePost("4", "Networking Mixer")}
                    >
                      <div className="d-flex align-items-center justify-content-between">
                        <div className="pe-2">
                          <label className="form-label fw-bold">
                            Networking Mixer
                          </label>
                          <div id="type-value">
                            Promote an upcoming event in the real estate
                            community.
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              ) : (
                ""
              )}
              {/* <!-- Step 4 ends here--> */}

              {/* <!-- Step 5 starts here--> */}
              {step4 ? (
                <div className="row">
                  <div className="col-md-8">
                    <div className="bg-light rounded-3 border p-3 mb-0">
                      <p>
                        <i className="fa fa-check" aria-hidden="true"></i> You
                        selected the '{category}' category.&nbsp;
                        <a onClick={(e) => TransactionStepBack("step2")}>
                          <NavLink>Change</NavLink>
                        </a>
                      </p>
                      <br />

                      <p>
                        <i className="fa fa-check" aria-hidden="true"></i> You
                        selected the '{channel}' .&nbsp;
                        <a onClick={(e) => TransactionStepBack("step3")}>
                          <NavLink>Change</NavLink>
                        </a>
                      </p>
                      <hr />

                      <h5 className="h4 mb-2 mt-3">
                        Add details for this post:
                      </h5>

                      <form className="needs-validation">
                        <div className="mb-3">
                          <h6>Attached Media</h6>
                          <img
                            className="blogImage"
                            src={formValues?.file}
                            alt="image"
                          />
                          {/* <img src={postResponce} alt="image" /> */}
                          <div className="mt-3">
                            <label className="form-label btn btn-sm btn-primary">
                              Upload Media
                              <input
                                className="btn btn-primary pull-right ms-5 mt-2 d-none"
                                id="inline-form-input"
                                type="file"
                                name="image"
                                onChange={handleFileUpload}
                              />
                            </label>
                          </div>

                          {/* <button className="btn btn-primary pull-right ms-5 mt-2" type="button">Upload Media</button> */}
                        </div>
                        <div className="mb-3">
                          <label className="col-form-label">
                            Post Title &nbsp;
                            <span className="required">*</span>
                          </label>
                          <input
                            className="form-control"
                            type="text"
                            id="text-input"
                            name="postTitle"
                            placeholder="Enter your post title"
                            onChange={handleChange}
                            autoComplete="on"
                            style={{
                              border: formErrors?.postTitle
                                ? "1px solid red"
                                : "1px solid #00000026",
                            }}
                            value={formValues.postTitle}
                          />
                          {formErrors.postTitle && (
                            <div className="invalid-tooltip">
                              {formErrors.postTitle}
                            </div>
                          )}
                        </div>
                        {console.log(apiResponse)}
                        <div className="mb-3">
                          <label className="col-form-label">Publish Date</label>
                          <input
                            className="form-control"
                            type="datetime-local"
                            id="text-input"
                            name="publishDate"
                            onChange={handleChange}
                            autoComplete="on"
                            value={
                              formValues.publishDate
                                ? formValues.publishDate
                                : apiResponse
                            }
                          />
                        </div>

                        <div className="mb-3">
                          <label className="col-form-label">
                            Link URL &nbsp;
                            <span className="option">*</span>
                          </label>
                          <br />
                          <input
                            className="form-control"
                            type="text"
                            id="text-input"
                            name="linkURL"
                            onChange={handleChange}
                            autoComplete="on"
                            value={formValues.linkURL}
                          />
                        </div>

                        <div className="mb-3">
                          <label className="col-form-label float-left w-100 mb-0">
                            Post Blurb &nbsp;
                            <span className="required">*</span>
                            <span>
                              <small className="pull-right">190chr</small>
                            </span>
                          </label>
                          <textarea
                            className="form-control mt-0"
                            id="textarea-input"
                            rows="5"
                            name="postBlurb"
                            onChange={handleChange}
                            autoComplete="on"
                            value={formValues.postBlurb}
                            style={{
                              border: formErrors?.postBlurb
                                ? "1px solid red"
                                : "1px solid #00000026",
                            }}
                          >
                            Hello World!
                          </textarea>
                          {formErrors.postBlurb && (
                            <div className="invalid-tooltip">
                              {formErrors.postBlurb}
                            </div>
                          )}
                        </div>

                        <div className="mb-3">
                          <label className="col-form-label">Full Message</label>
                          <Editor
                            editorState={editorState}
                            onEditorStateChange={handleChanges}
                            value={formValues.fullMessage}
                            toolbar={{
                              options: [
                                "inline",
                                "blockType",
                                "fontSize",
                                "list",
                                "textAlign",
                                "history",
                                "link", // Add link option here
                              ],
                              inline: {
                                options: [
                                  "bold",
                                  "italic",
                                  "underline",
                                  "strikethrough",
                                ],
                              },
                              list: { options: ["unordered", "ordered"] },
                              textAlign: {
                                options: ["left", "center", "right"],
                              },
                              history: { options: ["undo", "redo"] },
                              link: {
                                // Configure link options
                                options: ["link", "unlink"],
                              },
                            }}
                            wrapperClassName="demo-wrapper"
                            editorClassName="demo-editor"
                          />
                        </div>

                        <br />

                        {/* <!-- Inline radios --> */}
                        <div className="mb-3">
                          <label className="col-form-label w-100">
                            Publication Level
                          </label>
                          <p className="float-left w-100 mb-2">
                            Where should your post be shown?
                          </p>
                          <div
                            className="form-check d-flex"
                            onClick={(e) => CheckList(e)}
                          >
                            <div className="">
                              <input
                                className="form-check-input "
                                id="form-radio-four"
                                type="radio"
                                name="publicationLevel"
                                value="office"
                              />
                              <label className="form-check-label">
                                My Office Only
                              </label>
                            </div>
                            <div className="ms-5">
                              <input
                                className="form-check-input"
                                id="form-radio-4"
                                type="radio"
                                name="publicationLevel"
                                value="regional"
                              />
                              <label className="form-check-label ms-2">
                                Regional Offices
                              </label>
                            </div>

                            <div className="ms-5">
                              <input
                                className="form-check-input"
                                id="form-radio-4"
                                type="radio"
                                name="publicationLevel"
                                value="choose"
                              />
                              <label className="form-check-label ms-2">
                                Let Me Choose
                              </label>
                            </div>
                          </div>
                        </div>

                        {checkPublication === "choose" ? (
                          <>
                            <div className="mb-3">
                              <label className="col-form-label">
                                Specific Office
                              </label>
                              <select
                                className="form-select mt-2"
                                id="pr-city"
                                name="specific"
                                onChange={handleChange}
                                value={formValues.specific}
                              >
                                <option></option>
                                <option>Corporate</option>
                              </select>
                            </div>
                          </>
                        ) : (
                          ""
                        )}

                        <label className="col-form-label w-100">
                          Article Comments
                        </label>
                        <div className="mb-3">
                          <div className="form-check">
                            <input
                              className="form-check-input"
                              type="checkbox"
                              id="invalidCheck"
                              name="articleComments"
                              onChange={handleChange}
                              value="Yes"
                              required
                            />
                            <label className="form-check-label">
                              Yes, Allow everyone in the office to comment on
                              this post.
                            </label>
                          </div>
                        </div>

                        <div className="d-flex">
                          <label className="col-form-label p-0">
                            Pin Article
                          </label>
                          &nbsp;&nbsp;<span>STAFF ONLY OPTION</span>
                        </div>
                        <div className="my-2">
                          <div className="form-check">
                            <input
                              className="form-check-input"
                              type="checkbox"
                              id="invalidCheck"
                              name="pin_article"
                              onClick={(e) => handlePinArticle(e)}
                              value="Yes"
                              required
                            />
                            <label className="form-check-label">
                              Yes, Pin this article above standard posts.
                            </label>
                          </div>
                        </div>

                        {pinArticle === "Yes" ? (
                          <>
                            <div className="mb-3">
                              <label className="col-form-label">
                                Pin Removal Date
                              </label>
                              <input
                                className="form-control"
                                type="date"
                                id="text-input"
                                name="pin_date"
                                onChange={handleChange}
                                autoComplete="on"
                                value={formValues.pin_date}
                              />
                            </div>
                          </>
                        ) : (
                          ""
                        )}
                      </form>
                    </div>
                    <div className="pull-right mt-4">
                      <button
                        className="btn btn-secondary btn-sm"
                        onClick={handleCloseData}
                      >
                        Cancel & Go Back
                      </button>
                      <button
                        type="button"
                        className="btn btn-primary btn-sm ms-3"
                        id="save-button"
                        onClick={PostAll}
                      >
                        Publish Post
                      </button>
                    </div>
                  </div>
                  <div className="col-md-4">
                    <div id="accordionCards" className="">
                      <div className="card bg-secondary mb-2 mt-0">
                        <div className="card-body">
                          <h3 className="h3 card-title pt-1 mb-0">
                            ✅ Post Status
                          </h3>
                        </div>
                        <div
                          className="collapse show"
                          id="cardCollapse1"
                          data-bs-parent="#accordionCards"
                        >
                          <div className="card-body mt-n1 pt-0">
                            <ul className="list-unstyled mb-2">
                              <li className="d-flex align-items-center mb-2">
                                {category ? (
                                  <>
                                    <i className="fi-check text-primary me-2"></i>
                                    <a
                                      className="nav-link fw-normal ps-1 p-0"
                                      href="#basic-info"
                                      data-scroll=""
                                      data-scroll-offset="20"
                                    >
                                      Category
                                    </a>
                                  </>
                                ) : (
                                  <>
                                    <img src="https://cdn.pboffice.net/shared/transaction/page_select_drop.png" />
                                    <a
                                      className="nav-link fw-normal ps-1 p-0"
                                      href="#basic-info"
                                      data-scroll=""
                                      data-scroll-offset="20"
                                    >
                                      Category
                                    </a>
                                  </>
                                )}
                              </li>

                              <li className="d-flex align-items-center mb-2">
                                {channel ? (
                                  <>
                                    <i className="fi-check text-primary me-2"></i>
                                    <a
                                      className="nav-link fw-normal ps-1 p-0"
                                      href="#basic-info"
                                      data-scroll=""
                                      data-scroll-offset="20"
                                    >
                                      Channel
                                    </a>
                                  </>
                                ) : (
                                  <>
                                    <img src="https://cdn.pboffice.net/shared/transaction/page_select_drop.png" />
                                    <a
                                      className="nav-link fw-normal ps-1 p-0"
                                      href="#basic-info"
                                      data-scroll=""
                                      data-scroll-offset="20"
                                    >
                                      Channel
                                    </a>
                                  </>
                                )}
                              </li>
                            </ul>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              ) : (
                ""
              )}
            </>
          )}
        </div>
      </main>
    </div>
  );
}
export default UpdatePost;

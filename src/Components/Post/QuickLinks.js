import React, { useState, useEffect } from "react";
import Loader from "../../Pages/Loader/Loader.js";
import Toaster from "../../Pages/Toaster/Toaster.js";
import { NavLink, useNavigate } from "react-router-dom";
import { useParams } from "react-router-dom";
import user_service from "../service/user_service";
import jwt from "jwt-decode";
import moment from "moment";

const QuickLinks = () => {
  const navigate = useNavigate();
  const [loader, setLoader] = useState({ isActive: null });
  const { isActive } = loader;
  const [toaster, setToaster] = useState({
    types: null,
    isShow: null,
    toasterBody: "",
    message: "",
  });
  const { types, isShow, toasterBody, message } = toaster;
 

  const [link, setLink] = useState("");
  const LinkGet = async () => {
    const userid = jwt(localStorage.getItem("auth")).id;
    var pin = `?agentId=${userid}&pin=yes`;
    //var pin = `?pin=yes`;
    await user_service.linksGet(pin).then((response) => {
      if (response) {
        // console.log(response);
        setLink(response.data);
      }
    });
  };
 useEffect(()=>{
    LinkGet()
 },[])
  
  return (
    <div className="bg-secondary float-left w-100 pt-4 mb-4">
      <Loader isActive={isActive} />
      {isShow && (
        <Toaster
          types={types}
          isShow={isShow}
          toasterBody={toasterBody}
          message={message}
        />
      )}
      <div className="content-overlay mt-0">
        <main className="page-wrapper getContact_page_wrap">
          {/* <!-- Page container--> */}
          <div className=" ">
            <div className="d-flex align-items-center justify-content-between float-left mb-3">
              <h3 className="text-white text-left mb-0">Quick Resources</h3>
            </div>
            <div className="row">
                <div className="bg-light border rounded-3 p-3">
                {link.data && link.data.length > 0
                            ? link.data.map((items, index) => (
                                <NavLink  className="btn btn-secondary" target="_blank" to={items.link_url}>
                                  <p className="text-align-center m-0" key={index}>{items.title}</p>
                                </NavLink>
                              ))
                            : "Not any Link/File assigned"}
              </div>
             
            </div>
          </div>
        </main>
      </div>
    </div>
  );
};

export default QuickLinks;

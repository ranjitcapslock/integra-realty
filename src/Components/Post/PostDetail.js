import React, { useEffect, useState } from "react";
import { NavLink, useParams,useNavigate } from "react-router-dom";
import user_service from "../service/user_service";
import Loader from "../../Pages/Loader/Loader.js";
import Toaster from "../../Pages/Toaster/Toaster";
import jwt from "jwt-decode";
import Swal from "sweetalert2";
import "sweetalert2/dist/sweetalert2.css";

const PostDetail = () => {
  const [loader, setLoader] = useState({ isActive: null });
  const { isActive } = loader;
  const [toaster, setToaster] = useState({
    types: null,
    isShow: null,
    toasterBody: "",
    message: "",
  });
  const { types, isShow, toasterBody, message } = toaster;
  const navigate = useNavigate();
  const [detail, setDetail] = useState("");
  const params = useParams();
  const [formValues, setFormValues] = useState({ comment: "" });

  const handleChange = (e) => {
    const { name, value } = e.target;
    setFormValues({ ...formValues, [name]: value });
  };

  const PostGetById = async () => {
    await user_service.postDetail(params.id).then((response) => {
      setLoader({ isActive: false });
      if (response) {
        console.log(response.data);
        const publishDate = new Date(response.data.publishDate);
        setDetail({ ...response.data, publishDate });
      }
    });
  };

  const PostGet = async () => {
    await user_service.postGet().then((response) => {
      setLoader({ isActive: false });
      if (response) {
        console.log(response);
      }
    });
  };
  useEffect(() => {
    window.scrollTo(0, 0);
    setLoader({ isActive: true });
    PostGetById(params.id);
    AddCommentGetID(params.id);
  }, []);

  const AddCommentGetID = async () => {
    await user_service.AddCommentGet(params.id).then((response) => {
      setLoader({ isActive: false });
      if (response) {
        setFormValues(response.data);
      }
    });
  };
  // useEffect(() => { window.scrollTo(0, 0);
  //     setLoader({ isActive: true })
  //     AddCommentGetID(params.id)
  // }, []);

  const handleSubmit = async (e) => {
    e.preventDefault();

    const userData = {
      commentBy: jwt(localStorage.getItem("auth")).id,
      comment: formValues.comment,
      postId: params.id,
    };
    setLoader({ isActive: true });
    await user_service.AddCommentPost(userData).then((response) => {
      if (response) {
        // setFormValues(response.data);

        // const AddCommentGetID = async () => {
        //     await user_service.AddCommentGet(params.id).then((response) => {
        //         setLoader({ isActive: false })
        //         if (response) {
        //             // setFormValues(response.data)
        //             PostGetById(params.id)
        //         }
        //     });
        // }
        AddCommentGetID(params.id);

        setLoader({ isActive: false });
        setToaster({
          type: "Add Comment",
          isShow: true,
          toasterBody: response.data.message,
          message: "Add Comment Update Successfully",
        });
        setTimeout(() => {
          setToaster((prevToaster) => ({ ...prevToaster, isShow: false }));
        }, 1000);
      } else {
        setLoader({ isActive: false });
        setToaster({
          types: "error",
          isShow: true,
          toasterBody: response.data.message,
          message: "Error",
        });
      }
    });
  };

  const handleRemove = async (id) => {
    Swal.fire({
      title: "Are you sure?",
      text: "It will be permanently deleted!",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Yes, delete it!",
    }).then(async (result) => {
      if (result.isConfirmed) {
        try {
          const response = await user_service.AddCommentDelete(id);
          if (response) {
            const AddCommentGetID = async () => {
              await user_service.AddCommentGet(params.id).then((response) => {
                setLoader({ isActive: false });
                if (response) {
                  Swal.fire(
                    "Deleted!",
                    "Your file has been deleted.",
                    "success"
                  );
                  setFormValues(response.data);
                }
              });
            };
            AddCommentGetID(params.id);
          }
        } catch (error) {
          Swal.fire(
            "Error",
            "An error occurred while deleting the file.",
            "error"
          );
          console.error(error);
        }
      }
    });
  };

  const [formData, setFormData] = useState([]);

  const StaffRoleGetData = async () => {
    await user_service.StaffRoleGet().then((response) => {
      if (response) {
        console.log(response);
        setFormData(response.data.data);
      }
    });
  };
  useEffect(() => {
    if (formData) {
      StaffRoleGetData();
    }
  }, []);

  const handleRemovePost = async(id)=>{
    setLoader({ isActive: true });
    await user_service.postDelete(id).then((response) => {
      if (response) {
        setLoader({ isActive: false });
        setToaster({
          type: "Post Delete",
          isShow: true,
          message: "Post Deleted Successfully",
        });
        setTimeout(() => {
          setToaster((prevToaster) => ({
            ...prevToaster,
            isShow: false,
          }));
          navigate(`/`);
        }, 1000);
        PostGet()
      }
    });
  }

  return (
    <div className="bg-secondary float-left w-100 mb-4">
      <Loader isActive={isActive} />
      {isShow && (
        <Toaster
          types={types}
          isShow={isShow}
          toasterBody={toasterBody}
          message={message}
        />
      )}
      <main className="page-wrapper">
        {/* <!-- Page container--> */}
        <div className="">
          {/* <!-- Breadcrumb--> */}

          {(localStorage.getItem("auth") &&
            jwt(localStorage.getItem("auth")).contactType == "admin") ||
          jwt(localStorage.getItem("auth")).id == detail.author ? (
            <div className="mt-4">
              <button
                type="button"
                className="btn btn-primary dropdown-toggle pull-right ms-2"
                data-bs-toggle="dropdown"
                aria-haspopup="true"
                aria-expanded="false"
              >
                Action
              </button>
              <div className="dropdown-menu dropdown-menu-end my-1">
                <NavLink
                  to={`/update-post/${params.id}`}
                  className="dropdown-item"
                >
                  Edit Post
                </NavLink>
                <NavLink
                  to={`/admin/announce/post/${params.id}`}
                  className="dropdown-item"
                >
                  Announce
                </NavLink>
              </div>
            </div>
          ) : (
            <>
              {localStorage.getItem("auth") &&
                jwt(localStorage.getItem("auth")).contactType == "staff" &&
                (Array.isArray(formData) && formData.length > 0
                  ? formData.map((item) =>
                      item.roleStaff === "calender" ? (
                        <div className="">
                          <button
                            type="button"
                            className="btn btn-primary dropdown-toggle pull-right"
                            data-bs-toggle="dropdown"
                            aria-haspopup="true"
                            aria-expanded="false"
                          >
                            Action
                          </button>
                          <div className="dropdown-menu dropdown-menu-end my-1">
                            <NavLink
                              to={`/update-post/${params.id}`}
                              className="dropdown-item"
                            >
                              Edit Post
                            </NavLink>
                            <NavLink
                              to={`/admin/announce/post/${params.id}`}
                              className="dropdown-item"
                            >
                              Announce
                            </NavLink>
                          </div>
                        </div>
                      ) : (
                        ""
                      )
                    )
                  : "")}
            </>
          )}

          {jwt(localStorage.getItem("auth")).contactType == "admin" ? (
            <button className="btn btn-secondary pull-right" onClick={()=>handleRemovePost(detail._id)}>
              Remove Post 
            </button>
          ) : (
            ""
          )}

          <nav className="mb-3 pt-md-3" aria-label="breadcrumb">
            <ol className="breadcrumb">
              <li className="breadcrumb-item">
                <NavLink className="text-white" to="/">
                  Home
                </NavLink>
              </li>
              <li className="breadcrumb-item">
                <NavLink className="text-white" to="/">
                  Post Detail
                </NavLink>
              </li>
              <li className="breadcrumb-item active" aria-current="page">
                {/* <p className="text-white">{detail.postTitle}</p> */}
                <NavLink className="text-white" to="/">
                  {detail.postTitle}
                </NavLink>
              </li>
            </ol>
          </nav>

          {/* <!-- Page card like wrapper--> */}
          <div className="bg-light border rounded-3 p-3">
            {detail ? (
              <div className="row">
                <h5 className="mb-0">{detail.postTitle}</h5>

                <div className="mb-3">
                  <ul className="list-unstyled d-flex flex-wrap mb-0 text-nowrap">
                    <li className="me-3">
                      <i className="fi-calendar-alt me-2 mt-n1 opacity-60"></i>
                      {detail.publishDate &&
                        detail.publishDate.toLocaleDateString()}
                    </li>
                  </ul>
                </div>

                {detail.file ? (
                  <div className="">
                    <img
                      className="blogImage"
                      src={detail.file}
                      data-toggle="modal"
                      data-target="#adddocument"
                    />
                  </div>
                ) : (
                  ""
                )}
                <div className="post_details col-lg-12">
                  <div className="mb-3">
                    {detail.contact_details ? (
                      <>
                        <a
                          className="d-flex align-items-center text-body text-decoration-none"
                          href="#"
                        >
                          <div className="float-start w-100 mt-3">
                            <h2 className="h6 mb-1">
                              {detail.contact_details.firstName}{" "}
                              {detail.contact_details.lastName}
                            </h2>
                            <span className="fs-sm">
                              {detail.category} &nbsp; {detail.channel} <br />
                              {detail.postBlurb}
                            </span>
                          </div>
                        </a>
                      </>
                    ) : (
                      ""
                    )}
                  </div>

                  {detail.linkURL ? (
                    <div className="float-left w-100 mb-3">
                      {detail.linkURL ? (
                        <>
                          Go to:
                          <a
                            href={detail.linkURL}
                            target="_blank"
                            rel="noopener noreferrer"
                          >
                            Article Link
                          </a>
                        </>
                      ) : (
                        ""
                      )}
                    </div>
                  ) : (
                    ""
                  )}

                  <div className="float-left w-100">
                    {detail.fullMessage ? (
                      <>
                        <p
                          dangerouslySetInnerHTML={{
                            __html: detail.fullMessage,
                          }}
                        />
                      </>
                    ) : (
                      ""
                    )}
                  </div>

                  <div className="" id="comments">
                    <h5 className="">Comments</h5>
                    <div className="">
                      {detail.articleComments === "Yes" ? (
                        <>
                          {formValues.data
                            ? formValues.data.map((item) => (
                                <>
                                  <div className="d-flex align-items-center justify-content-between float-left w-100">
                                    <div className="w-100">
                                      <p className="mt-3">
                                        {item.comment}
                                        <br />
                                      </p>
                                      <div className="d-flex align-items-center">
                                        <div className="">
                                          <NavLink
                                            to={`/contact-profile/${item.contact_details._id}`}
                                          >
                                            <h6 className="fs-base mb-0">
                                              {item.contact_details.firstName}{" "}
                                              {item.contact_details.lastName}
                                            </h6>
                                          </NavLink>
                                        </div>
                                      </div>
                                    </div>

                                    {localStorage.getItem("auth") &&
                                    jwt(localStorage.getItem("auth"))
                                      .contactType == "admin" ? (
                                      <img
                                        onClick={(e) => handleRemove(item._id)}
                                        className="pull-right"
                                        src="https://cdn.pboffice.net/shared/transaction/page_select_drop.png"
                                      />
                                    ) : (
                                      ""
                                    )}
                                  </div>
                                </>
                              ))
                            : "No comments posted yet."}

                          <div className="card border-0">
                            <div className="card-body border-0 p-0">
                              <h6 className="mb-2 pb-sm-2">Add a Comment</h6>
                              <div className=" col-md-12 mb-3">
                                <textarea
                                  className="form-control mt-0"
                                  id="textarea-input"
                                  name="comment"
                                  onChange={handleChange}
                                  autoComplete="on"
                                  value={formValues.comment}
                                ></textarea>

                                <div className="col-12 mt-4">
                                  <button
                                    className="btn btn-primary btn-sm"
                                    onClick={handleSubmit}
                                  >
                                    Add Comment
                                  </button>
                                </div>
                              </div>
                            </div>
                          </div>
                        </>
                      ) : (
                        ""
                      )}
                    </div>
                  </div>
                </div>
              </div>
            ) : (
              ""
            )}
          </div>
        </div>

        <div className="modal in" role="dialog" id="adddocument">
          <div
            className="modal-dialog modal-lg modal-dialog-scrollable"
            role="document"
          >
            <div className="modal-content">
              <div className="modal-header">
                <h4 className="modal-title">
                  Media Gallery for Mortgage Rates Continue to Surge
                </h4>
                <button
                  className="btn-close"
                  id="closeModal"
                  type="button"
                  data-dismiss="modal"
                  aria-label="Close"
                ></button>
              </div>
              <div className="modal-body media-gallary">
                <div className="row">
                  <div className="col-md-12">
                    {detail.file ? (
                      <>
                        <img
                          className="img-fluid"
                          style={{ width: 500, height: 400 }}
                          src={detail.file}
                          data-toggle="modal"
                          data-target="#adddocument"
                        />
                      </>
                    ) : (
                      ""
                    )}
                  </div>
                  <div className="pull-right">
                    <a
                      href={detail.file}
                      type="button"
                      className="btn btn-primary pull-right mt-4"
                      rel="noreferrer"
                    >
                      Save
                    </a>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </main>
    </div>
  );
};

export default PostDetail;

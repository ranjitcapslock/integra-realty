import React, { useState, useEffect } from 'react';
import Loader from "../../Pages/Loader/Loader.js";
import Toaster from '../../Pages/Toaster/Toaster.js';
import { NavLink, useNavigate } from 'react-router-dom';
import user_service from '../service/user_service';

const CalenderSearch = () => {


    const [search_value, setSearchValue] = useState({ title: '' });
    const [result, setResult] = useState([])
    const [isLoading, setIsLoading] = useState(false);
    const [loader, setLoader] = useState({ isActive: null })
    const { isActive } = loader;
    const [toaster, setToaster] = useState({ types: null, isShow: null, toasterBody: "", message: "" });
    const { types, isShow, toasterBody, message } = toaster;





    const calenderSearchGet = async () => {
        try {
            setIsLoading(true);

            const response = await user_service.calenderSearch(search_value.title);
            if (response) {
                console.log(response);
                setResult(response.data.data);
            }

            setIsLoading(false);
        } catch (error) {
            console.error(error);
            setIsLoading(false);
        }
    };

    const handleChangesearch = (event) => {
        setSearchValue({ title: event.target.value });
    };

    return (
        <div className="bg-secondary float-left w-100 pt-4">
            <Loader isActive={isActive} />
            {isShow && <Toaster types={types}
                isShow={isShow}
                toasterBody={toasterBody}
                message={message} />}
            <main className="page-wrapper">
                {/* <!-- Page content--> */}
                {/* <!-- Page container--> */}
                <div className="container mb-md-4 ">
                    <div className="bg-light shadow-sm rounded-3 p-2 p-md-5 mb-2">
                        <div className="row">
                            <div className="col-md-9 ">
                                <h6 className="h3 mb-3">Upcoming Events</h6><br />
                                <div className='mb-5    '>


                                    {isLoading ? (
                                        <p>Loading...</p>
                                    ) : (
                                        result.map((post) => (
                                            <div>
                                                <a className='col-md-3'>{post.title}</a>
                                                <a  className='col-md-3'>{post.venue}</a>
                                                <a>{post.venue}</a>
                                                <a>{post.venue}</a>
                                            </div>

                                        ))
                                    )}

                                </div>
                            </div>

                            <div className="col-md-3">
                                <div className="form-outline d-flex">
                                    <input id="search-input-1 w-20" type="search" className="form-control"
                                        name="title"
                                        onChange={handleChangesearch}
                                        value={search_value.title}
                                    />
                                    <button
                                        onClick={calenderSearchGet}
                                        type="button" className="btn btn-translucent-primary">Search</button>

                                </div><br />

                                <div className="col-sm-12 mb-4">
                                    <label className="form-label" for="pr-birth-date">Start Date</label>
                                    <input className="form-control" type="date" id="inline-form-input"
                                        placeholder="Choose date"
                                        name="startDate"
                                        //   onChange={handleChange}
                                        //   value={formValues.startDate}

                                        data-datepicker-options="{&quot;altInput&quot;: true, &quot;altFormat&quot;: &quot;F j, Y&quot;, &quot;dateFormat&quot;: &quot;Y-m-d&quot;}" />
                                </div>


                                <div className="col-sm-12 mb-4">
                                    <label className="form-label" for="pr-birth-date">End Date</label>
                                    <input className="form-control" type="date" id="inline-form-input"
                                        placeholder="Choose date"
                                        name="endDate"
                                        //   onChange={handleChange}
                                        //   value={formValues.endDate}
                                        data-datepicker-options="{&quot;altInput&quot;: true, &quot;altFormat&quot;: &quot;F j, Y&quot;, &quot;dateFormat&quot;: &quot;Y-m-d&quot;}" />
                                </div>

                            </div>

                        </div>
                    </div>
                </div>
            </main>
            
        </div>
    )
}

export default CalenderSearch
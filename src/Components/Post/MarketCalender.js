import React, { useState, useEffect } from "react";
import { Calendar as BigCalendar, momentLocalizer } from "react-big-calendar";
import moment from "moment-timezone";
import "react-big-calendar/lib/css/react-big-calendar.css";
import Loader from "../../Pages/Loader/Loader.js";
import Toaster from "../../Pages/Toaster/Toaster.js";
import { NavLink, useNavigate } from "react-router-dom";
import user_service from "../service/user_service";
import jwt from "jwt-decode";
import _ from "lodash";
import axios from "axios";
import Swal from "sweetalert2";
import "sweetalert2/dist/sweetalert2.css";

import Slider from "react-slick";
import "slick-carousel/slick/slick-theme.css";
import "slick-carousel/slick/slick.css";

const localizer = momentLocalizer(moment);

const settings = {
  dots: true,
  infinite: true,
  speed: 500,
  slidesToShow: 1,
  slidesToScroll: 1,
  autoplay: true,
  autoplaySpeed: 7000,
  arrows: false,
};

const eventColors = {
  meeting: "#FF5733",
  duty: "#3366FF",
};

const MarKetCalender = () => {
  const navigate = useNavigate();

  const initialValues = {
    marketText: "",
    marketDate: "",
    image: "",
  };
  const [loader, setLoader] = useState({ isActive: null });
  const { isActive } = loader;
  const [toaster, setToaster] = useState({
    types: null,
    isShow: null,
    toasterBody: "",
    message: "",
  });
  const { types, isShow, toasterBody, message } = toaster;

  const CustomToolbar = () => {
    return (
      <div className="rbc-toolbar mt-3">
        <span className="rbc-toolbar-label"></span>
        <span className="rbc-btn-group mt-3"></span>
      </div>
    );
  };

  const [file, setFile] = useState(null);
  const [data, setData] = useState("");

  const [calenderGet, setCalenderGet] = useState([]);
  const [events, setEvents] = useState([]);
  const [selectedDate, setSelectedDate] = useState(null);
  const [formValues, setFormValues] = useState(initialValues);
  const [formErrors, setFormErrors] = useState({});
  const [isSubmitClick, setISSubmitClick] = useState(false);
  const [isModalVisible, setIsModalVisible] = useState(false);
  const [isModalVisibleUpdate, setIsModalVisibleUpdate] = useState(false);

  // const [currentDate, setCurrentDate] = useState("");
  const [textData, setTextData] = useState("");
  const [imageData, setImageData] = useState("");
  const [dateData, setDateData] = useState("");
  const [eventId, setEventId] = useState("");
  const [moreText, setMoreText] = useState(false);
  const [defaultContent, setDefaultContent] = useState([]);
  const [selectedMonth, setSelectedMonth] = useState(moment());
  const handleSelectSlot = (slotInfo) => {
    const today = moment().startOf("day");
    const selectedDate = moment(slotInfo.start).startOf("day");

    if (
      localStorage.getItem("auth") &&
      jwt(localStorage.getItem("auth")).contactType === "admin"
    ) {
      if (selectedDate.isSameOrAfter(today)) {
        setSelectedDate(slotInfo);
        setIsModalVisible(true);
        window.$("#modal-show-calender-market").modal("show");
      }
    }
  };

  const handleEventClick = (text, image, date, id) => {
    setTextData(text);
    setImageData(image);
    setDateData(date);
    setEventId(id);
  };

  const fetchCalendar = async () => {
    try {
      setLoader({ isActive: true });
      let queryParams;
      if (
        localStorage.getItem("auth") &&
        jwt(localStorage.getItem("auth")).contactType == "admin"
      ) {
        queryParams = `?page=1`;
      } else {
        queryParams = `?page=1&agentId=` + jwt(localStorage.getItem("auth")).id;
      }
      const response = await user_service.MarketCalenderGet(queryParams);

      setLoader({ isActive: false });

      if (response && response.data) {
        setCalenderGet(response.data);
      }
    } catch (error) {
      console.error("Error in fetchCalendar:", error);
      // Handle errors as needed, e.g., show a user-friendly error message
    }
  };

  const PageContentMarketingGet = async (month) => {
    try {
      const response = await user_service.pageContentMarketingGet();
      if (response) {
        // Filter the content for the selected month
        const filteredContent = response.data.data.filter(
          (item) => item.month === month && item.page === "marketing"
        );
        setDefaultContent(filteredContent);
      }
    } catch (error) {
      console.error("Error fetching content:", error);
    }
  };

  useEffect(() => {
    const currentMonth = moment().format("MMMM");
    PageContentMarketingGet(currentMonth); // Fetch content for the current month by default
  }, []);

  useEffect(() => {
    fetchCalendar();
  }, []);

  const generateEvents = () => {
    const generatedEvents = [];
    if (calenderGet && calenderGet.data) {
      calenderGet.data.forEach((date) => {
        if (!date.marketDate) {
          console.warn("marketDate is undefined for:", date);
          return;
        }

        let startDate;
        let endDate;
        if (moment(date.marketDate, "M/D/YYYY", true).isValid()) {
          startDate = moment
            .tz(date.marketDate, "M/D/YYYY", "America/Denver")
            .toDate();
          endDate = moment
            .tz(date.marketEnd, "M/D/YYYY", "America/Denver")
            .toDate();
        } else if (date.startDate) {
          startDate = moment.tz(date.marketDate, "America/Denver").toDate();
          endDate = moment.tz(date.marketEnd, "America/Denver").toDate();
        } else {
          console.warn("Invalid marketDate format for:", date);
          return;
        }

        const eventId = date._id;
        generatedEvents.push({
          title: date.marketText,
          start: startDate,
          end: endDate,
          id: eventId,
          type: "meeting",
          image: date.image ?? "",
        });
      });
    }
    return generatedEvents;
  };

  const MarkteCalenderGetById = async () => {
    await user_service.MarketCalenderGetId(eventId).then((response) => {
      setLoader({ isActive: false });
      if (response) {
        // Convert date from MM/DD/YYYY to YYYY-MM-DD format
        const rawDate = response.data.marketDate;
        const formattedDate = rawDate ? formatDateToYYYYMMDD(rawDate) : "";
        setFormValues({
          ...response.data,
          marketDate: formattedDate,
        });
      }
    });
  };

  // Helper function to convert MM/DD/YYYY to YYYY-MM-DD
  const formatDateToYYYYMMDD = (dateString) => {
    const [month, day, year] = dateString.split("/");
    return `${year}-${month.padStart(2, "0")}-${day.padStart(2, "0")}`;
  };

  // const MarkteCalenderGetById = async () => {
  //   await user_service
  //     .MarketCalenderGetId(eventId)
  //     .then((response) => {
  //       setLoader({ isActive: false });
  //       if (response) {
  //         // Manually parse the date to avoid timezone issues
  //         const rawDate = response.data.marketDate;
  //         const formattedDate = rawDate
  //           ? rawDate.split('T')[0] // Extract the YYYY-MM-DD part
  //           : '';
  //         setFormValues({
  //           ...response.data,
  //           marketDate: formattedDate,
  //         });
  //       }
  //     });
  // };

  useEffect(() => {
    if (eventId) {
      MarkteCalenderGetById();
    }
  }, [eventId]);

  useEffect(() => {
    const generatedEvents = generateEvents();
    setEvents(generatedEvents);
  }, [calenderGet]);

  const eventStyleGetter = (event, start) => {
    const isToday = moment(start).isSame(moment(), "day");

    const backgroundColor = isToday ? "#151e43" : eventColors[event.type];
    const borderColor = isToday ? "black" : "transparent";

    return {
      className: isToday ? "rbc-today-row" : "",
      style: {
        backgroundColor,
        borderColor,
      },
    };
  };

  useEffect(() => {
    if (formValues && isSubmitClick) {
      validate();
    }
  }, [formValues]);

  const handleChange = (e) => {
    const { name, value } = e.target;
    setFormValues({ ...formValues, [name]: value });
  };

  const handleFileUpload = async (e) => {
    const selectedFile = e.target.files[0];

    if (selectedFile) {
      setFile(selectedFile);

      const fileNameWithoutExtension = selectedFile.name.replace(
        /\.[^/.]+$/,
        ""
      ); // Extracts the file name without extension
      const formData = new FormData();
      formData.append("file", selectedFile);

      const config = {
        headers: {
          Accept: "application/json",
          "Content-Type": "multipart/form-data",
          Authorization: `Bearer ${localStorage.getItem("auth")}`,
        },
      };

      try {
        setLoader({ isActive: true });

        const uploadResponse = await axios.post(
          "https://api.brokeragentbase.com/upload",
          // "http://localhost:4000/upload",
          formData,
          config
        );
        const uploadedFileData = uploadResponse.data;

        setLoader({ isActive: false });
        setData(uploadedFileData);
        setFormValues((prevValues) => ({
          ...prevValues,
          image: "",
        }));
      } catch (error) {
        console.error("Error occurred during file upload:", error);
      }
    }
  };

  const validate = () => {
    const values = formValues;
    const errors = {};
    if (!values.marketText) {
      errors.marketText = "Market Text is required";
    }

    if (!values.marketDate) {
      errors.marketDate = "Market Date is required";
    }
    setFormErrors(errors);
    return errors;
  };

  const handleSubmit = async (e, slotInfo) => {
    e.preventDefault();
    setISSubmitClick(true);
    let checkValue = validate();
    if (_.isEmpty(checkValue)) {
      if (slotInfo) {
        const startDate = moment(slotInfo.start).format("M/D/YYYY");
        const endDate = moment(slotInfo.start).format("M/D/YYYY");

        const userData = {
          office_id: localStorage.getItem("active_office_id"),
          office_name: localStorage.getItem("active_office"),
          marketText: formValues.marketText,
          marketDate: startDate,
          marketEnd: endDate,
          agentId: jwt(localStorage.getItem("auth")).id,
          image: data,
        };
        try {
          setLoader({ isActive: true });
          const response = await user_service.MarketCalenderPost(userData);
          if (response && response.data) {
            setLoader({ isActive: false });
            setToaster({
              types: "Calender",
              isShow: true,
              toasterBody: response.data.message,
              message: "Event Post Successfully",
            });
            setTimeout(() => {
              setToaster((prevToaster) => ({ ...prevToaster, isShow: false }));
            }, 1000);

            fetchCalendar();
            document.getElementById("close").click();
          } else {
            setLoader({ isActive: false });
            setToaster({
              types: "error",
              isShow: true,
              toasterBody: response.data.message,
              message: "Error",
            });
          }
        } catch (error) {
          setLoader({ isActive: false });
          setToaster({
            types: "error",
            isShow: true,
            toasterBody: error.message,
            message: "Error",
          });
        }
      }
    }
  };

  const handleImageDownload = (imageUrl) => {
    const link = document.createElement("a");
    link.href = imageUrl;
    link.target = "_blank"; // Open link in new tab
    document.body.appendChild(link);
    link.click();
    document.body.removeChild(link);
  };

  const handleTextCopy = (text) => {
    navigator.clipboard.writeText(text).then(
      () => {
        alert("Text copied to clipboard!");
      },
      (err) => {
        console.error("Could not copy text: ", err);
      }
    );
  };

  const handleClicktext = () => {
    navigate("/market-text");
  };

  const handleCalender = (text, image) => {
    setIsModalVisibleUpdate(true);
    setFormValues((prevValues) => ({
      ...prevValues,
      marketText: text,
      image: image,
    }));

    setFormValues((prevValues) => ({
      ...prevValues,
      marketDate: "",
    }));
    setData("");
  };

  const handleSubmitClone = async (e) => {
    e.preventDefault();
    setISSubmitClick(true);
    let checkValue = validate();
    if (_.isEmpty(checkValue)) {
      const startDate = moment(formValues.marketDate).format("M/D/YYYY");
      const endDate = moment(formValues.marketDate).format("M/D/YYYY");

      const userData = {
        office_id: localStorage.getItem("active_office_id"),
        office_name: localStorage.getItem("active_office"),
        marketText: formValues.marketText,
        marketDate: startDate,
        marketEnd: endDate,
        agentId: jwt(localStorage.getItem("auth")).id,
        image: formValues.image ? formValues.image : data,
      };
      try {
        setLoader({ isActive: true });
        const response = await user_service.MarketCalenderUpdate(
          eventId,
          userData
        );
        if (response && response.data) {
          setLoader({ isActive: false });
          setToaster({
            types: "Calender",
            isShow: true,
            // toasterBody: response.data.message,
            message: "Event Post Successfully",
          });
          setTimeout(() => {
            setToaster((prevToaster) => ({ ...prevToaster, isShow: false }));
          }, 1000);

          fetchCalendar();
          setIsModalVisibleUpdate(false);
        } else {
          setLoader({ isActive: false });
          setToaster({
            types: "error",
            isShow: true,
            // toasterBody: response.data.message,
            message: "Error",
          });
        }
      } catch (error) {
        setLoader({ isActive: false });
        setToaster({
          types: "error",
          isShow: true,
          // toasterBody: error.message,
          message: "Error",
        });
      }
    }
  };

  const handleSubmitUpdate = async (e) => {
    if (eventId) {
      e.preventDefault();
      setISSubmitClick(true);
      let checkValue = validate();
      if (_.isEmpty(checkValue)) {
        const startDate = moment(formValues.marketDate).format("M/D/YYYY");
        const endDate = moment(formValues.marketDate).format("M/D/YYYY");

        const userData = {
          office_id: localStorage.getItem("active_office_id"),
          office_name: localStorage.getItem("active_office"),
          marketText: formValues.marketText,
          marketDate: startDate,
          marketEnd: endDate,
          agentId: jwt(localStorage.getItem("auth")).id,
          image: formValues.image ? formValues.image : data,
        };
        try {
          setLoader({ isActive: true });
          const response = await user_service.MarketCalenderUpdate(
            eventId,
            userData
          );
          if (response && response.data) {
            setFormValues("");
            setLoader({ isActive: false });
            setToaster({
              types: "Calender",
              isShow: true,
              // toasterBody: response.data.message,
              message: "Event Update Successfully",
            });
            setTimeout(() => {
              setToaster((prevToaster) => ({ ...prevToaster, isShow: false }));
            }, 1000);

            fetchCalendar();
            setIsModalVisibleUpdate(false);
            setDateData("");
            document.getElementById("closeModal").click();
          } else {
            setLoader({ isActive: false });
            setToaster({
              types: "error",
              isShow: true,
              // toasterBody: response.data.message,
              message: "Error",
            });
          }
        } catch (error) {
          setLoader({ isActive: false });
          setToaster({
            types: "error",
            isShow: true,
            // toasterBody: error.message,
            message: "Error",
          });
        }
      }
    }
  };

  const handleCloseUpdate = () => {
    setEventId("");
    setDateData("");
  };

  const handleClose = () => {
    setIsModalVisibleUpdate(false);
  };

  const handleNavigate = (date, view, action) => {
    switch (action) {
      case "TODAY":
        const today = moment();
        setSelectedMonth(today);
        const todayEvents = generateEvents(today);
        setEvents(todayEvents);
        PageContentMarketingGet(today.format("MMMM")); // Fetch content for the current month
        break;

      case "PREV":
        const previousMonth = moment(selectedMonth).subtract(1, "month");
        setSelectedMonth(previousMonth);
        const previousMonthEvents = generateEvents(previousMonth);
        setEvents(previousMonthEvents);
        PageContentMarketingGet(previousMonth.format("MMMM")); // Fetch content for the previous month
        break;

      case "NEXT":
        const nextMonth = moment(selectedMonth).add(1, "month");
        setSelectedMonth(nextMonth);
        const nextMonthEvents = generateEvents(nextMonth);
        setEvents(nextMonthEvents);
        PageContentMarketingGet(nextMonth.format("MMMM")); // Fetch content for the next month
        break;

      default:
        break;
    }
  };

  const handleRemoveEvent = async () => {
    if (eventId) {
      const confirmation = await Swal.fire({
        title: "Are you sure?",
        text: "It will be permanently deleted!",
        icon: "warning",
        showCancelButton: true,
        confirmButtonColor: "#3085d6",
        cancelButtonColor: "#d33",
        confirmButtonText: "Yes, delete it!",
      });

      if (confirmation.isConfirmed) {
        setLoader({ isActive: true });

        await user_service.MarketCalenderDelete(eventId).then((response) => {
          if (response) {
            setLoader({ isActive: false });
            setDateData("");
            setToaster({
              type: "success",
              isShow: true,
              message: "Event Deleted Successfully",
            });
            setTimeout(() => {
              setToaster((prevToaster) => ({
                ...prevToaster,
                isShow: false,
              }));
            }, 1000);
            fetchCalendar();
          }
        });
      }
    }
  };
  return (
    <div className="bg-secondary float-left w-100 pt-4 mb-4">
      <Loader isActive={isActive} />
      {isShow && (
        <Toaster
          types={types}
          isShow={isShow}
          toasterBody={toasterBody}
          message={message}
        />
      )}
      <main className="page-wrapper">
        <div className="row event-calender">
          <div className="col-md-8">
            <div className="align-items-center justify-content-between d-lg-flex d-md-flex d-sm-flex d-block w-100">
              <h3 className="text-white mb-lg-4 mb-md-4 mb-sm-4 mb-4">Marketing Calendar</h3>
              {localStorage.getItem("auth") &&
              jwt(localStorage.getItem("auth")).contactType == "admin" ? (
                <>
                  {defaultContent && defaultContent.length > 0 ? (
                    <NavLink
                      to={`/marketing-content/${defaultContent[0]._id}`} 
                        className="ms-lg-5 ms-md-5 ms-sm-0 ms-0 btn btn-primary mb-lg-4 mb-md-4 mb-sm-0 mb-0">
                      Update Marketing Calendar Content
                    </NavLink>
                  ) : (
                    <NavLink
                      to={`marketing-content`}
                          className="ms-lg-5 ms-md-5 ms-sm-0 ms-0"
                    >
                      Add Marketing Calendar Content
                    </NavLink>
                  )}
                </>
              ) : (
                ""
              )}
            </div>

            <div className="featured_images mb-4">
              <Slider {...settings}>
                {defaultContent &&
                  defaultContent.length > 0 &&
                  defaultContent.map((post) => {
                    if (post.marketingbanner) {
                      return (
                        <div key={post.title} className="carousel-item">
                          <span className="pull-left align-items-center justify-content-left d-flex w-100">
                            <img
                              alt=""
                              className="person-img w-100"
                              src={
                                post.marketingbanner
                                  ? post.marketingbanner.includes("http")
                                    ? post.marketingbanner
                                    : post.marketingbanner
                                  : ""
                              }
                            ></img>
                          </span>
                        </div>
                      );
                    }
                  })}
              </Slider>
            </div>
            <div className="bg-light border rounded-3 p-3">
              <div className="mb-3">
                {defaultContent && defaultContent.length > 0
                  ? defaultContent.map((item) => (
                      <div key={item._id}>
                        <span
                          dangerouslySetInnerHTML={{ __html: item.content }}
                        ></span>
                      </div>
                    ))
                  : ""}

                <hr className="w-100" />
              </div>
              <div className="">
                <div className="row mt-2 pt-0">
                  <div style={{ height: "500px" }}>
                    <BigCalendar
                      localizer={localizer}
                      events={events}
                      startAccessor="start"
                      eventPropGetter={eventStyleGetter}
                      style={{ flex: 1 }}
                      onSelectSlot={handleSelectSlot}
                      selectable={true}
                      onNavigate={handleNavigate}
                      components={{
                        event: ({ event }) => (
                          <div
                            className="rbc-event-content"
                            title={event.title}
                            onClick={() =>
                              handleEventClick(
                                event.title,
                                event.image,
                                event.start,
                                event.id
                              )
                            }
                            data-eventid={event.id}
                          >
                            <div>{event.title}</div>
                            {event.image && (
                              <img
                                src={event.image}
                                alt={event.title}
                                style={{ width: "30px", height: "30px" }}
                              />
                            )}
                          </div>
                        ),
                      }}
                      views={{
                        month: true,
                        week: false,
                        day: false,
                        agenda: false,
                      }}
                      component={{
                        toolbar: CustomToolbar,
                      }}
                    />
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div className="col-md-4 mt-lg-0 mt-md-0 mt-sm-4 mt-4">
            <div className="">
              {dateData ? (
                <div className="bg-light border rounded-3 p-3 mt-5">
                  <ul>
                    <li>
                      <NavLink className="d-block w-100">
                        {moment(dateData).format("MM/DD/YYYY")}
                        {imageData ? (
                          <div className="d-flex">
                            <img
                              src={imageData}
                              alt="Event"
                              style={{
                                cursor: "pointer",
                                width: "200px",
                                height: "200px",
                              }}
                              onClick={() => handleImageDownload(imageData)}
                            />
                            <a
                              style={{
                                cursor: "pointer",
                                width: "120px",
                                height: "50px",
                              }}
                              href={imageData}
                              target="_blank"
                              rel="noreferrer"
                              className="btn btn-primary mt-5 ms-2"
                              onClick={() => handleImageDownload(imageData)}
                            >
                              View/Download
                            </a>
                          </div>
                        ) : (
                          ""
                        )}
                        {textData ? (
                          <div
                            className="d-flex mt-2"
                            onClick={() => handleTextCopy(textData)}
                          >
                            <p style={{ cursor: "pointer" }}>{textData}</p>

                            <i className="fa fa-clone ms-3"></i>
                          </div>
                        ) : (
                          ""
                        )}
                      </NavLink>
                      {localStorage.getItem("auth") &&
                      jwt(localStorage.getItem("auth")).contactType ==
                        "admin" ? (
                        <button
                          className="btn btn-primary ms-2"
                          data-toggle="modal"
                          data-target="#market-calender"
                          onClick={() => handleCalender(textData, imageData)}
                        >
                          Clone
                        </button>
                      ) : (
                        ""
                      )}

                      {localStorage.getItem("auth") &&
                      jwt(localStorage.getItem("auth")).contactType ==
                        "admin" ? (
                        <button
                          className="btn btn-primary ms-2"
                          data-toggle="modal"
                          data-target="#market-calender-update"
                          // onClick={() => handleCalender(textData, imageData)}
                        >
                          Edit
                        </button>
                      ) : (
                        ""
                      )}

                      {localStorage.getItem("auth") &&
                      jwt(localStorage.getItem("auth")).contactType ==
                        "admin" ? (
                        <button
                          className="btn btn-primary ms-2"
                          onClick={() => handleRemoveEvent()}
                        >
                          Delete
                        </button>
                      ) : (
                        ""
                      )}
                    </li>
                  </ul>
                </div>
              ) : (
                  <div className="bg-light border rounded-3 p-3 mt-lg-5 mt-md-5 mt-sm-0 mt-0">
                  <p className="mt-3">
                    The key to a successful real estate social media calendar is
                    to map out your content in advance. Plan real estate posts
                    around key dates, holidays, events, and trends in the real
                    estate market. Share market updates to keep your audience
                    informed and engaged.
                  </p>
                  <h6>Posting Frequency</h6>
                  <p>
                    Decide how often you’ll post. Generally speaking, the more
                    you post, the higher the chances of growing as a real estate
                    agent. Still, it’s important to stick to a consistent
                    posting schedule that works for you, even if you don’t have
                    much time to post. Consistency is vital whether you want to
                    post thrice or seven times a week. However, don’t forget
                    that quality trumps quantity.
                    <br />
                    Focus on creating great ideas that resonate with your
                    audience. Don’t commit to an unrealistic posting calendar.
                  </p>
                  <button onClick={handleClicktext} className="btn btn-primary">
                    Show More
                  </button>
                  {moreText ? (
                    <>
                      <h6>Monitor and Adjust</h6>
                      <p>
                        Constantly monitor your social media performance. Use
                        free analytics tools like the ones integrated with your
                        social platforms. Monitor which posts perform best. Are
                        open houses bringing in the most engagement, or do
                        people react well to “just listed” posts? Take a step
                        back and assess whether your social media content is
                        performing as intended. Approach it with a marketer’s
                        mindset.
                      </p>
                      <h6>Use AI When It Makes Sense</h6>
                      <p>
                        Consider using AI chatbots to engage with your audience
                        24/7. Allow them to answer common questions and provide
                        immediate assistance. During this season, you can also
                        use AI to brainstorm digital marketing campaign ideas
                        during busy times. Use it to generate holiday marketing
                        content or Black Friday ideas.
                      </p>
                      <h4>10 Creative Real Estate Calendar Ideas</h4>
                      <h6>Property Spotlight Mondays</h6>
                      <p>
                        Start the week by showcasing a different property every
                        Monday. Share stunning photos, videos, and compelling
                        descriptions of new homes on the market. Highlight
                        unique features and potential benefits for buyers.
                      </p>
                      <h6>Testimonial Tuesdays</h6>
                      <p>
                        Share success stories from satisfied past clients.
                        Feature their testimonials, along with before-and-after
                        photos, if you can.
                        <br />
                        This builds trust and demonstrates your track record,
                        showing people why they should choose you over other
                        agents.
                      </p>
                      <h6>Tips and Tricks Wednesdays</h6>
                      <p>
                        Share valuable tips and insights related to the real
                        estate industry. Offer advice on: Buying Selling
                        Investing Home maintenance This will help you position
                        yourself as a knowledgeable industry expert and build
                        trust with your audience.
                      </p>
                      <h6>Throwback Thursdays</h6>
                      <p>
                        Everyone loves a TBT. Take a trip down memory lane and
                        post about a significant milestone or achievement in
                        your real estate career. Share stories of challenges
                        overcome and lessons learned. It’s also a chance to
                        inspire others and become a thought leader in the real
                        estate industry
                      </p>
                      <h6>Fun Facts Fridays</h6>
                      <p>
                        Share interesting and lesser-known facts about the local
                        real estate market or your community. This can be about
                        historical landmarks, local events, or intriguing
                        statistics. Use information from your blog to create
                        these social media posts, as it saves time.
                      </p>
                      <h6>Small Business Saturday</h6>
                      <p>
                        Small Business Saturday is the perfect opportunity to
                        show your support for local businesses and create
                        engaging content. Feature local businesses in your
                        community and improve your real estate farming efforts.
                        Share their stories, highlight unique offerings, and
                        encourage your audience to shop locally.
                      </p>
                      <h6>Market Update Mondays</h6>
                      <p>
                        Create short video tours of the properties you’re
                        representing. Videos perform best on social media,
                        offering the highest ROI from all other social media
                        content. Thus, make sure to use this content format more
                        often than just once a week. Although more difficult to
                        create, it’s shareable, clickable, and overall engaging.
                        Highlight key features, the neighborhood, and any recent
                        renovations or upgrades.
                      </p>
                      <h6>Ask Me Anything (AMA) Wednesdays</h6>
                      <p>
                        Encourage your audience to ask real estate-related
                        questions. We suggest using Instagram stories for this
                        particular type of content. Stories are personal and
                        prompt immediate engagement, as they disappear after 24
                        hours. Use them to answer your audience’s questions and
                        keep them on their toes each Wednesday, awaiting your
                        branded stories.
                      </p>
                      <h6>Weekend Wanderlust: Virtual Door Knocking</h6>
                      <p>
                        On weekends, share inspiring photos or stories of
                        beautiful homes and dream destinations. Make your social
                        media page feel like a virtual getaway for your
                        followers. Open up discussions about top destinations,
                        must-visit places in your state, “Best Spots To Visit in
                        California,” or dream homes. Connect the idea of
                        homeownership with a sense of adventure and aspiration.
                        Appealing to emotion is the real deal on social media,
                        so make sure to share popular images or custom content
                        that resonates well with their pathos.
                      </p>
                    </>
                  ) : (
                    ""
                  )}
                </div>
              )}
            </div>
          </div>

          {isModalVisible && (
            <>
              <div
                className="modal"
                role="dialog"
                id="modal-show-calender-market"
              >
                <div
                  className="modal-dialog modal-lg modal-dialog-scrollable"
                  role="document"
                >
                  <div className="modal-content">
                    <div className="modal-header">
                      <h4 className="modal-title">Add an Market Event</h4>
                      <button
                        className="btn-close"
                        type="button"
                        data-bs-dismiss="modal"
                        aria-label="Close"
                      ></button>
                    </div>
                    <div className="modal-body fs-sm">
                      <div className="col-md-12">
                        <div className="mb-3">
                          <label className="form-label" for="pr-city">
                            Market Text<span className="text-danger">*</span>
                          </label>
                          <textarea
                            className="form-control mt-0"
                            id="textarea-input"
                            rows="5"
                            name="marketText"
                            onChange={handleChange}
                            autoComplete="on"
                            value={formValues.marketText}
                            style={{
                              border: formErrors?.marketText
                                ? "1px solid red"
                                : "",
                            }}
                          ></textarea>
                          <div className="invalid-tooltip">
                            {formErrors.marketText}
                          </div>
                        </div>

                        <div className="mb-3">
                          <label className="form-label" for="pr-birth-date">
                            Market Date
                          </label>
                          <input
                            className="form-control"
                            type="date"
                            id="inline-form-input"
                            placeholder="Choose date"
                            name="marketDate"
                            onChange={handleChange}
                            value={formValues.marketDate}
                            style={{
                              border: formErrors?.marketDate
                                ? "1px solid red"
                                : "1px solid #00000026",
                            }}
                            data-datepicker-options='{"altInput": true, "altFormat": "F j, Y", "dateFormat": "Y-m-d"}'
                          />
                          <div className="invalid-tooltip">
                            {formErrors.marketDate}
                          </div>
                        </div>

                        <div className="mb-3">
                          <label className="form-label" for="pr-birth-date">
                            Uplaod Image
                          </label>
                          <input
                            className="file-uploader file-uploader-grid"
                            id="inline-form-input"
                            type="file"
                            name="image"
                            onChange={handleFileUpload}
                          />
                        </div>

                        <div className="pull-right mt-3">
                          {localStorage.getItem("auth") &&
                          jwt(localStorage.getItem("auth")).contactType ==
                            "admin" ? (
                            <button
                              onClick={(e) => handleSubmit(e, selectedDate)}
                              type="button"
                              className="btn btn-primary btn-sm "
                              id="save-button"
                            >
                              Add Event
                            </button>
                          ) : (
                            ""
                          )}
                          <button
                            type="button"
                            className="btn btn-secondary btn-sm ms-3"
                            id="closeUpdate"
                            data-bs-dismiss="modal"
                          >
                            Cancel & Close
                          </button>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </>
          )}

          {isModalVisibleUpdate && (
            <div className="modal" role="dialog" id="market-calender">
              <div
                className="modal-dialog modal-md modal-dialog-scrollable"
                role="document"
              >
                <div className="modal-content">
                  <div className="modal-header">
                    <h4 className="modal-title">Add an Market Event</h4>
                    <button
                      className="btn-close"
                      type="button"
                      data-bs-dismiss="modal"
                      aria-label="Close"
                      onClick={handleClose}
                    ></button>
                  </div>
                  <div className="modal-body fs-sm">
                    <div className="col-md-12">
                      <div className="mb-3">
                        <label className="form-label" for="pr-city">
                          Market Text<span className="text-danger">*</span>
                        </label>
                        <textarea
                          className="form-control mt-0"
                          id="textarea-input-market_calender"
                          rows="5"
                          name="marketText"
                          onChange={handleChange}
                          autoComplete="on"
                          value={formValues.marketText}
                          style={{
                            border: formErrors?.marketText
                              ? "1px solid red"
                              : "",
                          }}
                        ></textarea>
                        <div className="invalid-tooltip">
                          {formErrors.marketText}
                        </div>
                      </div>

                      <div className="mb-3">
                        <label className="form-label" for="pr-birth-date">
                          Market Date
                        </label>
                        <input
                          className="form-control"
                          type="date"
                          id="inline-form-input"
                          placeholder="Choose date"
                          name="marketDate"
                          onChange={handleChange}
                          value={formValues.marketDate}
                          style={{
                            border: formErrors?.marketDate
                              ? "1px solid red"
                              : "1px solid #00000026",
                          }}
                          data-datepicker-options='{"altInput": true, "altFormat": "F j, Y", "dateFormat": "Y-m-d"}'
                        />
                        <div className="invalid-tooltip">
                          {formErrors.marketDate}
                        </div>
                      </div>

                      {data ? (
                        <>
                          <img src={data} alt="Uploaded" />
                        </>
                      ) : formValues.image ? (
                        <>
                          <img src={formValues.image} alt="Uploaded" />
                          <div className="mb-3">
                            <label
                              className="form-label"
                              htmlFor="pr-birth-date"
                            >
                              Upload Image
                            </label>
                            <input
                              className="file-uploader file-uploader-grid"
                              id="inline-form-input"
                              type="file"
                              name="image"
                              onChange={handleFileUpload}
                            />
                          </div>
                        </>
                      ) : (
                        <div className="mb-3">
                          <label className="form-label" htmlFor="pr-birth-date">
                            Upload Image
                          </label>
                          <input
                            className="file-uploader file-uploader-grid"
                            id="inline-form-input"
                            type="file"
                            name="image"
                            onChange={handleFileUpload}
                          />
                        </div>
                      )}

                      <div className="pull-right mt-3">
                        {localStorage.getItem("auth") &&
                        jwt(localStorage.getItem("auth")).contactType ==
                          "admin" ? (
                          <button
                            onClick={(e) => handleSubmitClone(e)}
                            type="button"
                            className="btn btn-primary btn-sm "
                            id="save-button"
                          >
                            Add an Event
                          </button>
                        ) : (
                          ""
                        )}
                        <button
                          type="button"
                          className="btn btn-secondary btn-sm ms-3"
                          id="close"
                          data-bs-dismiss="modal"
                          onClick={handleClose}
                        >
                          Cancel & Close
                        </button>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          )}

          {eventId ? (
            <div className="modal" role="dialog" id="market-calender-update">
              <div
                className="modal-dialog modal-md modal-dialog-scrollable"
                role="document"
              >
                <div className="modal-content">
                  <div className="modal-header">
                    <h4 className="modal-title">Update Market Event</h4>
                  </div>
                  <div className="modal-body fs-sm">
                    <div className="col-md-12">
                      <div className="mb-3">
                        <label className="form-label" for="pr-city">
                          Market Text<span className="text-danger">*</span>
                        </label>
                        <textarea
                          className="form-control mt-0"
                          id="textarea-input-market_calender"
                          rows="5"
                          name="marketText"
                          onChange={handleChange}
                          autoComplete="on"
                          value={formValues.marketText}
                          style={{
                            border: formErrors?.marketText
                              ? "1px solid red"
                              : "",
                          }}
                        ></textarea>
                        <div className="invalid-tooltip">
                          {formErrors.marketText}
                        </div>
                      </div>

                      <div className="mb-3">
                        <label className="form-label" for="pr-birth-date">
                          Market Date
                        </label>
                        <input
                          className="form-control"
                          type="date"
                          id="inline-form-input"
                          placeholder="Choose date"
                          name="marketDate"
                          onChange={handleChange}
                          value={formValues.marketDate}
                          style={{
                            border: formErrors?.marketDate
                              ? "1px solid red"
                              : "1px solid #00000026",
                          }}
                          data-datepicker-options='{"altInput": true, "altFormat": "F j, Y", "dateFormat": "Y-m-d"}'
                        />
                        <div className="invalid-tooltip">
                          {formErrors.marketDate}
                        </div>
                      </div>

                      {data ? (
                        <>
                          <img src={data} alt="Uploaded" />
                        </>
                      ) : formValues.image ? (
                        <>
                          <img src={formValues.image} alt="Uploaded" />
                          <div className="mb-3">
                            <label
                              className="form-label"
                              htmlFor="pr-birth-date"
                            >
                              Upload Image
                            </label>
                            <input
                              className="file-uploader file-uploader-grid"
                              id="inline-form-input"
                              type="file"
                              name="image"
                              onChange={handleFileUpload}
                            />
                          </div>
                        </>
                      ) : (
                        <div className="mb-3">
                          <label className="form-label" htmlFor="pr-birth-date">
                            Upload Image
                          </label>
                          <input
                            className="file-uploader file-uploader-grid"
                            id="inline-form-input"
                            type="file"
                            name="image"
                            onChange={handleFileUpload}
                          />
                        </div>
                      )}

                      <div className="pull-right mt-3">
                        {localStorage.getItem("auth") &&
                        jwt(localStorage.getItem("auth")).contactType ==
                          "admin" ? (
                          <button
                            onClick={(e) => handleSubmitUpdate(e)}
                            type="button"
                            className="btn btn-primary btn-sm "
                            id="save-button"
                          >
                            Update an Event
                          </button>
                        ) : (
                          ""
                        )}
                        <button
                          type="button"
                          className="btn btn-secondary btn-sm ms-3"
                          data-dismiss="modal"
                          aria-label="Close"
                          id="closeModal"
                          onClick={handleCloseUpdate}
                        >
                          Cancel & Close
                        </button>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          ) : (
            ""
          )}
        </div>
      </main>
    </div>
  );
};

export default MarKetCalender;

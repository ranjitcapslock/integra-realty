import React, { useState, useEffect, useRef } from "react";
import { useNavigate, NavLink, useParams } from "react-router-dom";
import user_service from "../service/user_service";
import Loader from "../../Pages/Loader/Loader.js";
import Toaster from "../../Pages/Toaster/Toaster.js";
import _ from "lodash";
import axios from "axios";
import jwt from "jwt-decode";
import { Editor } from "react-draft-wysiwyg";
import { EditorState, convertToRaw } from "draft-js";
import draftToHtml from "draftjs-to-html";
import "react-draft-wysiwyg/dist/react-draft-wysiwyg.css";

function NewPost() {
  const [step1, setStep1] = useState("");
  const [category, setCategory] = useState("");
  const [step2, setStep2] = useState("");
  const [channel, setChannel] = useState("");
  const [step3, setStep3] = useState("");
  const [step4, setStep4] = useState("");

  const [loader, setLoader] = useState({ isActive: null });
  const { isActive } = loader;
  const [toaster, setToaster] = useState({
    types: null,
    isShow: null,
    toasterBody: "",
    message: "",
  });
  const { types, isShow, toasterBody, message } = toaster;

  const [file, setFile] = useState(null);
  const [data, setData] = useState("");

  const [editData, setEditData] = useState(false);
  const [pinArticle, setPinArticle] = useState("");
  const [checkPublication, setCheckPublication] = useState("");

  const [detail, setDetail] = useState("");
  const params = useParams();

  const initialValues = {
    category: "",
    channel: "",
    fullMessage: "",
    postBlurb: "",
    postTitle: "",
    author: "",
    articleComments: "No",
    linkURL: "",
    publicationLevel: "office",
    publishDate: "",
    file: "",
    pin_date: "",
    specific: "Corporate",
    pin_article: "No",
  };
  const [formValues, setFormValues] = useState(initialValues);
  const [formErrors, setFormErrors] = useState({});
  const [isSubmitClick, setISSubmitClick] = useState(false);

  const navigate = useNavigate();

  const editorRef = useRef(null);
  const log = () => {
    if (editorRef.current) {
      console.log(editorRef.current.getContent());
    }
  };

  const handleEdit = () => {
    setEditData(true);
  };

  const handlePinArticle = (e) => {
    setPinArticle(e.target.name);
    setPinArticle(e.target.value);
  };

  {
    /* <!-- Input onChange Start--> */
  }
  const handleChange = (e) => {
    const { name, value } = e.target;
    setFormValues({ ...formValues, [name]: value });
  };

  // const handleEditorChange = (e) => {
  //   setFormValues({ ...formValues, fullMessage: e });
  // };

  const CheckList = (e) => {
    setCheckPublication(e.target.name);
    setCheckPublication(e.target.value);
  };

  const handleFileUpload = async (e) => {
    const selectedFile = e.target.files[0];

    if (selectedFile) {
      setFile(selectedFile);

      const formData = new FormData();
      formData.append("file", selectedFile);

      const config = {
        headers: {
          Accept: "application/json",
          "Content-Type": "multipart/form-data",
          Authorization: `Bearer ${localStorage.getItem("auth")}`,
        },
      };

      try {
        const uploadResponse = await axios.post(
          "https://api.brokeragentbase.com/upload",
          formData,
          config
        );
        if (uploadResponse) {
          setData(uploadResponse.data);
        }
      } catch (error) {
        console.error("Error occurred during file upload:", error);
      }
    }
  };
  const [editorState, setEditorState] = useState(EditorState.createEmpty());

  const handleChanges = (editorState) => {
    setEditorState(editorState);
    const htmlContent = draftToHtml(
      convertToRaw(editorState.getCurrentContent())
    );
    setFormValues({ ...formValues, fullMessage: htmlContent });
  };

  useEffect(() => {
    if (formValues && isSubmitClick) {
      validate();
    }
  }, [formValues]);

  const validate = () => {
    const values = formValues;
    const errors = {};
    if (!values.postBlurb) {
      errors.postBlurb = "postBlurb is required!";
    }
    if (!values.postTitle) {
      errors.postTitle = "postTitle is required!";
    }
    if (pinArticle) {
      if (!values.pin_date) {
        errors.pin_date = "pin_date is required!";
      }
    }
    setFormErrors(errors);
    return errors;
  };
  {
    /* <!-- Form Validation End--> */
  }

  {
    /* <!-- Api call  Function Start--> */
  }
  const PostAll = async (e) => {
    e.preventDefault();
    setISSubmitClick(true);
    let checkValue = validate();
    if (_.isEmpty(checkValue)) {
      const userData = {
        office_id: localStorage.getItem("active_office_id"),
        office_name: localStorage.getItem("active_office"),
        channel: channel,
        category: category,
        fullMessage: formValues.fullMessage,
        postBlurb: formValues.postBlurb,
        postTitle: formValues.postTitle,
        file: data,
        author: jwt(localStorage.getItem("auth")).id,
        articleComments: formValues.articleComments,
        linkURL: formValues.linkURL,
        publicationLevel: checkPublication,
        publishDate: formValues.publishDate
          ? formValues.publishDate
          : defaultValue,
        pin_article: pinArticle,
        specific: formValues.specific,
        pin_date: formValues.pin_date,
      };
      console.log(userData);
      setLoader({ isActive: true });
      await user_service.postCreate(userData).then((response) => {
        if (response) {
          setData(response.data);
          setLoader({ isActive: false });
          setToaster({
            type: "Posted Successfully",
            isShow: true,
            toasterBody: response.data.message,
            message: "Posted Successfully",
          });
          setTimeout(() => {
            navigate("/ ");
          }, 500);
        } else {
          setLoader({ isActive: false });
          setToaster({
            types: "error",
            isShow: true,
            toasterBody: response.data.message,
            message: "Error",
          });
        }
      });
    }
  };
  {
    /* <!-- Api call  Function End--> */
  }

  {
    /* <!-- Add value  Function Start--> */
  }
  const AssociatePost = (stepno, value) => {
    if (stepno === "1") {
      setStep1(stepno);
      setCategory(value);
      //console.log(value)
    }
    if (stepno === "2") {
      setStep2(stepno);
      setChannel(value);
      //console.log(value)
    }
    if (stepno === "3") {
      setStep3(stepno);
      setCategory(value);
      //console.log(value)
    }
    if (stepno === "4") {
      setStep4(stepno);
      setChannel(value);
      //console.log(value)
    } else {
      setStep1(stepno);
    }
  };
  {
    /* <!-- Add value  Function End--> */
  }

  const TransactionStepBack = (step, value) => {
    console.log("1111111");
    if (step === "step2") {
      setStep1("");
      setStep2("");
      setStep3("");
      setStep4("");
    }
    if (step === "step3") {
      setStep2("");
      setStep3("");
      setStep4("");
    }

    if (step === "step4") {
      setStep1("");
      setStep2("");
      setStep3("");
      setStep4("");
    }
  };

  const currentDate = new Date();
  const day = currentDate.getDate().toString().padStart(2, "0");
  const month = (currentDate.getMonth() + 1).toString().padStart(2, "0");
  const year = currentDate.getFullYear();

  const hours = currentDate.getHours().toString().padStart(2, "0");
  const minutes = currentDate.getMinutes().toString().padStart(2, "0");

  const defaultValue = formValues.publishDate
    ? ""
    : `${year}-${month}-${day}T${hours}:${minutes}`;

  const handleCloseData = () => {
    navigate("/");
  };
  return (
    <div className="bg-secondary float-left w-100 pt-4 mb-4">
      <Loader isActive={isActive} />
      {isShow && (
        <Toaster
          types={types}
          isShow={isShow}
          toasterBody={toasterBody}
          message={message}
        />
      )}
      <main className="page-wrapper profile_page_wrap">
        {/* <!-- Page container--> */}
        <div className="content-overlay">
          <div className="justify-content-start mb-4">
            <h3 className="text-white mb-0">Post an Entry</h3>
          </div>
          <>
            {/* <!-- Step 1 start here--> */}
            {step1 === "" && step3 === "" ? (
              <div className="bg-light rounded-3 mb-0 p-3 p-3">
                <p>
                  With your help, the office can stay connected with the real
                  estate industry.
                  <br />
                  Be sure to review the Office Article Policy before posting a
                  new article.
                </p>
                <h5 className="mb-4">Let's get started, select a category:</h5>
                <div className="col-lg-12 mb-2">
                  <div
                    className="border rounded-3 p-3 selected_hover"
                    onClick={() => AssociatePost("1", "OfficePost")}
                  >
                    <div className="d-flex align-items-center justify-content-between">
                      <div className="pe-2">
                        <label className="col-form-label">Office Post</label>
                        <div id="type-value">
                          Promote key events and topics. Available to office
                          staff only.
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="col-lg-12 mb-2">
                  <div
                    className="border rounded-3 p-3 selected_hover"
                    onClick={() => AssociatePost("1", "groupPost")}
                  >
                    <div className="d-flex align-items-center justify-content-between">
                      <div className="pe-2">
                        <label className="col-form-label">Group Post</label>
                        <div id="type-value">
                          Share ideas and announcements to fellow group members.
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="col-lg-12 mb-2">
                  <div
                    className="border rounded-3 p-3 selected_hover"
                    onClick={() => AssociatePost("1", "associate")}
                  >
                    <div className="d-flex align-items-center justify-content-between">
                      <div className="pe-2">
                        <label className="col-form-label">Associate Post</label>
                        <div id="type-value">
                          Add a Have/Want, Open House, or an announcement.
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="col-lg-12 mb-2">
                  <div
                    className="border rounded-3 p-3 selected_hover"
                    onClick={() => AssociatePost("3", "Community")}
                  >
                    <div className="d-flex align-items-center justify-content-between">
                      <div className="pe-3">
                        <label className="col-form-label">Community News</label>
                        <div id="type-value">
                          Link to industry news and promote a mixer.
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            ) : (
              ""
            )}
            {/* <!-- Step 1 ends here--> */}

            {/* <!-- Step 2 ts here--> */}
            {step1 && step3 === "" && step2 === "" ? (
              <div className="bg-light rounded-3 mb-0 p-3">
                <p>
                  <i className="fa fa-check" aria-hidden="true"></i> You
                  selected the '{category}' category. &nbsp;
                  <a onClick={(e) => TransactionStepBack("step2")}>
                    <NavLink>Change</NavLink>
                  </a>
                </p>

                <h2 className="h4 mb-2">Select a channel:</h2>
                <div className="col-lg-12 mb-2">
                  <div
                    className="border rounded-3 p-3 selected_hover"
                    onClick={() => AssociatePost("2", "Agent Announcement")}
                  >
                    <div className="d-flex align-items-center justify-content-between">
                      <div className="pe-2">
                        <label className="col-form-label">
                          Agent Announcement
                        </label>
                        <div id="type-value">
                          Publish a message to fellow associates.
                        </div>
                      </div>
                    </div>
                  </div>
                </div>

                <div className="col-lg-12 mb-2">
                  <div
                    className="border rounded-3 p-3 selected_hover"
                    onClick={() => AssociatePost("2", "Have Item/Listing")}
                  >
                    <div className="d-flex align-items-center justify-content-between">
                      <div className="pe-2">
                        <label className="col-form-label">
                          Have Item/Listing
                        </label>
                        <div id="type-value">
                          Offer a listing or service you have.
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="col-lg-12 mb-2">
                  <div
                    className="border rounded-3 p-3 selected_hover"
                    onClick={() => AssociatePost("2", "Want Buyer/Vendor")}
                  >
                    <div className="d-flex align-items-center justify-content-between">
                      <div className="pe-2">
                        <label className="col-form-label">
                          Want Buyer/Vendor
                        </label>
                        <div id="type-value">
                          Identify a type of buyer or vendor you are looking
                          for.
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="col-lg-12 mb-2">
                  <div
                    className="border rounded-3 p-3 selected_hover"
                    onClick={() => AssociatePost("2", "Open House")}
                  >
                    <div className="d-flex align-items-center justify-content-between">
                      <div className="pe-2">
                        <label className="col-form-label">Open House</label>
                        <div id="type-value">
                          Publish an upcoming open house event.
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            ) : (
              ""
            )}
            {/* <!-- Step 2 ends here--> */}

            {/* <!-- Step 3 starts here--> */}
            {step2 && step3 === "" ? (
              <div className="row">
                <div className="col-md-8">
                  <div className="bg-light rounded-3 p-3 border mb-0">
                    <p>
                      <i className="fa fa-check" aria-hidden="true"></i> You
                      selected the '{category}' category.&nbsp;
                      <a onClick={(e) => TransactionStepBack("step2")}>
                        <NavLink>Change</NavLink>
                      </a>
                    </p>

                    <p>
                      <i className="fa fa-check" aria-hidden="true"></i> You
                      selected the '{channel}' .&nbsp;
                      <a onClick={(e) => TransactionStepBack("step3")}>
                        <NavLink>Change</NavLink>
                      </a>
                    </p>
                    <hr />

                    <h5 className="h4 mb-2 mt-3">Add details for this post:</h5>

                    <form className="needs-validation">
                      <div className="mb-3">
                        <label className="col-form-label">Post Title*</label>
                        <input
                          className="form-control"
                          type="text"
                          id="text-input"
                          name="postTitle"
                          placeholder="Enter your post title"
                          onChange={handleChange}
                          autoComplete="on"
                          style={{
                            border: formErrors?.postTitle
                              ? "1px solid red"
                              : "1px solid #00000026",
                          }}
                          value={formValues.postTitle}
                        />
                        {formErrors.postTitle && (
                          <div className="invalid-tooltip">
                            {formErrors.postTitle}
                          </div>
                        )}
                      </div>

                      <div className="mb-3">
                        <label className="col-form-label">Publish Date</label>
                        <input
                          className="form-control"
                          type="datetime-local"
                          id="text-input"
                          name="publishDate"
                          onChange={handleChange}
                          autoComplete="on"
                          value={
                            defaultValue ? defaultValue : formValues.publishDate
                          }
                        />
                      </div>

                      <div className="mb-3">
                        <label className="col-form-label">
                          Link URL&nbsp;{" "}
                          <small className="option">Optional</small>
                        </label>
                        <br />
                        <input
                          className="form-control"
                          type="text"
                          id="text-input"
                          name="linkURL"
                          onChange={handleChange}
                          autoComplete="on"
                          value={formValues.linkURL}
                        />
                      </div>

                      <div className="mb-3 mt-3">
                        <label className="col-form-label">
                          Attach a Photo or PDF File&nbsp;{" "}
                          <small className="option">Optional</small>
                        </label>
                        <input
                          className="file-uploader file-uploader-grid"
                          id="inline-form-input"
                          type="file"
                          name="image"
                          onChange={handleFileUpload}
                        />
                      </div>

                      <div className="mb-3 mt-3">
                        <label className="col-form-label float-left w-100 mb-0">
                          <p className="pull-left mr-2 mb-0">Post Blurb*</p>
                          <span>
                            <small className="pull-right mb-0">190chr</small>
                          </span>
                        </label>
                        <textarea
                          className="form-control mt-0"
                          id="textarea-input"
                          rows="5"
                          name="postBlurb"
                          onChange={handleChange}
                          autoComplete="on"
                          value={formValues.postBlurb}
                          style={{
                            border: formErrors?.postBlurb
                              ? "1px solid red"
                              : "1px solid #00000026",
                          }}
                        >
                          Hello World!
                        </textarea>
                        {formErrors.postBlurb && (
                          <div className="invalid-tooltip">
                            {formErrors.postBlurb}
                          </div>
                        )}
                      </div>

                      {editData ? (
                        <div className="mb-3">
                          <label className="col-form-label">Full Message</label>
                          <Editor
                            editorState={editorState}
                            onEditorStateChange={handleChanges}
                            value={formValues.fullMessage}
                            toolbar={{
                              options: [
                                "inline",
                                "blockType",
                                "fontSize",
                                "list",
                                "textAlign",
                                "history",
                                "link", // Add link option here
                              ],
                              inline: {
                                options: [
                                  "bold",
                                  "italic",
                                  "underline",
                                  "strikethrough",
                                ],
                              },
                              list: { options: ["unordered", "ordered"] },
                              textAlign: {
                                options: ["left", "center", "right"],
                              },
                              history: { options: ["undo", "redo"] },
                              link: {
                                // Configure link options
                                options: ["link", "unlink"],
                              },
                            }}
                            wrapperClassName="demo-wrapper"
                            editorClassName="demo-editor"
                          />
                          ;
                        </div>
                      ) : (
                        <label className="form-check-label d-flex">
                          Blurb too short, do you have more to say?
                          <NavLink>
                            <p onClick={(e) => handleEdit(e)}>
                              &nbsp;Add Full Message
                            </p>
                          </NavLink>
                        </label>
                      )}

                      {/* <!-- Inline radios --> */}
                      <div className="mb-2">
                        <label className="col-form-label">
                          Publication Level
                        </label>
                        <p className="form-check-label mb-2">
                          Where should your post be shown?
                        </p>

                        <div
                          className="form-check d-flex"
                          onClick={(e) => CheckList(e)}
                        >
                          <div className="">
                            <input
                              className="form-check-input"
                              id="form-radio-four"
                              type="radio"
                              name="publicationLevel"
                              value="office"
                            />
                            <label className="form-check-label">
                              My Office Only
                            </label>
                          </div>
                          <div className="ms-4">
                            <input
                              className="form-check-input"
                              id="form-radio-4"
                              type="radio"
                              name="publicationLevel"
                              value="regional"
                            />
                            <label className="form-check-label ps-2">
                              Regional Offices
                            </label>
                          </div>

                          <div className="ms-4">
                            <input
                              className="form-check-input"
                              id="form-radio-4"
                              type="radio"
                              name="publicationLevel"
                              value="choose"
                            />
                            <label className="form-check-label ps-2">
                              Let Me Choose
                            </label>
                          </div>
                        </div>
                      </div>

                      {checkPublication === "choose" ? (
                        <>
                          <div className="mb-3">
                            <label className="col-form-label">
                              Specific Office:{" "}
                            </label>
                            <select
                              className="form-select mt-2"
                              id="pr-city"
                              name="specific"
                              onChange={handleChange}
                              value={formValues.specific}
                            >
                              <option></option>
                              <option>Corporate</option>
                            </select>
                          </div>
                        </>
                      ) : (
                        ""
                      )}

                      <label className="col-form-label">Article Comments</label>
                      <div className="mb-3">
                        <div className="form-check">
                          <input
                            className="form-check-input"
                            type="checkbox"
                            id="invalidCheck"
                            name="articleComments"
                            onChange={handleChange}
                            value="Yes"
                            required
                          />
                          <label className="form-check-label">
                            Yes, Allow everyone in the office to comment on this
                            post.
                          </label>
                        </div>
                      </div>

                      <label className="form-check-label d-flex">
                        <h6 className="mb-0">Pin Article</h6>&nbsp;&nbsp;
                        <span>Staff only option</span>
                      </label>
                      <div className="my-2">
                        <div className="form-check">
                          <input
                            className="form-check-input"
                            type="checkbox"
                            id="invalidCheck"
                            name="pin_article"
                            onClick={(e) => handlePinArticle(e)}
                            value="Yes"
                            required
                          />
                          <label className="form-check-label">
                            Yes, Pin this article above standard posts.
                          </label>
                        </div>
                      </div>

                      {pinArticle === "Yes" ? (
                        <>
                          <div className="mb-3">
                            <label className="col-form-label">
                              Pin Removal Date:
                            </label>
                            <input
                              className="form-control"
                              type="date"
                              id="text-input"
                              name="pin_date"
                              onChange={handleChange}
                              autoComplete="on"
                              value={formValues.pin_date}
                              style={{
                                border: formErrors?.pin_date
                                  ? "1px solid red"
                                  : "1px solid #00000026",
                              }}
                            />
                            {formErrors.pin_date && (
                              <div className="invalid-tooltip">
                                {formErrors.pin_date}
                              </div>
                            )}
                          </div>
                        </>
                      ) : (
                        ""
                      )}
                    </form>
                  </div>
                  <div className="pull-right mt-4">
                    <button
                      type="button"
                      className="btn btn-primary btn-sm"
                      id="save-button"
                      onClick={PostAll}
                    >
                      Publish Post
                    </button>
                    <button
                      className="btn btn-secondary btn-sm ms-3"
                      onClick={handleCloseData}
                    >
                      Cancel & Go Back
                    </button>
                  </div>
                </div>
                <div className="col-md-4 mt-lg-0 mt-md-0 mt-sm-4 mt-4">
                  <div id="accordionCards" className="">
                    <div className="card bg-secondary mb-2 mt-0">
                      <div className="card-body">
                        <h3 className="h3 card-title pt-1 mb-0">
                          ✅ Post Status
                        </h3>
                      </div>
                      <div
                        className="collapse show"
                        id="cardCollapse1"
                        data-bs-parent="#accordionCards"
                      >
                        <div className="card-body mt-n1 pt-0">
                          <ul className="list-unstyled mb-2">
                            <li className="d-flex align-items-center mb-2">
                              {category ? (
                                <>
                                  <i className="fi-check text-primary me-2"></i>
                                  <a
                                    className="nav-link fw-normal ps-1 p-0"
                                    href="#basic-info"
                                    data-scroll=""
                                    data-scroll-offset="20"
                                  >
                                    Category
                                  </a>
                                </>
                              ) : (
                                <>
                                  <img src="https://cdn.pboffice.net/shared/transaction/page_select_drop.png" />
                                  <a
                                    className="nav-link fw-normal ps-1 p-0"
                                    href="#basic-info"
                                    data-scroll=""
                                    data-scroll-offset="20"
                                  >
                                    Category
                                  </a>
                                </>
                              )}
                            </li>

                            <li className="d-flex align-items-center mb-2">
                              {channel ? (
                                <>
                                  <i className="fi-check text-primary me-2"></i>
                                  <a
                                    className="nav-link fw-normal ps-1 p-0"
                                    href="#basic-info"
                                    data-scroll=""
                                    data-scroll-offset="20"
                                  >
                                    Channel
                                  </a>
                                </>
                              ) : (
                                <>
                                  <img src="https://cdn.pboffice.net/shared/transaction/page_select_drop.png" />
                                  <a
                                    className="nav-link fw-normal ps-1 p-0"
                                    href="#basic-info"
                                    data-scroll=""
                                    data-scroll-offset="20"
                                  >
                                    Channel
                                  </a>
                                </>
                              )}
                            </li>
                          </ul>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            ) : (
              ""
            )}
            {/* <!-- Step 3 ends here--> */}

            {/* <!-- Step 4 starts here--> */}
            {step3 && step4 === "" ? (
              <div className="bg-light rounded-3 mb-0 p-3">
                <p>
                  <i className="fa fa-check" aria-hidden="true"></i> You
                  selected the '{category}' category.&nbsp;
                  <a onClick={(e) => TransactionStepBack("step2")}>
                    <NavLink>Change</NavLink>
                  </a>
                </p>

                <h2 className="h4 mb-2">Select a channel:</h2>
                <div className="col-lg-12 mb-2">
                  <div
                    className="border rounded-3 p-3 selected_hover"
                    onClick={() => AssociatePost("4", "Industry News")}
                  >
                    <div className="d-flex align-items-center justify-content-between">
                      <div className="pe-2">
                        <label className="col-form-label">Industry News</label>
                        <div id="type-value">
                          Link to a resource in the real estate industry.
                        </div>
                      </div>
                    </div>
                  </div>
                </div>

                <div className="col-lg-12 mb-2">
                  <div
                    className="border rounded-3 p-3 selected_hover"
                    onClick={() => AssociatePost("4", "Networking Mixer")}
                  >
                    <div className="d-flex align-items-center justify-content-between">
                      <div className="pe-2">
                        <label className="col-form-label">
                          Networking Mixer
                        </label>
                        <div id="type-value">
                          Promote an upcoming event in the real estate
                          community.
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            ) : (
              ""
            )}
            {/* <!-- Step 4 ends here--> */}

            {/* <!-- Step 5 starts here--> */}
            {step4 ? (
              <div className="row">
                <div className="col-md-8">
                  <div className="bg-light rounded-3 border mb-0">
                    <p>
                      <i className="fa fa-check" aria-hidden="true"></i> You
                      selected the '{category}' category.&nbsp;
                      <a onClick={(e) => TransactionStepBack("step2")}>
                        <NavLink>Change</NavLink>
                      </a>
                    </p>

                    <p>
                      <i className="fa fa-check" aria-hidden="true"></i> You
                      selected the '{channel}' .&nbsp;
                      <a onClick={(e) => TransactionStepBack("step3")}>
                        <NavLink>Change</NavLink>
                      </a>
                    </p>
                    <hr />

                    <h5 className="h4 mb-2 mt-3">Add details for this post:</h5>
                    <form className="needs-validation">
                      <div className="mb-3">
                        <label className="form-label">
                          Post Title &nbsp;
                          <span className="required">Required</span>
                        </label>
                        <input
                          className="form-control"
                          type="text"
                          id="text-input"
                          name="postTitle"
                          placeholder="Enter your post title"
                          onChange={handleChange}
                          autoComplete="on"
                          style={{
                            border: formErrors?.postTitle
                              ? "1px solid red"
                              : "1px solid #00000026",
                          }}
                          value={formValues.postTitle}
                        />
                        {formErrors.postTitle && (
                          <div className="invalid-tooltip">
                            {formErrors.postTitle}
                          </div>
                        )}
                      </div>

                      <div className="mb-3">
                        <label className="form-label">Publish Date</label>
                        <input
                          className="form-control"
                          type="datetime-local"
                          id="text-input"
                          name="publishDate"
                          onChange={handleChange}
                          autoComplete="on"
                          value={
                            defaultValue ? defaultValue : formValues.publishDate
                          }
                        />
                      </div>

                      <div className="mb-3">
                        <label className="col-form-label">
                          Link URL&nbsp;{" "}
                          <small className="option">Optional</small>
                        </label>
                        <br />
                        <input
                          className="form-control"
                          type="text"
                          id="text-input"
                          name="linkURL"
                          onChange={handleChange}
                          autoComplete="on"
                          value={formValues.linkURL}
                        />
                      </div>

                      {/* <div className="mb-3 mt-5">
                    <label className="form-label">Attach a Photo or PDF File&nbsp; <span className="option">Optional</span></label><br />
                    Select File:
                    <input className="file-uploader file-uploader-grid"
                        type="file" multiple data-max-files="4" data-max-file-size="2MB"
                        accept="image/png, image/jpeg, video/mp4, video/mov"
                        data-label-idle='<div className="btn btn-primary mb-3">
                    <i className="fi-cloud-upload me-1"></i>Upload photos / video</div><div>or drag them in
                    </div>'
                        name="file"
                        onChange={handleFileUpload}
                        autoComplete="on"
                        value={formValues.file}
                        style={{
                            border: formErrors?.file ? '1px solid red' : '1px solid #00000026'
                        }} />
                </div> */}

                      <div className="mb-3 mt-5">
                        <label className="form-label">
                          Attach a Photo or PDF File&nbsp;{" "}
                          <small className="option">Optional</small>
                        </label>
                        <input
                          className="file-uploader file-uploader-grid"
                          id="inline-form-input"
                          type="file"
                          name="image"
                          onChange={handleFileUpload}
                        />
                      </div>

                      <div className="mb-3 mt-5">
                        <label className="form-label float-left w-100 mb-0">
                          <p className="pull-left mr-2 mb-0">Post Blurb *</p>
                          <span>
                            <p className="pull-right">190chr</p>
                          </span>
                        </label>
                        <textarea
                          className="form-control mt-0"
                          id="textarea-input"
                          rows="5"
                          name="postBlurb"
                          onChange={handleChange}
                          autoComplete="on"
                          value={formValues.postBlurb}
                          style={{
                            border: formErrors?.postBlurb
                              ? "1px solid red"
                              : "1px solid #00000026",
                          }}
                        >
                          Hello World!
                        </textarea>
                        {formErrors.postBlurb && (
                          <div className="invalid-tooltip">
                            {formErrors.postBlurb}
                          </div>
                        )}
                      </div>

                      {editData ? (
                        <div className="mb-3">
                          <label className="col-form-label">Full Message</label>
                          <Editor
                            editorState={editorState}
                            onEditorStateChange={handleChanges}
                            value={formValues.fullMessage}
                            toolbar={{
                              options: [
                                "inline",
                                "blockType",
                                "fontSize",
                                "list",
                                "textAlign",
                                "history",
                                "link", // Add link option here
                              ],
                              inline: {
                                options: [
                                  "bold",
                                  "italic",
                                  "underline",
                                  "strikethrough",
                                ],
                              },
                              list: { options: ["unordered", "ordered"] },
                              textAlign: {
                                options: ["left", "center", "right"],
                              },
                              history: { options: ["undo", "redo"] },
                              link: {
                                // Configure link options
                                options: ["link", "unlink"],
                              },
                            }}
                            wrapperClassName="demo-wrapper"
                            editorClassName="demo-editor"
                          />
                          ;
                        </div>
                      ) : (
                        <label className="form-check-label d-flex">
                          Blurb too short, do you have more to say?
                          <NavLink>
                            <p onClick={(e) => handleEdit(e)}>
                              &nbsp;Add Full Message
                            </p>
                          </NavLink>
                        </label>
                      )}
                      <br />

                      {/* <!-- Inline radios --> */}
                      <div className="mb-2">
                        <label className="form-check-label">
                          Publication Level
                        </label>
                        <p className="form-check-label">
                          Where should your post be shown?
                        </p>

                        <div
                          className="form-check d-flex"
                          onClick={(e) => CheckList(e)}
                        >
                          <div className="">
                            <input
                              className="form-check-input "
                              id="form-radio-four"
                              type="radio"
                              name="publicationLevel"
                              value="office"
                            />
                            <label className="form-check-label">
                              My Office Only
                            </label>
                          </div>
                          <div className="ms-4">
                            <input
                              className="form-check-input"
                              id="form-radio-4"
                              type="radio"
                              name="publicationLevel"
                              value="regional"
                            />
                            <label className="form-check-label ps-2">
                              Regional Offices
                            </label>
                          </div>

                          <div className="ms-4">
                            <input
                              className="form-check-input"
                              id="form-radio-4"
                              type="radio"
                              name="publicationLevel"
                              value="choose"
                            />
                            <label className="form-check-label ps-2">
                              Let Me Choose
                            </label>
                          </div>
                        </div>
                      </div>

                      {checkPublication === "choose" ? (
                        <>
                          <div className="mb-3">
                            <label className="col-form-label">
                              Specific Office:{" "}
                            </label>
                            <select
                              className="form-select mt-2"
                              id="pr-city"
                              name="specific"
                              onChange={handleChange}
                              value={formValues.specific}
                            >
                              <option></option>
                              <option>Corporate</option>
                            </select>
                          </div>
                        </>
                      ) : (
                        ""
                      )}

                      <label className="col-form-label">Article Comments</label>
                      <div className="mb-2">
                        <div className="form-check">
                          <input
                            className="form-check-input"
                            type="checkbox"
                            id="invalidCheck"
                            name="articleComments"
                            onChange={handleChange}
                            value="Yes"
                            required
                          />
                          <label className="form-check-label">
                            Yes, Allow everyone in the office to comment on this
                            post.
                          </label>
                        </div>
                      </div>
                      <br />

                      <p className="form-check-label d-flex">
                        <h6 className="mb-0">Pin Article</h6>&nbsp;&nbsp;
                        <span>STAFF ONLY OPTION</span>
                      </p>
                      <div className="mb-3 py-2">
                        <div className="form-check">
                          <input
                            className="form-check-input"
                            type="checkbox"
                            id="invalidCheck"
                            name="pin_article"
                            onClick={(e) => handlePinArticle(e)}
                            value="Yes"
                            required
                          />
                          <label className="form-check-label">
                            Yes, Pin this article above standard posts.
                          </label>
                        </div>
                      </div>

                      {pinArticle === "Yes" ? (
                        <>
                          <div className="mb-3">
                            <label className="col-form-label">
                              Pin Removal Date:
                            </label>
                            <input
                              className="form-control"
                              type="date"
                              id="text-input"
                              name="pin_date"
                              onChange={handleChange}
                              autoComplete="on"
                              value={formValues.pin_date}
                              style={{
                                border: formErrors?.pin_date
                                  ? "1px solid red"
                                  : "1px solid #00000026",
                              }}
                            />
                            {formErrors.pin_date && (
                              <div className="invalid-tooltip">
                                {formErrors.pin_date}
                              </div>
                            )}
                          </div>
                        </>
                      ) : (
                        ""
                      )}
                    </form>
                  </div>
                  <div className="pull-right mt-4">
                    <button
                      type="button"
                      className="btn btn-primary btn-sm"
                      id="save-button"
                      onClick={PostAll}
                    >
                      Publish Post
                    </button>
                    <button
                      className="btn btn-secondary btn-sm ms-3"
                      onClick={handleCloseData}
                    >
                      Cancel & Go Back
                    </button>
                  </div>
                </div>

                <div className="col-md-4">
                  <div id="accordionCards" className="">
                    <div className="card bg-secondary mb-2 mt-0">
                      <div className="card-body">
                        <h3 className="h3 card-title pt-1 mb-0">
                          ✅ Post Status
                        </h3>
                      </div>
                      <div
                        className="collapse show"
                        id="cardCollapse1"
                        data-bs-parent="#accordionCards"
                      >
                        <div className="card-body mt-n1 pt-0">
                          <ul className="list-unstyled mb-2">
                            <li className="d-flex align-items-center mb-2">
                              {category ? (
                                <>
                                  <i className="fi-check text-primary me-2"></i>
                                  <a
                                    className="nav-link fw-normal ps-1 p-0"
                                    href="#basic-info"
                                    data-scroll=""
                                    data-scroll-offset="20"
                                  >
                                    Category
                                  </a>
                                </>
                              ) : (
                                <>
                                  <img src="https://cdn.pboffice.net/shared/transaction/page_select_drop.png" />
                                  <a
                                    className="nav-link fw-normal ps-1 p-0"
                                    href="#basic-info"
                                    data-scroll=""
                                    data-scroll-offset="20"
                                  >
                                    Category
                                  </a>
                                </>
                              )}
                            </li>

                            <li className="d-flex align-items-center mb-2">
                              {channel ? (
                                <>
                                  <i className="fi-check text-primary me-2"></i>
                                  <a
                                    className="nav-link fw-normal ps-1 p-0"
                                    href="#basic-info"
                                    data-scroll=""
                                    data-scroll-offset="20"
                                  >
                                    Channel
                                  </a>
                                </>
                              ) : (
                                <>
                                  <img src="https://cdn.pboffice.net/shared/transaction/page_select_drop.png" />
                                  <a
                                    className="nav-link fw-normal ps-1 p-0"
                                    href="#basic-info"
                                    data-scroll=""
                                    data-scroll-offset="20"
                                  >
                                    Channel
                                  </a>
                                </>
                              )}
                            </li>
                          </ul>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            ) : (
              ""
            )}
          </>
        </div>
      </main>
    </div>
  );
}
export default NewPost;

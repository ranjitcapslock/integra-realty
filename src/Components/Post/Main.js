import React, { useState, useEffect } from "react";
import Pic from "../img/pic.png";

import { NavLink, useLocation } from "react-router-dom";
import { useLocalStorage } from "../../LocalStorageContext"; // Import the context

import Birthday from "../img/birthday.jpg";
import Picture from "../img/pic.png";

// import "../../style.css";
import { useNavigate, useParams } from "react-router-dom";
import user_service from "../service/user_service.js";
import Loader from "../../Pages/Loader/Loader.js";
import Toaster from "../../Pages/Toaster/Toaster.js";
import ReactPaginate from "react-paginate";
import { Calendar as BigCalendar, momentLocalizer } from "react-big-calendar";
import moment from "moment-timezone";
import "react-big-calendar/lib/css/react-big-calendar.css";

import Slider from "react-slick";
import "slick-carousel/slick/slick-theme.css";
import "slick-carousel/slick/slick.css";
import jwt from "jwt-decode";

const localizer = momentLocalizer(moment);
// console.log(localizer);
const eventColors = {
  meeting: "#FF5733",
  duty: "#3366FF",
};

const settings = {
  dots: true,
  infinite: true,
  speed: 500,
  slidesToShow: 1,
  slidesToScroll: 1,
  autoplay: true,
  autoplaySpeed: 7000,
  arrows: false,
};
function Main(props) {
  const { active_office_id } = useLocalStorage();

  // const [peoples, setPeoples] = useState(data);
  // const [active_office, setActive_office] = useState(localStorage.getItem('active_office'));
  // const [active_office_id, setActive_office_id] = useState(localStorage.getItem('active_office_id'));

  const [peoples, setPeoples] = useState([]);

  const [formData, setFormData] = useState("");
  const [activeButton, setActiveButton] = useState("blend");

  const params = useParams();

  const profilepic = (e) => {
    e.target.src = Picture;
  };

  const bannerGetAll = async () => {
    await user_service.bannerGetAll().then((response) => {
      if (response) {
        console.log(response);
        setPeoples(response.data.data);
      }
    });
  };

  const PlatformdetailsGet = async () => {
    await user_service.platformdetailsGet().then((response) => {
      if (response) {
        setFormData(response.data.data[0]);
      }
    });
  };

  useEffect(() => {
    window.scrollTo(0, 0);
    PlatformdetailsGet();
  }, []);

  const [isShowSidebar, setIsShowSidebar] = useState(false);
  const { pathname } = useLocation();
  const [value, onChange] = useState(new Date());
  const navigate = useNavigate();

  const [loader, setLoader] = useState({ isActive: null });
  const { isActive } = loader;
  const [toaster, setToaster] = useState({
    types: null,
    isShow: null,
    toasterBody: "",
    message: "",
  });
  const { types, isShow, toasterBody, message } = toaster;

  const [getPost, setGetPost] = useState([]);
  const [getIndustryPost, setGetIndustryPost] = useState([]);
  const [IndustrypageCount, setIndustryPageCount] = useState(0);
  const [IndustrytotalRecords, setIndustryTotalRecords] = useState("");

  const [pageCount, setPageCount] = useState(0);

  const [totalRecords, setTotalRecords] = useState("");
  const [activeNavlink, setActiveNavlink] = useState("blend");
  const [events, setEvents] = useState([]);

  const [calenderGet, setCalenderGet] = useState([]);

  const getTimeDifference = (createdDate) => {
    const currentDate = new Date();
    const diffInMilliseconds = currentDate - new Date(createdDate);
    const diffInMinutes = Math.floor(diffInMilliseconds / 60000);
    const hours = Math.floor(diffInMinutes / 60);
    const days = Math.floor(hours / 24);
    const months = Math.floor(days / 30);
    const years = Math.floor(months / 12);

    if (years > 0) {
      return `${years} year${years > 1 ? "s" : ""}`;
    } else if (months > 0) {
      return `${months} month${months > 1 ? "s" : ""}`;
    } else if (days > 0) {
      return `${days} day${days > 1 ? "s" : ""}`;
    } else if (hours > 24) {
      const remainingHours = hours - Math.floor(hours / 24) * 24;
      return `1 day, ${remainingHours} hr${remainingHours > 1 ? "s" : ""}`;
    } else {
      return `${hours} hr${hours > 1 ? "s" : ""}, ${diffInMinutes % 60} min${
        diffInMinutes % 60 > 1 ? "s" : ""
      }`;
    }
  };

  const generateEvents = () => {
    const generatedEvents = [];

    if (calenderGet && calenderGet.data) {
      calenderGet.data.forEach((date) => {
        let startDate;
        let endDate;

        // Check if date.startDate is defined before accessing its properties
        if (date.startDate && date.startDate.includes("/")) {
          startDate = moment
            .tz(date.startDate, "M/D/YYYY", "America/Denver")
            .toDate();
          endDate = moment
            .tz(date.endDate, "M/D/YYYY", "America/Denver")
            .toDate();
        } else if (date.startDate) {
          startDate = moment.tz(date.startDate, "America/Denver").toDate();
          endDate = moment.tz(date.endDate, "America/Denver").toDate();
        } else {
          // Handle the case where date.startDate is undefined
          console.warn("startDate is undefined for:", date);
          return; // Skip this iteration
        }

        const eventId = date._id;

        generatedEvents.push({
          title: date.title,
          start: startDate,
          end: endDate,
          type: "meeting",
          id: eventId,
        });
      });
    }
    return generatedEvents;
  };

  const [upcomingbirthday, setUpcomingbirthday] = useState("");
  const homeside = async () => {
    await user_service.homeside().then((response) => {
      if (response) {
        // console.log(response.data)
        setUpcomingbirthday(response.data.data);
      }
    });
  };

  const [newassociates, setNewassociates] = useState("");

  const homesidenewassociates = async () => {
    await user_service.newassociates().then((response) => {
      if (response) {
        // console.log(response.data);
        setNewassociates(response.data.data);
      }
    });
  };

  const fetchCalendar = async () => {
    setLoader({ isActive: true });
    await user_service.CalenderGet().then((response) => {
      setLoader({ isActive: false });
      if (response) {
        setCalenderGet(response.data);
      }
    });
  };

  const [question, setQuestion] = useState("");
  const QuestionGet = async () => {
    var pin = "?pin=yes";
    await user_service.questionsGet(pin).then((response) => {
      if (response) {
        // console.log(response);
        setQuestion(response.data);
      }
    });
  };
  const [link, setLink] = useState("");
  const LinkGet = async () => {
    const userid = jwt(localStorage.getItem("auth")).id;
    var pin = `?agentId=${userid}&pin=yes`;
    //var pin = `?pin=yes`;
    await user_service.linksGet(pin).then((response) => {
      if (response) {
        // console.log(response);
        setLink(response.data);
      }
    });
  };

  const [statsReport, setStatsReport] = useState("");
  const statsReportGet = async () => {
    // const userid = jwt(localStorage.getItem("auth")).id;
    // var query = `?agentId=${userid}`;
    let queryParams = `?page=1`;

    // queryParams += `&filing=filed_complted`;
    if (
      localStorage.getItem("auth") &&
      jwt(localStorage.getItem("auth")).contactType == "admin"
    ) {
      // queryParams += `&agentId=${jwt(localStorage.getItem("auth")).id}`;
    } else {
      queryParams += `&agentId=${jwt(localStorage.getItem("auth")).id}`;
    }
    await user_service.TransactionstatsReport(queryParams).then((response) => {
      if (response) {
        // console.log(response.data);
        setStatsReport(response.data);
      }
    });
  };

  useEffect(() => {
    window.scrollTo(0, 0);
    const generatedEvents = generateEvents();
    // console.log(generatedEvents);
    // console.log("generatedEvents");
    setEvents(generatedEvents);
  }, [calenderGet, active_office_id]);

  const eventStyleGetter = (event) => {
    const backgroundColor = eventColors[event.type];
    return {
      style: {
        backgroundColor,
      },
    };
  };

  const [currentDate, setCurrentDate] = useState("");
  useEffect(() => {
    window.scrollTo(0, 0);
    const interval = setInterval(() => {
      const options = { dateStyle: "full" };
      setCurrentDate(new Date().toLocaleDateString("en-US", options));
    }, 1000);

    return () => {
      clearInterval(interval);
    };
  }, []);

  useEffect(() => {
    window.scrollTo(0, 0);
    if (pathname === "/signin" || pathname === "/signup") {
      setIsShowSidebar(false);
    } else {
      setIsShowSidebar(true);
    }
  }, [pathname]);

  const AddPost = () => {
    navigate("/new-post");
  };

  const PostDetail = (id) => {
    // console.log(id)
    navigate(`/post-detail/${id}`);
  };

  const PostDetailIndustry = (url) => {
    window.open(url, "_blank");
  };

  const CalenderPage = () => {
    navigate("/calendar");
  };

  const increaseclick = async (banner_id) => {
    await user_service.increaseclickbanner(banner_id).then((response) => {
      if (response) {
      }
    });
  };

  const [blogcategory, setBlogcategory] = useState("nar");

  const IndustryPostGetAll = async (blogcategory = "") => {
    setLoader({ isActive: true });
    setBlogcategory(blogcategory);
    let pagee = "1";
    if (blogcategory != "") {
      pagee = "1" + "&blogcategory=" + blogcategory;
    } else {
      pagee = "1" + "&blogcategory=nar";
    }
    await user_service.IndustrypostGet(pagee).then((response) => {
      setLoader({ isActive: false });
      if (response) {
        setGetIndustryPost(response.data.data);
        setIndustryPageCount(Math.ceil(response.data.totalRecords / 5));
        setIndustryTotalRecords(response.data.totalRecords);
      }
    });
  };

  const handlePageClick = async (data) => {
    let currentPage = data.selected + 1;
    let skip = currentPage - 1 + currentPage;
    setLoader({ isActive: true });
    await user_service.postGet(currentPage, skip).then((response) => {
      setLoader({ isActive: false });
      if (response) {
        setGetPost(response.data.data);
        setPageCount(Math.ceil(response.data.totalRecords / 5));
      }
    });
  };

  const handleIndustryPageClick = async (data) => {
    let currentPage = data.selected + 1;
    let skip = currentPage - 1 + currentPage;
    setLoader({ isActive: true });
    let blogcategoryy = blogcategory;
    let pagee = currentPage + "&blogcategory=nar";
    if (blogcategoryy != "") {
      pagee = currentPage + "&blogcategory=" + blogcategoryy;
    } else {
      pagee = currentPage + "&blogcategory=nar";
    }
    await user_service.IndustrypostGet(pagee, skip).then((response) => {
      setLoader({ isActive: false });
      if (response) {
        setGetIndustryPost(response.data.data);
        setIndustryPageCount(Math.ceil(response.data.totalRecords / 5));
      }
    });
  };

  const CatagoryBlend = async (filter) => {
    setActiveNavlink(filter);
    if (filter === "blend") {
      setActiveButton(filter);

      setLoader({ isActive: true });
      await user_service.postFilter().then((response) => {
        setLoader({ isActive: false });
        if (response) {
          setGetPost(response.data.data);
          setPageCount(Math.ceil(response.data.totalRecords / 5));
          setTotalRecords(response.data.totalRecords);
        }
      });
    } else {
      setLoader({ isActive: true });
      await user_service.postFilter(filter).then((response) => {
        setLoader({ isActive: false });
        if (response) {
          setGetPost(response.data.data);
          setPageCount(Math.ceil(response.data.totalRecords / 5));
          setTotalRecords(response.data.totalRecords);
        }
      });
    }
  };

  const propertypic = (e) => {
    e.target.src =
      "https://integra.workspace.lwolf.com/common/handler/mls/photo_custom.ashx?s=1&t=5eada3e3-491e-46e2-938c-7c785a1f9c68&r=Seller";
  };

  const handleHome = () => {
    navigate("/");
  };
  const handleMyTransaction = () => {
    navigate("/transaction");
  };

  const handleMarket = () => {
    // navigate(`/contactlisting/listing/${jwt(localStorage.getItem("auth")).id}`);
    navigate(`/mylisting`);
  };

  const handleContact = () => {
    navigate("/contact");
  };
  const handleListing = () => {
    navigate("/listing");
  };

  const addRules = () => {
    navigate("/add-document-rules");
  };
  const addDetails = () => {
    navigate("/t-details");
  };
  const addLearn = () => {
    navigate("/add-learn");
  };

  const logoImage = () => {
    navigate("/");
  };

  const handletasks = () => {
    navigate("/task");
  };

  const handlestore = () => {
    navigate("/promote");
  };

  const handlesVendor = () => {
    navigate("/vender");
  };
  const handleDocs = () => {
    navigate("/control-panel/documents");
  };

  const handTraining = () => {
    navigate("/learn");
  };

  const [messages, setMessage] = useState("");
  const [agentData, setAgentData] = useState([]);
  const [isVisible, setIsVisible] = useState(false);

  const handleButtonClick = (msg) => {
    setMessage(msg);
  };

  const handleAgentData = (agentData) => {
    setAgentData(Array.isArray(agentData) ? agentData : [agentData]);
    setIsVisible(true);
    setMessage("");
  };

  const handleSubmitBirthday = async (e) => {
    const contactData = agentData.map((item) => ({
      email: item.email,
      firstName: item.firstName,
      is_view: "1",
      _id: item._id,
    }));
    e.preventDefault();
    const userData = {
      agentId: jwt(localStorage.getItem("auth")).id,
      office_id: localStorage.getItem("active_office_id"),
      office_name: localStorage.getItem("active_office"),
      category: "Birthday Wishes",
      message: messages,
      contactType: contactData,
      isview: `${profile.firstName} ${profile.lastName}`,
    };
    await user_service.noticeBirthday(userData).then((response) => {
      if (response) {
        setLoader({ isActive: false });
        setToaster({
          type: "Birthday",
          isShow: true,
          message: "Birthday send Successfully",
        });
        noticeGetAllData();
        setIsVisible(false);
        setTimeout(() => {
          setToaster((prevToaster) => ({ ...prevToaster, isShow: false }));
          // navigate("/send/notice");
        }, 2000);
      } else {
        setLoader({ isActive: false });
        setToaster({
          types: "error",
          isShow: true,
          toasterBody: response.data.message,
          message: "Error",
        });
        setTimeout(() => {
          setToaster((prevToaster) => ({ ...prevToaster, isShow: false }));
          // navigate("/control-panel/documents/");
        }, 2000);
      }
    });
  };

  const upcomingBirthdayData =
    upcomingbirthday && upcomingbirthday.length > 0 ? (
      upcomingbirthday.map((agent, index) => {
        if (index < 4) {
          return (
            <>
              {agent.contact_status === "active" && (
                <div className="float-start w-100" key={agent._id}>
                  <a
                    href="#"
                    data-toggle="modal"
                    data-target="#submitInprogress"
                    onClick={() => handleAgentData(agent)}
                  >
                    <div className="float-start w-100 d-flex align-items-center justify-conten-start my-2">
                      <img
                        className="img-fluid"
                        src={agent.image || Pic}
                        alt="Agent Avatar"
                      />
                      <span className="mb-0">
                        {agent.filterType === "birthday" && "Happy Birthday"}
                        <p className="mb-0">
                          <b>
                            {agent.firstName} {agent.lastName}
                          </b>{" "}
                          &nbsp;
                          {agent.filterType === "associatesince" &&
                            ` with ${agent.company ?? ""}`}
                        </p>
                        <p className="mb-1">{agent.title ?? ""}</p>
                        <p className="mb-0 mt-1">
                          {agent.filterType === "birthday" && (
                            <>
                              {agent.birthday
                                ? moment(agent.birthday).format("MMMM D")
                                : ""}
                            </>
                          )}
                          {agent.filterType === "associatesince" && (
                            <>
                              {agent?.additionalActivateFields?.associate_since
                                ? moment(
                                    agent?.additionalActivateFields
                                      ?.associate_since
                                  ).format("MMMM D")
                                : ""}
                            </>
                          )}
                          {/* Helper function to format date */}
                        </p>
                      </span>
                    </div>
                  </a>
                </div>
              )}
              <div className="float-start w-100">
                <hr className=""></hr>
              </div>
            </>
          );
        } else {
          return null;
        }
      })
    ) : (
      <p className="text-center mb-3">
        <img className="" src={Birthday} alt="Birthday" />
        <br />
        <p className="mt-2">No upcoming birthday.</p>
      </p>
    );

  const loadMoreLink =
    upcomingbirthday && upcomingbirthday.length > 4 ? (
      <div className="float-start w-100 text-center mt-3">
        <NavLink className="" to="/upcomingbirthday">
          Load More
        </NavLink>
      </div>
    ) : null;

  // Helper function to format date as "Day, Month Day" (e.g., "Friday, September 21")
  function getFormattedDate(dateString) {
    const options = { month: "long", day: "numeric" };
    const date = new Date(dateString);
    return date.toLocaleDateString("en-US", options);
  }

  const newassociatesData =
    newassociates && newassociates.length > 0 ? (
      newassociates.map((agent, index) => {
        if (index < 5) {
          return (
            <>
              {agent.contact_status === "active" && (
                <div className="float-start w-100" key={agent._id}>
                  <NavLink to={`/contact-profile/${agent._id}`}>
                    <div className="float-start w-100">
                      <hr className=""></hr>
                    </div>
                    <div className="float-start w-100 d-flex align-items-center justify-conten-start my-2">
                      <img
                        className="img-fluid"
                        src={agent.image || Pic}
                        alt="Agent Avatar"
                      />
                      <span className="mb-0">
                        <p className="mb-0">
                          <b>
                            {agent.firstName} {agent.lastName}
                          </b>
                        </p>
                        {/* <p className="mb-1">{agent.title}</p> */}
                        {agent.timeStamp ? (
                          <p className="mb-0 mt-1">
                            {moment(agent.timeStamp).format("MMMM D")}
                          </p>
                        ) : (
                          ""
                        )}

                        {agent?.additionalActivateFields &&
                        agent?.additionalActivateFields?.admin_note &&
                        agent?.additionalActivateFields?.admin_note != "" ? (
                          <>
                            <p className="mb-0 mt-1">
                              Moved From&nbsp;
                              {agent?.additionalActivateFields
                                ? agent?.additionalActivateFields?.admin_note
                                : "" || ""}
                            </p>
                          </>
                        ) : (
                          ""
                        )}
                      </span>
                    </div>
                  </NavLink>
                </div>
              )}
            </>
          );
        } else {
          return null; // Skip rendering when index is 4 or greater
        }
      })
    ) : (
      <p className="text-center mb-3">
        <img className="" src={Pic} />
        <br />
        <span className="mt-2">Not any new associate.</span>
      </p>
    );

  const newassociatesloadMoreLink =
    newassociates && newassociates.length > 4 ? (
      <div className="float-left w-100 text-center mt-3">
        <NavLink className="" to="/newassociates">
          Load More
        </NavLink>
      </div>
    ) : null;

  const initialValues = {
    allDayEvent: "yes",
    category: "",
    draft: "",
    endDate: "",
    guestSeats: "",
    information: "",
    publicEvent: "",
    registration: "",
    startDate: "",
    title: "",
  };

  const [selectedDate, setSelectedDate] = useState(null);
  const [formValues, setFormValues] = useState(initialValues);
  const [formErrors, setFormErrors] = useState({});

  const handleSelectSlot = (slotInfo) => {
    const today = moment().startOf("day");
    const selectedDate = moment(slotInfo.start).startOf("day");
    if (
      localStorage.getItem("auth") &&
      jwt(localStorage.getItem("auth")).contactType === "admin"
    ) {
      if (selectedDate.isSameOrAfter(today)) {
        setSelectedDate(slotInfo);
        // setIsModalVisible(true);
        window.$("#modal-show-calender").modal("show");
      }
    }
  };

  const handleEventClick = (event) => {
    event.preventDefault();
    const eventId = event.currentTarget.getAttribute("data-eventid");
    // navigate(`/event-detail/${eventId}`);
    navigate(`/calendar`);
  };
  {
    /* <!-- Input onChange Start--> */
  }
  const handleChange = (e) => {
    const { name, value } = e.target;
    setFormValues({ ...formValues, [name]: value });
  };

  {
    /* <!-- Form onSubmit Start--> */
  }
  const handleSubmit = async (e, slotInfo) => {
    e.preventDefault();

    if (slotInfo) {
      const startDate = moment(slotInfo.start).format("YYYY-MM-DD");
      const endDate = moment(slotInfo.start).format("YYYY-MM-DD");
      const userData = {
        office_id: localStorage.getItem("active_office_id"),
        office_name: localStorage.getItem("active_office"),
        category: formValues.category,
        allDayEvent: "yes",
        title: formValues.title,
        information: "information",
        registration: "registration",
        draft: "draft",
        agentId: jwt(localStorage.getItem("auth")).id,
        startDate: startDate,
        endDate: endDate,
        guestSeats: "guestSeats",
        publicEvent: "publicEvent",
      };

      try {
        setLoader({ isActive: true });
        const response = await user_service.CalenderPost(userData);
        if (response && response.data) {
          setLoader({ isActive: false });
          setToaster({
            types: "Calender",
            isShow: true,
            toasterBody: response.data.message,
            message: "Event Post Successfully",
          });
          setTimeout(() => {
            setToaster((prevToaster) => ({ ...prevToaster, isShow: false }));
          }, 1000);

          fetchCalendar();
          document.getElementById("close").click();
        } else {
          setLoader({ isActive: false });
          setToaster({
            types: "error",
            isShow: true,
            toasterBody: response.data.message,
            message: "Error",
          });
        }
      } catch (error) {
        setLoader({ isActive: false });
        setToaster({
          types: "error",
          isShow: true,
          toasterBody: error.message,
          message: "Error",
        });
      }
      setTimeout(() => {
        setToaster({
          types: "error",
          isShow: false,
          toasterBody: null,
          message: "Error",
        });
      }, 1000);
    }
  };

  {
    /* <!-- Form onSubmit End--> */
  }

  const [search_home, setSearch_home] = useState("");
  const [universaldata, setUniversaldata] = useState([]);
  const search_universal = async () => {
    //console.log(search_home);
    // setIsLoading(true);

    await user_service.Searchuiversalfilter(search_home).then((response) => {
      //setIsLoading(false);
      if (response) {
        //  console.log(response.data);
        setUniversaldata(response.data);
      }
    });
  };

  const CurrencyFormatter = ({ amount }) => {
    // Format the amount as currency
    const formattedAmount = new Intl.NumberFormat("en-US", {
      style: "currency",
      currency: "USD",
    }).format(amount / 100); // Assuming amount is in cents, convert it to dollars

    return <span>{formattedAmount}</span>;
  };

  const [formValuesNew, setFormValuesNew] = useState([]);
  const [formValuesVolume, setFormValuesVolume] = useState([]);

  const fetchTransactionData = async () => {
    try {
      setLoader({ isActive: true });
      const response = await user_service.Getfilledtransactionsmonthwise();
      if (response && response.data && response.data.monthlyResultearning) {
        setFormValuesNew(response.data.monthlyResultearning);
        setLoader({ isActive: false });

        // const newResponce = response.data
        // setFormValuesNew(newResponce);

        const newDataRespoce = response.data.monthlyTransactionCount;
        setFormValuesVolume(newDataRespoce);
      } else {
        console.error("Invalid response data structure:", response);
        setLoader({ isActive: false });
      }
    } catch (error) {
      console.error("Error fetching filled transactions monthwise:", error);
      setLoader({ isActive: false });
    }
  };

  const totalCountCommission = formValuesNew.reduce(
    (accumulator, item) =>
      accumulator +
      item.totalBuyerFee +
      item.totalHighValueFee +
      item.totalManagementFee +
      item.totalTransactionFee +
      item.totalWireFee +
      item.totalPersonalFee +
      item.totalSellerFee,
    0
  );

  const totalCountVolume = formValuesVolume.reduce(
    (accumulator, item) => accumulator + item.purchase_price,
    0
  );

  const [imageData, setImageData] = useState({});

  // const HomeImagePhotoGet = async () => {
  //   await user_service.homePagePhotoGet().then((response) => {
  //     if (response) {
  //       const groupedByImage = response.data.data.reduce((acc, item) => {
  //         const imageType = item.imageType; // Fixed variable name to be consistent
  //         if (!acc[imageType]) {
  //           acc[imageType] = [];
  //         }
  //         acc[imageType].push(item);
  //         return acc;
  //       }, {});
  //       setImageData(groupedByImage);
  //     }
  //   });
  // };

  const HomeImagePhotoGet = async () => {
    await user_service.homePagePhotoGet().then((response) => {
      if (response) {
        const groupedByImage = response.data.data.reduce((acc, item) => {
          const { imageType, is_active } = item; // Destructure to get is_active and imageType
          if (is_active === "1") {
            // Check if is_active is "1"
            if (!acc[imageType]) {
              acc[imageType] = [];
            }
            acc[imageType].push(item);
          }
          return acc;
        }, {});
        setImageData(groupedByImage);
      }
    });
  };

  const handlePosts = () => {
    navigate(`/new-post`);
  };

  const [reservationData, setReservationData] = useState([]);
  const [showModal, setShowModal] = useState(false);

  const noticeGetAllData = async () => {
    try {
      const response = await user_service.noticecontactGet();
      setLoader({ isActive: false });
      if (response && response.data && response.data.data) {
        const data = response.data.data;
        setReservationData(data);

        const hasNotices = data.some(
          (item) =>
            item?.contactType?.some((contact) => contact._id === userId) ||
            item?.contactGroup?.some((group) =>
              group?.group_member?.some((member) => member._id === userId)
            ) ||
            item?.officeGroup?.some((office) =>
              office?.associates?.some((associate) => associate._id === userId)
            )
        );

        setShowModal(hasNotices);
        noticeNotification(data);
      }
    } catch (error) {
      console.error("Error fetching reservation data:", error);
      setLoader({ isActive: false });
    }
  };

  const userId = jwt(localStorage.getItem("auth")).id;

  const noticeNotification = async (noticeData) => {
    const hasNotices = noticeData.some(
      (item) =>
        item?.contactType?.some((contact) => contact._id === userId) ||
        item?.contactGroup?.some((group) =>
          group?.group_member?.some((member) => member._id === userId)
        ) ||
        item?.officeGroup?.some((office) =>
          office?.associates?.some((associate) => associate._id === userId)
        )
    );

    setShowModal(hasNotices);

    // if (hasNotices) {
    //   // Trigger the modal
    //   const modalElement = document.getElementById("notice");
    //   if (modalElement) {
    //     modalElement.setAttribute("data-toggle", "modal");
    //     modalElement.setAttribute("data-target", "#notice");
    //     modalElement.click();
    //   }
    // }
  };

  const [totalMatches, setTotalMatches] = useState(0);

  useEffect(() => {
    let total = 0;

    reservationData.forEach((item) => {
      const contactTypeMatches = item.contactType
        ? item.contactType.filter((contact) => contact._id === userId).length
        : 0;

      const contactGroupMatches = item.contactGroup
        ? item.contactGroup.reduce((acc, group) => {
            const groupMemberMatches = group.group_member
              ? group.group_member.filter((member) => member._id === userId)
                  .length
              : 0;
            return acc + groupMemberMatches;
          }, 0)
        : 0;

      const officeGroupMatches = item.officeGroup
        ? item.officeGroup.reduce((acc, office) => {
            const officeMatches = office.associates
              ? office.associates.filter(
                  (associate) => associate._id === userId
                ).length
              : 0;
            return acc + officeMatches;
          }, 0)
        : 0;

      total += contactTypeMatches + contactGroupMatches + officeGroupMatches;
    });

    setTotalMatches(total);
  }, [reservationData, userId]);

  const formatDate = (dateString) => {
    const date = new Date(dateString);
    const options = {
      year: "numeric",
      month: "2-digit",
      day: "2-digit",
      // hour: '2-digit',
      // minute: '2-digit',
      // second: '2-digit',
    };
    return new Intl.DateTimeFormat("en-US", options).format(date);
  };

  useEffect(() => {
    window.scrollTo(0, 0);
    setLoader({ isActive: true });
    fetchCalendar();
    fetchTransactionData();
    noticeGetAllData();
    HomeImagePhotoGet();
    homeside();
    profileGetAll();
    homesidenewassociates();
    statsReportGet();
    contactLoginstatus();
    LinkGet();
    QuestionGet();
  }, []);

  const [profile, setProfile] = useState("");
  const profileGetAll = async () => {
    if (localStorage.getItem("auth")) {
      await user_service
        .profileGet(jwt(localStorage.getItem("auth")).id)
        .then((response) => {
          if (response) {
            setProfile(response.data);
          }
        });
    }
  };

  const [contactLoginStatus, setContactLoginStatus] = useState([]);
  const contactLoginstatus = async () => {
    await user_service.contactLoginstatus().then((response) => {
      if (response) {
        setContactLoginStatus(response.data.data);
      }
    });
  };

  const handleSignIn = () => {
    if(localStorage.getItem("auth") &&  jwt(localStorage.getItem("auth")).id){
      navigate(`/contact-profile/${profile._id}`);
    }
  };

  const handleSignContact = (id) => {
   navigate(`/contact-profile/${id}`);
  };


  


  useEffect(() => {
    CatagoryBlend("blend");
    IndustryPostGetAll();
  }, [active_office_id]);

  useEffect(() => {
    window.scrollTo(0, 0);
    bannerGetAll();
  }, [active_office_id]);

  const handleNotice = () => {
    navigate(`/notice/notification`);
  };

  const handleCLoseModal = () => {
    setShowModal(false);
  };

  const handleStatusChange = async (item, contactId, type) => {
    if (item && contactId) {
      let userData = {};
      const currentDate = new Date().toISOString();

      if (type === "contactType") {
        userData = {
          contactType: [
            {
              _id: contactId,
              is_view: "0",
            },
          ],
          assignDate: currentDate, // Assign current date and time
        };
      } else if (type === "contactGroup") {
        userData = {
          contactGroup: [
            {
              group_member: [
                {
                  _id: contactId,
                  is_view: "0",
                },
              ],
            },
          ],
          assignDate: currentDate,
        };
      } else if (type === "officeGroup") {
        userData = {
          officeGroup: [
            {
              associates: [
                {
                  _id: contactId,
                  is_view: "0",
                },
              ],
            },
          ],
        };
      }

      try {
        setLoader({ isActive: true });
        const response = await user_service.noticeUpdate(item._id, userData);

        if (response) {
          setLoader({ isActive: false });
          setToaster({
            types: "Notice Update",
            isShow: true,
            message: "Notice Updated Successfully",
          });
          setTimeout(() => {
            setToaster((prevToaster) => ({ ...prevToaster, isShow: false }));
          }, 3000);
          noticeGetAllData();
        }
      } catch (error) {
        setLoader({ isActive: false });
      }
    }
  };

  const timeAgo = (date) => {
    const now = new Date();
    const diff = Math.floor((now - date) / 1000); // Difference in seconds

    // if (diff < 60) {
    //   return `${diff} seconds ago`;
    // } else if (diff < 3600) {
    //   return `${Math.floor(diff / 60)} minutes ago`;
    // } else if (diff < 86400) {
    //   return `${Math.floor(diff / 3600)} hours ago`;
    // } else {
    //   return `${Math.floor(diff / 86400)} days ago`;
    // }


    if(diff < 86400){
      return `${Math.floor(diff / 3600)}`;
    }
  };
  return (
    <div className="bg-secondary float-left w-100 mb-4 pt-4">
      <Loader isActive={isActive} />
      {isShow && (
        <Toaster
          types={types}
          isShow={isShow}
          toasterBody={toasterBody}
          message={message}
        />
      )}
      <div className="">
        <main className="page-wrapper homepage_layout">
          {/* <!-- Page container--> */}
          {showModal ? (
            <div
              className="modal show"
              role="dialog"
              style={{ display: "block" }}
              id="notice"
            >
              <div
                className="modal-dialog modal-lg modal-dialog-scrollable"
                role="document"
              >
                <div className="modal-content">
                  <div className="modal-header">
                    <h4 className="modal-title">Notice</h4>
                    {/* <button
                      className="pull-right btn btn-primary"
                      onClick={handleCLoseModal}
                    >
                      X
                    </button> */}
                  </div>
                  <div className="modal-body fs-lg">
                    <div className="row bg-light p-3 border-0 rounded-3">
                      {reservationData.map((item, index) => {
                        const hasContactType =
                          item.contactType &&
                          item.contactType.some(
                            (contact) => contact._id === userId
                          );

                        const hasContactGroup =
                          item.contactGroup &&
                          item.contactGroup.some(
                            (group) =>
                              group.group_member &&
                              group.group_member.some(
                                (member) => member._id === userId
                              )
                          );

                        const hasOfficeGroup =
                          item.officeGroup &&
                          item.officeGroup.some(
                            (office) =>
                              office.associates &&
                              office.associates.some(
                                (associate) => associate._id === userId
                              )
                          );

                        if (
                          hasContactType ||
                          hasContactGroup ||
                          hasOfficeGroup
                        ) {
                          return (
                            <div key={index} className="col-md-12 mb-4">
                              {hasContactType && (
                                <ul>
                                  {item.contactType.map(
                                    (contact, i) =>
                                      contact._id === userId && (
                                        <li key={i}>
                                          <div className="card p-3">
                                            <div>
                                              {item.category && (
                                                <NavLink
                                                  style={{ fontSize: "24px" }}
                                                >
                                                  {item.category}
                                                </NavLink>
                                              )}
                                              <br />
                                              {item.message && (
                                                <span
                                                  dangerouslySetInnerHTML={{
                                                    __html: item.message,
                                                  }}
                                                ></span>
                                              )}
                                            </div>
                                            <div>
                                              <div className="pull-right d-flex align-items-center alert alert-danger p-2">
                                                <i
                                                  class="fa fa-exclamation-triangle me-2 text-danger"
                                                  aria-hidden="true"
                                                ></i>{" "}
                                                You are required to read and
                                                confirm that you read this
                                                notice.
                                                <button
                                                  className="btn btn-danger pull-right ms-4"
                                                  onClick={() =>
                                                    handleStatusChange(
                                                      item,
                                                      contact._id,
                                                      "contactType"
                                                    )
                                                  }
                                                >
                                                  Confirm
                                                </button>
                                              </div>
                                            </div>
                                          </div>
                                        </li>
                                      )
                                  )}
                                </ul>
                              )}

                              {hasContactGroup && (
                                <ul>
                                  {item.contactGroup.map(
                                    (group, i) =>
                                      group.group_member &&
                                      group.group_member.map(
                                        (member, j) =>
                                          member._id === userId && (
                                            <li key={j}>
                                              <div className="card p-3">
                                                <div>
                                                  {item.category && (
                                                    <NavLink
                                                      style={{
                                                        fontSize: "24px",
                                                      }}
                                                    >
                                                      {item.category}
                                                    </NavLink>
                                                  )}
                                                  {item.message && (
                                                    <span
                                                      dangerouslySetInnerHTML={{
                                                        __html: item.message,
                                                      }}
                                                    ></span>
                                                  )}
                                                </div>
                                                <div>
                                                  <div className="pull-right d-flex align-items-center alert alert-danger p-2">
                                                    <i
                                                      class="fa fa-exclamation-triangle me-2 text-danger"
                                                      aria-hidden="true"
                                                    ></i>{" "}
                                                    You are required to read and
                                                    confirm that you read this
                                                    notice.
                                                    <button
                                                      className="btn btn-danger pull-right ms-4"
                                                      onClick={() =>
                                                        handleStatusChange(
                                                          item,
                                                          member._id,
                                                          "contactGroup"
                                                        )
                                                      }
                                                    >
                                                      Confirm
                                                    </button>
                                                  </div>
                                                </div>
                                              </div>
                                            </li>
                                          )
                                      )
                                  )}
                                </ul>
                              )}

                              {hasOfficeGroup && (
                                <ul>
                                  {item.officeGroup.map(
                                    (office, i) =>
                                      office.associates &&
                                      office.associates.map(
                                        (associate, j) =>
                                          associate._id === userId && (
                                            <li key={j}>
                                              <div className="card p-3">
                                                <div>
                                                  {item.category && (
                                                    <NavLink
                                                      style={{
                                                        fontSize: "24px",
                                                      }}
                                                    >
                                                      {item.category}
                                                    </NavLink>
                                                  )}
                                                  {item.message && (
                                                    <span
                                                      dangerouslySetInnerHTML={{
                                                        __html: item.message,
                                                      }}
                                                    ></span>
                                                  )}
                                                </div>
                                                <div>
                                                  <div className="pull-right d-flex align-items-center alert alert-danger p-2">
                                                    <i
                                                      class="fa fa-exclamation-triangle me-2 text-danger"
                                                      aria-hidden="true"
                                                    ></i>{" "}
                                                    You are required to read and
                                                    confirm that you read this
                                                    notice.
                                                    <button
                                                      className="btn btn-danger pull-right ms-4"
                                                      onClick={() =>
                                                        handleStatusChange(
                                                          item,
                                                          associate._id,
                                                          "officeGroup"
                                                        )
                                                      }
                                                    >
                                                      Confirm
                                                    </button>
                                                  </div>
                                                </div>
                                              </div>
                                            </li>
                                          )
                                      )
                                  )}
                                </ul>
                              )}
                            </div>
                          );
                        }

                        return null;
                      })}
                    </div>
                  </div>
                </div>
              </div>
            </div>
          ) : (
            ""
          )}
          <div className="row event-calender">
            <div className="col-md-8">
              <div className="featured_images mb-3">
                <Slider {...settings}>
                  {peoples &&
                    peoples.length > 0 &&
                    peoples.map((post) =>
                      post.isactive === true &&
                      post.marque_image &&
                      post.banner_location === "home" ? (
                        <div key={post.title} className="carousel-item">
                          <a
                            onClick={() => increaseclick(post._id)}
                            href={post.url_link}
                            target="_blank"
                            className="sidenav-banner"
                          >
                            <span className="pull-left align-items-center justify-content-left d-flex w-100">
                              <img
                                alt=""
                                // className="w-100"
                                className="person-img w-100"
                                src={
                                  post.marque_image
                                    ? post.marque_image.includes("http")
                                      ? post.marque_image
                                      : post.marque_image
                                    : ""
                                }
                              ></img>
                            </span>
                          </a>
                        </div>
                      ) : null
                    )}
                </Slider>
              </div>
              <div className="search_bar mt-3">
                Search
                <input
                  type="text"
                  name="search_home"
                  className="form-control"
                  valvalue={search_home}
                  onChange={(event) => setSearch_home(event.target.value)}
                />
                <a href="#" onClick={search_universal}>
                  <i className="fa fa-search"></i>
                </a>
                {universaldata == "" || search_home == "" ? (
                  // <p>Loading...</p>
                  ""
                ) : (
                  <div className="activateaccountstaff_recruiter universal_filter_data shadow-sm">
                    {universaldata &&
                    universaldata.contact &&
                    universaldata.contact.length > 0 ? (
                      <div>
                        <h6 className="mb-4">Contact Data</h6>
                        {universaldata.contact.map((contact) => (
                          <div
                            className="activateaccountstaff"
                            key={contact._id}
                          >
                            <NavLink to={`contact-profile/${contact._id}`}>
                              <ul className="ps-0">
                                <li className="d-flex justify-content-start align-items-center">
                                  <div>
                                    <img
                                      className="rounded-circle profile_picture"
                                      src={Pic}
                                      alt="Profile"
                                    />
                                    <br />
                                  </div>
                                  <div>
                                    <strong>
                                      {contact.firstName} &nbsp;{" "}
                                      {contact.lastName}
                                    </strong>
                                    <br />
                                    <strong>{contact.company}</strong>
                                    <br />
                                  </div>
                                </li>
                              </ul>
                            </NavLink>
                          </div>
                        ))}
                      </div>
                    ) : null}
                    {universaldata &&
                    universaldata.calender &&
                    universaldata.calender.length > 0 ? (
                      <div>
                        <h6 className="mb-4">Calender</h6>
                        {universaldata.calender.map((calender) => (
                          <div
                            className="activateaccountstaff"
                            key={calender._id}
                          >
                            <NavLink to={`/event-detail/${calender._id}`}>
                              <ul className="ps-0">
                                <li className=" d-flex justify-content-start align-items-center">
                                  <div>
                                    <img
                                      className="rounded-circle profile_picture"
                                      src={Pic}
                                      alt="Profile"
                                    />
                                    <br />
                                  </div>
                                  <div>
                                    <strong>{calender.title}</strong>
                                  </div>
                                </li>
                              </ul>
                            </NavLink>
                          </div>
                        ))}
                      </div>
                    ) : null}

                    {universaldata &&
                    universaldata.posts &&
                    universaldata.posts.length > 0 ? (
                      <div>
                        <h6 className="mb-4">Posts</h6>
                        {universaldata.posts.map((posts) => (
                          <div className="activateaccountstaff" key={posts._id}>
                            <NavLink to={`/post-detail/${posts._id}`}>
                              <ul className="ps-0">
                                <li className="d-flex justify-content-start align-items-center">
                                  <div>
                                    <img
                                      className="rounded-circle profile_picture"
                                      src={Pic}
                                      alt="Profile"
                                    />
                                    <br />
                                  </div>
                                  <div>
                                    <strong>{posts.postTitle}</strong>
                                  </div>
                                </li>
                              </ul>
                            </NavLink>
                          </div>
                        ))}
                      </div>
                    ) : null}
                  </div>
                )}
              </div>
              <br />

              <div className="stories_blog_section mt-3">
                <div className="tabbable" id="tabs-459476">
                  {/* {totalMatches > 0 && (
                    <button className="btn btn-primary" onClick={handleNotice}>
                      Notice &nbsp;
                      <span className="notice_bages">{totalMatches}</span>
                    </button>
                  )} */}
                  {totalMatches > 0 ? (
                    <button className="btn btn-primary" onClick={handleNotice}>
                      Notice &nbsp;
                      <span className="notice_bages">{totalMatches}</span>
                    </button>
                  ) : (
                    ""
                  )}
                </div>
              </div>
              <div className="row">
                  <div className="col-md-2">
                    <p className="text-white">
                    <b> Agent Hub</b> 
                    </p>
                  </div>
                  <div className="col-md-10">
                    <p className="text-center text-white">
                    <b> Welcome, {profile?.firstName}&nbsp;{profile?.lastName} </b>
                    </p>
                  </div>
              </div>
              <div
                className="special_days platform_features mt-0 p-0"
                style={{
                  backgroundImage: `url(${
                    imageData?.HomeMenus && imageData?.HomeMenus[0]?.homeMenus
                  })`,
                }}
              >
                <div className="category_feature">
                  <span className="text-center" onClick={handleMyTransaction}>
                    <i className="fa fa-calculator" aria-hidden="true"></i>
                    Transactions
                  </span>
                  <span className="text-center" onClick={handleMarket}>
                    {/* <i className="fa fa-usd" aria-hidden="true"></i> */}
                    <i className="fa fa-home" aria-hidden="true"></i>
                    My Listings
                  </span>
                  <span className="text-center" onClick={handTraining}>
                    <i className="fa fa-file-video-o" aria-hidden="true"></i>
                    Training
                  </span>
                  <span className="text-center" onClick={handleDocs}>
                    <i className="fa fa-file-text-o"></i>
                    Office Docs
                  </span>
                  <span className="text-center" onClick={handletasks}>
                    <i className="fa fa-tasks" aria-hidden="true"></i>
                    Tasks
                  </span>
                  <span className="text-center" onClick={handleContact}>
                    <i className="fa fa-handshake-o" aria-hidden="true"></i>
                    My CRM
                  </span>
                  <span className="text-center" onClick={handlestore}>
                    <i className="fa fa-shopping-bag" aria-hidden="true"></i>
                    Store
                  </span>
                  <span className="text-center" onClick={handlesVendor}>
                    <i className="fa fa-users" aria-hidden="true"></i>
                    Vendors
                  </span>
                </div>
              </div>

              <div id="postsection" className="agent_hub_section mt-3">
                <div className="row">
                  {/* <div className="col-md-12">
                    <p className="text-white">
                      Agent <b>Hub</b>
                    </p>
                  </div> */}
                  <div className="col-md-4">
                    <div
                      className="my_files"
                      style={{
                        backgroundImage: `url(${
                          imageData.HomeLinks &&
                          imageData.HomeLinks[0]?.homeLinks
                        })`,
                      }}
                    >
                      <div className="file_links_section">
                        <p>
                          <b>My Links And Files</b>&nbsp; &nbsp;
                          <NavLink to="/add-linkfiles">+</NavLink>
                        </p>

                        {/* <ul className="float-left w-100">
                          {link.data && link.data.length > 0
                            ? link.data.map((items, index) => (
                                <NavLink target="_blank" to={items.link_url}>
                                  <li key={index}>{items.title}</li>
                                </NavLink>
                              ))
                            : "Not any Link/File assigned"}
                        </ul> */}

                        <ul className="float-left w-100">
                          {link.data && link.data.length > 0
                            ? link.data.slice(0, 5).map((items, index) => (
                                <NavLink target="_blank" to={items.link_url}>
                                  <li key={index}>{items.title}</li>
                                </NavLink>
                              ))
                            : "Not any Link/File assigned"}
                        </ul>
                        <NavLink to="/quick-links" className="learn_more">
                          Show More{" "}
                        </NavLink>

                        {question.data && question.data.length > 5 ? (
                          <div className="load_more w-100 text-center">
                            <NavLink className="mt-3" to="/question">
                              Load More
                            </NavLink>
                          </div>
                        ) : (
                          ""
                        )}
                      </div>
                    </div>
                  </div>

                  <div className="col-md-4">
                    <div
                      className="my_files"
                      style={{
                        backgroundImage: `url(${
                          imageData.HomeQuestions &&
                          imageData.HomeQuestions[0].homeQuestions
                        })`,
                      }}
                    >
                      <div className="file_links_section">
                        <p>
                          <b>Questions</b>
                        </p>
                        <ul className="float-left w-100">
                          {question.data && question.data.length > 0
                            ? question.data.slice(0, 3).map((items, index) => (
                                <NavLink to="/question">
                                  <li key={index}>{items.question}</li>
                                </NavLink>
                              ))
                            : "Not any Question assigned"}
                        </ul>
                        <NavLink to="/question" className="learn_more">
                          Show More{" "}
                        </NavLink>
                      </div>
                    </div>
                  </div>

                  <div className="col-md-4">
                    <div
                      className="my_files"
                      style={{
                        backgroundImage: `url(${
                          imageData.HomeStats &&
                          imageData.HomeStats[0].homeStats
                        })`,
                      }}
                    >
                      <div className="file_links_section">
                        <p>
                          <b>Stats</b>
                        </p>
                        <NavLink to={`/contacttransaction/transaction`}>
                          <div className="views_progress d-flex align-items-center justify-content-between mb-2">
                            <span className="pull-left">Transactions</span>
                            <span className="pull-right">
                              {statsReport?.totalRecords_transactions ?? "0"}
                              {/* {totalCount ?? "0"} */}
                            </span>
                          </div>
                        </NavLink>
                        <NavLink to={`/filed-transaction/transaction`}>
                          <div className="share_progress d-flex align-items-center justify-content-between mb-2">
                            <span className="pull-left">
                              Complete Transactions
                            </span>

                            {statsReport?.totalRecords_transactioncompleted ??
                              "0"}
                          </div>
                        </NavLink>
                        {localStorage.getItem("auth") &&
                        jwt(localStorage.getItem("auth")).contactType ==
                          "admin" ? (
                          <>
                            <div className="comments_progress d-flex align-items-center justify-content-between mb-2">
                              <span className="pull-left">Sales Volume</span>
                              <span className="pull-right">
                                {/* {statsReport?.totalsales ? <CurrencyFormatter amount={statsReport?.totalsales} />: ""} */}
                                $
                                {totalCountVolume
                                  ? totalCountVolume.toLocaleString("en-US")
                                  : "0"}
                              </span>
                            </div>

                            <div className="views_progress d-flex align-items-center justify-content-between mb-2">
                              <span className="pull-left">Commission</span>
                              <span className="pull-right">
                                {/* {statsReport?.totaltransactioncomission ?? "0"} */}
                                $
                                {totalCountCommission
                                  ? totalCountCommission.toLocaleString("en-US")
                                  : "0"}
                              </span>
                            </div>
                          </>
                        ) : (
                          ""
                          //   <div className="comments_progress d-flex align-items-center justify-content-between mb-2">
                          //   <span className="pull-left">Sales Volume</span>
                          //   <span className="pull-right">
                          //   {statsReport?.totalsales ? <CurrencyFormatter amount={statsReport?.totalsales} />: ""}
                          //   </span>
                          // </div>
                        )}
                      </div>
                    </div>
                  </div>
                </div>
              </div>

              <div className="stories_blog_section mt-3">
                <div className="tabbable" id="tabs-459476">
                  {/* <div className="d-flex float-left w-100"> */}
                  <ul className="nav nav-tabs mb-2">
                    <li
                      className={`nav-item ${
                        activeButton === "blend" ? "active" : ""
                      }`}
                      // className="nav-item"
                      onClick={(e) => CatagoryBlend("blend")}
                    >
                      {/* {activeNavlink ? 'step active' : ''} */}
                      <a className="nav-link" href="#tab2" data-toggle="tab">
                        Office
                      </a>
                    </li>
                    {/* <li
                      className="nav-item"
                        onClick={(e) => CatagoryBlend("blend")}
                      >
                        <a className="nav-link" href="#tab1" data-toggle="tab">
                          Stories and Blog
                        </a>
                      </li> */}

                    <li
                      className="nav-item"
                      onClick={(e) => IndustryPostGetAll("nar")}
                    >
                      <a className="nav-link" href="#tab3" data-toggle="tab">
                        NAR
                      </a>
                    </li>
                    <li
                      className="nav-item"
                      onClick={(e) => IndustryPostGetAll("biggerpockets")}
                    >
                      <a className="nav-link" href="#tab4" data-toggle="tab">
                        Bigger Pockets
                      </a>
                    </li>
                    <li
                      className="nav-item"
                      onClick={(e) => IndustryPostGetAll("housingwire")}
                    >
                      <a className="nav-link" href="#tab5" data-toggle="tab">
                        Housingwire
                      </a>
                    </li>
                    <li
                      className="nav-item"
                      onClick={(e) => IndustryPostGetAll("glennfelson")}
                    >
                      <a className="nav-link" href="#tab6" data-toggle="tab">
                        Glen Felson
                      </a>
                    </li>
                    <li
                      className="nav-item"
                      onClick={(e) => IndustryPostGetAll("inman")}
                    >
                      <a className="nav-link" href="#tab7" data-toggle="tab">
                        Inman
                      </a>
                    </li>
                    <button
                      onClick={handlePosts}
                      style={{
                        backgroundColor: "#E86712",
                        color: "white",
                        borderRadius: "10px",
                        border: "1px solid white",
                      }}
                    >
                      + Add Posts
                    </button>
                  </ul>

                  <div className="tab-content showblog_category">
                    <div className="tab-pane active" id="tab2">
                      {getPost.map((post, index) => {
                        return (
                          <div className="blog_details" key={index}>
                            {/* <img className="img-fluid" src="https://integra.workspace.lwolf.com/common/handler/file.ashx?T=thumb&F=74a971d3-dbd8-495e-b8bf-1b120ddcfac6"></img> */}
                            <div className="blogpost_image">
                              {post.file ? (
                                <img
                                  className="img-fluid"
                                  src={post.file ? post.file : ""}
                                  onError={(e) => propertypic(e)}
                                />
                              ) : (
                                // <img
                                //   className="img-fluid"
                                //   src="https://integra.workspace.lwolf.com/common/handler/mls/photo_custom.ashx?s=1&t=5eada3e3-491e-46e2-938c-7c785a1f9c68&r=Seller"
                                // />
                                ""
                              )}
                            </div>
                            <span>
                              <h6>
                                {post.postTitle.length > 55
                                  ? post.postTitle.substring(0, 55) + "..."
                                  : post.postTitle}
                              </h6>
                              <p>
                                {post.postBlurb.length > 100
                                  ? post.postBlurb.substring(0, 100) + "..."
                                  : post.postBlurb}
                              </p>
                              <div className="w-100 text-right">
                                <a
                                  className="pull-right"
                                  onClick={(e) => PostDetail(post._id)}
                                >
                                  Load More
                                </a>
                              </div>
                            </span>
                          </div>
                        );
                      })}
                      <div className="pagination_blog w-100 d-block mt-3">
                        {totalRecords > 5 ? (
                          <div className="justify-content-end mb-1">
                            <ReactPaginate
                              previousLabel={"Previous"}
                              nextLabel={"Next"}
                              breakLabel={"..."}
                              pageCount={pageCount}
                              marginPagesDisplayed={1}
                              pageRangeDisplayed={2}
                              onPageChange={handlePageClick}
                              containerClassName={
                                "pagination justify-content-center"
                              }
                              pageClassName={"page-item"}
                              pageLinkClassName={"page-link"}
                              previousClassName={"page-item"}
                              previousLinkClassName={"page-link"}
                              nextClassName={"page-item"}
                              nextLinkClassName={"page-link"}
                              breakClassName={"page-item"}
                              breakLinkClassName={"page-link"}
                              activeClassName={"active"}
                            />
                          </div>
                        ) : (
                          ""
                        )}
                      </div>
                    </div>
                    {/* <div className="tab-pane" id="tab1">
                        {getPost.map((post) => {
                          return (
                            <div className="blog_details">
                             {post.file ? (
                                <img
                                  className="img-fluid"
                                  src={post.file ? post.file : ""}
                                  onError={(e) => propertypic(e)}
                                />
                              ) : (
                                <img
                                  className="img-fluid"
                                  src="https://integra.workspace.lwolf.com/common/handler/mls/photo_custom.ashx?s=1&t=5eada3e3-491e-46e2-938c-7c785a1f9c68&r=Seller"
                                />
                              )}
                              <span>
                                <h6>
                                  {post.postTitle.length > 55
                                    ? post.postTitle.substring(0, 55) + "..."
                                    : post.postTitle}
                                </h6>
                                <p>
                                  {post.postBlurb.length > 100
                                    ? post.postBlurb.substring(0, 100) + "..."
                                    : post.postBlurb}
                                </p>
                                <div className="w-100 text-right">
                                  <a
                                  className="pull-right"
                                    onClick={(e) => PostDetail(post._id)}
                                  >
                                    Load More
                                  </a>
                                </div>
                              </span>
                            </div>
                          );
                        })}
                        <div className="pagination_blog w-100 d-block mt-3">
                          {totalRecords > 5 ? (
                            <div className="justify-content-end mb-1">
                              <ReactPaginate
                                previousLabel={"Previous"}
                                nextLabel={"Next"}
                                breakLabel={"..."}
                                pageCount={pageCount}
                                marginPagesDisplayed={1}
                                pageRangeDisplayed={2}
                                onPageChange={handlePageClick}
                                containerClassName={
                                  "pagination justify-content-center"
                                }
                                pageClassName={"page-item"}
                                pageLinkClassName={"page-link"}
                                previousClassName={"page-item"}
                                previousLinkClassName={"page-link"}
                                nextClassName={"page-item"}
                                nextLinkClassName={"page-link"}
                                breakClassName={"page-item"}
                                breakLinkClassName={"page-link"}
                                activeClassName={"active"}
                              />
                            </div>
                          ) : (
                            ""
                          )}
                        </div>
                      </div> */}
                    <div className="tab-pane" id="tab3">
                      {getIndustryPost.map((post, index) => {
                        return (
                          <div className="blog_details" key={index}>
                            {post.imageSrc ? (
                              <img
                                className="img-fluid"
                                src={post.imageSrc ? post.imageSrc : ""}
                                onError={(e) => propertypic(e)}
                              />
                            ) : (
                              // <img
                              //   className="img-fluid"
                              //   src="https://integra.workspace.lwolf.com/common/handler/mls/photo_custom.ashx?s=1&t=5eada3e3-491e-46e2-938c-7c785a1f9c68&r=Seller"
                              // />
                              ""
                            )}
                            <span>
                              <h6>
                                {post?.postTitle?.length > 55
                                  ? post?.postTitle.substring(0, 55) + "..."
                                  : post?.postTitle}
                              </h6>
                              <p>
                                {post?.shortSummary?.length > 100
                                  ? post?.shortSummary.substring(0, 100) + "..."
                                  : post?.shortSummary}
                              </p>
                              <div className="w-100 text-right">
                                <a
                                  className="pull-right"
                                  onClick={(e) =>
                                    PostDetailIndustry(post.linkURL)
                                  }
                                >
                                  Load More
                                </a>
                              </div>
                            </span>
                          </div>
                        );
                      })}
                      <div className="pagination_blog w-100 d-block mt-3">
                        {IndustrytotalRecords > 5 ? (
                          <div className="justify-content-end mb-1">
                            <ReactPaginate
                              previousLabel={"Previous"}
                              nextLabel={"Next"}
                              breakLabel={"..."}
                              pageCount={IndustrypageCount}
                              marginPagesDisplayed={1}
                              pageRangeDisplayed={2}
                              onPageChange={handleIndustryPageClick}
                              containerClassName={
                                "pagination justify-content-center"
                              }
                              pageClassName={"page-item"}
                              pageLinkClassName={"page-link"}
                              previousClassName={"page-item"}
                              previousLinkClassName={"page-link"}
                              nextClassName={"page-item"}
                              nextLinkClassName={"page-link"}
                              breakClassName={"page-item"}
                              breakLinkClassName={"page-link"}
                              activeClassName={"active"}
                            />
                          </div>
                        ) : (
                          ""
                        )}
                      </div>
                    </div>

                    <div className="tab-pane" id="tab4">
                      {getIndustryPost.map((post, index) => {
                        return (
                          <div className="blog_details" key={index}>
                            {post.imageSrc ? (
                              <img
                                className="img-fluid"
                                src={post.imageSrc ? post.imageSrc : ""}
                                onError={(e) => propertypic(e)}
                              />
                            ) : (
                              // <img
                              //   className="img-fluid"
                              //   src="https://integra.workspace.lwolf.com/common/handler/mls/photo_custom.ashx?s=1&t=5eada3e3-491e-46e2-938c-7c785a1f9c68&r=Seller"
                              // />
                              ""
                            )}
                            <span>
                              <h6>
                                {post?.postTitle?.length > 55
                                  ? post?.postTitle.substring(0, 55) + "..."
                                  : post?.postTitle}
                              </h6>
                              <p>
                                {post?.shortSummary?.length > 100
                                  ? post?.shortSummary.substring(0, 100) + "..."
                                  : post?.shortSummary}
                              </p>
                              <div className="w-100 text-right">
                                <a
                                  className="pull-right"
                                  onClick={(e) =>
                                    PostDetailIndustry(post.linkURL)
                                  }
                                >
                                  Load More
                                </a>
                              </div>
                            </span>
                          </div>
                        );
                      })}
                      <div className="pagination_blog w-100 d-block mt-3">
                        {IndustrytotalRecords > 5 ? (
                          <div className="justify-content-end mb-1">
                            <ReactPaginate
                              previousLabel={"Previous"}
                              nextLabel={"Next"}
                              breakLabel={"..."}
                              pageCount={IndustrypageCount}
                              marginPagesDisplayed={1}
                              pageRangeDisplayed={2}
                              onPageChange={handleIndustryPageClick}
                              containerClassName={
                                "pagination justify-content-center"
                              }
                              pageClassName={"page-item"}
                              pageLinkClassName={"page-link"}
                              previousClassName={"page-item"}
                              previousLinkClassName={"page-link"}
                              nextClassName={"page-item"}
                              nextLinkClassName={"page-link"}
                              breakClassName={"page-item"}
                              breakLinkClassName={"page-link"}
                              activeClassName={"active"}
                            />
                          </div>
                        ) : (
                          ""
                        )}
                      </div>
                    </div>
                    <div className="tab-pane" id="tab5">
                      {getIndustryPost.map((post, index) => {
                        return (
                          <div className="blog_details" key={index}>
                            {post.imageSrc ? (
                              <img
                                className="img-fluid"
                                src={post.imageSrc ? post.imageSrc : ""}
                                onError={(e) => propertypic(e)}
                              />
                            ) : (
                              // <img
                              //   className="img-fluid"
                              //   src="https://integra.workspace.lwolf.com/common/handler/mls/photo_custom.ashx?s=1&t=5eada3e3-491e-46e2-938c-7c785a1f9c68&r=Seller"
                              // />
                              ""
                            )}
                            <span>
                              <h6>
                                {post?.postTitle?.length > 55
                                  ? post?.postTitle.substring(0, 55) + "..."
                                  : post?.postTitle}
                              </h6>
                              <p>
                                {post?.shortSummary?.length > 100
                                  ? post?.shortSummary.substring(0, 100) + "..."
                                  : post?.shortSummary}
                              </p>
                              <div className="w-100 text-right">
                                <a
                                  className="pull-right"
                                  onClick={(e) =>
                                    PostDetailIndustry(post.linkURL)
                                  }
                                >
                                  Load More
                                </a>
                              </div>
                            </span>
                          </div>
                        );
                      })}
                      <div className="pagination_blog w-100 d-block mt-3">
                        {IndustrytotalRecords > 5 ? (
                          <div className="justify-content-end mb-1">
                            <ReactPaginate
                              previousLabel={"Previous"}
                              nextLabel={"Next"}
                              breakLabel={"..."}
                              pageCount={IndustrypageCount}
                              marginPagesDisplayed={1}
                              pageRangeDisplayed={2}
                              onPageChange={handleIndustryPageClick}
                              containerClassName={
                                "pagination justify-content-center"
                              }
                              pageClassName={"page-item"}
                              pageLinkClassName={"page-link"}
                              previousClassName={"page-item"}
                              previousLinkClassName={"page-link"}
                              nextClassName={"page-item"}
                              nextLinkClassName={"page-link"}
                              breakClassName={"page-item"}
                              breakLinkClassName={"page-link"}
                              activeClassName={"active"}
                            />
                          </div>
                        ) : (
                          ""
                        )}
                      </div>
                    </div>
                    <div className="tab-pane" id="tab6">
                      {getIndustryPost.map((post, index) => {
                        return (
                          <div className="blog_details" key={index}>
                            {post.imageSrc ? (
                              <img
                                className="img-fluid"
                                src={post.imageSrc ? post.imageSrc : ""}
                                onError={(e) => propertypic(e)}
                              />
                            ) : (
                              // <img
                              //   className="img-fluid"
                              //   src="https://integra.workspace.lwolf.com/common/handler/mls/photo_custom.ashx?s=1&t=5eada3e3-491e-46e2-938c-7c785a1f9c68&r=Seller"
                              // />
                              ""
                            )}
                            <span>
                              <h6>
                                {post?.postTitle?.length > 55
                                  ? post?.postTitle.substring(0, 55) + "..."
                                  : post?.postTitle}
                              </h6>
                              <p>
                                {post?.shortSummary?.length > 100
                                  ? post?.shortSummary.substring(0, 100) + "..."
                                  : post?.shortSummary}
                              </p>
                              <div className="w-100 text-right">
                                <a
                                  className="pull-right"
                                  onClick={(e) =>
                                    PostDetailIndustry(post.linkURL)
                                  }
                                >
                                  Load More
                                </a>
                              </div>
                            </span>
                          </div>
                        );
                      })}
                      <div className="pagination_blog w-100 d-block mt-3">
                        {IndustrytotalRecords > 5 ? (
                          <div className="justify-content-end mb-1">
                            <ReactPaginate
                              previousLabel={"Previous"}
                              nextLabel={"Next"}
                              breakLabel={"..."}
                              pageCount={IndustrypageCount}
                              marginPagesDisplayed={1}
                              pageRangeDisplayed={2}
                              onPageChange={handleIndustryPageClick}
                              containerClassName={
                                "pagination justify-content-center"
                              }
                              pageClassName={"page-item"}
                              pageLinkClassName={"page-link"}
                              previousClassName={"page-item"}
                              previousLinkClassName={"page-link"}
                              nextClassName={"page-item"}
                              nextLinkClassName={"page-link"}
                              breakClassName={"page-item"}
                              breakLinkClassName={"page-link"}
                              activeClassName={"active"}
                            />
                          </div>
                        ) : (
                          ""
                        )}
                      </div>
                    </div>
                    <div className="tab-pane" id="tab7">
                      {getIndustryPost.map((post, index) => {
                        return (
                          <div className="blog_details" key={index}>
                            {post.imageSrc ? (
                              <img
                                className="img-fluid"
                                src={post.imageSrc ? post.imageSrc : ""}
                                onError={(e) => propertypic(e)}
                              />
                            ) : (
                              // <img
                              //   className="img-fluid"
                              //   src="https://integra.workspace.lwolf.com/common/handler/mls/photo_custom.ashx?s=1&t=5eada3e3-491e-46e2-938c-7c785a1f9c68&r=Seller"
                              // />
                              ""
                            )}
                            <span>
                              <h6>
                                {post?.postTitle?.length > 55
                                  ? post?.postTitle.substring(0, 55) + "..."
                                  : post?.postTitle}
                              </h6>
                              <p>
                                {post?.shortSummary?.length > 100
                                  ? post?.shortSummary.substring(0, 100) + "..."
                                  : post?.shortSummary}
                              </p>
                              <div className="w-100 text-right">
                                <a
                                  className="pull-right"
                                  onClick={(e) =>
                                    PostDetailIndustry(post.linkURL)
                                  }
                                >
                                  Load More
                                </a>
                              </div>
                            </span>
                          </div>
                        );
                      })}
                      <div className="pagination_blog w-100 d-block mt-3">
                        {IndustrytotalRecords > 5 ? (
                          <div className="justify-content-end mb-1">
                            <ReactPaginate
                              previousLabel={"Previous"}
                              nextLabel={"Next"}
                              breakLabel={"..."}
                              pageCount={IndustrypageCount}
                              marginPagesDisplayed={1}
                              pageRangeDisplayed={2}
                              onPageChange={handleIndustryPageClick}
                              containerClassName={
                                "pagination justify-content-center"
                              }
                              pageClassName={"page-item"}
                              pageLinkClassName={"page-link"}
                              previousClassName={"page-item"}
                              previousLinkClassName={"page-link"}
                              nextClassName={"page-item"}
                              nextLinkClassName={"page-link"}
                              breakClassName={"page-item"}
                              breakLinkClassName={"page-link"}
                              activeClassName={"active"}
                            />
                          </div>
                        ) : (
                          ""
                        )}
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className="col-md-4">
              <div
                className="special_days agent_detailss"
                onClick={CalenderPage}
                style={{
                  backgroundImage: `url(${
                    imageData.HomeCalender &&
                    imageData.HomeCalender[0]?.homeCalender
                  })`,
                }}
              >
                <div className="d-flex align-items-center justify-content-between pb-2">
                  <h6 className="mb-0">{currentDate}</h6>
                  <span className="w-auto" onClick={CalenderPage}>
                    <i className="fa fa-plus" aria-hidden="true"></i>
                  </span>
                </div>
                <hr />
                <div className="caledar_view mt-2" style={{ height: "285px" }}>
                  <BigCalendar
                    localizer={localizer}
                    events={events}
                    startAccessor="start"
                    endAccessor="end"
                    onSelectSlot={handleSelectSlot}
                    selectable={true}
                    components={{
                      event: ({ event }) => (
                        <div
                          className="rbc-event-content"
                          title={event.title}
                          onClick={handleEventClick}
                          data-eventid={event.id}
                        >
                          {event.title}
                        </div>
                      ),
                    }}
                    views={{
                      month: true,
                      week: false,
                      day: false,
                      agenda: false,
                    }}
                    // components={{
                    //   toolbar: CustomToolbar,
                    // }}
                    eventPropGetter={eventStyleGetter}
                    style={{ flex: 1 }}
                  />
                </div>
              </div>
              <div
                className="special_days mt-3"
                style={{
                  backgroundImage: `url(${
                    imageData.HomeBirthdays &&
                    imageData.HomeBirthdays[0]?.homeBirthdays
                  })`,
                }}
              >
                <div className="agent_details">
                  <label className="col-form-label pt-0">
                    Birthdays & Work <b>Anniversaries</b>
                  </label>

                  {upcomingBirthdayData}
                  {loadMoreLink}
                </div>
              </div>

              <div
                className="float-start w-100 special_days mt-3"
                style={{
                  backgroundImage: `url(${
                    imageData.HomeAssociates &&
                    imageData.HomeAssociates[0]?.homeAssociates
                  })`,
                }}
              >
                <div className="agent_details float-start w-100">
                  <p>
                    Welcome <b>New Associates</b>
                  </p>
                  {newassociatesData}
                  <br />
                  {newassociatesloadMoreLink}
                  {/* <div className="float-left w-100 py-2">
                    <img className="img-fluid" src="https://brokeragentbase.s3.amazonaws.com/1685363953670-avtar.jpg"></img>
                    <span>
                      <p className="mb-0"><b>Agent Name</b></p>
                      <p className="mb-1">Office:</p>
                      <p className="mb-1">Joined:</p>
                      <p className="mb-0 mt-1">From</p>
                    </span>
                  </div>
                  <hr className="float-left w-100"></hr> */}
                </div>
              </div>

              <div
                className="special_days mt-3"
                // style={{
                //   backgroundImage: `url(${
                //     imageData.HomeAssociates &&
                //     imageData.HomeAssociates[0]?.homeAssociates
                //   })`,
                // }}
              >
                <div className="agent_details p-3 border-0 rounded-3">
                  <h6>ONLINE NOW</h6>
                  <hr />
                  <div className="float-start w-100 online_users mt-2">
                    {/* <h6 className="">ONLINE NOW</h6> */}
                    <span>
                      <img
                        className="img-fluid mb-0"
                        src={profile.image ?? Picture}
                        alt="profile picture"
                        onError={(e) => profilepic(e)}
                        onClick={handleSignIn}
                        style={{
                          cursor: "pointer",
                          color: "#cdc4c4",
                          backgroundColor: "#dedef7",
                        }}
                        title={`${profile.firstName} ${profile.lastName}`}
                      />
                        <div className="info_window">
                          <p>
                            {profile.firstName} {profile.lastName}
                          </p>
                        </div>
                    </span>
                    {contactLoginStatus.length > 0
                      ? contactLoginStatus.map((contact, index) =>{
                        let timee = timeAgo(new Date(contact.lastlogin));
                        if(timee < 24){
                          return (
                            <>
                              <span>
                                <img
                                  className="img-fluid mb-0"
                                  src={contact.image ?? Picture}
                                  alt="profile picture"
                                  onError={(e) => profilepic(e)}
                                  onClick={()=>handleSignContact(contact._id)}
                                  style={{
                                    cursor: "pointer",
                                    color: "#cdc4c4",
                                    backgroundColor: "#dedef7",
                                  }}
                                  title={`${contact.firstName} ${contact.lastName}`}
                                />
                                {contact.is_login === "yes" ? (
                                  <>
                                    {/* <i
                                      class="fa fa-circle"
                                      aria-hidden="true"
                                      style={{ color: "green" }}
                                    ></i>{" "} */}
                                    {/* {contact.firstName} {contact.lastName}
                                      Last seen:{" "}
                                      {timeAgo(new Date(contact.lastlogin))} */}
                                  </>
                                ) : (
                                  <>
                                    {/* <i
                                      class="fa fa-circle"
                                      aria-hidden="true"
                                      style={{ color: "red" }}
                                    ></i>{" "} */}
                                    {/* {contact.firstName} {contact.lastName}
                                      {new Date(
                                        contact.lastlogin
                                      ).toLocaleString()} */}
                                  </>
                                )}
                                <div className="info_window">
                                  <p>
                                    {contact.firstName} {contact.lastName}
                                  </p>
                                  {/* <p>Last seen:{" "}{timeAgo(new Date(contact.lastlogin))} </p> */}
                                  {/* <div className="float-start w-100">
                                    <button class="btn btn-primary btn-sm">Chat</button>
                                  </div> */}
                                </div>
                              </span>
                            </>
                          );

                        }
                      }
                      )
                      : ""}
                  </div>

                  {/* <table className="w-100">
                      <thead className="my-3">
                        <tr>
                          <th>Name</th>
                          <th className="pull-right">Login</th>
                        </tr>
                      </thead>
                      <tbody>
                        {contactLoginStatus.length > 0 ? (
                          contactLoginStatus.map((contact, index) => (
                            <tr key={contact.id}>
                              {contact.is_login === "yes" ? (
                                <>
                                  <td>
                                    {" "}
                                    <i
                                      class="fa fa-circle"
                                      aria-hidden="true"
                                      style={{ color: "green" }}
                                    ></i>{" "}
                                    {contact.firstName} {contact.lastName}
                                  </td>
                                  <td className="pull-right">
                                    Last seen:{" "}
                                    {timeAgo(new Date(contact.lastlogin))}
                                  </td>
                                </>
                              ) : (
                                <>
                                  <td>
                                    {" "}
                                    <i
                                      class="fa fa-circle"
                                      aria-hidden="true"
                                      style={{ color: "red" }}
                                    ></i>{" "}
                                    {contact.firstName} {contact.lastName}
                                  </td>
                                  <td colSpan="3">
                                    {new Date(
                                      contact.lastlogin
                                    ).toLocaleString()}
                                  </td>
                                </>
                              )}
                            </tr>
                          ))
                        ) : (
                          <tr>
                            <td colSpan="2">No contacts found</td>
                          </tr>
                        )}
                      </tbody>
                    </table> */}
                </div>
              </div>
            </div>
          </div>

          {isVisible ? (
            <div className="modal in" role="dialog" id="submitInprogress">
              <div
                className="modal-dialog modal-mg modal-dialog-scrollable"
                role="document"
              >
                <div className="modal-content">
                  <div className="modal-header">
                    <h4 className="modal-title">Send a Message</h4>
                    <button
                      className="btn-close"
                      type="button"
                      id="modalconnectclose"
                      data-dismiss="modal"
                      aria-label="Close"
                    ></button>
                  </div>
                  <div className="modal-body fs-lg">
                    <div className="row">
                      {agentData.length > 0 ? (
                        agentData.map((contact, index) => (
                          <div className="float-start w-100 d-flex align-items-center justify-conten-start my-2">
                            <img
                              src={contact.image || Pic}
                              alt="Agent Avatar"
                              width="100px"
                              height="100px"
                            />
                            <span className="mb-0 ms-3">
                              {contact.filterType === "birthday" &&
                                "Happy Birthday"}
                              <p className="mb-0">
                                <b>
                                  {contact.firstName} {contact.lastName}
                                </b>{" "}
                                &nbsp;
                                {contact.filterType === "associatesince" &&
                                  ` with ${contact.company ?? ""}`}
                              </p>
                              <p className="mb-1">{contact.title ?? ""}</p>
                              <p className="mb-0 mt-1">
                                {contact.filterType === "birthday" && (
                                  <>
                                    {contact.birthday
                                      ? moment(contact.birthday).format(
                                          "MMMM D"
                                        )
                                      : ""}
                                  </>
                                )}
                                {contact.filterType === "associatesince" && (
                                  <>
                                    {contact?.additionalActivateFields
                                      ?.associate_since
                                      ? moment(
                                          contact?.additionalActivateFields
                                            ?.associate_since
                                        ).format("MMMM D")
                                      : ""}
                                  </>
                                )}
                                {/* Helper function to format date */}
                              </p>
                            </span>
                          </div>
                        ))
                      ) : (
                        <p></p>
                      )}
                      <div className="col-md-12">
                        <button
                          className="btn btn-secondary"
                          onClick={() =>
                            handleButtonClick("Happy birthday to you🎂")
                          }
                        >
                          Happy birthday to you🎂
                        </button>
                        <button
                          className="btn btn-secondary ms-3"
                          onClick={() => handleButtonClick("Happy day")}
                        >
                          Happy day
                        </button>
                        <button
                          className="btn btn-secondary ms-3 mt-2"
                          onClick={() => handleButtonClick("Happy Birthday!🎉")}
                        >
                          Happy Birthday!🎉
                        </button>
                        <button
                          className="btn btn-secondary ms-3 mt-2"
                          onClick={() => handleButtonClick("Happy birthday")}
                        >
                          Happy birthday
                        </button>
                        <button
                          className="btn btn-secondary ms-3 mt-2"
                          onClick={() =>
                            handleButtonClick("Happy Birthday🎂🎉")
                          }
                        >
                          Happy Birthday🎂🎉
                        </button>
                        <button
                          className="btn btn-secondary ms-3 mt-2"
                          onClick={() =>
                            handleButtonClick("Happy Birthday!!🎂🎉")
                          }
                        >
                          Happy Birthday!!🎂🎉
                        </button>
                      </div>

                      <div className="col-md-12 mt-3">
                        <label className="form-label mt-3">
                          Birthday🎂🎉 Message
                        </label>
                        <textarea
                          className="form-control mt-0"
                          id="textarea-input"
                          rows="5"
                          name="messages"
                          value={messages}
                          onChange={(e) => setMessage(e.target.value)}
                          autoComplete="on"
                        ></textarea>
                        <button
                          className="btn btn-primary pull-right mt-3"
                          onClick={handleSubmitBirthday}
                        >
                          Send Meesage
                        </button>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          ) : (
            ""
          )}
        </main>
      </div>

      <div className="modal" role="dialog" id="modal-show-calender">
        <div
          className="modal-dialog modal-lg modal-dialog-scrollable"
          role="document"
        >
          <div className="modal-content">
            <div className="modal-header">
              <h4 className="modal-title">Add an Event</h4>
              <button
                className="btn-close"
                type="button"
                data-bs-dismiss="modal"
                aria-label="Close"
              ></button>
            </div>

            <div className="modal-body fs-sm">
              <div className="col-md-12">
                <div className="mb-3">
                  <label className="form-label" for="pr-city">
                    Event<span className="text-danger">*</span>
                  </label>
                  <input
                    className="form-control"
                    id="inline-form-input"
                    type="text"
                    placeholder="Title"
                    name="title"
                    onChange={handleChange}
                    value={formValues.title}
                    style={{
                      border: formErrors?.title ? "1px solid red" : "",
                    }}
                  />
                  <div className="invalid-tooltip">{formErrors.title}</div>
                </div>

                <div className="mb-3">
                  <label className="form-label" for="pr-city">
                    Category
                  </label>
                  <select
                    className="form-select"
                    id="pr-city"
                    name="category"
                    onChange={handleChange}
                    value={formValues.category}
                    style={{
                      border: formErrors?.category ? "1px solid red" : "",
                    }}
                  >
                    <option></option>
                    <option>Office</option>
                    <option>Personal</option>
                    <option>Staff</option>
                    <option>Training</option>
                    <option>Community</option>
                    <option>Courses</option>
                  </select>
                  <div className="invalid-tooltip">{formErrors.category}</div>
                </div>
                <div className="pull-right mt-3">
                  <button
                    onClick={(e) => handleSubmit(e, selectedDate)}
                    type="button"
                    className="btn btn-primary btn-sm"
                    id="save-button"
                  >
                    Add Event
                  </button>
                  <button
                    type="button"
                    className="btn btn-secondary btn-sm ms-3"
                    id="close"
                    data-bs-dismiss="modal"
                  >
                    Cancel & Close
                  </button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
export default Main;

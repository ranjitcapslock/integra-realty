import React, { useState, useEffect } from "react";
import user_service from "../service/user_service";

import Loader from "../../Pages/Loader/Loader.js";
import Toaster from "../../Pages/Toaster/Toaster";
import jwt from "jwt-decode";
import { NavLink } from "react-router-dom";

const BussinessTab = () => {
  const [loader, setLoader] = useState({ isActive: null });
  const [toaster, setToaster] = useState({
    types: null,
    isShow: null,
    toasterBody: "",
    message: "",
  });

  const [statsReport, setStatsReport] = useState("");
  const statsReportGet = async () => {
    let queryParams = `?page=1`;
    queryParams += `&agentId=${jwt(localStorage.getItem("auth")).id}`;
    await user_service
      .TransactionBussinessReport(queryParams)
      .then((response) => {
        if (response) {
          setStatsReport(response.data);
        }
      });
  };

  const [statsReportExpences, setStatsReportExpences] = useState("");
  const [platformData, setPlatFormData] = useState({});


  const StatsReportExpences = async () => {
    let queryParams = `?page=1`;
    queryParams += `&agentId=${jwt(localStorage.getItem("auth")).id}`;
    await user_service
      .TransactionBussinessExpences(queryParams)
      .then((response) => {
        if (response) {
          setStatsReportExpences(response.data);
        }
      });
  };

  const [profile, setProfile] = useState("");
  const profileGetAll = async () => {
    if (localStorage.getItem("auth")) {
      await user_service
        .profileGet(jwt(localStorage.getItem("auth")).id)
        .then((response) => {
          if (response) {
            setProfile(response.data);
          }
        });
    }
  };

  const PlatformdetailsGet = async () => {
    await user_service.platformdetailsGet().then((response) => {
      if (response) {
        setPlatFormData(response.data.data[0]);
      }
    });
  };


  useEffect(() => {
    profileGetAll();
    statsReportGet();
    StatsReportExpences();
    PlatformdetailsGet()
  }, [profile]);

  const [activeTab, setActiveTab] = useState("my_fee"); // 'earnings' or 'expenses'

  const handleFee = () => {
    setActiveTab("my_fee");
  };

  const handleEarnings = () => {
    setActiveTab("earnings");
  };

  const handleExpenses = () => {
    setActiveTab("expenses");
  };



  return (
    <div className="bg-secondary float-left w-100 pt-4 mb-4">
      <Loader isActive={loader.isActive} />
      {toaster.isShow && (
        <Toaster
          types={toaster.types}
          isShow={toaster.isShow}
          toasterBody={toaster.toasterBody}
          message={toaster.message}
        />
      )}
      <main className="page-wrapper">
        <div className="">
          <div className="row">
            <div className="col-md-12">
              <div className="bg-light border rounded-3  mb-4 p-3">
                <div className="">
                  <h3 className="text-left mb-0 justify-content-start d-flex align-items-center">
                    My Business
                  </h3>
                </div>
              </div>
              <div className="bg-light p-3 border-0 rounded-3 shadow-sm float-start w-100">
                <div className="d-lg-flex d-md-block d-sm-block d-block align-items-center justify-content-between py-4">
                  <div>
                  <button
                    className={`btn ${
                      activeTab === "my_fee" ? "btn-primary" : "btn-secondary"
                    }`}
                    onClick={handleFee}
                  >
                    My Fee Structure
                  </button>
                  <button
                    className={`btn ${
                      activeTab === "earnings"
                        ? "btn-primary ms-2"
                        : "btn-secondary ms-2"
                    }`}
                    onClick={handleEarnings}
                  >
                    1099 Earnings
                  </button>
                  <button
                    className={`btn ${
                      activeTab === "expenses"
                        ? "btn-primary ms-2"
                        : "btn-secondary ms-2"
                    }`}
                    onClick={handleExpenses}
                  >
                    Expenses
                  </button>
                  </div>
                  <div className="mt-lg-0 mt-md-3 mt-sm-3 mt-3">
                  <NavLink
                    to={`/contacttransaction/transaction`}
                    className="btn btn-primary"
                  >
                    Transactions
                  </NavLink>
                  {
                    profile?._id? 
                  <NavLink
                    to={`/contact-profile/${profile?._id}`}
                    className="btn btn-primary ms-2"
                  >
                    My Profile
                  </NavLink>
                  :""
                  }
                  </div>
                </div>

                <div className="row mt-4">
                  <div className="col-lg-4 col-md-6 col-sm-6 col-12 mb-lg-0 mb-md-3 mb-sm-3 mb-3">
                    <div className="icon-box card card-body h-100 border-0 shadow-sm card-hover text-center">

                    {/* {statsReport.totalRecords_transactioncompleted ? ( */}
                    <p>
                      <b>YTD Closings</b><br />
                      {statsReport?.totalRecords_transactioncompleted ?? "0"}
                    </p>
                    {/* ) : (
                      ""
                    )} */}
                    </div>
                  </div>

                  <div className="col-lg-4 col-md-6 col-sm-6 col-12 mb-lg-0 mb-md-3 mb-sm-3 mb-3">
                    <div className="icon-box card card-body h-100 border-0 shadow-sm card-hover text-center">
                    <p>
                      <b>YTD Income</b> <br />$
                      {statsReport.totalEstimateFee
                        ? statsReport.totalEstimateFee.toLocaleString("en-US")
                        : "0"}
                    </p>
                    </div>
                  </div>

                  <div className="col-lg-4 col-md-6 col-sm-6 col-12 mb-lg-0 mb-md-3 mb-sm-3 mb-3">
                    <div className="icon-box card card-body h-100 border-0 shadow-sm card-hover text-center">
                    <p>
                      <b>YTD Sales Volume</b>
                      <br /> $
                      {statsReport.totalsales
                        ? statsReport.totalsales.toLocaleString("en-US")
                        : "0"}
                    </p>
                    </div>
                  </div>
                </div>

                {activeTab === "my_fee" && (
                  <div className="mt-4">
                    <div className="col-md-12 bg-light border rounded-3 p-3 mt-3">
                      <div className="table-responsive">
                      <table
                        id="DepositLedger"
                        className="table table-striped mb-0"
                        width="100%"
                        border="0"
                        cellSpacing="0"
                        cellPadding="0"
                      >
                       
                            <thead>
                              <tr>
                                <th>Type of Transaction Fee</th>
                                <th>Description</th>
                                <th>Fee Breakdown</th>
                              </tr>
                            </thead>
                            <tbody>
                              <tr>
                                <td>Transaction Fee</td>
                                <td>
                                  $595.00 per transaction side for residential
                                  property and referrals.
                                  <br />
                                  (Referral fees do not apply toward the annual
                                  CAP.)
                                </td>
                                <td>$495.00 or $595.00</td>
                              </tr>

                              <tr>
                                <td>Wire Fee</td>
                                <td>
                                  Plus $20.00 for incoming commission wires.
                                </td>
                                <td>$20.00</td>
                              </tr>
                              <tr>
                                <td>High Value Properties Fee</td>
                                <td>
                                  For all transactions that have a final sale
                                  price
                                  <br /> over $1.5 million, agents pay an
                                  additional $300 transaction
                                  <br />
                                  fee per $500,000 price increment over $1.5
                                  million.
                                </td>
                                <td>$300.00</td>
                              </tr>

                              <tr>
                                <td>Commercial Transactions Fee</td>
                                <td>
                                  {" "}
                                  In Depth Realty agents who are allowed to
                                  participate
                                  <br />
                                  in commercial transactions pay a transaction
                                  fee of 10%
                                  <br />
                                  of the commission amount earned per
                                  transaction on all funded <br />
                                  commercial transactions, to include commercial
                                  sales and leases.
                                </td>
                              </tr>
                              <tr>
                                <td>
                                  Both Sides of a Transaction “Dual Agency”
                                </td>
                                <td>
                                  Any transaction where the licensee has 2 sides
                                  of a single <br />
                                  transaction it’s counted as two separate
                                  transactions.
                                  <br />
                                  This is considered a "Dual Agency" and the
                                  brokerage will <br />
                                  collect a transaction fee for both transaction
                                  sides.
                                  <br />
                                  <br />
                                  The minimum split is $495.00 or $595.00.
                                  Representing Both <br /> Side of a
                                  Transaction: Agents who represent both parties
                                  to a single <br /> real estate transaction in
                                  a dual agency scenario pay an additional $350
                                  Risk
                                </td>
                                <td>$495.00 or $595.00.</td>
                              </tr>

                              <tr>
                                <td>Risk Management Fee</td>
                                <td>
                                  Agents who represent both parties to a single
                                  real estate <br />
                                  transaction in a dual agency scenario pay an
                                  additional
                                  <br />
                                  $350 Risk Management Fee in addition to the
                                  transaction fee <br />
                                  due on each side.
                                </td>
                                <td>$350</td>
                              </tr>

                              <tr>
                                <td>
                                  Purchasing or Selling Personal Real Estate
                                </td>
                                <td>
                                  Agents pay a $350 Risk Management Fee when
                                  buying or selling <br />
                                  personal real estate property where the agent
                                  represents <br />
                                  him/herself. This includes all real estate
                                  property(ies)
                                  <br />
                                  where an agent has potential personal
                                  financial gain.
                                </td>
                                <td>$350</td>
                              </tr>

                              <tr>
                                <td>Lead program</td>
                                <td>
                                  Anyone choosing to participate in our lead
                                  program with <br /> Opacity/Realtor.com or any
                                  of our other lead programs agrees <br />
                                  to pay the In Depth Realty or the lead
                                  provider a referral <br />
                                  fee of 50% for any lead that closes. In
                                  addition to paying <br />
                                  the transaction fees owed to In Depth Realty
                                </td>
                              </tr>
                            </tbody>
                      
                      </table>
                      </div>
                    </div>
                  </div>
                )}

                {activeTab === "earnings" && (
                  <div className="row p-3">
                    <div className="col-md-12">
                      <h4 className="mb-2">1099 Earnings</h4>
                      <p>This information is not an official 1099 form. It is intended to provide a summary of your 1099-NEC non-employee compensation payments for the tax year for your convenience.   An official 1099-NEC form will be issued after your income has been reported to the IRS, following January 31st of each year.</p>
                      <div className="row">
                        {/* Payer's Info Box */}
                        <div className="col-md-4 mb-3">
                          <div className="card p-3 text-center">
                            <h5 className="card-title">Payer's Info</h5>
                            <p>
                              <strong>In Depth Realty</strong>
                              {/* <strong>Agent Name:</strong> {profile.firstName}{" "}
                              {profile.lastName} */}
                            </p>
                          </div>
                        </div>
                        {/* Address Box */}
                        <div className="col-md-4 mb-3">
                          <div className="card p-3 text-center">
                            <h5 className="card-title">Address</h5>
                            <p>
                            {platformData.addresline1??""}{platformData.addresline2??""}{platformData.addresline3?? ""}
                            </p>
                          </div>
                        </div>
                        {/* Nonemployee Compensation Box */}
                        <div className="col-md-4 mb-3">
                          <NavLink
                            to={`/bussiness-income`}
                            className="card p-3 text-center"
                          >
                            <h5 className="card-title">
                            Non- Employee Compensation
                            </h5>
                            <p>
                              <b>YTD Gross Income</b> <br />$
                              {statsReport.totalEstimateFee
                                ? statsReport.totalEstimateFee.toLocaleString(
                                    "en-US"
                                  )
                                : "0"}
                            </p>
                          </NavLink>
                        </div>
                      </div>
                    </div>
                  </div>
                )}

                {activeTab === "expenses" && (
                  <div className="">
                      <h4 className="mb-4">Expense</h4>
                        <div className="col-md-4 icon-box card card-body h-100 border-0 shadow-sm card-hover h-100 text-center">
                          <p>
                            <NavLink
                            style={{color:"black"}}
                            to={`/bussiness-expences`}>
                            <b>Expense Amount</b> <br />$
                            {statsReportExpences.totalsales
                              ? statsReportExpences.totalsales.toLocaleString(
                                  "en-US"
                                )
                              : "0"}
                            </NavLink>
                          </p>
                    </div>
                  </div>
                )}
              </div>
            </div>
          </div>
        </div>
      </main>
    </div>
  );
};

export default BussinessTab;

import React, { useState, useEffect } from "react";
import user_service from "../service/user_service";

import Loader from "../../Pages/Loader/Loader.js";
import Toaster from "../../Pages/Toaster/Toaster";
import jwt from "jwt-decode";
import { NavLink } from "react-router-dom";

const BussinessExpences = () => {
  const [loader, setLoader] = useState({ isActive: null });
  const [toaster, setToaster] = useState({
    types: null,
    isShow: null,
    toasterBody: "",
    message: "",
  });

  const [statsReportExpences, setStatsReportExpences] = useState("");
  const StatsReportExpences = async () => {
    let queryParams = `?page=1`;
    queryParams += `&agentId=${jwt(localStorage.getItem("auth")).id}`;
    await user_service
      .TransactionBussinessExpences(queryParams)
      .then((response) => {
        if (response) {
          setStatsReportExpences(response.data);
        }
      });
  };

  useEffect(() => {
    StatsReportExpences();
  }, []);

  return (
    <div className="bg-secondary float-left w-100 pt-4 mb-4">
      <Loader isActive={loader.isActive} />
      {toaster.isShow && (
        <Toaster
          types={toaster.types}
          isShow={toaster.isShow}
          toasterBody={toaster.toasterBody}
          message={toaster.message}
        />
      )}
      <main className="page-wrapper">
        <div className="">
          <div className="row">
            <div className="col-md-12">
              <div className="bg-light border rounded-3  mb-4 p-3">
                <div className="">
                  <h3 className="text-left mb-0 justify-content-start d-flex align-items-center">
                    Expense
                  </h3>
                </div>
              </div>
              <div className="bg-light p-3 border-0 rounded-3 shadow-sm float-start w-100">
                <div className="row">
                  <div className="table-responsive bg-light border rounded-3 p-3">
                    <table
                      id="DepositLedger"
                      className="table table-striped align-middle mb-0 w-100"
                      border="0"
                      cellspacing="0"
                      cellpadding="0"
                    >
                      <thead>
                        <tr>
                          <th>Transaction Address</th>
                          <th>Amount</th>
                        </tr>
                      </thead>
                      <tbody>
                        {Array.isArray(statsReportExpences?.transactionData) &&
                        statsReportExpences?.transactionData.length > 0 ? (
                          <>
                            {statsReportExpences.transactionData.map(
                              (transaction, index) =>
                                Array.isArray(transaction.transactiondata) &&
                                transaction.transactiondata.map((item, idx) => {
                                  // Find the corresponding property details for the current transaction item
                                  const matchingProperty =
                                    transaction.propertyDetails.find(
                                      (property) =>
                                        property._id.toString() ===
                                        item.propertyDetail?.toString()
                                    );

                                  // Find the matching expense details for the current transaction item
                                  const matchingExpenses =
                                    transaction.expencesDetails.filter(
                                      (expense) =>
                                        expense.transactionId === item._id
                                    );

                                  // Render the row only if there is a valid property and expense match
                                  if (
                                    matchingProperty &&
                                    matchingExpenses.length > 0
                                  ) {
                                    return (
                                      <tr key={`${index}-${idx}`}>
                                        {/* Render property details */}
                                        <td>
                                          <NavLink
                                            to={`/transaction-summary/${item._id}`}
                                          >
                                            {matchingProperty.streetAddress ||
                                              "No Address Provided"}
                                          </NavLink>
                                        </td>

                                        {/* Render expense details */}
                                        {matchingExpenses.map(
                                          (expenseItem, eIdx) => (
                                            <td key={eIdx}>
                                              <NavLink>
                                                {expenseItem.amountPaid || "0"}
                                              </NavLink>
                                            </td>
                                          )
                                        )}
                                      </tr>
                                    );
                                  }

                                  return null; // Return null if there is no match
                                })
                            )}
                            <tr>
                              <td>
                                <strong>Total Amount</strong>
                              </td>
                              <td>
                                <strong>
                                  {statsReportExpences.transactionData
                                    .reduce((acc, transaction) => {
                                      const total =
                                        transaction.expencesDetails?.reduce(
                                          (sum, expense) =>
                                            sum +
                                            (parseFloat(expense.amountPaid) ||
                                              0),
                                          0
                                        );
                                      return acc + total;
                                    }, 0)
                                    .toFixed(2)}
                                </strong>
                              </td>
                            </tr>
                          </>
                        ) : (
                          <tr>
                            <td colSpan="2">
                              No Data Found
                            </td>
                          </tr>
                        )}
                      </tbody>
                    </table>
                    <div>
                        <NavLink to={`/bussiness-tab`} className="btn btn-secondary mt-3 pull-right">Back</NavLink>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </main>
    </div>
  );
};

export default BussinessExpences;

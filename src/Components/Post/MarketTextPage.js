import React, { useState, useEffect } from "react";
import { Calendar as BigCalendar, momentLocalizer } from "react-big-calendar";
import moment from "moment-timezone";
import "react-big-calendar/lib/css/react-big-calendar.css";
import Loader from "../../Pages/Loader/Loader.js";
import Toaster from "../../Pages/Toaster/Toaster.js";
import { NavLink, useNavigate } from "react-router-dom";
import user_service from "../service/user_service";
import jwt from "jwt-decode";
import _ from "lodash";
import axios from "axios";

const localizer = momentLocalizer(moment);

const eventColors = {
  meeting: "#FF5733",
  duty: "#3366FF",
};

const MarketTextPage = () => {
  const [loader, setLoader] = useState({ isActive: null });
  const { isActive } = loader;
  const [toaster, setToaster] = useState({
    types: null,
    isShow: null,
    toasterBody: "",
    message: "",
  });
  const { types, isShow, toasterBody, message } = toaster;
  return (
    <div className="bg-secondary float-left w-100 pt-4 mb-4">
      <Loader isActive={isActive} />
      {isShow && (
        <Toaster
          types={types}
          isShow={isShow}
          toasterBody={toasterBody}
          message={message}
        />
      )}
      <main className="page-wrapper">
        <div className="row event-calender">
          <div className="col-md-12">
            <h3 className="text-white mb-4">Marketing Calendar</h3>
            <div className="bg-light border rounded-3 p-3">
              <div className="row mt-2 p-4 ms-3">
                <p className="mt-3">
                  The key to a successful real estate social media calendar is
                  to map out your content in advance. Plan real estate posts
                  around key dates, holidays, events, and trends in the real
                  estate market. Share market updates to keep your audience
                  informed and engaged.
                </p>
                <h6>Posting Frequency</h6>
                <p>
                  Decide how often you’ll post. Generally speaking, the more you
                  post, the higher the chances of growing as a real estate
                  agent. Still, it’s important to stick to a consistent posting
                  schedule that works for you, even if you don’t have much time
                  to post. Consistency is vital whether you want to post thrice
                  or seven times a week. However, don’t forget that quality
                  trumps quantity.
                  <br />
                  Focus on creating great ideas that resonate with your
                  audience. Don’t commit to an unrealistic posting calendar.
                </p>

                <h6>Monitor and Adjust</h6>
                <p>
                  Constantly monitor your social media performance. Use free
                  analytics tools like the ones integrated with your social
                  platforms. Monitor which posts perform best. Are open houses
                  bringing in the most engagement, or do people react well to
                  “just listed” posts? Take a step back and assess whether your
                  social media content is performing as intended. Approach it
                  with a marketer’s mindset.
                </p>
                <h6>Use AI When It Makes Sense</h6>
                <p>
                  Consider using AI chatbots to engage with your audience 24/7.
                  Allow them to answer common questions and provide immediate
                  assistance. During this season, you can also use AI to
                  brainstorm digital marketing campaign ideas during busy times.
                  Use it to generate holiday marketing content or Black Friday
                  ideas.
                </p>
                <h4>10 Creative Real Estate Calendar Ideas</h4>
                <h6>Property Spotlight Mondays</h6>
                <p>
                  Start the week by showcasing a different property every
                  Monday. Share stunning photos, videos, and compelling
                  descriptions of new homes on the market. Highlight unique
                  features and potential benefits for buyers.
                </p>
                <h6>Testimonial Tuesdays</h6>
                <p>
                  Share success stories from satisfied past clients. Feature
                  their testimonials, along with before-and-after photos, if you
                  can.
                  <br />
                  This builds trust and demonstrates your track record, showing
                  people why they should choose you over other agents.
                </p>
                <h6>Tips and Tricks Wednesdays</h6>
                <p>
                  Share valuable tips and insights related to the real estate
                  industry. Offer advice on:
                  <div>
                    <li>
                      <b>Buying</b>
                    </li>
                    <li>
                      <b>Selling</b>
                    </li>
                    <li>
                      <b>Investing</b>
                    </li>
                    <li>
                      <b>Home maintenance</b>
                    </li>
                  </div>
                  This will help you position yourself as a knowledgeable
                  industry expert and build trust with your audience.
                </p>

                <h6>Throwback Thursdays</h6>
                <p>
                  Everyone loves a TBT. Take a trip down memory lane and post
                  about a significant milestone or achievement in your real
                  estate career. Share stories of challenges overcome and
                  lessons learned. It’s also a chance to inspire others and
                  become a tho<b>ught leader in the real estate industry</b>
                </p>
                <h6>Fun Facts Fridays</h6>
                <p>
                  Share interesting and lesser-known facts about the local real
                  estate market or your community. This can be about historical
                  landmarks, local events, or intriguing statistics. Use
                  information from your blog to create these social media posts,
                  as it saves time.
                </p>
                <h6>Small Business Saturday</h6>
                <p>
                  Small Business Saturday is the perfect opportunity to show
                  your support for local businesses and create engaging content.
                  Feature local businesses in your community and improve your
                  real estate farming efforts. Share their stories, highlight
                  unique offerings, and encourage your audience to shop locally.
                </p>
                <h6>Market Update Mondays</h6>
                <p>
                  Create short video tours of the properties you’re
                  representing. Videos perform best on social media, offering
                  the highest ROI from all other social media content. Thus,
                  make sure to use this content format more often than just once
                  a week. Although more difficult to create, it’s shareable,
                  clickable, and overall engaging. Highlight key features, the
                  neighborhood, and any recent renovations or upgrades.
                </p>
                <h6>Ask Me Anything (AMA) Wednesdays</h6>
                <p>
                  Encourage your audience to ask real estate-related questions.
                  We suggest using Instagram stories for this particular type of
                  content. Stories are personal and prompt immediate engagement,
                  as they disappear after 24 hours. Use them to answer your
                  audience’s questions and keep them on their toes each
                  Wednesday, awaiting your branded stories.
                </p>
                <h6>Weekend Wanderlust: Virtual Door Knocking</h6>
                <p>
                  On weekends, share inspiring photos or stories of beautiful
                  homes and dream destinations. Make your social media page feel
                  like a virtual getaway for your followers. Open up discussions
                  about top destinations, must-visit places in your state, “Best
                  Spots To Visit in California,” or dream homes. Connect the
                  idea of homeownership with a sense of adventure and
                  aspiration. Appealing to emotion is the real deal on social
                  media, so make sure to share popular images or custom content
                  that resonates well with their pathos.
                </p>
              </div>
            </div>
            <div className="pull-right mt-3">
              <NavLink to="/market-calendar" className="btn btn-primary">
                Back
              </NavLink>
            </div>
          </div>
        </div>
      </main>
    </div>
  );
};

export default MarketTextPage;

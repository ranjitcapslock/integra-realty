import React, { useState, useRef, useEffect } from "react";
import Loader from "../../Pages/Loader/Loader.js";
import Toaster from "../../Pages/Toaster/Toaster.js";
import { Editor } from "react-draft-wysiwyg";
import { EditorState, ContentState, convertToRaw } from "draft-js";
import draftToHtml from "draftjs-to-html";
import htmlToDraft from "html-to-draftjs";
import "react-draft-wysiwyg/dist/react-draft-wysiwyg.css";

import { NavLink, useNavigate, useParams } from "react-router-dom";
import jwt from "jwt-decode";
import _ from "lodash";
import moment from "moment-timezone";
import user_service from "../service/user_service";

const UpdateEvent = ({ initialContent }) => {
  const navigate = useNavigate();

  const [loader, setLoader] = useState({ isActive: null });
  const { isActive } = loader;
  const [toaster, setToaster] = useState({
    types: null,
    isShow: null,
    toasterBody: "",
    message: "",
  });
  const { types, isShow, toasterBody, message } = toaster;
  const params = useParams();

  const initialValues = {
    allDayEvent: "yes",
    category: "Office",
    draft: "yes",
    endDate: moment.tz("America/Denver").format("M/D/YYYY"),
    guestSeats: "no",
    information: "no",
    publicEvent: "yes",
    registration: "no",
    startDate: moment.tz("America/Denver").format("M/D/YYYY"),
    title: "",
    venue: "",
    infoUrl: "",
    organizer: "Integra Reality",
    repeatingEvent: "yes",
    anotherEvent: "yes",
    eventDescription: initialContent || "event -add",
    // eventDescription: "event -add",
    startTime: "9:00 AM",
    endTime: "10:00 AM",
    publicationLevel: "",
    maxSeats: "Agent +1",
    seatCount: "24",
    question: "50 words add",
  };
  const [formValues, setFormValues] = useState(initialValues);
  const [formErrors, setFormErrors] = useState({});
  const [isSubmitClick, setISSubmitClick] = useState(false);
  const [checkAll, setCheckAll] = useState("yes");
  const [checkRegistration, setCheckRegistration] = useState("yes");
  const [checkGuest, setGuest] = useState("no");
  const [checkInformation, setInformation] = useState("no");
  const [checkRepeting, setCheckRepeting] = useState("yes");

  const [organizationGet, setOrganizationGet] = useState([]);

  useEffect(() => {
    //window.scrollTo(0, 0);
    if (formValues && isSubmitClick) {
      validate();
    }
  }, [formValues]);

  const OrganizationTreeGets = async () => {
    // setLoader({ isActive: true });
    if (jwt(localStorage.getItem("auth")).id) {
      await user_service.organizationTreeGet().then((response) => {
        // setLoader({ isActive: false });
        if (response) {
          setOrganizationGet(response.data);
        }
      });
    }
  };

  const CalenderId = async () => {
    try {
      const response = await user_service.CalenderGetById(params.eventId);
      setLoader({ isActive: false });

      if (response && response.data) {
        console.log(response);
        setFormValues(response.data);

        const eventDescription = response.data.eventDescription;

        if (eventDescription) {
          const contentBlock = htmlToDraft(eventDescription);

          if (contentBlock) {
            const contentState = ContentState.createFromBlockArray(
              contentBlock.contentBlocks
            );
            setEditorState(EditorState.createWithContent(contentState));
          }
        } else {
          console.warn("eventDescription is undefined or null.");
        }
      } else {
        console.warn("No data found in response.");
      }
    } catch (error) {
      console.error("An error occurred while fetching the event data:", error);
      setLoader({ isActive: false });
    }
  };

  useEffect(() => {
    OrganizationTreeGets();
    CalenderId(params.id);
  }, []);

  const handleChange = (e) => {
    const { name, value } = e.target;
    setFormValues({ ...formValues, [name]: value });
  };

  const [editorState, setEditorState] = useState(EditorState.createEmpty());

  const handleChanges = (editorState) => {
    setEditorState(editorState);
    const htmlContent = draftToHtml(
      convertToRaw(editorState.getCurrentContent())
    );
    setFormValues({ ...formValues, eventDescription: htmlContent });
  };

  const validate = () => {
    const values = formValues;
    const errors = {};
    if (!values.title) {
      errors.title = "title is required";
    }
    if (!values.startDate) {
      errors.startDate = "startDate is required";
    }

    if (!values.endDate) {
      errors.endDate = "endDate is required";
    }

    if (!values.publicationLevel) {
      errors.publicationLevel = "Publication Level is required";
    }

    setFormErrors(errors);
    return errors;
  };


  const CheckBox = (event) => {
    const select = event.target.value;
    setFormValues({
      ...formValues,
      allDayEvent: select,
    });
  };
 

  const CheckRepetingEvent = (e) => {
    setCheckRepeting(e.target.name);
    setCheckRepeting(e.target.value);
  };

  // {/ <!-- Form onSubmit Start--> / }

  const handleSubmit = async (e) => {
    if (params.eventId) {
      e.preventDefault();
      setISSubmitClick(true);
      let checkValue = validate();
      if (_.isEmpty(checkValue)) {
        let publicationLevelname = "Corporate";
        if (formValues.publicationLevel) {
          const organization = organizationGet.data.find(
            (item) => item._id === formValues.publicationLevel
          );
          if (organization && organization.name) {
            publicationLevelname = organization.name;
          }
        }

        const userData = {
          category: formValues.category,
          publicationLevel: publicationLevelname,
          office_id: formValues.office_id ? formValues.office_id : "Corporate",
          office_name: publicationLevelname,
          allDayEvent: checkAll,
          title: formValues.title,
          information: checkInformation,
          registration: checkRegistration,
          venue: formValues.venue,
          organizer: formValues.organizer,
          draft: formValues.draft,
          agentId: jwt(localStorage.getItem("auth")).id,
          startDate: formValues.startDate,
          endDate: formValues.endDate,
          guestSeats: checkGuest,
          publicEvent: formValues.publicEvent,
          repeatingEvent: formValues.repeatingEvent,
          anotherEvent: formValues.anotherEvent,
          eventDescription: formValues.eventDescription,
          startTime: formValues.startTime ?? "9:00 AM",
          endTime: formValues.endTime ?? "10:00 AM",
          infoUrl: formValues.infoUrl,
          maxSeats: formValues.maxSeats,
          seatCount: formValues.seatCount?? "24",
          question: formValues.question,
        };

        try {
          setLoader({ isActive: true });
          const response = await user_service.CalenderUpdate(
            params.eventId,
            userData
          );
          setLoader({ isActive: false });
          if (response) {
            console.log(response.data);
            setLoader({ isActive: false });
            setToaster({
              types: "Update Event",
              isShow: true,
              toasterBody: response.data.message,
              message: "Event Update Successfully",
            });
            setTimeout(() => {
              navigate(`/event-detail/${response.data._id}`);
            }, 1000);
          } else {
            setLoader({ isActive: false });
            setToaster({
              types: "error",
              isShow: true,
              toasterBody: response.data.message,
              message: "Error",
            });
          }
        } catch (error) {
          setLoader({ isActive: false });
          setToaster({
            types: "error",
            isShow: true,
            toasterBody: error,
            message: "Error",
          });
        }
      }
    }
  };


  const CheckRegistration = (event) => {
    const select = event.target.value;
    setFormValues({
      ...formValues,
      registration: select,
    });
  };

 
  return (
    <div className="bg-secondary float-left w-100 pt-4 mb-4">
      <Loader isActive={isActive} />
      {isShow && (
        <Toaster
          types={types}
          isShow={isShow}
          toasterBody={toasterBody}
          message={message}
        />
      )}
      <main className="page-wrapper getContact_page_wrap">
        {/* <!-- Page container--> */}
        <div className="">
          <div className="d-flex align-items-center justify-content-between float-left mb-4">
            <h3 className="text-white mb-0">Update Event</h3>
          </div>
          <div className="row">
            <div className="col-sm-8">
              <div className="bg-light border rounded-3 p-3">
                <div className="row">
                  <div className="col-sm-6 mb-3">
                    <label className="form-label" for="pr-city">
                      Category
                    </label>
                    <select
                      className="form-select"
                      id="pr-city"
                      name="category"
                      onChange={handleChange}
                      value={formValues.category}
                    >
                      <option>Office</option>
                      <option>Personal</option>
                      <option>Staff</option>
                      <option>Training</option>
                      <option>Community</option>
                      <option>Courses</option>
                    </select>
                    {/* <div className="invalid-tooltip">{formErrors.category}</div> */}
                  </div>

                  <div className="col-sm-6 mb-4">
                    <label className="form-label" for="pr-city">
                      Publication Level
                    </label>
                    <select
                      className="form-select "
                      id="pr-city"
                      name="publicationLevel"
                      onChange={handleChange}
                      value={formValues.publicationLevel}
                      style={{
                        border: formErrors?.publicationLevel
                          ? "1px solid red"
                          : "1px solid #00000026",
                      }}
                    >
                      <option>choose a publicationLevel</option>
                      {organizationGet.data
                        ? organizationGet.data.map((item) => (
                            <option value={item._id}>{item.name}</option>
                          ))
                        : ""}
                    </select>
                    {formErrors.publicationLevel && (
                      <div className="invalid-tooltip">
                        {formErrors.publicationLevel}
                      </div>
                    )}
                  </div>

                  <div className="col-sm-12 mb-3">
                    <label className="form-label" for="pr-city">
                      Title<span className="text-danger">*</span>
                    </label>
                    <input
                      className="form-control"
                      id="inline-form-input"
                      type="text"
                      placeholder="title"
                      name="title"
                      onChange={handleChange}
                      value={formValues.title}
                      style={{
                        border: formErrors?.title ? "1px solid red" : "",
                      }}
                    />
                    <div className="invalid-tooltip">{formErrors.title}</div>
                  </div>
                  <div className="col-sm-6 mb-3">
                    <label className="form-label" for="pr-birth-date">
                      Start Date
                    </label>
                    <input
                      className="form-control"
                      type="date"
                      id="inline-form-input"
                      placeholder="Choose date"
                      name="startDate"
                      onChange={handleChange}
                      value={formValues.startDate}
                      style={{
                        border: formErrors?.startDate ? "1px solid red" : "",
                      }}
                      data-datepicker-options='{"altInput": true, "altFormat": "F j, Y", "dateFormat": "Y-m-d"}'
                    />
                    <div className="invalid-tooltip">
                      {formErrors.startDate}
                    </div>
                  </div>
                  <div className="col-sm-6 mb-3">
                    <label className="form-label" for="pr-birth-date">
                      End Date
                    </label>
                    <input
                      className="form-control"
                      type="date"
                      id="inline-form-input"
                      placeholder="Choose date"
                      name="endDate"
                      onChange={handleChange}
                      value={formValues.endDate}
                      style={{
                        border: formErrors?.endDate ? "1px solid red" : "",
                      }}
                      data-datepicker-options='{"altInput": true, "altFormat": "F j, Y", "dateFormat": "Y-m-d"}'
                    />
                    <div className="invalid-tooltip">{formErrors.endDate}</div>
                  </div>

                  <div className="col-sm-12 mb-3">
                    <h6>All Day Event</h6>
                    <div className="d-flex">
                      <div className="form-check ps-3">
                        <input
                          className="form-check-input"
                          id="form-radio-4"
                          type="radio"
                          name="allDayEvent"
                          value="yes"
                          onChange={CheckBox}
                          checked={
                            formValues?.allDayEvent ===
                              "yes" ||
                            !formValues.allDayEvent
                          }
                        />
                        <label
                          className="form-check-label ms-2"
                          id="radio-level1"
                        >
                          Yes
                        </label>
                      </div>
                      <div className="form-check">
                        <input
                          className="form-check-input"
                          id="form-radio-4"
                          type="radio"
                          name="allDayEvent"
                          value="no"
                          onChange={CheckBox}
                          checked={
                            formValues?.allDayEvent === "no"
                          }
                        />
                        <label
                          className="form-check-label ms-2"
                          id="radio-level1"
                        >
                          No
                        </label>
                      </div>
                    </div>
                  </div>

                  {checkAll === "yes" ? (
                    <>
                      <div className="col-sm-6 mb-3">
                        <label className="form-label" for="pr-city">
                          Venue Name
                        </label>
                        <input
                          className="form-control"
                          id="inline-form-input"
                          type="text"
                          placeholder="venue"
                          name="venue"
                          onChange={handleChange}
                          value={formValues.venue}
                          style={{
                            border: formErrors?.venue ? "1px solid red" : "",
                          }}
                        />
                        <div className="invalid-tooltip">
                          {formErrors.venue}
                        </div>
                      </div>

                      <div className="col-sm-6 mb-3">
                        <label className="form-label" for="pr-city">
                          Organizer
                        </label>
                        <input
                          className="form-control"
                          id="inline-form-input"
                          type="text"
                          placeholder="organizer"
                          name="organizer"
                          onChange={handleChange}
                          value={formValues.organizer}
                          style={{
                            border: formErrors?.organizer
                              ? "1px solid red"
                              : "",
                          }}
                        />
                        <div className="invalid-tooltip">
                          {formErrors.organizer}
                        </div>
                      </div>
                    </>
                  ) : (
                    <>
                      <div className="col-sm-6 mb-3">
                        <label className="form-label" for="pr-birth-date">
                          Start Time
                        </label>
                        <select
                          className="form-select"
                          id="pr-city"
                          name="startTime"
                          value={formValues.startTime}
                          onChange={handleChange}
                        >
                          <option></option>
                          <option>12:00 AM</option>
                          <option>12:15 AM</option>
                          <option>12:30 AM</option>
                          <option>12:45 AM</option>
                          <option>1:00 AM</option>
                          <option>1:15 AM</option>
                          <option>1:30 AM</option>
                          <option>1:45 AM</option>
                          <option>2:00 AM</option>
                          <option>2:15 AM</option>
                          <option>2:30 AM</option>
                          <option>2:45 AM</option>
                          <option>3:00 AM</option>
                          <option>3:15 AM</option>
                          <option>3:30 AM</option>
                          <option>3:45 AM</option>
                          <option>4:00 AM</option>
                          <option>4:15 AM</option>
                          <option>4:30 AM</option>
                          <option>4:45 AM</option>
                          <option>5:00 AM</option>
                          <option>5:15 AM</option>
                          <option>5:30 AM</option>
                          <option>5:45 AM</option>
                          <option>6:00 AM</option>
                          <option>6:15 AM</option>
                          <option>6:30 AM</option>
                          <option>6:45 AM</option>
                          <option>7:00 AM</option>
                          <option>7:15 AM</option>
                          <option>7:30 AM</option>
                          <option>7:45 AM</option>
                          <option>8:00 AM</option>
                          <option>8:15 AM</option>
                          <option>8:30 AM</option>
                          <option>8:45 AM</option>
                          <option>9:00 AM</option>
                          <option>9:15 AM</option>
                          <option>9:30 AM</option>
                          <option>9:45 AM</option>
                          <option>10:00 AM</option>
                          <option>10:15 AM</option>
                          <option>10:30 AM</option>
                          <option>10:45 AM</option>
                          <option>11:00 AM</option>
                          <option>11:15 AM</option>
                          <option>11:30 AM</option>
                          <option>11:45 AM</option>
                          <option>12:00 PM</option>
                          <option>12:15 PM</option>
                          <option>12:30 PM</option>
                          <option>12:45 PM</option>
                          <option>1:00 PM</option>
                          <option>1:15 PM</option>
                          <option>1:30 PM</option>
                          <option>1:45 PM</option>
                          <option>2:00 PM</option>
                          <option>2:15 PM</option>
                          <option>2:30 PM</option>
                          <option>2:45 PM</option>
                          <option>3:00 PM</option>
                          <option>3:15 PM</option>
                          <option>3:30 PM</option>
                          <option>3:45 PM</option>
                          <option>4:00 PM</option>
                          <option>4:15 PM</option>
                          <option>4:30 PM</option>
                          <option>4:45 PM</option>
                          <option>5:00 PM</option>
                          <option>5:15 PM</option>
                          <option>5:30 PM</option>
                          <option>5:45 PM</option>
                          <option>6:00 PM</option>
                          <option>6:15 PM</option>
                          <option>6:30 PM</option>
                          <option>6:45 PM</option>
                          <option>7:00 PM</option>
                          <option>7:15 PM</option>
                          <option>7:30 PM</option>
                          <option>7:45 PM</option>
                          <option>8:00 PM</option>
                          <option>8:15 PM</option>
                          <option>8:30 PM</option>
                          <option>8:45 PM</option>
                          <option>9:00 PM</option>
                          <option>9:15 PM</option>
                          <option>9:30 PM</option>
                          <option>9:45 PM</option>
                          <option>10:00 PM</option>
                          <option>10:15 PM</option>
                          <option>10:30 PM</option>
                          <option>10:45 PM</option>
                          <option>11:00 PM</option>
                          <option>11:15 PM</option>
                          <option>11:30 PM</option>
                          <option>11:45 PM</option>
                        </select>
                      </div>

                      <div className="col-sm-6 mb-3">
                        <label className="form-label" for="pr-birth-date">
                          End Time<span className="text-danger">*</span>
                        </label>
                        <select
                          className="form-select"
                          id="pr-city"
                          name="endTime"
                          value={formValues.endTime}
                          onChange={handleChange}
                        >
                          <option></option>
                          <option>12:00 AM</option>
                          <option>12:15 AM</option>
                          <option>12:30 AM</option>
                          <option>12:45 AM</option>
                          <option>1:00 AM</option>
                          <option>1:15 AM</option>
                          <option>1:30 AM</option>
                          <option>1:45 AM</option>
                          <option>2:00 AM</option>
                          <option>2:15 AM</option>
                          <option>2:30 AM</option>
                          <option>2:45 AM</option>
                          <option>3:00 AM</option>
                          <option>3:15 AM</option>
                          <option>3:30 AM</option>
                          <option>3:45 AM</option>
                          <option>4:00 AM</option>
                          <option>4:15 AM</option>
                          <option>4:30 AM</option>
                          <option>4:45 AM</option>
                          <option>5:00 AM</option>
                          <option>5:15 AM</option>
                          <option>5:30 AM</option>
                          <option>5:45 AM</option>
                          <option>6:00 AM</option>
                          <option>6:15 AM</option>
                          <option>6:30 AM</option>
                          <option>6:45 AM</option>
                          <option>7:00 AM</option>
                          <option>7:15 AM</option>
                          <option>7:30 AM</option>
                          <option>7:45 AM</option>
                          <option>8:00 AM</option>
                          <option>8:15 AM</option>
                          <option>8:30 AM</option>
                          <option>8:45 AM</option>
                          <option>9:00 AM</option>
                          <option>9:15 AM</option>
                          <option>9:30 AM</option>
                          <option>9:45 AM</option>
                          <option>10:00 AM</option>
                          <option>10:15 AM</option>
                          <option>10:30 AM</option>
                          <option>10:45 AM</option>
                          <option>11:00 AM</option>
                          <option>11:15 AM</option>
                          <option>11:30 AM</option>
                          <option>11:45 AM</option>
                          <option>12:00 PM</option>
                          <option>12:15 PM</option>
                          <option>12:30 PM</option>
                          <option>12:45 PM</option>
                          <option>1:00 PM</option>
                          <option>1:15 PM</option>
                          <option>1:30 PM</option>
                          <option>1:45 PM</option>
                          <option>2:00 PM</option>
                          <option>2:15 PM</option>
                          <option>2:30 PM</option>
                          <option>2:45 PM</option>
                          <option>3:00 PM</option>
                          <option>3:15 PM</option>
                          <option>3:30 PM</option>
                          <option>3:45 PM</option>
                          <option>4:00 PM</option>
                          <option>4:15 PM</option>
                          <option>4:30 PM</option>
                          <option>4:45 PM</option>
                          <option>5:00 PM</option>
                          <option>5:15 PM</option>
                          <option>5:30 PM</option>
                          <option>5:45 PM</option>
                          <option>6:00 PM</option>
                          <option>6:15 PM</option>
                          <option>6:30 PM</option>
                          <option>6:45 PM</option>
                          <option>7:00 PM</option>
                          <option>7:15 PM</option>
                          <option>7:30 PM</option>
                          <option>7:45 PM</option>
                          <option>8:00 PM</option>
                          <option>8:15 PM</option>
                          <option>8:30 PM</option>
                          <option>8:45 PM</option>
                          <option>9:00 PM</option>
                          <option>9:15 PM</option>
                          <option>9:30 PM</option>
                          <option>9:45 PM</option>
                          <option>10:00 PM</option>
                          <option>10:15 PM</option>
                          <option>10:30 PM</option>
                          <option>10:45 PM</option>
                          <option>11:00 PM</option>
                          <option>11:15 PM</option>
                          <option>11:30 PM</option>
                          <option>11:45 PM</option>
                        </select>
                      </div>
                    </>
                  )}
                  <div className="col-sm-12 mb-3">
                    <div className="row">
                      <div className="col-md-12">
                        <label for="text-input" className="form-label">
                          More Info URL
                        </label>
                      </div>
                      <div className="col-md-6 col-md-6 col-sm-6 col-8">
                        <input
                          className="form-control"
                          type="text"
                          id="text-input"
                          name="infoUrl"
                          value={formValues.infoUrl}
                          onChange={handleChange}
                        />
                      </div>
                      <div className="col-md-6 col-md-6 col-sm-4 col-4">
                        <a
                          href={formValues.infoUrl}
                          target="_blank"
                          rel="noopener noreferrer"
                          className="btn btn-primary btn-large"
                        >
                          Test Link
                        </a>
                      </div>
                    </div>
                  </div>

                  <div
                      className="col-sm-6 mb-4">
                      <h6>Allow registration?</h6>
                      <div className="d-flex">
                        <div className="form-check ps-3">
                          <input
                            className="form-check-input"
                            id="form-radio-4"
                            type="radio"
                            name="registration"
                            onChange={CheckRegistration}
                            value="no"
                            checked={
                              formValues?.registration ===
                                "no" 
                            }
                          />
                          <label
                            className="form-check-label ms-2"
                            id="radio-level1"
                          >
                            No
                          </label>
                        </div>

                        <div className="form-check">
                          <input
                            className="form-check-input "
                            id="form-radio-4"
                            type="radio"
                            name="registration"
                            value="yes"
                            onChange={CheckRegistration}
                            checked={
                              formValues?.registration ===
                                "yes" ||
                              !formValues.registration
                            }
                          />
                          <label
                            className="form-check-label ms-2"
                            id="radio-level1"
                          >
                            Yes
                          </label>
                        </div>
                      </div>
                    </div>

                    {formValues.registration === "yes" ? (
                      <div className="col-sm-6 mb-4">
                        <label className="form-label">Seat Count</label>
                        <input
                          className="form-control"
                          id="inline-form-input"
                          type="text"
                          name="seatCount"
                          value={formValues.seatCount?? "24"}
                          onChange={handleChange}
                        />
                      </div>
                    ) : (
                      ""
                    )}


                  <div
                    className="col-sm-12 mb-3"
                    onClick={(e) => CheckRepetingEvent(e)}
                  >
                    <div className="form-check">
                      <input
                        className="form-check-input"
                        id="form-check-1"
                        type="checkbox"
                        name="repeatingEvent"
                        value="yes"
                        defaultChecked
                      />
                      <label className="form-check-label" for="form-check-1">
                        Once saved, show me options to copy this repeating
                        event. &nbsp;{" "}
                        <a href="#">
                          <i
                            className="fa fa-question-circle"
                            aria-hidden="true"
                          ></i>
                        </a>
                      </label>
                    </div>
                  </div>
                  <p>NOTE: Enter times local to your office (MDT)</p>
                  <h6>Event Description</h6>
                  <div className="event-description">
                    <Editor
                      editorState={editorState}
                      onEditorStateChange={handleChanges}
                      value={formValues.eventDescription}
                      toolbar={{
                        options: [
                          "inline",
                          "blockType",
                          "fontSize",
                          "list",
                          "textAlign",
                          "history",
                          "link", // Add link option here
                        ],
                        inline: {
                          options: [
                            "bold",
                            "italic",
                            "underline",
                            "strikethrough",
                          ],
                        },
                        list: { options: ["unordered", "ordered"] },
                        textAlign: {
                          options: ["left", "center", "right"],
                        },
                        history: { options: ["undo", "redo"] },
                        link: {
                          // Configure link options
                          options: ["link", "unlink"],
                        },
                      }}
                      wrapperClassName="demo-wrapper"
                      editorClassName="demo-editor"
                    />

                    {/* <Editor
                      onEditorChange={(e) => handleChanges(e)}
                     apiKey='gtzihoqhxnyb7apgj00x766eel9cqv0herq11druhd2j6hki'
                      onInit={(evt, editor) => (editorRef.current = editor)}
                      value={formValues.eventDescription}
                      initialValue=""
                      init={{
                        height: 500,
                        menubar: false,
                        plugins: [
                          "advlist autolink lists link image charmap print preview anchor",
                          "searchreplace visualblocks code fullscreen",
                          "insertdatetime media table paste code help wordcount",
                        ],
                        toolbar:
                          "undo redo | formatselect | " +
                          "bold italic backcolor | alignleft aligncenter " +
                          "alignright alignjustify | bullist numlist outdent indent | " +
                          "removeformat | help",
                        content_style:
                          "body { font-family:Helvetica,Arial,sans-serif; font-size:14px }",
                      }}
                    /> */}
                  </div>
                </div>
              </div>
              <div className="pull-right mt-3">
                <button
                  onClick={handleSubmit}
                  type="button"
                  className="btn btn-primary btn-sm"
                  id="save-button"
                >
                  Update Event
                </button>
                <NavLink
                  to="/calendar"
                  type="button"
                  className="btn btn-secondary btn-sm ms-3"
                  id="closeModal"
                  data-dismiss="modal"
                >
                  Cancel
                </NavLink>
              </div>
            </div>
            <div className="col-sm-4 mt-lg-0 mt-md-0 mt-sm-4 mt-4">
              <div id="accordionCards" className="">
                <div
                  className="card bg-secondary mb-2 mt-0"
                  data-bs-toggle="collapse"
                  data-bs-target="#cardCollapse1"
                >
                  <div className="card-body">
                    <h3 className="h3 card-title pt-1 mb-0">
                      💡 List of Resources
                    </h3>
                  </div>
                  <div
                    className="collapse show"
                    id="cardCollapse1"
                    data-bs-parent="#accordionCards"
                  >
                    <div className="card-body mt-n1 pt-0">
                      <p className="fs-sm">
                        <strong>
                          Conference Rooms are available Monday-Friday 8AM-5PM —
                          Corporate
                        </strong>
                        <br />
                        <br />
                        Conferance Rooms must be booked by emailing
                        admin@riverparksuites.com with the date , time and what
                        room you would like to use.
                      </p>
                      <p className="fs-sm">
                        <strong>Meeting room/ Day Office — Corporate</strong>{" "}
                        <br />
                        <br />
                        Setup includes a desk, 2 guest chairs, phone, and
                        internet connection. To book email:
                        admin@riverparksuites.com with the date, time and what
                        room you would like to use.
                      </p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </main>
    </div>
  );
};

export default UpdateEvent;

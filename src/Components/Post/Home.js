import React, { useState, useEffect } from 'react';
import data from "../../Pages/data.js"
import { NavLink, useLocation } from "react-router-dom";
import Associate from "../img/Associates.png";
import Community from "../img/Community.png";
import Officeposts from "../img/Officeposts.png";
import photo from "../img/photo.jpg";
import photo2 from "../img/photo2.jpg";
import photo1 from "../img/photo1.jpg";
import { PieChart } from 'react-minimal-pie-chart';
import { FiChevronRight, FiChevronLeft } from "react-icons/fi";
// import "../../style.css"
import { useNavigate } from "react-router-dom";
import user_service from '../service/user_service';
import Loader from '../../Pages/Loader/Loader.js';
import Toaster from '../../Pages/Toaster/Toaster';
import ReactPaginate from "react-paginate";
import { Calendar as BigCalendar, momentLocalizer } from 'react-big-calendar';
import moment from "moment-timezone";
import 'react-big-calendar/lib/css/react-big-calendar.css';

import Slider from "react-slick";
import "slick-carousel/slick/slick-theme.css";
import "slick-carousel/slick/slick.css";



const localizer = momentLocalizer(moment);

const eventColors = {
  meeting: '#FF5733',
  duty: '#3366FF',
};

const settings = {
  dots: true,
  infinite: true,
  speed: 500,
  slidesToShow: 1,
  slidesToScroll: 1,
  autoplay: true,
  autoplaySpeed: 7000,
};

function Home(props) {

  // const [peoples, setPeoples] = useState(data);
  const [peoples, setPeoples] = useState([]);
  const [index, setIndex] = useState(0);

  const [banners, setBanners] = useState("");

  const bannerGetAll = async () => {
    await user_service.bannerGetAll().then((response) => {
      if (response) {
        setBanners(response.data);
        setPeoples(response.data.data)
      }
    });
  };

  const CustomToolbar = () => {
    return (
      <div className="rbc-toolbar">
        <span className="rbc-toolbar-label"></span>
        <span className="rbc-btn-group">
          {/* <button type="button" className="rbc-active">Month</button> */}
          {/* <button type="button">Week</button> */}
          {/* <button type="button">Day</button> */}
          {/* <button type="button">Agenda</button> */}
        </span>
      </div>
    );
  };

  const [isShowSidebar, setIsShowSidebar] = useState(false);
  const { pathname } = useLocation();
  const [value, onChange] = useState(new Date());
  const navigate = useNavigate();

  const [loader, setLoader] = useState({ isActive: null })
  const { isActive } = loader;
  const [toaster, setToaster] = useState({ types: null, isShow: null, toasterBody: "", message: "" });
  const { types, isShow, toasterBody, message } = toaster;

  const [getPost, setGetPost] = useState([]);
  const [getIndustryPost, setGetIndustryPost] = useState([]);
  const [IndustrypageCount, setIndustryPageCount] = useState(0);
  const [IndustrytotalRecords, setIndustryTotalRecords] = useState("");

  const [getFilter, setGetFilter] = useState("");
  const [pageCount, setPageCount] = useState(0);
  const [totalRecords, setTotalRecords] = useState("");
  const [activeNavlink, setActiveNavlink] = useState("blend");
  const [events, setEvents] = useState([]);

  const [calenderGet, setCalenderGet] = useState([])


 
  const generateEvents = () => {
    const generatedEvents = [];

    if (calenderGet && calenderGet.data) {
      calenderGet.data.forEach((date) => {
        let startDate;
        let endDate;

        if (date.startDate.includes("/")) {
          startDate = moment.tz(date.startDate, "M/D/YYYY", "America/Denver").toDate();
          endDate = moment.tz(date.endDate, "M/D/YYYY", "America/Denver").toDate();
        } else {
          startDate = moment.tz(date.startDate, "America/Denver").toDate();
          endDate = moment.tz(date.endDate, "America/Denver").toDate();
        }

        const eventId = date._id;

        generatedEvents.push({
          title: date.title,
          start: startDate,
          end: endDate,
          type: "meeting",
          id: eventId,
        });
      });
    }
    return generatedEvents;
  };


  const fetchCalendar = async () => {
    setLoader({ isActive: true });
    await user_service.CalenderGet().then((response) => {
      setLoader({ isActive: false });
      if (response) {
        setCalenderGet(response.data);
      }
    });
  };

  useEffect(() => { window.scrollTo(0, 0);
    fetchCalendar();
    bannerGetAll();
  }, []);


  useEffect(() => { window.scrollTo(0, 0);
    const generatedEvents = generateEvents();
    // console.log(generatedEvents);
    setEvents(generatedEvents);
  }, [calenderGet]);



  const eventStyleGetter = (event) => {
    const backgroundColor = eventColors[event.type];
    return {
      style: {
        backgroundColor,
      },
    };
  };


  const [currentDate, setCurrentDate] = useState('');
  useEffect(() => { window.scrollTo(0, 0);
    const interval = setInterval(() => {
      // const options = { timeZone: 'Asia/Kolkata', dateStyle: 'full' };
      // setCurrentDate(new Date().toLocaleDateString('en-IN', options));
      setCurrentDate(new Date().toLocaleDateString());
    }, 1000);

    return () => {
      clearInterval(interval);
    };
  }, []);


  useEffect(() => { window.scrollTo(0, 0);
    if (pathname === "/signin" || pathname === "/signup") {
      setIsShowSidebar(false);
    } else {
      setIsShowSidebar(true);
    }
  }, [pathname]);




  // useEffect(() => { window.scrollTo(0, 0);
  //   const lastIndex = peoples.length - 1;
  //   if (index < 0) {
  //     setIndex(lastIndex);
  //   }
  //   if (index > lastIndex) {
  //     setIndex(0);
  //   }
  // }, [index, peoples]);

  // // autoslide, clearInterval = een cleanup functie noodzakelijk bij interval
  // useEffect(() => { window.scrollTo(0, 0);
  //   let slider = setInterval(() => {
  //     setIndex(index + 1);
  //   }, 3000);
  //   return () => clearInterval(slider);
  // }, [index]);


  const AddPost = () => {
    navigate("/new-post");
  }

  const PostDetail = (id) => {
   // console.log(id)
    navigate(`/post-detail/${id}`);
  }

  const PostDetailIndustry = (url) => {
    window.open(url, '_blank');
   }
  
  const CalenderPage = () => {
    navigate("/calendar");
  }

  const increaseclick = async (banner_id) => {
    await user_service.increaseclickbanner(banner_id).then((response) => {
      if (response) {

      }
    })
  }

  // paginate Function 
  const PostGetAll = async () => {
    setLoader({ isActive: true })
    await user_service.postGet(1).then((response) => {
      setLoader({ isActive: false })
      if (response) {
        setGetPost(response.data.data);
        setPageCount(Math.ceil(response.data.totalRecords / 5));
        setTotalRecords(response.data.totalRecords)
      }
    });
  }
  const IndustryPostGetAll = async () => {
    setLoader({ isActive: true })
    await user_service.IndustrypostGet(1).then((response) => {
      setLoader({ isActive: false })
      if (response) {
        setGetIndustryPost(response.data.data);
        setIndustryPageCount(Math.ceil(response.data.totalRecords / 5));
        setIndustryTotalRecords(response.data.totalRecords)
      }
    });
  }
  useEffect(() => { window.scrollTo(0, 0);
    PostGetAll();
    IndustryPostGetAll();
  }, [])




  const handlePageClick = async (data) => {
    let currentPage = data.selected + 1;
    let skip = (currentPage - 1) + currentPage;
    setLoader({ isActive: true })
    await user_service.postGet(currentPage, skip).then((response) => {
      setLoader({ isActive: false })
      if (response) {
        setGetPost(response.data.data);
        setPageCount(Math.ceil(response.data.totalRecords / 5));
      }
    });
  }

  const handleIndustryPageClick = async (data) => {
    let currentPage = data.selected + 1;
    let skip = (currentPage - 1) + currentPage;
    setLoader({ isActive: true })
    await user_service.IndustrypostGet(currentPage, skip).then((response) => {
      setLoader({ isActive: false })
      if (response) {
        setGetIndustryPost(response.data.data);
        setIndustryPageCount(Math.ceil(response.data.totalRecords / 5));
      }
    });
  }

  const CatagoryBlend = async (filter) => {
    setActiveNavlink(filter)
    if (filter === "blend") {
      await user_service.postGet(1).then((response) => {
        if (response) {
          setGetPost(response.data.data);
          setPageCount(Math.ceil(response.data.totalRecords / 5));
          setTotalRecords(response.data.totalRecords)
        }
      });
    }
    else {
      setLoader({ isActive: true })
      await user_service.postFilter(filter).then((response) => {
        setLoader({ isActive: false })
        if (response) {

          setGetPost(response.data.data);
        }
      });
    }

  }

  const propertypic = (e) => {
    e.target.src = "https://integra.workspace.lwolf.com/common/handler/mls/photo_custom.ashx?s=1&t=5eada3e3-491e-46e2-938c-7c785a1f9c68&r=Seller";
  }

  return (
    <div className="bg-secondary float-left w-100 pt-4">
      <Loader isActive={isActive} />
      {isShow && <Toaster types={types}
        isShow={isShow}
        toasterBody={toasterBody}
        message={message} />}
      <main className="page-wrapper">
        {/* <!-- Page container--> */}
        <div className="container">
          {/* <!-- Breadcrumb--> */}
          <nav className="mb-3 mb-md-4 pt-md-3" aria-label="Breadcrumb">
            <ol className="breadcrumb breadcrumb-light">
              <li className="breadcrumb-item"><a href="job-board-home-v1.html">Home</a></li>
              <li className="breadcrumb-item"><a href="job-board-account-profile.html">Account</a></li>
              <li className="breadcrumb-item active" aria-current="page">My Contact</li>
            </ol>
          </nav>

          {/* <!-- Page card like wrapper--> */}
          <div className="event-calender">
            <div className="row">
              {/* <!-- Content--> */}
              <div className="col-lg-8 col-md-7 mb-5">
                {/* {
                  peoples && peoples.length > 0 ?
                  <section className="section">
                    <div className="section-center">
                      {
                      peoples.map((person, personIndex) => {
                        if(person.marque_image){
                            console.log(person.marque_image)
                            console.log("person")
                            const { id, marque_image,url_link, name } = person;
                            let position = "nextSlide";
                            if (personIndex === index) {
                              position = "activeSlide";
                            }
                            if (
                              personIndex === index - 1 ||
                              (index === 0 && personIndex === peoples.length - 1)
                            ) {
                              position = "lastSlide";
                            }
                            return (
                              <article key={id} className={position}>
                                <a href={url_link} target="_blank"><img src={marque_image} alt={name} className="person-img" /></a>
                              </article>
                            );
                      }
                      })
                      }
                      <button className="prev" onClick={() => setIndex(index - 1)}>
                        <FiChevronLeft />
                      </button>
                      <button className="next" onClick={() => setIndex(index + 1)}>
                        <FiChevronRight />
                      </button>
                    </div>
                  </section>
                  :""
                } */}

                <Slider {...settings}>
                  {peoples && peoples.length > 0 && (
                    peoples.map((post) => (
                      post.isactive === true && post.marque_image ? (
                        <div key={post.title} className="carousel-item">
                          <a onClick={() => increaseclick(post._id)} href={post.url_link} target="_blank" className="sidenav-banner">
                            <span className="pull-left align-items-center justify-content-left d-flex w-100">
                              <img
                                alt=""
                                // className="w-100"
                                className="person-img w-100"
                                src={
                                  post.marque_image
                                    ? post.marque_image.includes('http')
                                      ? post.marque_image
                                      : post.marque_image
                                    : ""
                                }
                              ></img>
                            </span>
                          </a>
                        </div>
                      ) : null
                    ))
                  )}
                </Slider>

                {/* <!-- Banner ends here--> */}

                <div className="blogs mt-5">
                  <div className="collapse d-md-block" id="account-nav">
                    <ul className="nav nav-pills flex-column flex-md-row pt-3 pt-md-0 pb-md-4 border-bottom-md">
                      <li className="nav-item mb-md-0 me-md-2 pe-md-1" onClick={(e) => CatagoryBlend("blend")}>
                        <span className={activeNavlink === "blend" ? 'active btn btn-primary btn-sm ' : 'nav-link'}>The Blend</span>
                      </li>

                      <li className="nav-item mb-md-0 me-md-2 pe-md-1" onClick={(e) => CatagoryBlend('OfficePost')}>
                        <span className={activeNavlink === "OfficePost" ? 'active btn btn-primary btn-sm ' : 'nav-link'}>Office Posts</span>
                      </li>

                      <li className="nav-item mb-md-0 me-md-2 pe-md-1" onClick={(e) => CatagoryBlend('associate')}>
                        <span className={activeNavlink === "associate" ? 'active btn btn-primary btn-sm ' : 'nav-link'}>Associate Posts</span>
                      </li>

                      <li className="nav-item mb-md-0 me-md-2 pe-md-1" onClick={(e) => CatagoryBlend('Community')}>
                        <span className={activeNavlink === "Community" ? 'active btn btn-primary btn-sm ' : 'nav-link'}>Community Posts</span>
                      </li>

                      <li className="nav-item mb-md-0 me-md-2 pe-md-1 pull-right">
                        <button className="btn btn-primary btn-sm  ms-2 order-lg-3" onClick={AddPost}>Add a Post</button></li>
                    </ul>
                    <div className="p-2">Office Posts</div>
                  </div>

                  {
                    getPost.map((post) => {
                      return (
                        <div>

                          <blockquote className="blockquote blogs my-2">
                            <span className="post_type">{post.category}</span>
                            <div className="d-flex align-items-center" onClick={(e) => PostDetail(post._id)}>
                              <img className="rounded-circle me-1"
                                src={post.category === 'OfficePost' ? Officeposts : post.category === 'associate' ? Associate : post.category === 'Community' ? Community : ""}
                                width="48" alt="avtar" />
                              <div className="ps-2 pull-left w-100">
                                <span className="text-muted fw-normal fs-sm">{post.channel}</span>
                                <p className="fs-base mb-0">{post.postTitle.length > 55 ? post.postTitle.substring(0, 55) + '...' : post.postTitle}</p>
                                <div className="text-muted fw-normal fs-sm">{post.postBlurb.length > 100 ? post.postBlurb.substring(0, 100) + '...' : post.postBlurb}</div>
                              </div>
                              {post.file ?
                                <img className="postimage" src={post.file ? post.file : ""} />
                                :
                                <img className="postimage" src="https://integra.workspace.lwolf.com/common/handler/mls/photo_custom.ashx?s=1&t=5eada3e3-491e-46e2-938c-7c785a1f9c68&r=Seller" />
                              }
                            </div>
                            {
                              post.contact_details ?
                                <div className="d-flex align-items-center">
                                  <img className="" height="30" width="30 " src={post.contact_details.image} /> &nbsp;
                                  <div className="text-muted fw-normal fs-sm">{post.contact_details ? post.contact_details.firstName + ' ' + post.contact_details.lastName : ""}
                                  </div>
                                </div>
                                : ""
                            }

                          </blockquote>
                        </div>
                      )
                    })
                  }
                  {
                    totalRecords > 5 ?
                      <div className="justify-content-end mb-1">
                        <ReactPaginate
                          previousLabel={"Previous"}
                          nextLabel={"Next"}
                          breakLabel={"..."}
                          pageCount={pageCount}
                          marginPagesDisplayed={1}
                          pageRangeDisplayed={2}
                          onPageChange={handlePageClick}
                          containerClassName={"pagination justify-content-center"}
                          pageClassName={"page-item"}
                          pageLinkClassName={"page-link"}
                          previousClassName={"page-item"}
                          previousLinkClassName={"page-link"}
                          nextClassName={"page-item"}
                          nextLinkClassName={"page-link"}
                          breakClassName={"page-item"}
                          breakLinkClassName={"page-link"}
                          activeClassName={"active"}
                        />
                      </div>
                      : ""

                  }

                </div>

                {/* // industry posts */}

                { IndustrytotalRecords > 0 ?
                    <div className="blogs mt-5">
                      <div className="collapse d-md-block" id="account-nav1">
                        <div className="p-2">Industry Posts</div>
                      </div>

                      {
                        getIndustryPost.map((post) => {
                          return (
                            <div>

                              <blockquote className="blockquote blogs my-2">
                                <span className="post_type">
                                  {post?.categories && JSON.parse(post.categories).length > 0
                                    ? JSON.parse(post.categories)
                                        .slice(0, 2) // Get the first two categories
                                        .join(', ')
                                    : 'Industry Post'}
                                </span>

                                <div className="d-flex align-items-center" onClick={(e) => PostDetailIndustry(post.linkURL)}>
                                  {/* <img className="rounded-circle me-1"
                                    src={post.category === 'OfficePost' ? Officeposts : post.category === 'associate' ? Associate : post.category === 'Community' ? Community : ""}
                                    width="48" alt="avtar" /> */}
                                  <div className="ps-2 pull-left w-100">
                                    <p className="fs-base mb-0">{post?.postTitle?.length > 55 ? post?.postTitle.substring(0, 55) + '...' : post?.postTitle}</p>
                                    <div className="text-muted fw-normal fs-sm">{post?.shortSummary?.length > 100 ? post?.shortSummary.substring(0, 100) + '...' : post?.shortSummary}</div>
                                    {/* <div className="text-muted fw-normal fs-sm" dangerouslySetInnerHTML={{ __html: post?.fullMessage }} /> */}
                                  </div>
                                  {post.imageSrc ?
                                    <img className="postimage" src={post.imageSrc ? post.imageSrc : ""}  onError={e => propertypic(e)}/>
                                    :
                                    <img className="postimage" src="https://integra.workspace.lwolf.com/common/handler/mls/photo_custom.ashx?s=1&t=5eada3e3-491e-46e2-938c-7c785a1f9c68&r=Seller" />
                                  }
                                </div>
                                {
                                  post.author ?
                                    <div className="d-flex align-items-center">
                                      {/* <img className="" height="30" width="30 " src={post.contact_details.image} /> &nbsp; */}
                                      <div className="text-muted fw-normal fs-sm">{post?.author ? post?.author : ""}
                                      </div>
                                    </div>
                                    : ""
                                }
                              </blockquote>
                            </div>
                          )
                        })
                      }
                      {
                        IndustrytotalRecords > 5 ?
                          <div className="justify-content-end mb-1">
                            <ReactPaginate
                              previousLabel={"Previous"}
                              nextLabel={"Next"}
                              breakLabel={"..."}
                              pageCount={IndustrypageCount}
                              marginPagesDisplayed={1}
                              pageRangeDisplayed={2}
                              onPageChange={handleIndustryPageClick}
                              containerClassName={"pagination justify-content-center"}
                              pageClassName={"page-item"}
                              pageLinkClassName={"page-link"}
                              previousClassName={"page-item"}
                              previousLinkClassName={"page-link"}
                              nextClassName={"page-item"}
                              nextLinkClassName={"page-link"}
                              breakClassName={"page-item"}
                              breakLinkClassName={"page-link"}
                              activeClassName={"active"}
                            />
                          </div>
                          : ""
                       }

                    </div>
                  :""
                }

              </div>

              {/* <!-- Sidebar--> */}
              <aside className="col-lg-4 col-md-5 pe-xl-4 mb-5">
                <div onClick={CalenderPage} style={{ height: '400px' }}>
                  <div className="d-flex align-items-center justify-content-between pb-2">
                    <h6 className="mb-0">{currentDate}</h6>
                    <NavLink onClick={CalenderPage}>Calendar <i className="fa fa-angle-double-right" aria-hidden="true"></i></NavLink>        
                  </div>
                  <hr />

                  <div className="mt-2" style={{ height: '285px' }}>
                    <BigCalendar
                      localizer={localizer}
                      events={events}
                      startAccessor="start"
                      endAccessor="end"
                      views={{
                        month: true,
                        week: false,
                        day: false,
                        agenda: false,
                      }}
                      components={{
                        toolbar: CustomToolbar,
                      }}
                      eventPropGetter={eventStyleGetter}
                      style={{ flex: 1 }}
                    />
                  </div>
                  <br />
                </div>

                <iframe className="float-left w-100" width="560" height="315" src="https://www.youtube.com/embed/NCb8_JYpKB0"
                  title="YouTube video player"
                  allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share"
                >
                </iframe>
                <div className="featured_images my-3">
                  {/* <img className="p-1" src="https://cdn.pboffice.net/org/cache/image.ashx?/NORTHSTAR/Banner/Side/bfe8556e-228b-4e6d-ab3f-9c456e9d5290.jpg" />
                  <img className="p-1" src="https://cdn.pboffice.net/org/cache/image.ashx?/NORTHSTAR/Banner/Side/5c664a73-f222-4eb0-838a-5cc4fb85872c.jpg" /> */}
                  <Slider {...settings}>
                    {peoples && peoples.length > 0 && (
                      peoples.map((post) => (
                        post.isactive === true && post.side_image ? (
                          <div key={post.title} className="carousel-item">
                            <a href={post.url_link} target="_blank" className="sidenav-banner">
                              <span className="pull-left align-items-center justify-content-left d-flex w-100">
                                <img
                                  alt=""
                                  // className="w-100"
                                  className="p-1"
                                  src={
                                    post.side_image
                                      ? post.side_image.includes('http')
                                        ? post.side_image
                                        : post.side_image
                                      : ""
                                  }
                                ></img>
                              </span>
                            </a>
                          </div>
                        ) : null
                      ))
                    )}
                  </Slider>
                </div>

                <div className="access_activity">
                  <h5>Associate Access Activity</h5>
                  <PieChart
                    data={[
                      { title: 'One', value: 2, color: '#5A8E22' },
                      { title: 'Two', value: 23, color: '#606060' },
                      { title: 'Three', value: 41, color: '#A0A0A0' },
                      { title: 'Four', value: 81, color: '#E0E0E0E0' },
                    ]}
                  />
                </div>
                <br />

                <div className="access_activity">
                  <h5>Logged Associate</h5>
                  <div className="float-left w-100 mt-3">
                    <img className="p-3" src={photo2} />
                    <img className="p-3" src={photo} />
                    <img className="p-2" src={photo1} />
                  </div>
                </div>

              </aside>
            </div>
          </div>
        </div>

      </main >
     
    </div >



  );
}
export default Home;
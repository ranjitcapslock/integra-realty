import React, { useState, useEffect } from "react";
import Loader from "../../Pages/Loader/Loader.js";
import Toaster from "../../Pages/Toaster/Toaster.js";
import { NavLink, useNavigate } from "react-router-dom";
import { useParams } from "react-router-dom";
import user_service from "../service/user_service";
import jwt from "jwt-decode";
import moment from "moment";
import _ from "lodash";
import { classNames } from "@react-pdf-viewer/core";

const EventDetail = () => {
  const navigate = useNavigate();
  const [loader, setLoader] = useState({ isActive: null });
  const { isActive } = loader;
  const [toaster, setToaster] = useState({
    types: null,
    isShow: null,
    toasterBody: "",
    message: "",
  });
  const { types, isShow, toasterBody, message } = toaster;
  const [summary, setSummary] = useState("");
  const params = useParams();

  const [formValues, setFormValues] = useState("");

  const handleChange = (e) => {
    const { name, value } = e.target;
    setFormValues({ ...formValues, [name]: value });
  };

  useEffect(() => {
    window.scrollTo(0, 0);
    setLoader({ isActive: true });
    CalenderId(params.id);
  }, []);

  const CalenderId = async () => {
    await user_service.CalenderGetById(params.eventId).then((response) => {
      setLoader({ isActive: false });
      if (response) {
        setSummary(response.data);
      }
    });
  };

  const handleDelete = async () => {
    setLoader({ isActive: true });
    await user_service.calenderDelete(params.eventId).then((response) => {
      if (response) {
        setLoader({ isActive: false });
        setToaster({
          types: "Delete",
          isShow: true,
          toasterBody: response.data.message,
          // message: "",
        });
        setTimeout(() => {
          navigate(`/calendar`);
        }, 1000);
      }
    });
  };

  const handleUpdate = () => {
    navigate(`/update-Event/${params.eventId}`);
  };

  const handleBack = () => {
    navigate(`/calendar/`);
  };

  const formatDateTime = (date, time) => {
    const formattedDate = moment.utc(date).format("MM/DD/YYYY"); // Adjusted format to "Day/Month/Year"
    // const formattedTime = moment.utc(time, 'HH:mm').format('hh:mm A'); // Adjusted format to 12-hour time with AM/PM
    return `${formattedDate}`;
  };

  const [formData, setFormData] = useState([]);
  const [isModalVisibleUpdate, setIsModalVisibleUpdate] = useState(false);

  const StaffRoleGetData = async () => {
    await user_service.StaffRoleGet().then((response) => {
      if (response) {
        console.log(response);
        setFormData(response.data.data);
      }
    });
  };
  useEffect(() => {
    if (formData) {
      StaffRoleGetData();
    }
  }, []);

  const handleClose = () => {
    setIsModalVisibleUpdate(false);
  };

  const handleCalender = (text, date) => {
    setIsModalVisibleUpdate(true);
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
    const userData = {
      eventType: formValues.eventType,
    };
    try {
      setLoader({ isActive: true });
      const response = await user_service.CalenderUpdate(
        summary?._id,
        userData
      );
      if (response && response.data) {
        setLoader({ isActive: false });
        setToaster({
          types: "Calender",
          isShow: true,
          toasterBody: response.data.message,
          message: "Event Add Successfully",
        });
        setTimeout(() => {
          setToaster((prevToaster) => ({ ...prevToaster, isShow: false }));
          navigate(`/calendar`);
        }, 1000);
        document.getElementById("close").click();
      } else {
        setLoader({ isActive: false });
        setToaster({
          types: "error",
          isShow: true,
          toasterBody: response.data.message,
          message: "Error",
        });
      }
    } catch (error) {
      setLoader({ isActive: false });
      setToaster({
        types: "error",
        isShow: true,
        toasterBody: error.message,
        message: "Error",
      });
    }
  };


  useEffect(() => {
      profileGetAll();
  }, []);

  const [profile,setProfile] = useState("")
  const profileGetAll = async () => {
    if (localStorage.getItem("auth")) {
        const response = await user_service.profileGet(
          jwt(localStorage.getItem("auth")).id
        );
        if (response && response.data) {
         setProfile(response.data)
        } 
      } 
    }

    const handleRegister = async (e) => {
      e.preventDefault();
      const previousAttendees = Array.isArray(summary?.attendee) ? summary.attendee : [];
      const newAttendees = Array.isArray(profile) ? profile : [profile];
          
      const alreadyRegistered = newAttendees.some((newAttendee) =>
        previousAttendees.some(
          (existingAttendee) => existingAttendee._id === newAttendee._id
        )
      );
    
      if (alreadyRegistered) {
        setToaster({
          types: "warning",
          isShow: true,
          message: "You are already registered!",
        });
        setTimeout(() => {
          setToaster((prevToaster) => ({ ...prevToaster, isShow: false }));
        }, 2000);
        return; // Stop further processing
      }
    
      // Merge previous and new attendees (only if not already registered)
      const combinedAttendees = [...previousAttendees, ...newAttendees];
      try {
        setLoader({ isActive: true });
        const userData = { attendee: combinedAttendees };
        const response = await user_service.CalenderUpdate(summary?._id, userData);
    
        if (response && response.data) {
          setLoader({ isActive: false });
          setToaster({
            types: "success",
            isShow: true,
            message: "Event Added Successfully",
          });
          CalenderId();
    
          setTimeout(() => {
            setToaster((prevToaster) => ({ ...prevToaster, isShow: false }));
          }, 1000);
        }
      } catch (error) {
        setLoader({ isActive: false });
        setToaster({
          types: "error",
          isShow: true,
          message: "Error adding event!",
        });
    
        setTimeout(() => {
          setToaster((prevToaster) => ({ ...prevToaster, isShow: false }));
        }, 1000);
      }
    };


    const handleCancel = async (id) => {
      setLoader({ isActive: true });
      const updatedAttendees = summary.attendee.filter((attendee) => attendee._id !== id);
      const updatedSummary = { ...summary, attendee: updatedAttendees };
    
      try {
        const response = await user_service.CalenderUpdate(summary?._id, updatedSummary);
        if (response) {
          setLoader({ isActive: false });
          setToaster({
            types: "success",
            isShow: true,
            message: "Registration canceled successfully",
          });
    
          CalenderId();
          setTimeout(() => {
            setToaster((prevToaster) => ({ ...prevToaster, isShow: false }));
          }, 1000);
        }
      } catch (error) {
        console.error("Failed to cancel registration:", error);
        setLoader({ isActive: false });
        setToaster({
          types: "error",
          isShow: true,
          message: `Failed to cancel registration: ${error.message}`,
        });
        setTimeout(() => {
          setToaster((prevToaster) => ({ ...prevToaster, isShow: false }));
        }, 1000);
      }
    };
    
    
    

  return (
    <div className="bg-secondary float-left w-100 pt-4 mb-4">
      <Loader isActive={isActive} />
      {isShow && (
        <Toaster
          types={types}
          isShow={isShow}
          toasterBody={toasterBody}
          message={message}
        />
      )}
      <div className="content-overlay mt-0">
        <main className="page-wrapper getContact_page_wrap">
          {/* <!-- Page container--> */}
          <div className=" ">
            <div className="d-flex align-items-center justify-content-between float-left mb-3">
              <h3 className="text-white text-left mb-0">{summary.title}</h3>
          
            </div>
            <div className="row">
              <div className="col-md-8">
                <div className="bg-light border rounded-3 p-3">
                  <div className="row">
                    <h6>
                      {formatDateTime(summary?.startDate)} @{" "}
                      {summary.startTime ?? "N/A"} -{" "}
                      {formatDateTime(summary?.endDate)} @{" "}
                      {summary.endTime ?? "N/A"}
                    </h6>
                    <div className="col-md-6">
                      <h6>Details</h6>
                    </div>
                    <div className="col-md-6">
                      <h6>Venue</h6>
                    </div>
                    <div className="col-md-3">
                      <p>Date</p>
                    </div>
                    <div className="col-md-3">
                      <p>{formatDateTime(summary?.startDate)}</p>
                    </div>
                    <div className="col-md-3">
                      <p>Location</p>
                    </div>
                    <div className="col-md-3">
                      <p>{summary.venue}</p>
                    </div>
                    <div className="col-md-3">
                      <p>Time</p>
                    </div>
                    <div className="col-md-3">
                      <p>
                        {summary.startTime ?? "N/A"}&nbsp;
                        {summary.endTime ?? "N/A"}
                      </p>
                    </div>
                    <div className="col-md-3">
                      <p>Resource</p>
                    </div>
                    <div className="col-md-3">
                      <p>N/A</p>
                    </div>
                    <div className="col-md-3">
                      <p>Category</p>
                    </div>
                    <div className="col-md-3">
                      <p>{summary.category}</p>
                    </div>
                    <div className="col-md-3">
                      <p>Organizer</p>
                    </div>
                    <div className="col-md-3">
                      <p>{summary.organizer}</p>
                    </div>
                    <br />
                    <hr />
                    <br />

                    {summary.reservationId ? (
                      <div className="">
                        <div className="col-md-6">
                          <h6>Links & Info</h6>
                        </div>
                        <p>
                          For more information,&nbsp;
                          <NavLink
                            to={`/reservation-calender/${summary.reservationId}`}
                            className="primary"
                          >
                            click here.
                          </NavLink>
                        </p>
                      </div>
                    ) : (
                      ""
                    )}

                    {summary.infoUrl ? (
                      <div className="">
                        <div className="col-md-6">
                          <h6>Links & Info</h6>
                        </div>
                        <p>
                          For more information,&nbsp;
                          <a
                            href={summary.infoUrl}
                            target="_blank"
                            rel="noopener noreferrer"
                            className="primary"
                          >
                            click here.
                          </a>
                        </p>
                      </div>
                    ) : (
                      ""
                    )}

                    <div className="col-md-12">
                      {summary.eventDescription ? (
                        <>
                          <h6>Event Description</h6>
                          <p
                            dangerouslySetInnerHTML={{
                              __html: summary.eventDescription,
                            }}
                          />
                        </>
                      ) : (
                        ""
                      )}

                      <hr />
                      <br />
                      <h6>Event Publication</h6>
                      <div className="d-flex">
                        <div className="col-md-6">
                          <p>Publication Level</p>
                        </div>
                        <div className="col-md-6">
                          <p>{summary.publicationLevel}</p>
                        </div>
                      </div>

                      <div className="d-flex">
                        <div className="col-md-6">
                          <p>Last Modified By</p>
                        </div>
                        <div className="col-md-6">
                          <p>Shawn Kenney</p>
                        </div>
                      </div>
                      <div className="d-flex">
                        <div className="col-md-6">
                          <p>Last Modified</p>
                        </div>
                        <div className="col-md-6">
                          <p>
                            {formatDateTime(summary?.startDate)} @{" "}
                            {summary.startTime ?? "N/A"}
                          </p>
                        </div>
                      </div>

                      <div className="d-flex">
                        <div className="col-md-6">
                          <p>Date Created</p>
                        </div>
                        <div className="col-md-6">
                          <p>
                            {formatDateTime(summary?.startDate)} @{" "}
                            {summary.startTime ?? "N/A"}
                          </p>
                        </div>
                      </div>
                        {
                          summary.registration === "yes"? 
                          <>
                          <hr />
                          <h6 className="mt-2">Registration</h6>
                          <div className="">
                            {summary?.seatCount - (summary?.attendee?.length || 0)} Places Available
                              <p>(Total Capacity {summary?.seatCount})</p>
                              <NavLink to={`/event-roster/${params.eventId}`} className="btn btn-secondary">View Attendees</NavLink>
                          </div>
                        

                      <div className="mt-3">
                       {summary?.attendee?.some((attendee) => attendee._id === profile?._id) ? (
                      <button className="btn btn-secondary" onClick={() => handleCancel(profile._id)}>
                        Cancel Registration
                      </button>
                      ) : (
                        <button className="btn btn-primary" onClick={handleRegister}>
                          Register Now
                        </button>
                      )}
                      </div>

                          </>
                          :""
                        }

                   

                    </div>
                  </div>
                </div>
                <div className="pull-right">
                  <div className="float-left w-100 mt-3">
                    {localStorage.getItem("auth") &&
                    jwt(localStorage.getItem("auth")).contactType == "admin" ? (
                      <>
                        {summary && summary.eventType ? (
                          <button
                            type="button"
                            className="btn btn-secondary btn-sm ms-3"
                            id="save-button"
                            onClick={handleBack}
                            // onClick={() => handleCalender()}
                          >
                             Event Already Copy
                          </button>
                        ) : (
                          <button
                            type="button"
                            className="btn btn-primary btn-sm ms-3"
                            id="save-button"
                            data-toggle="modal"
                            data-target="#calender-event"
                            onClick={() => handleCalender()}
                          >
                            Copy Event Now
                          </button>
                        )}
                      </>
                    ) : (
                      ""
                    )}

                    {localStorage.getItem("auth") &&
                    jwt(localStorage.getItem("auth")).contactType == "admin" ? (
                      <>
                        <button
                          onClick={handleUpdate}
                          type="button"
                          className="btn btn-primary btn-sm ms-3"
                          id="save-button"
                        >
                          Edit Event
                        </button>
                        <button
                          onClick={() => handleDelete(params.eventId)}
                          type="button"
                          className="btn btn-secondary btn-sm ms-3"
                          id="save-button"
                        >
                          Remove
                        </button>
                      </>
                    ) : (
                      ""
                    )}

                    {localStorage.getItem("auth") &&
                      jwt(localStorage.getItem("auth")).contactType ==
                        "staff" &&
                      (Array.isArray(formData) && formData.length > 0
                        ? formData.map((item) =>
                            item.roleStaff === "calender" ? (
                              <>
                                <button
                                  onClick={handleUpdate}
                                  type="button"
                                  className="btn btn-primary btn-sm ms-3"
                                  id="save-button"
                                >
                                  Edit Event
                                </button>
                                <button
                                  onClick={() => handleDelete(params.eventId)}
                                  type="button"
                                  className="btn btn-secondary btn-sm ms-3"
                                  id="save-button"
                                >
                                  Remove
                                </button>
                              </>
                            ) : (
                              ""
                            )
                          )
                        : "")}

                    <button
                      type="button"
                      onClick={handleBack}
                      className="btn btn-secondary btn-sm ms-3"
                      id="save-button"
                    >
                      Back
                    </button>
                  </div>
                </div>
              </div>
              <div className="col-md-4"></div>
              {isModalVisibleUpdate && (
                <div className="modal mt-5" role="dialog" id="calender-event">
                  <div
                    className="modal-dialog modal-md modal-dialog-scrollable"
                    role="document"
                  >
                    <div className="modal-content mt-5">
                      <div className="modal-header">
                        <h4 className="modal-title">Add an Event</h4>
                        <button
                          className="btn-close"
                          type="button"
                          id="close"
                          data-bs-dismiss="modal"
                          aria-label="Close"
                          onClick={handleClose}
                        ></button>
                      </div>
                      <div className="modal-body fs-sm">
                        <div className="col-md-12">
                          <h6>
                            Are you sure you want to add this event to repeat on
                            the same day and time each year?
                          </h6>

                          <select
                            className="form-select "
                            id="pr-city"
                            name="eventType"
                            value={formValues.eventType}
                            onChange={handleChange}
                          >
                            <option>Select repeated option</option>
                            <option value="weekly">Weekly</option>
                            <option value="monthly">Monthly</option>
                            <option value="yearly">Yearly</option>
                          </select>
                          <div className="pull-right mt-3">
                            {localStorage.getItem("auth") &&
                            jwt(localStorage.getItem("auth")).contactType ==
                              "admin" ? (
                              <button
                                onClick={(e) => handleSubmit(e)}
                                type="button"
                                className="btn btn-primary btn-sm "
                                id="save-button"
                              >
                                Add Event
                              </button>
                            ) : (
                              ""
                            )}
                            <button
                              type="button"
                              className="btn btn-secondary btn-sm ms-3"
                              id="close"
                              data-bs-dismiss="modal"
                              onClick={handleClose}
                            >
                              Cancel & Close
                            </button>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              )}
            </div>
          </div>
        </main>
      </div>
    </div>
  );
};

export default EventDetail;

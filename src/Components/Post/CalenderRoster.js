import React, { useState, useEffect } from "react";
import Loader from "../../Pages/Loader/Loader.js";
import Toaster from "../../Pages/Toaster/Toaster.js";
import { NavLink, useNavigate } from "react-router-dom";
import { useParams } from "react-router-dom";
import user_service from "../service/user_service";
import jwt from "jwt-decode";
import moment from "moment";
import _ from "lodash";
import Pic from "../img/pic.png";
import defaultpropertyimage from "../../Components/img/defaultpropertyimage.jpeg";

const CalenderRoster = () => {
  const navigate = useNavigate();
  const [loader, setLoader] = useState({ isActive: null });
  const { isActive } = loader;
  const [toaster, setToaster] = useState({
    types: null,
    isShow: null,
    toasterBody: "",
    message: "",
  });
  const { types, isShow, toasterBody, message } = toaster;
  const [summary, setSummary] = useState("");
  const params = useParams();

  const [formValues, setFormValues] = useState("");

  const handleChange = (e) => {
    const { name, value } = e.target;
    setFormValues({ ...formValues, [name]: value });
  };

  useEffect(() => {
    window.scrollTo(0, 0);
    setLoader({ isActive: true });
    CalenderId(params.id);
  }, []);

  const CalenderId = async () => {
    await user_service.CalenderGetById(params.eventId).then((response) => {
      setLoader({ isActive: false });
      if (response) {
        setSummary(response.data);
      }
    });
  };

  const handleUpdate = () => {
    navigate(`/update-Event/${params.eventId}`);
  };

  const formatDateTime = (date, time) => {
    const formattedDate = moment.utc(date).format("MM/DD/YYYY"); // Adjusted format to "Day/Month/Year"
    // const formattedTime = moment.utc(time, 'HH:mm').format('hh:mm A'); // Adjusted format to 12-hour time with AM/PM
    return `${formattedDate}`;
  };

  const [formData, setFormData] = useState([]);
  const [isModalVisibleUpdate, setIsModalVisibleUpdate] = useState(false);

  const StaffRoleGetData = async () => {
    await user_service.StaffRoleGet().then((response) => {
      if (response) {
        console.log(response);
        setFormData(response.data.data);
      }
    });
  };
  useEffect(() => {
    if (formData) {
      StaffRoleGetData();
    }
  }, []);

  const handleClose = () => {
    setIsModalVisibleUpdate(false);
  };

  const handleCalender = (text, date) => {
    setIsModalVisibleUpdate(true);
  };

  const [contactType, setContactType] = useState("associate");
  const [contactStatus, setContactStatus] = useState("active");

  const [contactName, setContactName] = useState("");
  const [getContact, setGetContact] = useState([]);

  const SearchGetAll = async () => {
    try {
      const response = await user_service.SearchContactfilterAssociate(
        1,
        contactType,
        contactStatus,
        contactName
      );
      if (response) {
        setGetContact(response.data.data);
      }
    } catch (error) {
      console.error("Error fetching data:", error);
    }
  };

  useEffect(() => {
    window.scrollTo(0, 0);
    if (contactName) {
      SearchGetAll(1);
    }
  }, [contactType, contactName]);

  const [contactsAssociate, setContactsAssociate] = useState("");
  const handleContactAssociate = (contactAssociate) => {
    setContactsAssociate(contactAssociate);
    setGetContact([]);
    setContactName("");
  };

  const handleDeleteAssociate = () => {
    setContactsAssociate("");
  };

  const handleSubmit = async (e) => {
    e.preventDefault();

    const previousAttendees = Array.isArray(summary?.attendee)
      ? summary.attendee
      : [];
    const newAttendees = Array.isArray(contactsAssociate)
      ? contactsAssociate
      : [contactsAssociate];

    // Merge and remove duplicates
    const combinedAttendees = [...previousAttendees, ...newAttendees].filter(
      (item, index, self) =>
        self.findIndex(
          (attendee) => JSON.stringify(attendee) === JSON.stringify(item)
        ) === index
    );
    try {
      setLoader({ isActive: true });
      const userData = { attendee: combinedAttendees };
      const response = await user_service.CalenderUpdate(
        summary?._id,
        userData
      );
      if (response && response.data) {
        setLoader({ isActive: false });
        setToaster({
          types: "Calender",
          isShow: true,
          //   toasterBody: response.data.message,
          message: "Event Add Successfully",
        });

        setTimeout(() => {
          setToaster((prevToaster) => ({ ...prevToaster, isShow: false }));
          //   navigate(`/calendar`);
        }, 1000);
        setContactsAssociate("");
        setBack((prevBack) => !prevBack);
        CalenderId();
      }
    } catch (error) {
      setLoader({ isActive: false });
      setToaster({
        types: "error",
        isShow: true,
        message: "Error",
      });
    }
  };

  const formattedDate = (createdDate) => {
    return moment(createdDate).format("ddd, MM/DD/YYYY hh:mm A");
  };

  const [back, setBack] = useState(true);
  const handleBack = () => {
    setBack((prevBack) => !prevBack);
    setContactsAssociate("");
  };

  const handleDelete = async (id, index) => {
    setLoader({ isActive: true });
    const updatedAttendees = summary.attendee.filter((_, i) => i !== index);
    const updatedSummary = { ...summary, attendee: updatedAttendees };
    try {
      const response = await user_service.CalenderUpdate(
        summary?._id,
        updatedSummary
      );
      if (response) {
        setLoader({ isActive: false });
        setToaster({
          types: "Delete",
          isShow: true,
          message: "Attendee deleted successfully",
        });
        setTimeout(() => {
          setToaster((prevToaster) => ({ ...prevToaster, isShow: false }));
          //   navigate(`/calendar`);
        }, 1000);
        CalenderId();
      }
    } catch (error) {
      console.error("Failed to delete attendee:", error);
      setLoader({ isActive: false });
      setToaster({
        types: "Error",
        isShow: true,
        message: `Failed to delete attendee: ${error.message}`,
      });
      setTimeout(() => {
        setToaster((prevToaster) => ({ ...prevToaster, isShow: false }));
        //   navigate(`/calendar`);
      }, 1000);
    }
  };

  const handleBackPage = () => {
    navigate(`/event-detail/${params.eventId}`);
  };



  const formatDate = (dateString) => {
    const date = new Date(dateString);
    const options = {
      year: 'numeric',
      month: '2-digit',
      day: '2-digit',
    };
    return new Intl.DateTimeFormat('en-US', options).format(date);
  };


  const handleDownload = () => {
    if (Array.isArray(summary?.attendee) && summary.attendee.length > 0) {
      const headers = ["First Name", "Last Name", "Email",  "Office", "Created Date"];
      const rows = summary.attendee.map((item) => [
        item.firstName,
        item.lastName,
        item.email,
        item.active_office,
        item.created ? formatDate(item.created) : "",
      ]);
  
      // Combine headers and rows into CSV content
      const csvContent = [
        headers.join(","), // Add headers
        ...rows.map((row) => row.join(",")), // Add each row
      ].join("\n");
  
      // Create a Blob and a downloadable link
      const blob = new Blob([csvContent], { type: "text/csv;charset=utf-8;" });
      const url = URL.createObjectURL(blob);
      const link = document.createElement("a");
      link.href = url;
      link.download = "attendees.csv"; // Specify the file name
      link.click();
  
      // Clean up URL object
      URL.revokeObjectURL(url);
    } else {
      alert("No data available to download.");
    }
  };

  return (
    <div className="bg-secondary float-left w-100 pt-4 mb-4">
      <Loader isActive={isActive} />
      {isShow && (
        <Toaster
          types={types}
          isShow={isShow}
          toasterBody={toasterBody}
          message={message}
        />
      )}
      <div className="content-overlay mt-0">
        <main className="page-wrapper getContact_page_wrap">
          {/* <!-- Page container--> */}
          <div className="float-start w-100 bg-light border rounded-3 p-3">
            <div className="d-flex align-items-center justify-content-between float-left mb-3">
              <h6 className="text-left mb-0">
                {summary.title}, {summary.category} Event -
                {formatDateTime(summary?.startDate)} @{" "}
                {summary.startTime ?? "N/A"} -{" "}
                {formatDateTime(summary?.endDate)} @ {summary.endTime ?? "N/A"}
              </h6>
              {
                summary?.attendee?.length>0 ?
                <button className="btn btn-primary" onClick={handleDownload}>Download</button>
                :""
              }
            </div>
            <hr />
          
              <div className="float-start w-100 mt-4">
                <button
                  onClick={handleBack}
                  className="btn btn-primary pull-right"
                >
                  Add an Attendee
                </button>
              </div>
              {!back && (
                <div className="float-start w-100 border rounded-3 p-3 my-4">
                  <a className="mb-3" onClick={handleBack}>
                    Hide
                  </a>
                  <div className="row">
                  <div className="col-md-6">
                    <div className="float-start w-100 position-relative">
                    <label className="col-form-label">Search by name:</label>
                    <input
                      className="form-control"
                      id="inline-form-input"
                      type="text"
                      name="contactName"
                      onChange={(event) => setContactName(event.target.value)}
                    />{" "}
                    {contactName ? (
                      <>
                  <div className="all_associlates_list shadow px-2 py-2">
                    {getContact.length > 0 ? (
                      getContact.map((contact, index) => (
                        
                        <div
                          className="member_Associate mb-1 mt-1 p-2"
                          key={index}
                          onClick={() => handleContactAssociate(contact)}
                        >
                          <div
                            className="float-left w-100 d-flex align-items-center justify-content-start"
                            key={contact._id}
                          >
                            <img
                              className="rounded-circlee profile_picture"
                              height="30"
                              width="30"
                              src={
                                contact.image && contact.image !== "image"
                                  ? contact.image
                                  : Pic
                              }
                              alt="Profile"
                            />
                            <div className="ms-3">
                              <p className="mb-0">
                                {contact.firstName} {contact.lastName}
                              </p>
                              <p className="mb-0"><small>{contact.active_office}</small></p>
                            </div>
                          </div>
                        </div>
                        
                      ))
                    ) : (
                      <p></p>
                    )}
                      </div>
                      </>
                    ) : (
                      <></>
                    )}
                  </div>
                  </div>
                  <div className="col-md-4">
                    <label class="col-form-label"></label>
                    {contactsAssociate ? (
                      <div className="member_Associate mb-1 p-2 px-3">
                        <div
                          className="float-left w-100 d-flex align-items-center justify-content-between"
                          key={contactsAssociate._id}
                        >
                          <div className="d-flex align-items-center justify-content-start">
                          <img
                            className="rounded-circlee profile_picture"
                            height="30"
                            width="30"
                            src={
                              contactsAssociate.image &&
                              contactsAssociate.image !== "image"
                                ? contactsAssociate.image
                                : Pic
                            }
                            alt="Profile"
                          />

                            <div className="ms-3">
                              <p className="mb-0">
                                {contactsAssociate.firstName}{" "}
                                {contactsAssociate.lastName}
                              </p>
                              <p className="mb-0"><small>{contactsAssociate.active_office}</small></p>
                            </div>
                          </div>
                          <div
                            className="groupMail_delete"
                            onClick={() => handleDeleteAssociate()}
                          >
                            <i className="h2 fi-trash m-0 cursor-pointer"></i>
                          </div>
                        </div>
                      </div>
                    ) : (
                      ""
                    )}
                  </div>
                  </div>
                  {contactsAssociate ? (
                    <div className="pull-left">
                      <button
                        className="btn btn-primary mt-4"
                        onClick={handleSubmit}
                      >
                        Register Attendee
                      </button>
                    </div>
                  ) : (
                    ""
                  )}
                </div>
              )}

              <div className="mt-4">
                <h4 className="mb-0">Attendee Roster</h4>
                <div className="table-responsive border rounded-3 p-3 mt-3">
                  <table
                    id="DepositLedger"
                    className="table table-striped align-middle mb-0 w-100"
                    border="0"
                    cellspacing="0"
                    cellpadding="0"
                  >
                    <thead>
                      <tr>
                        <th>ATTENDEE NAME</th>
                        <th>REGISTER DATE</th>
                        <th>SEAT</th>
                        {localStorage.getItem("auth") &&
                        jwt(localStorage.getItem("auth")).contactType ==
                          "admin" ? (
                          <th>DELETE</th>
                        ) : (
                          ""
                        )}
                      </tr>
                    </thead>
                    <tbody>
                      {Array.isArray(summary?.attendee) &&
                      summary.attendee.length > 0 ? (
                        <>
                          {summary.attendee.map((item, index) => (
                            <tr key={index}>
                              <td>
                                <NavLink to={`/contact-profile/${item._id}`}>
                                <img
                                  className=""
                                  style={{"width": "65px", "height": "65px"}}
                                  src={
                                    item.image && item.image !== "image"
                                      ? item.image
                                      : Pic
                                  }
                                  alt={`${item.firstName} ${item.lastName}`}
                                />{" "}
                                {item.firstName} {item.lastName}
                                </NavLink>
                              </td>

                              <td>
                                {item.created
                                  ? formattedDate(item.created)
                                  : "No Date Available"}
                              </td>

                              <td>{item.seat}</td>
                              {localStorage.getItem("auth") &&
                              jwt(localStorage.getItem("auth")).contactType ==
                                "admin" ? (
                                <td>
                                  <button
                                    className="btn btn-secondary btn-sm"
                                    onClick={() => handleDelete(item._id, index)}
                                  >
                                    Delete
                                  </button>
                                </td>
                              ) : (
                                ""
                              )}
                            </tr>
                          ))}
                        </>
                      ) : (
                        <tr>
                          <td colSpan="2">
                            No one has registered for this event.
                          </td>
                        </tr>
                      )}
                    </tbody>
                  </table>
                </div>
              <div className="mt-3">
                <button
                  type="button"
                  onClick={handleBackPage}
                  className="btn btn-secondary pull-right"
                  id="save-button"
                >
                  Back
                </button>
              </div>
              </div>
         
          </div>
        </main>
      </div>
    </div>
  );
};

export default CalenderRoster;

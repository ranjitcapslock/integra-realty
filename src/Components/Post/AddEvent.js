import React, { useState, useEffect, useRef } from "react";
import Loader from "../../Pages/Loader/Loader.js";
import Toaster from "../../Pages/Toaster/Toaster.js";
import { Editor } from "react-draft-wysiwyg";
import { EditorState, convertToRaw } from "draft-js";
import draftToHtml from "draftjs-to-html";
import "react-draft-wysiwyg/dist/react-draft-wysiwyg.css";

import { NavLink } from "react-router-dom";
import { useNavigate } from "react-router-dom";
import user_service from "../service/user_service";
import jwt from "jwt-decode";
import _ from "lodash";

// const apiKey = "gtzihoqhxnyb7apgj00x766eel9cqv0herq11druhd2j6hki";

const AddEvent = () => {
  const editorRef = useRef(null);
  const navigate = useNavigate();

  const [loader, setLoader] = useState({ isActive: null });
  const { isActive } = loader;
  const [toaster, setToaster] = useState({
    types: null,
    isShow: null,
    toasterBody: "",
    message: "",
  });
  const { types, isShow, toasterBody, message } = toaster;

  const initialValues = {
    allDayEvent: "yes",
    category: "",
    draft: "yes",
    endDate: "",
    guestSeats: "no",
    information: "no",
    publicEvent: "yes",
    registration: "no",
    startDate: "",
    title: "",
    venue: "",
    infoUrl: "",
    organizer: "Integra Reality",
    repeatingEvent: "yes",
    anotherEvent: "yes",
    eventDescription: "event -add",
    startTime: "9:00 AM",
    endTime: "10:00 AM",
    publicationLevel: "",
    maxSeats: "Agent +1",
    seatCount: "24",
    question: "50 words add",
  };
  const [formValues, setFormValues] = useState(initialValues);
  const [formErrors, setFormErrors] = useState({});
  const [isSubmitClick, setISSubmitClick] = useState(false);
  const [checkRegistration, setCheckRegistration] = useState("no");
  const [checkGuest, setGuest] = useState("no");
  const [checkRepeting, setCheckRepeting] = useState("yes");
  const [checkAnotherEvent, setCheckAnotherEvent] = useState("yes");
  const [checkDraft, setCheckDraft] = useState("yes");

  const [checkInformation, setInformation] = useState("no");

  const [checkPublicEvent, setCheckPublicEvent] = useState("yes");

  const [organizationGet, setOrganizationGet] = useState([]);

  useEffect(() => {
    //window.scrollTo(0, 0);
    if (formValues && isSubmitClick) {
      validate();
    }
  }, [formValues]);

  useEffect(() => {
    OrganizationTreeGets();
  }, []);

  const OrganizationTreeGets = async () => {
    // setLoader({ isActive: true });
    if (jwt(localStorage.getItem("auth")).id) {
      await user_service.organizationTreeGet().then((response) => {
        // setLoader({ isActive: false });
        if (response) {
          setOrganizationGet(response.data);
        }
      });
    }
  };

  const [switchValue, setSwitchValue] = useState(false);

  const handleSwitchChange = () => {
    console.log(!switchValue);
    setSwitchValue(!switchValue);
  };

  {
    /* <!-- Input onChange Start--> */
  }
  const handleChange = (e) => {
    const { name, value } = e.target;
    setFormValues({ ...formValues, [name]: value });
  };

  const [editorState, setEditorState] = useState(EditorState.createEmpty());

  const handleChanges = (editorState) => {
    setEditorState(editorState);
    const htmlContent = draftToHtml(
      convertToRaw(editorState.getCurrentContent())
    );
    setFormValues({ ...formValues, eventDescription: htmlContent });
  };

  const validate = () => {
    const values = formValues;
    const errors = {};
    if (!values.title) {
      errors.title = "title is required";
    }
    if (!values.startDate) {
      errors.startDate = "startDate is required";
    }

    if (!values.endDate) {
      errors.endDate = "endDate is required";
    }

    if (!values.publicationLevel) {
      errors.publicationLevel = "Publication Level is required";
    }

    setFormErrors(errors);
    return errors;
  };
  {
    /* <!-- Form Validation End--> */
  }

  const CheckPublicEvent = (e) => {
    setCheckPublicEvent(e.target.name);
    setCheckPublicEvent(e.target.value);
  };

  const CheckRegistration = (e) => {
    setCheckRegistration(e.target.name);
    setCheckRegistration(e.target.value);
  };

  const CheckInformation = (e) => {
    setInformation(e.target.name);
    setInformation(e.target.value);
  };

  const CheckGuest = (e) => {
    setGuest(e.target.name);
    setGuest(e.target.value);
  };

  const CheckRepetingEvent = (e) => {
    setCheckRepeting(e.target.name);
    setCheckRepeting(e.target.value);
  };

  const CheckAnotherEvent = (e) => {
    setCheckAnotherEvent(e.target.name);
    setCheckAnotherEvent(e.target.value);
  };

  const CheckedDraft = (e) => {
    setCheckDraft(e.target.name);
    setCheckDraft(e.target.value);
  };

  // {/* <!-- Form onSubmit Start--> */ }

  const token = localStorage.getItem("auth");
  let userRole = null;

  if (token) {
    const decodedToken = jwt(token);
    userRole = decodedToken.contactType; // Extracting the user role (admin or agent)
  }

  const defaultCategory = userRole === "admin" ? "Office" : "Personal";

  // Ensure the form has the default category when it first loads
  useEffect(() => {
    if (!formValues.category) {
      setFormValues((prevValues) => ({
        ...prevValues,
        category: defaultCategory,
      }));
    }
  }, [defaultCategory]);

  const handleSubmit = async (e) => {
    e.preventDefault();
    setISSubmitClick(true);
    let checkValue = validate();
    if (_.isEmpty(checkValue)) {
      let publicationLevelname = "Corporate";
      if (formValues.publicationLevel) {
        const organization = organizationGet.data.find(
          (item) => item._id == formValues.publicationLevel
        );

        publicationLevelname = organization.name;
      }

      const userData = {
        category: formValues.category,
        publicationLevel: publicationLevelname,

        office_id: formValues?.publicationLevel
          ? formValues?.publicationLevel
          : "Corporate",
        office_name: publicationLevelname,

        allDayEvent: switchValue == true ? "yes" : "no",
        title: formValues.title,
        information: checkInformation
          ? checkInformation
          : formValues.information,
        registration: checkRegistration
          ? checkRegistration
          : formValues.registration,
        venue: formValues.venue,
        organizer: formValues.organizer,
        draft: checkDraft ? checkDraft : formValues.draft,
        agentId: jwt(localStorage.getItem("auth")).id,
        startDate: formValues.startDate,
        endDate: formValues.endDate,
        guestSeats: checkGuest ? checkGuest : formValues.guestSeats,
        publicEvent: checkPublicEvent
          ? checkPublicEvent
          : formValues.publicEvent,
        infoUrl: formValues.infoUrl,
        repeatingEvent: checkRepeting
          ? checkRepeting
          : formValues.repeatingEvent,
        anotherEvent: checkAnotherEvent
          ? checkAnotherEvent
          : formValues.anotherEvent,
        eventDescription: formValues.eventDescription,
        startTime: formValues.startTime,
        endTime: formValues.endTime,
        maxSeats: formValues.maxSeats,
        seatCount: formValues.seatCount,
        question: formValues.question,
      };
      try {
        setLoader({ isActive: true });
        const response = await user_service.CalenderPost(userData);
        setLoader({ isActive: false });
        if (response) {
          console.log(response.data);
          setLoader({ isActive: false });
          setToaster({
            types: "Add Event",
            isShow: true,
            toasterBody: response.data.message,
            message: "Event Add Successfully",
          });
          setTimeout(() => {
            navigate(`/event-detail/${response.data._id}`);
          }, 1000);
        } else {
          setLoader({ isActive: false });
          setToaster({
            types: "error",
            isShow: true,
            toasterBody: response.data.message,
            message: "Error",
          });
        }
      } catch (error) {
        setLoader({ isActive: false });
        setToaster({
          types: "error",
          isShow: true,
          toasterBody: error,
          message: "Error",
        });
      }
    }
  };

  {
    /* <!-- Form onSubmit End--> */
  }

  return (
    <div className="bg-secondary float-left w-100 pt-4 mb-4">
      <Loader isActive={isActive} />
      {isShow && (
        <Toaster
          types={types}
          isShow={isShow}
          toasterBody={toasterBody}
          message={message}
        />
      )}
      <main className="page-wrapper">
        {/* <!-- Page container--> */}
        <div className="">
          <div className="float-left w-100 d-flex align-items-center justify-content-between mb-4">
            <h3 className="text-white mb-0">Add an Event</h3>
          </div>
          <div className="row">
            <div className="col-md-8">
              <div className="border bg-light rounded-3 p-3">
                <div className="bg-light rounded-3 mb-3">
                  <div className="row">
                    <div className="col-md-12 mb-4">
                      <label className="form-label" for="pr-city">
                        Event Title<span className="text-danger">*</span>
                      </label>
                      <input
                        className="form-control"
                        id="inline-form-input"
                        type="text"
                        placeholder="Enter the event title"
                        name="title"
                        onChange={handleChange}
                        value={formValues.title}
                        style={{
                          border: formErrors?.title ? "1px solid red" : "",
                        }}
                      />
                      <div className="invalid-tooltip">{formErrors.title}</div>
                    </div>
                  </div>
                  <div className="row">
                    <div className="col-sm-6 mb-4">
                      <label className="form-label" for="pr-city">
                        Type of Event
                      </label>
                      <select
                        className="form-select"
                        id="pr-city"
                        name="category"
                        onChange={handleChange}
                        value={formValues.category}
                      >
                        {/* Default category based on user role */}
                        {userRole === "admin" ? (
                          <>
                            <option value="Office">Office</option>
                            <option value="Personal">Personal</option>
                            <option value="Staff">Staff</option>
                            <option value="Training">Training</option>
                            <option value="Community">Community</option>
                            <option value="Courses">Courses</option>
                          </>
                        ) : (
                          <option value="Personal">Personal</option>
                        )}
                      </select>
                      {/* <div className="invalid-tooltip">{formErrors.category}</div> */}
                    </div>
                    <div className="col-sm-6 mb-4">
                      <label className="form-label" for="pr-city">
                        Publication Level
                      </label>
                      <select
                        className="form-select "
                        id="pr-city"
                        name="publicationLevel"
                        onChange={handleChange}
                        value={formValues.publicationLevel}
                        style={{
                          border: formErrors?.publicationLevel
                            ? "1px solid red"
                            : "1px solid #00000026",
                        }}
                      >
                        <option>choose a publicationLevel</option>
                        {organizationGet.data
                          ? organizationGet.data.map((item) => (
                              <option value={item._id}>{item.name}</option>
                            ))
                          : ""}
                      </select>
                      {formErrors.publicationLevel && (
                        <div className="invalid-tooltip">
                          {formErrors.publicationLevel}
                        </div>
                      )}
                    </div>
                  </div>
                  <div className="row">
                    <div className="col-sm-6 mb-4">
                      <label className="form-label" for="pr-birth-date">
                        Start Date
                      </label>
                      <input
                        className="form-control"
                        type="date"
                        id="inline-form-input"
                        placeholder="Choose date"
                        name="startDate"
                        onChange={handleChange}
                        value={formValues.startDate}
                        style={{
                          border: formErrors?.startDate
                            ? "1px solid red"
                            : "1px solid #00000026",
                        }}
                        data-datepicker-options='{"altInput": true, "altFormat": "F j, Y", "dateFormat": "Y-m-d"}'
                      />
                      <div className="invalid-tooltip">
                        {formErrors.startDate}
                      </div>
                    </div>
                    <div className="col-sm-6 mb-4">
                      <label className="form-label" for="pr-birth-date">
                        End Date
                      </label>
                      <input
                        className="form-control"
                        type="date"
                        id="inline-form-input"
                        placeholder="Choose date"
                        name="endDate"
                        onChange={handleChange}
                        value={formValues.endDate}
                        style={{
                          border: formErrors?.endDate
                            ? "1px solid red"
                            : "1px solid #00000026",
                        }}
                        data-datepicker-options='{"altInput": true, "altFormat": "F j, Y", "dateFormat": "Y-m-d"}'
                      />
                      <div className="invalid-tooltip">
                        {formErrors.endDate}
                      </div>
                    </div>

                    <div className="col-sm-12 mb-2">
                      <div className="d-flex align-items-center justify-content-between">
                        <h6 className="mb-0">Is this a full day Event</h6>
                        <div className="form-check form-switch">
                          <input
                            className="form-check-input w-auto"
                            type="checkbox"
                            id="formSwitchDisabled"
                            name="allDayEvent"
                            checked={switchValue}
                            onChange={handleSwitchChange}
                          />
                          {/* <label
                            className="form-check-label"
                            htmlFor="formSwitchDisabled"
                          >
                            {switchValue ? "yes" : "no"}
                          </label> */}
                        </div>
                      </div>
                    </div>

                    {switchValue === true ? (
                      <>
                        <div className="col-sm-6">
                          <label className="form-label" for="pr-city">
                            Venue Name
                          </label>
                          <input
                            className="form-control"
                            id="inline-form-input"
                            type="text"
                            placeholder="venue"
                            name="venue"
                            onChange={handleChange}
                            value={formValues.venue}
                            style={{
                              border: formErrors?.venue ? "1px solid red" : "",
                            }}
                          />
                          <div className="invalid-tooltip">
                            {formErrors.venue}
                          </div>
                        </div>

                        <div className="col-sm-6">
                          <label className="form-label" for="pr-city">
                            Organizer
                          </label>
                          <input
                            className="form-control"
                            id="inline-form-input"
                            type="text"
                            placeholder="organizer"
                            name="organizer"
                            onChange={handleChange}
                            value={formValues.organizer}
                            style={{
                              border: formErrors?.organizer
                                ? "1px solid red"
                                : "",
                            }}
                          />
                          <div className="invalid-tooltip">
                            {formErrors.organizer}
                          </div>
                        </div>
                      </>
                    ) : (
                      <>
                        <div className="col-sm-6">
                          <label className="form-label" for="pr-birth-date">
                            Start Time
                          </label>
                          <select
                            className="form-select "
                            id="pr-city"
                            name="startTime"
                            value={formValues.startTime}
                            onChange={handleChange}
                          >
                            <option></option>
                            <option>12:00 AM</option>
                            <option>12:15 AM</option>
                            <option>12:30 AM</option>
                            <option>12:45 AM</option>
                            <option>1:00 AM</option>
                            <option>1:15 AM</option>
                            <option>1:30 AM</option>
                            <option>1:45 AM</option>
                            <option>2:00 AM</option>
                            <option>2:15 AM</option>
                            <option>2:30 AM</option>
                            <option>2:45 AM</option>
                            <option>3:00 AM</option>
                            <option>3:15 AM</option>
                            <option>3:30 AM</option>
                            <option>3:45 AM</option>
                            <option>4:00 AM</option>
                            <option>4:15 AM</option>
                            <option>4:30 AM</option>
                            <option>4:45 AM</option>
                            <option>5:00 AM</option>
                            <option>5:15 AM</option>
                            <option>5:30 AM</option>
                            <option>5:45 AM</option>
                            <option>6:00 AM</option>
                            <option>6:15 AM</option>
                            <option>6:30 AM</option>
                            <option>6:45 AM</option>
                            <option>7:00 AM</option>
                            <option>7:15 AM</option>
                            <option>7:30 AM</option>
                            <option>7:45 AM</option>
                            <option>8:00 AM</option>
                            <option>8:15 AM</option>
                            <option>8:30 AM</option>
                            <option>8:45 AM</option>
                            <option>9:00 AM</option>
                            <option>9:15 AM</option>
                            <option>9:30 AM</option>
                            <option>9:45 AM</option>
                            <option>10:00 AM</option>
                            <option>10:15 AM</option>
                            <option>10:30 AM</option>
                            <option>10:45 AM</option>
                            <option>11:00 AM</option>
                            <option>11:15 AM</option>
                            <option>11:30 AM</option>
                            <option>11:45 AM</option>
                            <option>12:00 PM</option>
                            <option>12:15 PM</option>
                            <option>12:30 PM</option>
                            <option>12:45 PM</option>
                            <option>1:00 PM</option>
                            <option>1:15 PM</option>
                            <option>1:30 PM</option>
                            <option>1:45 PM</option>
                            <option>2:00 PM</option>
                            <option>2:15 PM</option>
                            <option>2:30 PM</option>
                            <option>2:45 PM</option>
                            <option>3:00 PM</option>
                            <option>3:15 PM</option>
                            <option>3:30 PM</option>
                            <option>3:45 PM</option>
                            <option>4:00 PM</option>
                            <option>4:15 PM</option>
                            <option>4:30 PM</option>
                            <option>4:45 PM</option>
                            <option>5:00 PM</option>
                            <option>5:15 PM</option>
                            <option>5:30 PM</option>
                            <option>5:45 PM</option>
                            <option>6:00 PM</option>
                            <option>6:15 PM</option>
                            <option>6:30 PM</option>
                            <option>6:45 PM</option>
                            <option>7:00 PM</option>
                            <option>7:15 PM</option>
                            <option>7:30 PM</option>
                            <option>7:45 PM</option>
                            <option>8:00 PM</option>
                            <option>8:15 PM</option>
                            <option>8:30 PM</option>
                            <option>8:45 PM</option>
                            <option>9:00 PM</option>
                            <option>9:15 PM</option>
                            <option>9:30 PM</option>
                            <option>9:45 PM</option>
                            <option>10:00 PM</option>
                            <option>10:15 PM</option>
                            <option>10:30 PM</option>
                            <option>10:45 PM</option>
                            <option>11:00 PM</option>
                            <option>11:15 PM</option>
                            <option>11:30 PM</option>
                            <option>11:45 PM</option>
                          </select>
                          <p className="m-0">
                            {/* Selected Time: {formValues !== '' ? formValues : 'no time selected'} */}
                          </p>
                        </div>
                        <div className="col-sm-6">
                          <label className="form-label" for="pr-birth-date">
                            End Time<span className="text-danger">*</span>
                          </label>
                          <select
                            className="form-select "
                            id="pr-city"
                            name="endTime"
                            value={formValues.endTime}
                            onChange={handleChange}
                          >
                            <option></option>
                            <option>12:00 AM</option>
                            <option>12:15 AM</option>
                            <option>12:30 AM</option>
                            <option>12:45 AM</option>
                            <option>1:00 AM</option>
                            <option>1:15 AM</option>
                            <option>1:30 AM</option>
                            <option>1:45 AM</option>
                            <option>2:00 AM</option>
                            <option>2:15 AM</option>
                            <option>2:30 AM</option>
                            <option>2:45 AM</option>
                            <option>3:00 AM</option>
                            <option>3:15 AM</option>
                            <option>3:30 AM</option>
                            <option>3:45 AM</option>
                            <option>4:00 AM</option>
                            <option>4:15 AM</option>
                            <option>4:30 AM</option>
                            <option>4:45 AM</option>
                            <option>5:00 AM</option>
                            <option>5:15 AM</option>
                            <option>5:30 AM</option>
                            <option>5:45 AM</option>
                            <option>6:00 AM</option>
                            <option>6:15 AM</option>
                            <option>6:30 AM</option>
                            <option>6:45 AM</option>
                            <option>7:00 AM</option>
                            <option>7:15 AM</option>
                            <option>7:30 AM</option>
                            <option>7:45 AM</option>
                            <option>8:00 AM</option>
                            <option>8:15 AM</option>
                            <option>8:30 AM</option>
                            <option>8:45 AM</option>
                            <option>9:00 AM</option>
                            <option>9:15 AM</option>
                            <option>9:30 AM</option>
                            <option>9:45 AM</option>
                            <option>10:00 AM</option>
                            <option>10:15 AM</option>
                            <option>10:30 AM</option>
                            <option>10:45 AM</option>
                            <option>11:00 AM</option>
                            <option>11:15 AM</option>
                            <option>11:30 AM</option>
                            <option>11:45 AM</option>
                            <option>12:00 PM</option>
                            <option>12:15 PM</option>
                            <option>12:30 PM</option>
                            <option>12:45 PM</option>
                            <option>1:00 PM</option>
                            <option>1:15 PM</option>
                            <option>1:30 PM</option>
                            <option>1:45 PM</option>
                            <option>2:00 PM</option>
                            <option>2:15 PM</option>
                            <option>2:30 PM</option>
                            <option>2:45 PM</option>
                            <option>3:00 PM</option>
                            <option>3:15 PM</option>
                            <option>3:30 PM</option>
                            <option>3:45 PM</option>
                            <option>4:00 PM</option>
                            <option>4:15 PM</option>
                            <option>4:30 PM</option>
                            <option>4:45 PM</option>
                            <option>5:00 PM</option>
                            <option>5:15 PM</option>
                            <option>5:30 PM</option>
                            <option>5:45 PM</option>
                            <option>6:00 PM</option>
                            <option>6:15 PM</option>
                            <option>6:30 PM</option>
                            <option>6:45 PM</option>
                            <option>7:00 PM</option>
                            <option>7:15 PM</option>
                            <option>7:30 PM</option>
                            <option>7:45 PM</option>
                            <option>8:00 PM</option>
                            <option>8:15 PM</option>
                            <option>8:30 PM</option>
                            <option>8:45 PM</option>
                            <option>9:00 PM</option>
                            <option>9:15 PM</option>
                            <option>9:30 PM</option>
                            <option>9:45 PM</option>
                            <option>10:00 PM</option>
                            <option>10:15 PM</option>
                            <option>10:30 PM</option>
                            <option>10:45 PM</option>
                            <option>11:00 PM</option>
                            <option>11:15 PM</option>
                            <option>11:30 PM</option>
                            <option>11:45 PM</option>
                          </select>
                        </div>
                      </>
                    )}
                  </div>
                </div>

                <div className="bg-light rounded-3 mb-3 mt-0">
                  <div className="col-sm-12 mb-4 mt-2">
                    <label for="text-input" className="form-label">
                      More Info URL
                    </label>
                    <br />
                    <div className="col-sm-12 mb-4 d-flex">
                      <input
                        className="form-control"
                        type="text"
                        id="text-input"
                        name="infoUrl"
                        value={formValues.infoUrl}
                        onChange={handleChange}
                      />
                      <button
                        className="btn btn-primary btn-sm ms-3"
                        type="button"
                      >
                        Test Link
                      </button>
                    </div>
                  </div>
                </div>

                <div className="bg-light rounded-3 mb-3 mt-4">
                  <div className="row">
                    <div
                      className="col-sm-4 mb-4"
                      onClick={(e) => CheckRegistration(e)}
                    >
                      <h6>Allow registration?</h6>
                      <div className="d-flex">
                        <div className="form-check ps-3">
                          <input
                            className="form-check-input"
                            id="form-radio-4"
                            type="radio"
                            name="registration"
                            value="no"
                            defaultChecked
                          />
                          <label
                            className="form-check-label ms-2"
                            id="radio-level1"
                          >
                            No
                          </label>
                        </div>

                        <div className="form-check">
                          <input
                            className="form-check-input "
                            id="form-radio-4"
                            type="radio"
                            name="registration"
                            value="yes"
                          />
                          <label
                            className="form-check-label ms-2"
                            id="radio-level1"
                          >
                            Yes
                          </label>
                        </div>
                      </div>
                    </div>

                    <div
                      className="col-sm-4 mb-4"
                      onClick={(e) => CheckInformation(e)}
                    >
                      <h6>Ask for information?</h6>
                      <div className="d-flex">
                        <div className="form-check ps-3">
                          <input
                            className="form-check-input"
                            id="form-radio-6"
                            type="radio"
                            name="information"
                            value="no"
                            defaultChecked
                          />
                          <label className="form-check-label" id="radio-level4">
                            No
                          </label>
                        </div>
                        <div className="form-check ms-2">
                          <input
                            className="form-check-input"
                            id="form-radio-6"
                            type="radio"
                            name="information"
                            value="yes"
                          />
                          <label className="form-check-label" id="radio-level4">
                            Yes
                          </label>
                        </div>
                      </div>
                    </div>

                    <div
                      className="col-sm-4 mb-4"
                      onClick={(e) => CheckGuest(e)}
                    >
                      <h6>Allow guest seats?</h6>
                      <div className="d-flex">
                        <div className="form-check ps-3">
                          <input
                            className="form-check-input"
                            id="form-radio-5"
                            type="radio"
                            name="guestSeats"
                            onChange={handleChange}
                            value="no"
                            defaultChecked
                          />
                          <label className="form-check-label" id="radio-level3">
                            No
                          </label>
                        </div>
                        <div className="form-check ms-2">
                          <input
                            className="form-check-input"
                            id="form-radio-5"
                            type="radio"
                            name="guestSeats"
                            value="yes"
                          />
                          <label className="form-check-label" id="radio-level3">
                            Yes
                          </label>
                        </div>
                      </div>
                    </div>

                    {checkRegistration === "yes" ? (
                      <div className="col-sm-4 mb-4">
                        <label className="form-label">Seat Count</label>
                        <input
                          className="form-control"
                          id="inline-form-input"
                          type="text"
                          placeholder=""
                          name="seatCount"
                          value={formValues.seatCount}
                          onChange={handleChange}
                        />
                      </div>
                    ) : (
                      ""
                    )}

                    {checkInformation === "yes" ? (
                      <div className="col-sm-4 mb-4">
                        <label className="form-label">Max Seats</label>
                        <br />
                        <select
                          className="form-select "
                          id="pr-city"
                          name="maxSeats"
                          value={formValues.maxSeats}
                          onChange={handleChange}
                        >
                          <option>Agent +1</option>
                          <option>Agent +2</option>
                          <option>Agent +3</option>
                          <option>Agent +4</option>
                          <option>Agent +5</option>
                          <option>Agent +6</option>
                          <option>Agent +7</option>
                          <option>Agent +8</option>
                          <option>Agent +9</option>
                        </select>
                      </div>
                    ) : (
                      ""
                    )}
                    {checkGuest === "yes" ? (
                      <div className="col-sm-4 mb-4">
                        <label className="form-label">
                          Question Shown: (50 chrs)
                        </label>
                        <input
                          className="form-control"
                          id="inline-form-input"
                          type="text"
                          placeholder=""
                          name="question"
                          value={formValues.question}
                          onChange={handleChange}
                        />
                        {/* <div className="invalid-tooltip">{formErrors.associateName}</div> */}
                      </div>
                    ) : (
                      ""
                    )}
                  </div>
                  <div className="row">
                    <div className="col-sm-12 mb-2">
                      <h6>Public event</h6>
                      <div
                        className="d-flex"
                        onClick={(e) => CheckPublicEvent(e)}
                      >
                        <div className="form-check">
                          <input
                            className="form-check-input"
                            id="form-radio-7"
                            type="radio"
                            name="publicEvent"
                            value="yes"
                            defaultChecked
                          />
                          <label className="form-check-label" id="radio-level7">
                            Yes, Show to All
                          </label>
                        </div>

                        <div className="form-check ms-2">
                          <input
                            className="form-check-input"
                            id="form-radio-7"
                            type="radio"
                            name="publicEvent"
                            value="no"
                          />
                          <label className="form-check-label" id="radio-level7">
                            No, Only Associates
                          </label>
                        </div>
                      </div>
                    </div>

                    <div className="col-sm-12 mb-2">
                      <h6>Save as Draft</h6>
                      <div className="d-flex" onClick={(e) => CheckedDraft(e)}>
                        <div className="form-check ps-3">
                          <input
                            className="form-check-input"
                            id="form-radio-4"
                            type="radio"
                            name="draft"
                            value="yes"
                            defaultChecked
                          />
                          <label
                            className="form-check-label ms-2"
                            id="radio-level1"
                          >
                            Yes, Hide from associates.
                          </label>
                        </div>
                        <div className="form-check ms-2">
                          <input
                            className="form-check-input"
                            id="form-radio-4"
                            type="radio"
                            name="draft"
                            value="no"
                          />
                          <label
                            className="form-check-label ms-2"
                            id="radio-level1"
                          >
                            No, Show event.
                          </label>
                        </div>
                      </div>
                    </div>

                    <div
                      className="col-sm-6"
                      onClick={(e) => CheckRepetingEvent(e)}
                    >
                      <div className="form-check">
                        <input
                          className="form-check-input"
                          id="form-check-1"
                          type="checkbox"
                          name="repeatingEvent"
                          value="yes"
                          defaultChecked
                        />
                        <label className="form-check-label" for="form-check-1">
                          Once saved, show me options to copy this repeating
                          event. &nbsp;{" "}
                          <a href="#">
                            <i
                              className="fa fa-question-circle"
                              aria-hidden="true"
                            ></i>
                          </a>
                        </label>
                      </div>
                    </div>

                    <div
                      className="col-sm-6"
                      onClick={(e) => CheckAnotherEvent(e)}
                    >
                      <div className="form-check">
                        <input
                          className="form-check-input"
                          id="form-check-2"
                          type="checkbox"
                          name="anotherEvent"
                          value="yes"
                          defaultChecked
                        />
                        <label className="form-check-label" for="form-check-2">
                          Once saved, let me quickly create another event.
                          &nbsp;{" "}
                          <a href="#">
                            <i
                              className="fa fa-question-circle"
                              aria-hidden="true"
                            ></i>
                          </a>
                        </label>
                      </div>
                    </div>

                    <label className="form-check-label" for="form-check-2">
                      NOTE: Enter times local to your office (MDT)
                    </label>
                  </div>
                </div>

                <div className="bg-light rounded-3 mb-3 mt-4">
                  <h6>Event Description</h6>
                  <div className="float-left w-100 mt-2">
                    <Editor
                      editorState={editorState}
                      onEditorStateChange={handleChanges}
                      value={formValues.eventDescription}
                      toolbar={{
                        options: [
                          "inline",
                          "blockType",
                          "fontSize",
                          "list",
                          "textAlign",
                          "history",
                          "link", // Add link option here
                        ],
                        inline: {
                          options: [
                            "bold",
                            "italic",
                            "underline",
                            "strikethrough",
                          ],
                        },
                        list: { options: ["unordered", "ordered"] },
                        textAlign: {
                          options: ["left", "center", "right"],
                        },
                        history: { options: ["undo", "redo"] },
                        link: {
                          // Configure link options
                          options: ["link", "unlink"],
                        },
                      }}
                      wrapperClassName="demo-wrapper"
                      editorClassName="demo-editor"
                    />
                  </div>
                </div>
              </div>

              <div className="pull-right mt-4">
                <button
                  onClick={handleSubmit}
                  type="button"
                  className="btn btn-primary btn-sm"
                  id="save-button"
                >
                  Add Event now
                </button>
                <NavLink
                  to="/calendar"
                  type="button"
                  className="btn btn-secondary btn-sm ms-3"
                  id="closeModal"
                  data-dismiss="modal"
                >
                  Cancel
                </NavLink>
              </div>
            </div>

            <div className="col-md-4 mt-lg-0 mt-md-0 mt-sm-0 mt-4">
              <div id="accordionCards" className="">
                <div
                  className="card bg-secondary mb-2 mt-0"
                  data-bs-toggle="collapse"
                  data-bs-target="#cardCollapse1"
                >
                  <div className="card-body">
                    <h3 className="h3 card-title pt-1 mb-0">
                      💡 Office Resource
                    </h3>
                  </div>
                  <div
                    className="collapse show"
                    id="cardCollapse1"
                    data-bs-parent="#accordionCards"
                  >
                    <div className="card-body mt-n1 pt-0">
                      <p className="fs-sm">
                        <strong>
                          Conference Rooms are available Monday-Friday 8AM-5PM —
                          Corporate
                        </strong>
                        <br />
                        <br />
                        Conferance Rooms must be booked by emailing
                        admin@riverparksuites.com with the date , time and what
                        room you would like to use.
                      </p>
                      <p className="fs-sm">
                        <strong>Meeting room/ Day Office — Corporate</strong>{" "}
                        <br />
                        <br />
                        Setup includes a desk, 2 guest chairs, phone, and
                        internet connection. To book email:
                        admin@riverparksuites.com with the date, time and what
                        room you would like to use.
                      </p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </main>
    </div>
  );
};

export default AddEvent;

import React, { useState, useEffect } from "react";
import data from "../../Pages/data.js";
import { NavLink, useLocation } from "react-router-dom";
import OverlayTrigger from 'react-bootstrap/OverlayTrigger'
import Tooltip from 'react-bootstrap/Tooltip'
import Button from 'react-bootstrap/Button'
// import "../../style.css"
import jwt from "jwt-decode";

function Sidebar(props) {
  const handleclose = (e) => {
    document.getElementById("modalconnectclose").click();
    //navigate("/control-panel/documents/")
  };
  return (
    <>
      {localStorage.getItem("auth") ? (
        <>
          <aside id="left_sidebar" className="left_sidebar">
            <ul>
              <li>
                <OverlayTrigger
                  placement='right'
                  overlay={<Tooltip>Home</Tooltip>}
                >
                 <NavLink to="/"><i class="h2 fi-home opacity-80 me-2 text-light mb-0"></i><span>Home</span>
                  </NavLink>
                </OverlayTrigger>
              </li>
              <li>
                <OverlayTrigger
                  placement='right'
                  overlay={<Tooltip>Dashboard</Tooltip>}
                >
                  <NavLink to="/">
                    <i class="h2 fi-dashboard opacity-80 me-2 text-light mb-0"></i><span>Dashboard</span>
                  </NavLink>
                </OverlayTrigger>
              </li>
              <li>
                <OverlayTrigger
                  placement='right'
                  overlay={<Tooltip>My Info</Tooltip>}
                >
                  <NavLink
                    to={`/contact-profile/${jwt(localStorage.getItem("auth")).id
                      }`}
                  >
                    <i class="h2 fi-info-circle opacity-80 me-2 text-light mb-0"></i> <span>My Info</span>
                  </NavLink>
                </OverlayTrigger>
              </li>
              <li>
                <OverlayTrigger
                  placement='right'
                  overlay={<Tooltip>My Listings</Tooltip>}
                >
                  <NavLink to="/listing">
                    <i class="h2 fi-list opacity-80 me-2 text-light mb-0"></i><span>My Listings</span>
                  </NavLink>
                </OverlayTrigger>
              </li>

              {jwt(localStorage.getItem("auth")).contactType == "admin" ? (
                <li>
                  <OverlayTrigger
                    placement='right'
                    overlay={<Tooltip>Email/Notices</Tooltip>}
                  >
                    <a href="#" data-toggle="modal" data-target="#modalconnect">
                      <i class="h2 fi-mail opacity-80 me-2 text-light mb-0"></i> <span>Email/Notices</span>
                    </a>
                  </OverlayTrigger>                 
                </li>
              ) : (
                ""
              )}

              <li>
                <OverlayTrigger
                  placement='right'
                  overlay={<Tooltip>Contact</Tooltip>}
                >
                  <NavLink to="/contact">
                    <i class="h2 fi-phone opacity-80 me-2 text-light mb-0"></i><span>Contact</span>
                  </NavLink>
                </OverlayTrigger>
              </li>
              <li>
                <OverlayTrigger
                  placement='right'
                  overlay={<Tooltip>Market Calender</Tooltip>}
                >
                  <NavLink to="/market-calendar">
                    <i class="h2 fi-calendar opacity-80 me-2 text-light mb-0"></i><span>Market Calender</span>
                  </NavLink>
                </OverlayTrigger>
              </li>
              <li>
                <OverlayTrigger
                  placement='right'
                  overlay={<Tooltip>Utility Lookup Tool</Tooltip>}
                >
                  <NavLink to="/agent-utility">
                    <i class="fa fa-wrench opacity-80 me-2 text-light mb-0"></i><span>Utility Lookup Tool</span>
                  </NavLink>
                </OverlayTrigger>
              </li>
              <li>
                <OverlayTrigger
                  placement='right'
                  overlay={<Tooltip>My Business</Tooltip>}
                >
                  <NavLink to="/bussiness-tab">
                    <i class="fa fa-briefcase opacity-80 me-2 text-light mb-0"></i><span>My Business</span>
                  </NavLink>
                </OverlayTrigger>
              </li>

              <li>
                <OverlayTrigger
                  placement='right'
                  overlay={<Tooltip>Add Reservations</Tooltip>}
                >
                  <NavLink to="/reservation-listing">
                    <i class="fa fa-plus-square opacity-80 me-2 text-light mb-0"></i><span>Add Reservations</span>
                  </NavLink>
                </OverlayTrigger>
              </li>

              <li>
                <OverlayTrigger
                  placement='right'
                  overlay={<Tooltip>Notifications</Tooltip>}
                >
                  <NavLink to="/notice/notification">
                    <i class="fa fa-bell opacity-80 me-2 text-light mb-0"></i><span>Notifications</span>
                  </NavLink>
                </OverlayTrigger>
              </li>

              <li>
                <OverlayTrigger
                  placement='right'
                  overlay={<Tooltip>Print Marketing Center</Tooltip>}
                >
                  <NavLink to="/print-markting">
                    <i class="fa fa-file opacity-80 me-2 text-light mb-0"></i><span>Print Marketing Center</span>
                  </NavLink>
                </OverlayTrigger>
              </li>

              {/* <li>
                <OverlayTrigger
                  placement='right'
                  overlay={<Tooltip>Printed Listing Brochures</Tooltip>}
                >
                  <NavLink to="/brochure-mls-print">
                    <i class="h2 fi-file opacity-80 me-2 text-light mb-0"></i><span>Printed Listing Brochures</span>
                  </NavLink>
                </OverlayTrigger>
              </li> */}


              <li>
              <OverlayTrigger
                  placement='right'
                  overlay={<Tooltip>Login with Google</Tooltip>}
                >
                  <NavLink to="/google-login">
                  <i class="h2 fi-google opacity-100 me-2 text-light mb-0"></i>
                   <span>Login with Google</span>
                  </NavLink>
                </OverlayTrigger>
              </li>
            </ul>
          </aside>

          <div className="modal in" role="dialog" id="modalconnect">
            <div
              className="modal-dialog modal-lg modal-dialog-scrollable"
              role="document"
            >
              <div className="modal-content">
                <div className="modal-header">
                  <h4 className="modal-title">Send a Message</h4>
                  <button
                    className="btn-close"
                    type="button"
                    id="modalconnectclose"
                    data-dismiss="modal"
                    aria-label="Close"
                  ></button>
                </div>
                <div className="modal-body fs-sm">
                  <div className="d-flex alifn-items-center justify-content-center">
                    <NavLink
                      className="btn btn-primary"
                      to="/admin/announce"
                      onClick={(e) => handleclose(e)}
                    >
                      Send Bulk Email
                    </NavLink>
                    <NavLink
                      className="btn btn-primary ms-3"
                      to="/send/notice"
                      onClick={(e) => handleclose(e)}
                    >
                      Send a Notice
                    </NavLink>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </>
      ) : (
        ""
      )}
    </>
  );
}
export default Sidebar;

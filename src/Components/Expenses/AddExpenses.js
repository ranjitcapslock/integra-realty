import React, { useState, useEffect } from "react";
import { useNavigate, useParams } from "react-router-dom";
import user_service from "../service/user_service";
import Loader from "../../Pages/Loader/Loader.js";
import Toaster from "../../Pages/Toaster/Toaster.js";
import _ from "lodash";
import AllTab from "../../Pages/AllTab";
import jwt from "jwt-decode";
import { NavLink } from "react-bootstrap";

const AddExpenses = () => {
  const [loader, setLoader] = useState({ isActive: null });
  const { isActive } = loader;
  const [toaster, setToaster] = useState({
    types: null,
    isShow: null,
    toasterBody: "",
    message: "",
  });
  const { types, isShow, toasterBody, message } = toaster;

  const initialValues = {
    PaidFor: "",
    amountPaid: "",
    description: "",
    entryDate: "",
    PaidTo: "",
    type: "expenses",
  };

  const [formValues, setFormValues] = useState(initialValues);
  const [formErrors, setFormErrors] = useState({});
  const [isSubmitClick, setISSubmitClick] = useState(false);

  const navigate = useNavigate();
  const params = useParams();
  const [expense, setExpense] = useState("expenses");
  const [summary, setSummary] = useState("");

  const handleChangeExpenses = (event) => {
    const select = event.target.value;
    setFormValues({
      ...formValues,
      type: select,
    });
  };

  const TransactionGetById = async () => {
    await user_service.transactionGetById(params.id).then((response) => {
      if (response) {
        setSummary(response.data);
        setTimeout(() => {
          setLoader({ isActive: false });
        }, 9000);
      }
    });
  };
  {
    /* <!-- Input onChange Start--> */
  }
  const handleChange = (e) => {
    const { name, value } = e.target;
    setFormValues({ ...formValues, [name]: value });
  };
  {
    /* <!-- Input onChange End--> */
  }

  {
    /* <!-- Form Validation Start--> */
  }
  useEffect(() => {
    if (formValues && isSubmitClick) {
      validate();
    }
  }, [formValues]);

  const validate = () => {
    const values = formValues;
    const errors = {};

    if (!values.entryDate) {
      errors.entryDate = "field is required";
    }

    if (!values.PaidTo) {
      errors.PaidTo = "field is required";
    }

    if (!values.PaidFor) {
      errors.PaidFor = "field is required";
    }

    if (!values.amountPaid) {
      errors.amountPaid = "field required type number";
    }
    if (!values.description) {
      errors.description = "field is required";
    }

    if (!values.PaidTo) {
      errors.PaidTo = "field is required";
    }
    if (formValues.type === "mileage") {
      if (!values.distance) {
        errors.distance = "field required type number";
      }
    } else {
    }
    setFormErrors(errors);
    return errors;
  };
  {
    /* <!-- Form Validation End--> */
  }

  const ExpensesAll = async (e) => {
    if (params.addExpence) {
      e.preventDefault();
      setISSubmitClick(true);
      let checkValue = validate();
      if (_.isEmpty(checkValue)) {
        const userData = {
          type: formValues.type,
          entryDate: formValues.entryDate,
          PaidFor: formValues.PaidFor,
          amountPaid: formValues.amountPaid,
          description: formValues.description,
          PaidTo: formValues.PaidTo,
          transactionId: params.id,
          distance: formValues.distance ?? "",
        };
        try {
          setLoader({ isActive: true });
          await user_service
            .expensesCreateUpdate(params.addExpence, userData)
            .then((response) => {
              if (response) {
                const userDatan = {
                  addedBy: jwt(localStorage.getItem("auth")).id,
                  agentId: jwt(localStorage.getItem("auth")).id,
                  transactionId: params.id,
                  message: "Transaction Expense Update",
                  // transaction_property_address: "ADDRESS 2",
                  note_type: "Expense",
                };
                const responsen = user_service.transactionNotes(userDatan);

                setLoader({ isActive: false });
                setToaster({
                  type: "Expense Updated Successfully",
                  isShow: true,
                  toasterBody: response.data.message,
                  message: "Expense Updated Successfully",
                });
                setTimeout(() => {
                  setToaster((prevToaster) => ({
                    ...prevToaster,
                    isShow: false,
                  }));
                  navigate(`/expenses/${params.id}`);
                }, 2000);
              } else {
                setLoader({ isActive: false });
                setTimeout(() => {
                  setToaster({
                    types: "error",
                    isShow: false,
                    toasterBody: null,
                    message: "Error",
                  });
                }, 2000);
              }
            });
        } catch (err) {
          setToaster({
            type: "API Error",
            isShow: true,
            toasterBody: err.response.data.message,
            message: "API Error",
          });

          setTimeout(() => {
            setToaster((prevToaster) => ({ ...prevToaster, isShow: false }));
          }, 2000);
        }
      }
    } else {
      e.preventDefault();
      setISSubmitClick(true);
      let checkValue = validate();
      if (_.isEmpty(checkValue)) {
        const userData = {
          type: formValues.type,
          entryDate: formValues.entryDate,
          PaidFor: formValues.PaidFor,
          amountPaid: formValues.amountPaid,
          description: formValues.description,
          PaidTo: formValues.PaidTo,
          transactionId: params.id,
          distance: formValues.distance ?? "",
        };
        try {
          setLoader({ isActive: true });
          await user_service.expensesCreate(userData).then((response) => {
            if (response) {
              const userDatan = {
                addedBy: jwt(localStorage.getItem("auth")).id,
                agentId: jwt(localStorage.getItem("auth")).id,
                transactionId: params.id,
                message: "Transaction Expense Add",
                // transaction_property_address: "ADDRESS 2",
                note_type: "Expense",
              };
              const responsen = user_service.transactionNotes(userDatan);
              setLoader({ isActive: false });
              setToaster({
                type: "Expense Added Successfully",
                isShow: true,
                toasterBody: response.data.message,
                message: "Expense Added Successfully",
              });
              setTimeout(() => {
                setToaster((prevToaster) => ({
                  ...prevToaster,
                  isShow: false,
                }));
                navigate(`/expenses/${params.id}`);
              }, 2000);
            } else {
              setLoader({ isActive: false });
              setTimeout(() => {
                setToaster({
                  types: "error",
                  isShow: false,
                  toasterBody: null,
                  message: "Error",
                });
              }, 2000);
            }
          });
        } catch (err) {
          setLoader({ isActive: false });

          setToaster({
            type: "API Error",
            isShow: true,
            toasterBody: err.response.data.message,
            message: "API Error",
          });

          setTimeout(() => {
            setToaster((prevToaster) => ({ ...prevToaster, isShow: false }));
          }, 2000);
        }
      }
    }
  };

  useEffect(() => {
    window.scrollTo(0, 0);
    if (params.addExpence) {
      ExpenceGetId();
    }
    TransactionGetById(params.id);
  }, []);

  const ExpenceGetId = async () => {
    await user_service.typeExpensesGetId(params.addExpence).then((response) => {
      if (response) {
        setFormValues(response.data);
        setExpense(response.data.type);
      }
    });
  };

  const Cancel = () => {
    navigate(`/expenses/${params.id}`);
  };

  return (
    <div className="bg-secondary float-left w-100 pt-4 mb-4">
      <Loader isActive={isActive} />
      {isShow && (
        <Toaster
          types={types}
          isShow={isShow}
          toasterBody={toasterBody}
          message={message}
        />
      )}
      <main className="page-wrapper">
        {/* <AllTab transaction_data = {summary}/> */}
        {/* <!-- Page container--> */}
        <div className="mb-md-4">
          <div className="">
            {/* <!-- Add Expense contant Starts here--> */}
            <div className="content-overlay transaction_tab_view mt-0">
              <div className="row">
                <AllTab />
                <div className="col-md-12">
                  <div className="d-flex align-items-center justify-content-between mb-4">
                    <h3 className="text-white mb-0">Add Transaction Expense</h3>
                  </div>
                  <div className="bg-light border rounded-3 p-3">
                    <h6 className="">What type of expenses?</h6>
                    <div className="row">
                      <div className="col-md-4">
                        <div className="alert alert-secondary" role="alert">
                          <div className="form-check ps-0 mb-0">
                            <input
                              className="form-check-input me-2 ms-0"
                              id="form-radio-1"
                              type="radio"
                              name="type"
                              value="expenses"
                              onChange={(e) => handleChangeExpenses(e)}
                              checked={
                                formValues?.type === "expenses" ||
                                !formValues.type
                              }
                            />
                            <label
                              className="col-form-label p-0"
                              id="label-expenses"
                            >
                              Marketing Expenses
                            </label>
                            <p className="text-expense-two mt-2 mb-0">
                              Record a purchase or service for this transaction.
                            </p>
                          </div>
                        </div>
                      </div>
                      <div className="col-md-4">
                        <div className="alert alert-secondary" role="alert">
                          <div className="form-check ps-0 mb-0">
                            <input
                              className="form-check-input me-2 ms-0"
                              id="form-radio-1"
                              type="radio"
                              name="type"
                              value="mileage"
                              onChange={(e) => handleChangeExpenses(e)}
                              checked={
                                formValues?.type === "mileage" ||
                                !formValues.type
                              }
                            />
                            <label
                              className="col-form-label p-0"
                              id="label-expenses"
                            >
                              Vehicle Mileage
                            </label>
                            <p className="text-expense-two mt-2 mb-0">
                              Log showing tours and trips for this transaction.
                            </p>
                          </div>
                        </div>
                      </div>

                      <div className="col-md-4 text-center">
                        <div className="alert alert-secondary" role="alert">
                          <div className="form-check ps-0 mb-0">
                            <a
                              href="https://www.timetrex.com/resources/us-employee-mileage-reimbursement-calculator"
                              target="_blank"
                              rel="noopener noreferrer"
                            >
                              IRS Mileage Calculator
                            </a>
                          </div>
                        </div>
                      </div>
                    </div>

                    {formValues.type === "expenses" ? (
                      <div className="row">
                        <h6 className="">Expense Details</h6>
                        <div className="col-md-6">
                          <label className="col-form-label" id="label-expenses">
                            Entry Date *
                          </label>
                          <input
                            className="form-control"
                            id="inline-form-input"
                            type="date"
                            placeholder="Choose date and time"
                            name="entryDate"
                            onChange={handleChange}
                            autoComplete="on"
                            style={{
                              border: formErrors?.entryDate
                                ? "1px solid red"
                                : "1px solid #00000026",
                            }}
                            value={formValues.entryDate}
                          />
                          {formErrors.entryDate && (
                            <div className="invalid-tooltip">
                              {formErrors.entryDate}
                            </div>
                          )}
                        </div>
                        <div className="col-md-6">
                          <label className="col-form-label" id="label-expenses">
                            Amount Paid *
                          </label>
                          <input
                            className="form-control"
                            id="inline-form-input"
                            type="number"
                            placeholder="Enter your Amount"
                            name="amountPaid"
                            onChange={handleChange}
                            autoComplete="on"
                            style={{
                              border: formErrors?.amountPaid
                                ? "1px solid red"
                                : "1px solid #00000026",
                            }}
                            value={formValues.amountPaid}
                          />
                          {formErrors.amountPaid && (
                            <div className="invalid-tooltip">
                              {formErrors.amountPaid}
                            </div>
                          )}
                        </div>
                        <div className="col-md-6 mt-2">
                          <label className="col-form-label" id="label-expenses">
                            Paid For / Reason *
                          </label>
                          <input
                            className="form-control"
                            id="inline-form-input"
                            type="text"
                            placeholder="Paid For"
                            name="PaidFor"
                            onChange={handleChange}
                            autoComplete="on"
                            style={{
                              border: formErrors?.PaidFor
                                ? "1px solid red"
                                : "1px solid #00000026",
                            }}
                            value={formValues.PaidFor}
                          />
                          {formErrors.PaidFor && (
                            <div className="invalid-tooltip">
                              {formErrors.PaidFor}
                            </div>
                          )}
                        </div>
                        <div className="col-md-6 mt-2">
                          <label className="col-form-label" id="label-expenses">
                            Paid To
                          </label>
                          <input
                            className="form-control"
                            id="inline-form-input"
                            type="text"
                            placeholder="Paid To"
                            name="PaidTo"
                            onChange={handleChange}
                            autoComplete="on"
                            style={{
                              border: formErrors?.PaidTo
                                ? "1px solid red"
                                : "1px solid #00000026",
                            }}
                            value={formValues.PaidTo}
                          />
                          {formErrors.PaidTo && (
                            <div className="invalid-tooltip">
                              {formErrors.PaidTo}
                            </div>
                          )}
                        </div>
                        <div className="col-md-12 mt-2">
                          <label className="col-form-label" id="label-expenses">
                            Description
                          </label>
                          <input
                            className="form-control"
                            id="inline-form-input"
                            type="text"
                            placeholder="description"
                            name="description"
                            onChange={handleChange}
                            autoComplete="on"
                            style={{
                              border: formErrors?.description
                                ? "1px solid red"
                                : "1px solid #00000026",
                            }}
                            value={formValues.description}
                          />
                          {formErrors.description && (
                            <div className="invalid-tooltip">
                              {formErrors.description}
                            </div>
                          )}
                        </div>
                      </div>
                    ) : (
                      ""
                    )}

                    {formValues.type === "mileage" ? (
                      <div className="row">
                        <h6 className="">Expense Vehicle</h6>
                        <div className="col-md-6">
                          <label className="col-form-label" id="label-expenses">
                            Entry Date *
                          </label>
                          <input
                            className="form-control"
                            id="inline-form-input"
                            type="date"
                            placeholder="Choose date and time"
                            name="entryDate"
                            onChange={handleChange}
                            autoComplete="on"
                            style={{
                              border: formErrors?.entryDate
                                ? "1px solid red"
                                : "1px solid #00000026",
                            }}
                            value={formValues.entryDate}
                          />

                          {formErrors.entryDate && (
                            <div className="invalid-tooltip">
                              {formErrors.entryDate}
                            </div>
                          )}
                        </div>
                        <div className="col-md-6">
                          <label className="col-form-label" id="label-expenses">
                            Amount Paid *
                          </label>
                          <input
                            className="form-control"
                            id="inline-form-input"
                            type="number"
                            placeholder="Enter your Amount"
                            name="amountPaid"
                            onChange={handleChange}
                            autoComplete="on"
                            style={{
                              border: formErrors?.amountPaid
                                ? "1px solid red"
                                : "1px solid #00000026",
                            }}
                            value={formValues.amountPaid}
                          />
                          {formErrors.amountPaid && (
                            <div className="invalid-tooltip">
                              {formErrors.amountPaid}
                            </div>
                          )}
                        </div>
                        <div className="col-md-6 mt-2">
                          <label className="col-form-label" id="label-expenses">
                            Trip For*
                          </label>
                          <input
                            className="form-control"
                            id="inline-form-input"
                            type="text"
                            placeholder="Trip For"
                            name="PaidFor"
                            onChange={handleChange}
                            autoComplete="on"
                            style={{
                              border: formErrors?.PaidFor
                                ? "1px solid red"
                                : "1px solid #00000026",
                            }}
                            value={formValues.PaidFor}
                          />
                          {formErrors.PaidFor && (
                            <div className="invalid-tooltip">
                              {formErrors.PaidFor}
                            </div>
                          )}
                        </div>
                        <div className="col-md-6 mt-2">
                          <label className="col-form-label" id="label-expenses">
                            Destination
                          </label>
                          <input
                            className="form-control"
                            id="inline-form-input"
                            type="text"
                            placeholder="destination"
                            name="PaidTo"
                            onChange={handleChange}
                            autoComplete="on"
                            style={{
                              border: formErrors?.PaidTo
                                ? "1px solid red"
                                : "1px solid #00000026",
                            }}
                            value={formValues.PaidTo}
                          />
                          {formErrors.PaidTo && (
                            <div className="invalid-tooltip">
                              {formErrors.PaidTo}
                            </div>
                          )}
                        </div>
                        <div className="col-md-6 mt-2">
                          <label className="col-form-label" id="label-expenses">
                            Description
                          </label>
                          <input
                            className="form-control"
                            id="inline-form-input"
                            type="text"
                            placeholder="description"
                            name="description"
                            onChange={handleChange}
                            autoComplete="on"
                            style={{
                              border: formErrors?.description
                                ? "1px solid red"
                                : "1px solid #00000026",
                            }}
                            value={formValues.description}
                          />
                          {formErrors.description && (
                            <div className="invalid-tooltip">
                              {formErrors.description}
                            </div>
                          )}
                        </div>

                        <div className="col-md-6  mt-2">
                          <label className="col-form-label" id="label-expenses">
                            Distance
                          </label>
                          <input
                            className="form-control"
                            id="inline-form-input"
                            type="number"
                            placeholder="Enter your Distance"
                            name="distance"
                            onChange={handleChange}
                            autoComplete="on"
                            style={{
                              border: formErrors?.distance
                                ? "1px solid red"
                                : "1px solid #00000026",
                            }}
                            value={formValues.distance}
                          />
                          {formErrors.distance && (
                            <div className="invalid-tooltip">
                              {formErrors.distance}
                            </div>
                          )}
                        </div>
                      </div>
                    ) : (
                      ""
                    )}
                  </div>
                  <div className="pull-right mt-3">
                    <a
                      className="btn btn-secondary btn-sm pull-right ms-3 "
                      onClick={Cancel}
                    >
                      Cancel & Go Back
                    </a>

                    {summary.filing === "filed_complted" ? (
                      <button
                        type="button"
                        className="btn btn-primary btn-sm order-lg-3 pull-right"
                      >
                        Add
                      </button>
                    ) : (
                      <button
                        type="button"
                        className="btn btn-primary btn-sm order-lg-3 pull-right"
                        onClick={ExpensesAll}
                      >
                        Add
                      </button>
                    )}
                  </div>
                </div>
              </div>
            </div>

            {/* <!-- Add Expense contant End here--> */}
          </div>
        </div>
      </main>
    </div>
  );
};

export default AddExpenses;

import React, { useState, useEffect } from "react";
import { useNavigate, useParams, NavLink } from "react-router-dom";
import user_service from "../service/user_service";
import Loader from "../../Pages/Loader/Loader.js";
import Toaster from "../../Pages/Toaster/Toaster";
import ReactPaginate from "react-paginate";
import AllTab from "../../Pages/AllTab";
import moment from "moment";
// import StatusBar from "../../Pages/StatusBar.js";
import jwt from "jwt-decode";

const Expenses = () => {
  const [loader, setLoader] = useState({ isActive: null });
  const { isActive } = loader;
  const [toaster, setToaster] = useState({
    types: null,
    isShow: null,
    toasterBody: "",
    message: "",
  });
  const { types, isShow, toasterBody, message } = toaster;

  const [pageCount, setPageCount] = useState(10);
  const [expenceCount, setExpenceCount] = useState(0);
  const [mileageCount, setMileageCount] = useState(0);

  const [getExpenses, setGetExpenses] = useState([]);
  const navigate = useNavigate();
  const [summary, setSummary] = useState("");
  const ExpensesGetAll = async () => {
    setLoader({ isActive: true });
    await user_service.expensesGet(params.id).then((response) => {
      setLoader({ isActive: false });
      if (response) {
        const groupedBycategory = response.data.data.reduce((acc, item) => {
          const type = item.type;
          if (!acc[type]) {
            acc[type] = [];
          }
          acc[type].push(item);
          return acc;
        }, {});
        setGetExpenses(groupedBycategory);

        let expenseTotal = 0;
        let mileageTotal = 0;

        response.data.data.forEach((item) => {
          console.log(item.expenses);
          if (item.type === "expenses") {
            expenseTotal += parseInt(item.amountPaid);
          } else if (item.type === "mileage") {
            mileageTotal += parseInt(item.amountPaid);
          }
        });
        setExpenceCount(expenseTotal);
        setMileageCount(mileageTotal);
      }
    });
  };

  // const handlePage = async (data) => {
  //     let currentPage = data.selected + 1;
  //     let skip = (currentPage - 1) + currentPage;
  //     setLoader({ isActive: true })
  //     await user_service.typeExpensesGet(currentPage, skip).then((response) => {
  //         setLoader({ isActive: false })
  //         if (response) {
  //             setGetExpenses(response.data.data);
  //             setPageCount(Math.ceil(response.data.totalRecords / 10));
  //         }
  //     });
  // }

  // paginate Function

  useEffect(() => {
    window.scrollTo(0, 0);
    setLoader({ isActive: true });
    TransactionGetById(params.id);
    ExpensesGetAll();
  }, []);


  const params = useParams();

  const TransactionGetById = async () => {
    await user_service.transactionGetById(params.id).then((response) => {
      if (response) {
        setSummary(response.data);
        setTimeout(() => {
          setLoader({ isActive: false });
        }, 9000);
      }
    });
  };

  const handleRemove = async (id) => {
    setLoader({ isActive: true });
    await user_service.expenceDelete(id).then((response) => {
      if (response) {
        const ExpensesGetAll = async () => {
          setLoader({ isActive: true });
          await user_service.expensesGet(params.id).then((response) => {
            setLoader({ isActive: false });
            if (response) {
              const userDatan = {
                addedBy: jwt(localStorage.getItem("auth")).id,
                agentId: jwt(localStorage.getItem("auth")).id,
                transactionId: params.id,
                message: "Transaction Expense Delete",
                // transaction_property_address: "ADDRESS 2",
                note_type: "Expense",
              };
              const responsen = user_service.transactionNotes(userDatan);

              const groupedBycategory = response.data.data.reduce(
                (acc, item) => {
                  const type = item.type;
                  if (!acc[type]) {
                    acc[type] = [];
                  }
                  acc[type].push(item);
                  return acc;
                },
                {}
              );

              setGetExpenses(groupedBycategory);
            }
          });
        };
        ExpensesGetAll();
      }
    });
  };

  const AddExpenses = () => {
    navigate(`/add-expenses/${params.id}`);
  };

  return (
    <div className="bg-secondary float-left w-100 pt-4 mb-4">
      <Loader isActive={isActive} />
      {isShow && (
        <Toaster
          types={types}
          isShow={isShow}
          toasterBody={toasterBody}
          message={message}
        />
      )}
      <main className="page-wrapper">
        {/* <!-- Page content--> */}
        <div className="content-overlay transaction_tab_view">
          <div className="row">
            <AllTab />
            <div className="col-md-12">
              <div className="bg-light border rounded-3  mb-4 p-3">
                <div className="text-left mb-0 justify-content-start d-flex align-items-center">
                  <h5 className="w-25">Personal Expenses:</h5>
                  <span className="align-items-center">
                    Use this page to record business expenses for your
                    transaction, ensuring accurate financial records.
                    Maintaining detailed expense records is also crucial for tax
                    compliance.
                  </span>
                </div>
              </div>
              {/* <StatusBar /> */}
              <div className="bg-light p-3 border-0 rounded-3 shadow-sm float-start w-100">
                <div className="float-left w-100 d-flex align-items-center justify-content-between border-bottom pb-2 mb-4">
                  <h5 className="mb-0">Expense Listing</h5>

                  {summary.filing === "filed_complted" ? (
                    <a
                      className="btn btn-primary btn-sm  pull-right"
                      title="Your transaction was completed"
                    >
                      + Add Expenses
                    </a>
                  ) : (
                    <a
                      className="btn btn-primary btn-sm  pull-right"
                      onClick={AddExpenses}
                    >
                      + Add Expenses
                    </a>
                  )}
                </div>
                <div className="float-left w-100 d-flex align-items-center justify-content-between mb-4">
                  <h6 className="mb-0" id="">
                    Marketing Expenses
                  </h6>
                </div>
                <div className="float-left w-100">
                  <div className="table-responsive">
                    {getExpenses ? (
                      getExpenses.expenses ? (
                        <table
                          id="DepositLedger"
                          className="table table-striped"
                          width="100%"
                          border="0"
                          cellSpacing="0"
                          cellPadding="0"
                        >
                          <thead>
                            <tr>
                              <th className="hright">Entry Date</th>
                              <th className="hright">Paid For/Reason</th>
                              <th className="hright">Paid To</th>
                              <th className="hright">Description</th>
                              <th className="hright">Amount Paid</th>
                              <th className="hright">Action</th>
                            </tr>
                          </thead>

                          <tbody>
                            {getExpenses.expenses.map((item) => {
                              return (
                                <tr>
                                  <td>
                                    <span className="SuppressText">
                                      {moment(item.entryDate).format(
                                        "DD/MM/YYYY"
                                      )}
                                    </span>
                                  </td>

                                  <td>
                                    <span className="SuppressText">
                                      {item.PaidFor}
                                    </span>
                                  </td>
                                  <td>
                                    <span className="SuppressText">
                                      {item.PaidTo}
                                    </span>
                                  </td>
                                  <td>
                                    <span className="SuppressText">
                                      {item.description}
                                    </span>
                                  </td>

                                  <td>
                                    <span className="SuppressText">
                                      ${item.amountPaid}
                                    </span>
                                  </td>

                                  <td>
                                    <NavLink
                                      className=" SuppressText"
                                      to={`/add-expenses/${params.id}/${item._id}`}
                                    >
                                      <i
                                        className="h2 fi-edit opacity-80 mb-0"
                                        aria-hidden="true"
                                      ></i>
                                    </NavLink>
                                    <i
                                      className="h2 fi-trash opacity-80 mb-0 ms-2"
                                      onClick={(e) => handleRemove(item._id)}
                                    ></i>
                                  </td>
                                </tr>
                              );
                            })}
                            <tr>
                              <td colspan="4"></td>
                              <td>
                                <label className="col-form-label mb-0 p-0">
                                  Total: ${expenceCount}
                                </label>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      ) : (
                        <div className="float-left w-100 text-center my-5">
                          <p>No results to show</p>
                        </div>
                      )
                    ) : (
                      <p>No expenses entered.</p>
                    )}
                  </div>

                  {/* {
                                pageCount ?
                                    pageCount.lenght > 10 ?
                                        <div className="justify-content-end mb-1">
                                            <ReactPaginate
                                                previousLabel={"Previous"}
                                                nextLabel={"next"}
                                                breakLabel={"..."}
                                                pageCount={pageCount}
                                                marginPagesDisplayed={1}
                                                pageRangeDisplayed={2}
                                                onPageChange={handlePage}
                                                containerClassName={"pagination justify-content-center"}
                                                pageClassName={"page-item"}
                                                pageLinkClassName={"page-link"}
                                                previousClassName={"page-item"}
                                                previousLinkClassName={"page-link"}
                                                nextClassName={"page-item"}
                                                nextLinkClassName={"page-link"}
                                                breakClassName={"page-item"}
                                                breakLinkClassName={"page-link"}
                                                activeClassName={"active"}
                                            />
                                        </div>
                                        : ""
                                    : ""
                            } */}

                  <div className=""></div>
                </div>
                <div className="float-left w-100 d-flex align-items-center justify-content-between mt-2">
                  <h6 className="mb-4" id="">
                    Mileage Expenses
                  </h6>
                </div>
                <div className="float-left w-100">
                  <div className="table-responsive">
                    {getExpenses ? (
                      getExpenses.mileage ? (
                        <table
                          id="DepositLedger"
                          className="table table-striped"
                          width="100%"
                          border="0"
                          cellSpacing="0"
                          cellPadding="0"
                        >
                          <thead>
                            <tr>
                              <th className="hright">Entry Date</th>
                              <th className="hright">Trip For</th>
                              <th className="hright">Destination</th>
                              <th className="hright">Description</th>
                              <th className="hright">Amount Paid</th>
                              <th className="hright">Distance</th>
                              <th className="hright">Action</th>
                            </tr>
                          </thead>
                          <tbody>
                            {getExpenses.mileage.map((item) => {
                              return (
                                <tr>
                                  <td>
                                    <span className="SuppressText">
                                      {moment(item.entryDate).format(
                                        "DD/MM/YYYY"
                                      )}
                                    </span>
                                  </td>
                                  <td>
                                    <span className="SuppressText">
                                      {item.PaidFor}
                                    </span>
                                  </td>
                                  <td>
                                    <span className="SuppressText">
                                      {item.PaidTo}
                                    </span>
                                  </td>
                                  <td>
                                    <span className="SuppressText">
                                      {item.description}
                                    </span>
                                  </td>
                                  <td>
                                    <span className="SuppressText">
                                      ${item.amountPaid}
                                    </span>
                                  </td>

                                  <td>
                                    <span className="SuppressText">
                                      {item.distance ?? "0"}
                                    </span>
                                  </td>
                                  <td>
                                    <NavLink
                                      className=" SuppressText"
                                      to={`/add-expenses/${params.id}/${item._id}`}
                                    >
                                      <i
                                        className="h2 fi-edit opacity-80 mb-0"
                                        aria-hidden="true"
                                      ></i>
                                    </NavLink>
                                    <i
                                      className="h2 fi-trash opacity-80 mb-0 ms-2"
                                      onClick={(e) => handleRemove(item._id)}
                                    ></i>
                                  </td>
                                </tr>
                              );
                            })}
                            <tr>
                              <td colspan="4"></td>
                              <td>
                                <label className="col-form-label mb-0 p-0">
                                  Total: ${mileageCount}
                                </label>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      ) : (
                        <div className="float-left w-100 text-center my-5">
                          <p>No results to show</p>
                        </div>
                      )
                    ) : (
                      <p>No expenses entered.</p>
                    )}
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </main>
    </div>
  );
};

export default Expenses;

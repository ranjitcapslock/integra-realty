import React, { useState, useEffect } from 'react'
import AllTab from '../../Pages/AllTab'
import { NavLink, useNavigate, useParams } from 'react-router-dom';
import user_service from '../service/user_service';


const ManageCheckList = () => {
    const [summary, setSummary] = useState("");
    const params = useParams();

    // console.log(params); 
    useEffect(() => { window.scrollTo(0, 0);
        // setLoader({ isActive: true })
        const TransactionGetById = async () => {

            await user_service.transactionGetById(params.id).then((response) => {
                // setLoader({ isActive: false })
                if (response) {
                    setSummary(response.data);
                }
            });
        }
        TransactionGetById(params.id)
    }, []);


    const navigate = useNavigate();

    const addCheckList = () => {
        navigate(`/add-checklist/${params.id}`)
    }



    return (
        <div className="bg-secondary float-left w-100 pt-4">
            <main className="page-wrapper">
                <AllTab transaction_data = {summary}/>

                {/* <!-- Page container--> */}
                <div className="container mb-md-4 ">
                    <div className="bg-light shadow-sm rounded-3 p-2 p-md-5 mb-2">
                        <button className="btn btn-primary pull-right" onClick={addCheckList}>Add CheckList</button>
                        <div className="row">

                            <div className="d-flex align-items-center"><h1 className="h3 mb-3">Manage CheckList</h1><br /></div>

                            {/* <!-- Page content Start--> */}
                            <div className='col-md-12 mt-3 border border-light-dark'>
                                <div className='col-md-10 d-flex mt-3'>
                                    <div className="form-check">
                                        <input className="form-check-input" id="form-radio-1" type="radio" name="radios-stacked" checked />
                                        <label className="form-check-label-three" for="form-radio-1">All</label>
                                    </div>
                                    <div className="form-check">
                                        <input className="form-check-input" id="form-radio-2" type="radio" name="radios-stacked" />
                                        <label className="form-check-label-three" for="form-radio-2">Single - This Transaction (S)</label>
                                    </div>
                                    <div className="form-check">
                                        <input className="form-check-input" id="form-radio-2" type="radio" name="radios-stacked" />
                                        <label className="form-check-label-three" for="form-radio-2">Associate - My Transactions (A)</label>
                                    </div>
                                    <div className="form-check">
                                        <input className="form-check-input" id="form-radio-2" type="radio" name="radios-stacked" />
                                        <label className="form-check-label-three" for="form-radio-2">Profile - All Transactions (P)</label>
                                    </div>
                                    <div className="form-check">
                                        <input className="form-check-input" id="form-radio-2" type="radio" name="radios-stacked" />
                                        <label className="form-check-label-three" for="form-radio-2">Team Transactions (T)</label>
                                    </div>
                                </div>

                                <div className='col-md-9 ms-5 mt-3'>
                                    <p className='h5'>Post-Closing</p>
                                    <div className="alert alert-secondary mb-0  border rounded-3 p-3 selected_hover">
                                        [P] Add the client to your email/newsletter campaign
                                    </div>

                                    <div className="alert alert-secondary mb-0 border rounded-3 p-3 selected_hover">
                                        [P] Follow up call 1 week after closing to see if there are any questions
                                    </div>

                                    <div className="alert alert-secondary mb-0 border rounded-3 p-3 selected_hover">
                                        [P] Pick up your lockbox
                                    </div>

                                    <div className="alert alert-secondary mb-0 border rounded-3 p-3 selected_hover">
                                        [P] Pick up your yard sign
                                    </div>

                                    <div className="alert alert-secondary mb-0 border rounded-3 p-3 selected_hover">
                                        [P] Update the MLS Listing to Sold status
                                    </div>
                                </div><br /><br />


                                <div className='col-md-9 ms-5 mt-3'>
                                    <p className='h5'>Pre-Closing</p>
                                    <div className="alert alert-secondary mb-0  border rounded-3 p-3 selected_hover">
                                        [P] Ask the buyer's agent to have a walkthrough prior to closing and havemthe buyer sign a<br />
                                        walkthrough or decline of walkthrough form
                                    </div>

                                    <div className="alert alert-secondary mb-0 border rounded-3 p-3 selected_hover">
                                        [P] Attend the closing with the client
                                    </div>

                                    <div className="alert alert-secondary mb-0 border rounded-3 p-3 selected_hover">
                                        [P] Remind the client of the closing in 3 days
                                    </div>

                                    <div className="alert alert-secondary mb-0 border rounded-3 p-3 selected_hover">
                                        [P] Remind the client of the closing tomorrow at ??? time
                                    </div>

                                    <div className="alert alert-secondary mb-0 border rounded-3 p-3 selected_hover">
                                        [P] Review the settlement statement with client and cover expected cash (to or from)
                                    </div>
                                </div><br /><br />


                                <div className='col-md-9 ms-5 mt-3'>
                                    <p className='h5'>Contract</p>
                                    <div className="alert alert-secondary mb-0  border rounded-3 p-3 selected_hover">
                                        [P] Make sure that any request for changes to the contract be in writing and get signed<br /> amendments
                                    </div>

                                    <div className="alert alert-secondary mb-0 border rounded-3 p-3 selected_hover">
                                        [P] Make sure that utilities are left on for inspections
                                    </div>

                                    <div className="alert alert-secondary mb-0 border rounded-3 p-3 selected_hover">
                                        [P] Update the MLS Listing to Pending status
                                    </div>
                                </div><br /><br />

                                <div className='col-md-9 ms-5 mt-3'>
                                    <p className='h5'>Showing</p>
                                    <div className="alert alert-secondary mb-0  border rounded-3 p-3 selected_hover">
                                        [P] Explain the option period and the period to get financing to the seller.
                                    </div>

                                    <div className="alert alert-secondary mb-0 border rounded-3 p-3 selected_hover">
                                        [P] Has the contract been executed?
                                    </div>

                                    <div className="alert alert-secondary mb-0 border rounded-3 p-3 selected_hover">
                                        [P] Have all parties been notified of the execution of contract?
                                    </div>

                                    <div className="alert alert-secondary mb-0  border rounded-3 p-3 selected_hover">
                                        [P] Negotiate until terms are agreed
                                    </div>

                                    <div className="alert alert-secondary mb-0 border rounded-3 p-3 selected_hover">
                                        [P] Receive a fully signed copy of the sale contract and all applicable addenda
                                    </div>

                                    <div className="alert alert-secondary mb-0 border rounded-3 p-3 selected_hover">
                                        [P] Receive offer on the property
                                    </div>
                                </div><br /><br />

                                <div className='col-md-9 ms-5 mt-3'>
                                    <p className='h5'>Start</p>
                                    <div className="alert alert-secondary mb-0  border rounded-3 p-3 selected_hover">
                                        [P] Add the listing to the MLS
                                    </div>

                                    <div className="alert alert-secondary mb-0 border rounded-3 p-3 selected_hover">
                                        [P] Add your photos to the MLS Listing

                                    </div>

                                    <div className="alert alert-secondary mb-0 border rounded-3 p-3 selected_hover">
                                        [P] Contact the customer and get the listing property information
                                    </div>

                                    <div className="alert alert-secondary mb-0  border rounded-3 p-3 selected_hover">
                                        [P] Gather information for relevant addenda such as HOA, Utility District, and known defect<br /> disclosures
                                    </div>

                                    <div className="alert alert-secondary mb-0 border rounded-3 p-3 selected_hover">
                                        [P] Get any agency disclosures signed by the client
                                    </div>

                                    <div className="alert alert-secondary mb-0 border rounded-3 p-3 selected_hover">
                                        [P] Get the Listing Agreement signed and initialed
                                    </div>

                                    <div className="alert alert-secondary mb-0 border rounded-3 p-3 selected_hover">
                                        [P] Give the seller a Seller's Disclosure form to fill out (7 days)
                                    </div>

                                    <div className="alert alert-secondary mb-0 border rounded-3 p-3 selected_hover">
                                        [P] List with a Showing Service for appointments
                                    </div>

                                    <div className="alert alert-secondary mb-0 border rounded-3 p-3 selected_hover">
                                        [P] Make sure Lead-Based Paint is disclosed for structures built before 1978
                                    </div>

                                    <div className="alert alert-secondary mb-0 border rounded-3 p-3 selected_hover">
                                        [P] Perform a Comparative Market Analysis for sale of the property
                                    </div>

                                    <div className="alert alert-secondary mb-0 border rounded-3 p-3 selected_hover">
                                        [P] Put a lockbox on the property
                                    </div>

                                    <div className="alert alert-secondary mb-0 border rounded-3 p-3 selected_hover">
                                        [P] Put together a Listing Presentation, including the CMA
                                    </div>

                                    <div className="alert alert-secondary mb-0 border rounded-3 p-3 selected_hover">
                                        [P] Put your sign in the yard
                                    </div>

                                    <div className="alert alert-secondary mb-0 border rounded-3 p-3 selected_hover">
                                        [P] Send the Homeowner a Client Listing Worksheet
                                    </div>

                                    <div className="alert alert-secondary mb-0 border rounded-3 p-3 selected_hover">
                                        [P] Take pictures of the home
                                    </div>

                                    <div className="alert alert-secondary mb-0 border rounded-3 p-3 selected_hover">
                                        [P] Visit with the client and review the Listing Presentation and CMA
                                    </div>

                                </div><br /><br />






                            </div>
                            {/* <!-- Page content End--> */}


                        </div>
                    </div>
                </div>
            </main >
            

        </div >
    )
}

export default ManageCheckList
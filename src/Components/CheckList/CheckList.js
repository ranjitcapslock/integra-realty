import React from 'react';
import { useNavigate, useParams  } from "react-router-dom";
import user_service from '../service/user_service';
import Loader from '../../Pages/Loader/Loader.js';
import Toaster from '../../Pages/Toaster/Toaster';
import AllTab from '../../Pages/AllTab';
import jwt from "jwt-decode";

const CheckList = () => {
    
  

    return (
        <div className="bg-secondary float-left w-100 pt-4">
          <main className="page-wrapper">
      {/* <!-- Sign In Modal--> */}
      <div className="modal fade" id="signin-modal" tabindex="-1" aria-hidden="true">
        <div className="modal-dialog modal-lg modal-dialog-centered p-2 my-0 mx-auto">
          <div className="modal-content">
            <div className="modal-body px-0 py-2 py-sm-0">
              <button className="btn-close position-absolute top-0 end-0 mt-3 me-3" type="button" data-bs-dismiss="modal"></button>
              <div className="row mx-0 align-items-center">
                <div className="col-md-6 border-end-md p-4 p-sm-5">
                  <h2 className="h3 mb-4 mb-sm-5">Hey there!<br/>Welcome back.</h2>
                  <img className="d-block mx-auto" src="img/signin-modal/signin.svg" width="344" alt="Illustartion"/>
                  <div className="mt-4 mt-sm-5">Don't have an account? <a href="#signup-modal" data-bs-toggle="modal" data-bs-dismiss="modal">Sign up here</a></div>
                </div>
                <div className="col-md-6 px-4 pt-2 pb-4 px-sm-5 pb-sm-5 pt-md-5"><a className="btn btn-outline-info rounded-pill w-100 mb-3" href="#"><i className="fi-google fs-lg me-1"></i>Sign in with Google</a><a className="btn btn-outline-info rounded-pill w-100 mb-3" href="#"><i className="fi-facebook fs-lg me-1"></i>Sign in with Facebook</a>
                  <div className="d-flex align-items-center py-3 mb-3">
                    <hr className="w-100"/>
                    <div className="px-3">Or</div>
                    <hr className="w-100"/>
                  </div>
                  <form className="needs-validation" novalidate>
                    <div className="mb-4">
                      <label className="form-label mb-2" for="signin-email">Email address</label>
                      <input className="form-control" type="email" id="signin-email" 
                      placeholder="Enter your email" required/>
                    </div>
                    <div className="mb-4">
                      <div className="d-flex align-items-center justify-content-between mb-2">
                        <label className="form-label mb-0" for="signin-password">Password</label><a className="fs-sm" href="#">Forgot password?</a>
                      </div>
                      <div className="password-toggle">
                        <input className="form-control" type="password" id="signin-password" 
                        placeholder="Enter password" required/>
                        <label className="password-toggle-btn" aria-label="Show/hide password">
                          <input className="password-toggle-check" type="checkbox"/><span
                          className="password-toggle-indicator"></span>
                        </label>
                      </div>
                    </div>
                    <button className="btn btn-primary btn-lg rounded-pill w-100" type="submit">Sign in</button>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      {/* <!-- Sign Up Modal--> */}
      <div className="modal fade" id="signup-modal" tabindex="-1" aria-hidden="true">
        <div className="modal-dialog modal-lg modal-dialog-centered p-2 my-0 mx-auto">
          <div className="modal-content">
            <div className="modal-body px-0 py-2 py-sm-0">
              <button className="btn-close position-absolute top-0 end-0 mt-3 me-3" type="button" data-bs-dismiss="modal"></button>
              <div className="row mx-0 align-items-center">
                <div className="col-md-6 border-end-md p-4 p-sm-5">
                  <h2 className="h3 mb-4 mb-sm-5">Join Finder.<br/>Get premium benefits:</h2>
                  <ul className="list-unstyled mb-4 mb-sm-5">
                    <li className="d-flex mb-2"><i className="fi-check-circle text-primary mt-1 me-2"></i><span>Add and promote your listings</span></li>
                    <li className="d-flex mb-2"><i className="fi-check-circle text-primary mt-1 me-2"></i><span>Easily manage your wishlist</span></li>
                    <li className="d-flex mb-0"><i className="fi-check-circle text-primary mt-1 me-2"></i><span>Leave reviews</span></li>
                  </ul>
                  <img className="d-block mx-auto" src="img/signin-modal/signup.svg" width="344" alt="Illustartion"/>
                  <div className="mt-sm-4 pt-md-3">Already have an account? <a href="#signin-modal" data-bs-toggle="modal" data-bs-dismiss="modal">Sign in</a></div>
                </div>
                <div className="col-md-6 px-4 pt-2 pb-4 px-sm-5 pb-sm-5 pt-md-5"><a className="btn btn-outline-info rounded-pill w-100 mb-3" href="#"><i className="fi-google fs-lg me-1"></i>Sign in with Google</a><a className="btn btn-outline-info rounded-pill w-100 mb-3" href="#"><i className="fi-facebook fs-lg me-1"></i>Sign in with Facebook</a>
                  <div className="d-flex align-items-center py-3 mb-3">
                    <hr className="w-100"/>
                    <div className="px-3">Or</div>
                    <hr className="w-100"/>
                  </div>
                  <form className="needs-validation" novalidate>
                    <div className="mb-4">
                      <label className="form-label" for="signup-name">Full name</label>
                      <input className="form-control"
                       type="text" id="signup-name" placeholder="Enter your full name" required/>
                    </div>
                    <div className="mb-4">
                      <label className="form-label" for="signup-email">Email address</label>
                      <input className="form-control" type="email" id="signup-email" 
                      placeholder="Enter your email" required/>
                    </div>
                    <div className="mb-4">
                      <label className="form-label" for="signup-password">Password <span className='fs-sm text-muted'>min. 8 char</span></label>
                      <div className="password-toggle">
                        <input className="form-control" type="password" id="signup-password" minlength="8" required/>
                        <label className="password-toggle-btn" aria-label="Show/hide password">
                          <input className="password-toggle-check" type="checkbox"/><span className="password-toggle-indicator"></span>
                        </label>
                      </div>
                    </div>
                    <div className="mb-4">
                      <label className="form-label" for="signup-password-confirm">Confirm password</label>
                      <div className="password-toggle">
                        <input className="form-control" type="password" id="signup-password-confirm" 
                        minlength="8" required/>
                        <label className="password-toggle-btn" aria-label="Show/hide password">
                          <input className="password-toggle-check" type="checkbox"/><span className="password-toggle-indicator"></span>
                        </label>
                      </div>
                    </div>
                    <div className="form-check mb-4">
                      <input className="form-check-input" type="checkbox" id="agree-to-terms" required/>
                      <label className="form-check-label" for="agree-to-terms">By joining, I agree to the <a href='#'>Terms of use</a> and <a href='#'>Privacy policy</a></label>
                    </div>
                    <button className="btn btn-primary btn-lg rounded-pill w-100" type="submit">Sign up</button>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      {/* <!-- Navbar--> */}
      <header className="navbar navbar-expand-lg navbar-light fixed-top" data-scroll-header>
        <div className="container"><a className="navbar-brand me-3 me-xl-4" href="city-guide-home-v1.html">
          <img className="d-block" src="img/logo/logo-dark.svg" width="116" alt="Finder"/></a>
          <button className="navbar-toggler ms-auto" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation"><span className="navbar-toggler-icon"></span></button><a className="btn btn-sm text-primary d-none d-lg-block order-lg-3" href="#signin-modal" data-bs-toggle="modal"><i className="fi-user me-2"></i>Sign in</a><a className="btn btn-primary btn-sm rounded-pill ms-2 order-lg-3" href="city-guide-add-business.html"><i className="fi-plus me-2"></i>Add<span className='d-none d-sm-inline'> business</span></a>
          <div className="collapse navbar-collapse order-lg-2" id="navbarNav">
            <ul className="navbar-nav navbar-nav-scroll">
              {/* <!-- Demos switcher--> */}
              <li className="nav-item dropdown me-lg-2"><a className="nav-link dropdown-toggle align-items-center pe-lg-4" href="#" data-bs-toggle="dropdown" role="button" aria-expanded="false"><i className="fi-layers me-2"></i>Demos<span className="d-none d-lg-block position-absolute top-50 end-0 translate-middle-y border-end"></span></a>
                <ul className="dropdown-menu">
                  <li><a className="dropdown-item" href="real-estate-home-v1.html"><i className="fi-building fs-base opacity-50 me-2"></i>Real Estate Demo</a></li>
                  <li className="dropdown-divider"></li>
                  <li><a className="dropdown-item" href="car-finder-home.html"><i className="fi-car fs-base opacity-50 me-2"></i>Car Finder Demo</a></li>
                  <li className="dropdown-divider"></li>
                  <li><a className="dropdown-item" href="job-board-home-v1.html"><i className="fi-briefcase fs-base opacity-50 me-2"></i>Job Board Demo</a></li>
                  <li className="dropdown-divider"></li>
                  <li><a className="dropdown-item" href="city-guide-home-v1.html"><i className="fi-map-pin fs-base opacity-50 me-2"></i>City Guide Demo</a></li>
                  <li className="dropdown-divider"></li>
                  <li><a className="dropdown-item" href="index.html"><i className="fi-home fs-base opacity-50 me-2"></i>Main Page</a></li>
                  <li><a className="dropdown-item" href="components/typography.html"><i className="fi-list fs-base opacity-50 me-2"></i>Components</a></li>
                  <li><a className="dropdown-item" href="docs/dev-setup.html"><i className="fi-file fs-base opacity-50 me-2"></i>Documentation</a></li>
                </ul>
              </li>
              {/* <!-- Menu items--> */}
              <li className="nav-item dropdown active"><a className="nav-link dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">Home</a>
                <ul className="dropdown-menu">
                  <li><a className="dropdown-item" href="city-guide-home-v1.html">Homepage v.1</a></li>
                  <li><a className="dropdown-item" href="city-guide-home-v2.html">Homepage v.2</a></li>
                </ul>
              </li>
              <li className="nav-item dropdown"><a className="nav-link dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">Catalog</a>
                <ul className="dropdown-menu">
                  <li><a className="dropdown-item" href="city-guide-catalog.html">Grid with Filters</a></li>
                  <li><a className="dropdown-item" href="city-guide-single.html">Single Place - Gallery</a></li>
                  <li><a className="dropdown-item" href="city-guide-single-info.html">Single Place - Info</a></li>
                  <li><a className="dropdown-item" href="city-guide-single-reviews.html">Single Place - Reviews</a></li>
                </ul>
              </li>
              <li className="nav-item dropdown"><a className="nav-link dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">Account</a>
                <ul className="dropdown-menu">
                  <li><a className="dropdown-item" href="city-guide-account-info.html">Personal Info</a></li>
                  <li><a className="dropdown-item" href="city-guide-account-favorites.html">Favorites</a></li>
                  <li><a className="dropdown-item" href="city-guide-account-reviews.html">Reviews</a></li>
                  <li><a className="dropdown-item" href="city-guide-account-notifications.html">Notifications</a></li>
                  <li><a className="dropdown-item" href="signin-light.html">Sign In</a></li>
                  <li><a className="dropdown-item" href="signup-light.html">Sign Up</a></li>
                </ul>
              </li>
              <li className="nav-item dropdown"><a className="nav-link dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">Vendor</a>
                <ul className="dropdown-menu">
                  <li><a className="dropdown-item" href="city-guide-add-business.html">Add Business</a></li>
                  <li><a className="dropdown-item" href="city-guide-business-promotion.html">Business Promotion</a></li>
                  <li><a className="dropdown-item" href="city-guide-vendor-businesses.html">My Businesses</a></li>
                </ul>
              </li>
              <li className="nav-item dropdown"><a className="nav-link dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">Pages</a>
                <ul className="dropdown-menu">
                  <li><a className="dropdown-item" href="city-guide-about.html">About</a></li>
                  <li className="dropdown"><a className="dropdown-item dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">Blog</a>
                    <ul className="dropdown-menu">
                      <li><a className="dropdown-item" href="city-guide-blog.html">Blog Grid</a></li>
                      <li><a className="dropdown-item" href="city-guide-blog-single.html">Single Post</a></li>
                    </ul>
                  </li>
                  <li><a className="dropdown-item" href="city-guide-contacts.html">Contacts</a></li>
                  <li className="dropdown"><a className="dropdown-item dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">Help Center</a>
                    <ul className="dropdown-menu">
                      <li><a className="dropdown-item" href="city-guide-help-center.html">Help Topics</a></li>
                      <li><a className="dropdown-item" href="city-guide-help-center-single-topic.html">Single Topic</a></li>
                    </ul>
                  </li>
                  <li><a className="dropdown-item" href="city-guide-404.html">404 Not Found</a></li>
                </ul>
              </li>
              <li className="nav-item d-lg-none"><a className="nav-link" href="#signin-modal" data-bs-toggle="modal"><i className="fi-user me-2"></i>Sign in</a></li>
            </ul>
          </div>
        </div>
      </header>
      {/* <!-- Page content-->
      <!-- Hero--> */}
      <section className="container py-5 mt-5 mb-lg-3">
        <div className="row align-items-center mt-md-2">
          <div className="col-lg-7 order-lg-2 mb-lg-0 mb-4 pb-2 pb-lg-0">
            <img className="d-block mx-auto" src="img/city-guide/home/hero-img.jpg" width="746" alt="Hero image"/></div>
          <div className="col-lg-5 order-lg-1 pe-lg-0">
            <h1 className="display-5 mb-4 me-lg-n5 text-lg-start text-center mb-4">Start exploring <span className="dropdown d-inline-block"><a className="dropdown-toggle text-decoration-none" href="#" role="button" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Berlin</a><span className="dropdown-menu dropdown-menu-end my-1"><a className="dropdown-item fs-base fw-bold" href="#">Hamburg</a><a className="dropdown-item fs-base fw-bold" href="#">Munich</a><a className="dropdown-item fs-base fw-bold" href="#">Frankfurt am Main</a><a className="dropdown-item fs-base fw-bold" href="#">Stuttgart</a><a className="dropdown-item fs-base fw-bold" href="#">Cologne</a></span></span></h1>
            <p className="text-lg-start text-center mb-4 mb-lg-5 fs-lg">Find great places to stay, eat, shop, or visit from our partners and local experts.</p>
            {/* <!-- Search form--> */}
            <div className="me-lg-n5">
              <form className="form-group d-block d-md-flex position-relative rounded-md-pill me-lg-n5">
                <div className="input-group input-group-lg border-end-md"><span className="input-group-text text-muted rounded-pill ps-3"><i className="fi-search"></i></span>
                  <input className="form-control" type="text" placeholder="What are you looking for?"/>
                </div>
                <hr className="d-md-none my-2"/>
                <div className="d-sm-flex">
                  <div className="dropdown w-100 mb-sm-0 mb-3" data-bs-toggle="select">
                    <button className="btn btn-link btn-lg dropdown-toggle ps-2 ps-sm-3" type="button" data-bs-toggle="dropdown"><i className="fi-list me-2"></i><span className="dropdown-toggle-label">All categories</span></button>
                    <input type="hidden"/>
                    <ul className="dropdown-menu">
                      <li><a className="dropdown-item" href="#"><i className="fi-bed fs-lg opacity-60 me-2"></i><span className="dropdown-item-label">Accomodation</span></a></li>
                      <li><a className="dropdown-item" href="#"><i className="fi-cafe fs-lg opacity-60 me-2"></i><span className="dropdown-item-label">Food &amp; Drink</span></a></li>
                      <li><a className="dropdown-item" href="#"><i className="fi-shopping-bag fs-lg opacity-60 me-2"></i><span className="dropdown-item-label">Shopping</span></a></li>
                      <li><a className="dropdown-item" href="#"><i className="fi-museum fs-lg opacity-60 me-2"></i><span className="dropdown-item-label">Art &amp; History</span></a></li>
                      <li><a className="dropdown-item" href="#"><i className="fi-entertainment fs-lg opacity-60 me-2"></i><span className="dropdown-item-label">Entertainment</span></a></li>
                      <li><a className="dropdown-item" href="#"><i className="fi-meds fs-lg opacity-60 me-2"></i><span className="dropdown-item-label">Medicine</span></a></li>
                      <li><a className="dropdown-item" href="#"><i className="fi-makeup fs-lg opacity-60 me-2"></i><span className="dropdown-item-label">Beauty</span></a></li>
                      <li><a className="dropdown-item" href="#"><i className="fi-car fs-lg opacity-60 me-2"></i><span className="dropdown-item-label">Car Rental</span></a></li>
                    </ul>
                  </div>
                  <button className="btn btn-primary btn-lg rounded-pill w-100 w-md-auto ms-sm-3" type="button">Search</button>
                </div>
              </form>
            </div>
          </div>
        </div>
      </section>
      {/* <!-- Categories--> */}
      <section className="container d-flex flex-wrap flex-column flex-sm-row pb-5 mb-md-3"><a className="icon-box card flex-row align-items-center flex-shrink-0 card-hover border-0 shadow-sm rounded-pill py-2 ps-2 pe-4 mb-2 mb-sm-3 me-sm-3 me-xxl-4" href="city-guide-catalog.html">
          <div className="icon-box-media bg-faded-accent text-accent rounded-circle me-2"><i className="fi-bed"></i></div>
          <h3 className="icon-box-title fs-sm ps-1 pe-2 mb-0">Accommodation</h3></a><a className="icon-box card flex-row align-items-center flex-shrink-0 card-hover border-0 shadow-sm rounded-pill py-2 ps-2 pe-4 mb-2 mb-sm-3 me-sm-3 me-xxl-4" href="city-guide-catalog.html">
          <div className="icon-box-media bg-faded-warning text-warning rounded-circle me-2"><i className="fi-cafe"></i></div>
          <h3 className="icon-box-title fs-sm ps-1 pe-2 mb-0">Food &amp; Drink</h3></a><a className="icon-box card flex-row align-items-center flex-shrink-0 card-hover border-0 shadow-sm rounded-pill py-2 ps-2 pe-4 mb-2 mb-sm-3 me-sm-3 me-xxl-4" href="city-guide-catalog.html">
          <div className="icon-box-media bg-faded-primary text-primary rounded-circle me-2"><i className="fi-shopping-bag"></i></div>
          <h3 className="icon-box-title fs-sm ps-1 pe-2 mb-0">Shopping</h3></a><a className="icon-box card flex-row align-items-center flex-shrink-0 card-hover border-0 shadow-sm rounded-pill py-2 ps-2 pe-4 mb-2 mb-sm-3 me-sm-3 me-xxl-4" href="city-guide-catalog.html">
          <div className="icon-box-media bg-faded-success text-success rounded-circle me-2"><i className="fi-museum"></i></div>
          <h3 className="icon-box-title fs-sm ps-1 pe-2 mb-0">Art &amp; History</h3></a><a className="icon-box card flex-row align-items-center flex-shrink-0 card-hover border-0 shadow-sm rounded-pill py-2 ps-2 pe-4 mb-2 mb-sm-3 me-sm-3 me-xxl-4" href="city-guide-catalog.html">
          <div className="icon-box-media bg-faded-primary text-primary rounded-circle me-2"><i className="fi-entertainment"></i></div>
          <h3 className="icon-box-title fs-sm ps-1 pe-2 mb-0">Entertainment</h3></a>
        <div className="dropdown mb-2 mb-sm-3"><a className="icon-box card flex-row align-items-center flex-shrink-0 card-hover border-0 shadow-sm rounded-pill py-2 ps-2 pe-4" href="#" data-bs-toggle="dropdown">
            <div className="icon-box-media bg-faded-info text-info rounded-circle me-2"><i className="fi-dots-horisontal"></i></div>
            <h3 className="icon-box-title fs-sm ps-1 pe-2 mb-0">More</h3></a>
          <div className="dropdown-menu dropdown-menu-sm-end my-1"><a className="dropdown-item fw-bold" href="city-guide-catalog.html"><i className="fi-meds fs-base opacity-60 me-2"></i>Medicine</a><a className="dropdown-item fw-bold" href="city-guide-catalog.html"><i className="fi-makeup fs-base opacity-60 me-2"></i>Beauty</a><a className="dropdown-item fw-bold" href="city-guide-catalog.html"><i className="fi-car fs-base opacity-60 me-2"></i>Car Rental</a><a className="dropdown-item fw-bold" href="city-guide-catalog.html"><i className="fi-dumbell fs-base opacity-60 me-2"></i>Fitness &amp; Sport</a><a className="dropdown-item fw-bold" href="city-guide-catalog.html"><i className="fi-disco-ball fs-base opacity-60 me-2"></i>Night Club</a></div>
        </div>
      </section>
      {/* <!-- Where to stay--> */}
      <section className="container mb-sm-5 mb-4 pb-lg-4">
        <div className="d-sm-flex align-items-center justify-content-between mb-4 pb-2">
          <h2 className="h3 mb-sm-0">Where to stay in Berlin</h2><a className="btn btn-link fw-normal ms-sm-3 p-0" href="city-guide-catalog.html">View all<i className="fi-arrow-long-right ms-2"></i></a>
        </div>
        <div className="tns-carousel-wrapper tns-controls-outside-xxl tns-nav-outside">
          <div className="tns-carousel-inner" data-carousel-options="{&quot;items&quot;: 3, &quot;gutter&quot;: 24, &quot;responsive&quot;: {&quot;0&quot;:{&quot;items&quot;:1,&quot;nav&quot;:true},&quot;500&quot;:{&quot;items&quot;:2},&quot;850&quot;:{&quot;items&quot;:3},&quot;1400&quot;:{&quot;items&quot;:3,&quot;nav&quot;:false}}}">
            {/* <!-- Item--> */}
            <div>
              <div className="position-relative">
                <div className="position-relative mb-3">
                  <button className="btn btn-icon btn-light-primary btn-xs text-primary rounded-circle position-absolute top-0 end-0 m-3 zindex-5" type="button" data-bs-toggle="tooltip" data-bs-placement="left" title="Add to Favorites"><i className="fi-heart"></i></button>
                  <img className="rounded-3" src="img/city-guide/catalog/01.jpg" alt="Image"/>
                </div>
                <h3 className="mb-2 fs-lg"><a className="nav-link stretched-link" href="city-guide-single.html">Berlin Business Hotel</a></h3>
                <ul className="list-inline mb-0 fs-xs">
                  <li className="list-inline-item pe-1"><i className="fi-star-filled mt-n1 me-1 fs-base text-warning align-middle"></i><b>5.0</b><span className="text-muted">&nbsp;(48)</span></li>
                  <li className="list-inline-item pe-1"><i className="fi-credit-card mt-n1 me-1 fs-base text-muted align-middle"></i>$$</li>
                  <li className="list-inline-item pe-1"><i className="fi-map-pin mt-n1 me-1 fs-base text-muted align-middle"></i>1.4 km from center</li>
                </ul>
              </div>
            </div>
            {/* <!-- Item--> */}
            <div>
              <div className="position-relative">
                <div className="position-relative mb-3">
                  <button className="btn btn-icon btn-light-primary btn-xs text-primary rounded-circle position-absolute top-0 end-0 m-3 zindex-5" type="button" data-bs-toggle="tooltip" data-bs-placement="left" title="Add to Favorites"><i className="fi-heart"></i></button>
                  <img className="rounded-3" src="img/city-guide/catalog/02.jpg" alt="Image"/>
                </div>
                <h3 className="mb-2 fs-lg"><a className="nav-link stretched-link" href="city-guide-single.html">Big Tree Cottage</a></h3>
                <ul className="list-inline mb-0 fs-xs">
                  <li className="list-inline-item pe-1"><i className="fi-star-filled mt-n1 me-1 fs-base text-warning align-middle"></i><b>4.8</b><span className="text-muted">&nbsp;(24)</span></li>
                  <li className="list-inline-item pe-1"><i className="fi-credit-card mt-n1 me-1 fs-base text-muted align-middle"></i>$$$</li>
                  <li className="list-inline-item pe-1"><i className="fi-map-pin mt-n1 me-1 fs-base text-muted align-middle"></i>0.5 km from center</li>
                </ul>
              </div>
            </div>
            {/* <!-- Item--> */}
            <div>
              <div className="position-relative">
                <div className="position-relative mb-3">
                  <button className="btn btn-icon btn-light-primary btn-xs text-primary rounded-circle position-absolute top-0 end-0 m-3 zindex-5" type="button" data-bs-toggle="tooltip" data-bs-placement="left" title="Add to Favorites"><i className="fi-heart"></i></button>
                  <img className="rounded-3" src="img/city-guide/catalog/03.jpg" alt="Image"/>
                </div>
                <h3 className="mb-2 fs-lg"><a className="nav-link stretched-link" href="city-guide-single.html">Grand Resort &amp; Spa</a></h3>
                <ul className="list-inline mb-0 fs-xs">
                  <li className="list-inline-item pe-1"><i className="fi-star-filled mt-n1 me-1 fs-base text-warning align-middle"></i><b>4.9</b><span className="text-muted">&nbsp;(43)</span></li>
                  <li className="list-inline-item pe-1"><i className="fi-credit-card mt-n1 me-1 fs-base text-muted align-middle"></i>$$$</li>
                  <li className="list-inline-item pe-1"><i className="fi-map-pin mt-n1 me-1 fs-base text-muted align-middle"></i>1.8 km from center</li>
                </ul>
              </div>
            </div>
            {/* <!-- Item--> */}
            <div>
              <div className="position-relative">
                <div className="position-relative mb-3">
                  <button className="btn btn-icon btn-light-primary btn-xs text-primary rounded-circle position-absolute top-0 end-0 m-3 zindex-5" type="button" data-bs-toggle="tooltip" data-bs-placement="left" title="Add to Favorites"><i className="fi-heart"></i></button>
                  <img className="rounded-3" src="img/city-guide/catalog/04.jpg" alt="Image"/>
                </div>
                <h3 className="mb-2 fs-lg"><a className="nav-link stretched-link" href="city-guide-single.html">Merry Berry Motel</a></h3>
                <ul className="list-inline mb-0 fs-xs">
                  <li className="list-inline-item pe-1"><i className="fi-star-filled mt-n1 me-1 fs-base text-warning align-middle"></i><b>4.5</b><span className="text-muted">&nbsp;(13)</span></li>
                  <li className="list-inline-item pe-1"><i className="fi-credit-card mt-n1 me-1 fs-base text-muted align-middle"></i>$$</li>
                  <li className="list-inline-item pe-1"><i className="fi-map-pin mt-n1 me-1 fs-base text-muted align-middle"></i>0.4 km from center</li>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </section>
      {/* <!-- Banner + Where to eat--> */}
      <div className="container mb-5 pb-lg-4">
        <div className="row">
          {/* <!-- Banner--> */}
          <div className="col-lg-4 text-center text-lg-start mb-lg-0 mb-5"><a className="d-block text-decoration-none bg-faded-accent rounded-3 h-100" href="#">
              <div className="p-4">
                <h2 className="mb-0 p-2 text-primary text-nowrap"><i className="fi-phone mt-n1 me-2 pe-1 fs-3 align-middle"></i>Taxi<span className="text-dark">&nbsp;488</span></h2>
                <p className="mb-0 p-2 fs-lg text-body">The best way to get wherever you’re going!</p>
              </div>
              <img src="img/city-guide/illustrations/taxi.svg" alt="Illustration"/></a></div>
          {/* <!-- Where to eat--> */}
          <div className="col-lg-8">
            <div className="d-flex align-items-center justify-content-between mb-4 pb-2">
              <h2 className="h3 mb-0">Where to eat</h2><a className="btn btn-link fw-normal p-0" href="city-guide-catalog.html">View all<i className="fi-arrow-long-right ms-2"></i></a>
            </div>
            <div className="row">
              <div className="col-sm-6">
                {/* <!-- Item--> */}
                <div className="d-flex align-items-start position-relative mb-4">
                  <img className="flex-shrink-0 me-3 rounded-3" src="img/city-guide/brands/01.svg" alt="Brand logo"/>
                  <div>
                    <h3 className="mb-2 fs-lg"><a className="nav-link stretched-link" href="city-guide-single.html">Pina Pizza Restaurant</a></h3>
                    <ul className="list-unstyled mb-0 fs-xs">
                      <li><i className="fi-star-filled mt-n1 me-1 fs-base text-warning align-middle"></i><b>5.0</b><span className="text-muted">&nbsp;(48)</span></li>
                      <li><i className="fi-credit-card mt-n1 me-1 fs-base text-muted align-middle"></i>$$</li>
                      <li><i className="fi-map-pin mt-n1 me-1 fs-base text-muted align-middle"></i>1.4 km from center</li>
                    </ul>
                  </div>
                </div>
                {/* <!-- Item--> */}
                <div className="d-flex align-items-start position-relative mb-4">
                  <img className="flex-shrink-0 me-3 rounded-3" src="img/city-guide/brands/02.svg" alt="Brand logo"/>
                  <div>
                    <h3 className="mb-2 fs-lg"><a className="nav-link stretched-link" href="city-guide-single.html">KFC</a></h3>
                    <ul className="list-unstyled mb-0 fs-xs">
                      <li><i className="fi-star-filled mt-n1 me-1 fs-base text-warning align-middle"></i><b>4.0</b><span className="text-muted">&nbsp;(18)</span></li>
                      <li><i className="fi-credit-card mt-n1 me-1 fs-base text-muted align-middle"></i>$</li>
                      <li><i className="fi-map-pin mt-n1 me-1 fs-base text-muted align-middle"></i>1.8 km from center</li>
                    </ul>
                  </div>
                </div>
                {/* <!-- Item--> */}
                <div className="d-flex align-items-start position-relative mb-4">
                  <img className="flex-shrink-0 me-3 rounded-3" src="img/city-guide/brands/03.svg" alt="Brand logo"/>
                  <div>
                    <h3 className="mb-2 fs-lg"><a className="nav-link stretched-link" href="city-guide-single.html">Yum Restaurant</a></h3>
                    <ul className="list-unstyled mb-0 fs-xs">
                      <li><i className="fi-star-filled mt-n1 me-1 fs-base text-warning align-middle"></i><b>4.6</b><span className="text-muted">&nbsp;(48)</span></li>
                      <li><i className="fi-credit-card mt-n1 me-1 fs-base text-muted align-middle"></i>$</li>
                      <li><i className="fi-map-pin mt-n1 me-1 fs-base text-muted align-middle"></i>2.4 km from center</li>
                    </ul>
                  </div>
                </div>
              </div>
              <div className="col-sm-6">
                {/* <!-- Item--> */}
                <div className="d-flex align-items-start position-relative mb-4">
                  <img className="flex-shrink-0 me-3 rounded-3" src="img/city-guide/brands/04.svg" alt="Brand logo"/>
                  <div>
                    <h3 className="mb-2 fs-lg"><a className="nav-link stretched-link" href="city-guide-single.html">Tosaka Sushi Bar</a></h3>
                    <ul className="list-unstyled mb-0 fs-xs">
                      <li><i className="fi-star-filled mt-n1 me-1 fs-base text-warning align-middle"></i><b>5.0</b><span className="text-muted">&nbsp;(28)</span></li>
                      <li><i className="fi-credit-card mt-n1 me-1 fs-base text-muted align-middle"></i>$$$</li>
                      <li><i className="fi-map-pin mt-n1 me-1 fs-base text-muted align-middle"></i>2.5 km from center</li>
                    </ul>
                  </div>
                </div>
                {/* <!-- Item--> */}
                <div className="d-flex align-items-start position-relative mb-4">
                  
                  <img className="flex-shrink-0 me-3 rounded-3" src="img/city-guide/brands/05.svg" alt="Brand logo"/>
                  <div>
                    <h3 className="mb-2 fs-lg"><a className="nav-link stretched-link" href="city-guide-single.html">Dunkin' Donuts</a></h3>
                    <ul className="list-unstyled mb-0 fs-xs">
                      <li><i className="fi-star-filled mt-n1 me-1 fs-base text-warning align-middle"></i><b>5.0</b><span className="text-muted">&nbsp;(43)</span></li>
                      <li><i className="fi-credit-card mt-n1 me-1 fs-base text-muted align-middle"></i>$</li>
                      <li><i className="fi-map-pin mt-n1 me-1 fs-base text-muted align-middle"></i>1.8 km from center</li>
                    </ul>
                  </div>
                </div>
                {/* <!-- Item--> */}
                <div className="d-flex align-items-start position-relative">
                  <img className="flex-shrink-0 me-3 rounded-3" src="img/city-guide/brands/06.svg" alt="Brand logo"/>
                  <div>
                    <h3 className="mb-2 fs-lg"><a className="nav-link stretched-link" href="city-guide-single.html">Spicy Bar-Restaurant</a></h3>
                    <ul className="list-unstyled mb-0 fs-xs">
                      <li><i className="fi-star-filled mt-n1 me-1 fs-base text-warning align-middle"></i><b>5.0</b><span className="text-muted">&nbsp;(32)</span></li>
                      <li><i className="fi-credit-card mt-n1 me-1 fs-base text-muted align-middle"></i>$$$</li>
                      <li><i className="fi-map-pin mt-n1 me-1 fs-base text-muted align-middle"></i>0.4 km from center</li>
                    </ul>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      {/* <!-- Upcoming events--> */}
      <section className="container mb-5 pb-lg-3">
        <div className="d-md-flex align-items-center justify-content-between mb-4 pb-md-2">
          <h2 className="h3 w-100 mb-md-0">Upcoming events in Berlin</h2>
          {/* <!-- Sorting by date--> */}
          <div className="w-100 ms-md-3 mb-n3 pt-2 pb-3 px-1" data-simplebar data-simplebar-auto-hide="false">
            <div className="d-flex align-items-center">
              <div className="input-group input-group-sm flex-shrink-0 ms-md-auto me-3">
                <input className="form-control date-picker rounded-pill ps-5" type="text" placeholder="Choose date" 
                data-datepicker-options="{&quot;altInput&quot;: true, &quot;altFormat&quot;: &quot;F j, Y&quot;,
                 &quot;dateFormat&quot;: &quot;Y-m-d&quot;}"/><i className="fi-calendar position-absolute top-50 
                 start-0 translate-middle-y ms-3 ps-1"></i>
              </div><a className="btn btn-sm btn-secondary rounded-pill fw-normal ms-n1 me-3" href="#">Tomorrow</a><a className="btn btn-sm btn-secondary rounded-pill fw-normal ms-n1 me-3" href="#">This weekend</a><a className="btn btn-link ms-md-3 ms-auto p-0 fw-normal" href="city-guide-catalog.html">View all<i className="fi-arrow-long-right ms-2"></i></a>
            </div>
          </div>
        </div>
        {/* <!-- Carousel--> */}
        <div className="tns-carousel-wrapper tns-controls-outside-xxl tns-nav-outside tns-center">
          <div className="tns-carousel-inner" data-carousel-options="{&quot;items&quot;: 1, &quot;edgePadding&quot;: true, &quot;responsive&quot;: {&quot;0&quot;:{&quot;controls&quot;: false, &quot;gutter&quot;: 16},&quot;500&quot;:{&quot;controls&quot;: true, &quot;gutter&quot;: 16}, &quot;768&quot;: {&quot;gutter&quot;: 24}}}">
            {/* <!-- Item--> */}
            <div>
              <div className="card border-0 bg-size-cover pt-5">
                <div className="d-none d-md-block"></div>
                <div className="card-body text-center text-md-start pt-4 pt-xl-0">
                  <div className="d-md-flex justify-content-between align-items-end">
                    <div className="me-2 mb-4 mb-md-0">
                      <div className="d-flex justify-content-center justify-content-md-start text-light fs-sm mb-2">
                        <div className="text-nowrap me-3"><i className="fi-calendar-alt me-1 opacity-70"></i><span className="align-middle">Nov 15</span></div>
                        <div className="text-nowrap"><i className="fi-clock me-1 opacity-70"></i><span className="align-middle">21:00</span></div>
                      </div>
                      <h3 className="h5 text-light mb-0">Simon Rock Concert</h3>
                    </div>
                    <div className="btn-group"><a className="btn btn-primary rounded-pill rounded-end-0 px-3" href="#">Tickets from $50</a>
                      <div className="position-relative border-start border-light zindex-5"></div>
                      <button className="btn btn-primary rounded-pill rounded-start-0 px-3" type="button"><i className="fi-heart"></i></button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            {/* <!-- Item--> */}
            <div>
              <div className="card border-0 bg-size-cover pt-5">
                <div className="d-none d-md-block"></div>
                <div className="card-body text-center text-md-start pt-4 pt-xl-0">
                  <div className="d-md-flex justify-content-between align-items-end">
                    <div className="me-2 mb-4 mb-md-0">
                      <div className="d-flex justify-content-center justify-content-md-start text-light fs-sm mb-2">
                        <div className="text-nowrap me-3"><i className="fi-calendar-alt me-1 opacity-70"></i><span className="align-middle">Dec 2</span></div>
                        <div className="text-nowrap"><i className="fi-clock me-1 opacity-70"></i><span className="align-middle">10:00</span></div>
                      </div>
                      <h3 className="h5 text-light mb-0">Holi Festival</h3>
                    </div>
                    <div className="btn-group"><a className="btn btn-primary rounded-pill rounded-end-0 px-3" href="#">Tickets from $35</a>
                      <div className="position-relative border-start border-light zindex-5"></div>
                      <button className="btn btn-primary rounded-pill rounded-start-0 px-3" type="button"><i className="fi-heart"></i></button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            {/* <!-- Item--> */}
            <div>
              <div className="card border-0 bg-size-cover pt-5">
                <div className="d-none d-md-block"></div>
                <div className="card-body text-center text-md-start pt-4 pt-xl-0">
                  <div className="d-md-flex justify-content-between align-items-end">
                    <div className="me-2 mb-4 mb-md-0">
                      <div className="d-flex justify-content-center justify-content-md-start text-light fs-sm mb-2">
                        <div className="text-nowrap me-3"><i className="fi-calendar-alt me-1 opacity-70"></i><span className="align-middle">No 11</span></div>
                        <div className="text-nowrap"><i className="fi-clock me-1 opacity-70"></i><span className="align-middle">18:00</span></div>
                      </div>
                      <h3 className="h5 text-light mb-0">Football Match</h3>
                    </div>
                    <div className="btn-group"><a className="btn btn-primary rounded-pill rounded-end-0 px-3" href="#">Tickets from $40</a>
                      <div className="position-relative border-start border-light zindex-5"></div>
                      <button className="btn btn-primary rounded-pill rounded-start-0 px-3" type="button"><i className="fi-heart"></i></button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
      {/* <!-- What’s new--> */}
      <section className="container mt-n3 mt-md-0 mb-5 pb-lg-4">
        <h2 className="h3 mb-4 pb-2">What’s new in Berlin</h2>
        {/* <!-- Carousel--> */}
        <div className="tns-carousel-wrapper">
          <div className="tns-carousel-inner" data-carousel-options="{&quot;mode&quot;: &quot;gallery&quot;, &quot;nav&quot;: false, &quot;controlsContainer&quot;: &quot;#carousel-controls&quot;}">
            {/* <!-- Item--> */}
            <div>
              <div className="row">
                <div className="col-md-7 mb-md-0 mb-3">
                  <img className="position-relative rounded-3 zindex-5" src="img/city-guide/home/new-1.jpg" 
                  alt="Article image"/></div>
                <div className="col-md-5">
                  <h3 className="h4 from-top">Amusement Park</h3>
                  <ul className="list-unstyled delay-2 from-end">
                    <li className="mb-1 fs-sm"><i className="fi-map-pin text-muted me-2 fs-base"></i>Ollenhauer Str. 29, 10118</li>
                    <li className="mb-1 fs-sm"><i className="fi-clock text-muted me-2 fs-base"></i>9:00 – 23:00</li>
                    <li className="mb-1 fs-sm"><i className="fi-wallet text-muted me-2 fs-base"></i>$$</li>
                  </ul>
                  <p className="pb-2 delay-3 from-end d-none d-lg-block">Blandit lorem dictum in velit. Et nisi at faucibus mauris pretium enim. Risus sapien nisi aliquam egestas leo dignissim ut quis ac. Amet, cras orci justo, tortor nisl aliquet. Enim tincidunt tellus nunc, nulla arcu posuere quis. Velit turpis orci venenatis risus felis, volutpat convallis varius. Enim non euismod adipiscing a enim.</p>
                  <div className="delay-4 scale-up"><a className="btn btn-primary rounded-pill" href="city-guide-single.html">View more<i className="fi-chevron-right fs-sm ms-2"></i></a></div>
                </div>
              </div>
            </div>
            {/* <!-- Item--> */}
            <div>
              <div className="row">
                <div className="col-md-7 mb-md-0 mb-3">
                  <img className="position-relative rounded-3 zindex-5"
                   src="img/city-guide/home/new-2.jpg" alt="Article image"/></div>
                <div className="col-md-5">
                  <h3 className="h4 from-top">Mall of Berlin</h3>
                  <ul className="list-unstyled delay-2 from-end">
                    <li className="mb-1 fs-sm"><i className="fi-map-pin text-muted me-2 fs-base"></i>Ollenhauer Str. 29, 10118</li>
                    <li className="mb-1 fs-sm"><i className="fi-clock text-muted me-2 fs-base"></i>10:00 – 20:00</li>
                    <li className="mb-1 fs-sm"><i className="fi-wallet text-muted me-2 fs-base"></i>$$</li>
                  </ul>
                  <p className="pb-2 delay-3 from-end d-none d-lg-block">Sem nibh urna id arcu. Quis tortor vestibulum morbi volutpat. Et duis et sed tellus. Egestas ultrices viverra in pretium nec. Dui ornare fusce vel fringilla scelerisque posuere pharetra ut. Dui donec sapien, dictum nunc varius.</p>
                  <div className="delay-4 scale-up"><a className="btn btn-primary rounded-pill" href="city-guide-single.html">View more<i className="fi-chevron-right fs-sm ms-2"></i></a></div>
                </div>
              </div>
            </div>
          </div>
        </div>
        {/* <!-- Carousel custom controls--> */}
        <div className="tns-carousel-controls pt-2 mt-4" id="carousel-controls">
          <button className="me-3" type="button"><i className="fi-chevron-left fs-xs"></i></button>
          <button type="button"><i className="fi-chevron-right fs-xs"></i></button>
        </div>
      </section>
      {/* <!-- Mobile app CTA--> */}
      <section className="container">
        <div className="bg-faded-accent rounded-3">
          <div className="row align-items-center">
            <div className="col-lg-5 col-md-6 ps-lg-5">
              <div className="ps-xl-5 pe-md-0 pt-4 pb-md-4 px-3 text-center text-md-start">
                <h2 className="mb-md-3 pt-2 pt-md-0 mb-2 pb-md-0 pb-1">Get Our App</h2>
              </div>
            </div>
            <div className="col-lg-7 col-md-6">
              <img className="d-none d-md-block" src="img/city-guide/illustrations/app.png" width="698" alt="Illustration"/><img className="d-block d-md-none mx-auto" src="img/city-guide/illustrations/app-m.png" width="446" alt="Illustration"/></div>
          </div>
        </div>
      </section>
      {/* <!-- Blog: Latest posts--> */}
      <section className="container my-5 py-lg-4">
        <div className="d-sm-flex align-items-center justify-content-between mb-4 pb-2">
          <h2 className="h3 mb-sm-0">You may be also interested in</h2><a className="btn btn-link fw-normal ms-sm-3 p-0" href="city-guide-blog.html">Go to blog<i className="fi-arrow-long-right ms-2"></i></a>
        </div>
        {/* <!-- Carousel--> */}
        <div className="tns-carousel-wrapper tns-nav-outside mb-md-2">
          <div className="tns-carousel-inner d-block" data-carousel-options="{&quot;controls&quot;: false, &quot;gutter&quot;: 24, &quot;autoHeight&quot;: true, &quot;responsive&quot;: {&quot;0&quot;:{&quot;items&quot;:1,&quot;nav&quot;:true},&quot;500&quot;:{&quot;items&quot;:2},&quot;850&quot;:{&quot;items&quot;:3},&quot;1200&quot;:{&quot;items&quot;:3}}}">
            {/* <!-- Item--> */}
            <article><a className="d-block mb-3" href="city-guide-blog-single.html">
              <img className="rounded-3" src="img/city-guide/blog/01.jpg" alt="Post image"/></a><a className="fs-xs text-uppercase text-decoration-none" href="#">Travelling</a>
              <h3 className="fs-base pt-1"><a className="nav-link" href="city-guide-blog-single.html">Air Travel in the Time of COVID-19</a></h3><a className="d-flex align-items-center text-decoration-none" href="#">
                <img className="rounded-circle" src="img/avatars/16.png" width="44" alt="Avatar"/>
                <div className="ps-2">
                  <h6 className="fs-sm text-nav lh-base mb-1">Bessie Cooper</h6>
                  <div className="d-flex text-body fs-xs"><span className="me-2 pe-1"><i className="fi-calendar-alt opacity-70 mt-n1 me-1 align-middle"></i>May 24</span><span><i className="fi-chat-circle opacity-70 mt-n1 me-1 align-middle"></i>No comments</span></div>
                </div></a>
            </article>
            {/* <!-- Item--> */}
            <article><a className="d-block mb-3" href="city-guide-blog-single.html">
              <img className="rounded-3" src="img/city-guide/blog/02.jpg" alt="Post image"/></a><a className="fs-xs text-uppercase text-decoration-none" href="#">Entertainment</a>
              <h3 className="fs-base pt-1"><a className="nav-link" href="city-guide-blog-single.html">10 World-Class Museums You Can Visit Online</a></h3><a className="d-flex align-items-center text-decoration-none" href="#">
                <img className="rounded-circle" src="img/avatars/18.png" width="44" alt="Avatar"/>
                <div className="ps-2">
                  <h6 className="fs-sm text-nav lh-base mb-1">Annette Black</h6>
                  <div className="d-flex text-body fs-xs"><span className="me-2 pe-1"><i className="fi-calendar-alt opacity-70 mt-n1 me-1 align-middle"></i>Apr 28</span><span><i className="fi-chat-circle opacity-70 mt-n1 me-1 align-middle"></i>4 comments</span></div>
                </div></a>
            </article>
            {/* <!-- Item--> */}
            <article><a className="d-block mb-3" href="city-guide-blog-single.html">
              <img className="rounded-3" src="img/city-guide/blog/03.jpg" alt="Post image"/></a><a className="fs-xs text-uppercase text-decoration-none" href="#">Travelling</a>
              <h3 className="fs-base pt-1"><a className="nav-link" href="city-guide-blog-single.html">7 Tips for Solo Travelers in Africa</a></h3><a className="d-flex align-items-center text-decoration-none" href="#">
                <img className="rounded-circle" src="img/avatars/17.png" width="44" alt="Avatar"/>
                <div className="ps-2">
                  <h6 className="fs-sm text-nav lh-base mb-1">Ralph Edwards</h6>
                  <div className="d-flex text-body fs-xs"><span className="me-2 pe-1"><i className="fi-calendar-alt opacity-70 mt-n1 me-1 align-middle"></i>Apr 15</span><span><i className="fi-chat-circle opacity-70 mt-n1 me-1 align-middle"></i>2 comments</span></div>
                </div></a>
            </article>
          </div>
        </div>
      </section>
    </main>
            
        </div >
    )
}

export default CheckList
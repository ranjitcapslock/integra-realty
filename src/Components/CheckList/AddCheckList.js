import React, { useState, useEffect } from 'react'
import AllTab from '../../Pages/AllTab';
import { NavLink, useParams } from 'react-router-dom';
import _ from 'lodash';
import user_service from '../service/user_service';
import Loader from "../../Pages/Loader/Loader.js";
import Toaster from '../../Pages/Toaster/Toaster.js';
import jwt from "jwt-decode";

const AddCheckList = () => {

    const [loader, setLoader] = useState({ isActive: null })
    const { isActive } = loader;
    const [toaster, setToaster] = useState({ types: null, isShow: null, toasterBody: "", message: "" });
    const { types, isShow, toasterBody, message } = toaster;

    const [single, setSingle] = useState("single");
    const [check, setCheck] = useState("");

    const [checkAll, setCheckAll] = useState("");

    
    const initialValues = {
        format: "",formatId:"", trans_type: "", agentId: "",
       category: "Residential", type: "Sale & Lease", phase: "Start", side: "Both Sides", due_date: "", days: "3-Days", before: "before", datetime_phase: "Start", title: "", description: "", required: ""
    }
    const [formValues, setFormValues] = useState(initialValues);
    const [formErrors, setFormErrors] = useState({});
    const [isSubmitClick, setISSubmitClick] = useState(false)


    const [summary, setSummary] = useState("");
    const params = useParams();

  
    useEffect(() => { window.scrollTo(0, 0);
        // setLoader({ isActive: true })
        const TransactionGetById = async () => {

            await user_service.transactionGetById(params.id).then((response) => {
                // setLoader({ isActive: false })
                if (response) {
                   // console.log(response);
                    setSummary(response.data);
                }
            });
        }
        TransactionGetById(params.id)
    }, []);


    useEffect(() => { 
        if (formValues && isSubmitClick) {
            validate()
        }
    }, [formValues])




    const validate = () => {
        const values = formValues
        const errors = {};
        if (!values.title) {
            errors.title = "title is required";
        }

        if (!values.description) {
            errors.description = "description is required";
        }

        
        setFormErrors(errors)
        return errors;
    }

    const handleChange = (e) => {
        const { name, value } = e.target;
        setFormValues({ ...formValues, [name]: value });
    };



   
    {/* <!-- Form Validation End--> */ }

    const handleSubmit = async (e) => {
        e.preventDefault();

        setISSubmitClick(true)
        let checkValue = validate()
        if (_.isEmpty(checkValue)) {

        
        const userData = {
            formatId:single == "single" ? params.id : jwt(localStorage.getItem("auth")).id,
            format: single,
            trans_type: check,
            agentId: jwt(localStorage.getItem("auth")).id,
            category: formValues.category,
            type: formValues.type,
            side: formValues.side,
            phase: formValues.phase,
            due_date: formValues.due_date,
            days: formValues.days,
            before: formValues.before,
            datetime_phase: formValues.datetime_phase,
            title: formValues.title,
            description: formValues.description,
            required: formValues.required,
           transaction: params.id
        }
        console.log(userData)
    
    // else if(single=== "associate"){
    //     const userData = {

    //         format: params.id,
    //         trans_type: check,
    //         agentId: jwt(localStorage.getItem("auth")).id,
    //         category: formValues.category,
    //         type: formValues.type,
    //         side: formValues.side,
    //         phase: formValues.phase,
    //         due_date: formValues.due_date,
    //         days: formValues.days,
    //         before: formValues.before,
    //         datetime_phase: formValues.datetime_phase,
    //         title: formValues.title,
    //         description: formValues.description,
    //         required: formValues.required,
    //        transaction: params.id
    //     }
    //     console.log(userData)
    // }

        // setLoader({ isActive: true })
        // await user_service.checklistPost(userData).then((response) => {
        //     // setLoader({ isActive: false })
        //     if (response) {
        //         setToaster({ types: "success", isShow: true, toasterBody: response.data.message, message: "Success", });

        //     }
            

        // })
    }

    }

    const CheckList = (e) => {
        setSingle(e.target.name)
        setSingle(e.target.value)
    }

    const CheckListAnyTime = (e) => {
        console.log(e.target.name);
        console.log(e.target.value);

        setCheckAll(e.target.name)
        setCheckAll(e.target.value)
    }

    const CheckListAll = (e) => {
        console.log(e.target.name);
        console.log(e.target.value);

        setCheck(e.target.name)
        setCheck(e.target.value)
    }
    return (
        <div className="bg-secondary float-left w-100 pt-4">
            <Loader isActive={isActive} />
            {isShow && <Toaster types={types}
                isShow={isShow}
                toasterBody={toasterBody}
                message={message} />}
            <main className="page-wrapper">
                <AllTab transaction_data = {summary}/>

                {/* <!-- Page container--> */}
                <div className="container mb-md-4 ">
                    <div className="bg-light shadow-sm rounded-3 p-2 p-md-5 mb-2">
                        <NavLink className="btn btn-primary pull-right" onClick={handleSubmit}>Save Item</NavLink>


                        <div className="row mt-5">

                            <div className="col-md-6 mt-0">
                                <h6>This item belongs to:</h6>
                            </div>
                            <div className="col-md-6 pull-right">

                                <div className="form-check" onClick={(e) => CheckList(e)}>
                                    <div>
                                        <input
                                            className="form-check-input"
                                            id="form-radio-four"
                                            type="radio"
                                            name="check"
                                            value="single"
                                        />
                                        <label className="form-check-label" id="radio-level1">Single-This Transaction Only</label>

                                    </div>
                                    <div>
                                        <input
                                            className="form-check-input"
                                            id="form-radio-4"
                                            type="radio"
                                            name="check"
                                            value="associate"
                                        />
                                        <label className="form-check-label" id="radio-level1">Associate-My Transaction Only</label>
                                    </div>

                                    <div>
                                        <input
                                            className="form-check-input"
                                            id="form-radio-4"
                                            type="radio"
                                            name="check"
                                            value="profile"
                                        />
                                        <label className="form-check-label" id="radio-level1">Profile-All Transaction for Utah</label>

                                    </div>
                                </div>
                            </div>


                            <div className="col-md-6 mt-5">
                                <h6>Transactions this item applies to:</h6>
                            </div>
                            <div className="col-md-6 pull-right  mt-5">

                                <div className="form-check d-flex" onClick={(e) => CheckListAll(e)}>
                                    <div>
                                        <input
                                            className="form-check-input"
                                            id="form-radio-4"
                                            type="radio"
                                            name="checkAll"
                                            value="checkAllTrnsaction"
                                        />
                                        <label className="form-check-label" id="radio-level1">All Transaction</label>
                                    </div>

                                    <div>
                                        <input
                                            className="form-check-input ms-5"
                                            id="form-radio-4"
                                            type="radio"
                                            name="checkAll"
                                            value="specific"
                                        />
                                        <label className="form-check-label" id="radio-level1">Specific Category & Type</label>
                                    </div>

                                </div>
                                {
                                    check === "specific" ?
                                        <div>

                                            <select className="form-select" id="pr-city"
                                                name="category"
                                                onChange={handleChange}
                                                value={formValues.category}
                                            >
                                                <option>Residential</option>
                                                <option>Commercial</option>
                                                <option>Apartment</option>
                                                <option>Referral</option>

                                            </select>
                                            {/* <div className="invalid-tooltip">{formErrors.category}</div> */}



                                            <select className="form-select" id="pr-city"
                                                name="type"
                                                onChange={handleChange}
                                                value={formValues.type}
                                            >
                                                <option>Sale & Lease</option>
                                                <option>Sale</option>
                                                <option>Lease</option>
                                            </select>
                                            {/* <div className="invalid-tooltip">{formErrors.category}</div> */}



                                            <select className="form-select" id="pr-city"
                                                name="side"
                                                onChange={handleChange}
                                                value={formValues.side}
                                            >
                                                <option>Both Sides</option>
                                                <option>Seller</option>
                                                <option>Buyer</option>
                                                <option>Pre-Closing</option>
                                                <option>Post-Closing</option>

                                            </select>
                                            {/* <div className="invalid-tooltip">{formErrors.category}</div> */}

                                        </div>

                                        : ""
                                }

                            </div>









                            <div className="col-md-6 mt-5">
                                <h6>Phase this item should be shown:</h6>
                            </div>
                            <div className="col-md-6 pull-right  mt-5">
                                <select className="form-select" id="pr-city"
                                    name="phase"
                                    onChange={handleChange}
                                    value={formValues.phase}
                                >
                                    <option>Start</option>
                                    <option>Showing</option>
                                    <option>Contract</option>
                                    <option>Pre-Closing</option>
                                    <option>Post-Closing</option>

                                </select>
                                {/* <div className="invalid-tooltip">{formErrors.category}</div> */}
                            </div>




                            {
                                single === "single" ?
                                    <>
                                        <div className="col-md-6 mt-5">
                                            <h6>Date when this item is due:</h6>
                                        </div>
                                        <div className="col-md-6 pull-right  mt-5">
                                            <input className="form-control"
                                                id="inline-form-input"
                                                type="date"
                                                placeholder="Choose date and time"
                                                name="due_date"
                                                onChange={handleChange}
                                                value={formValues.due_date}
                                            />

                                            {/* <div className="invalid-tooltip">{formErrors.category}</div> */}


                                            <select className="form-select mt-2" id="pr-city"
                                                name="days"
                                                onChange={handleChange}
                                                value={formValues.days}
                                            >
                                                <option>0-Days</option>
                                                <option>1-Days</option>
                                                <option>2-Days</option>
                                                <option>3-Days</option>
                                                <option>4-days</option>
                                                <option>5-Days</option>
                                                <option>6-Days</option>
                                                <option>7-Days</option>
                                                <option>8-Days</option>
                                                <option>9-days</option>
                                                <option>10-Days</option>
                                                <option>11-Days</option>
                                                <option>12-Days</option>
                                                <option>13-Days</option>
                                                <option>14-days</option>
                                                <option>15-Days</option>
                                                <option>16-Days</option>
                                                <option>17-Days</option>
                                                <option>18-Days</option>
                                                <option>19-days</option>
                                                <option>20-Days</option>
                                                <option>21-Days</option>
                                                <option>22-Days</option>
                                                <option>23-Days</option>
                                                <option>24-days</option>
                                                <option>25-Days</option>
                                                <option>26-Days</option>
                                                <option>27-Days</option>
                                                <option>28-Days</option>
                                                <option>29-days</option>
                                                <option>30-days</option>
                                                <option>31-days</option>
                                            </select>


                                            <select className="form-select mt-2" id="pr-city"
                                                name="before"
                                                onChange={handleChange}
                                                value={formValues.before}
                                            >
                                                <option>Before</option>
                                                <option>After</option>

                                            </select>

                                            <select className="form-select mt-2" id="pr-city"
                                                name="datetime_phase"
                                                onChange={handleChange}
                                                value={formValues.datetime_phase}
                                            >
                                                <option>Start</option>
                                                <option>Contract</option>
                                                <option>Closing</option>
                                            </select>
                                        </div></>
                                    :
                                    ""

                            }

                            {
                                single === "associate" ?
                                    <>

                                        <div className="col-md-6 mt-5">
                                            <h6>Date when this item is due:</h6>
                                        </div>

                                        <div className="col-md-6 pull-right  mt-5">

                                            <div className="form-check d-flex" onClick={(e) => CheckListAnyTime(e)}>
                                                <div>
                                                    <input
                                                        className="form-check-input"
                                                        id="form-radio-4"
                                                        type="radio"
                                                        name="dateTime"
                                                        value="anyTime"
                                                    />
                                                    <label className="form-check-label" id="radio-level1">Any Time</label>
                                                </div>

                                                <div>
                                                    <input
                                                        className="form-check-input ms-5"
                                                        id="form-radio-4"
                                                        type="radio"
                                                        name="dateTime"
                                                        value={new Date()}
                                                    />
                                                    <label className="form-check-label" id="radio-level1">Set Due Date</label>
                                                </div>
                                            </div>

                                            {
                                                checkAll === "dueDate" ?
                                                    <>
                                                        <select className="form-select mt-2" id="pr-city"
                                                            name="days"
                                                            onChange={handleChange}
                                                            value={formValues.days}
                                                        >
                                                            <option>0-Days</option>
                                                            <option>1-Days</option>
                                                            <option>2-Days</option>
                                                            <option>3-Days</option>
                                                            <option>4-days</option>
                                                            <option>5-Days</option>
                                                            <option>6-Days</option>
                                                            <option>7-Days</option>
                                                            <option>8-Days</option>
                                                            <option>9-days</option>
                                                            <option>10-Days</option>
                                                            <option>11-Days</option>
                                                            <option>12-Days</option>
                                                            <option>13-Days</option>
                                                            <option>14-days</option>
                                                            <option>15-Days</option>
                                                            <option>16-Days</option>
                                                            <option>17-Days</option>
                                                            <option>18-Days</option>
                                                            <option>19-days</option>
                                                            <option>20-Days</option>
                                                            <option>21-Days</option>
                                                            <option>22-Days</option>
                                                            <option>23-Days</option>
                                                            <option>24-days</option>
                                                            <option>25-Days</option>
                                                            <option>26-Days</option>
                                                            <option>27-Days</option>
                                                            <option>28-Days</option>
                                                            <option>29-days</option>
                                                            <option>30-days</option>
                                                            <option>31-days</option>


                                                        </select>

                                                        <select className="form-select mt-2" id="pr-city"
                                                            name="before"
                                                            onChange={handleChange}
                                                            value={formValues.before}

                                                        >
                                                            <option>Before</option>
                                                            <option>After</option>

                                                        </select>

                                                        <select className="form-select mt-2" id="pr-city"
                                                            name="datetime_phase"
                                                            onChange={handleChange}
                                                            value={formValues.datetime_phase}
                                                        >
                                                            <option>Start</option>
                                                            <option>Contract</option>
                                                            <option>Closing</option>
                                                        </select>


                                                    </>

                                                    : ""
                                            }
                                        </div>

                                    </>
                                    :
                                    ""
                            }

                            {
                                single === "profile" ?
                                    <>
                                        <div className="col-md-6 mt-5">
                                            <h6>Date when this item is due:</h6>
                                        </div>

                                        <div className="col-md-6 pull-right  mt-5">

                                            <div className="form-check d-flex" onClick={(e) => CheckListAnyTime(e)}>
                                                <div>
                                                    <input
                                                        className="form-check-input"
                                                        id="form-radio-4"
                                                        type="radio"
                                                        name="dateTime"
                                                        value="anyTime"
                                                    />
                                                    <label className="form-check-label" id="radio-level1">Any Time</label>
                                                </div>

                                                <div>
                                                    <input
                                                        className="form-check-input ms-5"
                                                        id="form-radio-4"
                                                        type="radio"
                                                        name="dateTime"
                                                        value="dueDate"
                                                    />
                                                    <label className="form-check-label" id="radio-level1">Set Due Date</label>
                                                </div>
                                            </div>

                                            {
                                                checkAll === "dueDate" ?
                                                    <>
                                                        <select className="form-select mt-2" id="pr-city"
                                                            name="days"
                                                            onChange={handleChange}
                                                            value={formValues.days}

                                                        >

                                                            <option>1-Days</option>
                                                            <option>2-Days</option>
                                                            <option>3-Days</option>
                                                            <option>4-days</option>
                                                            <option>0-Days</option>
                                                            <option>1-Days</option>
                                                            <option>2-Days</option>
                                                            <option>3-Days</option>
                                                            <option>4-days</option>
                                                            <option>5-Days</option>
                                                            <option>6-Days</option>
                                                            <option>7-Days</option>
                                                            <option>8-Days</option>
                                                            <option>9-days</option>
                                                            <option>10-Days</option>
                                                            <option>11-Days</option>
                                                            <option>12-Days</option>
                                                            <option>13-Days</option>
                                                            <option>14-days</option>
                                                            <option>15-Days</option>
                                                            <option>16-Days</option>
                                                            <option>17-Days</option>
                                                            <option>18-Days</option>
                                                            <option>19-days</option>
                                                            <option>20-Days</option>
                                                            <option>21-Days</option>
                                                            <option>22-Days</option>
                                                            <option>23-Days</option>
                                                            <option>24-days</option>
                                                            <option>25-Days</option>
                                                            <option>26-Days</option>
                                                            <option>27-Days</option>
                                                            <option>28-Days</option>
                                                            <option>29-days</option>
                                                            <option>30-days</option>
                                                            <option>31-days</option>


                                                        </select>

                                                        <select className="form-select mt-2" id="pr-city"
                                                            name="before"
                                                            onChange={handleChange}
                                                            value={formValues.before}

                                                        >
                                                            <option>Before</option>
                                                            <option>After</option>

                                                        </select>

                                                        <select className="form-select mt-2" id="pr-city"
                                                            name="datetime_phase"
                                                            onChange={handleChange}
                                                            value={formValues.datetime_phase}

                                                        >
                                                            <option>Start</option>
                                                            <option>Contract</option>
                                                            <option>Closing</option>
                                                        </select>


                                                    </>

                                                    : ""
                                            }
                                        </div>
                                    </>
                                    :
                                    ""
                            }








                            <div className="col-md-6 mt-5">
                                <h6>Is this item required:</h6>
                            </div>
                            <div className="col-md-6 pull-right d-flex mt-5">

                                <div className="form-check">
                                    <input className="form-check-input" id="form-check-1" type="checkbox"
                                        name="required"
                                        onChange={handleChange}
                                        value="checkBox" />
                                    <label className="form-check-label" id="radio-level1">Yes, this item required</label>
                                </div>
                            </div>

                            <div className="col-md-9 mt-5">
                                <label className="form-label" for="pr-city">Title &nbsp;<NavLink>REQUIRED</NavLink></label>
                                <input className="form-control" id="inline-form-input" type="text"
                                    name="title"
                                    onChange={handleChange}
                                    value={formValues.title}
                                />
                                {formErrors.title && (
                                        <div className="invalid-tooltip">{formErrors.title}</div>
                                    )}
                            </div>

                            <div className="col-md-9 mt-3">

                                <label className="form-label" for="pr-city">Description</label>
                                <input className="form-control" id="inline-form-input" type="text"
                                    name="description"
                                    onChange={handleChange}
                                    value={formValues.description}
                                />
                                  {formErrors.description && (
                                        <div className="invalid-tooltip">{formErrors.description}</div>
                                    )}
                            </div>
                        </div>

                    </div>
                </div>
            </main >
        </div >
    )
}

export default AddCheckList
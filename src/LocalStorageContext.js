// Create a new context file (e.g., LocalStorageContext.js)
import React, { createContext, useContext, useState } from 'react';

const LocalStorageContext = createContext();

export function LocalStorageProvider({ children }) {
    const storedActiveOfficeId = localStorage.getItem('active_office_id');
    const [active_office_id, setActive_office_id] = useState(storedActiveOfficeId);
    return (
      <LocalStorageContext.Provider value={{ active_office_id, setActive_office_id }}>
        {children}
      </LocalStorageContext.Provider>
    );
  }

export function useLocalStorage() {
  return useContext(LocalStorageContext);
}

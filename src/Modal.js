import React, { useState, useEffect } from "react";
import avtar from "./Components/img/avtar.jpg";
import _ from "lodash";
import user_service from "./Components/service/user_service";
import jwt from "jwt-decode";
import Loader from "./Pages/Loader/Loader.js";
import Toaster from "./Pages/Toaster/Toaster";
import { useNavigate } from "react-router-dom";
import ReactPaginate from "react-paginate";

function Modal() {
  const initialValues = {
    firstName: "",
    lastName: "",
    company: "",
    email: "",
    phone: "",
    agentId: "",
    contactpermission: "",
    intranet_id:""
  };
  const [formValues, setFormValues] = useState(initialValues);
  const [formErrors, setFormErrors] = useState({});
  const [isSubmitClick, setISSubmitClick] = useState(false);
  const [loader, setLoader] = useState({ isActive: null });
  const { isActive } = loader;
  const [toaster, setToaster] = useState({
    types: null,
    isShow: null,
    toasterBody: "",
    message: "",
  });
  const { types, isShow, toasterBody, message } = toaster;

  const [clientId, setClientId] = useState("");
  const [summary, setSummary] = useState([]);
  const [arrayData, setArrayData] = useState([]);
  const [select, setSelect] = useState("");

  const [results, setResults] = useState([]);
  const [pageCount, setPageCount] = useState(0);
  const [currentPage, setCurrentPage] = useState(1);
  const [isLoading, setIsLoading] = useState(false);
  const [contactType, setContactType] = useState(false);

  const navigate = useNavigate();
  const [checkBox, setSetCheckbox] = useState(false);

  const handleCheck = () => {
    setSetCheckbox(checkBox ? true : false);
    console.log(checkBox);
  };

  {
    /* <!-- Input onChange Start--> */
  }
  const handleChange = (e) => {
    const { name, value } = e.target;
    setFormValues({ ...formValues, [name]: value });
  };
  {
    /* <!-- Input onChange End--> */
  }

  {
    /* <!-- Form Validation Start--> */
  }
  useEffect(() => {
    window.scrollTo(0, 0);
    if (formValues && isSubmitClick) {
      validate();
    }
  }, [formValues]);

  const validate = () => {
    const values = formValues;
    const errors = {};
    if (!values.firstName) {
      errors.firstName = "name is required";
    }

    if (!values.lastName) {
      errors.lastName = "lastName is required";
    }

    if (!values.email) {
      errors.email = "email is required";
    } else if (
      !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.email)
    ) {
      errors.email = "Invalid email address";
    }

    setFormErrors(errors);
    return errors;
  };
  {
    /* <!-- Form Validation End--> */
  }

  {
    /* <!--Api call Form onSubmit Start--> */
  }
  const handleNewContact = async (e) => {
    e.preventDefault();
    setISSubmitClick(true);
    let checkValue = validate();
    if (_.isEmpty(checkValue)) {
      const userData = {
        active_office: localStorage.getItem("active_office"),
        active_office_id: localStorage.getItem("active_office_id"),
        firstName: formValues.firstName,
        lastName: formValues.lastName,
        nickname: formValues.lastName + "" + formValues.firstName,
        company: formValues.company,
        email: formValues.email,
        phone: formValues.phone,
        agentId: jwt(localStorage.getItem("auth")).id,
        contactType: contactType,
        contactpermission: formValues.contactpermission,
      };
      console.log("userData", userData);
      try {
        setLoader({ isActive: true });
        const response = await user_service.contact(userData);

        if (response) {
          setLoader({ isActive: true });
          //const response = await user_service.contact(userData, formValues.firstName);
          const contactData = response.data;
          setSummary((prevSummary) => [...prevSummary, contactData]);
          setArrayData((prevArrayData) => [...prevArrayData, contactData._id]);
          setSelect("");
          setLoader({ isActive: false });
          if (contactType === "associate") {
            setToaster({
              type: "Agent contact Successfully",
              isShow: true,
              toasterBody: response.data.message,
              message: "Agent contact Successfully",
            });
          } else {
            setToaster({
              type: "User contact Successfully",
              isShow: true,
              toasterBody: response.data.message,
              message: "User contact Successfully",
            });
          }
          setTimeout(() => {
            setToaster((prevToaster) => ({ ...prevToaster, isShow: false }));
            if (contactType === "associate") {
              navigate(`/activate-account/${response.data._id}`);
            } else {
              navigate(`/contact-profile/${response.data._id}`);
            }
          }, 2000);
          document.getElementById("closeModal").click();
        }
      } catch (err) {
        setToaster({
          type: "API Error",
          isShow: true,
          toasterBody: err.response.data.message,
          message: "API Error",
        });

        setTimeout(() => {
          setToaster((prevToaster) => ({ ...prevToaster, isShow: false }));
        }, 2000);
      }
    }
  };
  {
    /* <!-- Api call Form onSubmit End--> */
  }

  const handleSearchTransaction = async (id) => {
    await user_service.contactGetById(id).then((response) => {
      setClientId(id);
      setSelect(response.data);
    });
  };

  const cancel = () => {
    setSelect("");
  };
  const selectContact = async (id) => {
    if (contactType === "associate") {
      navigate(`/activate-account/${id}`);
    } else {
      navigate(`/contact-profile/${id}`);
    }
  };

  {
    /* <!-- paginate Function Api call Start--> */
  }
  const [searchTerm, setSearchTerm] = useState("");

  // useEffect(() => { window.scrollTo(0, 0);
  //     if (formValues) {
  //         SearchGetAll(0);
  //     }
  // }, [formValues]);
  useEffect(() => {
    window.scrollTo(0, 0);
    const delayDebounceFn = setTimeout(() => {
      if (formValues) {
        SearchGetAll(0);
      }
    }, 1000);

    return () => clearTimeout(delayDebounceFn);
  }, [formValues]);

  const SearchGetAll = async () => {
    setIsLoading(true);

    await user_service
      .SearchContactGet(0,3,formValues.firstName)
      .then((response) => {
        setIsLoading(false);
        if (response) {
          setResults(response.data.data);
          setPageCount(Math.ceil(response.data.totalRecords / 10));
        }
      });
  };

  const handlePageClick = async (data) => {
    let currentPage = data.selected + 1;
    let skip = currentPage - 1 + currentPage;
    setLoader({ isActive: true });
    console.log(currentPage);
    await user_service
    .SearchContactGet(currentPage, 3, formValues.firstName)
      .then((response) => {
        setLoader({ isActive: false });
        if (response) {
          setSummary(response.data);
          setResults(response.data.data);
          setPageCount(Math.ceil(response.data.totalRecords / 10));
        }
      });
  };

  const handleSearch = (event) => {
    event.preventDefault();
    setCurrentPage(1);
    SearchGetAll(0);
  };
  {
    /* <!-- paginate Function Api call End--> */
  }

  return (
    <div>
      <Loader isActive={isActive} />
      {isShow && (
        <Toaster
          types={types}
          isShow={isShow}
          toasterBody={toasterBody}
          message={message}
        />
      )}
      <div className="modal" role="dialog" id="addcontact">
        <div className="modal-dialog" role="document">
          <div className="modal-content">
            <div className="modal-header">
              <h2 className="modal-title float-left w-100" id="myModalLabel">
                Add a new contact
              </h2>
              <button
                type="button"
                onClick={(e) => setContactType("")}
                className="close"
                data-dismiss="modal"
              >
                {" "}
                <span aria-hidden="true">×</span>{" "}
              </button>
            </div>

            <div className="modal-body row">
              {contactType && contactType != "" ? (
                <>
                  <div className="col-md-12">
                    {select === "" ? (
                      <>
                        <form onSubmit={handleSearch}>
                          {/* <!-- First Name input --> */}
                          <div className="mb-3">
                            <label className="form-label">First Name</label>
                            <br />
                            <input
                              className="form-control"
                              type="text"
                              id="text-input"
                              name="firstName"
                              placeholder="Enter first name"
                              onChange={(event) => handleChange(event)}
                              autoComplete="on"
                              style={{
                                border: formErrors?.firstName
                                  ? "1px solid red"
                                  : "1px solid #00000026",
                              }}
                              value={formValues.firstName}
                            />
                            <div className="invalid-tooltip">
                              {formErrors.firstName}
                            </div>
                          </div>

                          {/* <!-- Last Name input --> */}
                          <div className="mb-3">
                            <label className="form-label">Last Name</label>
                            <input
                              className="form-control"
                              type="text"
                              id="text-input"
                              name="lastName"
                              placeholder="Enter last name"
                              onChange={(event) => handleChange(event)}
                              autoComplete="on"
                              style={{
                                border: formErrors?.lastName
                                  ? "1px solid red"
                                  : "1px solid #00000026",
                              }}
                              value={formValues.lastName}
                            />
                            <div className="invalid-tooltip">
                              {formErrors.lastName}
                            </div>
                          </div>

                          {/* <!-- Company Input --> */}
                          <div className="mb-3">
                            <label className="form-label">Company</label>
                            <input
                              className="form-control"
                              type="text"
                              id="text-input"
                              name="company"
                              placeholder="Enter company"
                              onChange={(event) => handleChange(event)}
                              autoComplete="on"
                              value={formValues.company}
                            />
                          </div>

                          {/* <!-- Email input --> */}
                          <div className="mb-3">
                            <label className="form-label">Email</label>
                            <input
                              className="form-control"
                              id="email-input"
                              type="text"
                              name="email"
                              placeholder="Enter email"
                              onChange={(event) => handleChange(event)}
                              autoComplete="on"
                              style={{
                                border: formErrors?.email
                                  ? "1px solid red"
                                  : "1px solid #00000026",
                              }}
                              value={formValues.email}
                            />
                            <div className="invalid-tooltip">
                              {formErrors.email}
                            </div>
                          </div>

                          {/* <!-- Phone Input --> */}
                          <div className="mb-3">
                            <label className="form-label">Phone</label>
                            <input
                              className="form-control"
                              type="tel"
                              id="tel-input"
                              name="phone"
                              placeholder="Enter Phone"
                              onChange={(event) => handleChange(event)}
                              autoComplete="on"
                              pattern="[0-9]+"
                              required
                              title="Please enter a valid phone number"
                              value={formValues.phone}
                            />
                          </div>

                          {contactType && contactType == "associate" ? (
                            <div className="mb-3">
                              <label className="form-label">Permision As</label>
                              <select
                                className="form-select form-select-dark"
                                name="contactpermission"
                                value={formValues.contactpermission}
                                onChange={(event) => handleChange(event)}
                              >
                                <option value="full">Full Agent</option>
                                <option value="referralonly">
                                  Referral Only Agent
                                </option>
                              </select>
                            </div>
                          ) : (
                            ""
                          )}

                          {contactType && contactType == "associate" ? (
                            <div className="mb-3">
                              <label className="form-label">Intranet ID</label>
                              <input
                                className="form-select form-select-dark"
                                name="intranet_id"
                                value={formValues.intranet_id?? formValues.email}
                                onChange={(event) => handleChange(event)}
                              />
                            </div>
                          ) : (
                            ""
                          )}

                          {/* <!-- Checkbox Input --> */}
                          <div>
                            <div className="form-check">
                              <input
                                className="form-check-input"
                                name="checkbox"
                                type="checkbox"
                                id="agree-to-terms"
                                onClick={handleCheck}
                                value={checkBox.checkBox}
                              />
                              <div className="invalid-tooltip">
                                {checkBox.checkBox}
                              </div>
                              <label className="form-check-label">
                                No email address available.
                              </label>
                            </div>
                          </div>
                        </form>
                      </>
                    ) : (
                      <div className="col-md-6 mt-5 w-100">
                        <div className="card bg-secondary">
                          <h6>{select.represent}</h6>
                          <div className="card-body">
                            <img
                              className="pull-right"
                              onClick={cancel}
                              src="https://cdn.pboffice.net/shared/transaction/page_select_drop.png"
                            />
                            <div className="view_profile">
                              <img
                                className="pull-left"
                                src={
                                  select.image ||
                                  "https://integra.backagent.net/common/handler/pub/photo.ashx?V=1703c0890a0462fd68520b884a04f86f.0&C=people&S=2&P=%2f!Default%2fThumb%2fphoto_pending.jpg"
                                }
                                alt="Profile"
                              />

                              <span>
                                <a href="#" className="card-title mt-0">
                                  {select.firstName}&nbsp;{select.lastName}
                                </a>
                                <p>
                                  {select.company}
                                  <br />
                                  {select.phone}
                                  <br />
                                  <a href="#">Send Message</a>
                                </p>
                              </span>
                            </div>
                          </div>
                        </div>
                        <button
                          type="button"
                          className="btn btn-default btn btn-secondary btn-sm"
                          id="closeModal"
                          data-dismiss="modal"
                          onClick={cancel}
                        >
                          Cancel & Close
                        </button>
                        <button
                          type="button"
                          className="btn btn-primary btn-sm  pull-left mt-2"
                          onClick={(e) => selectContact(select._id)}
                        >
                          Select Contact
                        </button>
                      </div>
                    )}
                  </div>

                  <div className="modal-footer">
                    <button
                      type="button"
                      className="btn btn-default btn btn-secondary btn-sm"
                      id="closeModal"
                      data-dismiss="modal"
                    >
                      Cancel & Close
                    </button>
                    <button
                      type="button"
                      className="btn btn-primary btn-sm"
                      id="save-button"
                      onClick={handleNewContact}
                    >
                      Add New Contact
                    </button>
                  </div>
                </>
              ) : (
                <div className="row col-md-12 text-center">
                  <div className="col-md-12 text-center">
                    <div className="row m-4 text-center">
                      <ul
                        className="list-group col-md-4"
                        onClick={(e) => setContactType("private")}
                      >
                        <li className="list-group-item d-flex justify-content-between align-items-center">
                          <div className="mt-2 mb-2 w-100">
                            <div className="icon-box-media bg-faded-primary text-primary rounded-circle mb-3 mx-auto">
                              <i className="fi-shop"></i>
                            </div>
                            <strong>Private Contact</strong>
                            <br />
                          </div>
                        </li>
                      </ul>
                      <ul
                        className="list-group col-md-4"
                        onClick={(e) => setContactType("community")}
                      >
                        <li className="list-group-item d-flex justify-content-between align-items-center">
                          <div className="mt-2 mb-2 w-100">
                            <div className="icon-box-media bg-faded-primary text-primary rounded-circle mb-3 mx-auto">
                              <i className="fi-shop"></i>
                            </div>
                            <strong>Community Contact</strong>
                            <br />
                          </div>
                        </li>
                      </ul>
                      {localStorage.getItem("auth") &&
                      jwt(localStorage.getItem("auth")).contactType ==
                        "admin" ? (
                        <ul
                          className="list-group col-md-4"
                          onClick={(e) => setContactType("associate")}
                        >
                          <li className="list-group-item d-flex justify-content-between align-items-center">
                            <div className="mt-2 mb-2 w-100">
                              <div className="icon-box-media bg-faded-primary text-primary rounded-circle mb-3 mx-auto">
                                <i className="fi-shop"></i>
                              </div>
                              <strong>Associate</strong>
                              <br />
                            </div>
                          </li>
                        </ul>
                      ) : (
                        ""
                      )}
                    </div>
                  </div>
                </div>
              )}
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
export default Modal;

import { useEffect } from 'react';

function ScrollToTop() {
  useEffect(() => { window.scrollTo(0, 0);
    console.log('ScrollToTop executed'); // Add this line for debugging
    window.scrollTo(0, 0);
  }, []);

  return null;
}

export default ScrollToTop;

import React, { useState, useEffect } from "react";
import avtar from "./Components/img/avtar.jpg";
import _ from 'lodash';
import user_service from './Components/service/user_service';
import jwt from "jwt-decode";
import Loader from "./Pages/Loader/Loader.js";
import Toaster from './Pages/Toaster/Toaster';
import { useNavigate } from "react-router-dom";
import ReactPaginate from "react-paginate";

function Modalresetpassword(props) {

    const { contactId , onResetSuccess} = props;
    // console.log(contactId)
    const initialValues = { firstName: "", lastName: "", company: "", email: "", phone: "", agentId: "" };
    const [formValues, setFormValues] = useState(initialValues);
    const [formErrors, setFormErrors] = useState({});
    const [isSubmitClick, setISSubmitClick] = useState(false)
    const [loader, setLoader] = useState({ isActive: null })
    const { isActive } = loader;
    const [toaster, setToaster] = useState({ types: null, isShow: null, toasterBody: "", message: "" });
    const { types, isShow, toasterBody, message } = toaster;

    const [clientId, setClientId] = useState("")
    const [summary, setSummary] = useState("");
    const [results, setResults] = useState([]);
    const [pageCount, setPageCount] = useState(0);
    const [currentPage, setCurrentPage] = useState(1);
    const [isLoading, setIsLoading] = useState(false);
    const [contactType, setContactType] = useState(false);


    const navigate = useNavigate();
    const [checkBox, setSetCheckbox] = useState(false);

    const handleCheck = () => {
        setSetCheckbox(checkBox ? true : false);
       // console.log(checkBox)
    };

    {/* <!-- Input onChange Start--> */ }
    const handleChange = (e) => {
        const { name, value } = e.target;
        setFormValues({ ...formValues, [name]: value });
    };
    {/* <!-- Input onChange End--> */ }


    {/* <!-- Form Validation Start--> */ }
    useEffect(() => { 
        if (formValues && isSubmitClick) {
            validate()
        }
    }, [formValues])

    const validate = () => {
        const values = formValues
        const errors = {};
        if (!values.firstName) {
            errors.firstName = "name is required";
        }

        if (!values.lastName) {
            errors.lastName = "lastName is required";
        }

        if (!values.company) {
            errors.company = "company is required";
        }


    if (!values.email) {
        errors.email = "email is required";
      } else if (
        !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i.test(values.email)
      ) {
        errors.email = "Invalid email address";
      }

        // if (!values.email) {
        //     errors.email = "email is required";
        // }
        // else if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.email)) {
        //     errors.email = 'Invalid email address'
        // }
        if (!values.phone) {
            errors.phone = "phoneNumber is required";
        }

        if (values.phone && values.phone.length != 10) {
            errors.phone = "PhoneNumber is 10 digit required!";
        }

        setFormErrors(errors)
        return errors;
    }
    {/* <!-- Form Validation End--> */ }



    {/* <!--Api call Form onSubmit Start--> */ }
    const handlesetuppassword = async (e) => {
        e.preventDefault();
        setISSubmitClick(true)

            const userData = {
                _id:contactId,
                agentId: jwt(localStorage.getItem("auth")).id,
            };
           // console.log("userData", userData)
            try {
                setLoader({ isActive: true })
                const response = await user_service.resetpasswordlink(userData);
                if (response) {

                    setClientId(response.data._id);
                    setSummary(response.data)
                    // setResults(response.data);
                    setLoader({ isActive: false })
                    setFormValues(initialValues);
                    setToaster({ type: "Password Reset Link Sent Successfully", isShow: true, toasterBody: response.data.message, message: "Password Reset Link Sent Successfully", });
                    document.getElementById('closeModal').click();
                    // if(contactType === "associate" ){
                    //     navigate(`/activate-account/${response.data._id}`);
                    // }
                    
                    // If the parent component provided the onResetSuccess callback, call it to notify the parent of the success
                    if (typeof onResetSuccess === "function") {
                        onResetSuccess();
                    }
                }
                else {
                    setLoader({ isActive: false })
                    setToaster({ types: "error", isShow: true, toasterBody: response.data.message, message: "Error", });
                }
            }
            catch (error) {
                setLoader({ isActive: false })
                setToaster({ types: "error", isShow: true, toasterBody: error, message: "Error", });
            }
            setTimeout(() => {
                setToaster({ types: "error", isShow: false, toasterBody: null, message: "Error", });
            }, 2000);
        
    }
    {/* <!-- Api call Form onSubmit End--> */ }

    return (
        <div>
            <Loader isActive={isActive} />
            {isShow && <Toaster types={types} isShow={isShow} toasterBody={toasterBody} message={message} />}
            <div className="modal" role="dialog" id="Modalresetpassword">

                <div className="modal-dialog" role="document">
                    <div className="modal-content">

                    <div className="modal-header">
                        <h2 className="modal-title float-left w-100" id="myModalLabel" >
                        Reset Password
                        </h2>
                        <button type="button" onClick={(e) => setContactType("")} className="close" data-dismiss="modal"> <span aria-hidden="true">×</span> </button>
                    </div>

                        <div className="modal-body row">
                            <div className=" col-md-12 mt-12" >
                            <form >
                                <h6>Password Reset Process</h6>
                                <p>
                                    To ensure account privacy and security, safeguards have been put in place to protect the accounts of each associate in the system. To create a new password for this account, an account setup link must be created. Please use the following button to confirm you wish to create a new Account Setup Link for this account.
                                </p>
                                <div>
                                    <div className="form-check">
                                        <input className="form-check-input"
                                            name="checkbox"
                                            type="checkbox"
                                            id="agree-to-terms"
                                            onClick={handleCheck}
                                            value={checkBox.checkBox}
                                        />
                                        <label className="form-check-label">Send Password Reset Email Now</label>
                                    </div>
                                </div>
                            </form>
                            </div>
                            <div className="modal-footer">
                                <button type="button" className="btn btn-default btn btn-secondary btn-sm" id="closeModal" data-dismiss="modal">Cancel & Close</button>
                                <button type="button" className="btn btn-primary btn-sm" id="save-button" onClick={handlesetuppassword}>Create Account Setup Link</button>
                            </div>


                        </div>

                    </div>
                </div>
            </div>
        </div>
    )
}
export default Modalresetpassword;
import React from 'react';
import Spinner from 'react-bootstrap/Spinner';
import './Loader.scss';

function Loader({ isActive }) {

  if (isActive) {
    return (
      <div className='loader' >
        <Spinner animation="grow" variant="warning" />
      </div>
    );
  }

}

export default Loader;
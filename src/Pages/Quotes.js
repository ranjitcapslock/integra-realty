import React from 'react';

const Quotes = () => (
    <>
    <script type="text/javascript" src="https://www.brainyquote.com/link/quotebr.js"></script>
    <small><i><a href="/quote_of_the_day" target="_blank" rel="nofollow">more Quotes</a></i></small>
    </>
);

export default Quotes;
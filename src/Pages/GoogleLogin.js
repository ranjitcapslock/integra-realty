import React, { useState, useEffect, useRef } from "react";
import { LoginSocialGoogle } from "reactjs-social-login";
import { GoogleLoginButton } from "react-social-login-buttons";
import Loader from "./Loader/Loader.js";
import Toaster from "./Toaster/Toaster.js";
import { Editor } from "react-draft-wysiwyg";
import { EditorState, convertToRaw } from "draft-js";
import draftToHtml from "draftjs-to-html";
import "react-draft-wysiwyg/dist/react-draft-wysiwyg.css";

const CLIENT_ID =
  "765365535217-7anjfp0mabknck4mf2tib84b90j4i4p4.apps.googleusercontent.com";
const GoogleLogin = () => {
  const [accessTokens, setAccessToken] = useState(null);
  const [userInfo, setUserInfo] = useState("");
  const [emails, setEmails] = useState([]);
  const [inbox, setInbox] = useState([]);
  const [singleEmail, setSingleEmail] = useState("");
  const [singleSent, setSingleSent] = useState("");
  const [sentEmails, setSentEmails] = useState([]);
  const [issubmit, setIssubmit] = useState(false);

  const [loader, setLoader] = useState({ isActive: null });
  const { isActive } = loader;
  const [toaster, setToaster] = useState({
    types: null,
    isShow: null,
    toasterBody: "",
    message: "",
  });
  const { types, isShow, toasterBody, message } = toaster;

  const [formValues, setFormValues] = useState({
    to: "",
    cc: "",
    subject: "",
    body: "",
  });

  const editorRef = useRef(null);

  const onLoginStart = () => {
    console.log("login start");
  };

  const handleLoginResolve = async ({ provider, data }) => {
    setAccessToken(data?.access_token);
  };

  const handleLoginReject = (err) => {
    console.error("Login rejected:", err);
  };

  useEffect(() => {
    const fetchUserInfo = async () => {
      try {
        const response = await fetch(
          `https://www.googleapis.com/oauth2/v1/userinfo?access_token=${accessToken}`,
          {
            headers: {
              Authorization: `Bearer ${accessTokens}`,
            },
          }
        );

        if (response) {
          let data = await response.json();
          console.log(data);
          setUserInfo(data);
          fetchEmails(accessTokens);
        } else {
        }
      } catch (error) {}
    };

    const accessToken = accessTokens;
    if (accessToken) {
      fetchUserInfo(accessToken);
    }
  }, [accessTokens]);

  useEffect(() => {
    if (emails) {
      handleEmailClick();
    }
  }, [emails]);

  const fetchEmails = async (accessToken) => {
    try {
      const response = await fetch(
        `https://www.googleapis.com/gmail/v1/users/me/messages`,
        {
          headers: {
            Authorization: `Bearer ${accessToken}`,
          },
        }
      );

      if (response.ok) {
        const data = await response.json();
        setEmails(data.messages || []);
      } else {
        throw new Error("Failed to fetch emails");
      }
    } catch (error) {
      console.error("Error fetching emails:", error);
    }
  };

  const handleEmailClick = async () => {
    setSingleEmail("");
    setSingleSent("");
    try {
      const fetchedEmails = await Promise.all(
        emails.map(async (email) => {
          setLoader({ isActive: true });
          const response = await fetch(
            `https://www.googleapis.com/gmail/v1/users/me/messages/${email.id}`,
            {
              headers: {
                Authorization: `Bearer ${accessTokens}`,
              },
            }
          );
          if (response.ok) {
            setLoader({ isActive: false });
            const data = await response.json();
            return data; // Return the fetched data
          } else {
            throw new Error("Failed to fetch email");
          }
        })
      );

      setInbox(fetchedEmails);
    } catch (error) {
      console.error("Error fetching email:", error);
    }
  };

  const handleSingleGmailData = async (emailId) => {
    setSingleSent("");
    setSentEmails([]);
    try {
      const response = await fetch(
        `https://www.googleapis.com/gmail/v1/users/me/messages/${emailId}`,
        {
          headers: {
            Authorization: `Bearer ${accessTokens}`,
          },
        }
      );

      if (response.ok) {
        const data = await response.json();
        setSingleEmail(data); // Set the single email data
      } else {
        throw new Error("Failed to fetch email");
      }
    } catch (error) {
      console.error("Error fetching email:", error);
    }
  };

  const handleSingleGmailSent = async (emailId) => {
    setSentEmails([]);
    setSingleEmail("");
    try {
      const response = await fetch(
        `https://www.googleapis.com/gmail/v1/users/me/messages/${emailId}`,
        {
          headers: {
            Authorization: `Bearer ${accessTokens}`,
          },
        }
      );

      if (response.ok) {
        const data = await response.json();
        setSingleSent(data);
      } else {
        throw new Error("Failed to fetch email");
      }
    } catch (error) {
      console.error("Error fetching email:", error);
    }
  };

  const decodeBase64 = (str) => {
    if (!str || str === "") {
      console.error(
        "Base64 decoding failed: Input string is undefined or empty"
      );
      return "";
    }

    try {
      return decodeURIComponent(
        atob(str.replace(/-/g, "+").replace(/_/g, "/"))
          .split("")
          .map((c) => "%" + ("00" + c.charCodeAt(0).toString(16)).slice(-2))
          .join("")
      );
    } catch (err) {
      console.error("Base64 decoding failed:", err);
      return str;
    }
  };

  // const isPdfAttachment = (payload) => {
  //   if (!payload || !payload.parts) return false;
  //   return payload.parts.some(
  //     (part) =>
  //       part.mimeType === "application/pdf" ||
  //       part.mimeType.startsWith("image/") ||
  //       part.mimeType === "application/msword" ||
  //       (part.filename && part.filename.endsWith(".pdf"))
  //   );
  // };

  const [attachmentData, setAttachmentData] = useState([]);
  const fetchAttachment = async (messageId, attachmentId, accessToken) => {
    const response = await fetch(
      `https://www.googleapis.com/gmail/v1/users/me/messages/${messageId}/attachments/${attachmentId}`,
      {
        headers: {
          Authorization: `Bearer ${accessToken}`,
        },
      }
    );

    if (!response.ok) {
      throw new Error("Failed to fetch email attachment");
    }

    const data = await response.json();
    return cleanBase64String(data.data);
  };

  // Function to get the HTML part of the message
  const getHTMLPart = (parts) => {
    for (let part of parts) {
      if (!part.parts) {
        if (
          part.mimeType === "text/html" ||
          part.mimeType === "application/pdf" ||
          part.mimeType.startsWith("image/")
        ) {
          return part.body?.data || "";
        }
      } else {
        const result = getHTMLPart(part.parts);
        if (result) {
          return result;
        }
      }
    }
    return "";
  };

  // Function to get the body of the message
  const getBody = (message) => {
    let encodedBody = "";
    if (message && message.parts && message.parts.length > 0) {
      encodedBody = getHTMLPart(message.parts);
    } else if (message && message.body && message.body.data) {
      encodedBody = message.body.data;
    }
    return decodeBase64(encodedBody);
  };

  // Function to get attachment parts from the message
  const getAttachmentParts = async (message, accessToken) => {
    if (!message || !message.payload || !message.payload.parts) {
      return [];
    }

    const attachmentParts = message.payload.parts.filter(
      (part) => part.body && part.body.attachmentId
    );

    try {
      const attachmentsData = await Promise.all(
        attachmentParts.map(async (part) => {
          const data = await fetchAttachment(
            message.id,
            part.body.attachmentId,
            accessToken
          );
          return {
            mimeType: part.mimeType,
            filename: part.filename,
            data,
          };
        })
      );

      return attachmentsData;
    } catch (error) {
      console.error("Error fetching email attachments:", error);
      return [];
    }
  };

  const isAttachmentPresent = (payload) => {
    if (!payload || !payload.parts) return false;
    return payload.parts.some(
      (part) =>
        part.mimeType.startsWith("application/pdf") ||
        part.mimeType.startsWith("image/") ||
        part.mimeType.startsWith("application/msword") ||
        (part.filename && part.filename.endsWith(".pdf"))
    );
  };

  useEffect(() => {
    const fetchAttachments = async () => {
      try {
        let message = singleSent || singleEmail;
        if (message) {
          const data = await getAttachmentParts(message, accessTokens);
          setAttachmentData(data);
        }
      } catch (error) {
        console.error("Error fetching email:", error);
      }
    };

    fetchAttachments();
  }, [accessTokens, singleSent, singleEmail]);

  const cleanBase64String = (str) => {
    return str.replace(/_/g, "/").replace(/-/g, "+");
  };

  const handleSentEmail = async () => {
    setInbox([]);
    setSingleEmail("");
    try {
      const response = await fetch(
        `https://www.googleapis.com/gmail/v1/users/me/messages?q=is:sent`,
        {
          headers: {
            Authorization: `Bearer ${accessTokens}`,
          },
        }
      );

      if (response.ok) {
        const data = await response.json();
        const fetchedEmails = await Promise.all(
          data.messages.map(async (email) => {
            setLoader({ isActive: true });
            const response = await fetch(
              `https://www.googleapis.com/gmail/v1/users/me/messages/${email.id}`,
              {
                headers: {
                  Authorization: `Bearer ${accessTokens}`,
                },
              }
            );
            if (response.ok) {
              setLoader({ isActive: false });
              const emailData = await response.json();
              return emailData;
            } else {
              throw new Error("Failed to fetch sent email");
            }
          })
        );
        setSentEmails(fetchedEmails);
      } else {
        throw new Error("Failed to fetch sent emails");
      }
    } catch (error) {
      console.error("Error fetching sent emails:", error);
    }
  };

  const handleBackMail = () => {
    setSingleEmail("");
  };

  const [attachments, setAttachments] = useState([]);
  const handleBack = () => {
    setFormValues({});
  };

  const handleBackSent = () => {
    handleEmailClick();
    setSingleSent("");
  };

  const handleInputFile = async (e) => {
    const files = Array.from(e.target.files);

    const attachmentsData = await Promise.all(
      files.map(async (file) => {
        const base64Data = await readFileAsBase64(file);
        setToaster({
          type: "File Uploded",
          isShow: true,
          message: "File Uploded Successfully",
        });
        setTimeout(() => {
          setToaster((prevToaster) => ({ ...prevToaster, isShow: false }));
        }, 2000);

        return {
          name: file.name,
          type: file.type,
          content: base64Data,
          url: URL.createObjectURL(file),
        };
      })
    );
    setAttachments(attachmentsData);
  };

  const readFileAsBase64 = (file) => {
    return new Promise((resolve, reject) => {
      const reader = new FileReader();
      reader.onload = () => resolve(reader.result.split(",")[1]);
      reader.onerror = (error) => reject(error);
      reader.readAsDataURL(file);
    });
  };

  const handleInputChange = (e) => {
    setFormValues({
      ...formValues,
      [e.target.name]: e.target.value,
    });
  };

  const [editorState, setEditorState] = useState(EditorState.createEmpty());

  const handleInput = (editorState) => {
    setEditorState(editorState);
    const htmlContent = draftToHtml(
      convertToRaw(editorState.getCurrentContent())
    );
    setFormValues({ ...formValues, body: htmlContent });
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
    try {
      let email;

      if (attachments.length > 0) {
        setIssubmit(true);
        // console.log(attachments);
        email = await makeEmailBodyWithAttachments(formValues, attachments);
      } else {
        setIssubmit(true);
        email = await makeEmailBodyWithoutAttachments(formValues);
      }
      if (email) {
        await sendEmail(email, accessTokens);
        alert("Email sent successfully!");
        setIssubmit(false);
      }
    } catch (error) {
      console.error("Error sending email:", error);
      alert(`Failed to send email: ${error.message}`);
    }
  };

  const makeEmailBodyWithAttachments = async (
    { to, cc, subject, body },
    attachments
  ) => {
    const boundary = "foo_bar_baz";
    let email = [
      `To: ${to}`,
      `Subject: ${subject}`,
      "Content-Type: multipart/mixed; boundary=" + boundary,
      "",
      "--" + boundary,
      "Content-Type: text/plain; charset=UTF-8",
      "",
      body,
      "",
    ];

    if (cc) {
      email.splice(1, 0, `Cc: ${cc}`);
    }

    for (let attachment of attachments) {
      email = email.concat([
        "--" + boundary,
        `Content-Type: ${attachment.type}; name="${attachment.name}"`,
        "Content-Transfer-Encoding: base64",
        `Content-Disposition: attachment; filename="${attachment.name}"`,
        "",
        attachment.content,
        "",
      ]);
    }

    email.push("--" + boundary + "--");

    const rawEmail = email.join("\r\n");
    const base64EncodedEmail = btoa(unescape(encodeURIComponent(rawEmail)));

    return base64EncodedEmail;
  };

  const makeEmailBodyWithoutAttachments = async ({
    to,
    cc,
    subject,
    body,
    isHtml = true,
  }) => {
    const emailBody = [
      `To: ${to}`,
      `Subject: ${subject}`,
      "Content-Type: text/plain; charset=UTF-8",
      "",
      isHtml ? body : `\r\n${body}`,
    ];

    if (cc) {
      emailBody.splice(1, 0, `Cc: ${cc}`);
    }

    let email = emailBody.join("\r\n");

    if (isHtml) {
      email = `Content-Type: text/html; charset=utf-8\r\n${email}`;
    }

    const base64EncodedEmail = window.btoa(unescape(encodeURIComponent(email)));
    return base64EncodedEmail;
  };

  const sendEmail = async (email, accessToken) => {
    try {
      const response = await fetch(
        "https://www.googleapis.com/gmail/v1/users/me/messages/send",
        {
          method: "POST",
          headers: {
            Authorization: `Bearer ${accessToken}`,
            "Content-Type": "application/json",
          },
          body: JSON.stringify({
            raw: email,
          }),
        }
      );
      if (response.ok) {
        setToaster({
          type: "Email Send Successfully",
          isShow: true,
          message: "Email Send Successfully",
        });
        setTimeout(() => {
          setToaster((prevToaster) => ({ ...prevToaster, isShow: false }));
        }, 2000);
      } else {
        const errorData = await response.json();
        throw new Error(`Error: ${errorData.error.message}`);
      }

      return await response.json();
    } catch (error) {
      console.error("Error sending email:", error);
      throw error;
    }
  };

  const handleCompose = () => {
    setFormValues("");
  };

  return (
    <div className="bg-secondary float-left w-100 pt-4 mb-4">
      <Loader isActive={isActive} />
      {isShow && (
        <Toaster
          types={types}
          isShow={isShow}
          toasterBody={toasterBody}
          message={message}
        />
      )}
      <main className="page-wrapper">
        <div className="container">
          <div className="bg-light rounded-3 p-4">
            <div className="content-overlay">
              {userInfo ? (
                <>
                  <div className="mail-box">
                    <aside className="sm-side">
                      <div className="user-head">
                        <a className="inbox-avatar" href="javascript:;">
                          <img width="64" hieght="60" src={userInfo.picture} />
                        </a>
                        <div className="user-name">
                          <h5>
                            <a href="#">{userInfo.name}</a>
                          </h5>
                          <span>
                            <a href="#">{userInfo.email}</a>
                          </span>
                        </div>
                      </div>
                      <div className="inbox-body">
                        <button
                          type="button"
                          className="btn btn-primary"
                          data-toggle="modal"
                          data-target="#addCompose"
                          onClick={handleCompose}
                        >
                          Compose
                        </button>
                      </div>

                      <ul className="inbox-nav inbox-divider">
                        <li
                          className="active"
                          onClick={() => handleEmailClick()}
                        >
                          <a href="#">
                            <i className="fa fa-inbox"></i> Inbox{" "}
                            <span className="label label-danger pull-right">
                              {emails.length}
                            </span>
                          </a>
                        </li>
                        <li onClick={() => handleSentEmail()}>
                          <a href="#">
                            <i className="fa fa-envelope-o"></i> Sent Mail
                          </a>
                        </li>
                        {/* <li>
                            <a href="#">
                              <i className="fa fa-bookmark-o"></i> Important
                            </a>
                          </li> */}
                        {/* <li>
                            <a href="#">
                              <i className=" fa fa-external-link"></i> Drafts{" "}
                              <span className="label label-info pull-right">
                                30
                              </span>
                            </a>
                          </li>
                          <li>
                            <a href="#">
                              <i className=" fa fa-trash-o"></i> Trash
                            </a>
                          </li> */}
                      </ul>
                    </aside>
                    <aside className="lg-side">
                      <div className="inbox-body">
                        {singleEmail ? (
                          <>
                            <div>
                              <i
                                style={{
                                  cursor: "pointer",
                                  color: "black",
                                  fontSize: "20px",
                                }}
                                title="Back to All Mail"
                                className="fa fa-arrow-left"
                                aria-hidden="true"
                                onClick={handleBackMail}
                              ></i>
                            </div>
                            <div className="mt-3">
                              <h4>
                                {
                                  singleEmail.payload.headers.find(
                                    (header) => header.name === "Subject"
                                  ).value
                                }
                              </h4>
                              <p>
                                <strong>
                                  {
                                    singleEmail.payload.headers.find(
                                      (header) => header.name === "From"
                                    ).value
                                  }
                                </strong>
                                <br />

                                <div className="btn-group">
                                  to me
                                  <button
                                    type="button"
                                    className="dropdown-toggle ms-2"
                                    data-bs-toggle="dropdown"
                                    aria-haspopup="true"
                                    aria-expanded="false"
                                  ></button>
                                  <div className="dropdown-menu dropdown-menu-end my-1">
                                    {singleEmail.payload.headers.find(
                                      (header) => header.name === "From"
                                    ) && (
                                      <span className="dropdown-item">
                                        from:{" "}
                                        <strong>
                                          {
                                            singleEmail.payload.headers.find(
                                              (header) => header.name === "From"
                                            ).value
                                          }
                                        </strong>
                                      </span>
                                    )}

                                    {singleEmail.payload.headers.find(
                                      (header) => header.name === "Reply-To"
                                    ) && (
                                      <span className="dropdown-item">
                                        reply-to:{" "}
                                        <strong>
                                          {
                                            singleEmail.payload.headers.find(
                                              (header) =>
                                                header.name === "Reply-To"
                                            )?.value
                                          }
                                        </strong>
                                      </span>
                                    )}

                                    {singleEmail.payload.headers.find(
                                      (header) => header.name === "To"
                                    ) && (
                                      <span className="dropdown-item">
                                        to:{" "}
                                        <strong>
                                          {
                                            singleEmail.payload.headers.find(
                                              (header) => header.name === "To"
                                            ).value
                                          }
                                        </strong>
                                      </span>
                                    )}

                                    {singleEmail.payload.headers.find(
                                      (header) => header.name === "Date"
                                    ) && (
                                      <span className="dropdown-item">
                                        date:{" "}
                                        <strong>
                                          {
                                            singleEmail.payload.headers.find(
                                              (header) => header.name === "Date"
                                            ).value
                                          }
                                        </strong>
                                      </span>
                                    )}

                                    {singleEmail.payload.headers.find(
                                      (header) => header.name === "Subject"
                                    ) && (
                                      <span className="dropdown-item">
                                        Subject:{" "}
                                        <strong>
                                          {
                                            singleEmail.payload.headers.find(
                                              (header) =>
                                                header.name === "Subject"
                                            ).value
                                          }
                                        </strong>
                                      </span>
                                    )}
                                  </div>
                                </div>
                              </p>

                              {isAttachmentPresent(singleEmail?.payload) ? (
                                attachmentData.map((attachment, index) => {
                                  const base64Data = `data:${attachment.mimeType};base64,${attachment.data}`;
                                  const fileName = `attachment-${index}.${
                                    attachment.mimeType.split("/")[1]
                                  }`;

                                  return (
                                    <div key={index}>
                                      <a
                                        className="btn btn-primary mb-2"
                                        href={base64Data}
                                        download={fileName}
                                      >
                                        Download {attachment.filename}
                                      </a>
                                    </div>
                                  );
                                })
                              ) : (
                                <div
                                  dangerouslySetInnerHTML={{
                                    __html: singleEmail?.payload
                                      ? getBody(singleEmail.payload)
                                      : "",
                                  }}
                                />
                              )}
                            </div>
                          </>
                        ) : (
                          <table className="table table-inbox table-hover">
                            <tbody>
                              {inbox.map((email, index) => (
                                <tr
                                  className="unread"
                                  key={index}
                                  onClick={() =>
                                    handleSingleGmailData(email.id)
                                  }
                                >
                                  <td className="inbox-small-cells">
                                    <input
                                      type="checkbox"
                                      className="mail-checkbox"
                                    />
                                  </td>
                                  <td className="inbox-small-cells">
                                    <i className="fa fa-star"></i>
                                  </td>

                                  {email.payload.headers.map((header) =>
                                    header.name === "From" && header.value ? (
                                      <td
                                        className="view-message"
                                        key={header.name}
                                      >
                                        {header.value}
                                      </td>
                                    ) : (
                                      ""
                                    )
                                  )}

                                  {email.payload.headers.map((header) =>
                                    header.name === "Subject" &&
                                    header.value ? (
                                      <td
                                        className="view-message"
                                        key={header.name}
                                      >
                                        {header.value}
                                      </td>
                                    ) : (
                                      ""
                                    )
                                  )}
                                </tr>
                              ))}
                            </tbody>
                          </table>
                        )}

                        {singleSent ? (
                          <>
                            <div>
                              <i
                                style={{
                                  cursor: "pointer",
                                  color: "black",
                                  fontSize: "20px",
                                }}
                                title="Back to All Mail"
                                className="fa fa-arrow-left"
                                aria-hidden="true"
                                onClick={handleBackSent}
                              ></i>
                            </div>
                            <div className="mt-3">
                              <h4>
                                {
                                  singleSent.payload.headers.find(
                                    (header) => header.name === "Subject"
                                  ).value
                                }
                              </h4>
                              <p>
                                <strong>
                                  {
                                    singleSent.payload.headers.find(
                                      (header) => header.name === "From"
                                    ).value
                                  }
                                </strong>
                                <br />

                                <div className="btn-group">
                                    to me
                                    <button
                                      type="button"
                                      className="dropdown-toggle ms-2"
                                      data-bs-toggle="dropdown"
                                      aria-haspopup="true"
                                      aria-expanded="false"
                                    ></button>
                                    <div className="dropdown-menu dropdown-menu-end my-1">
                                      {singleSent.payload.headers.find(
                                        (header) => header.name === "From"
                                      ) && (
                                        <span className="dropdown-item">
                                          from:{" "}
                                          <strong>
                                            {
                                              singleSent.payload.headers.find(
                                                (header) =>
                                                  header.name === "From"
                                              ).value
                                            }
                                          </strong>
                                        </span>
                                      )}

                                      {singleSent.payload.headers.find(
                                        (header) => header.name === "Reply-To"
                                      ) && (
                                        <span className="dropdown-item">
                                          reply-to:{" "}
                                          <strong>
                                            {
                                              singleSent.payload.headers.find(
                                                (header) =>
                                                  header.name === "Reply-To"
                                              )?.value
                                            }
                                          </strong>
                                        </span>
                                      )}

                                      {singleSent.payload.headers.find(
                                        (header) => header.name === "To"
                                      ) && (
                                        <span className="dropdown-item">
                                          to:{" "}
                                          <strong>
                                            {
                                              singleSent.payload.headers.find(
                                                (header) => header.name === "To"
                                              ).value
                                            }
                                          </strong>
                                        </span>
                                      )}

                                      {singleSent.payload.headers.find(
                                        (header) => header.name === "Cc"
                                      ) && (
                                        <span className="dropdown-item">
                                          cc:{" "}
                                          <strong>
                                            {
                                              singleSent.payload.headers.find(
                                                (header) => header.name === "Cc"
                                              ).value
                                            }
                                          </strong>
                                        </span>
                                      )}

                                      {singleSent.payload.headers.find(
                                        (header) => header.name === "Date"
                                      ) && (
                                        <span className="dropdown-item">
                                          date:{" "}
                                          <strong>
                                            {
                                              singleSent.payload.headers.find(
                                                (header) =>
                                                  header.name === "Date"
                                              ).value
                                            }
                                          </strong>
                                        </span>
                                      )}

                                      {singleSent.payload.headers.find(
                                        (header) => header.name === "Subject"
                                      ) && (
                                        <span className="dropdown-item">
                                          Subject:{" "}
                                          <strong>
                                            {
                                              singleSent.payload.headers.find(
                                                (header) =>
                                                  header.name === "Subject"
                                              ).value
                                            }
                                          </strong>
                                        </span>
                                      )}
                                    </div>
                                  </div>

                              </p>

                              {isAttachmentPresent(singleSent?.payload) ? (
                                attachmentData.map((attachment, index) => {
                                  const base64Data = `data:${attachment.mimeType};base64,${attachment.data}`;
                                  const fileName = `attachment-${index}.${
                                    attachment.mimeType.split("/")[1]
                                  }`;

                                  return (
                                    <div key={index}>
                                      {/* {attachment.mimeType ===
              "application/pdf" ? (
                <iframe
                  src={base64Data}
                  type="application/pdf"
                  width="100%"
                  height="500px"
                  onError={(e) =>
                    console.error(
                      "PDF rendering error:",
                      e
                    )
                  }
                />
              ) : (
                <img
                  src={base64Data}
                  alt={`attachment-${index}`}
                  width="100%"
                  onError={(e) =>
                    console.error(
                      "Image rendering error:",
                      e
                    )
                  }
                />
              )} */}
                                      <a
                                        className="btn btn-primary mb-2"
                                        href={base64Data}
                                        download={fileName}
                                      >
                                        Download {attachment.filename}
                                      </a>
                                    </div>
                                  );
                                })
                              ) : (
                                <div
                                  dangerouslySetInnerHTML={{
                                    __html: singleSent?.payload
                                      ? getBody(singleSent.payload)
                                      : "",
                                  }}
                                />
                              )}
                            </div>
                          </>
                        ) : (
                          <table className="table table-inbox table-hover">
                            <tbody>
                              {sentEmails.map((email, index) => (
                                <tr
                                  className="unread"
                                  key={index}
                                  onClick={() =>
                                    handleSingleGmailSent(email.id)
                                  }
                                >
                                  <td className="inbox-small-cells">
                                    <input
                                      type="checkbox"
                                      className="mail-checkbox"
                                    />
                                  </td>
                                  <td className="inbox-small-cells">
                                    <i className="fa fa-star"></i>
                                  </td>

                                  {email.payload.headers.map((header) =>
                                    header.name === "From" && header.value ? (
                                      <td
                                        className="view-message"
                                        key={header.name}
                                      >
                                        {header.value}
                                      </td>
                                    ) : (
                                      ""
                                    )
                                  )}

                                  {email.payload.headers.map((header) =>
                                    header.name === "Subject" &&
                                    header.value ? (
                                      <td
                                        className="view-message"
                                        key={header.name}
                                      >
                                        {header.value}
                                      </td>
                                    ) : (
                                      ""
                                    )
                                  )}
                                </tr>
                              ))}
                            </tbody>
                          </table>
                        )}
                      </div>
                    </aside>
                  </div>
                </>
              ) : (
                <div className="w-25">
                  <LoginSocialGoogle
                    isOnlyGetToken
                    client_id={CLIENT_ID}
                    ux_mode="popup"
                    scope="https://www.googleapis.com/auth/gmail.readonly https://www.googleapis.com/auth/gmail.send"
                    cookiePolicy={"single_host_origin"}
                    fetch_basic_profile={true}
                    onLoginStart={onLoginStart}
                    onResolve={handleLoginResolve}
                    onReject={handleLoginReject}
                  >
                    <GoogleLoginButton />
                  </LoginSocialGoogle>
                </div>
              )}
            </div>
          </div>

          <div className="modal in" role="dialog" id="addCompose">
            <div
              className="modal-dialog modal-lg modal-dialog-scrollable"
              role="document"
            >
              <div className="modal-content mt-5">
                <div className="modal-header">
                  <h4 className="modal-title">New Message</h4>
                  <button
                    className="btn-close"
                    id="closeModal"
                    type="button"
                    data-dismiss="modal"
                    onClick={handleBack}
                    aria-label="Close"
                  ></button>
                </div>

                <div className="modal-body fs-sm">
                  {accessTokens && (
                    <form role="form" className="form-horizontal">
                      <div className="col-sm-10 mb-3">
                        <label className="form-label">TO</label>
                        <input
                          className="form-control"
                          type="text"
                          name="to"
                          value={formValues.to}
                          onChange={handleInputChange}
                        />
                      </div>

                      <div className="col-sm-10 mb-3">
                        <label className="form-label">Cc / Bcc</label>
                        <input
                          className="form-control"
                          type="text"
                          name="cc"
                          value={formValues.cc}
                          onChange={handleInputChange}
                        />
                      </div>

                      <div className="col-sm-10 mb-3">
                        <label className="form-label">Subject</label>
                        <input
                          className="form-control"
                          type="text"
                          name="subject"
                          value={formValues.subject}
                          onChange={handleInputChange}
                        />
                      </div>
                      <div className="col-sm-10 mb-3">
                        <label className="form-label">Body</label>
                        <Editor
                          editorState={editorState}
                          onEditorStateChange={handleInput}
                          value={formValues.body}
                          toolbar={{
                            options: [
                              "inline",
                              "blockType",
                              "fontSize",
                              "list",
                              "textAlign",
                              "history",
                              "link", // Add link option here
                            ],
                            inline: {
                              options: [
                                "bold",
                                "italic",
                                "underline",
                                "strikethrough",
                              ],
                            },
                            list: { options: ["unordered", "ordered"] },
                            textAlign: {
                              options: ["left", "center", "right"],
                            },
                            history: { options: ["undo", "redo"] },
                            link: {
                              // Configure link options
                              options: ["link", "unlink"],
                            },
                          }}
                          wrapperClassName="demo-wrapper"
                          editorClassName="demo-editor"
                        />
                      </div>

                      <div className="form-group">
                        <div className="col-sm-10 mb-3">
                          <span className="btn btn-secondary fileinput-button">
                            <i className="fa fa-plus fa fa-white"></i>
                            <span>Attachment</span>
                            <input
                              type="file"
                              name="attachments"
                              multiple
                              onChange={handleInputFile}
                            />
                          </span>
                          {formValues ? (
                            <button
                              onClick={handleSubmit}
                              disabled={issubmit}
                              className="btn btn-send"
                              type="submit"
                            >
                              {issubmit ? "Please Wait..." : "Send"}
                            </button>
                          ) : (
                            <button className="btn btn-send" type="submit">
                              Send
                            </button>
                          )}
                        </div>
                      </div>
                    </form>
                  )}
                </div>
              </div>
            </div>
          </div>
        </div>
      </main>
    </div>
  );
};

export default GoogleLogin;

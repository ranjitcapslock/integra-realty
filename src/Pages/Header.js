import React, { useState, useEffect } from "react";
import { useLocation } from "react-router-dom";
import { useNavigate } from "react-router-dom";
import { NavLink, useParams } from "react-router-dom";

import { useLocalStorage } from "../LocalStorageContext";
import image from "../Components/img/logo.png";
import Picture from "../Components/img/pic.png";
import user_service from "../Components/service/user_service";
import jwt from "jwt-decode";
import Loader from "./Loader/Loader.js";
import Toaster from "./Toaster/Toaster.js";
import $, { event } from "jquery";
import axios from "axios";

function Header() {
  const image = "https://brokeragentbase.s3.amazonaws.com/assets/logo.png";
  const { setActive_office_id } = useLocalStorage();

  const [loader, setLoader] = useState({ isActive: null });
  const { isActive } = loader;
  const [toaster, setToaster] = useState({
    types: null,
    isShow: null,
    toasterBody: "",
    message: "",
  });
  const { types, isShow, toasterBody, message } = toaster;

  const [auth, setAuth] = React.useState(null);
  const [isShows, setIsShow] = useState(false);
  const params = useParams();
  const { pathname } = useLocation();
  const navigate = useNavigate();
  const [profile, setProfile] = useState("");
  const [notice, setNotice] = useState("");
  const [organizationGet, setOrganizationGet] = useState([]);
  const [organizationData, setOrganizationData] = useState();
  const [organizationNameUpdate, setOrganizationNameUpdate] = useState("");
  const [sidebartoggle, setSidebartoggle] = useState(false);
  const [isExpired, setIsExpired] = useState("");

  const [quote, setQuote] = useState("");

  const profilepic = (e) => {
    e.target.src = Picture;
  };
  useEffect(() => {
    window.scrollTo(0, 0);
    const localUser = localStorage.getItem("auth");
    if (localUser) {
      setAuth(localUser);
      Quotesget();
      OrganizationTreeGets();
      AgentpapernameAdd();
    }
  }, []);

  const Quotesget = async () => {
    if (jwt(localStorage.getItem("auth")).id) {
      await user_service.Quotesget().then((response) => {
        if (response) {
          setQuote(response.data);
        }
      });
    }
  };

  const OrganizationTreeGets = async () => {
    // setLoader({ isActive: true });
    if (jwt(localStorage.getItem("auth")).id) {
      const agentId = jwt(localStorage.getItem("auth")).id;
      await user_service.organizationTreeGet().then((response) => {
        // setLoader({ isActive: false });
        if (response) {
          setOrganizationGet(response.data);
        }
      });
    }
  };
  const [agentpapernameAdd, setAgentpapernameAdd] = useState("");
  const [selectedPaperworkId, setSelectedPaperworkId] = useState("");
  const [selectedFilee, setSelectedFilee] = useState(null);
  const handlePaperworkIdChange = (event) => {
    const selectedValue = event.target.value;
    setSelectedPaperworkId(selectedValue);
  };
  const AgentpapernameAdd = async () => {
    await user_service.AgentpapernameAdd().then((response) => {
      if (response) {
        setAgentpapernameAdd(response.data);
      }
    });
  };
  const onFileChange = (event) => {
    const selectedFilee = event.target.files[0];
    setSelectedFilee(selectedFilee);
  };
  const [imageData, setImageData] = useState("");
  const [formValues, setFormValues] = useState({
    date_expire: "",
  });

  const handleChanges = (e) => {
    const { name, value } = e.target;
    setFormValues({ ...formValues, [name]: value });
  };

  const handleFileUploadpaperbook = async (e) => {
    if (!selectedFilee) {
      setToaster({
        type: "error",
        isShow: true,
        toasterBody: "Please select a file to upload.",
        message: "Error",
      });
      setTimeout(() => {
        setToaster((prevToaster) => ({ ...prevToaster, isShow: false }));
      }, 500);
      return;
    }

    // Check if the selected file is a PDF
    const fileExtension = selectedFilee.name.split(".").pop();
    if (fileExtension !== "pdf") {
      setToaster({
        type: "error",
        isShow: true,
        toasterBody: "Please upload only PDF files.",
        message: "Error",
      });
      setTimeout(() => {
        setToaster((prevToaster) => ({ ...prevToaster, isShow: false }));
      }, 500);
      document.getElementById("closeModalAttach").click();
      return;
    }

    const formData = new FormData();
    formData.append("file", selectedFilee);

    const config = {
      headers: {
        Accept: "application/json",
        "Content-Type": "multipart/form-data",
        Authorization: `Bearer ${localStorage.getItem("auth")}`,
      },
    };

    try {
      setLoader({ isActive: true });

      const uploadResponse = await axios.post(
        "https://api.brokeragentbase.com/upload",
        // "http://localhost:4000/upload",
        formData,
        config
      );
      const uploadedFileDatapaperwork = uploadResponse.data;

      const userData = {
        agentId: jwt(localStorage.getItem("auth")).id,
        paperwork_id: selectedPaperworkId,
        documenturl: uploadedFileDatapaperwork,
      };

      const additionalActivateFields = {
        date_expire:
          formValues.date_expire ??
          profile?.additionalActivateFields?.date_expire,
      };

      const userDataProfile = {
        ...profile,
        additionalActivateFields: {
          ...profile?.additionalActivateFields, // Preserve existing additionalActivateFields
          ...additionalActivateFields, // Override with new additionalActivateFields if needed
        },
      };


      // Make both API calls simultaneously using Promise.all
      await Promise.all([
        user_service.contactUpdate(profile._id, userDataProfile), // First API call: update contact profile
        user_service.agentpaperworkDocument(userData), // Second API call: upload paperwork
      ])
        .then(([profileResponse, paperworkResponse]) => {
          if (profileResponse && paperworkResponse) {
            document.getElementById("closeModalpaperwork").click();
            setImageData(paperworkResponse.data);
            profileGetAll(); // Assuming this refreshes the data

            setLoader({ isActive: false });
            setSelectedFilee(null);
            setSelectedPaperworkId("");

            setToaster({
              type: "success",
              isShow: true,
              message: "Profile and paperwork updated successfully",
            });
            setTimeout(() => {
              setToaster((prevToaster) => ({ ...prevToaster, isShow: false }));
            }, 3000);
          }
        })
        .catch((error) => {
          console.error("Error in API calls: ", error);
        });
    } catch (error) {
      console.error("Error uploading file: ", error);
    }
  };

  // Helper function to handle errors and reset states
  const handleError = () => {
    setLoader({ isActive: false });
    setToaster({
      type: "error",
      isShow: true,
      message: "Error",
    });
    setTimeout(() => {
      setToaster((prevToaster) => ({ ...prevToaster, isShow: false }));
    }, 3000);
  };

  const local = async (id) => {
    if (id) {
      const userData = {
        is_login: "no",
      };
      setLoader({ isActive: true });
      await user_service.contactUpdate(id, userData).then((response) => {
        if (response) {
          localStorage.removeItem("auth");
          window.location.href = "/signin";
        } else {
          setLoader({ isActive: false });
          setToaster({
            type: "error",
            isShow: true,
            toasterBody: response.data.message,
            message: "Error",
          });

          setTimeout(() => {
            setToaster((prevToaster) => ({
              ...prevToaster,
              isShow: false,
            }));
          }, 3000);
        }
      });
    }
  };

  useEffect(() => {
    window.scrollTo(0, 0);
    if (pathname === "/signin" || pathname === "/signup") {
      setIsShow(false);
    } else {
      setIsShow(true);
    }
  }, [pathname]);

  const logoImage = () => {
    navigate("/");
  };

  const profileGetAll = async () => {
    if (localStorage.getItem("auth")) {
      await user_service
        .profileGet(jwt(localStorage.getItem("auth")).id)
        .then((response) => {
          if (response) {
            // console.log(response);
            setProfile(response.data);
            if (
              response?.data &&
              response?.data?.additionalActivateFields?.date_expire
            ) {
              const expireDate =
                response?.data?.additionalActivateFields?.date_expire;
              const currentDate = new Date();
              const expirationDate = new Date(expireDate);
              const isExpired = expirationDate < currentDate;
              setIsExpired(isExpired);
            }
          }
        });
    }
  };

  const noticeGetAll = async () => {
    if (localStorage.getItem("auth")) {
      await user_service
        .noticegetall(jwt(localStorage.getItem("auth")).id, "no")
        .then((response) => {
          if (response) {
            if (response.data.data) {
              // document.getElementById("noticeopen").querySelector("a").click();
            }
            // console.log(response.data);
            setNotice(response.data.data);
          }
        });
    }
  };

  useEffect(() => {
    window.scrollTo(0, 0);
    profileGetAll();
    noticeGetAll();
  }, []);

  const handleSubmit = async (id, item) => {
    if (id) {
      const userData = {
        active_office: item,
        active_office_id: id,
      };
      setLoader({ isActive: true });
      await user_service
        .contactUpdate(jwt(localStorage.getItem("auth")).id, userData)
        .then((response) => {
          if (response) {
            localStorage.setItem("active_office_id", id);
            localStorage.setItem("active_office", item);
            setActive_office_id({ active_office_id: id });
            setLoader({ isActive: false });
            setToaster({
              type: "Location Changed",
              isShow: true,
              toasterBody: response.data.message,
              message: "Location changed to " + item,
            });
            setTimeout(() => {
              setToaster((prevToaster) => ({ ...prevToaster, isShow: false }));
            }, 2000);
            OrganizationTreeGets();
            profileGetAll();
            document.getElementById("officemodalclose").click();
          } else {
            setLoader({ isActive: false });
            setToaster({
              types: "error",
              isShow: true,
              toasterBody: response.data.message,
              message: "Error",
            });
          }
        });
    }
  };

  const confirmnotice = async (id) => {
    if (id) {
      const userData = {
        isview: "yes",
        viewDate: new Date(),
      };
      setLoader({ isActive: true });
      await user_service.noticeUpdate(id, userData).then((response) => {
        if (response) {
          setLoader({ isActive: false });
          // document.getElementById("noticemodalclose").click();
        } else {
          setLoader({ isActive: false });
          setToaster({
            types: "error",
            isShow: true,
            toasterBody: response.data.message,
            message: "Error",
          });
        }
      });
    }
  };

  const handlesidebar = async (e) => {
    e.preventDefault();
    // this.setState({ sidebartoggle: !this.state.sidebartoggle })
    setSidebartoggle(!sidebartoggle);
    if (sidebartoggle) {
      $(".left_sidebar").removeClass("active");
      $(".right_sidebar").removeClass("active");
    } else {
      $(".left_sidebar").addClass("active");
      $(".right_sidebar").addClass("active");
    }
  };

  const date = profile?.additionalActivateFields?.date_expire
    ? new Date(profile?.additionalActivateFields?.date_expire)
    : "";

  let formattedDate = "";

  if (date) {
    const month = date.getUTCMonth() + 1;
    const day = date.getUTCDate();
    const year = date.getUTCFullYear();
    formattedDate = `${day}/${month}/${year}`;
  }
  const [currentDate, setCurrentDate] = useState(new Date());
  const [diffDays, setDiffDays] = useState(0);
  const [diffHours, setDiffHours] = useState(0);
  const [diffMinutes, setDiffMinutes] = useState(0);
  const [diffSeconds, setDiffSeconds] = useState(0);

  useEffect(() => {
    const intervalId = setInterval(() => {
      setCurrentDate(new Date());
      const expiryDate = new Date(
        profile?.additionalActivateFields?.date_expire
      );
      const diffTime = expiryDate - new Date();
      const calculatedDiffDays = Math.floor(diffTime / (1000 * 60 * 60 * 24));
      const calculatedDiffHours = Math.floor(
        (diffTime % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60)
      );
      const calculatedDiffMinutes = Math.floor(
        (diffTime % (1000 * 60 * 60)) / (1000 * 60)
      );
      const calculatedDiffSeconds = Math.floor((diffTime % (1000 * 60)) / 1000);
      setDiffDays(calculatedDiffDays);
      setDiffHours(calculatedDiffHours);
      setDiffMinutes(calculatedDiffMinutes);
      setDiffSeconds(calculatedDiffSeconds);
    }, 1000); // Update every second

    return () => clearInterval(intervalId);
  }, [profile]);

  //   const endImpersonationSession = () => {
  //     // Swap tokens back: use impersonationToken as auth and clear impersonationToken
  //     const auth = localStorage.getItem("auth");

  //     if (impersonationToken) {
  //         localStorage.setItem("auth", impersonationToken);
  //         localStorage.removeItem("impersonationToken");
  //     }

  //     // Refresh the page or redirect to the original view
  //     window.location.reload();
  // };

  const endImpersonationSession = () => {
    // Swap tokens back: use impersonationToken as auth and clear impersonationToken
    const impersonationToken = localStorage.getItem("impersonationToken");

    if (impersonationToken) {
      localStorage.setItem("auth", impersonationToken);
      localStorage.removeItem("impersonationToken");
    }
    window.location.reload();
  };

  const [platformData, setPlatFormData] = useState("");
  const PlatformdetailsGet = async () => {
    await user_service.platformdetailsGet().then((response) => {
      if (response) {
        setPlatFormData(response.data.data[0]);
      }
    });
  };

  useEffect(() => {
    window.scrollTo(0, 0);
    PlatformdetailsGet();
  }, []);

  return (
    <div>
      <Loader isActive={isActive} />
      {isShow && (
        <Toaster
          types={types}
          isShow={isShow}
          toasterBody={toasterBody}
          message={message}
        />
      )}
      {isShows && (
        <>
          <header
            className="navbar navbar-expand-lg navbar-dark"
            data-scroll-header
          >
            <div className="container">
              {auth ? (
                <>
                  <div className="hamburger_navbar d-flex align-items-center justify-content-center">
                    <spam onClick={handlesidebar}>
                      <i className="fa fa-bars"></i>
                    </spam>

                    <a className="navbar-brand ms-3" onClick={logoImage}>
                      <img
                        className="d-block"
                        src={platformData.platform_logo ?? image}
                        alt="Finder"
                      />
                    </a>
                  </div>
                  <span
                    id="noticeopen"
                    className="float-left w-100 text-center d-none"
                  >
                    <a href="" data-toggle="modal" data-target="#noticemodal">
                      Notice
                    </a>
                  </span>
                </>
              ) : (
                <>
                  <div className="hamburger_navbar d-flex align-items-center justify-content-center">
                    {/* <spam onClick={handlesidebar}><i className="fa fa-bars"></i></spam> */}
                    <a className="navbar-brand ms-3" onClick={logoImage}>
                      <img
                        className="d-block"
                        src={platformData.platform_logo ?? image}
                        alt="Finder"
                      />
                    </a>
                  </div>
                </>
              )}
              {quote ? (
                <div className="quote-container">
                  <span className="text-white">Quote Of The Day:</span>
                  <br />
                  <span className="text-white">{quote}</span>
                </div>
              ) : (
                ""
              )}

              {auth ? (
                <>
                  {localStorage.getItem("impersonationToken") ? (
                    <button
                      className="btn btn-info btn-sm float-left mt-3"
                      onClick={endImpersonationSession}
                    >
                      Impersonating Account- End Session
                    </button>
                  ) : (
                    ""
                  )}

                  <div className="d-flex align-items-center justify-content-center order-lg-3 my-n2 text-center">
                    <div className="user_attention_links">
                      <div className="d-flex align-items-center justify-content-center">
                        <i class="h2 fi-bell opacity-80 mb-0"></i>
                        <i class="h2 fi-chat-circle opacity-80 mb-0"></i>

                        <div className=" ada_new_feature">
                          <div className="dropdown">
                            <i class="h2 fi-plus-square opacity-80 mb-0"></i>
                            <div className="dropdown-menu dropdown-menu-dark dropdown-menu-end">
                              {/* <NavLink
                                to={`contact-profile/${jwt(localStorage.getItem("auth")).id
                                  }`}
                                className=""
                              >
                                <div className="d-flex align-items-start border-bottom border-light px-3 py-1 mb-2">
                                  <img
                                    className="rounded-circle profile_thumbnail"
                                    src={profile.image ?? Picture}
                                    width="48"
                                    alt="profile picture"
                                    onError={(e) => profilepic(e)}
                                  />
                                  <div className="ps-2">
                                    <h6 className="fs-base text-light mb-0">
                                      {profile.name}
                                    </h6>
                                    <div className="fs-xs py-2">
                                      {profile.email}
                                    </div>
                                  </div>
                                </div>
                                <a className="dropdown-item" href="#">
                                  <i className="fi-settings me-2"></i>Profile
                                  Settings
                                </a>
                              </NavLink> */}
                              {localStorage.getItem("auth") &&
                              jwt(localStorage.getItem("auth")).contactType ==
                                "admin" ? (
                                <>
                                  <NavLink
                                    to="/reports"
                                    className="dropdown-item"
                                  >
                                    <i className="fi-file me-2"></i>Reports
                                  </NavLink>
                                </>
                              ) : (
                                ""
                              )}

                              {(localStorage.getItem("auth") &&
                                jwt(localStorage.getItem("auth")).contactType ==
                                  "admin") ||
                              (localStorage.getItem("auth") &&
                                jwt(localStorage.getItem("auth")).contactType ==
                                  "staff") ? (
                                <>
                                  <NavLink
                                    to="/control-panel"
                                    className="dropdown-item"
                                  >
                                    <i className="fi-file me-2"></i>Control
                                    panel
                                  </NavLink>
                                </>
                              ) : (
                                ""
                              )}
                              <a className="dropdown-item" href="#">
                                <i className="fi-file me-2"></i>My Resumes
                              </a>
                              <a className="dropdown-item" href="#">
                                <i className="fi-heart me-2"></i>Saved Jobs
                              </a>
                              <a className="dropdown-item" href="#">
                                <i className="fi-bell me-2"></i>Notifications
                              </a>
                              <div className="dropdown-divider"></div>
                              <a
                                className="dropdown-item"
                                onClick={() => local(profile._id)}
                              >
                                <i className="fi-logout me-2"></i>Sign Out
                              </a>
                            </div>
                          </div>
                        </div>
                      </div>
                      <NavLink
                        to="/add-suggestFeature"
                        className="btn btn-info btn-sm float-left mt-2"
                      >
                        Suggest New Feature
                      </NavLink>
                    </div>

                    <div className="user_attention_links ada_new_feature ms-3">
                      <div className="dropdown">
                        <a className="d-block py-2 pt-0" href="#">
                          <img
                            className="rounded-circle profile_thumbnail mb-0"
                            src={profile.image ?? Picture}
                            alt="profile picture"
                            onError={(e) => profilepic(e)}
                          />
                        </a>
                        <div className="dropdown-menu dropdown-menu-dark dropdown-menu-end">
                          <NavLink
                            to={`contact-profile/${
                              jwt(localStorage.getItem("auth")).id
                            }`}
                            className=""
                          >
                            <div className="d-flex align-items-start border-bottom border-light px-3 py-1 mb-2">
                              <img
                                className="rounded-circle profile_thumbnail"
                                src={profile.image ?? Picture}
                                width="48"
                                alt="profile picture"
                                onError={(e) => profilepic(e)}
                              />
                              <div className="ps-2">
                                <h6 className="fs-base text-light mb-0">
                                  {profile.name}
                                </h6>
                                <div className="fs-xs py-2">
                                  {profile.email}
                                </div>
                              </div>
                            </div>
                            <a className="dropdown-item" href="#">
                              <i className="fi-settings me-2"></i>Profile
                              Settings
                            </a>
                          </NavLink>
                          {localStorage.getItem("auth") &&
                          jwt(localStorage.getItem("auth")).contactType ==
                            "admin" ? (
                            <>
                              <NavLink to="/reports" className="dropdown-item">
                                <i className="fi-file me-2"></i>Reports
                              </NavLink>
                            </>
                          ) : (
                            ""
                          )}

                          {(localStorage.getItem("auth") &&
                            jwt(localStorage.getItem("auth")).contactType ==
                              "admin") ||
                          (localStorage.getItem("auth") &&
                            jwt(localStorage.getItem("auth")).contactType ==
                              "staff") ? (
                            <>
                              <NavLink
                                to="/control-panel"
                                className="dropdown-item"
                              >
                                <i className="fi-file me-2"></i>Control panel
                              </NavLink>
                            </>
                          ) : (
                            ""
                          )}
                          <a className="dropdown-item" href="#">
                            <i className="fi-file me-2"></i>My Resumes
                          </a>
                          <a className="dropdown-item" href="#">
                            <i className="fi-heart me-2"></i>Saved Jobs
                          </a>
                          <a className="dropdown-item" href="#">
                            <i className="fi-bell me-2"></i>Notifications
                          </a>
                          <div className="dropdown-divider"></div>
                          <a
                            className="dropdown-item"
                            onClick={() => local(profile._id)}
                          >
                            <i className="fi-logout me-2"></i>Sign Out
                          </a>
                        </div>
                      </div>

                      <span className="float-left w-100 text-center d-block">
                        {profile.firstName}&nbsp;{profile.lastName}
                      </span>

                      <span className="float-left w-100 text-center d-block my-1">
                        {
                          // localStorage.getItem("auth") && jwt(localStorage.getItem("auth")).contactType == "admin" ?
                          localStorage.getItem("auth") &&
                          (jwt(localStorage.getItem("auth")).contactType ==
                            "associate" ||
                            jwt(localStorage.getItem("auth")).contactType ==
                              "admin") ? (
                            <a
                              href=""
                              data-toggle="modal"
                              data-target="#officemodal"
                            >
                              {profile?.active_office
                                ? profile?.active_office
                                : "Corporate"}
                            </a>
                          ) : (
                            <>
                              {profile?.active_office
                                ? profile?.active_office
                                : "Corporate"}
                            </>
                          )
                        }
                      </span>
                      <span className="float-left w-100 text-center d-block">
                        {profile.additionalActivateFields?.date_expire ? (
                          isExpired ? (
                            <a
                              href=""
                              data-toggle="modal"
                              data-target="#ModalAddpaperworkdocumentlicence"
                            >
                              License Expired
                            </a>
                          ) : (
                            // "License Active"
                              <a className="btn btn-info btn-sm"
                              href=""
                              data-toggle="modal"
                              data-target="#ModalAddpaperworkdocumentlicence"
                            >
                              License Active
                            </a>
                          )
                        ) : (
                          ""
                        )}
                      </span>
                    </div>
                  </div>

                  {/* <a className="btn btn-primary btn-sm rounded-pill ms-2 order-lg-3" href="/add-transaction">
                                        <i className="fi-plus me-2"></i>
                                        New Transaction
                                    </a> */}

                  {/* <div className="collapse navbar-collapse order-lg-2" id="navbarNav">
                                        <ul className="navbar-nav navbar-nav-scroll">
                                            <li className="nav-item dropdown"><NavLink className="nav-link" to="/tasks">Tasks</NavLink></li>
                                            <li className="nav-item dropdown"><a className="nav-link" href="#">Inbox</a></li>
                                            <li className="nav-item dropdown"><NavLink to="/promote" className="nav-link" href="#">Promote</NavLink></li>
                                            <li className="nav-item dropdown"><NavLink to="/learn"  className="nav-link" href="#">Learn</NavLink></li>
                                        </ul>
                                    </div> */}
                </>
              ) : (
                ""
              )}
            </div>
          </header>
          {/* {
                        auth ?
                            <nav className="navbar navbar-expand-lg">
                                <div className="container">
                                    <div className="collapse navbar-collapse" id="navbarSupportedContent">
                                        <ul className="navbar-nav me-auto mb-2 mb-lg-0">
                                            <li className="nav-item" onClick={handleHome}>
                                                <NavLink to="/" className="nav-link active" aria-current="page">Start</NavLink>
                                            </li>
                                            <li className="nav-item" onClick={handleMyTransaction}>
                                                <NavLink to="/transaction" className="nav-link active" aria-current="page">Transactions</NavLink>
                                            </li>
                                            <li className="nav-item" onClick={handleListing}>
                                                <NavLink to="/listing" className="nav-link active" aria-current="page">Listing</NavLink>
                                            </li>
                                            <li className="nav-item" onClick={handleContact}>
                                                <NavLink to="/contact" className="nav-link active" aria-current="page">People</NavLink>
                                            </li>
                                            {
                                                localStorage.getItem("auth") && jwt(localStorage.getItem("auth")).contactType == "admin" ?
                                                    <>
                                                        <li className="nav-item" onClick={addRules}>
                                                            <NavLink to="/add-document-rules" className="nav-link active" aria-current="page">Add Rule</NavLink>
                                                        </li>
                                                        <li className="nav-item" onClick={addDetails}>
                                                            <NavLink to="/t-details" className="nav-link active" aria-current="page">Add Details Question</NavLink>
                                                        </li>
                                                        <li className="nav-item" onClick={addLearn}>
                                                            <NavLink to="/add-learn" className="nav-link active" aria-current="page">Add Learn Media</NavLink>
                                                        </li>
                                                    </>
                                                    : ""
                                            }

                                        </ul>

                                    </div>
                                </div>
                            </nav>
                            : ""
                    } */}
        </>
      )}

      <div className="modal in" role="dialog" id="officemodal">
        <div
          className="modal-dialog modal-dialog-centered modal-dialog-scrollable"
          role="document"
        >
          <div className="modal-content">
            <div className="modal-header">
              <h4 className="modal-title">Select an Office</h4>
              <button
                className="btn-close"
                id="officemodalclose"
                type="button"
                data-dismiss="modal"
                aria-label="Close"
              ></button>{" "}
            </div>
            <div className="modal-body fs-sm">
              <div className="">
                <div className="">
                  {organizationGet.data
                    ? organizationGet.data.map((item) => (
                        // console.log(item);
                        <div key={item._id}>
                          <div className="py-3 d-flex">
                            <NavLink className="">
                              <p
                                className="mb-0"
                                onClick={() =>
                                  handleSubmit(item._id, item.name)
                                }
                              >
                                <span className="pull-left">{item.tag}</span>
                                &nbsp;{item.name}
                              </p>
                            </NavLink>
                          </div>
                          <hr className="" />
                        </div>
                      ))
                    : ""}
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div className="modal" role="dialog" id="noticemodal">
        <div className="modal-dialog" role="document">
          <div className="modal-content">
            {notice && notice.length > 0
              ? notice.map(
                  (item) =>
                    item.isview === "no" && (
                      <>
                        <div className="modal-header">
                          <h5>{item.category}</h5>
                          <button
                            type="button"
                            className="btn btn-default btn btn-secondary btn-sm"
                            id="noticemodalclose"
                            data-dismiss="modal"
                          >
                            CLOSE
                          </button>
                        </div>
                        <div className="modal-body row">
                          <div className=" col-md-12">
                            <div className="row">
                              <div className="col-md-6 d-flex">
                                <h6>
                                  {" "}
                                  <b> {item.title}</b>
                                </h6>
                              </div>
                            </div>
                            <div className="row">
                              <div
                                className="col-md-6 d-flex"
                                dangerouslySetInnerHTML={{
                                  __html: item.message,
                                }}
                              />
                            </div>
                            <div className="row">
                              <div className="col-md-6 d-flex">
                                <button
                                  type="button"
                                  className="btn btn-default btn btn-secondary btn-sm"
                                  onClick={(e) => confirmnotice(item._id)}
                                >
                                  Confirm & Close
                                </button>
                              </div>
                            </div>
                          </div>
                        </div>
                      </>
                    )
                )
              : ""}
          </div>
        </div>
      </div>

      <div
        className="modal IN"
        role="dialog"
        id="ModalAddpaperworkdocumentlicence"
      >
        <div
          className="modal-dialog modal-md modal-dialog-scrollable"
          role="document"
        >
          <div className="modal-content">
            <div className="modal-header">
              <h4 className="modal-title" id="myModalLabel">
                Upload Paperwork Document
              </h4>
              <button
                className="btn-close"
                id="closeModalAttach"
                type="button"
                data-dismiss="modal"
                aria-label="Close"
              ></button>
            </div>

            <div className="modal-body fs-sm">
              <div className="col-md-12">
                <form>
                  <h6>Upload Paperwork</h6>
                  <div className="mb-3">
                    <div>
                      <p>
                        <b>Expire Date: {formattedDate}</b>
                      </p>
                      {diffDays > 0 ||
                      diffHours > 0 ||
                      diffMinutes > 0 ||
                      diffSeconds > 0 ? (
                        <p>
                          <b>
                            License Expiring in {diffDays} days {diffHours}{" "}
                            hours {diffMinutes} minutes {diffSeconds} seconds
                          </b>
                        </p>
                      ) : (
                        <p>
                          <b>License Expired Please Upload Licence</b>
                        </p>
                      )}
                    </div>
                    <div className="mb-3">
                      <label className="form-label">Licence Date</label>
                      <input
                        className="form-control"
                        type="date"
                        id="inline-form-input"
                        placeholder="Choose date"
                        name="date_expire"
                        onChange={handleChanges}
                        value={formValues.date_expire? formValues.date_expire : profile?.additionalActivateFields?.date_expire}
                      />
                    </div>

                    <label className="form-label">
                      Select Licence Document
                    </label>
                    <br />
                    <select
                      name="paperwork_id"
                      className="form-select"
                      value={selectedPaperworkId} // Set the value of the select to the current state value
                      onChange={handlePaperworkIdChange} // Handle the change event
                    >
                      <option>Select Document </option>
                      {agentpapernameAdd?.data &&
                      agentpapernameAdd.data.length > 0
                        ? agentpapernameAdd.data.map((item) => (
                            <option key={item._id} value={item._id}>
                              {item.paperwork_title}
                            </option>
                          ))
                        : null}
                    </select>
                    {/* <div className="invalid-tooltip">{formErrors.paperwork_id}</div> */}
                  </div>

                  <div>
                    <div className="form-check ps-0">
                      <label className="form-label">
                        Select File (Must be PDF format.)
                      </label>
                      <br />
                      <input
                        id="paperwork_document"
                        type="file"
                        accept="pdf"
                        name="file"
                        onChange={onFileChange}
                        value=""
                      />
                    </div>
                  </div>
                </form>
              </div>
              <div className="modal-footer pb-0 px-0">
                <button
                  type="button"
                  className="btn btn-secondary btn-sm m-0"
                  id="closeModalpaperwork"
                  data-dismiss="modal"
                >
                  Cancel & Close
                </button>
                <button
                  type="button"
                  className="btn btn-info btn-sm m-0 ms-3"
                  id="save-button"
                  disabled={isActive}
                  onClick={handleFileUploadpaperbook}
                >
                  {isActive ? "Please Wait..." : "Upload File Now"}
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
export default Header;

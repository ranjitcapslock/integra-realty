import React, { useState } from 'react';
import Toast from 'react-bootstrap/Toast';
import './Toaster.scss';
function Toaster({ types, isShow, toasterBody, message }) {
  const [show, setShow] = useState(isShow);

  return (
    <div className='toaster'>
      <Toast onClose={() => setShow(false)} delay={2000}  autohide className={`${types}`}>
        <Toast.Header>
          <img src="holder.js/20x20?text=%20" className="rounded me-2" alt="" />
          <p className="me-auto mb-0">{message}<br/>{toasterBody}</p>
          {/* {toasterHeading && <small>{toasterHeading}</small>} */}
        </Toast.Header>
        {/* <Toast.Body></Toast.Body> */}
      </Toast>



      {/* <Button onClick={() => setShow(true)}>Show Toast</Button> */}
      {/* 
      <div>
     <Toast onClose={() => setShow(false)} show={show} delay={3000} autohide className='mb-2 error'>
          <Toast.Header>
            <img
              src="holder.js/20x20?text=%20"
              className="rounded me-2"
              alt=""
            />
            <p className="me-auto">Error</p>
            <small>11 mins ago</small>
          </Toast.Header>
          <Toast.Body>Opsss, you're enter wrong field</Toast.Body>
        </Toast> 
        </div> */}
    </div>
  );
}

export default Toaster;
import React, { useEffect, useState } from "react";
import { useNavigate, useParams } from "react-router-dom";
import user_service from "../Components/service/user_service.js";
import Loader from "./Loader/Loader.js";
import Toaster from "./Toaster/Toaster.js";
import { useLocation } from "react-router-dom";
import Pic from "../Components/img/pic.png";
import jwt from "jwt-decode";
import defaultpropertyimage from "../Components/img/defaultpropertyimage.jpeg";

function AllTab() {
  const [activeButton, setActiveButton] = useState(null);
  const [transactionId, setTransactionId] = useState([]);
  const [getrequireddoucmentno, setGetrequireddoucmentno] = useState([]);
  const [requireddetailsaddeddata, setRequiredDetailsaddeddata] = useState([]);

  const [loader, setLoader] = useState({ isActive: null });
  const { isActive } = loader;
  const location = useLocation();
  const baseUrl = location.pathname.split("/")[1];
  const [toaster, setToaster] = useState({
    types: null,
    isShow: null,
    toasterBody: "",
    message: "",
  });
  const { types, isShow, toasterBody, message } = toaster;

  const navigate = useNavigate();
  const params = useParams();

  const [transactionFundingRequest, setTransactionFundingRequest] =
    useState("");
  const TransactionFundingRequest = async () => {
    await user_service.transactionFundingRequest(params.id).then((response) => {
      if (response) {
        setTransactionFundingRequest(response.data);
        // setLoader({ isActive: false });
      }
    });
  };

  // useEffect(() => {
  //   window.scrollTo(0, 0);
  //   setLoader({ isActive: true })
  //   if (params.id) {
  //     Getrequireddoucmentno();
  //     Detailsaddeddata();
  //     // setLoader({ isActive: true });
  //     TransactionGetById(params.id);
  //     TransactionFundingRequest(params.id);
  //   }
  //   setLoader({ isActive: false })
  // }, []);

  useEffect(() => {
    const fetchData = async () => {
      try {
        window.scrollTo(0, 0);
        setLoader({ isActive: true });
        if (params.id) {
          await Promise.all([
            Getrequireddoucmentno(),
            Detailsaddeddata(),
            TransactionGetById(params.id),
            TransactionFundingRequest(params.id),
          ]);
        }
      } catch (error) {
        console.error("Error in fetching data:", error);
        setLoader({ isActive: false });
      } finally {
        setLoader({ isActive: false });
      }
    };
  
    fetchData();
  }, [params.id]);
  

  const Getrequireddoucmentno = async () => {
    const agentId = jwt(localStorage.getItem("auth")).id;
    await user_service
      .Getrequireddoucmentno(agentId, params.id)
      .then((response) => {
        // setLoader({ isActive: false })
        if (response) {
          setGetrequireddoucmentno(response.data);
        }
      });
  };

  const Detailsaddeddata = async () => {
    const agentId = jwt(localStorage.getItem("auth")).id;
    await user_service.Detailsaddeddata(agentId, params.id).then((response) => {
      if (response) {
        const requiredYesArray = response.data.data.filter(
          (item) => item.required === "yes"
        );
        setRequiredDetailsaddeddata(requiredYesArray);
      }
      // setLoader({ isActive: false });
    });
  };

  const handlegetContact = (stepNo) => {
    if (stepNo == "1") {
      navigate(`/transaction-summary/${params.id}`);
      if (transactionId) {
        setActiveButton(stepNo);
      }
    }
    if (stepNo == "2") {
      navigate(`/transaction-property/${params.id}`);
      setActiveButton(stepNo);
    }

    if (stepNo == "3") {
      navigate(`/t-contact/${params.id}`);
      setActiveButton(stepNo);
    }
    if (stepNo === "4") {
      navigate(`/t-property/${params.id}`);
      setActiveButton(stepNo);
    }
    if (stepNo === "5") {
      navigate(`/document/${params.id}`);
      setActiveButton(stepNo);
    }
    if (stepNo === "6") {
      navigate(`/expenses/${params.id}`);
      setActiveButton(stepNo);
    }
    if (stepNo === "7") {
      navigate(`/commissions/${params.id}`);
      setActiveButton(stepNo);
    }

    if (stepNo === "8") {
      navigate(`/notifications/${params.id}`);
      setActiveButton(stepNo);
    }

    if (stepNo === "9") {
      navigate(`/transaction-details-funding/${params.id}`);
      setActiveButton(stepNo);
    }

    if (stepNo === "14") {
      navigate(`/hire-coordinator/${params.id}`);
      setActiveButton(stepNo);
    }
  };

  const handleNoteHistory = () => {
    navigate(`/transact/detail/history/${params.id}`);
  };

  const handleListingTool = () => {
    navigate(`/transaction/openHouse-listing/${params.id}`);
  };

  const handleReview = () => {
    if (
      localStorage.getItem("auth") &&
      jwt(localStorage.getItem("auth")).contactType == "admin"
    ) {
      navigate(`/transaction-review/${params.id}`);
    }
  };

  const handleFunding = () => {
    navigate(`/transaction-details-funding/${params.id}`);
  };

  const TransactionGetById = async () => {
    setLoader({ isActive: true });
    try {
      const response = await user_service.transactionGetById(params.id);
      
      if (response) {
        setTransactionId(response.data);
        // setLoader({ isActive: false });
        if (
          localStorage.getItem("auth") &&
          (jwt(localStorage.getItem("auth")).contactType == "admin" ||
            jwt(localStorage.getItem("auth")).contactType == "staff" ||
            jwt(localStorage.getItem("auth")).id == response.data.agentId)
        ) {
        } else {
          navigate("/");
        }
      }
    } catch (error) {
      console.error(error);
      setLoader({ isActive: false });
    }
  };

  const propertypic = (e) => {
    e.target.src = defaultpropertyimage;
  };

  const [diffDays, setDiffDays] = useState(0);
  const [diffHours, setDiffHours] = useState(0);
  const [diffMinutes, setDiffMinutes] = useState(0);
  const [diffSeconds, setDiffSeconds] = useState(0);

  useEffect(() => {
    const calculateTimeLeft = () => {
      const expiryDate = new Date(transactionId?.settlement_deadline);
      const diffTime = expiryDate - new Date();

      if (diffTime > 0) {
        const calculatedDiffDays = Math.floor(diffTime / (1000 * 60 * 60 * 24));
        const calculatedDiffHours = Math.floor(
          (diffTime % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60)
        );
        const calculatedDiffMinutes = Math.floor(
          (diffTime % (1000 * 60 * 60)) / (1000 * 60)
        );
        const calculatedDiffSeconds = Math.floor(
          (diffTime % (1000 * 60)) / 1000
        );

        setDiffDays(calculatedDiffDays);
        setDiffHours(calculatedDiffHours);
        setDiffMinutes(calculatedDiffMinutes);
        setDiffSeconds(calculatedDiffSeconds);
      } else {
        // Countdown has reached zero or passed deadline
        setDiffDays(0);
        setDiffHours(0);
        setDiffMinutes(0);
        setDiffSeconds(0);
      }
    };

    const intervalId = setInterval(calculateTimeLeft, 1000); // Update every second

    return () => clearInterval(intervalId);
  }, [transactionId]);

  return (
    <aside className="col-lg-12 col-md-12 mb-4 mt-xl-5 mt-md-5 mt-sm-5 mt-5">
      <Loader isActive={isActive} />
      {isShow && (
        <Toaster
          types={types}
          isShow={isShow}
          toasterBody={toasterBody}
          message={message}
        />
      )}
      <div className="multitab-action transaction_bar">
        <div className="product_transaction_summary float-start w-100 bg-light p-3 rounded-3 d-lg-flex d-md-flex d-md-flex d-block align-items-center justify-content-between align-items-center">
          <div className="float-start d-lg-flex d-md-flex d-sm-flex d-block w-100">
            <img
              className="getContact_picture"
              src={
                transactionId?.propertyDetail?.image &&
                transactionId?.propertyDetail?.image !== "image"
                  ? transactionId?.propertyDetail?.image
                  : defaultpropertyimage
              }
              alt="Property"
              onError={(e) => propertypic(e)}
            />
            <div className="ms-lg-3 ms-md-3 ms-sm-3 ms-0">
              <h5 className="mb-1">
                {transactionId.propertyDetail ? (
                  <>
                    <b>Address:</b>{" "}
                    {transactionId.propertyDetail.streetAddress
                      ? transactionId.propertyDetail.streetAddress + ","
                      : ""}
                    <br />
                    {transactionId.propertyDetail.city
                      ? transactionId.propertyDetail.city + ", "
                      : ""}{" "}
                    {transactionId.propertyDetail.state + ", "
                      ? transactionId.propertyDetail.state
                      : ""}{" "}
                    {transactionId.propertyDetail.zipCode
                      ? transactionId.propertyDetail.zipCode + ", "
                      : ""}
                  </>
                ) : (
                  ""
                )}
              </h5>

              <p className="my-1">
                {transactionId?.phase ? (
                  <span className="badge bg-success">
                    {transactionId?.phase === "pre-listed"
                      ? "Pre-Listed"
                      : transactionId?.phase === "active-listing"
                      ? "Active Listing"
                      : transactionId?.phase === "showing-homes"
                      ? "Showing Homes"
                      : transactionId?.phase === "under-contract"
                      ? "Under Contract"
                      : transactionId?.phase === "closed"
                      ? "Closed"
                      : transactionId?.phase === "canceled"
                      ? "Canceled"
                      : ""}
                  </span>
                ) : (
                  "N/A"
                )}
              </p>

              {/* <p className="my-1">{diffDays > 0 ? diffDays + " DAYS TO CLOSE" :""}</p> */}
              <p className="my-1">
                {diffDays > 0 ||
                diffHours > 0 ||
                diffMinutes > 0 ||
                diffSeconds > 0
                  ? `${diffDays} DAYS ${diffHours} HRS ${diffMinutes} MINS ${diffSeconds} SECS TO CLOSE`
                  : ""}
              </p>
            </div>
            <div className="ms-lg-5 ms-md-5 ms-sm-3 ms-0">
              <h6 className="mb-1">
                <b>You Represent:</b> The {""}
                {transactionId?.represent === "buyer"
                  ? "Buyer"
                  : transactionId?.represent === "seller"
                  ? "Seller"
                  : transactionId.represent === "both"
                  ? "Buyer & Seller"
                  : transactionId.represent === "referral"
                  ? "Referral"
                  : ""}
              </h6>
              <p className="my-0">
                <b>Ref#</b>&nbsp;
                {transactionId?.reference_no ? transactionId?.reference_no : ""}
              </p>
            </div>
          </div>
          {transactionId && (
            <>
              {transactionId.contact3?.[0]?.data?._id ? (
                <div className="transaction_profile_section float-start mb-3 border-0 mt-lg-0 mt-md-0 mt-sm-2 mt-2 text-lg-center text-md-center text-sm-left text-left">
                  <a
                    className="card-nav-link pb-0 border-0"
                    href={`/contact-profile/${
                      transactionId.contact3?.[0]?.data._id ?? ""
                    }`}
                    target="_blank"
                    rel="noopener noreferrer"
                  >
                    {" "}
                    {transactionId.contact3?.[0].data.image ? (
                      <img
                        className="rounded-circle"
                        src={transactionId.contact3?.[0].data.image}
                        alt="Contact Image 2"
                      />
                    ) : (
                      <img
                        className="transacragent"
                        src={Pic}
                        alt="Default Image"
                      />
                    )}
                    <label className="col-form-label ms-2 mb-0">
                      {transactionId.contact3?.[0].data.firstName}&nbsp;
                      {transactionId.contact3?.[0].data.lastName}
                    </label>
                  </a>
                </div>
              ) : (
                <img className="transacragent" src={Pic} alt="Default Image" />
              )}
            </>
          )}
        </div>
        <a
          className="float-start btn btn-outline-secondary bg-light d-block d-md-none w-100 mb-3"
          href="#account-nav"
          data-bs-toggle="collapse"
        >
          <i className="fi-align-justify me-2"></i>Menu
        </a>

        <div
          className="collapse bg-light rounded-3 d-md-block mt-3 float-start w-100 p-3"
          id="account-nav"
        >
          <div className="card-nav">
            <button
              className={`card-nav-link w-auto ${
                activeButton === "1" ? "active" : ""
              }${baseUrl === "transaction-summary" ? "active" : ""}`}
              onClick={() => handlegetContact("1")}
            >
              <i className="fi-home opacity-100 me-2"></i>
              Summary
            </button>

            <button
              className={`card-nav-link w-auto ${
                activeButton === "2" ? "active" : ""
              }${baseUrl === "transaction-property" ? "active" : ""}`}
              onClick={() => handlegetContact("2")}
            >
              <i className="fa fa-briefcase me-2" aria-hidden="true"></i>
              {getrequireddoucmentno.totalpropertydetails &&
              getrequireddoucmentno.totalpropertydetails > 0 ? (
                <span id="" className="badge_require">
                  {getrequireddoucmentno.totalpropertydetails}
                </span>
              ) : (
                ""
              )}
              Property
            </button>

            <button
              className={`card-nav-link w-auto ${
                activeButton === "3" ? "active" : ""
              }${baseUrl === "t-contact" ? "active" : ""}`}
              onClick={() => handlegetContact("3")}
            >
              <i className="fi-phone text-light me-2"></i>
              {getrequireddoucmentno.totalContactNoRequired &&
              getrequireddoucmentno.totalContactNoRequired > 0 ? (
                <span id="" className="badge_require">
                  {getrequireddoucmentno.totalContactNoRequired}
                </span>
              ) : (
                ""
              )}
              Contacts
            </button>

            <button
              className={`card-nav-link w-auto ${
                activeButton === "5" ? "active" : ""
              }${baseUrl === "document" ? "active" : ""}`}
              onClick={() => handlegetContact("5")}
            >
              <i className="fi-file fs-base me-2"></i>
              {getrequireddoucmentno.totalDocumentNoRequired &&
              getrequireddoucmentno.totalDocumentNoRequired > 0 ? (
                <span id="" className="badge_require">
                  {getrequireddoucmentno.totalDocumentNoRequired}
                </span>
              ) : (
                ""
              )}
              Documents
            </button>

            <button
              className={`card-nav-link w-auto ${
                activeButton === "6" ? "active" : ""
              }${baseUrl === "expenses" ? "active" : ""}`}
              onClick={() => handlegetContact("6")}
            >
              <i className="fa fa-usd me-2"></i>Personal Expenses
            </button>

            <button
              className={`card-nav-link w-auto ${
                activeButton === "7" ? "active" : ""
              }${baseUrl === "commissions" ? "active" : ""}`}
              onClick={() => handlegetContact("7")}
            >
              <i className="fa fa-industry me-2" aria-hidden="true"></i>
              {getrequireddoucmentno.totalComissionDetails &&
              getrequireddoucmentno.totalComissionDetails > 0 ? (
                <span id="" className="badge_require">
                  {getrequireddoucmentno.totalComissionDetails}
                </span>
              ) : (
                ""
              )}
              Commissions/Financial
            </button>

            <button
              className={`card-nav-link w-auto ${
                activeButton === "8" ? "active" : ""
              }${baseUrl === "notifications" ? "active" : ""}`}
              onClick={() => handlegetContact("8")}
            >
              <i className="fa fa-bell me-2" aria-hidden="true"></i>
              Notifications
            </button>

            {localStorage.getItem("auth") &&
              jwt(localStorage.getItem("auth")).contactType === "admin" && (
                <>
                  {transactionId.filing === "filed_complted" ? (
                    <>
                      <button
                        className="card-nav-link w-auto"
                        style={{
                          cursor: "no-drop",
                          // color: "#cdc4c4",
                          // backgroundColor: "#dedef7",
                        }}
                        title="Your transaction was completed"
                      >
                        <i className="fa fa-money me-2" aria-hidden="true"></i>
                        CDA/Funding Request
                      </button>
                    </>
                  ) : (
                    <>
                      {getrequireddoucmentno?.totalDetailsNoRequired == "0" ? (
                        getrequireddoucmentno?.totalContactNoRequired == "0" &&
                        getrequireddoucmentno?.totalpropertydetails == "0" &&
                        getrequireddoucmentno?.totalDocumentNoRequired == "0" &&
                        getrequireddoucmentno?.totalComissionDetails == "0" ? (
                          <button
                            className={`card-nav-link w-auto ${
                              activeButton === "9" ? "active" : ""
                            }${
                              baseUrl === "transaction-details-funding"
                                ? "active"
                                : ""
                            }`}
                            onClick={() => handlegetContact("9")}
                          >
                            <i
                              className="fa fa-money me-2"
                              aria-hidden="true"
                            ></i>
                            CDA/Funding Request
                          </button>
                        ) : (
                          <button
                            className="card-nav-link w-auto"
                            style={{
                              cursor: "no-drop",
                              // color: "#cdc4c4",
                              // backgroundColor: "#dedef7",
                            }}
                            title="To submit your transaction for final review, please ensure all required items and documents are completed. Review each transaction page above—any missing information or documents will be marked with a red circle and a number. Make sure all required fields are filled out before submitting to the office."
                          >
                            <i
                              className="fa fa-money me-2"
                              aria-hidden="true"
                            ></i>
                            CDA/Funding Request
                          </button>
                        )
                      ) : (
                        <button
                          className="card-nav-link w-auto"
                          style={{
                            cursor: "no-drop",
                            // color: "#cdc4c4",
                            // backgroundColor: "#dedef7",
                          }}
                          title="To submit your transaction for final review, please ensure all required items and documents are completed. Review each transaction page above—any missing information or documents will be marked with a red circle and a number. Make sure all required fields are filled out before submitting to the office."
                        >
                          <i
                            className="fa fa-money me-2"
                            aria-hidden="true"
                          ></i>
                          CDA/Funding Request
                        </button>
                      )}
                    </>
                  )}
                </>
              )}

            <button
              className={`card-nav-link w-auto ${
                activeButton === "10" ? "active" : ""
              }${baseUrl === "transact" ? "active" : ""}`}
              onClick={handleNoteHistory}
            >
              <i className="fi-logout opacity-100 me-2"></i>Notes & History
            </button>

            {localStorage.getItem("auth") &&
            jwt(localStorage.getItem("auth")).contactType == "admin" ? (
              <button
                className={`card-nav-link w-auto ${
                  activeButton === "10" ? "active" : ""
                }${baseUrl === "transaction-review" ? "active" : ""}`}
                onClick={handleReview}
              >
                <i className="fa fa-usd me-2"></i>Review Transaction
              </button>
            ) : (
              ""
            )}
          </div>
          <div className="collapse d-md-block mt-4 float-start w-100">
            <div className="card-nav">
              <div className="float-start w-100 text-center d-flex align-itmes-center justify-content-center">
                {transactionFundingRequest?.data?.length > 0 ? (
                  <>
                    {transactionId.filing === "filed_complted" ? (
                      <button
                        className="btn btn-primary w-auto m-0"
                        // style={{
                        //   cursor: "no-drop",
                        //   color: "#ffffff",
                        //   backgroundColor: "#445985",
                        // }}
                      >
                        <span>Filed Transaction</span>
                      </button>
                    ) : (
                      <button
                         className="btn btn-primary w-auto m-2"
                        onClick={handleFunding}
                        // style={{
                        //   cursor: "no-drop",
                        //   backgroundColor: "#dedef7",
                        // }}
                      >
                        <span className="">
                          Transaction Closed- Submit for Final review
                        </span>
                      </button>
                    )}
                  </>
                ) : (
                  <>
                    {getrequireddoucmentno?.totalDetailsNoRequired == "0" &&
                    getrequireddoucmentno?.totalContactNoRequired == "0" &&
                    getrequireddoucmentno?.totalpropertydetails == "0" &&
                    getrequireddoucmentno?.totalDocumentNoRequired == "0" &&
                    getrequireddoucmentno?.totalComissionDetails == "0" ? (
                      transactionId.phase === "canceled" ? (
                        localStorage.getItem("auth") &&
                        jwt(localStorage.getItem("auth")).contactType ===
                          "admin" ? (
                          <button
                            className="btn btn-primary w-auto m-2"
                            onClick={handleFunding}
                            // style={{
                            //   cursor: "no-drop",
                            //   backgroundColor: "#dedef7",
                            // }}
                          >
                            <span className="">Submit Your Transaction</span>
                          </button>
                        ) : (
                          <button
                            className="btn btn-primary w-auto m-2"
                            // style={{
                            //   cursor: "no-drop",
                            //   backgroundColor: "#dedef7",
                            // }}
                          >
                            <span className="">
                              Submit Transaction In Progress
                            </span>
                          </button>
                        )
                      ) : (
                        <button
                          className="btn btn-primary w-auto m-2"
                          href={`/transaction-details-funding/${params.id}`}
                          onClick={handleFunding}
                          // style={{
                          //   cursor: "no-drop",
                          //   backgroundColor: "#dedef7",
                          // }}
                        >
                          <span className="">Submit Your Transaction</span>
                        </button>
                      )
                    ) : (
                      <button
                        className="btn btn-primary w-auto m-2"
                        data-toggle="modal"
                        data-target="#submitInprogress"
                        // style={{
                        //   cursor: "no-drop",
                        //   color: "#cdc4c4",
                        //   backgroundColor: "#dedef7",
                        // }}
                        title="To submit your transaction for final review, please ensure all required items and documents are completed. Review each transaction page above—any missing information or documents will be marked with a red circle and a number. Make sure all required fields are filled out before submitting to the office."
                      >
                        <span className="">Submit Transaction In Progress</span>
                      </button>
                    )}
                  </>
                )}

                {transactionId?.phase === "canceled" ? (
                  <button
                    className={`btn btn-primary w-auto m-2 ${
                      activeButton === "11" ? "active" : ""
                    }${baseUrl === "cancel" ? "active" : ""}`}
                  >
                    <i className="fa fa-times" aria-hidden="true"></i> Cancelled
                    transaction
                  </button>
                ) : (
                  ""
                )}
              </div>
            </div>
          </div>
        </div>

        <div className="float-start w-100 bg-light rounded-3 mt-4">
          <div
            className="collapse d-md-block mt-3 float-start w-100 p-3"
            id="account-nav"
          >
            <div className="card-nav">
              {transactionId.represent === "seller" &&
              transactionId.propertyDetail?.propertyFrom === "mls" ? (
                  <button className="card-nav-link w-auto mb-3">
                <a
                  // style={{ color: "white" }}
                  href={`/landing-page/${transactionId.propertyDetail?._id}`}
                  target="_blank"
                    className={`card-nav-link w-auto p-0 m-0 ${
                    activeButton === "12" ? "active" : ""
                  }`}
                >
                  <i class="fa fa-paper-plane me-2" aria-hidden="true"></i>
                  Landing Page
                </a>
                  </button>
              ) : (
                ""
              )}

              {transactionId.represent === "seller" ? (
                <button
                  className={`card-nav-link w-auto mb-3 ${
                    activeButton === "12" ? "active" : ""
                  }${baseUrl === "transaction" ? "active" : ""}`}
                  onClick={handleListingTool}
                >
                  <i className="fi-logout opacity-100 me-2"></i>Listing Tools
                </button>
              ) : (
                ""
              )}

              <button
                className={`btn btn-primary w-auto ${
                  activeButton === "14" ? "active" : ""
                }${baseUrl === "hire-coordinator" ? "active" : ""}`}
                onClick={() => handlegetContact("14")}
              >
                {/* <i class="fa fa-thumb-tack me-2" aria-hidden="true"></i> */}
                Hire Transaction Coordinator
              </button>
            </div>
          </div>
        </div>

        <div className="modal in mt-5" role="dialog" id="submitInprogress">
          <div
            className="modal-dialog modal-md modal-dialog-scrollable"
            role="document"
          >
            <div className="modal-content mt-5">
              <div className="modal-header">
                <h4 className="modal-title">Submit Transaction InProgress</h4>
                <button
                  className="btn-close"
                  id="closeModalAttach"
                  type="button"
                  data-dismiss="modal"
                  // onClick={handleBack}
                  aria-label="Close"
                ></button>
              </div>

              <div className="modal-body fs-sm">
                <h6>
                  To submit your transaction for final review, please ensure all
                  required items and documents are completed. Review each
                  transaction page above—any missing information or documents
                  will be marked with a red circle and a number. Make sure all
                  required fields are filled out before submitting to the
                  office.
                </h6>
              </div>
            </div>
          </div>
        </div>
      </div>
    </aside>
  );
}

export default AllTab;

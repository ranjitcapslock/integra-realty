import React, { useState, useEffect } from "react";
import { NavLink } from "react-router-dom";
import user_service from "../Components/service/user_service";
import Loader from "../Pages/Loader/Loader.js";
import Toaster from "../Pages/Toaster/Toaster.js";
import ImageSlider from "react-image-gallery";
import "react-image-gallery/styles/css/image-gallery.css";
import jwt from "jwt-decode";
import Slider from "react-slick";
import "slick-carousel/slick/slick-theme.css";
import "slick-carousel/slick/slick.css";
import Pic from "../Components/img/pic.png";
import Birthday from "../Components/img/birthday.jpg";
import { Calendar as BigCalendar, momentLocalizer } from "react-big-calendar";
import moment from "moment-timezone";
import "react-big-calendar/lib/css/react-big-calendar.css";
import { useNavigate } from "react-router-dom";
const localizer = momentLocalizer(moment);

const settings = {
  dots: true,
  infinite: true,
  speed: 500,
  slidesToShow: 1,
  slidesToScroll: 1,
  autoplay: true,
  autoplaySpeed: 7000,
  arrows: false,
};
const PagesNavigationUI = () => {
  const [calenderGet, setCalenderGet] = useState([]);
  const [loader, setLoader] = useState({ isActive: null });
  const { isActive } = loader;
  const [toaster, setToaster] = useState({
    types: null,
    isShow: null,
    toasterBody: "",
    message: "",
  });
  const { types, isShow, toasterBody, message } = toaster;
  const navigate = useNavigate();
  const [banners, setBanners] = useState("");
  const bannerGetAll = async () => {
    await user_service.bannerGetAll().then((response) => {
      if (response) {
        setLoader({ isActive: false });
        setBanners(response.data.data);
      }
    });
  };

  useEffect(() => { window.scrollTo(0, 0); window.scrollTo(0, 0);
    setLoader({ isActive: true });
    bannerGetAll();
  }, []);

  const [currentDate, setCurrentDate] = useState("");
  useEffect(() => { window.scrollTo(0, 0);
    const interval = setInterval(() => {
      const options = {  dateStyle: "full" };
      setCurrentDate(new Date().toLocaleDateString("en-US", options));
    }, 1000);

    return () => {
      clearInterval(interval);
    };
  }, []);


  const increaseclick = async (banner_id) => {
    await user_service.increaseclickbanner(banner_id).then((response) => {
      if (response) {

      }
    })
  }


  const [upcomingbirthday, setUpcomingbirthday] = useState("");
  const [newassociates, setNewassociates] = useState("");


  const upcomingBirthdayData =
    upcomingbirthday && upcomingbirthday.length > 0 ? (
      upcomingbirthday.map((agent, index) => {
        if (index < 4) {
          return (
            <>
              <div className="float-left w-100 py-2" key={agent._id}>
                <NavLink to={`/contact-profile/${agent._id}`}>
                  <img
                    className="img-fluid"
                    src={agent.image || Pic}
                    alt="Agent Avatar"
                  />
                  <span>
                    <p className="mb-0">
                      <b>
                        {agent.firstName} {agent.lastName}
                      </b>
                    </p>
                    <p className="mb-1">{agent.title}</p>
                    <p className="mb-0 mt-1">
                      {agent.birthday? getFormattedDate(agent.birthday) :""}<br/>
                      {agent.anniversary ?getFormattedDate(agent.anniversary) : ""}
                      {/* Helper function to format date */}
                    </p>
                  </span>
                </NavLink>
              </div>
              <hr className="float-left w-100"></hr>
            </>
          );
        } else {
          return null; // Skip rendering when index is 4 or greater
        }
      })
    ) : (
      <p className="text-center mb-3"><img
      className=""
      src={Birthday}
    /><br/>
    <span className="mt-2">
       No upcoming birthday.
    </span></p>
    );

  const loadMoreLink =
    upcomingbirthday && upcomingbirthday.length > 4 ? (
      <div className="float-left w-100 text-center">
        <NavLink className="mt-3" to="/upcomingbirthday">
          Load More
        </NavLink>
      </div>
    ) : null;

  // Helper function to format date as "Day, Month Day" (e.g., "Friday, September 21")
  function getFormattedDate(dateString) {
    const options = { weekday: "long", month: "long", day: "numeric" };
    const date = new Date(dateString);
    return date.toLocaleDateString("en-US", options);
  }

  const newassociatesData =
    newassociates && newassociates.length > 0 ? (
      newassociates.map((agent, index) => {
        if (index < 5) {
          return (
            <>
              <div className="float-left w-100 py-2" key={agent._id}>
                <NavLink to={`/contact-profile/${agent._id}`}>
                  <img
                    className="img-fluid"
                    src={agent.image || Pic}
                    alt="Agent Avatar"
                  />
                  <span>
                    <p className="mb-0">
                      <b>
                        {agent.firstName} {agent.lastName}
                      </b>
                    </p>
                    {/* <p className="mb-1">{agent.title}</p> */}
                    {
                      agent.timeStamp?
                        <p className="mb-0 mt-1">
                          {getFormattedDate(agent.timeStamp)}
                        </p>
                      :""
                    }
                    <p className="mb-0 mt-1">
                      {agent?.additionalActivateFields
                        ? agent?.additionalActivateFields?.admin_note
                        : "" || ""}
                    </p>
                  </span>
                </NavLink>
              </div>
              <hr className="float-left w-100"></hr>
            </>
          );
        } else {
          return null; // Skip rendering when index is 4 or greater
        }
      })
    ) : (
      <p className="text-center mb-3"><img
        className=""
        src={Pic}
      /><br/>
      <span className="mt-2">
        Not any new associate.
      </span></p>
    );

  const newassociatesloadMoreLink =
    newassociates && newassociates.length > 4 ? (
      <div className="float-left w-100 text-center">
        <NavLink className="mt-3" to="/newassociates">
          Load More
        </NavLink>
      </div>
    ) : null;


    const initialValues = {
      allDayEvent: "yes", category: "", draft: "", endDate: "", guestSeats: "", information: "", publicEvent: "",
      registration: "", startDate: "", title: "",
    }
    
    const [formErrors, setFormErrors] = useState({});
    const [formValues, setFormValues] = useState(initialValues);
    const [selectedDate, setSelectedDate] = useState(null);
    const handleSelectSlot = (slotInfo) => {
      if(localStorage.getItem("auth") && jwt(localStorage.getItem("auth")).contactType == "admin" ){
        setSelectedDate(slotInfo);
        window.$('#modal-show-calender').modal('show');
      }
    };
    const handleEventClick = (event) => {
      event.preventDefault();
      const eventId = event.currentTarget.getAttribute('data-eventid');
      navigate(`/event-detail/${eventId}`);
    };
  {/* <!-- Input onChange Start--> */ }
  const handleChange = (e) => {
    const { name, value } = e.target;
    setFormValues({ ...formValues, [name]: value });
  };

    {/* <!-- Form onSubmit Start--> */ }
    const handleSubmit = async (e, slotInfo) => {
      e.preventDefault();
  
      if (slotInfo) {
        const startDate = moment(slotInfo.start).format("YYYY-MM-DD");
        const endDate = moment(slotInfo.start).format("YYYY-MM-DD");
        
        const userData = {
          office_id: localStorage.getItem("active_office_id"),
          office_name: localStorage.getItem("active_office"),
          category: formValues.category,
          allDayEvent: "yes",
          title: formValues.title,
          information: "information",
          registration: "registration",
          draft: "draft",
          agentId: jwt(localStorage.getItem("auth")).id,
          startDate: startDate,
          endDate: endDate,
          guestSeats: "guestSeats",
          publicEvent: "publicEvent"
        };
  
        try {
          setLoader({ isActive: true });
          const response = await user_service.CalenderPost(userData);
          if (response && response.data) {
            setLoader({ isActive: false });
            setToaster({
              types: "Calender",
              isShow: true,
              toasterBody: response.data.message,
              message: "Event Post Successfully",
            });
            setTimeout(() => {
              setToaster((prevToaster) => ({ ...prevToaster, isShow: false }));
            }, 1000);
  
            fetchCalendar();
            document.getElementById("close").click();
          } else {
            setLoader({ isActive: false });
            setToaster({
              types: "error",
              isShow: true,
              toasterBody: response.data.message,
              message: "Error",
            });
          }
        } catch (error) {
          setLoader({ isActive: false });
          setToaster({
            types: "error",
            isShow: true,
            toasterBody: error.message,
            message: "Error",
          });
        }
        setTimeout(() => {
          setToaster({
            types: "error",
            isShow: false,
            toasterBody: null,
            message: "Error",
          });
        }, 1000);
      }
    };
  
    {/* <!-- Form onSubmit End--> */ }
  
    
    const [search_home, setSearch_home] = useState("");
    const [universaldata, setUniversaldata] = useState([]);
    const search_universal = async () =>{
      console.log(search_home)
     // setIsLoading(true);
       
        await user_service.Searchuiversalfilter(search_home).then((response) => {
            //setIsLoading(false);
            if (response) {
                console.log(response.data)
                setUniversaldata(response.data);
            }
        });
    }
   
  const generateEvents = () => {
    const generatedEvents = [];

    if (calenderGet && calenderGet.data) {
      calenderGet.data.forEach((date) => {
        let startDate;
        let endDate;

        if (date.startDate.includes("/")) {
          startDate = moment.tz(date.startDate, "M/D/YYYY", "America/Denver").toDate();
          endDate = moment.tz(date.endDate, "M/D/YYYY", "America/Denver").toDate();
        } else {
          startDate = moment.tz(date.startDate, "America/Denver").toDate();
          endDate = moment.tz(date.endDate, "America/Denver").toDate();
        }

        const eventId = date._id;

        generatedEvents.push({
          title: date.title,
          start: startDate,
          end: endDate,
          type: "meeting",
          id: eventId,
        });
      });
    }
    return generatedEvents;
  };

   
  const homeside = async () => {
    await user_service.homeside().then((response) => {
      if (response) {
        // console.log(response.data)
        setUpcomingbirthday(response.data.data);
      }
    });
  };


    const homesidenewassociates = async () => {
      await user_service.newassociates().then((response) => {
        if (response) {
          // console.log(response.data);
          setNewassociates(response.data.data);
        }
      });
    };

    const fetchCalendar = async () => {
      setLoader({ isActive: true });
      await user_service.CalenderGet().then((response) => {
        setLoader({ isActive: false });
        if (response) {
          setCalenderGet(response.data);
        }
      });
    };

    useEffect(() => { window.scrollTo(0, 0);
      homeside();
      homesidenewassociates();
      fetchCalendar();
    }, []);
    const [events, setEvents] = useState([]);

    useEffect(() => { window.scrollTo(0, 0);
      const generatedEvents = generateEvents();
      // console.log(generatedEvents);
      setEvents(generatedEvents);
    }, [calenderGet]);
  
    const eventColors = {
      meeting: "#FF5733",
      duty: "#3366FF",
    };
    const eventStyleGetter = (event) => {
      const backgroundColor = eventColors[event.type];
      return {
        style: {
          backgroundColor,
        },
      };
    };
    const CalenderPage = () => {
      navigate("/calendar");
    };

    const handleHome = () => {
      navigate("/");
    };
    const handleMyTransaction = () => {
      navigate("/transaction");
    };
    const handleContact = () => {
      navigate("/contact");
    };
    const handleListing = () => {
      navigate("/listing");
    };
  
    const addRules = () => {
      navigate("/add-document-rules");
    };
    const addDetails = () => {
      navigate("/t-details");
    };
    const addLearn = () => {
      navigate("/add-learn");
    };
  
    const logoImage = () => {
      navigate("/");
    };
  
    const handletasks = () => {
      navigate("/tasks");
    };
  
    const handlestore = () => {
      navigate("/promote");
    };
    const handleDocs = () => {
      navigate("/control-panel/documents");
    };
  
    const handTraining = () => {
      navigate("/learn");
    };
  return (
    <div className="">
      <Loader isActive={isActive} />
      {isShow && (
        <Toaster
          types={types}
          isShow={isShow}
          toasterBody={toasterBody}
          message={message}
        />
      )}
    {/* <div className="container">  
      <main className="page-wrapper homepage_layout"> */}
        <div className="row event-calender">
          <div className="col-md-8">
                {banners && banners.length > 0 && (
                    <div className="featured_images my-3">
                     <Slider {...settings}>
                        {banners && banners.length > 0 && (
                          banners.map((post) => (
                            post.isactive === true && post.marque_image  && post.banner_location === 'promote' ? (
                              <div key={post.title} className="carousel-item">
                                <a onClick={() => increaseclick(post._id)} href={post.url_link} target="_blank" className="sidenav-banner">
                                  <span className="pull-left align-items-center justify-content-left d-flex w-100">
                                    <img
                                      alt=""
                                      // className="w-100"
                                      className="person-img w-100"
                                      src={
                                        post.marque_image
                                          ? post.marque_image.includes('http')
                                            ? post.marque_image
                                            : post.marque_image
                                          : ""
                                      }
                                    ></img>
                                  </span>
                                </a>
                              </div>
                            ) : null
                          ))
                        )}
                      </Slider>
                    </div>
                    ) 
                }
                    <div className="search_bar mt-3">
                          Search
                          {/* <select className="form-control" name="" id="">
                            <option value="">All</option>
                            <option value="">Saab</option>
                            <option value="">Mercedes</option>
                            <option value="">Audi</option>
                          </select> */}
                          <input type="text" name="search_home" className="form-control"
                          valvalue={search_home} 
                          onChange={(event) => setSearch_home(event.target.value)}
                          />
                          <a href="#" onClick={search_universal}><i className="fa fa-search"></i></a>
                                {universaldata == "" || search_home == ""? 
                                    // <p>Loading...</p>
                                    ""
                                  :
                                    <div className="activateaccountstaff_recruiter universal_filter_data shadow-sm">
                                  {
                                  universaldata && universaldata.contact && universaldata.contact.length > 0 ? (
                                    <div>
                                      <h6 className="mb-4">Contact Data</h6>
                                      {universaldata.contact.map((contact) => (
                                        <div className="activateaccountstaff" key={contact._id}>
                                          <NavLink to={`contact-profile/${contact._id}`}>
                                            <ul className="ps-0">
                                                <li className="d-flex justify-content-start align-items-center">
                                                    <div>
                                                        <img className="rounded-circle profile_picture" src={Pic} alt="Profile" /><br />
                                                    </div>
                                                    <div>
                                                        <strong>{contact.firstName} &nbsp; {contact.lastName}</strong><br />
                                                        <strong>{contact.company}</strong><br />
                                                    </div>
                                                    
                                                </li>
                                            </ul>
                                          </NavLink>
                                        </div>
                                      ))}
                                    </div>
                                  ) : null
                                  }
                                  {
                                  universaldata && universaldata.calender && universaldata.calender.length > 0 ? (
                                    <div>
                                      <h6 className="mb-4">Calender</h6>
                                      {universaldata.calender.map((calender) => (
                                        <div className="activateaccountstaff" key={calender._id}>
                                          <NavLink to={`/event-detail/${calender._id}`}>
                                          <ul className="ps-0">
                                                  <li className=" d-flex justify-content-start align-items-center">
                                                      <div>
                                                          <img className="rounded-circle profile_picture" src={Pic} alt="Profile" /><br />
                                                      </div>
                                                      <div>
                                                          <strong>{calender.title}</strong>
                                                      </div>
                                                      
                                                  </li>
                                              </ul>
                                              </NavLink>
                                        </div>
                                      ))}
                                    </div>
                                  ) : null
                                  }

                                  {
                                  universaldata && universaldata.posts && universaldata.posts.length > 0 ? (
                                      <div>
                                        <h6 className="mb-4">Posts</h6>
                                        {universaldata.posts.map((posts) => (
                                          <div className="activateaccountstaff" key={posts._id}>
                                            <NavLink to={`/post-detail/${posts._id}`}>
                                            <ul className="ps-0">
                                                    <li className="d-flex justify-content-start align-items-center">
                                                        <div>
                                                            <img className="rounded-circle profile_picture" src={Pic} alt="Profile" /><br />
                                                        </div>
                                                        <div>
                                                            <strong>{posts.postTitle}</strong>
                                                        </div>
                                                        
                                                    </li>
                                                </ul>
                                                </NavLink>
                                          </div>
                                        ))}
                                      </div>
                                    ) : null
                                  }
                                    </div>
                                }
                    </div>


            <div className="platform_features mt-3">
              <span className="text-center" onClick={handleMyTransaction}>
                <i className="fa fa-calculator" aria-hidden="true"></i>
                Transactions
              </span>
              <span className="text-center">
                {/* <i className="fa fa-usd" aria-hidden="true"></i> */}
                <i className="fa fa-inbox" aria-hidden="true"></i>
                Market Place
              </span>
              <span className="text-center" onClick={handTraining}>
                <i className="fa fa-file-video-o" aria-hidden="true"></i>
                Training
              </span>
              <span className="text-center" onClick={handleDocs}>
                <i className="fa fa-file-text-o"></i>
                Office Docs
              </span>
              <span className="text-center" onClick={handletasks}>
                <i className="fa fa-tasks" aria-hidden="true"></i>
                Tasks
              </span>
              <span className="text-center" onClick={handleContact}>
                <i className="fa fa-handshake-o" aria-hidden="true"></i>
                My CRM
              </span>
              <span className="text-center" onClick={handlestore}>
                <i className="fa fa-shopping-bag" aria-hidden="true"></i>
                Store
              </span>
              <span className="text-center" onClick={handlestore}>
                <i className="fa fa-users" aria-hidden="true"></i>
                Vendors
              </span>
            </div>
          </div>


           <div className="col-md-4">
                <div className="d-flex align-items-center justify-content-between pb-2">
                  <h6 className="mb-0">{currentDate}</h6>
                  <span onClick={CalenderPage}>
                    <i className="fa fa-plus" aria-hidden="true"></i>
                  </span>
                </div>
                <hr />
                <div className="mt-2" style={{ height: "285px" }}>
                <BigCalendar
                    localizer={localizer}
                    events={events}
                    startAccessor="start"
                    endAccessor="end"
                    onSelectSlot={handleSelectSlot}
                    selectable={true}
                    components={{
                      event: ({ event }) => (
                        <div
                          className="rbc-event-content"
                          title={event.title}
                          onClick={handleEventClick}
                          data-eventid={event.id}
                        >
                          {event.title}
                        </div>
                      ),
                    }}
                    views={{
                      month: true,
                      week: false,
                      day: false,
                      agenda: false,
                    }}
                    // components={{
                    //   toolbar: CustomToolbar,
                    // }}
                    eventPropGetter={eventStyleGetter}
                    style={{ flex: 1 }}
                  />
                </div>

                {/* <div className="agent_details mt-3">
                  <p>
                    Birthdays & Work <b>Anniversaries</b>
                  </p>
                  {upcomingBirthdayData}
                  {loadMoreLink}
                </div>
                <div className="agent_details mt-3">
                  <p>
                    Welcome To Our <b>New Associates</b>
                  </p>
                  {newassociatesData}
                  {newassociatesloadMoreLink}
                </div> */}
              </div>

        </div>

        <div className="modal" role="dialog" id="modal-show-calender">
          <div className="modal-dialog" role="document">
            <div className="modal-content">
              <div className="modal-body row">
                <div className="col-md-12 mt-5">
                  <h6>Add an Event</h6>

                  <div className="mb-3">
                    <label className="form-label" for="pr-city">Event<span className='text-danger'>*</span></label>
                    <input className="form-control" id="inline-form-input" type="text" placeholder="Title"
                      name="title"
                      onChange={handleChange}
                      value={formValues.title}
                      style={{
                        border: formErrors?.title ? '1px solid red' : ''
                      }}
                    />
                    <div className="invalid-tooltip">{formErrors.title}</div>
                  </div>

                  <div className="mb-3">
                    <label className="form-label" for="pr-city">Category</label>
                    <select className="form-select" id="pr-city"
                      name="category"
                      onChange={handleChange}
                      value={formValues.category}
                      style={{
                        border: formErrors?.category ? '1px solid red' : ''
                      }}
                    >
                      <option></option>
                      <option>Office</option>
                      <option>Personal</option>
                      <option>Staff</option>
                      <option>Training</option>
                      <option>Community</option>
                      <option>Courses</option>

                    </select>
                    <div className="invalid-tooltip">{formErrors.category}</div>

                  </div>
                  <button type="button" className="btn btn-default btn btn-secondary btn-sm" id="close" data-bs-dismiss="modal">Cancel & Close</button>
                  <button onClick={(e) => handleSubmit(e, selectedDate)} type="button" className="btn btn-primary btn-sm ms-3" id="save-button">Add Event</button>
                </div>

              </div>


            </div>
          </div>
        </div>

      {/* </main>
     
    </div> */}
    </div>
  );
};

export default PagesNavigationUI;

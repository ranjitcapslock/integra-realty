import React, { useEffect, useState } from "react";
import { useNavigate, useParams } from "react-router-dom";
import user_service from "../Components/service/user_service.js";
import Loader from "./Loader/Loader.js";
import Toaster from "./Toaster/Toaster.js";

import Pic from "../Components/img/pic.png";
import { NavLink } from "react-bootstrap";
import jwt from "jwt-decode";
import defaultpropertyimage from "../Components/img/defaultpropertyimage.jpeg";

function StatusBar({ reloadstatusbar }) {
  // const {reloadstatusbar} = props;
  // console.log(reloadstatusbar)
  const [activeButton, setActiveButton] = useState(null);
  const [transactionId, setTransactionId] = useState([]);
  const transactionphase = transactionId?.phase ?? "";
  const transactioncategory = transactionId?.category ?? "";
  const [getrequireddoucmentno, setGetrequireddoucmentno] = useState([]);
  const [requireddetailsaddeddata, setRequiredDetailsaddeddata] = useState([]);

  const [loader, setLoader] = useState({ isActive: null });
  const { isActive } = loader;
  const [toaster, setToaster] = useState({
    types: null,
    isShow: null,
    toasterBody: "",
    message: "",
  });
  const { types, isShow, toasterBody, message } = toaster;

  const navigate = useNavigate();
  const params = useParams();

  const [transactionFundingRequest, setTransactionFundingRequest] =
    useState("");
  const TransactionFundingRequest = async () => {
    await user_service.transactionFundingRequest(params.id).then((response) => {
      if (response) {
        setLoader({ isActive: false });
        setTransactionFundingRequest(response.data);
      }
    });
  };
  useEffect(() => {
    window.scrollTo(0, 0);
    //setLoader({ isActive: true })
    Getrequireddoucmentno();
    Detailsaddeddata();
    if (params.id) {
      setLoader({ isActive: true });
      TransactionGetById(params.id);
      TransactionFundingRequest(params.id);
    }
  }, []);

  useEffect(() => {
    window.scrollTo(0, 0);
    // console.log("dfgdgdg")
    if (reloadstatusbar) {
      setLoader({ isActive: true });
      Getrequireddoucmentno();
      Detailsaddeddata();
      if (params.id) {
        TransactionGetById(params.id);
        TransactionFundingRequest(params.id);
      }
    }
  }, [reloadstatusbar]);

  const Getrequireddoucmentno = async () => {
    const agentId = jwt(localStorage.getItem("auth")).id;
    await user_service
      .Getrequireddoucmentno(agentId, params.id)
      .then((response) => {
        // setLoader({ isActive: false })
        if (response) {
          // console.log(response.data)
          setGetrequireddoucmentno(response.data);
        }
      });
  };

  const Detailsaddeddata = async () => {
    const agentId = jwt(localStorage.getItem("auth")).id;
    await user_service.Detailsaddeddata(agentId, params.id).then((response) => {
      setLoader({ isActive: false });
      if (response) {
        const requiredYesArray = response.data.data.filter(
          (item) => item.is_documentRequired === "yes"
        );
        setRequiredDetailsaddeddata(requiredYesArray);
      }
    });
  };

  const handleReview = () => {
    if (
      localStorage.getItem("auth") &&
      jwt(localStorage.getItem("auth")).contactType == "admin"
    ) {
      navigate(`/transaction-review/${params.id}`);
    }
  };

  const handleFunding = () => {
    navigate(`/transaction-details-funding/${params.id}`);
  };

  const TransactionGetById = async () => {
    try {
      const response = await user_service.transactionGetById(params.id);
      setLoader({ isActive: false });

      if (response) {
        setTransactionId(response.data);
        if (
          localStorage.getItem("auth") &&
          (jwt(localStorage.getItem("auth")).contactType == "admin" || jwt(localStorage.getItem("auth")).contactType == "staff" ||
            jwt(localStorage.getItem("auth")).id == response.data.agentId)
        ) {
        } else {
          navigate("/");
        }
      }
    } catch (error) {
      console.error(error);
      setLoader({ isActive: false });
    }
  };

  const propertypic = (e) => {
    e.target.src = defaultpropertyimage;
  };

  const handleUpdate = async (phase) => {
    try {
      const userData = {
        phase: phase,
      };
      await user_service
        .TransactionUpdate(params.id, userData)
        .then((response) => {
          if (response.status === 200) {
            const userDatan = {
              addedBy: jwt(localStorage.getItem("auth")).id,
              agentId: jwt(localStorage.getItem("auth")).id,
              transactionId: params.id,
              message: "Transaction phase updated to " + phase,
              // transaction_property_address: "ADDRESS 2",
              note_type: "Status",
            };
            const responsen = user_service.transactionNotes(userDatan);

            TransactionGetById(params.id);
            window.location.reload();
            setToaster({
              type: "Add Transaction",
              isShow: true,
              toasterBody: response.data.message,
              message: "Transaction Phase Update Successfully",
            });
            setTimeout(() => {
              setToaster((prevToaster) => ({ ...prevToaster, isShow: false }));
            }, 3000);
          } else {
            setLoader({ isActive: false });
            setToaster({
              types: "error",
              isShow: true,
              toasterBody: response.data.message,
              message: "Error",
            });
          }
        });
    } catch (error) {
      setLoader({ isActive: false });
      setToaster({
        types: "error",
        isShow: true,
        toasterBody: error.response
          ? error.response.data.message
          : error.message,
        message: "Error",
      });
    }
  };

  // console.log(transactionId.filing === "complete");
  return (
    <div className="float-start w-100">
      <Loader isActive={isActive} />
      {isShow && (
        <Toaster
          types={types}
          isShow={isShow}
          toasterBody={toasterBody}
          message={message}
        />
      )}

      <div className="float-start w-100 mb-3">
        <div className="d-lg-flex d-md-flex align-items-center justify-content-between float-start w-100">
          <div className="status_bar float-start w-100">
            {transactionId.filing === "filed_complted" ? (
              <button
                type="button"
                className="btn index_one btn-sm"
                title="Your transaction was completed"
              >
                {transactionphase == "pre-listed" ? (
                  <i className="fi-check text-success me-2"></i>
                ) : (
                  ""
                )}
                Pre-Listed
              </button>
            ) : (
              <button
                onClick={() => handleUpdate("pre-listed")}
                type="button"
                className="btn index_one btn-sm"
              >
                {transactionphase == "pre-listed" ? (
                  <i className="fi-check text-success me-2"></i>
                ) : (
                  ""
                )}
                Pre-Listed
              </button>
            )}

            {transactionId.filing === "filed_complted" ? (
              <button
                type="button"
                className="btn btn-sm"
                title="Your transaction was completed"
              >
                {transactionphase == "active-listing" ? (
                  <i className="fi-check text-success me-2"></i>
                ) : (
                  ""
                )}
                Active Listing
              </button>
            ) : (
              <button
                onClick={() => handleUpdate("active-listing")}
                type="button"
                className="btn btn-sm"
              >
                {transactionphase == "active-listing" ? (
                  <i className="fi-check text-success me-2"></i>
                ) : (
                  ""
                )}
                Active Listing
              </button>
            )}

            {transactionId.filing === "filed_complted" ? (
              <button
                type="button"
                className="btn index_one btn-sm"
                title="Your transaction was completed"
              >
                {transactionphase == "showing-homes" ? (
                  <i className="fi-check text-success me-2"></i>
                ) : (
                  ""
                )}
                Showing Homes
              </button>
            ) : (
              <button
                onClick={() => handleUpdate("showing-homes")}
                type="button"
                className="btn index_one btn-sm"
              >
                {transactionphase == "showing-homes" ? (
                  <i className="fi-check text-success me-2"></i>
                ) : (
                  ""
                )}
                Showing Homes
              </button>
            )}

            {transactionId.filing === "filed_complted" ? (
              <button
                type="button"
                className="btn btn-sm"
                title="Your transaction was completed"
              >
                {transactionphase == "under-contract" ? (
                  <i className="fi-check text-success me-2"></i>
                ) : (
                  ""
                )}
                Under Contract
              </button>
            ) : (
              <button
                onClick={() => handleUpdate("under-contract")}
                type="button"
                className="btn btn-sm"
              >
                {transactionphase == "under-contract" ? (
                  <i className="fi-check text-success me-2"></i>
                ) : (
                  ""
                )}
                Under Contract
              </button>
            )}

            {transactionId.filing === "filed_complted" ? (
              <button
                type="button"
                className="btn btn-sm"
                title="Your transaction was completed"
              >
                {transactionphase == "closed" ? (
                  <i className="fi-check text-success me-2"></i>
                ) : (
                  ""
                )}
                Closed
              </button>
            ) : (
              <button
                onClick={() => handleUpdate("closed")}
                type="button"
                className="btn btn-sm"
              >
                {transactionphase == "closed" ? (
                  <i className="fi-check text-success me-2"></i>
                ) : (
                  ""
                )}
                Closed
              </button>
            )}

            {transactionId.filing === "filed_complted" ? (
              <button
                type="button"
                className="btn index_one btn-sm"
                title="Your transaction was completed"
              >
                {transactionphase == "canceled" ? (
                  <i className="fi-check text-success me-2"></i>
                ) : (
                  ""
                )}
                Cancelled
              </button>
            ) : (
              <button
                onClick={() => handleUpdate("canceled")}
                type="button"
                className="btn index_one btn-sm"
              >
                {transactionphase == "canceled" ? (
                  <i className="fi-check text-success me-2"></i>
                ) : (
                  ""
                )}
                Cancelled
              </button>
            )}
          </div>
        </div>
      </div>
    </div>
  );
}

export default StatusBar;

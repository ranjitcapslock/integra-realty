// import React, { useState } from "react";
// import { useGoogleLogin } from "@react-oauth/google";
// import Loader from "../Pages/Loader/Loader.js";
// import Toaster from "../Pages/Toaster/Toaster.js";

// const MeetingAgent = () => {
//   const [accessToken, setAccessToken] = useState(null);
//   const [userEmail, setUserEmail] = useState(null);
//   const [recipient, setRecipient] = useState("");
//   const [subject, setSubject] = useState("");
//   const [body, setBody] = useState("");

//   const [loader, setLoader] = useState({ isActive: null });
//   const { isActive } = loader;
//   const [toaster, setToaster] = useState({
//     types: null,
//     isShow: null,
//     toasterBody: "",
//     message: "",
//   });
//   const { types, isShow, toasterBody, message } = toaster;

//   // Google Login
//   const login = useGoogleLogin({
//     onSuccess: (response) => {
//       setAccessToken(response.access_token);
//       fetchUserEmail(response.access_token);
//     },
//     onError: () => console.error("Login Failed"),
//     scope: "https://www.googleapis.com/auth/gmail.send",
//   });

//   // Fetch User Email
//   const fetchUserEmail = async (token) => {
//     try {
//       const response = await fetch("https://www.googleapis.com/oauth2/v2/userinfo", {
//         headers: {
//           Authorization: `Bearer ${token}`,
//         },
//       });
//       const data = await response.json();
//       setUserEmail(data.email);
//     } catch (error) {
//       console.error("Error fetching user email:", error);
//     }
//   };

//   // Send Email
//   const sendEmail = async () => {
//     if (!accessToken) {
//       alert("Please login with Google first!");
//       return;
//     }

//     const emailContent = `
//       From: "Agent" <${userEmail}>
//       To: ${recipient}
//       Subject: ${subject}
      
//       ${body}
//     `;

//     // Encode email content for Gmail API
//     const encodedEmail = btoa(emailContent)
//       .replace(/\+/g, "-")
//       .replace(/\//g, "_")
//       .replace(/=+$/, "");

//     try {
//       const response = await fetch(
//         "https://gmail.googleapis.com/gmail/v1/users/me/messages/send",
//         {
//           method: "POST",
//           headers: {
//             Authorization: `Bearer ${accessToken}`,
//             "Content-Type": "application/json",
//           },
//           body: JSON.stringify({
//             raw: encodedEmail,
//           }),
//         }
//       );

//       if (response.ok) {
//         alert("Email sent successfully!");
//       } else {
//         const errorData = await response.json();
//         console.error("Failed to send email:", errorData);
//       }
//     } catch (error) {
//       console.error("Error sending email:", error);
//     }
//   };

//   return (
//     <div className="bg-secondary float-left w-100 pt-4 mb-4">
//       <Loader isActive={isActive} />

//       {isShow && (
//         <Toaster
//           types={types}
//           isShow={isShow}
//           toasterBody={toasterBody}
//           message={message}
//         />
//       )}
//       <main className="page-wrapper">
//         <div className="row">
//           <h1>Google Email Integration</h1>
//           {!accessToken ? (
//             <button onClick={login}>Login with Google</button>
//           ) : (
//             <p>Logged in as: {userEmail}</p>
//           )}

//           {/* Email Form */}
//           <div className="email-form">
//             <h2>Compose Email</h2>
//             <input
//               type="email"
//               placeholder="Recipient"
//               value={recipient}
//               onChange={(e) => setRecipient(e.target.value)}
//             />
//             <input
//               type="text"
//               placeholder="Subject"
//               value={subject}
//               onChange={(e) => setSubject(e.target.value)}
//             />
//             <textarea
//               placeholder="Body"
//               value={body}
//               onChange={(e) => setBody(e.target.value)}
//             />
//             <button onClick={sendEmail}>Send Email</button>
//           </div>

//           {/* Meeting Button */}
//           <button
//             onClick={() =>
//               window.open("https://meet.google.com/", "_blank", "noopener,noreferrer")
//             }
//           >
//             Schedule Meeting
//           </button>
//         </div>
//       </main>
//     </div>
//   );
// };

// export default MeetingAgent;

import React, { useEffect, useState } from 'react';
import { Routes, Route } from 'react-router-dom';
import jwt from 'jwt-decode';
import user_service from './Components/service/user_service';
import axios from 'axios';
import { LocalStorageProvider } from './LocalStorageContext';

// import { GoogleOAuthProvider } from '@react-oauth/google';
import Home from './Components/Post/Home.js';
import Main from './Components/Post/Main.js';

import PostDetail from './Components/Post/PostDetail.js';
import Header from './Pages/Header';

import Sidebar from "./Components/Post/Sidebar";
import Signin from './Components/Signin/signin-light.js';
import Signup from "./Components/Signup/signup-light.js";
import Signupinvite from "./Components/Signin/signupinvite.js";

import ProtectedRoute from './Components/ProtectedRoute/ProtectedRoute';
import EmptyRoute from './Components/ProtectedRoute/EmptyRoute.js';
import NewRouteEmpty from './Components/ProtectedRoute/NewRouteEmpty.js';

// import { HelmetProvider } from 'react-helmet-async';

import ProtectedRouteNew from  './Components/ProtectedRoute/ProtectRouteNew.js';

import Listing from './Components/Listing/Listing.js';
import MarketListing from "./Components/Listing/MarketListing.js"


import ContactListing from './Components/Contact/ContactListing.js';
import ContactTransaction from './Components/Contact/ContactTransaction.js';


import Contact from './Components/Contact/Contact.js';
import AddTransaction from './Components/Transaction-Page/AddTransaction';
import NewPost from './Components/Post/NewPost';
import MyTransaction from './Components/Transaction-Page/MyTransaction';
import TransactionSummary from './Components/Transaction-Page/TransactionSummary';
import Property from './Components/Property/Property';
import TransactionContact from "./Components/Transaction-Page/TransactionContact.js"
import CheckList from './Components/CheckList/CheckList.js';
import Documents from './Components/TransactionDocument/Documents.js';
import Expenses from './Components/Expenses/Expenses';
import NotesHistory from './Components/Note&History/NotesHistory';
import SelectProperty from './Components/Property/SelectProperty';
import ManageCheckList from './Components/CheckList/ManageCheckList';
import AddNote from './Components/Note&History/AddNote';
import AddExpenses from './Components/Expenses/AddExpenses';
import TransactionDate from './Components/Transaction-Page/TransactionDate';
import AddDocuments from './Components/TransactionDocument/AddDocuments';
import AddDocumentRules from './Components/ControlPanel/AddDocumentRules';
import PlatformDocuments from './Components/TransactionDocument/PlatformDocuments';

import PlatformChecklist from './Components/ControlPanel/PlatformChecklist';
import AddChecklistRules from './Components/ControlPanel/AddChecklistRules';
import ControlPanel from './Components/ControlPanel/ControlPanel';
import Banners from './Components/ControlPanel/Banners';

import Profile from './Components/Contact/Profile.js';
import Calendar from  "./Components/Post/Calender.js";
import ClosingCalender from  "./Components/Reports/ClosingCalender.js";


import Activity from './Components/Contact/Activity.js';
import Account from './Components/Contact/Account.js';
import Setting from './Components/Contact/Setting.js';
import MyMLSlisting from './Components/Listing/MyMLSlisting.js';
import SingleListingMLS from './Components/Listing/SingleListingMLS.js';

import AddListing from './Components/Listing/AddListing.js';
import SingleListing from './Components/Listing/SingleListing.js';
import UploadProperty from './Components/Property/UploadProperty.js';
import MlsPropertySet from './Components/Property/MlsPropertySet.js';
import AddEvent from './Components/Post/AddEvent.js';
import EventDetail from './Components/Post/EventDetail.js';
import UpdateEvent from './Components/Post/UpdateEvent.js';
import CalenderSearch from './Components/Post/CalenderSearch.js';
// import AttachDocument from './Components/TransactionDocument/AttachDocument.js';
import ContactProfile from "./Components/Contact/contactProfile.js"
import Reviews from './Components/Contact/Reviews.js';
import AddCheckList from './Components/CheckList/AddCheckList.js';
import Details from './Components/Property/Details.js';
import TransactionReview from './Components/Transaction-Page/TransactionReview.js';
import ActivateAccount from './Components/Contact/ActivateAccount.js';
import TransactionDoc from './Components/TransactionDocument/TransactionDoc.js';
import UpdateDeposit from './Components/Transaction-Page/UpdateDeposit.js';
import DepositAdd from './Components/Transaction-Page/DepositAdd.js';
import UpdateOffice from './Components/Transaction-Page/UpdateOffice.js';
import AccountSetup from "./Components/Signin/AccountSetup"
import AddPaperwork from './Components/Transaction-Page/AddPaperwork.js';
import Paperwork from './Components/Transaction-Page/Paperwork.js';
import OfficeNote from './Components/Contact/OfficeNote.js';
import AccountBalance from './Components/Transaction-Page/AccountBalance.js';
import AddBalance from './Components/Transaction-Page/AddBalance.js';
import Reports from './Components/Reports/Reports.js';
import DepositLedgerReport from './Components/Reports/DepositLedgerReport.js';
import NotesReport from './Components/Reports/NotesReport.js';
import ExpenseLedgerReport from './Components/Reports/ExpenseLedgerReport.js';
import AssocMLS from './Components/Reports/AssocMLSReport.js';
import AssocTasks from './Components/Reports/AssocTasks.js';
import AddAssocTasks from './Components/Reports/AddAssocTasks.js';
import UpcomingBirthday from './Components/Reports/UpcomingBirthday.js';
import NewAssociates from './Components/Reports/NewAssociates.js';
import AssocLicenses from './Components/Reports/AssocLicensesReport.js';
import AssocAdded from './Components/Reports/NewAssociates.js';
import Inapprovalassociates from './Components/Reports/Inapprovalassociates.js';

import TransactFundingReport from './Components/Reports/TransactFundingReport.js';
import AddLearn from './Components/ControlPanel/AddLearn.js';
import LearnMedia from './Components/ControlPanel/LearnMedia.js';
import AddBanner from './Components/ControlPanel/AddBanner.js';
import ProfileInfo from './Components/Contact/ProfileInfo.js';
import ContactInfo from './Components/Contact/ContactInfo.js';
import MemberPlan from './Components/ControlPanel/MemberPlan.js';
import AddMemberPlan from './Components/ControlPanel/AddMemberPlan.js';
import ManageShareDoc from './Components/ControlPanel/ManageShareDoc.js';
import AddSection from './Components/ControlPanel/AddSection.js';
import AddPageContent from './Components/ControlPanel/AddPageContent.js';
import AddAttachDocument from './Components/ControlPanel/AddAttachDocument.js';
import AddLink from './Components/ControlPanel/AddLink.js';
import DocumentContentEdit from './Components/ControlPanel/DocumentContentEdit.js';
import SinglePageContent from './Components/ControlPanel/SinglePageContent.js';
import ColorTheme from './Components/ControlPanel/ColorTheme.js';
import Logo from './Components/ControlPanel/Logo.js';
import UpdatePost from './Components/Post/UpdatePost';
import FundingRequest from './Components/Transaction-Page/FundingRequest';
import Organization from './Components/ControlPanel/Organization';
import AddOrganization from './Components/ControlPanel/AddOrganization';
import CalenderSubscriptions from './Components/Contact/CalenderSubscriptions';
import Subscription from './Components/Contact/Subscription';

import Learn from  "./Components/Learn/Learn.js";
import Promote from "./Components/ControlPanel/Promote.js"
import AdminPromote from "./Components/ControlPanel/AdminPromote.js"

import Addpromotocatalog from "./Components/ControlPanel/Addpromotocatalog.js"
import AdminCatalogitems from "./Components/ControlPanel/AdminCatalogitems.js"
import Invitecontacts from "./Components/ControlPanel/Invitecontacts.js"
import AnnounceMail from "./Components/ControlPanel/AnnounceMail.js"
import Noticemessage from "./Components/ControlPanel/Noticemessage.js"
import ContactNotice from "./Components/Contact/ContactNotice.js"

import FundingConfirmation from './Components/Transaction-Page/FundingConfirmation';
import AddAdminProduct from './Components/ControlPanel/AddAdminProduct';
import AdminProductListing from './Components/ControlPanel/AdminProductListing';
import PromoteProduct from './Components/ControlPanel/PromoteProduct';
import PromoteDetails from './Components/ControlPanel/PromoteDetails';
import PlatformDetails from './Components/Platform/PlatformDetails';
import AddQuestion from './Components/Platform/AddQuestion';
import Question from './Components/Platform/Question';

import AddLinkFiles from './Components/Platform/AddLinkFiles';
import QuickLinks from './Components/Post/QuickLinks';

import Link from './Components/Platform/Link';
import EmailSignature from "./Components/Contact/EmailSignature.js"
import StaffSignature from './Components/Contact/StaffSignature';
import StaffSignatureListing from './Components/Contact/StaffSignatureListing';
import BusinessCard from './Components/ControlPanel/BusinessCard';
// import AddBusinessCard from './Components/ControlPanel/AddBusinessCard';
import AddContact from './Components/Contact/AddContact';
import Vender from './Components/ControlPanel/Vender';
import AddSuggestFeature from './Components/SuggestFeature/AddSuggestFeature';
import SuggestFeature from './Components/SuggestFeature/SuggestFeature';
import NewTransaction from './Components/Transaction-Page/NewTransaction';
import Task from './Components/Reports/Task';
import ThanksPage from './Components/Contact/ThanksPage';
import Onboarding from './Components/Contact/Onboarding';
import Onboardingresubmit from './Components/Contact/Onboardingresubmit';


import OnboardingThankyou from './Components/Contact/OnboardingThankyou';


import TestOnboarding from './Components/Contact/TestOnboarding';
import MlsBoardDocument from './Components/ControlPanel/MlsBoardDocument';
import EditBusinessCard from './Components/ControlPanel/EditBusinessCard';
import PrivateThanks from './Components/Contact/PrivateThanks';
import PrivateContactEdit from './Components/Contact/PrivateContactEdit';
import CommunityContact from './Components/Contact/CommunityContact';
import VideoCategory from './Components/ControlPanel/VideoCategory.js';
import AddVideoCategory from './Components/ControlPanel/AddVideoCategory.js';
import VideoWorkSpace from './Components/ControlPanel/VideoWorkSpace.js';
import WorkspaceSinglePage from './Components/ControlPanel/WorkspaceSinglePage.js';
import VideoSinglePage from './Components/ControlPanel/VideoSinglePage.js';
import NewProperty from './Components/Transaction-Page/NewProperty.js';
// import NewPageProperty from './Components/Transaction-Page/NewPageProperty.js';
import Notification from './Components/Transaction-Page/Notification.js';
import Commissions from './Components/Transaction-Page/Commissions.js';
import TransactionReport from './Components/Reports/TransactionReport.js';
import Testimonials from './Components/Contact/Testimonials.js';
import TestimonialsThanks from './Components/Contact/TestimonialsThanks.js';
import FiledTransaction from './Components/Contact/FiledTransaction.js';
import ContactagentTrans from './Components/Contact/ContactagentTrans.js';
import LandingPage from './Components/Listing/LandingPage.js';
import SinglePageTestimonials from './Components/Contact/SinglePageTestimonials.js';
import FundingIncome from './Components/Reports/FundingIncome.js';
import RemoveAssociate from './Components/Reports/RemoveAssociate.js';
import InactiveAssociate from './Components/Reports/InactiveAssociate.js';
import AssociateHistory from './Components/Reports/AssociateHistory.js';
import SponsorAssociate from './Components/Reports/SponsorAssociate.js';
import AgentTestimonials from './Components/Reports/AgentTestimonials.js';
import FirstTransaction from './Components/Reports/FirstTransaction.js';
import ExpireListing from './Components/Reports/ExpireListing.js';
import DocumentReview from './Components/Reports/DocumentReview.js';
import HomePagePhoto from './Components/ControlPanel/HomePagePhoto.js';
import MarKetCalender from './Components/Post/MarketCalender.js';
import AddStaffRule from './Components/ControlPanel/AddStaffRule.js';
import CloseCalenderCount from './Components/Reports/CloseCalenderCount.js';
import MarketTextPage from './Components/Post/MarketTextPage.js';
import OpenHouse from './Components/Transaction-Page/OpenHouse.js';
import OpenHouseListing from './Components/Transaction-Page/OpenHouseListing.js';
import RealEstateSearch from './Components/Listing/RealEstateSearch.js';
import AddUtilityTool from './Components/ControlPanel/AddUtilityTool.js';
import UtilityTool from './Components/Transaction-Page/UtiltiyTool.js';
import ListingUtility from './Components/Transaction-Page/ListingUtilityTool.js';
import RealStatePrintPage from './Components/Listing/RealStatePrintPage.js';
import BussinessTab from './Components/Post/BussinessTab.js';
import ReportBug from './Components/ControlPanel/ReportBug.js';
import AddContactGroup from './Components/Contact/AddContactGroup.js';
import ContactGroupSingle from './Components/Contact/ContactGroupSingle.js';
import AddGroupMember from './Components/Contact/AddGroupMember.js';
import GroupMemberListing from './Components/Contact/GroupMemberListing.js';
import OnBoardingInfo from './Components/Reports/OnBoardingInfo.js';
import Reservations from './Components/Contact/Reservations.js';
import ReservationListing from './Components/Contact/ReservationListing.js';
import ReservationSinglePage from './Components/Contact/ReservationSinglePage.js';
import ConfigureSchedule from './Components/Contact/ConfigureSchedule.js';
import AddPolicy from './Components/Contact/AddPolicy.js';
import Notifications from './Components/ControlPanel/Notifications.js';
import PrintEditor from './Components/Listing/PrintEditor.js';
import DeailyDigest from './Components/Contact/DeailyDigest.js';
import PastNotice from './Components/Contact/PastNotice.js';
import NewsContent from './Components/ControlPanel/NewsContent.js';
import MarketingContent from './Components/ControlPanel/MarketingContent.js';
import FlyerContent from './Components/ControlPanel/FlyerContent.js';
import HireTransaction from './Components/Transaction-Page/HireTransaction.js';
import NewSubscriptionRoute from './Components/ProtectedRoute/NewSubscriptionRoute.js';
import NewFeatureRealse from './Components/ControlPanel/NewFeatureRealse.js';
import NonListedPrint from './Components/Listing/NonListedPrint.js';
import FeatureListing from './Components/ControlPanel/FeatureListing.js';
import AddVendor from './Components/ControlPanel/AddVendor.js';
import VendorThanks from './Components/Contact/VendorThanks.js';
import VendorOnboarding from './Components/Contact/VendorOnboarding.js';
import MarketingContentListing from './Components/ControlPanel/MarketingContentListing.js';
import AdminSubVendor from './Components/ControlPanel/AdminSubVendor.js';
import AddAdminSubVendor from './Components/ControlPanel/AddAdminSubVendor.js';
import UserSubVendor from './Components/ControlPanel/UserSubVendor.js';
import UpdateAdminProduct from './Components/ControlPanel/UpdateAdminProduct.js';
import CalenderRoster from './Components/Post/CalenderRoster.js';
import BussinessIncome from './Components/Post/BussinessIncome.js';
import BussinessExpences from './Components/Post/BussinessExpences.js';
import GoogleLogin from './Pages/GoogleLogin.js';
import BrochureMls from './Components/Listing/BrochureMls.js';
import PrintingMarkting from './Components/Listing/PrintingMarkting.js';
import PostCards from './Components/Listing/PostCards.js';
import PropertyCards from './Components/Listing/PropertyCard.js';
import AgentsProperty from './Components/Listing/AgentsProperty.js';
import PropertyLebel from './Components/Listing/PropertyLebel.js';
import BuyerAggrement from './Components/Reports/BuyerAggrement.js';
// import MeetingAgent from './Pages/MeetingAgent.js';

function App() {
  const [token, setToken] = useState(localStorage.getItem('auth'));
  const [loader, setLoader] = useState({ isActive: null })
  const { isActive } = loader;
  const [toaster, setToaster] = useState({ types: null, isShow: null, toasterBody: "", message: "" });
  const { types, isShow, toasterBody, message } = toaster;
  const helmetContext = {};

  // const googleClientId = '765365535217-7anjfp0mabknck4mf2tib84b90j4i4p4.apps.googleusercontent.com'; 
  
  const [themecolor, setThemecolor] = useState([])

  useEffect(() => { window.scrollTo(0, 0);
    if (token) {
    // GetThemecolor();
    const decodedToken = jwt(token);
    const currentTime = Date.now() / 1000;

    if (decodedToken.exp < currentTime) {
      setToken(null);
      localStorage.removeItem('auth');
      setTimeout(() => {
        window.location.href = "/signin";
      }, 1000);
    }
  }
}, [token]);

const [formValues, setFormValues] = useState("")

const ThemeColorGet = async () => {
  try {
      const response = await user_service.themecolorsGet();
      if (response && response.data) {
          const formattedData = response.data.data;

          // Make sure formattedData[0] exists before accessing properties
          if (formattedData && formattedData.length > 0) {
              setFormValues(formattedData[0]);
              applyThemeColors(formattedData[0]);
          } else {
              console.error("Formatted data is empty or undefined.");
          }
      }
  } catch (error) {
      console.error("Error fetching theme colors:", error);
  }
};
useEffect(() => {
  window.scrollTo(0, 0);
  ThemeColorGet();
}, []);



const applyThemeColors = (colors) => {
  const root = document.documentElement;
  root.style.setProperty('--branding-main-color', colors.Branding_Main_Color);
  root.style.setProperty('--branding-accent-color', colors.Branding_Accent_Color);
  root.style.setProperty('--branding-link-color', colors.Branding_Link_Color);
  root.style.setProperty('--branding-confirm-color', colors.Branding_Confirm_Color);
  root.style.setProperty('--branding-focus-color', colors.Branding_Focus_Color);
  root.style.setProperty('--branding-info-color', colors.Branding_Info_Color);
  root.style.setProperty('--branding-alert-color', colors.Branding_Alert_Color);
  root.style.setProperty('--branding-back-color', colors.Branding_Back_Color);
  root.style.setProperty('--branding-header-color', colors.Branding_Header_Color);
  root.style.setProperty('--branding-footer-color', colors.Branding_Footer_Color);
  root.style.setProperty('--transact-res-buyer-color', colors.Transact_Res_Buyer_Color);
  root.style.setProperty('--transact-res-seller-color', colors.Transact_Res_Seller_Color);
  root.style.setProperty('--transact-res-tenant-color', colors.Transact_Res_Tenant_Color);
  root.style.setProperty('--transact-res-landlord-color', colors.Transact_Res_Landlord_Color);
  root.style.setProperty('--transact-apartment-color', colors.Transact_Apartment_Color);
  root.style.setProperty('--transact-com-seller-color', colors.Transact_Com_Seller_Color);
  root.style.setProperty('--transact-com-buyer-color', colors.Transact_Com_Buyer_Color);
  root.style.setProperty('--transact-com-tenant-color', colors.Transact_Com_Tenant_Color);
  root.style.setProperty('--transact-com-landlord-color', colors.Transact_Com_Landlord_Color);
  root.style.setProperty('--transact-referral-only-color', colors.Transact_Referral_Only_Color);
  root.style.setProperty('--calendar-personal-color', colors.Calendar_Personal_Color);
  root.style.setProperty('--calendar-transaction-color', colors.Calendar_Transaction_Color);
  root.style.setProperty('--calendar-group-color', colors.Calendar_Group_Color);
  root.style.setProperty('--calendar-office-color', colors.Calendar_Office_Color);
  root.style.setProperty('--calendar-training-color', colors.Calendar_Training_Color);
  root.style.setProperty('--calendar-community-color', colors.Calendar_Community_Color);
  root.style.setProperty('--calendar-birthday-color', colors.Calendar_Birthday_Color);
  root.style.setProperty('--associate-color', colors.Associate_Color);
  root.style.setProperty('--associate-mls-color', colors.Associate_MLS_Color);
  root.style.setProperty('--associate-license-color', colors.Associate_License_Color);
  root.style.setProperty('--associate-staff-color', colors.Associate_Staff_Color);
  root.style.setProperty('--associate-yearly-color', colors.Associate_Yearly_Color);
  root.style.setProperty('--associate-contact-color', colors.Associate_Contact_Color);
  root.style.setProperty('--associate-community-color', colors.Associate_Community_Color);
  root.style.setProperty('--associate-buyer-color', colors.Associate_Buyer_Color);
  root.style.setProperty('--associate-seller-color', colors.Associate_Seller_Color);
  root.style.setProperty('--associate-landlord-color', colors.Associate_LandLord_Color);
  root.style.setProperty('--associate-tenant-color', colors.Associate_Tenant_Color);
  root.style.setProperty('--associate-vendor-color', colors.Associate_Vendor_Color);
  root.style.setProperty('--associate-co-op-color', colors.Associate_Co_Op_Color);
  root.style.setProperty('--associate-team-color', colors.Associate_Team_Color);
};

// const GetThemecolor = async () => {
//   // console.log("tid");
//   // console.log(params.id);

//   var r = document.querySelector(':root');
//   await user_service.themecolorsGet().then((response) => {
//       setLoader({ isActive: false })
//       // console.log(response)
//       // console.log("response")
//       if (response) {
//         const formattedData = response.data.data;
//         if (formattedData && formattedData.length > 0) {
//           setThemecolor(formattedData[0]);
        
//           r.style.setProperty('--Branding_Main_Color', formattedData[0].Branding_Main_Color); 
//           r.style.setProperty('--Branding_Accent_Color', formattedData[0].Branding_Accent_Color); 
//           r.style.setProperty('--Branding_Link_Color', formattedData[0].Branding_Link_Color); 
//           r.style.setProperty('--Branding_Confirm_Color', formattedData[0].Branding_Confirm_Color); 
//           r.style.setProperty('--Branding_Focus_Color', formattedData[0].Branding_Focus_Color); 
//           r.style.setProperty('--Branding_Info_Color', formattedData[0].Branding_Info_Color); 
//           r.style.setProperty('--Branding_Alert_Color', formattedData[0].Branding_Alert_Color); 
//           r.style.setProperty('--Branding_Back_Color', formattedData[0].Branding_Back_Color); 
//           r.style.setProperty('--Branding_Header_Style', formattedData[0].Branding_Header_Style);

//           // Transact
//           r.style.setProperty('--Transact_Res_Buyer_Color', formattedData[0].Transact_Res_Buyer_Color);
//           r.style.setProperty('--Transact_Res_Seller_Color', formattedData[0].Transact_Res_Seller_Color);
//           r.style.setProperty('--Transact_Res_Tenant_Color', formattedData[0].Transact_Res_Tenant_Color);
//           r.style.setProperty('--Transact_Res_Landlord_Color', formattedData[0].Transact_Res_Landlord_Color);
//           r.style.setProperty('--Transact_Apartment_Color', formattedData[0].Transact_Apartment_Color);
//           r.style.setProperty('--Transact_Com_Seller_Color', formattedData[0].Transact_Com_Seller_Color);
//           r.style.setProperty('--Transact_Com_Buyer_Color', formattedData[0].Transact_Com_Buyer_Color);
//           r.style.setProperty('--Transact_Com_Tenant_Color', formattedData[0].Transact_Com_Tenant_Color);
//           r.style.setProperty('--Transact_Com_Landlord_Color', formattedData[0].Transact_Com_Landlord_Color);
//           r.style.setProperty('--Transact_Referral_Only_Color', formattedData[0].Transact_Referral_Only_Color);

//           // Calendar
//           r.style.setProperty('--Calendar_Personal_Color', formattedData[0].Calendar_Personal_Color);
//           r.style.setProperty('--Calendar_Transaction_Color', formattedData[0].Calendar_Transaction_Color);
//           r.style.setProperty('--Calendar_Group_Color', formattedData[0].Calendar_Group_Color);
//           r.style.setProperty('--Calendar_Office_Color', formattedData[0].Calendar_Office_Color);
//           r.style.setProperty('--Calendar_Training_Color', formattedData[0].Calendar_Training_Color);
//           r.style.setProperty('--Calendar_Community_Color', formattedData[0].Calendar_Community_Color);
//           r.style.setProperty('--Calendar_Birthday_Color', formattedData[0].Calendar_Birthday_Color);

//           // Associate
//           r.style.setProperty('--Associate_Color', formattedData[0].Associate_Color);
//           r.style.setProperty('--Associate_MLS_Color', formattedData[0].Associate_MLS_Color);
//           r.style.setProperty('--Associate_License_Color', formattedData[0].Associate_License_Color);
//           r.style.setProperty('--Associate_Staff_Color', formattedData[0].Associate_Staff_Color);
//           r.style.setProperty('--Associate_Yearly_Color', formattedData[0].Associate_Yearly_Color);
//           r.style.setProperty('--Associate_Contact_Color', formattedData[0].Associate_Contact_Color);
//           r.style.setProperty('--Associate_Community_Color', formattedData[0].Associate_Community_Color);
//           r.style.setProperty('--Associate_Buyer_Color', formattedData[0].Associate_Buyer_Color);
//           r.style.setProperty('--Associate_Seller_Color', formattedData[0].Associate_Seller_Color);
//           r.style.setProperty('--Associate_LandLord_Color', formattedData[0].Associate_LandLord_Color);
//           r.style.setProperty('--Associate_Tenant_Color', formattedData[0].Associate_Tenant_Color);
//           r.style.setProperty('--Associate_Vendor_Color', formattedData[0].Associate_Vendor_Color);
//           r.style.setProperty('--Associate_Co_Op_Color', formattedData[0].Associate_Co_Op_Color);
//           r.style.setProperty('--Associate_Team_Color', formattedData[0].Associate_Team_Color);
//       }
//     }
//   });
// }

  return (
    <div className="App">
       <Routes>
        <Route path="/calendersubscriptions/:id/:type" element={ <Subscription/> } />
      </Routes>
      <LocalStorageProvider> 
      {/* <HelmetProvider context={helmetContext}> */}
      <Header />
      <Routes>
        <Route path="/old" element={<ProtectedRoute Component={<Home />} />}/>
        <Route path="/" element={<ProtectedRoute Component={<Main />} />} />
        <Route path="/post-detail/:id" element={<ProtectedRoute Component={<PostDetail />} />} />
        <Route path="/new-post" element={<ProtectedRoute Component={<NewPost />} />} />

        <Route path="/update-post/:id" element={<ProtectedRoute Component={<UpdatePost />} />} />
        <Route path="/calendar" element={<ProtectedRoute Component={<Calendar />} />} />
        <Route path="/market-calendar" element={<ProtectedRoute Component={<MarKetCalender />} />} />
        <Route path="/market-text" element={<ProtectedRoute Component={<MarketTextPage />} />} />

        <Route path="/add-Event" element={<ProtectedRoute Component={<AddEvent />} />} />

        <Route path="/report/close-calendar" element={<ProtectedRoute Component={<ClosingCalender />} />} />
        <Route path="/search-Event" element={<ProtectedRoute Component={<CalenderSearch />} />} />
        <Route path="/event-detail/:eventId" element={<ProtectedRoute Component={<EventDetail />} />} />
        <Route path="/event-roster/:eventId" element={<ProtectedRoute Component={<CalenderRoster />} />} />

        <Route path="/update-Event/:eventId" element={<ProtectedRoute Component={<UpdateEvent />} />} />
        <Route path="/add-transaction" element={<ProtectedRoute Component={<AddTransaction />} />} />
        <Route path="/transaction" element={<ProtectedRoute Component={<MyTransaction />} />} />
        <Route path="/new-transaction" element={<ProtectedRoute Component={<NewTransaction />} />} />


        <Route path="/transaction-summary/:id" element={<ProtectedRouteNew Component={<TransactionSummary />} />} />
        <Route path="/transaction-details-funding/:id?" element={<ProtectedRouteNew Component={<FundingRequest />} />} />
        <Route path="/transaction-details-funding-confirmation/:id?" element={<ProtectedRouteNew Component={<FundingConfirmation />} />} />
     
        <Route path="/transaction-review/:id" element={<ProtectedRouteNew Component={<TransactionReview />} />} />
        <Route path="/funding-income" element={<ProtectedRouteNew Component={<FundingIncome />} />} />
        <Route path="/close-calendar-count" element={<ProtectedRouteNew Component={<CloseCalenderCount />} />} />

        <Route path="/transactionDoc/:id/:itemId" element={<ProtectedRouteNew Component={<TransactionDoc />} />} />
        <Route path="/update-deposit/:id" element={<ProtectedRouteNew Component={<UpdateDeposit/>} />} />
        <Route path="/update-office/:id" element={<ProtectedRouteNew Component={<UpdateOffice/>} />} />
        <Route path="/deposit-add/:id/:depositId?" element={<ProtectedRouteNew Component={<DepositAdd/>} />} />
        {/* <Route path="/deposit-add/:id" element={<ProtectedRoute Component={<DepositAdd/>} />} /> */}
       
       
        <Route path="/admin/platformDetails" element={<ProtectedRoute Component={<PlatformDetails />} />} />
        <Route path="/add-question/:id?" element={<ProtectedRoute Component={<AddQuestion />} />} />
        <Route path="/add-staffRule" element={<ProtectedRoute Component={<AddStaffRule />} />} />


        <Route path="/question" element={<ProtectedRoute Component={<Question />} />} />
        <Route path="/add-linkfiles/:id?" element={<ProtectedRoute Component={<AddLinkFiles />} />} />
        <Route path="/quick-links" element={<ProtectedRoute Component={<QuickLinks />} />} />


        <Route path="/link" element={<ProtectedRoute Component={<Link />} />} />
        <Route path="/listing" element={<ProtectedRoute Component={<Listing />} />} />
        <Route path="/market-listing" element={<ProtectedRoute Component={<MarketListing />} />} />
        <Route path="/add-suggestFeature" element={<ProtectedRoute Component={<AddSuggestFeature />} />} />
        <Route path="/suggestFeature" element={<ProtectedRoute Component={<SuggestFeature/>} />} />
        <Route path="/onBoard-document" element={<ProtectedRoute Component={<MlsBoardDocument/>} />} />

        <Route path="/mylisting" element={<ProtectedRoute Component={<MyMLSlisting />} />} />
        <Route path="/real-estate-search" element={<ProtectedRoute Component={<RealEstateSearch />} />} />
        <Route path="/brochure-print-page" element={<ProtectedRoute Component={<RealStatePrintPage />} />} />
        <Route path="/brochure-mls-print" element={<ProtectedRoute Component={<BrochureMls />} />} />
        <Route path="/postcard-mls-print" element={<ProtectedRoute Component={<PostCards />} />} />
        <Route path="/propertycard-mls-print" element={<ProtectedRoute Component={<PropertyCards />} />} />
        <Route path="/propertylabel-mls-print" element={<ProtectedRoute Component={<PropertyLebel />} />} />
        <Route path="/mls-offer" element={<ProtectedRoute Component={<AgentsProperty />} />} />





        <Route path="/real-estate-print" element={<ProtectedRoute Component={<PrintEditor />} />} />
        <Route path="/print-markting" element={<ProtectedRoute Component={<PrintingMarkting />} />} />

        <Route path="/real-estate-non-listed-print" element={<ProtectedRoute Component={<NonListedPrint />} />} />

        <Route path="/add-utility" element={<ProtectedRoute Component={<AddUtilityTool />} />} />
        <Route path="/agent-utility" element={<ProtectedRoute Component={<UtilityTool />} />} />
        <Route path="/agent-utility-listing" element={<ProtectedRoute Component={<ListingUtility />} />} />


        <Route path="/single-listing-mls/:id" element={<ProtectedRoute Component={<SingleListingMLS />} />} />
        <Route path="/contactlisting/:type/:id?" element={<ProtectedRoute Component={<ContactListing />} />} />
        <Route path="/contacttransaction/:type" element={<ProtectedRoute Component={<ContactTransaction />} />} />
        <Route path="/filed-transaction/:type" element={<ProtectedRoute Component={<FiledTransaction />} />} />
        <Route path="/agentcontact/:type/:id?" element={<ProtectedRoute Component={<ContactagentTrans />} />} />

        <Route path="/single-listing/:id" element={<ProtectedRoute Component={<SingleListing />} />} />
        <Route path="/add-listing" element={<ProtectedRoute Component={<AddListing />} />} />

        <Route path="/t-property/:id" element={<ProtectedRouteNew Component={<Property />} />} />
        <Route path="/t-details" element={<ProtectedRouteNew Component={<Details />} />} />
        <Route path="/set-property/:id/:pid" element={<ProtectedRouteNew Component={<MlsPropertySet />} />} />

        <Route path="/uploadProperty/:id/:pid" element={<ProtectedRouteNew Component={<UploadProperty />} />} />
        <Route path="/select-property/:id" element={<ProtectedRouteNew Component={<SelectProperty />} />} />


        <Route path="/checklist" element={<ProtectedRouteNew Component={<CheckList />} />} />
        <Route path="/add-checklist/:id" element={<ProtectedRouteNew Component={<AddCheckList />} />} />
        <Route path="/manage-checklist/:id" element={<ProtectedRouteNew Component={<ManageCheckList />} />} />
        <Route path="/document/:id" element={<ProtectedRouteNew Component={<Documents />} />} />
        <Route path="/add-document/:id" element={<ProtectedRouteNew Component={<AddDocuments />} />} />
        
        <Route path="/add-document-rules" element={<ProtectedRoute Component={<AddDocumentRules />} />} />
        {/* <Route path="/attach-document" element={<ProtectedRoute Component={<AttachDocument />} />} /> */}
        <Route path="/t-dates/:id" element={<ProtectedRouteNew Component={<TransactionDate />} />} />
        <Route path="/expenses/:id" element={<ProtectedRouteNew Component={<Expenses />} />} />
        <Route path="/add-expenses/:id/:addExpence?" element={<ProtectedRouteNew Component={<AddExpenses />} />} />
        <Route path="/transact/detail/history/:id" element={<ProtectedRouteNew Component={<NotesHistory />} />} />
        <Route path="/add-note/:id" element={<ProtectedRouteNew Component={<AddNote />} />} />

        <Route path="/t-contact/:id" element={<ProtectedRouteNew Component={<TransactionContact />} />} />

        <Route path="/email-signature/:id?" element={<ProtectedRoute Component={<EmailSignature />} />} />
        <Route path="/add-signature/:id?" element={<ProtectedRoute Component={<StaffSignature />} />} />
        <Route path="/staff-signature/:id?" element={<ProtectedRoute Component={<StaffSignatureListing />} />} />
        
        <Route path="/contact" element={<ProtectedRoute Component={<Contact />} />} />
        <Route path="/contact-group" element={<ProtectedRoute Component={<AddContactGroup />} />} />
        <Route path="/contact-group-detail/:id" element={<ProtectedRoute Component={<ContactGroupSingle />} />} />
        <Route path="/contact-group-member-detail/:id" element={<ProtectedRoute Component={<AddGroupMember />} />} />
        <Route path="/group-member-listing/:id" element={<ProtectedRoute Component={<GroupMemberListing />} />} />



        <Route path="/add-contact" element={<ProtectedRoute Component={<AddContact />} />} />
        <Route path="/activate-account/" element={<ProtectedRoute Component={<ActivateAccount />} />} />
        <Route path="/contact-profile/:id" element={<ProtectedRoute Component={<ContactProfile />} />} />
        <Route path="/private-contact/:id" element={<ProtectedRoute Component={<PrivateContactEdit />} />} />
        <Route path="/community-contact/:id" element={<ProtectedRoute Component={<CommunityContact />} />} />
        <Route path="/thank-you/:id" element={<ProtectedRoute Component={<ThanksPage />} />} />
        <Route path="/thanks-page/:id" element={<ProtectedRoute Component={<VendorThanks />} />} />
        <Route path="/add-vendor" element={<ProtectedRoute Component={<AddVendor/>} />} />


        <Route path="/thanks/:id" element={<ProtectedRoute Component={<PrivateThanks />} />} />


        <Route path="/onboarding/:uid/:cid" element={<Onboarding/>} />
        <Route path="/vendor-onboarding/:id" element={<VendorOnboarding/>} />

        <Route path="/onboarding-resubmit/:uid/:cid/:did" element={<Onboardingresubmit/>} />
        <Route path="/onboard-thankyou" element={<OnboardingThankyou/>} />
        <Route path="/test-onboarding" element={<ProtectedRoute Component={<TestOnboarding />} />} />
        <Route path="/contact-reviews/:id" element={<ProtectedRoute Component={<Reviews />} />} />
        <Route path="/profile-info/" element={<ProtectedRoute Component={<ProfileInfo />} />} />
        <Route path="/contact-info/" element={<ProtectedRoute Component={<ContactInfo />} />} />
        <Route path="/profile-activity" element={<ProtectedRoute Component={<Activity />} />} />
        <Route path="/profile-setting" element={<ProtectedRoute Component={< Setting/>} />} />
        <Route path="/profile-account" element={<ProtectedRoute Component={<Account />} />} />
        <Route path="/profile" element={<ProtectedRoute Component={<Profile />} />} />
        <Route path="/learn" element={<ProtectedRoute Component={<Learn />} />} />

        <Route path="/profile-setting/calendar-subscription/:id" element={<ProtectedRoute Component={<CalenderSubscriptions />} />} />
        <Route path="/profile/settings/digest/:id?" element={ <DeailyDigest/> } />
        <Route path="/platform-documents" element={<ProtectedRoute Component={<PlatformDocuments />} />} />
        <Route path="/platform-checklist" element={<ProtectedRoute Component={<PlatformChecklist />} />} />
        <Route path="/add-checklist-rules" element={<ProtectedRoute Component={<AddChecklistRules />} />} />
        <Route path="/add-paperwork/:id?" element={<ProtectedRoute Component={<AddPaperwork />} />} />
        <Route path="/paperwork" element={<ProtectedRoute Component={<Paperwork />} />} />
        <Route path="/account-balance/:id" element={<ProtectedRoute Component={<AccountBalance />} />} />
        <Route path="/add-balance/:id/:addBalance?" element={<ProtectedRoute Component={<AddBalance />} />} />
        <Route path="/officeNote/:id/:officeId?" element={<ProtectedRoute Component={<OfficeNote />} />} />
        <Route path="/reports" element={<ProtectedRoute Component={<Reports />} />} />
        <Route path="/reports/transact/deposits" element={<ProtectedRoute Component={<DepositLedgerReport />} />} />
        <Route path="/reports/transact/notes" element={<ProtectedRoute Component={<NotesReport />} />} />
        <Route path="/reports/personal/expenses" element={<ProtectedRoute Component={<ExpenseLedgerReport />} />} />
        <Route path="/reports/office/assoc-mls" element={<ProtectedRoute Component={<AssocMLS />} />} />
        <Route path="/reports/office/production" element={<ProtectedRoute Component={<TransactionReport />} />} />
        <Route path="/bussiness-tab" element={<ProtectedRoute Component={<BussinessTab />} />} />
        <Route path="/bussiness-income" element={<ProtectedRoute Component={<BussinessIncome />} />} />
        <Route path="/bussiness-expences" element={<ProtectedRoute Component={<BussinessExpences />} />} />
        <Route path="/tasks" element={<ProtectedRoute Component={<AssocTasks />} />} />
        <Route path="/task/:id?" element={<ProtectedRoute Component={<Task />} />} />
        <Route path="/add-tasks" element={<ProtectedRoute Component={<AddAssocTasks />} />} />
        <Route path="/reports/office/assoc-licenses" element={<ProtectedRoute Component={<AssocLicenses />} />} />
        <Route path="/reports/office/assoc-added" element={<ProtectedRoute Component={<AssocAdded />} />} />
        <Route path="/reports/office/assoc-removed" element={<ProtectedRoute Component={<RemoveAssociate />} />} />
        <Route path="/reports/office/assoc-inactive" element={<ProtectedRoute Component={<InactiveAssociate />} />} />
        <Route path="/reports/office/onboarding-info" element={<ProtectedRoute Component={<OnBoardingInfo />} />} />
        <Route path="/reports/office/buyer-broker-aggrement" element={<ProtectedRoute Component={<BuyerAggrement />} />} />

        <Route path="/reports/office/associates" element={<ProtectedRoute Component={<AssociateHistory />} />} />
        <Route path="/reports/office/sponsorship" element={<ProtectedRoute Component={<SponsorAssociate />} />} />
        <Route path="/reports/office/agent-testimonials" element={<ProtectedRoute Component={<AgentTestimonials />} />} />
        <Route path="/reports/transact/first-transaction" element={<ProtectedRoute Component={<FirstTransaction />} />} />
        <Route path="/reports/transact/expired-listings/" element={<ProtectedRoute Component={<ExpireListing />} />} />
        <Route path="/reports/transact/documents/" element={<ProtectedRoute Component={<DocumentReview />} />} />
      
      
        <Route path="/reports/office/inapprovalassociates" element={<ProtectedRoute Component={<Inapprovalassociates />} />} />
        <Route path="/reports/transact/funding" element={<ProtectedRoute Component={<TransactFundingReport />} />} />
        <Route path="/control-panel" element={<ProtectedRoute Component={<ControlPanel />} />} />
        <Route path="/promote" element={<ProtectedRoute Component={<Promote />} />} />
        <Route path="/vender" element={<ProtectedRoute Component={<Vender />} />} />
        <Route path="/sub-vender/:id?" element={<ProtectedRoute Component={<UserSubVendor />} />} />

        <Route path="/transaction/openHouse/:id" element={<ProtectedRouteNew Component={<OpenHouse />} />} />
        <Route path="/hire-coordinator/:id" element={<ProtectedRouteNew Component={<HireTransaction />} />} />
        <Route path="/transaction/openHouse-listing/:id" element={<ProtectedRouteNew Component={<OpenHouseListing />} />} />
        <Route path="/promote/catalog-product/:id/:details?" element={<ProtectedRoute Component={<PromoteProduct />} />} />
        <Route path="/promote/catalog-details/:id?/:details?/:addProduct?" element={<ProtectedRoute Component={<PromoteDetails />} />} />

        <Route path="/admin-product-items/:id?/:details?" element={<ProtectedRoute Component={<AdminProductListing />} />} />
        <Route path="/add-markting/:id?/:details?" element={<ProtectedRoute Component={<AddAdminProduct />} />} />
        <Route path="/update-markting/:id?/:details?/:addProduct?" element={<ProtectedRoute Component={<UpdateAdminProduct />} />} />
      

        <Route path="/admin/promote" element={<ProtectedRoute Component={<AdminPromote />} />} />

        <Route path="/admin/sub-Vendor/:id?" element={<ProtectedRoute Component={<AdminSubVendor />} />} />
        <Route path="/add-subVendor/:id/:addProduct?" element={<ProtectedRoute Component={<AddAdminSubVendor />} />} />

        <Route path="/admin/catalogitems" element={<ProtectedRoute Component={<AdminCatalogitems />} />} />
        <Route path="/add-promotecatalog/:id?" element={<ProtectedRoute Component={<Addpromotocatalog />} />} />

        <Route path="/control-panel/locations/" element={<ProtectedRoute Component={<Organization />} />} />
        {/* <Route path="/contact/:id" element={<ProtectedRoute Component={<BusinessCard />} />} /> */}
        {/* <Route path="/control-panel/edit-businessCard/" element={<ProtectedRoute Component={<AddBusinessCard />} />} /> */}
        <Route path="edit-businessCard/:id?" element={<ProtectedRoute Component={<EditBusinessCard />} />} />


        <Route path="/control-panel/add-locations/:id?" element={<ProtectedRoute Component={<AddOrganization />} />} />
        <Route path="/control-panel/member-plan/" element={<ProtectedRoute Component={<MemberPlan />} />} />
        <Route path="/control-panel/member-plan/add/:id?" element={<ProtectedRoute Component={<AddMemberPlan />} />} />
        <Route path="/control-panel/documents/" element={<ProtectedRoute Component={<ManageShareDoc />} />} />
        <Route path="/control-panel/theme/" element={<ProtectedRoute Component={<ColorTheme />} />} />
        <Route path="/control-panel/logo/" element={<ProtectedRoute Component={<Logo />} />} />
        <Route path="/report-bug" element={<ProtectedRoute Component={<ReportBug />} />} />


        <Route path="/control-panel/homepage-photo/" element={<ProtectedRoute Component={< HomePagePhoto/>} />} />
        <Route path="/news-content" element={<ProtectedRoute Component={<NewsContent/>} />} />
        <Route path="/marketing-content/:id?" element={<ProtectedRoute Component={<MarketingContent/>} />} />
        <Route path="/marketing-content-listing" element={<ProtectedRoute Component={<MarketingContentListing/>} />} />

        <Route path="/flyer-content" element={<ProtectedRoute Component={<FlyerContent/>} />} />
        <Route path="/new-Feature" element={<ProtectedRoute Component={<NewFeatureRealse/>} />} />
        <Route path="/feature-Listing" element={<ProtectedRoute Component={<FeatureListing/>} />} />





        <Route path="/control-panel/documents/content/list/:id" element={<ProtectedRoute Component={<SinglePageContent />} />} />
        <Route path="/control-panel/documents/add-content/:id?" element={<ProtectedRoute Component={<AddPageContent />} />} />
        <Route path="/control-panel/documents/add-link/:id?/" element={<ProtectedRoute Component={<AddLink />} />} />
        <Route path="/control-panel/documents/content/edit/:id" element={<ProtectedRoute Component={<DocumentContentEdit />} />} />
        <Route path="/control-panel/documents/attach/:id?/" element={<ProtectedRoute Component={<AddAttachDocument />} />} />
        <Route path="/control-panel/add-section/:id?" element={<ProtectedRoute Component={<AddSection />} />} />
        <Route path="/control-panel/banners" element={<ProtectedRoute Component={<Banners />} />} />
        <Route path="/add-banner/:id?" element={<ProtectedRoute Component={<AddBanner />} />} />
        <Route path="/admin/invitecontacts" element={<ProtectedRoute Component={<Invitecontacts />} />} />
        <Route path="/admin/announce/:type?/:id?" element={<ProtectedRoute Component={<AnnounceMail />} />} />
        <Route path="/send/notice" element={<ProtectedRoute Component={<Noticemessage />} />} />
        <Route path="/notice/notification" element={<ProtectedRoute Component={<Notifications />} />} />
        <Route path="/contact/notice/:id?" element={<ProtectedRoute Component={<PastNotice />} />} />


        <Route path="/notice" element={<ProtectedRoute Component={<ContactNotice />} />} />
        <Route path="upcomingbirthday" element={<ProtectedRoute Component={<UpcomingBirthday />} />} />
        <Route path="newassociates" element={<ProtectedRoute Component={<NewAssociates />} />} />
        <Route path="add-reservations/:id?" element={<ProtectedRoute Component={<Reservations />} />} />
        <Route path="reservation-listing" element={<ProtectedRoute Component={<ReservationListing />} />} />
        <Route path="reservation-calender/:id" element={<ProtectedRoute Component={<ReservationSinglePage />} />} />
        <Route path="reservation-Schedule/:id" element={<ProtectedRoute Component={<ConfigureSchedule />} />} />
        <Route path="reservation-policy/:id" element={<ProtectedRoute Component={<AddPolicy />} />} />


        
        <Route path="/add-learn/:id?" element={<ProtectedRoute Component={<AddLearn />} />} />
        <Route path="/learnMedia/:type" element={<ProtectedRoute Component={<LearnMedia />} />} />
        <Route path="/videocategory/:type/:id?" element={<ProtectedRoute Component={<VideoCategory />} />} />
        <Route path="/videoWorkspaceOverview/:type/:id?" element={<ProtectedRoute Component={<VideoWorkSpace/>} />} />

        <Route path="/addvideocategory/:id?" element={<ProtectedRoute Component={<AddVideoCategory />} />} />
        <Route path="/workspaceview/:id?" element={<ProtectedRoute Component={<WorkspaceSinglePage />} />} />
        <Route path="/videotraningview/:id?" element={<ProtectedRoute Component={<VideoSinglePage />} />} />

        <Route path="/transaction-property/:id" element={<ProtectedRouteNew Component={<NewProperty />} />} />
        {/* <Route path="/property-summary" element={<ProtectedRouteNew Component={<NewPageProperty />} />} /> */}
        <Route path="/notifications/:id" element={<ProtectedRouteNew Component={<Notification />} />} />

        <Route path="/commissions/:id" element={<ProtectedRouteNew Component={<Commissions />} />} />


        <Route path="/contact/:id" element={<BusinessCard/>} />
        <Route path="/testimonial-page/:id?" element={<Testimonials />} />
        <Route path="/google-login" element={<ProtectedRoute Component={<GoogleLogin />} />} />


        <Route path="/testimonials-listing/:id?" element={<SinglePageTestimonials />} />

        <Route path="/testimonials-thanks" element={<EmptyRoute Component={<TestimonialsThanks />} />} />
      

        <Route path="/signin" element={<Signin/>} />
        <Route path="/signup" element={<Signup />} />
        <Route path="/account-setup/:uid/:cid" element={<AccountSetup/>} />
       
        <Route path="/landing-page/:id?" element={<NewRouteEmpty Component={<LandingPage />} />} />
        <Route path="/real-estate-search" element={<ProtectedRoute Component={<RealEstateSearch />} />} />
        {/* <Route path="/agent-meeting" element={<ProtectedRoute Component={  <GoogleOAuthProvider clientId={googleClientId}>
      <MeetingAgent />
    </GoogleOAuthProvider>} />} /> */}

   
        <Route path="/sign-up-invite/:id" element={<Signupinvite />} />
        
      </Routes>
      {/* </HelmetProvider> */}
      </LocalStorageProvider>
    </div>
  );
}

export default App;

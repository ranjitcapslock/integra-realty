const express = require('express');
const path = require('path');
const fetch = require('isomorphic-fetch');
const ejs = require('ejs');

const app = express();
const PORT = process.env.PORT || 3000;

const indexPath = path.resolve(__dirname, './build', 'index.html');

app.use(express.static(path.resolve(__dirname,  './build'), { maxAge: '30d' }));

app.get('/landing-page/:id', async (req, res) => {
    const postId = req.params.id;

    try {
        const fetchListingData = async (id) => {
            try {
                const response = await fetch(`https://api.brokeragentbase.com/listings/${id}`);
                if (!response.ok) {
                    throw new Error('Network response was not ok');
                }
                return await response.json();
            } catch (error) {
                console.error('Error fetching listing data:', error);
                return null;
            }
        };

   
        const post = await fetchListingData(postId);
        if (!post) {
            console.error('Post not found');
            return res.status(404).end('Post not found');
        }

    
        ejs.renderFile(indexPath, { post }, (err, htmlData) => {
            if (err) {
                console.error('Error during file rendering', err);
                return res.status(500).end();
            }

            htmlData = htmlData.replace(
                '<title>InDepth Realty</title>',
                `<title>${post.streetAddress}</title>`
            )
            .replace('InDepth Realty', post.streetAddress)
            .replace('InDepth Realty', `${post.streetAddress}, ${post.city}, ${post.state}, ${post.zipCode}`)
            .replace('InDepth Realty Image', post.image);

         
            res.set('Cache-Control', 'no-store');
            res.send(htmlData);
        });
    } catch (error) {
        console.error('Error fetching listing data:', error);
        return res.status(500).end();
    }
});

app.listen(PORT, () => {
    console.log(`Server is running on port ${PORT}`);
});

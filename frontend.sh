#!/bin/bash
cd /var/www/integra-realty
rm /var/www/integra-realty/package-lock.json
rm /var/www/integra-realty/yarn.lock
git pull origin main
yarn
yarn build
cp -a build/. /var/www/brokeragentbase.com/
service nginx restart

#!/bin/bash
cd /var/www/backend
rm /var/www/backend/package-lock.json
rm /var/www/backend/yarn.lock
git pull origin main
yarn
pm2 restart all --update-env
